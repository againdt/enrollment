/**
 * 
 */
package com.getinsured.hix.webservice.enrollment.endpoints;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.enrollment.service.AdminUpdateService;
import com.getinsured.hix.enrollment.service.EnrollmentService;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualResponse;

/**
 * @author panda_p
 *
 */
@Endpoint
public class AdminUpdateIndividualEndPoint {
	
	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/enrollment/adminUpdateIndividual";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminUpdateIndividualEndPoint.class);
	
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private UserService userService;
	@Autowired private AdminUpdateService adminUpdateService;
	
	@PayloadRoot(localPart = "adminUpdateIndividualRequest", namespace = TARGET_NAMESPACE)
	@ResponsePayload 
	public AdminUpdateIndividualResponse getAdminUpdateIndividual(@RequestPayload AdminUpdateIndividualRequest adminUpdateIndividualRequest)
	{
		LOGGER.info("IND 57 : AdminUpdateIndividualResponse Endpoint invoked.");
		AdminUpdateIndividualResponse adminUpdateIndividualResponse = new AdminUpdateIndividualResponse();

		try {
			LOGGER.info("============= START of IND 57 : Received AdminUpdate Individual Request details : =============");
			if(adminUpdateIndividualRequest.getHousehold()!=null){
				
				Integer enrollmentId =null;//(int) adminUpdateIndividualRequest.getHousehold().getEnrollmentId();
				Enrollment enrollment =null;
				List<Enrollment> enrollmentList = null;
				
				if(EnrollmentUtils.isNotNullAndEmpty(adminUpdateIndividualRequest.getHousehold().getEnrollmentId())){
					enrollmentId=(int) adminUpdateIndividualRequest.getHousehold().getEnrollmentId();
					enrollment= enrollmentService.findById(enrollmentId);
					if(enrollment != null){
						enrollmentList = new ArrayList<Enrollment>();
						enrollmentList.add(enrollment);
					}
				}else{
					LOGGER.info("IND 57 : Either Enrollment Id  is required ");
					adminUpdateIndividualResponse.setResponseCode("202");
					adminUpdateIndividualResponse.setResponseDescription("IND 57 : Either Enrollment Id is required");
					return adminUpdateIndividualResponse;
				}
				
				if(enrollmentList!=null && !enrollmentList.isEmpty()){
					AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
					Enrollment updatedEnrollment = null;
					for(Enrollment currentEnrollment:enrollmentList){
						updatedEnrollment = adminUpdateService.adminUpdateEnrollment(adminUpdateIndividualRequest,currentEnrollment, user, false);
						if(EnrollmentUtils.isNotNullAndEmpty(updatedEnrollment)){
							enrollmentService.saveEnrollment(updatedEnrollment);
						}
						updatedEnrollment = null;
					}
					
					adminUpdateIndividualResponse.setResponseCode("200");
					adminUpdateIndividualResponse.setResponseDescription("Success");
					LOGGER.info("IND 57 : Response formed successfully.");
					return adminUpdateIndividualResponse;
					
				}else{
					LOGGER.info("IND 57 : Incorrect Enrollment Id ");
					adminUpdateIndividualResponse.setResponseCode("202");
					adminUpdateIndividualResponse.setResponseDescription("IND 57 : Incorrect Enrollment Id ");
					return adminUpdateIndividualResponse;
				}
			}else{
				LOGGER.info("Called IND 57 with Null request");
				adminUpdateIndividualResponse.setResponseCode("201");
				adminUpdateIndividualResponse.setResponseDescription("Called IND 57 with Null request");
				return adminUpdateIndividualResponse;
			}
		} catch (GIException e) {
			LOGGER.error("GIException occured while processing IND 57 : AdminUpdateIndividualResponse  ");
			LOGGER.error("Error Message : " + e.getErrorMsg());
			LOGGER.error("Error Code : " + e.getErrorCode());
			
			adminUpdateIndividualResponse.setResponseCode(e.getErrorCode()>0?""+e.getErrorCode():"299");
			adminUpdateIndividualResponse.setResponseDescription(e.getMessage()!=null?e.getMessage():"Error occured while processing IND 57 ( Admin Update for Individual ) ");
			LOGGER.error(" ",e);
			return adminUpdateIndividualResponse;
		}catch (Exception e) {
			LOGGER.error("GIException occured while processing IND 57 : AdminUpdateIndividualResponse  ");
			LOGGER.error("Error Message : " + e.getMessage());
			LOGGER.error("Error Cause : " + e.getCause());
			
			adminUpdateIndividualResponse.setResponseCode("299");
			adminUpdateIndividualResponse.setResponseDescription("Error occured while processing IND 57 ( Admin Update for Individual ) " + e.getMessage());
			LOGGER.error(" " ,e);
			return adminUpdateIndividualResponse;
		}
		
		//return adminUpdateIndividualResponse;
	}

	
}
