/**
 * 
 */
package com.getinsured.hix.webservice.enrollment.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.enrollment.service.EnrollmentEmployerDisEnrollService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentRequest;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentResponse;
import com.thoughtworks.xstream.XStream;


/**
 * @author parhi_s
 *
 */
@Endpoint
public class EmployerDisEnrollmentEndPoint {
	
	
	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/enrollment/employerDisEnrollment";
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerDisEnrollmentEndPoint.class);
	
	@Autowired private EnrollmentEmployerDisEnrollService employerEnrollmentService;
	
	@PayloadRoot(localPart = "employerDisEnrollmentRequest", namespace = TARGET_NAMESPACE)
	@ResponsePayload 
	public EmployerDisEnrollmentResponse employerDisEnrollment(@RequestPayload EmployerDisEnrollmentRequest employerDisEnrollmentRequest)
	{
		LOGGER.info("EmployerDisEnrollmentRequest Endpoint invoked.");
		
		EmployerDisEnrollmentResponse employerDisEnrollmentResponse=new EmployerDisEnrollmentResponse();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		try{
			LOGGER.info("============= START of IND 29 : Received employerDisEnrollmentRequest  details : =============");
			LOGGER.info("" + xstream.toXML(employerDisEnrollmentRequest));
			employerDisEnrollmentResponse = employerEnrollmentService.employerDisEnrollment(employerDisEnrollmentRequest,false);
			LOGGER.info("============= End of IND 29 :EmployerDisEnrollment process ended Succesfully  : =============");
		   
		}catch (GIException e) {
			LOGGER.error("GIException occured while processing IND 29 :  Employer Disenrollment  ");
			LOGGER.error("Error Message : " + e.getErrorMsg());
			LOGGER.error("Error Code : " + e.getErrorCode());
			employerDisEnrollmentResponse.setResponseCode(""+e.getErrorCode());
			employerDisEnrollmentResponse.setResponseDescription(e.getErrorMsg());
			LOGGER.error(" ",e);
		}catch (Exception ex){
			LOGGER.error("Exception occured while processing IND 29 : Employer Disenrollment");
			LOGGER.error("Error Message : " + ex.getMessage());
			employerDisEnrollmentResponse.setResponseCode("299");
			employerDisEnrollmentResponse.setResponseDescription(" Error occured while processing IND 29 ( Employer Disenrollment)");
			LOGGER.error(" ",ex);
		}
		
		
		return employerDisEnrollmentResponse;
	}
	
}
