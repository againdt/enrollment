/**
 * 
 */
package com.getinsured.hix.webservice.enrollment.endpoints;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.enrollment.service.VerifyShopMecService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.VerifyNonESIMECRequest;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.VerifyNonESIMECResponse;
import com.thoughtworks.xstream.XStream;


/**
 * @author panda_p
 *
 */
@Endpoint
public class VerifyShopMecEndPoint {
	
	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/enrollment/VerifyShopMec";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyShopMecEndPoint.class);
	@Autowired private UserService userService;
	@Autowired private VerifyShopMecService verifyShopMecService;
	
	@PayloadRoot(localPart = "VerifyNonESIMECRequest", namespace = TARGET_NAMESPACE)
	@ResponsePayload 
	public VerifyNonESIMECResponse verifyShopMec(@RequestPayload VerifyNonESIMECRequest verifyNonESIMECRequest)
	{
		LOGGER.info("VerifyNonESIMECResponse Endpoint invoked.");
		VerifyNonESIMECResponse verifyNonESIMECResponse = new VerifyNonESIMECResponse();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		
		try {
			LOGGER.info("============= START of Verify Shop MEC service : Received VerifyNonESIMECRequest details : =============");
			LOGGER.info("" + xstream.toXML(verifyNonESIMECRequest));
			LOGGER.info("============= End of of Verify Shop MEC service : =============");
			if(verifyNonESIMECRequest != null && verifyNonESIMECRequest.getNonESIMECRequest() !=null){
								
				if(verifyNonESIMECRequest.getNonESIMECRequest().getNonESIMECindividualInformation() !=null){
					AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
					verifyNonESIMECResponse = verifyShopMecService.verifyShopMec(verifyNonESIMECRequest, user);
					return verifyNonESIMECResponse;					
				}
			}else{
				LOGGER.info("Verify Shop MEC service with Null request");
				//verifyNonESIMECResponse.setResponseCode("201");
				//verifyNonESIMECResponse.setResponseDescription("Called IND 57 with Null request");
				return verifyNonESIMECResponse;
			}
		} catch (GIException e) {
			LOGGER.error("GIException occured while processing Verify Shop MEC service");
			LOGGER.error("Error Message : " + e.getErrorMsg());
			LOGGER.error("Error Code : " + e.getErrorCode());			
			verifyNonESIMECResponse.getNonESIMECResponse().setResponseCode("4203");
			verifyNonESIMECResponse.getNonESIMECResponse().setResponseDescription("Unexpected System Error");
			LOGGER.error(" ",e);
			return verifyNonESIMECResponse;
		}catch (Exception e) {
			LOGGER.error("GIException occured while processing Verify Shop MEC service");
			LOGGER.error("Error Message : " + e.getMessage());
			LOGGER.error("Error Cause : " + e.getCause());
						verifyNonESIMECResponse.getNonESIMECResponse().setResponseCode("4203");
			verifyNonESIMECResponse.getNonESIMECResponse().setResponseDescription("Unexpected System Error");
			LOGGER.error(" " ,e);
			return verifyNonESIMECResponse;
		}
		
		return verifyNonESIMECResponse;
	}

	
}
