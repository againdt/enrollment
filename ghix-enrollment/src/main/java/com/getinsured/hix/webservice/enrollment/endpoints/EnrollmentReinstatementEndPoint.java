package com.getinsured.hix.webservice.enrollment.endpoints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.enrollment.service.EnrollmentReinstmtService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementRequest;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementResponse;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementSoapRequest;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementSoapResponse;

@Endpoint
public class EnrollmentReinstatementEndPoint {
	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/enrollment/enrollmentReinstatement";
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentReinstatementEndPoint.class);
	
	@Autowired private UserService userService;
	@Autowired private EnrollmentReinstmtService enrollmentReinstmtService;
	@PayloadRoot(localPart = "enrollmentReinstatementSoapRequest", namespace = TARGET_NAMESPACE)
	@ResponsePayload 
	public EnrollmentReinstatementSoapResponse enrollmentReinstatement(@RequestPayload EnrollmentReinstatementSoapRequest enrollmentReinstatementSoapRequest){
		LOGGER.info("EnrollmentReinstatementSoapRequest Endpoint invoked.");
		EnrollmentReinstatementSoapResponse enrollmentReinstatementSoapResponse =new EnrollmentReinstatementSoapResponse();
		EnrollmentReinstatementRequest enrollmentReinstatementRequest = new EnrollmentReinstatementRequest();
		EnrollmentReinstatementResponse enrollmentReinstatementResponse;
		if(enrollmentReinstatementSoapRequest.getEnrollments()==null || enrollmentReinstatementSoapRequest.getEnrollments().getEnrollmentId().isEmpty()){
			
			enrollmentReinstatementSoapResponse.setResponseCode(""+EnrollmentConstants.ERROR_CODE_201);
			enrollmentReinstatementSoapResponse.setResponseDescription(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID);
			return enrollmentReinstatementSoapResponse;
		}
		enrollmentReinstatementRequest.setEnrollmentIdList(enrollmentReinstatementSoapRequest.getEnrollments().getEnrollmentId());
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
		if(user != null){
			enrollmentReinstatementResponse = enrollmentReinstmtService.processReinstatementRequest(enrollmentReinstatementRequest, false, user);
		}
		else{
			enrollmentReinstatementResponse = new EnrollmentReinstatementResponse();
			enrollmentReinstatementResponse.setResponseCode(""+EnrollmentConstants.ERROR_CODE_209);
			enrollmentReinstatementResponse.setResponseDescription("No user found in system: "+GhixConstants.USER_NAME_EXCHANGE);
		}
		
		enrollmentReinstatementSoapResponse.setResponseCode(enrollmentReinstatementResponse.getResponseCode());
		enrollmentReinstatementSoapResponse.setResponseDescription(enrollmentReinstatementResponse.getResponseDescription());
		return enrollmentReinstatementSoapResponse;
		
	}

}
