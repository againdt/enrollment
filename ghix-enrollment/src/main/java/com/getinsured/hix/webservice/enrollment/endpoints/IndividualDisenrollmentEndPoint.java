/**
 * 
 */
package com.getinsured.hix.webservice.enrollment.endpoints;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.service.EnrollmentService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.webservice.enrollment.individualdisenrollment.IndividualDisenrollmentRequest.Enrollments.Enrollment;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.individualdisenrollment.IndividualDisenrollmentRequest;
import com.getinsured.hix.webservice.enrollment.individualdisenrollment.IndividualDisenrollmentResponse;
import com.getinsured.hix.webservice.enrollment.individualdisenrollment.IndividualDisenrollmentResponse.EnrollmentEventIds;
import com.getinsured.hix.webservice.enrollment.individualdisenrollment.IndividualDisenrollmentResponse.EnrollmentEventIds.EnrollmentEventId;
import com.thoughtworks.xstream.XStream;

/**
 * @author panda_p
 *
 */
@Endpoint
public class IndividualDisenrollmentEndPoint {
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private UserService userService;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/enrollment/individualDisenrollment";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualDisenrollmentEndPoint.class);
	
	@PayloadRoot(localPart = "individualDisenrollmentRequest", namespace = TARGET_NAMESPACE)
	@ResponsePayload 
	public IndividualDisenrollmentResponse individualDisEnrollment(@RequestPayload IndividualDisenrollmentRequest individualDisenrollmentRequest)
	{
		LOGGER.info("IndividualDisenrollmentRequest Endpoint invoked.");

		IndividualDisenrollmentResponse individualDisenrollmentResponse= new IndividualDisenrollmentResponse();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		try{
			LOGGER.info("============= START of IND56 : Received IndividualDisenrollmentRequest  details : =============");
			LOGGER.info("" + xstream.toXML(individualDisenrollmentRequest));


			individualDisEnrollmentService(individualDisenrollmentRequest, individualDisenrollmentResponse);

			LOGGER.info("============= End of IND56 : Received IndividualDisenrollmentRequest details : =============");
		}catch (Exception ex){
			if(individualDisenrollmentResponse.getResponseCode()==null){
				LOGGER.error("Exception occured while processing IND56 : Individual Disenrollment");
				LOGGER.error("Error Message : " + ex.getMessage());
				individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_299));
				individualDisenrollmentResponse.setResponseDescription(" Error occured while processing IND56 (Individual Disenrollment)");
				LOGGER.error(" ",ex);
			}

		}

		return individualDisenrollmentResponse;
	}
	
	/**
	 * 
	 * @param individualDisenrollmentRequest
	 * @return
	 */
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	private IndividualDisenrollmentResponse individualDisEnrollmentService(IndividualDisenrollmentRequest individualDisenrollmentRequest, IndividualDisenrollmentResponse individualDisenrollmentResponse) throws GIException{
		try{
			List<EnrollmentDisEnrollmentDTO> disEnrollmentDTOList= validateRequest(individualDisenrollmentRequest, individualDisenrollmentResponse);
			EnrollmentEventIds evetIds= new EnrollmentEventIds();
			List<EnrollmentEventId> responseEnrollmentId=evetIds.getEnrollmentEventId();
			if(disEnrollmentDTOList!=null && disEnrollmentDTOList.size()>0){
				List<com.getinsured.hix.model.enrollment.Enrollment> enrollmentList= new ArrayList<>();
				for(EnrollmentDisEnrollmentDTO  disEnrollmentDTO: disEnrollmentDTOList){
					disEnrollmentDTO.setInd56DisenrollmentCall(true);
					enrollmentList.addAll(enrollmentService.disEnrollEnrollment(disEnrollmentDTO, EnrollmentEvent.TRANSACTION_IDENTIFIER.ENROLLMENT_DISENROLL));
				}
				if(enrollmentList!=null){
					for (com.getinsured.hix.model.enrollment.Enrollment enrollment: enrollmentList){
						EnrollmentEventId enrEventMap= new EnrollmentEventId();
						enrEventMap.setEnrollmentId(enrollment.getId()+"");
						enrEventMap.setEventId(enrollment.getSubscriberForEnrollment().getLastEventId().getId()+"");
						responseEnrollmentId.add(enrEventMap);
					}
				}
			}
			individualDisenrollmentResponse.setEnrollmentEventIds(evetIds);
			individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_200));
			individualDisenrollmentResponse.setResponseDescription(GhixConstants.RESPONSE_SUCCESS);
		}catch(Exception e){
			if(individualDisenrollmentResponse.getResponseCode() ==null){
				LOGGER.error("Exception occured while processing IND56 : Individual Disenrollment");
				LOGGER.error("Error Message : " + e.getMessage());
				individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_299));
				individualDisenrollmentResponse.setResponseDescription(" Error occured while processing IND56 (Individual Disenrollment)");
				LOGGER.error(" ",e);
			}

		}
		return individualDisenrollmentResponse;

		}
	
	private List<EnrollmentDisEnrollmentDTO> validateRequest(IndividualDisenrollmentRequest individualDisenrollmentRequest,IndividualDisenrollmentResponse individualDisenrollmentResponse) throws GIException{
		List<EnrollmentDisEnrollmentDTO> disEnrollmentDTOList=null;
		AccountUser user = null;
		if(individualDisenrollmentRequest==null){
			LOGGER.error("Exception occured while processing IND56 : individualDisenrollmentRequest is null");
			individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_201));
			individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : individualDisenrollmentRequest is null");
			throw new GIException("Exception occured while processing IND56 : individualDisenrollmentRequest is null");
		}else if(individualDisenrollmentRequest.getEnrollments()==null ||individualDisenrollmentRequest.getEnrollments().getEnrollment()==null ||individualDisenrollmentRequest.getEnrollments().getEnrollment().size()==0){
			LOGGER.error("Exception occured while processing IND56 : enrollment list is null");
			individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_202));
			individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : enrollment list is null");
			throw new GIException("Exception occured while processing IND56 : enrollment list is null");
		}else {
			user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
			disEnrollmentDTOList= new ArrayList<>();
			for (Enrollment enrollment: individualDisenrollmentRequest.getEnrollments().getEnrollment()){
				String	enrollmentId = enrollment.getEnrollmentId();
				if(!EnrollmentUtils.isNotNullAndEmpty(enrollmentId)){
					LOGGER.error("Exception occured while processing IND56 : enrollment id is null");
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_203));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : enrollment id is null");
					throw new GIException("Exception occured while processing IND56 : enrollment id is null");
				}
				if(!EnrollmentUtils.isNotNullAndEmpty(enrollment.getTerminationDate())){
					LOGGER.error("Exception occured while processing IND56 : termination date is null for enrollment id : "+enrollmentId);
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_204));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : termination date is null for enrollment id : "+enrollmentId);
					throw new GIException("Exception occured while processing IND56 : termination date is null for enrollment id : "+enrollmentId);
				}
				if(!(DateUtil.isValidDate(enrollment.getTerminationDate(),GhixConstants.REQUIRED_DATE_FORMAT))){
					LOGGER.error("Exception occured while processing IND56 : termination date is not in required format for enrollment id : "+enrollmentId);
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_205));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : termination date is not in required format for enrollment id : "+enrollmentId);
					throw new GIException("Exception occured while processing IND56 : termination date is not in required format for enrollment id : "+enrollmentId);
				}
				if(!EnrollmentUtils.isNotNullAndEmpty(enrollment.getTerminationReasonCode())){
					LOGGER.error("Exception occured while processing IND56 : termination reason code is null for enrollment id : "+enrollmentId);
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_206));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : termination reason code is null for enrollment id : "+enrollmentId);
					throw new GIException("Exception occured while processing IND56 : termination reason code is null for enrollment id : "+enrollmentId);
				}
				if(enrollment.getTerminationReasonCode().equalsIgnoreCase(EnrollmentConstants.REASON_CODE_DEATH) && !EnrollmentUtils.isNotNullAndEmpty(enrollment.getDeathDate())){
					LOGGER.error("Exception occured while processing IND56 : death date is null when termination reason code is 03 for enrollment id : "+enrollmentId);
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_207));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : death date is null when termination reason code is 03 for enrollment id : "+enrollmentId);
					throw new GIException("Exception occured while processing IND56 : death date is null when termination reason code is 03 for enrollment id : "+enrollmentId);
				}
				if(enrollment.getTerminationReasonCode().equalsIgnoreCase(EnrollmentConstants.REASON_CODE_DEATH) && EnrollmentUtils.isNotNullAndEmpty(enrollment.getDeathDate()) && !(DateUtil.isValidDate(enrollment.getDeathDate(),GhixConstants.REQUIRED_DATE_FORMAT))) {
					LOGGER.error("Exception occured while processing IND56 : death date is not in required format for enrollment id : "+enrollmentId);
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_208));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : death date is not in required format for enrollment id : "+enrollmentId);
					throw new GIException("Exception occured while processing IND56 : death date is not in required format for enrollment id : "+enrollmentId);
				}
				if(!EnrollmentUtils.isNotNullAndEmpty(enrollment.getRetroaciveTerm())){
					LOGGER.error("Exception occured while processing IND56 : retro active term flag is null for enrollment id : "+enrollmentId);
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_209));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : retro active term flag is null for enrollment id : "+enrollmentId);
					throw new GIException("Exception occured while processing IND56 : retro active term flag is null for enrollment id : "+enrollmentId);
				}

				com.getinsured.hix.model.enrollment.Enrollment enrollmentDb= enrollmentRepository.findById(Integer.parseInt(enrollmentId));

				if(enrollmentDb==null){
					LOGGER.error("Exception occured while processing IND56 : no enrollment found for enrollment id : "+enrollmentId);
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_210));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : no enrollment found for enrollment id : "+enrollmentId);
					throw new GIException("Exception occured while processing IND56 : no enrollment found for enrollment id : "+enrollmentId);
				}else if(enrollmentDb.getEnrollmentStatusLkp()!=null && EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode())){
					LOGGER.error("Exception occured while processing IND56 : enrollment is already canceled for enrollment id : "+enrollmentId);
					individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_211));
					individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : enrollment is already canceled for enrollment id : "+enrollmentId);
					throw new GIException("Exception occured while processing IND56 : enrollment is already canceled for enrollment id : "+enrollmentId);
				}else if(enrollmentDb.getEnrollmentStatusLkp()!=null 
						 && (EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode())
							 || EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode()))
						){
					Date terminationDate = EnrollmentUtils.StringToEODDate(enrollment.getTerminationDate(),GhixConstants.REQUIRED_DATE_FORMAT);
					if(enrollment.getRetroaciveTerm().equalsIgnoreCase(EnrollmentConstants.FALSE)){
						LOGGER.error("Exception occured while processing IND56 : Retro Termination cannot be done on already terminated enrollment with retroaciveTerm flag set to false - enrollmentid : "+enrollmentId);
						individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_212));
						individualDisenrollmentResponse.setResponseDescription("Exception occured while processing IND56 : Retro Termination cannot be done on already terminated enrollment with retroaciveTerm flag set to false - enrollmentid : "+enrollmentId);
						throw new GIException("Exception occured while processing IND56 : Retro Termination cannot be done on already terminated enrollment with retroaciveTerm flag set to false - enrollmentid : "+enrollmentId);
					}
					else if(enrollment.getRetroaciveTerm().equalsIgnoreCase(EnrollmentConstants.TRUE) && 
							(terminationDate.before(enrollmentDb.getBenefitEffectiveDate())
									|| terminationDate.after(enrollmentDb.getBenefitEndDate())
									|| DateUtils.isSameDay(terminationDate, enrollmentDb.getBenefitEffectiveDate()))){
						LOGGER.error("Exception occured while processing IND56 : Retro Termination cannot be done on already terminated enrollment with retroaciveTerm flag set to true - enrollmentid : "+enrollmentId);
						individualDisenrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_213));
						individualDisenrollmentResponse.setResponseDescription(
								"Exception occured while processing IND56 : Retro Termination cannot be done on already terminated enrollment, - enrollmentid : "
										+ enrollmentId + " Given Termination Date -" + enrollment.getTerminationDate() +" is either before existing Enrollment's start date OR [after OR same date as existing Term Date.]");
						throw new GIException("Exception occured while processing IND56 : Retro Termination cannot be done on already terminated enrollment with retroaciveTerm flag set to true - enrollmentid : "+enrollmentId);
						
					}
				}
				EnrollmentDisEnrollmentDTO disEnrollmentDTO = new EnrollmentDisEnrollmentDTO();
				List<com.getinsured.hix.model.enrollment.Enrollment> enrollmentList= new ArrayList<>();
				enrollmentList.add(enrollmentDb);
				disEnrollmentDTO.setEnrollmentList(enrollmentList);
				disEnrollmentDTO.setTerminationDate(enrollment.getTerminationDate());
				disEnrollmentDTO.setTerminationReasonCode(enrollment.getTerminationReasonCode());
				disEnrollmentDTO.setDeathDate(enrollment.getDeathDate());
				/*HIX-100085 :: Start
				 * As part of this bug we allowed enrollee level retro termination all the time irrespective of flag
				 * Enrollmnet level retro termination validation is done before coming to this point of code
				 * if(enrollment.getRetroaciveTerm()!=null && enrollment.getRetroaciveTerm().equalsIgnoreCase(EnrollmentConstants.TRUE)){
					disEnrollmentDTO.setAllowRetroTermination(true);
				}else{
					disEnrollmentDTO.setAllowRetroTermination(false);
				}*/
				disEnrollmentDTO.setAllowRetroTermination(true);
				//HIX-100085 :: End
				disEnrollmentDTO.setUpdatedBy(user);
				disEnrollmentDTOList.add(disEnrollmentDTO);

			}

		}
		return disEnrollmentDTOList;
	}
	
	}
