package com.getinsured.hix.enrollment.controller;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.hix.enrollment.service.Enrollment1095Service;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;


@RestController
@Validated
@RequestMapping("/enrollment1095")
public class Enrollment1095Controller {
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095Controller.class);
	@Autowired private Enrollment1095Service enrollment1095Service;
	@Autowired private UserService userService;

	
	/**
	 * 
	 * @param householdCaseId
	 * @return
	 */
	@RequestMapping(value = "/fetchenrollment1095data", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<String> fetchEnrollment1095DataByHouseholdCaseId(@RequestBody @NotNull String householdCaseId){
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("ENROLLMENT_1095_REST_CONTROLLER:: Received request to fetch 1095 data by HouseholdCaseId: "+householdCaseId);
		}
		try{
			String response = enrollment1095Service.fetchEnrollment1095DataByHouseholdCaseId(householdCaseId);
			if(response != null){
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_1095_REST_CONTROLLER:: Exception occurred in fetchenrollment1095data API ",ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	/**
	 * 
	 * @param householdCaseId
	 * @param coverageYear
	 * @return
	 */
	@RequestMapping(value="/{householdCaseId}/resend1095", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Boolean> resendEnrollment1095(@PathVariable @NotNull @Min(1) final Integer householdCaseId, @RequestParam @NotNull @Pattern(regexp = "(^$|[0-9]{4})", message = "should be 4 digit number.") String coverageYear,
			@RequestBody @NotNull String phone){
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("ENROLLMENT_1095_REST_CONTROLLER:: Received request to resend 1095 data by HouseholdCaseId: "+householdCaseId +" Coverage year: "+coverageYear);
		}
		try{
			if(StringUtils.isBlank(phone) || (StringUtils.isNotBlank(phone) && (phone.length() != 10 || !NumberUtils.isNumber(phone)))){
				throw new RuntimeException("10 Digit Phone number required");
			}
			AccountUser accountUser = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
			if(accountUser != null){
				boolean isResend1095Success = enrollment1095Service.resendEnrollment1095(householdCaseId.toString(), Integer.parseInt(coverageYear), accountUser, phone);
				return new ResponseEntity<>(isResend1095Success, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_1095_REST_CONTROLLER:: Exception occurred in resend1095 API ",ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
