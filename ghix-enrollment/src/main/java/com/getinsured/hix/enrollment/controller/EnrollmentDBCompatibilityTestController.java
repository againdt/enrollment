package com.getinsured.hix.enrollment.controller;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

//import com.getinsured.hix.enrollment.repository.IEnrlPlanRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/testrepository")
public class EnrollmentDBCompatibilityTestController {

	@Autowired
	private WebApplicationContext webApplicationContext;
	//@Autowired private IEnrlPlanRepository iEnrlPlanRepository;
	private static final Logger LOGGER = Logger.getLogger(EnrollmentDBCompatibilityTestController.class);
	@Autowired
	private Gson platformGson;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/scanbypackagename", method = RequestMethod.POST)
	@ResponseBody
	public String scanByPackage(@RequestBody String packagePath) {
		RepositoryResponse response = new RepositoryResponse();
		if (StringUtils.isNotBlank(packagePath)) {
			try {
				List<Class> classesList = getClasses(packagePath);
				if (classesList != null && !classesList.isEmpty()) {
					List<TestResult> testResultList = new ArrayList<TestResult>();
					Map<String, Boolean> commonMethodMap = generateCommonMethodMap();
					Map<Class<?>, Object> parameterMap = populateSampleParameterMap();
					ExecutorService executor = Executors.newFixedThreadPool(10);
					List<Callable<TestResult>> taskList = new ArrayList<>();
					for (Class cla : classesList) {
						taskList.add(new EnrollmentDBTestThread(cla, commonMethodMap, parameterMap));
					}
					executor.invokeAll(taskList).stream().map(future -> {
						try {
							return future.get();
						} catch (Exception ex) {
							LOGGER.error("Exception occurred while fetching TestResult: " + ex.getMessage());
						}
						return null;
					}).filter(p -> p != null).forEach(e -> testResultList.add(e));

					executor.shutdown();
					response.setTestResultList(testResultList);
					response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					response.setStatus(GhixConstants.RESPONSE_SUCCESS);
				} else {
					response.setErrMsg("No classes found for repository scan: Package: " + packagePath);
					response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in scanByPackage: ", ex);
				response.setErrCode(EnrollmentConstants.ERROR_CODE_209);
				response.setErrMsg(EnrollmentUtils.getExceptionMessage(ex));
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} else {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			response.setErrMsg("Invalid package received in request for repository scanning");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}

		return platformGson.toJson(response);
	}
	
	@RequestMapping(value = "/scanbyclassname", method = RequestMethod.POST)
	@ResponseBody
	public String scanByClassName(@RequestBody String className) {
		RepositoryResponse response = new RepositoryResponse();
		if (StringUtils.isNotBlank(className)) {
			List<TestResult> testResultList = new ArrayList<TestResult>();
			try {
				Map<String, Boolean> commonMethodMap = generateCommonMethodMap();
				Map<Class<?>, Object> parameterMap = populateSampleParameterMap();
				testResultList.add(getTestResults(Class.forName(className), commonMethodMap, parameterMap));
				response.setTestResultList(testResultList);
				response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				response.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
			catch(Exception ex){
				LOGGER.error("Exception occurred in scanByClassName: className: "+className, ex);
				response.setErrCode(EnrollmentConstants.ERROR_CODE_209);
				response.setErrMsg(EnrollmentUtils.getExceptionMessage(ex));
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}else {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			response.setErrMsg("Invalid class name received in request for repository scanning");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(response);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private TestResult getTestResults(Class cla, final Map<String, Boolean> commonMethodMap,
			final Map<Class<?>, Object> parameterMap) {
		TestResult testResult = new TestResult();
		testResult.setClassName(cla.getSimpleName());
		System.out.println("Started: " + cla.getSimpleName());
		Map<String, String> failureMap = new HashMap<String, String>();
		Method[] declaredMethods = cla.getDeclaredMethods();
		if (declaredMethods != null && declaredMethods.length != 0) {
			testResult.setTotalMethodCount(declaredMethods.length);

		}
		Integer successCount = 0;
		Integer failureCount = 0;
		for (Method method : declaredMethods) {
			try {
				if (!commonMethodMap.containsKey(method.getName())) {
					Integer parameterCount = method.getParameterCount();
					if (parameterCount != null && parameterCount > 0) {
						Map<Integer, Object> parameterValueMap = new HashMap<Integer, Object>();
						Integer paramcount = 0;
						Type[] genericParameterTypes = method.getGenericParameterTypes();
						for (Type genericParameterType : genericParameterTypes) {
							System.out.println("Class Name: " + cla.getSimpleName() + " Method started: "
									+ method.getName() + "Parameter Type: " + genericParameterType);
							if (genericParameterType instanceof ParameterizedType) {
								ParameterizedType aType = (ParameterizedType) genericParameterType;
								Type[] parameterArgTypes = aType.getActualTypeArguments();
								for (Type parameterArgType : parameterArgTypes) {
									Class parameterArgClass = (Class) parameterArgType;
									System.out.println("parameterArgClass = " + parameterArgClass);
									parameterValueMap.put(paramcount, getGenericParameterType(
											((ParameterizedType) genericParameterType), parameterArgType));
								}
							} else if (parameterMap.containsKey(genericParameterType)) {
								parameterValueMap.put(paramcount, parameterMap.get(genericParameterType));
							} else {
								throw new GIException("Class Name: " + cla.getSimpleName() + " Method Name: "
										+ method.getName() + " Parameter Type not exists: " + genericParameterType);
							}
							paramcount++;
						}
						invokeMethod(parameterCount, method, parameterValueMap, webApplicationContext.getBean(cla));
					} else {
						invokeMethod(0, method, null, webApplicationContext.getBean(cla.getSimpleName()));
					}
					System.out.println("Method Ends: " + method.getName());
					successCount++;
				}
			} catch (Exception ex) {
				failureCount++;
				failureMap.put(method.getName(), EnrollmentUtils.getExceptionMessage(ex));
			}
		}
		testResult.setTotalSuccessCount(successCount);
		testResult.setTotalFailureCount(failureCount);
		testResult.setResponseList(failureMap);
		System.out.println("Ends: " + cla.getSimpleName());
		return testResult;
	}

	private void invokeMethod(Integer parameterCount, Method method, Map<Integer, Object> parameterMap,
			Object autowiredObject) throws InvocationTargetException, IllegalAccessException {
		switch (parameterCount) {
		case 0:
			method.invoke(autowiredObject);
			break;
		case 1:
			method.invoke(autowiredObject, parameterMap.get(0));
			break;
		case 2:
			method.invoke(autowiredObject, parameterMap.get(0), parameterMap.get(1));
			break;
		case 3:
			method.invoke(autowiredObject, parameterMap.get(0), parameterMap.get(1), parameterMap.get(2));
			break;
		case 4:
			method.invoke(autowiredObject, parameterMap.get(0), parameterMap.get(1), parameterMap.get(2),
					parameterMap.get(3));
			break;
		case 5:
			method.invoke(autowiredObject, parameterMap.get(0), parameterMap.get(1), parameterMap.get(2),
					parameterMap.get(3), parameterMap.get(4));
			break;
		case 6:
			method.invoke(autowiredObject, parameterMap.get(0), parameterMap.get(1), parameterMap.get(2),
					parameterMap.get(3), parameterMap.get(4), parameterMap.get(5));
			break;
		case 7:
			method.invoke(autowiredObject, parameterMap.get(0), parameterMap.get(1), parameterMap.get(2),
					parameterMap.get(3), parameterMap.get(4), parameterMap.get(5), parameterMap.get(6));
			break;
		case 8:
			method.invoke(autowiredObject, parameterMap.get(0), parameterMap.get(1), parameterMap.get(2),
					parameterMap.get(3), parameterMap.get(4), parameterMap.get(5), parameterMap.get(6),
					parameterMap.get(7));
			break;
		default:
			System.out.println("Not handled to invoke method with param count: " + parameterCount);
			break;
		}
	}

	@SuppressWarnings("rawtypes")
	private static List<Class> getClasses(String packageName) throws ClassNotFoundException, IOException {
		List<Class> classes = new ArrayList<Class>();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (classLoader != null) {
			String path = packageName.replace('.', '/');
			Enumeration<URL> resources = classLoader.getResources(path);
			List<File> dirs = new ArrayList<File>();
			while (resources.hasMoreElements()) {
				URL resource = resources.nextElement();
				dirs.add(new File(resource.getFile()));
			}

			for (File directory : dirs) {
				classes.addAll(findClasses(directory, packageName));
			}
		}
		return classes;
	}

	@SuppressWarnings("rawtypes")
	private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class> classes = new ArrayList<Class>();
		if (directory.exists()) {
			File[] files = directory.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					assert !file.getName().contains(".");
					classes.addAll(findClasses(file, packageName + "." + file.getName()));
				} else if (file.getName().endsWith(".class")) {
					classes.add(Class
							.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
				}
			}
		}
		return classes;
	}

	private Map<Class<?>, Object> populateSampleParameterMap() {
		Map<Class<?>, Object> parameterMap = new HashMap<Class<?>, Object>();

		List<Order> orderList = new ArrayList<Order>();

		Order order = new Order("id");
		orderList.add(order);
		Sort sort = new Sort(orderList);

		Employer employer = new Employer();
		employer.setId(1);

		Pageable pageable = new PageRequest(1, 1, null);

		parameterMap.put(Pageable.class, pageable);
		parameterMap.put(Integer.class, 18781);
		parameterMap.put(int.class, 18781);
		parameterMap.put(String.class, "01/01/2018");
		parameterMap.put(Date.class, DateUtil.StringToDate("01/01/2020", "MM/dd/yyyy"));
		parameterMap.put(Long.class, 1L);
		parameterMap.put(long.class, 1L);
		parameterMap.put(Float.class, 1.0f);
		parameterMap.put(Double.class, 1.0f);
		parameterMap.put(Sort.class, sort);
		parameterMap.put(Employer.class, employer);

		return parameterMap;
	}

	private Object getGenericParameterType(ParameterizedType parameterType, Type argType) {
		if (parameterType.getRawType().equals(List.class)) {
			if (argType.equals(String.class)) {
				List<String> statuslist = new ArrayList<String>();
				statuslist.add("PENDING");
				statuslist.add("CONFIRM");
				statuslist.add("TERM");
				return statuslist;
			} else if (argType.equals(Integer.class)) {
				List<Integer> idList = new ArrayList<Integer>();
				idList.add(1);
				idList.add(2);
				return idList;
			} else if (argType.equals(Long.class)) {
				List<Long> longList = new ArrayList<Long>();
				longList.add(1l);
				longList.add(2l);
				return longList;
			}
		} else if (parameterType.getRawType().equals(Set.class)) {
			if (argType.equals(String.class)) {
				Set<String> statusSet = new HashSet<String>();
				statusSet.add("PENDING");
				statusSet.add("CONFIRM");
				statusSet.add("TERM");
				return statusSet;
			} else if (argType.equals(Integer.class)) {
				Set<Integer> idSet = new HashSet<Integer>();
				idSet.add(1);
				idSet.add(2);
				return idSet;
			} else if (argType.equals(Long.class)) {
				Set<Long> longSet = new HashSet<Long>();
				longSet.add(1l);
				longSet.add(2l);
				return longSet;
			}
		}
		return null;
	}

	private Map<String, Boolean> generateCommonMethodMap() {
		Method[] commonMethod = null;
		//Method[] commonMethod = iEnrlPlanRepository.getClass().getMethods();
		HashMap<String, Boolean> commonMethodMap = new HashMap<String, Boolean>();
		for (Method method : commonMethod) {
			commonMethodMap.put(method.getName(), false);
		}
		return commonMethodMap;
	}

	public static class RepositoryResponse extends GHIXResponse {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		List<TestResult> testResultList;

		public List<TestResult> getTestResultList() {
			return testResultList;
		}

		public void setTestResultList(List<TestResult> testResultList) {
			this.testResultList = testResultList;
		}

	}

	public static class TestResult {
		private String className;
		private Map<String, String> responseList = new LinkedHashMap<String, String>();
		private Integer totalMethodCount;
		private Integer totalSuccessCount;
		private Integer totalFailureCount;

		public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		public Map<String, String> getResponseList() {
			return responseList;
		}

		public void setResponseList(Map<String, String> responseList) {
			this.responseList = responseList;
		}

		public Integer getTotalMethodCount() {
			return totalMethodCount;
		}

		public void setTotalMethodCount(Integer totalMethodCount) {
			this.totalMethodCount = totalMethodCount;
		}

		public Integer getTotalSuccessCount() {
			return totalSuccessCount;
		}

		public void setTotalSuccessCount(Integer totalSuccessCount) {
			this.totalSuccessCount = totalSuccessCount;
		}

		public Integer getTotalFailureCount() {
			return totalFailureCount;
		}

		public void setTotalFailureCount(Integer totalFailureCount) {
			this.totalFailureCount = totalFailureCount;
		}
	}

	@SuppressWarnings("rawtypes")
	public class EnrollmentDBTestThread implements Callable<TestResult> {
		Class cla;
		Map<String, Boolean> commonMethodMap;
		Map<Class<?>, Object> parameterMap;

		public EnrollmentDBTestThread(Class cla, Map<String, Boolean> commonMethodMap,
				Map<Class<?>, Object> parameterMap) {
			this.cla = cla;
			this.commonMethodMap = commonMethodMap;
			this.parameterMap = parameterMap;
		}

		@Override
		public TestResult call() throws Exception {
			return getTestResults(cla, commonMethodMap, parameterMap);
		}

	}
}