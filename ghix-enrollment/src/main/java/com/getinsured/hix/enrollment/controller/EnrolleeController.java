/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import java.util.Map;

import javax.xml.ws.http.HTTPException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberRequestDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipRepository;
import com.getinsured.hix.enrollment.service.EnrolleeService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import springfox.documentation.annotations.ApiIgnore;

/**
 * @author panda_p
 * @since 14/02/2013
 * 
 * This class handles all Enrollee related service requests  
 */

@Controller
@RequestMapping(value="/enrollee")
public class EnrolleeController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrolleeController.class);
	
	
	@Autowired private EnrolleeService enrolleeService;
	@Autowired private IEnrolleeRelationshipRepository enrolleeRelationshipRepository;
	@Autowired private Gson platformGson;
	
	
	/**
	 * @author panda_p
     * @since 14/02/2013
     * 
     * Returns EnrolleeResponse as XML string for Enrollee object on receiving correct enrollee Id and 
     * for incorrect  enrollee Id returns EnrolleeResponse as XML string with error status and message
     * 
	 * @param id
	 * @return
	 * @throws HTTPException
	 */
	@RequestMapping(value="/findbyid/{id}",method = RequestMethod.GET)
	@ResponseBody public String findEnrolleeById(@PathVariable("id") String id){
		LOGGER.info("============inside findEnrolleeById============");
//		LOGGER.info("Enrollee id received : "+id);
		EnrolleeResponse enrolleeResponse= new EnrolleeResponse();
		if(null==id || "0".equalsIgnoreCase(id)){
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}else{
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			Integer enrolleeId = Integer.valueOf(id);
			Enrollee  enrollee = enrolleeService.findById(enrolleeId);
			
			
			if(enrollee != null){
				Integer subscriberId = enrolleeService.findSubscriberIdByEnrollmentId(enrollee.getEnrollment().getId());
				EnrolleeRelationship  relationShipToSuscriber = enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrollee.getId(), subscriberId);

				if(relationShipToSuscriber != null && relationShipToSuscriber.getRelationshipLkp() != null){
					enrollee.setRelationshipToSubscriberLkp(relationShipToSuscriber.getRelationshipLkp());
				}
				enrolleeResponse.setEnrollee(enrollee);
				enrolleeResponse.setEnrollment(enrollee.getEnrollment());
				
			}
			
			else{
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLEE);
			}
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String response = xstream.toXML(enrolleeResponse);

		
		LOGGER.info("findEnrolleeById XML : "+ response);
		
		return response;
	}
	
	/**
	 * @author panda_p
     * @since 14/02/2013
     * 
     * Returns EnrolleeResponse as XML string with list of Enrollee object on receiving correct search criteria and 
     * for incorrect  search criteria returns EnrolleeResponse as XML string with error status and message
     * 
	 * @param id
	 * @return
	 * @throws HTTPException
	 */
	@ApiIgnore
	@RequestMapping(value="/searchEnrollee",method = RequestMethod.POST)
	@ResponseBody public String searchEnrollee(@RequestBody Map<String, Object> searchCriteria){
		LOGGER.info("============inside searchEnrollee============");
//		LOGGER.info("Enrollee search Criteria :  "+searchCriteria);
		
		Map<String, Object> enrolleesAndRecordCount = enrolleeService.searchEnrollee(searchCriteria);
		EnrolleeResponse enrolleeResponse= new EnrolleeResponse();
		
		if (enrolleesAndRecordCount!=null) {
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrolleeResponse.setSearchResultAndCount(enrolleesAndRecordCount);
		}else{
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RESULTS_FOUND);
		}
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String xml=xstream.toXML(enrolleeResponse);
		
		LOGGER.info("enrolleeSearch: "+xml);
		
		return xml;
	}
	
	/*@RequestMapping(value="/saveCarrierUpdatedDataResponse",method = RequestMethod.POST)
	@ResponseBody public String saveCarrierUpdatedDataResponse(@RequestBody List<SendUpdatedEnrolleeResponseDTO> inputval){
		LOGGER.info("============inside saveCarrierUpdatedDataResponse============");
		LOGGER.info("saveCarrierUpdatedDataResponse :  "+inputval);

		enrolleeService.saveCarrierUpdatedDataResponse(inputval);


		return "";
	}*/
	
	
	@RequestMapping(value="/findsubscriberbyenrollmentid/{id}",method = RequestMethod.GET)
	@ResponseBody public String findSubscriberByEnrollmentId(@PathVariable("id") String id){
		LOGGER.info("============inside findsubscriberbyenrollmentid ============");
		
//		LOGGER.info("Enrollment id received : "+id);
		EnrolleeResponse enrolleeResponse= new EnrolleeResponse();
		if(null == id || "0".equalsIgnoreCase(id)){
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}
		else{
			try{
			Integer enrollmentId = Integer.valueOf(id);
			Enrollee  enrollee = enrolleeService.findSubscriberbyEnrollmentIDId(enrollmentId);
			
				if(enrollee != null){
				Enrollee newEnrolleeObj = new Enrollee();
			
				newEnrolleeObj.setFirstName(enrollee.getFirstName());
				newEnrolleeObj.setLastName(enrollee.getLastName());
				newEnrolleeObj.setMiddleName(enrollee.getMiddleName());
				newEnrolleeObj.setSuffix(enrollee.getSuffix());
				newEnrolleeObj.setPreferredEmail(enrollee.getPreferredEmail());
					if(enrollee.getHomeAddressid()!=null){
				Location homeAddress = new Location();
				homeAddress.setAddress1(enrollee.getHomeAddressid().getAddress1());
				homeAddress.setAddress2(enrollee.getHomeAddressid().getAddress2());
				homeAddress.setCity(enrollee.getHomeAddressid().getCity());
				homeAddress.setZip(enrollee.getHomeAddressid().getZip());
				homeAddress.setState(enrollee.getHomeAddressid().getState());
				
				newEnrolleeObj.setHomeAddressid(homeAddress);
					}
				newEnrolleeObj.setPrimaryPhoneNo(enrollee.getPrimaryPhoneNo());
				newEnrolleeObj.setBirthDate(enrollee.getBirthDate());
				enrolleeResponse.setEnrollee(newEnrolleeObj);
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
				else{
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrolleeResponse.setErrMsg("No records Found");
			}
			}catch(Exception e){
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrolleeResponse.setErrMsg(" Error Occured : " + EnrollmentUtils.getExceptionMessage(e));
			}
			
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String xml=xstream.toXML(enrolleeResponse);
		return xml;
	}
	
	
	/**
	 * * This method is called to fetch list of enrollment's and enrollee's by passing
	 * parameters as EnrolleeRequest and return the success or failure message in the form of EnrolleeResponse.
	 * Excepts employeeId from the EnrollmentRequest
	 * 200 key is success message.
	 * 201 is error key for input enrolleeRequest is null.
	 * 202 is error key for input key employeeId is null.
	 * 203 is error key for "Fail to retrieve the enrollee".
	 * @param enrolleeRequestStr
	 * @return enrolleeResponse
	 */
	@RequestMapping(value="/findenrolleebyemployeeapplicationid",method = RequestMethod.POST)
	@ResponseBody public String findenrolleebyEmployeeapplicationid(@RequestBody String enrolleeRequestStr){
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrolleeResponse enrolleeResponse = findEnrolleeByApplicationId(enrolleeRequestStr, xstream);
		return xstream.toXML(enrolleeResponse);
	}

	/**
	 * * This method is called to fetch list of enrollment's and enrollee's by passing
	 * parameters as EnrolleeRequest and return the success or failure message in the form of EnrolleeResponse.
	 * Excepts employeeId from the EnrollmentRequest
	 * 200 key is success message.
	 * 201 is error key for input enrolleeRequest is null.
	 * 202 is error key for input key employeeId is null.
	 * 203 is error key for "Fail to retrieve the enrollee".
	 * @param enrolleeRequestStr
	 * @return enrolleeResponse
	 */
	@RequestMapping(value="/findenrolleebyapplicationid",method = RequestMethod.POST)
	@ResponseBody public String findenrolleebyapplicationid(@RequestBody String enrolleeRequestStr){
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrolleeResponse enrolleeResponse = findEnrolleeByApplicationId(enrolleeRequestStr, xstream);
		return xstream.toXML(enrolleeResponse);
	}
	
	
	/**
	 * @author panda_p
	 * @since 27-Jan-2015
	 * 
	 * This method is called to fetch list of enrollment's and enrollee's by passing
	 * parameters as EnrolleeRequest and return the success or failure message in the form of EnrolleeResponse.
	 * Excepts employeeId from the EnrollmentRequest
	 * 200 key is success message.
	 * 201 is error key for input enrolleeRequest is null.
	 * 202 is error key for input key employeeId is null.
	 * 203 is error key for "Fail to retrieve the enrollee".
	 * 204 No enrollment found for the given employee application Id
	 * 
	 * @param enrolleeRequestStr
	 * @return enrolleeResponse
	 */
	@RequestMapping(value="/findenrolleebyapplicationidjson",method = RequestMethod.POST)
	@ResponseBody public String findenrolleebyapplicationidjson(@RequestBody String enrolleeRequestStr){
		EnrolleeResponse enrolleeResponse = findEnrolleeByApplicationId(enrolleeRequestStr, null);
		return platformGson.toJson(enrolleeResponse);
	}
	
	
	/**
	 * 
	 * @param enrolleeRequestStr
	 * @param xstream
	 * @return
	 */
	private EnrolleeResponse findEnrolleeByApplicationId(final String enrolleeRequestStr, final XStream xstream) {
		EnrolleeResponse enrolleeResponse = new EnrolleeResponse();
		try {
			if (StringUtils.isNotBlank(enrolleeRequestStr)) {
				EnrolleeRequest enrolleeRequest = null;
				if (xstream != null) {
					enrolleeRequest = (EnrolleeRequest) xstream.fromXML(enrolleeRequestStr);
				} else {
					enrolleeRequest = platformGson.fromJson(enrolleeRequestStr, EnrolleeRequest.class);
				}
				if (enrolleeRequest != null) {
					enrolleeService.findEnrolleeByApplicationid(enrolleeRequest, enrolleeResponse);
				} else {
					enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLEE_REQUEST_IS_NULL);
				}
			} else {
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLEE_REQUEST_IS_NULL);
			}
		} catch (Exception ex) {
			LOGGER.warn("Exception occurred while calling service findEnrolleeByApplicationid ", ex);
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			enrolleeResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
		}
		return enrolleeResponse;
	}
	

	@RequestMapping(value="/getEnrolleeCoverageDetailsBeforeLastInvoice",method = RequestMethod.POST)
	@ResponseBody public String getEnrolleeCoverageDetailsBeforeLastInvoice(@RequestBody String enrolleeRequestStr){
		return enrolleeService.getEnrolleeCoverageDetailsBeforeLastInvoice(enrolleeRequestStr);
	}
	
	/**
	 * Jira Id: HIX-100544
	 * @param enrolleeRequestString
	 * @return
	 */
	@RequestMapping(value="/fetchenrollmentsubscriberdata",method= RequestMethod.POST)
	@ResponseBody public String fetchEnrollmentSubscriberData(@RequestBody String enrollmentSubscriberRequestDTOString){
		EnrolleeResponse enrolleeResponse = new EnrolleeResponse();
		enrolleeResponse.startResponse();
		try{
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTOString)){
				EnrollmentSubscriberRequestDTO enrollmentSubscriberRequestDTO = platformGson.fromJson(enrollmentSubscriberRequestDTOString, EnrollmentSubscriberRequestDTO.class);
				if(enrollmentSubscriberRequestDTO != null){
					enrolleeService.fetchEnrollmentSubscriberData(enrollmentSubscriberRequestDTO, enrolleeResponse);
				}
			}
			else{
				enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENROLLEE_CONTROLLER:: fetchEnrolleeData, Exception occurred: ", ex);
			enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
		}
		enrolleeResponse.endResponse();
		return platformGson.toJson(enrolleeResponse);
	}
}
