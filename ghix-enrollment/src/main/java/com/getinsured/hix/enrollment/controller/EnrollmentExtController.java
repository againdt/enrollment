/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.exception.RestfulException;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeExtDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeExtRelationshipDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentExtDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentExtResponseDTO;
import com.getinsured.hix.dto.enrollment.aca.*;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.service.EnrollmentExtService;
import com.getinsured.hix.enrollment.util.EnrollmentGIPayloadUtil;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Enrollment ACA Express controller
 * 
 * @author negi_s
 * @since 11/09/2017
 */
@Controller
@RequestMapping(value = "/ext")
public class EnrollmentExtController {

	private static final Logger LOGGER = Logger.getLogger(EnrollmentExtController.class);

	@Autowired
	private EnrollmentExtService enrollmentExtService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired private GIWSPayloadService giwsPayloadService;
	@Autowired private Gson platformGson;

	private static final String ERR_MSG_MISSING_SSAP_APPL = "No SSAP Application ID found in the request";
	private static final String ERR_MSG_MISSING_HIOS_ID = "HIOS Plan ID missing in the request";
	private static final String ERR_MSG_NO_MEMBERS = "No members found in the request";
	private static final String ERR_MSG_ENRL_FAILURE = "Failure in creating enrollment";
	private static final String ERR_MSG_ENRL_FAILURE_TAG = "Failure in creating enrollment on ACA Request";
	private static final String ERR_MSG_INVALID_API_KEY = "API key is not valid";
	private static final String ERR_MSG_NO_SUBSCRIBER = "No subscriber found in the request";
	private static final String ERR_MSG_MISSING_FFM_USERNAME = "FFM user name missing in the request";
	private static final String ERR_MSG_MISSING_NPN = "NPN missing in the request";
	private static final String ERR_MSG_INVALID_REQUEST_OR_MISSING_REQUIRED_FIELDS = "Invalid request or Missing required fields or Field mapping failed";
	private static String ACADATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String CONTENT_TYPE = "application/json";

	@RequestMapping(value = "/welcome/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String welcome(@PathVariable String id) {
		LOGGER.info("Welcome to EnrollmentExtController welcome()");
		return "Welcome to EnrollmentExtController welcome()";
	}


	@RequestMapping(value = "/enrollment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<EnrollmentExtResponseDTO> createEnrollment(@RequestBody EnrollmentExtDTO enrollmentExtDTO, HttpServletRequest request)
			throws GIException {
		EnrollmentExtResponseDTO enrollmentResponse = new EnrollmentExtResponseDTO();
		GIWSPayload giWsPayloadObj = null;
		try {
			giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService, platformGson.toJson(enrollmentExtDTO), null,
					"ghix-enrollment/ext/enrollment", "ACA-ENROLLMENT-CREATION", false);

			if (isMembersValid(enrollmentExtDTO, enrollmentResponse)) {
				enrollmentResponse = enrollmentExtService.createAcaEnrollment(enrollmentExtDTO, null);
				if (enrollmentResponse != null && EnrollmentPrivateConstants.ERROR_CODE_200 == enrollmentResponse.getResponseCode()) {
					giWsPayloadObj.setResponsePayload(platformGson.toJson(enrollmentResponse));
					EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj, giWsPayloadObj.getResponsePayload(), true);

					/** UPDATE SSAP APPLICATION STAGE and status */
					if (enrollmentExtDTO.getSsapApplicationId() != null) {
						updateApplicationStage(Long.valueOf(enrollmentExtDTO.getSsapApplicationId()), STAGE.ENROLLMENT_COMPLETED);
						updateConsumerApplicationById(Long.valueOf(enrollmentExtDTO.getSsapApplicationId()), EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING);
					}
				}
			} else if (enrollmentResponse.getResponseCode() == EnrollmentPrivateConstants.ERROR_CODE_206) {
				throw new RestfulException(ERR_MSG_NO_MEMBERS, HttpStatus.EXPECTATION_FAILED);
			}
		} catch (Exception e) {
			LOGGER.error(ERR_MSG_ENRL_FAILURE, e);
			if (StringUtils.isBlank(e.getMessage())) {
				throw new RestfulException(ERR_MSG_ENRL_FAILURE, HttpStatus.INTERNAL_SERVER_ERROR, ERR_MSG_ENRL_FAILURE_TAG, e);
			} else {
				throw new RestfulException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, ERR_MSG_ENRL_FAILURE_TAG, e);
			}
		}

		return new ResponseEntity<>(enrollmentResponse, HttpStatus.OK);
	}

	@Deprecated
	@RequestMapping(value = "/createenrollment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<EnrollmentExtResponseDTO> createEnrollment(@RequestBody String enrollmentAcaRequestStr, HttpServletRequest request) {
		LOGGER.info("Inside createEnrollment EnrollmentExtController");
		EnrollmentExtResponseDTO enrollmentResponse = new EnrollmentExtResponseDTO();
		EnrollmentExtDTO enrollmentExtDTO = new EnrollmentExtDTO();
		GIWSPayload giWsPayloadObj =null; 
		try {
			
			giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService,enrollmentAcaRequestStr, null,"ghix-enrollment/ext/createenrollment", 
															"ACA-ENROLLMENT-CREATION",false);
			
			EnrollmentAcaRequestDTO enrollmentAcaRequestDTO = platformGson.fromJson(enrollmentAcaRequestStr,EnrollmentAcaRequestDTO.class);
			
			if (null != enrollmentAcaRequestDTO) {
				
				ResponseData responseData = platformGson.fromJson(enrollmentAcaRequestDTO.getEnrollment().getResponseData(),ResponseData.class);
				if(null != responseData && null != responseData.getPolicyInfo() &&  !responseData.getPolicyInfo().isEmpty()) {
					List<PolicyInfo> policyInfos = responseData.getPolicyInfo();
					PolicyInfo policy = policyInfos.get(0);
					List<InsuredPeople> insuredPeoples = responseData.getInsuredPeople();
					
					String apiKey = request.getHeader("apiKey");
					if (isRequestValid(enrollmentAcaRequestDTO, enrollmentResponse, apiKey, policy)) {
						try{
							enrollmentExtDTO = prepareEnrollmentExtDTO(enrollmentAcaRequestDTO, insuredPeoples, policy);
						} catch (Exception e) {
							LOGGER.error("Invalid Response data", e);
							setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_208, e.getMessage());
							throw new RestfulException(enrollmentResponse.getResponseMsg(), HttpStatus.BAD_REQUEST);
						}
					}else {
						if(enrollmentResponse.getResponseCode()==EnrollmentPrivateConstants.ERROR_CODE_201) {
							throw new RestfulException(ERR_MSG_INVALID_API_KEY, HttpStatus.BAD_REQUEST);
						}else if(enrollmentResponse.getResponseCode()==EnrollmentPrivateConstants.ERROR_CODE_202) {
							throw new RestfulException(ERR_MSG_MISSING_SSAP_APPL, HttpStatus.EXPECTATION_FAILED);
						}else if(enrollmentResponse.getResponseCode()==EnrollmentPrivateConstants.ERROR_CODE_203) {
							throw new RestfulException(ERR_MSG_MISSING_HIOS_ID, HttpStatus.EXPECTATION_FAILED);
						}
					}
				}
			}else {
				setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_250, ERR_MSG_INVALID_REQUEST_OR_MISSING_REQUIRED_FIELDS);
				throw new RestfulException(ERR_MSG_INVALID_REQUEST_OR_MISSING_REQUIRED_FIELDS, HttpStatus.BAD_REQUEST);
			}
			
			/**
			 * Call Service to Save Enrollment
			 */
			if (isMemberDataValid(enrollmentExtDTO, enrollmentResponse)) {
				enrollmentResponse = enrollmentExtService.createAcaEnrollment(enrollmentExtDTO, giWsPayloadObj != null ? giWsPayloadObj.getId(): null);
				if(enrollmentResponse!=null && EnrollmentPrivateConstants.ERROR_CODE_200 == enrollmentResponse.getResponseCode()) {
					giWsPayloadObj.setResponsePayload(platformGson.toJson(enrollmentResponse));
					EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj,giWsPayloadObj.getResponsePayload() , true);
					
					/**UPDATE SSAP APPLICATION STAGE and status*/
					if( enrollmentExtDTO.getSsapApplicationId()!=null){
						updateApplicationStage(Long.valueOf(enrollmentExtDTO.getSsapApplicationId()), STAGE.ENROLLMENT_COMPLETED);
						updateConsumerApplicationById(Long.valueOf(enrollmentExtDTO.getSsapApplicationId()), EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING);
					}
				}
			}else {
				if(enrollmentResponse.getResponseCode()==EnrollmentPrivateConstants.ERROR_CODE_206) {
					throw new RestfulException(ERR_MSG_NO_MEMBERS, HttpStatus.EXPECTATION_FAILED);
				}else if(enrollmentResponse.getResponseCode()==EnrollmentPrivateConstants.ERROR_CODE_207) {
					throw new RestfulException(ERR_MSG_NO_SUBSCRIBER, HttpStatus.EXPECTATION_FAILED);
				}
			}
			
			
		}catch (RestfulException rfe) {
			LOGGER.error(ERR_MSG_ENRL_FAILURE, rfe);
			//setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_209, ERR_MSG_ENRL_FAILURE);
			throw rfe;
		}
		catch (Exception gie) {
			LOGGER.error(ERR_MSG_ENRL_FAILURE, gie);
			setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_209, ERR_MSG_ENRL_FAILURE);
			throw new RestfulException(enrollmentResponse.getResponseMsg(), HttpStatus.INTERNAL_SERVER_ERROR,ERR_MSG_ENRL_FAILURE_TAG, gie);
		}
		return new ResponseEntity<>(enrollmentResponse, HttpStatus.OK);
	}
	
	private boolean isRequestValid(EnrollmentAcaRequestDTO enrollmentAcaRequestDTO, EnrollmentExtResponseDTO enrollmentResponse,
			String requestApiKey, PolicyInfo policy) {
		boolean isValid = true;
		String apiKey = DynamicPropertiesUtil.getPropertyValue(EnrollmentPrivateConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_ACA_API_KEY);
		if (EnrollmentUtils.isNullOrEmpty(apiKey) || EnrollmentUtils.isNullOrEmpty(requestApiKey)
				|| !apiKey.trim().equalsIgnoreCase(requestApiKey.trim())) {
			setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_201, ERR_MSG_INVALID_API_KEY);
			isValid = false;
		} else if (!NumberUtils.isNumber(enrollmentAcaRequestDTO.getExternalKey())) {
			setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_202, ERR_MSG_MISSING_SSAP_APPL);
			isValid = false;
		} else if (EnrollmentUtils.isNullOrEmpty(policy.getEnrollmentGroup().getSelectedInsurancePlan().getInsurancePlanStandardComponentIdentification().getIdentificationID().getValue())) {
			setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_203, ERR_MSG_MISSING_HIOS_ID);
			isValid = false;
		} 
		
		return isValid;
	}

	private boolean isMemberDataValid(EnrollmentExtDTO enrollmentExtDTO, EnrollmentExtResponseDTO enrollmentResponse) {
		boolean isValid = true;
		if (null == enrollmentExtDTO.getMembers() || enrollmentExtDTO.getMembers().isEmpty()) {
			setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_206, ERR_MSG_NO_MEMBERS);
			isValid = false;
		} else {
			for (EnrolleeExtDTO member : enrollmentExtDTO.getMembers()) {
				if (EnrollmentPrivateConstants.Y.equalsIgnoreCase(member.getSubscriberIndicator())) {
					return isValid;
				}
			}
			setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_207, ERR_MSG_NO_SUBSCRIBER);
			isValid = false;
		}
		return isValid;
	}

	private void setErrorResponse(EnrollmentExtResponseDTO enrollmentResponse, int errorCode, String errMsg) {
		LOGGER.error(errMsg);
		enrollmentResponse.setResponseCode(errorCode);
		enrollmentResponse.setResponseMsg(errMsg);
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
	}

	private List<EnrolleeExtDTO> getMembers(List<InsuredPeople> insuredPeoples , String prefEmail, String ratingArea, final String primaryPhoneNumber) {
		List<EnrolleeExtDTO> members = new ArrayList<>();
		for (InsuredPeople people : insuredPeoples) {
			EnrolleeExtDTO member = new EnrolleeExtDTO();
			member.setPreferredEmail(prefEmail);
			member.setMemberId(people.getId());
			member.setFirstName(people.getPersonName().getPersonGivenName().getValue());
			member.setLastName(people.getPersonName().getPersonSurName().getValue());
			member.setDob(people.getPersonBirthDate().getDate().getValue());
			member.setGender(getPersonGender(people.getPersonSexCode().getValue()));
			member.setRatingArea(ratingArea);
			if(null != people.getPersonName().getPersonMiddleName())
				member.setMiddleName(people.getPersonName().getPersonMiddleName().getValue());
			if(null != people.getPersonName().getPersonNameSuffixText())
				member.setSuffix(people.getPersonName().getPersonNameSuffixText().getValue());
			if(people.getPersonSSNIdentification().getIdentificationID().getValue()!=null) {
				member.setSsn(people.getPersonSSNIdentification().getIdentificationID().getValue().replaceAll("-", ""));
			}
			
			List<EnrolleeExtRelationshipDTO> relationships = new ArrayList<>();
			for (PersonAssociation personAccociation : people.getPersonAugmentation().getPersonAssociation()) {
				if (personAccociation.getPersonReference().get(0).getRef().equalsIgnoreCase(member.getMemberId())
						&& personAccociation.getFamilyRelationshipCode().getValue() == 18) {
					member.setSubscriberIndicator("Y");
					member.setRelationshipToHcp(""+personAccociation.getFamilyRelationshipCode().getValue());
					break;
				}
			}
			if ("Y".equalsIgnoreCase(member.getSubscriberIndicator())) {
				for (PersonAssociation personAccociation : people.getPersonAugmentation().getPersonAssociation()) {
					EnrolleeExtRelationshipDTO relationshipDTO = new EnrolleeExtRelationshipDTO();
					relationshipDTO.setMemberId(personAccociation.getPersonReference().get(0).getRef());
					relationshipDTO.setRelationshipCode(getRelationShip(personAccociation.getFamilyRelationshipCode().getValue()));
					relationships.add(relationshipDTO);
				}
				member.setRelationships(relationships);
				member.setPrimaryPhoneNumber(primaryPhoneNumber);
			}
			/**setting RelationshipToHcp for dependent members*/
			for (PersonAssociation personAccociation : people.getPersonAugmentation().getPersonAssociation()) {
				if (!"Y".equalsIgnoreCase(member.getSubscriberIndicator())) {
					member.setRelationshipToHcp(getRelationShip(personAccociation.getFamilyRelationshipCode().getValue()));
				}
			}
			members.add(member);
		}
		return members;
	}

	private EnrollmentExtDTO prepareEnrollmentExtDTO(EnrollmentAcaRequestDTO enrollmentAcaRequestDTO,
													List<InsuredPeople> insuredPeoples, 
													PolicyInfo policy) throws Exception{
		
		EnrollmentExtDTO enrollmentExtDTO = new EnrollmentExtDTO();
		String prefEmail = enrollmentAcaRequestDTO.getApp().getEmail();
		String primaryContactInfo = enrollmentAcaRequestDTO.getApp().getPhone();
		
		if(enrollmentAcaRequestDTO.getApp().getStateCode()!=null) {
			enrollmentExtDTO.setStateCode(enrollmentAcaRequestDTO.getApp().getStateCode());
		}
		enrollmentExtDTO.setPlanCsLevel(enrollmentAcaRequestDTO.getApp().getCsr());
		enrollmentExtDTO.setIssuerName(enrollmentAcaRequestDTO.getEnrollment().getCarrier());
		enrollmentExtDTO.setPlanName(enrollmentAcaRequestDTO.getEnrollment().getPlanName());
		enrollmentExtDTO.setExchangeAssignPolicyNo(Integer.toString(enrollmentAcaRequestDTO.getEnrollment().getPolicyNo()));

		if (null != enrollmentAcaRequestDTO.getExternalKey()) {
			enrollmentExtDTO.setSsapApplicationId(enrollmentAcaRequestDTO.getExternalKey());
		}
		if(policy.getAffectedInsurancePremium()!=null){
			enrollmentExtDTO.setPremium(policy.getAffectedInsurancePremium().getInsurancePremiumAmount().getValue());
			enrollmentExtDTO.setAptc(policy.getAffectedInsurancePremium().getInsurancePremiumElectedAPTCAmount().getValue());
			if((enrollmentExtDTO.getPlanCsLevel()==null || "".equalsIgnoreCase(enrollmentExtDTO.getPlanCsLevel())) 
					&& policy.getAffectedInsurancePremium().getInsurancePlanVariantCategoryNumericCode() !=null) {
				enrollmentExtDTO.setPlanCsLevel(policy.getAffectedInsurancePremium().getInsurancePlanVariantCategoryNumericCode().getValue());
			}
			
			enrollmentExtDTO.setRatingArea(policy.getAffectedInsurancePremium().getRateArea().getRateAreaIdentification().getIdentificationID().getValue());
		}
		
		enrollmentExtDTO.setBenefitEffectiveDate(policy.getActionEffectiveDate().getDate().getValue());
		
		
		if(policy.getEnrollmentGroup().getSelectedInsurancePlan()!=null){
			/**Setting CSR level*/
			if(enrollmentExtDTO.getPlanCsLevel()==null || "".equalsIgnoreCase(enrollmentExtDTO.getPlanCsLevel())) {
				enrollmentExtDTO.setPlanCsLevel(policy.getEnrollmentGroup().getSelectedInsurancePlan().getInsurancePlanVariant()
								.getInsurancePlanVariantIdentification().getIdentificationID().getValue());
			}
			if(enrollmentExtDTO.getPlanCsLevel() !=null && enrollmentExtDTO.getPlanCsLevel().length()<2) {
				enrollmentExtDTO.setPlanCsLevel("0"+enrollmentExtDTO.getPlanCsLevel());
			}
			/**Setting hios Plan ID*/
			enrollmentExtDTO.setHiosPlanId(policy.getEnrollmentGroup().getSelectedInsurancePlan().getInsurancePlanStandardComponentIdentification().getIdentificationID().getValue());
			/**Adding CSR level to plan_id if missing*/
			if(enrollmentExtDTO.getHiosPlanId()!=null && enrollmentExtDTO.getHiosPlanId().length()==14) {
				enrollmentExtDTO.setHiosPlanId(policy.getEnrollmentGroup().getSelectedInsurancePlan()
											.getInsurancePlanStandardComponentIdentification().getIdentificationID().getValue()
											+ enrollmentExtDTO.getPlanCsLevel());
			}
		}
		
		enrollmentExtDTO.setFfmUserName(enrollmentAcaRequestDTO.getUserSettings().getFfmUserName());
		
		enrollmentExtDTO.setSubmittedToCarrierDate(new TSDate().toString());
		/**Setting Member level info*/
		enrollmentExtDTO.setMembers(getMembers(insuredPeoples, prefEmail, enrollmentExtDTO.getRatingArea(), primaryContactInfo));
		
		Integer cmrHouseHoldCaseID = getHouseHoldInfo(enrollmentExtDTO);
		if (null != cmrHouseHoldCaseID) {
			enrollmentExtDTO.setHouseholeCaseId(Integer.toString(cmrHouseHoldCaseID));	
		}
		
		Integer capAgentId = getCapAgentIdByHouseHoldId(cmrHouseHoldCaseID);
		if (null != capAgentId) {
			enrollmentExtDTO.setCapAgentId(Integer.toString(capAgentId));
		}
		
		try {
			/**Setting plan meta data*/
			String effDate = DateUtil.dateToString(DateUtil.StringToDate(enrollmentExtDTO.getBenefitEffectiveDate(), ACADATEFORMAT), "yyyy-MM-dd");
			PlanResponse planResponse = getPlanInfoByHiosPlanId(enrollmentExtDTO.getHiosPlanId(), effDate);
			if(planResponse!=null &&  planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
				enrollmentExtDTO.setPlanId(planResponse.getPlanId());
				enrollmentExtDTO.setIssuerId(planResponse.getIssuerId());
				if(!EnrollmentUtils.isNotNullAndEmpty(enrollmentExtDTO.getIssuerName())) {
					enrollmentExtDTO.setIssuerName(planResponse.getIssuerName());
				}
				if(!EnrollmentUtils.isNotNullAndEmpty(enrollmentExtDTO.getPlanName())) {
					enrollmentExtDTO.setPlanName(planResponse.getPlanName());
				}
				enrollmentExtDTO.setPlanLevel(planResponse.getPlanLevel());
				enrollmentExtDTO.setInsuranceTypeLabel(planResponse.getInsuranceType());
			}
		}catch(Exception ex) {
			LOGGER.error("Date Parsing error : "+EnrollmentUtils.getExceptionMessage(ex));
		}
		
		return enrollmentExtDTO;
	}

	private int getHouseHoldInfo(EnrollmentExtDTO enrollmentExtDTO) throws Exception{
		SsapApplicationDTO ssapApplicationDTO = null;

		ObjectMapper mapper = new ObjectMapper();
		String requestSsapApplication = null;

		try {
			requestSsapApplication = mapper.writeValueAsString(enrollmentExtDTO.getSsapApplicationId());
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.FIND_SSAP_APPLICATION_BY_ID, requestSsapApplication,
																String.class);
			if (response != null) {
				ssapApplicationDTO = mapper.readValue(response, SsapApplicationDTO.class);
			}else{
				LOGGER.error("Invalid externalKey !! No match found for the provided application id :: " + enrollmentExtDTO.getSsapApplicationId());
				throw new Exception("Invalid externalKey !! No match found for the provided application id");
			}
		} catch (Exception e) {
			LOGGER.error("Error in fetching SSAP Application for ssap application :: " + enrollmentExtDTO.getSsapApplicationId());
			throw new Exception("Invalid externalKey !! No match found for the provided application id");
		}

		return ssapApplicationDTO.getCmrHouseholdId();
	}

	private Integer getCapAgentIdByHouseHoldId(int cmrHouseHoldCaseId) {
		Household householdDTO = null;

		ObjectMapper mapper = new ObjectMapper();
		String requestSsapApplication = null;

		try {
			requestSsapApplication = mapper.writeValueAsString(cmrHouseHoldCaseId);
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID,
					requestSsapApplication, String.class);
			if (response != null) {
				XStream xstream = GhixUtils.getXStreamStaxObject();
				ConsumerResponse consumer = (ConsumerResponse) xstream.fromXML(response);
				householdDTO = consumer.getHousehold();
			}
		} catch (Exception e) {
			LOGGER.error("Error in fetching household for household ID :: "+ cmrHouseHoldCaseId);
		}
		return householdDTO.getServicedBy();
	}

	private String getPersonGender(int code) {
		if (code == 1)
			return "M";
		else if (code == 2)
			return "F";
		else
			return "U";
	}

	private String getRelationShip(int relationShipCode) {
		if (relationShipCode == 18)
			return "18";
		else if (relationShipCode == 1)
			return "01";
		return "19";
	}
	
	

	private void updateApplicationStage(long ssapApplicationId, STAGE stage){
		try {
			LOGGER.info("UPDATE SSAP APPLICATION STAGE updateApplicationStage()");
			SsapApplicationDTO ssapApplicationDTO = new SsapApplicationDTO();
			ssapApplicationDTO.setId(ssapApplicationId);
			ssapApplicationDTO.setApplicationStage(stage);
			int ssapApplicationResponse = ghixRestTemplate.postForObject(ConsumerServiceEndPoints.UPDATE_STAGE_BY_SSAP_APPLICATION_BY_ID,ssapApplicationDTO, Integer.class);
			LOGGER.info("SSAP APPLICATION RESPONSE ::" + ssapApplicationResponse);
		}catch(Exception e) {
			LOGGER.error("UPDATE SSAP APPLICATION STAGE failed :  \n"+EnrollmentUtils.getExceptionMessage(e));
		}
		
		
	}
	
	private void updateConsumerApplicationById(Long ssapApplicationId, String enrollmentStatus) {
		SsapApplicationDTO ssapApplicationDTO = new SsapApplicationDTO();
		ssapApplicationDTO.setId(ssapApplicationId);
		ssapApplicationDTO.setEnrollmentStatus(enrollmentStatus);
		try{
			ghixRestTemplate.postForObject(ConsumerServiceEndPoints.UPDATE_CONSUMER_APPLICATION_BY_ID,ssapApplicationDTO,String.class);
		}
		catch(Exception e){
			LOGGER.error("EnrollmentExtController.updateConsumerApplicationById - Error in updating enrollment status for SSAP Application Id="+ssapApplicationDTO.getId() , e);
		}
	}
	
	
	/**
 	 * @author panda_p
 	 * @since 31/10/2017
 	 * @param hiosPlanId
 	 * @param effDate
 	 * @return
 	 */
	private PlanResponse getPlanInfoByHiosPlanId(String hiosPlanId, String effDate) {
		PlanRequest planRequest = new PlanRequest();
		PlanResponse planResponse= null;
		planRequest.setHiosPlanId(hiosPlanId);
		planRequest.setEffectiveDate(effDate);
		GIWSPayload newGiWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService, platformGson.toJson(planRequest), 
				null,"plan/getPlanInfoByHiosPlanId", 
				"PLAN_INFO_BY_HIOSPLANID",false);
		try{
			String str = ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_INFO_BY_HIOS_PLAN_ID, planRequest,String.class);
			
			if(str!=null &&  !str.isEmpty()) {
				planResponse = platformGson.fromJson(str, PlanResponse.class);
				if(planResponse!=null && planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, newGiWsPayloadObj, str , true);
					return planResponse;
				}else {
					throw new Exception("Bad or Failure response received from PlanMgmt API getPlanInfoByHiosPlanId");
				}
			}else {
				throw new Exception("Null or Empty response received from PlanMgmt API getPlanInfoByHiosPlanId");
			}
		}
		catch(Exception e){
			LOGGER.error("EnrollmentExtController.getPlanInfoByHiosPlanId - Error from PlanMgmt API for Plan: ="+planRequest.getHiosPlanId() +" And EffDate : "+planRequest.getEffectiveDate(), e);
		}
		
		return planResponse;
	}
	
	private boolean isMembersValid(EnrollmentExtDTO enrollmentExtDTO, EnrollmentExtResponseDTO enrollmentResponse) {
		boolean isValid = true;
		if (null == enrollmentExtDTO.getMembers() || enrollmentExtDTO.getMembers().isEmpty()) {
			setErrorResponse(enrollmentResponse, EnrollmentPrivateConstants.ERROR_CODE_206, ERR_MSG_NO_MEMBERS);
			isValid = false;
		}
		return isValid;
	}
}
