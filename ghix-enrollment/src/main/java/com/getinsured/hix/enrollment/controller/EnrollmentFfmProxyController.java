package com.getinsured.hix.enrollment.controller;

import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNull;
import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeFfmProxyDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentFfmProxyDTO;
import com.getinsured.hix.dto.ffm.FFMEnrollmentMemberData;
import com.getinsured.hix.dto.ffm.FFMEnrollmentRequest;
import com.getinsured.hix.dto.ffm.FFMEnrollmentResponse;
import com.getinsured.hix.enrollment.repository.IEnrollmentPrivateRepository;
import com.getinsured.hix.enrollment.service.EnrollmentFfmProxyConfirmEmailNotificationService;
import com.getinsured.hix.enrollment.service.EnrollmentFfmProxyService;
import com.getinsured.hix.enrollment.service.EnrollmentPrivateService;
import com.getinsured.hix.enrollment.service.ExitSurveyEmailNotificationService;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.thoughtworks.xstream.XStream;

/*@Api(value="enrollment-Ffm-Proxy-Controller", description="Enrollment FFM Proxy Controller")*/
@Controller
@RequestMapping(value="/enrlproxy")
public class EnrollmentFfmProxyController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentFfmProxyController.class);
	
	@Autowired private EnrollmentFfmProxyService enrollmentFfmProxyService;
	@Autowired private RestTemplate restTemplate;
	@Autowired private EnrollmentPrivateService enrollmentService;
	@Autowired private LookupService lookupService;
	@Autowired private IEnrollmentPrivateRepository enrollmentRepository;
//	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private EnrollmentFfmProxyConfirmEmailNotificationService emailService;
	@Autowired private ExitSurveyEmailNotificationService exitSurveyEmailNotificationService;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	private static final int ERROR_CODE_104 = 104;
	private static final int ERROR_CODE_105 = 105;
	private static final int ERROR_CODE_106 = 106;
	private static final String CHANGE_IN_PREMIUM = "changeInPremium";
	private static final String LOGGER_ERR_FFM_MSG = " Problem in FFM call : Putting Enrollment to ABORT State  ";
	private static final String LOGGER_ERR_MSG_106 = " Failed to updated enrollment for change in premium due to :   ";
	
	/*@ApiOperation(value="Welcome", notes="", httpMethod="GET", produces="text/plain")*/
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to EnrollmentFfmProxyController welcome()");
		
		/*XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		EnrollmentFfmProxyDTO enrollmentFfmProxyDTO = new EnrollmentFfmProxyDTO();
		enrollmentFfmProxyDTO.setCmrHouseholdId(12343);
		enrollmentFfmProxyDTO.setSsapApplicationId(new Long(1233));
		enrollmentFfmProxyDTO.setPlanId(604);
		enrollmentFfmProxyDTO.setStateExchangeCode("GA");
		enrollmentFfmProxyDTO.setPartnerAssignedConsumerId("5779ab1e3d9e4c529ced6d9bab018ac6");
		
		enrollmentFfmProxyDTO.setFfeAssignedConsumerId("97178341");
		//enrollmentFfmProxyDTO.setMemberActionEffectiveDate("2014-04-01 00:00:00.0");
		enrollmentFfmProxyDTO.setPremiumTotalPremiumAmount(new Float(754.51));
		enrollmentFfmProxyDTO.setPremiumAPTCAppliedAmount(new Float(218.0));
		enrollmentFfmProxyDTO.setPremiumTotalIndividualResponsibilityAmount(new Float(536.51));
		enrollmentFfmProxyDTO.setPremiumCSRLevelApplicable("01");
		enrollmentFfmProxyDTO.setFfeUserId("exadmin@gmail.com");
		enrollmentFfmProxyDTO.setUiaFirstName("Sid");
		enrollmentFfmProxyDTO.setUiaLastName("Negi");
		enrollmentFfmProxyDTO.setPremiumRatingArea("3");
		
		enrollmentFfmProxyDTO.setEnrollmentGroupStartDate("2014-04-01 00:00:00.0");
		enrollmentFfmProxyDTO.setEnrollmentGroupEndDate("2014-04-01 00:00:00.0");
		enrollmentFfmProxyDTO.setEnrollmentGroupIssuerId("37001");
		
		List<EnrolleeFfmProxyDTO> enrolleeProxyList = new ArrayList<EnrolleeFfmProxyDTO>();
		EnrolleeFfmProxyDTO proxyEnrollee = new EnrolleeFfmProxyDTO();
		proxyEnrollee.setInsuranceAplicantId("InsuranceApplicant1");
		proxyEnrollee.setFfmPersonId("Person1");
		proxyEnrollee.setFirstName("Dear");
		proxyEnrollee.setMiddleName(null);
		proxyEnrollee.setLastName("Maymda");
		proxyEnrollee.setMemberIssuerAssignedMemberId("45597417094740680");
		proxyEnrollee.setMemberFFEAssignedApplicantId("45597417094740680");
		proxyEnrollee.setSsn("1234314");
		proxyEnrollee.setMemberSubscriberIndicator(Boolean.TRUE);
		proxyEnrollee.setMemberRelationshipToSubscriber("18");
		proxyEnrollee.setMemberTobaccoUseIndicator(Boolean.FALSE);
		
		proxyEnrollee.setDob("1955-09-06 00:00:00.0");
		proxyEnrollee.setMemberActionEffectiveDate("2014-04-01 00:00:00.0");
		proxyEnrollee.setGender("M");
		
		enrolleeProxyList.add(proxyEnrollee);
		enrollmentFfmProxyDTO.setEnrolleeList(enrolleeProxyList);
	
		String responseString = ghixRestTemplate.postForObject(EnrollmentEndPoints.CREATE_FFM_PROXY_ENROLLMENT, xstream.toXML(enrollmentFfmProxyDTO), String.class);*/
		
		return "Welcome to EnrollmentFfmProxyController welcome()";
	}
	
	/*@ApiOperation(value="Create FFM enrollment for proxy", notes="", httpMethod="POST", produces="application/xml")*/
	@RequestMapping(value = "/createFfmProxyEnrollment", method = RequestMethod.POST)
	@ResponseBody
	public String createProxyEnrollment(@RequestBody String enrollmentFfmProxyDtoStr ) {
		LOGGER.info("Inside createProxyEnrollment Request");
		
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		Enrollment enrollment = null;
		EnrollmentFfmProxyDTO enrollmentFfmProxyDTO = (EnrollmentFfmProxyDTO) xstream.fromXML(enrollmentFfmProxyDtoStr);
		if(enrollmentFfmProxyDTO !=null && (enrollmentFfmProxyDTO.getSsapApplicationId() !=null) && (null != enrollmentFfmProxyDTO.getFlow())){
			
			enrollment = enrollmentRepository.findBySsapApplicationId(enrollmentFfmProxyDTO.getSsapApplicationId());
			if(!isNotNullAndEmpty(enrollment)){
				LOGGER.info("No Enrollment found for given ssapApplicationId : "+enrollmentFfmProxyDTO.getSsapApplicationId() +", Proceeding to create a new Enrollment");
				enrollmentResponse = enrollmentFfmProxyService.createEnrollment(enrollmentFfmProxyDTO);	
				if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					enrollment = enrollmentResponse.getEnrollment();
				}else{
					enrollmentResponse = new EnrollmentResponse();
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
					enrollmentResponse.setErrCode(ERROR_CODE_104);
					enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ENROLLMENT_CREATE_FAILURE);
					LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_ENROLLMENT_CREATE_FAILURE);
					return xstream.toXML(enrollmentResponse);
				}
			}else{
				//This block is implemented based on HIX-57780
				enrollment.setGrossPremiumAmt(enrollmentFfmProxyDTO.getPremiumTotalPremiumAmount());
				enrollment.setAptcAmt(enrollmentFfmProxyDTO.getPremiumAPTCAppliedAmount());
				enrollment.setNetPremiumAmt(enrollmentFfmProxyDTO.getPremiumTotalIndividualResponsibilityAmount());
				try {
					enrollment = enrollmentFfmProxyService.updateReattemptEnrollment(enrollment, enrollmentFfmProxyDTO.getTargetId());
				} catch (Exception e) {
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
					enrollmentResponse.setErrCode(ERROR_CODE_106);
					enrollmentResponse.setErrMsg(LOGGER_ERR_MSG_106 + e.getMessage());
					LOGGER.error(LOGGER_ERR_MSG_106,e);
					return xstream.toXML(enrollmentResponse);
				}
			}
			if(EnrollmentFfmProxyDTO.Flow.FFM_PROXY.equals(enrollmentFfmProxyDTO.getFlow())){
				enrollment.setGiHouseholdId(enrollmentFfmProxyDTO.getPartnerAssignedConsumerId());
				enrollment.setEnrollmentReason(new Character('I'));
				enrollmentResponse = sendEnrollmentDetailsToFFM(enrollment , enrollmentFfmProxyDTO);
				
				if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					LOGGER.info("FFM call successfully done :  "+ enrollmentResponse.getErrMsg());
					enrollmentResponse = new EnrollmentResponse();
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					//enrollmentResponse.setEnrollment(enrollmentResponse.getEnrollment());
				}
				else if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
					LOGGER.error(LOGGER_ERR_FFM_MSG+ enrollmentResponse.getErrMsg());
					enrollmentService.abortEnrollment(enrollment);
					updateConsumerApplicationById(enrollment.getSsapApplicationid(),EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED);
					enrollmentResponse.setEnrollmentId(enrollment.getId());
					enrollmentResponse.setEnrollment(null);
					enrollmentResponse.setfFMEnrollmentResponse(null);
					enrollmentResponse.setGiAndFfmDataList(null);
				}
			}else if(EnrollmentFfmProxyDTO.Flow.CA_PROXY.equals(enrollmentFfmProxyDTO.getFlow())){
				LOGGER.info("CA Proxy enrollment successfully created");
				enrollmentResponse = new EnrollmentResponse();
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		
		}else{
			LOGGER.error("Request is null or ssap application id or flow information not received");
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
			enrollmentResponse.setErrCode(ERROR_CODE_105);
			enrollmentResponse.setErrMsg("Request is null or ssap application id or flow information not received");
		}
		
		String apiResponseStr = xstream.toXML(enrollmentResponse);
		LOGGER.info("Call to Enrollment Proxy Successful :: " + apiResponseStr);
		return apiResponseStr;
	}
	
	private EnrollmentResponse sendEnrollmentDetailsToFFM(Enrollment enrollmentObj, EnrollmentFfmProxyDTO enrollmentFfmProxyDTO) {

		XStream xStream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse =  null;
		try{
			FFMEnrollmentRequest populateFFMFields = populateFFMFieldsFromEnrollment(enrollmentObj , enrollmentFfmProxyDTO);
			LOGGER.info("===Calling HUB APPLICANT_ENROLLMENT_URL===");
			final String postResp = restTemplate.postForObject(GhixEndPoints.FfmEndPoints.APPLICANT_ENROLLMENT_URL,xStream.toXML(populateFFMFields), String.class);
			enrollmentResponse = (EnrollmentResponse) xStream.fromXML(postResp);
			FFMEnrollmentResponse fFMEnrollmentResponse = new FFMEnrollmentResponse();
			fFMEnrollmentResponse = enrollmentResponse.getfFMEnrollmentResponse();

			if(isNotNull(enrollmentResponse) && (isNotNullAndEmpty(enrollmentResponse.getStatus()) && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))){

				if(isNotNull(fFMEnrollmentResponse) && (isNotNullAndEmpty(fFMEnrollmentResponse.getResponseCode()) && fFMEnrollmentResponse.getResponseCode().equalsIgnoreCase("E-008"))){
					Integer enrollmentId = fFMEnrollmentResponse.getEnrollmentId();
					Enrollment  enrollment = enrollmentService.findById(enrollmentId);
					enrollmentResponse.setEnrollment(enrollment);	
					enrollmentResponse.setModuleStatusCode(fFMEnrollmentResponse.getResponseCode());
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setGiAndFfmDataList(fFMEnrollmentResponse.getGiAndFfmDataList());
					enrollmentResponse.setErrMsg("Applicant Enrollment  : There is a difference in the FFM and the GI premium amount");
				}
				else{
					enrollmentResponse.setErrMsg("Applicant Enrollment  : Try again Error !");
				}
				return enrollmentResponse;
			}

			if(fFMEnrollmentResponse.getEnrollmentId()==null || fFMEnrollmentResponse.getEnrollmentId()==0){
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
			}
			else{
				Integer enrollmentId = fFMEnrollmentResponse.getEnrollmentId();
				Enrollment  enrollment = enrollmentService.findById(enrollmentId);

				if(enrollment != null){
					enrollment.setExchangeAssignPolicyNo(fFMEnrollmentResponse.getFfeAssignedPolNo());
					enrollment.setIssuerAssignPolicyNo(fFMEnrollmentResponse.getIssuerAssignedPolNo());
					//Only if difference between premium is less than 2 dollars

					if(isNotNullAndEmpty(fFMEnrollmentResponse.getGiAndFfmDataList())){
						Map<String,String> giAndFfmDifMap=fFMEnrollmentResponse.getGiAndFfmDataList();
						Float netPremiumAmount=enrollment.getNetPremiumAmt()+Float.parseFloat(giAndFfmDifMap.get(CHANGE_IN_PREMIUM));
						enrollment.setNetPremiumAmt(netPremiumAmount);
						enrollment.setGrossPremiumAmt(netPremiumAmount+enrollment.getAptcAmt());
					}

					if(isNotNullAndEmpty(enrollment.getEnrollmentStatusLkp()) 
							&& enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED )){
						enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
						enrollment.setAbortTimestamp(null);
					}

					//save the policy no received in FF response
					//HIX-44501
					//setAssisterBrokerId(enrollment);

					// HIX-53481 Setting Cap Agent Id 
					LOGGER.info("Setting Cap Agent ID for Proxy Flow");
					LOGGER.info("Target ID :: "+ enrollmentFfmProxyDTO.getTargetId());
					LOGGER.info("Securables Application Data :: " + enrollmentFfmProxyDTO.getSecurablesApplicationData());
					if(!(EnrollmentPrivateConstants.Y.equalsIgnoreCase(enrollmentFfmProxyDTO.getSecurablesApplicationData())) && NumberUtils.isNumber(enrollmentFfmProxyDTO.getTargetId())){
						LOGGER.info("Cap Agent ID :: "+ enrollmentFfmProxyDTO.getTargetId());
						enrollment.setCapAgentId(Integer.parseInt(enrollmentFfmProxyDTO.getTargetId()));
					}
					enrollment = enrollmentService.saveEnrollment(enrollment);
					updateConsumerApplicationById(enrollment.getSsapApplicationid(), EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING);				
					//Send Confirmation email
					sendEmailToConsumer(enrollment.getCmrHouseHoldId(),enrollment);

					enrollmentResponse.setEnrollment(enrollment);
					enrollmentResponse.setfFMPolicyId(enrollment.getExchangeAssignPolicyNo());
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentService.publishPartnerEvent(enrollment.getId(), enrollment.getCmrHouseHoldId());
				}
				else{
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrMsg("No Enrollment record Found");
				}
			}
		} catch (Exception e) {
			giExceptionHandler.recordFatalException(Component.ENROLLMENT, null, "FFM proxy enrollment failed", e);
		}

		return enrollmentResponse;
	}

	private FFMEnrollmentRequest populateFFMFieldsFromEnrollment(Enrollment enrollmentObj, EnrollmentFfmProxyDTO enrollmentFfmProxyDTO) {

		FFMEnrollmentRequest ffmEnrollmentRequest = new FFMEnrollmentRequest();

		// Partner/ Consumer IdentificationenrollmentObj

		ffmEnrollmentRequest.setInformationExchangeSystemId(EnrollmentPrivateConstants.FFM_INFORMATION_EXCHANGE_SYSTEM_ID);

		if(isNotNull(enrollmentObj)){

			ffmEnrollmentRequest.setUserType(EnrollmentPrivateConstants.AGENT_ROLE);
			ffmEnrollmentRequest.setEnrollmentId(enrollmentObj.getId());
			ffmEnrollmentRequest.setStateExchangeCode(enrollmentObj.getStateExchangeCode());
			//ffmEnrollmentRequest.setStateExchangeCode("MS");

			ffmEnrollmentRequest.setPartnerAssignedConsumerId(enrollmentObj.getGiHouseholdId());
			//ffmEnrollmentRequest.setPartnerAssignedConsumerId(enrollmentObj.getPartnerAssignedConsumerId());
			ffmEnrollmentRequest.setFfeAssignedConsumerId(enrollmentObj.getHouseHoldCaseId());
			ffmEnrollmentRequest.setSsapApplicationId(enrollmentObj.getSsapApplicationid());

			// User Identity Assertion
			ffmEnrollmentRequest.setFfeUserId(enrollmentFfmProxyDTO.getFfeUserId());
			ffmEnrollmentRequest.setUiaFirstName(enrollmentFfmProxyDTO.getUiaFirstName());
			ffmEnrollmentRequest.setUiaMiddleName(null);
			ffmEnrollmentRequest.setUiaLastName(enrollmentFfmProxyDTO.getUiaLastName());

			// Agent/Broker Information
			/*ffmEnrollmentRequest.setAgentOrBrokerFirstName("GetInsured");
			ffmEnrollmentRequest.setAgentOrBrokerLastName("ZZZ");*/
			ffmEnrollmentRequest.setAgentOrBrokerFirstName("Michael");
			ffmEnrollmentRequest.setAgentOrBrokerLastName("Daugherty");
			ffmEnrollmentRequest.setAgentOrBrokerMiddleName(null);
			ffmEnrollmentRequest.setAgentOrBrokerNameSuffix(null);
			ffmEnrollmentRequest.setAgentOrBrokerIndicator("Agent");
			//ffmEnrollmentRequest.setAgentOrBrokerNationalProducerNumber(EnrollmentPrivateConstants.FFM_NPN);
			ffmEnrollmentRequest.setAgentOrBrokerNationalProducerNumber(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.DEFAULT_NPN));

			// Member Actions 
			ffmEnrollmentRequest.setMemberEnrollmentPeriodType(enrollmentObj.getEnrollmentTypeLkp().getLookupValueCode());

			if(isNotNullAndEmpty(enrollmentObj.getEnrollmentReason())
					&& (enrollmentObj.getEnrollmentReason().equals('I') || enrollmentObj.getEnrollmentReason().equals('i'))){
				ffmEnrollmentRequest.setMemberReasonCode("28");
				ffmEnrollmentRequest.setMemberTypeCode("21");
			}

			if(isNotNullAndEmpty(enrollmentObj.getBenefitEffectiveDate())){
				ffmEnrollmentRequest.setMemberActionEffectiveDate(DateUtil.dateToString(enrollmentObj.getBenefitEffectiveDate(),GhixConstants.REQUIRED_DATE_FORMAT));
			}

			// Premium Information	
			ffmEnrollmentRequest.setPremiumTotalPremiumAmount(enrollmentObj.getGrossPremiumAmt());
			//ffmEnrollmentRequest.setPremiumAPTCElectedPercentage(enrollmentObj.getAptcPercentage());
			ffmEnrollmentRequest.setPremiumAPTCElectedPercentage(enrollmentObj.getAptcAmt());
			ffmEnrollmentRequest.setPremiumAPTCAppliedAmount(enrollmentObj.getAptcAmt());
			ffmEnrollmentRequest.setPremiumTotalIndividualResponsibilityAmount(enrollmentObj.getNetPremiumAmt());

			//getting hios issuer id
			if(enrollmentObj.getHiosIssuerId()!=null && (!enrollmentObj.getHiosIssuerId().equalsIgnoreCase(""))){
				ffmEnrollmentRequest.setEnrollmentGroupIssuerId(enrollmentObj.getHiosIssuerId());
			}
			
			if(isNotNullAndEmpty(enrollmentObj.getAptcAmt()) && enrollmentObj.getAptcAmt()>0){
				ffmEnrollmentRequest.setAptcAttestation("true");
			}else{
				ffmEnrollmentRequest.setAptcAttestation("false");
			}

			// EnrollmentGroup Information
			if(isNotNullAndEmpty(enrollmentObj.getBenefitEffectiveDate())){
				ffmEnrollmentRequest.setEnrollmentGroupStartDate(DateUtil.dateToString(enrollmentObj.getBenefitEffectiveDate(),GhixConstants.REQUIRED_DATE_FORMAT));
			}
			if(isNotNullAndEmpty(enrollmentObj.getBenefitEndDate())){
				ffmEnrollmentRequest.setEnrollmentGroupEndDate(DateUtil.dateToString(enrollmentObj.getBenefitEndDate(),GhixConstants.REQUIRED_DATE_FORMAT));
			}
			if((isNotNullAndEmpty(enrollmentObj.getEnrollmentTypeLkp()) && 
					isNotNullAndEmpty(enrollmentObj.getEnrollmentTypeLkp().getLookupValueCode())) &&
					enrollmentObj.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INITIAL)){

				ffmEnrollmentRequest.setEnrollmentGroupTransactionType(EnrollmentPrivateConstants.FFM_ENROLLMENT_TXN_TYPE_E);
			}

			if(isNotNullAndEmpty(enrollmentObj.getCMSPlanID())){
				ffmEnrollmentRequest.setEnrollmentGroupAssignedPlanId(enrollmentObj.getCMSPlanID().substring(0, enrollmentObj.getCMSPlanID().length() - 2));
				ffmEnrollmentRequest.setPremiumCSRLevelApplicable(enrollmentObj.getCMSPlanID().substring(enrollmentObj.getCMSPlanID().length() - 2));
			}

			ffmEnrollmentRequest.setEnrollmentGroupFFEAssignedPolicyNumber(enrollmentObj.getGroupPolicyNumber());
			ffmEnrollmentRequest.setEnrollmentGroupIssuerPolicyNumber(enrollmentObj.getGroupPolicyNumber());

			List <Enrollee> enrolleeList = enrollmentObj.getEnrollees();

			for(Enrollee enrolleeObj :  enrolleeList){

				if(isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp()) && isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp().getLookupValueCode())){

					FFMEnrollmentMemberData objFFMEnrollmentMemberData = new FFMEnrollmentMemberData();
					// AptcAttestation Information
					if(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER)){
						ffmEnrollmentRequest.setAptcAttestationId(enrolleeObj.getFfmPersonId());
						ffmEnrollmentRequest.setAptcAttestationFirstName(enrolleeObj.getFirstName());
						ffmEnrollmentRequest.setAptcAttestationLastName(enrolleeObj.getLastName());
						ffmEnrollmentRequest.setAptcAttestationMiddleName(enrolleeObj.getMiddleName());
						ffmEnrollmentRequest.setAptcAttestationSuffixName(enrolleeObj.getSuffix());
						ffmEnrollmentRequest.setMemberIssuerAssignedMemberId(enrolleeObj.getExchgIndivIdentifier());
						ffmEnrollmentRequest.setAptcAttestationSsn(enrolleeObj.getTaxIdNumber());
						ffmEnrollmentRequest.setAptcAttestationDob(enrolleeObj.getBirthDate());
						objFFMEnrollmentMemberData.setMemberSubscriberIndicator(true);
						if(isNotNullAndEmpty(enrolleeObj.getGenderLkp())){
							ffmEnrollmentRequest.setAptcAttestationGender(enrolleeObj.getGenderLkp().getLookupValueCode());
						}
					}

					objFFMEnrollmentMemberData.setInsuranceAplicantId(enrolleeObj.getInsuranceAplicantId());
					objFFMEnrollmentMemberData.setFfmPersonId(enrolleeObj.getFfmPersonId());

					objFFMEnrollmentMemberData.setFirstName(enrolleeObj.getFirstName());
					objFFMEnrollmentMemberData.setLastName(enrolleeObj.getLastName());
					objFFMEnrollmentMemberData.setMiddleName(enrolleeObj.getMiddleName());
					objFFMEnrollmentMemberData.setSuffixName(enrolleeObj.getSuffix());
					objFFMEnrollmentMemberData.setMemberIssuerAssignedMemberId(enrolleeObj.getExchgIndivIdentifier());
					objFFMEnrollmentMemberData.setMemberFFEAssignedApplicantId(enrolleeObj.getExchgIndivIdentifier());
					objFFMEnrollmentMemberData.setSsn(enrolleeObj.getTaxIdNumber());
					objFFMEnrollmentMemberData.setDob(enrolleeObj.getBirthDate());
					if(isNotNullAndEmpty(enrolleeObj.getGenderLkp())){
						objFFMEnrollmentMemberData.setGender(enrolleeObj.getGenderLkp().getLookupValueCode());
					}


					// Premium Information
					if(enrollmentObj.getStateExchangeCode()!=null && enrolleeObj.getRatingArea()!=null){
						ffmEnrollmentRequest.setPremiumRatingArea(enrollmentObj.getStateExchangeCode()+ StringUtils.leftPad(enrolleeObj.getRatingArea(),3,'0'));	
					}

					// Member Actions :: Set all member level information
					if(isNotNullAndEmpty(enrolleeObj.getTobaccoUsageLkp()) && enrolleeObj.getTobaccoUsageLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.T)){
						objFFMEnrollmentMemberData.setMemberTobaccoUseIndicator(true);
						objFFMEnrollmentMemberData.setMemberLastDateOfTobaccoUse(getDateOfTobaccoUse(enrolleeObj , enrollmentFfmProxyDTO.getEnrolleeList()));
					}


					if(isNotNullAndEmpty(enrolleeObj.getEnrollmentReason())){
						String enrollmentReason =""+ enrolleeObj.getEnrollmentReason();
						if(enrollmentReason.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INITIAL)){
							objFFMEnrollmentMemberData.setMemberEnrollmentPeriodType("Initial");
						}else if(enrollmentReason.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SPECIAL)){
							objFFMEnrollmentMemberData.setMemberEnrollmentPeriodType("Special");
						}else {
							objFFMEnrollmentMemberData.setMemberEnrollmentPeriodType("Annual");
						}

					}
					objFFMEnrollmentMemberData.setMemberIssuerAssignedMemberId(enrolleeObj.getIssuerIndivIdentifier());
					objFFMEnrollmentMemberData.setMemberFFEAssignedMemberId(null);

					if(enrolleeObj.getRelationshipToHCPLkp()!=null){
						objFFMEnrollmentMemberData.setMemberRelationshipToSubscriber(enrolleeObj.getRelationshipToHCPLkp().getLookupValueCode());	
					}

					//LOGGER.info("objFFMEnrollmentMemberData : "+ objFFMEnrollmentMemberData);

					ffmEnrollmentRequest.getMembers().add(objFFMEnrollmentMemberData);

				}	
			}
		}

		//LOGGER.info("ffmEnrollmentRequest : "+ ffmEnrollmentRequest);
		return ffmEnrollmentRequest;

	}
	
	private void sendEmailToConsumer(Integer cmrHouseHoldId, Enrollment enrollmentObj) {
		Household household = null;
		try{			
			LOGGER.info("Getting Household from CmrHouseHoldId: " + cmrHouseHoldId);
			household = enrollmentExternalRestUtil.getHousehold(cmrHouseHoldId);
			LOGGER.info("Sending email to consumer ::" + household.getEmail());
			emailService.sendEmailNotification(household, enrollmentObj);
			String exitSurveyEmail = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXIT_SURVEY_EMAIL);
			if(exitSurveyEmail == null || "ON".equalsIgnoreCase(exitSurveyEmail)) {
				exitSurveyEmailNotificationService.sendEmailNotification(household);
			}
		}catch(Exception e){
			LOGGER.error("Exception while sending email notification: " ,e);
		}

	}
	
	private void updateConsumerApplicationById(Long ssapApplicationId, String enrollmentStatus) {
		
		SsapApplicationDTO ssapApplicationDTO = new SsapApplicationDTO();
		ssapApplicationDTO.setId(ssapApplicationId);
		ssapApplicationDTO.setEnrollmentStatus(enrollmentStatus);
		
		try{
			restTemplate.postForObject(ConsumerServiceEndPoints.UPDATE_CONSUMER_APPLICATION_BY_ID,ssapApplicationDTO,String.class);
		}
		catch(Exception e){
			LOGGER.error("EnrollmentFfmProxyController.updateConsumerApplicationById - Error in updating enrollment status for SSAP Application Id="+ssapApplicationDTO.getId(), e);
		}
	}
	
	/**
	 * Returns the last Tobacco Usage date for the enrollee from the EnrolleeFfmProxyDTO list
	 * @author negi_s
	 * @since  09/01/2015
	 * @param enrolleeObj
	 * @param enrolleeFfmProxyList
	 * @return Last Tobacco Use Date in "MM/dd/yyyy" format
	 */
	private String getDateOfTobaccoUse(Enrollee enrolleeObj,List<EnrolleeFfmProxyDTO> enrolleeFfmProxyList) {
		String tobaccoUsageDateTimestamp = null;
		String tobaccoUsageDateRequiredFormat = null;
		for(EnrolleeFfmProxyDTO proxyEnrollee : enrolleeFfmProxyList ){
			if(enrolleeObj.getExchgIndivIdentifier().equalsIgnoreCase(proxyEnrollee.getMemberFFEAssignedApplicantId())){
				tobaccoUsageDateTimestamp = proxyEnrollee.getMemberLastDateOfTobaccoUse();
				break;
			}
		}
		if(EnrollmentPrivateUtils.isNotNullAndEmpty(tobaccoUsageDateTimestamp)){
			Date tobaccoUsageDate = DateUtil.StringToDate(tobaccoUsageDateTimestamp, "yyyy-MM-dd hh:mm:ss.S");
			tobaccoUsageDateRequiredFormat = DateUtil.dateToString(tobaccoUsageDate, GhixConstants.REQUIRED_DATE_FORMAT);
		}
		return tobaccoUsageDateRequiredFormat;
	}

}
