/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNull;
import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.bb.BenefitBayRequestDTO;
import com.getinsured.hix.dto.bb.BenefitBayResponseDTO;
import com.getinsured.hix.dto.cap.consumerapp.CapApplicationDTO;
import com.getinsured.hix.dto.cap.consumerapp.CapOnExchangeFFMAppDto;
import com.getinsured.hix.dto.cap.consumerapp.EffectiveApplicationDTO;
import com.getinsured.hix.dto.cap.consumerapp.PolicyLiteDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.dto.enrollment.D2CEnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentCommissionDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentConsumerPortalDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMajorInfo;
import com.getinsured.hix.dto.enrollment.EnrollmentNonEligiblePersonDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPersonalInfo;
import com.getinsured.hix.dto.enrollment.EnrollmentPlanResponseDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.MarketType;
import com.getinsured.hix.dto.enrollment.EnrollmentResendDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateHouseholdDTO;
import com.getinsured.hix.dto.ffm.FFMEnrollmentMemberData;
import com.getinsured.hix.dto.ffm.FFMEnrollmentRequest;
import com.getinsured.hix.dto.ffm.FFMEnrollmentResponse;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.repository.IEnrollmentIssuerCommisionRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.service.EnrolleePrivateService;
import com.getinsured.hix.enrollment.service.EnrollmentCommissionService;
import com.getinsured.hix.enrollment.service.EnrollmentEventPrivateService;
import com.getinsured.hix.enrollment.service.EnrollmentOffExchangeConfirmEmailNotificationService;
import com.getinsured.hix.enrollment.service.EnrollmentPartnerNotificationService;
import com.getinsured.hix.enrollment.service.EnrollmentPrivateService;
import com.getinsured.hix.enrollment.service.ExitSurveyEmailNotificationService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.EnrollmentUtilsForCap;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerEnrollment.Status;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.Securables;
import com.getinsured.hix.model.TaxYear;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeDTO;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentCommission;
import com.getinsured.hix.model.enrollment.EnrollmentCommissionResponse;
import com.getinsured.hix.model.enrollment.EnrollmentDTO;
import com.getinsured.hix.model.enrollment.EnrollmentIssuerCommision;
import com.getinsured.hix.model.enrollment.EnrollmentPlan;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.EnrollmentSearchFilter;
import com.getinsured.hix.model.enrollment.ManualEnrollmentRequest;
import com.getinsured.hix.model.enrollment.ManualEnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.plandisplay.PlanDisplayResponse;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.ISecurableTargetRepository;
import com.getinsured.hix.platform.security.repository.ISecurablesRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.TaxFilingService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlandisplayEndpoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import springfox.documentation.annotations.ApiIgnore;


/**
 * @author panda_pratap
 * @since 13/02/2013
 */

/*@Api(value="enrollment-Private-Controller", description="Enrollment Private Controller")*/
@Controller
@RequestMapping(value = "/enrollment/private")
public class EnrollmentPrivateController {

	private static final Logger LOGGER = Logger.getLogger(EnrollmentPrivateController.class);

	@Autowired private RestTemplate restTemplate;
	@Autowired private EnrollmentPrivateService enrollmentService;
	@Autowired private EnrollmentPartnerNotificationService enrollmentPartnerNotificationService;
	@Autowired private EnrolleePrivateService enrolleeService;
	@Autowired private TaxFilingService taxFilingService;
	@Autowired private UserService userService;
	@Autowired private LookupService lookupService;
	@Autowired private EnrollmentEventPrivateService enrollmentEventPrivateService;
	@Autowired private EnrollmentCommissionService enrollmentCommissionService;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private ISecurablesRepository iSecurablesRepository;
	@Autowired private ISecurableTargetRepository iSecurableTargetRepository;
	@Autowired private EnrollmentOffExchangeConfirmEmailNotificationService enrlOffExchangeEmailNotificationService;
	@Autowired private ExitSurveyEmailNotificationService exitSurveyEmailNotificationService;
	@Autowired private GIExceptionHandler giExceptionHandler;
	@Autowired private EnrollmentPrivateUtils enrollmentPrivateUtils;
    @Autowired private Gson platformGson;
    @Autowired private IEnrollmentIssuerCommisionRepository enrollmentIssuerCommisionRepository;
	 
	private static final String SHOPPING_ID = "shopping_id";
	private static final String STATUS_CODE_E006 = "E-006";
	private static final String LEAD_ID = "leadId";
	private static final String CMR_HOUSEHOLD_ID = "cmrHouseholdId";
	private static final String FFM_LOGGED_IN_USER_ID = "ffmLoggedInUserId";
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	
	private static final String NON_ELIGIBLE_MEMBER_FNAME = "firstName";
	private static final String NON_ELIGIBLE_MEMBER_LNAME = "lastName";
	private static final String NON_ELIGIBLE_MEMBER_ELIGIBILITY_REASON  = "eligibilityReason";
	private static final String LOGGER_ERR_FFM_MSG = " Problem in FFM call : Putting Enrollment to ABORT State  ";
//	private static final String CHANGE_IN_PREMIUM = "changeInPremium";
	private static final String HEALTH = "HLT";
	private static final String DENTAL = "DEN";
	private static final String STM = "STM";
	private static final String AME = "AME";
	private static final String VISION = "VISION";
	private static final String LIFE = "LIFE";
	private static final String MEDICARE = "MEDICARE";
	private static final String FFM_PREMIUM_AMT = "ffmPremiumAmount";
	private static final String FFM_APTC_AMT = "ffmAptcAmount";
//	private static final String GI_PREMIUM_AMT = "giPremiumAmount";
	
	private static final int ERROR_CODE_104 = 104;
	
	@Value("#{configProp['tenant.bb.userName']}")
	private String benefitBayUser;

	/**
	 * @author panda_p This method is used to test the working of enrollment
	 *         module after deployment
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
	//@ApiOperation(value="welcome", notes="Welcome !", httpMethod="GET", produces="text/plain")
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to Enrollment module (welcome()) ::");
		String userName = "";
		String userId = "";
		try {
			userName = userService.getLoggedInUser().getUserName();
			userId = userService.getLoggedInUser().getId() + "";
			LOGGER.info("User ID = " + userId);
		} catch (InvalidUserException e) {
			LOGGER.error("Message : " + e.getMessage(),e);
			LOGGER.error("Cause : " + e.getCause(),e);
		}
		return "Welcome to Enrollment module (welcome()) :: UserName :"
				+ userName + ", UserId :" + userId;
	}

	/**
	 * @author panda_p This method is used to test the working of enrollment
	 *         module after deployment
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
	//@ApiOperation(value="Post testing", notes="", httpMethod="POST", produces="text/plain")
	@RequestMapping(value = "/testPost", method = RequestMethod.POST)
	@ResponseBody
	public String testPost(@RequestBody String year) {

		LOGGER.info("Welcome to Enrollment module (testPost()) ::");

		String userName = "";
		String userId = "";

		try {
			userName = userService.getLoggedInUser().getUserName();
			userId = userService.getLoggedInUser().getId() + "";
			LOGGER.info("User ID = " + userId);
		} catch (InvalidUserException e) {
			LOGGER.error("Message : " + e.getMessage(),e);
			LOGGER.error("Cause : " + e.getCause(),e);
		}
		return "Welcome to Enrollment module (testPost()) :: UserName :"
				+ userName + ", UserId :" + userId + ", Input year ::" + year;
	}

	//@ApiOperation(value="findenrollmentbyadmin", notes="Find enrollment by search criterias", httpMethod="POST", produces="application/xml")
	@RequestMapping(value = "/findenrollmentbyadmin", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentByAdmin(
			@RequestBody EnrollmentSearchFilter enrollmentSearchFilter,HttpServletRequest httpRequest) {
		LOGGER.info("=================== inside searchEnrollmentExt ===================");

		LOGGER.info("enrollmentSearchFilter :  " + enrollmentSearchFilter);
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (enrollmentSearchFilter == null ) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
		} else {
			long tenantId = 0;
			if(TenantContextHolder.getTenant() != null) {
				tenantId = TenantContextHolder.getTenant().getId();
			}
			
			Map<String, Object> enrollmentsAndRecordCount = enrollmentService.findEnrollmentByAdmin(enrollmentSearchFilter,tenantId);
			if (enrollmentsAndRecordCount == null) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse
						.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_NO_ENROLLMENT_FOUND);
			} else {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse
						.setEnrollmentsAndRecordCount(enrollmentsAndRecordCount);
			}
		}
		
		return xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * @author panda_p
	 * jira - HIX-92904
	 * @param id
	 * @return
	 * 
	 */
	@RequestMapping(value="/findbyenrollmentid/{id}",method = RequestMethod.GET, produces="application/json")
	@ResponseBody public String findbyenrollmentid(@PathVariable Integer id){
		LOGGER.info("============ findbyenrollmentid ============");	
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		EnrollmentDTO enrollmentDTO=null;
		
		try {	
			enrollmentDTO = enrollmentService.findbyenrollmentid(id);
			if(enrollmentDTO!=null){
				enrollmentResponse.setEnrollmentDTO(enrollmentDTO);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			}else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
			}
			return platformGson.toJson(enrollmentResponse);
		} catch (Exception e) {
			enrollmentResponse.setCapApplicatonDto(null);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			enrollmentResponse.setErrMsg(GhixUtils.getStackTrace(e));
			return platformGson.toJson(enrollmentResponse);
		}
	}
	
	//@ApiOperation(value="Find enrollment by ID", notes="", httpMethod="GET", produces="application/xml")
	@RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String findEnrollmentById(@PathVariable("id") String id) {
		LOGGER.info("============inside findEnrollmentById============");

		LOGGER.info("id1 : " + id);
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (id == null || id.equalsIgnoreCase("0")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse
					.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		} else {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			Integer enrollmentId = Integer.valueOf(id);
			Enrollment enrollment = enrollmentService.findById(enrollmentId);
			enrollmentResponse.setEnrollment(enrollment);
		}

		XStream xstream = GhixUtils.getXStreamStaxObject();
		return xstream.toXML(enrollmentResponse);
	}

	//@ApiOperation(value="Find Enrollment By Carrier Feed Details", notes="", httpMethod="POST", produces="application/json")
	@ApiIgnore
	@RequestMapping(value = "/findEnrollmentByCarrierFeedDetails", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentByCarrierFeedDetails(
			@RequestBody Map<String, Object> inputval) throws JsonProcessingException {

		LOGGER.info("=================== inside findEnrollmentByCarrierFeedDetails ===================");
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(ArrayList.class);
		List<Enrollment> enrollmentList = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		
		if (inputval == null || inputval.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse
					.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
		} else {
		 enrollmentList = enrollmentService
					.findEnrollmentByCarrierFeedDetails(inputval);
			if (enrollmentList == null || enrollmentList.size() == 0) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_CARRIER_FEED_DATA);
			} else {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setEnrollmentList(enrollmentList);
			}
		}

		return writer.writeValueAsString(enrollmentList);
		
				
	}

	//@ApiOperation(value="Resend enrollment to FFM", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value = "/resendenrollmenttoffm", method = RequestMethod.POST)
	@ResponseBody public String reSendenrollmentToFFM(@RequestBody String resendDataRequestString){
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse=null;
		EnrollmentResendDTO enrollmentResendDTO=(EnrollmentResendDTO) xstream.fromXML(resendDataRequestString);
		List<Enrollment> enrollmentList = enrollmentResendDTO.getEnrollmentList();
			
		EligLead eligLead = null;

		if (enrollmentResendDTO.getLeadId() != null) {
			Long leadId=(Long.parseLong(enrollmentResendDTO.getLeadId()));
			eligLead=getEligLead(leadId);
		}
		//String cmrHouseholdId = (String)inputval.get(CMR_HOUSEHOLD_ID);
		List<Enrollment> enrollmentList1 = new ArrayList<Enrollment>();
		for(Enrollment enrollmentObj: enrollmentList){
			// RE-SEND THE ENROLLMENT TO FFM
			enrollmentResponse = sendEnrollmentDetailsToFFM(enrollmentObj,eligLead, enrollmentResendDTO.getFfmLoggedInUserId(), enrollmentResendDTO.getIsUserSwitchToOtherView());
			
			if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				LOGGER.debug("FFM call successfully done. :  "+ enrollmentResponse.getErrMsg());
				Enrollment enrlmnt = enrollmentResponse.getEnrollment();
//				enrlmnt.setIndOrderItem(null);
				enrollmentList1.add(enrlmnt);
			}
			else if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE) 
					&& enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-009")){
				enrollmentList1.add(enrollmentObj);

				LOGGER.error(LOGGER_ERR_FFM_MSG+ enrollmentResponse.getErrMsg());
			}
			
		}
		enrollmentResponse.setEnrollmentList(enrollmentList1);
		return xstream.toXML(enrollmentResponse);
	}

	//@ApiOperation(value="Update enrollment by FFM data", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value = "/updateenrollmentwithffmdata", method = RequestMethod.POST)
	@ResponseBody public String updateenrollmentwithffmdata(@RequestBody String resendRequestString){
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse=null;
		EnrollmentResendDTO enrollmentResendDTO=(EnrollmentResendDTO) xstream.fromXML(resendRequestString);
		String ffmDifConfirmation = enrollmentResendDTO.getFfmDifConfirmation();
		List<Enrollment> enrollmentList = enrollmentResendDTO.getEnrollmentList();
		EligLead eligLead = null;
		
		if (enrollmentResendDTO.getLeadId() != null) {
			Long leadId=(Long.parseLong(enrollmentResendDTO.getLeadId()));
			eligLead=getEligLead(leadId);
		}
		
		if(ffmDifConfirmation!=null && !ffmDifConfirmation.equalsIgnoreCase("")){

			if(ffmDifConfirmation.equalsIgnoreCase("yes")){
				
				Float ffmPremiumAmount = Float.parseFloat(enrollmentResendDTO.getFfmPremiumAmount());
				Float ffmAptcAmount = Float.parseFloat(enrollmentResendDTO.getFfmAptcAmount());
				
				List<Enrollment> enrollmentList1 = new ArrayList<Enrollment>();
				for(Enrollment enrollmentObj: enrollmentList){
					// UPDATE THE ENROLLMENT GROSSPREMIUM, APTCAMOUNT AND NETPREMIUM WITH FFM PREMIUM AMOUNT 
					String giHouseHoldId= enrollmentObj.getGiHouseholdId();
					char enrollmentReason= enrollmentObj.getEnrollmentReason();
					
					enrollmentObj.setNetPremiumAmt(ffmPremiumAmount);
					enrollmentObj.setGrossPremiumAmt(ffmPremiumAmount+ffmAptcAmount);
					enrollmentObj.setAptcAmt(ffmAptcAmount);
					enrollmentObj = enrollmentService.saveEnrollment(enrollmentObj);
					
					enrollmentObj.setGiHouseholdId(giHouseHoldId);
					enrollmentObj.setEnrollmentReason(enrollmentReason);
					// SEND THE UPDATED ENROLLMENT TO FFM
					enrollmentResponse = sendEnrollmentDetailsToFFM(enrollmentObj,eligLead, enrollmentResendDTO.getFfmLoggedInUserId(), enrollmentResendDTO.getIsUserSwitchToOtherView());					
					
					if(enrollmentResponse != null)
					{
						if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
							LOGGER.debug("FFM call successfully done. :  "+ enrollmentResponse.getErrMsg());
							Enrollment enrlmnt = enrollmentResponse.getEnrollment();
//							enrlmnt.setIndOrderItem(null);
							enrollmentList1.add(enrlmnt);
						}
						else if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE) 
								&& enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-008")){
							
							enrollmentList1.add(enrollmentObj);
							LOGGER.error(LOGGER_ERR_FFM_MSG+ enrollmentResponse.getErrMsg());
						}
					}
					else
					{
						enrollmentResponse = new EnrollmentResponse();
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
					}
				}
				if(enrollmentResponse != null && !enrollmentList1.isEmpty()){
					enrollmentResponse.setEnrollmentList(enrollmentList1);
				}
			}else if(ffmDifConfirmation.equalsIgnoreCase("no")){
				// ABORT THE ENROLLMENT AND SEND BACK TO THE PLAN SELECTION PAGE.
				for(Enrollment enrollmentObj : enrollmentList){
					enrollmentService.abortEnrollment(enrollmentObj);
				}
				enrollmentResponse = new EnrollmentResponse();
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setModuleStatusCode("E-010");
				enrollmentResponse.setErrMsg("Enrollment Aborted successfully........!");
				
			}else{
				LOGGER.info("Incorrct input from model");
			}
		}
		
		return xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * 
	 * @param leadId
	 * @return
	 */
	private  EligLead getEligLead(Long leadId){
		EligLead eligLead = null;
		if (isNotNullAndEmpty(leadId)) {
			eligLead=restTemplate.postForObject(GhixEndPoints.PhixEndPoints.GET_LEAD, leadId.longValue(), EligLead.class);
		}
		return eligLead;
	}
	
	//@ApiOperation(value="Create Individual enrollment", notes="", httpMethod="POST", produces="application/xml")
	@ApiIgnore
	@RequestMapping(value = "/createIndividualEnrollment", method = RequestMethod.POST)
	@ResponseBody
	public String createIndividualEnrollment(
			@RequestBody Map<String, Object> inputval) {
		LOGGER.info("=================== inside createIndividualEnrollment ===================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		try {
			if (inputval == null || inputval.isEmpty()) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ORDER_ID_NULL_OR_EMPTY);
			} else {
				if (inputval.get(EnrollmentPrivateConstants.ORDER_ID) == null|| inputval.get(EnrollmentPrivateConstants.ORDER_ID).equals("")) {
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ORDER_ID_NULL_OR_EMPTY);
				} else {
					String orderId = null;
					// String pin_esig = null;
					String applicant_esig = null;
					String getRestResponse = null;
					String taxFiler_esign = null;
					String cmrHouseholdId = null;
					EligLead eligLead =null;
					String shopping_id = null;
					Long ssapApplicationId = null;
					Integer ffmLoggedInUserId = null;
					String isUserSwitchToOtherView = null;
					
					if (inputval.get(EnrollmentPrivateConstants.ORDER_ID) != null) {
						orderId = inputval.get(EnrollmentPrivateConstants.ORDER_ID).toString();
					}
					if (inputval.get(EnrollmentPrivateConstants.APPLICANT_ESIG) != null) {
						applicant_esig = inputval.get(EnrollmentPrivateConstants.APPLICANT_ESIG).toString();
					}
					if (inputval.get("taxFiler_esign") != null) {
						taxFiler_esign = inputval.get("taxFiler_esign").toString();
					}
					if (inputval.get(CMR_HOUSEHOLD_ID) != null) {
						cmrHouseholdId = inputval.get(CMR_HOUSEHOLD_ID).toString();
					}
					if (inputval.get(GhixConstants.SSAP_APPLICATION_ID) != null) {
						ssapApplicationId = Long.parseLong(inputval.get(GhixConstants.SSAP_APPLICATION_ID).toString());
					}
					
					if (inputval.get(LEAD_ID) != null) {
						Long leadId=(Long.parseLong((inputval.get(LEAD_ID).toString())));
						eligLead=getEligLead(leadId);
					}
					if (inputval.get(SHOPPING_ID) != null) {
						shopping_id = inputval.get(SHOPPING_ID).toString();
					}
					if (inputval.get(FFM_LOGGED_IN_USER_ID) != null) {
						ffmLoggedInUserId = (Integer) inputval.get(FFM_LOGGED_IN_USER_ID);
					}
					if (inputval.get(IS_USER_SWITCH_TO_OTHER_VIEW) != null) {
						isUserSwitchToOtherView = (String) inputval.get(IS_USER_SWITCH_TO_OTHER_VIEW);
					}
				
					try {
						LOGGER.info("Calling plan selection service to get plan and member data");
						if(isPHIXBBUser()){
							Map<String, String> paramsBB = new HashMap<String, String>();
							paramsBB.put(EnrollmentPrivateConstants.ID, orderId);
							getRestResponse = restTemplate.getForObject(PlandisplayEndpoints.FIND_BY_ORDERID,String.class, paramsBB);	
						}else{
							/*Map<String, Long> params = new HashMap<String, Long>();
							params.put("ssapApplicationId", ssapApplicationId);
							getRestResponse = restTemplate.getForObject(PlandisplayEndpoints.FIND_BY_SSAP_APPLICATION_ID +"/{ssapApplicationId}",String.class, params)*/
							Map<String, String> params = new HashMap<String, String>();
							params.put(EnrollmentPrivateConstants.ID, orderId);
							getRestResponse = restTemplate.getForObject(PlandisplayEndpoints.FIND_BY_ORDERID,String.class, params);	
						}
						//LOGGER.info("plandisplayResponse : " + getRestResponse);
					} catch (RestClientException re) {
						LOGGER.error("Message : " + re.getMessage(),re);
						LOGGER.error("Cause : " + re.getCause(),re);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_FAILED_TO_CALL_PLANDISPLAY_ENROLLMENT_DATA_SERVICE);
						enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
						enrollmentResponse.setErrCode(ERROR_CODE_104);
						return xstream.toXML(enrollmentResponse);
					} catch (Exception e) {
						LOGGER.error("Message : " + e.getMessage(),e);
						LOGGER.error("Cause : " + e.getCause(),e);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_FAILED_TO_CALL_PLANDISPLAY_ENROLLMENT_DATA_SERVICE);
						enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
						enrollmentResponse.setErrCode(ERROR_CODE_104);
						return xstream.toXML(enrollmentResponse);
					}
					if (getRestResponse == null) {
						LOGGER.info(EnrollmentPrivateConstants.ERR_MSG_NO_PLAN_DATA_FOUND);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_NO_PLAN_DATA_FOUND_IN_PLAN_SELECTION_SERVICE_RESPONSE);
						enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
						enrollmentResponse.setErrCode(ERROR_CODE_104);
						return xstream.toXML(enrollmentResponse);
					}
					PldOrderResponse pldOrderResponse = null;
					
					/* Use the below commented code To test the enrollment creation when plandisplayresponse [i.e getRestResponse] returns null data.  
					 getRestResponse="{\"planDataList\":[{\"plan\":{\"aptc\":\"10.00\",\"marketType\":\"FI\",\"planType\":\"HEALTH\",\"aptcPercentage\":\"0.00\",\"stateExchangeCode\":\"MS\",\"grossPremiumAmt\":\"100.00\",\"netPremiumAmt\":\"931.13\",\"orderItemId\":\"2500\",\"coverageStartDate\":\"2014-01-01 00:00:00.0\",\"planId\":\"2730\"},\"memberInfoList\":[{\"memberInfo\":{\"lastName\":\"Hansenmda\",\"dob\":\"1972-10-06\",\"ssn\":\"\",\"memberId\":\"1\",\"subscriberFlag\":\"YES\",\"firstName\":\"Well\",\"genderCode\":\"M\"},\"relationshipInfo\":[{\"relationshipCode\":\"18\",\"memberId\":\"1\"},{\"relationshipCode\":\"01\",\"memberId\":\"2\"}],\"errCode\":0,\"execDuration\":0,\"startTime\":0},{\"memberInfo\":{\"lastName\":\"Harringtonmda\",\"dob\":\"1962-09-16\",\"ssn\":\"\",\"memberId\":\"2\",\"subscriberFlag\":\"YES\",\"firstName\":\"Guilty\",\"genderCode\":\"M\"},\"relationshipInfo\":[{\"relationshipCode\":\"01\",\"memberId\":\"1\"}],\"errCode\":0,\"execDuration\":0,\"startTime\":0}],\"errCode\":0,\"execDuration\":0,\"startTime\":0}],\"enrollmentType\":\"I\",\"isSubsidized\":true,\"shoppingId\":\"358b2154-1c44-42f2-ba1a-7622bfa1451f\",\"householdId\":\"157507\",\"errCode\":0,\"execDuration\":0,\"startTime\":0}";
					*/
					pldOrderResponse = platformGson.fromJson(getRestResponse,PldOrderResponse.class);

					if (pldOrderResponse.getPlanDataList() == null
							|| pldOrderResponse.getPlanDataList().isEmpty()) {
						LOGGER.info(EnrollmentPrivateConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
						return xstream.toXML(enrollmentResponse);
					}

					// Getting individual Case id
					//String caseId = pldOrderResponse.getHouseholdCaseId();

					if (isNotNullAndEmpty(pldOrderResponse.getEnrollmentType())&& (pldOrderResponse.getEnrollmentType().equals('S') || pldOrderResponse.getEnrollmentType().equals('s'))) {
						LOGGER.info("Call Special Enrollment Creation");
						enrollmentResponse = enrollmentService.createSpecialEnrollment(pldOrderResponse,applicant_esig,taxFiler_esign,cmrHouseholdId);
					} else {
						LOGGER.info("Call Initial Enrollment Creation");
						enrollmentResponse = enrollmentService.createEnrollment(pldOrderResponse,applicant_esig,taxFiler_esign,cmrHouseholdId,ssapApplicationId);
					}

					if (enrollmentResponse != null&& enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						/*
						if (EnrollmentPrivateConfiguration.isCaCall() && ENRL_AHBX_WSDL_CALL_ENABLE) {
							// Call IND20
							LOGGER.info("Call IND20");
							enrollmentResponse = sendIndvPSDetails(caseId, orderId, enrollmentResponse);

							if (enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
								enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.MSG_ENROLLMENT_PROCESS_SUCCESS);
								enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E000);
							} else {
								LOGGER.info(" Problem in IND20 call :  "+ enrollmentResponse.getErrMsg());
								// IND-20 Failed Abort created enrollment
								for (Enrollment enrollment : enrollmentResponse.getEnrollmentList()) {
									enrollmentService.abortEnrollment(enrollment);
								}
							}
						}else */
						if (isPHIXBBUser()) {
							LOGGER.info("Call to BB ");
							enrollmentResponse = sendEnrollmentDetailsToBB(enrollmentResponse, shopping_id);
							if (enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
								LOGGER.info("BB call successfully done. :  ");
								//Call Lead stage Update for Enrollment creation complete.
								//saveEligLead(eligLead, orderId);
							}else{
								LOGGER.error(" Problem in BB call :  ");
								for (Enrollment enrollment : enrollmentResponse.getEnrollmentList()) {
									enrollmentService.abortEnrollment(enrollment);
								}
							}
						}else if(EnrollmentPrivateConfiguration.isPhixCall() && !isPHIXBBUser()) {
							
							LOGGER.debug("Call to FFM  ");
							
							//Start of getting NonEligiblePerson
							List<Map<String, String>> nonEligiblePersonNameList  =  pldOrderResponse.getNonEligiblePersonNameList();
							List<EnrollmentNonEligiblePersonDTO> nonEligibleMemberList = new ArrayList<EnrollmentNonEligiblePersonDTO>();
							if(nonEligiblePersonNameList!=null && !nonEligiblePersonNameList.isEmpty()){
								for(Map<String, String> nonEligibleMemberMap : nonEligiblePersonNameList ){
									EnrollmentNonEligiblePersonDTO dto = new EnrollmentNonEligiblePersonDTO();
									dto.setFirstName(nonEligibleMemberMap.get(NON_ELIGIBLE_MEMBER_FNAME));
									dto.setLastName(nonEligibleMemberMap.get(NON_ELIGIBLE_MEMBER_LNAME));
									dto.setEligibilityReason(nonEligibleMemberMap.get(NON_ELIGIBLE_MEMBER_ELIGIBILITY_REASON));
									nonEligibleMemberList.add(dto);
								}
								enrollmentResponse.setEnrollmentNonEligiblePersonDTO(nonEligibleMemberList);
							}
							//End of getting NonEligiblePerson
							
							List<Enrollment> enrollmentList = enrollmentResponse.getEnrollmentList();
							
							if(isNotNull(enrollmentList)){
								
								List<Enrollment> enrollmentList1 = new ArrayList<Enrollment>();
								Map<Integer, String> enrolleeNameMap = enrollmentResponse.getEnrolleeNameMap();
								
								for(Enrollment enrollmentObj: enrollmentList){
									
									enrollmentObj.setGiHouseholdId(pldOrderResponse.getGiHouseholdId());
									enrollmentObj.setEnrollmentReason(pldOrderResponse.getEnrollmentType());
									
									enrollmentResponse = sendEnrollmentDetailsToFFM(enrollmentObj,eligLead, ffmLoggedInUserId, isUserSwitchToOtherView);
									
									
									if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
										LOGGER.debug("FFM call successfully done. :  "+ enrollmentResponse.getErrMsg());
										enrollmentList1.add(enrollmentResponse.getEnrollment());
										
									}
									else if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE) 
											&& (enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-008")|| enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-009"))){
										LOGGER.error(LOGGER_ERR_FFM_MSG+ enrollmentResponse.getErrMsg());

										enrollmentList1.add(enrollmentObj);
										enrollmentService.abortEnrollment(enrollmentObj);
										updateConsumerApplicationById(enrollmentObj.getSsapApplicationid(), EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED);
									}else{
										LOGGER.error(LOGGER_ERR_FFM_MSG+ enrollmentResponse.getErrMsg());
										enrollmentService.abortEnrollment(enrollmentObj);
										
									}
								}
								enrollmentResponse.setEnrolleeNameMap(enrolleeNameMap);
								enrollmentResponse.setEnrollmentList(enrollmentList1);
							}
							
						}
					}else {
						if(null != enrollmentResponse){
							enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
							enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
							enrollmentResponse.setErrCode(ERROR_CODE_104);
						}
						LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_ENROLLMENT_CREATE_FAILURE);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Message : " + e.getMessage());
			LOGGER.error("Cause : " + e.getCause());
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("" + e.getMessage());
			enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
			enrollmentResponse.setErrCode(ERROR_CODE_104);
			LOGGER.error("Unknown Error occured. Exception = " + e.getMessage(),e);
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	
	/**@since 22/10/2013
	 * @author Pratap
	 * @param enrollmentResponse
	 * @return
	 */
	private EnrollmentResponse sendEnrollmentDetailsToBB(EnrollmentResponse enrollmentResponse, String shopping_id){
		try{
			BenefitBayRequestDTO benefitBayRequestDTO = enrollmentService.populateBenefitBayRequestDTO(enrollmentResponse);
			benefitBayRequestDTO.setHouseholdID(shopping_id);
			LOGGER.info("benefitBayRequest : " + benefitBayRequestDTO);
			BenefitBayResponseDTO benefitBayResponseDTO = restTemplate.postForObject(GhixEndPoints.FfmEndPoints.APPLICANT_ENROLLMENTBB_URL,benefitBayRequestDTO,BenefitBayResponseDTO.class);
			
	        if(benefitBayResponseDTO!=null && benefitBayResponseDTO.getAcceptanceStatus().equals("S")){
	        	enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_200);
	        }else{
	        	enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
				enrollmentResponse.setModuleStatusCode(STATUS_CODE_E006);
	        }
		}catch(Exception ex){
			LOGGER.error("Error occured in sendEnrollmentDetailsToBB() Exception = " + ex.getMessage(),ex);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
			enrollmentResponse.setModuleStatusCode(STATUS_CODE_E006);
		}
		
        return enrollmentResponse;
	}

	//@ApiOperation(value="Find enrollments by plan", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value = "/findenrollmentsbyplan", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentsByPlan(@RequestBody String enrollmentRequestStr) {

		LOGGER.info("=================== inside findEnrollmentsByPlan ===================");

		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<Enrollment> enrollmentList = null;
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream
				.fromXML(enrollmentRequestStr);
		EnrollmentResponse enrollmentResponse = null;
		Enum<MarketType> planMarketType = enrollmentRequest.getPlanMarket();
		String planId = (enrollmentRequest.getPlanId() != null) ? enrollmentRequest
				.getPlanId().toString() : null;
		String enrollmentStatus = isNotNullAndEmpty(enrollmentRequest
				.getEnrollmentStatus()) ? enrollmentRequest
				.getEnrollmentStatus().toString() : null;
		String planMarketTypeValue = null;
		if (planMarketType != null) {
			Integer lookUpValue = lookupService
					.getlookupValueIdByTypeANDLookupValueCode(
							EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE,
							planMarketType.toString());
			planMarketTypeValue = Integer.toString(lookUpValue);
		}

		try {
			enrollmentList = enrollmentService.findEnrollmentsByPlan(planId,
					enrollmentRequest.getPlanLevel(), planMarketTypeValue,
					enrollmentRequest.getRatingRegion(),
					enrollmentRequest.getStartDate(),
					enrollmentRequest.getEndDate(), enrollmentStatus);

			enrollmentResponse = getPlanDataFromEnrollmentList(enrollmentList);

		} catch (Exception e) {
			LOGGER.error("No enrollments found for the plan search criteria."
					+ e.getMessage(),e);
		}
		return xstream.toXML(enrollmentResponse);
	}


	/**
	 * This method is to iterate the enrollment list and setting the
	 * EnrollmentPlanResponseDTO values from the list.
	 * 
	 * @param enrollmentList
	 * @return List<EnrollmentPlanResponseDTO> in the form of EnrollmentResponse
	 */
	private EnrollmentResponse getPlanDataFromEnrollmentList(
			List<Enrollment> enrollmentList) {

		List<EnrollmentPlanResponseDTO> enrollmentPlanResponseDTOList = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();

		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			enrollmentPlanResponseDTOList = new ArrayList<EnrollmentPlanResponseDTO>();
			for (Enrollment enrollmentObj : enrollmentList) {
				if (enrollmentObj != null) {
					EnrollmentPlanResponseDTO enrlPlanRespObj = new EnrollmentPlanResponseDTO();
					enrlPlanRespObj.setEnrollmentId(enrollmentObj.getId());
					if (isNotNullAndEmpty(enrollmentObj.getPlanId())) {
						enrlPlanRespObj.setPlanId(enrollmentObj.getPlanId()
								.toString());
					}
					if (isNotNullAndEmpty(enrollmentObj.getIssuerId())) {
						enrlPlanRespObj.setIssuerId(Integer
								.toString(enrollmentObj.getIssuerId()));
					}
					if (isNotNullAndEmpty(enrollmentObj.getUpdatedOn())) {
						enrlPlanRespObj.setUpdatedDate(enrollmentObj
								.getUpdatedOn().toString());
					}
					if (isNotNullAndEmpty(enrollmentObj
							.getEnrollmentStatusLkp())) {
						enrlPlanRespObj.setEnrollmentStatus(enrollmentObj
								.getEnrollmentStatusLkp().getLookupValueCode());
					}

					enrollmentPlanResponseDTOList.add(enrlPlanRespObj);
				}

			}
			enrollmentResponse
					.setEnrollmentPlanResponseDTOList(enrollmentPlanResponseDTOList);
		}
		return enrollmentResponse;
	}

	//@ApiOperation(value="Find tax filing date for year", notes="", httpMethod="POST", produces="application/json")
	@RequestMapping(value = "/findtaxfilingdateforyear", method = RequestMethod.POST)
	@ResponseBody
	public String findTaxFilingDateForYear(@RequestBody String year) {
		TaxYear taxYearObj = null;
		try {
			LOGGER.info("current tax year= " + year);
			taxYearObj = taxFilingService.findTaxFilingDateByYear(year);

		} catch (Exception e) {
			LOGGER.error("No tax filing date found for the year " + year + ""
					+ e.getMessage(), e);

		}
		if (taxYearObj != null) {
			LOGGER.info("TaxDueYear= " + taxYearObj.getTaxDueYear());
			return platformGson.toJson(taxYearObj, taxYearObj.getClass());
		}

		return null;

	}

	//@ApiOperation(value="Create D2C Enrollment With Enrollees", notes="", httpMethod="POST", produces="application/json")
	@RequestMapping(value="/createD2CEnrollmentWithEnrollees", method=RequestMethod.POST)
	@ResponseBody
	public EnrollmentResponse createD2CEnrollmentWithEnrollees(@RequestBody D2CEnrollmentRequest d2CEnrollmentRequest) {
		EnrollmentResponse response = enrollmentService.createD2CEnrollmentWithEnrollees(d2CEnrollmentRequest);
		if(GhixConstants.RESPONSE_SUCCESS.equals(response.getStatus())) {
			enrollmentPartnerNotificationService.publishPartnerEvent(response.getEnrollmentId(), d2CEnrollmentRequest.getCmrHouseholdId());
		}
		return response;
	}
	
	//@ApiOperation(value="update enrollment cmr_household_id by ssap_application_id list", httpMethod="POST", produces="application/json")
	@RequestMapping(value="/updateEnrollmentCmrHouseholdId", method=RequestMethod.POST)
	@ResponseBody
	public EnrollmentUpdateHouseholdDTO updateEnrollmentCmrHouseholdId(@RequestBody EnrollmentUpdateHouseholdDTO requestDTO) {
		EnrollmentUpdateHouseholdDTO response = null;
		try{
			response = enrollmentService.updateEnrollmentCmrHouseholdId(requestDTO);
		}catch(Exception e){
			LOGGER.error("ERROR on updateEnrollmentCmrHouseholdId : " + e.getMessage(), e);
			response.setStatus(EnrollmentUpdateHouseholdDTO.Status.FAILURE);
			response.setErrorMsg(e.getMessage());
		}
			
		return response;
	}
	
	//@ApiOperation(value="Update D2C Enrollment Status", notes="", httpMethod="POST", produces="application/json")
	@RequestMapping(value="/updateD2CEnrollmentStatus", method=RequestMethod.POST)
	@ResponseBody
	public EnrollmentResponse updateD2CEnrollmentStatus(@RequestBody D2CEnrollmentRequest d2CEnrollmentRequest){
		return enrollmentService.updateD2CEnrollmentStatus(d2CEnrollmentRequest);
	}

	//@ApiOperation(value="Save Manual Enrollment", notes="", httpMethod="POST", produces="application/json")
	@RequestMapping(value = "/saveManualEnrollment", method = RequestMethod.POST)
	@ResponseBody
	public EnrollmentResponse saveManualEnrollment(@RequestBody ManualEnrollmentRequest manualEnrollmentRequest) {

		Enrollment createManualEnroll = null;
		String response = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		createManualEnroll = new Enrollment();

		if (manualEnrollmentRequest.getCmrHouseholdID() != null) {
			createManualEnroll.setCmrHouseHoldId(manualEnrollmentRequest.getCmrHouseholdID());
		}
		if (manualEnrollmentRequest.getIssuerName() != null) {
			createManualEnroll.setInsurerName(manualEnrollmentRequest.getIssuerName());
		}
		if (manualEnrollmentRequest.getPlanName() != null) {
			createManualEnroll.setPlanName(manualEnrollmentRequest.getPlanName());
		}
		if (manualEnrollmentRequest.getPlanLevel() != null) {
			createManualEnroll.setPlanLevel(manualEnrollmentRequest.getPlanLevel());
		}
		if (manualEnrollmentRequest.getEffectiveDate() != null) {
			createManualEnroll.setBenefitEffectiveDate(manualEnrollmentRequest.getEffectiveDate());
		}

		if (manualEnrollmentRequest.getAptc() != null) {
			createManualEnroll.setAptcAmt(manualEnrollmentRequest.getAptc());
		}	

		if (manualEnrollmentRequest.getPremium() != null) {
			createManualEnroll.setGrossPremiumAmt(manualEnrollmentRequest.getPremium());
		}

		if (manualEnrollmentRequest.getNetPremium()!= null) {
			createManualEnroll.setNetPremiumAmt(manualEnrollmentRequest
					.getNetPremium());
		}

		String exchangeType = manualEnrollmentRequest.getExchangeType();
		if (!StringUtils.isEmpty(exchangeType)) {
			exchangeType =  (("ON").equalsIgnoreCase(exchangeType)) ? EnrollmentPrivateConstants.EXCHANGE_TYPE_ONEXCHANGE : EnrollmentPrivateConstants.EXCHANGE_TYPE_OFFEXCHANGE;
			LookupValue exchTypeLookupVal = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, exchangeType.toUpperCase());	
			createManualEnroll.setExchangeTypeLkp(exchTypeLookupVal);
		}

		if (!("".equals(manualEnrollmentRequest.getConfirmationNumber()))) {
			createManualEnroll.setConfirmationNumber(manualEnrollmentRequest
					.getConfirmationNumber());
		}	
		if (manualEnrollmentRequest.getEnrollmentModality() != null
				&& !("".equals(manualEnrollmentRequest.getEnrollmentModality()))) {
			createManualEnroll.setEnrollmentModalityLkp(lookupService
					.getlookupValueByTypeANDLookupValueCode(
							EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE,
							manualEnrollmentRequest.getEnrollmentModality()));
		}

		if (manualEnrollmentRequest.getPolicyNumber() != null && !StringUtils.isEmpty(manualEnrollmentRequest.getPolicyNumber())) {
			createManualEnroll.setIssuerAssignPolicyNo(manualEnrollmentRequest.getPolicyNumber());
		}

		createManualEnroll.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,
						EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));

		// Manual application : save the application id : START
		if (manualEnrollmentRequest.getApplicationId() != null) {
			createManualEnroll.setSsapApplicationid(manualEnrollmentRequest
					.getApplicationId());
		}

		if (manualEnrollmentRequest.getCarrierAppId() != null) {
			createManualEnroll.setCarrierAppId(manualEnrollmentRequest
					.getCarrierAppId());
		}

		if(manualEnrollmentRequest.getPlanName() != null){
			createManualEnroll.setPlanName(manualEnrollmentRequest.getPlanName());
		}	
		if (manualEnrollmentRequest.getPlanId() != null) {
			Plan plan = new Plan();
			plan.setId(manualEnrollmentRequest.getPlanId());
			createManualEnroll.setPlanId(manualEnrollmentRequest.getPlanId());
			PlanResponse pr= getPlanResponse(""+manualEnrollmentRequest.getPlanId(), createManualEnroll.getBenefitEffectiveDate());
			createManualEnroll.setCMSPlanID(pr.getIssuerPlanNumber());
			setEnrollmentCommissionData(createManualEnroll, pr);
		}	
		// HIX-55363
		if (manualEnrollmentRequest.getInsuranceType() != null && !("".equals(manualEnrollmentRequest.getInsuranceType()))) {
			createManualEnroll.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE,manualEnrollmentRequest.getInsuranceType().toUpperCase()));
		}	

		if (org.apache.commons.lang.StringUtils.isNotEmpty(manualEnrollmentRequest.getPrefferedTimeToContact())) {
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueLabel("PREFERRED_TIME_TO_CONTACT",manualEnrollmentRequest.getPrefferedTimeToContact().toLowerCase());
			createManualEnroll.setPrefferedContactTime(lookupValue);
		}
		if(StringUtils.isNotEmpty(manualEnrollmentRequest.getSponsorName())){
			createManualEnroll.setSponsorName(manualEnrollmentRequest.getSponsorName());
		}	
		if(StringUtils.isNotEmpty(manualEnrollmentRequest.getSubscriberName())){
			createManualEnroll.setSubscriberName(manualEnrollmentRequest.getSubscriberName());
		}	
		if(StringUtils.isNotEmpty(manualEnrollmentRequest.getStateExchangeCode())){
			createManualEnroll.setStateExchangeCode(manualEnrollmentRequest.getStateExchangeCode());
		}
		if(StringUtils.isNotEmpty(manualEnrollmentRequest.getCarrierAppUrl())){
			createManualEnroll.setCarrierAppUrl(manualEnrollmentRequest.getCarrierAppUrl());
		}	
		if(manualEnrollmentRequest.getIssuerId()!=0){
			createManualEnroll.setIssuerId(manualEnrollmentRequest.getIssuerId());
		}
		if(StringUtils.isNotEmpty(manualEnrollmentRequest.getHiosIssuerId())){
			createManualEnroll.setHiosIssuerId(manualEnrollmentRequest.getHiosIssuerId());
		}	
		LookupValue enrollmentTypeLkp = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE, EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL);
		createManualEnroll.setEnrollmentTypeLkp(enrollmentTypeLkp);

		createManualEnroll.setSubmittedToCarrierDate(new TSDate());

		//HIX-63622 Update Enrollment ManualEnrollment API to accommodate Term Length, Benefit Amount
		if (manualEnrollmentRequest.getTermLength() != null) {
			createManualEnroll.setTermLength(manualEnrollmentRequest.getTermLength());
		}

		if (manualEnrollmentRequest.getBenefitAmount() != null) {
			createManualEnroll.setBenefitAmount(manualEnrollmentRequest.getBenefitAmount());
		}

		try {
			createManualEnroll.setCreatedBy(userService.getLoggedInUser());
			createManualEnroll.setUpdatedBy(userService.getLoggedInUser());
			// HIX-44501: Manual Enrollment creation
			EnrollmentUtilsForCap
			.setCapAgentId(createManualEnroll, userService);
			
			// Save enrollee and enrollment
			boolean bStatus = true;
			// HIX-54652 -CAP: Add Enrollment Functionality Appears to be
			// Failing
			// Old Manual enrollemnt UI will not send enrolle info
			if (null != manualEnrollmentRequest.getApplicantDtoList()
					&& !manualEnrollmentRequest.getApplicantDtoList().isEmpty()) {
				
				bStatus = enrollmentService.saveManualAppEnrolleeInfo(manualEnrollmentRequest.getApplicantDtoList(),createManualEnroll);
			}
			else{
				createManualEnroll = enrollmentService.saveEnrollment(createManualEnroll);
				if(null != createManualEnroll.getEnrollmentIssuerCommision()) {
					createManualEnroll.getEnrollmentIssuerCommision().setEnrollment(createManualEnroll);
					enrollmentIssuerCommisionRepository.saveAndFlush(createManualEnroll.getEnrollmentIssuerCommision());
				}
			}
			
			response = (bStatus == true) ? GhixConstants.RESPONSE_SUCCESS: GhixConstants.RESPONSE_FAILURE;

			if(response.equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
				//Send Email 
				//HIX-105020 commenting sending  email code. 
				//sendEmailToConsumer(createManualEnroll.getCmrHouseHoldId(), createManualEnroll, true);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setEnrollmentId(createManualEnroll.getId());
				String eventLogResponse=null;
				// HIX-45304
				eventLogResponse = enrollmentEventPrivateService.saveConsumerErnrollmentEvent(createManualEnroll , 
				EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString(),createManualEnroll.getEnrollmentStatusLkp().getLookupValueLabel());
				enrollmentPrivateUtils.recordApplicationEnrollmentPrivateEvent(createManualEnroll, EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString(), createManualEnroll.getEnrollmentStatusLkp().getLookupValueLabel());
				enrollmentService.publishPartnerEvent(createManualEnroll.getId(), createManualEnroll.getCmrHouseHoldId());
				LOGGER.info("Event logged for manual enrollment: ");
			}

		} catch (Exception ge) {
			giExceptionHandler.recordFatalException(Component.ENROLLMENT, EnrollmentConstants.MODULE_STATUS_CODE_E003, "Error Creating Manual Enrollment" , ge) ;
			LOGGER.error("Error Creating Manual Enrollment" + ge.getMessage(), ge);
			response = GhixConstants.RESPONSE_FAILURE;
			enrollmentResponse.setErrMsg(ge.getMessage());
		}
		return enrollmentResponse;
	}

	
	//@ApiOperation(value="Get Manual Enrollment By CmrHouseHoldId", notes="", httpMethod="POST", produces="application/json")
	@RequestMapping(value = "/getManualEnrollmentByCmrHouseHoldId", method = RequestMethod.POST)
	@ResponseBody
	public ManualEnrollmentResponse getManualEnrollmentByCmrHouseHoldId(
			@RequestBody ManualEnrollmentRequest manualEnrollmentRequest) {
		  Enrollment enrollment =  null;
		  ManualEnrollmentResponse  manualEnrollmentResponse = new ManualEnrollmentResponse();
		    try
		    {
		    	enrollment  =  enrollmentService.findLatestNonEcommittedEnrollmentByCmrHouseholdId(manualEnrollmentRequest.getCmrHouseholdID());
		    	if(enrollment != null){
		    		
		    		if(enrollment.getCmrHouseHoldId() != null){		    			
		    	    	manualEnrollmentResponse.setCmrHouseholdID(enrollment.getCmrHouseHoldId());
		    		}
		    		if(enrollment.getInsurerName() != null){	
		    			manualEnrollmentResponse.setIssuerName(enrollment.getInsurerName());
		    		}
		    		
		    		if(enrollment.getPlanName() != null){	
		    			manualEnrollmentResponse.setPlanName(enrollment.getPlanName());
		    		}
		    		if(enrollment.getPlanLevel() != null){	
		    			manualEnrollmentResponse.setPlanlevel(enrollment.getPlanLevel());
		    		}	
		    		
		    		if(enrollment.getConfirmationNumber() != null){	
		    			manualEnrollmentResponse.setConfirmationNumber(enrollment.getConfirmationNumber());
		    		}	
		    		
		    		if(enrollment.getEnrollmentModalityLkp()!= null){	
		    			manualEnrollmentResponse.setEnrollmentModality(enrollment.getEnrollmentModalityLkp().getLookupValueCode());
		    		}	
		    		
		    		if(enrollment.getExchangeTypeLkp() != null){	
		    			manualEnrollmentResponse.setExchangeType(enrollment.getExchangeTypeLkp().getLookupValueCode());
		    		}	
		    				    		
		    		if(enrollment.getGrossPremiumAmt() != null){	
		    			manualEnrollmentResponse.setPremium(enrollment.getGrossPremiumAmt());
		    		}
		    		if(enrollment.getAptcAmt() != null){	
		    			manualEnrollmentResponse.setAptc(enrollment.getAptcAmt());
		    		}
		    	
		    		
		    		if(enrollment.getBenefitEffectiveDate() != null){	
		    			manualEnrollmentResponse.setEffectiveDate(enrollment.getBenefitEffectiveDate());
		    		}
		    		
		    		if(enrollment.getIssuerAssignPolicyNo()!=null){
		    			manualEnrollmentResponse.setPolicyNumber(enrollment.getIssuerAssignPolicyNo());
		    		}else if(enrollment.getExchangeAssignPolicyNo()!=null){
		    			manualEnrollmentResponse.setPolicyNumber(enrollment.getExchangeAssignPolicyNo());
		    		}
		    		
		    		if(enrollment.getId() != null){
		    			manualEnrollmentResponse.setEnrollmentId(enrollment.getId());
		    		}
		    		
		    		if(enrollment.getEnrollmentStatusLkp().getLookupValueCode() != null){
		    			manualEnrollmentResponse.setEnrollmentStatus(enrollment.getEnrollmentStatusLkp().getLookupValueCode());
		    		}
		    		
		    		manualEnrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		    		
		    	}
		    	else{
			    		manualEnrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			    		
		    	}
		    		
		    }
		    catch (Exception ge) {
				LOGGER.error("Error retrieving Manual Enrollment" + ge.getMessage() , ge);
				
		    }
		    return manualEnrollmentResponse;
		
	}
	
	//@ApiOperation(value="Delete manual enrollment", notes="", httpMethod="POST", produces="application/json")
	@RequestMapping(value = "/deleteManualEnrollment", method = RequestMethod.POST)
	@ResponseBody
	public ManualEnrollmentResponse deleteManualEnrollment(
			@RequestBody ManualEnrollmentRequest manualEnrollmentRequest) {
		    ManualEnrollmentResponse  manualEnrollmentResponse = new ManualEnrollmentResponse();
		    Enrollment manualEnroll  =  enrollmentService.findManualEnrollmentByCmrHouseholdId(manualEnrollmentRequest.getCmrHouseholdID());
		    if(manualEnroll != null){
		      manualEnroll.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED));
		      manualEnroll.setAbortTimestamp(new TSDate());
		    try
			    {
			    	 enrollmentService.saveEnrollment(manualEnroll);
			    	 manualEnrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			    }
			    catch (Exception ge) {
					LOGGER.error("Error deleting Manual Enrollment" + ge.getMessage() , ge);
					manualEnrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			    }
		    }
		    else{
		    	manualEnrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		    }
		  
		return manualEnrollmentResponse;
	}


	//@ApiOperation(value="Find enrollment plan level count by broker", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value = "/findEnrollmentPlanLevelCountByBroker", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentPlanLevelCountByBroker(
			@RequestBody String enrollmentRequestStr) {
		List<EnrollmentCountDTO> enrollmentCountDTOList = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();

		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream
				.fromXML(enrollmentRequestStr);
		Integer assisterBrokerId = enrollmentRequest.getAssisterBrokerId();

		if (assisterBrokerId == null || assisterBrokerId == 0) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg("Broker Id is Null or 0");
			LOGGER.warn("Broker Id is Null or 0");
		} else if (!isNotNullAndEmpty(enrollmentRequest.getStartDate())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg("Start Date is Null or Empty");
			LOGGER.warn("Start Date is Null or Empty");
		} else if (!isNotNullAndEmpty(enrollmentRequest.getEndDate())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_203);
			enrollmentResponse.setErrMsg("End Date is Null or Empty");
			LOGGER.warn("End Date is Null or Empty");
		} else if (!isNotNullAndEmpty(enrollmentRequest.getRole())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_204);
			enrollmentResponse.setErrMsg("Role is Null or Empty");
			LOGGER.warn("Role is Null or Empty");
		} else {
			Date startDate = DateUtil.StringToDate(
					enrollmentRequest.getStartDate(),
					GhixConstants.REQUIRED_DATE_FORMAT);
			Date endDate = DateUtil.StringToDate(
					enrollmentRequest.getEndDate(),
					GhixConstants.REQUIRED_DATE_FORMAT);
			String role = enrollmentRequest.getRole().toString();

			enrollmentCountDTOList = enrollmentService
					.findEnrollmentPlanLevelCountByBroker(assisterBrokerId,
							role, startDate, endDate);
			enrollmentResponse.setEnrollmentCountDTO(enrollmentCountDTOList);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_200);
			enrollmentResponse.setErrMsg(GhixConstants.RESPONSE_SUCCESS);
		}
		return xstream.toXML(enrollmentResponse);
	}

	/**
	 * @author shanbhag_a
	 * 
	 *         Returns list of Enrollment for provided CMR_Household_Id. Also
	 *         returns Enrollment_Plan info for corresponding plan id of
	 *         enrollment
	 * 
	 * @param id
	 * @return
	 */

	//@ApiOperation(value="Find enrollments by CmrHouseholdId", notes="", httpMethod="GET", produces="application/xml")
	@RequestMapping(value = "/findEnrollmentsByCmrHouseholdId/{cmrHouseHoldId}", method = RequestMethod.GET)
	@ResponseBody
	public String findEnrollmentByCMRHouseholdId(
			@PathVariable("cmrHouseHoldId") String cmrHouseHoldId) {
		LOGGER.info("============inside findEnrollmentByCMRHouseholdId============");
		
		//long t0 = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("Received cmr household id : " + cmrHouseHoldId);
		EnrollmentPlan enrollmentPlan = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (isNotNullAndEmpty(cmrHouseHoldId)
				&& !cmrHouseHoldId.equalsIgnoreCase("0")) {
			List<Enrollment> enrollments = enrollmentService
					.findEnrollmentByCmrHouseholdId(Integer
							.parseInt(cmrHouseHoldId));
			enrollmentResponse.setEnrollmentList(enrollments);

			for (Enrollment enrollment : enrollments) {
				enrollment.setEnrollmentStatusLabel(enrollment
						.getEnrollmentStatusLkp().getLookupValueLabel());

				StringBuilder enrolleeNames = new StringBuilder();
				for (Enrollee enrollee : enrollment.getEnrollees()) {
					if (enrollee
							.getPersonTypeLkp()
							.getLookupValueCode()
							.equalsIgnoreCase(
									EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER)
							|| enrollee
									.getPersonTypeLkp()
									.getLookupValueCode()
									.equalsIgnoreCase(
											EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE)) {
						if (isNotNullAndEmpty(enrollee.getFirstName())) {
							enrolleeNames.append(enrollee.getFirstName());
							enrolleeNames.append(" ");
						}
						if (isNotNullAndEmpty(enrollee.getLastName())) {
							enrolleeNames.append(enrollee.getLastName());
						}
						enrolleeNames.append(", ");
					}
				}
				if (enrolleeNames.length() > 2) {
					enrolleeNames.delete(enrolleeNames.length() - 2,
							enrolleeNames.length());
				}
				enrollment.setEnrolleeNames(enrolleeNames.toString());
				enrollmentPlan = enrollmentService.findEnrollmentPlanByPlanId(enrollment.getPlanId());
				enrollment.setEnrollmentPlan(enrollmentPlan);
			}

			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} else {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse
					.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		//long t1 = TimeShifterUtil.currentTimeMillis();
		//String response = xstream.toXML(enrollmentResponse);
		/*long t2 = TimeShifterUtil.currentTimeMillis();
		
		long timeDiff1 = t2 - t0;
		long timeDiff2 = t2 - t1;
		
		System.out.println("time taken for the entire method" + timeDiff1);
		System.out.println("time taken for generating the xml " + timeDiff2);
			*/
		
		return xstream.toXML(enrollmentResponse);
	}
	
	//@ApiOperation(value="Find enrollments by SsapApplicationId", notes="", httpMethod="GET", produces="application/xml")
	@RequestMapping(value = "/findEnrollmentBySsapApplicationId/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public String findEnrollmentBySsapApplicationId(@PathVariable("ssapApplicationId") String ssapApplicationId) {
		LOGGER.debug("============inside findEnrollmentBySsapApplicationId============" + ssapApplicationId);
	
		//long t0 = TimeShifterUtil.currentTimeMillis();
		
		EnrollmentPlan enrollmentPlan = null;
		EnrollmentConsumerPortalDTO ecpDTO = new EnrollmentConsumerPortalDTO();
		Enrollment enrollment = null;
		
		if (isNotNullAndEmpty(ssapApplicationId) && !ssapApplicationId.equalsIgnoreCase("0")) {
			List<Enrollment> enrollments = enrollmentService.findEnrollmentBySsapApplicationId(Long.parseLong(ssapApplicationId));
			if(enrollments == null){
				ecpDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
			    enrollment = enrollments.get(0);
				enrollmentPlan = enrollmentService.findEnrollmentPlanByPlanId(enrollment.getPlanId());
				enrollment.setEnrollmentPlan(enrollmentPlan);
				ecpDTO = getSsapApplicationPlanMetaData(enrollment);
	    
				ecpDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		} else {
			ecpDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		//long t1 = TimeShifterUtil.currentTimeMillis();
		//String response = xstream.toXML(ecpDTO);
		//long t2 = TimeShifterUtil.currentTimeMillis();
		
		/*long timeDiff1 = t2 - t0;
		long timeDiff2 = t2 - t1;
		
		System.out.println("time taken for the entire method" + timeDiff1);
		System.out.println("time taken for generating the xml " + timeDiff2);*/
		return xstream.toXML(ecpDTO);
	}
	
	@RequestMapping(value = "/getenrolleeatributebyid", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrolleeAtributeById(@RequestBody String enrollmentRequestStr) {
		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		try{
			EnrollmentRequest enrollmentRequest = (EnrollmentRequest) platformGson.fromJson(enrollmentRequestStr, EnrollmentRequest.class);
			
			if(enrollmentRequest.getEnrollmentId() ==null || enrollmentRequest.getEnrollmentId() ==0){
				
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(enrollmentRequest.getEnrolleeAttributeList()==null || enrollmentRequest.getEnrolleeAttributeList().isEmpty()){
				
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EnrollmentAttributeEnum_IS_NULL_OR_EMPTY);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(enrollmentRequest.getEnrolleeAttributeList().contains(null)){
				
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EnrollmentAttributeEnum_IS_NULL_OR_EMPTY);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else {
				
				List<EnrolleeDTO> enrolleeDTOList = enrollmentService.getEnrolleeAtributeById(enrollmentRequest);
				
				if(null != enrolleeDTOList && !enrolleeDTOList.isEmpty() ){
					enrollmentResponse.setEnrolleeDTOList(enrolleeDTOList);
					enrollmentResponse.setEnrollmentExists(true);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}else{
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_NOT_FOUND_FOR_GIVEN_ID);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}

			}
		}catch(Exception e){
			enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(e,5));
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		
		return platformGson.toJson(enrollmentResponse);
	}

	private EnrollmentConsumerPortalDTO getSsapApplicationPlanMetaData(Enrollment enrollment) {
		LOGGER.debug("EnrollmentController.getSsapApplicationPlanMetaData - entered");
	
		if(enrollment==null){
			return null;
		}
			
		List<Status> activeStatus = new ArrayList<Status>();
		activeStatus.add(Status.ACTIVE);
		activeStatus.add(Status.PENDING);
		EnrollmentPlan enrollmentPlan = null;
		PlanResponse planResponse = null;
		EnrollmentConsumerPortalDTO ecpDTO = new EnrollmentConsumerPortalDTO();
		
		try {
			  ecpDTO = new EnrollmentConsumerPortalDTO();
	 			 			
	 			enrollmentPlan = enrollment.getEnrollmentPlan();
	 			if(enrollmentPlan==null){
	 				return null;
	 			}
	 			String planId =  ""+enrollment.getPlanId();
	 			if(!EnrollmentPrivateUtils.isNotNullAndEmpty(planId) && null != enrollment.getEnrollmentPlan()){
	 				planId = ""+enrollmentPlan.getPlanId();
	 			}
	 			planResponse= getPlanResponse(planId, enrollment.getBenefitEffectiveDate());
	 			ecpDTO.setSsapApplicationId(enrollment.getSsapApplicationid());
	 			ecpDTO.setPlanId(enrollmentPlan.getPlanId());
				ecpDTO.setPlanName(enrollment.getPlanName());
    			ecpDTO.setNetworkType(enrollmentPlan.getNetworkType());
	 			
	 			if(enrollmentPlan.getIndivDeductible()!=null){
	 		       ecpDTO.setIndivDeductible(enrollmentPlan.getIndivDeductible().doubleValue());
	 			}else if(enrollmentPlan.getFamilyDeductible()!=null){
	 				ecpDTO.setFamilyDeductible(enrollmentPlan.getFamilyDeductible().doubleValue());
	 			}
	 			
	 			ecpDTO.setGenericMedication(enrollmentPlan.getGenericMedication());
	 			ecpDTO.setOfficeVisit(enrollmentPlan.getOfficeVisit());
	 			if(enrollmentPlan.getIndivOopMax()!=null){
	 				ecpDTO.setIndividualOopMax(enrollmentPlan.getIndivOopMax().doubleValue());
	 			}
	 			if(enrollmentPlan.getFamilyOopMax()!=null){
	 				ecpDTO.setFamilyOopMax(enrollmentPlan.getFamilyOopMax().doubleValue());
	 			}
	 			 		
	 			ecpDTO.setIssuerName(enrollment.getInsurerName());
	 			String logoStr = (null != planResponse) ? planResponse.getIssuerLogo() : null;
	 			ecpDTO.setLogostr(logoStr);
	 			ecpDTO.setMonthlyPremium(enrollment.getGrossPremiumAmt());
	 			ecpDTO.setAptcAmount(enrollment.getAptcAmt());
	 			ecpDTO.setIssuerId(enrollment.getIssuerId());
	 			
	 			String issuerSiteUrl = enrollmentPlan.getIssuerSiteUrl();
	 			if(!StringUtils.isBlank(issuerSiteUrl)){
	 				ecpDTO.setIssuerSiteUrl(issuerSiteUrl);
	 			}
	 			ecpDTO.setEnrollmentId(enrollment.getId());
	 			 				
		} catch (Exception e) {
			LOGGER.error("exception in getting plan metadata from the plan management : ",e);
			
		}
		
		return ecpDTO;
	}

	
	/**
	 * @since 05/06/2014
	 * @param cmrHouseHoldId
	 * @author negi_s
	 * @return Rest API to return specific enrollment details to the consumer portal  
	 */
	//@ApiOperation(value="Get enrollment detail for consumer portal", notes="", httpMethod="GET", produces="application/xml")
	@RequestMapping(value = "/getenrollmentdetailsforconsumerportal/{cmrHouseHoldId}", method = RequestMethod.GET)
	@ResponseBody public String findConsumerDetailsByCMRHouseholdId(@PathVariable("cmrHouseHoldId") String cmrHouseHoldId) {
		LOGGER.info("============inside getenrollmentdetailsforconsumerportal============");
		LOGGER.info("Received cmr household id : " + cmrHouseHoldId);
		
		XStream xstream = GhixUtils.getXStreamStaxObject();		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		List<EnrollmentConsumerPortalDTO> enrollmentConsumerPortalDTOList = new ArrayList<EnrollmentConsumerPortalDTO>();
		PlanResponse planResponse = null;
		
		if (isNotNullAndEmpty(cmrHouseHoldId) && !cmrHouseHoldId.equalsIgnoreCase("0")) {
			List<Enrollment> enrollments = enrollmentService.findEnrollmentByCmrHouseholdId(Integer.parseInt(cmrHouseHoldId));
			for (Enrollment enrollment : enrollments) {
				EnrollmentConsumerPortalDTO enrollmentConsumerPortalDTO = new EnrollmentConsumerPortalDTO();
				planResponse = getPlanResponse(String.valueOf(enrollment.getPlanId()), enrollment.getBenefitEffectiveDate());
				enrollmentConsumerPortalDTO.setPlanId(enrollment.getPlanId());
				if(enrollment.getEnrollmentModalityLkp()!=null){
					enrollmentConsumerPortalDTO.setEnrollmentModality(enrollment.getEnrollmentModalityLkp().getLookupValueCode());
				}
				enrollmentConsumerPortalDTO.setPlanName(enrollment.getPlanName());
				enrollmentConsumerPortalDTO.setLogostr((null != planResponse) ? planResponse.getIssuerLogo() : null);
				enrollmentConsumerPortalDTO.setIssuerName(enrollment.getInsurerName());
				enrollmentConsumerPortalDTO.setMonthlyPremium(enrollment.getGrossPremiumAmt());
				if(null != enrollment.getAptcAmt() && null != enrollment.getGrossPremiumAmt()){
					enrollmentConsumerPortalDTO.setMonthlyPayment(enrollment.getGrossPremiumAmt()-enrollment.getAptcAmt());	
				}else if(null == enrollment.getAptcAmt()){
					enrollmentConsumerPortalDTO.setMonthlyPayment(enrollment.getGrossPremiumAmt()- 0.0f);
				}
				enrollmentConsumerPortalDTO.setSsapApplicationId(enrollment.getSsapApplicationid());
				if(isNotNullAndEmpty(enrollment.getD2cEnrollmentId())){
					enrollmentConsumerPortalDTO.setD2cEnrollmentId(enrollment.getD2cEnrollmentId());
				}
				enrollmentConsumerPortalDTO.setCarrierAppId(enrollment.getCarrierAppId());
				enrollmentConsumerPortalDTO.setIssuerAssignPolicyNo(enrollment.getIssuerAssignPolicyNo());
				
				
				if(enrollment.getInsuranceTypeLkp() != null && enrollment.getInsuranceTypeLkp().getLookupValueCode() != null){
					String insuranceType = enrollment.getInsuranceTypeLkp().getLookupValueCode();
					if(insuranceType.equalsIgnoreCase(HEALTH)){
						enrollmentConsumerPortalDTO.setInsuranceType(Plan.PlanInsuranceType.HEALTH.toString());
					}
					else if(insuranceType.equalsIgnoreCase(DENTAL)){
						enrollmentConsumerPortalDTO.setInsuranceType(Plan.PlanInsuranceType.DENTAL.toString());
					}
					else if(insuranceType.equalsIgnoreCase(STM)){
						enrollmentConsumerPortalDTO.setInsuranceType(Plan.PlanInsuranceType.STM.toString());
					}
					else if(insuranceType.equalsIgnoreCase(AME)){
						enrollmentConsumerPortalDTO.setInsuranceType(Plan.PlanInsuranceType.AME.toString());
					}
					else if(insuranceType.equalsIgnoreCase(VISION)){
						enrollmentConsumerPortalDTO.setInsuranceType(Plan.PlanInsuranceType.VISION.toString());
					}
					else if(insuranceType.equalsIgnoreCase(LIFE)){
						enrollmentConsumerPortalDTO.setInsuranceType(Plan.PlanInsuranceType.LIFE.toString());
					}
					else if(insuranceType.equalsIgnoreCase(MEDICARE)){
						enrollmentConsumerPortalDTO.setInsuranceType(Plan.PlanInsuranceType.MEDICARE.toString());
					}
			 }
							
			 if(enrollment.getEnrollmentStatusLkp() != null && enrollment.getEnrollmentStatusLkp().getLookupValueCode() != null){
				 enrollmentConsumerPortalDTO.setEnrollmentStatus(enrollment.getEnrollmentStatusLkp().getLookupValueCode());
		     }
				// ***** NEW ADDDITION *******
				enrollmentConsumerPortalDTO.setIssuerId(enrollment.getIssuerId());
				enrollmentConsumerPortalDTO.setAptcAmount(enrollment.getAptcAmt());
				enrollmentConsumerPortalDTO.setEmployerContribution(enrollment.getEmployerContribution());
				enrollmentConsumerPortalDTO.setEmployeeContribution(enrollment.getEmployeeContribution());
				enrollmentConsumerPortalDTO.setEnrollmentId(enrollment.getId());
				enrollmentConsumerPortalDTO.setExchangeAssignPolicyNo(enrollment.getExchangeAssignPolicyNo());
				if(isNotNullAndEmpty(enrollment.getBenefitEffectiveDate())){
					enrollmentConsumerPortalDTO.setBenefitEffectiveDateStr(DateUtil.dateToString(enrollment.getBenefitEffectiveDate(),GhixConstants.REQUIRED_DATE_FORMAT));
					enrollmentConsumerPortalDTO.setBenefitEffectiveDateStrWithOutSlashes(DateUtil.dateToString(enrollment.getBenefitEffectiveDate(),GhixConstants.REQUIRED_DATE_FORMAT_WITH_OUT_SLASHES));
					
				}
				enrollmentConsumerPortalDTO.setBenefitEffectiveDate(enrollment.getBenefitEffectiveDate());
				enrollmentConsumerPortalDTO.setBenefitEndDate(enrollment.getBenefitEndDate());
				enrollmentConsumerPortalDTO.setEnrolleeNames(getEnrolleeNames(enrollment));
				enrollmentConsumerPortalDTO.setCompanyLogo((null != planResponse) ? planResponse.getIssuerLogo() : null);
				
				enrollmentConsumerPortalDTOList.add(enrollmentConsumerPortalDTO);
			}
			enrollmentResponse.setEnrollmentConsumerPortalDTOList(enrollmentConsumerPortalDTOList);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} else {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}
		
		return xstream.toXML(enrollmentResponse);
	}
	
	
	/**
	 * @since 22/10/2013
	 * @author raja
	 * @param enrollmentResponse
	 * @return
	 */
	private EnrollmentResponse sendEnrollmentDetailsToFFM(Enrollment enrollmentObj, EligLead eligLead, Integer ffmLoggedInUserId, String isUserSwitchToOtherView) {
		XStream xStream = GhixUtils.getXStreamStaxObject();

		FFMEnrollmentRequest populateFFMFields = populateFFMFieldsFromEnrollment(enrollmentObj, eligLead, ffmLoggedInUserId, isUserSwitchToOtherView);
		LOGGER.info("===Calling HUB APPLICANT_ENROLLMENT_URL===");
		final String postResp = restTemplate.postForObject(GhixEndPoints.FfmEndPoints.APPLICANT_ENROLLMENT_URL,xStream.toXML(populateFFMFields), String.class);
		EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xStream.fromXML(postResp);
		FFMEnrollmentResponse fFMEnrollmentResponse = new FFMEnrollmentResponse();
		fFMEnrollmentResponse = enrollmentResponse.getfFMEnrollmentResponse();
		
		if(isNotNull(enrollmentResponse) && (isNotNullAndEmpty(enrollmentResponse.getStatus()) && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))){
			if(isNotNull(fFMEnrollmentResponse) && (isNotNullAndEmpty(fFMEnrollmentResponse.getResponseCode()) && fFMEnrollmentResponse.getResponseCode().equalsIgnoreCase("E-008"))){
				Integer enrollmentId = fFMEnrollmentResponse.getEnrollmentId();
				Enrollment  enrollment = enrollmentService.findById(enrollmentId);
				enrollmentResponse.setEnrollment(enrollment);	
				enrollmentResponse.setModuleStatusCode(fFMEnrollmentResponse.getResponseCode());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setGiAndFfmDataList(fFMEnrollmentResponse.getGiAndFfmDataList());
				enrollmentResponse.setErrMsg("Applicant Enrollment  : There is a difference in the FFM and the GI premium amount");
			}
			else{
				if(!EnrollmentUtils.isNotNullAndEmpty(enrollmentResponse.getErrMsg())){
				enrollmentResponse.setErrMsg("Applicant Enrollment  : Try again Error !");
				}
			}
			return enrollmentResponse;
		}
		
		if(enrollmentResponse==null || fFMEnrollmentResponse == null || fFMEnrollmentResponse.getEnrollmentId()==null || fFMEnrollmentResponse.getEnrollmentId()==0){
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}else{
			Integer enrollmentId = fFMEnrollmentResponse.getEnrollmentId();
			Enrollment  enrollment = enrollmentService.findById(enrollmentId);
			
			if(enrollment != null){
				enrollment.setExchangeAssignPolicyNo(fFMEnrollmentResponse.getFfeAssignedPolNo());
				enrollment.setIssuerAssignPolicyNo(fFMEnrollmentResponse.getIssuerAssignedPolNo());
				enrollment.setPaymentTxnId(fFMEnrollmentResponse.getPaymentTransactionId());
				
				//Only if difference between premium is less than 2 dollars
				if(isNotNullAndEmpty(fFMEnrollmentResponse.getGiAndFfmDataList())){
					Map<String,String> giAndFfmDifMap=fFMEnrollmentResponse.getGiAndFfmDataList();
//					Float netPremiumAmount=enrollment.getNetPremiumAmt()+Float.parseFloat(giAndFfmDifMap.get(CHANGE_IN_PREMIUM));
//					enrollment.setNetPremiumAmt(netPremiumAmount);
//					enrollment.setGrossPremiumAmt(netPremiumAmount+enrollment.getAptcAmt());
					Float netPremiumAmount = Float.parseFloat(giAndFfmDifMap.get(FFM_PREMIUM_AMT));
					Float aptcAmount = Float.parseFloat(giAndFfmDifMap.get(FFM_APTC_AMT));
					enrollment.setNetPremiumAmt(netPremiumAmount);
					enrollment.setAptcAmt(aptcAmount);
					enrollment.setGrossPremiumAmt(netPremiumAmount + aptcAmount);
				}
				
				if(isNotNullAndEmpty(enrollment.getEnrollmentStatusLkp()) 
						&& enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED )){
					enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
					enrollment.setAbortTimestamp(null);
				}
				//save the policy no received in FF response
				EnrollmentUtilsForCap.setCapAgentId(enrollment, ffmLoggedInUserId, iSecurableTargetRepository);
				enrollment = enrollmentService.saveEnrollment(enrollment);
				updateConsumerApplicationById(enrollment.getSsapApplicationid(), EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING);
				updateCartStatus(enrollment);
				enrollmentResponse.setEnrollment(enrollment);
				enrollmentResponse.setfFMPolicyId(enrollment.getExchangeAssignPolicyNo());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentService.publishPartnerEvent(enrollment.getId(), enrollment.getCmrHouseHoldId());
			}
			else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrMsg("No Enrollment record Found");
			}
		}
		
		/*if (fFMEnrollmentResponse != null) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		}*/
		return enrollmentResponse;
	}

	/**
	 * @author raja
	 * @since 22/10/2013
	 * @param enrollmentResponse
	 * @return FFMEnrollmentRequest
	 */
	private FFMEnrollmentRequest populateFFMFieldsFromEnrollment(Enrollment enrollmentObj, EligLead eligLead, Integer ffmLoggedInUserId, String isUserSwitchToOtherView){
		FFMEnrollmentRequest ffmEnrollmentRequest = new FFMEnrollmentRequest();
		
		LOGGER.info("populateFFMFieldsFromEnrollment: " 
				+ enrollmentObj != null ? enrollmentObj.getId() : "Enrollment_id == null"
				+ " LoggedInUser: " + ffmLoggedInUserId);
		// Partner/ Consumer IdentificationenrollmentObj
		
		ffmEnrollmentRequest.setInformationExchangeSystemId(EnrollmentPrivateConstants.FFM_INFORMATION_EXCHANGE_SYSTEM_ID);
		AccountUser userObj=null;
		if(isNotNullAndEmpty(ffmLoggedInUserId)){
			userObj = userService.findById(ffmLoggedInUserId.intValue());
		}else{
			try {
				userObj = userService.getLoggedInUser();
			} catch (InvalidUserException e) {
				LOGGER.error("InvalidUserException occured" , e );
			}
		}
		if(isNotNull(enrollmentObj)){
			
			ffmEnrollmentRequest.setEnrollmentId(enrollmentObj.getId());
			ffmEnrollmentRequest.setStateExchangeCode(enrollmentObj.getStateExchangeCode());
			//ffmEnrollmentRequest.setStateExchangeCode("MS");
			
			ffmEnrollmentRequest.setPartnerAssignedConsumerId(enrollmentObj.getGiHouseholdId());
			//ffmEnrollmentRequest.setPartnerAssignedConsumerId(enrollmentObj.getPartnerAssignedConsumerId());
			ffmEnrollmentRequest.setFfeAssignedConsumerId(enrollmentObj.getHouseHoldCaseId());
			ffmEnrollmentRequest.setSsapApplicationId(enrollmentObj.getSsapApplicationid());
			
			if(isNotNull(userObj)){
				Role roleObj = userService.getDefaultRole(userObj);
				if(isNotNull(roleObj)&& roleObj.getName()!=null){
					String role = roleObj.getName();
					// Check if the logged-in user is a csr, if yes, set the userType as 'Agent' to FFM
					/*if("Y".equalsIgnoreCase(isUserSwitchToOtherView)){ //HIX-91126
						ffmEnrollmentRequest.setUserType(EnrollmentPrivateConstants.AGENT_ROLE);
						ffmEnrollmentRequest.setFfeUserId(userObj.getFfmUserId());//HIX-88988
					}else{
						ffmEnrollmentRequest.setUserType(EnrollmentPrivateConstants.toTitleCase(role.toLowerCase()));
						ffmEnrollmentRequest.setFfeUserId(userObj.getUserName());
					}*/
					
					if(role.equalsIgnoreCase(EnrollmentPrivateConstants.CONSUMER_ROLE)){
						ffmEnrollmentRequest.setUserType(EnrollmentPrivateConstants.toTitleCase(role.toLowerCase()));
						ffmEnrollmentRequest.setFfeUserId(userObj.getUserName());
					}else{
						Securables securables = iSecurablesRepository.findExistedFfmUserInfoByTargetId(userObj.getId());
						
						ffmEnrollmentRequest.setUserType(EnrollmentPrivateConstants.AGENT_ROLE);
						if(null != securables && StringUtils.isNotBlank(securables.getUserName())){
							ffmEnrollmentRequest.setFfeUserId(securables.getUserName());
						}else{
							ffmEnrollmentRequest.setFfeUserId(userObj.getUserName());
						}
					}
				}
				// User Identity Assertion
				ffmEnrollmentRequest.setUiaFirstName(userObj.getFirstName());
				ffmEnrollmentRequest.setUiaMiddleName(null);
				ffmEnrollmentRequest.setUiaLastName(userObj.getLastName());
			}
			
			// Agent/Broker Information
			/*ffmEnrollmentRequest.setAgentOrBrokerFirstName("GetInsured");
			ffmEnrollmentRequest.setAgentOrBrokerLastName("ZZZ");*/
			ffmEnrollmentRequest.setAgentOrBrokerFirstName("Michael");
			ffmEnrollmentRequest.setAgentOrBrokerLastName("Daugherty");
			ffmEnrollmentRequest.setAgentOrBrokerMiddleName(null);
			ffmEnrollmentRequest.setAgentOrBrokerNameSuffix(null);
			ffmEnrollmentRequest.setAgentOrBrokerIndicator("Agent");
			//ffmEnrollmentRequest.setAgentOrBrokerNationalProducerNumber(EnrollmentPrivateConstants.FFM_NPN);
			ffmEnrollmentRequest.setAgentOrBrokerNationalProducerNumber(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.DEFAULT_NPN));
			
			// Member Actions 
			ffmEnrollmentRequest.setMemberEnrollmentPeriodType(enrollmentObj.getEnrollmentTypeLkp().getLookupValueCode());
			
			if(isNotNullAndEmpty(enrollmentObj.getEnrollmentReason())
					&& (enrollmentObj.getEnrollmentReason().equals('I') || enrollmentObj.getEnrollmentReason().equals('i'))){
				ffmEnrollmentRequest.setMemberReasonCode("28");
				ffmEnrollmentRequest.setMemberTypeCode("21");
			}
			
			if(isNotNullAndEmpty(enrollmentObj.getBenefitEffectiveDate())){
				ffmEnrollmentRequest.setMemberActionEffectiveDate(DateUtil.dateToString(enrollmentObj.getBenefitEffectiveDate(),GhixConstants.REQUIRED_DATE_FORMAT));
			}

			// Premium Information	
			ffmEnrollmentRequest.setPremiumTotalPremiumAmount(enrollmentObj.getGrossPremiumAmt());
			//ffmEnrollmentRequest.setPremiumAPTCElectedPercentage(enrollmentObj.getAptcPercentage());
			ffmEnrollmentRequest.setPremiumAPTCElectedPercentage(enrollmentObj.getAptcAmt());
			ffmEnrollmentRequest.setPremiumAPTCAppliedAmount(enrollmentObj.getAptcAmt());
			ffmEnrollmentRequest.setPremiumTotalIndividualResponsibilityAmount(enrollmentObj.getNetPremiumAmt());
			
			if(isNotNullAndEmpty(enrollmentObj.getCsrLevel())){
				ffmEnrollmentRequest.setPremiumCSRLevelApplicable(enrollmentObj.getCsrLevel());
			}
			//getting hios issuer id
			if(enrollmentObj.getHiosIssuerId()!=null && (!enrollmentObj.getHiosIssuerId().equalsIgnoreCase(""))){
				ffmEnrollmentRequest.setEnrollmentGroupIssuerId(enrollmentObj.getHiosIssuerId());
			}
			
			List <Enrollee> enrolleeList = enrollmentObj.getEnrollees();
			
			for(Enrollee enrolleeObj :  enrolleeList){
				
				if(isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp())){
				
					// AptcAttestation Information
					if(isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp().getLookupValueCode()) && enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT)){
						ffmEnrollmentRequest.setAptcAttestationId(enrolleeObj.getFfmPersonId());
						ffmEnrollmentRequest.setAptcAttestationFirstName(enrolleeObj.getFirstName());
						ffmEnrollmentRequest.setAptcAttestationLastName(enrolleeObj.getLastName());
						ffmEnrollmentRequest.setAptcAttestationMiddleName(enrolleeObj.getMiddleName());
						ffmEnrollmentRequest.setAptcAttestationSuffixName(enrolleeObj.getSuffix());
						ffmEnrollmentRequest.setMemberIssuerAssignedMemberId(enrolleeObj.getExchgIndivIdentifier());
						ffmEnrollmentRequest.setAptcAttestationSsn(enrolleeObj.getTaxIdNumber());
						ffmEnrollmentRequest.setAptcAttestationDob(enrolleeObj.getBirthDate());
						
						if(isNotNullAndEmpty(enrolleeObj.getGenderLkp())){
							ffmEnrollmentRequest.setAptcAttestationGender(enrolleeObj.getGenderLkp().getLookupValueCode());
						}
						
						
						if(isNotNullAndEmpty(enrollmentObj.getAptcAmt()) && enrollmentObj.getAptcAmt()>0){
							ffmEnrollmentRequest.setAptcAttestation("true");
						}else{
							ffmEnrollmentRequest.setAptcAttestation("false");
						}
					}
					
					if(isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp().getLookupValueCode()) && 
						(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER) || 
						 enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE))){
						
						FFMEnrollmentMemberData objFFMEnrollmentMemberData = new FFMEnrollmentMemberData();
						
						objFFMEnrollmentMemberData.setInsuranceAplicantId(enrolleeObj.getInsuranceAplicantId());
						objFFMEnrollmentMemberData.setFfmPersonId(enrolleeObj.getFfmPersonId());
						
						objFFMEnrollmentMemberData.setFirstName(enrolleeObj.getFirstName());
						objFFMEnrollmentMemberData.setLastName(enrolleeObj.getLastName());
						objFFMEnrollmentMemberData.setMiddleName(enrolleeObj.getMiddleName());
						objFFMEnrollmentMemberData.setSuffixName(enrolleeObj.getSuffix());
						objFFMEnrollmentMemberData.setMemberIssuerAssignedMemberId(enrolleeObj.getExchgIndivIdentifier());
						objFFMEnrollmentMemberData.setMemberFFEAssignedApplicantId(enrolleeObj.getExchgIndivIdentifier());
						objFFMEnrollmentMemberData.setSsn(enrolleeObj.getTaxIdNumber());
						objFFMEnrollmentMemberData.setDob(enrolleeObj.getBirthDate());
						if(isNotNullAndEmpty(enrolleeObj.getGenderLkp())){
							objFFMEnrollmentMemberData.setGender(enrolleeObj.getGenderLkp().getLookupValueCode());
						}
						
						// EnrollmentGroup Information
						if(isNotNullAndEmpty(enrollmentObj.getBenefitEffectiveDate())){
							ffmEnrollmentRequest.setEnrollmentGroupStartDate(DateUtil.dateToString(enrollmentObj.getBenefitEffectiveDate(),GhixConstants.REQUIRED_DATE_FORMAT));
						}
						if(isNotNullAndEmpty(enrollmentObj.getBenefitEndDate())){
							ffmEnrollmentRequest.setEnrollmentGroupEndDate(DateUtil.dateToString(enrollmentObj.getBenefitEndDate(),GhixConstants.REQUIRED_DATE_FORMAT));
						}
						if((isNotNullAndEmpty(enrollmentObj.getEnrollmentTypeLkp()) && 
							isNotNullAndEmpty(enrollmentObj.getEnrollmentTypeLkp().getLookupValueCode())) &&
							enrollmentObj.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INITIAL)){
							
							ffmEnrollmentRequest.setEnrollmentGroupTransactionType(EnrollmentPrivateConstants.FFM_ENROLLMENT_TXN_TYPE_E);
						}
						
						if(isNotNullAndEmpty(enrollmentObj.getCMSPlanID())){
							ffmEnrollmentRequest.setEnrollmentGroupAssignedPlanId(enrollmentObj.getCMSPlanID().substring(0, enrollmentObj.getCMSPlanID().length() - 2));
						}
						
						ffmEnrollmentRequest.setEnrollmentGroupFFEAssignedPolicyNumber(enrollmentObj.getGroupPolicyNumber());
						ffmEnrollmentRequest.setEnrollmentGroupIssuerPolicyNumber(enrollmentObj.getGroupPolicyNumber());
						
						// Premium Information
						if(enrollmentObj.getStateExchangeCode()!=null && enrolleeObj.getRatingArea()!=null){
							ffmEnrollmentRequest.setPremiumRatingArea(enrollmentObj.getStateExchangeCode()+ StringUtils.leftPad(enrolleeObj.getRatingArea(),3,'0'));	
						}
						
						// Member Actions :: Set all member level information
						if(isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp()) && enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER)){
							objFFMEnrollmentMemberData.setMemberSubscriberIndicator(true);
						}
						if(isNotNullAndEmpty(enrolleeObj.getTobaccoUsageLkp()) && enrolleeObj.getTobaccoUsageLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.T)){
							objFFMEnrollmentMemberData.setMemberTobaccoUseIndicator(true);
							if(isNotNullAndEmpty(eligLead) && isNotNullAndEmpty(eligLead.getLastUpdateTimestamp())){
								objFFMEnrollmentMemberData.setMemberLastDateOfTobaccoUse(DateUtil.dateToString(eligLead.getLastUpdateTimestamp(), GhixConstants.REQUIRED_DATE_FORMAT));
							}
						}
						
						
						if(isNotNullAndEmpty(enrolleeObj.getEnrollmentReason())){
							String enrollmentReason =""+ enrolleeObj.getEnrollmentReason();
							if(enrollmentReason.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INITIAL)){
								objFFMEnrollmentMemberData.setMemberEnrollmentPeriodType("Initial");
							}else if(enrollmentReason.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SPECIAL)){
								objFFMEnrollmentMemberData.setMemberEnrollmentPeriodType("Special");
							}else {
								objFFMEnrollmentMemberData.setMemberEnrollmentPeriodType("Annual");
							}
							
						}
						objFFMEnrollmentMemberData.setMemberIssuerAssignedMemberId(enrolleeObj.getIssuerIndivIdentifier());
						objFFMEnrollmentMemberData.setMemberFFEAssignedMemberId(null);
						
						/**
						 * Calculating Relationship To Subscriber using EnrolleeRelationship obj
						 */
						Integer subscriberId = enrolleeService.findSubscriberIdByEnrollmentId(enrolleeObj.getEnrollment().getId());
						EnrolleeRelationship  relationShipToSuscriber = enrolleeService.getRelationshipBySourceEndTargetId(enrolleeObj.getId(), subscriberId);

						if(relationShipToSuscriber != null && relationShipToSuscriber.getRelationshipLkp() != null){
							enrolleeObj.setRelationshipToSubscriberLkp(relationShipToSuscriber.getRelationshipLkp());
							objFFMEnrollmentMemberData.setMemberRelationshipToSubscriber(enrolleeObj.getRelationshipToSubscriberLkp().getLookupValueCode());
						}
						
//						LOGGER.info("objFFMEnrollmentMemberData : "+ objFFMEnrollmentMemberData);
						
						ffmEnrollmentRequest.getMembers().add(objFFMEnrollmentMemberData);
						
					 }
				 }	
			}
		}
		
//		LOGGER.info("ffmEnrollmentRequest : "+ ffmEnrollmentRequest);
		
		
		return ffmEnrollmentRequest;
	}
	
	private boolean isPHIXBBUser(){
		boolean isBBUser=false;
		try{
			if(benefitBayUser!=null){
				AccountUser actUser=userService.getLoggedInUser();
				if(actUser!=null && actUser.getUsername()!=null &&actUser.getUsername().equalsIgnoreCase(benefitBayUser) ){
					isBBUser=true;
				}
			}
		}catch(Exception e){
			LOGGER.error("Exception while trying to get logged in user in isBBUser()"+e.toString(),e);
		}
		return isBBUser;
	}
	
	/**
	 * Return a comma separated string of enrollee names
	 * @param enrollment
	 * @return String comma separated enrollee names
	 */
	private String getEnrolleeNames(Enrollment enrollment) {
		
		StringBuilder enrolleeNames = new StringBuilder();
		Boolean isPersonSubcriberOrEnrollee = Boolean.FALSE;
		for (Enrollee enrollee : enrollment.getEnrollees()) {
			
			LookupValue personTypeLkp = enrollee.getPersonTypeLkp();
			isPersonSubcriberOrEnrollee = (isNotNull(personTypeLkp))
					&& (personTypeLkp.getLookupValueCode().equalsIgnoreCase(
							EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER) || personTypeLkp
							.getLookupValueCode()
							.equalsIgnoreCase(
									EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
			
			if (isPersonSubcriberOrEnrollee) {
				if (isNotNullAndEmpty(enrollee.getFirstName())) {
					enrolleeNames.append(enrollee.getFirstName());
					enrolleeNames.append(" ");
				}
				if (isNotNullAndEmpty(enrollee.getLastName())) {
					enrolleeNames.append(enrollee.getLastName());
				}
				enrolleeNames.append(", ");
			}
		}
		if (enrolleeNames.length() > 2) {
			enrolleeNames.delete(enrolleeNames.length() - 2,
					enrolleeNames.length());
		}
		return enrolleeNames.toString();
	}
	
	//@ApiOperation(value="Update manual enrollment status", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value="/updatemanualenrollmentstatus",method = RequestMethod.POST)
	@ResponseBody public String updateManualEnrollmentStatus(@RequestBody String enrollmentRequestStr){
		LOGGER.info("============inside updateEnrollmentStatus============");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if(isNotNullAndEmpty(enrollmentRequestStr)) {
			EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
			if(!isNotNullAndEmpty(enrollmentRequest.getEnrollmentId())){
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
				enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
			}else if(!isNotNullAndEmpty(enrollmentRequest.getStatus())){
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ENROLLMENT_STATUS_NULL_OR_EMPTY);
     		}else{
     			Enrollment enrollmentObj = enrollmentService.updateEnrollment(enrollmentRequest.getEnrollmentId(), enrollmentRequest.getStatus(), enrollmentRequest.getEndDate(), enrollmentRequest.getDateClosed());
     			if(isNotNullAndEmpty(enrollmentObj)){
     				enrollmentObj.setEnrollmentStatusLabel(enrollmentRequest.getStatus());
     				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
     				enrollmentResponse.setEnrollment(enrollmentObj);
     				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);
     				enrollmentResponse.setEnrollment(enrollmentObj);
				   }else{
     				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
     				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_NO_RESULTS_FOUND);
				   }
         		}
		}else{
			enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_203);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setStatus(EnrollmentPrivateConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	//@ApiOperation(value="Update enrollment personal info", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value="/updateEnrollmentPersonalInfo",method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollmentPersonalInfo(
			@RequestBody EnrollmentPersonalInfo enrollmentPersonalInfo) {
		LOGGER.info("============inside updateEnrollmentStatus============");
		XStream xstream = GhixUtils.getXStreamStaxObject();
	//	EnrollmentPersonalInfo enrollmentResponse = new EnrollmentPersonalInfo();
		if (null != enrollmentPersonalInfo) {
			Enrollee enrollee  = enrolleeService.findById(Integer.parseInt(enrollmentPersonalInfo.getEnrolleeId()));
			enrollee.setPreferredEmail(enrollmentPersonalInfo.getTxtEmailId());
			enrollee.setPrimaryPhoneNo(enrollmentPersonalInfo.getTxtPhone());
			/*enrollee.setp
			Enrollment enrollmentObj = enrollmentService.updateEnrollment(
					enrollmentRequest.getEnrollmentId(),
					enrollmentRequest.getStatus());*/
		} 
		
		return xstream.toXML(enrollmentPersonalInfo);
	}
	
	//@ApiOperation(value="Update enrollment major info", notes="", httpMethod="POST", produces="text/plain")
	@RequestMapping(value = "/updateEnrollmentMajorInfo", method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollmentMajorInfo(@RequestBody EnrollmentMajorInfo enrollmentRequest) {
		Enrollment enrollment = null;
		String response = GhixConstants.RESPONSE_FAILURE;;
		if(null != enrollmentRequest && !StringUtils.isEmpty(enrollmentRequest.getEnrollmentId())){
			enrollment = enrollmentService.findById(Integer.parseInt(enrollmentRequest.getEnrollmentId()));	
		}
		
		if (enrollment != null) {
			
			if (enrollmentRequest.getTxtInsuranceCompany()!= null) {
				enrollment.setInsurerName(enrollmentRequest.getTxtInsuranceCompany());
			}
			
			if (!StringUtils.isEmpty(enrollmentRequest.getIssuerId())) {
				enrollment.setIssuerId(Integer.parseInt(enrollmentRequest.getIssuerId())); 
			}
			
			if (!StringUtils.isEmpty(enrollmentRequest.getTxtPlanName())) {
				enrollment.setPlanName(enrollmentRequest.getTxtPlanName());
			}
			
			enrollment.setPlanLevel(enrollmentRequest.getTxtMetalTier());
			
			if (!StringUtils.isEmpty(enrollmentRequest.getTxtInsuranceType())){
				LookupValue insuranceTypeLKP = lookupService
						.getlookupValueByTypeANDLookupValueLabel(
								EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE,
								enrollmentRequest.getTxtInsuranceType().toLowerCase());
				
				if (insuranceTypeLKP != null)
				{
					enrollment.setInsuranceTypeLkp(insuranceTypeLKP);
				}
			}

			if (!StringUtils.isEmpty(enrollmentRequest.getTxtAPTC())) {
				enrollment.setAptcAmt(Float.valueOf(enrollmentRequest.getTxtAPTC()));
			}

			if (!StringUtils.isEmpty(enrollmentRequest.getTxtGrossAmount())) {
				enrollment.setGrossPremiumAmt(Float.valueOf(enrollmentRequest.getTxtGrossAmount()));
			}
			
			if (!StringUtils.isEmpty(enrollmentRequest.getTxtExchangeType())) {
				enrollment.setExchangeTypeLkp(lookupService
						.getlookupValueByTypeANDLookupValueCode(
								EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE,
								enrollmentRequest.getTxtExchangeType().toUpperCase()));

			}
			
			if (enrollmentRequest.getTxtApplicationId()!= null) {
				enrollment.setCarrierAppId(this.trimLeadingZerosForNumber(enrollmentRequest.getTxtApplicationId()));
			}
			
			if (!StringUtils.isEmpty(enrollmentRequest.getTxtConfirmationId())) {
				enrollment.setConfirmationNumber(enrollmentRequest.getTxtConfirmationId());
			}

			//if (!StringUtils.isBlank(enrollmentRequest.getTxtPolicyId())) {
				enrollment.setIssuerAssignPolicyNo(this.trimLeadingZerosForNumber(enrollmentRequest.getTxtPolicyId()));
			//}
			
			if (!StringUtils.isEmpty(enrollmentRequest.getTxtEffectiveDate())) {
				Date effectiveDate = DateUtil.StringToDate(enrollmentRequest.getTxtEffectiveDate(), "MM/dd/yyyy");
				enrollment.setBenefitEffectiveDate(effectiveDate);
			}
			
			if (!StringUtils.isEmpty(enrollmentRequest.getTxtBenifitEndDate())) {
				Date benifitEndDate = DateUtil.StringToDate(enrollmentRequest.getTxtBenifitEndDate(), "MM/dd/yyyy");
				enrollment.setBenefitEndDate(benifitEndDate);
			}else{
				enrollment.setBenefitEndDate(null);
			}
			
			// filter by assisterBrokerId by
			if(!StringUtils.isEmpty(enrollmentRequest.getTxtServicedBy())) {
				enrollment.setCapAgentId(Integer.parseInt(enrollmentRequest.getTxtServicedBy()));
			}
			
			try {
				enrollment.setUpdatedBy(userService.getLoggedInUser());
			} catch (InvalidUserException e) {
				LOGGER.error("Error getting logged in user" , e);
			}

			try {
				enrollmentService.saveEnrollment(enrollment);
				response = GhixConstants.RESPONSE_SUCCESS;
			} catch (Exception ge) {
				LOGGER.error("Error Creating Manual Enrollment" , ge);
				response = GhixConstants.RESPONSE_FAILURE;
			}

		}

		return response;
	}
	
	//@ApiOperation(value="Please update text for updateEnrollmentVerificationEvent", produces="text/plain")
	@ApiIgnore
	@RequestMapping(value = "/updateEnrollmentVerificationEvent", method = {RequestMethod.POST, RequestMethod.GET})
	@ResponseBody
	public String updateEnrollmentVerificationEvent(@RequestBody Map<String,String> verificationMap){
			//@RequestParam("enrollmentId") String enrollmentId, @RequestParam("verificationEvent") String verificationEvent) {
		try{
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode("ENROLLMENT_STATUS", "VERIFIED");
			Set<String> enrollmentIds = verificationMap.keySet();
			for (String enrollmentId : enrollmentIds) {
				Integer id = Integer.valueOf(enrollmentId);
				Enrollment enrollment = enrollmentService.findById(id);
				String verificationEvent = verificationMap.get(enrollmentId);
				 if("Found on carrier portal".equalsIgnoreCase(verificationEvent))
			        {
			        	if("PENDING".equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode()) || "APPROVED".equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())) {
							enrollment.setEnrollmentStatusLkp(lookupValue);
						}
			        }
			        enrollment.setVerificationEvent(verificationEvent);
			        AccountUser user = userService.getLoggedInUser();
			        if(user != null && user.getId() != 0){
			        	enrollment.setUpdatedBy(user);
			        }
			        enrollmentService.saveEnrollment(enrollment);
			}
			
	        
	       
			//enrollmentService.updateEnrollmentVerification(id,verificationEvent);
		}catch(Exception e)
		{
			LOGGER.error("error saving verificationEvent:"+e.getMessage());
		}
		return "SUCCESS";
	}
	//@ApiOperation(value="Find enrollment commission", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value = "/findenrollmentcommission", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_COMMISSIONS')")
	@ResponseBody
	public String findenrollmentcommission(
			@RequestBody Integer enrollmentId) {
		LOGGER.info("=================== inside searchEnrollmentExt ===================");

		LOGGER.info("findenrollmentcommission :  " + enrollmentId);
		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<EnrollmentCommission> enrollmentCommissionList = new ArrayList<EnrollmentCommission>();
		EnrollmentCommissionResponse enrollmentCommissionResponse = new EnrollmentCommissionResponse();
		if (enrollmentId == null ) {
			enrollmentCommissionResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentCommissionResponse.setErrMsg("Error in fetchin commission history");
		} else {
			 enrollmentCommissionList = enrollmentCommissionService.findEnrollmentCommissionByEnrollmentId(enrollmentId);
			 enrollmentCommissionResponse.setEnrollmentCommissions(enrollmentCommissionList);
			 enrollmentCommissionResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
		
		return xstream.toXML(enrollmentCommissionResponse);
	}

	//@ApiOperation(value="Save enrollment commission", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value = "/saveenrollmentcommission", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_COMMISSIONS')")
	@ResponseBody
	public String saveEnrollmentCommission(
			@RequestBody EnrollmentCommissionDTO enrollmentCommissionDTO) {
		LOGGER.info("=================== inside searchEnrollmentExt ===================");

		LOGGER.info("saveEnrollmentCommission :  " );
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentCommission enrollmentCommission = null;
		EnrollmentCommissionResponse enrollmentCommissionResponse = new EnrollmentCommissionResponse();
		if (enrollmentCommissionDTO == null ) {
			enrollmentCommissionResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentCommissionResponse.setErrMsg("Error in saving commission history");
		} else {
			 enrollmentCommission = enrollmentCommissionService.saveEnrollmentCommission(enrollmentCommissionDTO);
			 if(null!=enrollmentCommission){
				 enrollmentCommissionResponse.setEnrollmentCommission(enrollmentCommission);
				 enrollmentCommissionResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			 }
		}
		
		return xstream.toXML(enrollmentCommissionResponse);
	}
	
	//@ApiOperation(value="Update enrollment commission", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value = "/updateenrollmentcommission", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_COMMISSIONS')")
	@ResponseBody
	public String updateEnrollmentCommission(
			@RequestBody EnrollmentCommissionDTO enrollmentCommissionDTO) {
		LOGGER.info("=================== inside searchEnrollmentExt ===================");

		LOGGER.info("updateEnrollmentCommission :  " );
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentCommission enrollmentCommission = null;
		EnrollmentCommissionResponse enrollmentCommissionResponse = new EnrollmentCommissionResponse();
		if (enrollmentCommissionDTO == null ) {
			enrollmentCommissionResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentCommissionResponse.setErrMsg("Error in updating commission history");
		} else {
			 enrollmentCommission = enrollmentCommissionService.updateEnrollmentCommission(enrollmentCommissionDTO);
			 if(null!=enrollmentCommission){
				 enrollmentCommissionResponse.setEnrollmentCommission(enrollmentCommission);
				 enrollmentCommissionResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			 }
		}
		
		return xstream.toXML(enrollmentCommissionResponse);
	}
	
	//@ApiOperation(value="Get enrollment list by housholdId", notes="", httpMethod="GET", produces="application/json")
	@RequestMapping(value = "/getEnrollmentList/{houseHoldId}", method = {RequestMethod.GET})
	@ResponseBody 
	public String getApplicationListInfo(@PathVariable("houseHoldId") String houseHoldId) {
		
		LOGGER.info("[getEnrollmentList] Get the application List Info details for Household id : " + houseHoldId);
		String appListInfoJSON="{\"error\" : \"Unable to get the EnrollmentList\"}";
		try{
		List<PolicyLiteDTO> policyLiteDTO = null;

		if (!StringUtils.isEmpty(houseHoldId)) {
			// get Consumer application dto
			policyLiteDTO = enrollmentService.getEnrollmentListByHouseholdId(Integer.parseInt(houseHoldId));
		}

		appListInfoJSON = platformGson.toJson(policyLiteDTO);
		}
		catch(Exception ex){
			LOGGER.error("Error in getting enrollment list for calllanding page: " + ex.getMessage());
		}
		LOGGER.debug("Application List Info JSON: " + appListInfoJSON);
		
		return appListInfoJSON;
	}
	
	//@ApiOperation(value="Save enrollees", notes="", httpMethod="POST", produces="application/xml")
	@ApiIgnore
	@RequestMapping(value="/saveEnrollees",method = RequestMethod.POST)
	@ResponseBody public String saveEnrollees(@RequestBody Map<String,CapApplicationDTO> enrollmentEnrollees){
		LOGGER.info("============ saveEnrollees ============");	
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		boolean bStatus=false;
		boolean isSubmitApplication = Boolean.TRUE;
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);	
		enrollmentResponse.setErrMsg("Unable to save ffm application data");
		
		try {	
			Set<String> enrollmentIds = enrollmentEnrollees.keySet();
			for (String enrollmentid : enrollmentIds) {
				if(StringUtils.isNotEmpty(enrollmentid)){
					Enrollment enrollment = enrollmentRepository.getEnrollmentByEnrollmentId(Integer.valueOf(enrollmentid));
					String enrollmentStatusBefore = enrollment.getEnrollmentStatusLkp().getLookupValueCode();
					if(null != enrollment)
					{
						CapApplicationDTO capApplicationDTO= enrollmentEnrollees.get(enrollmentid);
						if(null!=capApplicationDTO){
							//update enrollment object with effectiveApplicationDto properties
							if(null==enrollment.getInsuranceTypeLkp() && null!=capApplicationDTO.getSsapApplicationDTO() && null!=capApplicationDTO.getSsapApplicationDTO().getApplicationType()){
								enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE,capApplicationDTO.getSsapApplicationDTO().getApplicationType().toUpperCase()));
							}
							// HIX-57645 : set the enrollment modality 
							// Use case  if application is Agent Submit FFM ONEXCHANGE the modality should be FFM_AGENT_SUBMIT
							if(capApplicationDTO.isFfmAgentSubmit()) {
								enrollment.setEnrollmentModalityLkp(
										lookupService.getlookupValueByTypeANDLookupValueCode(
												EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE,
												EnrollmentPrivateConstants.FFM_AGENT_SUBMIT
												));
							}
							EffectiveApplicationDTO effectiveApplicationDTO = capApplicationDTO.getEffectiveApplicationDTO();
							if(null!=effectiveApplicationDTO){
								enrollment  = enrollmentService.updateEnrollmentDetails(effectiveApplicationDTO,enrollment,isSubmitApplication);
							}
							bStatus =	enrollmentService.saveManualAppEnrolleeInfo(capApplicationDTO.getApplicantDTOList(),enrollment, capApplicationDTO.isPendingStatusOnly());

						}

						boolean isOffExchangeEnrollment = isNotNullAndEmpty(enrollment.getExchangeTypeLkp()) && EnrollmentPrivateConstants.EXCHANGE_TYPE_OFFEXCHANGE.equalsIgnoreCase(enrollment.getExchangeTypeLkp().getLookupValueCode());
						if (isOffExchangeEnrollment && isNotNullAndEmpty(enrollment.getInsuranceTypeLkp())) {
							
							if(isNotNullAndEmpty(enrollmentStatusBefore)
									&& enrollmentStatusBefore.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_ECOMMITTED)
									&& isNotNullAndEmpty(enrollment.getEnrollmentStatusLkp())
									&& EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())){
								/**
								 * Send Email only when enrollment status is changed to PENDING
								 */
								sendEmailToConsumer(enrollment.getCmrHouseHoldId(), enrollment, true);
								updateCartStatus(enrollment);
							}
							
						}
					}	
				}
			}	

			if(bStatus) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrMsg(StringUtils.EMPTY);
			}
		} catch (Exception e) {
			giExceptionHandler.recordFatalException(Component.ENROLLMENT, null, "Unable to save the enrollment record with ffm data" , e) ;
			LOGGER.error("Unable to save the enrollment record with ffm data" , e);	
			enrollmentResponse.setErrMsg(e.getStackTrace().toString());
		}

		return xstream.toXML(enrollmentResponse);
	}
	
	//@ApiOperation(value="Is manual enrollment exists", notes="", httpMethod="POST", produces="application/xml")
	@ApiIgnore
	@RequestMapping(value="/manualEnrollmentExists",method = RequestMethod.POST)
	@ResponseBody public String manualEnrollmentExists(@RequestBody Map<String, String> mapRequest){
		LOGGER.info("============ manualEnrollmentCount ============");	
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);	
		enrollmentResponse.setErrMsg("Unable to get manual enrollment count");
		
		try {	
			String cmrId = mapRequest.get("householdId");
			String appType =  mapRequest.get("applicationType");
					
			int count = enrollmentService.findCountByCmrIdAndPlanTypeForManual(cmrId, appType);
			enrollmentResponse.setIsEnrollmentExists((count > 0));
			
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrMsg(StringUtils.EMPTY);
		} catch (Exception e) {
			LOGGER.error("Unable to get the enrollment record with data" , e);	
			enrollmentResponse.setErrMsg(e.getStackTrace().toString());
		}
			
		return xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * @author hardas_d
	 * @param ssapApplicationId
	 * @return
	 * This method will return the serialized CapApplicationDTO based on the SSAP application id.
	 * The DTO will be used to view/edit an SSAP_APPLICATON and ENROLLMENT
	 */
	@RequestMapping(value="/findBySsapApplicationId/{ssapApplicationId}",method = RequestMethod.GET, produces="application/json")
	@ResponseBody public CapApplicationDTO findBySsapApplicationId(@PathVariable String ssapApplicationId){
		LOGGER.info("============ findBySsapApplicationId ============");	
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		CapApplicationDTO capAppDto=null;
		
		try {	
			capAppDto = enrollmentService.findBySsapApplicationId(Long.parseLong(ssapApplicationId));
			
			enrollmentResponse.setCapApplicatonDto(capAppDto);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrMsg(StringUtils.EMPTY);
		} catch (Exception e) {
			LOGGER.debug("Error fetching CapApplicationDTO @ findBySsapApplicationId" , e);
			enrollmentResponse.setCapApplicatonDto(null);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(e.getStackTrace().toString());
		}
			
		return capAppDto;
	}


	//@ApiOperation(value="Save FFM AppData", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value="/saveFfmAppData",method = RequestMethod.POST)
	@ResponseBody public String saveFfmAppData(@RequestBody CapOnExchangeFFMAppDto capFfmAppDto){
		LOGGER.info("============ saveFfmAppData ============");	
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);	
		enrollmentResponse.setErrMsg("Unable to save ffm application data");
		
		try {	
			boolean bStatus = enrollmentService.saveFfmAppData(capFfmAppDto);
			
			if(bStatus) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrMsg(StringUtils.EMPTY);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to save the enrollment record with ffm data" , e);	
			enrollmentResponse.setErrMsg(e.getStackTrace().toString());
		}
			
		return xstream.toXML(enrollmentResponse);
	}
	
	//@ApiOperation(value="Save Enrollment details", notes="", httpMethod="POST", produces="application/xml")
	@RequestMapping(value="/saveEnrollmentDetails",method = RequestMethod.POST)
	@ResponseBody public String saveEnrollmentDetails(@RequestBody EffectiveApplicationDTO effectiveApplicationDTO){
		LOGGER.info("============ saveFfmAppData ============");	
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		Enrollment enrollment  =null;
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);	
		enrollmentResponse.setErrMsg("Unable to save ffm application data");
		boolean isSubmitApplication = Boolean.FALSE;
		
		try {	
			if(null!=effectiveApplicationDTO && StringUtils.isNotEmpty(effectiveApplicationDTO.getEnrollmentId())){
				enrollment  = enrollmentService.findById(Integer.valueOf(effectiveApplicationDTO.getEnrollmentId()));
				enrollment = enrollmentService.updateEnrollmentDetails(effectiveApplicationDTO, enrollment, isSubmitApplication);
			}
			if(null!=enrollment) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrMsg(StringUtils.EMPTY);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to save the enrollment record with ffm data" , e);	
			enrollmentResponse.setErrMsg(e.getStackTrace().toString());
		}
			
		return xstream.toXML(enrollmentResponse);
	}
	
	private void updateConsumerApplicationById(Long ssapApplicationId, String enrollmentStatus) {
		
		SsapApplicationDTO ssapApplicationDTO = new SsapApplicationDTO();
		ssapApplicationDTO.setId(ssapApplicationId);
		ssapApplicationDTO.setEnrollmentStatus(enrollmentStatus);
		
		try{
			restTemplate.postForObject(ConsumerServiceEndPoints.UPDATE_CONSUMER_APPLICATION_BY_ID,ssapApplicationDTO,String.class);
		}
		catch(Exception e){
			LOGGER.error("EnrollmentPrivateController.updateConsumerApplicationById - Error in updating enrollment status for SSAP Application Id="+ssapApplicationDTO.getId() , e);
		}
	}
	
//	private void updatePlanOrderStatus(int indOrderItemId) {
//		String orderId=null;
//		try {
//			/**
//			 * Make a DB call to get the orderId from DB by passing enrollment obj indv_order_id
//			 */
//			if(isNotNullAndEmpty(enrollmentOffExchangeService.getOrderIdFromIndvOrderItemId(indOrderItemId))){
//				orderId=enrollmentOffExchangeService.getOrderIdFromIndvOrderItemId(indOrderItemId).toString();
//				PlanDisplayResponse pldResponse = restTemplate.postForObject(GhixEndPoints.PlandisplayEndpoints.UPDATE_ORDER_STATUS,orderId, PlanDisplayResponse.class);
//				LOGGER.debug("Plandisplay OrderConfirmApi Response: "+ pldResponse.getStatus());
//
//				if (pldResponse.getStatus() != null&& pldResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
//					LOGGER.debug("Plandisplay OrderConfirmApi SUCCESS for Order id: "+ orderId);
//				} else {
//					LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_PLAN_DISPLAY_ORDER_CONFIRM_API_FAIL+ orderId);
//				}
//			}
//			else{
//				LOGGER.error("Order ID received is null or empty :"+ orderId);
//			}
//		} catch (Exception e) {
//			// This catch is added so as to continue even if "PlanDisplay Update_Orde_Status API" fails. (As per plan display team. Mail dated 30/07/2013.)
//			LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_PLAN_DISPLAY_ORDER_CONFIRM_API_FAIL + orderId);
//			LOGGER.error("Exception ::" + e.getMessage() , e);
//		}
//	}
	
	private void sendEmailToConsumer(Integer cmrHouseHoldId, Enrollment enrollmentObj, boolean isIndividualMember) {
		try{			
			LOGGER.info("Getting Household from consumer" + cmrHouseHoldId);
			Household household = enrollmentService.getHousehold(cmrHouseHoldId);
			
			if (household != null) {
				LOGGER.info("Sending email to consumer ::" + household.getEmail());
				enrlOffExchangeEmailNotificationService.sendEmailNotification(household, enrollmentObj, isIndividualMember);
				String exitSurveyEmail = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXIT_SURVEY_EMAIL);
				if(exitSurveyEmail == null || "ON".equalsIgnoreCase(exitSurveyEmail)) {
					exitSurveyEmailNotificationService.sendEmailNotification(household);
				}
			}
			else
			{
				LOGGER.error("No success response code data for cmrHouseholdId, No email sent : "+cmrHouseHoldId);
			}
		}catch(Exception e){
			LOGGER.error("Exception while sending email notification: " ,e);
		}

	}
	
	//HIX-57851
	//@ApiOperation(value="Update enrollment Carrier App ID", notes="", httpMethod="POST", produces="text/plain")
	@RequestMapping(value = "/updateEnrollmentCarrierAppId/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollmentCarrierAppId(@PathVariable("id") String id, @RequestBody String carrierAppId) {
		LOGGER.info("============inside updateEnrollmentCarrierAppId============");
		
		Enrollment enrollment = null;
		String response = GhixConstants.RESPONSE_FAILURE;
		if(null == carrierAppId || carrierAppId.isEmpty()){
			return response;
		}
		try {
			enrollment = enrollmentService.findById(Integer.parseInt(id));	
			if (enrollment != null) {
				enrollment.setCarrierAppId(carrierAppId);
				enrollment.setUpdatedBy(userService.getLoggedInUser());
				enrollmentService.saveEnrollment(enrollment);
				response = GhixConstants.RESPONSE_SUCCESS;
			}else{
				LOGGER.error("Error updateEnrollmentCarrierAppId : Enrollment id: " + id + " is not found.");
			}
		} catch (Exception ge) {
			LOGGER.error("Error updateEnrollmentCarrierAppId Enrollment ID: " + id + " CarrierAppId:" + carrierAppId, ge);
			response = GhixConstants.RESPONSE_FAILURE;
		}
		return response;
	}
	
	//@ApiOperation(value="Fetch Enrollment CAP Details", notes="", httpMethod="POST", produces="text/json")
	@RequestMapping(value = "/getEnrollmentCapInfo/{id}", method = RequestMethod.POST)
	@ResponseBody
	private EnrollmentResponse getEnrollmentCapInfo(@PathVariable("id") String enrollmentId){
		LOGGER.info("============ inside getEnrollmentCapInfo ============");
		LOGGER.info("Enrollment Id " + enrollmentId);
		return enrollmentService.getEnrollmentCapInfo(enrollmentId);
	}
	
	/**
	 * @author panda_p
	 * @since 20-Aug-2015
	 * @return
	 */
	private void updateCartStatus(Enrollment enrollment){
		try {
			List<Long> pdOrderitemIds = null;
			if(enrollment !=null){
				pdOrderitemIds = new ArrayList<Long>();
				if(isNotNullAndEmpty(enrollment.getPdOrderItemId())){
					pdOrderitemIds.add(enrollment.getPdOrderItemId());						
				}
				if(!pdOrderitemIds.isEmpty()){
					restTemplate.postForObject(GhixEndPoints.PlandisplayEndpoints.UPDATE_ORDER_ITEM_AS_ENROLLED, pdOrderitemIds, PlanDisplayResponse.class);
					LOGGER.info("Plandisplay UPDATE_ORDER_ITEM_AS_ENROLLED API Call Success");
				}
			}
		} catch (Exception e) {
			LOGGER.error("Plandisplay UPDATE_ORDER_ITEM_AS_ENROLLED API Call Failed ");
			LOGGER.error("Exception = " + e.getMessage() , e);
		}
	}
	
	/**
     * Method to call Plan Management API to get the plan response
     * @param planId
	 * @param effectiveDate 
     * @return PlanResponse
     */
	private PlanResponse getPlanResponse(String planId, Date effectiveDate) {

		PlanRequest planRequest= new PlanRequest();
		planRequest.setPlanId(planId);
		planRequest.setMarketType("INDIVIDUAL");
		if(null != effectiveDate) {
			planRequest.setCommEffectiveDate(DateUtil.dateToString(effectiveDate, GhixConstants.REQUIRED_DATE_FORMAT));
		}

		String planInfoResponse=null;
		try{
			planInfoResponse = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL,planRequest,String.class);
			LOGGER.info("getting successfull response of the plan data from plan management");
		}
		catch(Exception ex){
			LOGGER.error("exception in getting plan data from the plan management" , ex);
		}
		PlanResponse planResponse = null;
		try {
			planResponse = platformGson.fromJson(planInfoResponse,PlanResponse.class);
		} catch (Exception ex) {
			LOGGER.error("Error in unmarshalling planResponse" , ex);
		}
		return planResponse;
	}
	
	public String trimLeadingZerosForNumber(String value){
    	value = StringUtils.isEmpty(value) ? null : value.trim();
    	if( value != null && value.matches("\\d+") && (!value.matches("^[0]+$")) ){
    		value = value.replaceFirst("^0+(?!$)", "");
		}
    	return value;
    }
	
	private void setEnrollmentCommissionData(Enrollment enrollment, PlanResponse planResponse) {
		if(null != planResponse) {
			EnrollmentIssuerCommision enrlCommission = new EnrollmentIssuerCommision();
			enrlCommission.setInitialCommDollarAmt(planResponse.getFirstYearCommDollarAmt());
			enrlCommission.setInitialCommissionPercent(planResponse.getFirstYearCommPercentageAmt());
			enrlCommission.setRecurringCommDollarAmt(planResponse.getSecondYearCommDollarAmt());
			enrlCommission.setRecurringCommissionPercent(planResponse.getSecondYearCommPercentageAmt());
			enrlCommission.setNonCommissionFlag(planResponse.getNonCommissionFlag());
			if(!EnrollmentConstants.Y.equalsIgnoreCase(enrlCommission.getNonCommissionFlag())) {
				if(null != enrlCommission.getInitialCommDollarAmt()) {
					enrlCommission.setInitialCommissionAmt(enrlCommission.getInitialCommDollarAmt());
				}else if(null != enrlCommission.getInitialCommissionPercent() && null != enrollment.getGrossPremiumAmt()) {
					enrlCommission.setInitialCommissionAmt(EnrollmentUtils.round(enrlCommission.getInitialCommissionPercent() * enrollment.getGrossPremiumAmt(), 2));
				}
				if(null != enrlCommission.getRecurringCommDollarAmt()) {
					enrlCommission.setRecurringCommissionAmt(enrlCommission.getRecurringCommDollarAmt());
				}else if(null != enrlCommission.getRecurringCommissionPercent() && null != enrollment.getGrossPremiumAmt()) {
					enrlCommission.setRecurringCommissionAmt(EnrollmentUtils.round(enrlCommission.getRecurringCommissionPercent() * enrollment.getGrossPremiumAmt(), 2));
				}
			}else {
				enrlCommission.setInitialCommissionAmt(0d);
				enrlCommission.setRecurringCommissionAmt(0d);
			}
			enrlCommission.setEnrollment(enrollment);
			enrollment.setEnrollmentIssuerCommision(enrlCommission);
		}
	}
}
