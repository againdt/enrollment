/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.enrollment.service.EnrollmentPremiumService;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;

/**
 * 
 * Premium controller providing APIs for the Enrollment Premium table
 * @author negi_s
 * @since 06/20/2016
 */
@Controller
@RequestMapping(value = "/enrollment/premium")
public class EnrollmentPremiumController {
	
	private static final Logger LOGGER = Logger.getLogger(EnrollmentPremiumController.class);
	
	@Autowired EnrollmentPremiumService enrollmentPremiumService;
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to EnrollmentPremiumController");
		return "Welcome to EnrollmentPremiumController";
	}
	
	@RequestMapping(value = "/findpremiumbyenrollmentid/{enrollmentId}", method = RequestMethod.GET)
	@ResponseBody
	public EnrollmentResponse findPremiumByEnrollmentId(@PathVariable("enrollmentId") Integer enrollmentId) {
		LOGGER.info("Inside findPremiumByEnrollmentId  API");
		EnrollmentResponse enrollmentResponse = enrollmentPremiumService.findPremiumDetailsByEnrollmentId(enrollmentId);
		LOGGER.info("findPremiumByEnrollmentId API call successful");
		return enrollmentResponse;
	}
	

}
