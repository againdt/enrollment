package com.getinsured.hix.enrollment.controller;


/**
* This is an example of how you might extend the ApiDocumentationController in order to set your own RequestMapping
* (instead of the default "/api") among other possibilities.  Going this route, you do not necessarily have to define
* the controller in your servlet context.
*/
//@Controller
//@RequestMapping(value = "/documentation-ghix-enrollment")
//public class EnrollmentDocumentationController extends ApiDocumentationController {
//
//    /*public EnrollmentDocumentationController() {
//        setBaseControllerPackage("com.getinsured.hix.enrollment");
//        
//        setApiVersion("v1");
//    }
//
//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public String documentation() {
//        return "documentation";
//    }*/
//}
