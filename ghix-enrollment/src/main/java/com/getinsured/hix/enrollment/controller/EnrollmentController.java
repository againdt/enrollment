
/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.dto.enrollment.EnrolleeCurrentMonthDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeDataUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrolleePaymentDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.Enrollment834ResendDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentAutoRenewalDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentByMemberListRequestDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCurrentMonthDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCustomGroupDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentEventHistoryDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMailingAddressUpdateRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMembersDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPlanResponseDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.MarketType;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentStatusUpdateDto;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberEventDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.hix.dto.enrollment.MonthlyAPTCAmountDTO;
import com.getinsured.hix.dto.enrollment.ShopEnrollmentLogDto;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.service.AdminUpdateService;
import com.getinsured.hix.enrollment.service.EnrlAgentBobTransferService;
import com.getinsured.hix.enrollment.service.EnrolleeService;
import com.getinsured.hix.enrollment.service.EnrollmentCreationService;
import com.getinsured.hix.enrollment.service.EnrollmentCustomGroupingService;
import com.getinsured.hix.enrollment.service.EnrollmentEditToolService;
import com.getinsured.hix.enrollment.service.EnrollmentEmailNotificationService;
import com.getinsured.hix.enrollment.service.EnrollmentEmployerDisEnrollService;
import com.getinsured.hix.enrollment.service.EnrollmentEventService;
import com.getinsured.hix.enrollment.service.EnrollmentPDHandshakeService;
import com.getinsured.hix.enrollment.service.EnrollmentReinstmtService;
import com.getinsured.hix.enrollment.service.EnrollmentService;
import com.getinsured.hix.enrollment.service.ExternalEnrollmentService;
import com.getinsured.hix.enrollment.service.GroupInstallationService;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentGIPayloadUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.TaxYear;
import com.getinsured.hix.model.enrollment.EnrlAgentBOBTransfer;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeDTO;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentDTO;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentMemberDTO;
import com.getinsured.hix.model.enrollment.EnrollmentRemittanceDTO;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.model.plandisplay.PdOrderResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayResponse;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.TaxFilingService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentRequest;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentResponse;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementRequest;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementResponse;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.timeshift.TimeShifterUtil;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.ISO8601DateConverter;

import springfox.documentation.annotations.ApiIgnore;

/**
 * @author panda_pratap
 * @since 13/02/2013
 */

@Controller
@RequestMapping(value = "/enrollment")
public class EnrollmentController {

	private static final Logger LOGGER = Logger.getLogger(EnrollmentController.class);

	@Autowired private GhixRestTemplate restTemplate;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrolleeService enrolleeService;
	@Autowired private TaxFilingService taxFilingService;
	@Autowired private GroupInstallationService groupInstallationService;
	@Autowired private UserService userService;
	@Autowired private AdminUpdateService adminUpdateService;
	@Autowired private EnrollmentReinstmtService enrollmentReinstmtService;
	@Autowired private LookupService lookupService;
	@Autowired private EnrollmentEmployerDisEnrollService enrollmentEmployerDisEnrollService;
	@Autowired private EnrollmentEmailNotificationService enrollmentEmailNotificationService;
	@Autowired private EnrollmentEventService enrollmentEventService;
	@Autowired private EnrollmentPDHandshakeService enrollmentPDHandshakeService;
	@Autowired private IEnrollmentRepository iEnrollmentRepository;
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired private GIWSPayloadService giwsPayloadService;
	@Autowired private Gson platformGson;
	@Autowired private EnrollmentCreationService enrollmentCreationService;
	@Autowired private EnrollmentEditToolService enrollmentEditToolService;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	@Autowired private EnrlAgentBobTransferService enrlAgentBobTransferService;
	@Autowired private EnrollmentCustomGroupingService enrollmentCustomGroupingService;
	@Autowired private ExternalEnrollmentService extEnrollmentService;
	
	private static ObjectMapper mapper = new ObjectMapper();
	
	public static ISO8601DateConverter DATE_CONVERTER = new ISO8601DateConverter() {
        @Override
            public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
        return Date.class.isAssignableFrom(type);
        }
    }; 
	
	
	/**
	 * @author panda_p This method is used to test the working of enrollment
	 *         module after deployment
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to Enrollment module (welcome()) ::");
		String userName = "";
		String userId = "";
		try {
			userName = userService.getLoggedInUser().getUserName();
			userId = userService.getLoggedInUser().getId() + "";
			LOGGER.info("User ID = " + userId);
			
		} catch (InvalidUserException e) {
			LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage() , e );
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause() , e);
		}
		
		return "Welcome to Enrollment module (welcome()) :: UserName :" + userName + ", UserId :" + userId;
	}
	
	/**
	 * Performance test :: Evaluates the time taken to fetch an enrollment record 0.8 million times
	 * 
	 */
	@RequestMapping(value = "/perfTesting", method = RequestMethod.GET)
	@ResponseBody
	public String performanceTest() {
		LOGGER.info("Welcome to Enrollment module performance test");
		long i=0;
		long t1 = TimeShifterUtil.currentTimeMillis()/(EnrollmentConstants.ONE_THOUSAND);
		for(i =0 ; i<800000l;i++){
			iEnrollmentRepository.findOne(46);
		}
		long t2 = TimeShifterUtil.currentTimeMillis()/(EnrollmentConstants.ONE_THOUSAND);
		return "Records fetched : "+i+"; Time Taken : "+(t2-t1)+" seconds";
	}

	/**
	 * @author panda_p This method is used to test the working of enrollment
	 *         module after deployment
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/testPost", method = RequestMethod.POST)
	@ResponseBody
	public String testPost(@RequestBody String year) {
		LOGGER.info("Welcome to Enrollment module (testPost()) ::");
		String userName = "";
		String userId = "";
		try {
			userName = userService.getLoggedInUser().getUserName();
			userId = userService.getLoggedInUser().getId() + "";
			LOGGER.info("User ID = " + userId);
		} catch (InvalidUserException e) {
			LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage() , e);
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause() , e);
		}
		return "Welcome to Enrollment module (testPost()) :: UserName :"
				+ userName + ", UserId :" + userId + ", Input year ::" + year;
	}

	@ApiIgnore
	@RequestMapping(value = "/searchenrollment", method = RequestMethod.POST)
	@ResponseBody
	public String searchEnrollment(@RequestBody Map<String, Object> searchCriteria) {
		LOGGER.info("=================== inside searchEnrollmentExt ===================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (searchCriteria == null || searchCriteria.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
		} else {
			Map<String, Object> enrollmentsAndRecordCount = enrollmentService.searchEnrollment(searchCriteria);
			if (enrollmentsAndRecordCount == null) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND);
			} else {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setEnrollmentsAndRecordCount(enrollmentsAndRecordCount);
			}
		}
		return xstream.toXML(enrollmentResponse);
	}

	@RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String findEnrollmentById(@PathVariable("id") String id) {
		LOGGER.info("============inside findEnrollmentById============");
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		try{
			if (null == id || ("0").equalsIgnoreCase(id)) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			} else {
				Integer enrollmentId = Integer.valueOf(id);
				Enrollment enrollment = enrollmentService.findById(enrollmentId);
				enrollmentResponse.setEnrollment(enrollment);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			}
			return xstream.toXML(enrollmentResponse);
		}catch(Exception e){
			enrollmentResponse = new EnrollmentResponse();
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Unknown Error occuered in findEnrollmentById  : "+e.getMessage());
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			return xstream.toXML(enrollmentResponse);
		}
	}

	@ApiIgnore
	@RequestMapping(value = "/findbyemployer", method = RequestMethod.POST)
	@ResponseBody
	public String findApplicationByIdExternal(@RequestBody Map<String, Object> inputval) {
		LOGGER.info("============inside findApplicationByIdExternal============");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		xstream.addDefaultImplementation(java.sql.Date.class,java.util.Date.class);
		xstream.addDefaultImplementation(java.sql.Timestamp.class,java.util.Date.class);
		xstream.addDefaultImplementation(java.sql.Time.class,java.util.Date.class);
		xstream.registerConverter(DATE_CONVERTER); 
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (inputval == null || inputval.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		} 
		if (inputval.get(EnrollmentConstants.EMPLOYER_ID) == null || inputval.get(EnrollmentConstants.EMPLOYER_ID).equals("")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		}
		if (inputval.get(EnrollmentConstants.EMPLOYER_ENROLLMENT_ID) == null || inputval.get(EnrollmentConstants.EMPLOYER_ENROLLMENT_ID).equals("")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYER_ENROLLMENT_ID_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		}
		if(inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP) != null && !inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP).equals("")){
			if (inputval.get(EnrollmentConstants.CURRENT_DATE) == null || inputval.get(EnrollmentConstants.CURRENT_DATE).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_CURRENT_DATE_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			}
			if (inputval.get(EnrollmentConstants.STATUS) == null ) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_STATUS_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			}
		}
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		List<Object[]> enrollmentNewList = enrollmentService.findEnrollmentByEmployerId(
				Integer.parseInt(inputval.get(EnrollmentConstants.EMPLOYER_ID).toString()), 
				Integer.parseInt(inputval.get(EnrollmentConstants.EMPLOYER_ENROLLMENT_ID).toString()));
		List<Object[]> enrollmentOldList= new ArrayList<Object[]>();
		if(isNotNullAndEmpty(inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP))){
			enrollmentOldList= enrollmentService.findCurrentMonthEffectiveEnrollment(inputval);
		}
		if ((enrollmentNewList == null || enrollmentNewList.isEmpty()) &&(enrollmentNewList == null || enrollmentNewList.isEmpty()) ) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_CURRENT_MONTH);
			return xstream.toXML(enrollmentResponse); 
		}
		Map<String, Set<EnrollmentCurrentMonthDTO>> enrollmentFinanceMap= new HashMap<String,Set<EnrollmentCurrentMonthDTO>>();
		if(enrollmentNewList!=null && !enrollmentNewList.isEmpty()){
			SortedSet<EnrollmentCurrentMonthDTO> enrollmentNewSet=formatFinanceData(enrollmentNewList, inputval);
			if(enrollmentNewSet!=null && !enrollmentNewSet.isEmpty()){
				enrollmentFinanceMap.put(EnrollmentConstants.NEW_MAP_KEY, enrollmentNewSet);
			}
		}
		if(enrollmentOldList!=null && !enrollmentOldList.isEmpty()){
			SortedSet<EnrollmentCurrentMonthDTO> enrollmentOldSet=formatFinanceData(enrollmentOldList, inputval);
			if(enrollmentOldSet!=null && !enrollmentOldSet.isEmpty()){
				enrollmentFinanceMap.put(EnrollmentConstants.OLD_MAP_KEY, enrollmentOldSet);
			}
		}
		enrollmentResponse.setEnrollmentFinanceMap(enrollmentFinanceMap);
		return xstream.toXML(enrollmentResponse);
	}

	@ApiIgnore
	@RequestMapping(value = "/searchenrollmentbycurrentmonth", method = RequestMethod.POST)
	@ResponseBody
	public String findCurrentMonthEffectiveEnrollment(@RequestBody Map<String, Object> inputval) {
		LOGGER.info("=================== inside findCurrentMonthEffectiveEnrollment ===================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		xstream.addDefaultImplementation(java.sql.Date.class,java.util.Date.class);
		xstream.addDefaultImplementation(java.sql.Timestamp.class,java.util.Date.class);
		xstream.addDefaultImplementation(java.sql.Time.class,java.util.Date.class);
		xstream.registerConverter(DATE_CONVERTER);
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (inputval == null || inputval.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		} 
		if (inputval.get(EnrollmentConstants.CURRENT_DATE) == null || inputval.get(EnrollmentConstants.CURRENT_DATE).equals("")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_CURRENT_DATE_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		} 
		if (inputval.get(EnrollmentConstants.STATUS) == null || inputval.get(EnrollmentConstants.STATUS).equals("")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_STATUS_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		} 
		if (inputval.get(EnrollmentConstants.EMPLOYER_ID) == null || inputval.get(EnrollmentConstants.EMPLOYER_ID).equals("")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		}
		if (inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP) == null || inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP).equals("")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_LAST_INVOICE_DATE_IS_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		}
		List<Object[]> enrollmentList = enrollmentService.findCurrentMonthEffectiveEnrollment(inputval);
		if (enrollmentList == null || enrollmentList.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_CURRENT_MONTH);
			return xstream.toXML(enrollmentResponse); 
		}
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		SortedSet<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTOSet = formatFinanceData(enrollmentList, inputval);
		Map<String, Set<EnrollmentCurrentMonthDTO>> enrollmentFinanceMap= new HashMap<String,Set<EnrollmentCurrentMonthDTO>>();
		if(enrollmentCurrentMonthDTOSet!=null && !enrollmentCurrentMonthDTOSet.isEmpty()){
			enrollmentFinanceMap.put(EnrollmentConstants.CURRENT_MAP_KEY, enrollmentCurrentMonthDTOSet);
		}
		enrollmentResponse.setEnrollmentFinanceMap(enrollmentFinanceMap);
		return xstream.toXML(enrollmentResponse);
	}
	
	
	private SortedSet<EnrollmentCurrentMonthDTO> formatFinanceData(List<Object[]> enrollmentList, Map<String, Object> inputval){
		SortedSet<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTOSet = new TreeSet<EnrollmentCurrentMonthDTO>();
		for (Object[] enrollColData : enrollmentList) {
			EnrollmentCurrentMonthDTO enrollmentCurrentMonthDTO = new EnrollmentCurrentMonthDTO();
			enrollmentCurrentMonthDTO.setEnrollmentId((Integer) enrollColData[EnrollmentConstants.ZERO]);
			//enrollmentCurrentMonthDTO.setIssuer((Issuer) enrollColData[EnrollmentConstants.ONE]);
			enrollmentCurrentMonthDTO.setInsurerName((String) enrollColData[EnrollmentConstants.ONE]);
			enrollmentCurrentMonthDTO.setEmployer((Employer) enrollColData[EnrollmentConstants.TWO]);
			enrollmentCurrentMonthDTO.setInsuranceType((String) enrollColData[EnrollmentConstants.THREE]);
			enrollmentCurrentMonthDTO.setGroupPolicyNumber((String) enrollColData[EnrollmentConstants.FOUR]);
			enrollmentCurrentMonthDTO.setGrossPremiumAmt((Float) enrollColData[EnrollmentConstants.FIVE]);
			enrollmentCurrentMonthDTO.setBenefitEffectiveDate((Date) enrollColData[EnrollmentConstants.SIX]);
			enrollmentCurrentMonthDTO.setBenefitEndDate((Date) enrollColData[EnrollmentConstants.SEVEN]);
			enrollmentCurrentMonthDTO.setEmployeeId((Integer) enrollColData[EnrollmentConstants.EIGHT]);
			enrollmentCurrentMonthDTO.setEmployeeContribution((Float) enrollColData[EnrollmentConstants.NINE]);
			enrollmentCurrentMonthDTO.setPolicyNumber((String) enrollColData[EnrollmentConstants.TEN]);
			
			List<EnrollmentSubscriberEventDTO> enrollmentSubscriberEventDtoList = new ArrayList<EnrollmentSubscriberEventDTO>();
			List<EnrollmentEvent> subscriberEventList = enrollmentEventService.findSubscriberEventsForEnrollment((Integer) enrollColData[EnrollmentConstants.ZERO]);

			if(subscriberEventList != null){
				for(EnrollmentEvent subscriberEvent : subscriberEventList){
					EnrollmentSubscriberEventDTO subscriberEventDto = new EnrollmentSubscriberEventDTO();
					subscriberEventDto.setEventReasonCode(subscriberEvent.getEventReasonLkp().getLookupValueCode());
					subscriberEventDto.setEventTypeCode(subscriberEvent.getEventTypeLkp().getLookupValueCode());
					if(DateUtil.dateToString(subscriberEvent.getCreatedOn(), "MM/dd/yyyy hh:mm:ss.S") != null){
						subscriberEventDto.setEventCreatedOn(DateUtil.dateToString(subscriberEvent.getCreatedOn(), "MM/dd/yyyy hh:mm:ss.S"));
					}
					else
					{
						subscriberEventDto.setEventCreatedOn(null);
					}
					enrollmentSubscriberEventDtoList.add(subscriberEventDto);
				}
			}
			
			enrollmentCurrentMonthDTO.setEnrollmentSubscriberEventDtoList(enrollmentSubscriberEventDtoList);
			
			LookupValue enrollmentStatusLkp = (LookupValue) enrollColData[EnrollmentConstants.ELEVEN];
			if (isNotNullAndEmpty(enrollmentStatusLkp)) {
				enrollmentCurrentMonthDTO.setEnrollmentStatusValue(enrollmentStatusLkp.getLookupValueCode());
				enrollmentCurrentMonthDTO.setEnrollmentStatusLabel(enrollmentStatusLkp.getLookupValueLabel());

				// This is added in case if an Enrollment is Re-Instated
				if(	enrollmentStatusLkp.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || 
						enrollmentStatusLkp.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
					Date terminationDate = enrollmentEventService.getEnrollmentTerminationDate(enrollmentCurrentMonthDTO.getEnrollmentId());
					enrollmentCurrentMonthDTO.setTerminationDate(terminationDate);
				}
			}
			
			enrollmentCurrentMonthDTO.setEmployerContribution((Float) enrollColData[EnrollmentConstants.TWELVE]);
			enrollmentCurrentMonthDTO.setEnrollmentCreatedDate((Date) enrollColData[EnrollmentConstants.THIRTEEN]);
			enrollmentCurrentMonthDTO.setEmployerEnrollmentId((Integer)enrollColData[EnrollmentConstants.FOURTEEN]);
			enrollmentCurrentMonthDTO.setIssuerId((Integer)enrollColData[EnrollmentConstants.FIFTEEN]);
			try {
				List<Enrollee> enrolleList = enrolleeService.findEnrolledEnrolleeByEnrollmentID((Integer) enrollColData[EnrollmentConstants.ZERO]);
				if (isNotNullAndEmpty(enrolleList)) {
					enrollmentCurrentMonthDTO.setNoOfPersons(enrolleList.size());
				}
			} catch (GIException e) {
				LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage() , e);
				LOGGER.error(EnrollmentConstants.CAUSE + e.getCause() , e);
			}

			try {
				List<Enrollee> specialEnrolleList = enrolleeService.getSpecialEnrolleeByEnrollmentID(inputval,(Integer) enrollColData[EnrollmentConstants.ZERO]);
				if (isNotNullAndEmpty(specialEnrolleList)) {

					List<EnrolleeCurrentMonthDTO> enrolleeCurrentMonthDTOList = new ArrayList<EnrolleeCurrentMonthDTO>();
					EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO ;

					for (Enrollee enrollee : specialEnrolleList) {

						enrolleeCurrentMonthDTO = new EnrolleeCurrentMonthDTO();
						
						enrolleeCurrentMonthDTO.setEmployerResponsibilityAmt(enrollee.getTotEmpResponsibilityAmt());
						enrolleeCurrentMonthDTO.setEnrolleeId(enrollee.getId());

						enrolleeCurrentMonthDTO.setCoverageStartDate(enrollee.getEffectiveStartDate());
						enrolleeCurrentMonthDTO.setCoverageEndtDate(enrollee.getEffectiveEndDate());
						enrolleeCurrentMonthDTO.setCreatedOn(enrollee.getCreatedOn());

						enrolleeCurrentMonthDTO.setEnrollmentAmount(enrollee.getTotalIndvResponsibilityAmt());

						enrolleeCurrentMonthDTO.setEnrolleeStatus(enrollee.getEnrolleeLkpValue().getLookupValueCode());
						
						if(enrollee.getPersonTypeLkp() != null){
							enrolleeCurrentMonthDTO.setPersonType(enrollee.getPersonTypeLkp().getLookupValueCode());
						}
						if(enrollee.getEnrollmentEvents() != null && !enrollee.getEnrollmentEvents().isEmpty()){
							List<EnrollmentEvent> enrollmentEvents = enrollee.getEnrollmentEvents();
							Collections.sort(enrollmentEvents);

							for(int i = enrollmentEvents.size() - EnrollmentConstants.ONE; i>=EnrollmentConstants.ZERO; i--){
								EnrollmentEvent event = enrollmentEvents.get(i);
								if(isNotNullAndEmpty(event.getSpclEnrollmentReasonLkp())){
									enrolleeCurrentMonthDTO.setEventDate(event.getCreatedOn());
									enrolleeCurrentMonthDTO.setEventName(event.getEventReasonLkp().getLookupValueLabel());
									break;
								}
							}
						}

						enrolleeCurrentMonthDTOList.add(enrolleeCurrentMonthDTO);
					}
					enrollmentCurrentMonthDTO.setEnrolleeCurrentMonthDTOList(enrolleeCurrentMonthDTOList);
				}

			} catch (GIException e) {
				LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage() , e);
				LOGGER.error(EnrollmentConstants.CAUSE + e.getCause() , e);
			}
			//Get special enrolee list. repository method
			//Loop thru it and create enrolleCurrentMonthDTO 
			//Set the enrolleeCurrentMonthDTOLIst to enrollmentCurrentMonthDTO
			
			enrollmentCurrentMonthDTOSet.add(enrollmentCurrentMonthDTO);
		}
		
		return enrollmentCurrentMonthDTOSet;
	}

	@ApiIgnore
	@RequestMapping(value = "/searchenrollmentbystatusandterminationdate", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentByStatusAndTerminationdate(
			@RequestBody Map<String, Object> inputval) {
		LOGGER.info("=================== inside findEnrollmentByStatusAndTerminationdate ===================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (inputval == null || inputval.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse
					.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
		} else {
			List<Enrollment> enrollmentList = enrollmentService
					.findEnrollmentByStatusAndTerminationdate(inputval);
			if (enrollmentList == null || enrollmentList.isEmpty()) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse
						.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_STATUS_AND_DATE);
			} else {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setEnrollmentList(enrollmentList);
			}
		}

		return xstream.toXML(enrollmentResponse);
	}

	@ApiIgnore
	@RequestMapping(value = "/searchenrollmentbystatusandemployerandterminationdate", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentByStatusAndEmployerAndTerminationdate(
			@RequestBody Map<String, Object> inputval) {

		LOGGER.info("=================== inside findEnrollmentByStatusAndEmployerAndTerminationdate ===================");

		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (inputval == null || inputval.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse
					.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
		} else {
			List<Enrollment> enrollmentList = enrollmentService
					.findEnrollmentByStatusAndEmployerAndTerminationdate(inputval);
			if (enrollmentList == null || enrollmentList.isEmpty()) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse
						.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_STATUS_EMPLOYER_AND_DATE);
			} else {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setEnrollmentList(enrollmentList);
			}
		}

		return xstream.toXML(enrollmentResponse);
	}

	@RequestMapping(value = "/createGroupInstallation", method = RequestMethod.POST)
	@ResponseBody
	public String createGroupInstallation(@RequestBody List<Integer> inputList){
		try{
			if(isNotNullAndEmpty(inputList)){
				for(Integer enrollmentID : inputList){
					Enrollment enrollment= enrollmentService.findById(enrollmentID);

					if(enrollment!=null && enrollment.getEmployer() != null && enrollment.getIssuerId() != null && enrollment.getPlanId() != null){
						groupInstallationService.addOrUpdateEmployerGroup(enrollment.getEmployer().getId(), enrollment.getIssuerId(),enrollment.getPlanId(),enrollment.getEmployerEnrollmentId());
					}
				}
			}
		}catch(Exception e){
			LOGGER.error(e.getMessage() , e);
			return "FAILURE";
		}
		return "SUCCESS";
	}
	
	/**
	 * @author panda_p
	 * @since 01-jul-2014
	 * @param orderId
	 * @param enrollmentResponse
	 * @param pldOrderResponse
	 * @return
	 *//*
	private PldOrderResponse getPlanDispResponse(String orderId, EnrollmentResponse enrollmentResponse ){
		String getRestResponse = null;

		Map<String, String> params = new HashMap<String, String>();
		params.put(EnrollmentConstants.ID, orderId);
		PldOrderResponse pldOrderResponse = null;
		try {
			LOGGER.info("Calling plan selection service to get plan and member data");
			getRestResponse = restTemplate.getForObject(PlandisplayEndpoints.FIND_BY_ORDERID,String.class, params);
			LOGGER.info("plandisplayResponse : " + getRestResponse);
			
			if (getRestResponse == null) {
				LOGGER.info(EnrollmentConstants.ERR_MSG_NO_PLAN_DATA_FOUND);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_PLAN_DATA_FOUND_IN_PLAN_SELECTION_SERVICE_RESPONSE);
			}else{
				pldOrderResponse = new platformGson().fromJson(getRestResponse,PldOrderResponse.class);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		} catch (RestClientException re) {
			LOGGER.error(EnrollmentConstants.MESSAGE + re.getMessage() , re);
			LOGGER.error(EnrollmentConstants.CAUSE + re.getCause() , re);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_FAILED_TO_CALL_PLANDISPLAY_ENROLLMENT_DATA_SERVICE);
			enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
		} catch (Exception e) {
			LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage() , e);
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause() , e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_FAILED_TO_CALL_PLANDISPLAY_ENROLLMENT_DATA_SERVICE);
			enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
		}
		
		return pldOrderResponse;
	}
*/	
	
	
	private PldOrderResponse getPlanDispResponse(String pdHouseholdId, EnrollmentResponse enrollmentResponse, PdOrderResponse pdResponse, AutoRenewalRequest autoRenewalReq, String enrollmentType ){

		Map<String, String> params = new HashMap<String, String>();
		params.put(EnrollmentConstants.ID, pdHouseholdId);
		PldOrderResponse pldOrderResponse = null;
		
		pldOrderResponse= enrollmentPDHandshakeService.findByPdHouseholdId(pdHouseholdId, enrollmentResponse, pdResponse,autoRenewalReq, enrollmentType);
		if(enrollmentResponse.getStatus()==null ||!enrollmentResponse.getStatus().equalsIgnoreCase(EnrollmentConstants.FAILURE)){
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			enrollmentResponse.setStatus(EnrollmentConstants.SUCCESS);
		}
		return pldOrderResponse;
	}

	
	@ApiIgnore
	@RequestMapping(value = "/createIndividualEnrollment", method = RequestMethod.POST)
	@ResponseBody
	public String createIndividualEnrollment(@RequestBody Map<String, Object> inputval) {
		LOGGER.info("=================== inside createIndividualEnrollment ===================");
		String pdHouseholdId = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		try {
			if (inputval == null || inputval.isEmpty()) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ORDER_ID_NULL_OR_EMPTY);
			} else {
				
				String applicant_esig = null;
				String onBehalfActionCheckbox=null; 
				
				if (inputval.get(EnrollmentConstants.ORDER_ID) != null) {
					pdHouseholdId = (String) inputval.get(EnrollmentConstants.ORDER_ID);
				}
				if (inputval.get(EnrollmentConstants.APPLICANT_ESIG) != null) {
					applicant_esig = inputval.get(EnrollmentConstants.APPLICANT_ESIG).toString();
				}
				if (inputval.get("onBehalfActionCheckbox") != null) {
					onBehalfActionCheckbox = inputval.get("onBehalfActionCheckbox").toString();
				}
				
				if (inputval.get(EnrollmentConstants.ORDER_ID) == null || inputval.get(EnrollmentConstants.ORDER_ID).equals("")) {
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ORDER_ID_NULL_OR_EMPTY);
				} else {
					PldOrderResponse pldOrderResponse = null;
					pldOrderResponse = getPlanDispResponse(pdHouseholdId, enrollmentResponse, null, null, null);
					if (enrollmentResponse != null && enrollmentResponse.getStatus()!=null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
						return xstream.toXML(enrollmentResponse);
					}
					
					if (pldOrderResponse==null||(pldOrderResponse.getPlanDataList() == null || pldOrderResponse.getPlanDataList().isEmpty())) {
						LOGGER.info(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
						return xstream.toXML(enrollmentResponse);
					}
					pldOrderResponse.setPdHouseholdId(pdHouseholdId);
					if (isNotNullAndEmpty(pldOrderResponse.getEnrollmentType())&& (pldOrderResponse.getEnrollmentType().equals('S') || pldOrderResponse.getEnrollmentType().equals('s'))) {
						LOGGER.info("Call Special Enrollment Creation");
						enrollmentResponse = enrollmentCreationService.createSpecialEnrollment(pldOrderResponse,applicant_esig,onBehalfActionCheckbox, EnrollmentAhbxSync.RecordType.IND20_UPDATE);
						
						if(enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
							if(enrollmentResponse.getMemberAddedToEnrollment()!=null && !enrollmentResponse.getMemberAddedToEnrollment().isEmpty()){
								try{
									enrollmentCreationService.terminateNewlyAddedmembers(enrollmentResponse, EnrollmentEvent.TRANSACTION_IDENTIFIER.SPECIAL_DISENROLLMENT);
								}catch(Exception e){
									LOGGER.error("Error while disenrolling the newly added member to the disenrolled enrollment" , e);
								}
							}
						}
					} else {
						LOGGER.info("Call Initial Enrollment Creation");
						enrollmentResponse = enrollmentCreationService.createEnrollment(pldOrderResponse,applicant_esig,onBehalfActionCheckbox, EnrollmentAhbxSync.RecordType.IND20_INITIAL);
					}

					if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						// Call PlanDisplay Update_Orde_Status API
						updateCartStatus(enrollmentResponse);
					} else {
						if(null != enrollmentResponse){
							enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
							if(enrollmentResponse.getErrMsg()!=null &&enrollmentResponse.getErrMsg().startsWith("IND20 Failed")){
								enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E001);
							}else{
								if(!enrollmentResponse.getModuleStatusCode().equalsIgnoreCase(EnrollmentConstants.MODULE_STATUS_CODE_ENSSAP01)) {
									enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
								}
							}
							
						}
						LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_CREATE_FAILURE);
					}
				}
			}
		}catch (GIException e) {
			//giExceptionHandler.recordFatalException(Component.ENROLLMENT, EnrollmentConstants.MODULE_STATUS_CODE_E003, "Failed to create FFM Agent Redirect Enrollment" , e) ;
			LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage());
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause());
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("" + e.getMessage());
			if(enrollmentResponse.getErrMsg().startsWith("IND20 Failed")){
				enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E001);
			}else{
				enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
				enrollmentResponse.setErrMsg(enrollmentResponse.getErrMsg()+"\n"+EnrollmentUtils.getExceptionMessage(e));
			}
			LOGGER.error("Unknown Error occured in createIndividualEnrollment for pdHouseHoldId: "+pdHouseholdId +"  . Exception = " + e.getMessage() , e);
		} 
		catch (Exception e) {
			//giExceptionHandler.recordFatalException(Component.ENROLLMENT, EnrollmentConstants.MODULE_STATUS_CODE_E003, "Failed to create FFM Agent Redirect Enrollment" , e) ;
			LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage());
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause());
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("" + e.getMessage());
			if(enrollmentResponse.getErrMsg().startsWith("IND20 Failed")){
				enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E001);
			}else{
				enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
				enrollmentResponse.setErrMsg(enrollmentResponse.getErrMsg()+"\n"+EnrollmentUtils.getExceptionMessage(e));
			}
			LOGGER.error("Unknown Error occured in createIndividualEnrollment for pdHouseHoldId: "+pdHouseholdId +"  . Exception = " + e.getMessage() , e);
		}
		
		//Set plan to Null
		/** Used @XStreamOmitField in Enrollment.java Model to omit plan. Hence committed **/
		setRequiredFieldForResponse(enrollmentResponse);

		return xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * @author panda_p
	 * @since 01-jul-2014
	 * @param orderId
	 * @return
	 */
	@ApiIgnore
	@RequestMapping(value = "/createautorenewal", method = RequestMethod.POST)
	@ResponseBody
	public String createAutoRenewal(@RequestBody Map<String, Object> inputval) {
		LOGGER.info("============inside createAutoRenewal============");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		
		if (inputval == null || inputval.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ORDER_ID_NULL_OR_EMPTY);
			return xstream.toXML(enrollmentResponse);
		}
		
		boolean marketTypeSHOP = EnrollmentConfiguration.isNmCall() || EnrollmentConfiguration.isMsCall();
				
		String orderid = null;
		AutoRenewalRequest autoRenReq=null;
		String enrollmentType=null;
		PdOrderResponse pdResponse=null;
		String globalId = null;
		//String correlationId = null;
		if (inputval.get(EnrollmentConstants.ORDER_ID) != null) {
			orderid = inputval.get(EnrollmentConstants.ORDER_ID).toString();
		}
		if(inputval.get(EnrollmentConstants.ENROLLMENT_TYPE) != null){
			enrollmentType=inputval.get(EnrollmentConstants.ENROLLMENT_TYPE).toString();
		}
		if(isNotNullAndEmpty(inputval.get(EnrollmentConstants.AUTORENEWAL_REQUEST))){
			autoRenReq= platformGson.fromJson(inputval.get(EnrollmentConstants.AUTORENEWAL_REQUEST).toString(), AutoRenewalRequest.class );
		}
		if(isNotNullAndEmpty(inputval.get(EnrollmentConstants.PD_RESPONSE))){
			pdResponse= platformGson.fromJson(inputval.get(EnrollmentConstants.PD_RESPONSE).toString(), PdOrderResponse.class );
		}
//		if (inputval.get(EnrollmentConstants.GLOBAL_ID) != null) {
//			globalId = inputval.get(EnrollmentConstants.GLOBAL_ID).toString();
//		}
//		if (inputval.get("correlationId") != null) {
//			correlationId = inputval.get("correlationId").toString();
//		}
		
		if (orderid == null || orderid.equalsIgnoreCase("0")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		} else if( !marketTypeSHOP && !EnrollmentUtils.isNotNullAndEmpty( enrollmentType)){
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_TYPE_CANNOT_BE_NULL);
//		} else if (EnrollmentConfiguration.isCaCall() && !EnrollmentUtils.isNotNullAndEmpty(globalId)) {
//			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
//			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_206);
//			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_GLOBAL_ID_CANNOT_BE_NULL);
		}else {
			PldOrderResponse pldOrderResponse = null;
			
			if(marketTypeSHOP){
				pldOrderResponse = getPlanDispResponse(orderid, enrollmentResponse, null, null, null);
			}else{
//				if(pdResponse!=null){
//					pdResponse.setGlobalId(globalId);
//				}
				pldOrderResponse = getPlanDispResponse(orderid, enrollmentResponse, pdResponse, autoRenReq, enrollmentType);
			}

			if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				return xstream.toXML(enrollmentResponse);
			}
			
			if (pldOrderResponse==null||(pldOrderResponse.getPlanDataList() == null || pldOrderResponse.getPlanDataList().isEmpty())) {
				LOGGER.info(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
				return xstream.toXML(enrollmentResponse);
			}
			
			try {
				pldOrderResponse.setPdHouseholdId(orderid);
				if(isNotNullAndEmpty(pldOrderResponse.getEnrollmentType())&& (pldOrderResponse.getEnrollmentType().equals('S') || pldOrderResponse.getEnrollmentType().equals('s'))){
					enrollmentResponse = enrollmentCreationService.createSpecialEnrollment(pldOrderResponse,null,null, EnrollmentAhbxSync.RecordType.IND71_AUTOSPECIAL);
				}else{
					enrollmentResponse = enrollmentCreationService.createEnrollment(pldOrderResponse,null,null, EnrollmentAhbxSync.RecordType.IND71_RENEWAL);
				}
				
				if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					try{
						List <Enrollment> enrollmentList= enrollmentResponse.getEnrollmentList();
						if(enrollmentList!=null && !enrollmentList.isEmpty()){
							for (Enrollment enr : enrollmentList){
								Enrollment enrollment= enrollmentService.findById(enr.getId());
								if(enrollment!=null && enrollment.getEmployer() != null && enrollment.getIssuerId() != null && enrollment.getPlanId() != null){
									groupInstallationService.addOrUpdateEmployerGroup(enrollment.getEmployer().getId(), enrollment.getIssuerId(),enrollment.getPlanId(),enrollment.getEmployerEnrollmentId());
								}
							}
						}
					}catch (Exception e){
						LOGGER.error("Error while creating group installation" , e);
					}
					// Call PlanDisplay Update_Orde_Status API
					updateCartStatus(enrollmentResponse);
					
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);

				} else {
					if(null != enrollmentResponse){
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_CREATE_FAILURE);
					}
				}
			} catch (Exception e) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Unknown Error occured in Enrollment Auto flow : " + EnrollmentUtils.getExceptionMessage(e));
				enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
			}
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * Json based API, For Creating Auto Renewal 
	 * Jira Id: HIX-90479
	 * @param requestString
	 * @return
	 */
	@RequestMapping(value = "/createautorenewaljson", method = RequestMethod.POST)
	@ResponseBody
	public String createAutoRenewalJson(@RequestBody String requestString) {
		LOGGER.info("============inside createAutoRenewalJson============");
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if(StringUtils.isNotBlank(requestString)){
			try{
				boolean marketTypeSHOP = EnrollmentConfiguration.isNmCall() || EnrollmentConfiguration.isMsCall();
				EnrollmentAutoRenewalDTO enrollmentAutoRenewalDTO = platformGson.fromJson(requestString, EnrollmentAutoRenewalDTO.class);
				if(enrollmentAutoRenewalDTO != null){
					if (StringUtils.isBlank(enrollmentAutoRenewalDTO.getOrderid()) || (StringUtils.isNotBlank(enrollmentAutoRenewalDTO.getOrderid()) && 
							enrollmentAutoRenewalDTO.getOrderid().equalsIgnoreCase("0"))) {
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
					} else if( !marketTypeSHOP && !EnrollmentUtils.isNotNullAndEmpty(enrollmentAutoRenewalDTO.getEnrollmentType())){
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_TYPE_CANNOT_BE_NULL);
//					}else if(EnrollmentConfiguration.isCaCall() && !EnrollmentUtils.isNotNullAndEmpty(enrollmentAutoRenewalDTO.getGlobalId())){
//						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
//						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_206);
//						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_GLOBAL_ID_CANNOT_BE_NULL);
					}else {
						PldOrderResponse pldOrderResponse = null;
						
						if(marketTypeSHOP){
							pldOrderResponse = getPlanDispResponse(enrollmentAutoRenewalDTO.getOrderid(), enrollmentResponse, null, null, null);
						}else{
//							if(enrollmentAutoRenewalDTO.getPdOrderResponse() != null){
//								enrollmentAutoRenewalDTO.getPdOrderResponse().setGlobalId(enrollmentAutoRenewalDTO.getGlobalId());
//							}
							pldOrderResponse = getPlanDispResponse(enrollmentAutoRenewalDTO.getOrderid(), enrollmentResponse, enrollmentAutoRenewalDTO.getPdOrderResponse(),
									enrollmentAutoRenewalDTO.getAutoRenewalRequest(), enrollmentAutoRenewalDTO.getEnrollmentType());
						}

						if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
							return platformGson.toJson(enrollmentResponse);
						}
						
						if (pldOrderResponse==null||(pldOrderResponse.getPlanDataList() == null || pldOrderResponse.getPlanDataList().isEmpty())) {
							LOGGER.info(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
							enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
							enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
							enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
							return platformGson.toJson(enrollmentResponse);
						}
						pldOrderResponse.setPdHouseholdId(enrollmentAutoRenewalDTO.getOrderid());
						if(isNotNullAndEmpty(pldOrderResponse.getEnrollmentType())&& (pldOrderResponse.getEnrollmentType().equals('S') || pldOrderResponse.getEnrollmentType().equals('s'))){
							enrollmentResponse = enrollmentCreationService.createSpecialEnrollment(pldOrderResponse,null,null, EnrollmentAhbxSync.RecordType.IND71_AUTOSPECIAL);
						}else{
							enrollmentResponse = enrollmentCreationService.createEnrollment(pldOrderResponse,null,null, EnrollmentAhbxSync.RecordType.IND71_RENEWAL);
						}
						
						if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
							try{
								List <Enrollment> enrollmentList= enrollmentResponse.getEnrollmentList();
								if(enrollmentList!=null && !enrollmentList.isEmpty()){
									for (Enrollment enr : enrollmentList){
										Enrollment enrollment= enrollmentService.findById(enr.getId());
										if(enrollment!=null && enrollment.getEmployer() != null && enrollment.getIssuerId() != null && enrollment.getPlanId() != null){
											groupInstallationService.addOrUpdateEmployerGroup(enrollment.getEmployer().getId(), enrollment.getPlanId(),enrollment.getPlanId(),enrollment.getEmployerEnrollmentId());
										}
									}
								}
							}catch (Exception e){
								LOGGER.error("Error while creating group installation" , e);
							}
							// Call PlanDisplay Update_Orde_Status API
							updateCartStatus(enrollmentResponse);	
							enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
							enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);

						} else {
							if(null != enrollmentResponse){
								enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
								enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
								enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_CREATE_FAILURE);
							}
						}	
					}
				}
				else{
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ORDER_ID_NULL_OR_EMPTY);
				}
			}
			catch(Exception ex){
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Unknown Error occured in Enrollment Auto flow : " + EnrollmentUtils.getExceptionMessage(ex));
				enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
			}
		}
		else{
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ORDER_ID_NULL_OR_EMPTY);
		}
		
		enrollmentResponse.setEnrollmentList(null);
		enrollmentResponse.setEnrollment((null));
		return platformGson.toJson(enrollmentResponse);
	}
	
	
	/**
	 * @author panda_p
	 * @since 03/Aug/2018
	 * HIX-108700
 	 * API to Process CustomGroup Enrollment UPdate using IND71G;
	 * @param requestString
	 * @return
	 */
	@RequestMapping(value = "/processcustomgroupenrollments", method = RequestMethod.POST)
	@ResponseBody
	public String processCustomGroupEnrollments(@RequestBody String requestString) {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		EnrollmentResponse ghixResponse=new EnrollmentResponse();
		boolean isSuccess=false;
		GIWSPayload giWsPayloadObj=null;
			try{
		if(StringUtils.isNotBlank(requestString)){
			LOGGER.info("============inside processcustomgroupenrollments============");
			
				giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService,
						requestString, null,
						GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_CUSTOM_GROUPING, "ENROLLMENT_CUSTOM_GROUPING",
						isSuccess);
				EnrollmentCustomGroupDTO enrollmentCustomGroupDTO = platformGson.fromJson(requestString, EnrollmentCustomGroupDTO.class);
				if(enrollmentCustomGroupDTO != null){
					if (enrollmentCustomGroupDTO.getInd71GRequest() !=null && enrollmentCustomGroupDTO.getPdOrderResponse()!=null) {

						PldOrderResponse pldOrderResponse =enrollmentCustomGroupingService.getPDOrderData(enrollmentCustomGroupDTO, enrollmentResponse);
						if (enrollmentResponse != null &&GhixConstants.RESPONSE_FAILURE.equalsIgnoreCase(enrollmentResponse.getStatus())) {
							return platformGson.toJson(enrollmentResponse);
						}

						if (pldOrderResponse==null) {
							LOGGER.info(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
							enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
							enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
							enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
							return platformGson.toJson(enrollmentResponse);
						}
						if((pldOrderResponse.getPlanDataList() == null || pldOrderResponse.getPlanDataList().isEmpty()) && !isCompleteDisenrollmentRequest(enrollmentCustomGroupDTO)) {
							LOGGER.info(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
							enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
							enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
							enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO);
							return platformGson.toJson(enrollmentResponse);
						}
							enrollmentResponse = enrollmentCreationService.createSpecialEnrollment(pldOrderResponse,null,null,EnrollmentAhbxSync.RecordType.IND71G_CUSTOMGROUPING_SPECIAL);

						if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
							if(enrollmentResponse.getMemberAddedToEnrollment()!=null && !enrollmentResponse.getMemberAddedToEnrollment().isEmpty()){
								try{
									enrollmentCreationService.terminateNewlyAddedmembers(enrollmentResponse, EnrollmentEvent.TRANSACTION_IDENTIFIER.SPECIAL_DISENROLLMENT);
								}catch(Exception e){
									LOGGER.error("Error while disenrolling the newly added member to the disenrolled enrollment" , e);
								}
							}
							enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
							enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						}else {
							if(null != enrollmentResponse){
								enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
								enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
								enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_CREATE_FAILURE+ enrollmentResponse.getErrMsg());
							}
						}	
						isSuccess=true;
					}else {
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentResponse.setErrMsg("Invalid input : Either IND71GRequest or PdOrderResponse is null");
					}//End of request validation
				}else{
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				}
			
		}else{
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
		}
			}catch(Exception ex){
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Unknown Error occured in Enrollment custom grouping flow : " + EnrollmentUtils.getExceptionMessage(ex));
				enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
		}finally{
			if(enrollmentResponse!=null ) {
				ghixResponse.setStatus(enrollmentResponse.getStatus());
				ghixResponse.setErrMsg(enrollmentResponse.getErrMsg());
				ghixResponse.setModuleStatusCode(enrollmentResponse.getModuleStatusCode());
				ghixResponse.setErrCode(enrollmentResponse.getErrCode());
			}
			if(giWsPayloadObj!=null ){
				EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj,platformGson.toJson(ghixResponse) , isSuccess);
			}
		}
		//enrollmentResponse.setEnrollmentList(null);
		//enrollmentResponse.setEnrollment((null));
		return platformGson.toJson(ghixResponse);
	}
	

	/**
	 * @author panda_p
	 * @since 20-Aug-2015
	 * @param enrollmentResponse
	 * @param orderId
	 * @return
	 */
	private EnrollmentResponse updateCartStatus(EnrollmentResponse enrollmentResponse){
		try {
			List<Long> pdOrderitemIds = null;
			List <Enrollment> enrList = enrollmentResponse.getEnrollmentList();
			if(enrList !=null && !enrList.isEmpty()){
				pdOrderitemIds = new ArrayList<Long>();
				for(Enrollment enr : enrList){
					if(isNotNullAndEmpty(enr.getPdOrderItemId())){
						pdOrderitemIds.add(enr.getPdOrderItemId());						
					}
				}
				if(!pdOrderitemIds.isEmpty()){
					restTemplate.postForObject(GhixEndPoints.PlandisplayEndpoints.UPDATE_ORDER_ITEM_AS_ENROLLED, pdOrderitemIds, PlanDisplayResponse.class);
					LOGGER.info("Plandisplay UPDATE_ORDER_ITEM_AS_ENROLLED API Call Success");
				}
			}
		} catch (Exception e) {
			LOGGER.error("Plandisplay UPDATE_ORDER_ITEM_AS_ENROLLED API Call Failed ");
			LOGGER.error("Exception = " + e.getMessage() , e);
		}
		
		return enrollmentResponse;
	}
	
	

	/* */
	@RequestMapping(value = "/findenrollmentsbyplan", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentsByPlan(@RequestBody String enrollmentRequestStr) {

		LOGGER.info("=================== inside findEnrollmentsByPlan ===================");

		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<Enrollment> enrollmentList = null;
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
		EnrollmentResponse enrollmentResponse = null;
		Enum<MarketType> planMarketType = enrollmentRequest.getPlanMarket();
		String planNumber = (enrollmentRequest.getPlanNumber() != null) ? enrollmentRequest.getPlanNumber().toString() : null;

		String planMarketTypeValue = null;
		if (planMarketType != null) {
			Integer lookUpValue = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE,planMarketType.toString());
			planMarketTypeValue = Integer.toString(lookUpValue);
		}

		try {
			// Changed the Status field to hardcoded 'CONFIRM' as per discussion with Archana D.
			enrollmentList = enrollmentService.findEnrollmentsByPlan(planNumber,
					enrollmentRequest.getPlanLevel(), planMarketTypeValue,
					enrollmentRequest.getRatingRegion(),
					enrollmentRequest.getStartDate(),
					enrollmentRequest.getEndDate(),
					 "CONFIRM");

			enrollmentResponse = getPlanDataFromEnrollmentList(enrollmentList);

		} catch (Exception e) {
			LOGGER.error("No enrollments found for the plan search criteria."
					+ e.getMessage() ,  e);
		}
		return xstream.toXML(enrollmentResponse);
	}

	@RequestMapping(value = "/findenrollmentsbyissuer", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentsByIssuer(
			@RequestBody String enrollmentRequestStr) {

		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<Enrollment> enrollmentList = null;
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream
				.fromXML(enrollmentRequestStr);
		Enum<MarketType> planMarketType = enrollmentRequest.getPlanMarket();
//		String enrollmentStatus = isNotNullAndEmpty(enrollmentRequest
//				.getEnrollmentStatus()) ? enrollmentRequest
//				.getEnrollmentStatus().toString() : null;
		String planMarketTypeValue = null;
		EnrollmentResponse enrollmentResponse = null;
		if (planMarketType != null) {
			int lookUpValue = lookupService
					.getlookupValueIdByTypeANDLookupValueCode(
							EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE,
							planMarketType.toString());
			planMarketTypeValue = Integer.toString(lookUpValue);
		}

		try {
			// Changed the Status field to hardcoded 'CONFIRM' as per discussion with Archana D.
			enrollmentList = enrollmentService.findEnrollmentsByIssuer(
					enrollmentRequest.getIssuerName(),
					enrollmentRequest.getPlanLevel(), planMarketTypeValue,
					enrollmentRequest.getRatingRegion(),
					enrollmentRequest.getStartDate(),
					enrollmentRequest.getEndDate(),
					"CONFIRM");
			enrollmentResponse = getPlanDataFromEnrollmentList(enrollmentList);

		} catch (Exception e) {
			LOGGER.error("No enrollments found for the issuer search criteria."
					+ e.getMessage() , e);
		}
		return xstream.toXML(enrollmentResponse);
	}

	/**
	 * This method is to iterate the enrollment list and setting the
	 * EnrollmentPlanResponseDTO values from the list.
	 * 
	 * @param enrollmentList
	 * @return List<EnrollmentPlanResponseDTO> in the form of EnrollmentResponse
	 */
	private EnrollmentResponse getPlanDataFromEnrollmentList(
			List<Enrollment> enrollmentList) {

		List<EnrollmentPlanResponseDTO> enrollmentPlanResponseDTOList = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();

		if (enrollmentList != null) {
			enrollmentPlanResponseDTOList = new ArrayList<EnrollmentPlanResponseDTO>();
			for (Enrollment enrollmentObj : enrollmentList) {
				if (enrollmentObj != null) {
					EnrollmentPlanResponseDTO enrlPlanRespObj = new EnrollmentPlanResponseDTO();
					enrlPlanRespObj.setEnrollmentId(enrollmentObj.getId());
					if (isNotNullAndEmpty(enrollmentObj.getPlanId())) {
						enrlPlanRespObj.setPlanId(enrollmentObj.getPlanId().toString());
					}
					if (isNotNullAndEmpty(enrollmentObj.getIssuerId())) {
						enrlPlanRespObj.setIssuerId(Integer.toString(enrollmentObj.getIssuerId()));
					}
					if (isNotNullAndEmpty(enrollmentObj.getUpdatedOn())) {
						enrlPlanRespObj.setUpdatedDate(enrollmentObj.getUpdatedOn().toString());
					}
					if (isNotNullAndEmpty(enrollmentObj.getEnrollmentStatusLkp())) {
						enrlPlanRespObj.setEnrollmentStatus(enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode());
					}
					enrollmentPlanResponseDTOList.add(enrlPlanRespObj);
				}
			}
			enrollmentResponse.setEnrollmentPlanResponseDTOList(enrollmentPlanResponseDTOList);
		}
		return enrollmentResponse;
	}

	@RequestMapping(value = "/findtaxfilingdateforyear", method = RequestMethod.POST)
	@ResponseBody
	public String findTaxFilingDateForYear(@RequestBody String year) {
		TaxYear taxYearObj = null;
		try {
//			LOGGER.info("current tax year= " + year);
			taxYearObj = taxFilingService.findTaxFilingDateByYear(year);

		} catch (Exception e) {
//			LOGGER.error("No tax filing date found for the year " + year + "" + e.getMessage(), e);
			LOGGER.error("No tax filing date found for the year", e);

		}
		if (taxYearObj != null) {
			LOGGER.info("TaxDueYear= " + taxYearObj.getTaxDueYear());
			return platformGson.toJson(taxYearObj, taxYearObj.getClass());
		}

		return null;

	}

	/**
	 * * @author raja
	 * 
	 * This method is called to update Enrollment by passing parameters as
	 * EnrollmentRequest and return the success or failure message in the form
	 * of EnrollmentResponse. Excepts map from the EnrollmentRequest with
	 * following request object adminUpdateIndividualRequest 200 error key is
	 * success message "enrollment updated successfully". 201 is error key for
	 * input object adminUpdateIndividualRequest is null. 202 is error key for
	 * input object HouseHold is null. 203 is error key for input Invalid
	 * EnrollmentId. 204 is error key for Enrollment not found for the input
	 * enrollment id. 205 is error key for Invalid User. 206 is error key for
	 * "update enrollment is failed".
	 */

	@RequestMapping(value = "/adminupdateenrollment", method = RequestMethod.POST)
	@ResponseBody
	public String adminUpdateEnrollment(@RequestBody String enrollmentRequestStr) {
		LOGGER.info("Admin Update Request :: "  + enrollmentRequestStr);
		/*XStream xstream = GhixUtils.getXStreamStaxObject();*/
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		boolean isSuccess=false;
		//Enrollment updatedEnrollmentObj = null;
		GIWSPayload giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService,
				enrollmentRequestStr, null,
				GhixEndPoints.EnrollmentEndPoints.ADMIN_UPDATE_ENROLLMENT_URL, "ENROLLMENT_ADMIN_UPDATE",
				isSuccess);

		/*EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);*/
		try{
			EnrollmentRequest enrollmentRequest = platformGson.fromJson(enrollmentRequestStr, EnrollmentRequest.class);
			AdminUpdateIndividualRequest adminUpdateIndividualRequest = enrollmentRequest.getAdminUpdateIndividualRequest();
			
			/*Long updatedLocationId = adminUpdateIndividualRequest.getHousehold().getUpdatedMailingAddressLocationID();
			Boolean isLocationIdUpdated = adminUpdateIndividualRequest.getHousehold().isIsMailingAddressChanged();
			Boolean isOnlySsapAppIdUpdated = adminUpdateIndividualRequest.getHousehold().isIsOnlySsapAppIdUpdated();*/

			if (adminUpdateIndividualRequest == null) {
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ADMIN_UPDATE_INDIVIDUAL_REQUEST_IS_NULL);
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			} else if (adminUpdateIndividualRequest.getHousehold() == null) {
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_HOUSEHOLD_IS_NULL);
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			} else if((null != adminUpdateIndividualRequest.getHousehold().isIsMailingAddressChanged() && adminUpdateIndividualRequest.getHousehold().isIsMailingAddressChanged()) && (null != adminUpdateIndividualRequest.getHousehold().isIsOnlySsapAppIdUpdated() && adminUpdateIndividualRequest.getHousehold().isIsOnlySsapAppIdUpdated())){
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_207);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_REQUEST);
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			} else {
				adminUpdateService.adminUpdateService(adminUpdateIndividualRequest,  adminUpdateIndividualRequest.getHousehold().getUpdatedMailingAddressLocationID(),  adminUpdateIndividualRequest.getHousehold().isIsMailingAddressChanged(),  adminUpdateIndividualRequest.getHousehold().isIsOnlySsapAppIdUpdated(), enrlResp);
				isSuccess = true;
				giWsPayloadObj.setHouseHoldCaseId(String.valueOf(adminUpdateIndividualRequest.getHousehold().getHouseholdCaseId()));
				giWsPayloadObj.setSsapApplicationId(adminUpdateIndividualRequest.getHousehold().getSsapApplicationid());
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				enrlResp.setErrMsg(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);
				enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);

				LOGGER.info(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);
			}
		}catch(Exception e){
			
			if(enrlResp==null || enrlResp.getErrCode()==0){
				LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + e.getMessage() + EnrollmentConstants.CAUSE + e.getCause() , e);
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_206);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + e.getMessage() + EnrollmentConstants.CAUSE + e.getCause());
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
		}finally{
			if(giWsPayloadObj!=null && enrlResp!=null ){
				EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj,platformGson.toJson(enrlResp) , isSuccess);
				}
		}
		
		/*return xstream.toXML(enrlResp);*/
		return platformGson.toJson(enrlResp);
	}
	
	@ResponseBody
	@RequestMapping(method=RequestMethod.POST,value="/enrollmetmailingddressupdate" )
	public EnrollmentResponse enrollmetMailingAddressUpdate(@RequestBody EnrollmentRequest request){
		EnrollmentResponse response= new EnrollmentResponse();
		if(request!=null && request.getEnrollmentMailingAddressUpdateRequest()!=null){
			try{
				EnrollmentMailingAddressUpdateRequest mailingUpdateRequest=request.getEnrollmentMailingAddressUpdateRequest();
				
				if (mailingUpdateRequest.getHouseholdCaseId() == null) {
					response.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_HOUSEHOLD_IS_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if(mailingUpdateRequest.getUpdatedMailingAddressLocationID()==null){
					response.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_REQUEST);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else{
					enrollmentService.updateMailByHouseHoldId(mailingUpdateRequest, response);
					if( response.getErrCode()==0){
						response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						response.setErrMsg(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);
						response.setStatus(GhixConstants.RESPONSE_SUCCESS);
					}

					LOGGER.info(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);
				}
				
			}catch(Exception e){
				if(response==null || response.getErrCode()==0){
					LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + e.getMessage() + EnrollmentConstants.CAUSE + e.getCause() , e);
					response.setErrCode(EnrollmentConstants.ERROR_CODE_206);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + e.getMessage() + EnrollmentConstants.CAUSE + e.getCause());
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
			}
		}else{
			response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ADMIN_UPDATE_INDIVIDUAL_REQUEST_IS_NULL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		
		
		return response;
	}
	


	/**
	 * * @author raja
	 * 
	 * This method is called to disEnroll an Enrollee by passing parameters as
	 * EnrollmentRequest and return the success or failure message in the form
	 * of EnrollmentResponse. Excepts map from the EnrollmentRequest with
	 * following keys MemberId , EnrollmentID, TerminationDate ,
	 * EnrollmentStatus , DisEnrollmentType , TerminationReasonCode , DeathDate
	 * 200 error key is success message. 201 is error key for input key
	 * MemberId. 202 is error key for input key EnrollmentID. 203 is error key
	 * for input key TerminationDate. 204 is error key for input key
	 * EnrollmentStatus. 205 is error key for input key DisEnrollmentType. 206
	 * is error key for input key TerminationReasonCode. 207 is error key for
	 * input key DeathDate. 208 is error key for dis-enrollment service
	 * error/exception. 209 is error key for input key EnrollmentStatus should
	 * be CANCEL/TERM.
	 */
	
	
	@RequestMapping(value = "/disenrollbyenrollmentid", method = RequestMethod.POST)
	@ResponseBody
	public String disEnrollByEnrollmentIDS(@RequestBody String enrollmentRequestStr){
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String responseText = null;
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
		Map<Integer, String> enrollmentIdAndtermDateMap=null;
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();

		if (enrollmentRequest != null) {
			Map<String, Object> disEnrollmentMap = enrollmentRequest.getMapValue();
			if (disEnrollmentMap != null) {
				LOGGER.info("REQUEST MAP Text disEnrollByEmployeeID :" + disEnrollmentMap.toString());
				if (enrollmentRequest.getIdList() != null && enrollmentRequest.getIdList().isEmpty()) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (!isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_DATE_IS_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (!(DateUtil.isValidDate((String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE),GhixConstants.REQUIRED_DATE_FORMAT))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_DATE_FORMAT_ERROR);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (!isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_REASON_CODE_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE))
						&& (disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE).equals(EnrollmentConstants.REASON_CODE_DEATH))
						&& (!isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.DEATH_DATE)))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_205);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DEATH_DATE_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else {
					List<Integer> enrollmentIdsList = enrollmentRequest.getIdList();
					if (enrollmentIdsList != null && !enrollmentIdsList.isEmpty()) {
						try{
							enrollmentList = enrollmentService.findAllEnrollments(enrollmentIdsList);
							if(enrollmentList.isEmpty()){
								enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_207);
								enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID);
								enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
								return xstream.toXML(enrlResp);
							}
							EnrollmentDisEnrollmentDTO disEnrollmentDTO = new EnrollmentDisEnrollmentDTO();
							// Set Enrollment List to be dis enrolled
							disEnrollmentDTO.setEnrollmentList(enrollmentList);

							disEnrollmentDTO.setTerminationDate((String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE));
							disEnrollmentDTO.setTerminationReasonCode((String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE));
							disEnrollmentDTO.setDeathDate((String) disEnrollmentMap.get(EnrollmentConstants.DEATH_DATE));
							
							// Set Updated by as Logged In user
							AccountUser loggedInUser  = enrollmentService.getLoggedInUser();
							disEnrollmentDTO.setUpdatedBy(loggedInUser);
							disEnrollmentDTO.setAllowRetroTermination(true);
							disEnrollmentDTO.setNullifyDentalAPTCOnHealthTerm(true);
							if(EnrollmentConfiguration.isCaCall()) {
								disEnrollmentDTO.setInd56DisenrollmentCall(true);
							}
							enrollmentList = enrollmentService.disEnrollEnrollment(disEnrollmentDTO, EnrollmentEvent.TRANSACTION_IDENTIFIER.ENROLLMENT_DISENROLL);

							if (enrollmentList != null && !enrollmentList.isEmpty()) {
							// Dis-Enrollment Notification for SHOP Flow
							if(enrollmentList.get(0).getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
								enrollmentEmailNotificationService.sendEnrollmentStatusNotification(enrollmentList,(String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE));	
							}
							
								enrollmentIdAndtermDateMap= new HashMap<Integer, String>();
								for(Enrollment enrollment : enrollmentList){
									if(enrollment.getBenefitEndDate()!=null){
										enrollmentIdAndtermDateMap.put(enrollment.getId(), DateUtil.dateToString(enrollment.getBenefitEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
									}
									/**
									 * Logging ENROLLMENT_STATUS_UPDATE application Event.
									 */
									enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, isNotNullAndEmpty(enrollment.getAppEventReason())? enrollment.getAppEventReason() :
										EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString(), loggedInUser);
								}
							}
							enrlResp.setEnrollmentIdAndtermDateMap(enrollmentIdAndtermDateMap);
							
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
							enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
							enrlResp.setErrMsg(responseText);

							LOGGER.info("dis-enrollment has been done successfully...!");

						} catch (Exception exception) {
							LOGGER.error(EnrollmentConstants.ERR_MSG_DIS_ENROLLMENT_FAIL + " :: " + exception.getMessage() + EnrollmentConstants.CAUSE + exception.getCause() , exception);
							enrlResp.setIdList(enrollmentIdsList);
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_206);
							enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DIS_ENROLLMENT_FAIL + " :: " + exception.getMessage() + EnrollmentConstants.CAUSE + exception.getCause());
							enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
						}
					}
				}
			}
		}
		return xstream.toXML(enrlResp);
	}


	/**
	 * @author parhi_s
	 * 
	 *         This method is called to disEnroll an Enrollee by passing
	 *         parameters as EnrollmentRequest and return the success or failure
	 *         message in the form of EnrollmentResponse. Excepts map from the
	 *         EnrollmentRequest with following keys EmployeeAppID, TerminationDate
	 *         , TerminationReasonCode , DeathDate 200 error key is success
	 *         message. 201 is error key for input key EmployeeID. 202 is error
	 *         key for input key TerminationDate. 203 is error key for input key
	 *         TerminationDate Format. 204 is error key for input key
	 *         TerminationReasonCode. 205 is error key for input key DeathDate.
	 *         206 is error key for dis-enrollment service error/exception.
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/disenrollbyemployeeID", method = RequestMethod.POST)
	@ResponseBody
	public String disenrollbyemployeeApplicationID(@RequestBody String enrollmentRequestStr) {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		Integer employeeId = null;
		String responseText = null;
		List<Enrollment> enrollmentList = null;
		
		List<Integer> employeeAppIdList = null;
		Map<Integer, String> enrollmentIdAndtermDateMap=null;
		if (enrollmentRequest != null) {
			Map<String, Object> disEnrollmentMap = enrollmentRequest.getMapValue();
			
			if (disEnrollmentMap != null) {
				LOGGER.info("REQUEST MAP Text disEnrollByEmployeeID :" + disEnrollmentMap.toString());
				if (!isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.EMPLOYEE_ID_KEY)) && !isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.EMPLOYEE_APP_ID))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYEE_ID_AND_EMPLOYEE_ID_IS_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else if (!isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_DATE_IS_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (!(DateUtil.isValidDate((String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE),GhixConstants.REQUIRED_DATE_FORMAT))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_DATE_FORMAT_ERROR);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (!isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_REASON_CODE_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE))
						&& (disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE).equals(EnrollmentConstants.REASON_CODE_DEATH))
						&& (!isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.DEATH_DATE)))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_205);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DEATH_DATE_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else {
					try {
						employeeId = (Integer) disEnrollmentMap.get(EnrollmentConstants.EMPLOYEE_ID_KEY);
						employeeAppIdList = (List<Integer>)disEnrollmentMap.get(EnrollmentConstants.EMPLOYEE_APP_ID);

						if(isNotNullAndEmpty(employeeAppIdList) && !employeeAppIdList.isEmpty() ){
							enrollmentList = enrollmentService.findActiveEnrollmentByEmployeeApplicationID(employeeAppIdList,DateUtil.StringToDate((String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE),GhixConstants.REQUIRED_DATE_FORMAT));
						}else{
							enrollmentList = enrollmentService.findActiveEnrollmentByEmployeeID(employeeId,DateUtil.StringToDate((String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE),GhixConstants.REQUIRED_DATE_FORMAT));
						}
						
						if (enrollmentList != null && !enrollmentList.isEmpty()) {
							EnrollmentDisEnrollmentDTO disEnrollmentDTO = new EnrollmentDisEnrollmentDTO();
							disEnrollmentDTO.setEnrollmentList(enrollmentList);
							disEnrollmentDTO.setTerminationDate((String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE));
							disEnrollmentDTO.setTerminationReasonCode((String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE));
							disEnrollmentDTO.setDeathDate((String) disEnrollmentMap.get(EnrollmentConstants.DEATH_DATE));
							disEnrollmentDTO.setAllowRetroTermination(true);
							// Set Updated by as Logged In user
							AccountUser loggedInUser  = enrollmentService.getLoggedInUser();
							disEnrollmentDTO.setUpdatedBy(loggedInUser);
							
							enrollmentList = enrollmentService.disEnrollEnrollment(disEnrollmentDTO, EnrollmentEvent.TRANSACTION_IDENTIFIER.EMPLOYEE_DISENROLL);
							
							enrollmentEmailNotificationService.sendEnrollmentStatusNotification(enrollmentList,(String) disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE));
							LOGGER.info("dis-enrollment has been done successfully...!");
							if (enrollmentList != null && !enrollmentList.isEmpty()) {
								enrollmentIdAndtermDateMap= new HashMap<Integer, String>();
								for(Enrollment enrollment : enrollmentList){
									if(enrollment.getBenefitEndDate()!=null){
										enrollmentIdAndtermDateMap.put(enrollment.getId(), DateUtil.dateToString(enrollment.getBenefitEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
									}
									
								}
							}
							enrlResp.setEnrollmentIdAndtermDateMap(enrollmentIdAndtermDateMap);
							
							
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
							enrlResp.setErrMsg(responseText);
							enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
						}else{
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_207);
							enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ACTIVE_ENROLLMENT_FOUND_FOR_THE_GIVEN_EMP_APP_ID);
							enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS); /** Changed response to SUCCESS as requested by SHOP Team **/
							return xstream.toXML(enrlResp);
						}
						
					} catch (GIException ge) {
						LOGGER.error(EnrollmentConstants.ERR_MSG_DIS_ENROLLMENT_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause() , ge);
						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_206);
						enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DIS_ENROLLMENT_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause());
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
			}
		}
		return xstream.toXML(enrlResp);
	}

	/**
	 * @author negi_s
	 * 
	 *         This method is called to disEnroll an Enrollee by passing
	 *         parameters as EnrollmentRequest and return the success or failure
	 *         message in the form of EnrollmentResponse. Excepts EnrollmentDisEnrollmentDTO from the
	 *         EnrollmentRequest with following fields SsapApplicationID, EmployeeAppID, TerminationDate
	 *         , TerminationReasonCode , DeathDate 200 error key is success
	 *         message. 201 is error key for input key EmployeeID. 202 is error
	 *         key for input key TerminationDate. 203 is error key for input key
	 *         TerminationDate Format. 204 is error key for input key
	 *         TerminationReasonCode. 205 is error key for input key DeathDate.
	 *         206 is error key for dis-enrollment service error/exception.
	 */
	
	@RequestMapping(value = "/disenrollbyapplicationID", method = RequestMethod.POST)
	@ResponseBody
	public String disenrollbyApplicationID(@RequestBody String enrollmentRequestStr) {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		Long ssapApplicationId = null;
		String responseText = null;
		List<Enrollment> enrollmentList = null;
		List<Integer> employeeAppIdList = null;
		boolean sentNotification = false;
		if (enrollmentRequest != null) {
			EnrollmentDisEnrollmentDTO disEnrollmentDTO = enrollmentRequest.getEnrollmentDisEnrollmentDTO();
	
			if(isNotNullAndEmpty(disEnrollmentDTO)){
				LOGGER.info("REQUEST MAP Text disenrollbyApplicationID :" + disEnrollmentDTO.toString());
			}
			if (disEnrollmentDTO != null) {
				if (!isNotNullAndEmpty(disEnrollmentDTO.getEmployeeAppIdList()) && !isNotNullAndEmpty(disEnrollmentDTO.getSsapApplicationid())) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYEE_APP_ID_AND_SSAP_APP_ID_IS_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else if (!isNotNullAndEmpty(disEnrollmentDTO.getTerminationDate())) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_DATE_IS_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (!(DateUtil.isValidDate(disEnrollmentDTO.getTerminationDate() ,GhixConstants.REQUIRED_DATE_FORMAT))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_DATE_FORMAT_ERROR);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (!isNotNullAndEmpty(disEnrollmentDTO.getTerminationReasonCode())) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_TERMINATION_REASON_CODE_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (isNotNullAndEmpty(disEnrollmentDTO.getTerminationReasonCode())
						&& (disEnrollmentDTO.getTerminationReasonCode().equals(EnrollmentConstants.REASON_CODE_DEATH))
						&& (!isNotNullAndEmpty(disEnrollmentDTO.getDeathDate()))) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_205);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DEATH_DATE_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else {
					try {
						ssapApplicationId = disEnrollmentDTO.getSsapApplicationid();
						employeeAppIdList = disEnrollmentDTO.getEmployeeAppIdList();

						if(isNotNullAndEmpty(employeeAppIdList) && !employeeAppIdList.isEmpty() ){
							enrollmentList = enrollmentService.findActiveEnrollmentByEmployeeApplicationID(employeeAppIdList,DateUtil.StringToDate(disEnrollmentDTO.getTerminationDate() ,GhixConstants.REQUIRED_DATE_FORMAT));
							sentNotification = true;
						}else{
							if(disEnrollmentDTO.getPlanType() != null && ssapApplicationId != null) {
								String planType = disEnrollmentDTO.getPlanType();
								enrollmentList = enrollmentService.findActiveEnrollmentBySsapApplicationIdAndPlanType(ssapApplicationId,planType);
								}
								else {
								enrollmentList = enrollmentService.findActiveEnrollmentBySsapApplicationID(ssapApplicationId);
								}
						}
						if (enrollmentList != null && !enrollmentList.isEmpty()) {
							//termination date same year check applicable only for individual exchange
							if(null == employeeAppIdList || employeeAppIdList.isEmpty()) {
								if(!EnrollmentUtils.isSameYear(enrollmentList.get(0).getBenefitEffectiveDate(), EnrollmentUtils.StringToEODDate(disEnrollmentDTO.getTerminationDate(),GhixConstants.REQUIRED_DATE_FORMAT))) {
									enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_208);
									enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
									enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
									return xstream.toXML(enrlResp);
								}
							}
							disEnrollmentDTO.setEnrollmentList(enrollmentList);
							
							// Set Updated by as Logged In user
							AccountUser loggedInUser  = enrollmentService.getLoggedInUser();
							disEnrollmentDTO.setUpdatedBy(loggedInUser);
							disEnrollmentDTO.setAllowRetroTermination(true);
							if(EnrollmentConfiguration.isCaCall()) {
								disEnrollmentDTO.setInd56DisenrollmentCall(true);
							}
							// Call Dis Enrollment
							enrollmentList = enrollmentService.disEnrollEnrollment(disEnrollmentDTO, EnrollmentEvent.TRANSACTION_IDENTIFIER.APPLICATION_DISENROLL);
							
							// Send Email Notification for SHOP Flow only
							if(sentNotification){
								enrollmentEmailNotificationService.sendEnrollmentStatusNotification(enrollmentList,disEnrollmentDTO.getTerminationReasonCode());	
							}
							/**
							 * Logging ENROLLMENT_STATUS_UPDATE application event
							 */
							for(Enrollment enrollment : enrollmentList)
							{
								enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, isNotNullAndEmpty(enrollment.getAppEventReason())? enrollment.getAppEventReason() :
									EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString(), loggedInUser);
							}
							
							LOGGER.info("dis-enrollment has been done successfully...!");
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
							enrlResp.setErrMsg(responseText);
							enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
						}else{
							LOGGER.warn(EnrollmentConstants.ERR_MSG_ENROLLMENT_LIST_IS_NULL_OR_EMPTY);
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_207);
							enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ACTIVE_ENROLLMENT_FOUND_FOR_THE_GIVEN_SSAP_APP_ID);
							if(sentNotification){
								enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ACTIVE_ENROLLMENT_FOUND_FOR_THE_GIVEN_EMP_APP_ID);
							}
							
							enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
						}
					} catch (GIException ge) {
						LOGGER.error(EnrollmentConstants.ERR_MSG_DIS_ENROLLMENT_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause() , ge);
						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_206);
						enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DIS_ENROLLMENT_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause());
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
			}
		}
		return xstream.toXML(enrlResp);
	}

	/**
	 * * @author raja
	 * 
	 * This method is called to update shop enrollment by passing parameters as
	 * EnrollmentRequest and return the success or failure message in the form
	 * of EnrollmentResponse. Excepts loggedInUserName, employerId from the
	 * EnrollmentRequest 200 key is success message. 201 is error key for input
	 * enrollmentStatus is null. 202 is error key for input key employerId is
	 * null. 203 is error key for "Error in updating the shop enrollment".
	 */

	@RequestMapping(value = "/updateshopenrollmentstatus", method = { RequestMethod.POST })
	@ResponseBody
	public String updateShopEnrollmentStatus(@RequestBody String enrollmentRequestStr) {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
		if (enrollmentRequest.getEnrollmentStatus() == null) {
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_STATUS_IS_NULL);
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		} else if (enrollmentRequest.getEmployerEnrollmentId() == null) {
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_IS_NULL);
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		} else {
			try {
				enrlResp = enrollmentService.updateShopEnrollmentStatus(enrollmentRequest);
				if(isNotNullAndEmpty(enrlResp.getStatus()) && enrlResp.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrlResp.setErrMsg(EnrollmentConstants.MSG_SHOP_ENROLLMENT_UPDATE_SUCCESS);
					LOGGER.info(EnrollmentConstants.MSG_SHOP_ENROLLMENT_UPDATE_SUCCESS);
				}
			} catch (Exception e) {
				LOGGER.error(EnrollmentConstants.ERR_MSG_SHOP_ENROLLMENT_UPDATE_FAIL + " :: " + e.getMessage() + EnrollmentConstants.CAUSE + e.getCause() , e);
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_SHOP_ENROLLMENT_UPDATE_FAIL + e.getMessage() + EnrollmentConstants.CAUSE + e.getCause());
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		return xstream.toXML(enrlResp);
	}

	/**
	 * * @author raja
	 * 
	 * This method is called to fetch the planId, ind_order_items_id from
	 * enrollment table by passing parameter as EnrollmentRequest and return the
	 * success or failure message in the form of EnrollmentResponse. Excepts
	 * enrollmentId from the EnrollmentRequest 200 key is success message. 201
	 * is error key for input key enrollmentId. 202 is error key for input key
	 * "Error in fetching the enrollment".
	 */

	@RequestMapping(value = "/getplanidandorderitemidbyenrollmentid", method = RequestMethod.POST)
	@ResponseBody
	public String getPlanIdAndOrderItemIdByEnrollmentId(@RequestBody String enrollmentRequestStr)
	{
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		try
		{
			EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
			if(enrollmentRequest == null){
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				Integer enrollmentId = enrollmentRequest.getEnrollmentId();
				if (enrollmentId == null || enrollmentId == EnrollmentConstants.ZERO) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				} 
				else{
					Map<String, Object> enrollmentMap = enrollmentService.getPlanIdAndOrderItemIdByEnrollmentId(enrollmentId);
					
					enrlResp.setPlanId((Integer) enrollmentMap.get(EnrollmentConstants.PLAN_ID));
					enrlResp.setOrderItemId((Integer) enrollmentMap.get(EnrollmentConstants.ORDER_ITEM_ID));
					if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.CMS_PLAN_ID))){
						enrlResp.setCmsPlanId((String)enrollmentMap.get(EnrollmentConstants.CMS_PLAN_ID));
					}
					enrlResp.setSubscriberMemberId((String)enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_MEMBER_ID));
					if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.APTC))){
						enrlResp.setAptcAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.APTC).toString()));	
					}
					if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.STATE_SUBSIDY))){
						enrlResp.setStateSubsidyAmt(new BigDecimal(enrollmentMap.get(EnrollmentConstants.STATE_SUBSIDY).toString()));
					}
					if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.GROSS_PREMIUM_AMT))){
						enrlResp.setGrossPremiumAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.GROSS_PREMIUM_AMT).toString()));	
					}
					if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.NET_PREMIUM_AMT))){
						enrlResp.setNetPremiumAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.NET_PREMIUM_AMT).toString()));	
					}
					
					/**
					 * @since 06th August 2015
					 * Jira Id: HIX-73359
					 * Description: Pass subscriber zip and county in getplanidandorderitemidbyenrollmentid API
					 */
					if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_HOME_ADD_ZIP))){
						enrlResp.setSubscriberZip((String)enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_HOME_ADD_ZIP));	
					}
					
					if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_HOME_ADD_COUNTYCODE))){
						enrlResp.setSubscriberCountyCode((String)enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_HOME_ADD_COUNTYCODE));	
					}
					/**
					 * @since 14th May 2018 
					 * Jira HIX-107504
					 * 
					 */
					if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.PLAN_LEVEL))){
						enrlResp.setPlanLevel((String)enrollmentMap.get(EnrollmentConstants.PLAN_LEVEL));	
					}
					
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrlResp.setErrMsg(EnrollmentConstants.MSG_RETRIEVE_ENROLLMENT_SUCCESS);
					enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
					LOGGER.info(EnrollmentConstants.MSG_RETRIEVE_ENROLLMENT_SUCCESS);
				}
			}
		} catch (Exception e) {
			LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL
					+ " :: "
					+ e.getMessage()
					+ EnrollmentConstants.CAUSE
					+ e.getCause() , e);
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL
					+ e.getMessage() + EnrollmentConstants.CAUSE + e.getCause());
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return xstream.toXML(enrlResp);
	}
	

	/**
	 * new Json API for getplanidandorderitemidbyenrollmentid 
	 * Jira Id: HIX-90479
	 * @param enrollmentRequestStr
	 * @return
	 */
	@RequestMapping(value = "/getplanidandorderitemidbyenrollmentidjson", method = RequestMethod.POST)
	@ResponseBody
	public String getPlanIdAndOrderItemIdByEnrollmentIdJson(@RequestBody String enrollmentRequestStr)
	{
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		if(StringUtils.isNotBlank(enrollmentRequestStr)){
			try
			{
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(enrollmentRequestStr, EnrollmentRequest.class);
				if(enrollmentRequest == null){
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				else{
					Integer enrollmentId = enrollmentRequest.getEnrollmentId();
					if (enrollmentId == null || enrollmentId == EnrollmentConstants.ZERO) {
						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
						enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL);
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
					} 
					else{
						Map<String, Object> enrollmentMap = enrollmentService.getPlanIdAndOrderItemIdByEnrollmentId(enrollmentId);

						enrlResp.setPlanId((Integer) enrollmentMap.get(EnrollmentConstants.PLAN_ID));
						enrlResp.setOrderItemId((Integer) enrollmentMap.get(EnrollmentConstants.ORDER_ITEM_ID));
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.CMS_PLAN_ID))){
							enrlResp.setCmsPlanId((String)enrollmentMap.get(EnrollmentConstants.CMS_PLAN_ID));
						}
						enrlResp.setSubscriberMemberId((String)enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_MEMBER_ID));
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.APTC))){
							enrlResp.setAptcAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.APTC).toString()));	
						}
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.STATE_SUBSIDY))){
							enrlResp.setStateSubsidyAmt(new BigDecimal(enrollmentMap.get(EnrollmentConstants.STATE_SUBSIDY).toString()));
						}
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.GROSS_PREMIUM_AMT))){
							enrlResp.setGrossPremiumAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.GROSS_PREMIUM_AMT).toString()));	
						}
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.NET_PREMIUM_AMT))){
							enrlResp.setNetPremiumAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.NET_PREMIUM_AMT).toString()));	
						}

						/**
						 * @since 06th August 2015
						 * Jira Id: HIX-73359
						 * Description: Pass subscriber zip and county in getplanidandorderitemidbyenrollmentid API
						 */
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_HOME_ADD_ZIP))){
							enrlResp.setSubscriberZip((String)enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_HOME_ADD_ZIP));	
						}

						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_HOME_ADD_COUNTYCODE))){
							enrlResp.setSubscriberCountyCode((String)enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_HOME_ADD_COUNTYCODE));	
						}
						
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.CHANGE_PLAN_ALLOWED))){
							enrlResp.setChangePlanAllowed((String)enrollmentMap.get(EnrollmentConstants.CHANGE_PLAN_ALLOWED));	
						}
						
						/**
						 * @since 14th May 2018 
						 * Jira HIX-107504
						 * 
						 */
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.PLAN_LEVEL))){
							enrlResp.setPlanLevel((String)enrollmentMap.get(EnrollmentConstants.PLAN_LEVEL));	
						}
						
						List<EnrolleeDTO> memberDataList = new ArrayList<EnrolleeDTO>();
						List<Object[]> enrolleeAgeData = ((List<Object[]>) enrollmentMap.get(EnrollmentConstants.ENROLLEE_AGE_AND_MEMBER_DATA));
						if (enrolleeAgeData != null) {
							EnrolleeDTO enrolleeAgeAndMemberData = null;
							for (Object[] memberIdAndAge : enrolleeAgeData) {
								enrolleeAgeAndMemberData = new EnrolleeDTO();
								if (memberIdAndAge.length > 1 && memberIdAndAge[0] != null && memberIdAndAge[1] != null) {
									enrolleeAgeAndMemberData.setExchgIndivIdentifier((String) memberIdAndAge[0]);
									if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
													EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGE_BASED_REQUOTING))) {//HIX-105932
										enrolleeAgeAndMemberData.setAge((Integer) memberIdAndAge[1]);
									}
									 
								}else if (memberIdAndAge[0] != null ) {
									enrolleeAgeAndMemberData.setExchgIndivIdentifier((String) memberIdAndAge[0]);
									enrolleeAgeAndMemberData.setAge(null);
								}
								memberDataList.add(enrolleeAgeAndMemberData);
							}
							
							enrlResp.setEnrolleeAgeAndId(memberDataList);
						}
						if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.MAX_APTC))){
						enrlResp.setMaxAptc(Float.valueOf(enrollmentMap.get(EnrollmentConstants.MAX_APTC).toString()));
						}
						/*if(!memberDataList.isEmpty()){
							enrlResp.setEnrolleeAgeAndId(memberDataList);
						}*/

						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						enrlResp.setErrMsg(EnrollmentConstants.MSG_RETRIEVE_ENROLLMENT_SUCCESS);
						enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
						LOGGER.info(EnrollmentConstants.MSG_RETRIEVE_ENROLLMENT_SUCCESS);
					}
				}
			}
			catch (Exception e) {
				LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL
						+ " :: "
						+ e.getMessage()
						+ EnrollmentConstants.CAUSE
						+ e.getCause() , e);
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL
						+ e.getMessage() + EnrollmentConstants.CAUSE + e.getCause());
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		else{
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrlResp);
	}
	
	

	/**
	 * * @author raja
	 * 
	 * This method is called to dis-enroll the employer by passing parameters as
	 * EmployerDisEnrollmentRequest from the EnrollmentRequest object and return
	 * the success or failure message in the form of
	 * EmployerDisEnrollmentResponse as String.
	 */

	@RequestMapping(value = "/employerdisenrollment", method = RequestMethod.POST)
	@ResponseBody
	public String employerDisEnrollment(@RequestBody String enrollmentRequestStr) throws GIException {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		if(enrollmentRequestStr==null ){
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			return xstream.toXML(enrollmentResponse);
		}
		
		try {
			
			EmployerDisEnrollmentResponse employerDisEnrollmentResponse = new EmployerDisEnrollmentResponse();
			EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
			EmployerDisEnrollmentRequest employerDisEnrollmentRequest = enrollmentRequest.getEmployerDisEnrollmentRequest();
			
			if(employerDisEnrollmentRequest==null ){
				LOGGER.info("============= employerDisEnrollmentRequest is null =============");
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				return xstream.toXML(enrollmentResponse);
			}
			LOGGER.info("employerDisEnrollmentRequest : ===============" + xstream.toXML(employerDisEnrollmentRequest));
			
			LOGGER.info("============= START of IND 29 REST CALL: Received employerDisEnrollmentRequest  details : =============");
			employerDisEnrollmentResponse = enrollmentEmployerDisEnrollService.employerDisEnrollment(employerDisEnrollmentRequest, true);
			LOGGER.info("============= End of IND 29 REST CALL :EmployerDisEnrollment process ended Succesfully  : =============");
			
			if(employerDisEnrollmentRequest.getTerminationDate()==null){
				if(employerDisEnrollmentRequest.getEmployerEnrollmentId() == null){
					LOGGER.info("Employer Enrollment Id is null");
				}else{
					String earliestEffStartDate= employerDisEnrollmentResponse.getEarliestEffStartDate();
					if(earliestEffStartDate!=null){
						enrollmentResponse.setEarliestEffStartDate(earliestEffStartDate);
					}
				}
			}
			
			if(isNotNullAndEmpty(employerDisEnrollmentResponse.getResponseCode()) && employerDisEnrollmentResponse.getResponseCode().equalsIgnoreCase("200")){
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			enrollmentResponse.setErrCode(Integer.parseInt(employerDisEnrollmentResponse.getResponseCode()));
			enrollmentResponse.setErrMsg(employerDisEnrollmentResponse.getResponseDescription());
		} catch (GIException e) {
			LOGGER.error("GIException occured while processing IND 29 REST CALL:  Employer Disenrollment  ");
			LOGGER.error("Error Message : " + e.getErrorMsg());
			LOGGER.error("Error Code : " + e.getErrorCode());
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(e.getErrorCode());
			enrollmentResponse.setErrMsg(e.getErrorMsg());
			LOGGER.error(" ", e);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing IND 29 REST CALL : Employer Disenrollment");
			LOGGER.error("Error Message : " + ex.getMessage());
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			enrollmentResponse.setErrMsg(" Error occured while processing IND 29 REST CALL ( Employer Disenrollment)");
			LOGGER.error(" ", ex);
		}
		return xstream.toXML(enrollmentResponse);
	}


	@RequestMapping(value = "/findEnrollmentPlanLevelCountByBroker", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentPlanLevelCountByBroker(
			@RequestBody String enrollmentRequestStr) {
		List<EnrollmentCountDTO> enrollmentCountDTOList = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();

		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
		Integer assisterBrokerId = enrollmentRequest.getAssisterBrokerId();

		if (assisterBrokerId == null || assisterBrokerId == 0) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg("Broker Id is Null or 0");
			LOGGER.warn("Broker Id is Null or 0");
		} else if (!isNotNullAndEmpty(enrollmentRequest.getStartDate())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg("Start Date is Null or Empty");
			LOGGER.warn("Start Date is Null or Empty");
		} else if (!isNotNullAndEmpty(enrollmentRequest.getEndDate())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			enrollmentResponse.setErrMsg("End Date is Null or Empty");
			LOGGER.warn("End Date is Null or Empty");
		} else if (!isNotNullAndEmpty(enrollmentRequest.getRole())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentResponse.setErrMsg("Role is Null or Empty");
			LOGGER.warn("Role is Null or Empty");
		} else {
			Date startDate = DateUtil.StringToDate(enrollmentRequest.getStartDate(),GhixConstants.REQUIRED_DATE_FORMAT);
			Date endDate = DateUtil.StringToDate(enrollmentRequest.getEndDate(),GhixConstants.REQUIRED_DATE_FORMAT);
			String role = enrollmentRequest.getRole().toString().toUpperCase();

			enrollmentCountDTOList = enrollmentService.findEnrollmentPlanLevelCountByBroker(assisterBrokerId,role, startDate, endDate);
			enrollmentResponse.setEnrollmentCountDTO(enrollmentCountDTOList);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			enrollmentResponse.setErrMsg(GhixConstants.RESPONSE_SUCCESS);
		}
		return xstream.toXML(enrollmentResponse);
	}

	/**
	 * @author Sharma_k
	 * @since 25th August 2013
	 * @param id
	 * @return Rest controller call added to verify whether enrollment exists
	 *         for IND42, Instead of loading the enrollment
	 */
	@RequestMapping(value = "/isenrollmentexists/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String isEnrollmentExists(@PathVariable("id") String id) {
		LOGGER.info("============inside isEnrollmentExists============");

		LOGGER.info("Received enrollment id : " + id);
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (id == null || id.equalsIgnoreCase("0")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		} else {
			Integer enrollmentId = Integer.valueOf(id);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setIsEnrollmentExists(enrollmentService.isEnrollmentExists(enrollmentId));
		}

		XStream xstream = GhixUtils.getXStreamStaxObject();
		return xstream.toXML(enrollmentResponse);
	}

	/**
	 * @author shanbhag_a
	 * 
	 * Returns list of Enrollment for provided CMR_Household_Id. Also
	 * returns Enrollment_Plan info for corresponding plan id of
	 * enrollment
	 * 
	 * @param id
	 * @return
	 */

	@RequestMapping(value = "/findEnrollmentsByCmrHouseholdId/{cmrHouseHoldId}", method = RequestMethod.GET)
	@ResponseBody
	public String findEnrollmentByCMRHouseholdId(
			@PathVariable("cmrHouseHoldId") String cmrHouseHoldId) {
		LOGGER.info("============inside findEnrollmentByCMRHouseholdId============");
		LOGGER.info("Received cmr household id : " + cmrHouseHoldId);
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (isNotNullAndEmpty(cmrHouseHoldId)
				&& !cmrHouseHoldId.equalsIgnoreCase("0")) {
			List<Enrollment> enrollments = enrollmentService.findEnrollmentByCmrHouseholdId(Integer.parseInt(cmrHouseHoldId));
			enrollmentResponse.setEnrollmentList(enrollments);

			for (Enrollment enrollment : enrollments) {
				enrollment.setEnrollmentStatusLabel(enrollment.getEnrollmentStatusLkp().getLookupValueLabel());

				StringBuilder enrolleeNames = new StringBuilder();
				for (Enrollee enrollee : enrollment.getEnrollees()) {
					if (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)|| enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)) {
						if (isNotNullAndEmpty(enrollee.getFirstName())) {
							enrolleeNames.append(enrollee.getFirstName());
							enrolleeNames.append(" ");
						}
						if (isNotNullAndEmpty(enrollee.getLastName())) {
							enrolleeNames.append(enrollee.getLastName());
						}
						enrolleeNames.append(", ");
					}
				}
				if (enrolleeNames.length() > EnrollmentConstants.TWO) {
					enrolleeNames.delete(enrolleeNames.length() - EnrollmentConstants.TWO,enrolleeNames.length());
				}
				enrollment.setEnrolleeNames(enrolleeNames.toString());
			}

			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} else {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		return xstream.toXML(enrollmentResponse);
	}

	/**
	 * @author Sharma_k
	 * @since 28th Sept 2013
	 * @param id
	 * @return Rest call to return the Remittance report generation related data
	 *         by enrollment id
	 */
	@RequestMapping(value = "/getremittancedatabyenrollmentid/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getRemittanceDataByEnrollmentId(@PathVariable("id") String id) {
		LOGGER.info("============inside getRemittanceDataByEnrollmentId ============");

		LOGGER.info("Enrollment id received : " + id);
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (id == null || id.equalsIgnoreCase("0")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse
					.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
		} else {
			Integer enrollmentId = Integer.valueOf(id);
			List<Object[]> enrollmentRemittanceList = enrollmentService
					.getRemittanceDataByEnrollmentId(enrollmentId);

			if (enrollmentRemittanceList != null
					&& !enrollmentRemittanceList.isEmpty()) {
				EnrollmentRemittanceDTO enrollmentRemittanceDto = new EnrollmentRemittanceDTO();
				for (Object[] enrollColData : enrollmentRemittanceList) {
					enrollmentRemittanceDto
							.setEnrollmentId((Integer) enrollColData[EnrollmentConstants.ZERO]);
					enrollmentRemittanceDto
							.setIssuerSubscriberIdentifier((String) enrollColData[EnrollmentConstants.ONE]);
					enrollmentRemittanceDto
							.setGroupPolicyNumber((String) enrollColData[EnrollmentConstants.TWO]);
					enrollmentRemittanceDto
							.setCMSPlanID((String) enrollColData[EnrollmentConstants.THREE]);
					enrollmentRemittanceDto
							.setExchgSubscriberIdentifier((String) enrollColData[EnrollmentConstants.FOUR]);
					enrollmentRemittanceDto
							.setFirstName((String) enrollColData[EnrollmentConstants.FIVE]);
					enrollmentRemittanceDto
							.setLastName((String) enrollColData[EnrollmentConstants.SIX]);
					enrollmentRemittanceDto
							.setMiddleName((String) enrollColData[EnrollmentConstants.SEVEN]);
					enrollmentRemittanceDto
							.setSuffix((String) enrollColData[EnrollmentConstants.EIGHT]);
					if(enrollColData[EnrollmentConstants.NINE] != null){
						enrollmentRemittanceDto.setIndivExchAssignedPolicyNum(enrollColData[EnrollmentConstants.NINE].toString());
					}
					enrollmentRemittanceDto.setHealthCoveragePolicyNo((String) enrollColData[EnrollmentConstants.TEN]);
				}
				enrollmentResponse
						.setEnrollmentRemittanceDto(enrollmentRemittanceDto);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse
						.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			} else {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		return xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * @param id
	 * @return Rest call to return the Enrollee Coverage Start Data
	 *         by enrollment id
	 */
	@RequestMapping(value = "/getenrolleecoveragestartdatabyenrollmentid/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getEnrolleeExchgIndivIdentifierAndCoverageStartDateByEnrollmentId(@PathVariable("id") String id) {
		LOGGER.info("Enrollment id received : " + id);
		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (!isNotNullAndEmpty(id)) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
		} else {
			Integer enrollmentId = Integer.valueOf(id);
			List<Enrollee> enrolleeList = enrolleeService.getEnrolleeExchgIndivIdentifierAndCoverageStartDateByEnrollmentId(enrollmentId);
			enrollmentResponse.setIsEnrollmentExists(true);
			enrollmentResponse.setEnrolleeList(enrolleeList);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			}
		
		XStream xstream = GhixUtils.getXStreamStaxObject();

		return xstream.toXML(enrollmentResponse);
	}
	@RequestMapping(value = "/getEnrollmentPaymentInfoByID/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getEnrollmentPaymentInfoByID(
			@RequestBody 
			@PathVariable("id") String id) {

		XStream xstream = GhixUtils.getXStreamStaxObject();		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		Enrollment enrollment = null;
		EnrollmentPaymentDTO enrollmentPaymentDTO = null;
		try {
			Integer enrollmentId = Integer.valueOf(id);
			enrollment = enrollmentService.findById(enrollmentId);
			if(enrollment != null){
				enrollmentPaymentDTO = populateEnrollmentPaymentDTO(enrollment);
				if(enrollmentPaymentDTO != null){
					enrollmentResponse.setEnrollmentPaymentDTO(enrollmentPaymentDTO);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
			}
					
		} catch (Exception e) {
			LOGGER.info("No enrollments found for the issuer search criteria."
					+ e.getMessage() , e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return xstream.toXML(enrollmentResponse);
	}
    
	private EnrollmentPaymentDTO populateEnrollmentPaymentDTO(Enrollment enrollmentObj){
		Boolean isCaCall = EnrollmentConstants.STATE_CODE_CA.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode());
		EnrollmentPaymentDTO enrlPaymentObj = new EnrollmentPaymentDTO();
		enrlPaymentObj.setCmsPlanId(enrollmentObj.getCMSPlanID());
		enrlPaymentObj.setGrossPremiumAmt(enrollmentObj.getGrossPremiumAmt());
		enrlPaymentObj.setNetPremiumAmt(enrollmentObj.getNetPremiumAmt());
		enrlPaymentObj.setAptcAmt(enrollmentObj.getAptcAmt());
		if (enrollmentObj.getInsuranceTypeLkp() != null) {
			enrlPaymentObj.setInsuranceType(enrollmentObj.getInsuranceTypeLkp().getLookupValueLabel());
		}
		if (enrollmentObj.getExchgSubscriberIdentifier() != null) {
			enrlPaymentObj.setExchgSubscriberIdentifier(enrollmentObj.getExchgSubscriberIdentifier());
		}
		enrlPaymentObj.setBenefitEffectiveDate(enrollmentObj.getBenefitEffectiveDate());
		enrlPaymentObj.setEnrollmentId(enrollmentObj.getId());
		//HIX-59026
		enrlPaymentObj.setPaymentTxnId(enrollmentObj.getPaymentTxnId());
		StringBuffer enrolleeNamesStr = new StringBuffer();
		Enrollee subscriber = null;
		Enrollee responsiblePerson = null;
		Location addr = null;

		List<EnrolleePaymentDTO> enrolleePaymentList = new ArrayList<>();
		List<Enrollee> enrolleeList = enrollmentObj.getEnrollees();
		//		if (enrolleeList != null && enrolleeList.size() > 0) {
		if (isNotNullAndEmpty(enrolleeList)) {

			for (Enrollee enrolleeObj : enrolleeList) {
				if(enrolleeObj.getPersonTypeLkp() != null) {
					String personType =  enrolleeObj.getPersonTypeLkp().getLookupValueCode();
					if(personType.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) || personType.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)) {
						EnrolleePaymentDTO enrolleePaymentObj = new EnrolleePaymentDTO();
						enrolleePaymentObj.setFirstName(enrolleeObj.getFirstName());
						enrolleePaymentObj.setMiddleName(enrolleeObj.getMiddleName());
						enrolleePaymentObj.setLastName(enrolleeObj.getLastName());
						enrolleePaymentObj.setAge(EnrollmentUtils.getEnrolleeAge(enrolleeObj));
						enrolleePaymentObj.setMemberId(enrolleeObj.getExchgIndivIdentifier());
						enrolleePaymentObj.setTotalIndvResponsibilityAmt(enrolleeObj.getTotalIndvResponsibilityAmt());
						enrolleePaymentObj.setRelationshipCode( null != enrolleeObj.getRelationshipToHCPLkp() ? enrolleeObj.getRelationshipToHCPLkp().getLookupValueCode() : null);
						enrolleePaymentList.add(enrolleePaymentObj);

						if (personType.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)) {
							subscriber = enrolleeObj;
							if(!isCaCall) {
								enrlPaymentObj.setFirstName(enrolleeObj.getFirstName());
								enrlPaymentObj.setMiddleName(enrolleeObj.getMiddleName());
								enrlPaymentObj.setLastName(enrolleeObj.getLastName());
							}
							if(null == enrlPaymentObj.getExchgSubscriberIdentifier()) {
								enrlPaymentObj.setExchgSubscriberIdentifier(enrolleeObj.getExchgIndivIdentifier());
							}
						}else if (personType.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)) {
							enrolleeNamesStr.append(enrolleeObj.getFirstName() + "-" + enrolleeObj.getLastName() + ";");
						}
					}else if (personType.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON)) {
						if(isCaCall) {
							enrlPaymentObj.setFirstName(enrolleeObj.getFirstName());
							enrlPaymentObj.setMiddleName(enrolleeObj.getMiddleName());
							enrlPaymentObj.setLastName(enrolleeObj.getLastName());
						}
						responsiblePerson = enrolleeObj;


					} // end of else
				}// end of else
			} // end of enrollee loop
		} // end of enrollees check


		// HIX-53831
		if (responsiblePerson != null && responsiblePerson.getMailingAddressId() != null) {
			addr = responsiblePerson.getMailingAddressId();

		} else if (subscriber != null && subscriber.getMailingAddressId() != null) {
			addr = subscriber.getMailingAddressId();

		} else if (responsiblePerson != null && responsiblePerson.getHomeAddressid() != null) {
			addr = responsiblePerson.getHomeAddressid();
		}

		if (addr != null) {
			enrlPaymentObj.setAddressLine1(addr.getAddress1());
			enrlPaymentObj.setAddressLine2(addr.getAddress2());
			enrlPaymentObj.setState(addr.getState());
			enrlPaymentObj.setZip(addr.getZip());
			enrlPaymentObj.setCity(addr.getCity());
			enrlPaymentObj.setCountyCode(isCaCall ? addr.getCountycode() : null);
		} else {
			LOGGER.warn("Error in EnrollmentWebController: populateEnrollmentPaymentDTO, no address found for Enrollment ID :" + enrollmentObj.getId());
		}

		// Same for Email

		if (responsiblePerson != null && responsiblePerson.getPreferredEmail() != null) {
			enrlPaymentObj.setContactEmail(responsiblePerson.getPreferredEmail());
			enrlPaymentObj.setPrefferedEmail(responsiblePerson.getPreferredEmail());
		}else if (subscriber != null && subscriber.getPreferredEmail() != null) {
			enrlPaymentObj.setContactEmail(subscriber.getPreferredEmail());
			enrlPaymentObj.setPrefferedEmail(subscriber.getPreferredEmail());
		}

		//HIX-58646
		if(null != responsiblePerson){
			enrlPaymentObj.setPhoneNumber(responsiblePerson.getPrimaryPhoneNo());	
		}

		enrlPaymentObj.setEnrolleeNames(enrolleeNamesStr.toString());

		//Issuer issuer = iEnrIssuerRepository.getIssuerIdByIssuerId(enrollmentObj.getIssuerId());
		SingleIssuerResponse singleIssuerResponse  = enrollmentExternalRestUtil.getIssuerInfoById(enrollmentObj.getIssuerId());
		if(singleIssuerResponse!=null) {
			enrlPaymentObj.setPaymentUrl(singleIssuerResponse.getPaymentUrl());
		}
		enrlPaymentObj.setHiosIssuerId(enrollmentObj.getHiosIssuerId());
		enrlPaymentObj.setName(enrollmentObj.getInsurerName());

		if(isCaCall) {
			String languageCode = null;
			enrlPaymentObj.setCaseId(enrollmentObj.getExternalHouseHoldCaseId());
			if (responsiblePerson != null && responsiblePerson.getLanguageLkp() != null) {
				languageCode = responsiblePerson.getLanguageLkp().getLookupValueCode();
			}else if (subscriber != null && subscriber.getLanguageLkp() != null) {
				languageCode = subscriber.getLanguageLkp().getLookupValueCode();
			}
			enrlPaymentObj.setPreferredLanguage(null != languageCode ? EnrollmentConstants.LOCALE_MAP.get(languageCode.trim()): "en");
			if(!EnrollmentUtils.isNotNullAndEmpty(enrlPaymentObj.getCountyCode())){
				String countyCode = null;
				if (null != responsiblePerson.getHomeAddressid() && null != responsiblePerson.getHomeAddressid().getCountycode()) {
					countyCode = responsiblePerson.getHomeAddressid().getCountycode();
				} else if (null != subscriber.getMailingAddressId()	&& null != subscriber.getMailingAddressId().getCountycode()) {
					countyCode = subscriber.getMailingAddressId().getCountycode();
				} else if (null != subscriber.getHomeAddressid() && null != subscriber.getHomeAddressid().getCountycode()) {
					countyCode = subscriber.getHomeAddressid().getCountycode();
				}
				enrlPaymentObj.setCountyCode(countyCode);
			}
			enrlPaymentObj.setEnrolleePaymentDtoList(enrolleePaymentList);
		}
		return enrlPaymentObj;
	}
	 
	/**
	 * @author panda_p
	 * @since 27-Feb-2014
	 * 
	 * 
	 * @param enrollmentRequestStr
	 * @return
	 */
	@RequestMapping(value = "/reinstateenrollment", method = RequestMethod.POST)
	@ResponseBody
	public String reinstateEnrollment(@RequestBody String enrollmentRequestStr){
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		AccountUser userObj = null;

		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
		EnrollmentReinstatementRequest enrollmentReinstatementRequest = enrollmentRequest.getEnrollmentReinstatementRequest();

		if(enrollmentReinstatementRequest == null){
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_206);
			enrlResp.setErrMsg("enrollmentReinstatementRequest is null");
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		//		else if(enrollmentReinstatementRequest.getEmployeeId()==null || enrollmentReinstatementRequest.getEmployeeId().equalsIgnoreCase("")){
		//			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_207);
		//			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYEE_ID_IS_NULL_OR_EMPTY);
		//			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		//			// Null Check not required for JAXB generated RequestObject
		//		}else if(enrollmentReinstatementRequest.getEmployeeAppIdList().isEmpty()){
		//			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_208);
		//			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYEE_APP_ID_IS_NULL_OR_EMPTY);
		//			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		//		}
		else{
			try{
				userObj = userService.getLoggedInUser();
				EnrollmentReinstatementResponse reinstatementResponse = enrollmentReinstmtService.processReinstatementRequest(enrollmentReinstatementRequest, true, userObj);
				if( isNotNullAndEmpty(reinstatementResponse) &&  isNotNullAndEmpty(reinstatementResponse.getResponseCode())){
					if(reinstatementResponse.getResponseCode().equalsIgnoreCase(""+EnrollmentConstants.ERROR_CODE_200)){
						enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrlResp.setErrCode(Integer.parseInt(reinstatementResponse.getResponseCode()));
					}else{
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrlResp.setErrCode(Integer.parseInt(reinstatementResponse.getResponseCode()));
						enrlResp.setErrMsg(reinstatementResponse.getResponseDescription());
					}
				}else{
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrlResp.setErrCode(Integer.parseInt(reinstatementResponse.getResponseCode()));
					enrlResp.setErrMsg(reinstatementResponse.getResponseDescription());
				}

			}catch(InvalidUserException iue){
				LOGGER.error(EnrollmentConstants.ERR_MSG_INVALID_USER+iue.getMessage() , iue);
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_299);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_USER+iue.getMessage());
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		return xstream.toXML(enrlResp); 
	}
	
	/**
	 * This REST call is used to get Employee_Id and Plan_Id (Health and Dental) from an employer id
	 * 
	 * @param enrollmentRequestStr
	 * @return
	 */
	@RequestMapping(value = "/findEmployeeIdAndPlanIdByEmployerid", method = RequestMethod.POST)
	@ResponseBody
	public String findEmployeeIdAndPlanIdByEmployerid(@RequestBody String enrollmentRequestStr) {
		LOGGER.info("============inside findEmployeeIdAndPlanIdByEmployerid============");
		XStream xStream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xStream.fromXML(enrollmentRequestStr);
		return xStream.toXML(enrollmentService.findEmployeeIdAndPlanId(enrollmentRequest));
	}

	@RequestMapping(value = "/activeIssuer", method = RequestMethod.POST)
	@ResponseBody
	public String isIssuerActive(@RequestBody Integer id) {
		LOGGER.info("============inside isIssuerActive============");
		LOGGER.info("id1 : " + id);
		String isIssuerActive = "FALSE";
		if(id!=null && enrollmentService.isIssuerActive(id)){
			isIssuerActive="TRUE";
		}
		return isIssuerActive;
	}
	
	@ApiIgnore
	@RequestMapping(value = "/findActiveEnrollmentsForEmployer",method = RequestMethod.POST)
	@ResponseBody
	public String findActiveEnrollmentsForEmployer(@RequestBody Map<String, Object> inputval){
		LOGGER.info("=================== inside findActiveEnrollmentsForEmployer ===================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (inputval == null || inputval.isEmpty()) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
		}
		else if (inputval.get(EnrollmentConstants.CURRENT_DATE) == null || inputval.get(EnrollmentConstants.CURRENT_DATE).equals("")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_CURRENT_DATE_NULL_OR_EMPTY);
		} else if (!(inputval.get(EnrollmentConstants.STATUS) instanceof List<?>)) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_STATUS_NULL_OR_EMPTY);
		} else if (inputval.get(EnrollmentConstants.EMPLOYER_ID) == null || inputval.get(EnrollmentConstants.EMPLOYER_ID).equals("")) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_NULL_OR_EMPTY);
		}
		// Request is valid and has all data required. Proceed processiong request
		else{
			List<Integer> activeEnrollmentIdList = enrollmentService.findActiveEnrollmentsForEmployer(inputval);
			if(activeEnrollmentIdList != null && !activeEnrollmentIdList.isEmpty()){
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setIdList(activeEnrollmentIdList);
			}
			else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ACTIVE_ENROLLMENT_FOUND_FOR_EMPLOYER);
			}
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	/**This rest call is used by shop modeule
	 * It takes enrollment id and employee id passed from shop module
	 * And in return it sends list of enrollment id,enrollment type,coverage start date,ceverage end date
	 * and enrollment status 
	 * @param enrolleeRequestStr
	 * @return
	 * @author vasani_s
	 * @since 19/06/2014
	 */
	@RequestMapping(value = "/findEnrollmentByEmployeeIdAndEnrollmentId" ,method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentByEmployeeIdAndEnrollmentId(@RequestBody String enrolleeRequestStr){
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrolleeRequest enrolleeRequest = (EnrolleeRequest) xstream.fromXML(enrolleeRequestStr);
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if(enrolleeRequest != null ){
			List<Integer> enrollmentIdList = null;
			List<EnrollmentShopDTO> listEnrollmentShopDTO = new ArrayList<EnrollmentShopDTO>();
			if(enrolleeRequest.getEnrollmentId() != null){
				String enrollementIds[] = enrolleeRequest.getEnrollmentId().split(",");
				enrollmentIdList = new ArrayList<Integer>();
				for(String enrollmentIdStr : enrollementIds){
					enrollmentIdList.add(Integer.parseInt(enrollmentIdStr));
				}
			}
			List<Object[]>  enrollmentDataListByEmployee = enrollmentService.findEnrollmentByEmployeeIdAndEnrollmentId(enrolleeRequest.getEmployeeId(),enrollmentIdList);
			if(enrollmentDataListByEmployee != null){
				for (Object[] enrollmentDataByEmployeeId : enrollmentDataListByEmployee) {
					EnrollmentShopDTO enrollmentShopDto = new EnrollmentShopDTO();
					enrollmentShopDto.setEnrollmentId((Integer)enrollmentDataByEmployeeId[0]);
					enrollmentShopDto.setEnrollmentType((String)enrollmentDataByEmployeeId[1]);
					enrollmentShopDto.setCoverageStartDate((Date)enrollmentDataByEmployeeId[2]);
					enrollmentShopDto.setCoverageEndDate((Date)enrollmentDataByEmployeeId[EnrollmentConstants.THREE]);
					enrollmentShopDto.setEnrollmentStatus((String)enrollmentDataByEmployeeId[EnrollmentConstants.FOUR]);
					listEnrollmentShopDTO.add(enrollmentShopDto);
				}
			}
			else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201); 
			}
			enrollmentResponse.setEnrollmentShopDto(listEnrollmentShopDTO);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
		else{
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setStatus(EnrollmentConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * @author ajinkya_m
	 * @since 4th July 2014
	 * 
	 */
	@RequestMapping(value = "/findCoverageEndDateByEmployeeAppID", method = RequestMethod.POST)
	@ResponseBody
	public String findCoverageEndDateByEmployeeApplicationID(@RequestBody String enrollmentRequestStr) {
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		ShopEnrollmentLogDto shopEnrollmentLogDto= new ShopEnrollmentLogDto();
		List<ShopEnrollmentLogDto> shopList = new ArrayList<ShopEnrollmentLogDto>();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);
		Long employeeAppId = null;
		
		if (enrollmentRequest != null) {
				if (!isNotNullAndEmpty(enrollmentRequest.getEmployeeAppId())) {
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYEE_APP_ID_IS_NULL_OR_EMPTY);
					enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else{
					try {
						employeeAppId = enrollmentRequest.getEmployeeAppId();

						String coverageEndDate = null;
						if(isNotNullAndEmpty(employeeAppId)){
							coverageEndDate = enrollmentService.findCoverageEndDateByEmployeeApplicationID(employeeAppId);
						}
						if (coverageEndDate != null) {
							shopEnrollmentLogDto.setCoverageEndDate(coverageEndDate);
							shopList.add(shopEnrollmentLogDto);
							enrlResp.setShopEnrollmentLogDto(shopList);
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
							enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);

						}else{
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
							enrlResp.setErrMsg("Coverage End Date is NULL for Employee Application ID : "+ employeeAppId);
							enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
						}

					} catch (Exception ge) {
						LOGGER.error(EnrollmentConstants.ERR_MSG_DIS_ENROLLMENT_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause() , ge);
						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_206);
						enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DIS_ENROLLMENT_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause());
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
			}
		return xstream.toXML(enrlResp);
	}
	
	/**
	 * 
	 * @author Aditya-S
	 * @since  12-12-2014
	 * 
	 * @param enrollmentRequestStr
	 * @return
	 */
	@RequestMapping(value = "/updateEnrollmentAptc", method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollmentAptc(@RequestBody String enrollmentRequestStr) {
		EnrollmentResponse enrlResp = null;
		/*EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);*/
		EnrollmentRequest enrollmentRequest = platformGson.fromJson(enrollmentRequestStr, EnrollmentRequest.class);
		try{
			enrlResp = enrollmentService.updateEnrollmentAptc(enrollmentRequest);
		}
		catch(GIException gi)
		{
			enrlResp = new EnrollmentResponse();
			enrlResp.setErrCode(gi.getErrorCode());
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlResp.setErrMsg("Exception While Update APTC :: " + gi.getMessage());
		}
		catch (Exception exception){
			enrlResp = new EnrollmentResponse();
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlResp.setErrMsg("Exception While Update APTC :: " + exception.getMessage());
			LOGGER.error("Exception While Update APTC :: " + exception.getMessage());
		}
		/*return xstream.toXML(enrlResp);*/
		return platformGson.toJson(enrlResp);
	}
	
	/**
	 * @author Aditya-S
	 * @since 29-01-2015
	 * 
	 * @param ssapApplicationRequest
	 * @return
	 */
	@RequestMapping(value = "/isActiveEnrollmentForApplicationId", method = RequestMethod.POST)
	@ResponseBody
	public boolean isActiveEnrollmentForApplicationId(@RequestBody Long applicationId) {
		return enrollmentService.isActiveEnrollmentForApplicationId(applicationId, true);
	}
	
	/**
	 * @author Pratap
	 * @since 31-07-2015
	 * 
	 * @param ssapApplicationRequest
	 * @return
	 */
	@RequestMapping(value = "/isActiveEnrollmentForApplicationId/{coverageYear}", method = RequestMethod.POST)
	@ResponseBody
	public boolean isActiveEnrollmentForApplicationId(@RequestBody Long applicationId, @PathVariable("coverageYear") String coverageYear) {
		if(isNotNullAndEmpty(coverageYear) && StringUtils.isNumeric(coverageYear) && coverageYear.length()==4){
			return enrollmentService.isActiveEnrollmentForApplicationId(applicationId, coverageYear);	
		}else{
			return false;	
		}
	}
	
	/**
	 * @author panda_p
	 * @since 15/01/2018
	 * Jira - HIX-105863: Create an API for Agent Bob Transfer
	 * @param brokerReq
	 * @param response
	 */
	private void validateAgentBobTransferReq(EnrollmentBrokerUpdateDTO brokerReq, EnrollmentResponse response){
		try{
			if(brokerReq==null){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(!isNotNullAndEmpty(brokerReq.getMarketType()) || (!brokerReq.getMarketType().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)&& !brokerReq.getMarketType().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP))){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_MARKET_TYPE);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(!isNotNullAndEmpty(brokerReq.getOldAssisterBrokerId())){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_OLD_BROKER_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else if(!isNotNullAndEmpty(brokerReq.getAssisterBrokerId())){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_205);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_BROKER_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(!isNotNullAndEmpty(brokerReq.getRoleType()) || (!brokerReq.getRoleType().equalsIgnoreCase(EnrollmentConstants.AGENT_ROLE)&& !brokerReq.getRoleType().equalsIgnoreCase(EnrollmentConstants.ASSISTER_ROLE))){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_206);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_IVALID_USER_ROLE);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				response.setErrMsg(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);
				response.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		}catch(Exception e){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + EnrollmentUtils.shortStackTrace(e, 500));
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
	}
	
	@RequestMapping(value = "/agentbobtransfer", method = RequestMethod.POST)
	@ResponseBody
	public String agentBobTransfer(@RequestBody String request) {
		EnrollmentRequest req= null;
		EnrollmentResponse response= new EnrollmentResponse();
		try {
			if(isNotNullAndEmpty(request)){
				req= platformGson.fromJson(request, EnrollmentRequest.class);
				if(req==null){
					response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else{
					response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					response.setStatus(GhixConstants.RESPONSE_SUCCESS);
					
					EnrollmentBrokerUpdateDTO brokerReq	=req.getEnrollmentBrokerUpdateData();
					validateAgentBobTransferReq(brokerReq, response);
				}
			}else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} catch (Exception e) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + EnrollmentUtils.shortStackTrace(e, 500));
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		String resp = platformGson.toJson(response);
		try {
			EnrlAgentBOBTransfer enrlAgentBOBTransfer = new EnrlAgentBOBTransfer();
			
			AccountUser user = userService.getLoggedInUser();
			enrlAgentBOBTransfer.setUser(user);
			
			enrlAgentBOBTransfer.setRequest(platformGson.toJson(req.getEnrollmentBrokerUpdateData()));
			enrlAgentBOBTransfer.setResponse(resp !=null? resp.substring(0, resp.length()>999? 990:resp.length()):"");
			if(GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(response.getStatus())){
				enrlAgentBOBTransfer.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}else{
				enrlAgentBOBTransfer.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			enrlAgentBobTransferService.saveAgentBobTransferLog(enrlAgentBOBTransfer);
			
			//Sending Request for async process
			if(GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(response.getStatus())){
				enrlAgentBobTransferService.processAgentBOBTranferAsync(enrlAgentBOBTransfer);
			}
		}catch (Exception e) {
			LOGGER.error("Failed to log request and response in  ENRL_AGENT_BOB_TRNSFR ");
		}
		
		return resp;

	}
	
	@RequestMapping(value = "/updateEnrollmentBrokerDetails", method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollmentBrokerDetails(@RequestBody String request) {
		EnrollmentRequest req= null;
		EnrollmentResponse response= new EnrollmentResponse();
		try {
			if(isNotNullAndEmpty(request)){
				req= platformGson.fromJson(request, EnrollmentRequest.class);
				if(req==null){
					response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else{
					EnrollmentBrokerUpdateDTO brokerReq	=req.getEnrollmentBrokerUpdateData();
					validateBrokerUpdateReq(brokerReq, response);
					if(response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
					enrollmentService.updateBrokerDetails(brokerReq, response)	;
					}else{
						return platformGson.toJson(response);
					}
				}
			}else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} catch (Exception e) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(response);

	}
	
	private void validateBrokerUpdateReq(EnrollmentBrokerUpdateDTO brokerReq, EnrollmentResponse response){
		try{
			if(brokerReq==null){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(!isNotNullAndEmpty(brokerReq.getMarketType()) || (!brokerReq.getMarketType().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)&& !brokerReq.getMarketType().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP))){
				//Invalid Market Type
				response.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_MARKET_TYPE);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if((brokerReq.getMarketType().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL))
					&& (!(isNotNullAndEmpty(brokerReq.getHouseHoldCaseId()) || isNotNullAndEmpty(brokerReq.getExchgIndividualIdentifier())))){
				//Adding new check to verify Either HousHoldCaseId
				//Household case id null for Individual Market type
				response.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_HOUSEHOLD_OR_EXCHG_IDENTIFIER_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(brokerReq.getMarketType().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP) && !isNotNullAndEmpty(brokerReq.getEmployerEnrollmentId())){
				//Employer enrollment id not given for shop
				response.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYER_ENROLLMENT_ID_NULL_OR_EMPTY);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				
			}else if(!isNotNullAndEmpty(brokerReq.getAssisterBrokerId())){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_205);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_BROKER_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(!isNotNullAndEmpty(brokerReq.getRoleType()) || (!brokerReq.getRoleType().equalsIgnoreCase(EnrollmentConstants.AGENT_ROLE)&& !brokerReq.getRoleType().equalsIgnoreCase(EnrollmentConstants.ASSISTER_ROLE))){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_206);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_IVALID_USER_ROLE);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(!isNotNullAndEmpty(brokerReq.getAddremoveAction()) || (!brokerReq.getAddremoveAction().equalsIgnoreCase(EnrollmentConstants.ACTION_ADD)&& !brokerReq.getAddremoveAction().equalsIgnoreCase(EnrollmentConstants.ACTION_REMOVE))){
				
				response.setErrCode(EnrollmentConstants.ERROR_CODE_207);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_IVALID_ACTION);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}/*else if(!isNotNullAndEmpty(brokerReq.getBrokerTPAFlag()) || (!brokerReq.getBrokerTPAFlag().equalsIgnoreCase(EnrollmentConstants.Y)&& !brokerReq.getBrokerTPAFlag().equalsIgnoreCase(EnrollmentConstants.N))){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_208);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_IVALID_USER_ROLE);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}*/else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				response.setErrMsg(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);
				response.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}

		}catch(Exception e){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
	}
	
	@RequestMapping(value="/findEnrollmentStatusByEmployeeId", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentStatusByEmployeeId(@RequestBody String employeeId){
		  LOGGER.info("============inside findEnrollmentStatusByEmployeeId============");
		  LOGGER.info("employeeId : " + employeeId);
		  String enrl_status = null;
		  try{
		   enrl_status = enrollmentService.findEnrollmentStatusByEmployeeId(Integer.valueOf(employeeId));
		  }catch (Exception e) {
		   LOGGER.error("employeeId : " + employeeId +" Error Message :"+e.getMessage());
		  }
		  return enrl_status;
		 }
	
	@RequestMapping(value="/getEnrollmentDataForAssisterBroker", method = RequestMethod.POST)
	@ResponseBody
	public EnrollmentResponse getEnrollmentDataForAssisterBroker(@RequestBody String enrollmentId){
		LOGGER.info("============inside getEnrollmentDataForAssisterBroker============");
		LOGGER.info("enrollment Id : " + enrollmentId);
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		enrollmentResponse = enrollmentService.getEnrollmentDataForBroker(enrollmentId);
		return enrollmentResponse;
	}
	
	@RequestMapping(value="/automateSpecialEnrollment", method = RequestMethod.POST)
	@ResponseBody
	public String automateSpecialEnrollment(@RequestBody String request){
		EnrollmentUpdateRequest req= null;
		EnrollmentResponse response = new EnrollmentResponse();
		boolean isSuccess=false;
		GIWSPayload giWsPayloadObj=null;
		try {
			giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService,
					request, null,
					GhixEndPoints.EnrollmentEndPoints.AUTOMATE_SPECIAL_ENROLLMENT_URL, "AUTOMATE_SPECIAL_ENROLLMENT",
					isSuccess);
			if(isNotNullAndEmpty(request)){
				req= platformGson.fromJson(request, EnrollmentUpdateRequest.class);
				if(req==null){
					response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else{
					enrollmentCreationService.automateSpecialEnrollment(req, response);
					if(response.getStatus()==null || !response.getStatus().equalsIgnoreCase(EnrollmentConstants.FAILURE)) {
						response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						response.setStatus(GhixConstants.RESPONSE_SUCCESS);
					}
					isSuccess=true;
					
					if(response.getMemberAddedToEnrollment()!=null && !response.getMemberAddedToEnrollment().isEmpty()){
						try{
							enrollmentCreationService.terminateNewlyAddedmembers(response , EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTOMATE_SPECIAL_DISENROLL);
						}catch(Exception e){
							LOGGER.error("Error while disenrolling the newly added member to the disenrolled enrollment" , e);
						}
					}
				}
			}else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred in automateSpecialEnrollment flow: ",e); 
			response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: "+ EnrollmentUtils.shortenedStackTrace(e, 4));
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}finally{
			if(giWsPayloadObj!=null && response!=null ){
				EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj,platformGson.toJson(response) , isSuccess);
			}
		}
		return platformGson.toJson(response);
	}
	
	
	/**
	 * Jira Id: HIX-71256
	 * @author Sharma_k
	 * @since 25th June 2015
	 * @param houseHoldCaseIdList List<String>
	 * @return
	 * Api to fetch Enrollment and Enrollee data by list of HouseHoldCaseIds and by EnrollmentStatusList 
	 */
	@RequestMapping(value="/getenrollmentdatabyhouseholdcaseids", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentDataByHouseHoldCaseIds(@RequestBody String requestData)
	{
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		LOGGER.info("=========== started getEnrollmentDataByHouseHoldCaseIds ========");
		if(StringUtils.isNotEmpty(requestData))
		{
			try
			{
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(requestData, EnrollmentRequest.class);
				if (enrollmentRequest != null && enrollmentRequest.getHouseHoldCaseIdList() != null
						&& !enrollmentRequest.getHouseHoldCaseIdList().isEmpty())
				{
				
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setEnrollmentDataDtoListMap(enrollmentService.getEnrollmentDataByHouseHoldCaseIds(enrollmentRequest));
				
				} else if(enrollmentRequest.getIdList() != null && !enrollmentRequest.getIdList().isEmpty()){
					
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setEnrollmentDataDtoListMap(enrollmentService.getEnrollmentDataByHouseHoldCaseIds(enrollmentRequest));
					
				}else
				{
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
			catch(GIException gie)
			{
				enrollmentResponse.setErrCode(gie.getErrorCode());
				enrollmentResponse.setErrMsg(gie.getErrorMsg());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			catch(Exception ex)
			{
				LOGGER.error("Exception occurred in getEnrollmentDataByHouseHoldCaseId API: ",ex);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + " :: "+ EnrollmentUtils.shortenedStackTrace(ex, 4));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		else
		{
			LOGGER.info("Received Null or Empty request for getEnrollmentDataByHouseHoldCaseId");
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	/**
	 * Jira Id: HIX-72324
	 * Enrollment API for Renewal
	 * @since 21st July 2015
	 * @author Sharma_k
	 * @param requestData String
	 * @return Json String 
	 */
	@RequestMapping(value="/getenrollmentdatabyssapapplicationid", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentDataBySsapApplicationId(@RequestBody String requestData)
	{
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if(StringUtils.isNotEmpty(requestData))
		{
			try
			{
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(requestData, EnrollmentRequest.class);
				if(enrollmentRequest != null && enrollmentRequest.getSsapApplicationId() != null)
				{
					List<EnrollmentDataDTO> enrollmentDataDTOList = enrollmentService
							.getEnrollmentDataBySsapApplicationId(enrollmentRequest
									.getSsapApplicationId());
					if(enrollmentDataDTOList != null && !enrollmentDataDTOList.isEmpty())
					{
						enrollmentResponse.setEnrollmentDataDTOList(enrollmentDataDTOList);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					}
					else
					{
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
					}
				}
				else
				{
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
				}
			}
			catch(Exception ex)
			{
				LOGGER.error("Caught exception in getEnrollmentDataBySsapApplicationId(-) call :: ",ex);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL+":: "+ EnrollmentUtils.shortenedStackTrace(ex, 4));
			}
		}
		else
		{
			LOGGER.info("Received null or empty request in getEnrollmentDataBySsapApplicationId(-) call ");
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	/**
	 * Jira Id: 
	 * @author Sharma_k
	 * @since 25th June 2015
	 * @param houseHoldCaseIdList List<String>
	 * @return
	 * Api to fetch Enrollment and Enrollee data by list of HouseHoldCaseIds and by EnrollmentStatusList 
	 */
	@RequestMapping(value="/getenrollmentdataforbrokerbyhouseholdcaseids", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentDataForBrokerByHouseHoldCaseIds(@RequestBody String requestData)
	{
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		LOGGER.info("=========== started getEnrollmentDataForBrokerByHouseHoldCaseIds ========");
		if(StringUtils.isNotEmpty(requestData))
		{
			try
			{
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(requestData, EnrollmentRequest.class);
				if (enrollmentRequest != null && enrollmentRequest.getHouseHoldCaseIdList() != null
						&& !enrollmentRequest.getHouseHoldCaseIdList().isEmpty())
				{
				
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setEnrollmentDataDtoListMap(enrollmentService.getEnrollmentDataForBrokerByHouseHoldCaseIds(enrollmentRequest));
				}
				else
				{
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
			catch(GIException gie)
			{
				enrollmentResponse.setErrCode(gie.getErrorCode());
				enrollmentResponse.setErrMsg(gie.getErrorMsg());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			catch(Exception ex)
			{
				LOGGER.error("Exception occurred in getenrollmentdataforbrokerbyhouseholdcaseids API: ", ex);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + " :: "+ EnrollmentUtils.shortenedStackTrace(ex, 4));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		else
		{
			LOGGER.info("Received Null or Empty request for getEnrollmentDataForBrokerByHouseHoldCaseIds");
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	/**
	 * Jira Id: HIX-79398
	 * Enrollment API to convert Pending Enrollment to Confirmed
	 * @since 17th Nov 2015
	 * @author shinde_dh
	 * @param requestData String
	 * @return Json String 
	 */
	@RequestMapping(value="/updateEnrollmentStatus", method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollmentStatus(@RequestBody String requestData)
	{
		EnrollmentResponse enrollmentResponse =  new EnrollmentResponse();
		enrollmentResponse.startResponse();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentStatusUpdateDto.class);
		
		if(isNotNullAndEmpty(requestData))
		{
			try
			{
				EnrollmentStatusUpdateDto enrollmentStatusUpdateRequest = reader.readValue(requestData);
				enrollmentService.updateEnrollmentStatus(enrollmentStatusUpdateRequest,enrollmentResponse, EnrollmentEvent.TRANSACTION_IDENTIFIER.ENROLLMENT_EFFECTUATION);
			}
			catch(GIException gi)
			{
				enrollmentResponse.setErrCode(gi.getErrorCode());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Exception in updateEnrollmentStatus() :: " + gi.getMessage());
			}
			catch (Exception exception){
				LOGGER.error("Exception in updateEnrollmentStatus() :: ",exception);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Exception in updateEnrollmentStatus() :: " + exception.getMessage());
			}
		}
		else
		{
			LOGGER.info("Received null or empty request in updateEnrollmentStatus(-) call ");
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
		}
		enrollmentResponse.endResponse();
		return platformGson.toJson(enrollmentResponse);
	}
	
	@RequestMapping(value="/setResend834", method = RequestMethod.POST)
	@ResponseBody
	public String setResend834(@RequestBody String requestData)
	{
		EnrollmentResponse enrollmentResponse =  new EnrollmentResponse();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(Enrollment834ResendDTO.class);
		
		if(isNotNullAndEmpty(requestData))
		{
			try
			{
				Enrollment834ResendDTO enrollment834ResendDTO = reader.readValue(requestData);
				enrollmentService.updateEnrollmentResend(enrollment834ResendDTO,enrollmentResponse);
			}
			catch(GIException gi)
			{
				enrollmentResponse.setErrCode(gi.getErrorCode());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Exception in resend834() :: " + gi.getMessage());
			}
			catch (Exception exception){
				LOGGER.error("Exception in resend834() :: ",exception);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Exception in resend834() :: " + exception.getMessage());
			}
		}
		else
		{
			LOGGER.info("Received null or empty request in resend834(-) call ");
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	@ApiIgnore
	@RequestMapping(value = "/terminateenrollmentbyplanid", method = RequestMethod.POST)
	@ResponseBody
	public String terminateEnrollmentByPlanId(@RequestBody Map<String, String> inputval) {
		//LOGGER.info("=================== inside terminateEnrollmentByPlanId ===================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		try{
			if (inputval == null || inputval.isEmpty()) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			} 
			if (inputval.get(EnrollmentConstants.CMSPLAN_ID) == null || inputval.get(EnrollmentConstants.CMSPLAN_ID).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_DECERTIFIED_PLAN_ID_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			}		
			if (inputval.get(EnrollmentConstants.COVERAGE_YEAR) == null || inputval.get(EnrollmentConstants.COVERAGE_YEAR).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_COVERAGE_YEAR_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			}
			if (inputval.get(EnrollmentConstants.TERM_DATE) == null || inputval.get(EnrollmentConstants.TERM_DATE).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_DECERTIFIED_DATE_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			}
			
			enrollmentResponse = enrollmentService.terminateEnrollmentByPlanId(inputval.get(EnrollmentConstants.CMSPLAN_ID), inputval.get(EnrollmentConstants.COVERAGE_YEAR), inputval.get(EnrollmentConstants.TERM_DATE) );			
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred in terminateEnrollmentByPlanId API: For Plan Id: "+inputval != null ? inputval.get(EnrollmentConstants.CMSPLAN_ID) : null, ex);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	@ApiIgnore
	@RequestMapping(value = "/countenrollmentbyplanid", method = RequestMethod.POST)
	@ResponseBody
	public String countEnrollmentByPlanId(@RequestBody Map<String, String> inputval) {
		LOGGER.info("=================== inside countEnrollmentByPlanId ===================");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		try{
			if (inputval == null || inputval.isEmpty()) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			} 
			if (inputval.get(EnrollmentConstants.CMSPLAN_ID) == null || inputval.get(EnrollmentConstants.CMSPLAN_ID).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_DECERTIFIED_PLAN_ID_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			}		
			if (inputval.get(EnrollmentConstants.COVERAGE_YEAR) == null || inputval.get(EnrollmentConstants.COVERAGE_YEAR).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_COVERAGE_YEAR_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			}
			if (inputval.get(EnrollmentConstants.TERM_DATE) == null || inputval.get(EnrollmentConstants.TERM_DATE).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_DECERTIFIED_DATE_NULL_OR_EMPTY);
				return xstream.toXML(enrollmentResponse);
			}
			enrollmentResponse = enrollmentService.countEnrollmentByPlanId(inputval.get(EnrollmentConstants.CMSPLAN_ID), inputval.get(EnrollmentConstants.COVERAGE_YEAR), inputval.get(EnrollmentConstants.TERM_DATE) );
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred in countenrollmentbyplanid API, For Plan ID: "+inputval != null ? inputval.get(EnrollmentConstants.CMSPLAN_ID) : null, ex);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
		}	
		return xstream.toXML(enrollmentResponse);
	}
	

	@RequestMapping(value="/updateEnrollment", method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollment(@RequestBody String request){
		EnrollmentUpdateDTO req= null;
		EnrollmentResponse response = new EnrollmentResponse();
		boolean isSuccess = false;
		GIWSPayload giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService, request, null,
				GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_UPDATE, "ENROLLMENT_EDIT_TOOL", isSuccess);
		try {
			if(isNotNullAndEmpty(request)){
				req= platformGson.fromJson(request, EnrollmentUpdateDTO.class);
				if(req==null){
					response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else{
					LOGGER.info("Received request for updating Enrollment Id: "+req.getId());
					setEffectiveDate(req);
					if(req.getAction()!=null && req.getAction().toString().equalsIgnoreCase("CANCEL") ) {
						LOGGER.info("Received request for cancelling Enrollment Id: "+req.getId());
						setEffectiveDate(req);
						cancelEnrollmentByEditTool(req, response, giWsPayloadObj.getId());
					} else if (req.getAction()!=null && "ADD".equalsIgnoreCase(req.getAction().toString())) {
						LOGGER.info("Received request for edit tool ADD transaction for Enrollment Id: "+req.getId());
						enrollmentEditToolService.updateEnrollmentAddTxnEditTool(req, response, giWsPayloadObj.getId());
					}else {
						enrollmentService.updateEnrollment(req, response, giWsPayloadObj.getId());
					}
					if(response.getStatus()==null || !response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
						response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						response.setStatus(GhixConstants.RESPONSE_SUCCESS);
						isSuccess = true;
					}
				}
			}else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred in updateEnrollment API, For enrollment Id: "+req != null ? req.getId() : null,e);
			if(response.getErrCode()==0){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: "+ EnrollmentUtils.shortenedStackTrace(e, 4));
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj, platformGson.toJson(response), isSuccess);
		return platformGson.toJson(response);
	}	
	
	@RequestMapping(value="/cancelenrollment", method = RequestMethod.POST)
	@ResponseBody
	public String cancelEnrollment(@RequestBody String request){
		EnrollmentUpdateDTO req= null;
		EnrollmentResponse response = new EnrollmentResponse();
		boolean isSuccess = false;
		GIWSPayload giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService, request, null,
				GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_CANCEL, "ENROLLMENT_EDIT_TOOL", isSuccess);
		try {
			if(isNotNullAndEmpty(request)){
				req= platformGson.fromJson(request, EnrollmentUpdateDTO.class);
				if(req==null){
					response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else{
					LOGGER.info("Received request for cancelling Enrollment Id: "+req.getId());
					setEffectiveDate(req);
					cancelEnrollmentByEditTool(req, response, giWsPayloadObj.getId());
					if(response.getStatus()==null || !response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
						response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						response.setStatus(GhixConstants.RESPONSE_SUCCESS);
						isSuccess = true;
					}
				}
			}else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred in cancelEnrollment API: For Enrollment Id: "+req != null ? req.getId() : null, e);
			if(response.getErrCode()==0){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: "+ EnrollmentUtils.shortenedStackTrace(e, 4));
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj, platformGson.toJson(response), isSuccess);
		return platformGson.toJson(response);
	}
	
	/**
	 * @author panda_p
	 * @since 01-08-2017
	 * @param req
	 * @param response
	 * @param giWsPayloadId
	 * @return
	 */
	private EnrollmentResponse cancelEnrollmentByEditTool(EnrollmentUpdateDTO req, EnrollmentResponse response, Integer giWsPayloadId){
		if(req.getId()==null){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_NULL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}else{
			
			try {
				Map<String, String> requestParam = new HashMap<String, String>();
				EnrollmentDisEnrollmentDTO disEnrollmentDTO = new EnrollmentDisEnrollmentDTO();
				List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
				/** Set Enrollment List to be disenrolled*/
				List<Integer> enrollmentIdsList = new ArrayList<Integer>();
				enrollmentIdsList.add(req.getId());
				
				enrollmentList = enrollmentService.findAllEnrollments(enrollmentIdsList);
				disEnrollmentDTO.setEnrollmentList(enrollmentList);
				disEnrollmentDTO.setTerminationReasonCode(req.getCancellationReasonCode());
				disEnrollmentDTO.setSend834(req.isSend834());
				/** Set Updated by as Logged In user*/
				AccountUser loggedInUser  = enrollmentService.getLoggedInUser();
				disEnrollmentDTO.setUpdatedBy(loggedInUser);
				disEnrollmentDTO.setAllowRetroTermination(true);
				disEnrollmentDTO.setNullifyDentalAPTCOnHealthTerm(true);
				disEnrollmentDTO.setGiWsPayloadId(giWsPayloadId);
				enrollmentList = enrollmentService.disEnrollEnrollment(disEnrollmentDTO, EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);	
				requestParam.put(EnrollmentConstants.SEND_IN_834, String.valueOf(req.isSend834()).equals("true") ? EnrollmentConstants.YES : EnrollmentConstants.NO );
				enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(
						enrollmentService.findById(req.getId()),
						EnrollmentConstants.EnrollmentAppEvent.CANCEL_OVERRIDE.toString(), requestParam, req.getComment(), enrollmentService.getLoggedInUser());
			}catch (Exception e) {
				LOGGER.error("Exception occurred in cancelEnrollmentByEditTool ", e);
				if(response.getErrCode()==0){
					response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
					response.setErrMsg("Failed to Cancel enrollment" + " :: "+ EnrollmentUtils.shortenedStackTrace(e, 4));
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
			
		}
		return response;
	}
	
	@ApiIgnore
	@RequestMapping(value="/updateMailByHouseHoldId", method = RequestMethod.POST)
	@ResponseBody
	public String updateMailByHouseHoldId(@RequestBody Map<String, String> inputval){
		LOGGER.info(" ** Inside updateMailByHouseHoldId ** ");
		EnrollmentResponse enrollmentResponse =  new EnrollmentResponse();
		try{
			if(inputval == null || inputval.isEmpty()) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_INPUT);
				return platformGson.toJson(enrollmentResponse);
			}
			
			if(inputval.get(EnrollmentConstants.HOUSEHOLD_CASE_ID_KEY) == null || inputval.get(EnrollmentConstants.HOUSEHOLD_CASE_ID_KEY).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_HOUSEHOLD_IS_NULL);
				return platformGson.toJson(enrollmentResponse);
			}
			
			if(inputval.get(EnrollmentConstants.LOCATION_ID) == null || inputval.get(EnrollmentConstants.LOCATION_ID).equals("")) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_LOCATION_ID_IS_NULL);
				return platformGson.toJson(enrollmentResponse);
			}
			
			String householdCaseId = inputval.get(EnrollmentConstants.HOUSEHOLD_CASE_ID_KEY);
			Long locationID = Long.parseLong(inputval.get(EnrollmentConstants.LOCATION_ID));
			
			enrollmentResponse = enrollmentService.updateMailByHouseHoldId(householdCaseId, locationID);
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred in updateMailByHouseHoldId API, For Household: "+inputval != null ? inputval.get(EnrollmentConstants.HOUSEHOLD_CASE_ID_KEY) : null, ex);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	/*@RequestMapping(value="/validateEnrollmentCoverageOverlap", method = RequestMethod.POST)
	@ResponseBody
	public EnrollmentResponse validateEnrollmentCoverageOverlap(@RequestBody EnrollmentCoverageValidationRequest request){
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (null != request.getHouseholdCaseId() && (null != request.getCoverageStartDate() && DateUtil.isValidDate(request.getCoverageStartDate(), GhixConstants.REQUIRED_DATE_FORMAT))
				&& (null != request.getCoverageEndDate() && DateUtil.isValidDate(request.getCoverageEndDate(), GhixConstants.REQUIRED_DATE_FORMAT))
				&& (null != request.getMemberIdList() && !request.getMemberIdList().isEmpty())){
			enrollmentResponse = enrollmentService.validateEnrollmentOverlap(request);
		}else if(null == request.getMemberIdList() || request.getMemberIdList().isEmpty()){
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg("Member ID list is null or empty");
		}else if(null ==  request.getHouseholdCaseId()){
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg("Household Case ID is null");
		}else if(null == request.getCoverageStartDate() || null == request.getCoverageEndDate()){
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			enrollmentResponse.setErrMsg("Coverage Start or End Date is null");
		}else if ((null != request.getCoverageStartDate() && !DateUtil.isValidDate(request.getCoverageStartDate(), GhixConstants.REQUIRED_DATE_FORMAT))
					|| (null != request.getCoverageEndDate()&& !DateUtil.isValidDate(request.getCoverageEndDate(), GhixConstants.REQUIRED_DATE_FORMAT))) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentResponse.setErrMsg("Coverage Start or End Date is of invalid format. Required :: " + GhixConstants.REQUIRED_DATE_FORMAT);
		}else{
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentResponse.setErrMsg("Invalid Request");
		}
		return enrollmentResponse;
	}*/
	
	@RequestMapping(value = "/getmonthlyaptcamount", method = RequestMethod.POST)
	@ResponseBody
	public String getMonthlyAPTCAmount(@RequestBody String enrollmentRequestStr) {
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		List<MonthlyAPTCAmountDTO> monthlyAPTCAmountDTOList = null;

		try {
			EnrollmentRequest enrollmentRequest = platformGson.fromJson(enrollmentRequestStr, EnrollmentRequest.class);

			if (enrollmentRequest != null && (enrollmentRequest.getIdList() != null && !enrollmentRequest.getIdList().isEmpty())) {
				monthlyAPTCAmountDTOList = enrollmentService.getMonthlyAPTCAmount(enrollmentRequest.getIdList());

			} else {
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_ID_LIST_FOR_THE_GIVEN_REQUEST);
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				return platformGson.toJson(enrlResp);
			}

			if (monthlyAPTCAmountDTOList != null) {
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrlResp.setMonthlyAPTCAmountDTOList(monthlyAPTCAmountDTOList);
				return platformGson.toJson(enrlResp);
			} else {
				LOGGER.error(EnrollmentConstants.ERR_MSG_NO_ACTIVE_ENROLLMENT);
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ACTIVE_ENROLLMENT);
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			}

		} catch (Exception e) {
			LOGGER.error(EnrollmentConstants.ERR_MSG_MONTHLY_APTC_AMOUNTS_FAIL + " :: " + e.getMessage() + EnrollmentConstants.CAUSE + e.getCause(), e);
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_MONTHLY_APTC_AMOUNTS_FAIL + " :: " + e.getMessage() + EnrollmentConstants.CAUSE + e.getCause());
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
		}

		return platformGson.toJson(enrlResp);
	}
	
	@RequestMapping(value = "/getenrollmentatributebyid", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentAtributeById(@RequestBody String enrollmentRequestStr) {
		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		EnrollmentRequest enrollmentRequest = null;
		try{
			enrollmentRequest = (EnrollmentRequest) platformGson.fromJson(enrollmentRequestStr, EnrollmentRequest.class);
			if(enrollmentRequest.getEnrollmentId() ==null || enrollmentRequest.getEnrollmentId() ==0){
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(enrollmentRequest.getEnrollmentAttributeList()==null || enrollmentRequest.getEnrollmentAttributeList().isEmpty()){
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EnrollmentAttributeEnum_IS_NULL_OR_EMPTY);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if(enrollmentRequest.getEnrollmentAttributeList().contains(null)){
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EnrollmentAttributeEnum_IS_NULL_OR_EMPTY);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else {
				EnrollmentDTO enrollmentDTO = enrollmentService.getEnrollmentAtributeById(enrollmentRequest);
				if(null != enrollmentDTO){
					enrollmentResponse.setEnrollmentDTO(enrollmentDTO);
					enrollmentResponse.setEnrollmentExists(true);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}else{
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_NOT_FOUND_FOR_GIVEN_ID);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}

			}
		}catch(Exception e){
			LOGGER.error("Exception occurred in getEnrollmentAtributeById API, For Enrollment Id: "+enrollmentRequest != null ? enrollmentRequest.getEnrollmentId(): null, e);
			enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(e,5));
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		
		return platformGson.toJson(enrollmentResponse);
	}
	
	@RequestMapping(value ="/getenrollmentbyidandhousholdcaseids", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentDataByIdAndHouseholdCaseIds(@RequestBody String enrollmentRequestStr){
		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		LOGGER.info("=========== started getEnrollmentDataByIdAndHouseholdCaseIds ========");
		if(StringUtils.isNotEmpty(enrollmentRequestStr))
		{
			try
			{
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(enrollmentRequestStr, EnrollmentRequest.class);
				if (enrollmentRequest != null)
				{
					if((enrollmentRequest.getHouseHoldCaseIdList() != null && !enrollmentRequest.getHouseHoldCaseIdList().isEmpty()) || 
							(enrollmentRequest.getIdList() != null && !enrollmentRequest.getIdList().isEmpty())
						){
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrollmentResponse.setEnrollmentDataDtoListMap(enrollmentService.getEnrollmentDataByIdAndHouseholdCaseIds(enrollmentRequest));
					}
					else{
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentResponse.setErrMsg("Either Enrollment ID or Household Case ID list is expected in request");
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else
				{
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
			catch(GIException gie)
			{
				enrollmentResponse.setErrCode(gie.getErrorCode());
				enrollmentResponse.setErrMsg(gie.getErrorMsg() +" Received Request: "+enrollmentRequestStr);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			catch(Exception ex)
			{
				LOGGER.error("Exception occured in getEnrollmentDataByIdAndHouseholdCaseIds API: ",ex);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + " :: "+ EnrollmentUtils.shortenedStackTrace(ex, 4));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		else
		{
			LOGGER.info("Received Null or Empty request for getEnrollmentDataByIdAndHouseholdCaseIds");
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	@RequestMapping(value="/updateenrollmentaddtxn", method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollmentAddTxnByEditTool(@RequestBody String request){
		EnrollmentUpdateDTO req= null;
		EnrollmentResponse response = new EnrollmentResponse();
		boolean isSuccess = false;
		GIWSPayload giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService, request, null,
				GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_UPDATE_ADD_TXN, "ENROLLMENT_EDIT_TOOL", isSuccess);
		try {
			if(isNotNullAndEmpty(request)){
				req= platformGson.fromJson(request, EnrollmentUpdateDTO.class);
				if(req==null){
					response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}else{
					LOGGER.info("Received request for updating Enrollment Id: "+req.getId());
					setEffectiveDate(req);
					enrollmentEditToolService.updateEnrollmentAddTxnEditTool(req, response, giWsPayloadObj.getId());
					if(response.getStatus()==null ||! response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
						response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						response.setStatus(GhixConstants.RESPONSE_SUCCESS);
						isSuccess = true;
					}
				}
			}else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred in updateEnrollmentAddTxnByEditTool API, For Enrollment Id "+req != null ? req.getId() : null, e);
			if(response.getErrCode()==0){
				response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: "+ EnrollmentUtils.shortenedStackTrace(e, 4));
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj, platformGson.toJson(response), isSuccess);
		return platformGson.toJson(response);
	}	
	
	private void setEffectiveDate(EnrollmentUpdateDTO enrollmentUpdateDTO)throws ParseException{
		if(StringUtils.isNotBlank(enrollmentUpdateDTO.getBenefitEffectiveDateStr()) && enrollmentUpdateDTO.getBenefitEffectiveDate() == null){
			enrollmentUpdateDTO.setBenefitEffectiveDateStr(enrollmentUpdateDTO.getBenefitEffectiveDateStr());
		}
		if(StringUtils.isNotBlank(enrollmentUpdateDTO.getBenefitEndDateStr()) && enrollmentUpdateDTO.getBenefitEndDate() == null){
			enrollmentUpdateDTO.setBenefitEndDateStr(enrollmentUpdateDTO.getBenefitEndDateStr());
		}
		if(StringUtils.isNotBlank(enrollmentUpdateDTO.getPremiumPaidToDateEndStr()) && enrollmentUpdateDTO.getPremiumPaidToDateEnd() == null){
			enrollmentUpdateDTO.setPremiumPaidToDateEndStr(enrollmentUpdateDTO.getPremiumPaidToDateEndStr());
		}
		if(enrollmentUpdateDTO.getMembers() != null && !enrollmentUpdateDTO.getMembers().isEmpty()){
			List<EnrolleeDataUpdateDTO> enrolleeDataUpdateDtoList = enrollmentUpdateDTO.getMembers();
			for(EnrolleeDataUpdateDTO enrolleeDataUpdateDTO : enrolleeDataUpdateDtoList){
				if(StringUtils.isNotBlank(enrolleeDataUpdateDTO.getEffectiveStartDateStr()) && enrolleeDataUpdateDTO.getEffectiveStartDate() == null){
					enrolleeDataUpdateDTO.setEffectiveStartDateStr(enrolleeDataUpdateDTO.getEffectiveStartDateStr());
				}
				if(StringUtils.isNotBlank(enrolleeDataUpdateDTO.getEffectiveEndDateStr())  && enrolleeDataUpdateDTO.getEffectiveEndDate() == null){
					enrolleeDataUpdateDTO.setEffectiveEndDateStr(enrolleeDataUpdateDTO.getEffectiveEndDateStr());
				}
			}
		}
	}
	/**
	 * Jira Id: HIX-106334, HIX-107740 
	 * @param inputRequest
	 * @return
	 */
	@RequestMapping(value="/getenrollmentdatabymemberidlist", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentDataByMemberIdList(@RequestBody String inputRequest){
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if(isNotNullAndEmpty(inputRequest)){
			try{
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(inputRequest, EnrollmentRequest.class);
				if(enrollmentRequest != null && enrollmentRequest.getMemberIdList() != null && !enrollmentRequest.getMemberIdList().isEmpty()){
					Map<String, List<EnrollmentMemberDataDTO>> enrollmentMemberDataDTOMap = enrollmentService.getEnrollmentMemberDataDTO(enrollmentRequest);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setEnrollmentMemberDataDTOMap(enrollmentMemberDataDTOMap);
				}
				else{
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
			catch(Exception ex){
				LOGGER.error("ENROLLMENT_MEMBER_DATA:: Exception caught while processing enrollment member data", ex);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
				enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		else{
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	@RequestMapping(value="/getenrollmentcountbyssapid", method = RequestMethod.POST)
	@ResponseBody
	public String getCountOfHealthAndDentalByAppId(@RequestBody String inputRequest){
		
			EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
			Map<String, Integer> enrollmentCountMap = null;
			if(StringUtils.isNotEmpty(inputRequest))
			{
				try
				{
					EnrollmentRequest enrollmentRequest = platformGson.fromJson(inputRequest, EnrollmentRequest.class);
					if(enrollmentRequest != null && enrollmentRequest.getSsapApplicationId() != null)
					{
						enrollmentCountMap = enrollmentService.getAllEnrollmentDataBySsapApplicationId(enrollmentRequest.getSsapApplicationId());
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrollmentResponse.setEnrollmentCountMap(enrollmentCountMap);
					}
					else{
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_APP_ID_IS_NULL);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				} catch (Exception e) {
					LOGGER.error("COUNT_MAP : Exception occured while returing count map", e);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
					enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(e, EnrollmentConstants.FIVE));
				}
		}
			else{
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		return platformGson.toJson(enrollmentResponse);
	}
	
	@RequestMapping(value = "/getenrollmentsnapshot", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentSnapshot(@RequestBody String inputRequest) {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		List<EnrollmentDataDTO> enrollmentSnapshotList = null;
		if (StringUtils.isNotEmpty(inputRequest)) {
			try {
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(inputRequest, EnrollmentRequest.class);
				if (enrollmentRequest != null && enrollmentRequest.getEnrollmentId() != null) {
					enrollmentSnapshotList = enrollmentService.getEnrollmentSnapshot(enrollmentRequest);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setEnrollmentDataDTOList(enrollmentSnapshotList);
				} else {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_INPUT);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			} catch (Exception e) {
				LOGGER.error("ENROLLMENT_SNAPSHOT : Exception occured while returing enrollment snapshot", e);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
				enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(e, EnrollmentConstants.FIVE));
			}
		} else {
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	@RequestMapping(value = "/getAllSubscriberEvents", method = RequestMethod.POST)
	@ResponseBody
	public String getAllSubscriberEvents(@RequestBody String inputRequest) {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		List<EnrollmentEventHistoryDTO> enrollmentEventHistoryDTOList = null;
		if (StringUtils.isNotEmpty(inputRequest)) {
			try {
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(inputRequest, EnrollmentRequest.class);
				if (enrollmentRequest != null && enrollmentRequest.getEnrollmentId() != null) {
					enrollmentEventHistoryDTOList = enrollmentService.getAllSubscriberEvents(enrollmentRequest);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setEnrollmentEventHistoryDTOList(enrollmentEventHistoryDTOList);
				} else {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_207);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			} catch (Exception e) {
				LOGGER.error("ENROLLMENT_GET_ALL_SUBSCRIBER_EVENTS : Exception occured while returing enrollment subscriber events",e);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_250);
				enrollmentResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(e, EnrollmentConstants.FIVE));
			}
		} else {
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);
	}
	
	/**
	 * Jira Id: HIX-109786
	 * @author negi_s
	 * @since 09th Nov 2018
	 * @param externalCaseID List<String>
	 * @return
	 * Api to fetch Enrollment and Enrollee data by list of externalCaseID
	 */
	@RequestMapping(value = "/getenrollmentdatabyexternalcaseid", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public String getEnrollmentDataByExternalCaseIds(@RequestBody String requestData) {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		LOGGER.info("=========== started getenrollmentdatabyexternalcaseid ========");
		Gson enrollmentGson = EnrollmentUtils.enrollmentGson();
		LOGGER.debug("getenrollmentdatabyexternalcaseid request -" + requestData);
		if (StringUtils.isNotEmpty(requestData)) {
			try {
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(requestData, EnrollmentRequest.class);
				Map<String, List<EnrollmentDataDTO>> enrollmentDataDtoListMap = new HashMap<String, List<EnrollmentDataDTO>>();
				if (enrollmentRequest != null && enrollmentRequest.getExternalCaseIdList() != null
						&& !enrollmentRequest.getExternalCaseIdList().isEmpty()) {
					LOGGER.debug("getenrollmentdatabyexternalcaseid ExternalCaseIdList -" + enrollmentRequest.getExternalCaseIdList());
					enrollmentDataDtoListMap = enrollmentService.getEnrollmentDataByExternalCaseIds(enrollmentRequest);
					enrollmentResponse.setRenewalStatusMap(enrollmentService.getRenewalStatusMapByExternalCaseIds(enrollmentRequest.getExternalCaseIdList(), enrollmentRequest.getCoverageYear()));
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					if (enrollmentDataDtoListMap.isEmpty()) {
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_LINKED_HOUSEHOLD_FOUND);
					}
					enrollmentResponse.setEnrollmentDataDtoListMap(enrollmentDataDtoListMap);
				} else {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			} catch (GIException gie) {
				enrollmentResponse.setErrCode(gie.getErrorCode());
				enrollmentResponse.setErrMsg(gie.getErrorMsg());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in getenrollmentdatabyexternalcaseid API: ", ex);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + " :: "
						+ EnrollmentUtils.shortenedStackTrace(ex, 4));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} else {
			LOGGER.info("Received Null or Empty request for getenrollmentdatabyexternalcaseid");
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.debug("getenrollmentdatabyexternalcaseid response -"+ enrollmentGson.toJson(enrollmentResponse));
		return enrollmentGson.toJson(enrollmentResponse);
	}
	
	/**
	 * Jira Id: HIX-110099
	 * @author ajinkya_m
	 * @since 27th Nov 2018
	 * @param JSON String (EnrollmentMemberDTO)
	 * @return
	 * Api to Update Enrollment with provided SSAP Application id and return member ids
	 */
	@RequestMapping(value = "/updatessapapplicationid", method = RequestMethod.POST)
	@ResponseBody
	public String updateSsapApplicationId(@RequestBody String requestData){
		
		EnrollmentMemberDTO enrollmentResponse = new EnrollmentMemberDTO();
		LOGGER.info("started updatessapapplicationid");
		
		if (StringUtils.isNotEmpty(requestData)) {
			try {
				EnrollmentMemberDTO enrollmentMemberDto = platformGson.fromJson(requestData, EnrollmentMemberDTO.class);
				
				if (enrollmentMemberDto != null && enrollmentMemberDto.getEnrollmentId() != null
												&& enrollmentMemberDto.getSsapApplicationId() != null) {

					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setMemberIdSet(enrollmentService.updateSsapApplicationId(enrollmentMemberDto));
					enrollmentResponse.setEnrollmentId(enrollmentMemberDto.getEnrollmentId());
					enrollmentResponse.setSsapApplicationId(enrollmentMemberDto.getSsapApplicationId());

				} else {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				
			} catch (GIException gie) {
				enrollmentResponse.setErrCode(gie.getErrorCode());
				enrollmentResponse.setErrMsg(gie.getErrorMsg());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in getenrollmentdatabyexternalcaseid API: ", ex);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + " :: "
						+ EnrollmentUtils.shortenedStackTrace(ex, 4));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			
		}else {
			LOGGER.info("Received Null or Empty request for getenrollmentdatabyexternalcaseid");
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);
		
	}
	
	/**
	 * Jira Id: HIX-110239
	 * Api to get latest creation date of enrollment linked with provided SSAP Application id 
	 * @author negi_s
	 * @since 5th Dec 2018
	 * @return JSON String (EnrollmentResponse)
	 */
	@RequestMapping(value = "/getlatestenrollmentcreationtimebyssapid", method = RequestMethod.POST)
	@ResponseBody
	public String getLatestEnrollmentCreationTimeBySsapId(@RequestBody String requestData){

		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		LOGGER.info("Calling getLatestEnrollmentCreationTimeBySsapId API");

		if (StringUtils.isNotEmpty(requestData)) {
			try {
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(requestData, EnrollmentRequest.class);

				if (enrollmentRequest != null && enrollmentRequest.getSsapApplicationId() != null) {
					enrollmentResponse = enrollmentService.fetchLatestEnrollmentCreationTimeBySsapId(enrollmentRequest.getSsapApplicationId());

				} else {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SSAP_APP_ID_IS_NULL_OR_EMPTY);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}

			} catch (GIException gie) {
				enrollmentResponse.setErrCode(gie.getErrorCode());
				enrollmentResponse.setErrMsg(gie.getErrorMsg());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in getLatestEnrollmentCreationTimeBySsapId API: ", ex);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + " :: "
						+ EnrollmentUtils.shortenedStackTrace(ex, 4));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}

		}else {
			LOGGER.info("Received Null or Empty request for getLatestEnrollmentCreationTimeBySsapId");
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);

	}
	
	/**
	 * Jira Id: HIX-110394
	 * Api to get latest effective date of enrollment linked with provided SSAP Application id 
	 * @author negi_s
	 * @since 11th Dec 2018
	 * @return JSON String (EnrollmentResponse)
	 */
	@RequestMapping(value = "/getlatestenrollmenteffectivedatebyssapid", method = RequestMethod.POST)
	@ResponseBody
	public String getLatestEnrollmentEffectiveDateBySsapId(@RequestBody String requestData){

		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		LOGGER.info("Calling getLatestEnrollmentEffectiveDateBySsapId API");

		if (StringUtils.isNotEmpty(requestData)) {
			try {
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(requestData, EnrollmentRequest.class);

				if (enrollmentRequest != null && enrollmentRequest.getSsapApplicationId() != null) {
					enrollmentResponse = enrollmentService.fetchLatestEnrollmentEffectiveDateBySsapId(enrollmentRequest.getSsapApplicationId());

				} else {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SSAP_APP_ID_IS_NULL_OR_EMPTY);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}

			} catch (GIException gie) {
				enrollmentResponse.setErrCode(gie.getErrorCode());
				enrollmentResponse.setErrMsg(gie.getErrorMsg());
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in getLatestEnrollmentEffectiveDateBySsapId API: ", ex);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + " :: "
						+ EnrollmentUtils.shortenedStackTrace(ex, 4));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}

		}else {
			LOGGER.info("Received Null or Empty request for getLatestEnrollmentEffectiveDateBySsapId");
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentResponse);

	}
	
	/**
	 * Jira Id: HIX-110275
	 * Api to getplanhiosidforexternalmemberid
	 * @author Anoop
	 * @since 13th Feb 2019
	 * @return EnrollmentResponse
	 */
	@RequestMapping(value = "/getplanhiosidforexternalmemberid", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<EnrollmentResponse> getPlanHiosIdForExternalMemberId(@RequestBody String requestData) {

		LOGGER.info("Calling getplanhiosidforexternalmemberid API");
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (StringUtils.isNotEmpty(requestData)) {
			try {
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(requestData, EnrollmentRequest.class);
				if(EnrollmentUtils.isNotNullAndEmpty(enrollmentRequest.getCoverageYear()) && EnrollmentUtils.isNotNullAndEmpty(enrollmentRequest.getExternalMemberId())) {
					enrollmentResponse = enrollmentService.findHiosIdForEnrollment(enrollmentRequest.getCoverageYear(), enrollmentRequest.getExternalMemberId());
					return new ResponseEntity<>(enrollmentResponse,HttpStatus.OK);
				}else {
					LOGGER.error("Coverage year or external member ID missing in the request ::" + requestData);
					enrollmentResponse.setStatus(EnrollmentConstants.FAILURE);
					return new ResponseEntity<>(enrollmentResponse,HttpStatus.BAD_REQUEST);
				}
			} catch (GIException gie) {
				LOGGER.error("Exception in getplanhiosidforexternalmemberid API: ", gie);
				enrollmentResponse.setStatus(EnrollmentConstants.FAILURE);
				return new ResponseEntity<>(enrollmentResponse,HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in getplanhiosidforexternalmemberid API: ", ex);
				enrollmentResponse.setStatus(EnrollmentConstants.FAILURE);
				return new ResponseEntity<>(enrollmentResponse,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			enrollmentResponse.setStatus(EnrollmentConstants.FAILURE);
			return new ResponseEntity<>(enrollmentResponse,HttpStatus.BAD_REQUEST);
		}
	}
	
	private boolean isCompleteDisenrollmentRequest(EnrollmentCustomGroupDTO enrollmentCustomGroupDTO) {
		boolean isCompleteDisenrollmentRequest = true;
		for(PdOrderItemDTO pdOrderItemDTO : enrollmentCustomGroupDTO.getPdOrderResponse().getPdOrderItemDTOList()) {
			if(!pdOrderItemDTO.getDisenrollAllMembers()) {
				return false;
			}
		}
		return isCompleteDisenrollmentRequest;
	}
	
	/**
	 * HIX-112912 API to get active enrollments, all pending, confirmed enrollment.
	 * Plus terminated enrollment only if term date equal to system date
	 * @author negi_s
	 * @since 08-04-2019
	 * @param ssapApplicationRequest
	 * @return
	 */
	@RequestMapping(value = "/isactiveenrollmentforapplicationidwithoutlookback", method = RequestMethod.POST)
	@ResponseBody
	public boolean isActiveEnrollmentForApplicationIdWithoutLookBack(@RequestBody Long applicationId) {
		return enrollmentService.isActiveEnrollmentForApplicationId(applicationId, false);
	}
	
	
	@RequestMapping(value="/enrollmentsByMemberIds", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> getEnrollmentByMemberIdList(@RequestBody EnrollmentByMemberListRequestDTO inputRequest){
		
		if(isNotNullAndEmpty(inputRequest) || (null!=inputRequest.getMembers() && !(inputRequest.getMembers().isEmpty()))){
			try{
				boolean useMigratedEnrollmentsConfig= false;
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
						EnrollmentConfiguration.EnrollmentConfigurationEnum.USE_MIGRATION_ENROLLMENTS))) {
					useMigratedEnrollmentsConfig=true;
					LOGGER.info("Using Migrated Enrollment Data for the API call '/enrollmentsByMemberIds'");
				}
				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.set("dataFromMigratedEnrollments", Boolean.toString(useMigratedEnrollmentsConfig));
				 
				List<String> householdCaseIds= new ArrayList<String>();
				// Check the config , If useMigratedEnrollments is true then use externalMemberId otherwise use memberId's
				Map<String, List<EnrollmentWithMemberDataDTO>> enrollmentByMembersMap=null;
				if(!useMigratedEnrollmentsConfig)
				{
					Set<String> memberIds = 
						inputRequest.getMembers().stream()
					               .map(EnrollmentMembersDTO::getMemberId)
					               .collect(Collectors.toSet());
					
					EnrollmentRequest enrollmentRequest=new EnrollmentRequest();
					enrollmentRequest.setMemberIdList(new ArrayList(memberIds));
					enrollmentRequest.setCoverageYear(inputRequest.getCoverageYear());
					enrollmentRequest.setEnrollmentStatusList(inputRequest.getEnrollmentStatus());
					if(StringUtils.isNotBlank(inputRequest.getHouseholdCaseId()))
						householdCaseIds.add(inputRequest.getHouseholdCaseId());
					enrollmentRequest.setHouseHoldCaseIdList(householdCaseIds);
					enrollmentByMembersMap=enrollmentService.getEnrollmentDataForMembers(enrollmentRequest);
				}
				else
				{
				Set<String> externalMemberIds = 
						inputRequest.getMembers().stream().filter(e -> e.getExternalMemberId() != null)
					               .map(EnrollmentMembersDTO::getExternalMemberId)
					               .collect(Collectors.toSet());
					if (externalMemberIds == null || externalMemberIds.isEmpty()) {
						return new ResponseEntity("Invalid Request", HttpStatus.BAD_REQUEST);
					}
					enrollmentByMembersMap=extEnrollmentService.getEnrollmentDataForMembers(new ArrayList(externalMemberIds));
				
				}
				LOGGER.info(mapper.writeValueAsString(enrollmentByMembersMap));
				return new ResponseEntity(mapper.writeValueAsString(enrollmentByMembersMap),responseHeaders,HttpStatus.OK);
			}
			catch(Exception e)
			{
				LOGGER.info(e.getMessage());
				return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}
			return new ResponseEntity("Invalid Request",HttpStatus.BAD_REQUEST);	
			
	}
	
	private void setRequiredFieldForResponse(EnrollmentResponse enrollmentResponse) {
		List<Enrollment> filteredEnrollmentList = new ArrayList<>();
		List<Enrollment> enrollmentList = enrollmentResponse.getEnrollmentList();
		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			for (Enrollment enrollment : enrollmentList) {
				if (enrollment != null) {
					Enrollment enrObj = new Enrollment();
					enrObj.setId(enrollment.getId());
					enrObj.setPlanId(enrollment.getPlanId());
					enrObj.setPlanLevel(enrollment.getPlanLevel());
					enrObj.setIssuerId(enrObj.getIssuerId());
					enrObj.setIssuerLogo(enrollment.getIssuerLogo());
					enrObj.setGrossPremiumAmt(enrollment.getGrossPremiumAmt());
					enrObj.setEmployerContribution(enrollment.getEmployerContribution());
					enrObj.setAptcAmt(enrollment.getAptcAmt());
					enrObj.setStateSubsidyAmt(enrollment.getStateSubsidyAmt());
					enrObj.setInsuranceTypeLkp(enrollment.getInsuranceTypeLkp());
					enrObj.setBenefitEffectiveDate(enrollment.getBenefitEffectiveDate());
					enrObj.setBenefitEndDate(enrollment.getBenefitEndDate());
					enrObj.setInsurerName(enrollment.getInsurerName());
					enrObj.setPlanName(enrollment.getPlanName());
					enrObj.setFinancialEffectiveDate(enrollment.getFinancialEffectiveDate());
					enrObj.setNetPremiumAmt(enrollment.getNetPremiumAmt());
					filteredEnrollmentList.add(enrObj);
				}
			}
		}
		enrollmentResponse.setEnrollmentList(filteredEnrollmentList);
	}
	
	/**
	 * API returns enrollee information based on enrollment ID and change effective date
	 * @param enrollmentRequest
	 * @return
	 */
	@RequestMapping(value = "/getenrolleecoveragestartdatabyenrollmentidandchangedate", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrolleeExchgIndivIdentifierAndCoverageStartDateByEnrollmentIdAndDate(@RequestBody EnrollmentRequest enrollmentRequest) {
		LOGGER.info("Enrollment id received : " + enrollmentRequest.getEnrollmentId());
		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (!isNotNullAndEmpty(enrollmentRequest.getEnrollmentId())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
		} else if(!isNotNullAndEmpty(enrollmentRequest.getChangeEffectiveDate())){
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_CHANGE_EFFECTIVE_DATE_NULL_OR_EMPTY);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
		} else {
			Integer enrollmentId = Integer.valueOf(enrollmentRequest.getEnrollmentId());
			Date changeEffectiveDate  = DateUtil.StringToDate(enrollmentRequest.getChangeEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT);
			List<Enrollee> enrolleeList = enrolleeService.getEnrolleeExchgIndivIdentifierAndCoverageStartDateByEnrollmentIdAndChangeDate(enrollmentId, changeEffectiveDate);
			enrollmentResponse.setIsEnrollmentExists(true);
			enrollmentResponse.setEnrolleeList(enrolleeList);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			}
		
		XStream xstream = GhixUtils.getXStreamStaxObject();

		return xstream.toXML(enrollmentResponse);
	}
}
