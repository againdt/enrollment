
/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import java.util.List;

import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.enrollment.ApplicantExternalEnrollmentResponse;
import com.getinsured.hix.dto.enrollment.ExternalEnrollmentDTO;
import com.getinsured.hix.enrollment.service.ExternalEnrollmentService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.util.GhixConstants;


@Controller
@RequestMapping(value = "/enrollment/external")
public class ExternalEnrollmentController {

	private static final Logger LOGGER = Logger.getLogger(ExternalEnrollmentController.class);

	@Autowired private ExternalEnrollmentService externalEnrollmentService;
	
	
	
	/**
	 * @author  This method is used to test the working of enrollment
	 *         controller after deployment
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to Enrollment module (welcome()) ::");
		try {
			
			
		} catch (Exception e) {
			LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage() , e );
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause() , e);
		}
		
		return "Welcome to Enrollment module's External Enrollment Controller ";
	}
	
	

	@RequestMapping(value = "/enrollmentByApplicantId/{applicantId}", method = RequestMethod.GET)
	@ResponseBody
	public ApplicantExternalEnrollmentResponse findEnrollmentByExternalApplicantId(@PathVariable("applicantId") String applicantId) {
		LOGGER.info("============inside findEnrollmentByExternalApplicantId============");
		ApplicantExternalEnrollmentResponse enrollmentResponse = new ApplicantExternalEnrollmentResponse();
		try{
			if (applicantId.isEmpty()) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			} else {
				List<ExternalEnrollmentDTO>  externalEnrollments = externalEnrollmentService.getExternalEnrollmentDataByApplicantExternalId(applicantId);
				enrollmentResponse.setMemberId(applicantId);
				enrollmentResponse.setEnrollments(externalEnrollments);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			}
			return enrollmentResponse;
		}catch(Exception e){
			enrollmentResponse = new ApplicantExternalEnrollmentResponse();
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Unknown Error occuered in findEnrollmentByExternalApplicantId  : "+e.getMessage());
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			return enrollmentResponse;
		}
	}

}