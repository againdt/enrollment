package com.getinsured.hix.enrollment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.IssuerBrandNameResponseDTO;
import com.getinsured.hix.enrollment.dto.EnrlMissingCommissionRequest;
import com.getinsured.hix.enrollment.dto.EnrlMissingCommissionResponse;
import com.getinsured.hix.enrollment.dto.EnrollmentBobDTO;
import com.getinsured.hix.enrollment.dto.EnrollmentBobRequest;
import com.getinsured.hix.enrollment.dto.EnrollmentBobResponse;
import com.getinsured.hix.enrollment.dto.EnrollmentCommissionRequest;
import com.getinsured.hix.enrollment.dto.EnrollmentStarRequest;
import com.getinsured.hix.enrollment.dto.EnrollmentStarRequest.RenewalType;
import com.getinsured.hix.enrollment.dto.EnrollmentStarResponse;
import com.getinsured.hix.enrollment.dto.EnrollmentStarResponse.EnrollmentRenewalResponse;
import com.getinsured.hix.enrollment.dto.EnrollmentSubscriberDetailsDTO;
import com.getinsured.hix.enrollment.dto.EnrollmentUpdateDTO;
import com.getinsured.hix.enrollment.dto.LookupStarrRequest;
import com.getinsured.hix.enrollment.dto.LookupStarrResponse;
import com.getinsured.hix.enrollment.service.EnrollmentPrivateService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EnrollmentRenewals;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.enrollment.EnrollmentCommission;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.repository.TenantRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.google.gson.Gson;

/*@Api(value="EnrollmentAnonymousController", description="Enrollment Anonymous Controller")*/
@Controller
@RequestMapping(value = "/enrollment/anonymous")
public class EnrollmentAnonymousController {

	private static final Logger LOGGER = Logger.getLogger(EnrollmentAnonymousController.class);
	@Autowired private EnrollmentPrivateService enrollmentPrivateService;
	@Autowired private UserService userService;
	@Autowired private Gson platformGson;
	@Autowired private TenantRepository tenantRepository;
	
	/**
	 * 
	 * @param enrollmentBobRequest
	 * @return
	 */
	/*@ApiOperation(value="update enrollment for bob success", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/updateenrollmentforbobsuccess", method = RequestMethod.POST)
	@ResponseBody
	public String updateEnrollmentForBOBSuccess(@RequestBody String enrollmentBobRequestString){
		
		LOGGER.info("========= Inside updateEnrollmentForBOBSuccess Call ========");
		EnrollmentBobResponse enrollmentBobResponse = new EnrollmentBobResponse();
		try {
			if(StringUtils.isEmpty(enrollmentBobRequestString)){
				LOGGER.info("Received Null or Empty Request for updateEnrollmentForBOBSuccess call");
				enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentBobResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);			
			}
			else {
				EnrollmentBobRequest enrollmentBobRequest = platformGson.fromJson(enrollmentBobRequestString, EnrollmentBobRequest.class);
				if(enrollmentBobRequest != null && enrollmentBobRequest.getEnrollmentId() != null){
					enrollmentPrivateService.updateEnrollmentForBOBSuccess(enrollmentBobRequest);
					enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentBobResponse.setErrMsg("Updated Enrollment for BOB success request");
					enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
				else{
					enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentBobResponse.setErrMsg("Received null or Invalid request for updateEnrollmentForBOBSuccess call");
					enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}

		} catch (Exception ex) {
			LOGGER.error("Exception caught in 'updateenrollmentforbobsuccess' rest call : ", ex);
			enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			enrollmentBobResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentBobResponse);
	}
	
	/**
	 * 
	 * @param enrollmentBobRequest
	 * @return
	 */
	/*@ApiOperation(value="Fetch Enrollment based on BOB exact or fuzzy match logic", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/getenrollmentbybobrequest", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentByBOBRequest(@RequestBody String enrollmentBobRequestString) {
		LOGGER.info("========= inside getEnrollmentByBOBRequest rest call =========");
		EnrollmentBobResponse enrollmentBobResponse = new EnrollmentBobResponse();
		try{
			if(StringUtils.isEmpty(enrollmentBobRequestString)){
				enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentBobResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentBobRequest enrollmentBobRequest = platformGson.fromJson(enrollmentBobRequestString, EnrollmentBobRequest.class);
				if(enrollmentBobRequest != null){
					if(validateEnrollmentBOBRequest(enrollmentBobRequest)){
						enrollmentPrivateService.searchEnrollmentForBOBRequest(enrollmentBobRequest, enrollmentBobResponse);
						
						if(enrollmentBobResponse.getEnrollmentBobDTOList() == null || enrollmentBobResponse.getEnrollmentBobDTOList().isEmpty()){
							enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
							enrollmentBobResponse.setErrMsg("Unable to find Enrollment records for Given Request");
							enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						}
						else{
							enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
							enrollmentBobResponse.setErrMsg("Successfully retreived Enrollment record");
							enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						}
					}
					else{
						enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentBobResponse.setErrMsg("Received Invalid Request combinations, required request Combinations are"
								+ " InsuerName(Required) + (Policy Id OR CarrierAppId OR CarrierAppUid OR (FirstName and Lastname of scubscriber)");
						enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentBobResponse.setErrMsg("Received null or Invalid request for getEnrollmentByBOBRequest call");
					enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception caught in 'getEnrollmentByBOBRequest' rest call : ", ex);
			enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentBobResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentBobResponse,EnrollmentBobResponse.class);
	}
	
	
	/**
	 * 
	 * @param enrollmentBobRequest
	 * @return
	 */
	/*@ApiOperation(value="Fetch Enrollment based on BOB exact or fuzzy match logic", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/getenrollmentbybobmultilinerequest", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentByBOBMultilineRequest(@RequestBody String enrollmentBobRequestString) {
		LOGGER.info("========= inside getenrollmentbybobmultilinerequest rest call =========");
		EnrollmentBobResponse enrollmentBobResponse = new EnrollmentBobResponse();
		try{
			if(StringUtils.isEmpty(enrollmentBobRequestString)){
				enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentBobResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentBobRequest enrollmentBobRequest = platformGson.fromJson(enrollmentBobRequestString, EnrollmentBobRequest.class);
				if(enrollmentBobRequest != null){
					if(validateEnrollmentBOBRequest(enrollmentBobRequest)){
						enrollmentPrivateService.searchEnrollmentForBOBMultilineRequest(enrollmentBobRequest, enrollmentBobResponse);
					}
					else{
						enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentBobResponse.setErrMsg("Received Invalid Request combinations, required request Combinations are"
								+ " InsuerName(Required) + (Policy Id OR CarrierAppId OR CarrierAppUid OR (FirstName and Lastname of scubscriber)");
						enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrollmentBobResponse.setErrMsg("Received null or Invalid request for getEnrollmentByBOBRequest call");
					enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception caught in 'getEnrollmentByBOBRequest' rest call : ", ex);
			enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentBobResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentBobResponse,EnrollmentBobResponse.class);
	}
	
	
	/**
	 * HIX-81138
	 * @param enrollmentBobRequestString
	 * @return
	 */
	/*@ApiOperation(value="Fetch Enrollment and Subscriber details", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/getenrollmentandsubscriberdetails", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentAndSubscriberDetails(@RequestBody String enrollmentStarRequestString) {
		LOGGER.info("==============Inside getEnrollmentAndSubscriberDetails ===============");
		EnrollmentStarResponse enrollmentStarResponse = new EnrollmentStarResponse();
		enrollmentStarResponse.startResponse();
		try{
			if(StringUtils.isEmpty(enrollmentStarRequestString)){
				enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentStarResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentStarRequest enrollmentStarRequest = platformGson.fromJson(enrollmentStarRequestString, EnrollmentStarRequest.class);
				if(enrollmentStarRequest != null && enrollmentStarRequest.getEnrollmentIdList() != null && !enrollmentStarRequest.getEnrollmentIdList().isEmpty()){
					
					List<EnrollmentSubscriberDetailsDTO> enrollmentSubscriberDetailsDTOList = enrollmentPrivateService.getEnrollmentSubscriberDetails(enrollmentStarRequest.getEnrollmentIdList());
					if(enrollmentSubscriberDetailsDTOList != null && !enrollmentSubscriberDetailsDTOList.isEmpty()){
						enrollmentStarResponse.setEnrollmentSubscriberDetailsDTOList(enrollmentSubscriberDetailsDTOList);
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					}
					else{
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentStarResponse.setErrMsg("No Enrollment record found for given request");
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentStarResponse.setErrMsg("Received null or Invalid request for getEnrollmentAndSubscriberDetails call");
					enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred in getEnrollmentAndSubscriberDetails: ",ex);
			enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentStarResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		enrollmentStarResponse.endResponse();
		return platformGson.toJson(enrollmentStarResponse);
	}
	
	/*@ApiOperation(value="Fetch Enrollment by Policy Number", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/findenrollmentbypolicynumber", method = RequestMethod.POST)
	@ResponseBody
	public String findEnrollmentByPolicyNumber(@RequestBody String enrollmentStarRequestString) {
		LOGGER.info("==============Inside findEnrollmentByPolicyNumber ===============");
		EnrollmentStarResponse enrollmentStarResponse = new EnrollmentStarResponse();
		enrollmentStarResponse.startResponse();
		try{
			if(StringUtils.isEmpty(enrollmentStarRequestString)){
				enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentStarResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentStarRequest enrollmentStarRequest = platformGson.fromJson(enrollmentStarRequestString, EnrollmentStarRequest.class);
				if(enrollmentStarRequest != null && (StringUtils.isNotEmpty(enrollmentStarRequest.getPolicyNumber()) || StringUtils.isNotEmpty(enrollmentStarRequest.getHicn()))){
					
					List<EnrollmentSubscriberDetailsDTO> enrollmentSubscriberDetailsDTOList = enrollmentPrivateService.findEnrollmentByPolicyNumberHICN(enrollmentStarRequest);
					if(enrollmentSubscriberDetailsDTOList != null && !enrollmentSubscriberDetailsDTOList.isEmpty()){
						enrollmentStarResponse.setEnrollmentSubscriberDetailsDTOList(enrollmentSubscriberDetailsDTOList);
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					}
					else{
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentStarResponse.setErrMsg("No Enrollment record found for given request");
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentStarResponse.setErrMsg("Received null or Invalid request for findEnrollmentByPolicyNumber call");
					enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred in getEnrollmentAndSubscriberDetails: ",ex);
			enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentStarResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		enrollmentStarResponse.endResponse();
		return platformGson.toJson(enrollmentStarResponse);
	}
	
	/**
	 * Jira Id: HIX-79701   API to get BOB Expected Record
	 * @param enrollmentBobRequestString
	 * @return
	 */
	/*@ApiOperation(value="Fetch Enrollment records for BOB request", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/findbobexpectedenrollmentids", method = RequestMethod.POST)
	@ResponseBody
	public String findBobExpectedEnrollmentIds(@RequestBody String enrollmentBobRequestString) {
		
		LOGGER.info("========= inside findBobExpectedEnrollmentIds rest call =========");
		EnrollmentBobResponse enrollmentBobResponse = new EnrollmentBobResponse();
		enrollmentBobResponse.startResponse();
		try{
			if(StringUtils.isEmpty(enrollmentBobRequestString)){
				enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentBobResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentBobRequest enrollmentBobRequest = platformGson.fromJson(enrollmentBobRequestString, EnrollmentBobRequest.class);
				if(enrollmentBobRequest != null){
					if(validateEnrollmentBobExpectedRequest(enrollmentBobRequest)){
						List<EnrollmentBobDTO> enrollmentBobDtoList = null;
						String carrierName = enrollmentBobRequest.getCarrierName().toLowerCase();
						List<String> enrollmentStatusList  = new ArrayList<String>();
						for(EnrollmentBobRequest.EnrollmentStatus enrollmentStatus : enrollmentBobRequest.getEnrollmentStatusList()){
							enrollmentStatusList.add(enrollmentStatus.toString());
						}
						//enrollmentBobDTOList
						enrollmentBobDtoList = enrollmentPrivateService.findBobExpectedEnrollmentRecord(carrierName, enrollmentStatusList);
						if(enrollmentBobDtoList != null && !enrollmentBobDtoList.isEmpty()){
							enrollmentBobDtoList.removeAll(enrollmentBobRequest.getEnrollmentBobDtoList());
							
							enrollmentBobResponse.setEnrollmentBobDTOList(enrollmentBobDtoList);
							enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
							enrollmentBobResponse.setErrMsg("Successfully retreived Enrollment record");
							enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
							
						}
						else{
							enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
							enrollmentBobResponse.setErrMsg("Unable to find Enrollment records for Given Request");
							enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						}
					}
					else{
						enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentBobResponse.setErrMsg("Received Invalid Request combinations, required request Combinations are"
								+ " CarrierName(Required) + (EnrollmentIDs list AND Enrollment Status required)");
						enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentBobResponse.setErrMsg("Received null or Invalid request for findBobExpectedEnrollmentIds call");
					enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
			
		} catch (Exception ex) {
			LOGGER.error("Exception caught in 'findBobExpectedEnrollmentIds' rest call : ", ex);
			enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentBobResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		enrollmentBobResponse.endResponse();
		return platformGson.toJson(enrollmentBobResponse);
	}
	
	/**
	 * 
	 * @param enrollmentCommissionRequestString
	 * @return
	 */
	/*@ApiOperation(value="Save Enrollment commission", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/saveenrollmentcommission", method = RequestMethod.POST)
	@ResponseBody
	public String saveEnrollmentCommission(@RequestBody String enrollmentCommissionRequestString) {
		LOGGER.info("==============Inside findEnrollmentByPolicyNumber ===============");
		EnrollmentStarResponse enrollmentStarResponse = new EnrollmentStarResponse();
		enrollmentStarResponse.startResponse();
		try{
			if(StringUtils.isEmpty(enrollmentCommissionRequestString)){
				enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentStarResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentCommissionRequest enrollmentCommissionRequest = platformGson.fromJson(enrollmentCommissionRequestString, EnrollmentCommissionRequest.class);
				if(enrollmentCommissionRequest != null){
					EnrollmentCommission enrollmentCommission = enrollmentPrivateService.saveEnrollmentCommision(enrollmentCommissionRequest);
					if(enrollmentCommission != null && enrollmentCommission.getId() != null){
						enrollmentStarResponse.setEnrollmentCommissionId(enrollmentCommission.getId());
						enrollmentStarResponse.setEnrollmentCommissionSaveSuccess(Boolean.TRUE);
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						enrollmentStarResponse.setErrMsg("SUCCESSFULLY SAVED ENROLLMENT_COMMISSION...");
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					}
					else{
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentStarResponse.setErrMsg("Unable to save Enrollment_Commisssion");
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentStarResponse.setErrMsg("Received null or Invalid request for saveEnrollmentCommission call");
					enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception Occurred in saveenrollmentcommission API: ",ex);
			enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentStarResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		enrollmentStarResponse.endResponse();
		return platformGson.toJson(enrollmentStarResponse);
	}
	
	/**
	 * 
	 * @param enrollmentStarRequestString
	 * @return
	 */
	/*@ApiOperation(value="Enrollment Renewal API", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/enrollmentrenewal", method = RequestMethod.POST)
	@ResponseBody
	public String enrollmentRenewal(@RequestBody String enrollmentStarRequestString) {
		LOGGER.info("==============Inside findEnrollmentByPolicyNumber ===============");
		EnrollmentStarResponse enrollmentStarResponse = new EnrollmentStarResponse();
		enrollmentStarResponse.startResponse();
		try{
			if(StringUtils.isEmpty(enrollmentStarRequestString)){
				enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentStarResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentStarRequest enrollmentStarRequest = platformGson.fromJson(enrollmentStarRequestString, EnrollmentStarRequest.class);
				if(validateEnrollmentRenewalRequest(enrollmentStarRequest)){
					AccountUser user = findUserByEmail(enrollmentStarRequest.getLastUpdatedByUserEmail());
					if(user != null){
						EnrollmentRenewalResponse enrollmentRenewalResponse = null;
						if(enrollmentStarRequest.getRenewalType().equals(RenewalType.SINGLE_LINE) || enrollmentStarRequest.getRenewalType().equals(RenewalType.ROLL_OVER)){
							enrollmentRenewalResponse = enrollmentPrivateService.processSingleAndRollOverRowRenewal(
									enrollmentStarRequest.getEnrollmentId(),
									enrollmentStarRequest.getEnrollmentStatus().toString(),
									enrollmentStarRequest.getEnrollmentRenewalDTOList(), user, enrollmentStarRequest.getRenewalType().toString());
						}
						else{
							enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
							enrollmentStarResponse.setErrMsg("Invalid Renewal Type received in request");
							enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						}
						
						if(enrollmentRenewalResponse != null){
							enrollmentStarResponse.setEnrollmentRenewalResponse(enrollmentRenewalResponse);
							enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
							enrollmentStarResponse.setErrMsg("Success");
							enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						}
						
					}
					else{
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentStarResponse.setErrMsg("Invalid or No User account found for the given UserEmail in Request");
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentStarResponse.setErrMsg("Received null or Invalid request for enrollmentRenewal call");
					enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception Occurred in enrollmentRenewal: ",ex);
			enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentStarResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		enrollmentStarResponse.endResponse();
		return platformGson.toJson(enrollmentStarResponse);
	}
	
	
	@RequestMapping(value = "/getMissingCommissions", method = RequestMethod.POST)
	@ResponseBody
	public String getMissingCommissions(@RequestBody String request) {
		LOGGER.info("==============Inside getMissingCommissions ===============");
		EnrlMissingCommissionResponse response = new EnrlMissingCommissionResponse();
		try{
			EnrlMissingCommissionRequest requestDTO = platformGson.fromJson(request, EnrlMissingCommissionRequest.class);
			response = enrollmentPrivateService.getMissingCommissions(requestDTO);
			response.setStatus(EnrlMissingCommissionResponse.STATUS.SUCCESS);
		}catch(Exception e){
			LOGGER.error("ERROR in getMissingCommissions: " + e.getMessage(), e);
			response.setStatus(EnrlMissingCommissionResponse.STATUS.FAILURE);
			response.setErrMsg(e.getMessage());
		}
		return platformGson.toJson(response);
	}
	
	@RequestMapping(value="/saveEnrollmentDetails",method = RequestMethod.POST)
	@ResponseBody public String saveEnrollmentDetails(@RequestBody String enrollmentUpdateRequestString){
		LOGGER.info("==============Inside saveEnrollmentDetails ===============");
		EnrollmentStarResponse enrollmentStarResponse = new EnrollmentStarResponse();
		enrollmentStarResponse.startResponse();
		try{
			if(StringUtils.isEmpty(enrollmentUpdateRequestString)){
				enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentStarResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else{
				EnrollmentUpdateDTO enrollmentUpdateDTO = platformGson.fromJson(enrollmentUpdateRequestString, EnrollmentUpdateDTO.class);
				if(validateEnrollmentUpdateRequest(enrollmentUpdateDTO)){
					enrollmentPrivateService.updateEnrollmentForFeed(enrollmentUpdateDTO);
					enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentStarResponse.setErrMsg("Updated Enrollment for BOB success request");
					enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
			}
		}catch(Exception ex){
			enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentStarResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		enrollmentStarResponse.endResponse();
		return platformGson.toJson(enrollmentStarResponse);
	}
	
	@RequestMapping(value="/saveEnrollmentRenewalDetails",method = RequestMethod.POST)
	@ResponseBody public String saveEnrollmentRenewalDetails(@RequestBody String enrollmentBobRequestString){
		LOGGER.info("==============Inside saveEnrollmentDetails ===============");
		EnrollmentBobResponse enrollmentBobResponse = new EnrollmentBobResponse();
		enrollmentBobResponse.startResponse();
		try{
			if(StringUtils.isEmpty(enrollmentBobRequestString)){
				enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentBobResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else{
				EnrollmentBobRequest enrollmentBobRequest = platformGson.fromJson(enrollmentBobRequestString, EnrollmentBobRequest.class);
				if(validateEnrollmentRenewalUpdateRequest(enrollmentBobRequest)){
					enrollmentPrivateService.updateEnrollmentRenewalsForFeed(enrollmentBobRequest.getEnrollmentRenewalDTOs(),enrollmentBobRequest.getEnrollmentId());
					enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
			}
		}catch(Exception ex){
			enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentBobResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		enrollmentBobResponse.endResponse();
		return platformGson.toJson(enrollmentBobResponse);
	}
	
	
	private boolean validateEnrollmentRenewalUpdateRequest(EnrollmentBobRequest enrollmentBobRequest) {
		boolean isValidRequest = Boolean.TRUE;
		if(enrollmentBobRequest == null){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentBobRequest != null && enrollmentBobRequest.getEnrollmentId() == null)){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentBobRequest.getEnrollmentRenewalDTOs() == null ||
				(enrollmentBobRequest.getEnrollmentRenewalDTOs() != null && enrollmentBobRequest.getEnrollmentRenewalDTOs().isEmpty()))){
			isValidRequest = Boolean.FALSE;
		}
		return isValidRequest;
	}

	/**
	 * 
	 * @param enrollmentStarRequest
	 * @return
	 */
	private boolean validateEnrollmentRenewalRequest(EnrollmentStarRequest enrollmentStarRequest){
		boolean isValidRequest = Boolean.TRUE;
		if(enrollmentStarRequest == null){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && StringUtils.isEmpty(enrollmentStarRequest.getLastUpdatedByUserEmail())){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentStarRequest != null && enrollmentStarRequest.getEnrollmentId() == null)){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentStarRequest != null && enrollmentStarRequest.getRenewalType() == null)){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentStarRequest != null && enrollmentStarRequest.getEnrollmentStatus() == null)){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentStarRequest != null && 
				(enrollmentStarRequest.getEnrollmentRenewalDTOList() == null) ||
				(enrollmentStarRequest.getEnrollmentRenewalDTOList() != null && enrollmentStarRequest.getEnrollmentRenewalDTOList().isEmpty()))){
			isValidRequest = Boolean.FALSE;
		}
		return isValidRequest;
	}
	
	private boolean validateEnrollmentUpdateRequest(EnrollmentUpdateDTO enrollmentUpdateDTO){
		boolean isValidRequest = Boolean.TRUE;
		if(enrollmentUpdateDTO == null){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && StringUtils.isEmpty(enrollmentUpdateDTO.getLastUpdatedByUserEmail())){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentUpdateDTO != null && enrollmentUpdateDTO.getEnrollmentId() == null)){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentUpdateDTO != null && 
				(enrollmentUpdateDTO.getBenefitEffectiveDate() == null) && (enrollmentUpdateDTO.getVerificationEvent() == null))){
			isValidRequest = Boolean.FALSE;
		}
		return isValidRequest;
	}
	
	
	/**
	 * 
	 * @param enrollmentBobRequest
	 * @return
	 */
	private boolean validateEnrollmentBobExpectedRequest(EnrollmentBobRequest enrollmentBobRequest){
		boolean isValidRequest = Boolean.TRUE;
		if(StringUtils.isEmpty(enrollmentBobRequest.getCarrierName())){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentBobRequest.getEnrollmentBobDtoList()  == null || (enrollmentBobRequest.getEnrollmentBobDtoList() == null 
				&& enrollmentBobRequest.getEnrollmentBobDtoList().isEmpty()))){
			isValidRequest = Boolean.FALSE;
		}
		if(isValidRequest && (enrollmentBobRequest.getEnrollmentStatusList() == null || (enrollmentBobRequest.getEnrollmentStatusList() == null 
				&& enrollmentBobRequest.getEnrollmentStatusList().isEmpty()))){
			isValidRequest = Boolean.FALSE;
		}	
		return isValidRequest;
	}
	
	
	/**
	 *  
	 * @param enrollmentBobRequest
	 * @return
	 */
	private boolean validateEnrollmentBOBRequest(EnrollmentBobRequest enrollmentBobRequest){
		
		boolean isValidRequest = Boolean.TRUE;
		if(StringUtils.isEmpty(enrollmentBobRequest.getTenantCode())){
			isValidRequest = Boolean.FALSE;
		}else{
			Tenant tenant = tenantRepository.findByCode(enrollmentBobRequest.getTenantCode());
			if(tenant == null){
				isValidRequest = Boolean.FALSE;
				LOGGER.error("Invalid Tenant Code in request " + enrollmentBobRequest.getTenantCode());
			}else if(!enrollmentBobRequest.getTenantCode().equals(TenantContextHolder.getTenant().getCode())){
				isValidRequest = Boolean.FALSE;
				LOGGER.error("Given Tenant is mismatch with active Tenant of enrollment service " + enrollmentBobRequest.getTenantCode() + " - " + TenantContextHolder.getTenant().getCode());
			}
		}
		if(isValidRequest && StringUtils.isEmpty(enrollmentBobRequest.getCarrierName())){
			isValidRequest = Boolean.FALSE;
		}
		
		if(isValidRequest && !(StringUtils.isNotEmpty(enrollmentBobRequest.getPolicyId()) ||
				StringUtils.isNotEmpty(enrollmentBobRequest.getCarrierAppId()) ||
				StringUtils.isNotEmpty(enrollmentBobRequest.getCarrierAppUid()) || 
				(StringUtils.isNotEmpty(enrollmentBobRequest.getFirstName()) && StringUtils.isNotEmpty(enrollmentBobRequest.getLastName())))){
			isValidRequest = Boolean.FALSE;
		}
		return isValidRequest;
	}
	
	
	private AccountUser findUserByEmail(String email){
		return userService.findByEmail(email);
	}
	
	/**
	 * 
	 * @param enrollmentBobRequest
	 * @return
	 */
	/*@ApiOperation(value="Fetch Enrollment Renewal latest benefit date", notes="", httpMethod="POST", produces="text/Json")*/
	@RequestMapping(value = "/getenrollmentRenewalLatestBenefitDate", method = RequestMethod.POST)
	@ResponseBody
	public String getenrollmentRenewalLatestBenefitDate(@RequestBody String enrollmentBobRequestString) {
		LOGGER.info("========= inside getenrollmentRenewalLatestBenefitDate rest call =========");
		EnrollmentBobResponse enrollmentBobResponse = new EnrollmentBobResponse();
		try{
			if(StringUtils.isEmpty(enrollmentBobRequestString)){
				enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentBobResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentBobRequest enrollmentBobRequest = platformGson.fromJson(enrollmentBobRequestString, EnrollmentBobRequest.class);
				if(enrollmentBobRequest != null){
					if(enrollmentBobRequest.getEnrollmentId() != null){
						EnrollmentRenewals enrollmentRenewals = enrollmentPrivateService.findByEnrollmentIdWithLatestBenefitStartDate(enrollmentBobRequest.getEnrollmentId());
						
						if(enrollmentRenewals != null && enrollmentRenewals.getBenefitStartDate()  != null){
							EnrollmentBobDTO enrollmentBobDTO = new EnrollmentBobDTO();
							enrollmentBobDTO.setBenefitEffectiveDate(enrollmentRenewals.getBenefitStartDate());
							
							List<EnrollmentBobDTO> enrollmentBobDTOList = new ArrayList<EnrollmentBobDTO>();
							enrollmentBobDTOList.add(enrollmentBobDTO);
							enrollmentBobResponse.setEnrollmentBobDTOList(enrollmentBobDTOList);
							
						}
						enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						enrollmentBobResponse.setErrMsg("Successfully retreived Enrollment Renewal record");
						enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						
						
					}
					else{
						enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentBobResponse.setErrMsg("Received Invalid Request combinations. Enrollment Id Missing");
						enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentBobResponse.setErrMsg("Received null or Invalid request for getEnrollmentByBOBRequest call");
					enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception caught in 'getenrollmentRenewalLatestBenefitDate' rest call : ", ex);
			enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentBobResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(enrollmentBobResponse);
	}
	
	@RequestMapping(value = "/findenrollmentbybrandname", method = RequestMethod.POST)
	@ResponseBody
	public String findenrollmentbybrandname(@RequestBody String enrollmentStarRequestString) {
		LOGGER.info("==============Inside findenrollmentbybrandname ===============");
		EnrollmentStarResponse enrollmentStarResponse = new EnrollmentStarResponse();
		enrollmentStarResponse.startResponse();
		try{
			if(StringUtils.isEmpty(enrollmentStarRequestString)){
				enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentStarResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				EnrollmentStarRequest enrollmentStarRequest = platformGson.fromJson(enrollmentStarRequestString, EnrollmentStarRequest.class);
				if(enrollmentStarRequest != null && enrollmentStarRequest.getIssuerIds() != null && !enrollmentStarRequest.getIssuerIds().isEmpty()){
					Set<Integer> matchedIssuerIds = enrollmentPrivateService.setMatchedIssuerIds(enrollmentStarRequest.getIssuerIds(), enrollmentStarRequest.getCarrierName());
					
					if(matchedIssuerIds != null && !matchedIssuerIds.isEmpty()){
						enrollmentStarResponse.setIssuerIds(matchedIssuerIds);
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					}
					else{
						enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrollmentStarResponse.setErrMsg("No Enrollment record found for given request");
						enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentStarResponse.setErrMsg("Received null or Invalid request for findenrollmentbybrandname call");
					enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred in findenrollmentbybrandname: ",ex);
			enrollmentStarResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentStarResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			enrollmentStarResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		enrollmentStarResponse.endResponse();
		return platformGson.toJson(enrollmentStarResponse);
		
	}
	
	@RequestMapping(value = "/findlookupidsbylookupcode", method = RequestMethod.POST)
	@ResponseBody
	public String findLookupIDsByLookupCode(@RequestBody String enrollmentStarRequestString) {
		LOGGER.info("==============Inside findLookupIDsByLookupCode ===============");
		
		LookupStarrResponse lookupStarrResponse = new LookupStarrResponse();
		
		try{
			if(StringUtils.isEmpty(enrollmentStarRequestString)){
				lookupStarrResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				lookupStarrResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else{
				
				LookupStarrRequest lookupStarrRequest = platformGson.fromJson(enrollmentStarRequestString, LookupStarrRequest.class);
				if(lookupStarrRequest != null && StringUtils.isNotEmpty(lookupStarrRequest.getLookupTypeName()) && lookupStarrRequest.getLookupValueCodes().size() > 0){
					
				 Map<Integer,String> lookupValueDetails = enrollmentPrivateService.getLookupValueIdsByNames(lookupStarrRequest.getLookupTypeName(), lookupStarrRequest.getLookupValueCodes());
				 lookupStarrResponse.setLookupValueDetails(lookupValueDetails);
				 lookupStarrResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}else{
					lookupStarrResponse.setErrMsg("Received null or Invalid request for findLookupIDsByLookupCode call");
					lookupStarrResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occurred in findLookupIDsByLookupCode: ",ex);
			lookupStarrResponse.setErrMsg(EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
			lookupStarrResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		
		return platformGson.toJson(lookupStarrResponse);
	}
}
