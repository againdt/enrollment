/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.enrollment.EnrolleeECommittedDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentApplicationIdDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentECommittedDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentListWrapperDTO;
import com.getinsured.hix.dto.offexchange.OffExchangeRequest;
import com.getinsured.hix.dto.offexchange.OffExchangeResponse;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.enrollment.service.EnrollmentOffExchangeConfirmEmailNotificationService;
import com.getinsured.hix.enrollment.service.EnrollmentOffExchangeService;
import com.getinsured.hix.enrollment.service.EnrollmentPrivateService;
import com.getinsured.hix.enrollment.service.ExitSurveyEmailNotificationService;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.consumer.HouseholdEnrollment;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;
import com.getinsured.hix.model.ssap.ConsumerApplication;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

/**
 * @author raja ramesh
 * @since 22/11/2013
 * 
 * This class handles all private service requests  
 */

/*@Api(value="enrollment-Off-Exchange-Controller", description="Enrollment Off Exchange Controller")*/
@Controller
@RequestMapping(value="/offexchange")
public class EnrollmentOffExchangeController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentOffExchangeController.class);
	
	@Autowired private EnrollmentOffExchangeService enrollmentOffExchangeService;
	@Autowired private EnrollmentPrivateService enrollmentService;
	@Autowired private RestTemplate restTemplate;
//	@Autowired private EnrollmentUserPrivateService enrollmentUserService;
	@Autowired private EnrollmentOffExchangeConfirmEmailNotificationService enrlOffExchangeEmailNotificationService;
	@Autowired private ExitSurveyEmailNotificationService exitSurveyEmailNotificationService;
	//@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	
	/*@ApiOperation(value="Welcome", notes="", httpMethod="GET", produces="text/plain")*/
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {

		LOGGER.info("Welcome to offexchange welcome() ::");
		
		return "Welcome to offexchange welcome()";
	}
	
	/*@ApiOperation(value="Save Ecommitted Enrollment", notes="", httpMethod="POST", produces="application/xml")*/
	@RequestMapping(value="/saveecommittedenrollment",method = RequestMethod.POST)
	@ResponseBody public String saveEcommittedEnrollment(@RequestBody EnrollmentListWrapperDTO enrollmentListWrapperDTO){
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String response = null;
		EnrollmentResponse enrollmentResponse=new EnrollmentResponse ();
		try{
			enrollmentResponse= enrollmentOffExchangeService.saveEnrollment(enrollmentListWrapperDTO);
			if (enrollmentResponse!=null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				List<Enrollment> enrollmentList = enrollmentResponse.getEnrollmentList();
				if(enrollmentList != null){
					for(Enrollment enrollment:enrollmentList){
						if(enrollment.getId()!=null && enrollment.getId()>0){
							setHouseholdAndEnrollmentMapping(enrollment);
						}
					}
				}
			}else{
				LOGGER.error("Problem occured while creating enrollment on saveecommittedenrollment");
			}
			response = xstream.toXML(enrollmentResponse);	
		}catch (Exception e){
			LOGGER.error("Problem occured while creating enrollment on saveecommittedenrollment"+e.getMessage());
			enrollmentResponse=new EnrollmentResponse ();
			enrollmentResponse.setErrMsg(EnrollmentUtils.getExceptionMessage(e));
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
			response = xstream.toXML(enrollmentResponse);
		}
			
		return response;
	}
	
	

	/** This method returns the EnrollmentECommittedDTO taking input as ticket id.   
	 * @author rajaramesh_g
	 * @param ticketId
	 * @return EnrollmentECommittedDTO
	 */
	/*@ApiOperation(value="Get Ecommitted Enrollment", notes="", httpMethod="POST", produces="application/xml")*/
	@RequestMapping(value="/getecommittedenrollment",method = RequestMethod.POST)
	@ResponseBody public String getEcommittedEnrollment(@RequestBody String enrollmentId){
		
		LOGGER.info("============inside getEcommittedEnrollment============");
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse= new EnrollmentResponse(); 
				
		if(isNotNullAndEmpty(enrollmentId)) {
			
			Enrollment enrollmentObj = enrollmentOffExchangeService.findEnrollmentById(Integer.valueOf(enrollmentId));
			
			if(isNotNullAndEmpty(enrollmentObj)){
			    enrollmentResponse.setEnrollmentECommittedDto(populateEnrollmentEComiitedDTO(enrollmentObj));
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
			else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_ID + "::" +enrollmentId);
			}
			
		}else{
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_INVALID_ENROLLMENT_ID+ "::" +enrollmentId);
		}
				
		return xstream.toXML(enrollmentResponse);
	}
	
	/** This method updates the ECommitted Enrollment taking input as OffExchangeRequest.   
	 * @author rajaramesh_g
	 * @param OffExchangeRequest
	 * @return OffExchangeResponse
	 */
	/*@ApiOperation(value="Update Ecommitted Enrollment", notes="", httpMethod="POST", produces="application/xml")*/
	@RequestMapping(value="/updateecommittedenrollment",method = RequestMethod.POST)
	@ResponseBody public String updateEcommittedEnrollment(@RequestBody String offExchangeRequestStr){
		
		LOGGER.info("============inside UpdateEcommittedEnrollment============");
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		OffExchangeResponse offExchangeResponse= new OffExchangeResponse(); 
				
		if(isNotNullAndEmpty(offExchangeRequestStr)) {
			
			OffExchangeRequest offExchangeRequest = (OffExchangeRequest) xstream.fromXML(offExchangeRequestStr);
			
			if(!isNotNullAndEmpty(offExchangeRequest.getTicketId())){
				offExchangeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			    offExchangeResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_INVALID_TICKET_ID+ "::" +offExchangeRequest.getTicketId());
			
			}else if(!isNotNullAndEmpty(offExchangeRequest.getStatus())){
				offExchangeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			    offExchangeResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ENROLLMENT_STATUS_NULL_OR_EMPTY+ "::" +offExchangeRequest.getStatus());

			}else{	 
				//System.out.println("*************************APP ID is: "+offExchangeRequest.getAppID());
				Enrollment enrollmentObj = enrollmentOffExchangeService.updateEcommittedEnrollment(offExchangeRequest.getTicketId(), offExchangeRequest.getStatus(), offExchangeRequest.getCarrierConfirmationNumber(),offExchangeRequest.getAppID());
				if(isNotNullAndEmpty(enrollmentObj)){
					//Setting enrollment elig lead stage to ENROLLMENT COMPLETED
					try{
						//setEligLeadStage(offExchangeRequest.getEligLeadId(),enrollmentObj.getIndOrderItem().getId());
						updateApplicationStage(enrollmentObj.getSsapApplicationid());
						//						updatePlanOrderStatus(enrollmentObj.getIndOrderItem().getId());
					}catch(Exception e){
						LOGGER.error("Error Updating Stage !" , e);
					}
					//Sending email to consumer 
					//HIX-44921 : Temporarily stopping STM confirmation email
					//HIX-50912 : Reactivating STM mails again
					
					if (isNotNullAndEmpty(enrollmentObj.getInsuranceTypeLkp())
							&& !((EnrollmentPrivateConstants.AME_LOOKUP_VALUE_CODE)
									.equalsIgnoreCase(enrollmentObj
											.getInsuranceTypeLkp()
											.getLookupValueCode()) || (EnrollmentPrivateConstants.DENTAL_LOOKUP_VALUE_CODE)
									.equalsIgnoreCase(enrollmentObj
											.getInsuranceTypeLkp()
											.getLookupValueCode()))) {
						sendEmailToConsumer(enrollmentObj.getCmrHouseHoldId(),enrollmentObj,offExchangeRequest.isIndividualMember());
					}
					
					offExchangeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					offExchangeResponse.setErrMsg(EnrollmentPrivateConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);

				   }
				   else{
					   offExchangeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					   offExchangeResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_NO_RESULTS_FOUND);
				   }
			}
			
		}			
		return xstream.toXML(offExchangeResponse);
	}
	
	/*@ApiOperation(value="Find enrollments by CMR household ID", notes="", httpMethod="POST", produces="application/xml")*/
	@RequestMapping(value="/getenrollmentbycmrhouseholdid",method = RequestMethod.POST)
	@ResponseBody public String getEnrollmentsByCmrHouseholdID(@RequestBody String cmrHouseholdId){
		LOGGER.info("============inside getEnrollmentsByCmrHouseholdID============");

		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse= new EnrollmentResponse(); 
		List<EnrollmentApplicationIdDTO> enrollmentApplicationIdDTOList=new ArrayList<EnrollmentApplicationIdDTO>();
		List<Enrollment> enrollmentList = enrollmentService.findEnrollmentByCmrHouseholdId(Integer.valueOf(cmrHouseholdId));
		if(!enrollmentList.isEmpty()){
			for(Enrollment enr : enrollmentList){
				EnrollmentApplicationIdDTO enrollmentApplicationIdDTO=new EnrollmentApplicationIdDTO();
				enrollmentApplicationIdDTO.setEnrollmentId(enr.getId());
				enrollmentApplicationIdDTO.setSsapApplicationId(enr.getSsapApplicationid());
				enrollmentApplicationIdDTOList.add(enrollmentApplicationIdDTO);
			}
			enrollmentResponse.setEnrollmentApplicationIdDTOList(enrollmentApplicationIdDTOList);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
		else{
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("No Enrollments found for the given CMR_HOUSEHOLD_ID: "+cmrHouseholdId);
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	/*@RequestMapping(value="/testSsapAppUpdate",method = RequestMethod.GET)
	@ResponseBody public String updateTest(){
		updateApplicationStage(4432);
		return "Success"; 
	}
	@RequestMapping(value="/test",method = RequestMethod.GET)
	@ResponseBody public String test(){
		String _GET_RESP =  ghixRestTemplate.getForObject(EnrollmentEndPoints.GET_ENROLLMENT_BY_SSAP_APPLICATION_ID + 7636 , String.class);
		System.out.println(_GET_RESP);
		return "Success"; 
	}*/
	
	/*@ApiOperation(value="Find enrollments by SSAP application ID", notes="", httpMethod="GET", produces="application/xml")*/
	@RequestMapping(value="/findenrollmentsbyssapid/{ssapApplicationId}",method = RequestMethod.GET)
	@ResponseBody public String findEnrollmentsBySsapApplicationId(@PathVariable("ssapApplicationId") String ssapApplicationId){
		LOGGER.info("============inside findEnrollmentsBySsapApplicationId============");
		LOGGER.info("Received cmr ssapApplicationId  : " + ssapApplicationId);
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (isNotNullAndEmpty(ssapApplicationId)
				&& !ssapApplicationId.equalsIgnoreCase("0")) {
			List<Enrollment> enrollments = enrollmentOffExchangeService.findEnrollmentBySsapApplicationId(Long.parseLong(ssapApplicationId));
			enrollmentResponse.setEnrollmentList(enrollments);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} else {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse
					.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		return xstream.toXML(enrollmentResponse);
	}
	
	/*@ApiOperation(value="Abort enrollment by SSAP application ID", notes="", httpMethod="POST", produces="application/xml")*/
	@RequestMapping(value="/abortenrollmentbyssapid",method = RequestMethod.POST)
	@ResponseBody public String abortEnrollmentBySsapApplicationId(@RequestBody String ssapApplicationId){
	  LOGGER.info("============inside abortEnrollmentBySsapApplicationId============");
	  LOGGER.info("Received ssapApplicationId  : " + ssapApplicationId);
	  XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
	  EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
	  if (isNotNullAndEmpty(ssapApplicationId)
	    && !ssapApplicationId.equalsIgnoreCase("0")) {
	   List<Enrollment> enrollments = enrollmentOffExchangeService.findEnrollmentBySsapApplicationId(Long.parseLong(ssapApplicationId));
	   for(Enrollment enr : enrollments){
	    enrollmentService.abortEnrollment(enr);
	   }
	   enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
	  } else {
	   enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
	   enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
	  }
	  return xstream.toXML(enrollmentResponse);
	}

	private void sendEmailToConsumer(Integer cmrHouseHoldId, Enrollment enrollmentObj, boolean isIndividualMember) {
		Household household = null;
		try{
			LOGGER.info("Getting Household from CmrHouseHoldId: " + cmrHouseHoldId);
			household = enrollmentExternalRestUtil.getHousehold(cmrHouseHoldId);
			LOGGER.info("Sending email to consumer ::" + household.getEmail());
			enrlOffExchangeEmailNotificationService.sendEmailNotification(household, enrollmentObj, isIndividualMember);
			String exitSurveyEmail = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXIT_SURVEY_EMAIL);
			if(exitSurveyEmail == null || "ON".equalsIgnoreCase(exitSurveyEmail)) {
				exitSurveyEmailNotificationService.sendEmailNotification(household);
			}
		}catch(Exception e){
			LOGGER.error("Exception while sending email notification: " ,e);
		}

	}

	/*
	  private void setEligLeadStage(long eligLeadId, int indOrderItemId) {

		EligLead eligLead =null;
		if (isNotNullAndEmpty(eligLeadId)) {
			eligLead=restTemplate.postForObject(GhixEndPoints.PhixEndPoints.GET_LEAD, eligLeadId, EligLead.class);
		}
		if(eligLead!=null){
			try{
				eligLead.setStage(STAGE.ENROLLMENT_COMPLETED);
				restTemplate.postForObject(GhixEndPoints.PhixEndPoints.SAVE_LEAD,eligLead, EligLead.class);

				String orderId=null;
				try {
					*//**
					 * Make a DB call to get the orderId from DB by passing enrollment obj indv_order_id
					 *//*
					if(isNotNullAndEmpty(enrollmentOffExchangeService.getOrderIdFromIndvOrderItemId(indOrderItemId))){
						orderId=enrollmentOffExchangeService.getOrderIdFromIndvOrderItemId(indOrderItemId).toString();
						PlanDisplayResponse pldResponse = restTemplate.postForObject(GhixEndPoints.PlandisplayEndpoints.UPDATE_ORDER_STATUS,orderId, PlanDisplayResponse.class);
						LOGGER.debug("Plandisplay OrderConfirmApi Response: "+ pldResponse.getStatus());

						if (pldResponse.getStatus() != null&& pldResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
							LOGGER.debug("Plandisplay OrderConfirmApi SUCCESS for Order id: "+ orderId);
						} else {
							LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_PLAN_DISPLAY_ORDER_CONFIRM_API_FAIL+ orderId);
						}
					}
					else{
						LOGGER.error("Order ID received is null or empty :"+ orderId);
					}
				} catch (Exception e) {
					// This catch is added so as to continue even if "PlanDisplay Update_Orde_Status API" fails. (As per plan display team. Mail dated 30/07/2013.)
					LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_PLAN_DISPLAY_ORDER_CONFIRM_API_FAIL+ orderId);
					LOGGER.error("Exception :: " + e.getMessage() , e);
				}
			}catch(Exception e){
				LOGGER.error("Problem in calling SAVE_LEAD from Enrollment", e);
			}
		}
	}*/

	private void updateApplicationStage(long ssapApplicationId){
		
		LOGGER.info("UPDATE SSAP APPLICATION STAGE updateApplicationStage()");
		ConsumerApplication ssapApplication = new ConsumerApplication();
		ssapApplication.setId(ssapApplicationId);
		ssapApplication.setStage(STAGE.ENROLLMENT_COMPLETED);
		int ssapApplicationResponse = restTemplate.postForObject(ConsumerServiceEndPoints.UPDATE_STAGE_BY_SSAP_APPLICATION_BY_ID,ssapApplication, Integer.class);
		LOGGER.info("SSAP APPLICATION RESPONSE ::" + ssapApplicationResponse);
		
	}
	
//	private void updatePlanOrderStatus(int indOrderItemId) {
//		String orderId=null;
//		try {
//			/**
//			 * Make a DB call to get the orderId from DB by passing enrollment obj indv_order_id
//			 */
//			if(isNotNullAndEmpty(enrollmentOffExchangeService.getOrderIdFromIndvOrderItemId(indOrderItemId))){
//				orderId=enrollmentOffExchangeService.getOrderIdFromIndvOrderItemId(indOrderItemId).toString();
//				PlanDisplayResponse pldResponse = restTemplate.postForObject(GhixEndPoints.PlandisplayEndpoints.UPDATE_ORDER_STATUS,orderId, PlanDisplayResponse.class);
//				LOGGER.debug("Plandisplay OrderConfirmApi Response: "+ pldResponse.getStatus());
//
//				if (pldResponse.getStatus() != null&& pldResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
//					LOGGER.debug("Plandisplay OrderConfirmApi SUCCESS for Order id: "+ orderId);
//				} else {
//					LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_PLAN_DISPLAY_ORDER_CONFIRM_API_FAIL+ orderId);
//				}
//			}
//			else{
//				LOGGER.error("Order ID received is null or empty :"+ orderId);
//			}
//		} catch (Exception e) {
//			// This catch is added so as to continue even if "PlanDisplay Update_Orde_Status API" fails. (As per plan display team. Mail dated 30/07/2013.)
//			LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_PLAN_DISPLAY_ORDER_CONFIRM_API_FAIL + orderId);
//			LOGGER.error("Exception ::" + e.getMessage() , e);
//		}
//	}

	private EnrollmentECommittedDTO populateEnrollmentEComiitedDTO(Enrollment enrollmentObj){
		EnrollmentECommittedDTO enrollmentECommittedDTO = new EnrollmentECommittedDTO();
		Boolean isSubscriber = Boolean.FALSE;
		
		// Health Plan
		enrollmentECommittedDTO.setCarrierName(enrollmentObj.getInsurerName());
		enrollmentECommittedDTO.setPlanName(enrollmentObj.getPlanName());
		enrollmentECommittedDTO.setPlanId(enrollmentObj.getPlanId());
		enrollmentECommittedDTO.setMonthlyPremium(enrollmentObj.getGrossPremiumAmt());
		List<EnrolleeECommittedDTO> enrolleeECommittedDTOList = new ArrayList<EnrolleeECommittedDTO>();
		enrollmentECommittedDTO.setTicketId(enrollmentObj.getTicketId());
		enrollmentECommittedDTO.setEnrollmentStatus(enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode());
		enrollmentECommittedDTO.setCarrierAppUrl(enrollmentObj.getCarrierAppUrl());	
		enrollmentECommittedDTO.setSsapApplicationId(enrollmentObj.getSsapApplicationid());
	
		if(isNotNullAndEmpty(enrollmentObj.getIssuerAssignPolicyNo())){
			enrollmentECommittedDTO.setCarrierConfirmationNumber(enrollmentObj.getIssuerAssignPolicyNo());
		}
		
		if(isNotNullAndEmpty(enrollmentObj.getCarrierAppId())){
			enrollmentECommittedDTO.setCarrierAppId(enrollmentObj.getCarrierAppId());
		}
		
		if(isNotNullAndEmpty(enrollmentObj.getEnrollees())){
			for(Enrollee enrolleeObj : enrollmentObj.getEnrollees()){
				
				isSubscriber = (isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp())) && (enrolleeObj.getPersonTypeLkp().getLookupValueLabel().equalsIgnoreCase("Subscriber")); 
				if ((isNotNullAndEmpty(enrolleeObj.getRelationshipToHCPLkp()) && enrolleeObj.getRelationshipToHCPLkp().getLookupValueLabel().equalsIgnoreCase("Self"))
						|| isSubscriber) {
					
					// POPULATING HOUSEHOLDCONTACT INFORMATION OR SUBSCRIBER
					enrollmentECommittedDTO.setHouseHoldContactFirstName(enrolleeObj.getFirstName());
					enrollmentECommittedDTO.setHouseHoldContactLastName(enrolleeObj.getLastName());
					enrollmentECommittedDTO.setHouseHoldContactMiddleName(enrolleeObj.getMiddleName());
				
					
					if(isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp())){
						enrollmentECommittedDTO.setHouseHoldContactType(enrolleeObj.getPersonTypeLkp().getLookupValueLabel());
					}
					enrollmentECommittedDTO.setHouseHoldContactPhoneNumber(enrolleeObj.getPrimaryPhoneNo());
					enrollmentECommittedDTO.setHouseHoldContactEmail(enrolleeObj.getPreferredEmail());
					if(isNotNullAndEmpty(enrollmentObj.getPrefferedContactTime())){
						enrollmentECommittedDTO.setHouseHoldContactBestTimeToContact(enrollmentObj.getPrefferedContactTime().getLookupValueLabel());
					}
					if(isNotNullAndEmpty(enrolleeObj.getHomeAddressid())){
						enrollmentECommittedDTO.setHouseHoldContactAddressLine1(enrolleeObj.getHomeAddressid().getAddress1());
						enrollmentECommittedDTO.setHouseHoldContactAddressLine2(enrolleeObj.getHomeAddressid().getAddress2());
						enrollmentECommittedDTO.setHouseHoldContactState(enrolleeObj.getHomeAddressid().getState());
						enrollmentECommittedDTO.setHouseHoldContactZip(enrolleeObj.getHomeAddressid().getZip());
						enrollmentECommittedDTO.setHouseHoldContactCounty(enrolleeObj.getHomeAddressid().getCounty());
						enrollmentECommittedDTO.setHouseHoldContactCity(enrolleeObj.getHomeAddressid().getCity());
					}
				}
				
				// POPULATING ENROLLEES OTHER THAN SUBSCRIBER AND HOUSEHOLD
			 	EnrolleeECommittedDTO enrolleeECommittedDTO = new EnrolleeECommittedDTO();
				enrolleeECommittedDTO.setFirstName(enrolleeObj.getFirstName());
				enrolleeECommittedDTO.setLastName(enrolleeObj.getLastName());
				enrolleeECommittedDTO.setMiddleName(enrolleeObj.getMiddleName());
				enrolleeECommittedDTO.setDob(enrolleeObj.getBirthDate());
				if(isNotNullAndEmpty(enrolleeObj.getGenderLkp())){
					enrolleeECommittedDTO.setGender(enrolleeObj.getGenderLkp().getLookupValueCode());
				}
				if(isNotNullAndEmpty(enrolleeObj.getTobaccoUsageLkp())){
					enrolleeECommittedDTO.setUseOfTobacco(enrolleeObj.getTobaccoUsageLkp().getLookupValueCode());
				}
				if (isSubscriber) {
					enrolleeECommittedDTO.setEnrolleeType(enrolleeObj.getPersonTypeLkp().getLookupValueLabel());
				} else if (isNotNullAndEmpty(enrolleeObj.getRelationshipToHCPLkp())) {
					enrolleeECommittedDTO.setEnrolleeType(enrolleeObj.getRelationshipToHCPLkp().getLookupValueLabel());
				} else {
					enrolleeECommittedDTO.setEnrolleeType(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE);
				}
				enrolleeECommittedDTOList.add(enrolleeECommittedDTO);
				
			}
			
		}
		 enrollmentECommittedDTO.setEnrolleeList(enrolleeECommittedDTOList);
//		 LOGGER.info("POPULATE ENROLLMENT ECOMIITED DTO -->"+enrollmentECommittedDTO.toString());
		 
		SingleIssuerResponse issuerDto = enrollmentExternalRestUtil.getIssuerInfoById(enrollmentObj.getIssuerId());
		if(issuerDto != null){
			enrollmentECommittedDTO.setProducerUserName(issuerDto.getProducerUserName());
			enrollmentECommittedDTO.setProducerPassword(issuerDto.getProducerPassword());
		}
		
	return enrollmentECommittedDTO;	
	}
	
	/**
	 * getTkmTicket()
	 * @param enrollmentObj
	 * @param userObj
	 * @return TkmTickets
	 */
	/*private TkmTickets getTkmTicket(Enrollment enrollmentObj, AccountUser userObj){

		 TkmTicketsRequest tkmTicketRequest = new TkmTicketsRequest();
		 XStream xstream = GhixUtils.getXStreamStaxObject();
		 TkmTicketsResponse tkmTicketResponse = null;
		 TkmTickets tkmTickets = null;
		 String postResp = null;
		 
		 try{
			 
			 tkmTicketRequest.setCategory("Follow up");
			 tkmTicketRequest.setType("eCommitment Follow up");
			 tkmTicketRequest.setComment(GhixEndPoints.WebServiceEndPoints.GHIX_WEB_CRM_CONSUMER_GET_ECOMMITMENT_DETAILS_URL+"?enrollmentId="+enrollmentObj.getId()+"&householdId="+enrollmentObj.getCmrHouseHoldId());
			 tkmTicketRequest.setSubject("Process e-Commit consumer");
			 tkmTicketRequest.setSubModuleId(enrollmentObj.getCmrHouseHoldId());
			 tkmTicketRequest.setRequester(userObj.getId());
			 tkmTicketRequest.setCreatedBy(userObj.getId());
			 Role roleObj = enrollmentUserService.getRoleForUser(userObj);
				if((roleObj)!=null){
					tkmTicketRequest.setUserRoleId(roleObj.getId());
				}
			 tkmTicketRequest.setCreatedBy(userObj.getId());
			 postResp = restTemplate.postForObject(GhixEndPoints.TicketMgmtEndPoints.CREATENEWTICKET,tkmTicketRequest,String.class);
			 
			 if(postResp!=null){
				 tkmTicketResponse = (TkmTicketsResponse) xstream.fromXML(postResp);
				 tkmTickets = tkmTicketResponse.getTkmTickets();
			 }
			 
	     }catch(Exception e){
	    	 LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_ECOMMITTED_ENROLLMENT_TICKET_CREATE_FAILURE, e);		 
	    	 }
		 
	    return tkmTickets;
	  }*/
	
	/**
	 * Maps enrollment id to cmrhousehold id in the Enrollment HouseHold Table 
	 * @param enrollment
	 */
	private void setHouseholdAndEnrollmentMapping(Enrollment enrollment) {

		HouseholdEnrollment householdEnrollment=new HouseholdEnrollment();
		householdEnrollment.setEnrollmentId(enrollment.getId());
		householdEnrollment.setHouseholdId(enrollment.getCmrHouseHoldId());
		//householdEnrollment.setApplicationId(enrollment.getSsapApplicationId());
		try{
			ResponseEntity<HouseholdEnrollment> consumerResponseEntity =restTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.SAVE_HOUSEHOLD_ENROLLMENT, householdEnrollment, HouseholdEnrollment.class);
			if(consumerResponseEntity==null){
				LOGGER.info("Consumer Service Response is null.");
			}
		}
		catch(Exception e){
			LOGGER.error("Error in creating enrollment ad household mapping : ", e);
		}
	}

	/**
	 * This method sends enrollment confirmation email  
	 * @author kotnis_s
	 * @param offExchangeRequestStr
	 * @return
	 */
	/*@ApiOperation(value="Send confirmation email by enrollment ID", notes="", httpMethod="POST", produces="application/xml")*/
	@RequestMapping(value="/sendconfirmationemail",method = RequestMethod.POST)
	@ResponseBody public String sendConfirmationEmail(@RequestBody String enrollmentId){
		
		LOGGER.info("============inside sendConfirmationEmail ============");
		
		LOGGER.info("Received enrollmentId  : " + enrollmentId);
		  XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		  EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		  if (isNotNullAndEmpty(enrollmentId)
		    && !enrollmentId.equalsIgnoreCase("0")) {
			  
			  Enrollment enrollmentObj = enrollmentOffExchangeService.findEnrollmentById(Integer.valueOf(enrollmentId));
			  if(enrollmentObj != null){
				  sendEmailToConsumer(enrollmentObj.getCmrHouseHoldId(),enrollmentObj,true);
			  }
			   

		   enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		  } else {
		   enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		   enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		  }
		  return xstream.toXML(enrollmentResponse);
	}
}

