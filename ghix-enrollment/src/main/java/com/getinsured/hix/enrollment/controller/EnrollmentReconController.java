package com.getinsured.hix.enrollment.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.hix.dto.enrollment.DiscrepancyActionRequestDTO;
import com.getinsured.hix.dto.enrollment.DiscrepancyEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrlReconciliationResponse;
import com.getinsured.hix.dto.enrollment.EnrollmentReconRequest;
import com.getinsured.hix.enrollment.service.EnrlReconDiscrepancyService;
import com.getinsured.hix.enrollment.service.EnrlReconSummaryService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.google.gson.Gson;

/**
 * 
 * @author sharma_k
 *
 */
@RestController
@RequestMapping(value = "/enrollmentrecon")
public class EnrollmentReconController {
	private static final Logger LOGGER = Logger.getLogger(EnrollmentReconController.class);
	@Autowired private Gson platformGson;
	@Autowired private EnrlReconSummaryService enrlReconSummaryService;
	@Autowired private EnrlReconDiscrepancyService enrlReconDiscrepancyService;
	@Autowired private UserService userService;
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getpopulatedcarrierandmonthlist", method= RequestMethod.GET)
	public String getPopulatedCarrierAndMonthList(){
		EnrlReconciliationResponse enrlReconciliationResponse = new EnrlReconciliationResponse();
		enrlReconciliationResponse.startResponse();
		try{
			enrlReconSummaryService.getPopulatedMonthAndCarrierList(enrlReconciliationResponse);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_RECON:: Exception caught in getPopulatedCarrierAndMonthList : ",ex);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		enrlReconciliationResponse.endResponse();
		return platformGson.toJson(enrlReconciliationResponse);
	}
	
	
	/**
	 * 
	 * @param enrollmentReconRequestStr
	 * @return
	 */
	@RequestMapping(value = "/getmonthlyreconactivitydata", method = RequestMethod.POST)
	public String getMonthlyReconActivityData(@RequestBody final String enrollmentReconRequestStr){
		EnrlReconciliationResponse enrlReconciliationResponse = new EnrlReconciliationResponse();
		enrlReconciliationResponse.startResponse();
		try{
			if(StringUtils.isNotBlank(enrollmentReconRequestStr)){
				EnrollmentReconRequest enrollmentReconRequest = platformGson.fromJson(enrollmentReconRequestStr, EnrollmentReconRequest.class);
				if(enrollmentReconRequest != null){
					boolean isValidRequest = validateMontlyReconRequest(enrollmentReconRequest, enrlReconciliationResponse);
					if(isValidRequest){
						enrlReconSummaryService.getMonthlyActivityData(enrollmentReconRequest, enrlReconciliationResponse);
					}
					else{
						enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					}
				}
				else{
					enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
					enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				}
			}
			else{
				LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
				enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_RECON:: Exception caught in getMonthlyReconActivityData : ",ex);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		enrlReconciliationResponse.endResponse();
		return platformGson.toJson(enrlReconciliationResponse);
	}
	
	@RequestMapping(value = "/getsearchdiscrepancydropdowndata", method = RequestMethod.GET)
	public String populateDataForSearchDiscrepnacy(){
		EnrlReconciliationResponse enrlReconciliationResponse = new EnrlReconciliationResponse();
		enrlReconciliationResponse.startResponse();
		try{
			enrlReconSummaryService.populateSearchDiscrepancyCriteria(enrlReconciliationResponse);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_RECON:: Exception caught in populateDataForSearchDiscrepnacy : ",ex);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		
		enrlReconciliationResponse.endResponse();
		return platformGson.toJson(enrlReconciliationResponse);
	}
	
	
	
	
	@RequestMapping(value = "/searchenrollmentdiscrepancy", method = RequestMethod.POST)
	public String searchEnrollmentDiscrepancy(@RequestBody final String enrollmentReconRequestStr){
		
		EnrlReconciliationResponse enrlReconciliationResponse = new EnrlReconciliationResponse();
		enrlReconciliationResponse.startResponse();
		try{
			//TODO once the API is finalized add request validation code
			if(StringUtils.isNotBlank(enrollmentReconRequestStr)){
				EnrollmentReconRequest enrollmentReconRequest = platformGson.fromJson(enrollmentReconRequestStr, EnrollmentReconRequest.class);
				if(enrollmentReconRequest != null){
					enrlReconSummaryService.searchEnrlDiscrepancyData(enrollmentReconRequest, enrlReconciliationResponse);
					enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				}
				else{
					enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
					enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				}
			}
			else{
				LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
				enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_RECON::SearchDiscrepancy:: Exception caught in searchEnrollmentDiscrepency : ",ex);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		
		enrlReconciliationResponse.endResponse();
		return platformGson.toJson(enrlReconciliationResponse);
	}
	
	@RequestMapping(value = "/getenrlrecondiscrepancydetail", method = RequestMethod.POST)
	public String getEnrlReconDiscrepancyDetail(@RequestBody final String enrollmentReconRequestStr){
		EnrlReconciliationResponse enrlReconciliationResponse = new EnrlReconciliationResponse();
		enrlReconciliationResponse.startResponse();
		try{
			if(StringUtils.isNotBlank(enrollmentReconRequestStr)){
				EnrollmentReconRequest enrollmentReconRequest = platformGson.fromJson(enrollmentReconRequestStr, EnrollmentReconRequest.class);
				if(enrollmentReconRequest != null){
					if(enrollmentReconRequest.getHixEnrollmentId() != null && enrollmentReconRequest.getHixEnrollmentId() !=0){
						enrlReconDiscrepancyService.getEnrlReconDiscrepancyDetail(enrollmentReconRequest, enrlReconciliationResponse);
						enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					}
				}
				else{
					LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
					enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
					enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				}
			}
			else{
				LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
				enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
			
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_RECON::getOpenDiscrepancyDetails:: Exception caught in getOpenDiscrepancyDetails : ",ex);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		enrlReconciliationResponse.endResponse();
		return platformGson.toJson(enrlReconciliationResponse);
	}
	
	@RequestMapping(value = "/getenrlreconcommentdetail", method = RequestMethod.POST)
	public String getEnrlReconCommentDetail(@RequestBody final String enrollmentReconRequestStr){
		EnrlReconciliationResponse enrlReconciliationResponse = new EnrlReconciliationResponse();
		enrlReconciliationResponse.startResponse();
		try{
			if(StringUtils.isNotBlank(enrollmentReconRequestStr)){
				EnrollmentReconRequest enrollmentReconRequest = platformGson.fromJson(enrollmentReconRequestStr, EnrollmentReconRequest.class);
				if(enrollmentReconRequest != null){
					if(enrollmentReconRequest.getHixEnrollmentId() != null && enrollmentReconRequest.getHixEnrollmentId() !=0){
						enrlReconDiscrepancyService.getEnrlReconCommentDetail(enrollmentReconRequest, enrlReconciliationResponse);
						enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
						enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					}
				}
				else{
					LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
					enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
					enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				}
			}
			else{
				LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
				enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_RECON::getEnrlReconCommentDetail:: Exception caught in getEnrlReconCommentDetail : ",ex);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		enrlReconciliationResponse.endResponse();
		return platformGson.toJson(enrlReconciliationResponse);
	}
	
	
	@RequestMapping(value = "/addeditcommentorsnoozediscrepancy", method = RequestMethod.POST)
	public String addEditOrSnoozeDiscrepancy(@RequestBody final String discrepancyActionRequestDTOStr){
		EnrlReconciliationResponse enrlReconciliationResponse = new EnrlReconciliationResponse();
		enrlReconciliationResponse.startResponse();
		try{
			if(StringUtils.isNotBlank(discrepancyActionRequestDTOStr)){
				DiscrepancyActionRequestDTO discrepancyActionRequestDTO = platformGson.fromJson(discrepancyActionRequestDTOStr, DiscrepancyActionRequestDTO.class);
				if(discrepancyActionRequestDTO != null){
					if(validateDiscrepancyActionRequest(discrepancyActionRequestDTO, enrlReconciliationResponse)){
						AccountUser accountUser = userService.getLoggedInUser();
						if(accountUser != null && accountUser.getId() != EnrollmentConstants.ZERO){
							enrlReconDiscrepancyService.addEditCommentOrSnoozeDiscrepancy(discrepancyActionRequestDTO, accountUser);
							enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
							enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						}
						else{
							enrlReconciliationResponse.setErrMsg("Invalid or Login Disabled user found in request");
							enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
							enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						}
					}
					else
					{
						enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
						enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
				else{
					LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
					enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
					enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				}
			}
			else{
				LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
				enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_RECON::getEnrlReconCommentDetail:: Exception caught in getEnrlReconCommentDetail : ",ex);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		enrlReconciliationResponse.endResponse();
		return platformGson.toJson(enrlReconciliationResponse);
	}
	
	@RequestMapping(value = "/getvisualsummarydetail", method = RequestMethod.POST)
	public String getVisualSummaryDetail(@RequestBody final String enrollmentReconRequestStr){
		EnrlReconciliationResponse enrlReconciliationResponse = new EnrlReconciliationResponse();
		enrlReconciliationResponse.startResponse();
		try{
			if(StringUtils.isNotBlank(enrollmentReconRequestStr)){
				EnrollmentReconRequest enrollmentReconRequest = platformGson.fromJson(enrollmentReconRequestStr, EnrollmentReconRequest.class);
				if(enrollmentReconRequest != null){
					if(enrollmentReconRequest.getHixEnrollmentId() != null && enrollmentReconRequest.getHixEnrollmentId() !=0){
						DiscrepancyEnrollmentDTO discrepancyEnrollmentDTO = enrlReconDiscrepancyService.getEnrlReconVisualSummaryData(enrollmentReconRequest.getHixEnrollmentId());
						enrlReconciliationResponse.setDiscrepancyEnrollmentDTO(discrepancyEnrollmentDTO);
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
				}
				else{
					LOGGER.warn("ENROLLMENT_RECON:: Received null or Empty Request");
					enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
					enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				}
			}
			else{
				enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENROLLMENT_RECON::getVisualSummaryDetail:: Exception caught in getVisualSummaryDetail : ",ex);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		enrlReconciliationResponse.endResponse();
		return platformGson.toJson(enrlReconciliationResponse);
	}
	
	
	
	private boolean validateDiscrepancyActionRequest(final DiscrepancyActionRequestDTO discrepancyActionRequestDTO, EnrlReconciliationResponse enrlReconciliationResponse){
		boolean isValidRequest = true;
		 
		 if(discrepancyActionRequestDTO.getDiscrepancyAction() != null){
			 switch(discrepancyActionRequestDTO.getDiscrepancyAction()){
			 case ADD_COMMENT :
				 if(discrepancyActionRequestDTO.getDiscrepancyId() == null || 
				 (discrepancyActionRequestDTO.getDiscrepancyId() != null && discrepancyActionRequestDTO.getDiscrepancyId() == EnrollmentConstants.ZERO)){
					 isValidRequest = false;
					 enrlReconciliationResponse.setErrMsg("Received null or empty DiscrepancyID for ADD_COMMENT Action");
				 }
				 break;
			 case EDIT_COMMENT :
				 if(discrepancyActionRequestDTO.getCommentId() == null || 
				 (discrepancyActionRequestDTO.getCommentId() != null && discrepancyActionRequestDTO.getCommentId() == EnrollmentConstants.ZERO)){
					 isValidRequest = false;
					 enrlReconciliationResponse.setErrMsg("Received null or empty CommentId for EDIT_COMMENT Action");
				 }
				 break;
			 case HOLD :
				 if(discrepancyActionRequestDTO.getDiscrepancyId() == null || 
				 (discrepancyActionRequestDTO.getDiscrepancyId() != null && discrepancyActionRequestDTO.getDiscrepancyId() == EnrollmentConstants.ZERO)){
					 isValidRequest = false;
					 enrlReconciliationResponse.setErrMsg("Received null or empty DiscrepancyID for HOLD Action");
				 }
				 break;
			 case HOLD_ALL :
				 if(discrepancyActionRequestDTO.getEnrollmentId() == null || 
				 (discrepancyActionRequestDTO.getEnrollmentId() != null && discrepancyActionRequestDTO.getEnrollmentId() == EnrollmentConstants.ZERO)){
					 isValidRequest = false;
					 enrlReconciliationResponse.setErrMsg("Received null or empty EnrollmentId for HOLD_ALL Action");
				 }
				 break;
			default:
				 isValidRequest = false;
				 enrlReconciliationResponse.setErrMsg("Received Invalid Action in request: "+discrepancyActionRequestDTO.getDiscrepancyAction());
			 }
			 if(StringUtils.isBlank(discrepancyActionRequestDTO.getCommentText())){
				 isValidRequest = false;
				 enrlReconciliationResponse.setErrMsg("Received null or empty CommentText ");
			 }
		 }
		 else{
			 isValidRequest = false;
			 enrlReconciliationResponse.setErrMsg("Received null or empty DiscrepancyAction in request");
			 
		 }
		return isValidRequest;
	}
	
	/**
	 * 
	 * @param enrollmentReconRequest
	 * @param errMessage
	 * @return
	 */
	private boolean validateMontlyReconRequest(final EnrollmentReconRequest enrollmentReconRequest, EnrlReconciliationResponse enrlReconciliationResponse){
		boolean isRequestValid = true;
		if(enrollmentReconRequest.getMonthNum() == null || enrollmentReconRequest.getMonthNum() == 0 ||
				(enrollmentReconRequest.getMonthNum() != null && enrollmentReconRequest.getMonthNum() > EnrollmentConstants.TWELVE)){
			isRequestValid = false;
			enrlReconciliationResponse.setErrMsg("Invalid month number provided in request: "+enrollmentReconRequest.getMonthNum());
		}
		if(enrollmentReconRequest.getYear() == null || enrollmentReconRequest.getYear() == 0){
			isRequestValid = false;
			enrlReconciliationResponse.setErrMsg("Invalid year provided in request: "+enrollmentReconRequest.getYear());
		}
		return isRequestValid;
	}
}
