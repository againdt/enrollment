/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.enrollment.EnrollmentEventHistoryDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.enrollment.service.EnrollmentReportService;
import com.getinsured.hix.enrollment.service.EnrollmentService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;


/**
 * @author rajaramesh_g
 * @since 06/03/2014
 * This controller provides the reporting services of the enrollment
 */
@Controller
@RequestMapping(value = "/enrollmentreport")
public class EnrollmentReportController {

	private static final Logger LOGGER = Logger.getLogger(EnrollmentController.class);
	
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentReportService enrollmentReportService;
	@Autowired private Gson platformGson;
		
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Enrollment Report Controller :: welcome()");
		return "Welcome to Enrollment Report Controller";
	}
	
	@RequestMapping(value = "/findemployersbyenrollmentstartandenddate", method = RequestMethod.POST)
	@ResponseBody
	public String findEmployersByEnrollmentStartAndEndDate(@RequestBody String enrollmentRequestStr) {
		
		LOGGER.info("Enrollment Report Controller :: findEmployersByEnrollmentStartAndEndDate()");
		
		List<Integer> employerIdList = null;
		Set<Integer> employerIdSet = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();

		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);

		if (!isNotNullAndEmpty(enrollmentRequest.getEmployerEnrollmentStartDate())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_START_DATE_NULL_OR_EMPTY);
			LOGGER.error(EnrollmentConstants.ERR_MSG_START_DATE_NULL_OR_EMPTY);
		} else if (!isNotNullAndEmpty(enrollmentRequest.getEmployerEnrollmentEndDate())) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_END_DATE_NULL_OR_EMPTY);
			LOGGER.error(EnrollmentConstants.ERR_MSG_END_DATE_NULL_OR_EMPTY);
		} else {
			List<Object[]> employerIdDataList = enrollmentService.findEmployersByStartDateAndEndDate(enrollmentRequest.getEmployerEnrollmentStartDate(), enrollmentRequest.getEmployerEnrollmentEndDate());
			
			if(isNotNullAndEmpty(employerIdDataList) && !employerIdDataList.isEmpty()){
				employerIdList=new ArrayList<Integer>();
				employerIdSet = new HashSet<Integer>();
				for (Object[] employerIdData : employerIdDataList) {
					employerIdSet.add((Integer)employerIdData[1]);
				}  
				employerIdList.addAll(employerIdSet);
				enrollmentResponse.setIdList(employerIdList);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				enrollmentResponse.setErrMsg(GhixConstants.RESPONSE_SUCCESS);
			}
			else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RESULTS_FOUND);
				LOGGER.error(EnrollmentConstants.ERR_MSG_NO_RESULTS_FOUND);
			}
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	@RequestMapping(value = "/getShopEnrollmentLogDetails", method = RequestMethod.POST)
	@ResponseBody
	public String getShopEnrollmentLogDetails(@RequestBody String enrollmentRequestStr){
		EnrollmentResponse enrollmentResponse = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = (EnrollmentRequest) xstream.fromXML(enrollmentRequestStr);

		if(enrollmentRequest != null){
			if(!isNotNullAndEmpty(enrollmentRequest.getEmployerId())){
				enrollmentResponse = new EnrollmentResponse();
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_IS_NULL);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				return xstream.toXML(enrollmentResponse);
			}
			try{
				enrollmentResponse = enrollmentService.getShopEnrollmentLog(enrollmentRequest);
				if(enrollmentResponse == null){
					enrollmentResponse = new EnrollmentResponse();
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}catch(Exception exception){
				LOGGER.error("Exception" , exception);
				enrollmentResponse = new EnrollmentResponse();
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		return xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * Jira ID: HIX-72107
	 * Create a new Enrollment Events API for CAP Enrollment History functionality
	 * @since 20th July 2015
	 * @param enrollmentRequestString String
	 * @return Json Response <EnrollmentResponse>
	 */
	@RequestMapping(value = "/getEnrollmentEventHistoryData", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentEventHistoryData(@RequestBody String enrollmentRequestString)
	{
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		LOGGER.info("=========== started getEnrollmentEventHistoryData(-) ========");
		if(StringUtils.isNotEmpty(enrollmentRequestString))
		{
			try
			{
				EnrollmentRequest enrollmentRequest = platformGson.fromJson(enrollmentRequestString, EnrollmentRequest.class);
				if(enrollmentRequest != null)
				{
					List<EnrollmentEventHistoryDTO> enrollmentEventHistoryDTOList = enrollmentReportService.getEnrollmentEventHistoryDTOList(enrollmentRequest);
					if(enrollmentEventHistoryDTOList != null && !enrollmentEventHistoryDTOList.isEmpty())
					{
						enrollmentResponse.setEnrollmentEventHistoryDTOList(enrollmentEventHistoryDTOList);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					}
					else
					{
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
						enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
					}
				}
				else
				{
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
				}
			}
			catch(Exception ex)
			{
				LOGGER.error("Caught exception in getEnrollmentEventHistoryData(-) call ::"+ex);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_EVENT_RETRIEVE_FAIL+":: "+ EnrollmentUtils.shortenedStackTrace(ex, 4));
			}
		}
		else
		{
			LOGGER.info("Received null or empty request in getEnrollmentEventHistoryData(-) call ");
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_REQUEST_IS_NULL);
		}
		return platformGson.toJson(enrollmentResponse);
	}
}
