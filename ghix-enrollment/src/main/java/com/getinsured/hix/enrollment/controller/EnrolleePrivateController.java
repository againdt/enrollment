/**
 * 
 */
package com.getinsured.hix.enrollment.controller;

import java.util.Map;

import javax.xml.ws.http.HTTPException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import springfox.documentation.annotations.ApiIgnore;

import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipPrivateRepository;
import com.getinsured.hix.enrollment.service.EnrolleePrivateService;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

/**
 * @author panda_p
 * @since 14/02/2013
 * 
 * This class handles all Enrollee related service requests  
 */

/*@Api(value="enrollee-Private-Controller", description="Enrollee Private Controller")*/
@Controller
@RequestMapping(value="/enrollee/private")
public class EnrolleePrivateController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrolleePrivateController.class);
	
	
	@Autowired private EnrolleePrivateService enrolleeService;
	@Autowired private IEnrolleeRelationshipPrivateRepository enrolleeRelationshipRepository;
	
	
	/**
	 * @author panda_p
     * @since 14/02/2013
     * 
     * Returns EnrolleeResponse as XML string for Enrollee object on receiving correct enrollee Id and 
     * for incorrect  enrollee Id returns EnrolleeResponse as XML string with error status and message
     * 
	 * @param id
	 * @return
	 * @throws HTTPException
	 */
	/*@ApiOperation(value="Find enrollee by id", notes="", httpMethod="GET", produces="application/xml")*/
	@RequestMapping(value="/findbyid/{id}",method = RequestMethod.GET)
	@ResponseBody public String findEnrolleeById(@PathVariable("id") String id){
		LOGGER.info("============inside findEnrolleeById============");
		
		LOGGER.info("Enrollee id received");
		EnrolleeResponse enrolleeResponse= new EnrolleeResponse();
		if(id==null || id.equalsIgnoreCase("0")){
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}else{
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			Integer enrolleeId = Integer.valueOf(id);
			Enrollee  enrollee = enrolleeService.findById(enrolleeId);
			
			
			if(enrollee != null){
				Integer subscriberId = enrolleeService.findSubscriberIdByEnrollmentId(enrollee.getEnrollment().getId());
				EnrolleeRelationship  relationShipToSuscriber = enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrollee.getId(), subscriberId);

				if(relationShipToSuscriber != null && relationShipToSuscriber.getRelationshipLkp() != null){
					enrollee.setRelationshipToSubscriberLkp(relationShipToSuscriber.getRelationshipLkp());
				}
				enrolleeResponse.setEnrollee(enrollee);
				enrolleeResponse.setEnrollment(enrollee.getEnrollment());
				
			}
			
			else{
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrolleeResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_NO_ENROLLEE);
			}
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String response = xstream.toXML(enrolleeResponse);

		
		LOGGER.info("findEnrolleeById XML : "+ response);
		
		return response;
	}
	
	/**
	 * @author panda_p
     * @since 14/02/2013
     * 
     * Returns EnrolleeResponse as XML string with list of Enrollee object on receiving correct search criteria and 
     * for incorrect  search criteria returns EnrolleeResponse as XML string with error status and message
     * 
	 * @param id
	 * @return
	 * @throws HTTPException
	 */
	/*@ApiOperation(value="Search Enrollee", notes="", httpMethod="POST", produces="application/xml")*/
	@ApiIgnore
	@RequestMapping(value="/searchEnrollee",method = RequestMethod.POST)
	@ResponseBody public String searchEnrollee(@RequestBody Map<String, Object> searchCriteria){
		LOGGER.info("============inside searchEnrollee============");
		LOGGER.info("Enrollee search Criteria :  "+searchCriteria);
		
		Map<String, Object> enrolleesAndRecordCount = enrolleeService.searchEnrollee(searchCriteria);
		EnrolleeResponse enrolleeResponse= new EnrolleeResponse();
		
		if (enrolleesAndRecordCount!=null) {
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrolleeResponse.setSearchResultAndCount(enrolleesAndRecordCount);
		}else{
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_NO_RESULTS_FOUND);
		}
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String xml=xstream.toXML(enrolleeResponse);
		
		LOGGER.info("enrolleeSearch: "+xml);
		
		return xml;
	}
	
	/*@RequestMapping(value="/saveCarrierUpdatedDataResponse",method = RequestMethod.POST)
	@ResponseBody public String saveCarrierUpdatedDataResponse(@RequestBody List<SendUpdatedEnrolleeResponseDTO> inputval){
		LOGGER.info("============inside saveCarrierUpdatedDataResponse============");
		LOGGER.info("saveCarrierUpdatedDataResponse :  "+inputval);

		enrolleeService.saveCarrierUpdatedDataResponse(inputval);


		return "";
	}*/
	
	/*@ApiOperation(value="Find Subscriber by Enrollment ID", notes="", httpMethod="GET", produces="application/xml")*/
	@RequestMapping(value="/findsubscriberbyenrollmentid/{id}",method = RequestMethod.GET)
	@ResponseBody public String findSubscriberByEnrollmentId(@PathVariable("id") String id){
		LOGGER.info("============inside findsubscriberbyenrollmentid ============");
		
		LOGGER.info("Enrollment id received : "+id);
		EnrolleeResponse enrolleeResponse= new EnrolleeResponse();
		if(id==null || id.equalsIgnoreCase("0")){
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
		}
		else
		{
			Integer enrollmentId = Integer.valueOf(id);
			Enrollee  enrollee = enrolleeService.findSubscriberbyEnrollmentIDId(enrollmentId);
			
			if(enrollee != null)
			{
				Enrollee newEnrolleeObj = new Enrollee();
			
				newEnrolleeObj.setFirstName(enrollee.getFirstName());
				newEnrolleeObj.setLastName(enrollee.getLastName());
				newEnrolleeObj.setMiddleName(enrollee.getMiddleName());
				newEnrolleeObj.setSuffix(enrollee.getSuffix());
				enrolleeResponse.setEnrollee(newEnrolleeObj);
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
			else
			{
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrolleeResponse.setErrMsg("No records Found");
			}
		}
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String xml=xstream.toXML(enrolleeResponse);
		
		LOGGER.info("findSubscriberByEnrollmentId XML : "+xml);
		
		return xml;
	}
}
