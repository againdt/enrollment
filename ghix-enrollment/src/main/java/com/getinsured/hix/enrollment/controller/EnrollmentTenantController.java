package com.getinsured.hix.enrollment.controller;
import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.enrollment.service.EnrollmentTenantService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrollmentTenantResponse;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.google.gson.Gson;

/**
 * HIX-70722 Enrollment Tenant Controller
 * @author negi_s
 * @since 06/07/2015
 *
 */
@Controller
@RequestMapping(value = "/api")
public class EnrollmentTenantController {

	private static final Logger LOGGER = Logger.getLogger(EnrollmentTenantController.class);

	@Autowired
	private EnrollmentTenantService enrollmentTenantService;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired private Gson platformGson;

	private static final String ERR_MSG_201 = "leadId is null/empty or is not a numeric value";
	private static final String ERR_MSG_202 = "enrollmentId is null/empty or is not a numeric value";
	private static final String ERR_MSG_203 = "affliateId not found in session";
	private static final String ERR_MSG_204 = "Unexpected error in retrieving enrollment information";
	private static final String ERR_MSG_206 = "apiKey is not valid";
	private static final String ERR_MSG_207 = "Could not fetch lead from API";
	private static final String ERR_MSG_208 = "Data access is not allowed";

	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	/**
	 * This method is used to test the working of enrollment tenant controller after deployment
	 * @author negi_s
	 * @since 06/07/2015
	 * @return String Welcome Message
	 */
	@RequestMapping(value = "/enrollment/tenant", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to EnrollmentTenantController");
		return "Welcome to EnrollmentTenantController";
	}

	/**
	 * Enrollment API to return enrollment information based on the enrollment Id and the lead Id passed
	 * @author negi_s
	 * @param leadId
	 * @param enrollmentId
	 * @param request the HTTP request
	 * @return String EnrollmentTenantResponse json string
	 */
	@RequestMapping(value = "/enrollment/leads/{leadId}/enrollments/{enrollmentId}", method = RequestMethod.GET)
	@ResponseBody
	public String retrieveEnrollmentDetails(@PathVariable("leadId") Long leadId, @PathVariable("enrollmentId") Integer enrollmentId, HttpServletRequest request) {
		LOGGER.info("Start of retrieveEnrollmentDetails() for request: " + request.getRequestURL() + " apiKey:" + request.getHeader("apiKey"));
		EnrollmentTenantResponse enrollmentTenantResponse = new EnrollmentTenantResponse();
		try{
			String apiKey = request.getHeader("apiKey");
//			Long affiliateId = (Long) request.getSession().getAttribute("ClickTrackAffiliateId");
			Affiliate affiliate = findAffiliateByApiKey(apiKey);
			EligLead eligLead = getEligLead(leadId);
			if(affiliate == null) {
				setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_206, ERR_MSG_206);
			}
			
			if(affiliate != null && isRequestValid(leadId,enrollmentId,enrollmentTenantResponse,eligLead)){
				Long affiliateId = affiliate.getAffiliateId();
				enrollmentTenantResponse = enrollmentTenantService.getEnrollmentTenantResponse(leadId,enrollmentId, affiliateId);
			}
		}catch(Exception e){
			giExceptionHandler.recordFatalException(Component.ENROLLMENT,String.valueOf(EnrollmentConstants.ERROR_CODE_204),ERR_MSG_204, e);
			LOGGER.error(ERR_MSG_204, e);
			enrollmentTenantResponse = new EnrollmentTenantResponse();
			setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_204, ERR_MSG_204);
		}
		LOGGER.info("End of retrieveEnrollmentDetails() with response: " + platformGson.toJson(enrollmentTenantResponse));
		return platformGson.toJson(enrollmentTenantResponse);	
	}

	/**
	 * Validates the request parameters
	 * @param tenantId
	 * @param householdId
	 * @param enrollmentId
	 * @param enrollmentTenantResponse
	 * @param eligLead 
	 * @param affiliateId 
	 * @param apiKey 
	 * @return boolean Validity of request parameters
	 */
	private boolean isRequestValid(Long leadId, Integer enrollmentId, EnrollmentTenantResponse enrollmentTenantResponse, EligLead eligLead) {
		boolean isValid = true;
		 if(!EnrollmentUtils.isNotNullAndEmpty(leadId)){
			setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_201, ERR_MSG_201);
			isValid = false;
		} else if(!EnrollmentUtils.isNotNullAndEmpty(enrollmentId)){
			setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_202, ERR_MSG_202);
			isValid = false;
		} else if(!EnrollmentUtils.isNotNullAndEmpty(eligLead)){
			setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_207, ERR_MSG_207);
			isValid = false;
		} else if(EnrollmentUtils.isNotNullAndEmpty(eligLead) && !BooleanUtils.toBoolean(eligLead.getDataSharingConsent())){
			setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_208, ERR_MSG_208);
			isValid = false;
		}
		/*else if(!EnrollmentUtils.isNotNullAndEmpty(affiliateId)) {
			setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_203, ERR_MSG_203);
			isValid = false;
		} else if(!validateApiKey(affiliateId,apiKey)) {
			setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_206, ERR_MSG_206);
			isValid = false;
		}*/
		return isValid;
	}
	
	/**
	 * Validates the API key received from the request
	 * @param affiliateId
	 * @param apiKey
	 * @return boolean
	 */
	/*private boolean validateApiKey(Long affiliateId, String apiKey) {
		Boolean isApikeyValid = Boolean.FALSE;
		if(EnrollmentUtils.isNotNullAndEmpty(apiKey)){
			String url = String.format(GhixEndPoints.AffiliateServiceEndPoints.VALIDATE_APIKEY, affiliateId,apiKey);
			isApikeyValid = ghixRestTemplate.getForObject(url,Boolean.class);
		}
		return isApikeyValid;
	}*/
	
	private Affiliate findAffiliateByApiKey(String apiKey) {
		String url = String.format(GhixEndPoints.AffiliateServiceEndPoints.GET_AFFILIATE_BY_APIKEY, apiKey);
		Affiliate affiliate = ghixRestTemplate.getForObject(url,Affiliate.class);
		return affiliate;
	}
	
	/**
	 * Fetch eligLead from lead Id
	 * @param leadId
	 * @return EligLead
	 */
	private EligLead getEligLead(Long leadId){
		EligLead eligLead = null;
		if (isNotNullAndEmpty(leadId)) {
			eligLead=ghixRestTemplate.postForObject(GhixEndPoints.PhixEndPoints.GET_LEAD, leadId.longValue(), EligLead.class);
		}
		return eligLead;
	}
	
	/**
	 * Sets the error response parameters
	 * @param enrollmentTenantResponse
	 * @param errorCode
	 * @param errMsg
	 */
	private void setErrorResponse(EnrollmentTenantResponse enrollmentTenantResponse, int errorCode, String errMsg) {
		enrollmentTenantResponse.setErrCode(errorCode);
		enrollmentTenantResponse.setErrMsg(errMsg);
		enrollmentTenantResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
	}

}
