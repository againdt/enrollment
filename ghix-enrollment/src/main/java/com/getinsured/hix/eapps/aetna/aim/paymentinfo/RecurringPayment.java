//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.22 at 11:19:55 AM IST 
//


package com.getinsured.hix.eapps.aetna.aim.paymentinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RecurringPayment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecurringPayment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="electronicFundTransfer" type="{http://schema.aetna.com/2009/09/aim/PaymentInfo}ElectronicFundTransfer" minOccurs="0"/>
 *           &lt;element name="billMeMonthly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="paymentType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="2"/>
 *               &lt;enumeration value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecurringPayment", propOrder = {
    "electronicFundTransfer",
    "billMeMonthly",
    "paymentType"
})
public class RecurringPayment {

    protected ElectronicFundTransfer electronicFundTransfer;
    protected String billMeMonthly;
    @XmlElement(required = true)
    protected String paymentType;

    /**
     * Gets the value of the electronicFundTransfer property.
     * 
     * @return
     *     possible object is
     *     {@link ElectronicFundTransfer }
     *     
     */
    public ElectronicFundTransfer getElectronicFundTransfer() {
        return electronicFundTransfer;
    }

    /**
     * Sets the value of the electronicFundTransfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElectronicFundTransfer }
     *     
     */
    public void setElectronicFundTransfer(ElectronicFundTransfer value) {
        this.electronicFundTransfer = value;
    }

    /**
     * Gets the value of the billMeMonthly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillMeMonthly() {
        return billMeMonthly;
    }

    /**
     * Sets the value of the billMeMonthly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillMeMonthly(String value) {
        this.billMeMonthly = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

}
