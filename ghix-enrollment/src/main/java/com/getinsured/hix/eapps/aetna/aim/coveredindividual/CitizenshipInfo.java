//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.24 at 02:43:46 PM IST 
//


package com.getinsured.hix.eapps.aetna.aim.coveredindividual;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.eapps.aetna.aim.meta.FieldOptDate;
import com.getinsured.hix.eapps.aetna.aim.meta.FieldOptString;


/**
 * <p>Java class for CitizenshipInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CitizenshipInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nonCitizenCheck" type="{http://schema.aetna.com/2009/09/aim/Meta}Field-Opt-String"/>
 *         &lt;element name="arrivalDate" type="{http://schema.aetna.com/2009/09/aim/Meta}Field-Opt-Date" minOccurs="0"/>
 *         &lt;element name="insIDNumber" type="{http://schema.aetna.com/2009/09/aim/Meta}Field-Opt-String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CitizenshipInfo", propOrder = {
    "nonCitizenCheck",
    "arrivalDate",
    "insIDNumber"
})
public class CitizenshipInfo {

    @XmlElement(required = true)
    protected FieldOptString nonCitizenCheck;
    protected FieldOptDate arrivalDate;
    protected FieldOptString insIDNumber;

    /**
     * Gets the value of the nonCitizenCheck property.
     * 
     * @return
     *     possible object is
     *     {@link FieldOptString }
     *     
     */
    public FieldOptString getNonCitizenCheck() {
        return nonCitizenCheck;
    }

    /**
     * Sets the value of the nonCitizenCheck property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldOptString }
     *     
     */
    public void setNonCitizenCheck(FieldOptString value) {
        this.nonCitizenCheck = value;
    }

    /**
     * Gets the value of the arrivalDate property.
     * 
     * @return
     *     possible object is
     *     {@link FieldOptDate }
     *     
     */
    public FieldOptDate getArrivalDate() {
        return arrivalDate;
    }

    /**
     * Sets the value of the arrivalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldOptDate }
     *     
     */
    public void setArrivalDate(FieldOptDate value) {
        this.arrivalDate = value;
    }

    /**
     * Gets the value of the insIDNumber property.
     * 
     * @return
     *     possible object is
     *     {@link FieldOptString }
     *     
     */
    public FieldOptString getInsIDNumber() {
        return insIDNumber;
    }

    /**
     * Sets the value of the insIDNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldOptString }
     *     
     */
    public void setInsIDNumber(FieldOptString value) {
        this.insIDNumber = value;
    }

}
