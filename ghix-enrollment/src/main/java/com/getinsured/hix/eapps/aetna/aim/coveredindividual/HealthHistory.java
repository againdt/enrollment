//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.24 at 02:43:46 PM IST 
//


package com.getinsured.hix.eapps.aetna.aim.coveredindividual;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.eapps.aetna.aim.meta.Section;


/**
 * <p>Java class for HealthHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HealthHistory">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schema.aetna.com/2009/09/aim/Meta}Section">
 *       &lt;sequence>
 *         &lt;element name="questionAndAnswerList" type="{http://schema.aetna.com/2009/09/aim/CoveredIndividual}QuestionAndAnswerList" minOccurs="0"/>
 *         &lt;element name="prescriptionMedicationList" type="{http://schema.aetna.com/2009/09/aim/CoveredIndividual}PrescriptionMedicationList" minOccurs="0"/>
 *         &lt;element name="physicianList" type="{http://schema.aetna.com/2009/09/aim/CoveredIndividual}PhyscianList" minOccurs="0"/>
 *         &lt;element name="lastDoctorVisit" type="{http://schema.aetna.com/2009/09/aim/CoveredIndividual}DoctorVisit" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HealthHistory", propOrder = {
    "questionAndAnswerList",
    "prescriptionMedicationList",
    "physicianList",
    "lastDoctorVisit"
})
public class HealthHistory
    extends Section
{

    protected QuestionAndAnswerList questionAndAnswerList;
    protected PrescriptionMedicationList prescriptionMedicationList;
    protected PhyscianList physicianList;
    protected DoctorVisit lastDoctorVisit;

    /**
     * Gets the value of the questionAndAnswerList property.
     * 
     * @return
     *     possible object is
     *     {@link QuestionAndAnswerList }
     *     
     */
    public QuestionAndAnswerList getQuestionAndAnswerList() {
        return questionAndAnswerList;
    }

    /**
     * Sets the value of the questionAndAnswerList property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuestionAndAnswerList }
     *     
     */
    public void setQuestionAndAnswerList(QuestionAndAnswerList value) {
        this.questionAndAnswerList = value;
    }

    /**
     * Gets the value of the prescriptionMedicationList property.
     * 
     * @return
     *     possible object is
     *     {@link PrescriptionMedicationList }
     *     
     */
    public PrescriptionMedicationList getPrescriptionMedicationList() {
        return prescriptionMedicationList;
    }

    /**
     * Sets the value of the prescriptionMedicationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrescriptionMedicationList }
     *     
     */
    public void setPrescriptionMedicationList(PrescriptionMedicationList value) {
        this.prescriptionMedicationList = value;
    }

    /**
     * Gets the value of the physicianList property.
     * 
     * @return
     *     possible object is
     *     {@link PhyscianList }
     *     
     */
    public PhyscianList getPhysicianList() {
        return physicianList;
    }

    /**
     * Sets the value of the physicianList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhyscianList }
     *     
     */
    public void setPhysicianList(PhyscianList value) {
        this.physicianList = value;
    }

    /**
     * Gets the value of the lastDoctorVisit property.
     * 
     * @return
     *     possible object is
     *     {@link DoctorVisit }
     *     
     */
    public DoctorVisit getLastDoctorVisit() {
        return lastDoctorVisit;
    }

    /**
     * Sets the value of the lastDoctorVisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link DoctorVisit }
     *     
     */
    public void setLastDoctorVisit(DoctorVisit value) {
        this.lastDoctorVisit = value;
    }

}
