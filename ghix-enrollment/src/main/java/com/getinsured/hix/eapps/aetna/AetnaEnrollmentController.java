package com.getinsured.hix.eapps.aetna;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hix.dto.enrollment.AetnaEnrollment;
import com.getinsured.hix.dto.enrollment.AetnaEnrollmentDTO;
import com.getinsured.hix.eapps.aetna.aim.basicdatatypes.YesNoIndicator;
import com.getinsured.hix.eapps.aetna.aim.contactinfo.Address;
import com.getinsured.hix.eapps.aetna.aim.contactinfo.USStateCode;
import com.getinsured.hix.eapps.aetna.aim.contactinfo.USStateCodeField;
import com.getinsured.hix.eapps.aetna.aim.contactinfo.ZipCodeField;
import com.getinsured.hix.eapps.aetna.aim.coveredindividual.AddressList;
import com.getinsured.hix.eapps.aetna.aim.coveredindividual.CaseManagement;
import com.getinsured.hix.eapps.aetna.aim.coveredindividual.CaseManagementQAList;
import com.getinsured.hix.eapps.aetna.aim.coveredindividual.CitizenshipInfo;
import com.getinsured.hix.eapps.aetna.aim.coveredindividual.Dependent;
import com.getinsured.hix.eapps.aetna.aim.coveredindividual.Spouse;
import com.getinsured.hix.eapps.aetna.aim.coveredindividual.Subscriber;
import com.getinsured.hix.eapps.aetna.aim.individualapplication.ApplicationHeader;
import com.getinsured.hix.eapps.aetna.aim.individualapplication.CoveredIndividualList;
import com.getinsured.hix.eapps.aetna.aim.individualapplication.InBoundApplication;
import com.getinsured.hix.eapps.aetna.aim.individualapplication.InBoundIndividualApplication;
import com.getinsured.hix.eapps.aetna.aim.meta.Field;
import com.getinsured.hix.eapps.aetna.aim.meta.FieldList;
import com.getinsured.hix.eapps.aetna.aim.meta.FieldOptCompleteDate;
import com.getinsured.hix.eapps.aetna.aim.meta.FieldOptDate;
import com.getinsured.hix.eapps.aetna.aim.meta.FieldOptString;
import com.getinsured.hix.eapps.aetna.aim.meta.FieldOptUnsignedSmallInt;
import com.getinsured.hix.eapps.aetna.aim.meta.Section;
import com.getinsured.hix.eapps.aetna.aim.meta.SectionList;
import com.getinsured.hix.eapps.aetna.aim.paymentinfo.CreditCard;
import com.getinsured.hix.eapps.aetna.aim.paymentinfo.ElectronicFundTransfer;
import com.getinsured.hix.eapps.aetna.aim.paymentinfo.InitialPayment;
import com.getinsured.hix.eapps.aetna.aim.paymentinfo.PaymentInfo;
import com.getinsured.hix.eapps.aetna.aim.paymentinfo.RecurringPayment;
import com.getinsured.hix.eapps.aetna.aim.person.Name;
import com.getinsured.hix.eapps.aetna.ws.ApplicationSubmissionResponse;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

import org.springframework.beans.factory.annotation.Qualifier;

/**
 * 
 * @author meher_a
 * @since 08/07/2013
 */

@Controller
@RequestMapping(value = "/aetna")
public class AetnaEnrollmentController {
	
	private static final Logger LOGGER = Logger.getLogger(AetnaEnrollmentController.class);
	private static final String DATE_FORMAT = "MM/DD/yyyy";
	XStream xstream= GhixUtils.getXStreamStaxObject();

	
	/**
	 * @author panda_p
	 * This method is used to test the working of enrollment module after deployment  
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
    
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody public String welcome(){
		
		LOGGER.info("Welcome to Enrollment module (welcome()) ::");
		
		return aetnaEnrollmentWebServicMockSubmit("");
	}
	
	@Autowired 
	@Qualifier("wsEnrollment")
	private WebServiceTemplate aetnaWebServiceTemplate;
	/**
	 * @param stringAetnaEnrollmentDTO
	 * @return
	 */
	@RequestMapping(value = "/webservice/aetna/applicationsubmission", method = RequestMethod.POST)
	@ResponseBody public String aetnaEnrollmentWebServicMockSubmit(@RequestBody String  stringAetnaEnrollmentDTO){
		
		LOGGER.info("Aetna Enrollment Web Service Controller :: aetnaEnrollmentWebServicMockPageSubmit()");
		InBoundApplication application=new InBoundApplication();
		CoveredIndividualList coveredIndividualList = new CoveredIndividualList();
		
		AetnaEnrollmentDTO aetnaEnrollmentDTO=(AetnaEnrollmentDTO)xstream.fromXML(stringAetnaEnrollmentDTO);
		
		// setting the  data to the respective objects from the AetnaEnrollmentDTO object
	    if(aetnaEnrollmentDTO.getAetnaEnrollmentList() != null && !aetnaEnrollmentDTO.getAetnaEnrollmentList().isEmpty()){
	    	for(AetnaEnrollment aetnaEnrollment : aetnaEnrollmentDTO.getAetnaEnrollmentList()){
	    		
	    		  // setting the caseManagement Fields
	    			CaseManagement caseManagement = new CaseManagement();
		    		CaseManagementQAList caseManagementQAList = new CaseManagementQAList();
		    		Field field = new Field();
		    		field.setContextCode("CM100");
		    		field.setValue(aetnaEnrollment.getCaseManagementCM100());
		    		caseManagementQAList.getCaseManagementQA().add(field);
		    		Field field1 = new Field();
		    		field1.setContextCode("CM101");
		    		field1.setValue(aetnaEnrollment.getCaseManagementCM101());
		    		caseManagementQAList.getCaseManagementQA().add(field1);
		    		Field field2 = new Field();
		    		field2.setContextCode("CM102");
		    		field2.setValue(aetnaEnrollment.getCaseManagementCM102());
		    		caseManagementQAList.getCaseManagementQA().add(field2);
		    		Field field3 = new Field();
		    		field3.setContextCode("CM103");
		    		field3.setValue(aetnaEnrollment.getCaseManagementCM103());
		    		caseManagementQAList.getCaseManagementQA().add(field3);
		    		Field field4 = new Field();
		    		field4.setContextCode("CM104");
		    		field4.setValue(aetnaEnrollment.getCaseManagementCM104());
		    		caseManagementQAList.getCaseManagementQA().add(field4);
		    		Field field5 = new Field();
		    		field5.setContextCode("CM105");
		    		field5.setValue(aetnaEnrollment.getCaseManagementCM105());
		    		caseManagementQAList.getCaseManagementQA().add(field5);
		    		Field field6 = new Field();
		    		field6.setContextCode("CM106");
		    		field6.setValue(aetnaEnrollment.getCaseManagementCM106());
		    		caseManagementQAList.getCaseManagementQA().add(field6);
		    		Field field7 = new Field();
		    		field7.setContextCode("CM107");
		    		field7.setValue(aetnaEnrollment.getCaseManagementCM107());
		    		caseManagementQAList.getCaseManagementQA().add(field7);
		    		Field field8 = new Field();
		    		field8.setContextCode("CM108");
		    		field8.setValue(aetnaEnrollment.getCaseManagementCM108());
		    		caseManagementQAList.getCaseManagementQA().add(field8);
		    		Field field9 = new Field();
		    		field9.setContextCode("CM109");
		    		field9.setValue(aetnaEnrollment.getCaseManagementCM109());
		    		caseManagementQAList.getCaseManagementQA().add(field9);
		    		Field field10 = new Field();
		    		field10.setContextCode("CM110");
		    		field10.setValue(aetnaEnrollment.getCaseManagementCM110());
		    		caseManagementQAList.getCaseManagementQA().add(field10);
		    		Field field11 = new Field();
		    		field11.setContextCode("CM111");
		    		field11.setValue(aetnaEnrollment.getCaseManagementCM111());
		    		caseManagementQAList.getCaseManagementQA().add(field11);
		    		Field field12 = new Field();
		    		field12.setContextCode("CM112");
		    		field12.setValue(aetnaEnrollment.getCaseManagementCM112());
		    		caseManagementQAList.getCaseManagementQA().add(field12);
		    		Field field13 = new Field();
		    		field13.setContextCode("CM113");
		    		field13.setValue(aetnaEnrollment.getCaseManagementCM113());
		    		caseManagementQAList.getCaseManagementQA().add(field13);
		    		Field field14 = new Field();
		    		field14.setContextCode("CM114");
		    		field14.setValue(aetnaEnrollment.getCaseManagementCM114());
		    		caseManagementQAList.getCaseManagementQA().add(field14);
		    		Field field15 = new Field();
		    		field15.setContextCode("CM115");
		    		field15.setValue(aetnaEnrollment.getCaseManagementCM115());
		    		caseManagementQAList.getCaseManagementQA().add(field15);
		    		Field field16 = new Field();
		    		field16.setContextCode("CM116");
		    		field16.setValue(aetnaEnrollment.getCaseManagementCM116());
		    		caseManagementQAList.getCaseManagementQA().add(field16);
		    		Field field17 = new Field();
		    		field17.setContextCode("CM117");
		    		field17.setValue(aetnaEnrollment.getCaseManagementCM117());
		    		caseManagementQAList.getCaseManagementQA().add(field17);
		    		Field field18 = new Field();
		    		field18.setContextCode("CM118");
		    		field18.setValue(aetnaEnrollment.getCaseManagementCM118());
		    		caseManagementQAList.getCaseManagementQA().add(field18);
		    		Field field19 = new Field();
		    		field19.setContextCode("CM119");
		    		field19.setValue(aetnaEnrollment.getCaseManagementCM119());
		    		caseManagementQAList.getCaseManagementQA().add(field19);
		    		Field field20 = new Field();
		    		field20.setContextCode("CM120");
		    		field20.setValue(aetnaEnrollment.getCaseManagementCM120());
		    		caseManagementQAList.getCaseManagementQA().add(field20);
		    		Field field21 = new Field();
		    		field21.setContextCode("CM121");
		    		field21.setValue(aetnaEnrollment.getCaseManagementCM121());
		    		caseManagementQAList.getCaseManagementQA().add(field21);
		    		Field field22 = new Field();
		    		field22.setContextCode("CM122");
		    		field22.setValue(aetnaEnrollment.getCaseManagementCM122());
		    		caseManagementQAList.getCaseManagementQA().add(field22);
		    		// setting the text value CM123 field
		    		Field field23 = new Field();
		    		field23.setContextCode("CM123");
		    		field23.setValue(aetnaEnrollment.getCaseManagementOther());
		    		caseManagementQAList.getCaseManagementQA().add(field23);
		    		caseManagement.setCaseManagementQAList(caseManagementQAList);
		    		
		    		//setting the citizenshipinfo (insdNo, non-citizen Of US)
		    		CitizenshipInfo citizenshipInfo = new CitizenshipInfo();
		    		if(isNotNullAndEmpty(aetnaEnrollment.getInsdNo())){
		    			citizenshipInfo.setInsIDNumber(getfieldOptString(aetnaEnrollment.getInsdNo(), ""));
		    		}
		    		if(isNotNullAndEmpty(aetnaEnrollment.getNotCitizenOfUnitedStates())){
		    			citizenshipInfo.setNonCitizenCheck(getfieldOptString(aetnaEnrollment.getNotCitizenOfUnitedStates(), ""));
		    		}
		    		if(isNotNullAndEmpty(aetnaEnrollment.getArrivalDate())){
		    			citizenshipInfo.setArrivalDate(getFieldOptDate(aetnaEnrollment.getArrivalDate(),""));
		    		}
		    		Section employmentInfo = new Section();
		    		if(isNotNullAndEmpty(aetnaEnrollment.getEmploymentInfo())){
		    			employmentInfo.setContextCode("employmentInfo");
		    			FieldList employmentInfoFieldList = new FieldList();
		    			Field employmentInfoField = new Field();
		    			employmentInfoField.setContextCode("employmentInfo");
		    			employmentInfoField.setValue(aetnaEnrollment.getEmploymentInfo());
		    			employmentInfoFieldList.getField().add(employmentInfoField);
		    			employmentInfo.setFieldList(employmentInfoFieldList);
		    		}
		    		
		    		Section stateOfBirth = new Section();
		    		if(isNotNullAndEmpty(aetnaEnrollment.getStateOfBirth())){
		    			stateOfBirth.setContextCode("stateOfBirth");
		    			FieldList stateOfBirthFieldList = new FieldList();
		    			Field stateOfBirthField = new Field();
		    			stateOfBirthField.setContextCode("stateOfBirth");
		    			stateOfBirthField.setValue(aetnaEnrollment.getStateOfBirth());
		    			stateOfBirthFieldList.getField().add(stateOfBirthField);
		    			stateOfBirth.setFieldList(stateOfBirthFieldList);
		    		}
		    		
		    		FieldOptUnsignedSmallInt timeSpentOutOfState = new FieldOptUnsignedSmallInt();
		    		if(isNotNullAndEmpty(aetnaEnrollment.getTimeSpentOutOfState())){
		    			timeSpentOutOfState.setContextCode("timeSpentOutOfState");
		    			timeSpentOutOfState.setValue(Integer.parseInt(aetnaEnrollment.getTimeSpentOutOfState()));
		    		}
		    		
		    		Section otherInsuranceForHCR = new Section();
		    		if(isNotNullAndEmpty(aetnaEnrollment.getOtherInsuranceForHCR())){
		    			otherInsuranceForHCR.setContextCode("otherInsuranceForHCR");
		    			FieldList otherInsuranceForHCRFieldList = new FieldList();
		    			Field otherInsuranceForHCRField = new Field();
		    			otherInsuranceForHCRField.setContextCode("otherInsuranceForHCR");
		    			otherInsuranceForHCRField.setValue(aetnaEnrollment.getStateOfBirth());
		    			otherInsuranceForHCRFieldList.getField().add(otherInsuranceForHCRField);
		    			employmentInfo.setFieldList(otherInsuranceForHCRFieldList);
		    		}
		    		
		    		if(aetnaEnrollment.getFamilyCode() != null && aetnaEnrollment.getFamilyCode().equalsIgnoreCase("APP")){
	    			Subscriber subscriber = new Subscriber();
	    			//setting the values of enrollment reason
		    		subscriber.setEnrollmentReason(getfieldOptString(aetnaEnrollment.getEnrollmentReason(), ""));
		    		//setting the values of subscriber firstname and lastname
		    		Name name = new Name();
		    		name.setFirstName(getfieldOptString(aetnaEnrollment.getSubscriberFirstName(), ""));
		    		name.setLastName(getfieldOptString(aetnaEnrollment.getSubscriberLastName(), ""));
		    		subscriber.setName(name);
		    		//setting the  dob
		    		subscriber.setDob(getfieldOptCompleteDate(aetnaEnrollment.getSubscriberDob(), ""));
		    		//setting the familyCode	
		    		subscriber.setFamilyCode(getfieldOptString(aetnaEnrollment.getFamilyCode(), ""));
		    		//setting the Gender
		    		subscriber.setGender(getfieldOptString(aetnaEnrollment.getGender(), ""));
		    		//setting coveredIndividualStatus
		    		subscriber.setStatusCode(getfieldOptString(aetnaEnrollment.getCoveredIndividualStatus(), ""));
		    		//setting zipcode & state
		    		Address address = new Address();
		    		ZipCodeField zipCodeField = new ZipCodeField();
		    		zipCodeField.setValue(aetnaEnrollment.getSubscriberZip());
		    		address.setZipcode(zipCodeField);
		    		USStateCodeField usStateCodeField = new USStateCodeField();
		    		if(isNotNullAndEmpty(aetnaEnrollment.getState())){
		    			usStateCodeField.setValue(USStateCode.fromValue(aetnaEnrollment.getState()));
		    			address.setState(usStateCodeField);
		    		}
		    	 	AddressList addressList = new AddressList();
		    		addressList.setMailingAddress(address);
		    		subscriber.setAddressList(addressList);
		    		//setting tobacco Use
		    		if(isNotNullAndEmpty(aetnaEnrollment.getTobaccoUse())){
		    			subscriber.setTobaccoStatus(YesNoIndicator.fromValue(aetnaEnrollment.getTobaccoUse()));
		    		}
		    				    		
		    		//setting the citizenshipinfo
		    		subscriber.setCitizenshipInfo(citizenshipInfo);
		    		
		    		// setting the caseManagement 
		    		subscriber.setCaseManagement(caseManagement);
		    		// setting the enrollmentPeriodConfirm
		    		subscriber.setEnrollmentPeriodConfirm(getfieldOptString(aetnaEnrollment.getEnrollmentPeriodConfirm(), ""));
		    		// setting the enrollmentPeriodQns
		    		subscriber.setEnrollmentPeriodQns(getfieldOptString(aetnaEnrollment.getEnrollmentPeriodQns(), ""));
		    		// setting the enrollmentPeriodOther
		    		subscriber.setEnrollmentPeriodOther(getfieldOptString(aetnaEnrollment.getEnrollmentPeriodOther(), ""));
		    		// setting the dateOfEvent
		    		subscriber.setDateOfEvent(getFieldOptDate(aetnaEnrollment.getDateOfEvent(), ""));
		    		// setting the renewalDate
		    		subscriber.setRenewalDate(getFieldOptDate(aetnaEnrollment.getRenewalDate(), ""));
		    		// setting the employmentInfo
		    		subscriber.setEmploymentInfo(employmentInfo);
		    		// setting the stateOfBirth
		    		subscriber.setStateOfBirth(stateOfBirth);
		    		// setting the timeSpentOutOfState
		    		subscriber.setTimeSpentOutOfState(timeSpentOutOfState);
		    		// setting the otherInsuranceForHCR
		    		subscriber.setOtherInsuranceForHCR(otherInsuranceForHCR);
		    		
		    		coveredIndividualList.setSubscriber(subscriber);
		    		
		    		}else if(aetnaEnrollment.getFamilyCode() != null && aetnaEnrollment.getFamilyCode().equalsIgnoreCase("SP")){
		    			Spouse spouse = new Spouse();
		    			
			    		//setting the values of subscriber firstname and lastname
			    		Name name = new Name();
			    		name.setFirstName(getfieldOptString(aetnaEnrollment.getSubscriberFirstName(), ""));
			    		name.setLastName(getfieldOptString(aetnaEnrollment.getSubscriberLastName(), ""));
			    		spouse.setName(name);
			    		//setting the  dob
			    		spouse.setDob(getfieldOptCompleteDate(aetnaEnrollment.getSubscriberDob(), ""));
			    		//setting the familyCode	
			    		spouse.setFamilyCode(getfieldOptString(aetnaEnrollment.getFamilyCode(), ""));
			    		//setting the Gender
			    		spouse.setGender(getfieldOptString(aetnaEnrollment.getGender(), ""));
			    		//setting coveredIndividualStatus
			    		spouse.setStatusCode(getfieldOptString(aetnaEnrollment.getCoveredIndividualStatus(), ""));
			    		//setting zipcode & state
			    		Address address = new Address();
			    		ZipCodeField zipCodeField = new ZipCodeField();
			    		zipCodeField.setValue(aetnaEnrollment.getSubscriberZip());
			    		address.setZipcode(zipCodeField);
			    		USStateCodeField usStateCodeField = new USStateCodeField();
			    		usStateCodeField.setValue(USStateCode.fromValue(aetnaEnrollment.getState()));
			    		address.setState(usStateCodeField);
			    		//setting tobacco Use
			    		spouse.setTobaccoStatus(YesNoIndicator.fromValue(aetnaEnrollment.getTobaccoUse()));
			    		//setting the citizenshipinfo
			    		spouse.setCitizenshipInfo(citizenshipInfo);
			    		
			    		// setting the caseManagement
			    		spouse.setCaseManagement(caseManagement);
		    			coveredIndividualList.setSpouse(spouse);
		    			
		    			}else if(aetnaEnrollment.getFamilyCode() != null && aetnaEnrollment.getFamilyCode().equalsIgnoreCase("01")){
		    				Dependent dependent = new Dependent();
		    				
		    				//setting the values of subscriber firstname and lastname
				    		Name name = new Name();
				    		name.setFirstName(getfieldOptString(aetnaEnrollment.getSubscriberFirstName(), ""));
				    		name.setLastName(getfieldOptString(aetnaEnrollment.getSubscriberLastName(), ""));
				    		dependent.setName(name);
				    		//setting the  dob
				    		dependent.setDob(getfieldOptCompleteDate(aetnaEnrollment.getSubscriberDob(), ""));
				    		//setting the familyCode	
				    		dependent.setFamilyCode(getfieldOptString(aetnaEnrollment.getFamilyCode(), ""));
				    		//setting the Gender
				    		dependent.setGender(getfieldOptString(aetnaEnrollment.getGender(), ""));
				    		//setting coveredIndividualStatus
				    		dependent.setStatusCode(getfieldOptString(aetnaEnrollment.getCoveredIndividualStatus(), ""));
				    		//setting zipcode & state
				    		Address address = new Address();
				    		ZipCodeField zipCodeField = new ZipCodeField();
				    		zipCodeField.setValue(aetnaEnrollment.getSubscriberZip());
				    		address.setZipcode(zipCodeField);
				    		USStateCodeField usStateCodeField = new USStateCodeField();
				    		usStateCodeField.setValue(USStateCode.fromValue(aetnaEnrollment.getState()));
				    		address.setState(usStateCodeField);
				    		//setting tobacco Use
				    		dependent.setTobaccoStatus(YesNoIndicator.fromValue(aetnaEnrollment.getTobaccoUse()));
				    		//setting the citizenshipinfo
				    		dependent.setCitizenshipInfo(citizenshipInfo);
				    		
				    		// setting the caseManagement
				    	    dependent.setCaseManagement(caseManagement);
				    		
				    	    // setting eligibleMilitaryVeteran
				    	    dependent.setEligibleMilitaryVeteran(YesNoIndicator.fromValue(aetnaEnrollment.getEligibleMilitaryVeteran()));
				    	   // setting eligibleMilitaryVeteran
				    	    dependent.setRelationshipToApplicant(getfieldOptString(aetnaEnrollment.getRelationshipToApplicant(), ""));
				    	    coveredIndividualList.getDependent().add(dependent);
		    				
		    			}
	    	}
	    	application.setCoveredIndividualList(coveredIndividualList);
	    	
	    	application.setEnrollmentFormId(aetnaEnrollmentDTO.getEnrollmentFormId());
	    	
	    	application.setRequestedEffectiveDate(getfieldOptCompleteDate(aetnaEnrollmentDTO.getRequestedEffectiveDate(),""));
	    	
	    	application.setApplicationName(getfieldOptString(aetnaEnrollmentDTO.getApplicationId(),""));
	    	
	    	application.setApplicationStatus(getfieldOptString(aetnaEnrollmentDTO.getApplicationStatus(), ""));
	    	
	    	
	    	
	    	PaymentInfo paymentInfo = new PaymentInfo();
	    	
	    	InitialPayment initialPayment = new InitialPayment();
	    	
	    	if(aetnaEnrollmentDTO.getCreditCardAccountNumber() != null){
	    		initialPayment.setPaymentType("2"); // 2 = creditCard
	    		
	    		CreditCard card = new CreditCard();
	    		card.setAccountNumber(getfieldOptString(aetnaEnrollmentDTO.getCreditCardAccountNumber(), ""));
	    		card.setCardType(getfieldOptString(aetnaEnrollmentDTO.getCardType(), ""));
	    		
	    		initialPayment.setCreditCard(card);
	    		
	    	}else if(aetnaEnrollmentDTO.getElectronicFundAccountNumber() != null){
	    		initialPayment.setPaymentType("1"); // 1 = electronicFundTransfer
	    		
	    		ElectronicFundTransfer electronicFundTransfer = new ElectronicFundTransfer();
	    		electronicFundTransfer.setAccountNumber(getfieldOptString(aetnaEnrollmentDTO.getElectronicFundAccountNumber(), ""));
	    		electronicFundTransfer.setBankRoutingNumber(getfieldOptString(aetnaEnrollmentDTO.getRoutingNumber(), ""));
	    		
	    		initialPayment.setElectronicFundTransfer(electronicFundTransfer);
	    	}
	    	
	    	paymentInfo.setInitialPayment(initialPayment);
	    	
	    	RecurringPayment recurringPayment = new RecurringPayment(); 
	    	
	    	if(aetnaEnrollmentDTO.getElectronicFundAccountNumber() != null){
	    		recurringPayment.setPaymentType("2"); // 2 = electronicFundTransfer
	    		
	    		ElectronicFundTransfer electronicFundTransfer = new ElectronicFundTransfer();
	    		electronicFundTransfer.setAccountNumber(getfieldOptString(aetnaEnrollmentDTO.getElectronicFundAccountNumber(), ""));
	    		electronicFundTransfer.setBankRoutingNumber(getfieldOptString(aetnaEnrollmentDTO.getRoutingNumber(), ""));
	    		
	    		recurringPayment.setElectronicFundTransfer(electronicFundTransfer);
	    	}
	    	
	    	paymentInfo.setRecurringPayment(recurringPayment);
	    	
	    	application.setPaymentInfo(paymentInfo);
	    }
		
		aetnaWebServiceTemplate.setDefaultUri("http://localhost:8088/mockApplicationSubmissionSOAP?wsdl");
		
		ApplicationHeader header = new ApplicationHeader();
	    
	    
	    header.setContextCode("");
	    header.setFieldList(new FieldList());
	    header.setMetaURL("");
	    header.setSectionList(new SectionList());
	    
	    
	    
	    
	    InBoundIndividualApplication inBoundIndividualApplication = new InBoundIndividualApplication();

	    com.getinsured.hix.eapps.aetna.aim.individualapplication.ObjectFactory factory = new com.getinsured.hix.eapps.aetna.aim.individualapplication.ObjectFactory();
		
		
		//inBoundIndividualApplication = factory.createInBoundIndividualApplication();
		
		inBoundIndividualApplication.setHeader(header);
		inBoundIndividualApplication.setApplication(application);
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("XML -->\n"+xstream.toXML(inBoundIndividualApplication));
		}
		
		JAXBElement<InBoundIndividualApplication> jaxbInBoundIndividualApplication = factory.createInBoundIndividualApplication(inBoundIndividualApplication);
		
		JAXBElement<ApplicationSubmissionResponse> applicationSubmissionResponse = (JAXBElement<ApplicationSubmissionResponse>)aetnaWebServiceTemplate.marshalSendAndReceive(jaxbInBoundIndividualApplication);
		
		LOGGER.info("Aetna Enrollment Web Service Controller :: aetnaEnrollmentWebServicMockPageSubmit() Message "+applicationSubmissionResponse.getValue().getErrorMessage());
		
		LOGGER.info("Aetna Enrollment Web Service Controller :: aetnaEnrollmentWebServicMockPageSubmit() ResponseCode :"+applicationSubmissionResponse.getValue().getResponseCode());
		
	 return applicationSubmissionResponse.getValue().getErrorMessage();
	}
	
	private FieldOptString getfieldOptString(String value, String contextCode) {
		
	    FieldOptString fieldOptString = new FieldOptString();			
		fieldOptString.setValue(value);
		fieldOptString.setContextCode(contextCode);
		return fieldOptString;
	}
	
	private FieldOptCompleteDate getfieldOptCompleteDate(String dateString,  String contextCode) {
		FieldOptCompleteDate fieldOptCompleteDate;
	    Calendar calendar = TSCalendar.getInstance();
	    java.util.Date dt = DateUtil.StringToDate(dateString, DATE_FORMAT);
	    calendar.setTime(dt);
	    fieldOptCompleteDate = new FieldOptCompleteDate();
		fieldOptCompleteDate.setDay(calendar.get(Calendar.DATE));
		fieldOptCompleteDate.setMonth(calendar.get(Calendar.MONTH)+1);
		fieldOptCompleteDate.setYear(calendar.get(Calendar.YEAR));
		fieldOptCompleteDate.setContextCode(contextCode);
		return fieldOptCompleteDate;
	}
	
	private FieldOptDate getFieldOptDate(String dateString,  String contextCode) {
		FieldOptDate fieldOptDate;
	    Calendar calendar = TSCalendar.getInstance();
	    java.util.Date dt = DateUtil.StringToDate(dateString, DATE_FORMAT);
	    calendar.setTime(dt);
	    fieldOptDate = new FieldOptDate();
		fieldOptDate.setDay(calendar.get(Calendar.DATE));
		fieldOptDate.setMonth(calendar.get(Calendar.MONTH)+1);
		fieldOptDate.setYear(calendar.get(Calendar.YEAR));
		fieldOptDate.setContextCode(contextCode);
		return fieldOptDate;
	}
}
