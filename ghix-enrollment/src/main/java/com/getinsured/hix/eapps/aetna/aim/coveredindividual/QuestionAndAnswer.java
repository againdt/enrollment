//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.24 at 02:43:46 PM IST 
//


package com.getinsured.hix.eapps.aetna.aim.coveredindividual;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.eapps.aetna.aim.meta.Field;


/**
 * <p>Java class for QuestionAndAnswer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuestionAndAnswer">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schema.aetna.com/2009/09/aim/Meta}Field">
 *       &lt;sequence>
 *         &lt;element name="illnessConditionList" type="{http://schema.aetna.com/2009/09/aim/CoveredIndividual}IllnessConditionList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuestionAndAnswer", propOrder = {
    "illnessConditionList"
})
public class QuestionAndAnswer
    extends Field
{

    protected IllnessConditionList illnessConditionList;

    /**
     * Gets the value of the illnessConditionList property.
     * 
     * @return
     *     possible object is
     *     {@link IllnessConditionList }
     *     
     */
    public IllnessConditionList getIllnessConditionList() {
        return illnessConditionList;
    }

    /**
     * Sets the value of the illnessConditionList property.
     * 
     * @param value
     *     allowed object is
     *     {@link IllnessConditionList }
     *     
     */
    public void setIllnessConditionList(IllnessConditionList value) {
        this.illnessConditionList = value;
    }

}
