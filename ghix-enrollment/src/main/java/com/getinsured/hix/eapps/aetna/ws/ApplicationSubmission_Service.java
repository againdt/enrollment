
package com.getinsured.hix.eapps.aetna.ws;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "ApplicationSubmission", targetNamespace = "http://schema.aetna.com/2009/09/ws/aim/iqs/ApplicationSubmission", wsdlLocation = "file:/D:/vemo/Aetna/eVendor%20Schema%2001012014/ApplicationSubmission.wsdl")
public class ApplicationSubmission_Service
    extends Service
{

    private final static URL APPLICATIONSUBMISSION_WSDL_LOCATION;
    private final static Logger logger = Logger.getLogger(com.getinsured.hix.eapps.aetna.ws.ApplicationSubmission_Service.class.getName());

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = com.getinsured.hix.eapps.aetna.ws.ApplicationSubmission_Service.class.getResource(".");
            url = new URL(baseUrl, "file:/D:/vemo/Aetna/eVendor%20Schema%2001012014/ApplicationSubmission.wsdl");
        } catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: 'file:/D:/vemo/Aetna/eVendor%20Schema%2001012014/ApplicationSubmission.wsdl', retrying as a local file");
            logger.warning(e.getMessage());
        }
        APPLICATIONSUBMISSION_WSDL_LOCATION = url;
    }

    public ApplicationSubmission_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ApplicationSubmission_Service() {
        super(APPLICATIONSUBMISSION_WSDL_LOCATION, new QName("http://schema.aetna.com/2009/09/ws/aim/iqs/ApplicationSubmission", "ApplicationSubmission"));
    }

    /**
     * 
     * @return
     *     returns ApplicationSubmission
     */
    @WebEndpoint(name = "ApplicationSubmissionSOAP")
    public ApplicationSubmission getApplicationSubmissionSOAP() {
        return super.getPort(new QName("http://schema.aetna.com/2009/09/ws/aim/iqs/ApplicationSubmission", "ApplicationSubmissionSOAP"), ApplicationSubmission.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ApplicationSubmission
     */
    @WebEndpoint(name = "ApplicationSubmissionSOAP")
    public ApplicationSubmission getApplicationSubmissionSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://schema.aetna.com/2009/09/ws/aim/iqs/ApplicationSubmission", "ApplicationSubmissionSOAP"), ApplicationSubmission.class, features);
    }

}
