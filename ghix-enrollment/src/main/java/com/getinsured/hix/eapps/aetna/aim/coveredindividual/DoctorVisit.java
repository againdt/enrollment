//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.24 at 02:43:46 PM IST 
//


package com.getinsured.hix.eapps.aetna.aim.coveredindividual;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.eapps.aetna.aim.meta.FieldOptDate;
import com.getinsured.hix.eapps.aetna.aim.meta.FieldOptString;
import com.getinsured.hix.eapps.aetna.aim.meta.Section;


/**
 * <p>Java class for DoctorVisit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DoctorVisit">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schema.aetna.com/2009/09/aim/Meta}Section">
 *       &lt;sequence>
 *         &lt;element name="purpose" type="{http://schema.aetna.com/2009/09/aim/Meta}Field-Opt-String" minOccurs="0"/>
 *         &lt;element name="visitDate" type="{http://schema.aetna.com/2009/09/aim/Meta}Field-Opt-Date" minOccurs="0"/>
 *         &lt;element name="physician" type="{http://schema.aetna.com/2009/09/aim/CoveredIndividual}Physician" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DoctorVisit", propOrder = {
    "purpose",
    "visitDate",
    "physician"
})
public class DoctorVisit
    extends Section
{

    protected FieldOptString purpose;
    protected FieldOptDate visitDate;
    protected Physician physician;

    /**
     * Gets the value of the purpose property.
     * 
     * @return
     *     possible object is
     *     {@link FieldOptString }
     *     
     */
    public FieldOptString getPurpose() {
        return purpose;
    }

    /**
     * Sets the value of the purpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldOptString }
     *     
     */
    public void setPurpose(FieldOptString value) {
        this.purpose = value;
    }

    /**
     * Gets the value of the visitDate property.
     * 
     * @return
     *     possible object is
     *     {@link FieldOptDate }
     *     
     */
    public FieldOptDate getVisitDate() {
        return visitDate;
    }

    /**
     * Sets the value of the visitDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldOptDate }
     *     
     */
    public void setVisitDate(FieldOptDate value) {
        this.visitDate = value;
    }

    /**
     * Gets the value of the physician property.
     * 
     * @return
     *     possible object is
     *     {@link Physician }
     *     
     */
    public Physician getPhysician() {
        return physician;
    }

    /**
     * Sets the value of the physician property.
     * 
     * @param value
     *     allowed object is
     *     {@link Physician }
     *     
     */
    public void setPhysician(Physician value) {
        this.physician = value;
    }

}
