//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.24 at 02:43:46 PM IST 
//


package com.getinsured.hix.eapps.aetna.aim.coveredindividual;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.eapps.aetna.aim.meta.Section;


/**
 * <p>Java class for CaseManagement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseManagement">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schema.aetna.com/2009/09/aim/Meta}Section">
 *       &lt;sequence>
 *         &lt;element name="caseManagementQAList" type="{http://schema.aetna.com/2009/09/aim/CoveredIndividual}CaseManagementQAList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseManagement", propOrder = {
    "caseManagementQAList"
})
public class CaseManagement
    extends Section
{

    protected CaseManagementQAList caseManagementQAList;

    /**
     * Gets the value of the caseManagementQAList property.
     * 
     * @return
     *     possible object is
     *     {@link CaseManagementQAList }
     *     
     */
    public CaseManagementQAList getCaseManagementQAList() {
        return caseManagementQAList;
    }

    /**
     * Sets the value of the caseManagementQAList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseManagementQAList }
     *     
     */
    public void setCaseManagementQAList(CaseManagementQAList value) {
        this.caseManagementQAList = value;
    }

}
