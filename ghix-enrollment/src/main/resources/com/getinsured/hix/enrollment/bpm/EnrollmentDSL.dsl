[condition][$application]There is an Application=$application : ApplicationFact()
[condition][$application]- it has been approved=status == "APPROVED"
[condition][$application]- it is pending electronic signature=status == "ESIG_PENDING"
[consequence][]Send an email to the customer=insert(new ApplicationEmailFact());
[consequence][]Create a new review task=insert(new ApplicationTaskFact());