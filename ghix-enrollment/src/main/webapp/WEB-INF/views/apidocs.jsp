<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Enrollment API Docs</title>
<!-- needed for mobile devices -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<%String yamlFile = (String)request.getParameter("yaml");%>
	<c:set scope="request" var="yamlFile" value="<%=yamlFile%>" />
	<c:choose>
	    <c:when test="${not empty yamlFile}">
	<redoc spec-url="../../ghix-enrollment/resources/docs/API_yaml_files/${yamlFile}"></redoc>
	<script src="https://rebilly.github.io/ReDoc/releases/latest/redoc.min.js"></script>
	    </c:when>
	    <c:otherwise>
	    	<div style="margin: 20px">
	    		<h1> Enrollment API Documentation </h1>
	    		<ul style="list-style-type:square font">
		    	<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollment_API.yaml">Enrollment API</a></li>
		    	<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollee_API.yaml">Enrollee API</a></li>
    			</ul>
    			<ul style="list-style-type:square"> 
    			<b>Private Exchange</b>
    			<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollment_Private_API.yaml">Enrollment Private Exchange API</a></li>
				<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollee_Private_API.yaml">Enrollee Private Exchange API</a></li>
				<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollment_Anonymous_API.yaml">Enrollment Star Anonymous API</a></li>
				<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollment_FFM_API.yaml">Enrollment FFM Proxy API</a></li>
				<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollment_Off_Exchange_API.yaml">Enrollment Off Exchange API</a></li>
				<br>
				<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollment_Recon_API.yaml">Enrollment Reconciliation API</a></li>
				<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollment_Report_API.yaml">Enrollment Report API</a></li>
				<br>
				<li><a class="btn btn-primary btn-lg" href="docs?yaml=Enrollment_ACA_API.yaml">Enrollment ACA API</a></li>
				</ul>
			</div>
  		</c:otherwise>
    </c:choose>
</body>
</html>