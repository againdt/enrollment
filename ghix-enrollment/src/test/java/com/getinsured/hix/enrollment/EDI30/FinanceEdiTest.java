package com.getinsured.hix.enrollment.EDI30;

import java.math.BigDecimal;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.enrollment.BaseTest;
import com.getinsured.hix.model.IssuerPayments;
/**
 * 
 * @author Kuldeep_S
 *	test classes added to generate XML of IssuerPayment model{EDI 30}
 */
public class FinanceEdiTest extends BaseTest
{
	private static final Logger LOGGER = LoggerFactory.getLogger(FinanceEdiTest.class);
	
	@Test
	//@Ignore
	public void testIssuerPaymentsFunc()
	{
		LOGGER.info("---------------------------- testIssuerPaymentsFunc ----------------------------");
		IssuerPayments issuerPayments = null;
		
		
		try{
			issuerPayments = new IssuerPayments();
			issuerPayments.setStatus("PAID");
	//		issuerPayments.setAmountDueFromLastInvoice(320.00f);
			issuerPayments.setAmountDueFromLastInvoice(new BigDecimal("320.00"));
			
			
		JAXBContext context = JAXBContext.newInstance(IssuerPayments.class);
	    Marshaller marshaller = context.createMarshaller();
	    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	    marshaller.marshal(issuerPayments, System.out);
	    LOGGER.info("Received data is : "+marshaller.toString());
		}
		catch(JAXBException ex)
		{
			LOGGER.error(ex.getMessage());
		}
	}
}
