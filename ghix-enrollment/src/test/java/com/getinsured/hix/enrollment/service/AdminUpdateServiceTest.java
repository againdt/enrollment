/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.enrollment.BaseTest;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.thoughtworks.xstream.XStream;

/**
 * @author raja
 *
 */
public class AdminUpdateServiceTest extends BaseTest{

	@Autowired private EnrollmentService enrollmentService;
	@Autowired private AdminUpdateService adminUpdateService;
	@Autowired private UserService userService;	
	@Autowired private GhixRestTemplate restTemplate;
	
	
	
	/**
	 * Test method for {@link com.getinsured.hix.enrollment.controller.EnrollmentController#testAdminUpdateEnrollmentThroughRestCall(com.getinsured.hix.dto.enrollment.EnrollmentRequest)}.
	 */
	@Test
	public void testAdminUpdateEnrollmentThroughRestCall() {
		
	
		GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		 XStream xstream = GhixUtils.getXStreamGenericObject();
		 EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		 
		 AdminUpdateIndividualRequest adminUpdateIndividualRequest = new AdminUpdateIndividualRequest();
			 
		 AdminUpdateIndividualRequest.Household houseHoldObj = new AdminUpdateIndividualRequest.Household(); 
		 houseHoldObj.setEnrollmentId(19l);
		 houseHoldObj.setHouseholdCaseId(101);
		 	 
		 AdminUpdateIndividualRequest.Household.Members.Member member = new AdminUpdateIndividualRequest.Household.Members.Member();		 
		 member.setMemberId("25");
		 member.setMaintenanceReasonCode("25");
		 member.setFirstName("FirstName1");
		 member.setMiddleName("MiddleName1");
		 member.setLastName("LastName1");
		 AdminUpdateIndividualRequest.Household.Members members = new AdminUpdateIndividualRequest.Household.Members();
		 members.getMember().add(member);
		 houseHoldObj.setMembers(members);
		 
		 adminUpdateIndividualRequest.setHousehold(houseHoldObj);
		 enrollmentRequest.setAdminUpdateIndividualRequest(adminUpdateIndividualRequest);
		String postResp=null;
		
		
			
		  try {
			postResp = ghixRestTemplate
					.exchange(
							GhixEndPoints.EnrollmentEndPoints.ADMIN_UPDATE_ENROLLMENT_URL,
							"exadmin@ghix.com", HttpMethod.POST,
							MediaType.APPLICATION_JSON, String.class,
							xstream.toXML(enrollmentRequest)).getBody();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Opps eror...........");
		}
		
		assertTrue("Update Enrollment Response successfull" , postResp != null);
	}
		
}
