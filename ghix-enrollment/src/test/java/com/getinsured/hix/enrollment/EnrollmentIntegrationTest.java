package com.getinsured.hix.enrollment;

import com.getinsured.hix.enrollment.controller.EnrollmentTestConfiguration;
import com.getinsured.test.JndiInitializer;
import com.getinsured.test.PropertyInitializer;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { EnrollmentTestConfiguration.class})
@WebAppConfiguration
public abstract class EnrollmentIntegrationTest {

    @BeforeClass
    public static void init() throws Exception {
        PropertyInitializer.init();
        JndiInitializer.initialize();
    }

}
