package com.getinsured.hix.enrollment.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.enrollment.BaseTest;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

public class EnrolleeServiceImplTest extends BaseTest{

	@Autowired 
	private EnrolleeService enrolleeService;

	@Autowired 
	private  IEnrolleeRepository iEnrolleeRepository;

	@Autowired
	private  IEnrollmentRepository iEnrollmentRepository;

	@Autowired private GhixRestTemplate restTemplate;
	
	private Enrollee enrollee_1 = null;
	private Enrollment enrollment_1 = null;

	private static final String POLICY_NUMBER = "111";
	private static final String PLAN_NUMBER = "222";
	private static final String ENROLLEE_FIRST_NAME = "Albert";

	@Before
	public void createTestRecords(){
		Plan plan = new Plan();
		plan.setInsuranceType(Plan.PlanInsuranceType.HEALTH.toString());
		plan.setName("Health Plan -- Test");
		plan.setIssuerPlanNumber(PLAN_NUMBER);

		enrollment_1 = new Enrollment();
		enrollment_1.setGrossPremiumAmt(5000.00f);
		enrollment_1.setAptcAmt(1000.00f);
		enrollment_1.setCsrAmt(2000.00f);
		enrollment_1.setNetPremiumAmt(4000.00f);
		enrollment_1.setEmployeeContribution(4000.00f);
		enrollment_1.setEmployerContribution(1000.00f);
		enrollment_1.setGroupPolicyNumber(POLICY_NUMBER);
		enrollment_1 = iEnrollmentRepository.save(enrollment_1);

		enrollee_1 = new Enrollee();
		enrollee_1.setFirstName(ENROLLEE_FIRST_NAME);
		enrollee_1.setEnrollment(enrollment_1);
		enrollee_1 = iEnrolleeRepository.save(enrollee_1);
	}

	@After
	public void deleteTestRecords(){
		iEnrolleeRepository.delete(enrollee_1.getId());
		iEnrollmentRepository.delete(enrollment_1.getId());
	}


	@Test
	public void testFindById() {
		int enrolleeId = enrollee_1.getId();
		Enrollee enrollee = enrolleeService.findById(enrolleeId);

		assertTrue("No Enrollee found with id: " + enrolleeId, enrollee != null);
    	assertEquals("findById fails",enrollee.getId(), enrolleeId);
	}
	
	@Test
	public void testfindEnrolleeByEmployeeIdThroughRestURL() {
		GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		 XStream xstream = GhixUtils.getXStreamGenericObject();
		 EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		 enrolleeRequest.setEmployeeId(8);
		 
		 String postResp = null;
		 try{
			 postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_EMPLOYEE, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrolleeRequest)).getBody(); 
		  }catch(Exception e){
			 e.printStackTrace();
		 }
		 assertTrue("enrollee retrieved succcessfully...!" , postResp != null);
		 
	}
}
