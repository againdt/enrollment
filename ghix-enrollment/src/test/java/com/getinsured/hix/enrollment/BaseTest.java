package com.getinsured.hix.enrollment;

import java.sql.SQLException;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.apache.log4j.xml.DOMConfigurator;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * 
 * @author Samir Vasani
 *	
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/applicationContext.xml")
public abstract class BaseTest 
{
	protected static InitialContext		ic	= null;
	protected static Map<String, String> connInfo = null;

	protected static String				DB_CONNECT_URL	= "jdbc:oracle:thin:@ghixdb.com:1521:ghixdb";
	protected static String				DB_USERNAME		= "STATE_MAIN1";
	protected static String				DB_PASSWORD		= "STATE_MAIN1";
	protected static String				DB_JNDI_NAME	= "jdbc/ghixDS";
	protected static String             DB_JDBC_DRIVER  = "oracle.jdbc.OracleDriver";
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);
	@SuppressWarnings("unused")
	private  static ApplicationContext context = null;
	@BeforeClass
	public static void setUp()
	{
		bindJNDIDataSourceViaSpringContextBuilder();
		try
		{
			context = new ClassPathXmlApplicationContext("/applicationContext.xml");
			String rootPath = System.getenv("GHIX_HOME");
			DOMConfigurator.configure(rootPath + "/ghix-setup/conf/ghix-log4j.xml");
		}
		catch(Exception ex)
		{	
			LOGGER.error("",ex);
		}
	}
	protected static void bindJNDIDataSourceViaSpringContextBuilder()
	{
		try
		{
			SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
			OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();

			ds.setURL(getDbConnectUrl());
			ds.setUser(getDbUsername());
			ds.setPassword(getDbPassword());

			builder.bind(DB_JNDI_NAME, ds);
			builder.activate();
		}
		catch (NamingException ne)
		{
			ne.printStackTrace();
			LOGGER.error("bindJNDIDataSourceViaSpringContextBuilder() " +
					"NamingException occurred while registring JNDI " + getDbJndiName(), ne);
		}
		catch (SQLException sex)
		{
			sex.printStackTrace();
			LOGGER.error("bindJNDIDataSourceViaSpringContextBuilder() " +
					"SQLException occurred while creating OracleConnectionPoolDataSource()", sex);
		}
	}	
	protected static String getDbConnectUrl()
	{
		if(connInfo != null && connInfo.containsKey("url"))
		{
			return connInfo.get("url");
		}
		
		return DB_CONNECT_URL;
	}
	
	/**
	 * Returns database username.
	 * 
	 * @return username to connect to database.
	 */
	protected static String getDbUsername()
	{
		if(connInfo != null && connInfo.containsKey("username"))
		{
			return connInfo.get("username");
		}
		
		return DB_USERNAME;
	}
	
	/**
	 * Returns database password.
	 * 
	 * @return password to be used to connect to database.
	 */
	protected static String getDbPassword()
	{
		if(connInfo != null && connInfo.containsKey("password"))
		{
			return connInfo.get("password");
		}
		
		return DB_PASSWORD;
	}
	
	/**
	 * Returns JNDI name to bind to the system for database access.
	 * 
	 * @return JNDI name.
	 */
	protected static String getDbJndiName()
	{
		if(connInfo != null && connInfo.containsKey("name"))
		{
			return connInfo.get("name");
		}
		
		return DB_JNDI_NAME;
	}

	/**
	 * Returns JDBC driver used for database connection.
	 * 
	 * @return jdbc driver class name (as string) used for database connection.
	 */
	protected static String getDbJdbcDriver()
	{
		if(connInfo != null && connInfo.containsKey("driver"))
		{
			return connInfo.get("driver");
		}
		
		return DB_JDBC_DRIVER;
	}
		
}