/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.MarketType;
import com.getinsured.hix.enrollment.BaseTest;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;


/**
 * @author shanbhag_a
 *
 */
public class EnrollmentServiceImplTest extends BaseTest{

	@Autowired
	private IEnrollmentRepository iEnrollmentRepository;

	//	@Autowired
	//	private IEnrollmentPlanRepository iEnrollmentPlanRepository;

	@Autowired
	private EnrollmentService enrollmentService;
	@Autowired private GhixRestTemplate restTemplate;
	
	private boolean testSaveEnrollment = false;

	private Enrollment mEnrollment = null;
	private Enrollment mEnrollment_1 = null;

	private static final String POLICY_NUMBER = "1111";
	private static final String INDIVIDUAL = "INDV";
	private static final String SHOP = "SHOP";

	@Before
	public void createTestRecords(){
		Plan mPlan = new Plan();
	    mEnrollment = new Enrollment();
		mEnrollment.setGrossPremiumAmt(5000.00f);
		mEnrollment.setAptcAmt(1000.00f);
		mEnrollment.setCsrAmt(2000.00f);
		mEnrollment.setNetPremiumAmt(4000.00f);
		mEnrollment.setEmployeeContribution(4000.00f);
		mEnrollment.setEmployerContribution(1000.00f);
		mEnrollment.setGroupPolicyNumber(POLICY_NUMBER);
		if(mEnrollment!=null){
		mEnrollment = iEnrollmentRepository.save(mEnrollment);
		}
		else
		{
			System.out.println("test");
		}
		assertTrue("Failed To create test Enrollment" , mEnrollment != null);
	}

	@After
	public void deleteTestRecords(){
		iEnrollmentRepository.delete(mEnrollment.getId());
		// To delete records created only by testSaveEnrollment().
		if(testSaveEnrollment){
			testSaveEnrollment = false;
			iEnrollmentRepository.delete(mEnrollment_1.getId());
		}
	}

	
	
	
	@Test
	public void testFindEnrollmentsByIssuerThroughRestURL() {
		 XStream xstream = GhixUtils.getXStreamGenericObject();
		 GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		 EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		 enrollmentRequest.setEnrollmentId(3664);
		 enrollmentRequest.setIssuerName("Presbyterian Health Plan, Inc");
		 enrollmentRequest.setPlanLevel("SILVER");
		 enrollmentRequest.setPlanMarket(EnrollmentRequest.MarketType.SHOP);
		 enrollmentRequest.setRatingRegion("4");
		 String postResp = null;
		 try{
			 postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_ISSUER_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrollmentRequest)).getBody();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 System.out.println("PostResponse      "+postResp);
		 assertTrue("enrollment retrieved succcessfully...!" , postResp != null);
	
		 
	}
	
	@Test       
	public void testDisEnrollByEnrollmentThroughRestCall() {
		
		 XStream xstream = GhixUtils.getXStreamGenericObject();
		 EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		 Map<String,Object> disEnrollmentMap = new HashMap<String,Object>();
		 GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		 enrollmentRequest.setMapValue(disEnrollmentMap);
		 String postResp = null;
		 try{
			 postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.DISENROLL_BY_ENROLLMENT_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrollmentRequest)).getBody();
			 }catch(Exception e){
			 e.printStackTrace();
		 }
		 assertTrue("dis-enrolled succcessfully...!" , postResp != null);
		 
	}
	
	@Test
	public void testFindById() {
		int enrollmentId = mEnrollment.getId();
		Enrollment enrollment = enrollmentService.findById(enrollmentId);

		assertTrue("No Enrollee found with id: " + enrollmentId, enrollment != null);
		int enId=enrollment.getId();
		assertEquals("findById fails",enId, enrollmentId);
	}
	
	@Test
	public void testFindEnrollmentsByPlan() {
		
		XStream xstream = GhixUtils.getXStreamGenericObject();
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentResponse enrollmentResponse = null;
		GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		enrollmentRequest.setPlanId(8);
		enrollmentRequest.setPlanMarket(MarketType.INDIVIDUAL);
		String postResp = null;
		 try{
			 postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_PLAN_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrollmentRequest)).getBody();
			 enrollmentResponse = (EnrollmentResponse) xstream.fromXML(postResp);
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		assertTrue("No of Enrollments found for a plan: " + enrollmentResponse, enrollmentResponse != null);

	}
	
		/**
	 * Test method for {@link com.getinsured.hix.enrollment.service.EnrollmentServiceImpl#saveEnrollment(com.getinsured.hix.model.Enrollment)}.
	 */
	@Test
	public void testSaveEnrollment() {
		testSaveEnrollment = true;

		// To Test Save Method in Enrollment Service
		mEnrollment_1 = new Enrollment();
		mEnrollment_1.setGrossPremiumAmt(1000.00f);
		mEnrollment_1.setAptcAmt(1000.00f);
		mEnrollment_1.setCsrAmt(2000.00f);
		mEnrollment_1.setNetPremiumAmt(6000.00f);
		mEnrollment_1.setEmployeeContribution(5000.00f);
		mEnrollment_1.setEmployerContribution(1000.00f);
		mEnrollment_1.setGroupPolicyNumber(POLICY_NUMBER);
		Enrollment savedEnrollment = enrollmentService.saveEnrollment(mEnrollment_1);
		mEnrollment_1 = savedEnrollment;
		assertTrue("Saving Enrollment failed" , savedEnrollment != null);
		Enrollment retrievedEnrollment = iEnrollmentRepository.findOne(savedEnrollment.getId());
		assertTrue("No Enrollments with ID"  + savedEnrollment.getId(), retrievedEnrollment != null);
	}
	
		@Test
	public void testsearchEnrollment() {
		
		Map<String,Object> searchCriteriaMap = new HashMap<String,Object>();
		searchCriteriaMap.put("policynumber", POLICY_NUMBER); 
		searchCriteriaMap.put("status", null); //null
		searchCriteriaMap.put("plantype", null);//null
		searchCriteriaMap.put("issuer", null);//null
		searchCriteriaMap.put("plannumber", null);//null
		searchCriteriaMap.put("lookup", null);
		searchCriteriaMap.put("lookup", SHOP);
		searchCriteriaMap.put("lookup", INDIVIDUAL);
		searchCriteriaMap.put("sortBy", null);
		
		Map<String,Object> enrollmentMap = enrollmentService.searchEnrollment(searchCriteriaMap);
		assertTrue("No of enrollments found for the input search criteria : " + enrollmentMap.size(), enrollmentMap != null);

	}
	
	/**author raja
	 * Test method for {@link com.getinsured.hix.enrollment.service.EnrollmentServiceImpl#getCarrierUpdatedEnrollments()}.
	 */
	
	@Test
	public void testGetCarrierUpdatedEnrollments() {
		
		List<Enrollment> enrollmentList = enrollmentService.getCarrierUpdatedEnrollments();
		assertTrue("No of enrollments which are updated since last carrier update : " + enrollmentList.size(), enrollmentList != null);

	}

	@Test
	public void testUpdateShopEnrollmentStatus() {
		GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		 XStream xstream = GhixUtils.getXStreamGenericObject();
		 EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		 enrollmentRequest.setLoggedInUserName(GhixConstants.USER_NAME_EXCHANGE);
		 enrollmentRequest.setEmployerId(10);
		 enrollmentRequest.setEnrollmentStatus(EnrollmentStatus.PAYMENT_RECEIVED);
		 String postResp = null;
		 try{
			 postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_SHOP_ENROLLMENT_STATUS_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrollmentRequest)).getBody();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 assertTrue("update shop enrollment succcessfully...!" , postResp != null);
		 
	}
	
	/**
	 * Test method for {@link com.getinsured.hix.enrollment.controller.EnrollmentController#getPlanIdAndOrderItemIdByEnrollmentId(com.getinsured.hix.dto.enrollment.EnrollmentRequest)}.
	 */

	@Test
	public void testGetEnrollmentById() {
		GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		 XStream xstream = GhixUtils.getXStreamGenericObject();
		 EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		 enrollmentRequest.setEnrollmentId(10);
		 String postResp = null;
		 try{
			  postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_PLANID_AND_ORDERITEMID_BY_ENROLLMENT_ID_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrollmentRequest)).getBody();
     		}catch(Exception e){
			 e.printStackTrace();
		 }
		 assertTrue("enrollment retrieved succcessfully...!" , postResp != null);
		 
	}
	/**
	 * Test method for {@link com.getinsured.hix.enrollment.controller.EnrollmentController#findEnrollmentsByPlan(com.getinsured.hix.dto.enrollment.EnrollmentRequest)}.
	 */

	@Test
	public void testFindEnrollmentsByPlanThroughRestURL() {
		GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		 XStream xstream = GhixUtils.getXStreamGenericObject();
		 EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		 enrollmentRequest.setPlanName(null);
		 enrollmentRequest.setPlanLevel(null);
		 enrollmentRequest.setPlanMarket(null);
		 enrollmentRequest.setRatingRegion(null);
		 enrollmentRequest.setStartDate(null);
		 enrollmentRequest.setEndDate(null);
		 String postResp = null;
		 try{
			 postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_PLAN_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrollmentRequest)).getBody();
	
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 assertTrue("enrollment retrieved succcessfully...!" , postResp != null);
		 
	}

	@Test
	public void testFindEmployeeIdAndPlanIdByEmployerid() {
		XStream xstream = GhixUtils.getXStreamGenericObject();
		GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		enrollmentRequest.setEmployerId(82);
		String postResp = null;
		 try{
			 postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_EMPLOYEEID_AND_PLANID_BY_EMPLYERID, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrollmentRequest)).getBody();
			 enrollmentResponse = (EnrollmentResponse) xstream.fromXML(postResp);
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 assertTrue("employee id and plan id retrieved succcessfully...!" , enrollmentResponse != null);
	}

	@Test
	public void testFindEnrollmentByEmployeeIdAndEnrollmentId(){
		XStream xstream = GhixUtils.getXStreamGenericObject();
		GhixRestTemplate ghixRestTemplate=new GhixRestTemplate();
			EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		//	enrolleeRequest.setEnrollmentId("41");
			enrolleeRequest.setEmployeeId(61);
			EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
			String postResp = null;
			 try{
				 postResp= ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTID_BY_ENROLLMENTID_AND_ENPLOYEEID, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrolleeRequest)).getBody();
				}catch(Exception e){
				 e.printStackTrace();
			 }
			 assertTrue("enrollment id retrieved succcessfully...!" , postResp != null);
	}	
}
