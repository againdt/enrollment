package com.getinsured.hix.enrollment.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath:applicationEnrollment.xml"})
public class EnrollmentTestConfiguration {
}
