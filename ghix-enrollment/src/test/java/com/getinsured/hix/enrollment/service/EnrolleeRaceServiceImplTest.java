package com.getinsured.hix.enrollment.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.enrollment.BaseTest;
import com.getinsured.hix.enrollment.repository.IEnrolleeRaceRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRace;

public class EnrolleeRaceServiceImplTest extends BaseTest{
	
	@Autowired
	private IEnrolleeRaceRepository iEnrolleeRaceRepository;
	
	@Autowired
	private EnrolleeRaceService enrolleeRaceService;
	
	@Autowired 
	private  IEnrolleeRepository iEnrolleeRepository;

	private Enrollee enrollee_1 = null;
	private EnrolleeRace mEnrolleeRace = null;
	
	private static final String ENROLLEE_FIRST_NAME = "Albert";
	
	@Before
	public void createTestRecords(){
		enrollee_1 = new Enrollee();
		enrollee_1.setFirstName(ENROLLEE_FIRST_NAME);
		
		mEnrolleeRace = new EnrolleeRace();
		mEnrolleeRace.setEnrollee(enrollee_1);
		
		ArrayList<EnrolleeRace> enrolleeRaceList = new ArrayList<EnrolleeRace>();
		enrolleeRaceList.add(mEnrolleeRace);
		enrollee_1.setEnrolleeRace(enrolleeRaceList);
		
		mEnrolleeRace = iEnrolleeRaceRepository.save(mEnrolleeRace);
		enrollee_1 = iEnrolleeRepository.save(enrollee_1);
	}
	
	@After
	public void deleteTestRecords(){
		
	}
	
	
	/**
	 * Test method for {@link com.getinsured.hix.enrollment.service.EnrolleeRaceServiceImpl#deleteEnrolleeRace(com.getinsured.hix.model.Enrollee)}.
	 */
	@Test
	public void testDeleteEnrolleeRace() {
		enrolleeRaceService.deleteEnrolleeRace(enrollee_1.getEnrolleeRace());
		EnrolleeRace testEnrolleeRace = iEnrolleeRaceRepository.findOne(mEnrolleeRace.getId());
		assertTrue("Delete Enrollee Race failed", testEnrolleeRace == null);
	}

}
