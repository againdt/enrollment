package com.getinsured.hix.enrollment.audit;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.enrollment.BaseTest;
import com.getinsured.hix.enrollment.service.EnrolleeAuditService;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.platform.util.DateUtil;

public class EnrolleeAuditTest extends BaseTest {
	
	@Autowired	private EnrolleeAuditService enrolleeAuditService;
	
	@Test
	public void test() throws ParseException {
		
		 Date effectiveDate=null;
		 String coverageStartDate="08-JAN-14 03.18.48.703 PM";
		 String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,"dd-MMM-yy hh.mm.ss.SSS a","dd-MMM-yy hh.mm.ss.SSS a");
	     effectiveDate =  DateUtil.StringToDate(effectiveDateStr, "dd-MMM-yy hh.mm.ss.SSS a"); 
	     Enrollee enrolleeObj =  enrolleeAuditService.findRevisionByDate(41, effectiveDate).getEntity();
		 assertEquals(enrolleeObj.getFirstName(), "Jyoti");
	}
}
