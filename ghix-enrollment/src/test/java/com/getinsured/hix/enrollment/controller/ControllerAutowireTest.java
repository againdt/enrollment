package com.getinsured.hix.enrollment.controller;

import com.getinsured.hix.enrollment.EnrollmentIntegrationTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

public class ControllerAutowireTest extends EnrollmentIntegrationTest {

    @Autowired
    private WebApplicationContext wac;

//    private MockMvc mockMvc;
//
//
//    @Before
//    public void setup() throws Exception {
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
//    }

    @Test
    public void givenWac_whenServletContext_thenItProvidesControllers() {
        ServletContext servletContext = wac.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("enrolleeController"));
        Assert.assertNotNull(wac.getBean("enrolleePrivateController"));
        Assert.assertNotNull(wac.getBean("enrollmentAnonymousController"));
        Assert.assertNotNull(wac.getBean("enrollmentController"));
        Assert.assertNotNull(wac.getBean("enrollmentFfmProxyController"));
        Assert.assertNotNull(wac.getBean("enrollmentOffExchangeController"));
        Assert.assertNotNull(wac.getBean("enrollmentPremiumController"));
        Assert.assertNotNull(wac.getBean("enrollmentPrivateController"));
        Assert.assertNotNull(wac.getBean("enrollmentReconController"));
        Assert.assertNotNull(wac.getBean("enrollmentReportController"));
        Assert.assertNotNull(wac.getBean("enrollmentTenantController"));
    }
}
