/**
 * 
 */
package com.getinsured.hix.enrollment.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nakka_s
 *
 */
public class EnrollmentUpdateDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer enrollmentId;
	private Date benefitEffectiveDate;
	private String verificationEvent;
	private Integer tenantId;
	private String lastUpdatedByUserEmail;
	
	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	/**
	 * @return the benefitEffectiveDate
	 */
	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	/**
	 * @param benefitEffectiveDate the benefitEffectiveDate to set
	 */
	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	/**
	 * @return the verificationEvent
	 */
	public String getVerificationEvent() {
		return verificationEvent;
	}
	/**
	 * @param verificationEvent the verificationEvent to set
	 */
	public void setVerificationEvent(String verificationEvent) {
		this.verificationEvent = verificationEvent;
	}
	/**
	 * @return the tenantId
	 */
	public Integer getTenantId() {
		return tenantId;
	}
	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}
	/**
	 * @return the lastUpdatedByUserEmail
	 */
	public String getLastUpdatedByUserEmail() {
		return lastUpdatedByUserEmail;
	}
	/**
	 * @param lastUpdatedByUserEmail the lastUpdatedByUserEmail to set
	 */
	public void setLastUpdatedByUserEmail(String lastUpdatedByUserEmail) {
		this.lastUpdatedByUserEmail = lastUpdatedByUserEmail;
	}
	
	
}
