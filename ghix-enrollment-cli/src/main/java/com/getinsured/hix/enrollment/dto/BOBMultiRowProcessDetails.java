package com.getinsured.hix.enrollment.dto;

import java.util.Date;
import java.util.List;

public class BOBMultiRowProcessDetails {

	private String latestStatus;
	private Integer enrollmentId;
	private Date orgEffectiveDate;
	private String matchType;
	private List<BOBMultipleRowRenewalModel> renewalRecords;
	private EnrollmentBobDTO enrollmentBobDTO;

	public String getLatestStatus() {
		return latestStatus;
	}
	public void setLatestStatus(String latestStatus) {
		this.latestStatus = latestStatus;
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public List<BOBMultipleRowRenewalModel> getRenewalRecords() {
		return renewalRecords;
	}
	public void setRenewalRecords(List<BOBMultipleRowRenewalModel> renewalRecords) {
		this.renewalRecords = renewalRecords;
	}
	public String getMatchType() {
		return matchType;
	}
	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}
	public Date getOrgEffectiveDate() {
		return orgEffectiveDate;
	}
	public void setOrgEffectiveDate(Date orgEffectiveDate) {
		this.orgEffectiveDate = orgEffectiveDate;
	}
	public EnrollmentBobDTO getEnrollmentBobDTO() {
		return enrollmentBobDTO;
	}
	public void setEnrollmentBobDTO(EnrollmentBobDTO enrollmentBobDTO) {
		this.enrollmentBobDTO = enrollmentBobDTO;
	}
	
	
}
