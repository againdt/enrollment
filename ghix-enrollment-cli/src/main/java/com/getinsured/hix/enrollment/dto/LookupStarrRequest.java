package com.getinsured.hix.enrollment.dto;

import java.util.List;

public class LookupStarrRequest {
	
	private String lookupTypeName;
	private List<String> lookupValueCodes;
	
	
	public String getLookupTypeName() {
		return lookupTypeName;
	}
	public void setLookupTypeName(String lookupTypeName) {
		this.lookupTypeName = lookupTypeName;
	}
	public List<String> getLookupValueCodes() {
		return lookupValueCodes;
	}
	public void setLookupValueCodes(List<String> lookupValueCodes) {
		this.lookupValueCodes = lookupValueCodes;
	}

	
}
