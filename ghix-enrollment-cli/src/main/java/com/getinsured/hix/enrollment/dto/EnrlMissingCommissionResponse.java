package com.getinsured.hix.enrollment.dto;

import java.util.List;

public class EnrlMissingCommissionResponse {

	private STATUS status;
	private String errMsg;
	private List<EnrlMissingCommissionDTO> missingCommissionDTOList;
	private Long recordCount;
	
	/**
	 * @return the recordCount
	 */
	public Long getRecordCount() {
		return recordCount;
	}

	/**
	 * @param recordCount the recordCount to set
	 */
	public void setRecordCount(Long recordCount) {
		this.recordCount = recordCount;
	}
	
	public static enum STATUS{
		SUCCESS, FAILURE
	}
	/**
	 * @return the status
	 */
	public STATUS getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(STATUS status) {
		this.status = status;
	}

	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * @return the missingCommissionDTOList
	 */
	public List<EnrlMissingCommissionDTO> getMissingCommissionDTOList() {
		return missingCommissionDTOList;
	}

	/**
	 * @param missingCommissionDTOList the missingCommissionDTOList to set
	 */
	public void setMissingCommissionDTOList(
			List<EnrlMissingCommissionDTO> missingCommissionDTOList) {
		this.missingCommissionDTOList = missingCommissionDTOList;
	}
	
	
}
