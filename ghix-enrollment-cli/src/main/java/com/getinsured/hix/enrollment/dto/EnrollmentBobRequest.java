package com.getinsured.hix.enrollment.dto;

import java.util.Date;
import java.util.List;


public class EnrollmentBobRequest 
{
	public enum EnrollmentStatus{
		ABORTED, APPROVED, CANCEL, CONFIRM, DECLINED, DOES_NOT_EXIST, DUPLICATE, ECOMMITTED,
		ESIG_PENDING, INFORCE, INVALID, NOT_APPOINTED, ORDER_CONFIRMED, PAYMENT_FAILED,
		PAYMENT_PROCESSED, PAYMENT_RECEIVED, PENDING, SOLD, SUSPENDED, TERM, VERIFIED, WITHDRAWN;
	}
	
	private Integer enrollmentId;
	private String carrierName;
	private Date benefitEffectiveDate;
	private String benefitEffDateMatchingRangeFrom;
	private String benefitEffDateMatchingRangeTo;
	private Date benefitEffectiveEndDate;
	private String policyId;
	private String productType;
	private String hicn;
	private String carrierAppId;
	private String carrierAppUid;
	private String firstName;
	private String lastName;
	private String state;
	//private Integer tenantId;
	private String tenantCode;
	private EnrollmentStatus enrollmentStatus;
	private Date dateClosed;
	private String lastUpdatedByUserEmail;
	private Integer issuerId;
	private List<Integer> enrollmentIdsList;
	private List<EnrollmentStatus> enrollmentStatusList;
	private Integer duplicateEnrollmentId;
	private List<EnrollmentBobDTO> enrollmentBobDtoList;
	private String renewalFlag;
	private List<BOBMultipleRowRenewalModel> bobMultipleRowRenewalModels;
	private List<EnrollmentRenewalDTO> enrollmentRenewalDTOs;
	
	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	
	/**
	 * @return the carrierName
	 */
	public String getCarrierName() {
		return carrierName;
	}
	/**
	 * @param carrierName the carrierName to set
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	/**
	 * @return the benefitEffectiveDate
	 */
	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	/**
	 * @param benefitEffectiveDate the benefitEffectiveDate to set
	 */
	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	/**
	 * @return the benefitEffectiveEndDate
	 */
	public Date getBenefitEffectiveEndDate() {
		return benefitEffectiveEndDate;
	}
	/**
	 * @param benefitEffectiveEndDate the benefitEffectiveEndDate to set
	 */
	public void setBenefitEffectiveEndDate(Date benefitEffectiveEndDate) {
		this.benefitEffectiveEndDate = benefitEffectiveEndDate;
	}
	/**
	 * @return the policyId
	 */
	public String getPolicyId() {
		return policyId;
	}
	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	/**
	 * @return the hicn
	 */
	public String getHicn() {
		return hicn;
	}
	/**
	 * @param hicn the hicn to set
	 */
	public void setHicn(String hicn) {
		this.hicn = hicn;
	}
	/**
	 * @return the carrierAppId
	 */
	public String getCarrierAppId() {
		return carrierAppId;
	}
	/**
	 * @param carrierAppId the carrierAppId to set
	 */
	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}
	/**
	 * @return the carrierAppUid
	 */
	public String getCarrierAppUid() {
		return carrierAppUid;
	}
	/**
	 * @param carrierAppUid the carrierAppUid to set
	 */
	public void setCarrierAppUid(String carrierAppUid) {
		this.carrierAppUid = carrierAppUid;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the tenantCode
	 */
	public String getTenantCode() {
		return tenantCode;
	}
	/**
	 * @param tenantCode the tenantCode to set
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	/**
	 * @return the enrollmentStatus
	 */
	public EnrollmentStatus getEnrollmentStatus() {
		return enrollmentStatus;
	}
	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(EnrollmentStatus enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	/**
	 * @return the dateClosed
	 */
	public Date getDateClosed() {
		return dateClosed;
	}
	/**
	 * @param dateClosed the dateClosed to set
	 */
	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}
	/**
	 * @return the lastUpdatedByUserEmail
	 */
	public String getLastUpdatedByUserEmail() {
		return lastUpdatedByUserEmail;
	}
	/**
	 * @param lastUpdatedByUserEmail the lastUpdatedByUserEmail to set
	 */
	public void setLastUpdatedByUserEmail(String lastUpdatedByUserEmail) {
		this.lastUpdatedByUserEmail = lastUpdatedByUserEmail;
	}
	/**
	 * @return the issuerId
	 */
	public Integer getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the enrollmentIdsList
	 */
	public List<Integer> getEnrollmentIdsList() {
		return enrollmentIdsList;
	}
	/**
	 * @param enrollmentIdsList the enrollmentIdsList to set
	 */
	public void setEnrollmentIdsList(List<Integer> enrollmentIdsList) {
		this.enrollmentIdsList = enrollmentIdsList;
	}
	/**
	 * @return the enrollmentStatusList
	 */
	public List<EnrollmentStatus> getEnrollmentStatusList() {
		return enrollmentStatusList;
	}
	/**
	 * @param enrollmentStatusList the enrollmentStatusList to set
	 */
	public void setEnrollmentStatusList(List<EnrollmentStatus> enrollmentStatusList) {
		this.enrollmentStatusList = enrollmentStatusList;
	}
	/**
	 * @return the duplicateEnrollmentId
	 */
	public Integer getDuplicateEnrollmentId() {
		return duplicateEnrollmentId;
	}
	/**
	 * @param duplicateEnrollmentId the duplicateEnrollmentId to set
	 */
	public void setDuplicateEnrollmentId(Integer duplicateEnrollmentId) {
		this.duplicateEnrollmentId = duplicateEnrollmentId;
	}
	/**
	 * @return the enrollmentBobDtoList
	 */
	public List<EnrollmentBobDTO> getEnrollmentBobDtoList() {
		return enrollmentBobDtoList;
	}
	/**
	 * @param enrollmentBobDtoList the enrollmentBobDtoList to set
	 */
	public void setEnrollmentBobDtoList(List<EnrollmentBobDTO> enrollmentBobDtoList) {
		this.enrollmentBobDtoList = enrollmentBobDtoList;
	}
	
	public String getRenewalFlag() {
		return renewalFlag;
	}
	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}
	
	/**
	 * @return the benefitEffDateMatchingRangeFrom
	 */
	public String getBenefitEffDateMatchingRangeFrom() {
		return benefitEffDateMatchingRangeFrom;
	}
	/**
	 * @param benefitEffDateMatchingRangeFrom the benefitEffDateMatchingRangeFrom to set
	 */
	public void setBenefitEffDateMatchingRangeFrom(String benefitEffDateMatchingRangeFrom) {
		this.benefitEffDateMatchingRangeFrom = benefitEffDateMatchingRangeFrom;
	}
	/**
	 * @return the benefitEffDateMatchingRangeTo
	 */
	public String getBenefitEffDateMatchingRangeTo() {
		return benefitEffDateMatchingRangeTo;
	}
	/**
	 * @param benefitEffDateMatchingRangeTo the benefitEffDateMatchingRangeTo to set
	 */
	public void setBenefitEffDateMatchingRangeTo(String benefitEffDateMatchingRangeTo) {
		this.benefitEffDateMatchingRangeTo = benefitEffDateMatchingRangeTo;
	}
	public List<BOBMultipleRowRenewalModel> getBobMultipleRowRenewalModels() {
		return bobMultipleRowRenewalModels;
	}
	public void setBobMultipleRowRenewalModels(List<BOBMultipleRowRenewalModel> bobMultipleRowRenewalModels) {
		this.bobMultipleRowRenewalModels = bobMultipleRowRenewalModels;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public List<EnrollmentRenewalDTO> getEnrollmentRenewalDTOs() {
		return enrollmentRenewalDTOs;
	}
	public void setEnrollmentRenewalDTOs(List<EnrollmentRenewalDTO> enrollmentRenewalDTOs) {
		this.enrollmentRenewalDTOs = enrollmentRenewalDTOs;
	}
	
	
	
}
