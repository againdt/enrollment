package com.getinsured.hix.enrollment.dto;

import java.util.Date;

/**
 * Jira Id: HIX-79710 API for Enrollment Commission
 * EnrollmentCommissionRequest 
 * @since 11th January 2016
 * @version 1.0
 *
 */
public class EnrollmentCommissionRequest {
	
	private String carrier;
	private String state;
	private Integer enrollmentId;
	private String commissionAmt;
	private Date commissionDate;
	private String createdOn;
	private String updatedOn;
	private Date dueMonthYear;
	private String amtPaidToDate;
	private String commissionableAmount;
	private String mode;
	private String issuerId;
	private String commissionType;
	private Long carrierFeedDetailsId;
	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}
	/**
	 * @param carrier the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the enrollmentId
	 */
	
	/**
	 * @return the commissionAmt
	 */
	public String getCommissionAmt() {
		return commissionAmt;
	}
	/**
	 * @param commissionAmt the commissionAmt to set
	 */
	public void setCommissionAmt(String commissionAmt) {
		this.commissionAmt = commissionAmt;
	}
	/**
	 * @return the commissionDate
	 */
	
	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedOn
	 */
	public String getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the dueMonthYear
	 */
	public Date getDueMonthYear() {
		return dueMonthYear;
	}
	/**
	 * @param dueMonthYear the dueMonthYear to set
	 */
	public void setDueMonthYear(Date dueMonthYear) {
		this.dueMonthYear = dueMonthYear;
	}
	/**
	 * @return the amtPaidToDate
	 */
	public String getAmtPaidToDate() {
		return amtPaidToDate;
	}
	/**
	 * @param amtPaidToDate the amtPaidToDate to set
	 */
	public void setAmtPaidToDate(String amtPaidToDate) {
		this.amtPaidToDate = amtPaidToDate;
	}
	/**
	 * @return the commissionableAmount
	 */
	public String getCommissionableAmount() {
		return commissionableAmount;
	}
	/**
	 * @param commissionableAmount the commissionableAmount to set
	 */
	public void setCommissionableAmount(String commissionableAmount) {
		this.commissionableAmount = commissionableAmount;
	}
	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}
	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}
	/**
	 * @return the issuerId
	 */
	public String getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	/**
	 * @return the commissionDate
	 */
	public Date getCommissionDate() {
		return commissionDate;
	}
	/**
	 * @param commissionDate the commissionDate to set
	 */
	public void setCommissionDate(Date commissionDate) {
		this.commissionDate = commissionDate;
	}
	/**
	 * @return the commissionType
	 */
	public String getCommissionType() {
		return commissionType;
	}
	/**
	 * @param commissionType the commissionType to set
	 */
	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}
	/**
	 * @return the carrierFeedDetailsId
	 */
	public Long getCarrierFeedDetailsId() {
		return carrierFeedDetailsId;
	}
	/**
	 * @param carrierFeedDetailsId the carrierFeedDetailsId to set
	 */
	public void setCarrierFeedDetailsId(Long carrierFeedDetailsId) {
		this.carrierFeedDetailsId = carrierFeedDetailsId;
	}
}
