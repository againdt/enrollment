package com.getinsured.hix.enrollment.dto;

import java.util.Date;


public class BOBMultipleRowRenewalModel implements Comparable<BOBMultipleRowRenewalModel> {

	private Date benefitStartDate;
	private Date benefitEndDate;
	private float premium;
	private String enrollmentStatus;
	private Date termDate;
	private String feedRecordJson;
	private String policyId;
	private Long carrierFeedDetailsId;
	private String productType;
	private String firstName;
	private String lastName;
	private String state;
	private String carrierName;
	private String hicn;
	private String tenantCode;
	
	public BOBMultipleRowRenewalModel() {
	}
	
	public Date getBenefitStartDate() {
		return benefitStartDate;
	}

	public void setBenefitStartDate(Date benefitStartDate) {
		this.benefitStartDate = benefitStartDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}

	public float getPremium() {
		return premium;
	}

	public void setPremium(float premium) {
		this.premium = premium;
	}
	
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	
	public Date getTermDate() {
		return termDate;
	}

	public void setTermDate(Date termDate) {
		this.termDate = termDate;
	}
	
	/**
	 * @return the feedRecordJson
	 */
	public String getFeedRecordJson() {
		return feedRecordJson;
	}

	/**
	 * @param feedRecordJson the feedRecordJson to set
	 */
	public void setFeedRecordJson(String feedRecordJson) {
		this.feedRecordJson = feedRecordJson;
	}

	/**
	 * @return the policyId
	 */
	public String getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	
	/**
	 * @return the carrierFeedDetailsId
	 */
	public Long getCarrierFeedDetailsId() {
		return carrierFeedDetailsId;
	}

	/**
	 * @param carrierFeedDetailsId the carrierFeedDetailsId to set
	 */
	public void setCarrierFeedDetailsId(Long carrierFeedDetailsId) {
		this.carrierFeedDetailsId = carrierFeedDetailsId;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the carrierName
	 */
	public String getCarrierName() {
		return carrierName;
	}

	/**
	 * @param carrierName the carrierName to set
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	/**
	 * @return the tenantCode
	 */
	public String getTenantCode() {
		return tenantCode;
	}

	/**
	 * @param tenantCode the tenantCode to set
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	
	/**
	 * @return the hicn
	 */
	public String getHicn() {
		return hicn;
	}

	/**
	 * @param hicn the hicn to set
	 */
	public void setHicn(String hicn) {
		this.hicn = hicn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((benefitEndDate == null) ? 0 : benefitEndDate.hashCode());
		result = prime
				* result
				+ ((benefitStartDate == null) ? 0 : benefitStartDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BOBMultipleRowRenewalModel other = (BOBMultipleRowRenewalModel) obj;
		if (benefitEndDate == null) {
			if (other.benefitEndDate != null)
				return false;
		} else if (!benefitEndDate.equals(other.benefitEndDate))
			return false;
		if (benefitStartDate == null) {
			if (other.benefitStartDate != null)
				return false;
		} else if (!benefitStartDate.equals(other.benefitStartDate))
			return false;
		return true;
	}

	@Override
	public int compareTo(BOBMultipleRowRenewalModel o) {
		if (o != null && this.benefitStartDate != null)
		{
			return this.benefitStartDate.compareTo(o.getBenefitStartDate());
		}
		return 0;
	}

	@Override
	public String toString() {
		return "BOBMultipleRowModel [benefitStartDate=" + benefitStartDate
				+ ", benefitEndDate=" + benefitEndDate + ", premium=" + premium
				+ "]";
	}
	
	
}
