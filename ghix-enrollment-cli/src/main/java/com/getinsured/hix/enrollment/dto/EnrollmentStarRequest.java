package com.getinsured.hix.enrollment.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.getinsured.hix.enrollment.dto.EnrollmentBobRequest.EnrollmentStatus;

public class EnrollmentStarRequest {
	
	private List<Integer> enrollmentIdList;
	private String policyNumber;
	private String hicn;
	private String carrierUID;
	private String appID;
	private EnrollmentCommissionRequest enrollmentCommissionRequest;
	public enum RenewalType{SINGLE_LINE, MULTIPLE_LINE, ROLL_OVER}
	private Integer enrollmentId;
	/*private List<Date> benefitEffectiveDateList;*/
	public enum EnrollmentStatus{
		ABORTED, APPROVED, CANCEL, CONFIRM, DECLINED, DOES_NOT_EXIST, DUPLICATE, ECOMMITTED,
		ESIG_PENDING, INFORCE, INVALID, NOT_APPOINTED, ORDER_CONFIRMED, PAYMENT_FAILED,
		PAYMENT_PROCESSED, PAYMENT_RECEIVED, PENDING, SOLD, SUSPENDED, TERM, VERIFIED, WITHDRAWN;
	}
	private RenewalType renewalType;
	private EnrollmentStatus enrollmentStatus;
	private String lastUpdatedByUserEmail;
	private String renewalFlag;
	private List<EnrollmentRenewalDTO> enrollmentRenewalDTOList;
	private List<EnrollmentStatus> enrollmentStatusList;
	private List<Integer> enrollmentStatusIds;
	Set<Integer> issuerIds = new HashSet<Integer>();
	private String carrierName;
	/**
	 * @return the enrollmentIdList
	 */
	public List<Integer> getEnrollmentIdList() {
		return enrollmentIdList;
	}

	/**
	 * @param enrollmentIdList the enrollmentIdList to set
	 */
	public void setEnrollmentIdList(List<Integer> enrollmentIdList) {
		this.enrollmentIdList = enrollmentIdList;
	}

	/**
	 * @return the policyNumber
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * @param policyNumber the policyNumber to set
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	/**
	 * @return the enrollmentCommissionRequest
	 */
	public EnrollmentCommissionRequest getEnrollmentCommissionRequest() {
		return enrollmentCommissionRequest;
	}

	/**
	 * @param enrollmentCommissionRequest the enrollmentCommissionRequest to set
	 */
	public void setEnrollmentCommissionRequest(EnrollmentCommissionRequest enrollmentCommissionRequest) {
		this.enrollmentCommissionRequest = enrollmentCommissionRequest;
	}

	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	/**
	 * @return the benefitEffectiveDateList
	 */
	/*public List<Date> getBenefitEffectiveDateList() {
		return benefitEffectiveDateList;
	}*/
	
	/**
	 * @param benefitEffectiveDateList the benefitEffectiveDateList to set
	 */
	/*public void setBenefitEffectiveDateList(List<Date> benefitEffectiveDateList) {
		this.benefitEffectiveDateList = benefitEffectiveDateList;
	}*/

	/**
	 * @return the renewalType
	 */
	public RenewalType getRenewalType() {
		return renewalType;
	}

	/**
	 * @param renewalType the renewalType to set
	 */
	public void setRenewalType(RenewalType renewalType) {
		this.renewalType = renewalType;
	}

	/**
	 * @return the enrollmentStatus
	 */
	public EnrollmentStatus getEnrollmentStatus() {
		return enrollmentStatus;
	}

	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(EnrollmentStatus enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	/**
	 * @return the lastUpdatedByUserEmail
	 */
	public String getLastUpdatedByUserEmail() {
		return lastUpdatedByUserEmail;
	}

	/**
	 * @param lastUpdatedByUserEmail the lastUpdatedByUserEmail to set
	 */
	public void setLastUpdatedByUserEmail(String lastUpdatedByUserEmail) {
		this.lastUpdatedByUserEmail = lastUpdatedByUserEmail;
	}

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}

	/**
	 * @return the enrollmentRenewalDTOList
	 */
	public List<EnrollmentRenewalDTO> getEnrollmentRenewalDTOList() {
		return enrollmentRenewalDTOList;
	}

	/**
	 * @param enrollmentRenewalDTOList the enrollmentRenewalDTOList to set
	 */
	public void setEnrollmentRenewalDTOList(List<EnrollmentRenewalDTO> enrollmentRenewalDTOList) {
		this.enrollmentRenewalDTOList = enrollmentRenewalDTOList;
	}

	/**
	 * @return the enrollmentStatusList
	 */
	public List<EnrollmentStatus> getEnrollmentStatusList() {
		return enrollmentStatusList;
	}

	/**
	 * @param enrollmentStatusList the enrollmentStatusList to set
	 */
	public void setEnrollmentStatusList(List<EnrollmentStatus> enrollmentStatusList) {
		this.enrollmentStatusList = enrollmentStatusList;
	}

	/**
	 * @return the hicn
	 */
	public String getHicn() {
		return hicn;
	}

	/**
	 * @param hicn the hicn to set
	 */
	public void setHicn(String hicn) {
		this.hicn = hicn;
	}

	public Set<Integer> getIssuerIds() {
		return issuerIds;
	}

	public void setIssuerIds(Set<Integer> issuerIds) {
		this.issuerIds = issuerIds;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public List<Integer> getEnrollmentStatusIds() {
		return enrollmentStatusIds;
	}

	public void setEnrollmentStatusIds(List<Integer> enrollmentStatusIds) {
		this.enrollmentStatusIds = enrollmentStatusIds;
	}

	public String getCarrierUID() {
		return carrierUID;
	}

	public void setCarrierUID(String carrierUID) {
		this.carrierUID = carrierUID;
	}

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}
	
	
	
		
}
