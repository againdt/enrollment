package com.getinsured.hix.enrollment.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * @since 21st November 2015
 * @author Sharma_K
 * Jira ID: HIX-79658
 *
 */
public class EnrollmentBobResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public enum MatchType {
		HICN("HICN"),POLICY_ID("POLICY_ID"), APP_ID("APP_ID"), UID("UID"), FUZZY_MATCH("FUZZY_MATCH");
	public String matchType;

	private MatchType(String matchType){
		setMatchType(matchType);
	}
	
	/**
	 * @return the matchType
	 */
	public String getMatchType() {
		return matchType;
	}

	/**
	 * @param matchType the matchType to set
	 */
	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}
		
	
	};
	
	private List<EnrollmentBobDTO> enrollmentBobDTOList;
	private MatchType matchType;
	
	private String appID;
	private String status;
	private String enrollmentId;
	private String enrollmentStatus;
	private String errMsg;
	private int errCode;
	private long execDuration;
	private long startTime;

	private String moduleStatusCode;
	private List<Integer> enrollmentIdList;
	private EnrollmentBobDTO matchedEnrollmentBobDTO;
	private List<EnrollmentRenewalDTO> enrollmentRenewalDTOs;
	
	
	public void startResponse() {
		startTime = Calendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = Calendar.getInstance().getTimeInMillis();
		execDuration = endTime - startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public long getExecDuration() {
		return execDuration;
	}

	public void setExecDuration(long execDuration) {
		this.execDuration = execDuration;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getModuleStatusCode() {
		return moduleStatusCode;
	}

	public void setModuleStatusCode(String moduleStatusCode) {
		this.moduleStatusCode = moduleStatusCode;
	}
	
	
	/**
	 * @return the enrollmentBobDTOList
	 */
	public List<EnrollmentBobDTO> getEnrollmentBobDTOList() {
		return enrollmentBobDTOList;
	}
	/**
	 * @param enrollmentBobDTOList the enrollmentBobDTOList to set
	 */
	public void setEnrollmentBobDTOList(List<EnrollmentBobDTO> enrollmentBobDTOList) {
		this.enrollmentBobDTOList = enrollmentBobDTOList;
	}
	/**
	 * @return the matchType
	 */
	public MatchType getMatchType() {
		return matchType;
	}
	/**
	 * @param matchType the matchType to set
	 */
	public void setMatchType(MatchType matchType) {
		this.matchType = matchType;
	}

	/**
	 * @return the enrollmentIdList
	 */
	public List<Integer> getEnrollmentIdList() {
		return enrollmentIdList;
	}

	/**
	 * @param enrollmentIdList the enrollmentIdList to set
	 */
	public void setEnrollmentIdList(List<Integer> enrollmentIdList) {
		this.enrollmentIdList = enrollmentIdList;
	}

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public EnrollmentBobDTO getMatchedEnrollmentBobDTO() {
		return matchedEnrollmentBobDTO;
	}

	public void setMatchedEnrollmentBobDTO(EnrollmentBobDTO matchedEnrollmentBobDTO) {
		this.matchedEnrollmentBobDTO = matchedEnrollmentBobDTO;
	}

	public List<EnrollmentRenewalDTO> getEnrollmentRenewalDTOs() {
		return enrollmentRenewalDTOs;
	}

	public void setEnrollmentRenewalDTOs(List<EnrollmentRenewalDTO> enrollmentRenewalDTOs) {
		this.enrollmentRenewalDTOs = enrollmentRenewalDTOs;
	}
	
	
	
	
}
