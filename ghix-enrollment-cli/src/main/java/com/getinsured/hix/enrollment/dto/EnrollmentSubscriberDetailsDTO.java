package com.getinsured.hix.enrollment.dto;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
	



public class EnrollmentSubscriberDetailsDTO {
	
	private Integer enrollmentId;
	private String carrierName;
	private String planName;
	private String insuranceTypeLkpCode;
	private String insuranceTypeLkpType;
	private String issuerAssignPolicyNo;
	private String carrierAppId;
	private Date benefitEffectiveDate;
	private String enrollmentStatus;
	private Float netPremiumAmount;
	private String subscriberName;
	private Date subscriberDob;
	private String subscriberPrimaryPhoneNo;
	private String subscriberHomeAddress1;
	private String subscriberHomeAddress2;
	private String subscriberHomeCity;
	private String subscriberHomeState;
	private String subscriberHomeZipCode;
	private String homeAddress;
	private String insuranceType;
	private String policyId;
	private String carrierAppUId;
	private Integer enrollmentStatusLkpID;
	private Integer InsuranceTypeLkpId;
	private Float grossPremiumAmt;
	private String groupPolicyNumber;
	private String renewalFlag;
	private Integer issuerId;
		
	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	/**
	 * @return the carrierName
	 */
	public String getCarrierName() {
		return carrierName;
	}
	/**
	 * @param carrierName the carrierName to set
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	/**
	 * @return the issuerAssignPolicyNo
	 */
	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}
	/**
	 * @param issuerAssignPolicyNo the issuerAssignPolicyNo to set
	 */
	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}
	/**
	 * @return the carrierAppId
	 */
	public String getCarrierAppId() {
		return carrierAppId;
	}
	/**
	 * @param carrierAppId the carrierAppId to set
	 */
	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}
	/**
	 * @return the benefitEffectiveDate
	 */
	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	/**
	 * @param benefitEffectiveDate the benefitEffectiveDate to set
	 */
	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	/**
	 * @return the enrollmentStatus
	 */
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	/**
	 * @return the netPremiumAmount
	 */
	public Float getNetPremiumAmount() {
		return netPremiumAmount;
	}
	/**
	 * @param netPremiumAmount the netPremiumAmount to set
	 */
	public void setNetPremiumAmount(Float netPremiumAmount) {
		this.netPremiumAmount = netPremiumAmount;
	}
	/**
	 * @return the subscriberName
	 */
	public String getSubscriberName() {
		return subscriberName;
	}
	/**
	 * @param subscriberName the subscriberName to set
	 */
	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}
	/**
	 * @return the subscriberPrimaryPhoneNo
	 */
	public String getSubscriberPrimaryPhoneNo() {
		return subscriberPrimaryPhoneNo;
	}
	/**
	 * @param subscriberPrimaryPhoneNo the subscriberPrimaryPhoneNo to set
	 */
	public void setSubscriberPrimaryPhoneNo(String subscriberPrimaryPhoneNo) {
		this.subscriberPrimaryPhoneNo = subscriberPrimaryPhoneNo;
	}
	/**
	 * @return the subscriberHomeAddress1
	 */
	public String getSubscriberHomeAddress1() {
		return subscriberHomeAddress1;
	}
	/**
	 * @param subscriberHomeAddress1 the subscriberHomeAddress1 to set
	 */
	public void setSubscriberHomeAddress1(String subscriberHomeAddress1) {
		this.subscriberHomeAddress1 = subscriberHomeAddress1;
	}
	/**
	 * @return the subscriberHomeAddress2
	 */
	public String getSubscriberHomeAddress2() {
		return subscriberHomeAddress2;
	}
	/**
	 * @param subscriberHomeAddress2 the subscriberHomeAddress2 to set
	 */
	public void setSubscriberHomeAddress2(String subscriberHomeAddress2) {
		this.subscriberHomeAddress2 = subscriberHomeAddress2;
	}
	/**
	 * @return the subscriberHomeCity
	 */
	public String getSubscriberHomeCity() {
		return subscriberHomeCity;
	}
	/**
	 * @param subscriberHomeCity the subscriberHomeCity to set
	 */
	public void setSubscriberHomeCity(String subscriberHomeCity) {
		this.subscriberHomeCity = subscriberHomeCity;
	}
	/**
	 * @return the subscriberHomeState
	 */
	public String getSubscriberHomeState() {
		return subscriberHomeState;
	}
	/**
	 * @param subscriberHomeState the subscriberHomeState to set
	 */
	public void setSubscriberHomeState(String subscriberHomeState) {
		this.subscriberHomeState = subscriberHomeState;
	}
	/**
	 * @return the subscriberHomeZipCode
	 */
	public String getSubscriberHomeZipCode() {
		return subscriberHomeZipCode;
	}
	/**
	 * @param subscriberHomeZipCode the subscriberHomeZipCode to set
	 */
	public void setSubscriberHomeZipCode(String subscriberHomeZipCode) {
		this.subscriberHomeZipCode = subscriberHomeZipCode;
	}
	
	
	/**
	 * @return the insuranceType
	 */
	public String getInsuranceType() {
		return insuranceType;
	}

	/**
	 * @param insuranceType the insuranceType to set
	 */
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	/**
	 * @return the policyId
	 */
	public String getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	/**
	 * @return the carrierAppUId
	 */
	public String getCarrierAppUId() {
		return carrierAppUId;
	}

	/**
	 * @param carrierAppUId the carrierAppUId to set
	 */
	public void setCarrierAppUId(String carrierAppUId) {
		this.carrierAppUId = carrierAppUId;
	}

	/**
	 * @return the enrollmentStatusLkpID
	 */
	public Integer getEnrollmentStatusLkpID() {
		return enrollmentStatusLkpID;
	}

	/**
	 * @param enrollmentStatusLkpID the enrollmentStatusLkpID to set
	 */
	public void setEnrollmentStatusLkpID(Integer enrollmentStatusLkpID) {
		this.enrollmentStatusLkpID = enrollmentStatusLkpID;
	}

	/**
	 * @return the insuranceTypeLkpId
	 */
	public Integer getInsuranceTypeLkpId() {
		return InsuranceTypeLkpId;
	}

	/**
	 * @param insuranceTypeLkpId the insuranceTypeLkpId to set
	 */
	public void setInsuranceTypeLkpId(Integer insuranceTypeLkpId) {
		InsuranceTypeLkpId = insuranceTypeLkpId;
	}

	/**
	 * @return the grossPremiumAmt
	 */
	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}

	/**
	 * @param grossPremiumAmt the grossPremiumAmt to set
	 */
	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}

	/**
	 * @return the groupPolicyNumber
	 */
	public String getGroupPolicyNumber() {
		return groupPolicyNumber;
	}

	/**
	 * @param groupPolicyNumber the groupPolicyNumber to set
	 */
	public void setGroupPolicyNumber(String groupPolicyNumber) {
		this.groupPolicyNumber = groupPolicyNumber;
	}

	/**
	 * @return the homeAddress
	 */
	public String getHomeAddress() {
		return homeAddress;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getInsuranceTypeLkpCode() {
		return insuranceTypeLkpCode;
	}

	public void setInsuranceTypeLkpCode(String insuranceTypeLkpCode) {
		this.insuranceTypeLkpCode = insuranceTypeLkpCode;
	}

	public String getInsuranceTypeLkpType() {
		return insuranceTypeLkpType;
	}

	public void setInsuranceTypeLkpType(String insuranceTypeLkpType) {
		this.insuranceTypeLkpType = insuranceTypeLkpType;
	}

	public Date getSubscriberDob() {
		return subscriberDob;
	}

	public void setSubscriberDob(Date subscriberDob) {
		this.subscriberDob = subscriberDob;
	}

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}

	/**
	 * @return the issuerId
	 */
	public Integer getIssuerId() {
		return issuerId;
	}

	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	/**
	 * @param homeAddress the homeAddress to set
	 */
	public void setHomeAddress(String homeAddress) {
		if(StringUtils.isEmpty(homeAddress)){
			StringBuilder stringBuilder = new StringBuilder(64);
			if(StringUtils.isNotEmpty(this.subscriberHomeAddress1)){
				stringBuilder.append(this.subscriberHomeAddress1);
				stringBuilder.append(",");
			}
			if(StringUtils.isNotEmpty(this.subscriberHomeAddress2)){
				stringBuilder.append(this.subscriberHomeAddress2);
				stringBuilder.append(",");
			}
			if(StringUtils.isNotEmpty(this.subscriberHomeCity)){
				stringBuilder.append(this.subscriberHomeCity);
				stringBuilder.append(",");
			}
			if(StringUtils.isNotEmpty(this.subscriberHomeState)){
				stringBuilder.append(this.subscriberHomeState);
				stringBuilder.append(",");
			}
			if(StringUtils.isNotEmpty(this.subscriberHomeZipCode)){
				stringBuilder.append(this.subscriberHomeZipCode);
			}
			this.homeAddress = stringBuilder.toString();
		}
		else{
			this.homeAddress = homeAddress;
		}
			
	}
}
