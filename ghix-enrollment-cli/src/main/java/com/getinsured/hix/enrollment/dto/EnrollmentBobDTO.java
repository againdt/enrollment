package com.getinsured.hix.enrollment.dto;

import java.util.Date;

/**
 * @since 21st November 2015
 * @author Sharma_K
 * Jira Id: HIX-79658
 *
 */
public class EnrollmentBobDTO 
{
	private Integer enrollmentId;
	private Date benefitEffectiveDate;
	private String insuranceType;
	private String issuerAssignPolicyNo;
	private String exchangeAssignPolicyNo;
	private String carrierAppId;
	private String carrierAppUID;
	private String enrollmentStatus;
	private String renewalFlag;
	private Integer issuerId;
	
	public EnrollmentBobDTO(){
		//Default Constructor
	}
	
	public EnrollmentBobDTO(Integer enrollmentId, Date benefitEffectiveDate, String insuranceType, String issuerAssignPolicyNo, String exchangeAssignPolicyNo,
			String carrierAppId, String carrierAppUID, String enrollmentStatus,String renewalFlag, Integer issuerId) {
		this.enrollmentId = enrollmentId;
		this.benefitEffectiveDate = benefitEffectiveDate;
		this.insuranceType = insuranceType;
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
		this.carrierAppId = carrierAppId;
		this.carrierAppUID = carrierAppUID;
		this.enrollmentStatus = enrollmentStatus;
		this.renewalFlag = renewalFlag;
		this.issuerId = issuerId;
	}
	
	public EnrollmentBobDTO(Integer enrollmentId, String issuerAssignPolicyNo, String carrierAppId){
		this.enrollmentId = enrollmentId;
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
		this.carrierAppId = carrierAppId;
	}
	
	public EnrollmentBobDTO(Integer enrollmentId){
		this.enrollmentId = enrollmentId;
	}
	
	
	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	/**
	 * @return the benefitEffectiveDate
	 */
	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	/**
	 * @param benefitEffectiveDate the benefitEffectiveDate to set
	 */
	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	/**
	 * @return the insuranceType
	 */
	public String getInsuranceType() {
		return insuranceType;
	}
	/**
	 * @param insuranceType the insuranceType to set
	 */
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	/**
	 * @return the issuerAssignPolicyNo
	 */
	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}

	/**
	 * @param issuerAssignPolicyNo the issuerAssignPolicyNo to set
	 */
	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}

	/**
	 * @return the exchangeAssignPolicyNo
	 */
	public String getExchangeAssignPolicyNo() {
		return exchangeAssignPolicyNo;
	}

	/**
	 * @param exchangeAssignPolicyNo the exchangeAssignPolicyNo to set
	 */
	public void setExchangeAssignPolicyNo(String exchangeAssignPolicyNo) {
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
	}

	/**
	 * @return the carrierAppId
	 */
	public String getCarrierAppId() {
		return carrierAppId;
	}
	/**
	 * @param carrierAppId the carrierAppId to set
	 */
	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}
	/**
	 * @return the carrierAppUID
	 */
	public String getCarrierAppUID() {
		return carrierAppUID;
	}
	/**
	 * @param carrierAppUID the carrierAppUID to set
	 */
	public void setCarrierAppUID(String carrierAppUID) {
		this.carrierAppUID = carrierAppUID;
	}
	/**
	 * @return the enrollmentStatus
	 */
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	
	/**
	 * @return the renewalFlag
	 */
	public String getRenewalFlag() {
		return renewalFlag;
	}

	/**
	 * @param renewalFlag the renewalFlag to set
	 */
	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}
	
	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enrollmentId == null) ? 0 : enrollmentId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnrollmentBobDTO other = (EnrollmentBobDTO) obj;
		if (enrollmentId == null) {
			if (other.enrollmentId != null)
				return false;
		} else if (!enrollmentId.equals(other.enrollmentId))
			return false;
		return true;
	}
}
