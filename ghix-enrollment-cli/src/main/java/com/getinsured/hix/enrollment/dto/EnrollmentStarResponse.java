package com.getinsured.hix.enrollment.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EnrollmentStarResponse  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String appID;
	private String status;
	private String errMsg;
	private int errCode;
	private long execDuration;
	private long startTime;		
	private String moduleStatusCode;
	private List<EnrollmentSubscriberDetailsDTO> enrollmentSubscriberDetailsDTOList;
	private Integer enrollmentCommissionId;
	private boolean isEnrollmentCommissionSaveSuccess = false;
	Set<Integer> issuerIds = new HashSet<Integer>();
	
	public enum EnrollmentRenewalResponse {EFFECTIVE_DATE_MISMATCH,BOB_NO_STATUS_UPDATE,BOB_SUCCESS}
	private EnrollmentRenewalResponse enrollmentRenewalResponse;
	
	
	public void startResponse() {
		startTime = Calendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = Calendar.getInstance().getTimeInMillis();
		execDuration = endTime - startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public long getExecDuration() {
		return execDuration;
	}

	public void setExecDuration(long execDuration) {
		this.execDuration = execDuration;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getModuleStatusCode() {
		return moduleStatusCode;
	}

	public void setModuleStatusCode(String moduleStatusCode) {
		this.moduleStatusCode = moduleStatusCode;
	}

	/**
	 * @return the enrollmentSubscriberDetailsDTOList
	 */
	public List<EnrollmentSubscriberDetailsDTO> getEnrollmentSubscriberDetailsDTOList() {
		return enrollmentSubscriberDetailsDTOList;
	}

	/**
	 * @param enrollmentSubscriberDetailsDTOList the enrollmentSubscriberDetailsDTOList to set
	 */
	public void setEnrollmentSubscriberDetailsDTOList(
			List<EnrollmentSubscriberDetailsDTO> enrollmentSubscriberDetailsDTOList) {
		this.enrollmentSubscriberDetailsDTOList = enrollmentSubscriberDetailsDTOList;
	}

	/**
	 * @return the enrollmentCommissionId
	 */
	public Integer getEnrollmentCommissionId() {
		return enrollmentCommissionId;
	}

	/**
	 * @param enrollmentCommissionId the enrollmentCommissionId to set
	 */
	public void setEnrollmentCommissionId(Integer enrollmentCommissionId) {
		this.enrollmentCommissionId = enrollmentCommissionId;
	}

	/**
	 * @return the isEnrollmentCommissionSaveSuccess
	 */
	public boolean isEnrollmentCommissionSaveSuccess() {
		return isEnrollmentCommissionSaveSuccess;
	}

	/**
	 * @param isEnrollmentCommissionSaveSuccess the isEnrollmentCommissionSaveSuccess to set
	 */
	public void setEnrollmentCommissionSaveSuccess(boolean isEnrollmentCommissionSaveSuccess) {
		this.isEnrollmentCommissionSaveSuccess = isEnrollmentCommissionSaveSuccess;
	}

	/**
	 * @return the enrollmentRenewalResponse
	 */
	public EnrollmentRenewalResponse getEnrollmentRenewalResponse() {
		return enrollmentRenewalResponse;
	}

	/**
	 * @param enrollmentRenewalResponse the enrollmentRenewalResponse to set
	 */
	public void setEnrollmentRenewalResponse(EnrollmentRenewalResponse enrollmentRenewalResponse) {
		this.enrollmentRenewalResponse = enrollmentRenewalResponse;
	}

	public Set<Integer> getIssuerIds() {
		return issuerIds;
	}

	public void setIssuerIds(Set<Integer> issuerIds) {
		this.issuerIds = issuerIds;
	}

}
