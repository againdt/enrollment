package com.getinsured.hix.enrollment.dto;

import java.util.Map;

public class LookupStarrResponse {

	private String status;
	private String errMsg;
	private Map<Integer,String> lookupValueDetails;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public Map<Integer, String> getLookupValueDetails() {
		return lookupValueDetails;
	}
	public void setLookupValueDetails(Map<Integer, String> lookupValueDetails) {
		this.lookupValueDetails = lookupValueDetails;
	}
	
	
}
