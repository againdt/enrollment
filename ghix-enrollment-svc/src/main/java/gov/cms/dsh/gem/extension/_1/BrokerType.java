//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.26 at 04:42:57 PM IST 
//


package gov.cms.dsh.gem.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.niem.niem.niem_core._2.AddressType;
import gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for Representation for all the required information related to a Broker
 * 
 * <p>Java class for BrokerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BrokerType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}BrokerAugmentationType"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}AccountNumber" minOccurs="0"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}BrokerContactInformation" minOccurs="0"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}Address"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "BrokerType")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BrokerType", propOrder = {
	"brokerAugmentationType",
	"accountNumber",
    "brokerContactInformation",
    "address"
})
public class BrokerType
    extends ComplexObjectType
{

    @XmlElement(name = "BrokerAugmentationType", required = true)
    protected BrokerOrganizationAugmentationType brokerAugmentationType;
    @XmlElement(name = "AccountNumber")
    protected String accountNumber;
    @XmlElement(name = "BrokerContactInformation")
    protected SingleContactInformationType brokerContactInformation;
    @XmlElement(name = "Address", required = true)
    protected AddressType address;

    /**
     * Gets the value of the brokerAugmentationType property.
     * 
     * @return
     *     possible object is
     *     {@link BrokerOrganizationAugmentationType }
     *     
     */
    public BrokerOrganizationAugmentationType getBrokerAugmentationType() {
        return brokerAugmentationType;
    }

    /**
     * Sets the value of the brokerAugmentationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BrokerOrganizationAugmentationType }
     *     
     */
    public void setBrokerAugmentationType(BrokerOrganizationAugmentationType value) {
        this.brokerAugmentationType = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the brokerContactInformation property.
     * 
     * @return
     *     possible object is
     *     {@link SingleContactInformationType }
     *     
     */
    public SingleContactInformationType getBrokerContactInformation() {
        return brokerContactInformation;
    }

    /**
     * Sets the value of the brokerContactInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SingleContactInformationType }
     *     
     */
    public void setBrokerContactInformation(SingleContactInformationType value) {
        this.brokerContactInformation = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link AddressType }
     *     
     */
    public AddressType getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressType }
     *     
     */
    public void setAddress(AddressType value) {
        this.address = value;
    }

}
