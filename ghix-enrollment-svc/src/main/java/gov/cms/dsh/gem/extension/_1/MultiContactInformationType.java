//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.26 at 04:42:57 PM IST 
//


package gov.cms.dsh.gem.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import gov.niem.niem.niem_core._2.PersonNameType;
import gov.niem.niem.proxy.xsd._2.String;
import gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for Multi Contact Information.
 * 
 * <p>Java class for MultiContactInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultiContactInformationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}ContactType"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}ContactPersonName"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}EmployerContactNumber"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactEmailID"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}PreferredContactMode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultiContactInformationType", propOrder = {
    "contactType",
    "contactPersonName",
    "employerContactNumber",
    "contactEmailID",
    "preferredContactMode"
})
public class MultiContactInformationType
    extends ComplexObjectType
{

    @XmlElement(name = "ContactType", required = true)
    protected ContactCodeType contactType;
    @XmlElement(name = "ContactPersonName", required = true)
    protected PersonNameType contactPersonName;
    @XmlElement(name = "EmployerContactNumber", required = true)
    protected EmployerContactType employerContactNumber;
    @XmlElement(name = "ContactEmailID", namespace = "http://niem.gov/niem/niem-core/2.0", required = true, nillable = true)
    protected String contactEmailID;
    @XmlElement(name = "PreferredContactMode", required = true)
    protected PreferredContactModeCodeType preferredContactMode;

    /**
     * Gets the value of the contactType property.
     * 
     * @return
     *     possible object is
     *     {@link ContactCodeType }
     *     
     */
    public ContactCodeType getContactType() {
        return contactType;
    }

    /**
     * Sets the value of the contactType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactCodeType }
     *     
     */
    public void setContactType(ContactCodeType value) {
        this.contactType = value;
    }

    /**
     * Gets the value of the contactPersonName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameType }
     *     
     */
    public PersonNameType getContactPersonName() {
        return contactPersonName;
    }

    /**
     * Sets the value of the contactPersonName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameType }
     *     
     */
    public void setContactPersonName(PersonNameType value) {
        this.contactPersonName = value;
    }

    /**
     * Gets the value of the employerContactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link EmployerContactType }
     *     
     */
    public EmployerContactType getEmployerContactNumber() {
        return employerContactNumber;
    }

    /**
     * Sets the value of the employerContactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployerContactType }
     *     
     */
    public void setEmployerContactNumber(EmployerContactType value) {
        this.employerContactNumber = value;
    }

    /**
     * Gets the value of the contactEmailID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactEmailID() {
        return contactEmailID;
    }

    /**
     * Sets the value of the contactEmailID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactEmailID(String value) {
        this.contactEmailID = value;
    }

    /**
     * Gets the value of the preferredContactMode property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredContactModeCodeType }
     *     
     */
    public PreferredContactModeCodeType getPreferredContactMode() {
        return preferredContactMode;
    }

    /**
     * Sets the value of the preferredContactMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredContactModeCodeType }
     *     
     */
    public void setPreferredContactMode(PreferredContactModeCodeType value) {
        this.preferredContactMode = value;
    }

}
