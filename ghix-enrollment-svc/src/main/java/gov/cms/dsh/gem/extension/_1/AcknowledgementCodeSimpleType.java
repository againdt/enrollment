//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.26 at 04:42:57 PM IST 
//


package gov.cms.dsh.gem.extension._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcknowledgementCodeSimpleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AcknowledgementCodeSimpleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="HS000000"/>
 *     &lt;enumeration value="HX009000"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AcknowledgementCodeSimpleType")
@XmlEnum
public enum AcknowledgementCodeSimpleType {


    /**
     * File sucessfully processed
     * 
     */
    @XmlEnumValue("HS000000")
    HS_000000("HS000000"),

    /**
     * File Failed Schema Validation
     * 
     */
    @XmlEnumValue("HX009000")
    HX_009000("HX009000");
    private final String value;

    AcknowledgementCodeSimpleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AcknowledgementCodeSimpleType fromValue(String v) {
        for (AcknowledgementCodeSimpleType c: AcknowledgementCodeSimpleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
