//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.26 at 04:42:57 PM IST 
//


package gov.cms.dsh.gem.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import gov.niem.niem.niem_core._2.FullTelephoneNumberType;


/**
 * A data type for Employer Primary contact number restriction
 * 
 * <p>Java class for EmployerPrimaryNumberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmployerPrimaryNumberType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://niem.gov/niem/niem-core/2.0}FullTelephoneNumberType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}TelephoneNumberFullID"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}TelephoneSuffixID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmployerPrimaryNumberType")
public class EmployerPrimaryNumberType
    extends FullTelephoneNumberType
{


}
