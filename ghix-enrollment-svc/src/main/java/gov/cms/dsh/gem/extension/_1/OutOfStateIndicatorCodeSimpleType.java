//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.26 at 04:42:57 PM IST 
//


package gov.cms.dsh.gem.extension._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutOfStateIndicatorCodeSimpleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OutOfStateIndicatorCodeSimpleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="N"/>
 *     &lt;enumeration value="Y"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OutOfStateIndicatorCodeSimpleType")
@XmlEnum
public enum OutOfStateIndicatorCodeSimpleType {


    /**
     * No
     * 
     */
    N,

    /**
     * Yes
     * 
     */
    Y;

    public String value() {
        return name();
    }

    public static OutOfStateIndicatorCodeSimpleType fromValue(String v) {
        return valueOf(v);
    }

}
