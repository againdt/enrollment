//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.26 at 04:42:57 PM IST 
//


package gov.cms.dsh.gem.extension._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for Root Element - Transfers employer group information to the issuers system
 * 
 * <p>Java class for GroupEnrollmentRequestPayloadType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GroupEnrollmentRequestPayloadType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}FileInformation"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}Employer"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}Broker" maxOccurs="2" minOccurs="0"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}PlanInformation" minOccurs="0"/>
 *         &lt;element ref="{http://gem.dsh.cms.gov/extension/1.0}AdditionalNotes" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupEnrollmentRequestPayloadType", propOrder = {
    "fileInformation",
    "employer",
    "broker",
    "planInformation",
    "additionalNotes"
})
@XmlRootElement(name = "GroupEnrollmentRequestPayloadType")
public class GroupEnrollmentRequestPayloadType
    extends ComplexObjectType
{

    @XmlElement(name = "FileInformation", required = true)
    protected FileInformationType fileInformation;
    @XmlElement(name = "Employer", required = true)
    protected EmployerType employer;
    @XmlElement(name = "Broker")
    protected List<BrokerType> broker;
    @XmlElement(name = "PlanInformation")
    protected PlanType planInformation;
    @XmlElement(name = "AdditionalNotes")
    protected String additionalNotes;

    /**
     * Gets the value of the fileInformation property.
     * 
     * @return
     *     possible object is
     *     {@link FileInformationType }
     *     
     */
    public FileInformationType getFileInformation() {
        return fileInformation;
    }

    /**
     * Sets the value of the fileInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileInformationType }
     *     
     */
    public void setFileInformation(FileInformationType value) {
        this.fileInformation = value;
    }

    /**
     * Gets the value of the employer property.
     * 
     * @return
     *     possible object is
     *     {@link EmployerType }
     *     
     */
    public EmployerType getEmployer() {
        return employer;
    }

    /**
     * Sets the value of the employer property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployerType }
     *     
     */
    public void setEmployer(EmployerType value) {
        this.employer = value;
    }

    /**
     * Gets the value of the broker property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the broker property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBroker().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BrokerType }
     * 
     * 
     */
    public List<BrokerType> getBroker() {
        if (broker == null) {
            broker = new ArrayList<BrokerType>();
        }
        return this.broker;
    }

    /**
     * Gets the value of the planInformation property.
     * 
     * @return
     *     possible object is
     *     {@link PlanType }
     *     
     */
    public PlanType getPlanInformation() {
        return planInformation;
    }

    /**
     * Sets the value of the planInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanType }
     *     
     */
    public void setPlanInformation(PlanType value) {
        this.planInformation = value;
    }

    /**
     * Gets the value of the additionalNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalNotes() {
        return additionalNotes;
    }

    /**
     * Sets the value of the additionalNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalNotes(String value) {
        this.additionalNotes = value;
    }

}
