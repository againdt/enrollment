/**
 * 
 */
package com.getinsured.hix.enrollment1095.service;

import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Enrollment 1095 Notification Interface
 * @author negi_s
 *
 */
public interface Enrollment1095NotificationService {
	
	/**
	 * Send staging job completion email to internal group
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendStagingJobCompletionEmail(Map<String, String> requestMap)throws GIException;
	
	/**
	 * Jira Id: HIX-78077
	 * Send 1095 CMS XML statistics details 
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendCMSXMLGenerationEmail(Map<String, String> requestMap)throws GIException;
	
	/**
	 * Jira Id: HIX-76800
	 * Create an inbound batch for processing 1095-A Errors from CMS - Part 1
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendEnrollmentIn1095Email(Map<String, String> requestMap) throws GIException;
	
	/**
	 * Jira Id: HIX-76800
	 * Create an inbound batch for processing 1095-A Errors from CMS - Part 1
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendEnrollmentIn1095ProcessEmail(Map<String, String> emailDataMap) throws GIException;
	
	/**
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendDoNothingEmail(Map<String, String> emailDataMap) throws GIException;
}
