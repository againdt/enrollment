/**
 * 
 */
package com.getinsured.hix.enrollment1095.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.email.Enrollment1095AXMLNotification;
import com.getinsured.hix.enrollment.email.Enrollment1095InDoNothingNotification;
import com.getinsured.hix.enrollment.email.Enrollment1095InNotification;
import com.getinsured.hix.enrollment.email.Enrollment1095InProcessNotification;
import com.getinsured.hix.enrollment.email.Enrollment1095StagingNotification;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
@Service("enrollment1095NotificationService")
public class Enrollment1095NotificationServiceImpl implements Enrollment1095NotificationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095NotificationServiceImpl.class);
	
	@Autowired private Enrollment1095StagingNotification enrollment1095StagingNotification;
	@Autowired private Enrollment1095AXMLNotification enrollment1095AXMLNotification;
	@Autowired private Enrollment1095InNotification enrollment1095InNotification;
	@Autowired private Enrollment1095InProcessNotification enrollment1095InProcessNotification;
	@Autowired private Enrollment1095InDoNothingNotification enrollment1095InDoNothingNotification;
	
	@Override
	public void sendStagingJobCompletionEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("Enrollment1095NotificationService :: 1095 Staging job statistics email sending process");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("Enrollment1095NotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("Enrollment1095NotificationService:: Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollment1095StagingNotification.setEmailData(emailData);
			enrollment1095StagingNotification.setRequestData(requestMap);
			Notice noticeObj = enrollment1095StagingNotification.generateEmail();
			Notification notificationObj = enrollment1095StagingNotification.generateNotification(noticeObj);
			LOGGER.trace("Notice body :: "+noticeObj.getEmailBody());
			enrollment1095StagingNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex){
			throw new GIException(ex);
		}
	}

	@Override
	public void sendCMSXMLGenerationEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("Enrollment1095NotificationService:: 1095 CMS XML generated details");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("Enrollment1095NotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("Enrollment1095NotificationService:: Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollment1095AXMLNotification.setEmailData(emailData);
			enrollment1095AXMLNotification.setRequestData(requestMap);
			Notice noticeObj = enrollment1095AXMLNotification.generateEmail();
			Notification notificationObj = enrollment1095AXMLNotification.generateNotification(noticeObj);
			enrollment1095AXMLNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			LOGGER.error("Enrollment1095NotificationService:: Exception caught in sendCMSXMLGenerationEmail method. ");
			throw new GIException(ex);
		}
		
	}
	
	@Override
	public void sendEnrollmentIn1095Email(Map<String, String> requestMap) throws GIException {
		LOGGER.info("Enrollment1095NotificationService:: SEND EnrollmentIn1095Email details");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("Enrollment1095NotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("Enrollment1095NotificationService:: ( sendEnrollmentIn1095Email ) Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollment1095InNotification.setEmailData(emailData);
			enrollment1095InNotification.setRequestData(requestMap);
			Notice noticeObj = enrollment1095InNotification.generateEmail();
			Notification notificationObj = enrollment1095InNotification.generateNotification(noticeObj);
			enrollment1095InNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			LOGGER.error("Enrollment1095NotificationService:: Exception caught in sendEnrollmentIn1095Email method. ");
			throw new GIException(ex);
		}
	}

	@Override
	public void sendEnrollmentIn1095ProcessEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("Enrollment1095NotificationService:: SEND EnrollmentIn1095ProcessedEmail details");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("Enrollment1095NotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("Enrollment1095NotificationService:: (sendEnrollmentIn1095ProcessEmail) Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollment1095InProcessNotification.setEmailData(emailData);
			enrollment1095InProcessNotification.setRequestData(requestMap);
			Notice noticeObj = enrollment1095InProcessNotification.generateEmail();
			Notification notificationObj = enrollment1095InProcessNotification.generateNotification(noticeObj);
			enrollment1095InProcessNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			LOGGER.error("Enrollment1095NotificationService:: Exception caught in sendEnrollmentIn1095ProcessEmail method. ");
			throw new GIException(ex);
		}		
	}

	@Override
	public void sendDoNothingEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("Enrollment1095NotificationService:: SEND DoNothingEmail details");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("Enrollment1095NotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("Enrollment1095NotificationService:: (sendEnrollmentIn1095ProcessEmail) Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollment1095InDoNothingNotification.setEmailData(emailData);
			enrollment1095InDoNothingNotification.setRequestData(requestMap);
			Notice noticeObj = enrollment1095InDoNothingNotification.generateEmail();
			Notification notificationObj = enrollment1095InDoNothingNotification.generateNotification(noticeObj);
			enrollment1095InDoNothingNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			LOGGER.error("Enrollment1095NotificationService:: Exception caught in DoNothingEmail method. ");
			throw new GIException(ex);
		}	
		
	}
}
