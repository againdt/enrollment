package com.getinsured.hix.enrollment.service;

import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.history.Revision;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrolleePrivateRepository;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.model.enrollment.Enrollee;


@Service("enrolleeAuditPrivateService")
@Transactional
public class EnrolleeAuditPrivateServiceImpl implements EnrolleeAuditPrivateService{

	@Autowired	private IEnrolleePrivateRepository enrolleeRepository;
	@Override
	public Revision<Integer, Enrollee> findRevisionByDate(int enrolleeId,  Date selectDate) throws ParseException {

		Revision<Integer, Enrollee> revision = enrolleeRepository.findRevisionByDate(enrolleeId, selectDate,EnrollmentPrivateConstants.UPDATED_ON);
		if (revision != null){
			return revision;
		}
		return null;
	}
	
	public Revision<Integer, Enrollee> findRevisionByEvent(int enrolleeId,  Integer lastEventID) throws ParseException {

		Revision<Integer, Enrollee> revision = enrolleeRepository.findRevisionByEvent(enrolleeId, lastEventID,"lastEventId");
		if (revision != null){
			return revision;
		}
		return null;
	}
	
}
