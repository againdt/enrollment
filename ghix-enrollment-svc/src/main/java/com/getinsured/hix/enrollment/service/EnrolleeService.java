/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberRequestDTO;
import com.getinsured.hix.dto.enrollment.QhpReportDTO;
import com.getinsured.hix.dto.enrollment.QhpReportResponseDTO;
import com.getinsured.hix.dto.enrollment.SendUpdatedEnrolleeResponseDTO;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.EnrolleeUpdateSendStatus;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.exception.GIException;


/**
 * @author panda_p
 *
 */
public interface EnrolleeService {
	/**
	 * This methode return list of enrollees which are updated since last carrier update.
	 * 
	 * @return Enrollee List
	 */
	List<Enrollee> getCarrierUpdatedEnrollments();
	
	
	
	/**
	 * Return Enrollee for provided id
	 * @param id
	 * @return Enrollee
	 */
	Enrollee findById(Integer id);
	
	/**
	 * 
	 * @param searchCriteria
	 * @return
	 */
	Map<String, Object> searchEnrollee(Map<String, Object> searchCriteria);
	
	/**
	 * 
	 * @param inputVal
	 */
	void saveCarrierUpdatedDataResponse(List<SendUpdatedEnrolleeResponseDTO> inputVal);
	
	/**
	 * This method is used to find Enrollee by passing parameters memberID, enrollmentID, planID
	 * 
	 * @author parhi_s
	 * @param enrollmentID
	 * @param planID
	 * @param memberID
	 * @return List of Enrollee
	 * @throws Exception
	 */
	List<Enrollee> findEnrolleeByEnrollmentIDAndMemberIDAndPlanID(Integer enrollmentID, Integer planID, String memberID) throws GIException;
	/**
	 * Returns the active Enrollment for SSN and Coverage dates
	 * @param SSN
	 * @param Coverage Start Date
	 *  @param Coverage End Date
	 * @return
	 */
	List<Enrollment> findActiveEnrollmentBySSNAndCoverageDates(String ssn,Date coverageStartDate,Date coverageEndDate) throws GIException;
	List<Enrollment> findActiveEnrollmentBySSN(String ssn) throws GIException;
	/**
	 * This method is used to Update Enrollee
	 * 
	 * @author parhi_s
	 * @param enrollee
	 * @throws Exception
	 */
	
	void updateEnrollee(Enrollee enrollee) throws GIException ;
	
	/**
	 * This method is used to find Enrollee By passing parameters memberID and enrollmentID
	 * 
	 * @author parhi_s
	 * @param memberID
	 * @param enrollmentID
	 * @return
	 * @throws Exception
	 */
	List<Enrollee> findEnrolleeByEnrollmentIDAndMemberID(String memberID, Integer enrollmentID) throws GIException;
	
	/**
	 * 
	 * @author panda_p
	 * @param exchgIndivIdentifier
	 * @return
	 */
	Enrollee getEnrolleeByExchgIndivIdentifier(String exchgIndivIdentifier);
	
	/**
	 * @author parhi_s
	 * @param enrollmentID
	 * @return
	 * @throws GIException
	 */
	Enrollee findSubscriberbyEnrollmentIDId(Integer enrollmentID) ;
	Integer findSubscriberIdByEnrollmentId(Integer enrollmentID);
	
	List<Enrollee> getEnrolleeByStatusAndEnrollmentID(Integer enrollmentID, String status) throws GIException;
	List<Enrollee> getEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException;
	Enrollee getHouseholdContactByEnrollmentID(Integer enrollmentID)throws GIException;
	Enrollee getResponsiblePersonByEnrollmentID(Integer enrollmentID)throws GIException;
	List<Enrollee> findEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException;
	List<Enrollee> findEnrolledEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException;
	
	/**The getEnrollmentEmployeeIdAndEnrollmentStatus() takes the input parameters as 
	 * employeeId, enrollment status and return results as a list of enrollment objects  
	 * @author raja
	 * @since 08/30/2013
	 * @param employeeId, enrollmentStatus 
	 * @return List<Enrollment>
	 * @throws GIException
	 */
	List<Enrollment> getEnrollmentEmployeeIdAndEnrollmentStatus(Integer employeeId, Long employeeAppId,List<String> enrollmentStatus)throws GIException;
	
	/**
	 * The getEnrollmentEmployeeIdAndEnrollmentStatus() takes the input parameters as 
	 * ssapApplicationId, enrollment status and return results as a list of enrollment objects
	 * 
	 * @author panda_p
	 * @since 08/30/2013
	 * @param ssapAppId
	 * @param enrollmentStatus
	 * @return
	 * @throws GIException
	 */
	List<Enrollment> getEnrollmentBySsapApplictionIdAndEnrollmentStatus (Long ssapAppId,List<String> enrollmentStatus) throws GIException;
	
	/**
	 * Returns List of Enrolled (Not Canceled or Terminated) Enrollees for given Enrollment ID
	 * @param enrollmentID
	 * @return
	 * @throws GIException
	 */
	List<Enrollee> getEnrolledEnrolleesForEnrollmentID(Integer enrollmentID)throws GIException;
	
//	/**
//	 * Returns the active Enrollment for employeeID and memberID
//	 * @param employeeId
//	 * @param memberID
//	 * @return
//	 */
//	List<Enrollment> findActiveEnrollmentByEmployeeIDandMemberID(Integer employeeId, String memberID) throws GIException;
	
//	/**
//	 * Returns the active Enrollment for employeeID and memberID and employeeAppId
//	 * @param employeeId
//	 * @param memberID
//	 * @param employeeAppId
//	 * @return
//	 * @throws GIException
//	 */
//	List<Enrollment> findActiveEnrollmentByEmployeeIDandMemberID(Integer employeeId, String memberID, Integer employeeAppId)throws GIException;

	/**
	 * Returns List of Enrollees with enrollment_reason = 'S' for given Enrollment ID
	 * @param enrollmentID
	 * @return
	 * @throws GIException
	 */
	List<Enrollee> getSpecialEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException;
	
	/**
	 * Returns List of Enrollees with enrollment_reason = 'S' for given Enrollment ID
	 * @param enrollmentID
	 * @param enrollmentID, Map
	 * @return
	 * @throws GIException
	 */
	List<Enrollee> getSpecialEnrolleeByEnrollmentID(Map<String, Object> inputval,Integer enrollmentID)throws GIException;
	
	/**
	 * @author raja
	 * @param enrollmentResponse
	 * @return Integer
	 */
	List<Enrollee> getEnrolleeExchgIndivIdentifierAndCoverageStartDateByEnrollmentId(Integer enrollmentId);

	Enrollment getHealthEnrollmentEmployeeIdAndEnrollmentStatus(Integer employeeId,Long employeeAppId,List<String> status);
	
	Enrollment getDentalEnrollmentEmployeeIdAndEnrollmentStatus(Integer employeeId,Long employeeAppId,List<String> status);
	
	/**
	 * 
	 * @author Aditya_S
	 * 
	 * HIX-66734
	 * Returns Enrollee Aud information for given Enrollee Id and Aud record before Last Invoice Timestamp.
	 * 
	 * @param enrolleeRequestStr
	 * @return
	 */
	String getEnrolleeCoverageDetailsBeforeLastInvoice(String enrolleeRequestStr);
	
	/**
	 * Jira: HIX-101509
	 * Returns Enrollee and Enrollment information based in the ApplicationId
	 * @param enrolleeRequest Object<EnrolleeRequest>
	 * @param enrolleeResponse Object<EnrolleeResponse>
	 */
	void findEnrolleeByApplicationid(EnrolleeRequest enrolleeRequest, EnrolleeResponse enrolleeResponse);
	
	/**
	 * Jira Id: HIX-71256
	 * @author Sharma_k
	 * @since 25th June 2015
	 * @param enrollmentIdList List<Integer>
	 * @param personTypeList List<String>
	 * @param systemDate Date
	 * @return List<Object[]>
	 * 
	 * returns Active Enrollee Data for given Enrollment ID List 
	 */
	List<Object[]> findActiveEnrolleeDataByListOfEnrollmentIds(List<Integer> enrollmentIdList, List<String> personTypeList, Date systemDate);
	
	/**
	 * 
	 * @param enrollmentIdList
	 * @param personTypeList
	 * @return
	 */
	List<Object[]> getEnrolleeDataByListOfEnrollmentIdAndPersonType(List<Integer> enrollmentIdList, List<String> personTypeList);
	
	/**
	 * 
	 * @param enrollmentIdList
	 * @param personTypeList
	 * @return
	 */
	List<Object[]> getEnrolleeDataByListOfEnrollmentIdAndPersonTypeCA(List<Integer> enrollmentIdList, List<String> personTypeList);
	
	/**
	 * Jira Id: HIX-77598
	 * @param enrollmentIdList
	 * @param personTypeList
	 * @return
	 */
	List<Object[]> getEnrolleesDataByListOfEnrollmentId(List<Integer> enrollmentIdList, List<String> personTypeList);
	
	/**
	 * 
	 * @param inputVal
	 * @param enrolleeUpdateSendStatusObject
	 */
	void saveCarrierUpdatedDataResponse(List<SendUpdatedEnrolleeResponseDTO> inputVal, EnrolleeUpdateSendStatus enrolleeUpdateSendStatusObject);
	
	/**
	 * 
	 * @param Object<EnrollmentSubscriberRequestDTO> enrollmentSubscriberRequestDTO 
	 * @Param Object<EnrolleeResponse> enrolleeResponse
	 * @return List<Object<EnrollmentSubscriberDTO>>
	 * @throws GIException
	 */
	List<EnrollmentSubscriberDTO> fetchEnrollmentSubscriberData(EnrollmentSubscriberRequestDTO enrollmentSubscriberRequestDTO, EnrolleeResponse enrolleeResponse);

	/**
	 * Get subscriber data for get API
	 * @param enrollmentId
	 * @return List<Object[]>
	 */
	List<Object[]> getSubscriberDataByEnrollmentId(Integer enrollmentId);

	List<QhpReportDTO> getEnrollmentAndEnrolleeDataToGenerateQHPReports(Integer enrollmentId);

	boolean storeQHPReportData(List<QhpReportDTO> qhpReportList, Integer batchJobExecutionId);

	boolean cleanOldQHPReportDataByJobId(Integer batchJobExecutionId);

	/**
	 * 
	 * @param enrollmentId
	 * @param changeEffectiveDate
	 * @return
	 */
	List<Enrollee> getEnrolleeExchgIndivIdentifierAndCoverageStartDateByEnrollmentIdAndChangeDate(Integer enrollmentId,
			Date changeEffectiveDate);

}
