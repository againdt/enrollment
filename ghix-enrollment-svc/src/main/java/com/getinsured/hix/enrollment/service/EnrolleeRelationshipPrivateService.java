/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;

/**
 * @author panda_p
 *
 */
public interface EnrolleeRelationshipPrivateService {
	EnrolleeRelationship findBySourceEnrolleeAndTargetEnrollee(Enrollee sourceEnrollee, Enrollee targetEnrollee);
}
