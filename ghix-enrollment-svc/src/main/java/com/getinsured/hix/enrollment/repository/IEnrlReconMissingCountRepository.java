package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrlReconMissingCount;

public interface IEnrlReconMissingCountRepository extends JpaRepository<EnrlReconMissingCount, Integer>{
	
	@Query(" FROM EnrlReconMissingCount ermc  WHERE ermc.processingMonth = :processingMonth AND  ermc.processingYear=:processingYear AND ermc.hiosIssuerId=:hiosIssuerId ")
	List<EnrlReconMissingCount> getMissingCountByMonthYearIssuer(@Param("processingMonth") Integer processingMonth,@Param("processingYear") Integer processingYear,@Param("hiosIssuerId") String hiosIssuerId);
	
	
	@Query("SELECT ermc.processingMonth, ermc.processingYear, SUM(enrlCntMissingInHix), SUM(enrlCntMissingInFile)"
			+ " FROM EnrlReconMissingCount ermc  WHERE ermc.processingMonth = :processingMonth AND  ermc.processingYear=:processingYear AND ermc.hiosIssuerId IN(:hiosIssuerIdList)"
			+ " GROUP BY ermc.processingMonth, ermc.processingYear")
	List<Object[]> getHighLevelSummaryMissingCount(@Param("processingMonth") Integer processingMonth, @Param("processingYear") Integer processingYear, @Param("hiosIssuerIdList") List<String> hiosIssuerIdList);
	
	@Query("SELECT ermc.processingMonth, ermc.processingYear, SUM(enrlCntMissingInHix), SUM(enrlCntMissingInFile)"
			+ " FROM EnrlReconMissingCount ermc  WHERE ermc.processingMonth = :processingMonth AND  ermc.processingYear=:processingYear AND ermc.hiosIssuerId =:hiosIssuerId"
			+ " GROUP BY ermc.processingMonth, ermc.processingYear")
	List<Object[]> getIssuerLevelMissingCount(@Param("processingMonth") Integer processingMonth, @Param("processingYear") Integer processingYear, @Param("hiosIssuerId")String hiosIssuerId);


}
