package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.shop.EmployeeDetailsDTO;
import com.getinsured.hix.dto.shop.RestEmployeeDTO;
import com.getinsured.hix.enrollment.email.EnrollmentEmailNotification;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * @author raja
 * @since 07/09/2013
 */
@Service("enrollmentEmailNotificationPrivateService")
@Transactional
public class EnrollmentEmailNotificationServicePrivateImpl implements EnrollmentEmailNotificationPrivateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEmailNotificationServicePrivateImpl.class);
	
	
	@Autowired private RestTemplate restTemplate;
	@Autowired private EnrolleePrivateService enrolleeService;
	@Autowired private EnrollmentEmailNotification emailNotification;
		
	
	@Override
	@Transactional
	public void sendEnrollmentStatusNotification(List<Enrollment> enrollmentList) throws GIException {
		
		if(enrollmentList!=null){
		    
			 for(Enrollment enrollment : enrollmentList){
			
				 if(enrollment.getEnrollmentStatusLkp()!=null &&  enrollment.getEmployeeId()!=null &&
						 (enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL) || 
						 (enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM)))){
					  
					  List<Enrollee> enrolleeList = enrolleeService.findEnrolleeByEnrollmentID(enrollment.getId()); 
					  Employer employer = enrollment.getEmployer();
					  
					  for(Enrollee enrollee:enrolleeList){
						  try{
							  if((enrollee.getPersonTypeLkp()!=null ) && 
									  (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER))){

								  XStream xstream = GhixUtils.getXStreamStaxObject();
								  EmployeeDetailsDTO employeeDetails=null;
								  String getResp = null;


								  RestEmployeeDTO restEmployeeDTO = new RestEmployeeDTO();
								  restEmployeeDTO.setEmployeeId(enrollment.getEmployeeId());
								//  String temp = GhixUtils.getXStreamStaxObject().toXML(restEmployeeDTO);
								  getResp = restTemplate.postForObject(GhixEndPoints.ShopEndPoints.GET_EMPLOYEE_DETAILS, restEmployeeDTO,String.class);
								  if(getResp!=null){
									  employeeDetails = (EmployeeDetailsDTO) xstream.fromXML(getResp);
								  }

								  Map<String,Object> emailData = new HashMap<String, Object>();

								  //required for notice template
								  String emailTitle = null;

								  if(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)){
									  emailTitle="EnrollmentStatusCancelled";
									  emailData.put("title","Enrollment Status Cancelled"); 
									  emailData.put("textCancelTerm",enrollment.getEnrollmentStatusLkp().getLookupValueLabel().toLowerCase());
								  }
								  else{
									  emailTitle="EnrollmentStatusTerminated";
									  emailData.put("textCancelTerm",enrollment.getEnrollmentStatusLkp().getLookupValueLabel().toLowerCase());
								  }
								  emailData.put("EmailTitle",emailTitle); //Required for pdf filename and title for the email
								  emailData.put("ModuleID",String.valueOf(enrollment.getEmployeeId()));//Required for pdf filename
								  emailData.put("ModuleName",ModuleUserService.EMPLOYEE_MODULE);//Required for pdf filename
								  List<String> sendToEmailList = new ArrayList<String>(); //Required for sending email, notifying a notice is generated.
								  if(null != employeeDetails){
									  sendToEmailList.add(employeeDetails.getEmail());
									  emailData.put("toFullName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());// who is sending email
									  emailData.put("employeeName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());
								  }
								  emailData.put("sendToEmailList",sendToEmailList);
								  emailData.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
								  emailData.put("fileName","Notice-"+emailData.get("EmailTitle")+"-"+new TSDate().getTime()+".pdf");
								  emailData.put("exchangePhoneNumber",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
								  emailData.put("exchangeURL",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
								  emailData.put("exchangeFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));


								  //required for email template - [EnrollmentStatusCancelTermNotification.html]
								 
								  emailData.put("enrolledPlanName",enrollment.getPlanName());
								  emailData.put("issuerName",enrollment.getInsurerName());
								  emailData.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
								  emailData.put("employerName",employer.getName());
								  emailData.put("terminationDate",enrollee.getEffectiveEndDate());
								  emailData.put("employerPhoneNumber",employer.getContactNumber());
								  emailData.put("exchangeSignature",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_SIGNATURE));

								  // send notification to the respective employee
								  try{
									  emailNotification.setNotificationData(emailData);
							// 		  emailNotification.generateNotice();
								  } catch (Exception ex) {
									  LOGGER.error("Enrollment status email notification failed.",ex);
									  throw new GIException("Enrollment status email notification failed.",ex);
								  }

							  } 
						  }catch(Exception e){
							  LOGGER.error("Invalid employee id->"+enrollment.getEmployeeId()+"",e);
							  //								 throw new GIException("Invalid employee id->"+enrollment.getEmployeeId()+"",e.getCause());
						  }
					  }
				   } 
			   }	 
		  }
		
	}
	

}
