package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.enrollment.EnrollmentPLRIn;

public interface IEnrollmentPLRInRepository extends JpaRepository<EnrollmentPLRIn, Integer> {
	
}
