package com.getinsured.hix.enrollment.repository;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.LocationAud;

public interface IEnrollmentLocationAudRepository extends JpaRepository<LocationAud,AudId> {
	
	@Query("FROM LocationAud la where la.id= :locationID and la.updated = (SELECT MAX(updated) FROM LocationAud WHERE id= :locationID and updated <=:lastDate) ORDER BY updated DESC")
	List<LocationAud> getLocationAudByIdAndDate(@Param("locationID") Integer locationID,
			@Param("lastDate") Date lastDate);
	
	@Query("FROM LocationAud la where la.id= :locationID and la.rev = :rev ORDER BY updated DESC")
	List<LocationAud> getLocationAudByIdAndRev(@Param("locationID") Integer locationID,
			@Param("rev") Integer rev);
	
	
	@Query("FROM LocationAud la where la.id= :locationID and la.revType=0")
	LocationAud getInitialLocationAudById(@Param("locationID") Integer locationID);

}
