package com.getinsured.hix.enrollment.util;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.PropertiesEnumMarker;

/**
 * @author Raja
 * @since 31/07/2013
 * 
 * This class set value for all enrollment constants from gi_app_config table  
 */

@Component
@DependsOn("dynamicPropertiesUtil")
public final class EnrollmentPrivateConfiguration {
   		public enum EnrollmentConfigurationEnum implements PropertiesEnumMarker
		{  
			SOURCE_EXCHANGE_ID ("enrollment.SourceExchangeId"),
			GENDER ("enrollment.Gender"),
			ERRMESSAGE_E000 ("enrollment.errMessage.E-000"),
			ERRMESSAGE_E001 ("enrollment.errMessage.E-001"),
			ERRMESSAGE_E002 ("enrollment.errMessage.E-002"),
			ERRMESSAGE_E003 ("enrollment.errMessage.E-003"),
			ERRMESSAGE_E004 ("enrollment.errMessage.E-004"),
			ERRMESSAGE_E005 ("enrollment.errMessage.E-005"),
			ERRMESSAGE_E006 ("enrollment.errMessage.E-006"),
			ERRMESSAGE_E007 ("enrollment.errMessage.E-007"),
			DISPLAY_PROPERTIES_DISPLAY_CONFIRMATION_DISCLAIMERS ("enrollment.displayProperties.displayConfirmationDisclaimers"),
			DISPLAY_PROPERTIES_DISPLAY_CONFIRMATION_MAKING_CHANGES_TO_YOUR_PLANS ("enrollment.displayProperties.displayConfirmationMakingChangesToYourPlans"),
			DISPLAY_PROPERTIES_DISPLAY_SIGNATURE_PIN ("enrollment.displayProperties.displaySignaturePIN"),
			DISPLAY_PROPERTIES_DISPLAY_FILE_TAX_RETURN ("enrollment.displayProperties.displayFileTaxReturn"),
   			ENROLLMENT_EXT_RESPONSE_URL("enrollment.ext.responseUrl"),
   			ENROLLMENT_ACA_API_KEY("enrollment.aca.apiKey");
			
			private final String value;	  
			
			@Override
			public String getValue(){return this.value;}
			
			EnrollmentConfigurationEnum(String value){
		        this.value = value;
		    }
		}
		
		private static final String STATE_CODE_CA = "CA";
		private static final String STATE_CODE_NM = "NM";
		private static final String STATE_CODE_MS = "MS";
		private static final String STATE_CODE_PHIX = "PHIX";
		
		private static String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
   		
   		public static boolean isNmCall(){
   			boolean isNmCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_NM)){
   				isNmCall = true;
   			}
   			return isNmCall;
   		}

   		public static boolean isCaCall(){
   			boolean isCaCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_CA)){
   				isCaCall = true;
   			}
   			return isCaCall;
   		}
   		
   		public static boolean isMsCall(){
   			boolean isMsCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_MS)){
   				isMsCall = true;
   			}
   			return isMsCall;
   		}
   		
   		public static boolean isPhixCall(){
   			boolean isPhixCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_PHIX)){
   				isPhixCall = true;
   			}
   			return isPhixCall;
   		}
   		
   		public static void getStateCode(){
   			STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
   		}
}