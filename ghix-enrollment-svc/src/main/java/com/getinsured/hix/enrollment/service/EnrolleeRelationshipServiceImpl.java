/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipRepository;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.enrollment.service.EnrolleeRelationshipService;
/**
 * @author panda_p
 *
 */



@Service("enrolleeRelationshipService")
public class EnrolleeRelationshipServiceImpl implements EnrolleeRelationshipService {
	
	@Autowired private IEnrolleeRelationshipRepository enrolleeRelationshipRepository;
	@Override
	public EnrolleeRelationship findBySourceEnrolleeAndTargetEnrollee(Enrollee sourceEnrollee, Enrollee targetEnrollee) {
		if (sourceEnrollee != null && targetEnrollee != null) {
			
			return enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(sourceEnrollee.getId(),targetEnrollee.getId());
		} else {
			
			return null;
		}
	}
	@Override
	public List<EnrolleeRelationship> getEnrolleeRelationshipDataBySourceEnrollee(Integer sourceEnrolleeId) {
		return enrolleeRelationshipRepository.getEnrolleeRelationshipDataBySourceEnrollee(sourceEnrolleeId);
	}

}
