package com.getinsured.hix.enrollment.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.email.ReconciliationEmailNotification;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("reconciliationEmailNotificationService")
public class ReconciliationEmailNotificationServiceImpl implements ReconciliationEmailNotificationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReconciliationEmailNotificationServiceImpl.class);
	
	@Autowired 
	private ReconciliationEmailNotification reconciliationEmailNotification;

	@Override
	public void sendReconAlertEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("ReconciliationEmailNotificationService :: Reconciliation alert email sending process");
		
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_ALERT_EMAIL_ADDRESS);
			
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("ReconciliationEmailNotificationService :: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("ReconciliationEmailNotificationService :: Received null or empty RequestMap");
			}
			
			emailData.put("recipient", recipientEmailAddress);
			emailData.put("Subject",requestMap.get("Subject"));
			reconciliationEmailNotification.setEmailData(emailData);
			reconciliationEmailNotification.setRequestData(requestMap);
			Notice noticeObj = reconciliationEmailNotification.generateEmail();
			Notification notificationObj = reconciliationEmailNotification.generateNotification(noticeObj);
			LOGGER.info("Notice body :: "+noticeObj.getEmailBody());
			reconciliationEmailNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex){
			
			throw new GIException(ex);
		}

	}

}
