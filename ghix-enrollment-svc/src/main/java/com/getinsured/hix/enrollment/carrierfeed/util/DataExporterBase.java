package com.getinsured.hix.enrollment.carrierfeed.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by jabelardo on 7/10/14.
 */
public abstract class DataExporterBase<T> implements DataExporter<T> {

    private FileWriter fileWriter;

    private Map<FileType, FileWriter<T>> fileWriterCreators = new HashMap<>();

    public DataExporterBase(String dataFilename, FileType fileType, String mappingsFilename, String templateFilename) throws IOException {
        registerFileWriterCreators();
        initializeFileWriter(dataFilename, fileType, mappingsFilename, templateFilename);
    }

    public DataExporterBase(String outputDateFilename, FileType fileType, String mappingsFilename) throws IOException {
        this(outputDateFilename, fileType, mappingsFilename, null);
    }

    protected void register(FileType fileType, FileWriter<T> fileWriter) {
        fileWriterCreators.put(fileType, fileWriter);
    }

    protected abstract void registerFileWriterCreators();

    private void initializeFileWriter(String dataFilename, FileType fileType, String mappingsFilename, String templateFilename) throws IOException {
        fileWriter = fileWriterCreators.get(fileType);
        if (fileWriter == null) {
            throw new RuntimeException(String.format("%s fileWriter for fileType %s is not registered",
                    this.getClass().getCanonicalName(), fileType));
        }
        fileWriter.initialize(dataFilename, mappingsFilename, templateFilename);
    }

    @Override
    public void export(Iterator<T> iterator) throws IOException {
        fileWriter.export(iterator);
    }

    @Override
    public void exportAll(List<T> list) throws IOException {
        fileWriter.exportAll(list);
    }

}
