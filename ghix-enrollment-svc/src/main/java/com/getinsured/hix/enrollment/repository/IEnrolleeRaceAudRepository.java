/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.enrollment.EnrolleeRaceAud;

public interface IEnrolleeRaceAudRepository extends JpaRepository<EnrolleeRaceAud, AudId> {
	
	@Query(" SELECT rev FROM EnrolleeRaceAud as en " +
		   " WHERE en.enrollee.id = :enrolleeID and en.rev = :rev")
	Integer getEnrolleeRaceAudByEnrolleeIdAndRev(@Param("enrolleeID") Integer enrolleeID,
			@Param("rev") int rev);
	
	@Query(" FROM EnrolleeRaceAud as en " +
		   " WHERE en.enrollee.id = :enrolleeID and en.rev = :rev")
	List<EnrolleeRaceAud> getRaceAudByEnrolleeIdAndRev(@Param("enrolleeID") Integer enrolleeID,
			@Param("rev") int rev);
	
	@Query(" SELECT rev FROM EnrolleeRaceAud as en " +
		   " WHERE en.enrollee.id = :enrolleeID and en.createdOn = (SELECT MAX(createdOn) FROM EnrolleeRaceAud " +
		                                                          " WHERE enrollee.id = :enrolleeID and createdOn <=:lastDate)")
	Integer findLastRaceRevisionByDate(@Param("enrolleeID") Integer enrolleeID,
			@Param("lastDate") Date lastDate);
}
