package com.getinsured.hix.enrollment.service;

import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.enrollment.Enrollment;

public interface EnrollmentFeedService {
	
	List<Enrollment> findEnrollmentByCarrierAppId(String carrierAppId, String issuerName,Long tenantId);
	
	List<Enrollment> findEnrollmentByCarrierUID(String carrierAppUID, String issuerName,Long tenantId);
	
	List<Enrollment> findEnrollmentByissuerPolicyId(String policyId, String issuerName,Long tenantId);
	
	List<Enrollment> findEnrollmentByEnrolleeAndEffectiveDateQuery(String issuerName, String firstName, String lastName, String reqDateRangeFrom, String reqDateRangeTo,Long tenantId);
	
	List<Enrollment> getEnrollmentsForFinalExceptionCheck(String issuerName, int feedSummaryId,String missingEnrollmentException,Long tenantId);
}
