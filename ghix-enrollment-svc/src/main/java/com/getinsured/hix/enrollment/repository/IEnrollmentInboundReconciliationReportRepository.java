package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentInboundReconciliationReport;

public interface IEnrollmentInboundReconciliationReportRepository extends JpaRepository<EnrollmentInboundReconciliationReport, Integer>{

	@Query("FROM EnrollmentInboundReconciliationReport WHERE hiosIssuerId = :hiosIssuerId AND isa13 = :isa13 AND isa10 = :isa10")
	List<EnrollmentInboundReconciliationReport> findReconRcordForTA1(@Param("hiosIssuerId")String hiosIssuerId,@Param("isa13") Integer isa13,@Param("isa10") String isa10);
	
	@Query("FROM EnrollmentInboundReconciliationReport WHERE hiosIssuerId = :hiosIssuerId AND isa13 = :isa13 AND gs04= :gs04 AND gs05= :gs05 AND gs06= :gs06 AND st02= :st02")
	List<EnrollmentInboundReconciliationReport> findReconRecordForIk5(@Param("hiosIssuerId")String hiosIssuerId, @Param("isa13")Integer isa13,@Param("gs04") String gs04,@Param("gs05") String gs05,@Param("gs06") Integer gs06,@Param("st02") String st02);
	
	@Query("FROM EnrollmentInboundReconciliationReport WHERE hiosIssuerId = :hiosIssuerId AND isa13 = :isa13 AND gs04= :gs04 AND gs05= :gs05 AND gs06= :gs06")
	List<EnrollmentInboundReconciliationReport> findReconRecordForAk9(@Param("hiosIssuerId")String hiosIssuerId,@Param("isa13") Integer isa13,@Param("gs04") String gs04,@Param("gs05") String gs05,@Param("gs06") Integer gs06);

}
