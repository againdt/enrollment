package com.getinsured.hix.enrollment.carrierfeed.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.lookup.service.LookupService;

@Component
public class BOBCarrierFeedQueryUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BOBCarrierFeedQueryUtil.class);
	
	private static final DecimalFormat decimalFormat = new DecimalFormat("#.00");
	
	@Autowired private LookupService lookupService;
	
	//Common Clauses
	String selectClause = "SELECT en FROM Enrollment AS en ";
	String joinQueryClasue = " LEFT OUTER JOIN en.insuranceTypeLkp AS insuranceTypeLkp";
	String tenantClause = " en.tenantId = :tenantId ";
	 

	//--CarrierAppIds / findEnrollmentByCarrierAppId
	public String getEnrollmentByCarrierAppIdQuery(String carrierAppId, String issuerName){
		
		String enrollmentQuery = null;
		StringBuilder queryBuilder = new StringBuilder();
		
		//select * from enrollment e, lookup_value val where e.INSURANCE_TYPE_LKP = val.LOOKUP_VALUE_ID --AND val.LOOKUP_VALUE_CODE = 'STM'
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		//need to confirm with enrollment team
		String statusQueryClause = "en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')";
		 
		enrollmentQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
							.append(" AND ").append("en.carrierAppId = '"+carrierAppId+"'")
							.append(" AND ").append(statusQueryClause)
							.append(" AND ").append(tenantClause)
							.toString();
		 
		LOGGER.debug("FindEnrollmentByCarrierAppId QUERY: "+enrollmentQuery);
		
		return enrollmentQuery;
	}
	
	//--CarrierAppUID / findEnrollmentByCarrierUID
	public String getEnrollmentByCarrierUIDQuery(String carrierAppUID, String issuerName){
		
		String enrollmentQuery = null;
		StringBuilder queryBuilder = new StringBuilder();
		
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String statusQueryClause = "en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')";
		
		 
		enrollmentQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
							.append(" AND ").append("en.carrierAppUID = '"+carrierAppUID+"'")
							.append(" AND ").append(statusQueryClause)
							.append(" AND ").append(tenantClause)
							.toString();
		 
		LOGGER.debug("FindEnrollmentByCarrierUID QUERY: "+enrollmentQuery);
		
		return enrollmentQuery;
	}
	
	//--PolicyID / findEnrollmentByissuerPolicyId
	public String getEnrollmentByissuerPolicyIdQuery(String policyId, String issuerName){
		
		String enrollmentQuery = null;
		StringBuilder queryBuilder = new StringBuilder();
		
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String policyNoByExchangeTypeClause = "LTRIM(en.issuerAssignPolicyNo, '0') = '"+policyId+"'"; 
		String statusQueryClause = "en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')";
		
		 
			enrollmentQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append(policyNoByExchangeTypeClause)
								.append(" AND ").append(statusQueryClause)
								.append(" AND ").append(tenantClause)
								.toString();
		LOGGER.info("FindEnrollmentByissuerPolicyId QUERY: "+enrollmentQuery);
		
		return enrollmentQuery;
	}
	
	public String getEnrollmentByEnrolleeAndEffectiveDateQuery(String issuerName, String firstName, String lastName, String reqDateRangeFrom, String reqDateRangeTo ){
		String statusStr = "PENDING,VERIFIED,DECLINED,WITHDRAWN,TERM,INFORCE";
		String[] strings = statusStr.split(",");
		List<String> statuslist = Arrays.asList(strings);
		
		List<String> personTypeList = new ArrayList<String>();
		personTypeList.add("SUBSCRIBER");
		
		List<LookupValue> personTypeLookupList = lookupService.getLookupValueListForBOBFeed("PERSON_TYPE", personTypeList);
		List<LookupValue> statusLookupList = lookupService.getLookupValueListForBOBFeed("ENROLLMENT_STATUS", statuslist);
		
		StringBuffer enrollmentStatusLkp = new StringBuffer();
		
		for(int i = 0;i<statusLookupList.size();i++){
			LookupValue lookupValue = statusLookupList.get(i);
			enrollmentStatusLkp.append(lookupValue.getLookupValueId());
			
			if(i < (statusLookupList.size() -1)){
				enrollmentStatusLkp.append(",");
			}
		}
		
		StringBuilder enrollmentQuery = new StringBuilder();
			enrollmentQuery.append("SELECT en FROM Enrollment AS en, Enrollee AS el where " );
			
			if (!(StringUtils.isEmpty(reqDateRangeFrom) || StringUtils.isEmpty(reqDateRangeTo))){
				enrollmentQuery.append(" to_date(to_char(en.benefitEffectiveDate, 'YYYY-MM-DD'), 'YYYY-MM-DD') between to_date('"+reqDateRangeFrom+"', 'YYYY-MM-DD') and to_date('"+reqDateRangeTo+"', 'YYYY-MM-DD')  AND ");
			}
			enrollmentQuery.append(" en.enrollmentStatusLkp.lookupValueId in ("+ enrollmentStatusLkp.toString() +")"  );
			enrollmentQuery.append(" AND en.id = el.enrollment.id "  );
			enrollmentQuery.append(" AND lower(el.firstName) = '"+ firstName.replaceAll("'", "''") +"' AND lower(el.lastName) = '"+ lastName.replaceAll("'", "''") +"'" );
			enrollmentQuery.append(" AND lower(en.insurerName) like lower('%"+issuerName+"%') "  );
			enrollmentQuery.append(" AND el.personTypeLkp.lookupValueId = "+ personTypeLookupList.get(0).getLookupValueId());
			enrollmentQuery.append(" AND ").append(tenantClause);
		
		LOGGER.info("getEnrollmentByEnrolleeAndEffectiveDateQuery QUERY: "+enrollmentQuery.toString());
		
		return enrollmentQuery.toString();
	}
	
	public String getEnrollmentsForFinalExceptionCheck(String issuerName,int feedSummaryId,String missingEnrollmentException){
		
		
		String enrollmentQuery = null;
		StringBuilder queryBuilder = new StringBuilder();
		
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String feedSummaryQueryClause = "cfd.carrierFeedSummaryId = " + feedSummaryId + " AND cfd.enrollmentId is not null ";
		String statusQueryClause = "en.enrollmentStatusLkp.lookupValueCode IN ('TERM','INFORCE')";
		String carrierQueryClause = "SELECT  cfd.enrollmentId FROM CarrierFeedDetails cfd ";
		
		String feedExceptionQueryClause = "SELECT fpe.enrollmentId FROM FeedPolicyException fpe WHERE fpe.reason = '"+missingEnrollmentException+"' and fpe.enrollmentId is not null "
				+ " and fpe.tenantId = :fpeTenantId ";

		enrollmentQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
							.append(" AND ").append(statusQueryClause)
							.append(" AND ").append(tenantClause)
							.append(" AND en.id not in (").append(carrierQueryClause)
							.append(" WHERE ").append(feedSummaryQueryClause).append(" )")
							.append(" AND en.id not in (").append(feedExceptionQueryClause).append(" )")
							.toString();
		 
		LOGGER.info("getEnrollmentsForFinalExceptionCheck QUERY: "+enrollmentQuery);
		
		return enrollmentQuery;
		
	}
	
	//--Unique Enrollments
	public Set<Enrollment> getUniqueEnrollments(List<Enrollment> enrollments) throws Exception{
		List<Enrollment> possibleEnrollments = new ArrayList<Enrollment>();
		Set<Enrollment> uniqEnrollments = new HashSet<Enrollment>(possibleEnrollments);
		
		if(uniqEnrollments == null || uniqEnrollments.isEmpty()){
			throw new Exception("Set is Empty");
		}
		else{
			LOGGER.info("Set Size: "+uniqEnrollments.size());
		}
		return uniqEnrollments;
	}
	
	

	public static Float getFormattedFloatValue(Float value)
    {
		if (value != null)
     	{
			return Float.parseFloat(decimalFormat.format(value));
     	}
		return 0.00f;
    }

	public static String getFormattedFloatValue(String value)
    {
        if (value != null)
        {
        	return decimalFormat.format(Float.parseFloat(value));
        }
        return "0.00";
    }
	
}

