package com.getinsured.hix.enrollment.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

public class ResourceResolver  implements LSResourceResolver {
	
	private static final Logger LOGGER = Logger.getLogger(ResourceResolver.class);

	private String schemaBasePath;
	
	/**
	 * 
	 * @param schemaBasePath
	 */
	public ResourceResolver(String schemaBasePath) {
        this.schemaBasePath = schemaBasePath;
     }
	
	/**
	 * 
	 */
	public LSInput resolveResource(String type, String namespaceURI,
			String publicId, String systemId, String baseURI) {
		if(StringUtils.isNotEmpty(systemId) && systemId.startsWith("../")){
			systemId = systemId.substring(3, systemId.length());
		}
		systemId = schemaBasePath + systemId;
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream resourceAsStream = classLoader.getResourceAsStream(systemId);
		if(null == resourceAsStream) {
			try {
				resourceAsStream =  new FileInputStream(new File(systemId));
			} catch (FileNotFoundException e) {
				LOGGER.error("File not found ::" + systemId);
			}
		}
		return new DocumentInput(publicId, systemId, resourceAsStream);
	}
}