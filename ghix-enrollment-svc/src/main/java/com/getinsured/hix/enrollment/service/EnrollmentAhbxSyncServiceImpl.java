package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrolleeDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPremiumDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAhbxSyncRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync.RecordType;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.google.gson.Gson;

@Service("enrollmentahbxsyncservice")
@Transactional
public class EnrollmentAhbxSyncServiceImpl implements EnrollmentAhbxSyncService {
	
	private static final Logger LOGGER = Logger.getLogger(EnrollmentAhbxSyncServiceImpl.class);
	
	@Autowired private IEnrollmentAhbxSyncRepository enrollmentahbxsyncrepository;
	@Autowired private IEnrollmentEventRepository enrollmentEventRepository;
	@Autowired private IEnrolleeRepository enrolleeRepository;
	@Autowired private IEnrollmentPremiumRepository enrollmentPremiumRepository;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IEnrolleeRelationshipRepository enrolleeRelationshipRepository;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private UserService userService;
	@Autowired private GIWSPayloadService giWSPayloadService;
	
	private Gson enrollmentGson = EnrollmentUtils.enrollmentGson();

	@Override
	@Async
	public void saveEnrollmentAhbxSync(List<Enrollment> enrollmentList, String houseHoldCaseID, RecordType recordType) {
		LOGGER.info("Start saveEnrollmentAhbxSync for "+recordType.toString() +" flow at : "+ new Date());
		for (Enrollment enrollment : enrollmentList) {
			
			EnrollmentAhbxSync enrlAhbxSync = new EnrollmentAhbxSync();
			
			enrlAhbxSync.setHouseHoldCaseId(enrollment.getHouseHoldCaseId());
			enrlAhbxSync.setExternalHouseHoldCaseId(enrollment.getExternalHouseHoldCaseId());
			enrlAhbxSync.setEnrollmentId(enrollment.getId());
			EnrollmentDataDTO enrollmentDataDto = null;
			
			enrollmentDataDto = new EnrollmentDataDTO();
			enrollmentDataDto.setEnrollmentId(enrollment.getId());
			enrollmentDataDto.setPlanId(enrollment.getPlanId());
			enrollmentDataDto.setHouseHoldCaseId(enrollment.getHouseHoldCaseId());
			enrollmentDataDto.setEnrollmentBenefitEffectiveStartDate(enrollment.getBenefitEffectiveDate());
			enrollmentDataDto.setEnrollmentBenefitEffectiveEndDate(enrollment.getBenefitEndDate());
			enrollmentDataDto.setGrossPremiumAmt(enrollment.getGrossPremiumAmt());
			enrollmentDataDto.setNetPremiumAmt(enrollment.getNetPremiumAmt());
			enrollmentDataDto.setEnrollmentStatus((enrollment.getEnrollmentStatusLkp() != null ) ? enrollment.getEnrollmentStatusLkp().getLookupValueLabel() : null);
			enrollmentDataDto.setCreationTimestamp(enrollment.getCreatedOn());
			enrollmentDataDto.setPlanType(enrollment.getInsuranceTypeLkp().getLookupValueLabel());
			enrollmentDataDto.setPlanLevel(enrollment.getPlanLevel());
			enrollmentDataDto.setPlanName(enrollment.getPlanName());
			enrollmentDataDto.setCarrierName(enrollment.getInsurerName());
			enrollmentDataDto.setCarrierId(enrollment.getIssuerId());
			enrollmentDataDto.setAptcAmount(enrollment.getAptcAmt());
			enrollmentDataDto.setRenewalFlag(enrollment.getRenewalFlag());
			enrollmentDataDto.setPriorEnrollmentId(enrollment.getPriorEnrollmentId());
			enrollmentDataDto.setCsrLevel(enrollment.getCsrLevel());
			enrollmentDataDto.setLastUpdateDate(enrollment.getUpdatedOn());
			enrollmentDataDto.setLastUpdatedBy((enrollment.getUpdatedBy() != null) ? enrollment.getUpdatedBy().getId() : null );
			enrollmentDataDto.setSubmittedToCarrierDate(enrollment.getSubmittedToCarrierDate());
			enrollmentDataDto.setAptcEffectuveDate(enrollment.getAptcEffDate());
			enrollmentDataDto.setStateSubsidyAmount(enrollment.getStateSubsidyAmt());
			enrollmentDataDto.setStateSubsidyEffectiveDate(enrollment.getStateSubsidyEffDate());
			
			Integer lastCreatedById = (enrollment.getCreatedBy() != null) ? enrollment.getCreatedBy().getId() : null;
			enrollmentDataDto.setCreatedBy(lastCreatedById);
			
			enrollmentDataDto.setCsrAmount(enrollment.getCsrAmt());
			enrollmentDataDto.setTransactionId(enrollment.getPaymentTxnId());
			enrollmentDataDto.setSsapApplicationId(enrollment.getSsapApplicationid());
			enrollmentDataDto.setPriorSsapApplicationId(enrollment.getPriorSsapApplicationid());
			
			if(null != enrollment.getSsapApplicationid()) {
				String ssapcaseNumber = enrollmentRepository.getSsapCaseNumber(enrollment.getSsapApplicationid());
				enrollmentDataDto.setSsapCaseNumber(ssapcaseNumber);
			}
			
			enrollmentDataDto.setCMSPlanID(enrollment.getCMSPlanID());
			enrollmentDataDto.setIssuerId((enrollment.getIssuerId() != null ) ? enrollment.getIssuerId().toString() : null);
			enrollmentDataDto.setEhbSlcspAmt(enrollment.getSlcspAmt());
			enrollmentDataDto.setEsEhbPrmAmt(enrollment.getEhbPercent());
			enrollmentDataDto.setDntlEhbAmt(enrollment.getDntlEhbAmt());
			enrollmentDataDto.setLastUpdatedUserName((enrollment.getUpdatedBy() != null) ?  enrollment.getUpdatedBy().getUserName() : null );
			enrollmentDataDto.setLastUpdatedRoleName((enrollment.getUpdatedBy() != null) ? enrollment.getUpdatedBy().getCurrentUserRole() : null );
			
			enrollmentDataDto.setSubmittedByUserName(enrollment.getCreatedBy() != null ? enrollment.getCreatedBy().getUserName() : null);
			
			AccountUser submittedByUser = userService.findById(lastCreatedById);
			
			if(null != submittedByUser){
				Role defauleSubmittedByUserRole = userService.getDefaultRole(submittedByUser);
				if(null != defauleSubmittedByUserRole && null != defauleSubmittedByUserRole.getName()){
					enrollmentDataDto.setSubmittedByRoleName(defauleSubmittedByUserRole.getName());
					enrollmentDataDto.setUserRole(defauleSubmittedByUserRole.getName());
				}
			}
			
			enrollmentDataDto.setEnrollmentConfirmationDate(enrollment.getEnrollmentConfirmationDate());
			enrollmentDataDto.setEncEnrollmentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(enrollment.getId())));
			enrollmentDataDto.setEhbPercentage(enrollment.getEhbPercent());
			
			enrollmentDataDto.setEnrolleeDataDtoList(getEnrolleeDataDtoList(enrollment, enrollmentDataDto));
			enrollmentDataDto.setEnrollmentPremiumDtoList(getEnrollmentPremiumDtoList(enrollment));
			
			enrollmentDataDto.setPremiumEffDate(getPremiumEffDate(enrollment));
			enrollmentDataDto.setCsrMultiplier(enrollment.getCsrMultiplier());
			enrollmentDataDto.setSubmittedByUserId(0);
			enrollmentDataDto.setAgentName(enrollment.getAgentBrokerName());
			enrollmentDataDto.setAgentId(enrollment.getAssisterBrokerId());
			enrollmentDataDto.setAgentTpaNumber(enrollment.getBrokerTPAAccountNumber1());		
	
			enrollmentDataDto.setAssisterBrokerId(enrollment.getAssisterBrokerId());
			enrollmentDataDto.setCsrAmt(enrollment.getCsrAmt());
			enrollmentDataDto.setPolicyId(enrollment.getId());
			enrollmentDataDto.setEnrollmentType(enrollment.getEnrollmentTypeLkp().getLookupValueCode());
			enrollmentDataDto.setRenewalFlag(enrollment.getRenewalFlag());
			enrollmentDataDto.setLogGlobalId(enrollment.getLogGlobalId());
			enrollmentDataDto.setFinancialEffectiveDate(enrollment.getFinancialEffectiveDate());
			enrollmentDataDto.setExternalHouseHoldCaseId(enrollment.getExternalHouseHoldCaseId());
			
			if (null != enrollment.getPriorSsapApplicationid()) {
				List<GIWSPayload> payloadList = giWSPayloadService.findByEndpointFunctionAndSSAPID("ERP-TRANSFERACCOUNT", enrollment.getPriorSsapApplicationid());
				if (null != payloadList && !payloadList.isEmpty()) {
					enrollmentDataDto.setCorrelationId(payloadList.get(0).getCorrelationId());
				}
			}
			
			Map<String, EnrollmentDataDTO> enrollmentDataDtomap = new HashMap<String, EnrollmentDataDTO>();
			enrollmentDataDtomap.put(enrollmentList.get(0).getExternalHouseHoldCaseId(), (enrollmentDataDto));

			enrlAhbxSync.setEnrollmentJson(enrollmentGson.toJson(enrollmentDataDtomap));

			enrlAhbxSync.setRecord_type(recordType.toString());
			enrlAhbxSync.setCreatedOn(new Date());
			
			AccountUser user = null;
			try {
				user = userService.getLoggedInUser();
			} catch (InvalidUserException invalidUserException) {
				LOGGER.error("Error occured in EnrollmentServiceImpl.getLoggedInUser" , invalidUserException);
			}
			
			enrlAhbxSync.setCreatedBy(user);	
			
			Integer eventId = enrollmentEventRepository.findMaxSubscriberEventIdForEnrollment(enrollment.getId());
			LOGGER.info(" EnrollmentAhbxSync eventId : "+ eventId);
			enrlAhbxSync.setEventId(eventId);
			
			LOGGER.info(" Saving EnrollmentAhbxSync to Database for houseHoldCaseID : "+ enrollment.getHouseHoldCaseId() +" and recordType : "+recordType);
			enrollmentahbxsyncrepository.saveAndFlush(enrlAhbxSync);
	}
	}

	private List<EnrollmentPremiumDTO> getEnrollmentPremiumDtoList(Enrollment enrollment) {
		List<EnrollmentPremiumDTO> enrollmentPremiumDtoList = new ArrayList<>();
		EnrollmentPremiumDTO enrollmentPremiumDTO = null;
		
		List<EnrollmentPremium> enrollmentPremiumList = enrollmentPremiumRepository.findByEnrollmentId(enrollment.getId());
		
		for (EnrollmentPremium enrollmentPremium : enrollmentPremiumList) {
			
			if(enrollmentPremium.getGrossPremiumAmount() != null){
				
				enrollmentPremiumDTO = new EnrollmentPremiumDTO();
				
				enrollmentPremiumDTO.setMonth(enrollmentPremium.getMonth());
				enrollmentPremiumDTO.setYear(enrollmentPremium.getYear());
				enrollmentPremiumDTO.setGrossPremiumAmount(enrollmentPremium.getGrossPremiumAmount());
				enrollmentPremiumDTO.setAptcAmount(enrollmentPremium.getAptcAmount());
				enrollmentPremiumDTO.setNetPremiumAmount(enrollmentPremium.getNetPremiumAmount());
				enrollmentPremiumDTO.setSlcspPremiumAmount(enrollmentPremium.getSlcspPremiumAmount());
				enrollmentPremiumDTO.setMaxAptc(enrollmentPremium.getMaxAptc());
				enrollmentPremiumDTO.setMaxStateSubsidy(enrollmentPremium.getMaxStateSubsidy());
				enrollmentPremiumDTO.setStateSubsidyAmount(enrollmentPremium.getStateSubsidyAmount());
				
				if(enrollmentPremium.getAptcAmount() == null) {
					enrollmentPremiumDTO.setFinancial(false); 
				}else {
					enrollmentPremiumDTO.setFinancial(true); 
				}
				
				enrollmentPremiumDtoList.add(enrollmentPremiumDTO);
			}
		}

		return enrollmentPremiumDtoList;
	}

	private List<EnrolleeDataDTO> getEnrolleeDataDtoList(Enrollment enrollment, EnrollmentDataDTO enrollmentDataDto) {
		
		List<EnrolleeDataDTO> enrolleeDataDtoList = new ArrayList<>();
		EnrolleeDataDTO enrolleeDataDto = null;
		int enrolleecount = 0;
		
		List <Enrollee> enrolleeList = enrolleeRepository.findEnrolleesByEnrollmentID(enrollment.getId());
		
		Enrollee subscriberEnrollee = enrolleeRepository.findSubscriberByEnrollmentID(enrollment.getId());
		Integer subscriberId = 0;
		
		if(subscriberEnrollee != null) {
			subscriberId = subscriberEnrollee.getId();
			LOGGER.info(" EnrollmentAhbxSync subscriberId : "+ subscriberId);
		}

		for (Enrollee enrollee : enrolleeList) {
			
			if(enrollee.getPersonTypeLkp()!=null && 
					(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) 
				    || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) )){
				
				enrolleeDataDto = new EnrolleeDataDTO();
				enrolleecount++;
				
				enrolleeDataDto.setEnrollmentId(enrollment.getId());
				
				enrolleeDataDto.setSuffix(enrollee.getSuffix());
				enrolleeDataDto.setFirstName(enrollee.getFirstName());
				enrolleeDataDto.setMiddleName(enrollee.getMiddleName());
				enrolleeDataDto.setLastName(enrollee.getLastName());
				
				enrolleeDataDto.setEnrolleeEffectiveStartDate(enrollee.getEffectiveStartDate());
				enrolleeDataDto.setEnrolleeEffectiveEndDate(enrollee.getEffectiveEndDate());			
				
				if (isNotNullAndEmpty(enrollee.getEffectiveStartDate())) {
					enrolleeDataDto.setEffectiveStartDate(
							DateUtil.dateToString(enrollee.getEffectiveStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				} if (isNotNullAndEmpty(enrollee.getEffectiveStartDate())) {
					enrolleeDataDto.setEffectiveEndDate(
							DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
					
				enrolleeDataDto.setCoveragePolicyNumber(enrollee.getHealthCoveragePolicyNo());	
				
				EnrolleeRelationship  relationShipToSuscriber = enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrollee.getId(), subscriberId);

				if(relationShipToSuscriber != null && relationShipToSuscriber.getRelationshipLkp() != null){
					enrolleeDataDto.setRelationshipToSubscriber(relationShipToSuscriber.getRelationshipLkp().getLookupValueLabel());
				}
				
				if(enrollee.getHomeAddressid() != null) {
					enrolleeDataDto.setEnrolleeAddress1(enrollee.getHomeAddressid().getAddress1());
					enrolleeDataDto.setEnrolleeAddress2(enrollee.getHomeAddressid().getAddress2());
					enrolleeDataDto.setEnrolleeCity(enrollee.getHomeAddressid().getCity());
					enrolleeDataDto.setEnrolleeState(enrollee.getHomeAddressid().getState());
					enrolleeDataDto.setEnrolleeZipcode(enrollee.getHomeAddressid().getZip());
					enrolleeDataDto.setEnrolleeCountyCode(enrollee.getHomeAddressid().getCountycode());
				}
				
				enrolleeDataDto.setPhoneNumber(enrollee.getPrimaryPhoneNo());
				enrolleeDataDto.setEmailAddress(enrollee.getPreferredEmail());
				enrolleeDataDto.setMemberId(enrollee.getExchgIndivIdentifier());
				enrolleeDataDto.setQuotingDate(enrollee.getQuotingDate());
				
				enrolleeDataDto.setBirthDate(enrollee.getBirthDate());
				
				if(enrollee.getGenderLkp() != null) {
					enrolleeDataDto.setGenderLookupCode(enrollee.getGenderLkp().getLookupValueCode());
					enrolleeDataDto.setGenderLookupLabel(enrollee.getGenderLkp().getLookupValueLabel());
				}

				enrolleeDataDto.setTaxIdentificationNumber(enrollee.getTaxIdNumber());
				enrolleeDataDto.setEnrolleeId(enrollee.getId());
				enrolleeDataDto.setSubscriberFlag(null);
				
				String enrolleeStstus = (enrollee.getEnrolleeLkpValue() != null) ? enrollee.getEnrolleeLkpValue().getLookupValueCode() : null;
				enrolleeDataDto.setEnrolleeStatus(enrolleeStstus);
				
				//If Status is TERM / CANCEL
				if(isNotNullAndEmpty(enrolleeStstus)) {
				if(enrolleeStstus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) ||
						enrolleeStstus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) ) {
					
					List<EnrollmentEvent> enrollmentEventList = enrollmentEventRepository.getCancelTermEventByEnrolleeId(enrollee.getId());
					
					EnrollmentEvent termEnrollmentEvent = null;
					
					if(enrollmentEventList != null && !enrollmentEventList.isEmpty() ) {
						termEnrollmentEvent = enrollmentEventList.get(0);
					}
					
					if(termEnrollmentEvent != null) {
						enrolleeDataDto.setTerminationDate(enrollee.getEffectiveEndDate());
						enrolleeDataDto.setTerminationReasonCode(termEnrollmentEvent.getEventReasonLkp().getLookupValueCode());
						enrolleeDataDto.setTerminationReasonLabel(termEnrollmentEvent.getEventReasonLkp().getLookupValueLabel());
					}
				}
				}
				
				enrolleeDataDto.setTobaccoStatus((enrollee.getTobaccoUsageLkp() != null ) ? enrollee.getTobaccoUsageLkp().getLookupValueCode() : null);
				
				if(enrollee.getMailingAddressId() != null) {
					enrolleeDataDto.setMailingAddress1(enrollee.getMailingAddressId().getAddress1());
					enrolleeDataDto.setMailingAddress2(enrollee.getMailingAddressId().getAddress2());
					enrolleeDataDto.setMailingCity(enrollee.getMailingAddressId().getCity());
					enrolleeDataDto.setMailingState(enrollee.getMailingAddressId().getState());
					enrolleeDataDto.setMailingZipcode(enrollee.getMailingAddressId().getZip());
					enrolleeDataDto.setMailingCounty(enrollee.getMailingAddressId().getCounty());
					enrolleeDataDto.setMailingCountyCode(enrollee.getMailingAddressId().getCountycode());
				}
				
				enrolleeDataDto.setRatingArea(enrollee.getRatingArea());
				enrolleeDataDto.setRatingAreaEffectiveDate(enrollee.getRatingAreaEffDate());
				
				enrolleeDataDto.setRelationshipToHouseHoldContact((enrollee.getRelationshipToHCPLkp() != null) ? enrollee.getRelationshipToHCPLkp().getLookupValueLabel() : null);
				enrolleeDataDto.setAge(enrollee.getAge());
				
				enrolleeDataDto.setRelationshipToSubscriberCode((enrollee.getRelationshipToSubscriberLkp() != null) ? enrollee.getRelationshipToSubscriberLkp().getLookupValueCode() : null);
				enrolleeDataDto.setTobaccoStatusCode((enrollee.getTobaccoUsageLkp() != null) ? enrollee.getTobaccoUsageLkp().getLookupValueCode() : null );
				
				EnrollmentEvent lastEvent = enrollee.getLastEventId();
				enrolleeDataDto.setEventType(lastEvent.getEventTypeLkp().getLookupValueCode());
				enrolleeDataDto.setEventReason(lastEvent.getEventReasonLkp().getLookupValueCode());
				enrolleeDataDto.setPersonType(null);
				
				enrolleeDataDto.setTotalIndvResponsibleAmt(enrollee.getTotalIndvResponsibilityAmt());
				enrolleeDataDto.setPersonType(enrollee.getPersonTypeLkp().getLookupValueCode());
				enrolleeDataDto.setLastPremiumPaidDate(enrollee.getLastPremiumPaidDate());
				
				enrolleeDataDtoList.add(enrolleeDataDto);
				
				if(enrollee.getPersonTypeLkp() != null && 
						(   EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())
						|| EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueLabel())) ) {
					
					enrollmentDataDto.setRatingArea(EnrollmentUtils.getFormatedRatingArea((String) enrollee.getRatingArea()));
					enrollmentDataDto.setRatingAreaEffectiveDate(enrollee.getRatingAreaEffDate());
					
					if(enrollee.getHomeAddressid() != null) {
						enrollmentDataDto.setHomeAddress1(enrollee.getHomeAddressid().getAddress1());
						enrollmentDataDto.setHomeAddress2(enrollee.getHomeAddressid().getAddress2());
						enrollmentDataDto.setHomeCity(enrollee.getHomeAddressid().getCity());
						enrollmentDataDto.setHomeState(enrollee.getHomeAddressid().getState());
						enrollmentDataDto.setHomeZipcode(enrollee.getHomeAddressid().getZip());
						enrollmentDataDto.setHomeCountyCode(enrollee.getHomeAddressid().getCounty());
						enrollmentDataDto.setHomeCountyCode(enrollee.getHomeAddressid().getCountycode());
					}
					
					if(enrollee.getMailingAddressId() != null) {
						enrollmentDataDto.setMailingAddress1(enrollee.getMailingAddressId().getAddress1());
						enrollmentDataDto.setMailingAddress2(enrollee.getMailingAddressId().getAddress2());
						enrollmentDataDto.setMailingCity(enrollee.getMailingAddressId().getCity());
						enrollmentDataDto.setMailingState(enrollee.getMailingAddressId().getState());
						enrollmentDataDto.setMailingZipcode(enrollee.getMailingAddressId().getZip());
						enrollmentDataDto.setMailingCounty(enrollee.getMailingAddressId().getCounty() );
						enrollmentDataDto.setMailingCountyCode(enrollee.getMailingAddressId().getCountycode());
					}
					
					enrollmentDataDto.setEventType(lastEvent.getEventTypeLkp().getLookupValueCode());
					enrollmentDataDto.setEventReason(lastEvent.getEventReasonLkp().getLookupValueCode());
					enrollmentDataDto.setEventCreationDate(lastEvent.getCreatedOn());
					enrollmentDataDto.setEventCreatedBy(lastEvent.getCreatedBy() != null ? lastEvent.getCreatedBy().getFirstName()+" "+lastEvent.getCreatedBy().getLastName(): null);
				}
			}
		}
		
		enrollmentDataDto.setNumberOfEnrollees(enrolleecount);
		return enrolleeDataDtoList;
	}
	
	private String getPremiumEffDate(Enrollment enrollment) {
		
		Date grossPremEffDate = enrollment.getGrossPremEffDate();
		Date netPremEffDate = enrollment.getNetPremEffDate();
		Date aptcEffDate = enrollment.getAptcEffDate();
		Date maxPremEffDate =null;
		
				if(grossPremEffDate!=null){
					maxPremEffDate = grossPremEffDate;
					if(netPremEffDate!=null && netPremEffDate.after(grossPremEffDate)){
						maxPremEffDate = netPremEffDate;
					}
					if(aptcEffDate!=null && aptcEffDate.after(maxPremEffDate)){
						maxPremEffDate = aptcEffDate;
					}
				}else if(netPremEffDate!=null){
					maxPremEffDate = netPremEffDate;
					if(aptcEffDate!=null && aptcEffDate.after(netPremEffDate)){
						maxPremEffDate = aptcEffDate;
					}
				}
				
				if(maxPremEffDate !=null){
					return DateUtil.dateToString( maxPremEffDate, GhixConstants.REQUIRED_DATE_FORMAT);
				}
		
		return null;
	}
}