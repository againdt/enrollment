package com.getinsured.hix.enrollment.email;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component
@Scope("prototype")
public class EnrollmentIRSOutboundFailureEmailNotification extends EnrollmentNotificationAgent {
	
}
