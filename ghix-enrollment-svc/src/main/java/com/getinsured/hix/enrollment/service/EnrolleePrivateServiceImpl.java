/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.SendUpdatedEnrolleeResponseDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipPrivateRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleePrivateRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeUpdateSendStatusPrivateRepository;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.EnrolleeUpdateSendStatus;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author panda_p
 *
 */

@Service("enrolleePrivateService")
@Transactional
public class EnrolleePrivateServiceImpl implements EnrolleePrivateService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrolleePrivateServiceImpl.class);
	@Autowired private IEnrolleeUpdateSendStatusPrivateRepository enrolleeUpdateSendStatusRepository;
	@Autowired private IEnrolleeRelationshipPrivateRepository enrolleeRelationshipRepository;
	@Autowired private IEnrolleePrivateRepository enrolleeRepository;
	//@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private UserService userService;
	@Autowired private LookupService lookupService;

	//@Value("#{configProp['enrollment.sendCarrierUpdatedDataUrl']}")
	//private String sendCarrierUpdatedDataUrl;

	/**
	 * @Since 26-Jul-2013
	 * @author panda_pratap
	 * @return Logged In AccountUser
	 */
//	private AccountUser getLoggedInUser(){
//		AccountUser accountUser =null;
//		try {
//			 accountUser = userService.getLoggedInUser();
//		} catch (InvalidUserException invalidUserException) {
//			LOGGER.error("Error occured in EnrolleeServiceImpl.getLoggedInUser : "+ invalidUserException);
//		}
//		return accountUser;
//	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Enrollee> getCarrierUpdatedEnrollments() {
		Date lastRunDate=getLastRunDate(EnrollmentPrivateConstants.SEND_CARRIER_UPDATED_ENROLLMENT_JOB);
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
		LOGGER.info("userService: "+user);
		LOGGER.info("lastRunDate: "+lastRunDate);
		if(lastRunDate!=null){
			return enrolleeRepository.getCarrierUpdatedEnrollments(lastRunDate, user.getId());
		}else{
			return enrolleeRepository.getCarrierUpdatedEnrollments(user.getId());
		}

	}

	/**
	 * @author panda_p
	 * 
	 * @param jobName
	 * @return lastRunDate
	 */
	private Date getLastRunDate(String jobName){
		Date lastRunDate= null;
		if (jobName != null) {
			lastRunDate = batchJobExecutionService.getLastJobRunDate(jobName);
		}
		return lastRunDate;
	}

	/**
	 * 
	 */
	@Override	
	@Transactional
	public void saveCarrierUpdatedDataResponse(List<SendUpdatedEnrolleeResponseDTO> inputVal){

		LOGGER.info(" inside saveCarrierUpdatedDataResponse  : inputVal =  " +inputVal);
		for(SendUpdatedEnrolleeResponseDTO sendUpdatedEnrolleeResponseDTO: inputVal){
			//SendUpdatedEnrolleeResponseDTO sendUpdatedEnrolleeResponseDTO  = (SendUpdatedEnrolleeResponseDTO) objPlan;
			EnrolleeUpdateSendStatus enrolleeUpdateSendStatus = new EnrolleeUpdateSendStatus();

			enrolleeUpdateSendStatus.setAhbxStatusCode(sendUpdatedEnrolleeResponseDTO.getAhbxStatusCode());
			enrolleeUpdateSendStatus.setAhbxStatusDesc(sendUpdatedEnrolleeResponseDTO.getAhbxStatusDescription());
			enrolleeUpdateSendStatus.setExchgIndivIdentifier(sendUpdatedEnrolleeResponseDTO.getExchgIndivIdentifier());
			enrolleeUpdateSendStatus.setEnrolleeStatus(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, sendUpdatedEnrolleeResponseDTO.getEnrolleeStatus()));
			if(sendUpdatedEnrolleeResponseDTO.getEnrolleeId()!=null){
				Enrollee en = new Enrollee();
				en.setId(Integer.parseInt(sendUpdatedEnrolleeResponseDTO.getEnrolleeId()));
				enrolleeUpdateSendStatus.setEnrollee(en);
			}
			try{
				enrolleeUpdateSendStatusRepository.save(enrolleeUpdateSendStatus);
			}catch(Exception e){
				LOGGER.error("Exception occured while saving the response for IND21 from AHBX ");
				LOGGER.error("Exception occured while saving the response for IND21 from AHBX : ", e );
			}
		}
	}

	/**
	 * @author panda_p
	 * @param enrollmentID
	 * @return
	 */
	@Override
	public List<Enrollee> getSubscriberAndEnrollees(Integer enrollmentID){
		return enrolleeRepository.getEnrolleeByEnrollmentID(enrollmentID);
	}
	
	@Override	
	@Transactional(readOnly=true)
	public Enrollee findSubscriberbyEnrollmentIDId(Integer enrollmentID) {	
		
			return enrolleeRepository.findSubscriberByEnrollmentID(enrollmentID);
	}
	
	@Override	
	@Transactional(readOnly=true)
	public Integer findSubscriberIdByEnrollmentId(Integer enrollmentID) {	
		return enrolleeRepository.findSubscriberIdByEnrollmentId(enrollmentID);
	}
	

	@Override	
	@Transactional(readOnly=true)
	public Enrollee findById(Integer id) {		
//		return enrolleeRepository.findOne(id);
		return enrolleeRepository.findById(id); //Tenant Aware Implementation	
	}
	
	@Override
	@Transactional(readOnly=true)
	public Map<String, Object> searchEnrollee(Map<String, Object> searchCriteria) {
		int pageNumber = (searchCriteria.get(EnrollmentPrivateConstants.PAGE_NUMBER)!=null)?Integer.parseInt(searchCriteria.get(EnrollmentPrivateConstants.PAGE_NUMBER).toString()):EnrollmentPrivateConstants.DEFAULT_PAGE_NUMBER;
		int pageSize = (searchCriteria.get(EnrollmentPrivateConstants.PAGE_SIZE)!=null)?Integer.parseInt(searchCriteria.get(EnrollmentPrivateConstants.PAGE_SIZE).toString()):GhixConstants.PAGE_SIZE;
		Sort sortObject = null;
		Pageable pageable = null;

		String sortBy = (searchCriteria.get(EnrollmentPrivateConstants.SORT_BY)!=null)?searchCriteria.get(EnrollmentPrivateConstants.SORT_BY).toString():EnrollmentPrivateConstants.DEFAULT_SORT_COLUMN;
		String sortOrder = (searchCriteria.get(EnrollmentPrivateConstants.SORT_ORDER)!=null)?searchCriteria.get(EnrollmentPrivateConstants.SORT_ORDER).toString(): EnrollmentPrivateConstants.DEFAULT_SORT_ORDER;

		Sort.Direction direction = null;
		if(sortOrder.equalsIgnoreCase(EnrollmentPrivateConstants.DEFAULT_SORT_ORDER)){
			direction = Sort.Direction.ASC;
		}else{
			direction = Sort.Direction.DESC;
		}

		sortObject = new Sort(new Sort.Order(direction, sortBy));

		if (pageNumber < 1) {
			pageNumber = 1;
		}

		pageable = new PageRequest(pageNumber - 1, pageSize, sortObject);

		String policynumber = null;
		String enrollmentStatusLkp = null;
		String plantype = null;
		String plannumber = null;
		int issuer = 0;
		String name = null;
		String enrollmentTypeLkp = null;
		String employerName = null;

		if(isNotNullAndEmpty(searchCriteria.get("policynumber"))){
			policynumber = (String)searchCriteria.get("policynumber");
		}
		if(isNotNullAndEmpty(searchCriteria.get(EnrollmentPrivateConstants.STATUS))){
			enrollmentStatusLkp = (String)searchCriteria.get(EnrollmentPrivateConstants.STATUS);
		}
		if(isNotNullAndEmpty(searchCriteria.get("plantype"))){
			plantype = (String)searchCriteria.get("plantype");
		}
		if(isNotNullAndEmpty(searchCriteria.get("plannumber"))){
			plannumber = (String)searchCriteria.get("plannumber");
		}
		if(isNotNullAndEmpty(searchCriteria.get("issuer"))){
			issuer = Integer.parseInt(searchCriteria.get("issuer").toString());
		}
		if(isNotNullAndEmpty(searchCriteria.get("name"))){
			name = (String)searchCriteria.get("name");
		}
		if(isNotNullAndEmpty(searchCriteria.get("lookup"))){ 
			enrollmentTypeLkp = (String)searchCriteria.get("lookup");			
		}
		if((EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP).equals(enrollmentTypeLkp) && isNotNullAndEmpty(searchCriteria.get("employerName"))){ 
			employerName = (String)searchCriteria.get("employerName");			
		}

		Page<Object[]> data = null;
		List<Map<String, Object>> dataset = null;

		if((EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP).equals(enrollmentTypeLkp)){
			data = enrolleeRepository.getShopEnrolleeByIssuerData(policynumber,enrollmentStatusLkp, plantype,plannumber,issuer,employerName,name, enrollmentTypeLkp,pageable);
			dataset = getShopDataSet(data);
		}

		if((EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL).equals(enrollmentTypeLkp)){
			data = enrolleeRepository.getIndividualEnrolleeByIssuerData(policynumber,enrollmentStatusLkp, plantype,plannumber,issuer,name, enrollmentTypeLkp,pageable);
			dataset = getIndividualDataSet(data);
		}


		Map<String,Object> enroleesAndRecordCount = new HashMap<String,Object>();
		enroleesAndRecordCount.put("enroleelist", dataset);
		if(null != data){
			enroleesAndRecordCount.put("recordCount", data.getTotalElements());
		}
		return enroleesAndRecordCount;
	}

	private List<Map<String, Object>> getShopDataSet(Page<Object[]> data) {
		List<Map<String, Object>> dataset = new ArrayList<Map<String, Object>>();
		
		List<String> selectColumns = getShopColumnList();
		
		String columnName = null;
		for (Object[] tuple : data) {
			Map<String, Object> temp = new HashMap<String, Object>();
			
			for (int i = 0; i < selectColumns.size(); i++) {
				columnName = selectColumns.get(i);
				
				if (columnName.contains(".")) {
					columnName = columnName.replace(".", "");
				}
				
				temp.put(columnName, tuple[i]);
			}
			dataset.add(temp);
		}
		return dataset;
	}
	
	private List<Map<String, Object>> getIndividualDataSet(Page<Object[]> data) {
		List<Map<String, Object>> dataset = new ArrayList<Map<String, Object>>();
		
		List<String> selectColumns = getIndividualColumnList();
		
		String columnName = null;
		for (Object[] tuple : data) {
			Map<String, Object> temp = new HashMap<String, Object>();
			
			for (int i = 0; i < selectColumns.size(); i++) {
				columnName = selectColumns.get(i);
				
				if (columnName.contains(".")) {
					columnName = columnName.replace(".", "");
				}
				
				temp.put(columnName, tuple[i]);
			}
			dataset.add(temp);
		}
		return dataset;
	}

	private List<String> getShopColumnList() {
		List<String> selectColumns = new ArrayList<String>();
		selectColumns.add(EnrollmentPrivateConstants.ID);
		selectColumns.add(EnrollmentPrivateConstants.HEALTH_COVERAGE_POLICY_NO);
		selectColumns.add("enrollment.enrollmentStatusLkp.lookupValueCode");
		selectColumns.add("enrollment.enrollmentStatusLkp.lookupValueLabel");
		selectColumns.add("enrollment.insuranceTypeLkp.lookupValueCode");
		selectColumns.add("enrollment.enrollmentTypeLkp.lookupValueCode");
		selectColumns.add("enrollment.planName");
		selectColumns.add("enrollment.CMSPlanID");
		selectColumns.add("enrollment.insurerName");
		selectColumns.add(EnrollmentPrivateConstants.FIRST_NAME);
		selectColumns.add(EnrollmentPrivateConstants.MIDDLE_NAME);
		selectColumns.add(EnrollmentPrivateConstants.LAST_NAME);	        
		selectColumns.add(EnrollmentPrivateConstants.EFFECTIVE_START_DATE);
		selectColumns.add("enrollment.employer.id");
		selectColumns.add("enrollment.employer.name");
		selectColumns.add("enrollment.benefitEffectiveDate");
		selectColumns.add("enrollment.benefitEndDate");		
		return selectColumns;
	}
	
	private List<String> getIndividualColumnList() {
		List<String> selectColumns = new ArrayList<String>();
		selectColumns.add(EnrollmentPrivateConstants.ID);
		selectColumns.add(EnrollmentPrivateConstants.HEALTH_COVERAGE_POLICY_NO);
		selectColumns.add("enrollment.enrollmentStatusLkp.lookupValueCode");
		selectColumns.add("enrollment.enrollmentStatusLkp.lookupValueLabel");
		selectColumns.add("enrollment.insuranceTypeLkp.lookupValueCode");
		selectColumns.add("enrollment.enrollmentTypeLkp.lookupValueCode");
		selectColumns.add("enrollment.planName");
		selectColumns.add("enrollment.CMSPlanID");
		selectColumns.add("enrollment.insurerName");
		selectColumns.add(EnrollmentPrivateConstants.FIRST_NAME);
		selectColumns.add(EnrollmentPrivateConstants.MIDDLE_NAME);
		selectColumns.add(EnrollmentPrivateConstants.LAST_NAME);	        
		selectColumns.add(EnrollmentPrivateConstants.EFFECTIVE_START_DATE);
		selectColumns.add("enrollment.benefitEffectiveDate");
		selectColumns.add("enrollment.benefitEndDate");		
		return selectColumns;
	}


	/**
	 * @since
	 * @author parhi_s
	 * 
	 * This method is used to find Enrollee by passing parameters memberID, enrollmentID, planID
	 */
	@Override
	public List<Enrollee> findEnrolleeByEnrollmentIDAndMemberIDAndPlanID(Integer enrollmentID,
			Integer planID, String memberID) throws GIException  {
		List<Enrollee> enrolleeList= new ArrayList<Enrollee>();
		try{
			enrolleeList=enrolleeRepository.findEnrolleeByEnrollmentIDAndMemberIDAndPlanID(memberID, enrollmentID, planID);
		}
		catch(Exception e){
			LOGGER.error("Error in findEnrolleeByOrderIDAndMemberIDAndPlanID() == "+e.getMessage());
			LOGGER.error("Error in findEnrolleeByOrderIDAndMemberIDAndPlanID : "+ e);
			throw new GIException("Error in findEnrolleeByOrderIDAndMemberIDAndPlanID() == "+e.getMessage(),e);
		}
		return enrolleeList;
	}
	/**
	 * @since
	 * @author parhi_s
	 * This method is used to Update Enrollee
	 */

	@Override
	public void updateEnrollee(Enrollee enrollee) throws GIException {
		try{
			enrolleeRepository.saveAndFlush(enrollee);
		}catch(Exception e){
			LOGGER.error("Error in updateEnrollee() == "+e.getMessage());
			LOGGER.error("Exception in updateEnrollee : "+ e);
			throw new GIException("Error in updateEnrollee() == "+e.getMessage(),e);
		}
	}

	/**
	 * @since
	 * @author parhi_s
	 * 
	 * This method is used to find Enrollee By passing parameters memberID and enrollmentID
	 */

	@Override
	public Enrollee findEnrolleeByEnrollmentIDAndMemberID(
			String memberID, Integer enrollmentID) throws GIException{
		try{
			return enrolleeRepository.findEnrolleeByEnrollmentIDAndMemberID(memberID, enrollmentID);
		}
		catch(Exception e){
			LOGGER.error("Error in findEnrolleeByEnrollmentIDAndMemberID() == "+e.getMessage());
			LOGGER.error("Exception in findEnrolleeByEnrollmentIDAndMemberID : "+ e);
			throw new GIException("Error in findEnrolleeByEnrollmentIDAndMemberID() == "+e.getMessage(),e);
		}

	}

	/**
	 * @author panda_p
	 * 
	 * @param exchgIndivIdentifier
	 * @return
	 */
	@Override
	public Enrollee getEnrolleeByExchgIndivIdentifier(String exchgIndivIdentifier){
		return enrolleeRepository.getEnrolleeByExchgIndivIdentifier(exchgIndivIdentifier);
	}

	/* (non-Javadoc)
	 * @author parhi_s
	 * @param enrollmentID
	 * @param statusID
	 * @see com.getinsured.hix.enrollment.service.EnrolleeService#getEnrolleeByStatusAndEnrollmentID(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Enrollee> getEnrolleeByStatusAndEnrollmentID(Integer enrollmentID, String status) throws GIException {
		try{
			return enrolleeRepository.getEnrolleeByStatusAndEnrollmentID(enrollmentID, status);
		}catch(Exception e){
			LOGGER.error("Error in  getEnrolleeByStatusAndEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception getEnrolleeByStatusAndEnrollmentID : "+ e);
			throw new GIException("Error in  getEnrolleeByStatusAndEnrollmentID() == "+e.getMessage(),e);
		}

	}

	/* (non-Javadoc)
	 * @author priya
	 * @param enrollmentID
	 */
	@Override
	public List<Enrollee> getEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException {

		try{
			return enrolleeRepository.getEnrolleeByEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error("Error in  getEnrolleeByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  getEnrolleeByEnrollmentID : "+ e);
			throw new GIException("Error in  getEnrolleeByEnrollmentID() == "+e.getMessage(),e);
		}

	}
	
	@Override
	public List<Enrollee> getEnrolledEnrolleesForEnrollmentID(Integer enrollmentID)throws GIException {
		try{
			return enrolleeRepository.getEnrolledEnrolleesForEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error("Error in  getEnrolleeByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  getEnrolleeByEnrollmentID : "+ e);
			throw new GIException("Error in  getEnrolleeByEnrollmentID() == "+e.getMessage(),e);
		}

	}
	
	/* (non-Javadoc)
	 * @author parhi_s
	 * @param enrollmentID
	 */
	@Override
	public List<Enrollee> findEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException {
		try{
			return enrolleeRepository.findEnrolleesByEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error("Error in  findEnrolleeByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  findEnrolleeByEnrollmentID :"+ e);
			throw new GIException("Error in  findEnrolleeByEnrollmentID() == "+e.getMessage(),e);
		}

	}

	/* (non-Javadoc)
	 * @author priya
	 * @param enrollmentID
	 */
	@Override
	public Enrollee getHouseholdContactByEnrollmentID(Integer enrollmentID)throws GIException{
		try{
			return enrolleeRepository.getHouseholdContactByEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error("Error in  getHouseholdContactByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  getHouseholdContactByEnrollmentID : "+ e);
			throw new GIException("Error in  getHouseholdContactByEnrollmentID() == "+e.getMessage(),e);
		}

	}

	/* (non-Javadoc)
	 * @author priya
	 * @param enrollmentID
	 */
	@Override
	public Enrollee getResponsiblePersonByEnrollmentID(Integer enrollmentID)throws GIException{
		try{
			return enrolleeRepository.getResponsiblePersonByEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error("Error in  getResponsiblePersonByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  getResponsiblePersonByEnrollmentID : "+ e);
			throw new GIException("Error in  getResponsiblePersonByEnrollmentID() == "+e.getMessage(),e);
		}

	}
	@Override
	public List<Enrollment> findActiveEnrollmentByEmployeeIDandMemberID(Integer employeeId, String memberID) throws GIException {
		return enrolleeRepository.findActiveEnrollmentByEmployeeIDandMemberID(employeeId,memberID);
	}
	
	@Override
	public List<Enrollment> getEnrollmentEmployeeIdAndEnrollmentStatus(Integer employeeId, List<String> enrollmentStatus) throws GIException {
	
	 return enrolleeRepository.getEnrollmentEmployeeIdAndEnrollmentStatus(employeeId, enrollmentStatus);
	}
	
	@Override
	public EnrolleeRelationship getRelationshipBySourceEndTargetId(Integer enrolleeId,Integer subscriberId){
		return enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrolleeId, subscriberId);
	}

	@Override
	public Boolean isEnrolleeNotHouseHoldAndCustodialAndResponsible(Enrollee enrolleeObj) {
		Boolean isEnrollee = Boolean.FALSE;
		if(!(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT)) &&
		   !(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT)) &&
		   !(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON))){
			
			isEnrollee = Boolean.TRUE;
		}
		return isEnrollee;
	}

	@Override
	public Boolean isEnrolleeCustodialParent(Enrollee enrolleeObj) {
		Boolean isEnrollee = Boolean.FALSE;
		if(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT)){
			isEnrollee = Boolean.TRUE;
		}
		return isEnrollee;
	}

	@Override
	public Boolean isEnrolleeResponsiblePerson(Enrollee enrolleeObj) {
		Boolean isEnrollee = Boolean.FALSE;
		if(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON)){
			isEnrollee = Boolean.TRUE;
		}
		return isEnrollee;
	}

	@Override
	public Boolean isEnrolleeHouseHoldContact(Enrollee enrolleeObj) {
		Boolean isEnrollee = Boolean.FALSE;
		if(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT)){
			isEnrollee = Boolean.TRUE;
		}
		return isEnrollee;
	}

    @Override
    public Enrollee findEnrolleeByEnrollmentIdAndPersonTypeLkp_LookupValueCode(Integer enrollmentId, String lookupValueCode) {
        return enrolleeRepository.findEnrolleeByEnrollmentIdAndPersonTypeLkp_LookupValueCode(enrollmentId, lookupValueCode);
    }

}
