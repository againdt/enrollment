/**
 *
 */
package com.getinsured.hix.enrollment.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.EnrollmentCommission;

/**
 * @author Priya C
 *
 */
@Repository
@Transactional
public interface IEnrollmentCommissionRepository extends JpaRepository<EnrollmentCommission, Integer>{

	@Query(" SELECT e FROM EnrollmentCommission e " +
		   " WHERE e.enrollmentId = :enrollmentId order by commissionDate desc, premiumMonth desc")
	List<EnrollmentCommission> findByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);

	@Query("select case when count(e.id) > 0 then true else false end" +
			" from EnrollmentCommission e" +
			" where e.enrollmentId = :enrollmentId and e.commissionDate between :begDate and :endDate")
	boolean existsByEnrollmentIdAndCommissionDateBetween(@Param("enrollmentId") Integer enrollmentId, @Param("begDate") Date begDate, @Param("endDate") Date endDate);

	@Query("select coalesce(sum(e.commissionAmt), 0) from EnrollmentCommission e where e.enrollmentId = :enrollmentId and e.commissionDate <= :commissionDate and (e.commissionDate != :commissionDate or e.premiumMonth <= :premiumMonth ) ")
	Number sumCommissionAmtByEnrollmentIdAndCommissionDateLT(@Param("enrollmentId") Integer enrollmentId, @Param("commissionDate") Date commissionDate, @Param("premiumMonth") Date premiumMonth);

	@Query(" SELECT e FROM EnrollmentCommission e " +
			" WHERE e.enrollmentId = :enrollmentId and e.commissionDate >= :commissionDate and (e.commissionDate != :commissionDate or e.premiumMonth > :premiumMonth ) order by e.commissionDate, e.premiumMonth, e.id")
	List<EnrollmentCommission> findByEnrollmentIdAndCommissionDateGE(@Param("enrollmentId") Integer enrollmentId, @Param("commissionDate") Date commissionDate, @Param("premiumMonth") Date premiumMonth);
	
	@Query("select coalesce(sum(e.commissionAmt), 0) from EnrollmentCommission e where e.enrollmentId = :enrollmentId and e.commissionDate <= :commissionDate and (e.commissionDate != :commissionDate or e.premiumMonth <= :premiumMonth and ( e.premiumMonth != :premiumMonth or e.id < :id ) )  ")
	Number sumCommissionAmtByEnrollmentIdAndCommissionDateLT(@Param("enrollmentId") Integer enrollmentId, @Param("commissionDate") Date commissionDate, @Param("premiumMonth") Date premiumMonth, @Param("id") Integer id);

	@Query(" SELECT e FROM EnrollmentCommission e " +
			" WHERE e.enrollmentId = :enrollmentId and e.commissionDate >= :commissionDate and (e.commissionDate != :commissionDate or e.premiumMonth >= :premiumMonth and ( e.premiumMonth != :premiumMonth or e.id >= :id ) ) order by e.commissionDate, e.premiumMonth, e.id")
	List<EnrollmentCommission> findByEnrollmentIdAndCommissionDateGE(@Param("enrollmentId") Integer enrollmentId, @Param("commissionDate") Date commissionDate, @Param("premiumMonth") Date premiumMonth, @Param("id") Integer id);
}
