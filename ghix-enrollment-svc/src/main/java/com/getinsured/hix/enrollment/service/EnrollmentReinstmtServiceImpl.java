package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerEnrollment;
import com.getinsured.hix.model.EmployerEnrollment.Status;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementRequest;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementResponse;
import com.getinsured.timeshift.util.TSDate;
import com.thoughtworks.xstream.XStream;

@Service("enrollmentReinstmtService")
public class EnrollmentReinstmtServiceImpl implements EnrollmentReinstmtService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentReinstmtServiceImpl.class);
	@Autowired	private IEnrolleeRepository enrolleeRepository;
	@Autowired	private IEnrollmentRepository enrollmentRepository;
	@Autowired private LookupService lookupService;
	@Autowired private GhixRestTemplate restTemplate;
	@Autowired private EnrollmentEmailNotificationService enrollmentEmailNotificationService;
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired private EnrollmentRequotingService enrollmentRequotingService;
	@Autowired private EnrollmentAhbxSyncService enrollmentAhbxSyncService;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentCreationService enrollmentCreationService;

	public static final String  HIGH_ERROR_SEVERITY="HIGH";
	public static final String  MEMBER_MSG="The Enrollee with member ID = ";
	public static final String 	REINST_STATE_MSG=" is not in a Reinstatable state.";
	@Transactional
	@Override
	public EnrollmentReinstatementResponse processReinstatementRequest(EnrollmentReinstatementRequest enrollmentReinstatementRequest, boolean isInternal,  AccountUser user){
		EnrollmentReinstatementResponse enrollmentReinstatementResponse = new EnrollmentReinstatementResponse();
		try{
			LOGGER.info("Internal Call = " + isInternal);
			List<Enrollment> enrollmentList = null;
			if(isNotNullAndEmpty(enrollmentReinstatementRequest.getEmployeeAppIdList()) && !enrollmentReinstatementRequest.getEmployeeAppIdList().isEmpty()){
				enrollmentList = findAndValidateShopEnrollments(enrollmentReinstatementRequest.getEmployeeAppIdList());
			}else if(isNotNullAndEmpty(enrollmentReinstatementRequest.getApplicationId())){
				enrollmentList = findAndValidateIndividualEnrollments(enrollmentReinstatementRequest.getApplicationId());
			}else if(isNotNullAndEmpty(enrollmentReinstatementRequest.getEnrollmentIdList())){
				enrollmentList = new ArrayList<Enrollment>();
				for(Long enrlId : enrollmentReinstatementRequest.getEnrollmentIdList()){
					Enrollment en = enrollmentRepository.findById(enrlId.intValue());
					if(isEnrollmentReinstateble(en)){
						enrollmentList.add(en);
					}else{
						LOGGER.error("ERROR in enrollmentReinstatement processing :: "+EnrollmentConstants.MSG_ENROLLMENT_NOT_IN_REINSTATABLE_STATE);
						enrollmentReinstatementResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_202));
						enrollmentReinstatementResponse.setResponseDescription(EnrollmentConstants.MSG_ENROLLMENT_NOT_IN_REINSTATABLE_STATE);
						return enrollmentReinstatementResponse;
					}
				}
			}else if(isNotNullAndEmpty(enrollmentReinstatementRequest.getEnrollmentId())){
				enrollmentList = new ArrayList<Enrollment>();
				Enrollment en = enrollmentRepository.findById(enrollmentReinstatementRequest.getEnrollmentId().intValue());
					if(isEnrollmentReinstateble(en)){
						enrollmentList.add(en);
					}else{
						LOGGER.error("ERROR in enrollmentReinstatement processing :: "+EnrollmentConstants.MSG_ENROLLMENT_NOT_IN_REINSTATABLE_STATE);
						enrollmentReinstatementResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_202));
						enrollmentReinstatementResponse.setResponseDescription(EnrollmentConstants.MSG_ENROLLMENT_NOT_IN_REINSTATABLE_STATE);
						return enrollmentReinstatementResponse;
					}
			}
			else{
				throw new GIException(EnrollmentConstants.ERROR_CODE_209,EnrollmentConstants.ERR_MSG_SSAP_APP_ID_IS_NULL_OR_EMPTY,HIGH_ERROR_SEVERITY);
			}

			if(enrollmentList != null && !enrollmentList.isEmpty()){
				enrollmentReinstatementResponse = processReisnstate( enrollmentList, enrollmentReinstatementRequest, user);
			}else{
				throw new GIException(EnrollmentConstants.ERROR_CODE_302,"Enrollments "+REINST_STATE_MSG,HIGH_ERROR_SEVERITY);
			}
			
		}catch(GIException e){
			LOGGER.error("ERROR in processReinstatementRequest() :: "+EnrollmentConstants.ERR_MSG_APPLICATION_ERROR+ EnrollmentUtils.getExceptionMessage(e) , e);
			if(e.getErrorCode()>0){
				enrollmentReinstatementResponse.setResponseCode(Integer.toString(e.getErrorCode()));
				enrollmentReinstatementResponse.setResponseDescription(GhixConstants.RESPONSE_FAILURE+": "+e.getErrorMsg());
			}else{
				enrollmentReinstatementResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_299));
				enrollmentReinstatementResponse.setResponseDescription(GhixConstants.RESPONSE_FAILURE+": "+EnrollmentConstants.ERR_MSG_APPLICATION_ERROR);
			}
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		catch(Exception e){
			LOGGER.error("ERROR in processReinstatementRequest() :: "+EnrollmentConstants.ERR_MSG_APPLICATION_ERROR+ e.toString() , e);
			enrollmentReinstatementResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_299));
			enrollmentReinstatementResponse.setResponseDescription(EnrollmentConstants.ERR_MSG_APPLICATION_ERROR);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return enrollmentReinstatementResponse;
	}
	
	/**
	 * @author panda_p
	 * @since 16-Nov-2016
	 * Jira Id  = HIX-91210
	 * @param enrollmentList
	 * @param enrollmentReinstatementRequest
	 * @param user
	 * @return
	 * @throws GIException
	 */
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	private EnrollmentReinstatementResponse processReisnstate(List<Enrollment> enrollmentList, EnrollmentReinstatementRequest enrollmentReinstatementRequest, AccountUser user) throws GIException{
		List<Enrollment> updatedEnrollments= new ArrayList<Enrollment>();
		EnrollmentReinstatementResponse enrollmentReinstatementResponse = new EnrollmentReinstatementResponse();
		for(Enrollment enrollment: enrollmentList){
			List<Enrollee> enrolleeList = null;
			if(EnrollmentConstants.ENROLLMENT_TYPE_SHOP.equalsIgnoreCase(enrollment.getEnrollmentTypeLkp().getLookupValueCode())){
				enrolleeList = findMembers(enrollmentReinstatementRequest.getMembers().getMemberId(), enrollment);
			}
			else{
				enrolleeList = findMembers(enrollment);
			}
			
			Date effectiveEndDate =null;  //findEffectiveEndDate(enrollment);
			String newStatus=null;
			String enrollmentType= enrollment.getEnrollmentTypeLkp().getLookupValueCode();
			if(isNotNullAndEmpty(enrollmentType) && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
				effectiveEndDate=EnrollmentUtils.getYearEndDate(enrollment.getBenefitEffectiveDate());
				if(enrollment.getEnrollmentConfirmationDate()!=null){
					newStatus= EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM;
				}else{
					newStatus= EnrollmentConstants.ENROLLMENT_STATUS_PENDING;
				}
			}else if(enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP) && enrollment.getEmployer()!=null){
				//get Employer End date by making shop rest call
				try{
					ShopResponse shopResponse = getEmployerInfo(enrollment.getEmployer().getId(),enrollment.getEmployeeAppId());//enrollmentService.getEmployerInfo(Integer.toString(enrollment.getEmployer().getId())) ;
					if(isNotNullAndEmpty(shopResponse)){
						/*String benefitEndDate = (String)shopResponse.getResponseData().get("coverageEndDate");*/
						/*
						 * Jira Id: HIX-79028
						 * For ReInstatement Instead of Using coverageEndDate, we need to refer groupCoverageEndDate
						 */
						String benefitEndDate = (String)shopResponse.getResponseData().get("groupCoverageEndDate");
						effectiveEndDate = EnrollmentUtils.StringToEODDate(benefitEndDate, GhixConstants.REQUIRED_DATE_FORMAT);
						//effectiveEndDate=DateUtil.StringToDate(benefitEndDate, GhixConstants.REQUIRED_DATE_FORMAT);
						
						
						if(enrollment.getEnrollmentConfirmationDate()!=null){
							newStatus= EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM;
						}else{
							EmployerEnrollment.Status employerEnrollmentStatus = (EmployerEnrollment.Status) shopResponse.getResponseData().get(EnrollmentConstants.ENROLLMENT_STATUS_KEY1);
							if(employerEnrollmentStatus!=null  && (employerEnrollmentStatus.equals(Status.ACTIVE) || employerEnrollmentStatus.equals(Status.TERMINATED) )){
								newStatus=EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED;
							}else{
								newStatus= EnrollmentConstants.ENROLLMENT_STATUS_PENDING;
							}
							
						}
						
					}
					else{
						LOGGER.error("No shopResponse for employer id "+enrollment.getEmployer().getId()+" Employee Application Id"+enrollment.getEmployeeAppId());
						throw new GIException("No shopResponse for employer id "+enrollment.getEmployer().getId()+" Employee Application Id"+enrollment.getEmployeeAppId());
					}
				}catch(Exception e){
					LOGGER.error("Error while making rest call to shop to get Employer details." , e);
					throw new GIException(EnrollmentConstants.ERROR_CODE_304,"Error while making rest call to shop to get Employer details." + e.getMessage(),HIGH_ERROR_SEVERITY);
				}
			}
			LookupValue previousStatus=null;
			if(newStatus!=null){
				previousStatus=lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,newStatus);
			}
			if(enrolleeList!=null && enrolleeList.size()>0){
				//String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				for(Enrollee enrollee: enrolleeList){
					//LookupValue previousStatus=enrolleeAudRepository.getPrevStatusForReInstmt(enrollee.getId());
					updateEnrollee(enrollee,previousStatus, user, effectiveEndDate);
					createEventForEnrollee(enrollee, EnrollmentConstants.EVENT_TYPE_REINSTATEMENT, EnrollmentConstants.EVENT_REASON_REENROLLMENT, user);
					/*//HIX-81670
					if(null != enrollment.getEnrollmentConfirmationDate() && EnrollmentConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode)){
						enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
					}*/
					if(enrollee.getEnrolleeLkpValue()!=null 
							&& !EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode()) 
							&& !EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode())){
						enrollee.setDisenrollTimestamp(null);
					}
				}
				enrollment.setEnrollmentStatusLkp(previousStatus);
				/*if(null != enrollment.getEnrollmentConfirmationDate() && EnrollmentConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode)){
					enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
				}else{
					enrollment.setEnrollmentStatusLkp(enrollmentAudRepository.getPrevStatusForReInstmt(enrollment.getId()));
				}*/
				enrollment.setBenefitEndDate(effectiveEndDate);
				enrollment.setAbortTimestamp(null);
				enrollment.setPremiumPaidToDateEnd(null);
				enrollment.setUpdatedBy(user);
				enrollmentRequotingService.updateMonthlyPremiumForEnrollment(enrollment, true, true, null, true);
				updatedEnrollments.add(enrollment);

				/**
				 * Logging ENROLLMENT_STATUS_UPDATE application event.
				 */
				enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.REINSTATEMENT.toString(), user);
			}else{
				throw new GIException(EnrollmentConstants.ERROR_CODE_303,"No Enrollee Found For Reinstatement",HIGH_ERROR_SEVERITY);
			}
		}//End of for loop
		
		if(updatedEnrollments != null && !updatedEnrollments.isEmpty()){
			if(enrollmentCreationService.validateEnrollmentMemberOverlapping(updatedEnrollments, null)) {
				throw new GIException(EnrollmentConstants.ERROR_CODE_305, "EnrollmentOverlap : One or more member already enrolled with another plan for given period", HIGH_ERROR_SEVERITY);
			}
			updatedEnrollments = enrollmentService.saveAllEnrollment(updatedEnrollments);
			if(EnrollmentConfiguration.isCaCall()) {
				final List<Enrollment> finalEnrollmentList= updatedEnrollments;
				final EnrollmentAhbxSync.RecordType record = EnrollmentAhbxSync.RecordType.IND69_REINSTATE;
				LOGGER.info("Async Call to enrollmentAhbxSyncService saveEnrollmentAhbxSync IND69_REINSTATE");
				TransactionSynchronizationManager
				.registerSynchronization(new TransactionSynchronizationAdapter() {
					public void afterCommit() {
				enrollmentAhbxSyncService.saveEnrollmentAhbxSync(finalEnrollmentList, finalEnrollmentList.get(0).getHouseHoldCaseId(), record);
					}
				});
			}
			

			enrollmentReinstatementResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_200));
			enrollmentReinstatementResponse.setResponseDescription(GhixConstants.RESPONSE_SUCCESS);
			
			EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
			enrollmentResponse = enrollmentService.updateIndividualStatusForReinstatementOrAdd(updatedEnrollments, enrollmentResponse);

			if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && enrollmentResponse.getErrCode() == EnrollmentConstants.ERROR_CODE_204) {
				enrollmentReinstatementResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_204));
				enrollmentReinstatementResponse.setResponseDescription(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERROR_CODE_204, EnrollmentConstants.ERR_MSG_ACT_ON_NEW_APP, HIGH_ERROR_SEVERITY);
			} else if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				throw new GIException(EnrollmentConstants.ERROR_CODE_222, enrollmentResponse.getErrMsg(), HIGH_ERROR_SEVERITY);
			}

		}
		else{
			throw new GIException(EnrollmentConstants.ERROR_CODE_302, "No Enrollment Found For Reinstatement", HIGH_ERROR_SEVERITY);
		}
		
		if(!(EnrollmentConfiguration.isCaCall() || EnrollmentConfiguration.isIdCall())){
			enrollmentEmailNotificationService.sendEnrollmentReinstateNotification(enrollmentList);
		}

		return enrollmentReinstatementResponse;
	}
	/**
	 * @author parhi_s
	 * @param enrollee
	 * @param statustoUpdate
	 * @param user
	 * 
	 * This method is used to update Enrollee
	 */
	private void updateEnrollee(Enrollee enrollee, LookupValue statustoUpdate, AccountUser user, Date effectiveEndDate){
		if(enrollee!=null){
			enrollee.setEnrolleeLkpValue(statustoUpdate);
			if(statustoUpdate.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || statustoUpdate.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
				enrollee.setDisenrollTimestamp(new TSDate());
			}
			enrollee.setEffectiveEndDate(effectiveEndDate);
			enrollee.setDeathDate(null);
			enrollee.setUpdatedBy(user);
		}
	}
	
//	private Date findEffectiveEndDate(Enrollment enrollment){
//		Date effectiveEndDate = null;
//		if(enrollment!=null && enrollment.getEnrollmentTypeLkp()!=null){
//			String enrollmentType= enrollment.getEnrollmentTypeLkp().getLookupValueCode();
//			if(isNotNullAndEmpty(enrollmentType) && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
//				effectiveEndDate=EnrollmentUtils.getYearEndDate(enrollment.getBenefitEffectiveDate());
//			}else if(enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP) && enrollment.getEmployer()!=null){
//				//get Employer End date by making shop rest call
//				try{
//					ShopResponse shopResponse = getEmployerInfo(enrollment.getEmployer().getId(),enrollment.getEmployeeAppId());//enrollmentService.getEmployerInfo(Integer.toString(enrollment.getEmployer().getId())) ;
//					if(isNotNullAndEmpty(shopResponse)){
//						/*String benefitEndDate = (String)shopResponse.getResponseData().get("coverageEndDate");*/
//						/*
//						 * Jira Id: HIX-79028
//						 * For ReInstatement Instead of Using coverageEndDate, we need to refer groupCoverageEndDate
//						 */
//						String benefitEndDate = (String)shopResponse.getResponseData().get("groupCoverageEndDate");
//						effectiveEndDate = EnrollmentUtils.StringToEODDate(benefitEndDate, GhixConstants.REQUIRED_DATE_FORMAT);
//						//effectiveEndDate=DateUtil.StringToDate(benefitEndDate, GhixConstants.REQUIRED_DATE_FORMAT);
//					}
//					else{
//						LOGGER.error("Error in findEffectiveEndDate:: Error while making rest call to shop to get Employer details.");
//					}
//				}catch(Exception e){
//					LOGGER.error("Error in findEffectiveEndDate:: Error while making rest call to shop to get Employer details." , e);
//				}
//			}
//		}
//		return effectiveEndDate;
//	}
	
	/**
	 * @author parhi_s
	 * @since
	 * @param enrollmentId
	 * @return
	 * @throws GIException
	 * 
	 * This method is used to Find and Validate Enrollments based on Employee Application ID List
	 */

	private List<Enrollment> findAndValidateShopEnrollments(List<String> employeeAppIdList) throws GIException{
		List<Enrollment> enrollmentlist = new ArrayList<Enrollment>();
		if(employeeAppIdList != null){
			for(String strEmployeeId : employeeAppIdList){

				if(EnrollmentUtils.isNumeric(strEmployeeId)){
					//employeeAppIdList.add(Long.parseLong(strEmployeeId));
					Long empAppId = Long.parseLong(strEmployeeId);
					Enrollment latestHealthEnrollment = enrollmentRepository.findLatestShopHealthEnrollment(empAppId);
					Enrollment latestDentalEnrollment = enrollmentRepository.findLatestShopDentalEnrollment(empAppId);

					if(latestHealthEnrollment != null ||  latestDentalEnrollment != null){
						if(latestHealthEnrollment != null && validateEnrollment(latestHealthEnrollment)){
							enrollmentlist.add(latestHealthEnrollment);
						}
						if(latestDentalEnrollment!=null && validateEnrollment(latestDentalEnrollment)){
							enrollmentlist.add(latestDentalEnrollment);
						}

					}else{
						throw new GIException(EnrollmentConstants.ERROR_CODE_301,EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_EMP_APP_ID,HIGH_ERROR_SEVERITY);
					}
				}else{
					throw new GIException(EnrollmentConstants.ERROR_CODE_209,EnrollmentConstants.ERR_MSG_EMPLOYEE_APP_ID_IS_NULL_OR_EMPTY,HIGH_ERROR_SEVERITY);
				}
			}
		}
		return enrollmentlist;
	}
	
	/**
	 * 
	 * @author Aditya-S
	 * @since 28/07/2014 
	 * 
	 * HIX-44171
	 * 
	 * This method is used to Find and Validate Enrollments based on SSAP Application ID.
	 * 
	 * @param ssapApplicationidStr
	 * @return
	 * @throws GIException
	 */
	private List<Enrollment> findAndValidateIndividualEnrollments(Long ssapApplicationid) throws GIException{
		List<Enrollment> enrollmentlist = new ArrayList<Enrollment>();
			Enrollment latestHealthEnrollment = enrollmentRepository.findLatestIndividualHealthEnrollment(ssapApplicationid);
			Enrollment latestDentalEnrollment = enrollmentRepository.findLatestIndividualDentalEnrollment(ssapApplicationid);

			if(latestHealthEnrollment != null ||  latestDentalEnrollment != null){
				if(latestHealthEnrollment != null && validateEnrollment(latestHealthEnrollment)){
					enrollmentlist.add(latestHealthEnrollment);
				}
				if(latestDentalEnrollment!=null && validateEnrollment(latestDentalEnrollment)){
					enrollmentlist.add(latestDentalEnrollment);
				}

			}else{
			throw new GIException(EnrollmentConstants.ERROR_CODE_301,
					String.format(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_SSAP_APP_ID, ssapApplicationid),
					HIGH_ERROR_SEVERITY);
			}
		return enrollmentlist;
	}

	private boolean validateEnrollment(Enrollment enrollment) throws GIException{
		boolean isValid=false;
		if(enrollment!=null ){
			if(isNotNullAndEmpty(enrollment.getEnrollmentStatusLkp())&&
					(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) ||
					enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
				isValid= true;
			}else{
				throw new GIException(EnrollmentConstants.ERROR_CODE_302,"Enrollment "+REINST_STATE_MSG,HIGH_ERROR_SEVERITY);
			}
		}
		return isValid;
	}

	private boolean isEnrollmentReinstateble(Enrollment enrollment){
		boolean isValid=false;
		if(enrollment!=null ){
			if(isNotNullAndEmpty(enrollment.getEnrollmentStatusLkp())&&
					(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) ||
					enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
				isValid= true;
			}
		}
		return isValid;
	}
	/**
	 * 
	 * @author parhi_s
	 * @param members
	 * @param enrollmentId
	 * @return
	 * @throws GIException
	 * 
	 * This method is used to validate the members provided for Reinstatement
	 * 
	 */

	private List<Enrollee> findMembers(List<String> members, Enrollment enrollment) throws GIException{
		List<Enrollee> enrolleeList= new ArrayList<Enrollee>();
		for(String memberId: members){
			Enrollee enrollee=null;
			List<Enrollee> enrollees= enrolleeRepository.findEnrolleeByEnrollmentIDAndMemberID(memberId,enrollment.getId());
			if(enrollees!=null && enrollees.size()>0){
				enrollee=enrollees.get(0);
			}
			if(enrollee.getPersonTypeLkp() != null 
					&& (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))
					&& (enrollee.getEffectiveEndDate() ==null || DateUtils.isSameDay(enrollee.getEffectiveEndDate(), enrollment.getBenefitEndDate()))
					){
				enrolleeList.add(enrollee);
			}
		}
		return enrolleeList;
	}
	
	private List<Enrollee> findMembers(Enrollment enrollment)throws GIException{
		//For Id profile loading Subscriber's last termination along with any member added after Subsscriber's termination
		
		/*Integer rev = iEnrollmentEventAudRepository.getLatestSubscriberDisEnrollmentRevision(enrollmentId);
		List<Integer>  enrolleeIdList = iEnrolleeAudRepository.getEnrolleesForReInstateByRev(enrollmentId, rev);
		
		//Throw error if no Enrollee Found by Enrollment Event Revision
		if(enrolleeIdList == null || (enrolleeIdList != null && enrolleeIdList.isEmpty())){
			throw new GIException(EnrollmentConstants.ERROR_CODE_203, "No Enrollee Aud history found for Revision :"+rev , HIGH_ERROR_SEVERITY);
		}
		
		 * Fetch Enrollee added after Subscriber termination
		 
		Integer revAfterSubscriberTermination = iEnrollmentEventAudRepository.getMemberDisenrolledAfterSubscriber(rev, enrollmentId);
		if(revAfterSubscriberTermination != null && revAfterSubscriberTermination != 0){
			List<Integer>  enrolleeAddedAfterTermination = iEnrolleeAudRepository.getEnrolleesForReInstateByRev(enrollmentId, revAfterSubscriberTermination);
			if(enrolleeAddedAfterTermination != null && !enrolleeAddedAfterTermination.isEmpty()){
				enrolleeIdList.addAll(enrolleeAddedAfterTermination);
			}
		}
		
		List<Enrollee> enrolleeList = (List<Enrollee>) iEnrolleeRepository.findAll(enrolleeIdList);
		*/
		
		List<Enrollee> enrolleeList=new ArrayList<>();
		if(enrollment!=null ){
			List<Enrollee> totalEnrollees= enrollment.getEnrollees();
			if(totalEnrollees!=null && totalEnrollees.size()>0){
				for (Enrollee enrollee: totalEnrollees){
					if(enrollee.getPersonTypeLkp() != null 
							&& (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))
							&& (enrollee.getEffectiveEndDate() ==null || DateUtils.isSameDay(enrollee.getEffectiveEndDate(), enrollment.getBenefitEndDate()))
							){
						enrolleeList.add(enrollee);
					}
				}
			}
		}
		
		return enrolleeList;
	}

	/**
	 * 
	 * @since
	 * @author parhi_s
	 * @param enrollee
	 * @param maintReasonCode
	 * @param user
	 * 
	 * This method is Called to create EnrollmentEvent 
	 * 
	 */

	private void createEventForEnrollee(Enrollee enrollee,String eventType ,String eventReasonCode, AccountUser user){
		EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
		enrollmentEvent.setEnrollee(enrollee);
		enrollmentEvent.setEnrollment(enrollee.getEnrollment());
		enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.REINSTATEMENT.toString());
		enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,eventType));
		//add null check
		if(isNotNullAndEmpty(eventReasonCode)){ 
				if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode))){
					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode));
				}else{
					throw new RuntimeException("Lookup value is null");
				}
		}else{
			throw new RuntimeException("Event Reason code  is null or empty");
		}
		if(user!=null){
			enrollmentEvent.setCreatedBy(user);
		}
		enrollee.setLastEventId(enrollmentEvent);
	}
	/**
	 * This method makes a rest call to SHOP to get details of an Employer
	 * 
	 * @param employerId
	 * @param employeeAppId
	 * @return
	 * @throws GIException
	 */
	public ShopResponse getEmployerInfo(int employerId, Long employeeAppId) throws GIException{
		if (isNotNullAndEmpty(employerId)) {
			LOGGER.info("Calling shop service to get employer info for employerId =="+employerId);
			LOGGER.info("shop service URL == "+ShopEndPoints.GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYEE_APP_ID + "?employerId=" + employerId + "&employeeApplicationId=" + employeeAppId);
			String postResponse =null;

			try{
				postResponse = restTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYEE_APP_ID + "?employerId=" + employerId + "&employeeApplicationId=" + employeeAppId, String.class);
				XStream xstream = GhixUtils.getXStreamStaxObject();
				ShopResponse shopResponse  = (ShopResponse) xstream.fromXML(postResponse);

				if(shopResponse != null){
					return shopResponse;
				}else{
					LOGGER.error("Error fetching Employer data for EmployerId :: " + employerId);
					throw new GIException("Error fetching Employer data");
				}
			}catch(RestClientException re){
				LOGGER.error("Error fetching Employer data for EmployerId :: " + employerId);
				LOGGER.error("Message : "+ re.getMessage());
				LOGGER.error("Cause : "+ re.getCause());
				throw new GIException("Error fetching Employer data",re);
			}
			catch (Exception e) {
				LOGGER.error("Error fetching Employer data for EmployerId :: " + employerId);
				LOGGER.error("Message : "+ e.getMessage());
				LOGGER.error("Cause : "+ e.getCause());
				throw new GIException("Error fetching Employer data",e);
			}
		}else{
			throw new GIException("No Employer found");
		}
	}
	
}
