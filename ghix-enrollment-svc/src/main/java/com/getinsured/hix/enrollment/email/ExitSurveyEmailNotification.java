package com.getinsured.hix.enrollment.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;


import com.getinsured.hix.platform.notification.NotificationAgent;
import com.getinsured.hix.platform.util.GhixEndPoints;
@Component
public class ExitSurveyEmailNotification extends NotificationAgent {

	private Map<String, String> singleData;
	private String emailAddress; 
	private String firstName;
	private Map<String, String> requestMap;
	private static final String LINK = "link";
	private static final String LOGIN_POSTFIX = "account/user/login";
	
	@Override
	public Map<String, String> getSingleData() {
		Map<String,String> bean = new HashMap<String, String>();

		bean.put("customerName", getFirstName());
		bean.put("host", GhixEndPoints.GHIXWEB_SERVICE_URL );

		bean.put(LINK,GhixEndPoints.GHIXWEB_SERVICE_URL+LOGIN_POSTFIX);
		if(requestMap != null && !requestMap.isEmpty())
		{
			bean.putAll(requestMap);
		}
		
		setTokens(bean);

		Map<String, String> data = new HashMap<String, String>();
		data.put("To", getEmailAddress());
		
		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
	}
	public void setSingleData(Map<String, String> singleData) {
		this.singleData = singleData;
	}

	public String getEmailAddress(){
		return emailAddress;
	}
	
	public void setEmailAddress(String emailAddress){
		this.emailAddress = emailAddress;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	
	/**
	 * @return the requestMap
	 */
	public Map<String, String> getRequestMap() {
		return requestMap;
	}
	/**
	 * @param requestMap the requestMap to set
	 */
	public void setRequestMap(Map<String, String> requestMap) {
		this.requestMap = requestMap;
	}
}
