package com.getinsured.hix.enrollment.email;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.NotificationAgent;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.util.exception.GIException;

public class EnrollmentNotificationAgent extends NotificationAgent {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentNotificationAgent.class);
	private Map<String, String> enrollmentEmailDetails;
	private Map<String, String> singleData;
	private AccountUser userObj;
	@Autowired private NoticeService noticeService;
	public void setUserObj(AccountUser userObj) {
		this.userObj = userObj;
	}
	public void setEnrollmentEmailDetails(Map<String, String> enrollmentEmailDetails) {
		this.enrollmentEmailDetails = enrollmentEmailDetails;
	}
	
	public void setNotificationData(Map<String, Object> notificationData){
		this.notificationData = notificationData;
	}
	
	public Map<String, Object> getNotificationData(){
		return this.notificationData;
	}
	
	@Override
	public Map<String, String> getSingleData() {
		Map<String, String> data = new HashMap<String, String>();
		Map<String, String> bean =null;
		String isReinstatementEmailSend="true";
		if(null != notificationData && notificationData.get("isReinstatementEmailSend") != null && isReinstatementEmailSend.equals(notificationData.get("isReinstatementEmailSend"))){
			bean = new HashMap<String, String>();
			List<String> toEmailList = (List<String>)notificationData.get("sendToEmailList") ;
			bean.put("name",(String) notificationData.get("employeeName"));
			bean.put("currentDate",(String) notificationData.get("currentDate"));
			bean.put("employeeName",(String) notificationData.get("employeeName"));
			bean.put("addressLine1",(String) notificationData.get("addressLine1"));
			bean.put("addressLine2",(String) notificationData.get("addressLine2"));
			bean.put("city",(String) notificationData.get("city"));
			bean.put("state",(String) notificationData.get("state"));
			bean.put("exchangeName",(String) notificationData.get("exchangeName"));
			bean.put("terminationDate",(String) notificationData.get("terminationDate"));
			bean.put("employerName",(String) notificationData.get("employerName"));
			bean.put("effectiveStartDate",(String) notificationData.get("effectiveStartDate"));
			bean.put("enrolledPlanName",(String) notificationData.get("enrolledPlanName"));
			bean.put("issuerName",(String) notificationData.get("issuerName"));
			bean.put("zipCode",(String) notificationData.get("zipCode"));
			bean.put("exchangePhoneNumber",(String) notificationData.get("exchangePhoneNumber"));
			bean.put("employerPhoneNumber",(String) notificationData.get("employerPhoneNumber"));
			setTokens(bean);
			data.put("To", (String)toEmailList.get(0));
		}
		else if (null != notificationData && notificationData.get("isIRSNACKResponseFailureNotification") != null && "true".equalsIgnoreCase(notificationData.get("isIRSNACKResponseFailureNotification").toString())) {
			data.put("To", notificationData.get("To").toString());
		}
		else if (null != this.enrollmentEmailDetails
				&& !this.enrollmentEmailDetails.isEmpty()
				&& EnrollmentConstants.TRUE.equalsIgnoreCase(this.enrollmentEmailDetails.get("isIRSOutboundFailureNotification"))) {
			setTokens(this.enrollmentEmailDetails);
			data.putAll(this.enrollmentEmailDetails);
		}
		return data;
	}
	
	
	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}
	
	public void sendMail() {
		try {
		
			super.sendEmail(super.generateEmail());
	
		}catch(NotificationTypeNotFound e) {
		LOGGER.error("Notification type \"" + getClass().getSimpleName() + "\" not found.Email can not be sent.",e);
	}
 }
	
public void sendNotification(Boolean isSecureNotice,Boolean isPaperNotice,Map<String,Object> contentMap,GhixNoticeCommunicationMethod ghixNoticeCommRef) throws GIException {
		
		
		/**
		 *	default setting required for secure notice & mail(paper) notice
		 *
		 */
		List<String> toEmailList = (List<String>)contentMap.get("sendToEmailList") ;
		
		
		/**
		 *	if Location is not set in Mail(paper) notices, throw exception
		 *
		 */
		Location location = null;
		if(isPaperNotice){
			try{
				location = (Location)contentMap.get("location");
				
			}catch(Exception e){
				LOGGER.error("Exception occured in sendNotification method of EnrollmentNotificationAgent.java.",e);
				throw new GIException("Location is not set for paper notification");
			}
		}
		
		/**
		 *	override default setting if only mail(paper) notice should be sent and secure inbox notice is not required.
		 *
		 */
		/*if(isPaperNotice && !isSecureNotice){
			toEmailList = null;
		} */
		
			try {
				noticeService.createModuleNotice
				(getClass().getSimpleName(),
						GhixLanguage.US_EN,
						contentMap,
						"notification/"+contentMap.get("ModuleName").toString()+"/"+contentMap.get("ModuleID"),
						contentMap.get("fileName").toString(), 
						contentMap.get("ModuleName").toString(),
						Long.parseLong(contentMap.get("ModuleID").toString()),
						toEmailList,
						contentMap.get("fromFullName").toString(),
						contentMap.get("toFullName").toString(),
						location,
						ghixNoticeCommRef);
			} catch (NoticeServiceException e) {
				LOGGER.error("NoticeServiceException while sending " + getClass().getSimpleName(),e,e.toString());
			}catch (Exception e) {
				LOGGER.error("Exception while sending " + getClass().getSimpleName() + " isPaperNotice = "+isPaperNotice + " isSecureNotice="+isSecureNotice,e,e.toString());
				throw new GIException("Exception while sending " + getClass().getSimpleName(),e);
			}
	}
	
}
