package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrolleeTenantDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentTenantDTO;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentTenantResponse;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * HIX-70722 Enrollment Tenant Service Implementation
 * @author negi_s
 * @since 08/07/2015
 */
@Service("enrollmentTenantService")
@Transactional
public class EnrollmentTenantServiceImpl implements EnrollmentTenantService {

	private static final Logger LOGGER = Logger.getLogger(EnrollmentTenantServiceImpl.class);
	
	private static final String ERR_MSG_205 = "No enrollments found for the given parameters";

	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IEnrollmentAudRepository enrollmentAudRepository;
	
	@Override
	public EnrollmentTenantResponse getEnrollmentTenantResponse(Long leadId, Integer enrollmentId, Long affiliateId) {
		EnrollmentTenantResponse enrollmentTenantResponse = new EnrollmentTenantResponse();
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		Enrollment enrollment = findEnrollmentForTenant(leadId, enrollmentId, affiliateId);
		enrollmentTenantResponse.setLeadId(leadId.toString());
		if(null != enrollment){
			enrollmentList.add(enrollment);	
			setAgentIdAndRole(enrollmentTenantResponse,enrollmentList);
			List<EnrollmentTenantDTO> enrollmentTenantList = getEnrollmentTenantList(enrollmentList);
			enrollmentTenantResponse.setEnrollments(enrollmentTenantList);
			enrollmentTenantResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentTenantResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		}else{
			LOGGER.info(ERR_MSG_205);
			setErrorResponse(enrollmentTenantResponse, EnrollmentConstants.ERROR_CODE_205, ERR_MSG_205);
		}
		return enrollmentTenantResponse;
	}

	@Override
	public Enrollment findEnrollmentForTenant(Long leadId, Integer enrollmentId, Long affiliateId) {
		LOGGER.info("Fetching enrollments for tenant @ findEnrollmentForTenant()");
		return enrollmentRepository.getEnrollmentForTenant(enrollmentId, leadId, affiliateId);
	}

	/**
	 * Populates the EnrollmentTenantDTO from the enrollment list
	 * @author negi_s
	 * @since 08/07/2015
	 * @param enrollmentList
	 * @return List of EnrollmentTenantDTOs
	 */
	private List<EnrollmentTenantDTO> getEnrollmentTenantList(List<Enrollment> enrollmentList) {
		List<EnrollmentTenantDTO> enrollmentTenantDTOs= new ArrayList<EnrollmentTenantDTO>();
		for(Enrollment enrollment : enrollmentList){
			EnrollmentTenantDTO enrTenantDTO = new EnrollmentTenantDTO();
			enrTenantDTO.setEnrollmentId(enrollment.getId());
			enrTenantDTO.setExchangePlanId(enrollment.getCMSPlanID());

			if(EnrollmentUtils.isNotNullAndEmpty(enrollment.getPlanLevel())){
				enrTenantDTO.setPlanTier(enrollment.getPlanLevel().toUpperCase());
			}
			enrTenantDTO.setMarketType(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL);
			if(null != enrollment.getEnrollmentTypeLkp()){
				enrTenantDTO.setMarketType(enrollment.getEnrollmentTypeLkp().getLookupValueCode());
			}
			if(null != enrollment.getBenefitEffectiveDate()){
				enrTenantDTO.setCoverageStartDate(DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT));
			}
//			enrTenantDTO.setProductCategory("H"); // H : Health, M : Medical
			enrTenantDTO.setGrossPremiumAmount(enrollment.getGrossPremiumAmt());
			enrTenantDTO.setAptcAmount(enrollment.getAptcAmt());
			enrTenantDTO.setNetPremiumAmount(enrollment.getNetPremiumAmt());
			if(null != enrollment.getEnrollmentStatusLkp()){
				enrTenantDTO.setEnrollmentStatus(EnrollmentTenantDTO.EnrollmentStatus.valueOf(enrollment.getEnrollmentStatusLkp().getLookupValueCode().toUpperCase()));
			}
			if(null != enrollment.getInsuranceTypeLkp()){
				enrTenantDTO.setInsuranceType(enrollment.getInsuranceTypeLkp().getLookupValueLabel().toUpperCase());
			}
			enrTenantDTO.setEnrollmentSubmissionDate(getCarrierSubmissionDate(enrollment.getId()));
			enrTenantDTO.setEnrollmentType(EnrollmentTenantDTO.EnrollmentType.I); // Check ?
			List<EnrolleeTenantDTO> enrolleeTenantList = getEnrolleeData(enrollment.getEnrolleesAndSubscriber(), enrollment.getSubscriberForEnrollment());
			enrTenantDTO.setEnrollees(enrolleeTenantList);
			enrollmentTenantDTOs.add(enrTenantDTO);
		}
		return enrollmentTenantDTOs;
	}

	/**
	 * Populates the EnrolleeTenantDTO from the enrollee list
	 * @param enrollees
	 * @param subscriber Enrollee subscriber
	 * @return List of EnrolleeTenantDTOs
	 */
	private List<EnrolleeTenantDTO> getEnrolleeData(List<Enrollee> enrollees, Enrollee subscriber) {
		List<EnrolleeTenantDTO> enrolleeTenantList = new ArrayList<EnrolleeTenantDTO>();
		String subscriberMemberId = null;
		if(null != subscriber){
			subscriberMemberId = subscriber.getExchgIndivIdentifier();
		}
		if(null != enrollees && !enrollees.isEmpty()){
			for(Enrollee enrollee : enrollees){
				EnrolleeTenantDTO enrolleeTenantDTO = new EnrolleeTenantDTO();
				enrolleeTenantDTO.setFirstName(enrollee.getFirstName());
				enrolleeTenantDTO.setLastName(enrollee.getLastName());
				enrolleeTenantDTO.setMiddleName(enrollee.getMiddleName());
				enrolleeTenantDTO.setSuffix(enrollee.getSuffix());
				enrolleeTenantDTO.setSubscriberFlag(EnrollmentConstants.NO);
				if(null != enrollee.getPersonTypeLkp() && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
					enrolleeTenantDTO.setSubscriberFlag(EnrollmentConstants.YES);
				}
				enrolleeTenantDTO.setExchgIndivIdentifier(enrollee.getExchgIndivIdentifier());
				enrolleeTenantDTO.setTaxIdNumber(enrollee.getTaxIdNumber());
				enrolleeTenantDTO.setTotalIndvResponsibilityAmt(enrollee.getTotalIndvResponsibilityAmt());
				if(null != enrollee.getEffectiveStartDate()){
					enrolleeTenantDTO.setEffectiveStartDate(DateUtil.dateToString(enrollee.getEffectiveStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				if(null != enrollee.getEffectiveEndDate()){
					enrolleeTenantDTO.setEffectiveEndDate(DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				if(null != enrollee.getBirthDate()){
					enrolleeTenantDTO.setDob(DateUtil.dateToString(enrollee.getBirthDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				if(null != enrollee.getGenderLkp()){
					enrolleeTenantDTO.setGender(enrollee.getGenderLkp().getLookupValueCode());
				}
				if(null != enrollee.getTobaccoUsageLkp() && EnrollmentConstants.T.equalsIgnoreCase(enrollee.getTobaccoUsageLkp().getLookupValueCode())){
					enrolleeTenantDTO.setTobaccoUsage(EnrollmentConstants.YES);
				}else if(null != enrollee.getTobaccoUsageLkp()){
					enrolleeTenantDTO.setTobaccoUsage(EnrollmentConstants.NO);
				}
				if(null != enrollee.getMaritalStatusLkp()){
					enrolleeTenantDTO.setMaritalStatus(enrollee.getMaritalStatusLkp().getLookupValueCode());
				}
				enrolleeTenantDTO.setPrimaryPhoneNo(enrollee.getPrimaryPhoneNo());
				enrolleeTenantDTO.setSecondaryPhoneNo(enrollee.getSecondaryPhoneNo());
				enrolleeTenantDTO.setPreferredSMS(enrollee.getPreferredSMS());
				enrolleeTenantDTO.setPreferredEmail(enrollee.getPreferredEmail());
				Location homeAddress = enrollee.getHomeAddressid();
				if(null != homeAddress){
					enrolleeTenantDTO.setHomeAddress1(homeAddress.getAddress1());
					enrolleeTenantDTO.setHomeAddress2(homeAddress.getAddress2());
					enrolleeTenantDTO.setHomeAddressState(homeAddress.getState());
					enrolleeTenantDTO.setHomeAddressZip(homeAddress.getZip());
					enrolleeTenantDTO.setHomeAddressCountyCode(homeAddress.getCountycode());
					enrolleeTenantDTO.setHomeAddressCity(homeAddress.getCity());
				}
				if(null != enrollee.getLanguageLkp()){
					enrolleeTenantDTO.setLanguageSpoken(enrollee.getLanguageLkp().getLookupValueCode());
				}
				if(null != enrollee.getLanguageWrittenLkp()){
					enrolleeTenantDTO.setLanguageWritten(enrollee.getLanguageWrittenLkp().getLookupValueCode());
				}
				enrolleeTenantDTO.setEnrolleeRace(getEnrolleeRaceMap(enrollee.getEnrolleeRace()));
				Map<String, String> enrolleeRelationshipMap  = getEnrolleeRelationshipMap(enrollee.getEnrolleeRelationship());
				if(null == enrolleeRelationshipMap || enrolleeRelationshipMap.isEmpty()){
					enrolleeRelationshipMap = getRelationshipMapWithSubscriber(subscriberMemberId,enrollee.getRelationshipToHCPLkp());
				}
				enrolleeTenantDTO.setEnrolleeRelationship(enrolleeRelationshipMap);
				enrolleeTenantList.add(enrolleeTenantDTO);
			}
		}
		return enrolleeTenantList;
	}

	/**
	 * Populates the Enrollee Relationshipshiip map from the enrollee relationship list 
	 * @param enrolleeRelationshipList
	 * @return Map of Enrollee Relationships
	 */
	private Map<String, String> getEnrolleeRelationshipMap(List<EnrolleeRelationship> enrolleeRelationshipList) {
		Map<String, String> enrolleeRelationshipMap = null;
		if(null != enrolleeRelationshipList && !enrolleeRelationshipList.isEmpty()){
			enrolleeRelationshipMap = new HashMap<String, String>();
			for(EnrolleeRelationship enrolleeRelationship : enrolleeRelationshipList){
				if(null != enrolleeRelationship.getTargetEnrollee() && null != enrolleeRelationship.getRelationshipLkp() ){
					enrolleeRelationshipMap.put(enrolleeRelationship.getTargetEnrollee().getExchgIndivIdentifier(), enrolleeRelationship.getRelationshipLkp().getLookupValueCode());
				}
			}
		}
		return enrolleeRelationshipMap;
	}
	
	/**
	 * Sets the relationship of the member to the subscriber when enrollee relationship is not available
	 * @param subscriberMemberId
	 * @param relationshipToHCPLkp Lookup Value
	 * @return Map of Enrollee Relationship
	 */
	private Map<String, String> getRelationshipMapWithSubscriber(String subscriberMemberId, LookupValue relationshipToHCPLkp) {
		Map<String, String> enrolleeRelationshipMap =  new HashMap<String, String>();
		if(EnrollmentUtils.isNotNullAndEmpty(subscriberMemberId) && null != relationshipToHCPLkp){
			enrolleeRelationshipMap.put(subscriberMemberId,relationshipToHCPLkp.getLookupValueCode());	
		}
		return enrolleeRelationshipMap;
	}

	/**
	 * Populates the enrollee race map from the enrollee race list 
	 * @param enrolleeRaceList
	 * @return Map of Enrollee Race
	 */
	private Map<String, String> getEnrolleeRaceMap(List<EnrolleeRace> enrolleeRaceList) {
		Map<String, String> enrolleeRaceMap =  null;
		if(null != enrolleeRaceList && !enrolleeRaceList.isEmpty()){
			enrolleeRaceMap = new HashMap<String, String>();
			for(EnrolleeRace enrolleeRace : enrolleeRaceList){
				if(null != enrolleeRace.getRaceEthnicityLkp()){
					enrolleeRaceMap.put(enrolleeRace.getRaceEthnicityLkp().getLookupValueCode(), enrolleeRace.getRaceDescription());
				}
			}
		}
		return enrolleeRaceMap;
	}
	
/*	private List<EnrolleeRaceTenantDTO> getEnrolleeRace(List<EnrolleeRace> enrolleeRaceList) {
		List<EnrolleeRaceTenantDTO> enrolleeRaceTenantList = null;
		if(null != enrolleeRaceList && !enrolleeRaceList.isEmpty()){
			enrolleeRaceTenantList =  new ArrayList<EnrolleeRaceTenantDTO>();
			for(EnrolleeRace enrolleeRace : enrolleeRaceList){
				EnrolleeRaceTenantDTO enrolleeRaceTenantDTO = new EnrolleeRaceTenantDTO();
				if(null != enrolleeRace.getRaceEthnicityLkp()){
					enrolleeRaceTenantDTO.setRaceEthnicityCode(enrolleeRace.getRaceEthnicityLkp().getLookupValueCode());
				}
				enrolleeRaceTenantDTO.setRaceEthnicityDesc(enrolleeRace.getRaceDescription());
				enrolleeRaceTenantList.add(enrolleeRaceTenantDTO);
			}
		}
		return enrolleeRaceTenantList;
	}
	
	private List<EnrolleeRelationshipTenantDTO> getEnrolleeRelationship(List<EnrolleeRelationship> enrolleeRelationshipList) {
		List<EnrolleeRelationshipTenantDTO> enrolleeRelationshipTenantList = null;
		if(null != enrolleeRelationshipList && !enrolleeRelationshipList.isEmpty()){
			enrolleeRelationshipTenantList = new ArrayList<EnrolleeRelationshipTenantDTO>();
			for(EnrolleeRelationship enrolleeRelationship : enrolleeRelationshipList){
				EnrolleeRelationshipTenantDTO enrolleeRelationshipTenantDTO = new EnrolleeRelationshipTenantDTO();
				if(null != enrolleeRelationship.getTargetEnrollee()){
					enrolleeRelationshipTenantDTO.setMemberId(enrolleeRelationship.getTargetEnrollee().getExchgIndivIdentifier());	
				}
				if(null != enrolleeRelationship.getRelationshipLkp()){
					enrolleeRelationshipTenantDTO.setRelationshipCode(enrolleeRelationship.getRelationshipLkp().getLookupValueCode());	
				}
				enrolleeRelationshipTenantList.add(enrolleeRelationshipTenantDTO);
			}
		}
		return enrolleeRelationshipTenantList;
	}
	
	private List<EnrolleeRelationshipTenantDTO> getRelationshipWithSubscriber(String subscriberMemberId, LookupValue relationshipToHCPLkp) {
		List<EnrolleeRelationshipTenantDTO> enrolleeRelationshipTenantList = new ArrayList<EnrolleeRelationshipTenantDTO>();
		EnrolleeRelationshipTenantDTO enrolleeRelationshipTenantDTO = new EnrolleeRelationshipTenantDTO();
		if(EnrollmentUtils.isNotNullAndEmpty(subscriberMemberId)){
			enrolleeRelationshipTenantDTO.setMemberId(subscriberMemberId);	
		}
		if(null != relationshipToHCPLkp){
			enrolleeRelationshipTenantDTO.setRelationshipCode(relationshipToHCPLkp.getLookupValueCode());	
		}
		enrolleeRelationshipTenantList.add(enrolleeRelationshipTenantDTO);
		return enrolleeRelationshipTenantList;
	}

	*/
	
	/**
	 * Sets agent Id and the user role in the response object 
	 * @param enrollmentTenantResponse
	 * @param enrollmentList
	 */
	private void setAgentIdAndRole(EnrollmentTenantResponse enrollmentTenantResponse, List<Enrollment> enrollmentList) {
		Enrollment enrollment = enrollmentList.get(0);
		if(EnrollmentUtils.isNotNullAndEmpty(enrollment.getAssisterBrokerId()) && EnrollmentUtils.isNotNullAndEmpty(enrollment.getBrokerRole())){
//			enrollmentTenantResponse.setAgentId(enrollment.getAssisterBrokerId().toString());
//			enrollmentTenantResponse.setUserRole(enrollment.getBrokerRole().toUpperCase());
			enrollmentTenantResponse.setAgentId(null);
			enrollmentTenantResponse.setUserRole(null);
		}
	}
	
	/**
	 * Sets the error response parameters
	 * @param enrollmentTenantResponse
	 * @param errorCode
	 * @param errMsg
	 */
	private void setErrorResponse(EnrollmentTenantResponse enrollmentTenantResponse, int errorCode, String errMsg) {
		enrollmentTenantResponse.setErrCode(errorCode);
		enrollmentTenantResponse.setErrMsg(errMsg);
		enrollmentTenantResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
	}
	
	
	/**
	 * Returns the initial carrier submission date, when the enrollment went to PENDING status for the first time
	 * @param id the enrollment Id
	 * @return String carrier submission date
	 */
	private String getCarrierSubmissionDate(Integer id) {
		Date carrierSubmissionDate =  enrollmentAudRepository.getCarrierSubmissionDate(id);
		String carrierSubmissionDateStr = null;
		if(null != carrierSubmissionDate){
			carrierSubmissionDateStr = DateUtil.dateToString(carrierSubmissionDate, GhixConstants.REQUIRED_DATE_FORMAT);
		}
		return carrierSubmissionDateStr;
	}


}
