package com.getinsured.hix.enrollment.util;

import java.io.IOException;
import java.io.Writer;

import com.sun.xml.bind.marshaller.CharacterEscapeHandler;
/**
 * @since 7th Feb 2014
 * @author parhi_s
 * This class is used to customize the JAXB marshaller escape character Handler  
 */
public class NullCharacterEscapeHandler implements CharacterEscapeHandler {

    public NullCharacterEscapeHandler() {
        super();
    }

   /**
    * @since 7th Feb 2014
    * @author parhi_s
    * This method just skips the usual escape handler
    */
    @Override
    public void escape(char[] ch, int start, int length, boolean isAttVal, Writer writer) throws IOException {
        writer.write( ch, start, length );
    }
}