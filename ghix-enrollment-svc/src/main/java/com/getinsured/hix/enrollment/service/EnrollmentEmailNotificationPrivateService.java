/**
 * 
 */

package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author raja
 * @since 23/09/2013
 */
public interface EnrollmentEmailNotificationPrivateService {
	
	void sendEnrollmentStatusNotification(List<Enrollment> enrollmentList) throws GIException;
}
