/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.enrollment.repository.IEnrlAgentBobTransferRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmenAgentBOBTransferThread;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrlAgentBOBTransfer;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.google.gson.Gson;

/**
 * @author panda_p
 *
 */
@Service("enrlAgentBobTransferService")
public class EnrlAgentBobTransferServiceImpl implements EnrlAgentBobTransferService{
	
	@Autowired private IEnrlAgentBobTransferRepository iEnrlAgentBobTransferRepository;
	@Autowired private Gson platformGson;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	
	
	@Override
	public EnrlAgentBOBTransfer saveAgentBobTransferLog(EnrlAgentBOBTransfer enrlAgentBOBTransfer) {
		return iEnrlAgentBobTransferRepository.saveAndFlush(enrlAgentBOBTransfer);
	}
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlAgentBobTransferServiceImpl.class);

	@Override
	public void  serveEnrlAgentBOBTransferJob(EnrlAgentBOBTransfer bobTransfer) throws Exception{
			if(bobTransfer!=null){
				Integer maxEnrollmentPerThread=2000;
				String enrolAgentBOBThread = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGENT_BOB_TRANSFER_THREAD);
				if(enrolAgentBOBThread!=null && !enrolAgentBOBThread.trim().equalsIgnoreCase("")){
					maxEnrollmentPerThread= Integer.parseInt(enrolAgentBOBThread);
				}
					List<Integer> enrollmentIds=null;
					
					try{
						EnrollmentBrokerUpdateDTO brokerReq=platformGson.fromJson(bobTransfer.getRequest(), EnrollmentBrokerUpdateDTO.class);
						if(brokerReq!=null) {
							brokerReq.setUser(bobTransfer.getUser());
						}
						int defaultThreadPoolSize = 5;
						String enrolAgentBOBThreadPool = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGENT_BOB_TRANSFER_THREAD_POOL);
						if(enrolAgentBOBThreadPool!=null && !"".equals(enrolAgentBOBThreadPool.trim())){
							defaultThreadPoolSize= Integer.parseInt(enrolAgentBOBThreadPool);
						}
						ExecutorService executor = Executors.newFixedThreadPool(defaultThreadPoolSize);
						bobTransfer.setBatchProcessedStatus( EnrlAgentBOBTransfer.BATCH_PROCESSING_STATUS.PROCESSED.toString());
						enrollmentIds= findEnrollmentIds(bobTransfer, brokerReq);
						if(enrollmentIds!=null && enrollmentIds.size()>0){
							List<List<Integer>> splittedList= splitList(enrollmentIds, maxEnrollmentPerThread);
							List<Callable<Map<String,String>>> taskList = new ArrayList<>();
							
							
							
							for(List<Integer> enrollmentSubList:splittedList ){
								taskList.add(new EnrollmenAgentBOBTransferThread(enrollmentSubList, this,brokerReq ));
							}
							List<Future<Map<String,String>>> futureObjects=executor.invokeAll(taskList);
							Map<String, String> enrollmentSkipMap= new HashMap<>();
							if(futureObjects!=null && futureObjects.size()>0){
								for (Future<Map<String,String>> ftr:futureObjects ){
									Map<String,String>  skipMap=ftr.get();
									if(skipMap!=null && skipMap.size()>0){
										enrollmentSkipMap.putAll(skipMap);
									}
								}
							}
							executor.shutdown();
							
							if(enrollmentSkipMap.size()>0){
								bobTransfer.setSkipError(enrollmentSkipMap.toString());
								bobTransfer.setSkipEnrollmentId(StringUtils.join(enrollmentSkipMap.keySet(), ','));
								bobTransfer.setBatchProcessedStatus( EnrlAgentBOBTransfer.BATCH_PROCESSING_STATUS.PARTIALLY_PROCESSED.toString());
								
							}else{
								bobTransfer.setBatchProcessedStatus( EnrlAgentBOBTransfer.BATCH_PROCESSING_STATUS.PROCESSED.toString());
								bobTransfer.setSkipEnrollmentId(null);
								bobTransfer.setSkipError(null);
							}
						}else{
							bobTransfer.setBatchProcessedStatus( EnrlAgentBOBTransfer.BATCH_PROCESSING_STATUS.PROCESSED.toString());
							bobTransfer.setSkipEnrollmentId(null);
							LOGGER.error("No enrollment found for given data of Agent BOB transfer request with id " + bobTransfer.getId());
							bobTransfer.setSkipError("No enrollment found for given request");
						}
				
					}catch(Exception e){
						LOGGER.error("Error while processing Agent BOB transfer request with id " + bobTransfer.getId()+ e.toString());
						String errorDescription = EnrollmentUtils.shortenedStackTrace(e, 5);
						bobTransfer.setSkipError(errorDescription);
						bobTransfer.setBatchProcessedStatus(EnrlAgentBOBTransfer.BATCH_PROCESSING_STATUS.FAILED.toString());
						enrollmentGIMonitorUtil.logToGiMonitor(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT),
								EnrollmentConstants.GiErrorCode.BOB_TRANSFER_ERROR_CODE, "Failed to process agent transfer", errorDescription);
						
					}finally{
						iEnrlAgentBobTransferRepository.saveAndFlush(bobTransfer);
					}
			}
	}
	
	private List<Integer> findEnrollmentIds(EnrlAgentBOBTransfer bobTransfer, EnrollmentBrokerUpdateDTO brokerReq) throws Exception{
		
			List<Integer> enrollmentIds=null;
			if(bobTransfer.getStatus()==null && EnrlAgentBOBTransfer.BATCH_PROCESSING_STATUS.REPROCESS.toString().equalsIgnoreCase( bobTransfer.getStatus()) && bobTransfer.getSkipEnrollmentId()!=null){
				Pattern pattern = Pattern.compile(",");
				List<Integer> list = pattern.splitAsStream(bobTransfer.getSkipEnrollmentId())
				                            .map(Integer::valueOf)
				                            .collect(Collectors.toList());
				enrollmentIds = enrollmentRepository.getIndivEnrollmentForBOBTransfer(brokerReq.getOldAssisterBrokerId(),list);
			}else {
				if(bobTransfer.getRequest()!=null){
					
					Date startDate=null;
					Date endDate=null;
					try{
						if(brokerReq.getTransferStartDate()!=null){
							startDate=DateUtil.StringToDate(brokerReq.getTransferStartDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY );
						}
						
						if(brokerReq.getTransferEndDate()!=null){
							endDate=DateUtil.StringToDate(brokerReq.getTransferEndDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY );
						}
					}catch(Exception e){
						throw new Exception("Either Transfer StartDate or End date parsing failed  ",e);
					}
					if(startDate!=null && endDate!=null){
						enrollmentIds = enrollmentRepository.getIndivEnrollmentForBOBTransfer(brokerReq.getOldAssisterBrokerId(), EnrollmentUtils.getStartDateTime(startDate),EnrollmentUtils.getEODDate(endDate));
					}else{
						enrollmentIds = enrollmentRepository.getIndivEnrollmentForBOBTransfer(brokerReq.getOldAssisterBrokerId());
					}
				}
			}
			
			return enrollmentIds;
	}
	
	@Override
	public void processBOBTransferRequest(List<Integer> enrollmentIdList, Map<String, String> enrollmentSkipMap, EnrollmentBrokerUpdateDTO brokerReq){
		EnrollmentBrokerUpdateDTO newDTO= new EnrollmentBrokerUpdateDTO();
		copyBrokerDTOFields(brokerReq, newDTO);
		for(Integer enrollmentId: enrollmentIdList){
			try{
				newDTO.setEnrollmentId(enrollmentId);
				EnrollmentResponse response= new EnrollmentResponse();
				enrollmentService.updateBrokerDetails(newDTO, response);
				if( !response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
					enrollmentSkipMap.put(enrollmentId+"", response.getErrMsg());
				}
			}catch(Exception e){
				enrollmentSkipMap.put(enrollmentId+"", EnrollmentUtils.shortenedStackTrace(e, 4));
			}
		}
	}
	
	private void copyBrokerDTOFields(EnrollmentBrokerUpdateDTO oldDTO, EnrollmentBrokerUpdateDTO newDTO){
		if(oldDTO!=null && newDTO!=null){
			newDTO.setAgentBrokerName(oldDTO.getAgentBrokerName());
			newDTO.setAddremoveAction(EnrollmentConstants.ACTION_ADD);
			newDTO.setAssisterBrokerId(oldDTO.getAssisterBrokerId());
			newDTO.setBrokerTPAAccountNumber2(oldDTO.getBrokerTPAAccountNumber2());
			newDTO.setBrokerTPAFlag(oldDTO.getBrokerTPAFlag());
			newDTO.setMarketType(oldDTO.getMarketType());
			newDTO.setRoleType(oldDTO.getRoleType());
			newDTO.setStateEINNumber(oldDTO.getStateEINNumber());
			newDTO.setStateLicenseNumber(oldDTO.getStateLicenseNumber());
			newDTO.setUser(oldDTO.getUser());
		}
	}
	
	/**
	 * Method to split List
	 * @param list
	 * @param size
	 * @return
	 */
	private List<List<Integer>> splitList(List<Integer> list, Integer size){
		List<List<Integer>> splittedList=null;
		if(list!=null && !list.isEmpty()){
			splittedList= new ArrayList<List<Integer>>();
			int totalCount=list.size();
			for(int i=0; i<totalCount; i+=size){
				splittedList.add(new ArrayList<Integer>(list.subList(i, Math.min(totalCount, i+size))));
			}
		}
		
		return splittedList;
	}
	
	@Override
	@Async
	public void processAgentBOBTranferAsync(EnrlAgentBOBTransfer bobTransfer) {
		try {
			serveEnrlAgentBOBTransferJob(bobTransfer);
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while processing of AgentBobTransfer: ", ex);
		}
	}	
}
