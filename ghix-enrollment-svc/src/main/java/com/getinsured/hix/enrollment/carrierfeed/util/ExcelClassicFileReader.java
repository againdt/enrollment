package com.getinsured.hix.enrollment.carrierfeed.util;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by jabelardo on 7/9/14.
 */
public class ExcelClassicFileReader<T> extends ExcelFileReaderBase<T> {

    @Override
    protected Workbook createWorkbook(InputStream inStream) throws IOException {
        return new HSSFWorkbook(inStream);
    }
}
