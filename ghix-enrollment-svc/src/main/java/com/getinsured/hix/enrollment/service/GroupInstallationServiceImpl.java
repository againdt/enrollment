package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.poi.util.IOUtils;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.repository.IEnrDesignateBrokerRepository;
import com.getinsured.hix.enrollment.repository.IEnrlBrokerRepository;
import com.getinsured.hix.enrollment.repository.IEnrlEmployerEnrollmentRepository;
import com.getinsured.hix.enrollment.repository.IEnrlEmployerRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.repository.IGroupInstallationRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.NullCharacterEscapeHandler;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.Employer.OrgType;
import com.getinsured.hix.model.EmployerEnrollment;
import com.getinsured.hix.model.EmployerEnrollment.Status;
import com.getinsured.hix.model.EmployerLocation;
import com.getinsured.hix.model.ExchgPartnerLookup;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.GroupInstallation;
import com.getinsured.hix.model.enrollment.GroupPlan;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;

import gov.cms.dsh.gem.extension._1.BrokerContactType;
import gov.cms.dsh.gem.extension._1.BrokerOrganizationAugmentationType;
import gov.cms.dsh.gem.extension._1.BrokerPrimaryNumberType;
import gov.cms.dsh.gem.extension._1.BrokerType;
import gov.cms.dsh.gem.extension._1.ContactCodeSimpleType;
import gov.cms.dsh.gem.extension._1.ContactCodeType;
import gov.cms.dsh.gem.extension._1.EmployerContactNumberInformationType;
import gov.cms.dsh.gem.extension._1.EmployerContactType;
import gov.cms.dsh.gem.extension._1.EmployerOrganizationAugmentationType;
import gov.cms.dsh.gem.extension._1.EmployerPrimaryNumberType;
import gov.cms.dsh.gem.extension._1.EmployerType;
import gov.cms.dsh.gem.extension._1.EnrollmentActionCodeSimpleType;
import gov.cms.dsh.gem.extension._1.FileInformationType;
import gov.cms.dsh.gem.extension._1.GroupEnrollmentRequestPayloadType;
import gov.cms.dsh.gem.extension._1.InsurancePolicyStatusCodeSimpleType;
import gov.cms.dsh.gem.extension._1.InsurancePolicyStatusCodeType;
import gov.cms.dsh.gem.extension._1.MultiContactInformationType;
import gov.cms.dsh.gem.extension._1.PlanDetailRestrictionType;
import gov.cms.dsh.gem.extension._1.PlanInformationType;
import gov.cms.dsh.gem.extension._1.PlanType;
import gov.cms.dsh.gem.extension._1.PreferredContactModeCodeSimpleType;
import gov.cms.dsh.gem.extension._1.PreferredContactModeCodeType;
import gov.cms.dsh.gem.extension._1.SingleContactInformationType;
import gov.cms.hix._0_1.hix_core.TransmissionMetadataType;
import gov.cms.hix._0_1.hix_ee.InsurancePolicyType;
import gov.cms.hix._0_1.hix_types.ActuarialValueMetallicTierCodeSimpleType;
import gov.cms.hix._0_1.hix_types.ActuarialValueMetallicTierCodeType;
import gov.cms.hix._0_1.hix_types.EmployerCodeSimpleType;
import gov.cms.hix._0_1.hix_types.EmployerCodeType;
import gov.niem.niem.niem_core._2.AddressType;
import gov.niem.niem.niem_core._2.DateType;
import gov.niem.niem.niem_core._2.PersonNameTextType;
import gov.niem.niem.niem_core._2.PersonNameType;
import gov.niem.niem.niem_core._2.ProperNameTextType;
import gov.niem.niem.niem_core._2.QuantityType;
import gov.niem.niem.niem_core._2.StreetType;
import gov.niem.niem.niem_core._2.StructuredAddressType;
import gov.niem.niem.niem_core._2.TextType;
import gov.niem.niem.proxy.xsd._2.Date;
import gov.niem.niem.usps_states._2.USStateCodeSimpleType;
import gov.niem.niem.usps_states._2.USStateCodeType;
/**
 * 
 * @author meher_a
 *
 */
@Service("groupInstallationService")
@Transactional
public class GroupInstallationServiceImpl implements GroupInstallationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupInstallationServiceImpl.class);
	private final SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.GROUP_ENRL_DATE_FORMAT);
	private final SimpleDateFormat timeFormat = new SimpleDateFormat(GhixConstants.GROUP_ENRL_TIME_FORMAT);
	
	@Autowired private IGroupInstallationRepository groupInstallationRepository;
	@Autowired private IEnrlEmployerRepository employerRepository;
	@Autowired private IEnrlBrokerRepository brokerRepository;
	@Autowired private IEnrDesignateBrokerRepository designateBrokerRepository;
	@Autowired private LookupService lookupService;
	@Autowired private ValidationFolderPathService validationFolderPathService;
	@Autowired private IEnrlEmployerEnrollmentRepository employerEnrollmentRepository;
	private List<GroupInstallation> groupInstallationNoXMLList;
	private List<GroupInstallation> groupInstallationXMLChangYList;
	private List<GroupInstallation> groupInstallationXMLChangNList;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	
	@Override
	public void getGroupInstallationXML(String regenerate,String employer_enrollment_Id, String issuer_Id, String groupAction)  throws GIException {
		
		if(regenerate != null && regenerate.equalsIgnoreCase(EnrollmentConstants.YES)){
			
			regenerateGroupInstallationXML(employer_enrollment_Id,issuer_Id, groupAction);
			
		}else{
			
			getAllGroupInstallationXML();
		}
		
	}
	
	
	private void getAllGroupInstallationXML() throws GIException {
		
		//For all group in enrl_group_installation table check if XML data is present
		//If XML Data present 
		//		check for update in employer,brokeror plan data 
		//			if updated Generate new Group XML
		//If XML Data not present 
		//		check for employer Payment status
		//			if paid   Generate XML
		
		try{
			//Check for employer-broker designation
			this.checkEmployerBrokerDesignation();
			
			//populate groupInstallationNoXMLList and groupInstallationXMLList  
			this.populateGroupList();
			
			Status paymentStatus = null;
			Set<GroupInstallation> groupSet = new HashSet<GroupInstallation>();
			
			for (GroupInstallation groupInstallation : groupInstallationNoXMLList) {
				
				//check employer payment status
				if(groupInstallation.getEmployerId() != null && groupInstallation.getEmployerEnrollmentId() != null){
					
					paymentStatus = getEmployerPaidStatus(groupInstallation.getEmployerEnrollmentId());
				}
				
				if(paymentStatus != null && paymentStatus.equals(Status.ACTIVE)){
					groupSet.add(groupInstallation);
				}
			}
			
			LookupValue enrollmentActionMTN = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_MENT);
			
			for (GroupInstallation groupInstallation : groupInstallationXMLChangNList) {
				
				String groupDataXML = groupInstallation.getGroupData();
				
				if(EnrollmentUtils.isNotNullAndEmpty(groupDataXML)){
					
					// compare employer and Broker object if change regenaret tha XML and send this will be Update
					GroupEnrollmentRequestPayloadType groupEnrollmentRequestPayloadType = getGroupEnrollmentRequestPayloadTypeFromXML(groupDataXML);

					if(groupEnrollmentRequestPayloadType!= null && groupInstallation.getEmployerId() != null){
						EmployerType newEmployerType = getEmployerType(groupInstallation.getEmployerId(),false);
						
						EmployerType oldEmployerType = groupEnrollmentRequestPayloadType.getEmployer();
						
						if(!compareEmployerType(oldEmployerType, newEmployerType)){
							groupInstallation.setEnrollmentActionLkp(enrollmentActionMTN);
							groupSet.add(groupInstallation);
							continue;
						}
					}
					
					if(groupEnrollmentRequestPayloadType!= null && groupInstallation.getBrokerId() != null){
						
						BrokerType newBrokerType = getBrokerType(groupInstallation.getBrokerId(), false);
						
						List<BrokerType> brokerTypeList = groupEnrollmentRequestPayloadType.getBroker();
						BrokerType oldBrokerType = null;
						
						if(brokerTypeList != null && !brokerTypeList.isEmpty()){
							
							oldBrokerType = brokerTypeList.get(0);
						}
						
						//New Broker Added, No need to compare generate XML
						if((brokerTypeList == null || oldBrokerType == null) && newBrokerType != null){
							groupInstallation.setEnrollmentActionLkp(enrollmentActionMTN);
							groupSet.add(groupInstallation);
							continue;
						}
						
						//Compare broker type
						if(!compareBrokerType(oldBrokerType, newBrokerType)){
							groupInstallation.setEnrollmentActionLkp(enrollmentActionMTN);
							groupSet.add(groupInstallation);
							continue;
						}
					}
				}
			}
			
			for (GroupInstallation groupInstallation : groupInstallationXMLChangYList) {
				groupSet.add(groupInstallation);
			}
			
			//pass set to Generate/Update XML
			generateXML(groupSet);
			
		} catch (GIException gex) {
			LOGGER.error("Error in getGroupInstallationXML : " + gex.toString(),gex);
		}
		
	}
	
private void regenerateGroupInstallationXML(String employer_enrollment_Id,String issuer_Id, String groupAction) throws GIException {
		
		GroupInstallation groupInstallation = null;
		List<GroupInstallation> groupInstallationList = null;
			
			if(isNotNullAndEmpty(issuer_Id) && isNotNullAndEmpty(employer_enrollment_Id)){
				if(isNotNullAndEmpty(groupAction) && groupAction.equalsIgnoreCase(EnrollmentConstants.GROUP_ACTION_TYPE_TERM)) {
					groupInstallation=groupInstallationRepository.getLastTerminatedByEmployerIdAndIssuerId(Integer.parseInt(employer_enrollment_Id),Integer.parseInt(issuer_Id));
				}else{
					groupInstallation = groupInstallationRepository.getGroupInstallationByEmployerIdAndIssuerId(Integer.parseInt(employer_enrollment_Id),Integer.parseInt(issuer_Id));
					if(groupInstallation==null){
						groupInstallation=groupInstallationRepository.getLastTerminatedByEmployerIdAndIssuerId(Integer.parseInt(employer_enrollment_Id),Integer.parseInt(issuer_Id));
					}
				}
			}else if(isNotNullAndEmpty(issuer_Id)){
				
				groupInstallationList = groupInstallationRepository.getGroupInstallationByIssuerId(Integer.parseInt(issuer_Id));
			}
			
			if(groupInstallation != null && groupInstallation.getGroupData() != null){
				
				generateXML(groupInstallation,false);
				
			}
			
			if(groupInstallationList != null){
				for (GroupInstallation grpInstallation : groupInstallationList) {
					generateXML(grpInstallation,false);
				}
			}
	}

	private void populateGroupList() {
		
		List<GroupInstallation> allGroupInstallation = groupInstallationRepository.findAllGroupInstallation();
		
		groupInstallationNoXMLList = new ArrayList<GroupInstallation> ();
		groupInstallationXMLChangYList = new ArrayList<GroupInstallation> ();
		groupInstallationXMLChangNList = new ArrayList<GroupInstallation> ();
		
		for (GroupInstallation groupInstallation : allGroupInstallation) {
			
			if(isNotNullAndEmpty(groupInstallation.getGroupData())){
				
				if(groupInstallation.getIsChanged().equalsIgnoreCase(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y)){
					
					groupInstallationXMLChangYList.add(groupInstallation);
				
				}else{
					
					groupInstallationXMLChangNList.add(groupInstallation);
				}
				
			}else{
				
				groupInstallationNoXMLList.add(groupInstallation);
			}
		}
	}
	
	private void checkEmployerBrokerDesignation() {
		List<GroupInstallation> allGroupInstallation = groupInstallationRepository.findAllGroupInstallation();
		DesignateBroker designatedBroker = null;
		int grpBrokerId = 0;
		int designatedBrokerId = 0;
		LookupValue enrollmentActionLKP_MNT = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_MENT);
		LookupValue enrollmentActionLKP_CAN = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_DEL);
		LookupValue enrollmentActionLKP_TER = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_TERM);
		
		for (GroupInstallation groupInstallation : allGroupInstallation) {
			
			designatedBroker = designateBrokerRepository.findDesigBrokerByEmployerIdAndStatus(groupInstallation.getEmployerId().intValue(),DesignateBroker.Status.Active);
			
			if(groupInstallation.getBrokerId() != null){
				grpBrokerId = groupInstallation.getBrokerId().intValue();
			}
			
			if(designatedBroker != null){
				designatedBrokerId = designatedBroker.getBrokerId();
			}
			
			if(designatedBrokerId != 0 && grpBrokerId == 0){
				// Broker added
				groupInstallation.setBrokerId(Integer.valueOf(designatedBrokerId));
				groupInstallation.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y);
				/*if(isNotNullAndEmpty(groupInstallation.getGroupData()) && (groupInstallation.getEnrollmentActionLkp() != enrollmentActionLKP_CAN && groupInstallation.getEnrollmentActionLkp() != enrollmentActionLKP_TER)){*/
				if(isNotNullAndEmpty(groupInstallation.getGroupData()) && (!groupInstallation.getEnrollmentActionLkp().equals(enrollmentActionLKP_CAN)   && !groupInstallation.getEnrollmentActionLkp().equals(enrollmentActionLKP_TER))){
				
					groupInstallation.setEnrollmentActionLkp(enrollmentActionLKP_MNT);
				}
				groupInstallationRepository.saveAndFlush(groupInstallation);
				
			}else if((grpBrokerId != 0 && designatedBrokerId != 0) && (grpBrokerId != designatedBrokerId)){
				
				// Broker updated
				groupInstallation.setBrokerId(Integer.valueOf(designatedBrokerId));
				groupInstallation.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y);
			//	if(isNotNullAndEmpty(groupInstallation.getGroupData()) && (groupInstallation.getEnrollmentActionLkp() != enrollmentActionLKP_CAN && groupInstallation.getEnrollmentActionLkp() != enrollmentActionLKP_TER)){
				if(isNotNullAndEmpty(groupInstallation.getGroupData()) && (!groupInstallation.getEnrollmentActionLkp().equals(enrollmentActionLKP_CAN) && !groupInstallation.getEnrollmentActionLkp().equals(enrollmentActionLKP_TER))){
					groupInstallation.setEnrollmentActionLkp(enrollmentActionLKP_MNT);
				}
				groupInstallationRepository.saveAndFlush(groupInstallation);
			
			}else if (designatedBrokerId == 0 && grpBrokerId != 0){
				// Broker de-deligated
				groupInstallation.setBrokerId(null);
				groupInstallation.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y);
			//	if(isNotNullAndEmpty(groupInstallation.getGroupData()) && (groupInstallation.getEnrollmentActionLkp() != enrollmentActionLKP_CAN && groupInstallation.getEnrollmentActionLkp() != enrollmentActionLKP_TER)){
				if(isNotNullAndEmpty(groupInstallation.getGroupData()) && !(groupInstallation.getEnrollmentActionLkp().equals(enrollmentActionLKP_CAN) && groupInstallation.getEnrollmentActionLkp().equals(enrollmentActionLKP_TER))){
					groupInstallation.setEnrollmentActionLkp(enrollmentActionLKP_MNT);
				}
				groupInstallationRepository.saveAndFlush(groupInstallation);
			}
			grpBrokerId = 0;
			designatedBrokerId = 0;
		}
	}


	private void generateXML(Set<GroupInstallation> groupSet) throws GIException {
		LOGGER.debug("EnrollmentXMLGroupBatch Total No. of XML file :"+groupSet.size());
		
		for (GroupInstallation groupInstallation : groupSet) {
			generateXML(groupInstallation,true);
		}
	}

	private void generateXML(GroupInstallation groupInstallation, boolean regenerate) throws GIException {
		String outputPath = null;
		String fileName = null;
		
		String tradingPartnerID = null;
		boolean isError = false;
		
		if(regenerate){
			isError = addGroupData(groupInstallation, true);
		}
		
		if(!isError){
			
			tradingPartnerID = getHiosIssuerId(groupInstallation.getIssuerId());
			//Adding 2 second delay to avoid file override 
			delay(EnrollmentConstants.TWO_THOUSAND);
			java.util.Date fileDate = new java.util.Date();
			try
			{
				Calendar calendar = TSCalendar.getInstance();
				calendar.setTime(fileDate);
				int oneTenthOfSecond = calendar.get(Calendar.SECOND) /EnrollmentConstants.TEN ;
				ExchgPartnerLookup exchgPartnerLookup = null;
				String hiosIssuerId = getHiosIssuerId(groupInstallation.getIssuerId());
				String groupXmlExtractPathfromDb = null;
				String isa15= "";
				exchgPartnerLookup = validationFolderPathService.findExchgPartnerforGroupInstallByHiosIssuerID(hiosIssuerId,GhixConstants.ENROLLMENT_TYPE_SHOP,GhixConstants.OUTBOUND);
				if(exchgPartnerLookup != null && exchgPartnerLookup.getSourceDir() != null){
				 groupXmlExtractPathfromDb = exchgPartnerLookup.getSourceDir();
				 isa15= exchgPartnerLookup.getIsa15();
				}
				
				outputPath = groupXmlExtractPathfromDb;
				
				if(outputPath!=null && !("".equals(outputPath.trim()))){
					if (!(new File(outputPath).exists())) {
						LOGGER.error("enrollmentXMLGroupJob SERVICE : No Source Directory Exists defined in ExchangePartner Lookup Table (OUT-PUT DIRECTORY :: "+outputPath+" FOR ISSUER ID :: "+groupInstallation.getIssuerId());
						
					}
				}else{
					LOGGER.error("enrollmentXMLGroupJob SERVICE :No Source Directory Defined in ExchangePartner Lookup Table FOR ISSUER ID :: "+groupInstallation.getIssuerId()+" ");
				}
				
				fileName = tradingPartnerID+".GROP.D"+dateFormat.format(new java.util.Date())+".T"+timeFormat.format(fileDate)+oneTenthOfSecond+"."+isa15+".OUT";
				outputPath = outputPath+"//"+fileName;
			}
			catch(Exception e){
				
				LOGGER.error("Error in enrollmentXMLGroupJob() ::"+e.getMessage(),e);		
			}
			
			/**
			 * HIX-71117 Fix HP-FOD: Unreleased Resource: Streams issues for Enrollment
			 */
			try(PrintStream grpXmlFileStream = new PrintStream(new FileOutputStream(outputPath));) {
				LOGGER.debug("Started Generatng group XML file for IssuerId ( "+groupInstallation.getIssuerId()+") and EmployerId ("+groupInstallation.getEmployerId()+") and Employer_Enrollment_Id ("+groupInstallation.getEmployerEnrollmentId()+") in to path ("+outputPath+")");
				
				grpXmlFileStream.print(groupInstallation.getGroupData());
				LOGGER.debug("Done Generatng group XML file for IssuerId ( "+groupInstallation.getIssuerId()+") and EmployerId ("+groupInstallation.getEmployerId()+") and Employer_Enrollment_Id ("+groupInstallation.getEmployerEnrollmentId()+") in to path ("+outputPath+")");
			} catch (Exception ex) {
				LOGGER.error("Exception @ enrollmentXMLGroupJob batch while writing File to Directory" , ex);
			}
			groupInstallation.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_N);
			groupInstallation.setFileName(fileName);
			
			if(groupInstallation.getEnrollmentActionLkp()!=null && groupInstallation.getEnrollmentActionLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.GROUP_ACTION_TYPE_TERM)){
				
				groupInstallation.setEnrollmentActionLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_DEL));
			}
			
			groupInstallationRepository.saveAndFlush(groupInstallation);
		}
	}

	private void delay(int i) {
		try {
		    Thread.sleep(i);
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
	}


	private boolean compareBrokerType(BrokerType oldBrokerType,BrokerType newBrokerType) {
	
		String oldBrokerTypeStr = getBrkStringFromXMLObject(oldBrokerType);
		String newBrokerTypeStr = getBrkStringFromXMLObject(newBrokerType);
		
		return compareXMLAsString(oldBrokerTypeStr,newBrokerTypeStr);
	}

	private String getBrkStringFromXMLObject(BrokerType oldBrokerType) {
		StringWriter sw = new StringWriter();
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(BrokerType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	 
			jaxbMarshaller.marshal(oldBrokerType, sw);
			
		} catch (JAXBException e) {
			LOGGER.error("Error in getBrkStringFromXMLObject::"+e.toString(),e);
		}
		return sw.toString();
	}

	private boolean compareEmployerType(EmployerType oldEmployerType,EmployerType newEmployerType) {
		
		String oldEmployerTypeStr = getEmpStringFromXMLObject(oldEmployerType);
		String newEmployerTypeStr = getEmpStringFromXMLObject(newEmployerType);
		
		return compareXMLAsString(oldEmployerTypeStr, newEmployerTypeStr);
	}

	private boolean compareXMLAsString(String oldXMLStr,String newXMLStr) {
		XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreAttributeOrder(true);
       // boolean isEqual=true;
        DetailedDiff diff = null;
		try {
			diff = new DetailedDiff(XMLUnit.compareXML(oldXMLStr, newXMLStr));
		} catch (Exception e) {
			LOGGER.error("Exception @ CompareXMLAsString ", e);
		}
		
		if (diff != null) {
			return diff.identical();
		} else {
			return false;
		}
	}

	private String getEmpStringFromXMLObject(EmployerType employerType) {
		StringWriter sw = new StringWriter();
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(EmployerType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	 
			jaxbMarshaller.marshal(employerType, sw);
			
		} catch (JAXBException e) {
			LOGGER.error("Error in getEmpStringFromXMLObject::"+e.toString(),e);
		}
		return sw.toString();
	}

	private GroupEnrollmentRequestPayloadType getGroupEnrollmentRequestPayloadTypeFromXML(String groupDataXML) {
	//	JAXBContext jaxbContext;
		GroupEnrollmentRequestPayloadType groupEnrollmentRequestPayloadType = null;
		InputStream is = null;
		try {
			
		/*	jaxbContext = JAXBContext.newInstance(GroupEnrollmentRequestPayloadType.class);
			
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			groupEnrollmentRequestPayloadType = (GroupEnrollmentRequestPayloadType) jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(groupDataXML.getBytes()));*/
			is = new ByteArrayInputStream(groupDataXML.getBytes());
			GhixUtils.inputStreamToObject(is, GroupEnrollmentRequestPayloadType.class);
		}catch(Exception e){
			LOGGER.error("Exception in getGroupEnrollmentRequestPayloadTypeFromXML ::"+ e.getMessage());
		}finally{
			IOUtils.closeQuietly(is);
		}
		
		return groupEnrollmentRequestPayloadType;
	}

	@Override
	public void addOrUpdateEmployerGroup(Integer empId, Integer issuerId, Integer planId,Integer employerEnrollmentId) {

		GroupInstallation groupInstallation = groupInstallationRepository.getGroupInstallationByEmployerId(empId,issuerId,employerEnrollmentId);
		boolean gropInstallationUpdated = false;
		LookupValue enrollmentActionLKP = null;
		
		if(groupInstallation != null){
			
			//Check for Plan add if new plan
			if(planId != null){
				
				GroupPlan groupPlan = groupInstallationRepository.getGroupInstallationByGroupAndPlan(groupInstallation.getId(),planId);
				
				if(groupPlan == null){
					GroupPlan grpPlan = new GroupPlan();
					grpPlan.setPlanId(planId);
					grpPlan.setGroupInstallation(groupInstallation);
					
					List<GroupPlan> groupPlanList = new ArrayList<GroupPlan>();
					groupPlanList.add(grpPlan);
					
					groupInstallation.setGroupPlan(groupPlanList);
					gropInstallationUpdated = true;
				}
			}
						
			if(gropInstallationUpdated){
				groupInstallation.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y);
				/*
				 * Check if XML data is present 
				 * If XML is present set ACTION as  Maintenance.
				 * If No XML then set ACTION as OpenEnrollment
				 * 
				 * First XML should be always OpenEnrollment.
				 */
				if(isNotNullAndEmpty(groupInstallation.getGroupData())){
					
					enrollmentActionLKP = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_MENT);
					groupInstallation.setEnrollmentActionLkp(enrollmentActionLKP);
					
				}else{
					enrollmentActionLKP = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_INITIAL);
					groupInstallation.setEnrollmentActionLkp(enrollmentActionLKP);
				}
				
				groupInstallationRepository.saveAndFlush(groupInstallation);
			}
			
		}else{
			
			GroupInstallation newGroupInstallation = new GroupInstallation();
			
			newGroupInstallation.setEmployerId(empId);
			
			newGroupInstallation.setIssuerId(issuerId);
			
			newGroupInstallation.setEmployerEnrollmentId(employerEnrollmentId);
			
			// XML will be created In Batch Job when EmployerInvoice status is Paid
			newGroupInstallation.setGroupData(null);  
			
			newGroupInstallation.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y);
			
			enrollmentActionLKP = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_INITIAL);

			newGroupInstallation.setEnrollmentActionLkp(enrollmentActionLKP);
			
			GroupPlan groupPlan = new GroupPlan();
			groupPlan.setPlanId(planId);
			groupPlan.setGroupInstallation(newGroupInstallation);
			
			List<GroupPlan> groupPlanList = new ArrayList<GroupPlan>();
			groupPlanList.add(groupPlan);
			
			newGroupInstallation.setGroupPlan(groupPlanList);
			
			groupInstallationRepository.saveAndFlush(newGroupInstallation);
		}
	}
	
	private String getHiosIssuerId(Integer issuerId) {
		
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
			return enrollmentRepository.getHiosIssuerIdByIssuerIdPg(issuerId);
		}else if("ORACLE".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			return enrollmentRepository.getHiosIssuerIdByIssuerIdOracle(issuerId);
		}
		return "";
	}

	private boolean addGroupData(GroupInstallation groupInstallation, boolean escapeFlag) {

		boolean isError = false;
		
		if (groupInstallation.getEmployerId() != null && groupInstallation.getGroupPlan() != null) {

			//String groupData = getGroupData(groupInstallation.getEmployerId(),groupInstallation.getBrokerId(),groupInstallation.getGroupPlan(),groupInstallation.getIssuerId(),groupInstallation.getEnrollmentActionLkp().getLookupValueCode(), escapeFlag);
			String groupData = getGroupData(groupInstallation, escapeFlag);
			
			if(groupData != null){
				groupInstallation.setGroupData(groupData);
			}else{
				isError = true;
			}

		} else {
			LOGGER.info("Not Creating Group Instalation XML Reson : empId = "+groupInstallation.getEmployerId() +" groupPlanList = "+groupInstallation.getGroupPlan());
		}
		
		return isError;

	}

	private String getGroupData(GroupInstallation groupInstallation, boolean escapeFlag) {
		
		GroupEnrollmentRequestPayloadType groupEnrollmentRequestPayloadType = new GroupEnrollmentRequestPayloadType();

		FileInformationType fileInformationType = getFileInformationType(groupInstallation.getIssuerId(), groupInstallation.getEnrollmentActionLkp().getLookupValueCode(),groupInstallation.getTerminatedOn(), escapeFlag);

		if (fileInformationType != null) {
			groupEnrollmentRequestPayloadType.setFileInformation(fileInformationType);
		}else{
			return null;
		}

		EmployerType employerType = getEmployerType(groupInstallation.getEmployerId(), escapeFlag);
		if (employerType != null) {
			groupEnrollmentRequestPayloadType.setEmployer(employerType);
		} else {
			return null;
		}

		if(groupInstallation.getBrokerId() != null){
			BrokerType brokerType = getBrokerType(groupInstallation.getBrokerId(), escapeFlag);
			if (brokerType != null) {
				groupEnrollmentRequestPayloadType.getBroker().add(brokerType);
			} else {
				return null;
			}
		}
		
		
		PlanType planType = getPlanType(groupInstallation.getGroupPlan(),groupInstallation.getEmployerEnrollmentId(),groupInstallation.getEnrollmentActionLkp().getLookupValueCode(), groupInstallation.getTerminatedOn(),escapeFlag);
		if (planType != null) {
		groupEnrollmentRequestPayloadType.setPlanInformation(planType);
		
		}else{
			return null;
		}
		
		groupEnrollmentRequestPayloadType.setAdditionalNotes("");
		
		StringWriter sw = new StringWriter();
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(GroupEnrollmentRequestPayloadType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			
			jaxbMarshaller.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new NullCharacterEscapeHandler());
			
			jaxbMarshaller.marshal(groupEnrollmentRequestPayloadType, sw);
			
		} catch (JAXBException e) {
			//e.printStackTrace();
			LOGGER.error("Error in getGroupData::"+e.toString(),e);
		}
		return sw.toString();
	}
	
	private gov.niem.niem.proxy.xsd._2.String getXMLStringFromString(String string, boolean escapeFlag) {
		gov.niem.niem.proxy.xsd._2.String text = new gov.niem.niem.proxy.xsd._2.String();
		//string=escapeCharacterHandler(string, escapeFlag);
		text.setValue(escapeCharacterHandler(string, escapeFlag));
		return text;
	}

	private DateType stringToXmlDate(java.util.Date date) {
		
		if(date == null){	return null;	}
		
		Calendar gcal = new GregorianCalendar();
		gcal.setTime(date);
		
		XMLGregorianCalendar xmlCalender = toXMLGregorianCalendarDateOnly(gcal);
		
		Date mntDate = new Date();
		mntDate.setValue(xmlCalender);
		
		DateType dateType = new DateType();
		dateType.setDate(mntDate);
		
		return dateType;
	}
	
	private XMLGregorianCalendar toXMLGregorianCalendarDateOnly(Calendar c){
	    GregorianCalendar gc = new GregorianCalendar();
	    gc.setTimeInMillis(c.getTimeInMillis());
	    XMLGregorianCalendar xc= null;
	   try {
		   // need to add 1 in month as Calender month is zero base and XMLGregorianCalendar month is 1 base.
	       xc = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH)+1,gc.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED);
	   } catch (DatatypeConfigurationException dex) {
		   LOGGER.error("DatatypeConfigurationException @ enrollmentXMLGroupJob batch" , dex);
	   }
	    return xc;
	   }
	private String escapeCharacterHandler(String parText, boolean escapeFlag){
		String text=parText;
		if(text!=null){
			if((text.contains("&amp;") || text.contains("&quot;")) || (text.contains("&lt;") || text.contains("&gt;"))){
				text=text.replaceAll("&amp;", "&");
				text=text.replaceAll("&quot;","\"");
				text=text.replaceAll("&lt;", "<");
				text=text.replaceAll("&gt;", ">");
				if(escapeFlag){
					text= "<![CDATA[" + text + "]]>";
				}
			}
			if(text.contains("℠")){
				text=text.replaceAll("℠", "");
			}
		}
		return text;
	}
	
	private TextType stringToTextType(String text,  boolean escapeFlag){
		TextType textType = new TextType();
		//text= escapeCharacterHandler(text, escapeFlag);
		textType.setValue(escapeCharacterHandler(text, escapeFlag));
		return textType;
	}
	
	private AddressType getAddressType(String streetFullText, String streetExtensionText, String locationCityName, USStateCodeSimpleType usPostalCode, String locationPostalCode, boolean escapeFlag){
		
		AddressType addressType = new AddressType();
		
		StructuredAddressType structuredAddressType = new StructuredAddressType();
		ProperNameTextType properNameTextType = new ProperNameTextType();
		properNameTextType.setValue(escapeCharacterHandler(locationCityName, escapeFlag));
		structuredAddressType.setLocationCityName(properNameTextType);
		
		structuredAddressType.setLocationPostalCode(getXMLStringFromString(locationPostalCode,escapeFlag));
		
		USStateCodeType uSStateCodeType = new USStateCodeType();
		uSStateCodeType.setValue(usPostalCode);
		structuredAddressType.setLocationStateUSPostalServiceCode(uSStateCodeType);
		
		StreetType streetType = new StreetType();
		streetType.setStreetExtensionText(stringToTextType(streetExtensionText, escapeFlag));
		streetType.setStreetFullText(stringToTextType(streetFullText, escapeFlag));
		
		structuredAddressType.setLocationStreet(streetType);
		
		addressType.setStructuredAddress(structuredAddressType);
		
		return addressType;
	}
	
	private PersonNameType getPersonNameType(String firstName,String lastName, boolean escapeFlag){
		PersonNameType personNameType = new PersonNameType();
		
		PersonNameTextType personNameTextType = new PersonNameTextType();
		personNameTextType.setValue(escapeCharacterHandler(firstName, escapeFlag));
		
		personNameType.setPersonGivenName(personNameTextType);
		
		PersonNameTextType personSurNameTextType = new PersonNameTextType();
		personSurNameTextType.setValue(escapeCharacterHandler(lastName, escapeFlag));
		
		personNameType.setPersonSurName(personSurNameTextType);
		
		return personNameType;
	}
	
	private PlanType getPlanType(List<GroupPlan> groupPlanList,Integer emp_enrollmentId,String enrollmentAction,java.util.Date terminatedOn, boolean escapeFlag) {
		
		PlanType planType = new PlanType();
		List<PlanInformationType> planInformationList = planType.getPlan();
		
		PlanInformationType planInformationType = null;
		PlanDetailRestrictionType planDetailRestrictionType = null;
		
		InsurancePolicyType insurancePolicyType = null;
		InsurancePolicyStatusCodeType insurancePolicyStatusCodeType = null;
		ActuarialValueMetallicTierCodeType actuarialValueMetallicTierCodeType = null;
		
		PlanResponse planResponse =null; 
		
		for (GroupPlan groupPlan : groupPlanList) {
			planResponse = enrollmentExternalRestUtil.getPlanInfo(""+groupPlan.getPlanId(),EnrollmentConstants.ENROLLMENT_TYPE_SHOP);
			//plan = planRepository.findOne(groupPlan.getPlanId());

			if(planResponse != null){

				planInformationType = new PlanInformationType();

				planDetailRestrictionType = new PlanDetailRestrictionType();

				planDetailRestrictionType.setInsurancePlanIdentification(stringToTextType(planResponse.getIssuerPlanNumber(), escapeFlag));

				ProperNameTextType plnProperNameTextType = new ProperNameTextType();
				plnProperNameTextType.setValue(escapeCharacterHandler(planResponse.getPlanName(), escapeFlag));
				planDetailRestrictionType.setInsurancePlanName(plnProperNameTextType);

				planInformationType.setInsurancePlan(planDetailRestrictionType);

				insurancePolicyType = new InsurancePolicyType();

				insurancePolicyType.setInsurancePolicyGroupIdentification(stringToTextType("",escapeFlag));

				planInformationType.setGroupID(insurancePolicyType);

				planInformationType.setCoverageType(stringToTextType("",escapeFlag));

				java.util.Date coverageStartDate = null;
				if(enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_TERM) && emp_enrollmentId != null){
					coverageStartDate = employerEnrollmentRepository.getTermCoverageStartDateByEmployerId(emp_enrollmentId);

				} else if( (enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_INITIAL) || enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_MENT) ) &&  emp_enrollmentId != null){
					coverageStartDate = employerEnrollmentRepository.getCoverageStartDateByEmployerId(emp_enrollmentId);
				}

				planInformationType.setOriginalEffectiveDate(stringToXmlDate(coverageStartDate));

				java.util.Date renewalEffectiveDate = null;
				if(coverageStartDate != null){
					renewalEffectiveDate = DateUtil.addToDate(coverageStartDate,EnrollmentConstants.THREE_SIXTY_FIVE);
				}
				planInformationType.setRenewalEffectiveDate(stringToXmlDate(renewalEffectiveDate));

				planInformationType.setMaintenanceEffectiveDate(stringToXmlDate(new java.util.Date()));

				insurancePolicyStatusCodeType = new InsurancePolicyStatusCodeType();

				if(enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_TERM)){
					insurancePolicyStatusCodeType.setValue(InsurancePolicyStatusCodeSimpleType.T);
					if(terminatedOn != null){
						planInformationType.setMaintenanceEffectiveDate(stringToXmlDate(terminatedOn));				
					}
				}
				else if(enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_INITIAL)){
					insurancePolicyStatusCodeType.setValue(InsurancePolicyStatusCodeSimpleType.A);
				}
				else if(enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_MENT)){
					insurancePolicyStatusCodeType.setValue(InsurancePolicyStatusCodeSimpleType.U);
				}
				planInformationType.setInsurancePolicyStatusCode(insurancePolicyStatusCodeType);

				planInformationType.setOutOfStateIndicator(stringToTextType("", escapeFlag));

				actuarialValueMetallicTierCodeType = new ActuarialValueMetallicTierCodeType();

				//planHealth = plan.getPlanHealth();

				if(planResponse.getPlanLevel() != null && getXMLPlanFromPlanLevel(planResponse.getPlanLevel()) != null){
					actuarialValueMetallicTierCodeType.setValue(getXMLPlanFromPlanLevel(planResponse.getPlanLevel()));
				}

				/*PlanDental planDental = plan.getPlanDental();

				if(planDental != null && getXMLPlanFromPlanLevel(planDental.getPlanLevel()) != null){
					actuarialValueMetallicTierCodeType.setValue(getXMLPlanFromPlanLevel(planDental.getPlanLevel()));
				}*/

				planInformationType.setMetalLevelCode(actuarialValueMetallicTierCodeType);

				planInformationList.add(planInformationType);
			}
		}
		
		if(isMandatoryfieldMissingInPlanType(planType)){
			return null;
		}else{
			return planType;
		}
	}

	private boolean isMandatoryfieldMissingInPlanType(PlanType planType) {
		
		if(planType.getPlan() != null){
			
			List<PlanInformationType> planInfoList = planType.getPlan();
			
			for (PlanInformationType planInformationType : planInfoList) {
				
				if(planInformationType.getInsurancePlan().getInsurancePlanIdentification() == null){
					LOGGER.error("MFM_Error In PlanType Missing field : InsurancePlanIdentification");
					return true;
				}
				if(planInformationType.getInsurancePlan().getInsurancePlanName() == null){
					LOGGER.error("MFM_Error In PlanType Missing field : InsurancePlanName");
					return true;
				}
				if(planInformationType.getOriginalEffectiveDate() == null){
					LOGGER.error("MFM_Error In PlanType Missing field : OriginalEffectiveDate");
					return true;
				}
				if(planInformationType.getRenewalEffectiveDate()== null){
					LOGGER.error("MFM_Error In PlanType Missing field : RenewalEffectiveDate");
					return true;
				}
				if(planInformationType.getInsurancePolicyStatusCode()== null){
					LOGGER.error("MFM_Error In PlanType Missing field : InsurancePolicyStatusCode");
					return true;
				}
				if(planInformationType.getMetalLevelCode()== null){
					LOGGER.error("MFM_Error In PlanType Missing field : MetalLevelCode");
					return true;
				}
			}
		}else{
			LOGGER.error("MFM_Error In PlanType Missing field : Plan");
			return true;
			
		}
		
		return false;
	}

	private ActuarialValueMetallicTierCodeSimpleType getXMLPlanFromPlanLevel(String planLevel) {
		if (planLevel.equals("GOLD")) {

			return ActuarialValueMetallicTierCodeSimpleType.GOLD;
		} else if (planLevel.equals("SILVER")) {

			return ActuarialValueMetallicTierCodeSimpleType.SILVER;
		} else if (planLevel.equals("BRONZE")) {

			return ActuarialValueMetallicTierCodeSimpleType.BRONZE;
		} else if (planLevel.equals("PLATINUM")) {

			return ActuarialValueMetallicTierCodeSimpleType.PLATINUM;
		}else if (planLevel.equalsIgnoreCase("HIGH")) {

			return ActuarialValueMetallicTierCodeSimpleType.HIGH;
		}else if (planLevel.equalsIgnoreCase("LOW")) {

			return ActuarialValueMetallicTierCodeSimpleType.LOW;
		}
		
		return null;// set as blank insted of null HIX-22632
	}

	private BrokerType getBrokerType(Integer brokerId, boolean escapeFlag) {
		
		Broker broker =  brokerRepository.findOne(brokerId);
		
		BrokerType brokerType = new BrokerType();
		
		if(broker != null){
			
			BrokerOrganizationAugmentationType brokerOrganizationAugmentationType = new BrokerOrganizationAugmentationType();
			
			List<TextType> broNameList = brokerOrganizationAugmentationType.getOrganizationLegalName();
			broNameList.add(stringToTextType(broker.getCompanyName(), escapeFlag));
			
		   /*List<QuantityType> broquantityList = brokerOrganizationAugmentationType.getOrganizationMemberQuantity();
			QuantityType broquantity = new QuantityType();
			broquantity.setValue(new BigDecimal(0));
			broquantityList.add(broquantity);	*/
			
			List<TextType> broidentificationTypeList = brokerOrganizationAugmentationType.getTINIdentification();
			broidentificationTypeList.add(stringToTextType(broker.getFederalEIN(),escapeFlag));
			
			brokerType.setBrokerAugmentationType(brokerOrganizationAugmentationType);
			
			brokerType.setAccountNumber(escapeCharacterHandler(broker.getLicenseNumber(), escapeFlag));
			
			SingleContactInformationType singleContactInformationType =  new SingleContactInformationType();
			
			BrokerContactType broemployerContactType = new BrokerContactType();
			
			BrokerPrimaryNumberType brokerPrimaryNumberType = new BrokerPrimaryNumberType();
			
			String broContactNumber = broker.getContactNumber();
			if(broContactNumber != null){broContactNumber = broContactNumber.replace("-", "");}
			
			brokerPrimaryNumberType.setTelephoneNumberFullID(getXMLStringFromString(broContactNumber, escapeFlag));
			
			brokerPrimaryNumberType.setTelephoneSuffixID(getXMLStringFromString("", escapeFlag));
			
			broemployerContactType.setBrokerMainTelephoneNumber(brokerPrimaryNumberType);
			
			EmployerContactNumberInformationType broemployerContactNumberInformationType = new EmployerContactNumberInformationType();
			
			String broAltContactNumber = broker.getAlternatePhoneNumber();
			if(broAltContactNumber != null){broAltContactNumber = broAltContactNumber.replace("-", "");}
			
			broemployerContactNumberInformationType.setTelephoneNumberFullID(getXMLStringFromString(broAltContactNumber, escapeFlag));
			

			broemployerContactType.setAlternateTelephoneNumber(broemployerContactNumberInformationType);
			
			EmployerContactNumberInformationType broemployerFaxNumberInformationType = new EmployerContactNumberInformationType();
			
			String broFaxNumber = broker.getFaxNumber();
			if(broFaxNumber != null){broFaxNumber = broFaxNumber.replace("-", "");}
			
			broemployerFaxNumberInformationType.setTelephoneNumberFullID(getXMLStringFromString(broFaxNumber, escapeFlag));
			
			broemployerContactType.setContactFaxNumber(broemployerFaxNumberInformationType);

			AccountUser userBroker = broker.getUser();
			if(userBroker != null){
				
				singleContactInformationType.setContactPersonName(getPersonNameType(userBroker.getFirstName(),userBroker.getLastName(),escapeFlag));
				
				singleContactInformationType.setContactEmailID(getXMLStringFromString(userBroker.getEmail(), escapeFlag));
			}

			singleContactInformationType.setBrokerContactNumber(broemployerContactType);
			
			PreferredContactModeCodeType preferredContactModeCodeType = new PreferredContactModeCodeType();
			preferredContactModeCodeType.setValue(PreferredContactModeCodeSimpleType.EMAIL);
			
			singleContactInformationType.setPreferredContactMode(preferredContactModeCodeType);
			
			brokerType.setBrokerContactInformation(singleContactInformationType);
			
			Location broLocation = broker.getLocation();
			if(broLocation != null){
				brokerType.setAddress(getAddressType(broLocation.getAddress1(),broLocation.getAddress2(),broLocation.getCity(),getUSStateCodeEnumFromStateString(broLocation.getState()),broLocation.getZip(), escapeFlag));
			}
		}
		
		if(isMandatoryfieldMissingInBrokerType(brokerType)){
			return null;
		}else{
			return brokerType;
		}
		
	}

	private boolean isMandatoryfieldMissingInBrokerType(BrokerType brokerType) {
		
		if(brokerType.getBrokerAugmentationType().getOrganizationLegalName() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : OrganizationLegalName");
			return true;
		}
		if(brokerType.getBrokerAugmentationType().getTINIdentification() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : OrganizationLegalName");
			return true;
		}
		if(brokerType.getBrokerContactInformation().getContactPersonName().getPersonGivenName() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : PersonGivenName");
			return true;
		}
		if(brokerType.getBrokerContactInformation().getContactPersonName().getPersonSurName() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : PersonSurName");
			return true;
		}
		if(brokerType.getBrokerContactInformation().getBrokerContactNumber().getBrokerMainTelephoneNumber().getTelephoneNumberFullID() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : TelephoneNumberFullID");
			return true;
		}
		if(brokerType.getAddress().getStructuredAddress().getLocationStreet().getStreetFullText() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : StreetFullText");
			return true;
		}
		if(brokerType.getAddress().getStructuredAddress().getLocationCityName() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : LocationCityName");
			return true;
		}
		if(brokerType.getAddress().getStructuredAddress().getLocationPostalCode() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : LocationPostalCode");
			return true;
		}
		if(brokerType.getAddress().getStructuredAddress().getLocationStateUSPostalServiceCode() == null){
			LOGGER.error("MFM_Error In BrokerType Missing field : LocationStateUSPostalServiceCode");
			return true;
		}
		
		return false;
	}


	private EmployerType getEmployerType(Integer empId, boolean escapeFlag) {
		
		Employer employer = employerRepository.findOne(empId);
		EmployerType employerType = new EmployerType();
		
		if(employer != null){
			
			EmployerOrganizationAugmentationType employerOrganizationAugmentationType = new EmployerOrganizationAugmentationType();

			List<TextType> orgNameList = employerOrganizationAugmentationType.getOrganizationLegalName();
			orgNameList.add(stringToTextType(employer.getName(),escapeFlag));
			
			List<QuantityType> quantityList = employerOrganizationAugmentationType.getOrganizationMemberQuantity();
			QuantityType quantity = new QuantityType();
			quantity.setValue(new BigDecimal(employer.getTotalEmployees()));
			quantityList.add(quantity);
			
			List<TextType> identificationTypeList = employerOrganizationAugmentationType.getTINIdentification();
			String federalEIN = employer.getFederalEIN();
			if(federalEIN != null){
				identificationTypeList.add(stringToTextType(federalEIN, escapeFlag));
			}
			
			employerType.setEmployerAugmentationType(employerOrganizationAugmentationType); 
			
			employerType.setEmployerID(Integer.toString(employer.getId()));
			
			EmployerCodeType employerCodeType = new EmployerCodeType();
			employerCodeType.setValue(getEmployerCode(employer.getOrgType()));
			employerType.setEmployerTypeCode(employerCodeType);
			
			List<EmployerLocation> employerLocationList = employer.getLocations();
			EmployerLocation prmEmployerLocation = null;
			
			if(employerLocationList != null && !employerLocationList.isEmpty()){
				for (EmployerLocation employerLocation : employerLocationList) {
					
					if(employerLocation.getPrimaryLocation() == EmployerLocation.PrimaryLocation.YES){
						
						prmEmployerLocation = employerLocation;
					}
				}
				
				if(prmEmployerLocation != null && prmEmployerLocation.getLocation() != null){
					
					Location empLocation = prmEmployerLocation.getLocation();
					employerType.setAddress(getAddressType(empLocation.getAddress1(),empLocation.getAddress2(),empLocation.getCity(),getUSStateCodeEnumFromStateString(empLocation.getState()),empLocation.getZip(), escapeFlag));
				}
			}
			
			List<MultiContactInformationType> employerContactInformationList = employerType.getEmployerContactInformation();
			
			MultiContactInformationType multiContactInformationType = new MultiContactInformationType();
			
			ContactCodeType contactCodeType = new ContactCodeType();
			contactCodeType.setValue(ContactCodeSimpleType.BOTH);
			multiContactInformationType.setContactType(contactCodeType);
			
			multiContactInformationType.setContactPersonName(getPersonNameType(employer.getContactFirstName(),employer.getContactLastName(),escapeFlag));
			
			EmployerContactType employerContactType = new EmployerContactType();
			
			EmployerPrimaryNumberType employerPrimaryNumberType = new EmployerPrimaryNumberType();
			
			String empContactNumber = employer.getContactNumber();
			if(empContactNumber != null){empContactNumber = empContactNumber.replace("-", "");}
			
			employerPrimaryNumberType.setTelephoneNumberFullID(getXMLStringFromString(empContactNumber, escapeFlag));
			employerPrimaryNumberType.setTelephoneSuffixID(getXMLStringFromString("", escapeFlag));
			 
			 EmployerContactNumberInformationType employerContactNumberInformationType = new EmployerContactNumberInformationType();

			 employerContactNumberInformationType.setTelephoneNumberFullID(getXMLStringFromString("", escapeFlag));
			 
			 EmployerContactNumberInformationType employerFaxNumberInformationType = new EmployerContactNumberInformationType();

			 employerFaxNumberInformationType.setTelephoneNumberFullID(getXMLStringFromString("", escapeFlag));
			 
			employerContactType.setEmployerMainTelephoneNumber(employerPrimaryNumberType);
			employerContactType.setAlternateTelephoneNumber(employerContactNumberInformationType);
			employerContactType.setContactFaxNumber(employerFaxNumberInformationType);
			
			multiContactInformationType.setEmployerContactNumber(employerContactType);
			
			multiContactInformationType.setContactEmailID(getXMLStringFromString(employer.getContactEmail(), escapeFlag));
			
			PreferredContactModeCodeType preferredContactModeCodeType = new PreferredContactModeCodeType();
			preferredContactModeCodeType.setValue(PreferredContactModeCodeSimpleType.EMAIL);
			
			multiContactInformationType.setPreferredContactMode(preferredContactModeCodeType);
			
			employerContactInformationList.add(multiContactInformationType);
			
			 employerType.setContinuationCoverageType(stringToTextType("", escapeFlag));
			 
			 employerType.setProbationaryPeriod(stringToTextType("", escapeFlag));  
		}
		
		if(isMandatoryfieldMissingInEmployerType(employerType)){
			return null;
		}else{
			return employerType;
		}
	}

	private boolean isMandatoryfieldMissingInEmployerType(EmployerType employerType) {
		if(employerType.getEmployerAugmentationType().getOrganizationLegalName() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : OrganizationLegalName");
			return true;
		}
		if(employerType.getEmployerAugmentationType().getTINIdentification() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : TINIdentification");
			return true;
		}
		if(employerType.getEmployerAugmentationType().getOrganizationMemberQuantity() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : OrganizationMemberQuantity");
			return true;
		}
		if(employerType.getEmployerID() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : EmployerID");
			return true;
		}
		if(employerType.getEmployerTypeCode() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : EmployerTypeCode");
			return true;
		}
		if(employerType.getAddress().getStructuredAddress().getLocationStreet().getStreetFullText() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : (Address)StreetFullText");
			return true;
		}
		if(employerType.getAddress().getStructuredAddress().getLocationCityName() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : (Address)LocationCityName");
			return true;
		}
		if(employerType.getAddress().getStructuredAddress().getLocationStateUSPostalServiceCode() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : (Address)LocationStateUSPostalServiceCode");
			return true;
		}
		if(employerType.getAddress().getStructuredAddress().getLocationPostalCode() == null){
			LOGGER.error("MFM_Error In EmployerType Missing field : (Address)LocationPostalCode");
			return true;
		}
		
		if(employerType.getEmployerContactInformation() != null){
			
			MultiContactInformationType multiContactInformationType = employerType.getEmployerContactInformation().get(0);
			
			if(multiContactInformationType != null && multiContactInformationType.getContactType() == null ){
				LOGGER.error("MFM_Error In EmployerType Missing field : (EmployerContactInformation)ContactType");
				return true;
			}
			if(multiContactInformationType != null && multiContactInformationType.getContactPersonName().getPersonGivenName() == null ){
				LOGGER.error("MFM_Error In EmployerType Missing field : (EmployerContactInformation)PersonGivenName");
				return true;
			}
			if(multiContactInformationType != null && multiContactInformationType.getContactPersonName().getPersonSurName() == null ){
				LOGGER.error("MFM_Error In EmployerType Missing field : (EmployerContactInformation)PersonSurName");
				return true;
			}
			
			if(multiContactInformationType != null && multiContactInformationType.getContactEmailID() == null){
				LOGGER.error("MFM_Error In EmployerType Missing field : (EmployerContactInformation) ContactEmailID");
				return true;
			}
			
			if(multiContactInformationType != null && multiContactInformationType.getPreferredContactMode() == null){
				LOGGER.error("MFM_Error In EmployerType Missing field : (EmployerContactInformation) ContactEmailID");
				return true;
			}
			
			if(multiContactInformationType != null && multiContactInformationType.getEmployerContactNumber().getEmployerMainTelephoneNumber().getTelephoneNumberFullID() == null){
				LOGGER.error("MFM_Error In EmployerType Missing field : (EmployerContactInformation) TelephoneNumberFullID");
				return true;
			}
			
			if(multiContactInformationType != null && multiContactInformationType.getEmployerContactNumber().getEmployerMainTelephoneNumber().getTelephoneSuffixID() == null){
				LOGGER.error("MFM_Error In EmployerType Missing field : (EmployerContactInformation) TelephoneSuffixID");
				return true;
			}
			
		}else{
			LOGGER.error("MFM_Error In EmployerType Missing field : (EmployerContactInformation) EmployerContactInformation");
			return true;
		}
		
		return false;
	}


	private USStateCodeSimpleType getUSStateCodeEnumFromStateString(String stateCode) {
		/*
		 *  Map State String From SHOP Module to  enum USStateCodeSimpleType
		 *  see com.getinsured.hix.platform.util.StateHelper.states
		 */
		return USStateCodeSimpleType.fromValue(stateCode);
	}

	private EmployerCodeSimpleType getEmployerCode(OrgType orgType) {
		
		switch (orgType) {
		
		case CORPORATION: 
			
		return EmployerCodeSimpleType.C_CORP;
			
		case NON_PROFIT: 
			
		return EmployerCodeSimpleType.TAX_EXEMPT_ORGANIZATION;
		
		case PARTNERSHIP: 
			
		return EmployerCodeSimpleType.PARTNERSHIP;
		
		case SOLE_PROPRIETORSHIP: 
			
		return EmployerCodeSimpleType.SELF_EMPLOYED;
		
		default:
			
		return null;
		}
	}

	private FileInformationType getFileInformationType(Integer issuerId,String enrollmentAction,java.util.Date terminatedOn, boolean escapeFlag) {
		FileInformationType fileInformationType = new FileInformationType();
		
		TransmissionMetadataType transmissionMetadataType = new TransmissionMetadataType();
		transmissionMetadataType.setTransmissionID(getXMLStringFromString(UUID.randomUUID().toString(), escapeFlag));
		transmissionMetadataType.setSenderID(getXMLStringFromString(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.SOURCE_EXCHANGE_ID), escapeFlag));
		transmissionMetadataType.setReceiverID(getXMLStringFromString(getHiosIssuerId(issuerId), escapeFlag));
		
		fileInformationType.setFileDetailsMetadata(transmissionMetadataType);
		
		fileInformationType.setPaymentTransactionID(""); //PaymentTransactionID : Not being used in 820 so will not send. Modification from CMS Schema as they require it.
		
		fileInformationType.setMaintenanceEffectiveDate(stringToXmlDate(new java.util.Date()));
		
		if(enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_INITIAL)){
			
			fileInformationType.setEnrollmentAction(EnrollmentActionCodeSimpleType.OPEN_ENROLLMENT);
			
		}else if(enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_MENT)){
			
			fileInformationType.setEnrollmentAction(EnrollmentActionCodeSimpleType.MAINTENANCE);
		
		}else if(enrollmentAction.equals(EnrollmentConstants.GROUP_ACTION_TYPE_TERM)){
			
			fileInformationType.setEnrollmentAction(EnrollmentActionCodeSimpleType.TERMINATE);
			if(terminatedOn != null){
				 fileInformationType.setMaintenanceEffectiveDate(stringToXmlDate(terminatedOn));				
			}
		}
		
		if(isMandatoryfieldMissingInFileType(fileInformationType)){
			return null;
		}else{
			return fileInformationType;
		}
	}
	
	private boolean isMandatoryfieldMissingInFileType(FileInformationType fileInformationType) {
		
		if(fileInformationType.getFileDetailsMetadata().getSenderID() == null || ! isNotNullAndEmpty(fileInformationType.getFileDetailsMetadata().getSenderID())){
			LOGGER.error("MFM_Error In FileInformationType Missing field : SenderID");
			return true;
		}
		return false;
	}


	@Transactional
	@Override
	public void terminateGroupForEmployer(Integer employerId,Integer employer_enrollment_Id,java.util.Date terminationDate) throws GIException{
		try{
			LookupValue groupTermLookUp=null;
			LookupValue groupDepreLookUp=null;
			List<GroupInstallation> groupList= null;
			if(employerId!=null){
				groupTermLookUp= lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_TERM);
				groupDepreLookUp= lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GROUP_ACTION_TYPE, EnrollmentConstants.GROUP_ACTION_TYPE_DEL);
				groupList= groupInstallationRepository.getGroupInstallationByEmployerId(employerId,employer_enrollment_Id);
				if(groupList!=null && !groupList.isEmpty()){
					for(GroupInstallation groupObj:groupList){
						if(groupObj.getGroupData()!=null){
							groupObj.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y);
							groupObj.setEnrollmentActionLkp(groupTermLookUp);
						}else{
							groupObj.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_N);
							groupObj.setEnrollmentActionLkp(groupDepreLookUp);
						}
						groupObj.setTerminatedOn(terminationDate);
						groupInstallationRepository.saveAndFlush(groupObj);
					}
				}
				
			}
			
		}catch(Exception e){
			LOGGER.error("Error in terminateGroupForEmployer()::"+e.toString(),e);
		}
		
	}
	/*@Override
	public List<Integer> getGroups(java.util.Date lastRunDate) throws GIException{
		List<Integer> groupIDList=null;
		TreeSet<Integer> groupSet=null;
		try{
			if(lastRunDate==null){
				groupIDList= groupInstallationRepository.getAllGroupID();
			}else{
				groupSet= new TreeSet<Integer>();
				List<Integer> changedGroup=groupInstallationRepository.getUpdatedGroupID(lastRunDate);
				List<Integer> employerUpdatedGroup= groupInstallationRepository.getEmployerUpdatedGroupID(lastRunDate);
				List<Integer> employerAddressUpdatedGroup= groupInstallationRepository.getEmployerAddressUpdatedGroupID(lastRunDate) ;
				List<Integer> brokerUpdatedGroup= groupInstallationRepository.getBrokerUpdatedGroupID(lastRunDate);
				List<Integer> brokerAddressUpdatedGroup= groupInstallationRepository.getBrokerAddressUpdatedGroupID(lastRunDate) ;
				
				if(changedGroup!=null && changedGroup.size()>0){
					groupSet.addAll(changedGroup);
				}
				if(employerUpdatedGroup!=null && employerUpdatedGroup.size()>0){
					groupSet.addAll(employerUpdatedGroup);
				}
				if(employerAddressUpdatedGroup!=null && employerAddressUpdatedGroup.size()>0){
					groupSet.addAll(employerAddressUpdatedGroup);
				}
				if(brokerUpdatedGroup!=null && brokerUpdatedGroup.size()>0 ){
					groupSet.addAll(brokerUpdatedGroup);
				}
				if(brokerAddressUpdatedGroup!=null && brokerAddressUpdatedGroup.size()>0){
					groupSet.addAll(brokerAddressUpdatedGroup);
				}
				
				groupIDList = new ArrayList<Integer>(groupSet);
			}
			
		}catch(Exception e){
			LOGGER.error("Error in getGroups::"+e.toString(),e);
		}
		return groupIDList;
	}*/
	
	private Status getEmployerPaidStatus(Integer employer_enrollment_id){
		
		//EmployerEnrollment employerEnrollment = employerEnrollmentRepository.checkStatusActive(employerId);
		EmployerEnrollment employerEnrollment = employerEnrollmentRepository.findOne(employer_enrollment_id);
		
		if(employerEnrollment != null && employerEnrollment.getStatus().equals(Status.ACTIVE)){
			
			return employerEnrollment.getStatus();
		
		}else{
			return null;
		}
	}
}
