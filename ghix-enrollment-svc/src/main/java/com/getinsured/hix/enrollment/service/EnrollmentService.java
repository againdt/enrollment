/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getinsured.hix.dto.bb.BenefitBayRequestDTO;
import com.getinsured.hix.dto.enrollment.Enrollment834ResendDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCoverageValidationRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentEventHistoryDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMailingAddressUpdateRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentStatusUpdateDto;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.hix.dto.enrollment.MonthlyAPTCAmountDTO;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentDTO;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentMemberDTO;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.request.ssap.SsapApplicationResponse;


/**
 * @author panda_p
 * @since 13/02/2013
 */
public interface EnrollmentService {
	/**
	 * Return Enrollment for provided id
	 * 
	 * @param id
	 * @return Enrollment
	 */
	Enrollment findById(Integer id);
	
	
	String findEnrollmentStatusByEmployeeId(Integer employeeId);
	
	/**
	 * Save / Update Enrollment in database
	 * @param enrollment
	 * @return
	 */
	Enrollment saveEnrollment(Enrollment enrollment);
	
	/**
	 * This methode return list of Enrollment which are updated since last carrier update.
	 * 
	 * @return List of Enrollment
	 */
	List<Enrollment> getCarrierUpdatedEnrollments();
	
	/**
	 * 
	 * @param searchCriteria
	 * @return
	 */
	Map<String,Object> searchEnrollment(Map<String,Object> searchCriteria);
	
	/**
	 * Return List of Enrollment with given Employer
	 * 
	 * @param employer
	 * @return
	 */
	List<Enrollment> findEnrollmentByEmployer(Employer employer);
	
	/**
	 * Return List of Enrollment with given Employer and employerEnrollmentId
	 * @param employer
	 * @param employerEnrollmentId
	 * @return
	 */
	List<Enrollment> findByEmployerAndEmployerEnrollmentId(int employerId,Integer employerEnrollmentId);
	
	/*
	 * 
	 */
	List<Enrollment> findByEmployerAndEmployerEnrollmentIdAndDate(int employerId,Integer employerEnrollmentId, Date terminationDate);
	/**
	 * 
	 * @param employerId
	 * @return
	 */
	List<Object[]> findEnrollmentByEmployerId(int employerId, Integer employerEnrollmentId);
	
	/**
	 * @param inputval
	 * @return
	 */
	List<Object[]> findCurrentMonthEffectiveEnrollment(Map<String, Object> inputval);
	
	/**
	 * 
	 * @param inputval
	 * @return
	 */
	List<Enrollment> findEnrollmentByStatusAndEmployerAndTerminationdate(Map<String, Object> inputval);
	
	/**
	 * 
	 * @param inputval
	 * @return
	 */
	List<Enrollment> findEnrollmentByStatusAndTerminationdate(Map<String, Object> inputval);
		
	/**
	 * Change status of Enrollment from given Employer to Confirmed
	 * 
	 * @author meher_a, raja
	 * @param EnrollmentRequest
	 * 
	 * @author panda_p
	 * Updated return type
	 * @return  EnrollmentResponse
	 */
	EnrollmentResponse updateShopEnrollmentStatus(EnrollmentRequest enrollmentRequest);
	
		
	/**
	 * 
	 * @param planName
	 * @param planLevel
	 * @param planMarket
	 * @param regionName
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Enrollment> findEnrollmentsByPlan(String planId, String planLevel, String planMarket, String regionName, String startDate, String endDate, String status);
	
	/**
	 * 
	 * @param issuerName
	 * @param planLevel
	 * @param planMarket
	 * @param regionName
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Enrollment> findEnrollmentsByIssuer(String issuerName, String planLevel, String planMarket, String regionName, String startDate, String endDate, String status);
	
	/**
	 * Used bi IND-29
	 * @param externalID
	 * @return Enrollment List
	 */
	List<Enrollment> findEnrollmentByExternalID(String externalID);
	
		
	/**
	 * @author raja
	 * @since 28 MAY 2013
	 *
	 * This methods takes the input as enrollment id and  returns the planId and ind_order_items_id for the enrollment 
	 * 
	 * @param enrollmentId
	 * @return it returns Map<String,Object>.
	 */
	Map<String,Object> getPlanIdAndOrderItemIdByEnrollmentId(Integer enrollmentId);
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to create event for a particular Enrollee
	 * @param enrollee
	 * @param eventType
	 * @param eventReasonCode
	 * @param user
	 */
	void createEventForEnrollee(Enrollee enrollee,String eventType ,String eventReasonCode, AccountUser user, boolean isSpecial);
	
	
	/**
	 * @author parhi_s
	 * @since
	 * 
	 * This method is used to update the status of the Enrollment when any enrollee status gets changed
	 * 
	 * @param enrollment
	 * @param user
	 */
	Enrollment checkEnrollmentStatus( AccountUser user, Enrollment enrollment);
	
	/**
	 * @author parhi_s
	 * @since
	 * 
	 * This method is used to update the status of the Enrollment when any enrollee status gets changed
	 * 
	 * @param enrollment
	 * @param user
	 */
	void checkEnrollmentStatus(Enrollment enrollment, AccountUser user);
	
	/**
	 * 
	 * @param userName
	 * @return
	 */
	AccountUser getUserByName(String userName);
	
	/**
	 *  @author aditya_s
	 *  @since 22/07/2013
	 *  This method returns list of {@link EnrollmentCountDTO} for given assisterBrokerID, role, startDate and endDate.
	 * 
	 * @param assisterBrokerId
	 * @param role
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<EnrollmentCountDTO> findEnrollmentPlanLevelCountByBroker(Integer assisterBrokerId, String role, Date startDate, Date endDate);
	
	List<Employer> findDistinctEmployer();
	
	/**
	 * Used to abort created enrollments.
	 */
	void abortEnrollment(Enrollment enrollment);
	
	/**
	 * 
	 * @author Sharma_k
	 * @since 25th August 2013
	 * @param enrollmentId
	 * @return
	 */
	boolean isEnrollmentExists(int enrollmentId);

	/**
	 * Returns list of Enrollment for provided CMR_Household_Id.
	 * @param cmrHouseholdId
	 * @return
	 */
	List<Enrollment> findEnrollmentByCmrHouseholdId(int cmrHouseholdId);
	
	/**
	 * @author Sharma_k
	 * @since 27th September 2013
	 * @param enrollmentId
	 * @return
	 * service call written to fetch remittance related data.
	 */
	List<Object[]> getRemittanceDataByEnrollmentId(Integer enrollmentId);
	
	/**
	 * Returns the active Enrollments for a particular Employee Application ID
	 * @author parhi_s
	 * @since
	 * 
	 * @param employeeAppId
	 * @return
	 */
	List<Enrollment> findActiveEnrollmentByEmployeeApplicationID(List<Integer> employeeAppIdList, Date terminationDate);
	
	/**
	 * @author Pratap
	 * @since 22/10/2013
	 * @param enrollmentResponse
	 * @return String
	 */
	BenefitBayRequestDTO populateBenefitBayRequestDTO(
			EnrollmentResponse enrollmentResponse);
	/**
	 * @author rajaramesh_g
	 * @since 04/03/2014
	 * @param start date
	 * @param end date
	 * @return 
	 * service call written to fetch all the employer in between start date and end date.
	 */
	List<Object[]> findEmployersByStartDateAndEndDate(Date startDate, Date endDate);

/**
	 * Returns events for all Enrollees provided an employerId and start and end date
	 * @author shanbhag_a
	 * @since 5/03/2013
	 * @return
	 */
	EnrollmentResponse getShopEnrollmentLog(EnrollmentRequest enrollmentRequest);
	
	/**
	 * 
	 * @param employerID
	 * @return
	 */
	Date findEarliestEffStartDate(Integer employerID);
	
	
	/**
	 * This method returns true if there is any active enrollment else return false
	 * @param issuerID
	 * @return
	 */
	boolean isIssuerActive(Integer issuerID);

	/**
	 * Returns list of active enrollment Ids for given employer 
	 * 
	 * @param inputval
	 * @return
	 */
	List<Integer> findActiveEnrollmentsForEmployer(Map<String, Object> inputval);
	
	/**
	 * This method finds list of enrollments based on employee id and enrollment id
	 * @param employeeId based on this variable enrollments will be searched
	 * @return
	 */
	List<Object[]> findEnrollmentByEmployeeIdAndEnrollmentId(Integer employeeId,List<Integer> enrollmentId);
	
	/**
	 * This method finds coverage EndDate of enrollments based on Employee Application ID
	 * @param employeeId
	 * @return
	 */
	String findCoverageEndDateByEmployeeApplicationID(Long employeeId);
	
	/**
	 * Returns the active Enrollments for a particular Employee ID
	 * @param employeeId
	 * @return
	 */
	List<Enrollment> findActiveEnrollmentByEmployeeID(Integer employeeId, Date terminationDate);
	
	/**
	 * Returns the active enrollments for the given SSAP Application ID
	 * @author negi_s
	 * @param ssapApplicationId
	 * @return
	 */
	List<Enrollment> findActiveEnrollmentBySsapApplicationID(Long ssapApplicationId);

	/**
	 * Returns the active enrollments for the given Household Case ID
	 * @author bradley
	 * @param householdCaseId
	 * @return
	 */
	List<Enrollment> findActiveEnrollmentByHouseholdCaseId(Long householdCaseId);
	
	/**
	 * Return Enrollment for provided id
	 * 
	 * @param id
	 * @return Enrollment
	 */
	List<Enrollment> findBySsapApplicationid(Long ssapAppId);

	/**
	 * @since 10-30-2014
	 * @author Aditya-S
	 * @param enrollee
	 * @param maintReasonCode
	 * @param user
	 * 
	 * This method is Called to create EnrollmentEvent during during Inbound Process
	 * 
	 */
	void createEnrolleeEvent(Enrollee enrollee, String eventType,String eventReasonCode, AccountUser user,  boolean sendToCarrier, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier);
	/**
	 * @author panda_p
	 * @since 27-Jun-2016
	 * @param enrollee
	 * @param eventType
	 * @param eventReasonCode
	 * @param user
	 * @param sendToCarrier
	 * @param isSpecial
	 * @param txn_idfier
	 */
	void createEnrolleeEvent(Enrollee enrollee,String eventType ,String eventReasonCode, AccountUser user, boolean sendToCarrier, boolean isSpecial, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier);
	/**
	 * @since 01-11-2014
	 * @author Aditya-S
	 * 
	 * Used to Dis-Enroll List fo Enrollments.
	 * 
	 * @param disEnrollmentDTO
	 * @return
	 * @throws GIException
	 */
	List<Enrollment> disEnrollEnrollment(EnrollmentDisEnrollmentDTO disEnrollmentDTO, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) throws GIException;

	/**
	 * @Since 26-Jul-2013
	 * @author panda_pratap
	 * @return Logged In AccountUser
	 */
	AccountUser getLoggedInUser();
	
	/**
	 * @Since 04-Nov-2014
	 * @author Aditya-S
	 * 
	 * Returns all enrollment for given enrollmentIds
	 * 
	 * @param enrollmentIds
	 * @return
	 */
	List<Enrollment> findAllEnrollments(List<Integer> enrollmentIds);

	/**
	 * @author Aditya-S
	 * @since 27-11-2014
	 * 
	 * @param enrollmentList
	 * @return
	 * @throws GIException
	 */
	List<Enrollment> saveAllEnrollment(List<Enrollment> enrollmentList);
			//throws GIException;
	
	/**
	 * 
	 * @author Aditya-S
	 * @since  12-12-2014
	 * 
	 * Used to Update Enrollment APTC. (Gross - UpdatedAptc)
	 * Re-Calculated Net Premium Amount, Creates Enrollment-Event with Reason as No Reason Given.
	 * 
	 * @param enrollmentRequest
	 * @return
	 * @throws GIException 
	 */
	EnrollmentResponse updateEnrollmentAptc(EnrollmentRequest enrollmentRequest) throws Exception;

	/**
	 * @author Aditya-S
	 * @since 29-01-2015
	 * 
	 * @param applicationId
	 * @param useLookBackLogic 
	 * @return
	 */
	boolean isActiveEnrollmentForApplicationId(Long applicationId, boolean useLookBackLogic);
	
	/**
	 * @author Pratap
	 * @since 31-07-2015
	 * 
	 * @param ssapApplicationRequest
	 * @return
	 */
	boolean isActiveEnrollmentForApplicationId(Long applicationId, String coverageYear);
	
	/**
	 * @author parhi_s
	 * @since 25-02-2015
	 * @param brokerReq
	 * @param response
	 */
	void updateBrokerDetails(EnrollmentBrokerUpdateDTO brokerReq, EnrollmentResponse response);

	/*
	 * 
	 */
	List<Enrollee> getEnrolleesAndSubscriber(Enrollment enrollment);
	List<Enrollee> getEnrolleesOnly(Enrollment enrollment);


	EnrollmentResponse findEmployeeIdAndPlanId(EnrollmentRequest enrollmentRequest);
	
	EnrollmentResponse getEnrollmentDataForBroker(String enrollmentId);
	
	/**
	 * Jira Id: HIX-70358
	 * @author Sharma_k
	 * @since 25th June 2015
	 * @param houseHoldCaseIdList List<String>
	 * @return List<Object[]>
	 * Service to fetch Enrollment data by requested List of houseHoldCaseIds
	 */
	Map<String, List<EnrollmentDataDTO>> getEnrollmentDataByHouseHoldCaseIds (EnrollmentRequest enrollmentRequest)throws GIException;
	
	/**
	 * Jira Id: 
	 * @param enrollmentRequest
	 * @return
	 * @throws GIException
	 */
	Map<String, List<EnrollmentDataDTO>> getEnrollmentDataForBrokerByHouseHoldCaseIds (EnrollmentRequest enrollmentRequest)throws GIException;
	
	/**
	 * Jira ID: HIX-72324 [Enrollment API for Renewal]
	 * @since 21st July 2015
	 * @author Sharma_k
	 * @param ssapApplicationId Long
	 * @return List<Object<EnrollmentDataDTO>>
	 * Service to fetch Enrollment data by given SSAP_application_Id
	 */
	List<EnrollmentDataDTO> getEnrollmentDataBySsapApplicationId(Long ssapApplicationId);
	
	
	/**
	 * Jira ID: HIX-108666 [API to return the insurance count]
	 * @since 06th August 2018
	 * @param ssapApplicationd
	 * @return Map<String,Integer>
	 * Service to return the Health and Dental Enrollment Count
	 * 
	 */
	Map<String,Integer> getAllEnrollmentDataBySsapApplicationId(Long ssapApplicationd);

	
	/**
	 * To Fetch enrollment by SSAP_Application_ID where enrollment status not in CANCEL or ABORT
	 * Jira ID: HIX-78557 
	 * @since 02nd November 2015
	 * @author Sharma_K
	 * @param ssapApplicationId
	 * @return List<Object<Enrollment>>
	 */
	List<Enrollment> getEnrollmentNotInCancelAndAbortBySsapApplicationId(Long ssapApplicationId);
	
	/**
	 * To update enrollment status where enrollment status is PENDING or TERM/CANCEL
	 * Jira ID: HIX-79398 
	 * @since 17th Nov 2015
	 * @author shinde_dh
	 * @param enrollmentId
	 * @return 
	 */
	void updateEnrollmentStatus (EnrollmentStatusUpdateDto enrollmentStatusUpdateRequest ,EnrollmentResponse enrollmentResponse, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) throws GIException;
	
	/**
	 * @since4th Jan 2016
	 * @author parhi_s
	 * @param enrollment834ResendDTO
	 * @param enrollmentResponse
	 * @throws GIException
	 */
	void updateEnrollmentResend(Enrollment834ResendDTO enrollment834ResendDTO,EnrollmentResponse enrollmentResponse) throws GIException;
	
	/**
	 * terminate enrollment with given plan id, coverage year and having end date less than decertified_date
	 * @param decertified_plan_id
	 * @param coverage_year
	 * @param decertified_date
	 * @return 
	 */
	EnrollmentResponse terminateEnrollmentByPlanId(String decertified_plan_id, String coverage_year, String decertified_date);
	
	/**
	 * count enrollment with given plan id, coverage year and having end date less than decertified_date
	 * @param cmsPlanId
	 * @param coverageYear
	 * @param terminationDate
	 * @return
	 */
	EnrollmentResponse countEnrollmentByPlanId(String cmsPlanId, String coverageYear, String terminationDate);
	
	
	/**
	 * Update Mailing Address for given HouseHold id
	 * @param inputval
	 */
	EnrollmentResponse updateMailByHouseHoldId(String householdCaseId, Long locationID);
	
	/**
	 * Validate enrollment overlap scenario using member Ids
	 * @param request
	 * @return
	 */
	boolean validateEnrollmentOverlap(EnrollmentCoverageValidationRequest request);

	List<MonthlyAPTCAmountDTO> getMonthlyAPTCAmount(List<Integer> idList);


	PlanResponse getPlanInfo(String planId, String marketType);


	void updateEnrollment(EnrollmentUpdateDTO enrollmentUpdateDto, EnrollmentResponse response, Integer giWsPayloadId) throws GIException;
	
	List<Enrollee> getHouseholdContactResponsiblePerson(String memberId, List<Enrollee> enrollees);


	EnrollmentDTO getEnrollmentAtributeById(EnrollmentRequest enrollmentRequest) throws GIException;
	
	/**
	 * 
	 * @param enrollmentRequest
	 * @return
	 * @throws GIException
	 */
	Map<String, List<EnrollmentDataDTO>> getEnrollmentDataByIdAndHouseholdCaseIds(EnrollmentRequest enrollmentRequest)throws GIException;
	
	/**
	 * @param healthEnrollment
	 * @param loggedInUser
	 * @return
	 * @throws GIException
	 */
	List<Enrollment> updateDentalAPTCOnHealthDisEnrollment(Enrollment healthEnrollment, AccountUser loggedInUser,  EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) throws GIException ;
	
	AccountUser retreiveLoggedInUserByEmail(final String loggedInUserEmail, EnrollmentResponse enrollmentResponse)throws GIException;
	
	EnrollmentCoverageValidationRequest prepareCoverageValidationRequest(EnrollmentUpdateDTO enrollmentUpdateDto, Enrollment enrollmentDb);
	
	/**
	 * 
	 * @param Object<EnrollmentRequest> enrollmentRequest
	 * @return
	 */
	Map<String, List<EnrollmentMemberDataDTO>> getEnrollmentMemberDataDTO(final EnrollmentRequest enrollmentRequest);
	
	/**
	 * 
	 * @param mailingUpdateRequest
	 * @return
	 */
	EnrollmentResponse updateMailByHouseHoldId(EnrollmentMailingAddressUpdateRequest mailingUpdateRequest, EnrollmentResponse response);


	List<Enrollment> synchronizeQuotingDates(Enrollment enrollment, AccountUser user, EnrollmentEvent.TRANSACTION_IDENTIFIER txnIdentifier);
	
	
	List<EnrollmentDataDTO> getEnrollmentSnapshot(EnrollmentRequest enrollmentRequest);


	List<EnrollmentEventHistoryDTO> getAllSubscriberEvents(EnrollmentRequest enrollmentRequest);


	Map<String, List<EnrollmentDataDTO>> getEnrollmentDataByExternalCaseIds(EnrollmentRequest enrollmentRequest) throws GIException;
	
	Set<String> updateSsapApplicationId(EnrollmentMemberDTO enrollmentMemberDTO) throws GIException;

	/**
	 * Fetch creation date of latest enrollment by ssap ID
	 * @param ssapApplicationId
	 * @return EnrollmentResponse
	 * @throws GIException
	 */
	EnrollmentResponse fetchLatestEnrollmentCreationTimeBySsapId(Long ssapApplicationId) throws GIException;

	/**
	 * Fetch effective daye of latest enrollment by ssap ID
	 * @param ssapApplicationId
	 * @return EnrollmentResponse
	 * @throws GIException
	 */
	EnrollmentResponse fetchLatestEnrollmentEffectiveDateBySsapId(Long ssapApplicationId) throws GIException;
	
	List<Enrollment> findActiveEnrollmentBySsapApplicationIdAndPlanType(Long ssapApplicationId, String planType);
	
	/**
	 * Jira Id: HIX-110275
	 * Api to getHiosIdForMnsureIndvId
	 * @author Anoop
	 * @since 13th Feb 2019
	 * @param externalIndivId
	 * @return EnrollmentResponse
	 */
	EnrollmentResponse findHiosIdForEnrollment(String externalIndivId, String coverageYear) throws GIException;
	Map<String, List<EnrollmentWithMemberDataDTO>> getEnrollmentDataForMembers(EnrollmentRequest enrollmentRequest) throws GIException;

	List<Integer> getEnrollmentIdListByCoverageYearForQHPReports(Integer coverageYear);

	/**
	 * HIX-115619 Get renewal status map
	 * @param externalCaseIdList
	 * @return Map
	 */
	Map<String, String> getRenewalStatusMapByExternalCaseIds(List<String> externalCaseIdList, String year);

	public SsapApplicationResponse updateIndividualStatusForCancel(List<Enrollment> enrollmentList, boolean isEnrolleeUpdateByEditTool) throws GIException;

	public EnrollmentResponse updateIndividualStatusForReinstatementOrAdd(List<Enrollment> enrollmentList, EnrollmentResponse enrollmentResponse);

}
