package com.getinsured.hix.enrollment.service.carrierfeed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

enum GIStatus{
	ESIG_PENDING,
	PENDING,
	WITHDRAWN,
	DECLINED,
	APPROVED,
	SOLD,
	INFORCE,
	CANCEL, 
	TERM,    
	CONFIRM,
	ORDER_CONFIRMED,
	PAYMENT_RECEIVED,
	ABORTED,
	ECOMMITTED,
	UNKNOWN
}

public class AetnaCarrierFeedMappings {

	static Map<String,GIStatus> mapCarrier2GIStatus = new HashMap<String, GIStatus>();
	static Map<String, String> mapFollowup = new HashMap<String, String>();
	static Map<String, GIStatus> mapIgnoreStatus = new HashMap<String, GIStatus>();
	static List<String> listRateUp = new ArrayList<String>();
	static Map<String, String> mapUWComments = new HashMap<String, String>();
    
	static{

	mapCarrier2GIStatus.put("pending applicant completion", GIStatus.ESIG_PENDING);
    mapCarrier2GIStatus.put("received by aetna", GIStatus.PENDING);
    mapCarrier2GIStatus.put("pending", GIStatus.PENDING);
    mapCarrier2GIStatus.put("pended for agent endorsement", GIStatus.PENDING);
    mapCarrier2GIStatus.put("pending initial review", GIStatus.PENDING);
    mapCarrier2GIStatus.put("pending rate acceptance", GIStatus.PENDING);
    mapCarrier2GIStatus.put("application returned to client", GIStatus.WITHDRAWN);
    mapCarrier2GIStatus.put("approved with rate-up - pending applicant response",GIStatus.PENDING);
    mapCarrier2GIStatus.put("medical underwriting in progress", GIStatus.PENDING);
    mapCarrier2GIStatus.put("pending incomplete information", GIStatus.PENDING);
    mapCarrier2GIStatus.put("no interest", GIStatus.WITHDRAWN);
    mapCarrier2GIStatus.put("application received", GIStatus.PENDING);
    mapCarrier2GIStatus.put("application logged", GIStatus.PENDING);
    mapCarrier2GIStatus.put("usa received", GIStatus.PENDING);
    mapCarrier2GIStatus.put("usa pended for incomplete info",GIStatus.PENDING);
    mapCarrier2GIStatus.put("medical underwriting", GIStatus.PENDING);
    mapCarrier2GIStatus.put("uw pended", GIStatus.PENDING);
    mapCarrier2GIStatus.put("withdraw", GIStatus.WITHDRAWN);
    mapCarrier2GIStatus.put("withdrawn", GIStatus.WITHDRAWN);
    mapCarrier2GIStatus.put("closed", GIStatus.WITHDRAWN);
    mapCarrier2GIStatus.put("declined", GIStatus.DECLINED);
    mapCarrier2GIStatus.put("approved", GIStatus.APPROVED);
    mapCarrier2GIStatus.put("approved with rate up", GIStatus.APPROVED);
    mapCarrier2GIStatus.put("approved with rate-up", GIStatus.APPROVED);
    mapCarrier2GIStatus.put("usa pend for incomplete info",  GIStatus.PENDING);
    mapCarrier2GIStatus.put("approved with rate-up - pending response", GIStatus.APPROVED);
    mapCarrier2GIStatus.put("processing completed- mixed status" , GIStatus.APPROVED);
    mapCarrier2GIStatus.put("pend for agent signature", GIStatus.PENDING);
    mapCarrier2GIStatus.put("pended for agent endorsement", GIStatus.PENDING);
    mapCarrier2GIStatus.put("application in progress", GIStatus.PENDING);
    mapCarrier2GIStatus.put("pended for agent esignature", GIStatus.PENDING);
    mapCarrier2GIStatus.put("application submitted online", GIStatus.PENDING);
    mapCarrier2GIStatus.put("awaiting acceptance-approved w/rate up", GIStatus.PENDING);
    mapCarrier2GIStatus.put("acknowledgement sent to aetna", GIStatus.PENDING);
    mapCarrier2GIStatus.put("enrollment form mailed", GIStatus.PENDING);
    mapCarrier2GIStatus.put("enrollment form emailed", GIStatus.PENDING);
    mapCarrier2GIStatus.put("existing business change", GIStatus.PENDING);
    mapCarrier2GIStatus.put("pended for medical underwriting", GIStatus.PENDING);
    mapCarrier2GIStatus.put("approved with rate-up - mixed status", GIStatus.APPROVED);
    mapCarrier2GIStatus.put("declined - mixed status", GIStatus.DECLINED);
    mapCarrier2GIStatus.put("approved - mixed status", GIStatus.APPROVED);
    mapCarrier2GIStatus.put("pended for incomplete information", GIStatus.PENDING);
	
	
    mapIgnoreStatus.put("plan selected", GIStatus.SOLD);
    mapIgnoreStatus.put("application downloaded", GIStatus.SOLD);
    mapIgnoreStatus.put("application sent", GIStatus.SOLD);
    mapIgnoreStatus.put("active", GIStatus.INFORCE);
    mapIgnoreStatus.put("application started online", GIStatus.SOLD);
    mapIgnoreStatus.put("quotes", GIStatus.SOLD);
    mapIgnoreStatus.put("pended for agent endorsement", GIStatus.PENDING);
    
    listRateUp.add("approved with rate up");
    listRateUp.add("approved with rate-up");
    listRateUp.add("awaiting acceptance-approved w/rate up");
    listRateUp.add("pended for medical records -call 866-772-6331");
    
    
    mapUWComments.put("medical information not received", "FOLLOWUP NEEDED");
    mapUWComments.put("we are closing your case per your request", "FOLLOWUP NEEDED"); 
    mapUWComments.put("health questionnaire inconsistent with claims", "FOLLOWUP NEEDED"); 
    mapUWComments.put("pended for medical records -call 866-772-6331", "CLIENT NEEDS APS"); 
    mapUWComments.put("aps requested - call 866-772-6331", "CLIENT NEEDS APS");
    mapUWComments.put("pending aps.", "CLIENT NEEDS APS");
    mapUWComments.put("u.s. residency", "FOLLOWUP NEEDED");
    mapUWComments.put("pending phone interview", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("telephone interview requested - call 866-747-5240", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("pending telephone interview review", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("follow up information never received", "FOLLOWUP NEEDED");
    mapUWComments.put("pending phone interviews for all apps", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("sent for interview.", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("sent for interview", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("sent for intervew on app", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("sent for phone inter", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("pend for telephone interview", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("pend for interview", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("requested records.", "FOLLOWUP NEEDED");
    mapUWComments.put("phone interview", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("sent for interview on app", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("pend for phone interview.", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("pended for phone interview - call 866-747-5240", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("pend for phone interview", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("pend for aps", "CLIENT NEEDS APS");
    mapUWComments.put("plan selection is missing", "FOLLOWUP NEEDED");
    mapUWComments.put("pending aps review", "MEDICAL RECORDS ORDERED");
    mapUWComments.put("med recs now needed for", "MEDICAL RECORDS ORDERED");
    mapUWComments.put("phone interview for all", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("sent for interview on all", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("send for pu", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("send pu", "CLIENT NEEDS TO CALL CARRIER");
    mapUWComments.put("sent all for interview.", "CLIENT NEEDS TO CALL CARRIER"); 
    mapUWComments.put("check not received", "FOLLOWUP NEEDED"); 
    mapUWComments.put("complete unanswered questions", "FOLLOWUP NEEDED"); 
    mapUWComments.put("incomplete application", "FOLLOWUP NEEDED");  
    mapUWComments.put("not signed and dated", "FOLLOWUP NEEDED"); 
    
    mapFollowup.put("usa pend for incomplete info", "FOLLOWUP NEEDED");  
    mapFollowup.put("approved with rate up", "APP APPROVED WITH RATEUP");  
    mapFollowup.put("approved with rate-up", "APP APPROVED WITH RATEUP");  
    mapFollowup.put("pend for agent signature", "FOLLOWUP NEEDED"); 
    mapFollowup.put("pended for agent endorsement", "FOLLOWUP NEEDED");  
    mapFollowup.put("enrollment form mailed", "FOLLOWUP NEEDED");  
    mapFollowup.put("enrollment form emailed", "FOLLOWUP NEEDED");  
    mapFollowup.put("awaiting acceptance-approved w/rate up", "APP APPROVED WITH RATEUP");  
    mapFollowup.put("existing business change", "FOLLOWUP NEEDED"); 
    mapFollowup.put("approved with rate-up - mixed status", "FOLLOWUP NEEDED"); 
    mapFollowup.put("approved - mixed status", "FOLLOWUP NEEDED"); 
    mapFollowup.put("pended for incomplete information", "FOLLOWUP NEEDED"); 
	
	}
	
}
