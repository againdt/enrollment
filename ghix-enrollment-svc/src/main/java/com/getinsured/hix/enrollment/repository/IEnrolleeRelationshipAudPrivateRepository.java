package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrolleeRelationshipAud;

public interface IEnrolleeRelationshipAudPrivateRepository extends JpaRepository<EnrolleeRelationshipAud, Integer> {
	@Query(" FROM EnrolleeRelationshipAud as en " +
			" WHERE en.sourceEnrollee.id = :sourceID " +
			" AND en.targetEnrollee.id = :targetID and en.rev = :rev")
	EnrolleeRelationshipAud findrelationShipBySourceTargetRev(
			@Param("sourceID") Integer sourceID,
			@Param("targetID") Integer targetID, @Param("rev") Integer rev);

}
