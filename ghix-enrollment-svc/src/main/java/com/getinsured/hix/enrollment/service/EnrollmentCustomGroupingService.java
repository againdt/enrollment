package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.dto.enrollment.EnrollmentCustomGroupDTO;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;

public interface EnrollmentCustomGroupingService {
	
	PldOrderResponse getPDOrderData(EnrollmentCustomGroupDTO enrollmentCustomGroupDTO, EnrollmentResponse response)throws Exception;

}
