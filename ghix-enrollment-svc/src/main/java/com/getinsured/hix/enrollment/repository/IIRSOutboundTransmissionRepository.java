/**
 * 
 */
package com.getinsured.hix.enrollment.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.getinsured.hix.model.enrollment.IRSOutboundTransmission;;

/**
 * @author Priya C
 *
 */
@Repository
@Transactional
public interface IIRSOutboundTransmissionRepository extends JpaRepository<IRSOutboundTransmission, Integer>{
	@Query("FROM IRSOutboundTransmission as irs where irs.batchId =:batchId")
	List<IRSOutboundTransmission> findIRSOutboundTransmissionByBatchId(@Param("batchId") String batchId);

	@Query("FROM IRSOutboundTransmission as irs where irs.documentFileName =:documentFileName")
	IRSOutboundTransmission findIRSOutboundTransmissionByFileName(@Param("documentFileName") String documentFileName);
	
	@Query("FROM IRSOutboundTransmission as irs where irs.documentSeqId =:documentSeqId and  irs.batchId =:batchId")
	IRSOutboundTransmission findIRSOutboundTransmissionByDocumentSeqId(@Param("documentSeqId") String documentSeqId,@Param("batchId") String batchId);
	

}
