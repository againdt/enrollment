package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAdminEffactuationRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentAdminEffactuation;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("enrollmentAdminEffectuationService")
@Transactional
public class EnrollmentAdminEffectuationServiceImpl  implements EnrollmentAdminEffectuationService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAdminEffectuationServiceImpl.class);
	
	@Autowired private UserService userService;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private LookupService lookupService;
	@Autowired private EnrollmentAhbxSyncService enrollmentAhbxSyncService;
	@Autowired	private IEnrollmentRepository enrollmentRepository;
	@Autowired	private IEnrollmentPremiumRepository enrollmentPremiumRepository;
	@Autowired private IEnrollmentAdminEffactuationRepository enrollmentAdminEffactuationRepository;
	@Autowired private EnrollmentRequotingService enrollmentRequotingService;
	
	@Transactional
	@Override
	public void insertAdminEffactuation(List<String[]> effectuationList, String fileName, String processingStatus, String errorMessage ) {
		List<EnrollmentAdminEffactuation> adminEffactuationList=null;
			if(effectuationList!=null && effectuationList.size()>0){
				adminEffactuationList=new ArrayList<>();
				
				for (String[] strings :effectuationList){
					EnrollmentAdminEffactuation eae= new EnrollmentAdminEffactuation();
					eae.setFileName(fileName);
					eae.setStatus(processingStatus);
					eae.setSkipReason(errorMessage);
					if(strings.length>EnrollmentConstants.Issuer_HIOS_ID){
						eae.setHiosIssuerId(strings[EnrollmentConstants.Issuer_HIOS_ID]);
					}
					if(strings.length>EnrollmentConstants.Exchange_Assigned_Policy_ID){
						eae.setExchgAssignedPolicyId(strings[EnrollmentConstants.Exchange_Assigned_Policy_ID]);
					}
					if(strings.length>EnrollmentConstants.Exchange_Assigned_Invidivual_ID){
						eae.setExchgAssignedMemberId(strings[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
					}
					if(strings.length>EnrollmentConstants.Subscriber_Flag){
						eae.setSubscriberFlag(strings[EnrollmentConstants.Subscriber_Flag]);
					}
					if(strings.length>EnrollmentConstants.Issuer_Assigned_Policy_ID){
						eae.setIssuerAssignedPolicyId(strings[EnrollmentConstants.Issuer_Assigned_Policy_ID]);
					}
					if(strings.length>EnrollmentConstants.Issuer_Assigned_Invidivual_ID){
						eae.setIssuerAssignedIndId(strings[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);
					}
					if(strings.length>EnrollmentConstants.Last_Premium_Paid_Date){
						eae.setLastPremiumPaidDate(strings[EnrollmentConstants.Last_Premium_Paid_Date]);
					}
					if(strings.length>EnrollmentConstants.Status){
						eae.setMemberStatus(strings[EnrollmentConstants.Status]);
					}
					if(strings.length>EnrollmentConstants.Coverage_End_Date){
						eae.setCoverageEndDate(strings[EnrollmentConstants.Coverage_End_Date]);
					}
					adminEffactuationList.add(eae);
				}
				if(adminEffactuationList!=null && adminEffactuationList.size()>0){
					enrollmentAdminEffactuationRepository.save(adminEffactuationList);
				}
				
			}	
	}
	
	/**
	 * Method to split List
	 * @param <T>
	 * @param list
	 * @param size
	 * @return
	 *//*
	private <T> List<List<T>> splitList(List<T> list, Integer size){
		List<List<T>> splittedList=null;
		if(list!=null && !list.isEmpty()){
			splittedList= new ArrayList<List<T>>();
			int totalCount=list.size();
			for(int i=0; i<totalCount; i+=size){
				splittedList.add(new ArrayList<T>(list.subList(i, Math.min(totalCount, i+size))));
			}
		}
		
		return splittedList;
	}*/
	
	private void validateRequest(List<String[]> request, String enrollment_id) throws GIException{
		if(request!=null && request.size()>0){
			for(String[] strs: request){
				if(strs.length<8){
					throw new GIException("Some of mandatory fields missing in request for the Exchange_Assigned_Policy_ID : "+ enrollment_id);
				}
				if(!EnrollmentUtils.isNotNullAndEmpty(strs[EnrollmentConstants.Issuer_HIOS_ID])){
					throw new GIException("Issuer_HIOS_ID missing in request for the Exchange_Assigned_Policy_ID : "+ enrollment_id);
				}
				if(!EnrollmentUtils.isNotNullAndEmpty(strs[EnrollmentConstants.Exchange_Assigned_Invidivual_ID])){
					throw new GIException("Exchange_Assigned_Invidivual_ID missing in request for the Exchange_Assigned_Policy_ID : "+ enrollment_id);
				}
				if(!EnrollmentUtils.isNotNullAndEmpty(strs[EnrollmentConstants.Subscriber_Flag])){
					throw new GIException("Subscriber_Flag missing in request for the Exchange_Assigned_Policy_ID : "+ enrollment_id +" and Exchange_Assigned_Invidivual_ID : "+ strs[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
				}
				if(!EnrollmentUtils.isNotNullAndEmpty(strs[EnrollmentConstants.Status])){
					throw new GIException("Status missing in request for the Exchange_Assigned_Policy_ID : "+ enrollment_id +" and Exchange_Assigned_Invidivual_ID : "+ strs[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
				}
			}
		}
	}
	
	@Override
	public boolean updateEnrollmentByCSV(Map<String, List<String[]>> effectuationMap, String fileName, String wipFolderDir) throws GIException {

		Enrollment enrollmentDb = null;
		boolean containsBadEnrollments = false;
		//StringBuilder badEnrollmentDetailsBuffer = new StringBuilder();
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
		
	    // For each key value in the map get the " effectuationList " 
	    // from the list find the subscriber
	    if(effectuationMap != null && effectuationMap.size() > 0){
	    	for (Map.Entry<String,List<String[]>> entry : effectuationMap.entrySet())
	    	{
	    		
	    		//validate request
	    		
	    		boolean foundFailedEnrollee = false;
	    		boolean updateEnrollmentFlag = false;
	    		String[] subscriberArray = null;	    		
	    		try{
	    			validateRequest(entry.getValue(), entry.getKey());
	    			Integer enrollment_id = Integer.valueOf(entry.getKey());
		    		for (String[] strings : entry.getValue()) {
		    			if(strings[EnrollmentConstants.Subscriber_Flag].equalsIgnoreCase("Y")){
		    				subscriberArray = strings;
		    				break;
		    			}
		    		}

		    	
	    			if(subscriberArray !=null ){
	    				String subscriberStatus = subscriberArray[EnrollmentConstants.Status];
	    				//subscriber level confirmation
	    				if(enrollment_id != null && enrollment_id != 0){
	    					enrollmentDb = enrollmentRepository.getEnrollmentForInbound(enrollment_id);
	    				}
	    				
	    				if(enrollmentDb == null){
	    					foundFailedEnrollee = true;
	    					//createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,subscriberArray,EnrollmentConstants.NO_ACTIVE_ENROLLMENT_FOUND);
	    					LOGGER.info("No Unique Enrollment found with EnrollmentId " + enrollment_id);
	    					throw new Exception("No Unique Enrollment found with EnrollmentId " + enrollment_id);
	    				}else{
	    					if(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(subscriberStatus)) {
	    						for (Enrollee enrolleeDb : getActiveEnrolleesForEnrollment(enrollmentDb.getEnrollees())) {
	    							String existingEnrolleeStatus=null;

	    							if(enrolleeDb!=null && enrolleeDb.getEnrolleeLkpValue()!=null){
	    								existingEnrolleeStatus=enrolleeDb.getEnrolleeLkpValue().getLookupValueCode();

	    								if(existingEnrolleeStatus!=null && !existingEnrolleeStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)){
	    									/*if(EnrollmentUtils.isNotNullAndEmpty(subscriberArray[EnrollmentConstants.Benefit_Effective_Date] )){
	    									Date benefitDate=DateUtil.StringToDate(subscriberArray[EnrollmentConstants.Benefit_Effective_Date], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
	    									String updateEffectuateDateFlag =DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.UPDATE_INBOUND_EFFECTUATION_DATE);
	    									if(getBooleanFromYesOrYFlag(updateEffectuateDateFlag)){
													enrolleeDb.setEffectiveStartDate(benefitDate);
	    									}
	    								}*/
	    									if(enrolleeDb.getPersonTypeLkp()!=null && EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrolleeDb.getPersonTypeLkp().getLookupValueCode())){
	    										if(subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID].equalsIgnoreCase(enrolleeDb.getExchgIndivIdentifier()) ){
	    											if(EnrollmentUtils.isNotNullAndEmpty(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID] )){
	    												enrolleeDb.setIssuerIndivIdentifier(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);
	    												enrollmentDb.setIssuerSubscriberIdentifier(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);	
	    											}
	    										}else{
	    											// ERROR AS SUBSCRIBER FLAG from FROM CSV does not match with EnrolleeDB.
	    											foundFailedEnrollee = true;
	    											//createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrolleeArray,"Enrollment with incorrect  Subscriber flag for memberId = "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    											LOGGER.info("Enrollment with incorrect  Subscriber flag for memberId= "+subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    											throw new Exception("Enrollment with incorrect  Subscriber flag for memberId= "+subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    											//break;
	    										}
	    									}



	    									if(subscriberArray.length>EnrollmentConstants.Last_Premium_Paid_Date && EnrollmentUtils.isNotNullAndEmpty(subscriberArray[EnrollmentConstants.Last_Premium_Paid_Date])){
	    										enrolleeDb.setLastPremiumPaidDate(DateUtil.StringToDate(subscriberArray[EnrollmentConstants.Last_Premium_Paid_Date], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));	
	    									}


	    									if(enrolleeDb.getEnrolleeLkpValue() != null && 
	    											(!enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) && 
	    													!enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))){
	    										enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
	    									}
	    									
	    									//HIX-110973 Accepting Confirmation Date on a Term Enrollment
											if (enrollmentDb.getEnrollmentConfirmationDate() == null
													&& enrollmentDb.getEnrollmentStatusLkp() != null
													&& enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
												enrollmentDb.setEnrollmentConfirmationDate(new TSDate());
											}
											
	    									if(subscriberArray.length>EnrollmentConstants.Issuer_Assigned_Policy_ID && EnrollmentUtils.isNotNullAndEmpty(subscriberArray[EnrollmentConstants.Issuer_Assigned_Policy_ID])){
	    										enrolleeDb.setHealthCoveragePolicyNo(subscriberArray[EnrollmentConstants.Issuer_Assigned_Policy_ID]);
	    									}

	    									enrolleeDb.setUpdatedBy(user);

	    									enrollmentService.createEnrolleeEvent(enrolleeDb, EnrollmentConstants.EVENT_TYPE_ADDITION, 
	    											EnrollmentConstants.EVENT_REASON_INITIAL_ENROLLMENT, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_EFFECTUATION_JOB);

	    									updateEnrollmentFlag=true;
	    								}
	    							}
	    						}
	    					}else if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(subscriberStatus)) {
	    						if(subscriberArray.length<=EnrollmentConstants.Coverage_End_Date && EnrollmentUtils.isNotNullAndEmpty(subscriberArray[EnrollmentConstants.Coverage_End_Date])) {
	    							LOGGER.info("No enrollment end date passed when status is TERM with EnrollmentId " + enrollment_id);
	    	    					throw new Exception("No enrollment end date passed when status is TERM with EnrollmentId " + enrollment_id);
	    						}
								//TERM CASE
								//******* CASE STARTS HERE 
								List<Enrollee> termEnrolleeList = getPendingConfirmTermEnrollees(enrollmentDb.getEnrollees());
								Date terminationDate = DateUtil.StringToDate(subscriberArray[EnrollmentConstants.Coverage_End_Date], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
								if(!EnrollmentUtils.isSameYear(terminationDate, enrollmentDb.getBenefitEffectiveDate())) {
									LOGGER.info("Enrollment end date passed is outside coverage year of policy : " + terminationDate);
	    	    					throw new Exception("Enrollment end date passed is outside coverage year of policy : " + terminationDate);
								}
								enrollmentDb.setBenefitEndDate(terminationDate);
								for(Enrollee enrolleeDb : termEnrolleeList) 
								{	
									if(enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) 
											&& EnrollmentUtils.removeTimeFromDate(terminationDate).after(enrolleeDb.getEffectiveEndDate())){
										if(!enrolleeDb.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
											continue;
										}
										else{
											LOGGER.info("Cannot extend termination date of an already terminated policy : "+enrollment_id);
											throw new Exception("Cannot extend termination date of an already terminated policy : "+enrollment_id);
										}
									}
									else{
										if(enrolleeDb.getPersonTypeLkp()!=null && EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrolleeDb.getPersonTypeLkp().getLookupValueCode())){
											if(subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID].equalsIgnoreCase(enrolleeDb.getExchgIndivIdentifier()) ){
												if(EnrollmentUtils.isNotNullAndEmpty(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID] )){
													enrolleeDb.setIssuerIndivIdentifier(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);
													enrollmentDb.setIssuerSubscriberIdentifier(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);	
												}
											}else{
												// ERROR AS SUBSCRIBER FLAG from FROM CSV does not match with EnrolleeDB.
												foundFailedEnrollee = true;
												LOGGER.info("Enrollment with incorrect  Subscriber flag for memberId= "+subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
												throw new Exception("Enrollment with incorrect  Subscriber flag for memberId= "+subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
											}
										}
    									if(subscriberArray.length>EnrollmentConstants.Last_Premium_Paid_Date && EnrollmentUtils.isNotNullAndEmpty(subscriberArray[EnrollmentConstants.Last_Premium_Paid_Date])){
    										enrolleeDb.setLastPremiumPaidDate(DateUtil.StringToDate(subscriberArray[EnrollmentConstants.Last_Premium_Paid_Date], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));	
    									}
										if(EnrollmentUtils.removeTimeFromDate(terminationDate).after(EnrollmentUtils.removeTimeFromDate(enrolleeDb.getEffectiveStartDate()))){
											enrolleeDb.setEffectiveEndDate(EnrollmentUtils.getEODDate(terminationDate));
											enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_TERM));
										}
										else if (EnrollmentUtils.removeTimeFromDate(enrolleeDb.getEffectiveStartDate())
												.after(EnrollmentUtils.removeTimeFromDate(terminationDate))
												|| EnrollmentUtils.removeTimeFromDate(enrolleeDb.getEffectiveStartDate())
														.equals(EnrollmentUtils.removeTimeFromDate(terminationDate))) {
											//Only CANCELS the member based on Date received in request 
											enrolleeDb.setEffectiveEndDate(enrolleeDb.getEffectiveStartDate());
											enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
											enrolleeDb.setDisenrollTimestamp(new TSDate());
										}
										enrolleeDb.setUpdatedBy(user);
										enrollmentService.createEnrolleeEvent(enrolleeDb, EnrollmentConstants.EVENT_TYPE_CANCELLATION, EnrollmentConstants.EVENT_REASON_NON_PAYMENT, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_EFFECTUATION_JOB);
										updateEnrollmentFlag=true;
									}
									//******* CASE ENDS HERE
								} 
							
	    					}else if(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(subscriberStatus)) {
									List<Enrollee> cancelEnrolleeList = getPendingConfirmTermEnrollees(enrollmentDb.getEnrollees());
									enrollmentDb.setBenefitEndDate(enrollmentDb.getBenefitEffectiveDate());
									if(cancelEnrolleeList != null && !cancelEnrolleeList.isEmpty()){
										for (Enrollee enrolleeDb : cancelEnrolleeList) {

											if(enrolleeDb.getPersonTypeLkp()!=null && EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrolleeDb.getPersonTypeLkp().getLookupValueCode())){
	    										if(subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID].equalsIgnoreCase(enrolleeDb.getExchgIndivIdentifier()) ){
	    											if(EnrollmentUtils.isNotNullAndEmpty(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID] )){
	    												enrolleeDb.setIssuerIndivIdentifier(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);
	    												enrollmentDb.setIssuerSubscriberIdentifier(subscriberArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);	
	    											}
	    										}else{
	    											// ERROR AS SUBSCRIBER FLAG from FROM CSV does not match with EnrolleeDB.
	    											foundFailedEnrollee = true;
	    											LOGGER.info("Enrollment with incorrect  Subscriber flag for memberId= "+subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    											throw new Exception("Enrollment with incorrect  Subscriber flag for memberId= "+subscriberArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    										}
	    									}

											enrolleeDb.setEffectiveEndDate(enrolleeDb.getEffectiveStartDate());
											enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
											enrolleeDb.setDisenrollTimestamp(new TSDate());
											enrolleeDb.setUpdatedBy(user);
											enrollmentService.createEnrolleeEvent(enrolleeDb, EnrollmentConstants.EVENT_TYPE_CANCELLATION, EnrollmentConstants.EVENT_REASON_NON_PAYMENT, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_EFFECTUATION_JOB);
											updateEnrollmentFlag = true;

										}
									}else{
										LOGGER.info("CANCEL enrollment request no enrollees found:  "+enrollmentDb.getId());
									}
							
	    					}else {
	    						LOGGER.info("Rejecting transaction, incorrect status received : " + subscriberArray[EnrollmentConstants.Status]);
								throw new Exception("Rejecting transaction, incorrect status received : " + subscriberArray[EnrollmentConstants.Status]);
	    					}
	    				}
	    			}else{
	    				if(entry.getValue() != null && !entry.getValue().isEmpty()){

	    				if(enrollment_id != null && enrollment_id != 0){
	    					enrollmentDb = enrollmentRepository.getEnrollmentForInbound(enrollment_id);
	    				}

	    				if(enrollmentDb == null){
	    					foundFailedEnrollee = true;
	    					//createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,subscriberArray,EnrollmentConstants.NO_ACTIVE_ENROLLMENT_FOUND);
	    					LOGGER.info("No Unique Enrollment found with EnrollmentId " + enrollment_id);
	    					throw new Exception("No Unique Enrollment found with EnrollmentId " + enrollment_id);
	    				}else{

	    					for (String[] enrolleeArray : entry.getValue()) {
	    						String enrolleeStatus = enrolleeArray[EnrollmentConstants.Status];
								if (EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrolleeStatus)
										|| EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrolleeStatus)) {
									LOGGER.info("Rejecting txn since member level Cancel or Term status sent for enrollee : "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    							throw new Exception("Rejecting txn since member level Cancel or Term status sent for enrollee : "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
								}else if(!EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(enrolleeStatus)) {
									LOGGER.info("Rejecting transaction, incorrect status received : " + enrolleeArray[EnrollmentConstants.Status]);
									throw new Exception("Rejecting transaction, incorrect status received : " + enrolleeArray[EnrollmentConstants.Status]);
								}
	    							
	    						// FOR ERROR LOGING ONLY
	    						subscriberArray = enrolleeArray;
	    						
	    						Enrollee enrolleeDb = getActiveEnrolleeForConfirmation(enrollmentDb.getEnrollees(), enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);

	    						String existingEnrolleeStatus=null;

	    						if(enrolleeDb!=null && enrolleeDb.getEnrolleeLkpValue()!=null){
	    							existingEnrolleeStatus=enrolleeDb.getEnrolleeLkpValue().getLookupValueCode();
	    						}

	    						if(enrolleeDb == null){
	    							foundFailedEnrollee = true;
	    							//createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrolleeArray,EnrollmentConstants.ERR_MSG_NO_ENROLLEE + " :- " + enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    							LOGGER.info(EnrollmentConstants.ERR_MSG_NO_ENROLLEE + " :- " + enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    							throw new Exception(EnrollmentConstants.ERR_MSG_NO_ENROLLEE + " :- " + enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
	    							
	    						}else{

	    							if(existingEnrolleeStatus!=null && !existingEnrolleeStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)){

	    								
	    								//SUBSCRIBER match 
	    								if(EnrollmentUtils.isNotNullAndEmpty(enrolleeArray[EnrollmentConstants.Subscriber_Flag]) && 
	    										EnrollmentUtils.getBooleanFromYesOrYFlag(enrolleeArray[EnrollmentConstants.Subscriber_Flag])){
	    									if(enrolleeDb.getPersonTypeLkp()!=null && EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrolleeDb.getPersonTypeLkp().getLookupValueCode())){
		    									enrollmentDb.setIssuerSubscriberIdentifier(enrolleeArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);	
		    								}else{
		    									// ERROR AS SUBSCRIBER FLAG from FROM CSV does not match with EnrolleeDB.
		    									foundFailedEnrollee = true;
		    									//createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrolleeArray,"Enrollment with incorrect  Subscriber flag for memberId = "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
		    									LOGGER.info("Enrollment with incorrect  Subscriber flag for memberId= "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
		    									throw new Exception("Enrollment with incorrect  Subscriber flag for memberId= "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
		    									//break;
		    								}
	    								}else if(EnrollmentUtils.isNotNullAndEmpty(enrolleeArray[EnrollmentConstants.Subscriber_Flag]) && 
	    										!EnrollmentUtils.getBooleanFromYesOrYFlag(enrolleeArray[EnrollmentConstants.Subscriber_Flag])){
	    									if(enrolleeDb.getPersonTypeLkp()!=null && EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrolleeDb.getPersonTypeLkp().getLookupValueCode())){
	    										foundFailedEnrollee = true;
		    									//createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrolleeArray,"Enrollment with incorrect  Subscriber flag for memberId = "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
		    									LOGGER.info("Enrollment with incorrect  Subscriber flag for memberId= "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
		    									throw new Exception("Enrollment with incorrect  Subscriber flag for memberId= "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
		    									//break;	
		    								}
	    								}
	    								if(enrolleeArray.length>EnrollmentConstants.Issuer_Assigned_Invidivual_ID && EnrollmentUtils.isNotNullAndEmpty(enrolleeArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID] )){
	    									enrolleeDb.setIssuerIndivIdentifier(enrolleeArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);
	    								}


	    								if(enrolleeArray.length>EnrollmentConstants.Last_Premium_Paid_Date && EnrollmentUtils.isNotNullAndEmpty(enrolleeArray[EnrollmentConstants.Last_Premium_Paid_Date])){
	    									enrolleeDb.setLastPremiumPaidDate(DateUtil.StringToDate(enrolleeArray[EnrollmentConstants.Last_Premium_Paid_Date], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));	
	    								}


	    								if(enrolleeDb.getEnrolleeLkpValue() != null && 
	    										(!enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) && 
	    												!enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))){
	    									enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
	    								}
	    								if(enrolleeArray.length>EnrollmentConstants.Issuer_Assigned_Policy_ID && EnrollmentUtils.isNotNullAndEmpty(enrolleeArray[EnrollmentConstants.Issuer_Assigned_Policy_ID])){
	    									enrolleeDb.setHealthCoveragePolicyNo(enrolleeArray[EnrollmentConstants.Issuer_Assigned_Policy_ID]);
	    								}
	    								
	    								enrolleeDb.setUpdatedBy(user);

	    								enrollmentService.createEnrolleeEvent(enrolleeDb, EnrollmentConstants.EVENT_TYPE_ADDITION, EnrollmentConstants.EVENT_REASON_INITIAL_ENROLLMENT, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_EFFECTUATION_JOB);

	    								updateEnrollmentFlag=true;
	    							}else{
	    								//createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrolleeArray,"Enrollee with Exchange Individual Identifier = " + enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID] + " and enrollment ID = " + enrollmentDb.getId() + " is already Confirmed");
	    								LOGGER.info("This Enrollee with Exchange Individual Identifier = "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]+" and enrollment ID ="+enrollmentDb.getId()+" is already Confirmed");
	    								foundFailedEnrollee=true;
	    								throw new Exception("This Enrollee with Exchange Individual Identifier = "+enrolleeArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]+" and enrollment ID ="+enrollmentDb.getId()+" is already Confirmed");
	    							}
	    						}
	    					}//End of Enrollee
	    				}
	    			}}
	    			
	    			if(foundFailedEnrollee){
	    				containsBadEnrollments=true;
	    			}
	    			//check Enrollment Status
	    			if(updateEnrollmentFlag && !containsBadEnrollments){
	    				List<Enrollment> enrollmentList= new ArrayList<Enrollment>();
	    				enrollmentList.add(enrollmentDb);
	    				enrollmentService.checkEnrollmentStatus(user,enrollmentDb);
						if (enrollmentDb.getEnrollmentConfirmationDate() == null
								&& (enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)
										|| enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM))) {
							enrollmentDb.setEnrollmentConfirmationDate(new TSDate());
						}
	    				enrollmentDb.setEnrollmentPremiums(enrollmentDb.getEnrollmentPremiums());
	    				String carrierStatus = enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode();
	    				boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
	    				if(isNotNullAndEmpty(carrierStatus) && (carrierStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)
	    						|| carrierStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
	    					if(populateMonthlyPremium ){
	    						enrollmentRequotingService.updateMonthlyPremiumForCancelTerm( enrollmentDb, null);
	    					}
	    					if(EnrollmentConfiguration.isIdCall() && EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollmentDb.getInsuranceTypeLkp().getLookupValueCode())){
	    						List<Enrollment> dentalEnrollments=enrollmentService.updateDentalAPTCOnHealthDisEnrollment(enrollmentDb, user, EnrollmentEvent.TRANSACTION_IDENTIFIER.APTC_CH_HLT_CARRIER_DISENROLL);
	    						if(dentalEnrollments!=null && dentalEnrollments.size()>0){
	    							enrollmentList.addAll(dentalEnrollments);
	    						}
	    					}
	    				}
	    				
	    				enrollmentList = enrollmentService.saveAllEnrollment(enrollmentList);
	    				
	    				Boolean isCaCall = EnrollmentConstants.STATE_CODE_CA.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode());
	    			    
	    			    if(isCaCall) {
	    			            //Calling EnrollmentAhbxSyncService to save enrollment update Data replacement of IND21
	    			            LOGGER.info("Async Call to enrollmentAhbxSyncService saveEnrollmentAhbxSync IND21_UPDATE");
	    			            enrollmentDb.setEnrollmentPremiums(enrollmentPremiumRepository.findByEnrollmentId(enrollmentDb.getId()));
	    			            enrollmentAhbxSyncService.saveEnrollmentAhbxSync(Arrays.asList(enrollmentDb), enrollmentDb.getHouseHoldCaseId(), EnrollmentAhbxSync.RecordType.IND21_UPDATE);
	    			    }
	    			}
	    			insertAdminEffactuation(entry.getValue(), fileName, EnrollmentAdminEffactuation.PROCESSING_STATUS.SUCCESS.toString(), null);
	    			
	    		}catch(Exception exception){
	    			LOGGER.error(exception.getMessage() , exception);
	    			foundFailedEnrollee = true;
	    			containsBadEnrollments=true;
	    			//createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, subscriberArray, exception.getMessage());
	    			insertAdminEffactuation(entry.getValue(), fileName, EnrollmentAdminEffactuation.PROCESSING_STATUS.FAILURE.toString(), exception.getMessage());
	    		}

	    	}//MAP FOR LOOP
	    	
	    	/*if(containsBadEnrollments){
				// Log failing enrollment data to file.
				File targetFolder = new File(wipFolderDir);
				if (!(targetFolder.exists())) {
					targetFolder.mkdirs();
				}
				EnrollmentUtils.logFailingRecordsData(fileName, badEnrollmentDetailsBuffer, wipFolderDir);
			}
*/
	    }
		return containsBadEnrollments;
	}

	public void createContentForBadEnrollmentFile(StringBuilder badEnrollmentDetailsBuilder, String[] enrollmentArray, String reason){
		if(badEnrollmentDetailsBuilder != null){
			badEnrollmentDetailsBuilder.append(reason);
			if(enrollmentArray != null){
				badEnrollmentDetailsBuilder.append("Reason for Failure: ");
				badEnrollmentDetailsBuilder.append(", Exchange_Assigned_Invidivual_ID=");
				badEnrollmentDetailsBuilder.append(enrollmentArray[EnrollmentConstants.Exchange_Assigned_Invidivual_ID]);
				badEnrollmentDetailsBuilder.append(",Issuer_Assigned_Invidivual_ID=");
				badEnrollmentDetailsBuilder.append(enrollmentArray[EnrollmentConstants.Issuer_Assigned_Invidivual_ID]);
				badEnrollmentDetailsBuilder.append(",Enrollment_ID=");
				badEnrollmentDetailsBuilder.append(enrollmentArray[EnrollmentConstants.Exchange_Assigned_Policy_ID]);
				//badEnrollmentDetailsBuilder.append(", Status=");
				//badEnrollmentDetailsBuilder.append(enrollmentArray[EnrollmentConstants.Status]);
				badEnrollmentDetailsBuilder.append("\n");
			}
		}
		
	}
    
	/**
	 * @author parhi_s
	 * 
	 * Returns Returns all Active Enrollees for Enrollment.
	 * 
	 * @return
	 */
	private Enrollee getActiveEnrolleeForConfirmation(List<Enrollee> enrollees, String memberId ){
		Enrollee activeEnrollee = null;
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if(enrollee!=null && enrollee.getExchgIndivIdentifier()!=null && enrollee.getExchgIndivIdentifier().equalsIgnoreCase(memberId) ){
					if(enrollee.getPersonTypeLkp()!=null && ((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))
							&& enrollee.getEnrolleeLkpValue()!=null
							&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))
							//&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
							&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED))){
						activeEnrollee=enrollee;
						break;
					}
				}
			}
		}
		return activeEnrollee;
	}
	/**
	 * @author ajinkya
	 * 
	 * Returns Returns all Active Enrollees for Enrollment.
	 * 
	 * @return list of enrollee
	 */
	
	public List<Enrollee> getActiveEnrolleesForEnrollment(List<Enrollee> enrolleeList){
		List<Enrollee> activeEnrolleeList = new ArrayList<Enrollee>();
		if(enrolleeList != null){
			for(Enrollee enrollee : enrolleeList){
				if((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(Enrollment.PERSON_TYPE_ENROLLEE) || 
						                      enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(Enrollment.PERSON_TYPE_SUBSCRIBER))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(Enrollment.ENROLLMENT_STATUS_CANCEL))
//						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(Enrollment.ENROLLMENT_STATUS_TERM))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(Enrollment.ENROLLMENT_STATUS_ABORTED))){
					activeEnrolleeList.add(enrollee);
				}
			}
		}
		return activeEnrolleeList;
	}
	
	private boolean getBooleanFromYesOrYFlag(String inputStringFlag){
		boolean flag = false;
		if(isNotNullAndEmpty(inputStringFlag) && (inputStringFlag.equalsIgnoreCase(EnrollmentConstants.YES) || inputStringFlag.equalsIgnoreCase(EnrollmentConstants.Y))){
			flag = true;
		}
		return flag;
	}
	
	private List<Enrollee> getPendingConfirmTermEnrollees(List<Enrollee> enrollees){
		List<Enrollee> filteredEnrollees = new ArrayList<Enrollee>();
		if(enrollees != null ){
			for(Enrollee enrollee : enrollees){
					if(enrollee.getPersonTypeLkp()!=null && ((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))
							&& enrollee.getEnrolleeLkpValue()!=null
							&&((enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING))||(enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) || enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
						filteredEnrollees.add(enrollee);
					}
			}
		}
		return filteredEnrollees;
	}
}
