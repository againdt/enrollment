package com.getinsured.hix.enrollment.carrierfeed.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by jabelardo on 7/14/14.
 */
public abstract class ExcelFileWriterBase<T> extends FileWriter<T> {

    protected abstract Workbook createWorkbook(InputStream inStream) throws IOException;

    protected abstract Workbook createWorkbook();

    private static final Logger logger = LoggerFactory.getLogger(ExcelFileWriterBase.class);

    @Override
    protected void write(OutputStream outputStream, Iterator<T> inputIterator) throws IOException {
        if (hasTemplate()) {
            ByteArrayOutputStream templateOutputStream = new ByteArrayOutputStream();
            Files.copy(Paths.get(getTemplateFilename()), templateOutputStream);
            InputStream templateInputStream = new ByteArrayInputStream(templateOutputStream.toByteArray());
            Workbook workbook = createWorkbook(templateInputStream);
            Sheet sheet = workbook.getSheetAt(0);
            int firstDataRow = getMappings().get("firstDataRow").asInt();
            while (firstDataRow < sheet.getPhysicalNumberOfRows()) {
                sheet.removeRow(sheet.getRow(sheet.getLastRowNum()));
            }
            createDataRows(sheet, firstDataRow, inputIterator);
            workbook.write(outputStream);

        } else {
            Workbook workbook = createWorkbook();
            Sheet sheet = workbook.createSheet();
            int firstDataRow = getMappings().get("firstDataRow").asInt();
            if (firstDataRow > 0) {
                createHeader(sheet, firstDataRow);
            }
            createDataRows(sheet, firstDataRow, inputIterator);
            workbook.write(outputStream);
        }
    }

    private void createDataRows(Sheet sheet, int firstDataRow, Iterator<T> inputIterator) {
        int rowNum = firstDataRow;
        while (inputIterator.hasNext()) {
            T model = inputIterator.next();
            createRow(sheet, rowNum, model);
            rowNum++;
        }
    }

    private void createHeader(Sheet sheet, int firstDataRow) {
        int skipRowCount = 0;
        Row row = null;
        while (skipRowCount < firstDataRow) {
            row = sheet.createRow(skipRowCount);
            skipRowCount++;
        }
        if (row != null) {
            fillHeader(row);
        }
    }

    private void fillHeader(Row row) {
        JsonNode fieldNodes = getMappings().get("fields");
        int column = 0;
        for (JsonNode fieldNode : fieldNodes) {
            String fieldName = fieldNode.get("fieldName").asText();
            Cell cell = row.createCell(column, Cell.CELL_TYPE_STRING);
            cell.setCellValue(fieldName);
            column++;
        }
    }

    private void createRow(Sheet sheet, int rowNum, T model) {
        Row sheetRow = sheet.createRow(rowNum);
        int column = 0;
        JsonNode fieldNodes = getMappings().get("fields");
        for (JsonNode fieldNode : fieldNodes) {
            Cell cell = sheetRow.createCell(column);
            String fieldName = fieldNode.get("fieldName").asText();
            Field field = null;
            try {
                field = model.getClass().getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                logger.warn(String.format("Can't find in class %s field %s", model.getClass().getCanonicalName(),
                        fieldName) , e);
                continue;
            }
            field.setAccessible(true);
            Class<?> fieldType = field.getType();

            Object value = null;
            try {
                value = field.get(model);
            } catch (IllegalAccessException e) {
                logger.warn(String.format("Can't read value in class %s field %s",
                        model.getClass().getCanonicalName(), fieldName) , e);
            }
            try {
                if (value == null) {
                    cell.setCellType(Cell.CELL_TYPE_BLANK);

                } else if (fieldType.equals(Date.class)) {
                    CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                    CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
                    cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("mm/dd/yyyy"));
                    cell.setCellStyle(cellStyle);
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(DateUtil.getExcelDate((Date) value));

                } else if (fieldType.equals(String.class)) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    cell.setCellValue((String) value);

                } else if (fieldType.equals(Character.class)) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    cell.setCellValue(Character.toString((Character) value));

                } else if (fieldType.equals(Boolean.class)) {
                    cell.setCellType(Cell.CELL_TYPE_BOOLEAN);
                    cell.setCellValue((Boolean) value);

                } else {
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue((Double) value);
                }
            } catch (ClassCastException e) {
                logger.warn(String.format("Don't know how to read in class %s field %s type %s",
                        model.getClass().getCanonicalName(), fieldName, fieldType.toString()) , e);

            }
            column++;
        }
    }
}
