package com.getinsured.hix.enrollment.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.notification.NotificationAgent;

@Component
public class EnrollmentEmailNotification extends NotificationAgent {
	private Map<String, String> enrollmentStatusDetails;
	private Map<String, String> singleData;
	
	
	public void setEmployerEligibilityDetails(Map<String, String> enrollmentStatusDetails) {
		this.enrollmentStatusDetails = enrollmentStatusDetails;
	}
	
	public void setNotificationData(Map<String, Object> notificationData){
		this.notificationData = notificationData;
	}
	
	public Map<String, Object> getNotificationData(){
		return this.notificationData;
	}
	
	@Override
	public Map<String, String> getSingleData() {
		Map<String, String> data = new HashMap<String, String>();
		
		if(this.enrollmentStatusDetails != null && !this.enrollmentStatusDetails.isEmpty()){
			setTokens(this.enrollmentStatusDetails);
			data.putAll(this.enrollmentStatusDetails);
		}

		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
		
	}
	
	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}
}
