package com.getinsured.hix.enrollment.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.email.UpdateEnrollmentByCSVFailureNotification;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.UpdateEnrollmentByCSVfileThread;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentCSVDto;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("updateEnrollmentByCSVfileService")
public class UpdateEnrollmentByCSVfileServiceImpl implements UpdateEnrollmentByCSVfileService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateEnrollmentByCSVfileServiceImpl.class);
	
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private LookupService lookupService;
	@Autowired private UserService userService;
	@Autowired private UpdateEnrollmentByCSVFailureNotification UpdateEnrollmentByCSVFailureNotification;
	
	private static final String SPACE = " ";

	@Override
	public boolean updateEnrollmentByCSV(List<EnrollmentCSVDto> enrollmentCSVList, String fileName, String wipFolderDir) throws Exception{
		List<EnrollmentCSVDto> errorCSVDataList = new ArrayList<>();
		int defaultThreadPoolSize = 10;
		ExecutorService executor = Executors.newFixedThreadPool(defaultThreadPoolSize);
		
		int iteamPerThread = enrollmentCSVList.size() > defaultThreadPoolSize ? enrollmentCSVList.size()/defaultThreadPoolSize : 1;
		
		Collection<List<EnrollmentCSVDto>> enrollmentCSVSubList = EnrollmentUtils.partitionBasedOnSize(enrollmentCSVList, iteamPerThread);

		List<Callable<List<EnrollmentCSVDto>>> taskList = new ArrayList<>();
		
		Iterator<List<EnrollmentCSVDto>> enrollmentCSVSubListIterator = enrollmentCSVSubList.iterator();

		while(enrollmentCSVSubListIterator.hasNext()){
			taskList.add(new UpdateEnrollmentByCSVfileThread(enrollmentCSVSubListIterator.next(), this));
		}
		
		executor.invokeAll(taskList)
		.stream()
		.map(future -> { 
						try{
							return future.get();
							
							}catch(Exception ex){
							
								LOGGER.error("Exception occurred in makeAndWriteCMSXml: ", ex);
						    }
						return null;
					} 
			)
		.filter(p -> p != null)
		.forEach(e -> errorCSVDataList.addAll(e));
		
		executor.shutdown();
		
		if(errorCSVDataList != null && errorCSVDataList.size() > 0) {
			// Log failing enrollment data to file.
			File targetFolder = new File(wipFolderDir);
			if (!(targetFolder.exists())) {
				targetFolder.mkdirs();
			}
			
			StringBuilder badEnrolFromEnrl =  errorCSVDataList.stream().map(a -> a.getErrorMessage()).map(b -> b.append("\n")).reduce(new StringBuilder(),StringBuilder::append);
			EnrollmentUtils.logFailingRecordsData(fileName, badEnrolFromEnrl, wipFolderDir);
			
			if(errorCSVDataList != null && errorCSVDataList.size() > 0) {
				StringBuilder badCSVFromEnrl =  errorCSVDataList.stream().map(a -> a.getErrorCSV()).map(b -> b.append("\n")).reduce(new StringBuilder(),StringBuilder::append);
				createFailureCSVFile(wipFolderDir, badCSVFromEnrl, "enrollment");
			}
			
			prepareDetailMapAndSendNotification(fileName);
			
			return true;
			
		}else {
			
			return false;
		}
	}

	public List<EnrollmentCSVDto> updateEnrollmentByCSV(List<EnrollmentCSVDto> enrlCSVDtoist) {
		return enrlCSVDtoist.parallelStream().map(enrlcsv -> updateEnrollment(enrlcsv)).filter(p -> p != null).collect(Collectors.toList());
	}

	private EnrollmentCSVDto updateEnrollment(EnrollmentCSVDto enrlCSVDto) {
		EnrollmentCSVDto enrollmentCSVDto = null;
		
		try {
			Enrollment enrl = enrollmentRepository.findById(enrlCSVDto.getEnrollmentIDPhix());
			if(enrl != null) {
				enrl.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, enrlCSVDto.getEnrollmentStatus()));
				enrl.setBenefitEffectiveDate(enrlCSVDto.getEffectiveStartDate());
				enrl.setBenefitEndDate(enrlCSVDto.getEffectiveEndDate());
				enrl.setAptcAmt(enrlCSVDto.getAptc());
				enrl.setCapAgentId(getCapAgentId(enrlCSVDto.getCapAgentNPN(), enrlCSVDto.getEmailAddress()));
				enrl.setInsurerName(enrlCSVDto.getInsurerName());
				
				enrollmentRepository.saveAndFlush(enrl);
			}else {
				enrollmentCSVDto =  new EnrollmentCSVDto(new StringBuilder("No Enrollment found for Id : "+enrlCSVDto.getEnrollmentIDPhix()) , enrlCSVDto.toStringBuilder());
			}
			
		} catch (Exception e) {
			enrollmentCSVDto =  new EnrollmentCSVDto(new StringBuilder("Enrollment Id : "+enrlCSVDto.getEnrollmentIDPhix()+" Not Updated Exception : "+ EnrollmentUtils.shortenedStackTrace(e, 3)),
													 enrlCSVDto.toStringBuilder());
		}
		
		return enrollmentCSVDto;
	}

	private Integer getCapAgentId(String capAgentNPN, String emailAddress) {
		AccountUser user = null;
		
		if (capAgentNPN != null) {
			
			user = userService.findByUserNpn(capAgentNPN);

		} else if (emailAddress != null) {
			
			user = userService.findByEmail(emailAddress);
		}
		
		if(user != null) {
			
			return user.getId();
		}
		
		return null;
	}
	
	/**
	 * Create text file at given path containing test from Stringbuilder
	 * @param parFileName
	 * @param badEnrollmentDetailsBuilder
	 * @param failureFolderPath
	 */
	@Override
	public void createFailureCSVFile(String failureFolderPath, StringBuilder csvData, String failureType){
		File failureFile = new File(failureFolderPath + File.separator + failureType + "_failedRecord.csv");
		File failureFolder = new File(failureFolderPath);
		FileWriter writer = null;
		BufferedWriter bufferedWriter = null;
		try {
			if(!failureFolder.exists()){
				failureFolder.mkdirs();
			}
			if(!failureFile.exists()){
				failureFile.createNewFile();
			}
			writer = new FileWriter(failureFile);
			bufferedWriter = new BufferedWriter(writer,EnrollmentConstants.BUFFER_SIZE);
			bufferedWriter.write(csvData.toString());
			bufferedWriter.flush();
		} catch (IOException e) {
			LOGGER.error("Error Writing failed records file" , e);
		}finally{
			IOUtils.closeQuietly(writer);
			IOUtils.closeQuietly(bufferedWriter);
		}
	}
	
	@Override
	public void moveCSVFileToGoodBadFolder(String fileName, String fileDirectory, boolean contailsBadEnrollment) {
		try {
		if(contailsBadEnrollment){
				EnrollmentUtils.moveFiles(fileDirectory, fileDirectory+ File.separator +"BAD_CSV",fileName, null);
		}else{
			EnrollmentUtils.moveFiles(fileDirectory, fileDirectory+ File.separator +"GOOD_CSV",fileName, null);
		}
		} catch (GIException e) {
			LOGGER.error("Error in moveToFolder "+e.toString(),e);
		}
		
	}
	
	@Override
	public void sendFailNotificationEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("Update Enrollment By CSV File :: Notification email sending process");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EMAIL_GRP_ITPROD_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("updateEnrollmentByCSVfileService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("updateEnrollmentByCSVfileService:: Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			emailData.put("Subject",requestMap.get("Subject"));
			UpdateEnrollmentByCSVFailureNotification.setEmailData(emailData);
			UpdateEnrollmentByCSVFailureNotification.setRequestData(requestMap);
			Notice noticeObj = UpdateEnrollmentByCSVFailureNotification.generateEmail();
			Notification notificationObj = UpdateEnrollmentByCSVFailureNotification.generateNotification(noticeObj);
			LOGGER.info("Notice body :: "+noticeObj.getEmailBody());
			UpdateEnrollmentByCSVFailureNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex){
			throw new GIException(ex);
		}
	}
	
	/**
	 * Send notification mail after failure
	 * @param transferLogList
	 * @param reportType
	 * @param transferDirection
	 */
	private synchronized void prepareDetailMapAndSendNotification(String fileName) {
		
		Map<String, String> emailDataMap=new HashMap<>();
		Calendar cal = Calendar.getInstance();
		StringBuffer subjectBuilder = new StringBuffer();
		
		emailDataMap.put("date", cal.getTime().toString());
		
		subjectBuilder.append("Failure : Update Enrollment By CSV file Job").append(SPACE)
				      .append("for").append(SPACE).append(String.format("%02d", cal.get(Calendar.DATE))).append("/")
				                            .append(String.format("%02d", cal.get(Calendar.MONTH) + 1)).append("/")
				                            .append(String.valueOf(cal.get(Calendar.YEAR)));
		String tdPrefix = "<td style='border:1px solid #000;'>";
		String tdPostfix = "</td>";
		
		StringBuffer tableContentBuilder = new StringBuffer();
		
		tableContentBuilder.append("<tr>");
		tableContentBuilder.append(tdPrefix).append(fileName).append(tdPostfix);
		tableContentBuilder.append(tdPrefix).append(String.format("%02d", cal.get(Calendar.MONTH) + 1))
				.append("/").append(cal.get(Calendar.YEAR)).append(tdPostfix);
		tableContentBuilder.append("</tr>");
				
		emailDataMap.put("tableContent", tableContentBuilder.toString());
		emailDataMap.put("Subject", subjectBuilder.toString());
		try {
			sendFailNotificationEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending staging email notification :: ", e);
		}
	}
}
