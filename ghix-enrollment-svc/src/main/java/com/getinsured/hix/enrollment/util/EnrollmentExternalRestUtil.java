package com.getinsured.hix.enrollment.util;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.dto.externalassister.DesignateAssisterResponse;
import com.getinsured.hix.dto.feeds.IssuerInfoDTO;
import com.getinsured.hix.dto.feeds.IssuerListDetailsDTO;
import com.getinsured.hix.dto.planmgmt.IssuerBrandNameResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerListRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanCrossWalkRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanCrossWalkResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanListResponse;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.BrokerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ExternalAssisterEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.request.ssap.SsapApplicationRequest;
import com.getinsured.iex.request.ssap.SsapApplicationResponse;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * @since 22nd September, 2015
 * @author Sharma_K
 * Class for common Rest Calls used across enrollment.
 *
 */
@Component
public class EnrollmentExternalRestUtil 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentExternalRestUtil.class);
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private Gson platformGson;
	@Autowired private GIWSPayloadService giwsPayloadService;
	/**
	 * Method to fetch HouseHold Object from Consumer Rest call 
	 * @param householdId
	 * @return
	 * @throws GIException
	 */
	public Household getHousehold(final Integer householdId) throws GIException
	{
		Household household = null;
		if(householdId != null) 
		{
			XStream xstream = GhixUtils.getXStreamStaxObject();
			String getResponse = ghixRestTemplate.postForObject(ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID,
					platformGson.toJson(householdId.toString()), String.class);
			if(StringUtils.isNotEmpty(getResponse))
			{
				ConsumerResponse consumerResponse = (ConsumerResponse) xstream.fromXML(getResponse);
				if(consumerResponse != null)
				{
					if (StringUtils.isNotEmpty(consumerResponse.getStatus())
							&& GhixConstants.RESPONSE_SUCCESS.equals(consumerResponse.getStatus()))
					{
						household = consumerResponse.getHousehold();
					}
					else
					{
						throw new GIException(consumerResponse.getErrCode(), consumerResponse.getErrMsg(),
								EnrollmentConstants.HIGH);
					}
				}
				else
				{
					throw new GIException(EnrollmentConstants.ERROR_CODE_201, "Unable to fetch ConsumerResponse for HouseHoldId: "+householdId,
							EnrollmentConstants.HIGH);
				}
			}
			else
			{
				throw new GIException(EnrollmentConstants.ERROR_CODE_202,
						"Empty OR Null Response received from "
								+ ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID + " Service.", EnrollmentConstants.HIGH);
			}
		}
		return household;
	}

	/**
	 * 
	 * @param issuerListDetailsDTO
	 * @return
	 * @throws GIException
	 */
	public List<IssuerInfoDTO> getIssuerDetails(final IssuerListDetailsDTO issuerListDetailsDTO) throws GIException {
		List<IssuerInfoDTO> issuerInfoDtoList = null;
		if(issuerListDetailsDTO != null){
			issuerInfoDtoList = new ArrayList<IssuerInfoDTO>();
			try{
				String responseStr = ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_LIST,  platformGson.toJson(issuerListDetailsDTO), String.class);
				if(StringUtils.isNotBlank(responseStr)){
					IssuerListDetailsDTO issuerListDetailsDTOResponse = platformGson.fromJson(responseStr, IssuerListDetailsDTO.class);

					if(issuerListDetailsDTOResponse != null && issuerListDetailsDTOResponse.getIssuersList() != null && !issuerListDetailsDTOResponse.getIssuersList().isEmpty()){
						issuerInfoDtoList = issuerListDetailsDTOResponse.getIssuersList();
					}
					else{
						throw new GIException(EnrollmentConstants.ERROR_CODE_202, "No issuer List found in the response", EnrollmentConstants.HIGH);
					}
				}
				else{
					throw new GIException(EnrollmentConstants.ERROR_CODE_201, "Received null or empty response from GET_ISSUER_LIST API ", EnrollmentConstants.HIGH);
				}
			}
			catch(Exception ex){
				LOGGER.error("Exception occurred while fetching issuer details, API- getIssuerList ", ex);
			}
		}
		return issuerInfoDtoList;
	}
	/**
	 * 
	 * @param hiosIssuerId
	 * @return
	 */
	public String getIssuerNameByHiosIssuerId(final String hiosIssuerId){
		if(StringUtils.isNotBlank(hiosIssuerId)){
			IssuerByHIOSIdRequestDTO issuerByHIOSIdRequestDTO = new IssuerByHIOSIdRequestDTO();
			issuerByHIOSIdRequestDTO.setHiosId(hiosIssuerId);
			try{
				String responseStr = ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_INFO_BY_HIOS_ID,  issuerByHIOSIdRequestDTO, String.class);
				if(StringUtils.isNotBlank(responseStr)){
					IssuerByHIOSIdResponseDTO issuerByHIOSIdResponseDTO = platformGson.fromJson(responseStr, IssuerByHIOSIdResponseDTO.class);
					if(issuerByHIOSIdResponseDTO != null){
						return issuerByHIOSIdResponseDTO.getName();
					}
				}
			}
			catch(Exception ex){
				LOGGER.error("Exception occurred while fetching issuerByHiosId details, API- getIssuerByHIOSId ", ex);
			}
		}
		return null;
	}

	/**
	 * Get issuer information from Plan Management API
	 * @param issuerId
	 * @return SingleIssuerResponse
	 */
	public SingleIssuerResponse getIssuerInfoById(final Integer issuerId){
		SingleIssuerResponse singleIssuerResponse = new SingleIssuerResponse();
		IssuerRequest issuerRequest = new IssuerRequest();
		issuerRequest.setId(issuerId.toString());
		try{
			singleIssuerResponse =  ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUERS_BY_ID, issuerRequest, SingleIssuerResponse.class);
		}catch(Exception e){
			LOGGER.error("Error fetching issuer information from plan management API", e);
		}
		return singleIssuerResponse;
	}

	/**
	 * 
	 * @param planRateBenefitRequest
	 * @return
	 */
	public PlanRateBenefitResponse getBenchmarkPlanBenefitRate(final PlanRateBenefitRequest planRateBenefitRequest) {
		PlanRateBenefitResponse planRateBenefitResponse = null;
		GIWSPayload giWsPayloadObj =null; 
		boolean isSuccess=false;
		try {
			giWsPayloadObj = EnrollmentGIPayloadUtil.save(giwsPayloadService,
					platformGson.toJson(planRateBenefitRequest), null,
					GhixEndPoints.PlanMgmtEndPoints.GET_BENCHMARK_PLAN_RATE_URL, "GET_BENCHMARK_PLAN_RATE_URL",
					isSuccess);
			String responseStr = ghixRestTemplate.postForObject(PlanMgmtEndPoints.GET_BENCHMARK_PLAN_RATE_URL,
					planRateBenefitRequest, String.class);
			isSuccess = true;
			giWsPayloadObj.setResponsePayload(responseStr);
			if (StringUtils.isNotBlank(responseStr)) {
				planRateBenefitResponse = platformGson.fromJson(responseStr, PlanRateBenefitResponse.class);
			}
		}catch(Exception e){
			throw e;
		}finally{
			EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj,giWsPayloadObj.getResponsePayload() , isSuccess);
		}
		return planRateBenefitResponse;
	}

	/**
	 * @author Panda_P
	 * JIRA HIX-86685 : Implement IsSamePlan Functionality for Renewal Flow
	 * 
	 * @param hiosPlanId- Old Enrollment CMS_PLAN_ID
	 * @param effectiveDate- New enrollment effectiveDate
	 * @param isCatastrophic- New Plan Catastrophic
	 * @param subscriberZip- New  subscriberZip
	 * @param subscriberCountyCode- New subscriberCountyCode
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	public String getHiosPlanIdFromCrossWalkApi(final String hiosPlanId, final Date effectiveDate, final String isCatastrophic, final String subscriberZip, final String subscriberCountyCode) throws Exception{
		try{
			PlanCrossWalkRequestDTO planCrossWalkRequest = new PlanCrossWalkRequestDTO();
			planCrossWalkRequest.setHiosPlanNumber(hiosPlanId);
			planCrossWalkRequest.setEffectiveDate(DateUtil.dateToString(effectiveDate, "yyyy-MM-dd"));
			planCrossWalkRequest.setIsEligibleForCatastrophic(isCatastrophic);
			planCrossWalkRequest.setZip(subscriberZip);
			planCrossWalkRequest.setCountyCode(subscriberCountyCode);
			String response = ghixRestTemplate.postForObject(PlanMgmtEndPoints.GET_CROSSWALK_HIOS_PLAN_ID, planCrossWalkRequest, String.class);
			PlanCrossWalkResponseDTO planCrossWalkResponse = platformGson.fromJson(response, PlanCrossWalkResponseDTO.class);

			if(isNotNullAndEmpty(planCrossWalkResponse)) {
				//				if(planCrossWalkResponse.getIsAvailable()!=null && planCrossWalkResponse.getIsAvailable().equalsIgnoreCase("Y")){
				//					if(isNotNullAndEmpty(planCrossWalkResponse.getHiosPlanNumber())){
				//						return planCrossWalkResponse.getHiosPlanNumber();
				//					}
				//				}
				return planCrossWalkResponse.getHiosPlanNumber();
			}else{
				throw new Exception(" CROSSWALK_HIOS_PLAN_ID from is empty or NUll " + planCrossWalkResponse);
			}
		}catch(Exception e){
			LOGGER.error("Getting Error While calling GET_CROSSWALK_HIOS_PLAN_ID (PlanMgmt API) : ", e);
			throw new Exception("Getting Error While calling GET_CROSSWALK_HIOS_PLAN_ID (PlanMgmt API) "+ e.getMessage());
		}
	}

	/**
	 * 
	 * @param planId
	 * @param marketType
	 * @return
	 */
	public PlanResponse getPlanInfo(String planId, String marketType) {
		PlanRequest planRequest = new PlanRequest();
		PlanResponse planResponse = new PlanResponse();
		planRequest.setPlanId(planId);
		if (isNotNullAndEmpty(marketType) && marketType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
			planRequest.setMarketType("INDIVIDUAL");
		}
		String planResponseString = null;
		GIWSPayload giWsPayloadObj =null; 
		boolean isSuccess=false;
		try {
			giWsPayloadObj=	EnrollmentGIPayloadUtil.save(giwsPayloadService, platformGson.toJson(planRequest), null,
					GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL, "GET_PLAN_DATA_BY_ID_URL", isSuccess);
			planResponseString = ghixRestTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL, planRequest, String.class);
			isSuccess=true;
			giWsPayloadObj.setResponsePayload(planResponseString);
			if (StringUtils.isNotBlank(planResponseString)) {
				planResponse = platformGson.fromJson(planResponseString, PlanResponse.class);
			}
		} catch (Exception ex) {
			LOGGER.error("exception in getting plan data from the plan management", ex);
		}finally{
			EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj,giWsPayloadObj.getResponsePayload() , isSuccess);
		}
		return planResponse;
	}

	public PlanListResponse getPlanListInfo(Set<String> planIds, String marketType) {
		PlanRequest planRequest = new PlanRequest();
		PlanListResponse planResponse = new PlanListResponse();
		//planRequest.setPlanId(planId);
		planRequest.setPlanIds(planIds.stream().map(s->Integer.valueOf(s)).collect(Collectors.toList()));
		if (isNotNullAndEmpty(marketType) && marketType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
			planRequest.setMarketType("INDIVIDUAL");
		}
		String planResponseString = null;
		GIWSPayload giWsPayloadObj =null; 
		boolean isSuccess=false;
		try {
			giWsPayloadObj=	EnrollmentGIPayloadUtil.save(giwsPayloadService, platformGson.toJson(planRequest), null,
					GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_DATA_BY_LIST_OF_IDS_URL, "GET_PLAN_DATA_BY_LIST_OF_IDS_URL", isSuccess);
			planResponseString = ghixRestTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_LIST_OF_IDS_URL, planRequest, String.class);
			isSuccess=true;
			giWsPayloadObj.setResponsePayload(planResponseString);
			if (StringUtils.isNotBlank(planResponseString)) {
				planResponse = platformGson.fromJson(planResponseString, PlanListResponse.class);
			}
			if(planResponse!=null && planResponse.getStatusCode().compareTo(HttpStatus.OK)!=0){
				LOGGER.error("Error in getting plan data from the plan management :: response sent a status code "+planResponse.getStatusCode());
			}
		} catch (Exception ex) {
			LOGGER.error("exception in getting plan data from the plan management", ex);
		}finally{
			EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj,giWsPayloadObj.getResponsePayload() , isSuccess);
		}
		return planResponse;
	}

	public EntityResponseDTO getAssisterBrokerDetail(String id) {
		String brokerInfoResponse = null;
		if (StringUtils.isNotBlank(id)) {
			brokerInfoResponse = ghixRestTemplate.getForObject(BrokerServiceEndPoints.GET_ASSISTER_BROKER_DETAIL + id, String.class);
			if(brokerInfoResponse==null){
				LOGGER.error("No Broker data found : null brokerInfoResponse received : "+ BrokerServiceEndPoints.GET_ASSISTER_BROKER_DETAIL+id);
			}else{
				return platformGson.fromJson(brokerInfoResponse, EntityResponseDTO.class);
			}
		}
		return null;
	}

	/**
	 * @author panda_p
	 * @since 15-06-2018
	 * Jira - HIX-108051 - 
	 * Agent information not passing in EDI file for Plan per Person Case
	 * @param brokerId
	 * @return EntityResponseDTO
	 */
	public EntityResponseDTO getBrokerDetail(String brokerId) {
		String brokerInfoResponse = null;
		if (StringUtils.isNotBlank(brokerId)) {
			brokerInfoResponse = ghixRestTemplate.getForObject(BrokerServiceEndPoints.GET_BROKER_DETAIL + brokerId, String.class);
			if(brokerInfoResponse==null){
				LOGGER.error("No Broker data found :null brokerInfoResponse received : "+BrokerServiceEndPoints.GET_BROKER_DETAIL + brokerId);
			}else{
				return (EntityResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(brokerInfoResponse);
			}
		}
		return null;
	}

	public ShopResponse getEmployerInfo(String employerId, Long employeeAppId) throws GIException{
		ShopResponse shopResponse = null;
		if (isNotNullAndEmpty(employerId)) {
			//LOGGER.info("Calling shop service to get employer info for employerId =="+employerId);
			LOGGER.info("shop service URL == "+ShopEndPoints.GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYEE_APP_ID + "?employerId=" + employerId + "&employeeApplicationId=" + employeeAppId);
			String postResponse =null;
			try{
				postResponse = ghixRestTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYEE_APP_ID + "?employerId=" + employerId + "&employeeApplicationId=" + employeeAppId, String.class);
				if(StringUtils.isNotBlank(postResponse)){
					XStream xstream = GhixUtils.getXStreamStaxObject();
					shopResponse = (ShopResponse) xstream.fromXML(postResponse);
				}
				else{
					LOGGER.error("Error fetching Employer data for EmployerId :: " + employerId);
					throw new GIException("Error fetching Employer data");
				}
			}catch(RestClientException re){
				//				LOGGER.error("Error fetching Employer data for EmployerId :: " + employerId);
				//				LOGGER.error("Message : "+ re.getMessage());
				//				LOGGER.error("Cause : "+ re.getCause());
				LOGGER.error("Error fetching Employer data for EmployerId :: "+ employerId, re);
				throw new GIException("Error fetching Employer data",re);
			}
			catch (Exception e) {
				//				LOGGER.error("Error fetching Employer data for EmployerId :: " + employerId);
				//				LOGGER.error("Message : "+ e.getMessage());
				//				LOGGER.error("Cause : "+ e.getCause());
				LOGGER.error("Error fetching Employer data for EmployerId :: "+ employerId, e);
				throw new GIException("Error fetching Employer data",e);
			}

		}else{
			throw new GIException("No Employer found");
		}
		return shopResponse;
	}

	/**
	 * 
	 * @param issuerIdList
	 * @return
	 * @throws GIException
	 */
	public IssuerBrandNameResponseDTO getIssuerBrandNameByIssuerIdList(List<Integer> issuerIdList)throws GIException
	{
		if (issuerIdList != null && !issuerIdList.isEmpty() && issuerIdList.contains(null)) {
			issuerIdList = issuerIdList.stream()                
					.filter(line -> (line != null))  
					.collect(Collectors.toList());
		}

		if (issuerIdList != null && !issuerIdList.isEmpty()) {
			IssuerBrandNameResponseDTO issuerBrandNameResponseDTO = null;
			IssuerListRequestDTO issuerListRequestDTO = new IssuerListRequestDTO();
			issuerListRequestDTO.setIssuerIds(issuerIdList);
			try {
				issuerBrandNameResponseDTO = ghixRestTemplate.postForObject(
						GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_BRANDNAME_BY_ISSUER_ID_LIST, issuerListRequestDTO,
						IssuerBrandNameResponseDTO.class);

			} catch (Exception ex) {
				LOGGER.error("Exception occured in Rest call for getIssuerBrandNameByIssuerIdList: ", ex);
				throw new GIException(ex);
			}
			return issuerBrandNameResponseDTO;
		} else {
			throw new GIException(EnrollmentConstants.ERROR_CODE_201, "Null or Empty request found ",
					EnrollmentConstants.HIGH);
		}
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public LifeSpanEnrollmentQuotingResponseDTO getPlanManagementQuotingResponse(final LifeSpanEnrollmentQuotingRequestDTO lifeSpanEnrollmentQuotingRequestDTO) throws Exception{
		LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuotingResponseDTO=null;
		boolean isSuccess=false;
		GIWSPayload giWsPayloadObj =null; 
		try{

			if(lifeSpanEnrollmentQuotingRequestDTO!=null){
				giWsPayloadObj=	EnrollmentGIPayloadUtil.save(giwsPayloadService, platformGson.toJson(lifeSpanEnrollmentQuotingRequestDTO), null,
						GhixEndPoints.PlanMgmtEndPoints.LIFE_SPAN_ENROLLMENT_QUOTING, "ENROLLMENTMT_REQUOTING", isSuccess);
				String requotingResponse=ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.LIFE_SPAN_ENROLLMENT_QUOTING, lifeSpanEnrollmentQuotingRequestDTO, String.class);
				giWsPayloadObj.setResponsePayload(requotingResponse);
				isSuccess=true;
				if(EnrollmentUtils.isNotNullAndEmpty(requotingResponse)){

					lifeSpanEnrollmentQuotingResponseDTO= platformGson.fromJson(requotingResponse, LifeSpanEnrollmentQuotingResponseDTO.class);
					if(lifeSpanEnrollmentQuotingResponseDTO!=null ){
						if(!lifeSpanEnrollmentQuotingResponseDTO.getStatus().equalsIgnoreCase(EnrollmentConstants.SUCCESS)) {
							LOGGER.error(lifeSpanEnrollmentQuotingResponseDTO.getErrCode() +" :: "+lifeSpanEnrollmentQuotingResponseDTO.getErrMsg());
							throw new Exception(lifeSpanEnrollmentQuotingResponseDTO.getErrCode() +" :: "+lifeSpanEnrollmentQuotingResponseDTO.getErrMsg());
						}

					}else{
						LOGGER.error("Plan Management Requoting response is null");
						throw new Exception("Plan Management Requoting response is null");
					}
				}else{
					LOGGER.error("Plan Management Requoting response is null");
					throw new Exception("Plan Management Requoting response is null");
				}
			}
		}catch(Exception e){
			//throw exception for failure conditions
			LOGGER.error("Exception occurred while calling LIFE_SPAN_ENROLLMENT_QUOTING API: ",e);
			throw new Exception("Enrollment Requoting Failure: Call to plan management API failed :: "+e.getMessage(), e);
		}finally{
			EnrollmentGIPayloadUtil.updateResponsePayload(giwsPayloadService, giWsPayloadObj,giWsPayloadObj.getResponsePayload() , isSuccess);
		}
		return lifeSpanEnrollmentQuotingResponseDTO;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> fetchSepEventDetailsByHousehold(String householdCaseId, String enrollmentYear){
		Map<String, String> response = null;
		Map<String, String> request = new HashMap<>();
		request.put("houseHoldId", householdCaseId);
		request.put("enrollmentYear", enrollmentYear);
		try {
			response = ghixRestTemplate.postForObject(GhixEndPoints.EligibilityEndPoints.FETCH_SEP_EVENT_DETAILS, request, HashMap.class);	
		}catch(Exception ex) {
			LOGGER.error("Exception occured in Rest call for fetchSepEventDetailsByHousehold: ", ex);
		}
		return response;
	}
	
	/**
	 * @author Anoop
	 * @since 02-01-2019
	 * Jira - HIX-111489 - 
	 * get external broker details
	 * @param cmrHouseholdId
	 * @return DesignateAssisterResponse
	 */
	public DesignateAssisterResponse getExternalBrokerDetails(String cmrHouseholdId) {
		String designateAssisterResponse = null;
		if (cmrHouseholdId != null) {
			int householdId = Integer.valueOf(cmrHouseholdId);
			designateAssisterResponse = ghixRestTemplate.getForObject(ExternalAssisterEndPoints.GET_ASSISTER_DETAIL_FOR_HOUSEHOLD + "/" + householdId ,String.class);
			if(designateAssisterResponse==null){
				LOGGER.error("No external Broker data found : got respose as null "+ExternalAssisterEndPoints.GET_ASSISTER_DETAIL_FOR_HOUSEHOLD + cmrHouseholdId);
			}else{
				return platformGson.fromJson(designateAssisterResponse, DesignateAssisterResponse.class);
			}
		}
		return null;
	}
	
	/**
	 * @author Anoop
	 * @since 05-22-2019
	 * Jira - HIX-114482 - 
	 * update Ssap Application table application status
	 * @param ssapApplicationRequest
	 * @return Integer
	 */
	public SsapApplicationResponse updateSsapApplicationById(SsapApplicationRequest ssapApplicationRequest) {
		SsapApplicationResponse ssapApplicationResponse = null;
		if (ssapApplicationRequest != null) {
			ssapApplicationResponse = ghixRestTemplate.postForObject(
					GhixEndPoints.ELIGIBILITY_URL + "application/updateSsapApplicationById", ssapApplicationRequest,
					SsapApplicationResponse.class);
			if(ssapApplicationResponse != null && ssapApplicationResponse.getCount() == 0){
				LOGGER.info("No rows been affected in the updateSsapApplication call ");
				return ssapApplicationResponse;
			}else{
				return ssapApplicationResponse;
			}
		}
		return null;
	}
	
	/**
	 * @author Anoop
	 * @since 06-21-2019
	 * Jira - HIX-115325 - 
	 * get application status from id
	 * @param ssapApplicationId
	 * @return String
	 */
	public String getApplicationStatusById(Long ssapApplicationId) {
		String applciationStatus = null;
		if (ssapApplicationId != null) {
			applciationStatus = ghixRestTemplate.getForObject(
					GhixEndPoints.EligibilityEndPoints.GET_SSAP_APPLICATION_STATUS + "/" + ssapApplicationId,
					String.class);
			if (applciationStatus == null) {
				LOGGER.info("No status been returned from eligibility");
			} else {
				return applciationStatus;
			}
		}
		return null;
	}
	
	/**
	 * @author Anoop
	 * @since 07-15-2019
	 * Jira - HIX-115619  
	 * get renewalStatus
	 * @param houseHoldCaseId
	 * @return String
	 */
	public String getRenewalStatus(Integer houseHoldCaseId, String year) {
		String renewalStatus = null;
		if (houseHoldCaseId != null) {
			renewalStatus = ghixRestTemplate
					.getForObject(GhixEndPoints.EligibilityEndPoints.FETCH_HOUSEHOLD_RENEWAL_STATUS + "/"
							+ houseHoldCaseId + "/" + year, String.class);
			if (renewalStatus == null) {
				LOGGER.info("No status been returned from fetchHouseholdRenewalStatus API");
			} else {
				return renewalStatus;
			}
		}
		return null;
	}
}
