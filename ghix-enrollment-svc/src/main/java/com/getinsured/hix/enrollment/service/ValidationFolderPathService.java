package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.ExchgPartnerLookup;

public interface ValidationFolderPathService {
	
	ExchgPartnerLookup getDropOffLocatonForIndiv834Outbound(String hiosIssuerId,String isa06,String isa05,String gs08,String market,String direction);
	ExchgPartnerLookup getDropOffLocatonForTA1Inbound834(String fileName);
	ExchgPartnerLookup getDropOffLocatonFor999Inbound834(String fileName);
	ExchgPartnerLookup getPickUpLocatonFor834Inbound(String fileName);
	ExchgPartnerLookup findExchgPartnerByHiosIssuerID(String hiosIssuerId,String market,String direction);
	ExchgPartnerLookup findExchgPartnerforGroupInstallByHiosIssuerID(String hiosIssuerId,String market,String direction);
	void setValidationFolderPath(ExchgPartnerLookup exchgPartnerLookup);
	void setISA13Controlnumber(ExchgPartnerLookup exchgPartnerLookup);
	List<ExchgPartnerLookup>findAllHiosIssuerIdForInbound();
	List<ExchgPartnerLookup>findByHiosIssuerId(String hiosIssuerId);
}
