/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.enrollment.Enrollee;

/**
 * @author panda_p
 *
 */
public interface EnrolleeRacePrivateService {
	void deleteEnrolleeRace(Enrollee enrollee);
     
}
