/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.history.Revision;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrolleeRacePrivateRepository;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.model.enrollment.EnrolleeRace;


@Service("enrolleeRaceAuditPrivateService")
@Transactional
public class EnrolleeRaceAuditPrivateServiceImpl implements EnrolleeRaceAuditPrivateService{

	@Autowired	private IEnrolleeRacePrivateRepository enrolleeRaceRepository;
	
	@Override
	public Revision<Integer, EnrolleeRace> findRaceRevisionByDate(int enrolleeRaceId,  Date selectDate) throws ParseException {

		Revision<Integer, EnrolleeRace> revision = enrolleeRaceRepository.findRevisionByDate(enrolleeRaceId, selectDate,EnrollmentPrivateConstants.UPDATED_ON);
		if (revision != null){
			return revision;
		}
		return null;
	}
	
	
}
