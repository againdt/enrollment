package com.getinsured.hix.enrollment.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrlReconLkp;

public interface IEnrlReconLkpRepository extends JpaRepository<EnrlReconLkp, Integer> {
	
	@Query("SELECT erp.id FROM EnrlReconLkp erp where erp.fieldName = :fieldName")
	List<Integer> findLkpIdByFieldName(@Param("fieldName") String fieldName);
	
	@Query("SELECT erp.id FROM EnrlReconLkp erp where erp.code = :code")
	List<Integer> findLkpIdByCode(@Param("code") String code);
	
	/**
	 * 
	 * @return
	 */
	@Query("SELECT erp.id, erp.code, erp.label, erp.category FROM EnrlReconLkp erp ORDER by erp.id")
	List<Object[]> getEnrlReconLkpData();
	
}
