/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.cap.consumerapp.PolicyLiteDTO;
import com.getinsured.hix.enrollment.dto.EnrollmentBobDTO;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

/**
 * @author panda_p
 *
 */

public interface IEnrollmentPrivateRepository extends TenantAwareRepository<Enrollment, Integer>{


	List<Enrollment> findByEmployer(Employer employer);
	
	@Query("SELECT en.id," +
			" en.issuerId," +
			" en.employer," +
			" en.insuranceTypeLkp.lookupValueLabel," +
			" en.groupPolicyNumber," +
			" en.grossPremiumAmt," +
			" en.benefitEffectiveDate," +
			" en.benefitEndDate," +
			" en.enrollmentStatusLkp," +
			" en.employeeId," +
			" en.employeeContribution," +
			" ee.healthCoveragePolicyNo," +
			" en.employerContribution" +
		" FROM Enrollment as en," + 
			 " Enrollee as ee"+
		" where ee.enrollment.id = en.id" +
			" and en.employer.id = :employerid" +
			" and ee.personTypeLkp.lookupValueCode ='SUBSCRIBER'")
	List<Object[]> findByEmployerId(@Param("employerid") int employerid);
	
	@Query("SELECT en.id," +
				" en.issuerId," +
				" en.employer," +
				" en.insuranceTypeLkp.lookupValueLabel," +
				" en.groupPolicyNumber," +
				" en.grossPremiumAmt," +
				" en.benefitEffectiveDate," +
				" en.benefitEndDate," +
				" en.employeeId," +
				" en.employeeContribution," +
				" ee.healthCoveragePolicyNo," +
				" en.enrollmentStatusLkp," +
				" en.employerContribution" +
			" FROM Enrollment as en," +
				 " Enrollee as ee"+
			" where ee.enrollment.id = en.id" +
				" and en.benefitEffectiveDate <= :currentDate "+
				" and (en.benefitEndDate > :lastInvoiceDate or en.benefitEndDate is null)"+
				" and en.enrollmentStatusLkp.lookupValueCode = :status"+
				" and en.employer.id = :employerid" +
				" and ee.personTypeLkp.lookupValueCode ='SUBSCRIBER'")
	List<Object[]> findCurrentMonthEffectiveEnrollment(@Param("currentDate") Date currentDate,@Param("lastInvoiceDate") Date lastInvoiceDate,@Param("status") String status,@Param("employerid") int employerid);
	

	@Query("FROM Enrollment as an "+
			" where an.enrollmentStatusLkp.lookupValueCode = :status "+
			" and an.benefitEndDate = :benefitEndDate order by an.employer.id, an.insurerName ASC")
	List<Enrollment> findEnrollmentByStatusAndBenefitEndDate(@Param("status") String status, @Param("benefitEndDate") Date benefitEndDate,  Sort sort);

	@Query("FROM Enrollment as an "+
			" where an.enrollmentStatusLkp.lookupValueCode = :status "+
			" and an.employer.id= :employerid " +
			" and an.benefitEndDate = :benefitEndDate order by an.employer.id, an.insurerName ASC")
	List<Enrollment> findEnrollmentByStatusAndEmployerAndBenefitEndDate(@Param("status") String status, @Param("employerid") int employerid, @Param("benefitEndDate") Date benefitEndDate, Sort sort);
	
	@Query("FROM Enrollment as an "+
			" where an.enrollmentTypeLkp.lookupValueId = :enrollmentType "+
			" and an.enrollmentStatusLkp.lookupValueId = :status"+
			" and an.employer.id = :employerid ")
	List<Enrollment> getPendingShopEnrollment(@Param("employerid") int employerid,@Param("enrollmentType") int enrollmentType, @Param("status") int statusType);
	
	@Query("FROM Enrollment as an "+
			" where an.benefitEffectiveDate <= :currentDate "+
			" and an.enrollmentTypeLkp.lookupValueId = :enrollmentType "+
			" and an.enrollmentStatusLkp.lookupValueId = :status"+
			" and an.employer.id = :employerid ")
	List<Enrollment> getPendingShopEnrollment(@Param("employerid") int employerid,
	@Param("currentDate") Date currentDate, @Param("enrollmentType") int enrollmentType, @Param("status") int statusType);
	
	
//	@Query("FROM Enrollment as enrollment where enrollment.indOrderItem.id = :indOrderItemsId")
//	List<Enrollment> findByIndOrderItemId(@Param("indOrderItemsId") int indOrderItemId);
	
	@Query("FROM Enrollment as enrollment where enrollment.issuerSubscriberIdentifier = :issuerSubscriberIdentifier")
	Enrollment findByIssuerSubscriberIdentifier(@Param("issuerSubscriberIdentifier") String issuerSubscriberIdentifier);
	
	
	@Query("FROM Enrollment as enrollment where enrollment.issuerId = :issuerId")
	List<Enrollment> findEnrollmentByIssuerID(@Param("issuerId") Integer issuerId);
	
	@Query("FROM Enrollment as enrollment where enrollment.issuerId = :issuerId and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId and enrollment.updatedOn BETWEEN :startDate AND :endDate ")
	List<Enrollment> findEnrollmentByIssuerIDAndEnrollmentType(@Param("issuerId") Integer issuerId,@Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("enrollmentTypeLookupId") int enrollmentTypeLookupId);
	
	@Query("FROM Enrollment as enrollment where enrollment.issuerId = :issuerId and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId and enrollment.updatedOn BETWEEN :startDate AND :endDate and enrollment.enrollmentStatusLkp.lookupValueCode <> :status ")
	List<Enrollment> findEnrollmentByIssuerIDAndEnrollmentType(@Param("issuerId") Integer issuerId,@Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("enrollmentTypeLookupId") int enrollmentTypeLookupId,@Param("status") String status );

	
	@Query("FROM Enrollment as enrollment where enrollment.issuerId = :issuerId and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId and enrollment.enrollmentStatusLkp.lookupValueCode <>'ABORTED'")
	List<Enrollment> findEnrollmentByIssuerIDAndEnrollmentType(@Param("issuerId") Integer issuerId,@Param("enrollmentTypeLookupId") int enrollmentTypeLookupId);
	
	@Query("FROM Enrollment as enrollment where enrollment.issuerId = :issuerId and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId  and enrollment.enrollmentStatusLkp.lookupValueCode <> :status and enrollment.enrollmentStatusLkp.lookupValueCode <>'ABORTED'")
	List<Enrollment> findEnrollmentByIssuerIDAndEnrollmentType(@Param("issuerId") Integer issuerId,@Param("enrollmentTypeLookupId") int enrollmentTypeLookupId, @Param("status") String status );
	
	@Query("SELECT distinct en.issuerId FROM Enrollment en WHERE en.updatedOn BETWEEN :startDate AND :endDate and en.enrollmentStatusLkp.lookupValueCode <>'ABORTED'")
	List<Integer> findIssuerByupdateDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate );
	
	@Query("SELECT distinct en.issuerId FROM Enrollment en")
	List<Integer> findAllIssuer();
	
	/*@Query("SELECT en.issuer FROM Enrollment en WHERE en.insurerTaxIdNumber = :insurerTaxIdNumber")
	Issuer getIssuerByinsurerTaxIdNumber(@Param("insurerTaxIdNumber") String insurerTaxIdNumber);*/

	@Query("FROM Enrollment as en WHERE en.updatedOn >=  :lastRunDate and en.updatedBy.id = :updatedBy and en.enrollmentStatusLkp.lookupValueCode <>'PENDING'")
	List<Enrollment> getCarrierUpdatedEnrollments(@Param("lastRunDate") Date lastRunDate, @Param("updatedBy") Integer updatedBy);
	
	@Query("SELECT DISTINCT e FROM Enrollment e, Enrollee ee WHERE " +
			"e.id = ee.enrollment.id AND " +
			"((( :planId IS NOT NULL) AND (e.planId =cast(:planId as integer))) OR (( :planId IS NULL) AND ((e.planId IS NULL) OR (e.planId IS NOT NULL))))  AND " +
			"((( :planMarket IS NOT NULL) AND (e.enrollmentTypeLkp.lookupValueId=cast(:planMarket as integer))) OR (( :planMarket IS NULL) AND ((e.enrollmentTypeLkp.lookupValueId IS NULL) OR (e.enrollmentTypeLkp.lookupValueId IS NOT NULL))))  AND " +
			"((( :planLevel IS NOT NULL) AND (e.planLevel in (:planLevel))) OR (( :planLevel IS NULL) AND ((e.planLevel IS NULL) OR (e.planLevel IS NOT NULL))))  AND " +
			"((( :regionName IS NOT NULL) AND (ee.ratingArea =:regionName)) OR (( :regionName IS NULL) AND ((ee.ratingArea IS NULL) OR (ee.ratingArea IS NOT NULL)))) AND" +
			"((( :startDate IS NOT NULL) AND ((TO_DATE(TO_CHAR(e.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE( :startDate,'MM/DD/YYYY')))) OR (( :startDate IS NULL) AND (e.createdOn IS NULL OR e.createdOn IS NOT NULL))) AND " +
			"((( :endDate IS NOT NULL) AND ((TO_DATE(TO_CHAR(e.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE( :endDate,'MM/DD/YYYY')))) OR (( :endDate IS NULL) AND (e.createdOn IS NULL OR e.createdOn IS NOT NULL))) AND " +
			"((( :status IS NOT NULL) AND (e.enrollmentStatusLkp.lookupValueCode = UPPER(:status))) OR (( :status IS NULL) AND ((e.enrollmentStatusLkp.lookupValueCode IS NULL) OR (e.enrollmentStatusLkp.lookupValueCode IS NOT NULL)))) " +
			"order by e.createdOn")
    List<Enrollment> getEnrollmentsByPlanData(@Param("planId") String planId, @Param("planLevel") String planLevel, @Param("planMarket") String planMarket, @Param("regionName") String regionName, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("status") String status);
	
	@Query("SELECT DISTINCT e FROM Enrollment e, Enrollee ee WHERE " +
			"e.id = ee.enrollment.id AND " +
			"((( :issuerName IS NOT NULL) AND (e.insurerName =:issuerName)) OR (( :issuerName IS NULL) AND ((e.insurerName IS NULL) OR (e.insurerName IS NOT NULL))))  AND " +
			"((( :planMarket IS NOT NULL) AND (e.enrollmentTypeLkp.lookupValueId = cast(:planMarket as integer))) OR (( :planMarket IS NULL) AND ((e.enrollmentTypeLkp.lookupValueId IS NULL) OR (e.enrollmentTypeLkp.lookupValueId IS NOT NULL))))  AND " +
			"((( :planLevel IS NOT NULL) AND (e.planLevel in (:planLevel))) OR (( :planLevel IS NULL) AND ((e.planLevel IS NULL) OR (e.planLevel IS NOT NULL))))  AND " +
			"((( :regionName IS NOT NULL) AND (ee.ratingArea =:regionName)) OR (( :regionName IS NULL) AND ((ee.ratingArea IS NULL) OR (ee.ratingArea IS NOT NULL)))) AND " +
			"((( :startDate IS NOT NULL) AND ((TO_DATE(TO_CHAR(e.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE( :startDate,'MM/DD/YYYY')))) OR (( :startDate IS NULL) AND (e.createdOn IS NULL OR e.createdOn IS NOT NULL))) AND " +
			"((( :endDate IS NOT NULL) AND ((TO_DATE(TO_CHAR(e.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE( :endDate,'MM/DD/YYYY')))) OR (( :endDate IS NULL) AND (e.createdOn IS NULL OR e.createdOn IS NOT NULL))) AND " +
			"((( :status IS NOT NULL) AND (e.enrollmentStatusLkp.lookupValueCode = UPPER(:status))) OR (( :status IS NULL) AND ((e.enrollmentStatusLkp.lookupValueCode IS NULL) OR (e.enrollmentStatusLkp.lookupValueCode IS NOT NULL)))) " +
			"order by e.createdOn")
    List<Enrollment> getEnrollmentsByIssuerData(@Param("issuerName") String issuerName, @Param("planLevel") String planLevel, @Param("planMarket") String planMarket, @Param("regionName") String regionName, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("status") String status);
	
	@Query("FROM Enrollment as en  WHERE en.employer.externalId= :externalID AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
	List<Enrollment> findEnrollmentByExternalID(@Param("externalID") String externalID);
	
	@Query("FROM Enrollment as en  WHERE en.employeeId= :employeeID AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
	List<Enrollment> findActiveEnrollmentByEmployeeID(@Param("employeeID") Integer employeeID);
	

	@Query("FROM  Enrollment as en  where en.enrollmentStatusLkp.lookupValueCode = :status  and en.enrollmentTypeLkp.lookupValueCode= :enrollmentType and en.insurerTaxIdNumber= :insurerTaxId and  (en.enrollmentConfirmationDate BETWEEN :startDate AND :endDate ) ")
	List<Enrollment> findEnrollmentByStatusAndUpdateDate(@Param("status") String status, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("enrollmentType") String enrollmentType , @Param("insurerTaxId") String insurerTaxId );

	@Query("FROM  Enrollment as en  where en.enrollmentStatusLkp.lookupValueCode = :status and  (en.enrollmentConfirmationDate BETWEEN :startDate AND :endDate ) ")
	List<Enrollment> findEnrollmentByStatusAndUpdateDate(@Param("status") String status, @Param("startDate") Date startDate, @Param("endDate") Date endDate );


	@Query("SELECT distinct en.insurerTaxIdNumber FROM  Enrollment as en inner join en.enrollees as enrollee where en.enrollmentStatusLkp.lookupValueCode = :status and en.enrollmentTypeLkp.lookupValueCode= :enrollmentType  and  (en.enrollmentConfirmationDate BETWEEN :startDate AND :endDate ) and enrollee.enrolleeLkpValue.lookupValueCode = :status")
	List<String> getinsurerTaxIdNumbers(@Param("status") String status, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("enrollmentType") String enrollmentType  );
	
	@Query("FROM Enrollment as enrollment where enrollment.houseHoldCaseId = :houseHoldCaseId and enrollment.planId = :planID and enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	Enrollment findByHouseHoldCaseIdAndPlanId(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("planID") int planID);
	
	@Query("SELECT e.planId FROM Enrollment as e where e.id = :enrollmentId")
	List<Object[]> getPlanIdAndOrderItemIdByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
	
	@Query("FROM Enrollment as en where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	Enrollment getEnrollmentByExchgSubscriberIdentifier(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId);
	
	@Query("FROM Enrollment as en WHERE en.updatedOn BETWEEN :startDate AND :endDate")
	List<Enrollment> getEnrollmentsUpdatedAfter(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query("SELECT planLevel FROM Enrollment as en WHERE en.assisterBrokerId = :assisterBrokerId and en.brokerRole = :role and (en.updatedOn BETWEEN :startDate AND :endDate ) and en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') ")
	List<String> getEnrollmentPlanLevelCountByBroker(@Param ("assisterBrokerId") Integer assisterBrokerId, @Param("role") String role, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query("SELECT distinct en.employer FROM Enrollment en")
	List<Employer> getDistinctEmployers();
	
	@Query("FROM  Enrollment as en  where en.enrollmentStatusLkp.lookupValueCode IN ('CANCEL','TERM') and  (en.updatedOn BETWEEN :startDate AND :endDate ) ")
	List<Enrollment> findCancelTerminatedEnrollmentByUpdateDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query("FROM Enrollment as en "+
			" where en.cmrHouseHoldId = :cmrHouseholdId "+
			" and en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED')"+
	    	" and (en.enrollmentModalityLkp.lookupValueId != :enrollmentModalityLkp OR en.enrollmentModalityLkp IS NULL)" +
			" and createdOn is not null order by createdOn desc")
	List<Enrollment> findEnrollmentByCmrHouseholdId(@Param("cmrHouseholdId") Integer cmrHouseholdId,@Param("enrollmentModalityLkp") Integer enrollmentModalityLkp);	
	
	/*@Query("FROM Enrollment as en "+
			" where en.ssapApplicationId = :ssapApplicationid "+
			" and en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','CONFIRM','ECOMMITTED')"+
	    	" and (en.enrollmentModalityLkp.lookupValueId != :enrollmentModalityLkp OR en.enrollmentModalityLkp IS NULL)")	
	Enrollment findEnrollmentBySsapApplicationId(@Param("ssapApplicationId") Long ssapApplicationId,@Param("enrollmentModalityLkp") Integer enrollmentModalityLkp);	
	
	*/
	@Query("FROM Enrollment as en where en.cmrHouseHoldId = :cmrHouseholdId and en.enrollmentStatusLkp.lookupValueCode IN ('PENDING') and en.enrollmentModalityLkp.lookupValueCode IN ('Manual')")
	Enrollment findManualEnrollmentByCmrHouseholdId(@Param("cmrHouseholdId") Integer cmrHouseholdId);
	
	@Query("FROM Enrollment as en where en.cmrHouseHoldId = :cmrHouseholdId and en.enrollmentStatusLkp.lookupValueCode IN ('PENDING') AND en.updatedOn = (SELECT max(updatedOn) FROM Enrollment as bn where bn.cmrHouseHoldId = :cmrHouseholdId and bn.enrollmentStatusLkp.lookupValueCode IN ('PENDING'))")
	Enrollment findPendingEnrollmentByCmrHouseholdId(@Param("cmrHouseholdId") Integer cmrHouseholdId);
	
	@Query("FROM Enrollment as en where en.cmrHouseHoldId = :cmrHouseholdId and en.enrollmentStatusLkp.lookupValueCode != 'ECOMMITTED' AND en.updatedOn = (SELECT max(updatedOn) FROM Enrollment as bn where bn.cmrHouseHoldId = :cmrHouseholdId and bn.enrollmentStatusLkp.lookupValueCode != 'ECOMMITTED')")
	Enrollment findNonEcommittedEnrollmentByCmrHouseholdId(@Param("cmrHouseholdId") Integer cmrHouseholdId);
		
	@Query("SELECT en.id, en.issuerSubscriberIdentifier, en.groupPolicyNumber, en.CMSPlanID, en.exchgSubscriberIdentifier,"
			+ " ee.firstName, ee.lastName, ee.middleName, ee.suffix, en.employerCaseId"
			+ " FROM Enrollment as en,"
			+ " Enrollee as ee"
			+ " WHERE en.id = :enrollmentId"
			+ " AND ee.enrollment.id = en.id"
			+ " AND ee.personTypeLkp.lookupValueCode='SUBSCRIBER'")
	List<Object[]> getRemittanceDataByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
	
	Enrollment findByTicketId(String ticketId);
	Enrollment findById(Integer enrollmentId);
	
//	@Query("SELECT en.indOrderItem.pldOrder.id FROM Enrollment en WHERE en.indOrderItem.id = :indOrderItemId")
//	Integer getPldOrderId(@Param("indOrderItemId")int indOrderItemId);
	
	@Query("FROM Enrollment as en "+
			" where en.ssapApplicationid = :ssapApplicationId "+
			" and en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','CONFIRM','ECOMMITTED')"+
	    	" and (en.enrollmentModalityLkp.lookupValueId != :enrollmentModalityLkp OR en.enrollmentModalityLkp IS NULL)")	
	List<Enrollment> findEnrollmentBySsapApplicationId(@Param("ssapApplicationId") Long cmrHouseholdId,@Param("enrollmentModalityLkp") Integer enrollmentModalityLkp);
	
	Enrollment findByCarrierAppId(String carrierAppId);
	
	Enrollment findByissuerAssignPolicyNo(String policyNo);
	
	Enrollment findBycarrierAppUID(String carrierAppUID);
	
	@Query("FROM Enrollment as en, Enrollee as el where lower(el.firstName) = :firstName and lower(el.lastName) = :lastName and en.id = el.enrollment.id")
	List<Object[]> findEnrollmentAndEnrolleeDataByName(@Param("firstName")String firstName , @Param("lastName")String lastName);
	
	/**
	 * 
	 * @param firstName - cannot be null
	 * @param lastName - cannot be null
	 * @return
	 */
	 @Query("select e, hh from Enrollment as e, Household as hh where e.cmrHouseHoldId = hh.id and lower(hh.firstName) = :firstName and lower(hh.lastName) = :lastName")
	 List<Object[]> findByFirstNameAndLastNameLowercase(@Param("firstName") String firstName, @Param("lastName") String lastName);

//	 @Query("select e, hh from Enrollment as e, Household as hh where e.cmrHouseHoldId = hh.id and lower(hh.firstName) = :firstName and lower(hh.lastName) = :lastName")
//	 List<Object[]> findEnrollmentAndHouseholdByIdFirstNameAndLastName(@Param("firstName") String firstName, @Param("lastName") String lastName);

	Enrollment findByIdAndCarrierAppId(Integer enrollmentId, String carrierAppId);
	
	@Query("FROM Enrollment as en where en.d2cEnrollmentId = :d2CEnrollmentID and en.enrollmentModalityLkp.lookupValueCode = :modality")
	Enrollment findD2CEnrollment(@Param("d2CEnrollmentID")Long d2CEnrollmentID, @Param("modality")String modality);
    
    /* CAP  : Queries for enrollment to populate data in view application */
	@Query("FROM Enrollment as en where en.ssapApplicationid = :ssapApplicationId and enrollmentStatusLkp.lookupValueCode != 'ABORTED'")
	Enrollment findBySsapAppIdNotAborted(@Param("ssapApplicationId") long ssapApplicationId);
	
	@Query("FROM Enrollment as en where en.ssapApplicationid = :ssapApplicationId")
	Enrollment findBySsapApplicationId(@Param("ssapApplicationId") long ssapApplicationId);
	
	/* CAP  : Queries for enrollment to populate data in view application ENDS*/

    @Query("select e from Enrollment e where e.id in :enrollmentIds")
    List<Enrollment> findEnrollmentById(@Param("enrollmentIds") List<Integer> enrollmentIds);

    @Query("select e from Enrollment e where e.enrollmentStatusLkp.lookupValueCode in :lookupValueCodes and e.createdOn <= :createdOn")
    List<Enrollment> findEnrollmentIdByEnrollmentStatusLkp_lookupValueCodeAndCreatedOnBefore(@Param("lookupValueCodes") List<String> lookupValueCodes,
                                                                                             @Param("createdOn") Date createdOn);
    
    @Query(" SELECT new com.getinsured.hix.dto.cap.consumerapp.PolicyLiteDTO(en.insurerName, " +
    		"en.carrierAppUrl, en.planName,en.id, en.insuranceTypeLkp.lookupValueLabel, "+
			 " en.grossPremiumAmt,exTyplkp.lookupValueLabel , en.cmrHouseHoldId, count(ee.id), "+
			 " enStatuslkp.lookupValueLabel, en.createdOn  , en.benefitEffectiveDate)  "+
			  " FROM Enrollment as  en " +
			  " left outer join en.enrollees as ee " +
			  " left outer join en.exchangeTypeLkp as exTyplkp " +
			  " left outer join en.enrollmentTypeLkp as enTyplkp " +
			  " left outer join en.enrollmentStatusLkp as enStatuslkp " +
			  " where enStatuslkp.lookupValueLabel NOT IN ('eCommitted','Aborted') and en.cmrHouseHoldId = :cmrHouseholdId group by   "+
			  " en.insurerName, en.carrierAppUrl, en.planName,en.id, en.insuranceTypeLkp.lookupValueLabel, "+
			 " en.grossPremiumAmt,exTyplkp.lookupValueLabel , en.cmrHouseHoldId,  "+
			 " enStatuslkp.lookupValueLabel,en.createdOn  , en.benefitEffectiveDate  ")
	List<PolicyLiteDTO> findEnrollmentListByCmrHouseholdId(@Param("cmrHouseholdId") Integer cmrHouseholdId);

    @Query("SELECT count(*) FROM Enrollment as en where en.cmrHouseHoldId = :cmrHouseholdId and en.enrollmentStatusLkp.lookupValueCode != 'ECOMMITTED' AND en.insuranceTypeLkp.lookupValueCode = :appType")
    Long findCountByCmrIdAndPlanTypeForManual(@Param("cmrHouseholdId")  Integer cmrHouseholdId, @Param("appType") String appType);

	@Query("select e from Enrollment e  where e.enrollmentStatusLkp.lookupValueCode = :status")
	List<Enrollment> findEnrollmentByStatus(@Param("status") String status);

	@Query(nativeQuery = true,
			value = "select count(distinct e.id) from Enrollment e join CMR_HOUSEHOLD h ON (e.CMR_HOUSEHOLD_ID = h.ID) " +
					"join ELIG_LEAD l ON (h.ELIG_LEAD_ID = l.ID) where l.AFFILIATE_ID = :affiliateId")
	BigDecimal countEnrollmentsByAffiliateId(@Param("affiliateId") Long affiliateId);
	
	/**
	 * Fetches the ind_order_item_id of all enrollments whose enrollee do not have the phone numbers
	 * @return List<Integer> indOrderItemIdList
	 */
//  HIX-67133 Remove DB FK Constraint and Model reference for PldOrderItem from Enrollment
//	@Query("SELECT DISTINCT(e.indOrderItem.id) FROM Enrollment e, Enrollee ee WHERE e.id = ee.enrollment.id "
//			+ "AND ee.primaryPhoneNo IS null AND e.indOrderItem.id IS NOT null "
//			+ "AND ee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT') "
//			+ "AND e.enrollmentModalityLkp.lookupValueCode IN ('FFM_REDIRECT','FFM_PROXY') "
//			+ "AND e.exchangeTypeLkp.lookupValueCode IN ('ONEXCHANGE') "
//			+ "AND e.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED')")
//	List<Integer> getIndOrderItemIdForMissingPhNo();
//	
	/**
	 * Fetches the ind_order_item_id of all enrollments whose enrollee do not have the phone numbers
	 * @return List<Long> ssapApplicationid
	 */
	@Query("SELECT DISTINCT(e.ssapApplicationid) FROM Enrollment e, Enrollee ee WHERE e.id = ee.enrollment.id "
			+ "AND ee.primaryPhoneNo IS null AND e.ssapApplicationid IS NOT null "
			+ "AND ee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT') "
			+ "AND e.enrollmentModalityLkp.lookupValueCode IN ('FFM_REDIRECT','FFM_PROXY') "
			+ "AND e.exchangeTypeLkp.lookupValueCode IN ('ONEXCHANGE') "
			+ "AND e.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED')")
	List<Long> getSsapApplicationIdForMissingPhNo();
	
//	@Query("FROM Enrollment as enrollment where enrollment.indOrderItem.id = :indOrderItemsId and enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED')")
//	List<Enrollment> findByIndOrderItemIdActive(@Param("indOrderItemsId") int indOrderItemId);
	
	@Query("FROM Enrollment as enrollment where enrollment.ssapApplicationid = :ssapApplicationid and enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED')")
	List<Enrollment> findBySsapApplicationIdActive(@Param("ssapApplicationid") Long ssapId);
	
	@Query("SELECT carrierAppId, "
			+ "enrollmentStatusLkp.lookupValueCode, "
			+ "insuranceTypeLkp.lookupValueLabel, "
			+ "submittedToCarrierDate, "
			+ "benefitEffectiveDate, "
			+ "grossPremiumAmt, "
			+ "aptcAmt, "
			+ "capAgentId, "
			+ "exchangeTypeLkp.lookupValueLabel,"
			+ "planLevel, "
			+ "planName, "
			+ "planId, "
			+ "insurerName "
			+ "FROM Enrollment "
			+ "WHERE id = :enrollmentId")
	List<Object[]> getEnrollmentCapData(@Param("enrollmentId") Integer enrollmentId);
	
	@Modifying
	@Transactional
	@Query("UPDATE Enrollment SET cmrHouseHoldId = :cmrHouseholdId WHERE ssapApplicationid in (:ssapIdList)")
	int updateEnrollmentCmrHouseholdBySsapId(@Param("cmrHouseholdId") Integer cmrHouseholdId, @Param("ssapIdList") List<Long> ssapIdList);

	/**
	 * 
	 * @param insurerName
	 * @param policyId
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ "  enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE LOWER(enr.insurerName) LIKE '%'||LOWER(:insurerName)||'%' "
			+ " AND enr.id IN (:enrollmentIds)") 
	List<EnrollmentBobDTO> getEnrollmentBobDTOByIdAndCarrierName(@Param("insurerName") String insurerName, @Param("enrollmentIds") List<Integer> enrollmentIds);
	
	/**
	 * 
	 * @param insurerName
	 * @param policyId
	 * @param statusList
	 * @return
	 */
	@Query(nativeQuery = true, 
			value = "SELECT en.id as enrollmentId, en.benefit_Effective_Date as effectiveDate,insuranceTypeLkp.lookup_Value_Code as insuranceType,"
			+ " en.ISSUER_ASSIGN_POLICY_NO as policyNo, en.EXCHANGE_ASSIGN_POLICY_NO as exchangePolicyNo, en.CARRIER_APP_ID as applicationId,"
			+ " en.CARRIER_APP_UID as applicationUId, enrollmentStatusLkp.lookup_Value_Code as enrollStatusCode, en.RENEWAL_FLAG as renewalFlag, en.issuer_id as issuerId "		
			+ " FROM Enrollment AS en "
			+ " left outer join lookup_value as insuranceTypeLkp on en.INSURANCE_TYPE_LKP = insuranceTypeLkp.lookup_value_id "
			+ " left outer join lookup_value as enrollmentStatusLkp on  en.ENROLLMENT_STATUS_LKP = enrollmentStatusLkp.lookup_value_id "
			+ " left outer join CMR_MEMBER as P ON P.CMR_HOUSEHOLD_ID=en.CMR_HOUSEHOLD_ID "
			+ " WHERE  LOWER(en.INSURER_NAME) LIKE '%'||LOWER(:insurerName)||'%' "
			+ " AND P.RELATIONSHIP='SELF' AND (P.member_Extension -> 'medicare' ->> 'hicn' = :hicn) "
			+ " AND enrollmentStatusLkp.lookup_Value_Code IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE') AND en.tenant_id = :tenantId ") 
	List<Object[]> getEnrollmentByHICN(@Param("insurerName") String insurerName, @Param("hicn") String hicn, @Param("tenantId") Long tenantId);
	
	/**
	 * 
	 * @param policyId
	 * @param statusList
	 * @return
	 */
	@Query(nativeQuery = true, 
			value = "SELECT en.id as enrollmentId, en.benefit_Effective_Date as effectiveDate,insuranceTypeLkp.lookup_Value_Code as insuranceType,"
			+ " en.ISSUER_ASSIGN_POLICY_NO as policyNo, en.EXCHANGE_ASSIGN_POLICY_NO as exchangePolicyNo, en.CARRIER_APP_ID as applicationId,"
			+ " en.CARRIER_APP_UID as applicationUId, enrollmentStatusLkp.lookup_Value_Code as enrollStatusCode, en.RENEWAL_FLAG as renewalFlag, en.issuer_id as issuerId "		
			+ " FROM Enrollment AS en "
			+ " left outer join lookup_value as insuranceTypeLkp on en.INSURANCE_TYPE_LKP = insuranceTypeLkp.lookup_value_id "
			+ " left outer join lookup_value as enrollmentStatusLkp on  en.ENROLLMENT_STATUS_LKP = enrollmentStatusLkp.lookup_value_id "
			+ " left outer join CMR_MEMBER as P ON P.CMR_HOUSEHOLD_ID=en.CMR_HOUSEHOLD_ID "
			+ " WHERE P.RELATIONSHIP='SELF' AND (P.member_Extension -> 'medicare' ->> 'hicn' = :hicn) "
			+ " AND enrollmentStatusLkp.lookup_Value_Code IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE') AND en.tenant_id = :tenantId ") 
	List<Object[]> getEnrollmentByHICN(@Param("hicn") String hicn, @Param("tenantId") Long tenantId);

	/**
	 * 
	 * @param insurerName
	 * @param policyId
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ "  enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE LOWER(enr.insurerName) LIKE '%'||LOWER(:insurerName)||'%' "
			+ " AND enr.issuerAssignPolicyNo = :policyId "
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')") 
	List<EnrollmentBobDTO> getEnrollmentByissuerPolicyId(@Param("insurerName") String insurerName, @Param("policyId") String policyId);
	
	/**
	 * 
	 * @param policyId
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ "  enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE enr.issuerAssignPolicyNo = :policyId "
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')") 
	List<EnrollmentBobDTO> getEnrollmentByissuerPolicyId(@Param("policyId") String policyId);
	
	
	/**
	 * 
	 * @param insurerName
	 * @param carrierAppId
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ " enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE LOWER(enr.insurerName) LIKE '%'||LOWER(:insurerName)||'%' "
			+ " AND enr.carrierAppId = :carrierAppId"
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')") 
	List<EnrollmentBobDTO> getEnrollmentByCarrierAppId(@Param("insurerName") String insurerName, @Param("carrierAppId") String carrierAppId);
	
	/**
	 * 
	 * @param carrierAppId
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ " enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE enr.carrierAppId = :carrierAppId"
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')") 
	List<EnrollmentBobDTO> getEnrollmentByCarrierAppId(@Param("carrierAppId") String carrierAppId);
	
	/**
	 * 
	 * @param insurerName
	 * @param carrierAppUId
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ " enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE LOWER(enr.insurerName) LIKE '%'||LOWER(:insurerName)||'%' "
			+ " AND enr.carrierAppUID = :carrierAppUId"
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')") 
	List<EnrollmentBobDTO> getEnrollmentBycarrierAppUId(@Param("insurerName") String insurerName, @Param("carrierAppUId") String carrierAppUId);

	/**
	 * 
	 * @param carrierAppUId
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ " enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE enr.carrierAppUID = :carrierAppUId"
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')") 
	List<EnrollmentBobDTO> getEnrollmentBycarrierAppUId(@Param("carrierAppUId") String carrierAppUId);
	/**
	 * 
	 * @param insurerName
	 * @param firstName
	 * @param lastName
	 * @param effectiveDate
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ " enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr, Enrollee AS enrl "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE enr.id = enrl.enrollment.id "
			+ " AND (enr.benefitEffectiveDate BETWEEN TO_DATE (:effectiveDateRangeFrom, 'MM/DD/YYYY') AND TO_DATE (:effectiveDateRangeTo, 'MM/DD/YYYY')) "
			+ " AND LOWER(enr.insurerName) LIKE '%'||LOWER(:insurerName)||'%' "
			+ " AND LOWER(enrl.firstName) = :firstName "
			+ " AND LOWER(enrl.lastName) = :lastName "
			+ " AND enrl.personTypeLkp.lookupValueCode ='SUBSCRIBER' "
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')")
	List<EnrollmentBobDTO> findEnrollmentByEnrolleeAndEffectiveDateQuery(@Param("insurerName") String insurerName, @Param("firstName") String firstName, @Param("lastName") String lastName, @Param("effectiveDateRangeFrom") String effectiveDateRangeFrom, @Param("effectiveDateRangeTo") String effectiveDateRangeTo);
	
	/**
	 * 
	 * @param firstName
	 * @param lastName
	 * @param effectiveDate
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ " enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr, Enrollee AS enrl "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE enr.id = enrl.enrollment.id "
			+ " AND (enr.benefitEffectiveDate BETWEEN TO_DATE (:effectiveDateRangeFrom, 'MM/DD/YYYY') AND TO_DATE (:effectiveDateRangeTo, 'MM/DD/YYYY')) "
			+ " AND LOWER(enrl.firstName) = :firstName "
			+ " AND LOWER(enrl.lastName) = :lastName "
			+ " AND enrl.personTypeLkp.lookupValueCode ='SUBSCRIBER' "
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')")
	List<EnrollmentBobDTO> findEnrollmentByEnrolleeAndEffectiveDateQuery(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("effectiveDateRangeFrom") String effectiveDateRangeFrom, @Param("effectiveDateRangeTo") String effectiveDateRangeTo);
	
	/**
	 * 
	 * @param insurerName
	 * @param firstName
	 * @param lastName
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ " enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr, Enrollee AS enrl "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE enr.id = enrl.enrollment.id "
			+ " AND LOWER(enr.insurerName) LIKE '%'||LOWER(:insurerName)||'%' "
			+ " AND LOWER(enrl.firstName) = :firstName"
			+ " AND LOWER(enrl.lastName) = :lastName"
			+ " AND enrl.personTypeLkp.lookupValueCode ='SUBSCRIBER' "
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')")
	List<EnrollmentBobDTO> findEnrollmentByEnrolleeByFirstNameAndLastName(@Param("insurerName") String insurerName, @Param("firstName") String firstName, @Param("lastName") String lastName);
	
	/**
	 * 
	 * @param firstName
	 * @param lastName
	 * @param statusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(enr.id, enr.benefitEffectiveDate, lkp.lookupValueCode,"
			+ " enr.issuerAssignPolicyNo, enr.exchangeAssignPolicyNo, enr.carrierAppId, enr.carrierAppUID, enr.enrollmentStatusLkp.lookupValueCode, enr.renewalFlag, enr.issuerId)"
			+ " FROM Enrollment AS enr, Enrollee AS enrl "
			+ " LEFT JOIN enr.insuranceTypeLkp as lkp"
			+ " WHERE enr.id = enrl.enrollment.id "
			+ " AND LOWER(enrl.firstName) = :firstName"
			+ " AND LOWER(enrl.lastName) = :lastName"
			+ " AND enrl.personTypeLkp.lookupValueCode ='SUBSCRIBER' "
			+ " AND enr.enrollmentStatusLkp.lookupValueCode IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')")
	List<EnrollmentBobDTO> findEnrollmentByEnrolleeByFirstNameAndLastName(@Param("firstName") String firstName, @Param("lastName") String lastName);
	
	@Query("SELECT en.id, en.insurerName, en.planName,"
			+ " lkp.lookupValueCode, lkp.lookupValueLabel,"
			+ " en.issuerAssignPolicyNo, en.carrierAppId, en.benefitEffectiveDate,"
			+ " en.enrollmentStatusLkp.lookupValueCode, en.netPremiumAmt,"
			+ " en.subscriberName, enr.birthDate, enr.primaryPhoneNo,"
			+ " loc.address1, loc.address2, loc.city, loc.state, loc.zip, en.renewalFlag"
			+ " FROM Enrollment AS en, Enrollee AS enr "
			+ " LEFT JOIN enr.homeAddressid loc"
			+ " LEFT JOIN en.insuranceTypeLkp lkp"
			+ " WHERE en.id = enr.enrollment.id AND en.id in(:enrollmentIdList)"
			+ " AND enr.personTypeLkp.lookupValueCode ='SUBSCRIBER'")
	List<Object[]> getEnrollmentAndSubscriberDetails(@Param("enrollmentIdList")List<Integer> enrollmentIdList);
	
	/**
	 * 
	 * @param policyNum
	 * @return
	 */
	@Query(nativeQuery = true, value = "SELECT en.id, en.INSURER_NAME, en.CARRIER_APP_ID, en.ENROLLMENT_STATUS_LKP, en.issuer_id, en.ISSUER_ASSIGN_POLICY_NO"
			+ " FROM Enrollment en WHERE (en.ISSUER_ASSIGN_POLICY_NO = :policyNum OR en.CARRIER_APP_UID = :carrierUID OR en.CARRIER_APP_ID = :appId ) AND "
			+ " en.ENROLLMENT_STATUS_LKP in (:enrollmentStatusIds) AND en.tenant_id = :tenantId ")
	List<Object[]> findEnrollmentByPolicyNo(@Param("appId") String appId, @Param("policyNum") String policyNum, @Param("carrierUID") String carrierUID, @Param("enrollmentStatusIds")List<Integer> enrollmentStatusIds, @Param("tenantId") Long tenantId);
	
	/**
	 * 
	 * @param HICN
	 * @return
	 */
	@Query(nativeQuery = true, value= " SELECT en.id, en.INSURER_NAME, en.CARRIER_APP_ID, en.ENROLLMENT_STATUS_LKP, en.issuer_id, en.ISSUER_ASSIGN_POLICY_NO "
			+ " FROM enrollment en INNER JOIN CMR_MEMBER cm ON cm.CMR_HOUSEHOLD_ID=en.CMR_HOUSEHOLD_ID "
			+ " Where cm.RELATIONSHIP='SELF' AND (cm.member_Extension -> 'medicare' ->> 'hicn' = :hicn) "
			+ " AND en.ENROLLMENT_STATUS_LKP in (:enrollmentStatusIds) AND en.tenant_id = :tenantId ")
	List<Object[]> findEnrollmentByHICN(@Param("hicn") String hicn,  @Param("enrollmentStatusIds")List<Integer> enrollmentStatusIds, @Param("tenantId") Long tenantId);
	
	/**
	 * 
	 * @param carrierName
	 * @param enrollmentStatusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.enrollment.dto.EnrollmentBobDTO(en.id, en.issuerAssignPolicyNo, en.carrierAppId) FROM Enrollment AS en WHERE LOWER(en.insurerName) LIKE '%'||:carrierName||'%' AND "
			+ " en.enrollmentStatusLkp.lookupValueCode IN (:enrollmentStatusList)")
	List<EnrollmentBobDTO> findBobExpectedEnrollmentRecord(@Param("carrierName") String carrierName, @Param("enrollmentStatusList") List<String> enrollmentStatusList);
	
	/**
	 * 
	 * @param enrollmentId
	 * @return
	 */
	@Query("SELECT en.renewalFlag FROM Enrollment AS en WHERE en.id = :enrollmentId")
	String isRenewalFlagSet(@Param("enrollmentId") Integer enrollmentId);
	
	/**
	 * Get Enrollment Status Lookup value code
	 * @param enrollmentId
	 * @return
	 */
	@Query("SELECT submitByUser.id "
			+ " FROM Enrollment as en "
			+ " LEFT JOIN en.submittedBy as submitByUser"
			+ " WHERE en.id = :enrollmentId")
	Integer getEnrollmentStatusNSubmittedInfoById(@Param("enrollmentId") Integer enrollmentId);
	
	@Modifying
	@Transactional
	@Query("UPDATE Enrollment SET renewalFlag = :renewalFlag WHERE id = :enrollmentId ")
	int updateEnrollmentisRenewalFlag(@Param("enrollmentId") Integer enrollmentId, @Param("renewalFlag") String renewalFlag);
	
	@Query("select en.id From Enrollment AS en where ((en.createdOn >= :timestamp "
			+ " AND (en.carrierResendFlag IS NULL or en.carrierResendFlag='')) or en.carrierResendFlag IN ('RESEND')) "
			+ " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED', 'ECOMMITTED')")
	List<Integer> getEnrollmentIdsCreatedAfterTimeStamp(@Param("timestamp")Date timestamp);
	
	@Query("SELECT en.issuerId, en.exchangeAssignPolicyNo, en.netPremiumAmt, "
			+ " en.grossPremiumAmt, "
			+ " en.aptcAmt, en.insuranceTypeLkp.lookupValueCode, "
			+ " en.issuerAssignPolicyNo, "
			+ " en.planName, en.planLevel, en.exchangeTypeLkp.lookupValueLabel, "
			+ " en.tenantId, en.carrierAppId, en.dateClosed, "
			+ " en.benefitEffectiveDate, en.insurerName, en.hiosIssuerId, "
			+ " en.submittedBy.id, en.CMSPlanID, en.submittedToCarrierDate, "
			+ " en.enrollmentStatusLkp.lookupValueCode, en.renewalFlag, "
			+ " en.insurerName, en.id "
			+ " FROM Enrollment AS en WHERE en.id=:id")
	List<Object[]> getEnrollmentsById(@Param("id")Integer id);
	
	@Modifying
	@Transactional
	@Query("UPDATE Enrollment SET carrierResendFlag = :resendFlag, giWsPayloadId = :giWsId WHERE id = :id ")
	void updateEnrollmentCarrierResendFlag(@Param("id") Integer id, @Param("resendFlag") String resendFlag, @Param("giWsId") Integer giWsId);
}
