/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.getinsured.hix.dto.enrollment.EnrolleeDataUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateDTO;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Enrollment Edit Tool Service Implementation
 * @author negi_s
 * @since 02/08/2017
 */
@Service
public class EnrollmentEditToolServiceImpl implements EnrollmentEditToolService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEditToolServiceImpl.class);

	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentRequotingService enrollmentRequotingService;
	@Autowired private LookupService lookupService;
	@Autowired private EnrollmentCreationService enrollmentCreationService;
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired private EnrollmentAhbxSyncService enrollmentAhbxSyncService;
	
	public static final String HIGH_ERROR_SEVERITY="HIGH";

	@Override
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public void updateEnrollmentAddTxnEditTool(EnrollmentUpdateDTO enrollmentUpdateDTO, EnrollmentResponse response, Integer giWsPayloadId) throws GIException {
		LOGGER.info("Enrollment Re-instate as ADD function");
		if(enrollmentUpdateDTO.getId() == null) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_NULL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}else {
			Map<String, String> requestParam = new HashMap<String, String>();

			AccountUser user= enrollmentService.getLoggedInUser();

			setMemberDates(enrollmentUpdateDTO, response);
			Enrollment enrollmentDb= enrollmentRepository.findById(enrollmentUpdateDTO.getId());
			if(enrollmentDb != null){
				Enrollee subscriber= enrollmentDb.getSubscriberForEnrollment();
				if(null == enrollmentUpdateDTO.getMembers().stream().filter(member -> member.getEnrolleeId() == subscriber.getId()).findAny().orElse(null)) {
					response.setErrCode(EnrollmentConstants.ERROR_CODE_226);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_MISSING_SUBSCRIBER);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_MISSING_SUBSCRIBER);
				}
				List<String> uniqueMemberList = new ArrayList<>();
				for(EnrolleeDataUpdateDTO member : enrollmentUpdateDTO.getMembers()) {
					if(uniqueMemberList.contains(member.getExchgIndivIdentifier())) {
						response.setErrCode(EnrollmentConstants.ERROR_CODE_216);
						response.setErrMsg(EnrollmentConstants.ERR_MSG_ENRLMNT_OVRLAP);
						response.setStatus(GhixConstants.RESPONSE_FAILURE);
						throw new GIException(EnrollmentConstants.ERR_MSG_ENRLMNT_OVRLAP);
					}else {
						uniqueMemberList.add(member.getExchgIndivIdentifier());
					}
				}
				boolean isOverlap = enrollmentCreationService.validateEnrollmentOverlap(enrollmentService.prepareCoverageValidationRequest(enrollmentUpdateDTO, enrollmentDb));
				if(isOverlap) {
					response.setErrCode(EnrollmentConstants.ERROR_CODE_216);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_ENRLMNT_OVRLAP);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_ENRLMNT_OVRLAP);
				}
				if(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode())) {
					updateEnrollmentEndDate(enrollmentDb, enrollmentUpdateDTO, user, response);
					updateEnrollmentEffectiveDate(enrollmentDb, enrollmentUpdateDTO, user, response);
					enrollmentDb.setPremiumPaidToDateEnd(null);
					enrollmentDb.setAbortTimestamp(null);
					
					List<Enrollment> enrollmentList = new ArrayList<>();
					enrollmentList.add(enrollmentDb);
					enrollmentList.addAll(enrollmentService.synchronizeQuotingDates(enrollmentDb, user, EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL_QUOTING_DATE_UPDATE));
					
					for(Enrollment enrollment : enrollmentList) {
						enrollmentDb.setUpdatedBy(user);
						enrollmentRequotingService.updateMonthlyPremiumForEnrollment(enrollment, true, false, null, true);	
					}
					updateEnrollmentAptcSS(enrollmentDb, enrollmentUpdateDTO, user, response);
					enrollmentRequotingService.updatePremiumAfterMonthlyAPTCChange(enrollmentDb, true, true, true);
					if(null != enrollmentDb.getAptcAmt() && null != enrollmentDb.getGrossPremiumAmt() && enrollmentDb.getAptcAmt().compareTo(enrollmentDb.getGrossPremiumAmt()) > 0 ){
						//Cap APTC
						capEnrollmentAptc(enrollmentDb);
					}
					
					if(response.getStatus()==null || !response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE) ){
						enrollmentDb.setUpdatedOn(new TSDate());
						enrollmentDb.setUpdatedBy(user);
						enrollmentDb.setGiWsPayloadId(giWsPayloadId);
						
						response = enrollmentService.updateIndividualStatusForReinstatementOrAdd(enrollmentList, response);

						if (response != null && response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && response.getErrCode() == EnrollmentConstants.ERROR_CODE_204) {
							response.setErrCode(EnrollmentConstants.ERROR_CODE_1004);
							response.setErrMsg(EnrollmentConstants.ERR_MSG_ACT_ON_NEW_APP);
							response.setStatus(GhixConstants.RESPONSE_FAILURE);
							throw new GIException(EnrollmentConstants.ERR_MSG_ACT_ON_NEW_APP);
						} else if (response != null && response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
							response.setStatus(GhixConstants.RESPONSE_FAILURE);
							throw new GIException(response.getErrMsg());
						}
						
						enrollmentList = enrollmentService.saveAllEnrollment(enrollmentList);
						requestParam.put(EnrollmentConstants.SEND_IN_834, String.valueOf(enrollmentUpdateDTO.isSend834()).equalsIgnoreCase("true")? EnrollmentConstants.YES: EnrollmentConstants.NO);
						enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(
								enrollmentRepository.findById(enrollmentDb.getId()),
								EnrollmentConstants.EnrollmentAppEvent.ADD_OVERRIDE.toString(), requestParam, enrollmentUpdateDTO.getComment(), user);
						if (EnrollmentConfiguration.isCaCall()) {
							LOGGER.info("Async Call to enrollmentAhbxSyncService saveEnrollmentAhbxSync EDIT_TOOL Add transaction");
							final List<Enrollment> finalEnrollmentList = enrollmentList;
							TransactionSynchronizationManager
									.registerSynchronization(new TransactionSynchronizationAdapter() {
										public void afterCommit() {
											enrollmentAhbxSyncService.saveEnrollmentAhbxSync(finalEnrollmentList,
													enrollmentDb.getHouseHoldCaseId(),
													EnrollmentAhbxSync.RecordType.EDIT_TOOL);
										}
									});
						}
					}
				}
			}else{
				response.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_NOT_FOUND);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
	}

	private void setMemberDates(EnrollmentUpdateDTO enrollmentUpdateDTO, EnrollmentResponse response) throws GIException {
		if(EnrollmentUtils.isNullOrEmpty(enrollmentUpdateDTO.getBenefitEffectiveDateStr())) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_225);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_MISSING_EFFECTIVE_DATE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_MISSING_EFFECTIVE_DATE);
		}
		if(EnrollmentUtils.isNullOrEmpty(enrollmentUpdateDTO.getBenefitEndDateStr())) {
			try {
				enrollmentUpdateDTO.setBenefitEndDateStr("12/31/" + enrollmentUpdateDTO.getBenefitEffectiveDateStr()
				.substring(enrollmentUpdateDTO.getBenefitEffectiveDateStr().lastIndexOf("/") + 1));
			} catch (ParseException e) {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_224);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_DATE_FORMAT);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_DATE_FORMAT);
			}
		}
		if(null != enrollmentUpdateDTO.getMembers() && !enrollmentUpdateDTO.getMembers().isEmpty()) {
			enrollmentUpdateDTO.getMembers().forEach(new Consumer<EnrolleeDataUpdateDTO>() {
				@Override
				public void accept(EnrolleeDataUpdateDTO t) {
					try {
						t.setEffectiveStartDateStr(enrollmentUpdateDTO.getBenefitEffectiveDateStr());
						t.setEffectiveEndDateStr(enrollmentUpdateDTO.getBenefitEndDateStr());
					} catch (ParseException e) {
						LOGGER.error("Unable to parse date");
					}
				}
			});
		}
	}
	
	/**
	 * Updates enrollment effective start date from the DTO received
	 * @param enrollmentDb
	 * @param enrollmentUpdateDTO
	 * @param user
	 * @param response
	 * @throws GIException
	 */
	private void updateEnrollmentEffectiveDate(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDTO, AccountUser user, EnrollmentResponse response ) throws GIException {

		validateEnrollmentStartDateChangeRequest(enrollmentDb, enrollmentUpdateDTO, response, EnrollmentUtils.getPendingTermEnrollees(enrollmentDb.getEnrollees()));

		Date currentEnrlEffectiveDate = enrollmentDb.getBenefitEffectiveDate();
		Date effectiveStartDate=enrollmentUpdateDTO.getBenefitEffectiveDate();
		if(effectiveStartDate.before(currentEnrlEffectiveDate)){
			// say current date is 1 june and new date is 1 March

			//Update Enrollment
			enrollmentDb.setBenefitEffectiveDate(effectiveStartDate);
			if(enrollmentDb.getNetPremEffDate()!=null &&  DateUtils.isSameDay(currentEnrlEffectiveDate,enrollmentDb.getNetPremEffDate() )){
				enrollmentDb.setNetPremEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getEmplContEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getEmplContEffDate())){
				enrollmentDb.setEmplContEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getGrossPremEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getGrossPremEffDate())){
				enrollmentDb.setGrossPremEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getEhbEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getEhbEffDate())){
				enrollmentDb.setEhbEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getStateEhbEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getStateEhbEffDate())){
				enrollmentDb.setStateEhbEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getDntlEhbEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getDntlEhbEffDate())){
				enrollmentDb.setDntlEhbEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getFinancialEffectiveDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getFinancialEffectiveDate())){
				enrollmentDb.setFinancialEffectiveDate(effectiveStartDate);
			}
			if(enrollmentDb.getAptcEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getAptcEffDate())){
				enrollmentDb.setAptcEffDate(effectiveStartDate);
			}
            if(enrollmentDb.getStateSubsidyEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getStateSubsidyEffDate())){
                enrollmentDb.setStateSubsidyEffDate(effectiveStartDate);
            }
			if(enrollmentDb.getSlcspEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getSlcspEffDate()) ){
				enrollmentDb.setSlcspEffDate(effectiveStartDate);
			}

			if(enrollmentDb.getCsrEffDate()!=null && DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getCsrEffDate()) ){
				enrollmentDb.setCsrEffDate(effectiveStartDate);
			}

		}else if(effectiveStartDate.after(currentEnrlEffectiveDate)){
			// say current date is 1 june and new date is 1 Aug

			enrollmentDb.setBenefitEffectiveDate(effectiveStartDate);

			if(enrollmentDb.getAptcEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getAptcEffDate())
					&& effectiveStartDate.after(enrollmentDb.getAptcEffDate())){
				enrollmentDb.setAptcEffDate(effectiveStartDate);
			}
            if(enrollmentDb.getStateSubsidyEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getStateSubsidyEffDate())
                    && effectiveStartDate.after(enrollmentDb.getStateSubsidyEffDate())){
                enrollmentDb.setStateSubsidyEffDate(effectiveStartDate);
            }
			if(enrollmentDb.getEmplContEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getEmplContEffDate())
					&& effectiveStartDate.after(enrollmentDb.getEmplContEffDate())){
				enrollmentDb.setEmplContEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getEhbEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getStateEhbEffDate())
					&& effectiveStartDate.after(enrollmentDb.getStateEhbEffDate())){
				enrollmentDb.setEhbEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getStateEhbEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getStateEhbEffDate())
					&& effectiveStartDate.after(enrollmentDb.getStateEhbEffDate())){
				enrollmentDb.setStateEhbEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getDntlEhbEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getDntlEhbEffDate())
					&& effectiveStartDate.after(enrollmentDb.getDntlEhbEffDate())){
				enrollmentDb.setDntlEhbEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getFinancialEffectiveDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getFinancialEffectiveDate())
					&& effectiveStartDate.after(enrollmentDb.getFinancialEffectiveDate())){
				enrollmentDb.setFinancialEffectiveDate(effectiveStartDate);
			}
			if(enrollmentDb.getNetPremEffDate()!=null &&  !DateUtils.isSameDay(effectiveStartDate,enrollmentDb.getNetPremEffDate() ) 
					&& effectiveStartDate.after(enrollmentDb.getNetPremEffDate())){
				enrollmentDb.setNetPremEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getGrossPremEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getGrossPremEffDate()) 
					&& effectiveStartDate.after(enrollmentDb.getGrossPremEffDate())){
				enrollmentDb.setGrossPremEffDate(effectiveStartDate);
			}
			if(enrollmentDb.getSlcspEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getSlcspEffDate()) 
					&& effectiveStartDate.after(enrollmentDb.getSlcspEffDate())){
				enrollmentDb.setSlcspEffDate(effectiveStartDate);
			}

			if(enrollmentDb.getCsrEffDate()!=null && !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getCsrEffDate()) 
					&& effectiveStartDate.after(enrollmentDb.getCsrEffDate())){
				enrollmentDb.setCsrEffDate(effectiveStartDate);
			}
		}
		enrollmentDb.setUpdatedBy(user);
		// Update Enrollee
		List<Enrollee> enrollees = enrollmentDb.getEnrolleesAndSubscriber();
		if(null != enrollmentUpdateDTO.getMembers() && enrollees!=null && !enrollees.isEmpty()){
			for(EnrolleeDataUpdateDTO member :enrollmentUpdateDTO.getMembers()) {
				List<Enrollee> filteredEnrollees = enrollees.stream().filter(e -> e.getId() == member.getEnrolleeId()).collect(Collectors.toList());
				for(Enrollee enrollee : filteredEnrollees) {
					if(!DateUtil.isEqual(effectiveStartDate, enrollee.getEffectiveStartDate()) && effectiveStartDate.after(enrollee.getBirthDate())){
						enrollee.setEffectiveStartDate(effectiveStartDate);
						enrollee.setTotIndvRespEffDate(effectiveStartDate);
						enrollee.setRatingAreaEffDate(effectiveStartDate);
						if(!enrollee.isEventCreated()){
							enrollmentService.createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_ADDITION, EnrollmentConstants.EVENT_REASON_BENEFIT_SELECTION , user, enrollmentUpdateDTO.isSend834(), false, EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
							enrollee.setEventCreated(true);
						}
						enrollee.setUpdatedBy(user);
					}
				}
				checkEnrollmentStatus(user, enrollmentDb);
			}
		}
	}

	/**
	 * Enrollment coverage start date change validations
	 * @param enrollment
	 * @param enrollmentUpdateDTO
	 * @param response
	 * @param enrollees
	 * @throws GIException
	 */
	private void validateEnrollmentStartDateChangeRequest(Enrollment enrollment, EnrollmentUpdateDTO enrollmentUpdateDTO, EnrollmentResponse response, List<Enrollee> enrollees) throws GIException{

		if(enrollmentUpdateDTO.getBenefitEffectiveDate()==null){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFFDATE_NULL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFFDATE_NULL);
		}

		if(enrollmentUpdateDTO.getBenefitEffectiveDate().after(enrollmentUpdateDTO.getBenefitEndDate()) && !DateUtils.isSameDay(enrollmentUpdateDTO.getBenefitEffectiveDate(), enrollmentUpdateDTO.getBenefitEndDate())){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_212);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
		}

		if(!EnrollmentUtils.isSameYear(enrollmentUpdateDTO.getBenefitEffectiveDate(), enrollmentUpdateDTO.getBenefitEndDate()) && !EnrollmentUtils.isSameYear(enrollmentUpdateDTO.getBenefitEffectiveDate(),enrollment.getBenefitEndDate())){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_215);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
		}

		Enrollee subscriber = enrollment.getSubscriberForEnrollment();
		for(EnrolleeDataUpdateDTO memberDto: enrollmentUpdateDTO.getMembers()){
			if(!DateUtils.isSameDay(memberDto.getEffectiveStartDate(), memberDto.getEffectiveEndDate())){
				if(!DateUtils.isSameDay(enrollmentUpdateDTO.getBenefitEffectiveDate(), memberDto.getEffectiveStartDate()) && enrollmentUpdateDTO.getBenefitEffectiveDate().after(memberDto.getEffectiveStartDate())) {
					// Leads to Orphan Scenario
					response.setErrCode(EnrollmentConstants.ERROR_CODE_207);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_ORPHAN_MEMBER+memberDto.getExchgIndivIdentifier());
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_ORPHAN_MEMBER+memberDto.getExchgIndivIdentifier());
				}
				if(subscriber.getId() == memberDto.getEnrolleeId() && 
						subscriber.getBirthDate()!=null && subscriber.getBirthDate().after(enrollmentUpdateDTO.getBenefitEffectiveDate()) && !DateUtils.isSameDay(subscriber.getBirthDate(), enrollmentUpdateDTO.getBenefitEffectiveDate())){
					// Enrollee birth date outside enrollment coverage
					response.setErrCode(EnrollmentConstants.ERROR_CODE_210);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_BDATE+"subscriber "+memberDto.getExchgIndivIdentifier());
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_BDATE+"subscriber "+memberDto.getExchgIndivIdentifier());
				}
			}
		}
	}

	/**
	 * Updates enrollment end date from the DTO received and reinstates the enrollment
	 * @param enrollmentDb
	 * @param enrollmentUpdateDTO
	 * @param user
	 * @param response
	 * @throws GIException
	 */
	private void updateEnrollmentEndDate(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDTO, AccountUser user, EnrollmentResponse response) throws GIException{

		Date currentEndDate = enrollmentDb.getBenefitEndDate();
		if(enrollmentUpdateDTO.getBenefitEndDate()==null){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFF_END_NULL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFF_END_NULL);
		}

		if(!EnrollmentUtils.isSameYear(enrollmentUpdateDTO.getBenefitEndDate(), enrollmentUpdateDTO.getBenefitEffectiveDate()) && !EnrollmentUtils.isSameYear(enrollmentUpdateDTO.getBenefitEndDate(), enrollmentDb.getBenefitEffectiveDate())){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_215);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
		}

		if(enrollmentUpdateDTO.getBenefitEffectiveDate().after(enrollmentUpdateDTO.getBenefitEndDate()) && !DateUtils.isSameDay(enrollmentUpdateDTO.getBenefitEffectiveDate(), enrollmentUpdateDTO.getBenefitEndDate())){
			response.setErrCode(EnrollmentConstants.ERROR_CODE_212);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
		}

		if(!DateUtils.isSameDay(enrollmentUpdateDTO.getBenefitEndDate(), currentEndDate)){

			List<Enrollee> enrollees= enrollmentDb.getEnrolleesAndSubscriber();
			Date effectiveEndDate;
			if(DateUtils.isSameDay(enrollmentUpdateDTO.getBenefitEndDate(), enrollmentDb.getBenefitEffectiveDate())){
				effectiveEndDate = enrollmentDb.getBenefitEffectiveDate();
			}else{
				effectiveEndDate = EnrollmentUtils.getEODDate(enrollmentUpdateDTO.getBenefitEndDate());
			}

			if(null != enrollmentUpdateDTO.getMembers() && enrollees!=null && !enrollees.isEmpty()){
				if(effectiveEndDate.after(currentEndDate)){
					for(EnrolleeDataUpdateDTO member: enrollmentUpdateDTO.getMembers()){
						List<Enrollee> filteredEnrollees = enrollees.stream().filter(e -> e.getId() == member.getEnrolleeId()).collect(Collectors.toList());
						for(Enrollee enrollee : filteredEnrollees) {
							if(null != enrollmentDb.getEnrollmentConfirmationDate()){
								enrollee.setEnrolleeLkpValue( lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
							}else {
								enrollee.setEnrolleeLkpValue( lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_PENDING));
							}
							enrollee.setDisenrollTimestamp(null);
							enrollee.setEffectiveEndDate(effectiveEndDate);
							if(!enrollee.isEventCreated()){
								enrollmentService.createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_ADDITION, EnrollmentConstants.EVENT_REASON_BENEFIT_SELECTION , user, enrollmentUpdateDTO.isSend834(), false, EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
								enrollee.setEventCreated(true);
							}
						}
					}	
				}
				enrollmentDb.setBenefitEndDate(effectiveEndDate);
				enrollmentDb.setUpdatedBy(user);
				checkEnrollmentStatus(user, enrollmentDb);
			}
		}
	}
	
	/**
	 * Update Enrollment status
	 * @param user
	 * @param enrollmentDb
	 */
	private void checkEnrollmentStatus(AccountUser user, Enrollment enrollmentDb) {
		enrollmentService.checkEnrollmentStatus(user, enrollmentDb);
		if(null != enrollmentDb.getEnrollmentConfirmationDate()) {
			enrollmentDb.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));	
		}
	}

	/**
	 * Updates APTC value for the covered months from the value received from the DTO
	 * @param enrollmentDb
	 * @param enrollmentUpdateDTO
	 * @param user
	 * @param response
	 */
	private void updateEnrollmentAptcSS(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDTO, final AccountUser user,EnrollmentResponse response) {
		Float aptcAmt = enrollmentUpdateDTO.getAptcAmount();
		BigDecimal stateSubsidyAmount = enrollmentUpdateDTO.getStateSubsidyAmount();
		Map<Integer, EnrollmentPremium> enrollmentPremiumMap = enrollmentDb.getEnrollmentPremiumMap();
		int startMonth = EnrollmentUtils.getMonthFromDate(enrollmentDb.getBenefitEffectiveDate());
		int endMonth = EnrollmentUtils.getMonthFromDate(enrollmentDb.getBenefitEndDate());
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(enrollmentDb.getBenefitEffectiveDate());
		for(int month = startMonth; month <= endMonth ; month++) {
			if(enrollmentPremiumMap.containsKey(month)) {
				enrollmentPremiumMap.get(month).setAptcAmount(aptcAmt);
				if(stateSubsidyAmount != null){
					enrollmentPremiumMap.get(month).setStateSubsidyAmount(stateSubsidyAmount);
				}
			}else {
				if(null == enrollmentDb.getEnrollmentPremiums()) {
					enrollmentDb.setEnrollmentPremiums(new ArrayList<>());
				}
				EnrollmentPremium enrollmentPremiumDb = new EnrollmentPremium();
				enrollmentPremiumDb.setAptcAmount(aptcAmt);
				if(stateSubsidyAmount != null){
					enrollmentPremiumDb.setStateSubsidyAmount(stateSubsidyAmount);
				}
				enrollmentPremiumDb.setMonth(month);
				enrollmentPremiumDb.setYear(cal.get(Calendar.YEAR));
				enrollmentPremiumDb.setEnrollment(enrollmentDb);
				enrollmentDb.getEnrollmentPremiums().add(enrollmentPremiumDb);
			}
		}
	}	
	
	/**
	 * Cap enrollment APTC if APTC is higher than gross premium
	 * @param enrollmentDb
	 */
	private void capEnrollmentAptc(Enrollment enrollmentDb) {
		Map<Integer, EnrollmentPremium> enrollmentPremiumMap = enrollmentDb.getEnrollmentPremiumMap();
		int startMonth = EnrollmentUtils.getMonthFromDate(enrollmentDb.getBenefitEffectiveDate());
		int endMonth = EnrollmentUtils.getMonthFromDate(enrollmentDb.getBenefitEndDate());
		int aptcEffMonth = EnrollmentUtils.getMonthFromDate(enrollmentDb.getAptcEffDate());
		Float aptcAmount = enrollmentPremiumMap.get(aptcEffMonth).getAptcAmount();
		Float netPremiumAmount = enrollmentPremiumMap.get(aptcEffMonth).getNetPremiumAmount();
		Float grossPremiumAmount = enrollmentPremiumMap.get(aptcEffMonth).getGrossPremiumAmount();
		for(int month = startMonth; month <= endMonth ; month++) {
			if(enrollmentPremiumMap.containsKey(month)) {
				enrollmentPremiumMap.get(month).setActAptcAmount(aptcAmount);
			}
		}
		enrollmentDb.setAptcAmt(aptcAmount);
		enrollmentDb.setNetPremiumAmt(netPremiumAmount);
		enrollmentDb.setGrossPremiumAmt(grossPremiumAmount);
	}
}
