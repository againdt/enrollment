package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.enrollment.util.EnrollmentReconciliationInfo;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author shanbhag_a
 * @since 04/02/2015
 * 
 * HIX-59688
 */
public interface EnrollmentReconciliationReportService {

	/**
	 * Takes 
	 * 
	 * @param fileName
	 * @param timeStamp
	 * @return 
	 * @throws GIException 
	 */
	boolean serveEnrollmentBatchReconciliationJob(String fileName, String timeStamp, EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException;
	
	/**
	 * Process CSV 
	 * 
	 * @param fileName
	 * @param timeStamp
	 * @return 
	 * @throws GIException 
	 */
	boolean serveEnrollmentBatchCSVReconciliationJob(String fileName, String timeStamp, EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException;
	
	/**
	 * 
	 * @param fileName
	 * @param timeStamp
	 * @return
	 * @throws GIException
	 */
	boolean processEnrollmentReconTrackingFiles(String fileName, String timeStamp, EnrollmentReconciliationInfo enrollmentReconciliationInfo)throws GIException;

	/**
	 * Process inbound tracking files
	 * @param fileName
	 * @param timeStamp
	 * @param enrollmentReconciliationInfo
	 * @return
	 */
	boolean processEnrollmentReconInboundTrackingFiles(String fileName, String timeStamp, EnrollmentReconciliationInfo enrollmentReconciliationInfo)throws GIException;
}
