package com.getinsured.hix.enrollment.carrierfeed.util;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by jabelardo on 7/10/14.
 */
public class CsvFileReader<T> extends FileReader<T> {

    private static final Logger logger = LoggerFactory.getLogger(CsvFileReader.class);

    @Override
    protected List<T> createImportList(Class<T> modelClass) throws IOException {
        List<T> importList = new ArrayList<>();
        ICsvListReader reader = null;
        java.io.FileReader fr = null;
        try {
        	fr = new java.io.FileReader(getDataFilename());
            reader = new CsvListReader(fr, CsvPreference.STANDARD_PREFERENCE);
            int firstDataRow = getMappings().get("firstDataRow").asInt();
            List<String> row;
            int skipRowCount = 0;
            while ((row = reader.read()) != null) {
                if (skipRowCount++ < firstDataRow) {
                    continue;
                }
                T model = importModel(modelClass, getMappings().get("fields"), row);
                importList.add(model);
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
            IOUtils.closeQuietly(fr);
        }
        return importList;
    }

    private T importModel(Class<T> modelClass, JsonNode fieldNodes, List<String> row) {
        T model = null;
        try {
            model = modelClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        int cellNum = 0;
        for (JsonNode fieldNode : fieldNodes) {
            String cell = row.get(cellNum);
            cellNum++;
            if (cell == null) {
                continue;
            }
            String fieldName = fieldNode.get("fieldName").asText();
            JsonNode fieldFormatNode = fieldNode.get("fieldFormat");
            String fieldFormat = fieldFormatNode == null ? null : fieldFormatNode.asText();
            Field field = null;
            try {
                field = model.getClass().getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                logger.error(String.format("Can't find in class %s field %s", modelClass.getCanonicalName(), fieldName) , e);
                continue;
            }
            field.setAccessible(true);
            Object value = getInputValue(modelClass, field, cell, fieldFormat);
            setFieldValue(model, field, value);
        }
        return model;
    }

    private Object getInputValue(Class<T> modelClass, Field field, String inputString, String fieldFormat) {
        Object value = null;
        Class<?> fieldType = field.getType();
        if (fieldType.equals(String.class)) {
            value = inputString;

        } else if (!inputString.isEmpty()) {
            if (fieldType.equals(Date.class) ) {
                value = getDateValue(inputString, fieldFormat);

            } else if (fieldType.equals(Character.class)) {
                value = inputString.charAt(0);

            } else if (fieldType.equals(Boolean.class) ) {
                value = getBooleanValue(inputString);

            } else {
                try {
                    if (fieldType.equals(Byte.class) ) {
                        value = Byte.valueOf(inputString);

                    } else if (fieldType.equals(Short.class) ) {
                        value = Short.valueOf(inputString);

                    } else if (fieldType.equals(Integer.class) ) {
                        value = Integer.valueOf(inputString);

                    } else if (fieldType.equals(Long.class) ) {
                        value = Long.valueOf(inputString);

                    } else if (fieldType.equals(Float.class) ) {
                        value = Float.valueOf(inputString);

                    } else if (fieldType.equals(Double.class) ) {
                        value = Double.valueOf(inputString);

                    } else {
                        logger.warn(String.format("Don't know how to read in class %s field %s type %s",
                                modelClass.getCanonicalName(), field.getName(), fieldType.toString()));
                    }
                } catch (NumberFormatException e) {
                    logger.error(String.format("Don't know how to parse number %s in class %s field %s type %s",
                            inputString, modelClass.getCanonicalName(), field.getName(), fieldType.toString()));
                }
            }
        }
        return value;
    }

    private Boolean getBooleanValue(String inputString) {
        if (Arrays.binarySearch(booleanTrueValues, inputString.toLowerCase()) > -1) {
            return true;
        }
        if (Arrays.binarySearch(booleanFalseValues, inputString.toLowerCase()) > -1) {
            return false;
        }
        logger.warn(String.format("Unknown boolean value %s set to null", inputString));
        return null;
    }

    private Date getDateValue(String inputString, String fieldFormat) {
        String format = fieldFormat == null ? "MM/dd/yyyy" : fieldFormat;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            return formatter.parse(inputString);
        } catch (ParseException e) {
            logger.error(String.format("Can't parse date value with format %s value %s ", format, inputString));
        }
        return null;
    }

    private void setFieldValue(T model, Field field, Object value) {
        try {
            field.set(model, value);
        } catch (IllegalAccessException e) {
            logger.error(String.format("Can't set field value in class %s field %s",
                    model.getClass().getCanonicalName(), field.getName()) , e);
        }
    }

}
