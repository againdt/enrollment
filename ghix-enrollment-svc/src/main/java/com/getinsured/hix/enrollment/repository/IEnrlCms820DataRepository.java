package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.EnrlCms820Data;


public interface IEnrlCms820DataRepository extends JpaRepository<EnrlCms820Data, Integer> {
	
	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENRL_CMS_820_DATA WHERE EXCHANGE_ASSIGNED_POLICY_ID =:exchgAssignedPolicyId AND SUMMARY_ID =:summaryId")
	void deleteEnrlCms820Data(@Param("exchgAssignedPolicyId") Long exchgAssignedPolicyId, @Param("summaryId") Integer summaryId);
	
	@Query("FROM EnrlCms820Data ecd WHERE ecd.exchangeAssignedPolicyId =:exchgAssignedPolicyId AND ecd.summaryId =:summaryId")
	List<EnrlCms820Data> findByExchgAssignedPolicyId(@Param("exchgAssignedPolicyId") Long exchgAssignedPolicyId , @Param("summaryId") Integer summaryId);
	
	@Query("SELECT DISTINCT ecd.exchangeAssignedPolicyId FROM EnrlCms820Data ecd WHERE ecd.summaryId =:summaryId")
	List<Long> getDistinctEnrollmentIds(@Param("summaryId") Integer summaryId);

	@Query("FROM EnrlCms820Data ecd WHERE ecd.summaryId =:summaryId AND ecd.issuerAptcTotal IS NOT NULL AND ecd.issuerCsrTotal IS NOT NULL"
			+ " AND ecd.issuerUfTotal IS NOT NULL AND ecd.exchangePaymentType IN ('APTC','CSR') ORDER BY ecd.id ASC")
	List<EnrlCms820Data> find820DataBySummaryId(@Param("summaryId")Integer summaryId, Pageable pageable);
}
