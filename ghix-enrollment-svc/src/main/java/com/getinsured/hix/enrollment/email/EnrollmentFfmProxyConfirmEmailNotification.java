/**
 * 
 */
package com.getinsured.hix.enrollment.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.enrollment.EnrollmentOffExchangeEmailDTO;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;

/**
 * @author negi_s
 *
 */
@Component
public class EnrollmentFfmProxyConfirmEmailNotification extends	NotificationAgent {


//	private Map<String, String> singleData;
	private EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO;
	
	private static final String PLAN_TYPE = "planType";
	private static final String PLAN_NAME = "planName";
	private static final String OFFICE_VISIT_AMT = "officeVisit";
	private static final String GENERIC_MEDICATION_AMT = "genericMedication";
	private static final String DEDUCTIBLE = "deductible";
	private static final String MAX_OOP = "maxOop";
	private static final String NET_PREMIUM_AMT = "netPremiumAmount";
	private static final String LINK = "link";
	private static final String LOGIN_POSTFIX = "account/user/login";
	private static final String INSURANCE_COMPANY = "insuranceCompany";
	private static final String COMPANY_LOGO = "companyLogo";
	private static final String PHONE_NUM = "phoneNum";
	private static final String LOGO_URL = "logoUrl";
	private static final String REDIRECT_URL = "redirectUrl";
	private static final String CUSTOMER_COMPANY_NAME = "customerCompanyName";
	private static final String DISCLAIMER_CONTENT = "disclaimerContent";
	
	/*public Map<String, String> getSingleData() {
		Map<String,String> bean = new HashMap<String, String>();

		bean.put("customerName", enrollmentOffExchangeEmailDTO.getRecipientName());
		bean.put("host", GhixEndPoints.GHIXWEB_SERVICE_URL );
		
		 * Put Plan Details into bean  
		 
		bean.put(PLAN_TYPE,enrollmentOffExchangeEmailDTO.getPlanType());
		bean.put(OFFICE_VISIT_AMT,enrollmentOffExchangeEmailDTO.getOfficeVisit());
		bean.put(GENERIC_MEDICATION_AMT,enrollmentOffExchangeEmailDTO.getGenericMedication());
		bean.put(DEDUCTIBLE,enrollmentOffExchangeEmailDTO.getDeductible());
		bean.put(MAX_OOP,enrollmentOffExchangeEmailDTO.getMaxOop());
		bean.put(NET_PREMIUM_AMT,enrollmentOffExchangeEmailDTO.getNetPremiumAmount());
		bean.put(PLAN_NAME,enrollmentOffExchangeEmailDTO.getPlanName());
		//bean.put(LINK,enrollmentOffExchangeEmailDTO.getLink());
		bean.put(LINK,GhixEndPoints.GHIXWEB_SERVICE_URL+LOGIN_POSTFIX);
		bean.put(INSURANCE_COMPANY, enrollmentOffExchangeEmailDTO.getInsuranceCompany());
		bean.put(COMPANY_LOGO, enrollmentOffExchangeEmailDTO.getCompanyLogo());
		bean.put(PHONE_NUM, enrollmentOffExchangeEmailDTO.getPhoneNum());
		bean.put(LOGO_URL, enrollmentOffExchangeEmailDTO.getLogoUrl());
		bean.put(REDIRECT_URL, enrollmentOffExchangeEmailDTO.getRedirectUrl());
		bean.put(CUSTOMER_COMPANY_NAME, enrollmentOffExchangeEmailDTO.getCompanyName());
		bean.put(DISCLAIMER_CONTENT, enrollmentOffExchangeEmailDTO.getDisclaimerContent());
		setTokens(bean);

		Map<String, String> data = new HashMap<String, String>();
		data.put("To", enrollmentOffExchangeEmailDTO.getRecipient());
		data.put("Subject", "Congratulations! You've got health insurance");

		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
	}
	
	public void setSingleData(Map<String, String> singleData) {
		this.singleData = singleData;
	}
	*/
	
	public EnrollmentOffExchangeEmailDTO getEnrollmentOffExchangeEmailDTO() {
		return enrollmentOffExchangeEmailDTO;
	}
	public void setEnrollmentOffExchangeEmailDTO(
			EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO) {
		this.enrollmentOffExchangeEmailDTO = enrollmentOffExchangeEmailDTO;
	}
	
	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		Map<String,String> bean = new HashMap<String, String>();
		if(null != enrollmentOffExchangeEmailDTO){
			bean.put("customerName", enrollmentOffExchangeEmailDTO.getRecipientName());
			bean.put("host", GhixEndPoints.GHIXWEB_SERVICE_URL );
			// Put Plan Details into bean  
			bean.put(PLAN_TYPE,enrollmentOffExchangeEmailDTO.getPlanType());
			bean.put(OFFICE_VISIT_AMT,enrollmentOffExchangeEmailDTO.getOfficeVisit());
			bean.put(GENERIC_MEDICATION_AMT,enrollmentOffExchangeEmailDTO.getGenericMedication());
			bean.put(DEDUCTIBLE,enrollmentOffExchangeEmailDTO.getDeductible());
			bean.put(MAX_OOP,enrollmentOffExchangeEmailDTO.getMaxOop());
			bean.put(NET_PREMIUM_AMT,enrollmentOffExchangeEmailDTO.getNetPremiumAmount());
			bean.put(PLAN_NAME,enrollmentOffExchangeEmailDTO.getPlanName());
			//bean.put(LINK,enrollmentOffExchangeEmailDTO.getLink());
			bean.put(LINK,GhixEndPoints.GHIXWEB_SERVICE_URL+LOGIN_POSTFIX);
			bean.put(INSURANCE_COMPANY, enrollmentOffExchangeEmailDTO.getInsuranceCompany());
			bean.put(COMPANY_LOGO, enrollmentOffExchangeEmailDTO.getCompanyLogo());
			bean.put(PHONE_NUM, enrollmentOffExchangeEmailDTO.getPhoneNum());
			bean.put(LOGO_URL, enrollmentOffExchangeEmailDTO.getLogoUrl());
			bean.put(REDIRECT_URL, enrollmentOffExchangeEmailDTO.getRedirectUrl());
			bean.put(CUSTOMER_COMPANY_NAME, enrollmentOffExchangeEmailDTO.getCompanyName());
			bean.put(DISCLAIMER_CONTENT, enrollmentOffExchangeEmailDTO.getDisclaimerContent());
		}
		return bean;
	}
	
	@Override
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		Map<String, String> data = new HashMap<String, String>();
		if(null != enrollmentOffExchangeEmailDTO){
			data.put("To", enrollmentOffExchangeEmailDTO.getRecipient());
			data.put("Subject", "Congratulations! You've got health insurance");
		}
		return data;
	}
	
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}
