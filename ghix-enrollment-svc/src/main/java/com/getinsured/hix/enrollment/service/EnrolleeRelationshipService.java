/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;

/**
 * @author panda_p
 *
 */
public interface EnrolleeRelationshipService {
	EnrolleeRelationship findBySourceEnrolleeAndTargetEnrollee(Enrollee sourceEnrollee, Enrollee targetEnrollee);
	
	/**
	 * 
	 * @param targetEnrolleeId
	 * @return
	 */
	List<EnrolleeRelationship> getEnrolleeRelationshipDataBySourceEnrollee(Integer sourceEnrolleeId);
}
