package com.getinsured.hix.enrollment.util;

public class EnrollmentReconCSVDto {
	
	private String hiosIssuerId;
	private Integer exchgReconId;
	private Integer isa13;
	private Integer gs06;
	private Integer st02;
	private String subscriberLineNo;
	private Integer enrollmentId;
	private Integer enrollmentEventId;
	private String ins03EventTypeCode;
	private String ins04EventReasonCode;
	private String houseHoldCaseId;
	private String errorReason;
	private String isa09;
	private String isa10;
	private String gs04;
	private String gs05;
	private Long batchExecutionId;
	private String ackLevel;
	private String ackCode;
	private String receivedRecord;
	
	public EnrollmentReconCSVDto(){
		
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getErrorReason() {
		return errorReason;
	}
	public void setErrorReason(String errorReason) {
		this.errorReason = errorReason;
	}
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	public String getSubscriberLineNo() {
		return subscriberLineNo;
	}
	public void setSubscriberLineNo(String subscriberLineNo) {
		this.subscriberLineNo = subscriberLineNo;
	}
	public Integer getEnrollmentEventId() {
		return enrollmentEventId;
	}
	public void setEnrollmentEventId(Integer enrollmentEventId) {
		this.enrollmentEventId = enrollmentEventId;
	}
	public String getIns03EventTypeCode() {
		return ins03EventTypeCode;
	}
	public void setIns03EventTypeCode(String ins03EventTypeCode) {
		this.ins03EventTypeCode = ins03EventTypeCode;
	}
	public String getIns04EventReasonCode() {
		return ins04EventReasonCode;
	}
	public void setIns04EventReasonCode(String ins04EventReasonCode) {
		this.ins04EventReasonCode = ins04EventReasonCode;
	}
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}
	public String getIsa09() {
		return isa09;
	}
	public void setIsa09(String isa09) {
		this.isa09 = isa09;
	}
	public String getIsa10() {
		return isa10;
	}
	public void setIsa10(String isa10) {
		this.isa10 = isa10;
	}
	public String getGs04() {
		return gs04;
	}
	public void setGs04(String gs04) {
		this.gs04 = gs04;
	}
	public String getGs05() {
		return gs05;
	}
	public void setGs05(String gs05) {
		this.gs05 = gs05;
	}
	public Long getBatchExecutionId() {
		return batchExecutionId;
	}
	public void setBatchExecutionId(Long batchExecutionId) {
		this.batchExecutionId = batchExecutionId;
	}
	public Integer getExchgReconId() {
		return exchgReconId;
	}
	public void setExchgReconId(Integer exchgReconId) {
		this.exchgReconId = exchgReconId;
	}
	public Integer getIsa13() {
		return isa13;
	}
	public void setIsa13(Integer isa13) {
		this.isa13 = isa13;
	}
	public String getAckLevel() {
		return ackLevel;
	}
	public void setAckLevel(String ackLevel) {
		this.ackLevel = ackLevel;
	}
	public String getAckCode() {
		return ackCode;
	}
	public void setAckCode(String ackCode) {
		this.ackCode = ackCode;
	}
	public Integer getGs06() {
		return gs06;
	}
	public void setGs06(Integer gs06) {
		this.gs06 = gs06;
	}
	public Integer getSt02() {
		return st02;
	}
	public void setSt02(Integer st02) {
		this.st02 = st02;
	}
	public String getReceivedRecord() {
		return receivedRecord;
	}
	public void setReceivedRecord(String receivedRecord) {
		this.receivedRecord = receivedRecord;
	}
}
