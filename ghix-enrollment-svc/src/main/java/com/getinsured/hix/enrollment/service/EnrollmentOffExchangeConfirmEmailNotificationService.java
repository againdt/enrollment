package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

public interface EnrollmentOffExchangeConfirmEmailNotificationService {

	void sendEmailNotification(Household household, Enrollment enrollment, boolean isIndividualMember) throws NotificationTypeNotFound;
}
