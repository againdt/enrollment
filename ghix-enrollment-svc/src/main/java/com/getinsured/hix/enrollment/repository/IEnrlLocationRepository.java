package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Location;

public interface IEnrlLocationRepository  extends JpaRepository<Location, Integer> {

}
