/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepository;


/**
 * @author panda_p
 *
 */
@Repository
public interface IEnrolleeRelationshipPrivateRepository extends JpaRepository<EnrolleeRelationship, Integer>, EnversRevisionRepository<EnrolleeRelationship, Integer, Integer>{
	@Query(" FROM EnrolleeRelationship as en " +
		   " WHERE en.sourceEnrollee.id = :sourceEnrolleeID " +
		   " AND  en.targetEnrollee.id = :targetEnrolleeID " )
	EnrolleeRelationship getRelationshipBySourceEndTargetId(@Param("sourceEnrolleeID") Integer sourceEnrolleeID, @Param("targetEnrolleeID") Integer targetEnrolleeID);
}
