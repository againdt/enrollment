/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.enrollment.EnrollmentEsignature;



/**
 * @author panda_p
 *
 */
@Repository
public interface IEnrollmentEsignaturePrivateRepository extends JpaRepository<EnrollmentEsignature, Integer>{
	
	@Query("SELECT enrollesign FROM EnrollmentEsignature enrollesign WHERE enrollesign.enrollment.id = :enrollmentId")
    EnrollmentEsignature findByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
}
