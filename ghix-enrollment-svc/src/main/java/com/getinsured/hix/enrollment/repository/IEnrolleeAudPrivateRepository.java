/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.platform.repository.TenantAwareRepository;


public interface IEnrolleeAudPrivateRepository extends TenantAwareRepository<EnrolleeAud, AudId>{
	
	@Query("FROM EnrolleeAud as en WHERE en.id = :enrolleeAudID and en.rev = :rev")
	EnrolleeAud getEnrolleeAudByIdAndRev(
			@Param("enrolleeAudID") Integer enrolleeAudID,
			@Param("rev") Integer rev);
	
	@Query("FROM EnrolleeAud as en WHERE en.id = :enrolleeAudID and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','PENDING')")
	List<EnrolleeAud> getEnrolleeByIdRevAndNoPendingStatus(@Param("enrolleeAudID") Integer enrolleeAudID);
}
