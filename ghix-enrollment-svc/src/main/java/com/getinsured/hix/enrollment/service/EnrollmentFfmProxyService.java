package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.dto.enrollment.EnrollmentFfmProxyDTO;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;

public interface EnrollmentFfmProxyService {
	EnrollmentResponse createEnrollment(EnrollmentFfmProxyDTO enrollmentFfmProxyDTO );
	
	
	/**
	 * @author panda_p
	 * @since 16-DEC-2014
	 * 
	 * This Method will update the Enrollment for change in premium amounts while re-attempting proxy FFM flow
	 * 
	 * @param enrollment
	 * @param targetId
	 * @return
	 * @throws Exception
	 */
	Enrollment updateReattemptEnrollment (Enrollment enrollment, String targetId)throws Exception;
		
	
}
