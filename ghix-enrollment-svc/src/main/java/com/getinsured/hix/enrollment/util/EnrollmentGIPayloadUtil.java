package com.getinsured.hix.enrollment.util;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;

public class EnrollmentGIPayloadUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentGIPayloadUtil.class);
	
//	@Autowired private GIWSPayloadService giwsPayloadService;
	
	public static GIWSPayload save(GIWSPayloadService giwsPayloadService, String requestXML, String responseXML, String endpointUrl, String endpointOperationName, Boolean isSuccess) {
		LOGGER.info("EnrollmentGIPayloadUtil @ Save Request Response to Payload Table");
		GIWSPayload giWSPayloadObj = null;
		try {
			giWSPayloadObj = populateGiWSPayload(requestXML, responseXML, endpointUrl, endpointOperationName);
			if (isSuccess) {
				giWSPayloadObj.setStatus("SUCCESS");
			} else {
				giWSPayloadObj.setStatus("FAILURE");
			}
			giWSPayloadObj = giwsPayloadService.save(giWSPayloadObj);
		} catch (Exception e) {
			LOGGER.error("EnrollmentGIPayloadUtil @ Error Saving to Payload Table", e);
			return new GIWSPayload();
		}
		return giWSPayloadObj;
	}
	
	public static boolean updateResponsePayload(GIWSPayloadService giwsPayloadService, GIWSPayload objPayloadFromController, String responseXML, Boolean isSuccess) {
		LOGGER.info("EnrollmentGIPayloadUtil @ Save Request Response to Payload Table");
		try {
			if (null != objPayloadFromController) {
				objPayloadFromController.setResponsePayload(responseXML);
				if (isSuccess) {
					objPayloadFromController.setStatus("SUCCESS");
				} else {
					objPayloadFromController.setStatus("FAILURE");
				}
				giwsPayloadService.save(objPayloadFromController);
			}else{
				LOGGER.error("EnrollmentGIPayloadUtil @ Received payload object is null, nothing to update");
				return false;
			}
		} catch (Exception e) {
			LOGGER.error("EnrollmentGIPayloadUtil @ Error Saving to Payload Table", e);
			return false;
		}
		return true;
	}

	private static GIWSPayload populateGiWSPayload(String requestXML, String responseXML, String endpointUrl, String endpointFunction) {

		GIWSPayload newGiWSPayload = new GIWSPayload();

		newGiWSPayload.setCreatedTimestamp(new TSDate());
		newGiWSPayload.setEndpointUrl(endpointUrl);
		newGiWSPayload.setRequestPayload(requestXML);
		newGiWSPayload.setResponsePayload(responseXML);
		newGiWSPayload.setEndpointFunction(endpointFunction);
		
		return newGiWSPayload;
	}

}
