package com.getinsured.hix.enrollment.util;
import java.lang.management.ManagementFactory;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.enrollment.service.EnrollmentReconciliationReportService;
import com.getinsured.hix.platform.util.exception.GIException;
/**
 * 
 * @author negi_s
 *
 */
public class EnrollmentEDIMonitoringInboundJobThread implements Callable<Boolean> {
	private EnrollmentReconciliationReportService enrollmentReconciliationReportService;
	private String fileName;
	private String timeStamp;
	private EnrollmentReconciliationInfo enrollmentReconciliationInfo;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEDIMonitoringInboundJobThread.class);
	public EnrollmentEDIMonitoringInboundJobThread(String fileName, String timeStamp, EnrollmentReconciliationReportService enrollmentReconciliationReportService, EnrollmentReconciliationInfo enrollmentReconciliationInfo){
		this.fileName = fileName;
		this.timeStamp = timeStamp;
		this.enrollmentReconciliationReportService = enrollmentReconciliationReportService;
		this.enrollmentReconciliationInfo = enrollmentReconciliationInfo;
	}

	@Override
	public Boolean call() throws GIException {
		boolean status = false;
		try{
				status = enrollmentReconciliationReportService.processEnrollmentReconInboundTrackingFiles(fileName, timeStamp, enrollmentReconciliationInfo);
				LOGGER.info("Time taken by thread with ID = "+ Thread.currentThread().getId() +" For File = "+fileName+" consumed "+ ManagementFactory.getThreadMXBean().getThreadCpuTime(Thread.currentThread().getId())+" nano second of cpu time.");
			
		}catch(Exception e){
			LOGGER.error("Error while processing file "+fileName +" :: "+e.getMessage(), e);
		}
		return status;
	}

}