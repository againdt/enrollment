package com.getinsured.hix.enrollment.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.EnrollmentRenewals;

public interface IEnrollmentRenewalRepository extends JpaRepository<EnrollmentRenewals, Integer>
{
	/**
	 * 
	 * @param enrollmentId
	 * @return
	 */
	@Query("FROM EnrollmentRenewals as e WHERE e.enrollmentId = :enrollmentId ORDER BY e.benefitStartDate")
	List<EnrollmentRenewals> findEnrollmentRenwalsByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
	
	/**
	 * 
	 * @param enrollmentId
	 * @return
	 */
	@Query("SELECT max(e.benefitStartDate) from EnrollmentRenewals e where e.enrollmentId = :enrollmentId")
	Date getLatestRenewalEffectiveDate(@Param("enrollmentId") Integer enrollmentId);
	
	@Query("SELECT e from EnrollmentRenewals e where e.enrollmentId = :enrollmentId order by benefitStartDate DESC")
	Page<EnrollmentRenewals> findByEnrollmentIdWithLatestBenefitStartDate(@Param("enrollmentId") Integer enrollmentId,Pageable pageable);
	
	@Query(nativeQuery = true, value = "select count(*) from enrollment_renewals where enrollment_id = :enrollmentId and (benefit_start_date between :effectiveDateFrom and :effectiveDateTo)")
	long checkRenewalExists(@Param("enrollmentId") Integer enrollmentId,@Param("effectiveDateFrom") Date effectiveDateFrom,@Param("effectiveDateTo") Date effectiveDateTo);
	
	@Query(nativeQuery = true, value = "SELECT DISTINCT ENROLLMENT_ID FROM ENROLLMENT_RENEWALS WHERE POLICY_ID = :policyId AND PRODUCT_TYPE = :productType AND ENROLLMENT_STATUS IN ('PENDING','VERIFIED','DECLINED','WITHDRAWN','TERM','INFORCE')")
	List<Integer> getEnrollmentIdByRenewalPolicyId(@Param("policyId") String policyId,@Param("productType") String productType);
	
}
