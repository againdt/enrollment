package com.getinsured.hix.enrollment.service;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.enrollment.EnrollmentOffExchangeEmailDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.email.EnrollmentUniversalConfirmNotification;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.notification.ConfigurationService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.repository.AffiliateFlowRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixUtils.EMAIL_STATS;
import com.google.gson.Gson;

@Service("enrollmentOffExchangeConfirmEmailNotificationService")
public class EnrollmentOffExchangeConfirmEmailNotificationServiceImpl implements
		EnrollmentOffExchangeConfirmEmailNotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentOffExchangeConfirmEmailNotificationServiceImpl.class);
	@Autowired private EnrollmentUniversalConfirmNotification universalEmailNotification;
	@Autowired private RestTemplate restTemplate;
	@Autowired private AffiliateFlowRepository iAffiliateFlowRepository;
	@Autowired private ConfigurationService configurationService;
	@Autowired private Gson platformGson;
	
	private static final int ASCII_SPACE = 32;
	private static final int ASCII_NINE = 57;
	private static final String EMPTY_STRING = "";
	
	@Override
	public void sendEmailNotification(Household household, Enrollment enrollment, boolean isIndividualMember) throws NotificationTypeNotFound {
		if (household != null && StringUtils.isNotBlank(household.getEmail())) {
			LOGGER.info("Email Process :: Start");
			Notice noticeObj =null;
			PlanResponse planResponse = getPlanInfo(enrollment);
			
			EnumMap<EMAIL_STATS, String> notificationTrackingAttributes = new EnumMap<EMAIL_STATS, String>(EMAIL_STATS.class);
			EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO = setCommonData(household,  enrollment, planResponse, isIndividualMember, notificationTrackingAttributes);
			Map<String, Object> notificationContext = new HashMap<String, Object>();
			notificationContext.put("enrollmentOffExchangeEmailDTO", enrollmentOffExchangeEmailDTO);
			universalEmailNotification.setEnrollmentOffExchangeEmailDTO(enrollmentOffExchangeEmailDTO);
			noticeObj=universalEmailNotification.generateEmail(universalEmailNotification.getClass().getSimpleName(), null, universalEmailNotification.getEmailData(notificationContext), universalEmailNotification.getTokens(notificationContext));
			if(noticeObj == null){
				throw new NotificationTypeNotFound("Failed to find the notification for location:"+universalEmailNotification.getClass().getSimpleName());
			}
			universalEmailNotification.sendEmail(noticeObj, notificationTrackingAttributes);
			LOGGER.info("Email Process :: End");
		}else{
			LOGGER.info("Failed to send notification :: Email address is blank");
		}
	}
	
	private EnrollmentOffExchangeEmailDTO setCommonData(
			Household household, Enrollment enrollment,
			PlanResponse planResponse, boolean isIndividualMember, EnumMap<EMAIL_STATS,String> notificationTrackingAttributes) {
		
		String phoneNum = null;
		String logoUrl = null;
		String redirectUrl = null;
		String emailFrom = null;
		Long tenantId = null;
		Integer flowId = null;
		Long affiliateId = null;
		String disclaimerContent = null;
		String callCenterHours = null;
		
		tenantId = enrollment.getTenantId();
		EligLead eligLead = household.getEligLead();
		if(null != eligLead){
			flowId = eligLead.getFlowId();
			affiliateId = eligLead.getAffiliateId();
		}else{
			LOGGER.error("ENROLLMENT-SendEmailNotification setCommonData: eligLead is null for HOUSEHOLD_ID = " + household.getId());
		}
		
		if(null != flowId && 0 == flowId){
			flowId = eligLead.getFlowId() > 0 ? eligLead.getFlowId() : null;
			LOGGER.error("ENROLLMENT-SendEmailNotification setCommonData: Flow ID is null for CMR_HOUSEHOLD_ID = " + household.getId());
		}
		if(null != affiliateId && 0 == affiliateId){
			affiliateId = eligLead.getAffiliateId() > 0 ? eligLead.getAffiliateId() : null;
			LOGGER.error("ENROLLMENT-SendEmailNotification setCommonData: Affiliate ID is null for CMR_HOUSEHOLD_ID = " + household.getId());
		}
		
		notificationTrackingAttributes.put(EMAIL_STATS.FLOW_ID, String.valueOf(flowId));
		notificationTrackingAttributes.put(EMAIL_STATS.AFFILIATE_ID, String.valueOf(affiliateId));
		notificationTrackingAttributes.put(EMAIL_STATS.TENANT_ID, String.valueOf(tenantId));
		
		EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO = new EnrollmentOffExchangeEmailDTO();
		
		String insuranceType = enrollment.getInsuranceTypeLkp().getLookupValueLabel();
		enrollmentOffExchangeEmailDTO.setInsuranceType(insuranceType);
		String recipientName=household.getFirstName()+" "+household.getLastName();
		enrollmentOffExchangeEmailDTO.setRecipient(household.getEmail());
		enrollmentOffExchangeEmailDTO.setRecipientName(recipientName);
		enrollmentOffExchangeEmailDTO.setInsuranceCompany(enrollment.getInsurerName());
		enrollmentOffExchangeEmailDTO.setNetPremiumAmount("$"+enrollment.getNetPremiumAmt());
		
		phoneNum = configurationService.customerCareNum(flowId, affiliateId, tenantId);
		logoUrl = configurationService.logoUrl(flowId, affiliateId, tenantId);
		redirectUrl = configurationService.baseUrl(flowId, affiliateId, tenantId);
		emailFrom = configurationService.fromEmailAddress(affiliateId, tenantId);
		disclaimerContent = configurationService.getDisclaimerTextForEmailFooter(flowId, affiliateId, tenantId);
		callCenterHours = configurationService.callCenterHours(affiliateId, tenantId);
		
		if(null != phoneNum && phoneNum.length() == 10){
			String phoneFormat = "(%s) %s-%s";
			phoneNum = String.format(phoneFormat, phoneNum.substring(0, 3), phoneNum.substring(3, 6), phoneNum.substring(6, 10));
		}
		
		enrollmentOffExchangeEmailDTO.setPhoneNum(phoneNum);
		enrollmentOffExchangeEmailDTO.setLogoUrl(logoUrl);
		enrollmentOffExchangeEmailDTO.setRedirectUrl(redirectUrl);
		enrollmentOffExchangeEmailDTO.setEmailFrom(emailFrom);
		enrollmentOffExchangeEmailDTO.setDisclaimerContent(disclaimerContent);
		enrollmentOffExchangeEmailDTO.setCallCenterHours(callCenterHours);
		
		/*
		 * Set Plan Related Data here
		 */
		populateEmailPlanInfo(enrollmentOffExchangeEmailDTO, planResponse, isIndividualMember);
		
		enrollmentOffExchangeEmailDTO.setCompanyLogo((null != planResponse.getIssuerLogo()) ? planResponse.getIssuerLogo() : EMPTY_STRING);
		return enrollmentOffExchangeEmailDTO;
	}

	/**
	 * Fetch numeric portion of string
	 * @param input
	 * @return
	 */
	private String getNumericPart(String input){
		if(input==null || input.trim().length()<1){
			return null;
		}
		StringBuilder sb = new StringBuilder();
		char[] inpArray = input.toCharArray();
		for(char c : inpArray){
			if(c==ASCII_SPACE) {
				break;
			}
			if(c>ASCII_NINE){
				break;
			}
			if (Character.isDigit(c)) {
				sb.append(c);
			}
		}
		/*try{
			Float.parseFloat(sb.toString());
		}catch(NumberFormatException ex){
			LOGGER.error(ex.getMessage() + sb.toString());
			return null;
		}*/
		if(sb.length() > 0) {
			return sb.toString();
		} else {
			return "0";
		}
	}
	
	/**
	 * Fetch plan duration information from PlanResponse
	 * @param planResponse
	 * @return planDuration
	 */
	private String getPlanDuration(PlanResponse planResponse) {
		String policyLength = "";
		String policyLengthUnit = "";
		if (planResponse != null) {
			if (null != planResponse.getPolicyLength()) {
				policyLength = planResponse.getPolicyLength();
			}
			if (null != planResponse.getPolicyLengthUnit()) {
				policyLengthUnit = planResponse.getPolicyLengthUnit();
			}
		}
		if (policyLength.isEmpty() && policyLengthUnit.isEmpty()) {
			return "";
		}
		return policyLength + " " + policyLengthUnit + " - ";
	}
	
	/**
	 * Fetch plan related information from Plan Management API
	 * @param enrollment
	 * @return planDuration
	 */
	private PlanResponse getPlanInfo(Enrollment enrollment){
		
		PlanRequest planRequest = new PlanRequest();
		String planId =  ""+enrollment.getPlanId();
		if(!EnrollmentPrivateUtils.isNotNullAndEmpty(planId) && null != enrollment.getEnrollmentPlan()){
			planId = ""+enrollment.getEnrollmentPlan().getPlanId();
		}
		LOGGER.info("Fetching Plan Data from Plan Management API :: " + planId);
		
		planRequest.setPlanId(planId);
		planRequest.setMarketType(GhixConstants.INDIVIDUAL.toUpperCase());
        
		String planResponseStr = "";
		PlanResponse planResponse = null;
		try {
			planResponseStr = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL, planRequest,String.class);
		} catch (Exception e) {
			LOGGER.error("Error in fetching plan details", e);
		}
		if(!"".equalsIgnoreCase(planResponseStr)){
			planResponse = (PlanResponse) platformGson.fromJson(planResponseStr, PlanResponse.class);	
		}
		return planResponse; 
	}
	
	private void populateEmailPlanInfo(EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO, PlanResponse planResponse, boolean isIndvMember) {
		
		LOGGER.info("Populating email data with Plan Info");
		String officeVisit = null;
		
		
		if (planResponse != null) {
			enrollmentOffExchangeEmailDTO.setPlanName((null != planResponse.getPlanName()) ? planResponse.getPlanName() : EMPTY_STRING);
			enrollmentOffExchangeEmailDTO.setDeductible("$0");
			enrollmentOffExchangeEmailDTO.setMaxOop("$0");
			enrollmentOffExchangeEmailDTO.setPlanType(planResponse.getNetworkType());
			if(null != planResponse.getDeductible()){
				enrollmentOffExchangeEmailDTO.setDeductible("$"+planResponse.getDeductible());
			}
			if(null != planResponse.getOopMax()){
				enrollmentOffExchangeEmailDTO.setMaxOop("$"+ planResponse.getOopMax());
			}
			officeVisit = getNumericPart(planResponse.getOfficeVisit());
			enrollmentOffExchangeEmailDTO.setOfficeVisit("$"+(( officeVisit != null) ? officeVisit : "0"));
			if(null == enrollmentOffExchangeEmailDTO.getInsuranceCompany()){
				enrollmentOffExchangeEmailDTO.setInsuranceCompany(planResponse.getIssuerName());
			}
		}
		
			/**
			 * To Do : Individual Member Flag
			 */
//			if (isIndvMember && planResponse.getDeductible() != null) {
//				emailData.put(DEDUCTIBLE, planResponse.getDeductible());
//			} else if (!isIndvMember
//					&& planResponse.getDeductibleFamily() != null) {
//				emailData.put(DEDUCTIBLE, planResponse.getDeductibleFamily());
//			}
//
//			if (planResponse.getOopMax() != null && isIndvMember) {
//				emailData.put(MAX_OOP, planResponse.getOopMax());
//			} else if (planResponse.getOopMaxFamily() != null && !isIndvMember) {
//				emailData.put(MAX_OOP, planResponse.getOopMaxFamily());
//			} else {
//				emailData.put(MAX_OOP, HTML_SPACE);
//			}

	}
	
}
