/**
 *
 */
package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepository;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

/**
 * Validation deck queries for CMS validations
 * @author negi_s
 *
 */
public interface IEnrollmentCmsValidationRepository extends TenantAwareRepository<Enrollment, Integer>, EnversRevisionRepository<Enrollment, Integer, Integer>{

	
	@Query("SELECT DISTINCT(en.id) FROM Enrollment en WHERE "
            + " (SELECT COUNT(epr.id) FROM EnrollmentPremium epr WHERE epr.enrollment.id = en.id AND epr.grossPremiumAmount IS NOT NULL) "
            + " <> (TO_NUMBER(TO_CHAR(en.benefitEndDate,'MM'),'999999999.99') - TO_NUMBER(TO_CHAR(en.benefitEffectiveDate,'MM'), '999999999.99')) +1 "
            + " AND en.id IN (:enrollmentIdList) AND en.enrollmentStatusLkp.lookupValueCode <> 'CANCEL'")
	List<Integer> getRecordsWithCoveragePremiumMismatch(@Param("enrollmentIdList") List<Integer> enrollmentIdList);
	
	@Query("SELECT enr.id FROM Enrollment enr WHERE to_char(enr.benefitEffectiveDate, 'YYYY') <> to_char(enr.benefitEndDate, 'YYYY') AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForCoverageYearMismatch(@Param("enrollmentIdList") List<Integer> enrollmentIdList);
	
	@Query("SELECT enr.id FROM Enrollment enr WHERE enr.benefitEffectiveDate > enr.benefitEndDate"
			+ " AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForCoverageStartGreaterThanEndDate(@Param("enrollmentIdList") List<Integer> enrollmentIdList);

	@Query("SELECT DISTINCT(enr.id) FROM Enrollment enr, Enrollee ee WHERE ee.enrollment.id = enr.id AND ee.effectiveStartDate IS NOT NULL "
			+ " AND ee.effectiveEndDate IS NOT NULL AND to_char(ee.effectiveStartDate, 'YYYY') <> to_char(ee.effectiveEndDate, 'YYYY') "
			+ " AND ee.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForMemberCoverageYearMismatch(@Param("enrollmentIdList") List<Integer> enrollmentIdList);
	
	@Query("SELECT DISTINCT(enr.id) FROM Enrollment enr, Enrollee ee WHERE ee.enrollment.id = enr.id AND ee.effectiveStartDate > ee.effectiveEndDate"
			+ " AND ee.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForMemberCoverageGreaterThanEndDate(@Param("enrollmentIdList") List<Integer> enrollmentIdList);
	
	@Query("SELECT DISTINCT(enr.id) FROM Enrollment enr, Enrollee ee WHERE ee.enrollment.id = enr.id AND ee.effectiveStartDate < ee.birthDate"
			+ " AND ee.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForMemberCoverageBeforeBirthDate(@Param("enrollmentIdList") List<Integer> enrollmentIdList);
	
	@Query("SELECT DISTINCT(enr.id) FROM Enrollment enr, EnrollmentPremium epr WHERE epr.enrollment.id = enr.id"
			+ " AND epr.aptcAmount IS NOT NULL AND epr.aptcAmount < 0 AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForNegativeAptc(@Param("enrollmentIdList") List<Integer> enrollmentIdList);

	@Query("SELECT DISTINCT(enr.id) FROM Enrollment enr, EnrollmentPremium epr WHERE epr.enrollment.id = enr.id"
			+ " AND epr.netPremiumAmount IS NOT NULL AND epr.netPremiumAmount < 0 AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForNegativePremium(@Param("enrollmentIdList") List<Integer> enrollmentIdList);
	
	@Query("SELECT DISTINCT(enr.id) FROM Enrollment enr WHERE NOT EXISTS (SELECT 1 FROM Enrollee ee WHERE ee.enrollment.id = enr.id "
			+ " AND ee.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') AND ee.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL', 'ABORTED')) AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForNoActiveMembers(@Param("enrollmentIdList") List<Integer> enrollmentIdList);

	@Query("SELECT enr.id FROM Enrollment enr WHERE enr.enrollmentConfirmationDate IS NULL AND enr.id IN(:enrollmentIdList)")
	List<Integer> getRecordsForConfirmationDateMissing(@Param("enrollmentIdList") List<Integer> enrollmentIdList);
	
}