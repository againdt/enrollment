package com.getinsured.hix.enrollment.service;

public interface EnrollmentEffectuationService {
	
	void effectuateEnrollment();

}
