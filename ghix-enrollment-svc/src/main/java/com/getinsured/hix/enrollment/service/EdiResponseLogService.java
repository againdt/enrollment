package com.getinsured.hix.enrollment.service;

import java.util.Map;

import com.getinsured.hix.model.enrollment.EdiResponseLog;

public interface EdiResponseLogService {
	
	EdiResponseLog addEdiResponseLog(EdiResponseLog ediResponseLog);
	
	void addAllEdiResponseLog(String jobName, long stepexecutionid, long jobId,String jobStepName, long jobInstanceId, Map<String, Integer>resultMap );
	
	
}
