package com.getinsured.hix.enrollment.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.getinsured.hix.model.enrollment.EnrollmentEvent;
public interface IEnrollmentEventPrivateRepository extends JpaRepository<EnrollmentEvent, Integer> {

	@Query("FROM EnrollmentEvent as ev WHERE ev.id =  :eventID and ev.enrollment.planId = :planID")
	EnrollmentEvent findEnrollmentByeventIDAndplanID(@Param("eventID") Integer eventID, @Param("planID") Integer planID);
	
	@Query("FROM EnrollmentEvent as ev WHERE ev.enrollment.id =  :enrollmentID and ev.enrollee.id = :enrolleeID and ev.createdOn >= :createdDate ORDER BY ev.createdOn ASC")
	List<EnrollmentEvent> findEventByenrollmentIDAndEnrolleeIDAndCreatedDate(@Param("enrollmentID") Integer enrollmentID, @Param("enrolleeID") Integer enrolleeID,@Param("createdDate") Date createdDate );

}
