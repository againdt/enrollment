package com.getinsured.hix.enrollment.repository;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.Enrollment1095;

/**
 * @author negi_s
 *
 */
public interface IEnrollment1095Repository extends JpaRepository<Enrollment1095, Integer> {
	
	Enrollment1095 findById(Integer enrollment1095Id);
	
	
	@Query("FROM Enrollment1095 as en WHERE en.houseHoldCaseId = :householdCaseID AND en.coverageYear = :coverageYear AND en.isActive='Y' AND en.isObsolete = 'N'"
			+ " AND (en.resendPdfFlag IS NULL OR en.resendPdfFlag = 'N') AND pdfGeneratedOn IS NOT NULL")
	List<Enrollment1095> getEnrollment1095ForResend(@Param("householdCaseID") String householdCaseID, @Param("coverageYear") Integer coverageYear);
	
	@Query("SELECT enMem.address1 || ', ' ||"
			+ " CASE WHEN enMem.address2 IS NOT NULL THEN (enMem.address2 || ', ') END ||"
			+ " enMem.city || ' ' || enMem.state || ' '|| enMem.zip "
			+ " FROM Enrollment1095 as en, EnrollmentMember1095 enMem"
			+ " WHERE en.id = enMem.enrollment1095.id AND en.houseHoldCaseId = :householdCaseID AND enMem.memberType = 'RECEPIENT' AND en.isObsolete = 'N'"
			+ " AND en.isActive = 'Y' AND en.updatedOn = (SELECT MAX(enIn.updatedOn) FROM Enrollment1095 as enIn WHERE enIn.id = en.id)")
	String fetch1095HouseholdAddress(@Param("householdCaseID") String householdCaseID);

	@Query("SELECT en.coverageYear, COUNT(*), en.houseHoldCaseId,"
			+ " COUNT(CASE WHEN (en.isActive = 'N' OR en.pdfSkippedFlag = 'Y' OR (en.isActive = 'Y' AND en.pdfGeneratedOn IS NULL)) THEN '1' END) AS FAILED,"
			+ " COUNT(CASE WHEN en.isActive = 'Y' AND en.pdfGeneratedOn IS NOT NULL AND (en.resendPdfFlag IS NULL OR en.resendPdfFlag = 'N') THEN '1' END) AS GENERATED,"
			+ " COUNT(CASE WHEN en.isActive = 'Y' AND en.pdfGeneratedOn IS NOT NULL AND en.resendPdfFlag IS NOT NULL AND en.resendPdfFlag = 'Y' THEN 1 END) AS SCHEDULED"
			+ " FROM Enrollment1095 as en WHERE en.houseHoldCaseId = :householdCaseID AND en.isObsolete = 'N'"
			+ " AND en.coverageYear      <= :coverageYear"
			+ " GROUP BY en.houseHoldCaseId, en.coverageYear ORDER BY en.coverageYear DESC")
	List<Object[]> fetchEnrollment1095CoverageYearData(@Param("householdCaseID") String householdCaseID, @Param("coverageYear") Integer coverageYear);
	
	@Query("FROM Enrollment1095 as en WHERE en.exchgAsignedPolicyId = :enrollmentId")
	Enrollment1095 findByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
	
	@Query("SELECT COUNT(en.id) FROM Enrollment1095 as en WHERE en.coverageYear = :coverageYear")
	Long getEnrollmentCountForYear(@Param("coverageYear") Integer applicableYear);
	
	@Query(" SELECT DISTINCT en.houseHoldCaseId FROM Enrollment1095 as en WHERE( correctionIndicatorPdf='Y' OR resendPdfFlag='Y'  " +
            " OR (pdfGeneratedOn IS NULL " +
            " AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y' ) ) ) " +
            " AND en.isActive='Y' AND en.isObsolete = 'N' " +
            " AND (pdfSkippedFlag IS NULL OR pdfSkippedFlag = 'N') " +
            " AND en.coverageYear < :coverageYear"+
            " ORDER BY  en.houseHoldCaseId "
            )
	List<String> getUniqueHouseholdIdsFor1095Pdf(@Param("coverageYear") Integer applicableYear);
	
	@Query(" SELECT en.id FROM Enrollment1095 as en WHERE( correctionIndicatorPdf='Y' OR resendPdfFlag='Y'  " +
            " OR (pdfGeneratedOn IS NULL " +
            " AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y' ) ) ) " +
            " AND en.isActive='Y' AND en.isObsolete = 'N' " +
            " AND (pdfSkippedFlag IS NULL OR pdfSkippedFlag = 'N') " +
            " AND en.coverageYear < :coverageYear"+
            " AND en.houseHoldCaseId IN :householdIds"+
            " ORDER BY  en.houseHoldCaseId ")
	List<Integer> getUniqueIdsFor1095Pdf(@Param("coverageYear") Integer applicableYear, @Param("householdIds") List<String> householdIds);
	
	@Query(" SELECT en.id FROM Enrollment1095 as en WHERE( correctionIndicatorPdf='Y' OR resendPdfFlag='Y'  " +
            " OR (pdfGeneratedOn IS NULL " +
            " AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y' ) ) ) " +
            " AND en.isActive='Y' AND en.isObsolete = 'N' " +
            " AND (pdfSkippedFlag IS NULL OR pdfSkippedFlag = 'N') " +
            " AND en.coverageYear < :coverageYear"+
            " ORDER BY  en.houseHoldCaseId ")
	List<Integer> getUniqueIdsFor1095Pdf(@Param("coverageYear") Integer applicableYear);

	@Query(" SELECT DISTINCT en.id FROM Enrollment1095 as en WHERE en.coverageYear = :coverageYear AND en.cmsXmlFileName IS NULL AND en.cmsXmlGeneratedOn IS NULL AND en.originalBatchId IS NULL AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y') AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Integer> getFreshEnrollment1095Id(@Param("coverageYear") Integer applicableYear);

	@Query(" SELECT DISTINCT en.coverageYear FROM Enrollment1095 as en "
			+ " WHERE en.cmsXmlFileName IS NULL "
			+ " AND en.cmsXmlGeneratedOn IS NULL "
			+ " AND en.originalBatchId IS NULL "
			+ " AND en.isActive='Y' "
			+ " AND en.coverageYear < :coverageYear"
			+ " AND en.isObsolete='N'")
	List<Integer> getCoverageYearListForFreshRun(@Param("coverageYear") Integer applicableYear);

	@Query(" SELECT DISTINCT en.id FROM Enrollment1095 as en WHERE en.originalBatchId = :originalBatchId AND en.batchCategoryCode = :batchCategoryCode AND en.correctionIndicatorXml = 'Y' AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y') "
			+ " AND (en.inboundAction IS NULL OR en.inboundAction NOT IN ('REGEN_XML','REGEN_ZIP')) AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Integer> getCorrectionEnrollment1095ByBatchIdAndCategoryCode(@Param("originalBatchId") String batchId, @Param("batchCategoryCode")	String batchCategoryCode);
	
	@Query(" SELECT DISTINCT en.originalBatchId FROM Enrollment1095 as en WHERE (en.correctionIndicatorXml = 'Y' OR en.inboundAction IN ('REGEN_XML','REGEN_ZIP'))  and en.originalBatchId IS NOT NULL AND  en.isActive='Y' AND en.isObsolete='N' ")
	List<String> getOriginalBatchIdListFromStagingCorrections();
	
	@Query(" SELECT MAX(en.coverageYear) FROM Enrollment1095 as en WHERE en.originalBatchId = :originalBatchId AND  en.isActive='Y' AND en.isObsolete='N' ")
	Integer getCoverageYearOfOriginalBatchId(@Param("originalBatchId") String batchId);
																						
	@Query(" SELECT DISTINCT en.id FROM Enrollment1095 as en WHERE en.originalBatchId = :originalBatchId AND en.voidCheckboxindicator ='Y' AND  en.correctionIndicatorXml = 'Y' AND (en.resubCorrectionInd IS NULL OR en.resubCorrectionInd <> 'Y') AND (en.xmlSkippedFlag IS NULL OR en.xmlSkippedFlag <> 'Y') AND (en.inboundAction IS NULL OR en.inboundAction NOT IN ('REGEN_XML','REGEN_ZIP')) AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Integer> getInitialVoidEnrollment1095ByBatchId(@Param("originalBatchId") String batchId);

	@Query(" SELECT DISTINCT en.id FROM Enrollment1095 as en WHERE en.originalBatchId = :originalBatchId AND en.batchCategoryCode IN (:batchCategoryCodeList) AND en.voidCheckboxindicator ='Y' AND en.correctionIndicatorXml = 'Y' AND en.resubCorrectionInd = 'Y' AND (en.xmlSkippedFlag IS NULL OR en.xmlSkippedFlag <> 'Y') AND (en.inboundAction IS NULL OR en.inboundAction NOT IN ('REGEN_XML','REGEN_ZIP')) AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Integer> getVoidEnrollment1095ByBatchIdAndCategoryCode(@Param("originalBatchId") String batchId, @Param("batchCategoryCodeList") List<String> batchCategoryCodeList);

	@Query(" FROM Enrollment1095 as en WHERE en.exchgAsignedPolicyId IN (SELECT e.id FROM Enrollment as e WHERE e.enrollmentStatusLkp.lookupValueCode = 'CANCEL')  AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y') AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Enrollment1095> fetchCancelledEnrollment1095();
	
	@Query(" SELECT DISTINCT en.id FROM Enrollment1095 as en WHERE en.originalBatchId = :originalBatchId "
			+ " AND en.batchCategoryCode IN (:batchCategoryCodeList) AND en.correctionIndicatorXml = 'Y' "
			+ " AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y') "
			+ " AND (en.resubCorrectionInd IS NULL OR en.resubCorrectionInd <> 'Y') AND (en.xmlSkippedFlag IS NULL OR en.xmlSkippedFlag <> 'Y') "
			+ " AND (en.inboundAction IS NULL OR en.inboundAction NOT IN ('REGEN_XML','REGEN_ZIP')) AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Integer> getSubmitCorrectionListFromStaging(@Param("originalBatchId") String batchId, @Param("batchCategoryCodeList") List<String> batchCategoryCodeList);

	@Query(" SELECT DISTINCT en.id FROM Enrollment1095 as en WHERE en.originalBatchId = :originalBatchId "
			+ " AND en.batchCategoryCode IN (:batchCategoryCodeList) AND en.correctionIndicatorXml = 'Y' "
			+ " AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y') "
			+ " AND en.resubCorrectionInd = 'Y' AND (en.xmlSkippedFlag IS NULL OR en.xmlSkippedFlag <> 'Y') "
			+ " AND (en.inboundAction IS NULL OR en.inboundAction NOT IN ('REGEN_XML','REGEN_ZIP')) AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Integer> getReSubmitCorrectionListFromStaging(@Param("originalBatchId") String batchId, @Param("batchCategoryCodeList") List<String> batchCategoryCodeList);

	@Query(" SELECT DISTINCT en.id FROM Enrollment1095 as en WHERE en.originalBatchId = :originalBatchId "
			+ " AND en.batchCategoryCode = :batchCategoryCode "
			+ " AND (en.xmlSkippedFlag IS NULL OR en.xmlSkippedFlag <> 'Y') "
			+ " AND en.inboundAction IN ('REGEN_XML','REGEN_ZIP') AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Integer> getRegenerateEnrollment1095ByBatchIdAndCategoryCode(@Param("originalBatchId") String batchId, @Param("batchCategoryCode")	String batchCategoryCode);
	
	@Query(" FROM Enrollment1095 as en " +
			   " WHERE en.exchgAsignedPolicyId IN(:exchgAsignedPolicyIdList) " +
			   " AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Enrollment1095> getEnrollment1095ByExchgAsignedPolicyId(@Param("exchgAsignedPolicyIdList")List<Integer> enrollmentArray);
	
	
	@Query(" SELECT DISTINCT en.houseHoldCaseId FROM Enrollment1095 as en WHERE TO_DATE(TO_CHAR(en.policyStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.policyStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY')  AND en.isActive='Y' AND en.isObsolete='N' AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y') ")
	List<String> getHouseholdIdsFromStaging(@Param("startDate") String startDateString, @Param("endDate") String endDateString);
	
	@Query("FROM Enrollment1095 as en WHERE en.houseHoldCaseId = cast (:houseHoldCaseId as integer) AND TO_DATE(TO_CHAR(en.policyStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.policyStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY')  AND en.isActive='Y' AND en.isObsolete='N' AND (en.voidCheckboxindicator IS NULL OR en.voidCheckboxindicator <> 'Y') ")
	List<Enrollment1095> getByHouseholdCaseId(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("startDate") String startDateString, @Param("endDate") String endDateString);
	
	@Query("  SELECT DISTINCT en.originalBatchId FROM Enrollment1095 as en WHERE en.exchgAsignedPolicyId = :enrollmentId")
	String findOriginalBatchId(@Param("enrollmentId") Integer enrollmentId);
	
	
	//Enrollment1095 Validation Repository starts
	
	
	@Query("SELECT DISTINCT(ep.enrollment1095.id)"
			+ " FROM EnrollmentPremium1095 ep, Enrollment1095 en"
			+ " WHERE ep.enrollment1095.id = en.id"
			+ " AND ep.grossPremium != 0  AND ( (ep.slcspAmount = 0) OR (ep.slcspAmount < 0) OR (ep.slcspAmount IS NULL))"
			+ " AND ((en.voidCheckboxindicator IS NULL) OR (en.voidCheckboxindicator ='N'))"
			+ " AND ep.isActive = 'Y' AND en.isActive = 'Y' AND en.isObsolete = 'N' AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordForInvalidEhbSlcspAmt(@Param("coverageYearList")List<Integer> coverageYearList);

	
	@Query("SELECT DISTINCT(em.enrollment1095.id)"
			+ " FROM EnrollmentMember1095 em, Enrollment1095 en"
			+ " WHERE em.enrollment1095.id = en.id"
			+ " AND (em.address1 IS NULL OR em.city IS NULL OR em.zip IS NULL)"
			+ " AND em.memberType  = 'RECEPIENT' AND em.isActive = 'Y' AND en.isActive = 'Y' AND en.isObsolete = 'N'"
			+ " AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordForInvalidAddressFields(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT enr.id FROM Enrollment1095 enr"
			+ " WHERE (enr.policyStartDate IS NULL OR enr.policyEndDate IS NULL)"
			+ " AND enr.isActive = 'Y' AND enr.isObsolete = 'N' AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordForInvalidPolicyDates(@Param("coverageYearList")List<Integer> coverageYearList);
	
	/*@Query("SELECT ENROLLMENT1095_ID FROM (SELECT en.id 'ENROLLMENT1095_ID', COUNT(em.id) member_count"
			+ " FROM Enrollment1095 en, EnrollmentMember1095 em"
			+ " WHERE en.id = em.enrollment1095.id AND em.memberType = 'MEMBER'"
			+ " AND en.isActive   = 'Y' AND en.coverageYear IN(:coverageYearList)"
			+ " AND em.isActive   = 'Y' GROUP BY en.id HAVING COUNT(DISTINCT(em.id)) = 0)")
	List<Integer> getRecordForNoActiveMembers(@Param("coverageYearList")List<Integer> coverageYearList);*/
	
	@Query(nativeQuery = true, value ="SELECT ENROLLMENT1095_ID FROM (SELECT en.ID ENROLLMENT1095_ID, COUNT(em.ID) member_count"
			+ " FROM enrollment_1095 en, enrollment_member_1095 em"
			+ " WHERE en.ID = em.enrollment_1095_id AND em.member_type = 'MEMBER' AND en.is_active   = 'Y' AND en.is_obsolete = 'N'"
			+ " AND en.coverage_year IN(:coverageYearList) AND em.is_active = 'N'"
			+ " GROUP BY en.ID HAVING COUNT(DISTINCT(em.ID)) = (SELECT count(id) from enrollment_member_1095 where enrollment_1095_id =en.id"
			+ "	and member_type='MEMBER')) as enrollment1095Id")
	List<BigDecimal> getRecordForNoActiveMembers(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT DISTINCT(em.enrollment1095.id) FROM EnrollmentMember1095 em, Enrollment1095 en"
			+ " WHERE em.enrollment1095.id = en.id AND (LENGTH(em.ssn) < 9  OR LENGTH(em.ssn) > 9)"
			+ " AND em.isActive = 'Y' AND en.isActive = 'Y' AND en.isObsolete = 'N' AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordForSsnLengthInvalid(@Param("coverageYearList")List<Integer> coverageYearList);
	
	
	@Query("SELECT DISTINCT(em.enrollment1095.id) FROM EnrollmentMember1095 em, Enrollment1095 en"
			+ " WHERE em.enrollment1095.id = en.id "
			+ " AND Length(em.ssn) != 9"
			+ " AND em.isActive = 'Y' AND en.isActive = 'Y' AND en.isObsolete = 'N' AND en.coverageYear IN(:coverageYearList) AND em.ssn is not null")
	List<Integer> getRecordsForSsnValueInvalid(@Param("coverageYearList")List<Integer> coverageYearList);
	
	
	@Query("SELECT DISTINCT(em.enrollment1095.id) FROM EnrollmentMember1095 em, Enrollment1095 en"
			+ " WHERE em.enrollment1095.id = en.id AND (em.coverageStartDate IS NULL"
			+ " OR em.coverageEndDate IS NULL) AND em.memberType = 'MEMBER'"
			+ " AND em.isActive = 'Y' AND en.isActive = 'Y' AND en.isObsolete = 'N' AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForInvalidMemberCoverageDate(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT DISTINCT(em.enrollment1095.id) FROM EnrollmentMember1095 em, Enrollment1095 en"
			+ " WHERE em.enrollment1095.id = en.id AND em.coverageStartDate  < em.birthDate"
			+ " AND em.memberType = 'MEMBER' AND em.isActive = 'Y' AND en.isActive = 'Y' AND en.isObsolete = 'N'"
			+ " AND (en.voidCheckboxindicator is NULL OR en.voidCheckboxindicator != 'Y') AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForCoverageStartBeforeBirthDate(@Param("coverageYearList")List<Integer> coverageYearList);
	
	/*@Query("SELECT DISTINCT(mem.enrollment1095.id) FROM EnrollmentMember1095 mem, Enrollment1095 enr"
			+ " WHERE mem.enrollment1095.id = enr.id AND mem.enrollment1095.id NOT IN"
			+ " (SELECT DISTINCT(ENROLLMENT1095_ID) FROM (SELECT em.enrollment1095.id 'ENROLLMENT1095_ID', em.memberType"
					+ " FROM EnrollmentMember1095 em WHERE em.memberType= 'RECEPIENT'"
					+ " AND em.isActive    = 'Y' GROUP BY em.enrollment1095.id, em.memberType HAVING COUNT(em.id) > 0))"
					+ " AND mem.isActive = 'Y' AND enr.isActive = 'Y' AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForMissingReceipientRecord(@Param("coverageYearList")List<Integer> coverageYearList);*/
	
	@Query(nativeQuery = true, value = "SELECT DISTINCT(mem.ENROLLMENT_1095_ID) FROM enrollment_member_1095 mem, Enrollment_1095 enr"
			+" WHERE mem.ENROLLMENT_1095_ID = enr.id AND mem.ENROLLMENT_1095_ID NOT IN"
			+" (SELECT DISTINCT(ENROLLMENT_1095_ID) FROM (SELECT em.ENROLLMENT_1095_ID ENROLLMENT_1095_ID, em.MEMBER_TYPE "
			+" FROM enrollment_member_1095 em WHERE em.member_type= 'RECEPIENT' AND em.is_active = 'Y' GROUP BY em.ENROLLMENT_1095_ID, em.MEMBER_TYPE"
			+" HAVING COUNT(em.id) > 0 ) as enrollment1095Id ) AND mem.IS_ACTIVE = 'Y' AND enr.IS_ACTIVE = 'Y' AND enr.IS_OBSOLETE = 'N' AND enr.coverage_year IN (:coverageYearList)")
	List<BigDecimal> getRecordsForMissingReceipientRecord(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT enr.id FROM Enrollment1095 enr WHERE enr.policyStartDate > enr.policyEndDate"
			+ " AND enr.isActive = 'Y' AND enr.isObsolete = 'N' AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForCoverageStartGreaterThanEndDate(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT DISTINCT(em.enrollment1095.id) FROM EnrollmentMember1095 em, Enrollment1095 en"
			+ " WHERE em.enrollment1095.id = en.id AND em.coverageStartDate > em.coverageEndDate"
			+ " AND em.memberType = 'MEMBER' AND em.isActive = 'Y' AND en.isActive = 'Y' AND en.isObsolete = 'N' AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForMemberCoverageStartGreaterThanEndDate(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT enr.id FROM Enrollment1095 enr WHERE enr.voidCheckboxindicator = 'Y' AND enr.correctedCheckBoxIndicator = 'Y'"
			+ " AND enr.isActive = 'Y' AND enr.isObsolete = 'N' AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForVoidCorrectedCheckboxSet(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT enr.id FROM Enrollment1095 enr WHERE enr.exchgAsignedPolicyId IS NULL AND enr.isActive = 'Y' AND enr.isObsolete = 'N'  AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForAssignedPolicyNumberNull(@Param("coverageYearList")List<Integer> coverageYearList);
	
	
	@Query("SELECT enr.id FROM Enrollment1095 enr WHERE (enr.policyIssuerName IS NULL OR trim(enr.policyIssuerName) IS NULL)"
			+ " AND enr.isActive = 'Y' AND enr.isObsolete = 'N' AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForPolicyIssuerNameNull(@Param("coverageYearList")List<Integer> coverageYearList);
	
	
	@Query("SELECT enr.id FROM Enrollment1095 enr WHERE TO_DATE(TO_CHAR((enr.policyStartDate), 'MM/DD/YYYY'), 'MM/DD/YYYY') = TO_DATE(TO_CHAR((enr.policyEndDate), 'MM/DD/YYYY'), 'MM/DD/YYYY')"
			+ " AND (enr.voidCheckboxindicator IS NULL OR enr.voidCheckboxindicator <> 'Y') AND enr.isActive = 'Y'  AND enr.isObsolete = 'N' AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForNonVoidCaseSameStartAndEndDate(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT e.id FROM Enrollment1095 e, Enrollment en WHERE e.exchgAsignedPolicyId = en.id"
			+ " AND en.planLevel = 'CATASTROPHIC' AND e.isActive = 'Y' AND e.isObsolete = 'N' AND e.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForCatastrophicEnrollment(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT e.id FROM Enrollment1095 e, Enrollment en WHERE e.exchgAsignedPolicyId = en.id"
			+ " AND en.insuranceTypeLkp.lookupValueCode = 'DEN' AND e.isActive = 'Y' AND e.isObsolete = 'N' AND e.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForDentalEnrollment(@Param("coverageYearList")List<Integer> coverageYearList);
	
	
	@Query("SELECT DISTINCT(epr.enrollment1095.id) FROM EnrollmentPremium1095 epr, Enrollment1095 enr"
			+ " WHERE enr.id = epr.enrollment1095.id AND epr.monthNumber >= extract(month from enr.policyStartDate)"
			+ " AND epr.monthNumber <= extract(month from enr.policyEndDate) AND ((enr.voidCheckboxindicator IS NULL) OR  (enr.voidCheckboxindicator ='N'))"
			+ " AND (epr.grossPremium = 0 or epr.grossPremium is null OR epr.grossPremium < 0) AND epr.isActive = 'Y'"
			+ " AND enr.isActive = 'Y' AND enr.isObsolete = 'N' AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForZeroGrossPremium(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT enr.id FROM Enrollment1095 enr WHERE to_char(enr.policyStartDate, 'YYYY') <> to_char(enr.policyEndDate, 'YYYY') AND enr.isActive = 'Y' AND enr.isObsolete = 'N' AND enr.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForCoverageYearMismatch(@Param("coverageYearList")List<Integer> coverageYearList);
	
	
/*	@Query(nativeQuery = true, value = "SELECT ENROLLMENT1095_ID FROM (SELECT en.id as ENROLLMENT1095_ID, COUNT(ep.id) month_count FROM Enrollment1095 en"
			+ " LEFT JOIN EnrollmentPremium1095 ep ON (en.id = ep.enrollment1095.id)"
			+ " WHERE en.isActive = 'Y' AND ((en.voidCheckboxindicator IS NULL) OR  (en.voidCheckboxindicator ='N'))"
			+ " AND en.coverageYear IN(:coverageYearList) GROUP BY en.id HAVING COUNT(DISTINCT(ep.id)) < 1)")
	List<Integer> getRecordsForNoPremiumMonthPresent(@Param("coverageYearList")List<Integer> coverageYearList);*/
	
	@Query(nativeQuery = true, value = "SELECT ENROLLMENT1095_ID  FROM (SELECT en.ID ENROLLMENT1095_ID, COUNT(ep.ID) month_count"
			+ " FROM enrollment_1095 en LEFT JOIN enrollment_premium_1095 ep ON (en.ID = ep.enrollment_1095_id)"
			+ " WHERE en.is_active = 'Y' AND en.is_obsolete = 'N' AND ((en.VOID_CHECKBOX_INDICATOR IS NULL) OR (en.VOID_CHECKBOX_INDICATOR ='N'))"
			+ " AND en.coverage_year IN(:coverageYearList) GROUP BY en.ID HAVING COUNT(DISTINCT(ep.ID)) < 1) as enrollment1095Id")
		List<BigDecimal> getRecordsForNoPremiumMonthPresent(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT DISTINCT(em.enrollment1095.id) FROM EnrollmentMember1095 em, Enrollment1095 en"
			+ " WHERE em.enrollment1095.id = en.id AND em.birthDate IS NULL"
			+ " AND em.memberType = 'MEMBER' AND em.isActive = 'Y' AND en.isActive = 'Y' AND en.isObsolete = 'N' AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForNullBirthDate(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT DISTINCT(en.id) FROM Enrollment1095 en WHERE en.isActive = 'Y' AND en.isObsolete = 'N'"
            + " AND ((en.voidCheckboxindicator IS NULL) OR  (en.voidCheckboxindicator ='N'))"
            + " AND (TO_NUMBER(TO_CHAR(en.policyEndDate,'MM'),'999999999.99') - TO_NUMBER(TO_CHAR(en.policyStartDate,'MM'), '999999999.99')) +1 <>"
            + " (SELECT COUNT(epr.monthNumber) FROM EnrollmentPremium1095 epr"
            + " WHERE epr.enrollment1095.id= en.id AND epr.isActive = 'Y') AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForCoverageAndPremiumMonthMismatch(@Param("coverageYearList")List<Integer> coverageYearList);
	
	
	@Query("SELECT DISTINCT(epr.enrollment1095.id) FROM EnrollmentPremium1095 epr, Enrollment1095 en"
			+ " WHERE epr.enrollment1095.id = en.id AND epr.aptcAmount < 0  AND en.isActive = 'Y'"
			+ " AND ((en.voidCheckboxindicator IS NULL) OR  (en.voidCheckboxindicator ='N'))"
			+ " AND epr.isActive            = 'Y' AND en.isObsolete = 'N' AND en.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForNegativeAptc(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT DISTINCT(enOne.id) FROM Enrollment1095 enOne, Enrollment1095 entwo"
			+ " WHERE enOne.houseHoldCaseId = entwo.houseHoldCaseId AND enOne.coverageYear = entwo.coverageYear"
			+ " AND enOne.id <> entwo.id AND enOne.voidCheckboxindicator IS NULL AND entwo.voidCheckboxindicator IS NULL"
			+ " AND enOne.isActive = 'Y' AND entwo.isActive = 'Y' AND enOne.isObsolete = 'N' AND entwo.isObsolete = 'N' AND enOne.coverageYear IN(:coverageYearList)"
			+ " AND entwo.coverageYear IN(:coverageYearList) AND ( TO_DATE(TO_CHAR((enOne.policyStartDate), 'MM/DD/YYYY'), 'MM/DD/YYYY')<=TO_DATE(TO_CHAR((entwo.policyEndDate), 'MM/DD/YYYY'), 'MM/DD/YYYY')"
			+ " AND TO_DATE(TO_CHAR((enOne.policyEndDate), 'MM/DD/YYYY'), 'MM/DD/YYYY')    >=TO_DATE(TO_CHAR((entwo.policyStartDate), 'MM/DD/YYYY'), 'MM/DD/YYYY') )")
	List<Integer> getRecordsForEnrollment1095OverlappingCoverage(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT DISTINCT (enOne.id) FROM Enrollment1095 enOne, EnrollmentMember1095 enMemOne, Enrollment1095 entwo, EnrollmentMember1095 enMemTwo"
			+ " WHERE ( TO_DATE(TO_CHAR((enMemOne.coverageStartDate), 'MM/DD/YYYY'), 'MM/DD/YYYY')<=TO_DATE(TO_CHAR((enMemTwo.coverageEndDate), 'MM/DD/YYYY'), 'MM/DD/YYYY') "
			+ " AND TO_DATE(TO_CHAR((enMemOne.coverageEndDate), 'MM/DD/YYYY'), 'MM/DD/YYYY') >= TO_DATE(TO_CHAR((enMemTwo.coverageStartDate), 'MM/DD/YYYY'), 'MM/DD/YYYY') )"
			+ " AND enOne.houseHoldCaseId = entwo.houseHoldCaseId AND enOne.id = enMemOne.enrollment1095.id"
			+ " AND entwo.id = enMemTwo.enrollment1095.id AND enMemOne.id != enMemTwo.id AND enMemOne.memberType ='MEMBER'"
			+ " AND enMemTwo.memberType ='MEMBER' AND enMemOne.memberId = enMemTwo.memberId AND enMemOne.isActive = 'Y'"
			+ " AND enMemTwo.isActive = 'Y' AND enOne.coverageYear IN(:coverageYearList) AND entwo.coverageYear IN(:coverageYearList)"
			+ " AND enOne.voidCheckboxindicator IS NULL AND entwo.voidCheckboxindicator IS NULL AND enOne.id <>entwo.id"
			+ " AND enOne.isActive = 'Y' AND entwo.isActive  = 'Y' AND enOne.isObsolete = 'N' AND entwo.isObsolete = 'N' AND enOne.id NOT IN (SELECT enOne.id FROM Enrollment1095 enOne, Enrollment1095 entwo"
			+ " WHERE enOne.houseHoldCaseId =entwo.houseHoldCaseId AND enOne.coverageYear =entwo.coverageYear AND enOne.id <> entwo.id"
			+ " AND enOne.voidCheckboxindicator IS NULL AND entwo.voidCheckboxindicator IS NULL AND enOne.coverageYear IN(:coverageYearList)"
			+ " AND entwo.coverageYear IN(:coverageYearList) AND ( TO_DATE(TO_CHAR((enOne.policyStartDate), 'MM/DD/YYYY'), 'MM/DD/YYYY')<=TO_DATE(TO_CHAR((entwo.policyEndDate), 'MM/DD/YYYY'), 'MM/DD/YYYY')"
			+ " AND TO_DATE(TO_CHAR((enOne.policyEndDate), 'MM/DD/YYYY'), 'MM/DD/YYYY') >=TO_DATE(TO_CHAR((entwo.policyStartDate), 'MM/DD/YYYY'), 'MM/DD/YYYY') )"
			+ ")")
	List<Integer> getRecordsForMemberLevelOverlapping(@Param("coverageYearList")List<Integer> coverageYearList);
	
	@Query("SELECT e.id FROM Enrollment1095 e, Enrollment en WHERE e.exchgAsignedPolicyId = en.id AND e.houseHoldCaseId <> en.houseHoldCaseId "
			+ " AND e.isActive = 'Y' AND e.isObsolete = 'N' AND e.coverageYear IN(:coverageYearList)")
	List<Integer> getRecordsForHouseholdMismatch(@Param("coverageYearList")List<Integer> coverageYearList);
	//Enrollment1095 Validation Repository ends
	
	@Query("SELECT DISTINCT(en.coverageYear) FROM Enrollment1095 en WHERE en.coverageYear IS NOT NULL")
	List<Integer> getAllCoverageYear();

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENROLLMENT_PREMIUM_1095_AUD WHERE ENROLLMENT_1095_ID = :enrollment1095Id")
	void deletePremiumAudit(@Param("enrollment1095Id") Integer enrollment1095Id);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENROLLMENT_PREMIUM_1095 WHERE ENROLLMENT_1095_ID = :enrollment1095Id")
	void deletePremium(@Param("enrollment1095Id") Integer enrollment1095Id);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENROLLMENT_MEMBER_1095_AUD WHERE ENROLLMENT_1095_ID = :enrollment1095Id")
	void deleteMemberAudit(@Param("enrollment1095Id") Integer enrollment1095Id);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENROLLMENT_MEMBER_1095 WHERE ENROLLMENT_1095_ID = :enrollment1095Id")
	void deleteMember(@Param("enrollment1095Id") Integer enrollment1095Id);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENROLLMENT_1095_AUD WHERE ID = :enrollment1095Id")
	void deleteEnrollment1095Audit(@Param("enrollment1095Id") Integer enrollment1095Id);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENROLLMENT_1095 WHERE ID = :enrollment1095Id")
	void deleteEnrollment1095(@Param("enrollment1095Id") Integer enrollment1095Id);
}
