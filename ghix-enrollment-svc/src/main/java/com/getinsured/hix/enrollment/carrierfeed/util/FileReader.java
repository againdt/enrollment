package com.getinsured.hix.enrollment.carrierfeed.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jabelardo on 7/8/14.
 */
public abstract class FileReader<T> {

    protected static String[] booleanTrueValues = {"1", "t", "y", "true", "yes"};

    protected static String[] booleanFalseValues = {"0", "f", "n", "false", "no"};

    private String dataFilename;
    private JsonNode mappings;
    private List<T> importList;

    public void initialize(String dataFilename, String mappingsFilename) throws IOException {
        this.dataFilename = dataFilename;
        ObjectMapper mapper = new ObjectMapper();
        mappings = mapper.readTree(new File(mappingsFilename));
    }

    protected String getDataFilename() {
        return dataFilename;
    }

    public List<T> importAll(Class<T> modelClass) throws IOException {
        importList = createImportList(modelClass);
        return importList;
    }

    public Iterator<T> importIterator(Class<T> modelClass) throws IOException {
        importList = createImportList(modelClass);
        return importList.iterator();
    }

    protected abstract List<T> createImportList(Class<T> modelClass) throws IOException;

    public JsonNode getMappings() {
        return mappings;
    }

    public List<String> getHeader() {
        List<String> headers = new ArrayList<>();
        JsonNode fieldNodes = getMappings().get("fields");
        for (JsonNode fieldNode : fieldNodes) {
            String fieldName = fieldNode.get("fieldName").asText();
            headers.add(fieldName);
        }
        return headers;
    }
}
