/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrollmentPremiumDTO;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * Service implementation for the Enrollment Premium table
 * @author negi_s
 * @since 06/20/2016
 */
@Service("enrollmentPremiumService")
@Transactional
public class EnrollmentPremiumServiceImpl implements EnrollmentPremiumService {
	
	private static final Logger LOGGER = Logger.getLogger(EnrollmentPremiumService.class);
	
	@Autowired IEnrollmentPremiumRepository enrollmentPremiumRepository;

	/* (non-Javadoc)
	 * @see com.getinsured.hix.enrollment.service.EnrollmentPremiumService#findPremiumDetailsByEnrollmentId(java.lang.Integer)
	 */
	@Override
	public EnrollmentResponse findPremiumDetailsByEnrollmentId(Integer enrollmentId) {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		enrollmentResponse.setEnrollmentId(enrollmentId);
		List<EnrollmentPremiumDTO> enrollmentPremiumDtoList =  new ArrayList<EnrollmentPremiumDTO>();
		try{
			List<EnrollmentPremium> enrollmentPremiumList = enrollmentPremiumRepository.findByEnrollmentId(enrollmentId);
			if(null != enrollmentPremiumList && !enrollmentPremiumList.isEmpty()){
				for(EnrollmentPremium enrollmentPremium :  enrollmentPremiumList){
					EnrollmentPremiumDTO epDto = new EnrollmentPremiumDTO();
					epDto.setYear(enrollmentPremium.getYear());
					epDto.setMonth(enrollmentPremium.getMonth());
					epDto.setAptcAmount(enrollmentPremium.getAptcAmount());
					epDto.setGrossPremiumAmount(enrollmentPremium.getGrossPremiumAmount());
					epDto.setNetPremiumAmount(enrollmentPremium.getNetPremiumAmount());
					epDto.setSlcspPremiumAmount(enrollmentPremium.getSlcspPremiumAmount());
					epDto.setSlcspPlanid(enrollmentPremium.getSlcspPlanid());
					enrollmentPremiumDtoList.add(epDto);
				}
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				enrollmentResponse.setEnrollmentPremiumDTOList(enrollmentPremiumDtoList);
			}else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("No premium data found for given enrollment Id :: " + enrollmentId);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
		}catch(Exception e){
			LOGGER.error("Error fetching data from enrollment premium table for enrollment Id :: " + enrollmentId, e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Error fetching data from enrollment premium table for enrollment Id :: " + enrollmentId);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
		}
		return enrollmentResponse;
	}
	
	@Override
	public EnrollmentResponse findPremiumDetailsByEnrollmentIdMonthAndYear(Integer enrollmentId, Integer month, Integer year ){
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		enrollmentResponse.setEnrollmentId(enrollmentId);
		List<EnrollmentPremiumDTO> enrollmentPremiumDtoList =  new ArrayList<EnrollmentPremiumDTO>();
		try{
			List<EnrollmentPremium> enrollmentPremiumList = enrollmentPremiumRepository.findByEnrollmentIdMonthAndYear(enrollmentId, month, year);
			if(null != enrollmentPremiumList && !enrollmentPremiumList.isEmpty()){
				for(EnrollmentPremium enrollmentPremium :  enrollmentPremiumList){
					EnrollmentPremiumDTO epDto = new EnrollmentPremiumDTO();
					epDto.setYear(enrollmentPremium.getYear());
					epDto.setMonth(enrollmentPremium.getMonth());
					epDto.setAptcAmount(enrollmentPremium.getAptcAmount());
					epDto.setGrossPremiumAmount(enrollmentPremium.getGrossPremiumAmount());
					epDto.setNetPremiumAmount(enrollmentPremium.getNetPremiumAmount());
					epDto.setSlcspPremiumAmount(enrollmentPremium.getSlcspPremiumAmount());
					epDto.setSlcspPlanid(enrollmentPremium.getSlcspPlanid());
					enrollmentPremiumDtoList.add(epDto);
				}
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				enrollmentResponse.setEnrollmentPremiumDTOList(enrollmentPremiumDtoList);
			}else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("No premium data found for given enrollment Id :: " + enrollmentId);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			}
		}catch(Exception e){
			LOGGER.error("Error fetching data from enrollment premium table for enrollment Id :: " + enrollmentId, e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Error fetching data from enrollment premium table for enrollment Id :: " + enrollmentId);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
		}
		return enrollmentResponse;
		
	}
}
