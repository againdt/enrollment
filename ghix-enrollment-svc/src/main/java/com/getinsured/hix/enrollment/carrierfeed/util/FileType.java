package com.getinsured.hix.enrollment.carrierfeed.util;

public enum FileType {
    EXCEL_CLASSIC,
    EXCEL_OOXML,
    CSV
    //more to come later.
}
