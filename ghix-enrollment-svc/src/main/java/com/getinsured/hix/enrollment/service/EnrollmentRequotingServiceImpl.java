package com.getinsured.hix.enrollment.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

import com.getinsured.hix.platform.config.PlanDisplayConfiguration;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingResponseDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanMemberRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanMemberResponseDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanPeriodLoopDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentLocationAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.LocationAud;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentAud;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;

@Service("enrollmentEditToolService")
public class EnrollmentRequotingServiceImpl implements EnrollmentRequotingService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentRequotingServiceImpl.class);
	@Autowired IEnrollmentPremiumRepository enrollmentPremiumRepository;
	@Autowired private IEnrolleeAudRepository enrolleeAudRepository;
	@Autowired private IEnrollmentAudRepository enrollmentAudRepository;
	@Autowired private IEnrollmentLocationAudRepository locationAudRepository;
	@Autowired private Gson platformGson;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	@Override 
	public void updateMonthlyPremiumForEnrollment(Enrollment enrollment, boolean updateLastSliceDate, boolean updateAPTC, String fillOnlySLCSP, boolean updateEffectiveDates) throws GIException{
		try{
			boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
			
			if(populateMonthlyPremium && enrollment!=null){
				if(enrollment.getEnrollmentStatusLkp()!=null && EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())){
					updateMonthlyPremiumForCancelTerm(enrollment, null);
					return;
				}
				//Loop through months to populate the members to call plan management
				List<Enrollee> enrolleeList=this.getNonCancelledEnrolleesAndSubscriber(enrollment);

				if(enrolleeList!=null && enrolleeList.size()>0){
					Enrollee householdContact= enrollment.getHouseholdContactForEnrollment();
					if(enrollment.getEnrollmentPremiums()==null ||enrollment.getEnrollmentPremiums().size()==0 ){
						enrollment.setEnrollmentPremiums(prepareEmptyMonthlyPremiums(enrollment));
					}

					//getCurrentMonthlyPremiums(enrollment);
					LifeSpanEnrollmentQuotingRequestDTO lifeSpanEnrollmentQuotingRequestDTO = new LifeSpanEnrollmentQuotingRequestDTO();  
					lifeSpanEnrollmentQuotingRequestDTO.setPlanId(""+enrollment.getPlanId());
					List<LifeSpanPeriodLoopDTO> periodLoopList= new ArrayList<>();
					Enrollee currentSubscriber= enrollment.getSubscriberForEnrollment();
					Date netPremiumEffectiveDate= enrollment.getNetPremEffDate();
					for(EnrollmentPremium prem: enrollment.getEnrollmentPremiums()){

						Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(prem.getMonth()-1,prem.getYear()) ); //Month Start Date
						Date monthEndDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(prem.getMonth()-1,prem.getYear()) ); //Month End Date



						EnrolleeAud subscriber=null;
						List<EnrolleeAud> subscAudList= enrolleeAudRepository.getEnrollSubscByAmtCovDate(enrollment.getId(), monthEndDate);
						if(subscAudList!=null && subscAudList.size()>0){
							subscriber=subscAudList.get(0);
						}
						if(subscriber==null){
							//this case arises when start date is preponed
							subscAudList= enrolleeAudRepository.geOldestSubscByDate(enrollment.getId(), monthEndDate);
							subscriber=subscAudList.get(0);
						}
						//enrollment.getSubscriberForEnrollment();
						//enrolleeAudRepository.getOldestSubscriberForMonth(enrollment.getId(), monthStartDate);//
						List<LifeSpanMemberRequestDTO> memberList= prepareMonthlyMembers(enrolleeList, monthStartDate, monthEndDate, subscriber, currentSubscriber, netPremiumEffectiveDate, householdContact);
						if(memberList!=null && memberList.size()>0){

							if(fillOnlySLCSP==null && (updateAPTC || prem.getActAptcAmount()==null)){
								if(enrollment.getAptcEffDate() != null && EnrollmentUtils.truncateTime(enrollment.getAptcEffDate()).before(monthEndDate))
								{
									//prem.setAptcAmount(enrollment.getAptcAmt());
									prem.setActAptcAmount(enrollment.getAptcAmt());
								}else if(prem.getActAptcAmount()==null){
									EnrollmentAud enrollmentAud=null;
									List<EnrollmentAud> enrollmentAPTCAudList=enrollmentAudRepository.getMaxAPTCEnrollmentForId( enrollment.getId(), DateUtil.dateToString(monthEndDate, GhixConstants.REQUIRED_DATE_FORMAT) );
									if(enrollmentAPTCAudList!=null && enrollmentAPTCAudList.size()>0){
										enrollmentAud=enrollmentAPTCAudList.get(0);
									}
									if(enrollmentAud!=null){
										//prem.setAptcAmount(enrollmentAud.getAptcAmt());
										prem.setActAptcAmount(enrollmentAud.getAptcAmt());

									}else{
										Pageable pageable = new PageRequest(0, 1);
										enrollmentAPTCAudList=enrollmentAudRepository.getOldestAPTCEnrollmentForId( enrollment.getId(), DateUtil.dateToString(monthEndDate, GhixConstants.REQUIRED_DATE_FORMAT), pageable );
										if(enrollmentAPTCAudList!=null && enrollmentAPTCAudList.size()>0){
											enrollmentAud=enrollmentAPTCAudList.get(0);
										}
										if(enrollmentAud!=null){
											//prem.setAptcAmount(enrollmentAud.getAptcAmt());
											prem.setActAptcAmount(enrollmentAud.getAptcAmt());
										}
									}
									
								}

								if(enrollment.getStateSubsidyEffDate() != null && EnrollmentUtils.truncateTime(enrollment.getStateSubsidyEffDate()).before(monthEndDate))
								{
									if(enrollment.getStateSubsidyAmt() != null) {
										prem.setActStateSubsidyAmount(enrollment.getStateSubsidyAmt());
									}
									else if(prem.getActStateSubsidyAmount()==null){
										EnrollmentAud enrollmentAud=null;
										List<EnrollmentAud> enrollmentStateSubsidyAudList=enrollmentAudRepository.getMaxStateSubsidyEnrollmentForId( enrollment.getId(), DateUtil.dateToString(monthEndDate, GhixConstants.REQUIRED_DATE_FORMAT) );
										if(enrollmentStateSubsidyAudList!=null && enrollmentStateSubsidyAudList.size()>0){
											enrollmentAud=enrollmentStateSubsidyAudList.get(0);
										}
										if(enrollmentAud!=null){
											//prem.setAptcAmount(enrollmentAud.getAptcAmt());
											prem.setActStateSubsidyAmount(enrollmentAud.getStateSubsidyAmt());

										}else{
											Pageable pageable = new PageRequest(0, 1);
											enrollmentStateSubsidyAudList=enrollmentAudRepository.getOldestStateSubsidyEnrollmentForId( enrollment.getId(), DateUtil.dateToString(monthEndDate, GhixConstants.REQUIRED_DATE_FORMAT), pageable );
											if(enrollmentStateSubsidyAudList!=null && enrollmentStateSubsidyAudList.size()>0){
												enrollmentAud=enrollmentStateSubsidyAudList.get(0);
											}
											if(enrollmentAud!=null){
												//prem.setAptcAmount(enrollmentAud.getAptcAmt());
												prem.setStateSubsidyAmount(enrollmentAud.getStateSubsidyAmt());
											}
										}

									}
								}
							}
							LifeSpanPeriodLoopDTO periodLoop= new LifeSpanPeriodLoopDTO();
							periodLoop.setPeriodId(prem.getYear()+String.format("%02d", prem.getMonth()));
							periodLoop.setPeriodEffectiveDate(DateUtil.dateToString(monthStartDate,EnrollmentConstants.DATE_FORMAT_YYYY_MM_DD));
							
							boolean currentAddress=false;
							if(currentSubscriber!=null && currentSubscriber.getHomeAddressid()!=null && EnrollmentUtils.truncateTime(currentSubscriber.getTotIndvRespEffDate()).compareTo(monthEndDate)<0){
								periodLoop.setSubscriberCountyCode(currentSubscriber.getHomeAddressid().getCountycode());
								periodLoop.setSubscriberZipCode(currentSubscriber.getHomeAddressid().getZip());
								currentAddress=true;
							}
							
							if(subscriber!=null && subscriber.getHomeAddressid()!=null && !currentAddress){
								Date updateDate=null;
								updateDate=subscriber.getUpdatedOn();
								if(updateDate!=null){
									Calendar calendar = TSCalendar.getInstance();
									calendar.setTime(updateDate);
									calendar.add(Calendar.SECOND, EnrollmentConstants.FIVE);
									updateDate= calendar.getTime();
								}
								List<LocationAud> loc= locationAudRepository.getLocationAudByIdAndDate(subscriber.getHomeAddressid().getId(), updateDate);
								if(loc!=null && loc.size()>0){
									periodLoop.setSubscriberCountyCode(loc.get(0).getCountycode());
									periodLoop.setSubscriberZipCode(loc.get(0).getZip());
								}else{
									//get oldest location data
									LocationAud initialLocation= locationAudRepository.getInitialLocationAudById(subscriber.getHomeAddressid().getId());
									if(initialLocation!=null){
										periodLoop.setSubscriberCountyCode(initialLocation.getCountycode());
										periodLoop.setSubscriberZipCode(initialLocation.getZip());
									}
								}
							}
							
							if( !EnrollmentUtils.isNotNullAndEmpty(periodLoop.getSubscriberCountyCode())){
								throw new GIException("Subscriber county code is null for period loop id : "+ periodLoop.getPeriodId()+" enrollment id : "+enrollment.getId()+" subscriber id : "+subscriber.getId());
							}
							if( !EnrollmentUtils.isNotNullAndEmpty(periodLoop.getSubscriberZipCode())){
								throw new GIException("Subscriber zip code is null for period loop id : "+ periodLoop.getPeriodId()+" enrollment id : "+enrollment.getId()+" subscriber id : "+subscriber.getId());
							}

							periodLoop.setRequestMemberList(memberList);
							periodLoopList.add(periodLoop);
						}
					}
					//Call Plan mgmt //write a method to call n get response
					LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuotingResponseDTO=null;

					if(periodLoopList!=null && periodLoopList.size()>0){
						lifeSpanEnrollmentQuotingRequestDTO.setPeriodLoopList(periodLoopList);
						try{
							lifeSpanEnrollmentQuotingResponseDTO= enrollmentExternalRestUtil.getPlanManagementQuotingResponse(lifeSpanEnrollmentQuotingRequestDTO);
						}catch(Exception e){
							if(LOGGER.isDebugEnabled()){
								LOGGER.debug("LifeSpanQuoting Request: "+platformGson.toJson(lifeSpanEnrollmentQuotingRequestDTO));
							}
							LOGGER.error("Exception occurred in LifeSpanQuoting call ", e);
							throw new Exception("PlanManagement call to get LifeSpanQuoting data Failed :: "+ e.getMessage());
						}
					}else{
						throw new Exception("No Period loop formed for enrollment id : "+enrollment.getId());
					}

					//After getting response check if events need to be created and do the same

					if(lifeSpanEnrollmentQuotingResponseDTO!=null && lifeSpanEnrollmentQuotingResponseDTO.getPeriodLoopList()!=null){
						//Event creation part is kept out as it creates different type of event for different requirements


						//Update premium data to premium tables
						updatePremiumData(enrollment, enrolleeList,lifeSpanEnrollmentQuotingResponseDTO, fillOnlySLCSP, updateLastSliceDate);

						//Update Enrollment Premium and Enrollee Premium and effective date
						if(updateLastSliceDate){
							updateLastSliceAmountAndDate(enrollment, enrolleeList, updateEffectiveDates);
						}
					}


				}
			}

		}catch(Exception e){
			LOGGER.error("Error while updating monthly premiums for enrollment with id :: "+enrollment.getId()+ " :: "+ e.getMessage(),e);
			throw new GIException("Error while updating monthly premiums for enrollment with id :: "+enrollment.getId()+ " :: "+ e.getMessage(), e);
		}

	}
	
	private void updatePremiumData(Enrollment enrollment,List<Enrollee> enrollees ,LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuotingResponseDTO, String fillOnlySLCSP, boolean updateLastSlice)throws Exception{
		if(enrollment!=null && lifeSpanEnrollmentQuotingResponseDTO!=null && lifeSpanEnrollmentQuotingResponseDTO.getPeriodLoopList()!=null && lifeSpanEnrollmentQuotingResponseDTO.getPeriodLoopList().size()>0){
			Enrollee subscriber= enrollment.getSubscriberForEnrollment();

			// sort in reverse order of their loop id
			/*Collections.sort(lifeSpanEnrollmentQuotingResponseDTO.getPeriodLoopList(),new Comparator<LifeSpanPeriodLoopDTO>(){
                @Override
				public int compare(LifeSpanPeriodLoopDTO p1,LifeSpanPeriodLoopDTO p2){
                	return (Integer.compare(Integer.parseInt(p2.getPeriodId()), Integer.parseInt(p1.getPeriodId())));
                }});*/
			Collections.sort(enrollment.getEnrollmentPremiums());
			for(EnrollmentPremium prem: enrollment.getEnrollmentPremiums()){
				LifeSpanPeriodLoopDTO period=getPeriodForPremium(lifeSpanEnrollmentQuotingResponseDTO, prem);
				if(period!=null){
					if(!period.getStatus().equalsIgnoreCase(EnrollmentConstants.SUCCESS)) {
						LOGGER.error("Plan Management Requoting error response for period id :: "+period.getPeriodId()+ " is : "+period.getErrorCode() +" :: "+period.getErrorMsg());
						throw new Exception("Plan Management Requoting error response for period id :: "+period.getPeriodId()+ " is : "+period.getErrorCode() +" :: "+period.getErrorMsg());
					}
					setMonthlyPremiumAmounts(prem, period, enrollees, enrollment, fillOnlySLCSP, updateLastSlice);
					prem.setLastEventId(subscriber.getLastEventId());
					//prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
					//prem.setAptcChangeLogic(EnrollmentPremium.);
				}else if(fillOnlySLCSP ==null){
					if(prem.getAptcAmount()!=null|| prem.getGrossPremiumAmount()!=null){
						prem.setLastEventId(subscriber.getLastEventId());
						//prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
						resetMonthlyPremium(prem);
					}


				}
			}
		}
	}
	
	private LifeSpanPeriodLoopDTO getPeriodForPremium(LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuotingResponseDTO,EnrollmentPremium prem){
		LifeSpanPeriodLoopDTO period=null;
		for(LifeSpanPeriodLoopDTO prd: lifeSpanEnrollmentQuotingResponseDTO.getPeriodLoopList()){
			if((prem.getYear()+String.format("%02d", prem.getMonth())).equalsIgnoreCase(prd.getPeriodId())){
				period=prd;
				break;
			}
		}
		return period;
	}
	
	/* *
	 * Send only enrollees and subscribers
	 */
	private int calculateActiveMemberCount(List<Enrollee> enrollees, Date monthStartDate, Date monthEndDate){
		Set<String> uniqueMemberIds= new java.util.HashSet<String>();
		
		if(enrollees!=null && enrollees.size()>0 && monthEndDate!=null && monthEndDate!=null){
			
			for (Enrollee enrollee : enrollees){
				if(enrollee.getEffectiveStartDate().before(monthEndDate) && (enrollee.getEffectiveEndDate() == null || enrollee.getEffectiveEndDate().after(monthStartDate) )){
					uniqueMemberIds.add(enrollee.getExchgIndivIdentifier());
				}
				
			}
			
		}
		return uniqueMemberIds.size();
	}
	
	private void setMonthlyPremiumAmounts( EnrollmentPremium prem, LifeSpanPeriodLoopDTO period,List<Enrollee> enrollees, Enrollment enrollment, String fillOnlySLCSP, boolean updateLastSlice)throws Exception{
		if(prem!=null && period!=null && enrollees!=null && enrollees.size()>0){
			if(fillOnlySLCSP==null){
				Float totalGrossPremium=0.0f;
				Float actualGrossPremium=0.0f;
				Date monthStartDate=EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(prem.getMonth()-1,prem.getYear()));
				Date monthEndDate=EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(prem.getMonth()-1,prem.getYear())); //Month End Date;
				Enrollee subscriber=null;
				for(LifeSpanMemberResponseDTO member: period.getResponseMemberList()){
					Float memberContribution=0.0f;
					if(EnrollmentUtils.isNotNullAndEmpty(member.getMemberLevelPreContribution())){
						Enrollee enrollee=null;
						for(Enrollee enr : enrollees){
							if(enr.getId()==member.getId()){
								enrollee=enr;
								break;
							}
						}
						if(enrollee!=null){
							if(enrollee.getPersonTypeLkp()!=null && EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())){
								subscriber=enrollee;
							}
							memberContribution=Float.parseFloat(member.getMemberLevelPreContribution());
							if(enrollee.getLastSliceTotalIndvRespAmt()==null){
								enrollee.setLastSliceTotalIndvRespAmt(memberContribution);
								/*if(member.getRatingArea()!=null && !EnrollmentUtils.compareString(member.getRatingArea(), enrollee.getRatingArea())){
									enrollee.setRatingArea(member.getRatingArea());
									enrollee.setRatingAreaEffDate(enrollment.getNetPremEffDate());
									enrollee.setRatingAreaChanged(true);
								}*/
							}
							
							int daysInMonth=EnrollmentUtils.daysInMonth(monthStartDate);
							int activeDays=daysInMonth;
							if(EnrollmentUtils.isNotNullAndEmpty(member.getDaysActive())){
								activeDays=Integer.parseInt(member.getDaysActive());
							}
							totalGrossPremium=totalGrossPremium+(( memberContribution*activeDays)/daysInMonth);
							if (enrollee.getEffectiveStartDate().before(monthEndDate) && (enrollee.getEffectiveEndDate() == null || enrollee.getEffectiveEndDate().after(monthStartDate) )) {
								actualGrossPremium += memberContribution;
							}
						}
					}
				}
				prem.setGrossPremiumAmount(totalGrossPremium);
				prem.setActGrossPremiumAmount(actualGrossPremium);

				if(null != prem.getGrossPremiumAmount()){
						calculatePremiumAndSubsidies(prem, enrollment, enrollees, monthStartDate, monthEndDate, false, false);
				}
				else{
					prem.setAptcAmount(null);
					prem.setStateSubsidyAmount(null);
					prem.setNetPremiumAmount(null);
				}
				
				if(enrollment.getFinancialEffectiveDate()!=null && enrollment.getFinancialEffectiveDate().before(monthEndDate)) {
					if(enrollment.getMaxAPTCAmt()!=null) {
						prem.setMaxAptc(enrollment.getMaxAPTCAmt());
					}else if(null == prem.getAptcAmount()){
						prem.setMaxAptc(null);
					}
					if(enrollment.getMaxStateSubsidyAmt()!=null){
						prem.setMaxStateSubsidy(enrollment.getMaxStateSubsidyAmt());
					}
				}
				
				if (EnrollmentConfiguration.isCaCall() && EnrollmentUtils.isNotNullAndEmpty(period.getSlspPremium())) {
					prem.setSlcspPremiumAmount((float) period.getSlspPremium());
					prem.setSlcspPlanid(period.getSlspHiosPlanId());
				} else if (!EnrollmentConfiguration.isCaCall()
						&& EnrollmentUtils.isNotNullAndEmpty(period.getSlspHiosPlanId())
						&& EnrollmentUtils.isNotNullAndEmpty(enrollment.getSlcspAmt())) {
					prem.setSlcspPremiumAmount(enrollment.getSlcspAmt());
					prem.setSlcspPlanid(period.getSlspHiosPlanId());
				}
			}else{
				if(EnrollmentConstants.FILL_ONLY_SLCSP_ALL.equalsIgnoreCase(fillOnlySLCSP)){
					if (EnrollmentConfiguration.isCaCall()
							&& EnrollmentUtils.isNotNullAndEmpty(period.getSlspPremium())) {
						prem.setSlcspPremiumAmount((float) period.getSlspPremium());
						prem.setSlcspPlanid(period.getSlspHiosPlanId());
					} else if (!EnrollmentConfiguration.isCaCall()
							&& EnrollmentUtils.isNotNullAndEmpty(period.getSlspHiosPlanId())
							&& EnrollmentUtils.isNotNullAndEmpty(enrollment.getSlcspAmt())) {
						prem.setSlcspPremiumAmount(enrollment.getSlcspAmt());
						prem.setSlcspPlanid(period.getSlspHiosPlanId());
					}
				} else if (EnrollmentConstants.FILL_ONLY_SLCSP_MISSING_MONTHS.equalsIgnoreCase(fillOnlySLCSP)) {
					if (EnrollmentConfiguration.isCaCall() && EnrollmentUtils.isNotNullAndEmpty(period.getSlspPremium())
							&& prem.getSlcspPremiumAmount() == null) {
						prem.setSlcspPremiumAmount((float) period.getSlspPremium());
						prem.setSlcspPlanid(period.getSlspHiosPlanId());
					}
					if (!EnrollmentConfiguration.isCaCall()
							&& EnrollmentUtils.isNotNullAndEmpty(period.getSlspHiosPlanId())
							&& prem.getSlcspPremiumAmount() == null
							&& EnrollmentUtils.isNotNullAndEmpty(enrollment.getSlcspAmt())) {
						prem.setSlcspPremiumAmount(enrollment.getSlcspAmt());
						prem.setSlcspPlanid(period.getSlspHiosPlanId());
					}
				}
				
			}
			
			
		}
	}
	
	
	
	
	private void setMonthlyPremiumAmounts( EnrollmentPremium prem,List<Enrollee> enrollees, Date monthStartDate, Date monthEndDate, Enrollment enrollment)throws Exception{
		if(prem!=null && enrollees!=null && enrollees.size()>0 && enrollment!=null){
			Float totalGrossPremium=0.0f;
			Float actualGrossPremium=0.0f;
			for(Enrollee enrollee: enrollees){
				Float memberContribution=0.0f;
				if(EnrollmentUtils.isNotNullAndEmpty(enrollee.getTotalIndvResponsibilityAmt())){

					if(enrollee.getEffectiveStartDate().before(monthEndDate) && (enrollee.getEffectiveEndDate() == null || enrollee.getEffectiveEndDate().after(monthStartDate) )){
						memberContribution=enrollee.getTotalIndvResponsibilityAmt();
						int daysInMonth=EnrollmentUtils.daysInMonth(EnrollmentUtils.getMonthStartDate(prem.getMonth()-1,prem.getYear()));
						int activeDays=daysInMonth;
						activeDays= calculateActiveDays(enrollee.getEffectiveStartDate(), enrollee.getEffectiveEndDate(), monthStartDate, monthEndDate);
						totalGrossPremium=totalGrossPremium+(( memberContribution*activeDays)/daysInMonth);
						if (enrollee.getEffectiveStartDate().before(monthEndDate) && (enrollee.getEffectiveEndDate() == null || enrollee.getEffectiveEndDate().after(monthStartDate) )) {
							actualGrossPremium += memberContribution;
						}
					}
				}
			}
			prem.setGrossPremiumAmount(totalGrossPremium);
			prem.setActGrossPremiumAmount(actualGrossPremium);
			if(null != prem.getGrossPremiumAmount()){
				calculatePremiumAndSubsidies(prem, enrollment, enrollees, monthStartDate, monthEndDate, false, false);
			}else{
				prem.setStateSubsidyAmount(null);
				prem.setAptcAmount(null);
				prem.setNetPremiumAmount(null);
			}

		}
	}
	
@SuppressWarnings("unused")
private EnrollmentPremium getEnrollmentPremiumByPeriodId(String periodLoopId, Enrollment enrollment){
	EnrollmentPremium periodPrem= null;
	if(EnrollmentUtils.isNotNullAndEmpty(periodLoopId) && enrollment!=null && enrollment.getEnrollmentPremiums()!=null && enrollment.getEnrollmentPremiums().size()>0){
		for (EnrollmentPremium prem: enrollment.getEnrollmentPremiums()){
			if((prem.getYear()+String.format("%02d", prem.getMonth())).equalsIgnoreCase(periodLoopId)){
				periodPrem=prem;
				break;
			}
		}
	}
	return periodPrem;
}
	
private void updateLastSliceAmountAndDate(Enrollment enrollment, List<Enrollee> enrollees, boolean updateEffectiveDates) throws Exception{
	boolean useSameEffectiveDate = Boolean.parseBoolean(DynamicPropertiesUtil
			.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.USE_SAME_EFFECTIVE_DATE));
	boolean isAptcChange = false;
	boolean isGrossPremChange = false;
	boolean isStatesubsidyChange = false;
	Date eventEffectiveDate=null;
	List<EnrollmentPremium> premiums=enrollment.getEnrollmentPremiums();
	if(enrollment!=null && premiums!=null && premiums.size()>0){
		//sort in descending order of Year and Month
		Collections.sort(premiums);

		//Generate values for null Gross premium and APTC 
		generateActualGrossAndAptc(premiums, enrollees, enrollment);
		
		//loop in descending order to find last slice
		//Float lastGrossPrem=null;
		//Float lastAptc=null;
		//Float lastNetPrem=null;
		Float actualGrossPrem=null;
		Float actualAptc=null;
		BigDecimal actualStateSubsidy = null;
		boolean populateCSRForZeroAPTC=false;		 
		populateCSRForZeroAPTC=Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.POPULATE_CSR_APTC_ZERO));
		for(EnrollmentPremium prem: premiums){
			if(prem.getMonth().intValue()!=12){
				eventEffectiveDate= EnrollmentUtils.getMonthStartDate(prem.getMonth(),prem.getYear());
			}
			if(actualGrossPrem!=null && actualGrossPrem!=0.0f && !EnrollmentUtils.isFloatEqual(actualGrossPrem, prem.getActGrossPremiumAmount())){
				isGrossPremChange = true;
				break;
			}
			if(actualGrossPrem!=null && actualGrossPrem!=0.0f && !EnrollmentUtils.isFloatEqual(actualAptc, prem.getActAptcAmount())){
				isAptcChange = true;
			}
			if(actualGrossPrem!=null && actualGrossPrem!=0.0f && actualStateSubsidy != null && prem.getActStateSubsidyAmount() != null && actualStateSubsidy.compareTo(prem.getActStateSubsidyAmount()) != 0){
				isStatesubsidyChange = true;
			}
			if(isAptcChange || isStatesubsidyChange){
				break;
			}
			/*if(lastNetPrem!=null && lastNetPrem!=0.0f && !EnrollmentUtils.isFloatEqual(lastNetPrem, prem.getNetPremiumAmount())){
				break;
			}*/
			if(prem.getMonth().intValue()==1){
				eventEffectiveDate= EnrollmentUtils.getMonthStartDate(prem.getMonth()-1,prem.getYear());
			}
			//lastGrossPrem= prem.getGrossPremiumAmount();
			//lastAptc=prem.getAptcAmount();
			//lastNetPrem=prem.getNetPremiumAmount();
			actualGrossPrem = prem.getActGrossPremiumAmount();
			actualAptc = prem.getActAptcAmount();
			actualStateSubsidy = prem.getActStateSubsidyAmount();
		}

		if(eventEffectiveDate!=null){
			if(eventEffectiveDate.before(enrollment.getBenefitEffectiveDate())){
				eventEffectiveDate=enrollment.getBenefitEffectiveDate();
			}
			
			//Set Amounts
			enrollment.setGrossPremiumAmt(actualGrossPrem);
			if(enrollment.getCMSPlanID() != null && !enrollment.getCMSPlanID().substring(enrollment.getCMSPlanID().length()-2, enrollment.getCMSPlanID().length()).equalsIgnoreCase("01")
            		&& (populateCSRForZeroAPTC ||(enrollment.getAptcAmt() != null && enrollment.getAptcAmt() > 0))){
				if(enrollment.getGrossPremiumAmt()!=null && enrollment.getCsrMultiplier()!=null){
					enrollment.setCsrAmt(EnrollmentUtils.precision((enrollment.getGrossPremiumAmt() * enrollment.getCsrMultiplier()), 2));
				}
			}
			
			enrollment.setAptcAmt(actualAptc);

			Float totalSubsidy = actualAptc;

			if(actualStateSubsidy != null){
				enrollment.setStateSubsidyAmt(actualStateSubsidy);
				if(totalSubsidy != null) {
					totalSubsidy = totalSubsidy + actualStateSubsidy.floatValue();
				}
				else{
					totalSubsidy = actualStateSubsidy.floatValue();
				}
			}

			if(null != actualGrossPrem && null != totalSubsidy){
				enrollment.setNetPremiumAmt(EnrollmentUtils.roundFloat((actualGrossPrem - totalSubsidy), 2) );
			}else{
				enrollment.setNetPremiumAmt(actualGrossPrem);
			}
			
			
			//HIX-118147 Setting effective date to all if use same effective date configuration is true
			if (updateEffectiveDates) {
				if (useSameEffectiveDate || !(isGrossPremChange || isAptcChange || isStatesubsidyChange)) {
					if (enrollment.getCsrAmt() != null) {
						enrollment.setCsrEffDate(eventEffectiveDate);
					}
					enrollment.setGrossPremEffDate(eventEffectiveDate);
					enrollment.setNetPremEffDate(eventEffectiveDate);
					if(isAptcChange) {
						enrollment.setAptcEffDate(eventEffectiveDate);
					}
					if(isStatesubsidyChange){
						enrollment.setStateSubsidyEffDate(eventEffectiveDate);
					}
					for (Enrollee enrollee : enrollees) {
						enrollee.setTotIndvRespEffDate(eventEffectiveDate);
						if (enrollee.getLastSliceTotalIndvRespAmt() != null) {
							enrollee.setTotalIndvResponsibilityAmt(enrollee.getLastSliceTotalIndvRespAmt());
						}
						/*
						 * if(enrollee.isRatingAreaChanged() ||(enrollee.getRatingAreaEffDate()!=null &&
						 * enrollee.getRatingAreaEffDate().after(eventEffectiveDate))){
						 * enrollee.setRatingAreaEffDate(enrollment.getNetPremEffDate()); }
						 */
						// HIX-101450: Rating Area Effective Date prior to Coverage Date
						if (enrollee.getRatingAreaEffDate() != null
								&& enrollee.getRatingAreaEffDate().before(enrollment.getBenefitEffectiveDate())) {
							enrollee.setRatingAreaEffDate(enrollment.getBenefitEffectiveDate());
						}
					}
				} else {
					enrollment.setNetPremEffDate(eventEffectiveDate);
					for (Enrollee enrollee : enrollees) {
						if (enrollee.getLastSliceTotalIndvRespAmt() != null
								&& !EnrollmentUtils.isFloatEqual(enrollee.getTotalIndvResponsibilityAmt(),
										enrollee.getLastSliceTotalIndvRespAmt())) {
							enrollee.setTotIndvRespEffDate(eventEffectiveDate);
						} else if (enrollee.getTotIndvRespEffDate().before(enrollment.getBenefitEffectiveDate())) {
							enrollee.setTotIndvRespEffDate(enrollment.getBenefitEffectiveDate());
						}
						if (enrollee.getLastSliceTotalIndvRespAmt() != null) {
							enrollee.setTotalIndvResponsibilityAmt(enrollee.getLastSliceTotalIndvRespAmt());
						}
						// HIX-101450: Rating Area Effective Date prior to Coverage Date
						if (enrollee.getRatingAreaEffDate() != null
								&& enrollee.getRatingAreaEffDate().before(enrollment.getBenefitEffectiveDate())) {
							enrollee.setRatingAreaEffDate(enrollment.getBenefitEffectiveDate());
						}
					}
					if (isGrossPremChange) {
						if (enrollment.getCsrAmt() != null) {
							enrollment.setCsrEffDate(eventEffectiveDate);
						}
						enrollment.setGrossPremEffDate(eventEffectiveDate);
					}
					if (isAptcChange) {
						enrollment.setAptcEffDate(eventEffectiveDate);
					}
					if(isStatesubsidyChange){
						enrollment.setStateSubsidyEffDate(eventEffectiveDate);
					}
				}
			}
		}// End of if eventEffectiveDate null check
	}
}
	
private List<LifeSpanMemberRequestDTO> prepareMonthlyMembers(List<Enrollee> enrolleeList, Date monthStartDate, Date monthEndDate, EnrolleeAud subscriber, Enrollee currentSubscriber, Date netPremiumEffectiveDate, Enrollee householdContact)throws Exception{
	List<LifeSpanMemberRequestDTO> monthlyMembers= new ArrayList<>();
	for(Enrollee enrollee: enrolleeList){
		if(enrollee.getEffectiveStartDate().before(monthEndDate) && (enrollee.getEffectiveEndDate() == null || enrollee.getEffectiveEndDate().after(monthStartDate) )){
			//monthlyEnrollees.add(enrollee);
			LifeSpanMemberRequestDTO  member= new LifeSpanMemberRequestDTO();
			member.setId(enrollee.getId());
			if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
				EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGE_BASED_REQUOTING)) && enrollee.getBirthDate()!=null) {//HIX-105931
				/*if(EnrollmentUtils.isNotNullAndEmpty(enrollee.getAge())) {
					member.setAge(enrollee.getAge());
				}*/
				if(enrollee.getQuotingDate()!=null){
					member.setAge(EnrollmentUtils.getRequotingAge(enrollee.getBirthDate(), enrollee.getQuotingDate()));
				}else{
					member.setAge(EnrollmentUtils.getRequotingAge(enrollee.getBirthDate(), enrollee.getEffectiveStartDate()));
				}
				
			}
			
			member.setCoverageStartDate(DateUtil.dateToString(enrollee.getEffectiveStartDate(),EnrollmentConstants.DATE_FORMAT_YYYY_MM_DD));
			if(enrollee.getBirthDate()!=null){
				member.setDob(DateUtil.dateToString(enrollee.getBirthDate(), EnrollmentConstants.DATE_FORMAT_YYYY_MM_DD));
			}
			/*if(enrollee.getGenderLkp()!=null){
					member.setGender(enrollee.getGenderLkp().getLookupValueCode());
				}*/
			member.setDaysActive(""+calculateActiveDays(enrollee.getEffectiveStartDate(), enrollee.getEffectiveEndDate(), monthStartDate, monthEndDate));
			{
				LookupValue relationToHCP=null;
				if(enrollee.getUpdatedOn()!=null && enrollee.getUpdatedOn().before(monthEndDate)){
					relationToHCP=enrollee.getRelationshipToHCPLkp();
				}else{
					List<EnrolleeAud> enrolleeLatest= enrolleeAudRepository.findEnrolleeRevByDate(enrollee.getId(), monthEndDate);
					if(enrolleeLatest!=null && enrolleeLatest.size()>0){
						relationToHCP=enrolleeLatest.get(0).getRelationshipToHCPLkp();
					}
					
					if(relationToHCP==null){
						EnrolleeAud initialAud= enrolleeAudRepository.getMinEnrolleeAud(enrollee.getId());
						if(initialAud!=null){
							relationToHCP= initialAud.getRelationshipToHCPLkp();
						}else{
							relationToHCP=enrollee.getRelationshipToHCPLkp();
						}
					}
				}
				
				if(relationToHCP!=null){
					member.setRelation(relationToHCP.getLookupValueCode());
				}else if(householdContact!=null && enrollee.getExchgIndivIdentifier().equalsIgnoreCase(householdContact.getExchgIndivIdentifier())){
					member.setRelation(EnrollmentConstants.RELATIONSHIP_SELF_CODE);
				}else {
					member.setRelation(EnrollmentConstants.OTHER_RELATION_CODE);
				}
				
			}
			
			
			
			{
				LookupValue tobacco=null;
				if(enrollee.getLastTobaccoUseDate()!=null && enrollee.getLastTobaccoUseDate().before(monthEndDate)){
					tobacco=enrollee.getTobaccoUsageLkp();
				}else{
					EnrolleeAud tobaccoAud=null;
					List<EnrolleeAud> enrolleeAuds= enrolleeAudRepository.findEnrolleeRevByTobacoDate(enrollee.getId(), monthEndDate);
					if(enrolleeAuds!=null && enrolleeAuds.size()>0){
						tobaccoAud=enrolleeAuds.get(0);
					}else{
						tobaccoAud=enrolleeAudRepository.findInsertAudByEnrolleeId(enrollee.getId());
					}
					if(tobaccoAud!=null){
						tobacco=tobaccoAud.getTobaccoUsageLkp();
					}
				}
				
				
				if(tobacco!=null){
					if(EnrollmentConstants.T.equalsIgnoreCase(tobacco.getLookupValueCode())){
					member.setTobacco(EnrollmentConstants.Y);
				}else{
					member.setTobacco(EnrollmentConstants.N);
				}
			}else{
				member.setTobacco(EnrollmentConstants.N);
			}
				
			}
			
			monthlyMembers.add(member);
		}
	}
	return monthlyMembers;
}
	
	@Override
	public void populateMonthlyPremiumsForNewEnrollment(Enrollment enrollment) throws GIException{
		try{
			boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
			List<Enrollee> enrollees=getNonCancelledEnrolleesAndSubscriber(enrollment);
		if(populateMonthlyPremium &&enrollment!=null ){
			if(enrollment.getEnrollmentPremiums()==null ||enrollment.getEnrollmentPremiums().size()==0 ){
				enrollment.setEnrollmentPremiums(prepareEmptyMonthlyPremiums(enrollment));
			}

			Enrollee subscriber= enrollment.getSubscriberForEnrollment();
			for(EnrollmentPremium enrollmentPremium: enrollment.getEnrollmentPremiums()){

				Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(enrollmentPremium.getMonth()-1,enrollmentPremium.getYear())); //Month Start Date
				Date monthEndDate = EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(enrollmentPremium.getMonth()-1,enrollmentPremium.getYear())); //Month End Date
				if(enrollment.getBenefitEffectiveDate().before(monthEndDate) && (enrollment.getBenefitEndDate() == null || enrollment.getBenefitEndDate().after(monthStartDate) )){
					enrollmentPremium.setMaxAptc(enrollment.getMaxAPTCAmt());
					enrollmentPremium.setMaxStateSubsidy(enrollment.getMaxStateSubsidyAmt());
					enrollmentPremium.setGrossPremiumAmount(calculateProratedAmount(enrollment.getBenefitEffectiveDate(), enrollment.getBenefitEndDate(), monthStartDate, monthEndDate, enrollment.getGrossPremiumAmt()));//calculate the prorated amount
					enrollmentPremium.setActGrossPremiumAmount(enrollment.getGrossPremiumAmt());
					if(null != enrollmentPremium.getGrossPremiumAmount()){
						calculatePremiumAndSubsidies(enrollmentPremium, enrollment, enrollees, monthStartDate, monthEndDate, false, false);
					}else{
						enrollmentPremium.setAptcAmount(null);
						enrollmentPremium.setStateSubsidyAmount(null);
						enrollmentPremium.setNetPremiumAmount(null);
					}

					enrollmentPremium.setSlcspPremiumAmount(enrollment.getSlcspAmt());
					enrollmentPremium.setSlcspPlanid(enrollment.getSlcspPlanid());

					enrollmentPremium.setLastEventId(subscriber.getLastEventId());
					//enrollmentPremium.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());

				}
			}
		}
		}catch(Exception e){
			LOGGER.error("Error while populating monthly premiums for enrollment with id :: "+enrollment.getId(), e);
			throw new GIException("Error while populating monthly premiums for enrollment with id :: "+enrollment.getId(), e);
		}
	}

	@SuppressWarnings("unused")
	private List<EnrollmentPremium> getCurrentMonthlyPremiums(Enrollment enrollment){
		List<EnrollmentPremium> enrollmentPremiumList=null;
		if(enrollment!=null){
			enrollmentPremiumList = enrollmentPremiumRepository.findByEnrollmentId(enrollment.getId());
			if(enrollmentPremiumList==null || enrollmentPremiumList.size()<1){
				enrollmentPremiumList= prepareEmptyMonthlyPremiums(enrollment);
			}
		}
		return enrollmentPremiumList;

	}

	private List<EnrollmentPremium>  prepareEmptyMonthlyPremiums(Enrollment enrollment){
		List<EnrollmentPremium> enrollmentPremiumList= null;
		if(enrollment!=null){
			enrollmentPremiumList= new ArrayList<EnrollmentPremium>();
		}
		new ArrayList<EnrollmentPremium>();
		//boolean isShop=false;
		int startMonth=01;
		Calendar start=TSCalendar.getInstance();
		start.setTime(enrollment.getBenefitEffectiveDate());
		int startYear=start.get(Calendar.YEAR);
		/*if(enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
			isShop=true;
			//startMonth= enrollment.getBenefitEffectiveDate().getMonth()+1;//sld be employerenrollment start month 
		}*/
		for(int i=startMonth;i<=12;i++){
			enrollmentPremiumList.add(prepareEmptyMonthlyPremium(enrollment, i, startYear));
		}
		//TODO shop part wont be implemented for now
		/*	if(isShop){
			for(int i=1;i<=startMonth;i++){
				enrollmentPremiumList.add(prepareEmptyMonthlyPremium(enrollment, i, startYear+1));
			}
		}

		 */			return enrollmentPremiumList;
	}

	private EnrollmentPremium prepareEmptyMonthlyPremium(Enrollment enrollment, int month, int year){
		EnrollmentPremium prem= new EnrollmentPremium();
		prem.setMonth(month);
		prem.setYear(year);
		prem.setEnrollment(enrollment);
		return prem;
	}
	private void  resetMonthlyPremium(EnrollmentPremium prem){
		if(prem!=null){
			prem.setAptcAmount(null);
			prem.setGrossPremiumAmount(null);
			prem.setNetPremiumAmount(null);
			prem.setSlcspPremiumAmount(null);
			prem.setSlcspPlanid(null);
			prem.setActAptcAmount(null);
			prem.setActGrossPremiumAmount(null);
			prem.setStateSubsidyAmount(null);
			prem.setActStateSubsidyAmount(null);
		}
	}
	
	// any method calling this should pass non null dates for all parameters
	private int calculateActiveDays(Date coverageStartDate, Date coverageEndDate,Date monthStartDate, Date monthEndDate) throws GIException{
		int daysActive=0;
		coverageStartDate = EnrollmentUtils.truncateTime(coverageStartDate); 
		coverageEndDate = EnrollmentUtils.truncateTime(coverageEndDate); 
		if(coverageStartDate != null && coverageEndDate !=null ){ 
			boolean isCoverageAfterMonthStart = coverageStartDate.after(monthStartDate);
			//					boolean isCoverageBeforeMonthEnd = coverageStartDate.before(endDate);
			boolean isCoverageTermBeforeMonthEnd = coverageEndDate.before(monthEndDate);
			//					boolean isCoverageTermAfterMonthStart = coverageEndDate.after(startDate);
			daysActive = EnrollmentUtils.daysInMonth(monthStartDate) ;

			if(isCoverageAfterMonthStart && isCoverageTermBeforeMonthEnd){
				// Calculate number of days between coverage start date and coverage end date
				daysActive = EnrollmentUtils.getDaysBetween(coverageStartDate, coverageEndDate) + 1;

			}else if(isCoverageAfterMonthStart){
				// Calculate number of days between coverage start date and month end date
				daysActive = EnrollmentUtils.getDaysBetween(coverageStartDate, monthEndDate);
			}else if(isCoverageTermBeforeMonthEnd){
				// Calculate number of days between month start date and coverage term date
				daysActive = EnrollmentUtils.getDaysBetween(monthStartDate, coverageEndDate) + 1;
			}
		}
		return daysActive;
	}
	
	private Float calculateProratedAmount(Date coverageStartDate, Date coverageEndDate,Date monthStartDate, Date monthEndDate, Float premAmount ) throws GIException {
		Float proratedAmount=0.f;
		if((null != premAmount && 0 != premAmount.compareTo(0f)) ){

			int daysActive=calculateActiveDays(coverageStartDate, coverageEndDate,monthStartDate, monthEndDate);				
			proratedAmount = premAmount / EnrollmentUtils.daysInMonth(monthStartDate) * daysActive;	
		}
		return proratedAmount;
	}
	


	/**
	 * @author parhi_s
	 * 
	 * Returns Subscriber and Enrollees for Enrollment, Returns all Active and Non-Active Enrolles which ar enot in cancel state.
	 * 
	 * @return
	 */
	private List<Enrollee> getNonCancelledEnrolleesAndSubscriber(Enrollment enrollment){
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		List<Enrollee> enrollees=enrollment.getEnrollees();
		if(enrollees != null && !enrollees.isEmpty()){
			for(Enrollee enrollee : enrollees){
				if(enrollee.getPersonTypeLkp() != null 
						&& (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) 
								|| enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))
						&& enrollee.getEnrolleeLkpValue()!=null && !EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode())){
					enrolleeList.add(enrollee);
				}
			}
		}
		return enrolleeList;
	}
	
	@Override
	public void updateMonthlyPremiumsForAPTCSliderChange(Enrollment enrollment) throws GIException{
		try{
			boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
			if(populateMonthlyPremium && enrollment!=null && enrollment.getEnrollmentPremiums()!=null &&enrollment.getEnrollmentPremiums().size()>0 ){
				List<Enrollee> enrollees=getNonCancelledEnrolleesAndSubscriber(enrollment);
				Enrollee subscriber= enrollment.getSubscriberForEnrollment();
				Date aptcEffectiveDate= EnrollmentUtils.truncateTime(enrollment.getAptcEffDate());

                for(EnrollmentPremium enrollmentPremium: enrollment.getEnrollmentPremiums()){

                    Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(enrollmentPremium.getMonth()-1,enrollmentPremium.getYear())); //Month Start Date
                    Date monthEndDate = EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(enrollmentPremium.getMonth()-1,enrollmentPremium.getYear())); //Month End Date
                    if(aptcEffectiveDate.before(monthEndDate)  && monthEndDate.compareTo(EnrollmentUtils.getNextMonthStartDate(enrollment.getBenefitEndDate()))<=0){
                        enrollmentPremium.setGrossPremiumAmount(calculateProratedAmount(enrollment.getBenefitEffectiveDate(), enrollment.getBenefitEndDate(), monthStartDate, monthEndDate, enrollment.getGrossPremiumAmt()));//calculate the prorated amount
                        enrollmentPremium.setActGrossPremiumAmount(enrollment.getGrossPremiumAmt());
                        if(null != enrollmentPremium.getGrossPremiumAmount()){
							calculatePremiumAndSubsidies(enrollmentPremium, enrollment, enrollees, monthStartDate, monthEndDate, false, false);
                        }else{
                            enrollmentPremium.setAptcAmount(null);
                            enrollmentPremium.setStateSubsidyAmount(null);
                            enrollmentPremium.setNetPremiumAmount(null);
                        }

                        enrollmentPremium.setSlcspPremiumAmount(enrollment.getSlcspAmt());
                        enrollmentPremium.setSlcspPlanid(enrollment.getSlcspPlanid());

                        enrollmentPremium.setLastEventId(subscriber.getLastEventId());
                        //enrollmentPremium.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());

                    }
                }
			}
		}catch(Exception e){
			LOGGER.error("Error while updating monthly premiums for APTC Slider change of enrollment with id :: "+enrollment.getId(), e);
			throw new GIException("Error while updating monthly premiums for APTC Slider change of enrollment with id :: "+enrollment.getId(), e);
		}
	}
	
	//retro end date needs to be passed only if member level retro termination is there, in case of enrollment level retro its not needed
	//This method handles premium update for Enrollment level cancellation or termination or any retro termination for a terminated enrollment
	@Override
	public void updateMonthlyPremiumForCancelTerm(Enrollment enrollment, Date retroDate) throws GIException{
		try{
			boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
			if(populateMonthlyPremium && enrollment!=null && enrollment.getEnrollmentStatusLkp()!=null){
				Enrollee subscriber= enrollment.getSubscriberForEnrollment();
				if(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())){
					for (EnrollmentPremium prem: enrollment.getEnrollmentPremiums()){
						if(prem.getGrossPremiumAmount()!=null){
							resetMonthlyPremium(prem);
							prem.setLastEventId(subscriber.getLastEventId());
							//prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
						}
					}
				}else if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())&& retroDate==null){
					List<Enrollee> enrolleeList=getNonCancelledEnrolleesAndSubscriber(enrollment);

					for(EnrollmentPremium prem : enrollment.getEnrollmentPremiums()){
						Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(prem.getMonth()-1,prem.getYear()) ); //Month Start Date
						Date monthEndDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(prem.getMonth()-1,prem.getYear()) ); //Month End Date
						if(enrollment.getBenefitEndDate().after(monthStartDate) && enrollment.getBenefitEndDate().before(monthEndDate)){
							setMonthlyPremiumAmounts(prem, enrolleeList, monthStartDate, monthEndDate, enrollment);
							prem.setLastEventId(subscriber.getLastEventId());
							//prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
						}else if(monthStartDate.after(enrollment.getBenefitEndDate())){
							resetMonthlyPremium(prem);
							prem.setLastEventId(subscriber.getLastEventId());
							//prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
						}

					}


				}else if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())&& retroDate!=null){
					List<Enrollee> enrolleeList=getNonCancelledEnrolleesAndSubscriber(enrollment);
					for(EnrollmentPremium prem : enrollment.getEnrollmentPremiums()){
						Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(prem.getMonth()-1,prem.getYear()) ); //Month Start Date
						Date monthEndDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(prem.getMonth()-1,prem.getYear()) ); //Month End Date
						if(monthEndDate.after(retroDate)&& monthStartDate.before(enrollment.getBenefitEndDate())){
							setMonthlyPremiumAmounts(prem, enrolleeList, monthStartDate, monthEndDate, enrollment);
								prem.setLastEventId(subscriber.getLastEventId());
								//prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
						}else if(monthStartDate.after(enrollment.getBenefitEndDate()) && prem.getGrossPremiumAmount()!=null){
							resetMonthlyPremium(prem);
							prem.setLastEventId(subscriber.getLastEventId());
							//prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
						}


					}

				}
			}
		}catch(Exception e){
			LOGGER.error("Error while updating monthly premiums for Disenrollment of enrollment with id :: "+enrollment.getId(), e);
			throw new GIException("Error while updating monthly premiums forDisenrollment of enrollment with id :: "+enrollment.getId(), e);
		}

	}
	
	@Override
	public void updatePremiumAfterMonthlyAPTCChange(Enrollment enrollment, boolean isAdd, boolean isMonthlyAptcChange, boolean isMonthlyStateSubsidyChange) throws GIException{
		try{
			boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));

			if(populateMonthlyPremium && enrollment!=null){
				List<Enrollee> enrollees=getNonCancelledEnrolleesAndSubscriber(enrollment);
				Enrollee subscriber= enrollment.getSubscriberForEnrollment();
				if(enrollment.getEnrollmentPremiums()!=null && enrollment.getEnrollmentPremiums().size()>0){
					for (EnrollmentPremium prem: enrollment.getEnrollmentPremiums()){
						Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(prem.getMonth()-1,prem.getYear()) ); //Month Start Date
						Date monthEndDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(prem.getMonth()-1,prem.getYear()) ); //Month End Date
						if(null != prem.getGrossPremiumAmount()){
							calculatePremiumAndSubsidies(prem, enrollment, enrollees, monthStartDate, monthEndDate, isMonthlyAptcChange, isMonthlyStateSubsidyChange);
						}else{
							prem.setAptcAmount(null);
							prem.setNetPremiumAmount(null);
							prem.setMaxAptc(null);
							prem.setMaxStateSubsidy(null);
						}
						prem.setLastEventId(subscriber.getLastEventId());
						if(isAdd && enrollment.getBenefitEndDate().after(monthStartDate) && enrollment.getBenefitEffectiveDate().before(monthEndDate)){
							if(isMonthlyAptcChange) {
								prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
							}
							if(isMonthlyStateSubsidyChange){
								prem.setStateSubsidyChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
							}
						}
					}
					updateLastSliceAmountAndDate(enrollment, getNonCancelledEnrolleesAndSubscriber(enrollment), true);
				}
			}
		}catch(Exception e){
			LOGGER.error("Error while updating monthly premiums for Monthly APTC update of enrollment with id :: "+enrollment.getId(), e);
			throw new GIException("Error while updating monthly premiums for Monthly APTC update of enrollment with id :: "+enrollment.getId(), e);
		}
	}
	
	/**
	 * Method to generate Missing Actual Gross Premium and APTC value 
	 * @param enrollmentPremiumList
	 * @param enrolledMemberList
	 * @param enrollment
	 */
	private void generateActualGrossAndAptc(List<EnrollmentPremium> enrollmentPremiumList, final List<Enrollee> enrolledMemberList, Enrollment enrollment) throws Exception  {
		for(EnrollmentPremium premium: enrollmentPremiumList){
			if(premium.getGrossPremiumAmount() != null && premium.getActGrossPremiumAmount() == null){
				try{
				//Generate Actual Gross premium
					Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(premium.getMonth()-1,premium.getYear()) ); //Month Start Date
					Date monthEndDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(premium.getMonth()-1,premium.getYear()) ); //Month End Date
					float actualGrossPremium = 0.0f;
					for(Enrollee enrollee : enrolledMemberList){
						if (enrollee.getTotalIndvResponsibilityAmt() != null &&
								(enrollee.getEffectiveStartDate().before(monthEndDate) && (enrollee.getEffectiveEndDate() == null || enrollee.getEffectiveEndDate().after(monthStartDate) ))) {
							actualGrossPremium += enrollee.getTotalIndvResponsibilityAmt();
						}
					}
					premium.setActGrossPremiumAmount(actualGrossPremium);
			
				}catch(Exception e){
						LOGGER.error("Error while updating Missing Actual Gross and APTC  premiums for enrollment with id :: "+enrollment.getId()+ " for premium Month :: "+premium.getMonth()+  " :: "+ e.getMessage(),e);
						throw new GIException("Error while updating Missing Actual Gross and APTC  premiums for enrollment with id :: "+enrollment.getId()+ " for premium Month :: "+premium.getMonth()+  " :: "+ e.getMessage(),e);
				}
			
			}
//			if(premium.getAptcAmount() != null && premium.getActAptcAmount() == null){
//				//TODO generate Actual gross premium Amount
//			}
		}
	}
	private boolean isEditTool(Enrollee subscriber){
		boolean editTool=false;
		if(subscriber!=null &&subscriber.getLastEventId()!=null && subscriber.getLastEventId().getTxnIdentifier()!=null){
			String transIdentifier=subscriber.getLastEventId().getTxnIdentifier().toString();
			if(transIdentifier.contains("EDIT_TOOL")||transIdentifier.contains("EDITTOOL")){
				editTool=true;
			}
		}
		
		return editTool;
	}
	
	private Float prorateAPTCAmount(Date coverageStartDate, Date coverageEndDate,Date monthStartDate, Date monthEndDate, Float aptcAmount, EnrollmentPremium prem) throws GIException {
		boolean prorateAPTC= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.APTC_PRORATION));
		Float proratedAPTC=aptcAmount;
		if(aptcAmount!=null  && prorateAPTC && prem!=null ){
			proratedAPTC=EnrollmentUtils.roundFloat(calculateProratedAmount(coverageStartDate, coverageEndDate, monthStartDate, monthEndDate, aptcAmount), 2);
			if(!EnrollmentUtils.isFloatEqual(aptcAmount, proratedAPTC)){
				prem.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
			}
		}
		return proratedAPTC;
	}

	private BigDecimal prorateStateSubsidyAmount(Date coverageStartDate, Date coverageEndDate,Date monthStartDate, Date monthEndDate, BigDecimal stateSubsidyAmount, Float aptc, BigDecimal premium, BigDecimal minimum, EnrollmentPremium prem) throws GIException {
		boolean prorateAPTC= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.APTC_PRORATION));
		BigDecimal proratedStateSubsidy=stateSubsidyAmount;
		if(stateSubsidyAmount!=null  && prorateAPTC && prem!=null ){
			proratedStateSubsidy=BigDecimal.valueOf(EnrollmentUtils.roundFloat(calculateProratedAmount(coverageStartDate, coverageEndDate, monthStartDate, monthEndDate, stateSubsidyAmount.floatValue()), 2));
			if(!EnrollmentUtils.isFloatEqual(stateSubsidyAmount.floatValue(), proratedStateSubsidy)){
				prem.setStateSubsidyChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.PRORATION.toString());
			}
		}

		/*
		Max Prorated State Subsidy = Prorated Premium - (Prorated Max APTC - Minimum)
		Prorated Premium is already passed in before calculatePremiumAndSubsidies
		 */
		BigDecimal proratedAptc = BigDecimal.valueOf(EnrollmentUtils.roundFloat(calculateProratedAmount(coverageStartDate, coverageEndDate, monthStartDate, monthEndDate, aptc), 2));
		BigDecimal premiumMinusAptc = premium.subtract(proratedAptc);
		BigDecimal proratedMax = premiumMinusAptc.subtract(minimum);

		return proratedStateSubsidy.min(proratedMax);
	}

	/**
	 * Calculates premium and subsidies for the given month
	 * @param enrollmentPremium
	 * @param enrollment
	 * @param enrollees
	 * @param monthStartDate
	 * @param monthEndDate
	 * @param isMonthlyAptcChange
	 * @throws GIException
	 */
	private void calculatePremiumAndSubsidies(EnrollmentPremium enrollmentPremium, Enrollment enrollment, List<Enrollee> enrollees, Date monthStartDate, Date monthEndDate, boolean isMonthlyAptcChange, boolean isMonthlyStateSubsidyChange) throws GIException {
		BigDecimal aptc = null;
		BigDecimal premiumBeforeCredit = BigDecimal.valueOf(enrollmentPremium.getGrossPremiumAmount()).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal premiumAfterCredit = premiumBeforeCredit;
		BigDecimal stateSubsidy = null;

		BigDecimal minPremiumPerHousehold = BigDecimal.ZERO;
		BigDecimal ehbPercentage = null;
		BigDecimal applicablePremium = BigDecimal.ZERO;

		boolean applyOneDollarRule= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.APPLY_ONE_DOLLAR_RULE));

		if(applyOneDollarRule  && enrollment.getInsuranceTypeLkp()!=null && EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())){
			int activeEnrolleeCount = calculateActiveMemberCount(enrollees, EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(enrollmentPremium.getMonth()-1,enrollmentPremium.getYear())), EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(enrollmentPremium.getMonth()-1,enrollmentPremium.getYear()) ) );
			String minPremiumPerMember = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.MIN_PREMIUM_PER_MEMBER);
			minPremiumPerHousehold = BigDecimal.valueOf(activeEnrolleeCount).multiply(new BigDecimal(minPremiumPerMember));
		}

		if(enrollment.getAptcEffDate() != null){
			Float proratedAPTC;
			if(isMonthlyAptcChange) {
				//HIX-115724 Get APTC from enrollment premium row and not enrollment in case of edit tool over-ride
				proratedAPTC = enrollmentPremium.getAptcAmount();
				enrollmentPremium.setActAptcAmount(enrollmentPremium.getAptcAmount());
			} else if (enrollment.getAptcEffDate().before(monthEndDate) && (enrollment.getBenefitEndDate() == null || enrollment.getBenefitEndDate().after(monthStartDate))) {
				proratedAPTC = prorateAPTCAmount(enrollment.getBenefitEffectiveDate(), enrollment.getBenefitEndDate(), monthStartDate, monthEndDate, enrollment.getAptcAmt(), enrollmentPremium);
				enrollmentPremium.setActAptcAmount(enrollment.getAptcAmt());
			} else {
				proratedAPTC = prorateAPTCAmount(enrollment.getBenefitEffectiveDate(), enrollment.getBenefitEndDate(), monthStartDate, monthEndDate, enrollmentPremium.getActAptcAmount(), enrollmentPremium);
			}

			enrollmentPremium.setAptcAmount(proratedAPTC!=null ?(Math.min(proratedAPTC, (enrollment.getEhbPercent() !=null ? (enrollmentPremium.getGrossPremiumAmount()* enrollment.getEhbPercent()):enrollmentPremium.getGrossPremiumAmount()))):null);
			if(enrollmentPremium.getAptcAmount() != null) {
				aptc = BigDecimal.valueOf(enrollmentPremium.getAptcAmount()).setScale(2, BigDecimal.ROUND_HALF_UP);
			}
		}

		if(EnrollmentUtils.isNotNullAndEmpty(enrollment.getEhbPercent())) {
			ehbPercentage = new BigDecimal(enrollment.getEhbPercent());
		}
		if(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())) {
			BigDecimal defaultEHB = BigDecimal.valueOf(1.0);

			ehbPercentage = ehbPercentage == null ? defaultEHB  : ehbPercentage;
			applicablePremium = premiumBeforeCredit.multiply(ehbPercentage);

		}
		else {
			ehbPercentage = ehbPercentage == null ? BigDecimal.valueOf(1.0) : ehbPercentage;
			applicablePremium = premiumBeforeCredit.multiply(ehbPercentage);
		}

		BigDecimal ehbMinimum = premiumBeforeCredit.subtract(applicablePremium);

		BigDecimal actualMinimum = minPremiumPerHousehold.max(ehbMinimum);

		if(enrollment.getStateSubsidyEffDate() != null) {
			if(isMonthlyStateSubsidyChange) {
				stateSubsidy = enrollmentPremium.getStateSubsidyAmount();

				//State Subsidy might be null on months where current month is not within State Subsidy Effective date
				if(stateSubsidy != null) {
					//Applicable State Subsidy is dependent on Premium - (Max APTC - Minimum), regardless of elected APTC
					BigDecimal maxAptc = BigDecimal.ZERO;

					if (enrollmentPremium.getMaxAptc() != null) {
						maxAptc = new BigDecimal(enrollmentPremium.getMaxAptc());
					}
					BigDecimal maxApplicableStateSubsidy = premiumBeforeCredit.subtract(maxAptc.subtract(actualMinimum).abs()).abs();

					if (stateSubsidy.compareTo(maxApplicableStateSubsidy) > 0) {
						stateSubsidy = maxApplicableStateSubsidy;
					}
				}

				enrollmentPremium.setActStateSubsidyAmount(stateSubsidy);
			}
			else if (enrollment.getStateSubsidyEffDate().before(monthEndDate) && (enrollment.getBenefitEndDate() == null || enrollment.getBenefitEndDate().after(monthStartDate))) {
				stateSubsidy = enrollment.getStateSubsidyAmt();

				enrollmentPremium.setActStateSubsidyAmount(stateSubsidy);
				enrollmentPremium.setStateSubsidyAmount(stateSubsidy);
			}
			else{
				stateSubsidy = enrollmentPremium.getActStateSubsidyAmount();
			}
		}

		if(aptc != null && aptc.compareTo(BigDecimal.ZERO) > 0) {
			BigDecimal appliedAptc = aptc;

			if (premiumBeforeCredit.subtract(appliedAptc).compareTo(actualMinimum) < 0) {
				appliedAptc = premiumBeforeCredit.subtract(actualMinimum).setScale(2, BigDecimal.ROUND_HALF_UP);
				premiumAfterCredit = actualMinimum;
			} else {
				premiumAfterCredit = premiumBeforeCredit.subtract(appliedAptc).setScale(2, BigDecimal.ROUND_HALF_UP);
			}

			enrollmentPremium.setAptcAmount(appliedAptc.floatValue());
		}

		if(stateSubsidy != null && stateSubsidy.compareTo(BigDecimal.ZERO) > 0){
			BigDecimal proratedSubsidy;

			if(isMonthlyStateSubsidyChange){
				proratedSubsidy = enrollmentPremium.getActStateSubsidyAmount();
			}
			else {
				proratedSubsidy = prorateStateSubsidyAmount(enrollment.getBenefitEffectiveDate(), enrollment.getBenefitEndDate(), monthStartDate, monthEndDate, stateSubsidy, enrollmentPremium.getMaxAptc(), premiumBeforeCredit, actualMinimum, enrollmentPremium);
			}

			BigDecimal appliedStateSubsidy = stateSubsidy.min(proratedSubsidy);

			if(premiumAfterCredit.compareTo(actualMinimum) > 0) {
				if (premiumAfterCredit.subtract(appliedStateSubsidy).compareTo(actualMinimum) < 0) {
					appliedStateSubsidy = premiumAfterCredit.subtract(actualMinimum).setScale(2,BigDecimal.ROUND_HALF_UP);
					premiumAfterCredit = actualMinimum;
				}
				else {
					premiumAfterCredit = premiumAfterCredit.subtract(appliedStateSubsidy).setScale(2,BigDecimal.ROUND_HALF_UP);
				}

				enrollmentPremium.setStateSubsidyAmount(appliedStateSubsidy.setScale(2, BigDecimal.ROUND_HALF_UP));
				//enrollmentPremium.setActStateSubsidyAmount(appliedStateSubsidy.setScale(2, BigDecimal.ROUND_HALF_UP));
			} else{
				enrollmentPremium.setStateSubsidyAmount(new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP));
				enrollmentPremium.setActStateSubsidyAmount(new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
		}

		enrollmentPremium.setNetPremiumAmount(premiumAfterCredit.floatValue());
	}
	
}
