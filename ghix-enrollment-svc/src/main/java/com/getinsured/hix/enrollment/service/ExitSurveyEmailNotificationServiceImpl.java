package com.getinsured.hix.enrollment.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.email.ExitSurveyEmailNotification;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.notification.ConfigurationService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

@Service("exitSurveyEmailNotificationService")
public class ExitSurveyEmailNotificationServiceImpl implements
ExitSurveyEmailNotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExitSurveyEmailNotificationServiceImpl.class);
	@Autowired private ExitSurveyEmailNotification emailNotification;
	@Autowired private ConfigurationService configurationService;
	
	private static final String PHONE_NUM = "phoneNum";
	private static final String LOGO_URL = "logoUrl";
	private static final String REDIRECT_URL = "redirectUrl";
	private static final String DISCLAIMER_CONTENT = "disclaimerContent";
	
	@Override
	public void sendEmailNotification(Household household) throws NotificationTypeNotFound {
		Notice noticeObj =null;
		
		if(household == null) {
			return;
		}
		String phoneNum = null;
		String logoUrl = null;
		String redirectUrl = null;
		Long tenantId = null;
		Integer flowId = null;
		Long affiliateId = null;
		String disclaimerContent = null;
		
		EligLead eligLead = household.getEligLead();
		tenantId = household.getTenantId();

		if(null != eligLead){
			flowId = eligLead.getFlowId();
			affiliateId = eligLead.getAffiliateId();
		}else{
			LOGGER.error("ExitSurveyEmailNotification-SendEmailNotification setCommonData: eligLead is null for HOUSEHOLD_ID = " + household.getId());
		}
		
		if(null != flowId && 0 == flowId){
			flowId = eligLead.getFlowId() > 0 ? eligLead.getFlowId() : null;
			LOGGER.error("ExitSurveyEmailNotification-SendEmailNotification setCommonData: Flow ID is null for CMR_HOUSEHOLD_ID = " + household.getId());
		}
		if(null != affiliateId && 0 == affiliateId){
			affiliateId = eligLead.getAffiliateId() > 0 ? eligLead.getAffiliateId() : null;
			LOGGER.error("ExitSurveyEmailNotification-SendEmailNotification setCommonData: Affiliate ID is null for CMR_HOUSEHOLD_ID = " + household.getId());
		}
		
		phoneNum = configurationService.customerCareNum(flowId, affiliateId, tenantId);
		logoUrl = configurationService.logoUrl(flowId, affiliateId, tenantId);
		redirectUrl = configurationService.baseUrl(flowId, affiliateId, tenantId);
		disclaimerContent = configurationService.getDisclaimerTextForEmailFooter(flowId, affiliateId, tenantId);
		
		if(null != phoneNum && phoneNum.length() == 10){
			String phoneFormat = "(%s) %s-%s";
			phoneNum = String.format(phoneFormat, phoneNum.substring(0, 3), phoneNum.substring(3, 6), phoneNum.substring(6, 10));
		}
		
		emailNotification.setEmailAddress(household.getEmail());
		emailNotification.setFirstName(household.getFirstName());
		//Set Tenant Specific data before sending email.
		Map<String, String>  requestMap = new HashMap<String, String>();
		
		requestMap.put(PHONE_NUM, phoneNum);
		requestMap.put(LOGO_URL, logoUrl);
		requestMap.put(REDIRECT_URL, redirectUrl);
		requestMap.put(DISCLAIMER_CONTENT, disclaimerContent);
		emailNotification.setRequestMap(requestMap);
		noticeObj=emailNotification.generateEmail();
		emailNotification.sendEmail(noticeObj);
		
		LOGGER.info("Email Process :: End");
	}
}
