package com.getinsured.hix.enrollment.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.enrollment.ExtractionJobStatus;
public interface IExtractionJobStatusRepository extends JpaRepository<ExtractionJobStatus, Integer> {
	
}
