package com.getinsured.hix.enrollment.service;

public interface EnrollmentPartnerNotificationService {
	void publishPartnerEvent(Integer enrollmentId, Integer householdId);
	/*Household getHousehold(Integer householdId);*/
}
