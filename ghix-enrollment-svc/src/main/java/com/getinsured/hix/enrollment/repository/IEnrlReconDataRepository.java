package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.EnrlReconData;

public interface IEnrlReconDataRepository extends JpaRepository<EnrlReconData, Integer> {
	
	
	/*@Query("SELECT DISTINCT erd.issuerAssignedPolicyId FROM EnrlReconData erd " +
			"WHERE erd.fileId = :fileId AND erd.reconGenStatus is NULL")
	List<Long> getIssuerPolicyIdsByFileId(@Param("fileId") Integer  fileId);
	
	@Query("SELECT DISTINCT erd.issuerAssignedPolicyId FROM EnrlReconData erd " +
			"WHERE erd.fileId IN (:fileIds )")
	List<Long> getIssuerPolicyIdsByFileIds(@Param("fileIds") List<Integer>  fileIds);
	
	@Query("FROM EnrlReconData erd " +
			"WHERE erd.issuerAssignedPolicyId = :issuerPolicyId and erd.fileId = :fileId ")
	List<EnrlReconData> getReconDataByIssuerPolicyId(@Param("issuerPolicyId") Long  issuerPolicyId, @Param("fileId") Integer  fileId);
	
	@Modifying
	@Transactional
    @Query(" UPDATE EnrlReconData SET reconGenStatus = 'SUCCESS' "+
           " WHERE issuerAssignedPolicyId = :issuerPolicyId and fileId = :fileId ")
	void updateReconDataToSuccess(@Param("issuerPolicyId") Long  issuerPolicyId, @Param("fileId") Integer  fileId);
	
	@Modifying
	@Transactional
    @Query(" UPDATE EnrlReconData SET reconGenStatus = 'FAILURE', reconErrorMessage= :errorMsg "+
           " WHERE issuerAssignedPolicyId =:issuerPolicyId and fileId = :fileId ")
	void updateReconDataToFailure(@Param("issuerPolicyId") Long  issuerPolicyId, @Param("errorMsg") String errorMsg, @Param("fileId") Integer  fileId);*/
	
	@Query(" SELECT COUNT(*) FROM EnrlReconData erd "
		  + "WHERE erd.fileId = :fileId "
		  + "AND erd.subscriberIndicator = 'Y' ")
	Integer getTotalSubscriberCount(@Param("fileId") Integer  fileId);
	
	@Query(" SELECT COUNT(*) FROM EnrlReconData erd "
			  + "WHERE erd.fileId = :fileId "
			  + "AND erd.subscriberIndicator = 'N' ")
	Integer getTotalDependentCount(@Param("fileId") Integer  fileId);
	
	@Query(" SELECT COUNT( DISTINCT erd.enrollmentId ) "
			+" FROM EnrlReconData erd "
			+ "WHERE erd.fileId = :fileId ")
	Integer getTotalEnrlCount(@Param("fileId") Integer  fileId);
	
	@Query("SELECT count( DISTINCT erd.issuerAssignedPolicyId) FROM EnrlReconData erd WHERE erd.fileId = :fileId AND erd.enrollmentId IN (SELECT e.id FROM Enrollment e) ")
	Integer countEnrollmentsInHix( @Param("fileId") Integer  fileId);
	
	@Query("SELECT  DISTINCT TO_CHAR(erd.benefitStartDate, 'YYYY') FROM EnrlReconData erd  WHERE erd.fileId IN (:fileIds)")
	List<String> getCoverageYearByFileIds(@Param("fileIds") List<Integer>  fileIds);
	
	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENRL_RECON_DATA WHERE ID IN (:id) ")
	void deleteEnrlReconData(@Param("id") List<Integer>  id);
	
	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENRL_RECON_DATA WHERE TO_DATE(TO_CHAR( CREATION_TIMESTAMP,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:cutOffDate,'MM/DD/YYYY') ")
	void deleteEnrlReconDataByCutOffDate(@Param("cutOffDate") String  cutOffDate);
	
	@Query("SELECT DISTINCT erd.enrollmentId FROM EnrlReconData erd " +
			"WHERE erd.fileId = :fileId AND erd.reconGenStatus is NULL")
	List<Long> getEnrollmentIdsByFileId(@Param("fileId") Integer  fileId);
	
	@Query("SELECT DISTINCT erd.enrollmentId FROM EnrlReconData erd " +
			"WHERE erd.fileId IN (:fileIds )")
	List<Long> getEnrollmentIdsByFileIds(@Param("fileIds") List<Integer>  fileIds);
	
	@Query("FROM EnrlReconData erd " +
			"WHERE erd.enrollmentId = :enrollmentId and erd.fileId = :fileId ")
	List<EnrlReconData> getReconDataByEnrollmentId(@Param("enrollmentId") Long  enrollmentId, @Param("fileId") Integer  fileId);
	
	@Modifying
	@Transactional
    @Query(" UPDATE EnrlReconData SET reconGenStatus = 'SUCCESS' "+
           " WHERE enrollmentId = :enrollmentId and fileId = :fileId ")
	void updateReconDataToSuccess(@Param("enrollmentId") Long  enrollmentId, @Param("fileId") Integer  fileId);
	
	@Modifying
	@Transactional
    @Query(" UPDATE EnrlReconData SET reconGenStatus = 'FAILURE', reconErrorMessage= :errorMsg "+
           " WHERE enrollmentId =:enrollmentId and fileId = :fileId ")
	void updateReconDataToFailure(@Param("enrollmentId") Long  enrollmentId, @Param("errorMsg") String errorMsg, @Param("fileId") Integer  fileId);
	
	@Query(" SELECT DISTINCT e.id "
			+" FROM Enrollment e "
			+ "WHERE e.createdOn < :cutOffDate AND TO_CHAR(e.benefitEffectiveDate, 'YYYY') = :coverageYear  AND e.hiosIssuerId = :hiosIssuerId  "
			+ "AND e.enrollmentStatusLkp.lookupValueCode NOT IN  ('ABORTED') "
			+ "AND e.id NOT IN (SELECT DISTINCT ers.enrollmentId  FROM EnrlReconData ers WHERE ers.fileId = :fileId AND ers.enrollmentId IS NOT NULL ) "
			+ "AND e.id NOT IN (SELECT DISTINCT err.enrollmentId FROM EnrlReconError err where err.fileId = :fileId AND err.enrollmentId IS NOT NULL) ")
	List<Integer> getEnrollmentsMissingInFile(@Param("fileId") Integer fileId, @Param("cutOffDate") Date cutOffDate,  @Param("hiosIssuerId") String hiosIssuerId, @Param("coverageYear") String coverageYear);
}
