package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.dto.enrollment.DiscrepancyActionRequestDTO;
import com.getinsured.hix.dto.enrollment.DiscrepancyEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrlReconciliationResponse;
import com.getinsured.hix.dto.enrollment.EnrollmentReconRequest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.exception.GIException;

public interface EnrlReconDiscrepancyService {

	/**
	 * 
	 * @param enrollmentReconRequest
	 * @param enrlReconciliationResponse
	 * @return
	 */
	public EnrlReconciliationResponse getEnrlReconDiscrepancyDetail(EnrollmentReconRequest enrollmentReconRequest, EnrlReconciliationResponse enrlReconciliationResponse);
	
	/**
	 * 
	 * @param enrollmentReconRequest
	 * @param enrlReconciliationResponse
	 * @return
	 */
	public EnrlReconciliationResponse getEnrlReconCommentDetail(EnrollmentReconRequest enrollmentReconRequest, EnrlReconciliationResponse enrlReconciliationResponse);
	
	/**
	 * 
	 * @param discrepancyActionRequestDTO
	 */
	public void addEditCommentOrSnoozeDiscrepancy(DiscrepancyActionRequestDTO discrepancyActionRequestDTO, AccountUser accountUser) throws GIException;
	
	
	/**
	 * 
	 * @param enrollmentId
	 * @return
	 * @throws GIException
	 */
	public DiscrepancyEnrollmentDTO getEnrlReconVisualSummaryData(Integer enrollmentId)throws GIException;
	
}
