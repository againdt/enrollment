package com.getinsured.hix.enrollment.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;

/**
 * @since 07th August 2015
 * @author Sharma_k
 * Util class to populate EventInfoDto for capturing Enrollment Application Event.
 * @see com.getinsured.hix.platform.eventlog.service.AppEventServiceImpl
 */
@Component
public class EnrollmentAppEventLogUtil 
{
	@Autowired private AppEventService appEventService;
	@Autowired private LookupService lookupService;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAppEventLogUtil.class);
	
	/**
	 * Method recordApplicationEnrollmentEvent to save Enrollment Events 
	 * @param enrollmentAppEventDataDTO Object<EnrollmentAppEventDataDTO>
	 * @param appEnrollmentEventLkpValueCode String
	 */
	public void recordApplicationEnrollmentEvent(final Enrollment enrollment, final String appEnrollmentEventLkpValueCode, AccountUser loggedInUser){
		prepareAndSetApplicationEvent(enrollment, appEnrollmentEventLkpValueCode, null, null, loggedInUser);
	}
	
	
	public void recordApplicationEnrollmentEvent(final Enrollment enrollment, final String appEnrollmentEventLkpValueCode, final Map<String, String> requestParam, AccountUser loggedInUser){
		prepareAndSetApplicationEvent(enrollment, appEnrollmentEventLkpValueCode, requestParam, null, loggedInUser);
	}
	
	public void recordApplicationEnrollmentEvent(final Enrollment enrollment, final String appEnrollmentEventLkpValueCode, final Map<String, String> requestParam, final String comment, AccountUser loggedInUser){
		prepareAndSetApplicationEvent(enrollment, appEnrollmentEventLkpValueCode, requestParam, comment, loggedInUser);
	}
	
	private void prepareAndSetApplicationEvent(Enrollment enrollment, String appEnrollmentEventLkpValueCode, Map<String, String> requestParam, String comment, AccountUser loggedInUser){
		try{
			LOGGER.info("ENROLLMENT_APP_EVENT:: Received event: "+appEnrollmentEventLkpValueCode);
			Map<String, String> mapParameters = null;
			if(enrollment != null && StringUtils.isNotEmpty(appEnrollmentEventLkpValueCode)){
				if(enrollment.getEnrollmentTypeLkp()!=null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
					if(StringUtils.isNotEmpty(enrollment.getHouseHoldCaseId())){
						EventInfoDto eventInfoDto = new EventInfoDto();
						mapParameters = populateEnrollmentMapData(enrollment, requestParam);
						/*
						 * Application event is logged in for Individual Account so passing CMR HouseHold Case Id
						 * as for off Exchange flow no individual account will be present.
						 */
						eventInfoDto.setModuleId(Integer.parseInt(enrollment.getHouseHoldCaseId()));
						eventInfoDto.setModuleName("INDIVIDUAL");//HardCoded as no Enum found for this role found
						if(StringUtils.isNotEmpty(comment)){
							eventInfoDto.setComments(comment);
						}
						LookupValue lookupValue = lookupService
								.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.APP_ENROLLMENT_EVENT,
										appEnrollmentEventLkpValueCode);
						if(lookupValue != null) {
							eventInfoDto.setEventLookupValue(lookupValue);
							LOGGER.info("ENROLLMENT_APP_EVENT:: Prepared EventInfoDTO : "+eventInfoDto.toString()+ " lookupValue: "+lookupValue.getLookupValueCode());
							LOGGER.info("ENROLLMENT_APP_EVENT:: Map Parameters: "+mapParameters);
							appEventService.record(eventInfoDto, mapParameters, loggedInUser);
						}else {
							LOGGER.info("NO Lookup Found for Lookup_value_code :"+ appEnrollmentEventLkpValueCode);
						}
					}else{
						LOGGER.warn("ENROLLMENT_APP_EVENT:: No CMR HouseHold ID found for EnrollmentId: "+enrollment.getId());
					}
				}else{
					LOGGER.info("ENROLLMENT_APP_EVENT:: Enrollment Application event "+appEnrollmentEventLkpValueCode+"  Received for EnrollmentType SHOP");
				}
			}else{
				LOGGER.warn("ENROLLMENT_APP_EVENT:: Invalid or empty request received for AppEventLog");
			}
		}
		catch(Exception ex){
			/**
			 * Suppressing Exception, to avoid existing service layer abrupt
			 */
			LOGGER.error("ENROLLMENT_APP_EVENT:: Exception caught in recordApplicationEnrollmentEvent(-) method::, EnrollmentId: "+(enrollment != null ? enrollment.getId() : null) +" Application Event: "+appEnrollmentEventLkpValueCode,ex);
		}
	}
	
	/**
	 * 
	 * @param enrollment
	 * @return
	 */
	private Map<String, String> populateEnrollmentMapData(final Enrollment enrollment, Map<String, String> requestParam)
	{
		Map<String, String> responseDataMap = new HashMap<String, String>();
		if(enrollment != null)
		{	
			if(enrollment.getId() != null){
				responseDataMap.put("Enrollment Id", Integer.toString(enrollment.getId()));
			}
			if(enrollment.getEnrollmentStatusLkp() != null){
				responseDataMap.put("Enrollment Status", enrollment.getEnrollmentStatusLkp().getLookupValueCode());
			}
			if(StringUtils.isNotEmpty(enrollment.getInsurerName())){
				responseDataMap.put("Insurer Name", enrollment.getInsurerName());
			}
			if(StringUtils.isNotEmpty(enrollment.getPlanName())){
				responseDataMap.put("Plan Name", enrollment.getPlanName());
			}
			if(StringUtils.isNotEmpty(enrollment.getFileName834())){
				responseDataMap.put("File Name", enrollment.getFileName834());
			}
			Enrollee subscriber = enrollment.getSubscriberForEnrollment();
			if(subscriber !=null && subscriber.getLastEventId()!=null){
				responseDataMap.put("Event Id", ""+subscriber.getLastEventId().getId());
			}
			
		}
		if(requestParam != null && !requestParam.isEmpty()){
			responseDataMap.putAll(requestParam);
		}
		return responseDataMap;
	}
}
