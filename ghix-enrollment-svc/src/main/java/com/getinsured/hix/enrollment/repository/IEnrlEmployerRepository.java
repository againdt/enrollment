package com.getinsured.hix.enrollment.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Employer;

public interface IEnrlEmployerRepository extends JpaRepository<Employer, Integer> {
	
/*	@Query("SELECT coverageDateStart FROM EmployerEnrollment employerEnrollment where employer.id = :employerId AND status IN ('ACTIVE','PENDING')")
	Date getCoverageStartDateByEmployerId(@Param("employerId") int employerId);
	
	@Query("SELECT coverageDateStart FROM EmployerEnrollment employerEnrollment where employer.id = :employerId AND status IN ('TERMINATED') AND ROWNUM = 1 ORDER BY updated DESC")
	Date getTermCoverageStartDateByEmployerId(@Param("employerId") int employerId);*/
	
}
