/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.email.CmsSftpAlertNotification;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
@Service("cmsSftpNotificationService")
public class CmsSftpNotificationServiceImpl implements CmsSftpNotificationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CmsSftpNotificationServiceImpl.class);
	
	@Autowired private CmsSftpAlertNotification cmsSftpAlertNotification;

	@Override
	public void sendYhiAlertEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("CmsSftpNotificationService :: YHI alert email sending process");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EMAIL_GRP_IRS_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("CmsSftpNotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("CmsSftpNotificationService:: Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			emailData.put("Subject",requestMap.get("Subject"));
			cmsSftpAlertNotification.setEmailData(emailData);
			cmsSftpAlertNotification.setRequestData(requestMap);
			Notice noticeObj = cmsSftpAlertNotification.generateEmail();
			Notification notificationObj = cmsSftpAlertNotification.generateNotification(noticeObj);
			LOGGER.info("Notice body :: "+noticeObj.getEmailBody());
			cmsSftpAlertNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex){
			throw new GIException(ex);
		}
	}

}
