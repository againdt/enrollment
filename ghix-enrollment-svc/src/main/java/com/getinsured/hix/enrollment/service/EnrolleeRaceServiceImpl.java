/**
 * 
 */
package com.getinsured.hix.enrollment.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrolleeRaceRepository;
import com.getinsured.hix.model.enrollment.EnrolleeRace;;


/**
 * @author panda_p
 *
 */
@Service("enrolleeRaceService")
@Transactional
public class EnrolleeRaceServiceImpl implements EnrolleeRaceService {

	@Autowired private IEnrolleeRaceRepository enrolleeRaceRepository;
	
	@Override
	public void deleteEnrolleeRace(List<EnrolleeRace> enrolleeRaces){
		//enrolleeRaceRepository.deleteByEnrollee(enrollee);
				if(enrolleeRaces!=null && !enrolleeRaces.isEmpty()){
					enrolleeRaceRepository.deleteInBatch(enrolleeRaces);
				}
	}
	
	
}
