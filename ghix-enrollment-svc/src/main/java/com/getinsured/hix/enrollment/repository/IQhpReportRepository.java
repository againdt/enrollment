package com.getinsured.hix.enrollment.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.QhpReport;

/**
 * JpaRepository for manipulating QhpReport data.
 * 
 * @author Bhavin Parmar
 * @since May 27, 2019
 */
public interface IQhpReportRepository extends JpaRepository<QhpReport, Integer> {

	@Modifying
	@Transactional
	@Query("delete from QhpReport qr where qr.batchJobExecutionId not in(:batchJobExecutionId)")
	void deleteAllQhpRptByNotInBatchJobExecId(@Param("batchJobExecutionId") Integer batchJobExecutionId);

}
