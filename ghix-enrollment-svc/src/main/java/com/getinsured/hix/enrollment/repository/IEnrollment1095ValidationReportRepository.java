package com.getinsured.hix.enrollment.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.Enrollment1095ValidationReport;


public interface IEnrollment1095ValidationReportRepository extends JpaRepository<Enrollment1095ValidationReport, Integer>{
	
	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM ENRL_1095_VALIDATION_REPORT WHERE ENROLLMENT_1095_ID = :enrollment1095Id")
	void deleteValidationReport(@Param("enrollment1095Id") Integer enrollment1095Id);

}
