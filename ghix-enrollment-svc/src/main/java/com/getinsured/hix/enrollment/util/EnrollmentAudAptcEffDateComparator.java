/**
 * 
 */
package com.getinsured.hix.enrollment.util;

import java.util.Comparator;

import com.getinsured.hix.model.enrollment.EnrollmentAud;

/**
 * @author negi_s
 *
 */
public class EnrollmentAudAptcEffDateComparator implements Comparator<EnrollmentAud> {

	@Override
	public int compare(EnrollmentAud o1, EnrollmentAud o2) {
		int result = 0;
		if(null != o1.getAptcEffDate() && null != o2.getAptcEffDate()){
			result = o1.getAptcEffDate().compareTo(o2.getAptcEffDate());
		}
		return result;
	}

}
