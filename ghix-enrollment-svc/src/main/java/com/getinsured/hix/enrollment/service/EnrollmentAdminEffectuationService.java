package com.getinsured.hix.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

public interface EnrollmentAdminEffectuationService {
	boolean updateEnrollmentByCSV(Map<String,List<String[]>> effectuationMap, String fileName, String wipFolderDir)  throws GIException;
	void insertAdminEffactuation(List<String[]> effectuationList, String fileName, String processingStatus, String errorMessage );
}
