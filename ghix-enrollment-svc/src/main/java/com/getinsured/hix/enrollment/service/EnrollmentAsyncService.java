package com.getinsured.hix.enrollment.service;
/**
 * 
 * @author meher_a
 *
 */
public interface EnrollmentAsyncService {
	
	void enrollmentAsyncTerminatorTask(String cmsPlanId,String coverageYear, String terminationDate);

}
