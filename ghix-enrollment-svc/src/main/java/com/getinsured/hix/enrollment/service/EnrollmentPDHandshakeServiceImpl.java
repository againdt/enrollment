package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrollmentIndShopRequest;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemPersonDTO;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PdOrderResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.PldMemberData;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.model.plandisplay.PldPlanData;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlandisplayEndpoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.EnrollmentVal;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember.Enrollments;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Race;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Relationship;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.ResponsiblePerson;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Shop;
import com.getinsured.hix.webservice.plandisplay.individualinformation.YesNoVal;
import com.google.gson.Gson;

@Service("enrollmentPDHandshakeService")
@Transactional
public class EnrollmentPDHandshakeServiceImpl implements EnrollmentPDHandshakeService {
	@Autowired private GIWSPayloadService giWSPayloadService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private IEnrolleeRepository enrolleeRepository;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private Gson platformGson;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentPDHandshakeServiceImpl.class);
	@Override
	public PldOrderResponse findByPdHouseholdId( String pdHouseholdId,EnrollmentResponse enrlResponse,  PdOrderResponse pdResponse, AutoRenewalRequest autoRenewalReq, String enrollmentTypeStr ) throws GIRuntimeException
	{		
		// 1. Call Plan Display service to get plan selection data
		PdOrderResponse pdOrderResponse=null;
		PldOrderResponse pldOrderResponse=null;
		try{
			if(pdResponse==null){
				pdOrderResponse = findbyhouseholdidpd(pdHouseholdId);
			}else{
				pdOrderResponse=pdResponse;
			}
			if(pdOrderResponse == null){
				throw new GIException(EnrollmentConstants.PLANDISPLAY_ENROLLMENT_DATA_SERVICE_RETURNS_NULL + "pdHouseholdId : "+ pdHouseholdId);
			}
		}catch(Exception e){
			LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage() , e);
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause() , e);
			enrlResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlResponse.setErrMsg(EnrollmentConstants.ERR_MSG_FAILED_TO_CALL_PLANDISPLAY_ENROLLMENT_DATA_SERVICE + "pdHouseholdId : "+pdHouseholdId+"Error Stack : "+e.getStackTrace());
			enrlResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
			return pldOrderResponse;
		}
		
		// 2. Get ind19Request from GIWSPayload
		IndividualInformationRequest ind19Request = null;
        try {
        	ind19Request=getPayloadRequest(pdOrderResponse, autoRenewalReq, enrollmentTypeStr);
        } catch (Exception e) {
        	LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage() , e);
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause() , e);
			enrlResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlResponse.setErrMsg(EnrollmentConstants.ERR_MSG_FAILED_TO_GET_PAYLOAD + pdOrderResponse.getGiWsPayloadId()+"Error Stack : "+e.getStackTrace());
			enrlResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E003);
			return pldOrderResponse;
        } 
        

        String globalId= pdOrderResponse.getGlobalId();
		// 3. Merge plan selection data and ind19Request
		 pldOrderResponse = new PldOrderResponse();
		 pldOrderResponse.setGlobalId(globalId);
		 if(pdOrderResponse.getGiWsPayloadId()!=null){
			 pldOrderResponse.setGiWsPayloadId(Integer.valueOf(pdOrderResponse.getGiWsPayloadId().toString()));
		 }
		IndividualInformationRequest.Household household = ind19Request != null ? ind19Request.getHousehold(): null;
		if(household != null){
			Set<String> planIds= new HashSet<>();			
			pldOrderResponse.setApplicationId(household.getApplicationId());
			pldOrderResponse.setUserRoleId(household.getUserRoleId());
			Date financialEffectiveDate=null;
			if(household.getIndividual()!=null && isNotNullAndEmpty(household.getIndividual().getFinancialEffectiveDate())){
				financialEffectiveDate=DateUtil.StringToDate(household.getIndividual().getFinancialEffectiveDate(),"MM/dd/yyyy");
			}
			pldOrderResponse.setSadpFlag("NO");
			
			if(isNotNullAndEmpty(household.getAutoRenewal())){
				pldOrderResponse.setAutoRenewal(household.getAutoRenewal().toString().charAt(0));
			}
			
			if(isNotNullAndEmpty(household.getUserRoleType())){
				pldOrderResponse.setUserRoleType(household.getUserRoleType().toString());
			}
			
			/*if(isNotNullAndEmpty(household.getTerminationFlag())){
				pldOrderResponse.setTerminationFlag(household.getTerminationFlag().toString());
			}
			if(isNotNullAndEmpty(household.getDentalTerminationFlag())){
				pldOrderResponse.setDentalTerminationFlag(household.getDentalTerminationFlag().toString());
			}*/
			
			Map<String,String> houseHoldContact = getHouseholdContact(household.getHouseHoldContact());
			pldOrderResponse.setHouseHoldContact(houseHoldContact);
			
			Map<String,String> responsiblePerson = getResponsiblePerson(household.getResponsiblePerson());
			pldOrderResponse.setResponsiblePerson(responsiblePerson);
			
			if(household.getDisenrollMembers() != null){
				List<String> disEnrollMemberIds= new ArrayList<String>();
				List<PldMemberData> disenrollMemberInfoList = getDisenrollMemberList(household.getDisenrollMembers(), disEnrollMemberIds);
				if(!disEnrollMemberIds.isEmpty()){
					pldOrderResponse.setInd19Disenrolledmembers(disEnrollMemberIds);
				}
				pldOrderResponse.setDisenrollMemberInfoList(disenrollMemberInfoList);
			}
			pldOrderResponse.setApplicationType(ind19Request.getHousehold().getApplicationType());
			EnrollmentVal enrollmentType = null;
			String coverageStartDateStr = null;
			String marketType = null;
			if (household.getIndividual() != null) {
				setHouseholdForIndividual(household.getIndividual(), pldOrderResponse);
				enrollmentType = household.getIndividual().getEnrollmentType();
				coverageStartDateStr = household.getIndividual().getCoverageStartDate();
				marketType = "FI";
				// setting EhbAmount was missing while creating handshake Added with 
				if(null != household.getIndividual().getEhbAmount() && household.getIndividual().getEhbAmount() != 0.0f){
					pldOrderResponse.setEhbAmount(household.getIndividual().getEhbAmount());
				}
			} else {
				setHouseholdForEmployee(household.getShop(), pldOrderResponse);
				enrollmentType = household.getShop().getEnrollmentType();
				coverageStartDateStr = household.getShop().getCoverageStartDate();
				marketType = "24";
			}
			Members householdMemberList = household.getMembers();
	    	List<PdOrderItemDTO> pdOrderItemDTOList = pdOrderResponse.getPdOrderItemDTOList();
	    	//EnrollmentResponse existingQHPDetails=null;
	    	//EnrollmentResponse existingQDPDetails=null;
	    	Map<String,String> insuranceTypeMap= new HashMap<>();
	    	Map<String, EnrollmentResponse> existingEnrollmentMap= new HashMap<String, EnrollmentResponse>();
	    	if(EnrollmentVal.S.equals(enrollmentType)){
	    		if(householdMemberList!=null&& householdMemberList.getMember()!=null && !householdMemberList.getMember().isEmpty()){
	    			for(Member householdMember : householdMemberList.getMember()){
	    				EnrollmentResponse existingEnrollmentDetails=null;
	    				if(StringUtils.isNotBlank(householdMember.getExistingMedicalEnrollmentID()) && !existingEnrollmentMap.containsKey(householdMember.getExistingMedicalEnrollmentID())){
	    					existingEnrollmentDetails=  getPlanIdFromEnrollmentId(householdMember.getExistingMedicalEnrollmentID());
	    					if(existingEnrollmentDetails.getMemberIds() == null){
	    						throw new GIRuntimeException("QHP Enrollment : Unable to find Member ID for existing EnrollmentID: "+householdMember.getExistingMedicalEnrollmentID() + 
	    													" MemberId: "+householdMember.getMemberId());
	    					}
	    					existingEnrollmentMap.put(householdMember.getExistingMedicalEnrollmentID(), existingEnrollmentDetails);
	    					insuranceTypeMap.put(EnrollmentConstants.INSURANCE_TYPE_HEALTH, householdMember.getExistingMedicalEnrollmentID());
	    					//insuranceTypeMap.put(householdMember.getExistingMedicalEnrollmentID(), value);
	    					/*existingQHPDetails = getPlanIdFromEnrollmentId(householdMember.getExistingMedicalEnrollmentID(), pldOrderResponse.getTerminationFlag());
	    					if(existingQDPDetails.getMemberIds() == null){
	    						throw new GIRuntimeException("QHP Enrollment : Unable to find Member ID for existing EnrollmentID: "+householdMember.getExistingMedicalEnrollmentID() + 
	    													" Termination Flag: "+pldOrderResponse.getTerminationFlag()+
	    													" MemberId: "+householdMember.getMemberId());
	    					}
	    					*/
	    				}
	    				if(StringUtils.isNotBlank(householdMember.getExistingSADPEnrollmentID()) && !existingEnrollmentMap.containsKey(householdMember.getExistingSADPEnrollmentID())){
	    					existingEnrollmentDetails = getPlanIdFromEnrollmentId(householdMember.getExistingSADPEnrollmentID());
	    					if(existingEnrollmentDetails.getMemberIds() == null){
	    						throw new GIRuntimeException("QDP Enrollment : Unable to find Member ID for existing EnrollmentID: "+householdMember.getExistingSADPEnrollmentID() + 
	    													" MemberId: "+householdMember.getMemberId());
	    					}
	    					existingEnrollmentMap.put(householdMember.getExistingSADPEnrollmentID(), existingEnrollmentDetails);
	    					insuranceTypeMap.put(EnrollmentConstants.INSURANCE_TYPE_DENTAL, householdMember.getExistingSADPEnrollmentID());
	    					/*existingQDPDetails = getPlanIdFromEnrollmentId(householdMember.getExistingSADPEnrollmentID(), pldOrderResponse.getDentalTerminationFlag());
	    					if(existingQDPDetails.getMemberIds() == null){
	    						throw new GIRuntimeException("QDP Enrollment : Unable to find Member ID for existing EnrollmentID: "+householdMember.getExistingSADPEnrollmentID() + 
	    													" Termination Flag: "+pldOrderResponse.getTerminationFlag()+
	    													" MemberId: "+householdMember.getMemberId());
	    					}*/
	    				}
	    			}
	    			
	    		}
	    	}
			
			ArrayList<PldPlanData> planDataList = new ArrayList<PldPlanData>();
			//boolean isDentalSelected=false;
			//boolean isHealthSelected=false;
			Set<String> processedEnrollmentIDs=new HashSet<String>();
			if(pdOrderItemDTOList!=null && !pdOrderItemDTOList.isEmpty()){
				
				for(PdOrderItemDTO pdOrderItemDTO : pdOrderItemDTOList){
					PldPlanData pldPlanData=new PldPlanData();
					Map<String,String> plan = getPlanData(pdOrderItemDTO, coverageStartDateStr, marketType);
					pldPlanData.setPlan(plan);
					pldPlanData.setFinancialEffectiveDate(financialEffectiveDate);
					EnrollmentResponse existingEnrollment= existingEnrollmentMap.get(insuranceTypeMap.get(pdOrderItemDTO.getInsuranceType().toString()));
					formPldPlanData(pdOrderItemDTO, existingEnrollment, householdMemberList, enrollmentType, coverageStartDateStr, pldOrderResponse, marketType,   pldPlanData);
					processedEnrollmentIDs.add(insuranceTypeMap.get(pdOrderItemDTO.getInsuranceType().toString()));
					/*if(EnrollmentConstants.INSURANCE_TYPE_HEALTH.equalsIgnoreCase(pdOrderItemDTO.getInsuranceType().toString())){
						formPldPlanData(pdOrderItemDTO, existingQHPDetails, householdMemberList, enrollmentType, coverageStartDateStr, pldOrderResponse,pldOrderResponse.getTerminationFlag(), marketType,   pldPlanData);
						isHealthSelected=true;
					}else if(EnrollmentConstants.INSURANCE_TYPE_DENTAL.equalsIgnoreCase(pdOrderItemDTO.getInsuranceType().toString())){
						formPldPlanData(pdOrderItemDTO, existingQDPDetails, householdMemberList, enrollmentType, coverageStartDateStr, pldOrderResponse, pldOrderResponse.getDentalTerminationFlag(), marketType,   pldPlanData);
						isDentalSelected=true;
					}*/
					planDataList.add(pldPlanData);
					planIds.add(""+pdOrderItemDTO.getPlanId());
				}
				pldOrderResponse.setPlanDataList(planDataList);
			}
			pldOrderResponse.setPlans(planIds);
			pldOrderResponse.setMarketType(marketType);
			// Plan exist in enrollment db but not selected during special
			if(EnrollmentVal.S.equals(enrollmentType)){
				Date coverageStartDate= DateUtil.StringToDate(coverageStartDateStr, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
				String coverageEndDateStr= DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(coverageStartDate),GhixConstants.REQUIRED_DATE_FORMAT);
				for (String existingEnrollmentId:existingEnrollmentMap.keySet()){
					if(!processedEnrollmentIDs.contains(existingEnrollmentId)){
						EnrollmentResponse existingEnrollment= existingEnrollmentMap.get(existingEnrollmentId);
						addMemberToDisenrollMemberList(existingEnrollment.getMemberIds(), existingEnrollment, pldOrderResponse, coverageEndDateStr, householdMemberList, EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString());
					}
				}
				/*if(!isHealthSelected && existingQHPDetails!=null){
					addMemberToDisenrollMemberList(existingQHPDetails.getMemberIds(), existingQHPDetails, pldOrderResponse, coverageEndDateStr, householdMemberList, EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString());
				}
				if(!isDentalSelected && existingQDPDetails!=null){
					addMemberToDisenrollMemberList(existingQDPDetails.getMemberIds(), existingQDPDetails, pldOrderResponse, coverageEndDateStr, householdMemberList, EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString());
				}*/
			}
		}
		return pldOrderResponse;
	}

	
	private PldPlanData formPldPlanData(PdOrderItemDTO pdOrderItemDTO, EnrollmentResponse existingEnrollmentData, Members householdMemberList, EnrollmentVal enrollmentType, String coverageStartDateStr, PldOrderResponse pldOrderResponse, String marketType, PldPlanData pldPlanData){
		//PldPlanData pldPlanData = new PldPlanData();
		PlanResponse planResponse= null;
		if(existingEnrollmentData!=null){
			pldPlanData.setTerminationFlag(existingEnrollmentData.isTerminationFlag());
		}
		if( isNotNullAndEmpty(pdOrderItemDTO.getPlanId())){
			planResponse=  enrollmentService.getPlanInfo(pdOrderItemDTO.getPlanId().toString(), marketType);
			pldPlanData.setPlanResponse(planResponse);
		}
		if(EnrollmentVal.I.equals(enrollmentType) || EnrollmentVal.A.equals(enrollmentType) || EnrollmentVal.N.equals(enrollmentType)){
			if(pldOrderResponse.getApplicationType()!=null && pldOrderResponse.getApplicationType().equalsIgnoreCase(EnrollmentConstants.QLE_INDICATOR_QEP)) {
				pldPlanData.setQleReason(EnrollmentConstants.QLE_REASON_QLE);
			}
				/**Setting renewal flag for IND19 Renewal (Manual Renewal flow)        HIX-89049       */
				if(EnrollmentVal.A.equals(enrollmentType) && pldOrderResponse.getAutoRenewal()==null){
					pldOrderResponse.setAutoRenewal('N');
				}
						
			//add all members to PldPlanData
			List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
			for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()){
				PldMemberData pldMemberData=formMemberData(pdOrderItemDTO, enrollmentType, pdOrderItemPersonDTO, householdMemberList);
				if(pldMemberData!=null){
					memberInfoList.add(pldMemberData);
				}
			}
			if(!memberInfoList.isEmpty()){
				pldPlanData.setNewMemberInfoList(memberInfoList);
			}
		}else if (EnrollmentVal.S.equals(enrollmentType)){
			if(existingEnrollmentData==null){
				pldPlanData.setCreateNewEnrollment(EnrollmentConstants.YES);
				//this case handles if the plan is added in special
				List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
				for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()){
					PldMemberData pldMemberData=formMemberData(pdOrderItemDTO, enrollmentType, pdOrderItemPersonDTO, householdMemberList);
					if(pldMemberData!=null){
						memberInfoList.add(pldMemberData);
					}
				}
				if(!memberInfoList.isEmpty()){
					pldPlanData.setNewMemberInfoList(memberInfoList);
				}
			}else if(existingEnrollmentData.getCmsPlanId().substring(0,Math.min(14,existingEnrollmentData.getCmsPlanId().length())).equalsIgnoreCase(planResponse.getIssuerPlanNumber().substring(0,Math.min(14,planResponse.getIssuerPlanNumber().length())))){
				String subscriberId= getSubscriberId(pdOrderItemDTO.getPdOrderItemPersonDTOList());
				String newEnrollmentOnSubscriberChangeFlag =DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.NEW_ENROLLMENT_ON_SUBSCRIBER_CHANGE);
				//check for CS level change
				if(!existingEnrollmentData.getCmsPlanId().substring(Math.max(existingEnrollmentData.getCmsPlanId().length() - 2, 0)).equalsIgnoreCase(planResponse.getIssuerPlanNumber().substring(Math.max(planResponse.getIssuerPlanNumber().length() - 2, 0)))){
					pldPlanData.setCreateNewEnrollment(EnrollmentConstants.YES);
					pldPlanData.setNewEnrollmentReason(EnrollmentConstants.EnrollmentAppEvent.CS_LEVEL_CHANGE_ENROLLMENT.toString());
					pldPlanData.setQleReason(EnrollmentConstants.QLE_REASON_CS_CHANGE);
					pldPlanData.setExistingEnrollmentId(existingEnrollmentData.getEnrollmentId().toString());
					Date coverageStartDate= DateUtil.StringToDate(coverageStartDateStr, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
					String coverageEndDateStr= DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(coverageStartDate), GhixConstants.REQUIRED_DATE_FORMAT);
					addMemberToDisenrollMemberList(existingEnrollmentData.getMemberIds(), existingEnrollmentData, pldOrderResponse, coverageEndDateStr, householdMemberList, EnrollmentConstants.EnrollmentAppEvent.CS_LEVEL_CHANGE_DISENROLLMENT.toString());
					//add all members into memberdetails
					List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
					for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()){
						
						PldMemberData pldMemberData=formMemberData(pdOrderItemDTO,  enrollmentType, pdOrderItemPersonDTO, householdMemberList);
						if(pldMemberData!=null){
							memberInfoList.add(pldMemberData);
						}
					}
					if(!memberInfoList.isEmpty()){
						pldPlanData.setNewMemberInfoList(memberInfoList);
					}
				}else if(EnrollmentUtils.getBooleanFromYesOrYFlag(newEnrollmentOnSubscriberChangeFlag)&&!pldPlanData.isTerminationFlag() && !existingEnrollmentData.getSubscriberMemberId().equalsIgnoreCase(subscriberId)){
					//subscriber changed
					// disenroll all members with termination date as a day before to termination loop
					pldPlanData.setCreateNewEnrollment(EnrollmentConstants.YES);
					pldPlanData.setNewEnrollmentReason(EnrollmentConstants.EnrollmentAppEvent.SUBSCRIBER_CHANGE_ENROLLMENT.toString());
					pldPlanData.setQleReason(EnrollmentConstants.QLE_REASON_SUB_CHANGE);
					pldPlanData.setExistingEnrollmentId(existingEnrollmentData.getEnrollmentId().toString());
					Date coverageStartDate= DateUtil.StringToDate(coverageStartDateStr, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
					String coverageEndDateStr= DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(coverageStartDate), GhixConstants.REQUIRED_DATE_FORMAT);
					addMemberToDisenrollMemberList(existingEnrollmentData.getMemberIds(), existingEnrollmentData, pldOrderResponse, coverageEndDateStr, householdMemberList, EnrollmentConstants.EnrollmentAppEvent.PLAN_CHANGE_DISENROLLMENT.toString());
					//add all members into memberdetails
					List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
					for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()){
						
						PldMemberData pldMemberData=formMemberData(pdOrderItemDTO,  enrollmentType, pdOrderItemPersonDTO, householdMemberList);
						if(pldMemberData!=null){
							memberInfoList.add(pldMemberData);
						}
					}
					if(!memberInfoList.isEmpty()){
						pldPlanData.setNewMemberInfoList(memberInfoList);
					}
				}else{
					pldPlanData.setCreateNewEnrollment(EnrollmentConstants.NO);
					pldPlanData.setExistingEnrollmentId(existingEnrollmentData.getEnrollmentId().toString());
					//no change plan so compare all members
					//new members add to new member list
					List<PldMemberData> newMemberInfoList = new ArrayList<PldMemberData>();
					List<PldMemberData> existingMemberInfoList = new ArrayList<PldMemberData>();
					for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()){
						PldMemberData pldMemberData=formMemberData(pdOrderItemDTO,enrollmentType, pdOrderItemPersonDTO, householdMemberList);
						if(pldMemberData!=null){
							if(existingEnrollmentData.getMemberIds().contains(pdOrderItemPersonDTO.getExternalId())){
								//add to existing member loop
								existingMemberInfoList.add(pldMemberData);
							}else{
								//add to new member loop
								newMemberInfoList.add(pldMemberData);
							}
							
						}
					}
					if(!newMemberInfoList.isEmpty()){
						pldPlanData.setNewMemberInfoList(newMemberInfoList);
					}
					if(!existingMemberInfoList.isEmpty()){
						pldPlanData.setExistingMemberInfoList(existingMemberInfoList);
					}
					List<String> membersToTerminate =getMembersNotInPdPersonList( pdOrderItemDTO.getPdOrderItemPersonDTOList(), existingEnrollmentData.getMemberIds());
					if(membersToTerminate!=null && !membersToTerminate.isEmpty()){
						Date coverageStartDate= DateUtil.StringToDate(coverageStartDateStr, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
						String coverageEndDateStr= DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(coverageStartDate),GhixConstants.REQUIRED_DATE_FORMAT);
						addMemberToDisenrollMemberList(membersToTerminate, existingEnrollmentData, pldOrderResponse, coverageEndDateStr, householdMemberList, EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString());
					}
					
					validateIdahoFinancialEffDate( existingEnrollmentData, pldPlanData);
				}
				
			}else{
				// disenroll all members with termination date as a day before to termination loop
				pldPlanData.setCreateNewEnrollment(EnrollmentConstants.YES);
				pldPlanData.setExistingEnrollmentId(existingEnrollmentData.getEnrollmentId().toString());
				pldPlanData.setQleReason(EnrollmentConstants.QLE_REASON_PLAN_CHANGE);
				pldPlanData.setNewEnrollmentReason(EnrollmentConstants.EnrollmentAppEvent.PLAN_CHANGE_ENROLLMENT.toString());
				Date coverageStartDate= DateUtil.StringToDate(coverageStartDateStr, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
				String coverageEndDateStr= DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(coverageStartDate), GhixConstants.REQUIRED_DATE_FORMAT);
				addMemberToDisenrollMemberList(existingEnrollmentData.getMemberIds(), existingEnrollmentData, pldOrderResponse, coverageEndDateStr, householdMemberList, EnrollmentConstants.EnrollmentAppEvent.PLAN_CHANGE_DISENROLLMENT.toString());
				//add all members into memberdetails
				List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
				for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()){
					
					PldMemberData pldMemberData=formMemberData(pdOrderItemDTO,  enrollmentType, pdOrderItemPersonDTO, householdMemberList);
					if(pldMemberData!=null){
						memberInfoList.add(pldMemberData);
					}
				}
				if(!memberInfoList.isEmpty()){
					pldPlanData.setNewMemberInfoList(memberInfoList);
				}
			}
			
		}
		return pldPlanData;
	}
	
	private List<String> getMembersNotInPdPersonList(List<PdOrderItemPersonDTO> pdPersonList, List<String> existingMemberList){
		List<String> terminateMembers= new ArrayList<String>();
		if(existingMemberList!=null && !existingMemberList.isEmpty() ){
			if(pdPersonList==null || pdPersonList.isEmpty()){
				terminateMembers.addAll(existingMemberList);
			}else{
				for (String memberId :existingMemberList){
					boolean found=false;
					for(PdOrderItemPersonDTO pdPerson: pdPersonList){
						if(memberId.equalsIgnoreCase(pdPerson.getExternalId())){
							found=true;
							break;
						}
					}
					if(!found){
						terminateMembers.add(memberId);
					}
				}
			}
		}
		return terminateMembers;
	}
	
	private PldMemberData formMemberData(PdOrderItemDTO pdOrderItemDTO, EnrollmentVal enrollmentType,  PdOrderItemPersonDTO pdOrderItemPersonDTO, Members householdMemberList){
		Member householdMember = getMemberFromMemberList(householdMemberList.getMember(), pdOrderItemPersonDTO);
		Map<String,Long> enrollmentPlanMap = null;
		/*if(EnrollmentVal.S.equals(enrollmentType)){
			enrollmentPlanMap = formEnrollmentPlanMap(householdMember, existingQhpPlanId, existingSadpPlanId);
		}*/
		
		Map<String,String> memberInfo = createMemberInfoMapFromPersonData(householdMember, pdOrderItemDTO.getInsuranceType(), enrollmentType, enrollmentPlanMap, pdOrderItemDTO.getPlanId());				
		String subscriberFlag = YorN.Y.equals(pdOrderItemPersonDTO.getSubscriberFlag()) ? "YES" : "NO";
		memberInfo.put("subscriberFlag", subscriberFlag);
		memberInfo.put("ratingArea",""+pdOrderItemPersonDTO.getRegion());
		memberInfo.put("individualPremium",""+pdOrderItemPersonDTO.getPremium());
		memberInfo.put("contribution",""+pdOrderItemPersonDTO.getAppliedSubsidy());
		if(pdOrderItemPersonDTO.getAge()!=null) {
			memberInfo.put("age",""+pdOrderItemPersonDTO.getAge());
		}
		if(pdOrderItemPersonDTO.getPersonCoverageStartDate()!=null) {
			memberInfo.put("quotingDate",DateUtil.dateToString(pdOrderItemPersonDTO.getPersonCoverageStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		}
		
		List<Map<String,String>> raceInfoList = new ArrayList<Map<String,String>>();
		List<Race> raceList = householdMember.getRace();
		for(Race race: raceList){
			Map<String,String> raceInfo = new HashMap<String,String>();
			raceInfo.put("raceEthnicityCode",race.getRaceEthnicityCode());
			raceInfo.put("description",race.getDescription()); // Optional
			raceInfoList.add(raceInfo);
		}
		        		
		List<Map<String,String>> relationInfoList = createMemberRelationshipInfoList(householdMember.getRelationship());        		
		PldMemberData pldMemberData = new PldMemberData();
		pldMemberData.setMemberInfo(memberInfo);
		pldMemberData.setRaceInfo(raceInfoList);
		pldMemberData.setRelationshipInfo(relationInfoList);
		return pldMemberData;
	}
	
	
	/*private void findExistingPlanDetails(EnrollmentResponse existingQHPDetails,EnrollmentResponse existingQDPDetails,Members householdMemberList ){
		
		if(householdMemberList!=null&& householdMemberList.getMember()!=null && !householdMemberList.getMember().isEmpty()){
			for(Member householdMember : householdMemberList.getMember()){
				if(StringUtils.isNotBlank(householdMember.getExistingMedicalEnrollmentID()) && existingQHPDetails==null){
					existingQHPDetails = getPlanIdFromEnrollmentId(householdMember.getExistingMedicalEnrollmentID());
				}
				if(StringUtils.isNotBlank(householdMember.getExistingSADPEnrollmentID()) && existingQDPDetails==null){
					existingQDPDetails = getPlanIdFromEnrollmentId(householdMember.getExistingSADPEnrollmentID());
				}
			}
			
		}
	}*/

	private IndividualInformationRequest  getPayloadRequest(PdOrderResponse pdOrderResponse, AutoRenewalRequest autoRenewalReq, String enrollmentType) throws GIException{
		
		IndividualInformationRequest ind19Request = null;
		try{
			Long giWsPayloadId=pdOrderResponse.getGiWsPayloadId();
		if(giWsPayloadId!=null && autoRenewalReq==null){
			
    		GIWSPayload giWSPayload = giWSPayloadService.findById(giWsPayloadId.intValue());
    		
    		if(giWSPayload==null ||giWSPayload.getRequestPayload()==null ){
    			throw new GIException(EnrollmentConstants.PAYLOAD_OR_IND19_NULL + giWsPayloadId);
    		}
    		pdOrderResponse.setGlobalId(giWSPayload.getGlobalId());
        	/*String payload= giWSPayload.getRequestPayload();
        	 * 
			JAXBContext context  = JAXBContext.newInstance(IndividualInformationRequest.class);
			StringReader stringReaderIND19 = new StringReader(payload);*/
    		ind19Request = (IndividualInformationRequest) GhixUtils.inputStreamToObject(IOUtils.toInputStream(giWSPayload.getRequestPayload(), EnrollmentConstants.ENCODING_UTF_8), IndividualInformationRequest.class);  
		}else if(autoRenewalReq!=null){
			autoRenewalReq.getHousehold().getIndividual().setPlanId(null);
			String reqStr= platformGson.toJson(autoRenewalReq);
			ind19Request = platformGson.fromJson(reqStr, EnrollmentIndShopRequest.class);
			
			if(ind19Request!=null){
				if(enrollmentType.equalsIgnoreCase(EnrollmentVal.A.toString())){
					ind19Request.getHousehold().setAutoRenewal(YesNoVal.Y);
				}
				ind19Request.getHousehold().getIndividual().setEnrollmentType(EnrollmentVal.fromValue(enrollmentType));
			}
		}
		}catch(Exception e){
			throw new GIException(EnrollmentConstants.ERR_MSG_FAILED_TO_GET_PAYLOAD,e);
		}
		
		return ind19Request;
	}
	
	
	private Map<String,String> createMemberInfoMapFromPersonData(Member personDataObj, InsuranceType insuranceType, EnrollmentVal enrollmentType, Map<String,Long> enrollmentPlanMap, Long planId) {
		Map<String,String> memberInfo = new HashMap<String,String>();
		memberInfo.put("memberId",personDataObj.getMemberId());
		if(EnrollmentVal.S.equals(enrollmentType)){
   			memberInfo.put("maintenanceReasonCode", personDataObj.getMaintenanceReasonCode());
   			//this.setSpecialEnrollmentFlags(memberInfo, personDataObj, insuranceType, enrollmentPlanMap, planId, currentDentalMemberList, currentSadpPlanId);
   		}
		if(EnrollmentVal.A.equals(enrollmentType)){
			memberInfo.put("existingQhpEnrollmentId", personDataObj.getExistingMedicalEnrollmentID());
			memberInfo.put("existingSadpEnrollmentId", personDataObj.getExistingSADPEnrollmentID());
		}
   		memberInfo.put("firstName",personDataObj.getFirstName());
   		memberInfo.put("middleName",personDataObj.getMiddleName()); // Optional
   		memberInfo.put("lastName",personDataObj.getLastName());
   		memberInfo.put("suffix",personDataObj.getSuffix()); // Optional
   		memberInfo.put("dob",""+personDataObj.getDob());
   		if(personDataObj.getTobacco() != null){
   			memberInfo.put("tobacco",personDataObj.getTobacco().toString()); // Optional
   			memberInfo.put("lastTobaccoUseDate","");
   		}
    	memberInfo.put("ssn",personDataObj.getSsn()); // Optional
    	memberInfo.put("primaryPhone",personDataObj.getPrimaryPhone());
    	memberInfo.put("secondaryPhone",personDataObj.getSecondaryPhone()); // Optional
    	memberInfo.put("preferredPhone",personDataObj.getPreferredPhone()); // Optional
    	memberInfo.put("preferredEmail",personDataObj.getPreferredEmail()); // Optional
    	memberInfo.put("homeAddress1",personDataObj.getHomeAddress1());
    	memberInfo.put("homeAddress2",personDataObj.getHomeAddress2()); // Optional
    	memberInfo.put("homeCity",personDataObj.getHomeCity());
    	memberInfo.put("homeState",personDataObj.getHomeState());
    	memberInfo.put("homeZip",personDataObj.getHomeZip());
    	memberInfo.put("genderCode",personDataObj.getGenderCode());
    	memberInfo.put("maritalStatusCode",personDataObj.getMaritalStatusCode());
    	memberInfo.put("citizenshipStatusCode",personDataObj.getCitizenshipStatusCode()); // Optional
    	memberInfo.put("spokenLanguageCode",personDataObj.getSpokenLanguageCode()); // Optional
    	memberInfo.put("writtenLanguageCode",personDataObj.getWrittenLanguageCode()); // Optional
    	memberInfo.put("mailingAddress1",personDataObj.getMailingAddress1());
    	memberInfo.put("mailingAddress2",personDataObj.getMailingAddress2()); // Optional
    	memberInfo.put("mailingCity",personDataObj.getMailingCity());
    	memberInfo.put("mailingState",personDataObj.getMailingState());
    	memberInfo.put("mailingZip",personDataObj.getMailingZip());
    	memberInfo.put("custodialParentId",personDataObj.getCustodialParentId()); // Optional
    	memberInfo.put("custodialParentFirstName",personDataObj.getCustodialParentFirstName()); // Optional
    	memberInfo.put("custodialParentMiddleName",personDataObj.getCustodialParentMiddleName()); // Optional
    	memberInfo.put("custodialParentLastName",personDataObj.getCustodialParentLastName()); // Optional
    	memberInfo.put("custodialParentSuffix",personDataObj.getCustodialParentSuffix()); // Optional
    	memberInfo.put("custodialParentSsn",personDataObj.getCustodialParentSsn()); // Optional
    	memberInfo.put("custodialParentPrimaryPhone",personDataObj.getCustodialParentPrimaryPhone()); // Optional
    	memberInfo.put("custodialParentSecondaryPhone",personDataObj.getCustodialParentSecondaryPhone()); // Optional
    	memberInfo.put("custodialParentPreferredPhone",personDataObj.getCustodialParentPreferredPhone()); // Optional
    	memberInfo.put("custodialParentPreferredEmail",personDataObj.getCustodialParentPreferredEmail()); // Optional
    	memberInfo.put("custodialParentHomeAddress1",personDataObj.getCustodialParentHomeAddress1()); // Optional
    	memberInfo.put("custodialParentHomeAddress2",personDataObj.getCustodialParentHomeAddress2()); // Optional
    	memberInfo.put("custodialParentHomeCity",personDataObj.getCustodialParentHomeCity()); // Optional
    	memberInfo.put("custodialParentHomeState",personDataObj.getCustodialParentHomeState()); // Optional
    	memberInfo.put("custodialParentHomeZip",personDataObj.getCustodialParentHomeZip()); // Optional
    	memberInfo.put("houseHoldContactRelationship",personDataObj.getResponsiblePersonRelationship());
    	memberInfo.put("financialHardshipExemption",personDataObj.getFinancialHardshipExemption().toString());
    	if(personDataObj.getCatastrophicEligible() != null){
    		memberInfo.put("catastrophicEligible",personDataObj.getCatastrophicEligible().toString());
    	}
    	//memberInfo.put("childOnlyPlanEligibile",personDataObj.getChildOnlyPlanEligibile().toString());
    	memberInfo.put("countyCode",personDataObj.getCountyCode());
    	if(personDataObj.getExternalMemberId() != null){
    		memberInfo.put("externalMemberId",personDataObj.getExternalMemberId());
    	}
    
    	return memberInfo;
	}
	
	/*public void setSpecialEnrollmentFlags(Map<String,String> memberInfo, Member personDataObj, InsuranceType insuranceType, Map<String,Long> enrollmentPlanMap, Long currentPlanId, List<PdOrderItemPersonDTO> currentDentalMemberList, Long currentSadpPlanId) 
	{
		if(InsuranceType.HEALTH.equals(insuranceType)){
			// Person with null existingQhpEnrollmentId doing Health plan selection is new member
			if(enrollmentPlanMap.get("existingQhpEnrollmentId") == null){
				memberInfo.put("pldNewPersonFlag", "YES");
				memberInfo.put("disenrollQhpFlag", "NO");
				memberInfo.put("disenrollSadpFlag", "NO");
				memberInfo.put("existingQhpEnrollmentId", "");
				memberInfo.put("existingSadpEnrollmentId", "");
			}else{
				// If user has selected same HEALTH plan as of existing -- KEEP (No Change in Health)
				if(enrollmentPlanMap.get("existingEnrolledQhpPlanId").compareTo(currentPlanId) == 0){
					memberInfo.put("pldNewPersonFlag", "NO");
					memberInfo.put("disenrollQhpFlag", "NO");
					memberInfo.put("existingQhpEnrollmentId", String.valueOf(enrollmentPlanMap.get("existingQhpEnrollmentId")));
					setDentalPlanFlags(memberInfo, enrollmentPlanMap, personDataObj, currentDentalMemberList, currentSadpPlanId);
				}else{
					// If user has selected NEW HEALTH plan as of existing -- SHOP
					memberInfo.put("pldNewPersonFlag", "YES");
					memberInfo.put("disenrollQhpFlag", "YES");
					memberInfo.put("existingQhpEnrollmentId", String.valueOf(enrollmentPlanMap.get("existingQhpEnrollmentId")));
					setDentalPlanFlags(memberInfo, enrollmentPlanMap, personDataObj, currentDentalMemberList, currentSadpPlanId);
				}
			}
		}
		else{
			// Person with null existingSadpEnrollmentId doing Dental plan selection is new member
			if(enrollmentPlanMap.get("existingSadpEnrollmentId") == null){
				memberInfo.put("pldNewPersonFlag", "YES");
				memberInfo.put("disenrollQhpFlag", "NO");
				memberInfo.put("disenrollSadpFlag", "NO");
				memberInfo.put("existingQhpEnrollmentId", "");
				memberInfo.put("existingSadpEnrollmentId", "");
			}else{
				if(enrollmentPlanMap.get("existingEnrolledSadpPlanId").compareTo(currentPlanId) == 0){
					memberInfo.put("pldNewPersonFlag", "NO");
					memberInfo.put("disenrollSadpFlag", "NO");
					memberInfo.put("existingSadpEnrollmentId", String.valueOf(enrollmentPlanMap.get("existingSadpEnrollmentId")));
				}else{
					memberInfo.put("pldNewPersonFlag", "YES");
					memberInfo.put("disenrollSadpFlag", "YES");
					memberInfo.put("existingSadpEnrollmentId", String.valueOf(enrollmentPlanMap.get("existingSadpEnrollmentId")));
				}
				
				
				if(enrollmentPlanMap.get("existingQhpEnrollmentId") != null){
					// If user has existing HEALTH Plan
					memberInfo.put("existingQhpEnrollmentId", String.valueOf(enrollmentPlanMap.get("existingQhpEnrollmentId")));
				}else{
					// If user don't have existing HEALTH Plan
					memberInfo.put("existingQhpEnrollmentId", "");
				}
				
				//we are not tracking change of health plan in DENTAL member loop
				memberInfo.put("disenrollQhpFlag", "NO");
			}
		}
	}
*/
	/*public void setDentalPlanFlags(Map<String,String> memberInfo, Map<String,Long> enrollmentPlanMap, Member personDataObj, List<PdOrderItemPersonDTO> currentDentalMemberList, Long currentSadpPlanId) {
		// If user has existing DENTAL Plan
		if(enrollmentPlanMap.get("existingSadpEnrollmentId") != null){
			// If user is no more in DENTAL Plan -- AGE OUT / Disenroll Member (Change in Dental)
			String disenrollSadpFlag = "YES";
			for(PdOrderItemPersonDTO pdOrderItemPersonDTO : currentDentalMemberList){
				if(currentSadpPlanId != null && enrollmentPlanMap.get("existingEnrolledSadpPlanId").compareTo(currentSadpPlanId) == 0 && personDataObj.getMemberId().equalsIgnoreCase(pdOrderItemPersonDTO.getExternalId())){
					// If user is in DENTAL Plan -- NO CHANGE in Dental -- we are not tracking change of dental plan in HEALTH member loop
					disenrollSadpFlag = "NO";
				}
			}
			memberInfo.put("disenrollSadpFlag", disenrollSadpFlag);
			memberInfo.put("existingSadpEnrollmentId", String.valueOf(enrollmentPlanMap.get("existingSadpEnrollmentId")));
		}else{
			// If user dont have existing DENTAL Plan
			memberInfo.put("disenrollSadpFlag", "NO");
			memberInfo.put("existingSadpEnrollmentId", "");
		}		
	}
*/
	public List<Map<String,String>> createMemberRelationshipInfoList(List<Relationship> relationshipList) {
		List<Map<String,String>> relationInfoList = new ArrayList<Map<String,String>>();
		for(Relationship relationship: relationshipList){
			/*boolean isMemeberQuoted = false;
			for(PdOrderItemPersonDTO pdOrderItemPersonDTO: pdOrderItemPersonDTOList){
				if(relationship.getMemberId().equalsIgnoreCase(pdOrderItemPersonDTO.getExternalId().toString())){
					isMemeberQuoted = true;
				}
			}*/
			//if(isMemeberQuoted){
				 Map<String,String> relationInfo = new HashMap<String, String>();
				 relationInfo.put("memberId",relationship.getMemberId());
	 	 	 	 relationInfo.put("relationshipCode",relationship.getRelationshipCode());
	 	 	 	 relationInfoList.add(relationInfo);
   			//}
 	 	}
		return relationInfoList;
	}

	/*public Map<String,Long> formEnrollmentPlanMap(Member personDataObj, String existingQhpPlanId, String existingSadpPlanId) {
		Map<String,Long> enrollmentPlanMap = new HashMap<String,Long>();

		if(personDataObj.getExistingMedicalEnrollmentID() != null && !personDataObj.getExistingMedicalEnrollmentID().equals("")){
			enrollmentPlanMap.put("existingQhpEnrollmentId", Long.parseLong(personDataObj.getExistingMedicalEnrollmentID()));
			enrollmentPlanMap.put("existingEnrolledQhpPlanId", Long.parseLong(existingQhpPlanId));
		}
		if(personDataObj.getExistingSADPEnrollmentID() != null && !personDataObj.getExistingSADPEnrollmentID().equals("")){
			enrollmentPlanMap.put("existingSadpEnrollmentId", Long.parseLong(personDataObj.getExistingSADPEnrollmentID()));
			enrollmentPlanMap.put("existingEnrolledSadpPlanId", Long.parseLong(existingSadpPlanId));
		}		
		return enrollmentPlanMap;
	}
*/	
	private Member getMemberFromMemberList(List<Member> householdMembers, PdOrderItemPersonDTO pdOrderItemPersonDTO) {
		Member currentMember = new Member();
		for(Member householdMember : householdMembers){
			if(householdMember.getMemberId().equals(pdOrderItemPersonDTO.getExternalId().toString())){
				currentMember = householdMember;
			}
		}
		return currentMember;
	}
	
	private Member getMemberFromMemberList(List<Member> householdMembers, String memberId) {
		Member currentMember = null;
		for(Member householdMember : householdMembers){
			if(householdMember.getMemberId().equals(memberId)){
				currentMember = householdMember;
			}
		}
		return currentMember;
	}

	private EnrollmentResponse getPlanIdFromEnrollmentId(String existingEnrollmentId){
		if(existingEnrollmentId == null || existingEnrollmentId.equals(""))	{
			return null;
		}
		EnrollmentResponse enrollmentResponse=getPlanDetailsByEnrollmentId(Integer.parseInt(existingEnrollmentId));
		return enrollmentResponse;
	}
		
	private Map<String,String> getPlanData(PdOrderItemDTO pdOrderItemDTO, String coverageStartDate, String marketType) {
		Float employeeContribution = 0f;
		Float dependentContribution = 0f;
		Map<String,String> plan = new HashMap<String,String>();
		
		plan.put("orderItemId", ""+pdOrderItemDTO.getId());
		plan.put("planId", ""+pdOrderItemDTO.getPlanId());
		plan.put("planType", pdOrderItemDTO.getInsuranceType().toString());
		plan.put("coverageStartDate", DateUtil.changeFormat(coverageStartDate, "MM/dd/yyyy", "yyyy-MM-dd HH:mm:ss.S"));
		plan.put("netPremiumAmt", ""+pdOrderItemDTO.getPremiumAfterCredit());
		plan.put("grossPremiumAmt", ""+pdOrderItemDTO.getPremiumBeforeCredit());
		plan.put("marketType", marketType);
		plan.put(EnrollmentConstants.MAX_APTC,""+ pdOrderItemDTO.getAggrAptc());
		if(pdOrderItemDTO.getMaxStateSubsidy() != null){
			plan.put(EnrollmentConstants.MAX_STATE_SUBSIDY,""+ pdOrderItemDTO.getMaxStateSubsidy());
		}
		if(isNotNullAndEmpty(pdOrderItemDTO.getPdHousehold()) && isNotNullAndEmpty(pdOrderItemDTO.getPdHousehold().getCostSharing())){
			plan.put(EnrollmentConstants.CSR, pdOrderItemDTO.getPdHousehold().getCostSharing().toString());	
		}
		
		
		if(marketType.equals("24")){
			for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()) {
				if(PlanDisplayEnum.Relationship.SELF.equals(pdOrderItemPersonDTO.getRelationship())) {
					employeeContribution = pdOrderItemPersonDTO.getAppliedSubsidy();
				} else {
					if(pdOrderItemPersonDTO.getAppliedSubsidy()!=null){
						dependentContribution += pdOrderItemPersonDTO.getAppliedSubsidy();
					}
				}
			}
			plan.put("employeeContribution", ""+employeeContribution);
			plan.put("dependentContribution", ""+dependentContribution);
		}else {
			if (pdOrderItemDTO.getMonthlySubsidy() != null) {
			plan.put("aptc", "" + pdOrderItemDTO.getMonthlySubsidy());
			}

			if (pdOrderItemDTO.getMonthlyStateSubsidy() != null) {
			plan.put("stateSubsidy", pdOrderItemDTO.getMonthlyStateSubsidy().toString());
			}
		}
		
		return plan;
	}

	private List<PldMemberData> getDisenrollMemberList(DisenrollMembers disenrollMembers, List<String> disEnrollMemberIds) {
		List<PldMemberData> disenrollMemberInfoList = new ArrayList<PldMemberData>();
		
		for(DisenrollMember disenrollMember : disenrollMembers.getDisenrollMember()){
			PldMemberData pldMemberData = new PldMemberData();
			pldMemberData.setAppEventReason(EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString());
			Map<String,String> disenrolMbr = new HashMap<String, String>();
			disenrolMbr.put("memberId", disenrollMember.getMemberId());
			disenrolMbr.put("deathDate", disenrollMember.getDeathDate());
			disenrolMbr.put("terminationDate", disenrollMember.getTerminationDate());
			disenrolMbr.put("terminationReasonCode", disenrollMember.getTerminationReasonCode());
			pldMemberData.setMemberInfo(disenrolMbr);
			disEnrollMemberIds.add(disenrollMember.getMemberId());
			Enrollments enrollments = disenrollMember.getEnrollments();
			
			pldMemberData.setEnrollmentIds(enrollments.getEnrollmentId());
			 
			disenrollMemberInfoList.add(pldMemberData);
		}
		return disenrollMemberInfoList;
	}

	
	private void addMemberToDisenrollMemberList(List<String> memberIds , EnrollmentResponse existingenrollmentData, PldOrderResponse pldResponse, String coverageEndDate, Members householdMemberList, String appEventReason) {
		List<PldMemberData> disenrollMemberInfoList = pldResponse.getDisenrollMemberInfoList();
		if(disenrollMemberInfoList== null ){
			disenrollMemberInfoList= new ArrayList<PldMemberData>();
		}
		for (String memberId:memberIds ){
			boolean found=false;
			if(EnrollmentUtils.isNotNullAndEmpty(memberId)){
				
				
				for(PldMemberData disMemberData: disenrollMemberInfoList){
					
					if(disMemberData!=null&& memberId.equalsIgnoreCase(disMemberData.getMemberInfo().get(EnrollmentConstants.MEMBER_ID)) ){
						found=true;
						Map<String,String> disenrolMbr=disMemberData.getMemberInfo();
						String oldDisEnrollmentDate=disenrolMbr.get("terminationDate");
						if(oldDisEnrollmentDate !=null && coverageEndDate!=null &&  DateUtil.StringToDate(coverageEndDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY).before(DateUtil.StringToDate(oldDisEnrollmentDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY))) {
							disenrolMbr.put("terminationDate", coverageEndDate);
						}
						disMemberData.setMemberInfo(disenrolMbr);
						List<String> enrollmentIds=disMemberData.getEnrollmentIds();
						if(!enrollmentIds.contains(existingenrollmentData.getEnrollmentId().toString())){
							if(isNotNullAndEmpty(appEventReason)){
								disMemberData.setAppEventReason(appEventReason);
							}
							enrollmentIds.add(existingenrollmentData.getEnrollmentId().toString());
							disMemberData.setEnrollmentIds(enrollmentIds);
							if(disMemberData.getRelationshipInfo()==null || disMemberData.getRelationshipInfo().isEmpty()){
								Member householdMember = getMemberFromMemberList(householdMemberList.getMember(), memberId);
								if(householdMember!=null){
									List<Map<String,String>> relationInfoList = createMemberRelationshipInfoList(householdMember.getRelationship());
									disMemberData.setRelationshipInfo(relationInfoList);
								}
							}
						}
						break;
					}
				}
				if(!found){
					Member householdMember = getMemberFromMemberList(householdMemberList.getMember(), memberId);
					if(householdMember==null && EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(existingenrollmentData.getEnrollmentStatusLabel())){
						continue;
					}
					List<String> enrollmentIds= new ArrayList<String>();
					PldMemberData pldMemberData= new PldMemberData();
					if(isNotNullAndEmpty(appEventReason)){
						pldMemberData.setAppEventReason(appEventReason);
					}
					Map<String,String> disenrolMbr = new HashMap<String, String>();
					disenrolMbr.put("memberId", memberId);
					//disenrolMbr.put("deathDate", householdMember.);
					disenrolMbr.put("terminationDate", coverageEndDate);
					if(householdMember!=null){
						List<Map<String,String>> relationInfoList = createMemberRelationshipInfoList(householdMember.getRelationship());
						pldMemberData.setRelationshipInfo(relationInfoList);
						if(isNotNullAndEmpty( householdMember.getMaintenanceReasonCode())){
							disenrolMbr.put("terminationReasonCode", householdMember.getMaintenanceReasonCode());
						}
					}else{
						disenrolMbr.put("terminationReasonCode", EnrollmentConstants.EVENT_REASON_AI);
					}
					
					pldMemberData.setMemberInfo(disenrolMbr);
					
					enrollmentIds.add(existingenrollmentData.getEnrollmentId().toString());
					pldMemberData.setEnrollmentIds(enrollmentIds);
					 
					disenrollMemberInfoList.add(pldMemberData);
				}
				
			}
		}

		pldResponse.setDisenrollMemberInfoList(disenrollMemberInfoList);
	}
	
	private Map<String, String> getResponsiblePerson(ResponsiblePerson responsiblePersonObj) {
		Map<String,String> responsiblePerson = new HashMap<String, String>();
		if(responsiblePersonObj.getResponsiblePersonId() != null && responsiblePersonObj.getResponsiblePersonId().length() != 0){
			responsiblePerson.put("responsiblePersonId", responsiblePersonObj.getResponsiblePersonId()); // Optional
			responsiblePerson.put("responsiblePersonFirstName", responsiblePersonObj.getResponsiblePersonFirstName()); // Optional
			responsiblePerson.put("responsiblePersonMiddleName", responsiblePersonObj.getResponsiblePersonMiddleName()); // Optional
			responsiblePerson.put("responsiblePersonLastName", responsiblePersonObj.getResponsiblePersonLastName()); // Optional
			responsiblePerson.put("responsiblePersonSuffix", responsiblePersonObj.getResponsiblePersonSuffix()); // Optional
			responsiblePerson.put("responsiblePersonSsn", responsiblePersonObj.getResponsiblePersonSsn()); // Optional
			responsiblePerson.put("responsiblePersonPrimaryPhone", responsiblePersonObj.getResponsiblePersonPrimaryPhone()); // Optional
			responsiblePerson.put("responsiblePersonSecondaryPhone", responsiblePersonObj.getResponsiblePersonSecondaryPhone()); // Optional
			responsiblePerson.put("responsiblePersonPreferredPhone", responsiblePersonObj.getResponsiblePersonPreferredPhone()); // Optional
			responsiblePerson.put("responsiblePersonPreferredEmail", responsiblePersonObj.getResponsiblePersonPreferredEmail()); // Optional
			responsiblePerson.put("responsiblePersonHomeAddress1", responsiblePersonObj.getResponsiblePersonHomeAddress1());
			responsiblePerson.put("responsiblePersonHomeAddress2", responsiblePersonObj.getResponsiblePersonHomeAddress2()); // Optional
			responsiblePerson.put("responsiblePersonHomeCity", responsiblePersonObj.getResponsiblePersonHomeCity());
			responsiblePerson.put("responsiblePersonHomeState", responsiblePersonObj.getResponsiblePersonHomeState());
			responsiblePerson.put("responsiblePersonHomeZip", responsiblePersonObj.getResponsiblePersonHomeZip());
			responsiblePerson.put("responsiblePersonType", responsiblePersonObj.getResponsiblePersonType()); // Optional
		}
		return responsiblePerson;
	}

	private Map<String, String> getHouseholdContact(HouseHoldContact houseHoldContactObj) {
		Map<String,String> houseHoldContact = new HashMap<String, String>();
		houseHoldContact.put("houseHoldContactId", houseHoldContactObj.getHouseHoldContactId());
		houseHoldContact.put("houseHoldContactFirstName", houseHoldContactObj.getHouseHoldContactFirstName());
		houseHoldContact.put("houseHoldContactMiddleName", houseHoldContactObj.getHouseHoldContactMiddleName()); // Optional
		houseHoldContact.put("houseHoldContactLastName", houseHoldContactObj.getHouseHoldContactLastName());
		houseHoldContact.put("houseHoldContactSuffix", houseHoldContactObj.getHouseHoldContactSuffix()); // Optional
		houseHoldContact.put("houseHoldContactFederalTaxIdNumber", houseHoldContactObj.getHouseHoldContactFederalTaxIdNumber()); // Optional
		houseHoldContact.put("houseHoldContactPrimaryPhone", houseHoldContactObj.getHouseHoldContactPrimaryPhone()); // Optional
		houseHoldContact.put("houseHoldContactSecondaryPhone", houseHoldContactObj.getHouseHoldContactSecondaryPhone()); // Optional
		houseHoldContact.put("houseHoldContactPreferredPhone", houseHoldContactObj.getHouseHoldContactPreferredPhone()); // Optional
		houseHoldContact.put("houseHoldContactPreferredEmail", houseHoldContactObj.getHouseHoldContactPreferredEmail()); // Optional
		houseHoldContact.put("houseHoldContactHomeAddress1", houseHoldContactObj.getHouseHoldContactHomeAddress1()); // Optional
		houseHoldContact.put("houseHoldContactHomeAddress2", houseHoldContactObj.getHouseHoldContactHomeAddress2()); // Optional
		houseHoldContact.put("houseHoldContactHomeCity", houseHoldContactObj.getHouseHoldContactHomeCity());
		houseHoldContact.put("houseHoldContactHomeState", houseHoldContactObj.getHouseHoldContactHomeState());
		houseHoldContact.put("houseHoldContactHomeZip", houseHoldContactObj.getHouseHoldContactHomeZip());
		return houseHoldContact;
	}

	private void setHouseholdForEmployee(Shop shop,	PldOrderResponse pldOrderResponse) {
		pldOrderResponse.setHouseholdCaseId(""+shop.getHouseholdCaseId());
		pldOrderResponse.setEnrollmentType(shop.getEnrollmentType().value().charAt(0));
		pldOrderResponse.setSerc(shop.getSerc());
		pldOrderResponse.setEmployerId(""+shop.getEmployerId());
		pldOrderResponse.setEmployeeId(""+shop.getHouseholdCaseId());
		pldOrderResponse.setSubsidized(false);
	}

	private void setHouseholdForIndividual(Individual individual,PldOrderResponse pldOrderResponse) {
		pldOrderResponse.setHouseholdCaseId(""+individual.getHouseholdCaseId());
		pldOrderResponse.setEnrollmentType(individual.getEnrollmentType().value().charAt(0));
		pldOrderResponse.setSerc(individual.getSerc());
		pldOrderResponse.setSubsidized(false);
	}
	
	/*private PdOrderResponse findbyhouseholdidpd(String cartId) {
		PdOrderResponse pdOrderResponse = new PdOrderResponse();

		Long pdHouseholdId = Long.parseLong(cartId);
		PdHousehold pdHousehold = pdHouseholdService.findById(pdHouseholdId);
		List<PdOrderItem> pdOrderItems = pdOrderItemService.findByHouseholdId(pdHouseholdId);
		planDisplaySvcHelper.createPdOrderResponseForEnrollment(pdHousehold, pdOrderItems, pdOrderResponse);
		pdOrderResponse.setGiWsPayloadId(pdHousehold.getGiWsPayloadId());
		return pdOrderResponse;
	}*/
	
	private PdOrderResponse findbyhouseholdidpd(String pdHouseholdId) throws GIRuntimeException {
		try {
			String response =  ghixRestTemplate.getForObject(PlandisplayEndpoints.FIND_BY_HOUSEHOLD_ID+"/"+pdHouseholdId, String.class);
			PdOrderResponse pdOrderResponse = platformGson.fromJson(response, PdOrderResponse.class);
			return pdOrderResponse;
		} catch (Exception e) {
			throw new GIRuntimeException(EnrollmentConstants.ERR_MSG_FAILED_TO_CALL_PLANDISPLAY_ENROLLMENT_DATA_SERVICE+" " +pdHouseholdId);
		} 
	}

	private EnrollmentResponse getPlanDetailsByEnrollmentId(Integer enrollmentId){
		EnrollmentResponse enrlResp= null;
			boolean termFlag= false;
			Map<String, Object> enrollmentMap = enrollmentService.getPlanIdAndOrderItemIdByEnrollmentId(enrollmentId);
			if(enrollmentMap!=null && !enrollmentMap.isEmpty()){
				enrlResp= new EnrollmentResponse();
				enrlResp.setEnrollmentId(enrollmentId);
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.ENROLLMENT_STATUS))){
				enrlResp.setEnrollmentStatusLabel((String)enrollmentMap.get(EnrollmentConstants.ENROLLMENT_STATUS));
				if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrlResp.getEnrollmentStatusLabel())){
					termFlag=true;
					enrlResp.setTerminationFlag(termFlag);
				}
			}
			enrlResp.setPlanId((Integer) enrollmentMap.get(EnrollmentConstants.PLAN_ID));
			enrlResp.setOrderItemId((Integer) enrollmentMap.get(EnrollmentConstants.ORDER_ITEM_ID));
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.CMS_PLAN_ID))){
				enrlResp.setCmsPlanId((String)enrollmentMap.get(EnrollmentConstants.CMS_PLAN_ID));
			}
			enrlResp.setSubscriberMemberId((String)enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_MEMBER_ID));
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.APTC))){
				enrlResp.setAptcAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.APTC).toString()));	
			}
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.STATE_SUBSIDY))){
				enrlResp.setStateSubsidyAmt(new BigDecimal(enrollmentMap.get(EnrollmentConstants.STATE_SUBSIDY).toString()));
			}
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.GROSS_PREMIUM_AMT))){
				enrlResp.setGrossPremiumAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.GROSS_PREMIUM_AMT).toString()));	
			}
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.NET_PREMIUM_AMT))){
				enrlResp.setNetPremiumAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.NET_PREMIUM_AMT).toString()));	
			}
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.EFFECTIVE_START_DATE))){
				enrlResp.setEffectiveStartDate(DateUtil.StringToDate(enrollmentMap.get(EnrollmentConstants.EFFECTIVE_START_DATE).toString(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
			}
			
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.EFFECTIVE_END_DATE))){
				enrlResp.setEffectiveEndDate(DateUtil.StringToDate(enrollmentMap.get(EnrollmentConstants.EFFECTIVE_END_DATE).toString(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
			}
			
			List<String> memberIds= null;
			if(termFlag){
				memberIds=enrolleeRepository.getExchgIndivIdentifierByEnrollmentID(enrollmentId);
			}else{
				memberIds=enrolleeRepository.getActiveExchgIndivIdentifierByEnrollmentID(enrollmentId);
			}
			
			if(memberIds!=null && !memberIds.isEmpty()){
				enrlResp.setMemberIds(memberIds);
			}
			
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			enrlResp.setErrMsg(EnrollmentConstants.MSG_RETRIEVE_ENROLLMENT_SUCCESS);
			enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
			LOGGER.info(EnrollmentConstants.MSG_RETRIEVE_ENROLLMENT_SUCCESS);
			}
		return enrlResp;
	}
	public boolean getBooleanFromPlanDisplayFlags(String planDisplayFlag){
		boolean flag = false;
		if(isNotNullAndEmpty(planDisplayFlag) && (planDisplayFlag.equalsIgnoreCase(EnrollmentConstants.YES) || planDisplayFlag.equalsIgnoreCase(EnrollmentConstants.Y))){
			flag = true;
		}
		return flag;
	}
	
	private String getSubscriberId(List<PdOrderItemPersonDTO> pdPersonList){
		String subscriberId=null;
			
		if(pdPersonList!=null && !pdPersonList.isEmpty()){
			for(PdOrderItemPersonDTO personDto: pdPersonList){
				if(YorN.Y.equals(personDto.getSubscriberFlag())){
					subscriberId= personDto.getExternalId();
					break;
				}
			}
		}
		return subscriberId;
	}
	
	 // HIX-104030
		private void validateIdahoFinancialEffDate(EnrollmentResponse existingEnrollmentData,PldPlanData pldPlanData) {
		Date financialEffectiveDate=pldPlanData.getFinancialEffectiveDate();
		String validationFailureDesc=null;
		if(EnrollmentConfiguration.isIdCall() && financialEffectiveDate!=null){
			if(!EnrollmentUtils.isFloatEqual(pldPlanData.getPlan().get(EnrollmentConstants.GROSS_PREMIUM_AMT), existingEnrollmentData.getGrossPremiumAmt())){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_GROSS_CHANGE + " for Enrollment Id :: "+ existingEnrollmentData.getEnrollmentId() +" for financial Effective Date"+ financialEffectiveDate ;
				pldPlanData.setFinancialEffectiveDate(null);
			}else if(existingEnrollmentData.getEffectiveEndDate().before(DateUtils.truncate(financialEffectiveDate, Calendar.DAY_OF_MONTH))||
					!EnrollmentUtils.isSameYear(existingEnrollmentData.getEffectiveStartDate(), financialEffectiveDate)
					){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_OUT_COV + " for Enrollment Id :: "+ existingEnrollmentData.getEnrollmentId() +" for financial Effective Date"+ financialEffectiveDate ;
				pldPlanData.setFinancialEffectiveDate(null);
			}else if(financialEffectiveDate.before(existingEnrollmentData.getEffectiveStartDate())){
				pldPlanData.setFinancialEffectiveDate(existingEnrollmentData.getEffectiveStartDate());
			}else if(DateUtil.getDateOfMonth(financialEffectiveDate)!=1 && DateUtil.getDateOfMonth(existingEnrollmentData.getEffectiveStartDate())==1){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_APTC_MID_MONTH + " for Enrollment Id :: "+ existingEnrollmentData.getEnrollmentId() +" for financial Effective Date"+ financialEffectiveDate ;
				pldPlanData.setFinancialEffectiveDate(null);
			}
			if(EnrollmentUtils.isNotNullAndEmpty(validationFailureDesc)){
				String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
				enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.FINANCIALEFFDATE, "Financial Effective Date Validation Failure", validationFailureDesc);
			}
		}
	}
}
