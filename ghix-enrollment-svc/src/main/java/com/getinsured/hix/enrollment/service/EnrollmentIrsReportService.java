package com.getinsured.hix.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.enrollment.EnrlEligibleEmployeeIRSDTO;
import com.getinsured.hix.platform.util.exception.GIException;

import us.gov.treasury.irs.common.SHOPEmployerType;
import us.gov.treasury.irs.common.SHOPExchangeType;


/**
 * Provides service level implementation aand processing for IRS Reports.
 * Mostly used in ghix-batch to generate IRS reports.
 * 
 * @author shanbhag_a
 * @since 06/19/2014
 *
 */
public interface EnrollmentIrsReportService {
	void generateShopIrsReport(int month,int year) throws GIException;
	void testPortalApi();
	void generateShopIrsReportbByListParam(List<Integer> employerEnrollmentIdlist,int month,int year) throws GIException;
	List<String> getUniqueHouseHolds(int month, int year) throws GIException;
	void logSkippedHouseholds(Map<String, String> skippedHousholds, String batchDate);
	String getDateTime();
	
	List<Integer> getUniqueEmployerEnrIdList(int month, int year) throws GIException;
	
	SHOPEmployerType getMonthWiseShopEmployerList(List<SHOPEmployerType> shopEmployerTypeList,int reportingMonth, int applicableYear, 
			List<EnrlEligibleEmployeeIRSDTO> enrlEligibleEmployeeIRSDTOList,
			Map<String, String> skippedEnrollment, List<Integer> employerEnrollmentIdList) throws GIException;
	SHOPExchangeType getEmployerInfoFromShop(List<Integer> employerEnrIdList);
	Map<Integer, List<EnrlEligibleEmployeeIRSDTO>> getEmployeeEligibilityFromShop(List<Integer> employerEnrIdList, int month, int year)throws GIException;
	void sendShopEmployerListForReportProcessing(List<SHOPEmployerType> shopEmployerList, int month, int year, int i)throws GIException;
	
	
	List<Object[]> getEmployerIdEmployerEnrollmentIdMap(int month, int year) throws GIException;
	List<Object[]> getEmployerIdEmployerEnrollmentIdMap(int month, int year, List<Integer> employerEnrollmentIds) throws GIException;
}

