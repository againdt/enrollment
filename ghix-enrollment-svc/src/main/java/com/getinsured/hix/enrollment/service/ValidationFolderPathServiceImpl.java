package com.getinsured.hix.enrollment.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IValidationFolderPathRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.model.ExchgPartnerLookup;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixConstants;



@Service("validationFolderPathService")
@Transactional
public class ValidationFolderPathServiceImpl implements ValidationFolderPathService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationFolderPathServiceImpl.class);
	@Autowired private IValidationFolderPathRepository validationFolderPathRepository;

	@Override
	public ExchgPartnerLookup getDropOffLocatonForIndiv834Outbound(String hiosIssuerId,String isa06,String isa05,String gs08,String market,String direction) {


		if(hiosIssuerId != null){
			return validationFolderPathRepository.getDropOffLocatonFor834Outbound(hiosIssuerId,isa06,isa05,gs08,market,direction);
		}
		else{

			return null;
		}
	}
	@Override
	public ExchgPartnerLookup getDropOffLocatonForTA1Inbound834(String fileName) {

		String[] parts = fileName.split("_");
		String hiosIssuerId = parts[1];
		String ISA05=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA05);
		String ISA06=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA06);
		String GS08=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA08);
		if(hiosIssuerId != null){

			return validationFolderPathRepository.getDropOffLocatonForTA1Inbound834(hiosIssuerId,ISA06.trim(),ISA05.trim(),GS08.trim(),GhixConstants.INDIVIDUAL,GhixConstants.INBOUND);
		}else{

			return null;
		}
	}
	@Override
	public ExchgPartnerLookup getDropOffLocatonFor999Inbound834(String fileName) {
		String ISA05=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA05);
		String ISA06=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA06);
		String GS08=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA08);
		String[] parts = fileName.split("_");
		String hiosIssuerId = parts[1];

		if(hiosIssuerId != null){

			return validationFolderPathRepository.getDropOffLocatonFor999Inbound834(hiosIssuerId,ISA06.trim(),ISA05.trim(),GS08.trim(),GhixConstants.INDIVIDUAL,GhixConstants.INBOUND);
		}else{

			return null;
		}
	}

	@Override
	public ExchgPartnerLookup getPickUpLocatonFor834Inbound(String hiosIssuerId) {
		String ISA05=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA05);
		String ISA06=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA06);
		String GS08=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ISA08);
		if(hiosIssuerId != null && !("".equals(hiosIssuerId))){

			return validationFolderPathRepository.getPickUpLocatonFor834Inbound(hiosIssuerId,ISA06.trim(),ISA05.trim(),GS08.trim(),GhixConstants.INDIVIDUAL,GhixConstants.INBOUND);
		}else{

			return null;
		}
	}
	@Override
	public ExchgPartnerLookup findExchgPartnerByHiosIssuerID(String hiosIssuerId,String market,String direction) {

		if(!("".equals(hiosIssuerId))){

			return validationFolderPathRepository.findExchgPartnerByHiosIssuerID(hiosIssuerId,market,direction);
		}else{

			return null;
		}
	}
	@Override
	public ExchgPartnerLookup findExchgPartnerforGroupInstallByHiosIssuerID(String hiosIssuerId,String market,String direction) {

		if(!("".equals(hiosIssuerId))){

			return validationFolderPathRepository.findExchgPartnerforGroupInstallByHiosIssuerID(hiosIssuerId,market,direction);
		}else{
			return null;
		}
	}

	@Override
	public List<ExchgPartnerLookup>findAllHiosIssuerIdForInbound(){
		return validationFolderPathRepository.findAllHiosIssuerIdForInbound(GhixConstants.INBOUND,"834");

	}
	@Override
	public List<ExchgPartnerLookup>findByHiosIssuerId(String hiosIssuerId){
		return validationFolderPathRepository.findByHiosIssuerId(hiosIssuerId);

	}

	@Override
	public void setValidationFolderPath(ExchgPartnerLookup exchgPartnerLookup) {

		validationFolderPathRepository.save(exchgPartnerLookup);

		LOGGER.info("ExchgPartnerLookup Save !!");
	}

	@Override
	public void setISA13Controlnumber(ExchgPartnerLookup exchgPartnerLookup) {

		validationFolderPathRepository.save(exchgPartnerLookup);

		LOGGER.info("ExchgPartnerLookup Save for ISA 13 !!");
	}

}
