package com.getinsured.hix.enrollment.carrierfeed.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.enrollment.Enrollment;

public class CarrierFeedQueryUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CarrierFeedQueryUtil.class);
	
	private static final DecimalFormat decimalFormat = new DecimalFormat("#.00"); 
	//Common Clauses
	String selectClause = "SELECT en FROM Enrollment AS en";
	String joinQueryClasue = " LEFT OUTER JOIN en.insuranceTypeLkp AS insuranceTypeLkp";
	
	//--CarrierAppIds / findEnrollmentByCarrierAppId
	public String getEnrollmentByCarrierAppIdQuery(String carrierAppId, String issuerName, String giProductType){
		
		String enrollemntQuery = null;
		StringBuilder queryBuilder = new StringBuilder();
		
		//select * from enrollment e, lookup_value val where e.INSURANCE_TYPE_LKP = val.LOOKUP_VALUE_ID --AND val.LOOKUP_VALUE_CODE = 'STM'
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String productTypeQueryClause = "insuranceTypeLkp.lookupValueCode = '"+giProductType+"'";
		
		if(giProductType != null){
			enrollemntQuery = queryBuilder.append(selectClause).append(joinQueryClasue).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append("en.carrierAppId = '"+carrierAppId+"'")
								.append(" AND ").append(productTypeQueryClause)
								.toString();
		}else{
			enrollemntQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append("en.carrierAppId = '"+carrierAppId+"'")
								.toString();
		}
			
		LOGGER.info("FindEnrollmentByCarrierAppId QUERY: "+enrollemntQuery);
		

		/*String enrollemntQuery = null;
		queryBuilder = new StringBuilder();
		
		//select * from enrollment e, lookup_value val where e.INSURANCE_TYPE_LKP = val.LOOKUP_VALUE_ID --AND val.LOOKUP_VALUE_CODE = 'STM'
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String productTypeQueryClause = "insuranceTypeLkp.lookupValueCode = '"+giProductType+"'";
		
		if(giProductType != null){
			enrollemntQuery = queryBuilder.append(selectClause).append(joinQueryClasue).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append("en.carrierAppId = '"+carrierAppId+"'")
								.append(" AND ").append(productTypeQueryClause)
								.toString();
		}else{
			enrollemntQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append("en.carrierAppId = '"+carrierAppId+"'")
								.toString();
		}
			
		LOGGER.info("FindEnrollmentByCarrierAppId QUERY: "+enrollemntQuery);	*/
		
		return enrollemntQuery;
	}
	
	//--CarrierAppUID / findEnrollmentByCarrierUID
	public String getEnrollmentByCarrierUIDQuery(String carrierAppUID, String issuerName, String giProductType){
		
		String enrollemntQuery = null;
		StringBuilder queryBuilder = new StringBuilder();
		
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String productTypeQueryClause = "insuranceTypeLkp.lookupValueCode = '"+giProductType+"'";
		
		if(giProductType != null){
			enrollemntQuery = queryBuilder.append(selectClause).append(joinQueryClasue).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append(" en.carrierAppUID = '"+carrierAppUID+"'")
								.append(" AND ").append(productTypeQueryClause)
								.toString();
		}else{
			enrollemntQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append("en.carrierAppUID = '"+carrierAppUID+"'")
								.toString();
		}

		LOGGER.info("FindEnrollmentByCarrierUID QUERY: "+enrollemntQuery);
		
		/*String enrollemntQuery = null;
		queryBuilder = new StringBuilder();
		
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String productTypeQueryClause = "insuranceTypeLkp.lookupValueCode = '"+giProductType+"'";
		
		if(giProductType != null){
			enrollemntQuery = queryBuilder.append(selectClause).append(joinQueryClasue).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append(" en.carrierAppUID = '"+carrierAppUID+"'")
								.append(" AND ").append(productTypeQueryClause)
								.toString();
		}else{
			enrollemntQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append("en.carrierAppUID = '"+carrierAppUID+"'")
								.toString();
		}

		LOGGER.info("FindEnrollmentByCarrierUID QUERY: "+enrollemntQuery);*/
		
		return enrollemntQuery;
	}
	
	//--PolicyID / findEnrollmentByissuerPolicyId
	public String getEnrollmentByissuerPolicyIdQuery(String policyId, String issuerName, String giProductType){
		
		String enrollmentQuery = null;
		StringBuilder queryBuilder = new StringBuilder();
		
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String productTypeQueryClause = "insuranceTypeLkp.lookupValueCode = '"+giProductType+"'";
		String policyNoByExchangeTypeClause = "(en.issuerAssignPolicyNo = '"+policyId+"'"+" OR en.exchangeAssignPolicyNo = '"+policyId+"')";
		
		if(giProductType != null){
			enrollmentQuery = queryBuilder.append(selectClause).append(joinQueryClasue).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append(productTypeQueryClause).append(" AND ").append(policyNoByExchangeTypeClause)
								.toString();
		}else{
			enrollmentQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append(policyNoByExchangeTypeClause)
								.toString();
		}
		
		LOGGER.info("FindEnrollmentByissuerPolicyId QUERY: "+enrollmentQuery);
		
		
		
		/*String enrollmentQuery = null;
		queryBuilder = new StringBuilder();
		
		String issuerNameQueryClause = "lower(en.insurerName) like lower('%"+issuerName+"%')";
		String productTypeQueryClause = "insuranceTypeLkp.lookupValueCode = '"+giProductType+"'";
		String policyNoByExchangeTypeClause = "(en.issuerAssignPolicyNo = '"+policyId+"'"+" OR en.exchangeAssignPolicyNo = '"+policyId+"')";
		
		if(giProductType != null){
			enrollmentQuery = queryBuilder.append(selectClause).append(joinQueryClasue).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append(productTypeQueryClause).append(" AND ").append(policyNoByExchangeTypeClause)
								.toString();
		}else{
			enrollmentQuery = queryBuilder.append(selectClause).append(" WHERE ").append(issuerNameQueryClause)
								.append(" AND ").append(policyNoByExchangeTypeClause)
								.toString();
		}
		
		LOGGER.info("FindEnrollmentByissuerPolicyId QUERY: "+enrollmentQuery);*/
		
		
		return enrollmentQuery;
	}
	
	//--Unique Enrollments
	public Set<Enrollment> getUniqueEnrollments(List<Enrollment> enrollments) throws Exception{
		List<Enrollment> possibleEnrollments = new ArrayList<Enrollment>();
		Set<Enrollment> uniqEnrollments = new HashSet<Enrollment>(possibleEnrollments);
		
		if(uniqEnrollments == null || uniqEnrollments.isEmpty()){
			throw new Exception("Set is Empty");
		}
		else{
			LOGGER.info("Set Size: "+uniqEnrollments.size());
		}
		return uniqEnrollments;
	}
	
	/*public static void main(String[] args) throws Exception {
		CarrierFeedQueryUtil util = new CarrierFeedQueryUtil();
		
		String issuerName = "Aetna";
		String giProductType = null;
		
		String carrierAppId = "A951";
		String carrierAppUID = "IA216452248201502010";
		String policyId = "12233";//"MUL0716143";
		
		*//**
		 *  -- CARRIER_APP_ID = A951
			-- Carrier_App_ID = IA216452248201502010
			-- Exchange_Assign_Policy_No = 12233, issuer_Assign_Policy_No = MUL0716143
			
			SELECT id, CARRIER_APP_ID, CARRIER_APP_UID, issuer_Assign_Policy_No, exchange_Assign_Policy_No FROM Enrollment  en 
			WHERE lower(en.insurer_Name) like lower('%Aetna%') 
			--AND en.CARRIER_APP_ID ='A951'
			--AND en.CARRIER_APP_UID = 'IA216452248201502010'
			--AND (en.issuer_Assign_Policy_No = '12233' 
			--OR en.exchange_Assign_Policy_No = '12233')
			;
		 *//*
		
		//--CarrierAppId
		util.getEnrollmentByCarrierAppIdQuery(carrierAppId, issuerName, giProductType);
		
		//--CarrierAppUID
		util.getEnrollmentByCarrierUIDQuery(carrierAppUID, issuerName, giProductType);
		
		//--PolicyNo
		util.getEnrollmentByissuerPolicyIdQuery(policyId, issuerName, giProductType);
	}*/
	
	public static Float getFormattedFloatValue(Float value)
    {
    	if (value != null)
    	{
    		return Float.parseFloat(decimalFormat.format(value));
    	}
    	return 0.00f;
    }
    
    public static String getFormattedFloatValue(String value)
    {
    	if (value != null)
    	{
    		return decimalFormat.format(Float.parseFloat(value));
    	}
    	return "0.00";
    }
}
