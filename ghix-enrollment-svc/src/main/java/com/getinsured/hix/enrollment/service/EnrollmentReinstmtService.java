package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementRequest;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementResponse;

public interface EnrollmentReinstmtService {
	
	/**
	 * 
	 * @param enrollmentReinstatementRequest Object<EnrollmentReinstatementRequest>
	 * @param isInternal boolean
	 * @param user Object<AccountUser>
	 * @return Object<EnrollmentReinstatementResponse>
	 */
	EnrollmentReinstatementResponse processReinstatementRequest(EnrollmentReinstatementRequest enrollmentReinstatementRequest, boolean isInternal, AccountUser user);

}
