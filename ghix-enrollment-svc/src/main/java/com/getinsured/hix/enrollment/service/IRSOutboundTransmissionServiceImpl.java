package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.enrollment.repository.IIRSOutboundTransmissionRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.enrollment.IRSOutboundTransmission;

@Service("IRSOutboundTransmissionService")
public class IRSOutboundTransmissionServiceImpl implements IRSOutboundTransmissionService
{
	@Autowired private IIRSOutboundTransmissionRepository iIRSOutboundTransmissionRepository;
	@Override
	public List<IRSOutboundTransmission> findIRSOutboundTransmissionByBatchId(String batchId){
		
		return iIRSOutboundTransmissionRepository.findIRSOutboundTransmissionByBatchId(batchId);
	}
	
	public IRSOutboundTransmission findIRSOutboundTransmissionByDocumentSeqId(String documentSeqId,String batchId){
		return iIRSOutboundTransmissionRepository.findIRSOutboundTransmissionByDocumentSeqId(documentSeqId, batchId);
	}

	
	
}