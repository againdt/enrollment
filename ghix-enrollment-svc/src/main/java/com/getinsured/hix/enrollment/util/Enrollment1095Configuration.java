package com.getinsured.hix.enrollment.util;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.PropertiesEnumMarker;

@Component
@DependsOn("dynamicPropertiesUtil")
public final class Enrollment1095Configuration {

	public enum Enrollment1095ConfigurationEnum implements PropertiesEnumMarker{
		ASSIGNED_POLICY_NUMBER_NULL("enrollment1095.validation.ASSIGNED_POLICY_NUMBER_NULL"),
		CATASTROPHIC_ENROLLMENT_RECORD("enrollment1095.validation.CATASTROPHIC_ENROLLMENT_RECORD"),
		COVERAGE_AND_PREMIUM_MONTH_MISMATCH("enrollment1095.validation.COVERAGE_AND_PREMIUM_MONTH_MISMATCH"),
		COVERAGE_START_BEFORE_BIRTH_DATE("enrollment1095.validation.COVERAGE_START_BEFORE_BIRTH_DATE"),
		COVERAGE_START_GREATER_THAN_END_DATE("enrollment1095.validation.COVERAGE_START_GREATER_THAN_END_DATE"),
		COVERAGE_YEAR_MISMATCH("enrollment1095.validation.COVERAGE_YEAR_MISMATCH"),
		DENTAL_ENROLLMENT("enrollment1095.validation.DENTAL_ENROLLMENT"),
		ENROLLMENT_1095_OVERLAPPING_COVERAGE("enrollment1095.validation.ENROLLMENT_1095_OVERLAPPING_COVERAGE"),
		INVALID_ADDRESS_FIELDS("enrollment1095.validation.INVALID_ADDRESS_FIELDS"),
		INVALID_EHB_SLCSP_AMT("enrollment1095.validation.INVALID_EHB_SLCSP_AMT"),
		INVALID_MEMBER_COVERAGE_DATE("enrollment1095.validation.INVALID_MEMBER_COVERAGE_DATE"),
		INVALID_POLICY_DATES("enrollment1095.validation.INVALID_POLICY_DATES"),
		MEMBER_COVERAGE_START_GREATER_THAN_END_DATE("enrollment1095.validation.MEMBER_COVERAGE_START_GREATER_THAN_END_DATE"),
		MEMBER_LEVEL_OVERLAPPING("enrollment1095.validation.MEMBER_LEVEL_OVERLAPPING"),
		MISSING_RECEIPIENT_RECORD("enrollment1095.validation.MISSING_RECEIPIENT_RECORD"),
		NEGATIVE_APTC("enrollment1095.validation.NEGATIVE_APTC"),
		NON_VOID_CASE_SAME_START_AND_END_DATE("enrollment1095.validation.NON_VOID_CASE_SAME_START_AND_END_DATE"),
		NO_ACTIVE_MEMBERS("enrollment1095.validation.NO_ACTIVE_MEMBERS"),
		NO_PREMIUM_MONTH_PRESENT("enrollment1095.validation.NO_PREMIUM_MONTH_PRESENT"),
		NULL_BIRTH_DATE("enrollment1095.validation.NULL_BIRTH_DATE"),
		POLICY_ISSUER_NAME_NULL("enrollment1095.validation.POLICY_ISSUER_NAME_NULL"),
		SSN_LENGTH_INVALID("enrollment1095.validation.SSN_LENGTH_INVALID"),
		SSN_VALUE_INVALID("enrollment1095.validation.SSN_VALUE_INVALID"),
		VOID_CORRECTED_CHECKBOX_SET("enrollment1095.validation.VOID_CORRECTED_CHECKBOX_SET"),
		ZERO_GROSS_PREMIUM("enrollment1095.validation.ZERO_GROSS_PREMIUM"),
		HOUSEHOLD_MISMATCH("enrollment1095.validation.HOUSEHOLD_MISMATCH"),
		IS_VALIDATE_ALL_RECORDS("enrollment1095.validation.IsValidateALLRecords");
		
		private final String value;	  
		
		@Override
		public String getValue(){return this.value;}
		
		Enrollment1095ConfigurationEnum(String value){
	        this.value = value;
	    }
	}
}
