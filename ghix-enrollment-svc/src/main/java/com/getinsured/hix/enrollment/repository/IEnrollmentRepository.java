/**
 *
 */
package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepository;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

/**
 * @author panda_p
 *
 */

public interface IEnrollmentRepository extends TenantAwareRepository<Enrollment, Integer>, EnversRevisionRepository<Enrollment, Integer, Integer>{

	Enrollment findById(Integer enrollmentId);
	
	@Query("FROM Enrollment as en where en.employer=:employer and en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	List<Enrollment> findByEmployer(@Param("employer") Employer employer);

	@Query(" FROM Enrollment as en where en.employer.id =:employerId " +
		   " AND en.employerEnrollmentId=:employerEnrollmentId " +
		   " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	List<Enrollment> findByEmployerAndEmployerEnrollmentId(@Param("employerId") int employerId, @Param("employerEnrollmentId") Integer employerEnrollmentId);

	@Query("FROM Enrollment as en where en.employer.id =:employerId " +
		   " AND en.employerEnrollmentId=:employerEnrollmentId " +
		   " AND (en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM') OR (en.enrollmentStatusLkp.lookupValueCode='TERM' " +
		   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') > TO_DATE(:terminationDate,'MM/DD/YYYY') ))")
	List<Enrollment> findByEmployerAndEmployerEnrollmentId(@Param("employerId") int employerId, @Param("employerEnrollmentId") Integer employerEnrollmentId, @Param("terminationDate") String terminationDateString);

	@Query("SELECT en.id," +
			" en.insurerName," +
			" en.employer," +
			" en.insuranceTypeLkp.lookupValueLabel," +
			" en.groupPolicyNumber," +
			" en.grossPremiumAmt," +
			" en.benefitEffectiveDate," +
			" en.benefitEndDate," +
			" en.employeeId," +
			" en.employeeContribution," +
			" ee.healthCoveragePolicyNo," +
			" en.enrollmentStatusLkp," +
			" en.employerContribution," +
			" en.createdOn," +
			" en.employerEnrollmentId,"+
			" en.issuerId "+
		" FROM Enrollment as en, Enrollee as ee " +
		" where ee.enrollment.id = en.id" +
			" and en.employer.id = :employerid" +
			" and ee.personTypeLkp.lookupValueCode ='SUBSCRIBER'"+
			" and en.employerEnrollmentId=:employerEnrollmentId"
			)
	List<Object[]> findByEmployerId(@Param("employerid") int employerid, @Param("employerEnrollmentId")Integer employerEnrollmentId);

	@Query("SELECT en.id," +
			" en.insurerName," +
			" en.employer," +
			" en.insuranceTypeLkp.lookupValueLabel," +
			" en.groupPolicyNumber," +
			" en.grossPremiumAmt," +
			" en.benefitEffectiveDate," +
			" en.benefitEndDate," +
			" en.employeeId," +
			" en.employeeContribution," +
			" ee.healthCoveragePolicyNo," +
			" en.enrollmentStatusLkp," +
			" en.employerContribution," +
			" en.createdOn," +
			" en.employerEnrollmentId,"+
			" en.issuerId "+
		" FROM Enrollment as en," +
			 " Enrollee as ee, EnrollmentEvent ev "+
		" where ee.enrollment.id = en.id " +
			" and en.employerEnrollmentId <> :employerEnrollmentId"+
			" and ev.enrollee.id = ee.id" +
			" and ee.personTypeLkp.lookupValueCode ='SUBSCRIBER'"+
			" and en.benefitEffectiveDate <= :currentDate "+
			" and (en.benefitEndDate > :lastInvoiceDate or (ev.updatedOn > :lastInvoiceDate and ev.updatedOn <= :currentDate  and ev.eventTypeLkp.lookupValueCode IN ('024')) )"+
			" and en.enrollmentStatusLkp.lookupValueCode in (:status)"+
			" and en.employer.id = :employerid" +
			" and ee.terminationFlag IS NULL "
			)
	List<Object[]> findCurrentMonthEffectiveEnrollment(@Param("currentDate") Date currentDate,@Param("lastInvoiceDate") Date lastInvoiceDate,@Param("status") List<String> status,@Param("employerid") int employerid, @Param("employerEnrollmentId") Integer employerEnrollmentId);

	@Query("SELECT en.id," +
			" en.insurerName," +
			" en.employer," +
			" en.insuranceTypeLkp.lookupValueLabel," +
			" en.groupPolicyNumber," +
			" en.grossPremiumAmt," +
			" en.benefitEffectiveDate," +
			" en.benefitEndDate," +
			" en.employeeId," +
			" en.employeeContribution," +
			" ee.healthCoveragePolicyNo," +
			" en.enrollmentStatusLkp," +
			" en.employerContribution," +
			" en.createdOn," +
			" en.employerEnrollmentId,"+
			" en.issuerId "+
			" FROM Enrollment as en," +
			" Enrollee as ee,"+
			" EnrollmentEvent event "+
			" where ee.enrollment.id = en.id " +
			" and event.enrollee.id=ee.id"+
			" and en.enrollmentStatusLkp.lookupValueCode in (:status)"+
			" and en.employer.id = :employerid" +
			" and en.employerEnrollmentId <> :employerEnrollmentId"+
			" and ee.enrollmentReason = 'S' " +
			" and event.spclEnrollmentReasonLkp IS NOT NULL"+
			" and ee.terminationFlag IS NOT NULL " +
			" and event.updatedOn <= :currentDate " +
			" and event.updatedOn > :lastInvoiceDate " +
			" and ee.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
			" and ee.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
			" and ee.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' "
			)
	List<Object[]> findCurrentMonthEffectiveEnrollmentSpecial(@Param("currentDate") Date currentDate,@Param("lastInvoiceDate") Date lastInvoiceDate,@Param("status") List<String> status,@Param("employerid") int employerid, @Param("employerEnrollmentId") Integer employerEnrollmentId);

	@Query(" FROM Enrollment as an "+
			" WHERE an.enrollmentStatusLkp.lookupValueCode = :status "+
			" AND an.benefitEndDate = :benefitEndDate order by an.employer.id, an.insurerName ASC")
	List<Enrollment> findEnrollmentByStatusAndBenefitEndDate(@Param("status") String status, @Param("benefitEndDate") Date benefitEndDate,  Sort sort);

	@Query(" FROM Enrollment as an "+
			" WHERE an.enrollmentStatusLkp.lookupValueCode = :status "+
			" AND an.employer.id= :employerid " +
			" AND an.benefitEndDate = :benefitEndDate order by an.employer.id, an.insurerName ASC")
	List<Enrollment> findEnrollmentByStatusAndEmployerAndBenefitEndDate(@Param("status") String status, @Param("employerid") int employerid, @Param("benefitEndDate") Date benefitEndDate, Sort sort);

	@Query(" FROM Enrollment as an "+
			" WHERE an.enrollmentTypeLkp.lookupValueId = :enrollmentType "+
			" AND an.enrollmentStatusLkp.lookupValueId = :status"+
			" AND an.employerEnrollmentId = :employerEnrollmentId ")
	List<Enrollment> getPendingShopEnrollment(@Param("employerEnrollmentId") int employerEnrollmentId,@Param("enrollmentType") int enrollmentType, @Param("status") int statusType);

	@Query("FROM Enrollment as an "+
			" WHERE an.benefitEffectiveDate <= :currentDate "+
			" AND an.enrollmentTypeLkp.lookupValueId = :enrollmentType "+
			" AND an.enrollmentStatusLkp.lookupValueId = :status"+
			" AND an.employer.id = :employerid ")
	List<Enrollment> getPendingShopEnrollment(@Param("employerid") int employerid,
	@Param("currentDate") Date currentDate, @Param("enrollmentType") int enrollmentType, @Param("status") int statusType);

	@Query("SELECT distinct enrollment.id "+
						"FROM Enrollment enrollment, EnrollmentEvent event "+
						"where enrollment.issuerId = :issuerId " +
						"and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId "+
						"and ((event.createdOn BETWEEN :startDate AND :endDate) OR (event.extractionStatus = 'RESEND' AND event.createdOn <= :endDate)) " +
						"and event.enrollment = enrollment.id "+
						"and (event.sendToCarrierFlag = 'true' OR event.sendToCarrierFlag IS NULL) " +
						"and enrollment.enrollmentStatusLkp.lookupValueCode NOT IN (:statusList) ORDER BY  enrollment.id ASC")
	List<Integer> findEnrollmentByIssuerIDAndEnrollmentType(@Param("issuerId") Integer issuerId,@Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("enrollmentTypeLookupId") int enrollmentTypeLookupId, @Param("statusList") List<String> statusList);

	@Query("SELECT distinct enrollment.id "+
			"FROM Enrollment enrollment, EnrollmentEvent event "+
			"where enrollment.issuerId = :issuerId " +
			"and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId "+
			"and ((event.createdOn BETWEEN :startDate AND :endDate) OR (event.extractionStatus = 'RESEND' AND event.createdOn <= :endDate)) " +
			"and event.enrollment = enrollment.id "+
			"and (event.sendToCarrierFlag = 'true' OR event.sendToCarrierFlag IS NULL) " +
			"and enrollment.enrollmentStatusLkp.lookupValueCode NOT IN (:statusList )" +
		    "and enrollment.carrierResendFlag = :resendFlag ORDER BY  enrollment.id ASC")
	List<Integer> findEnrollmentByIssuerIDAndEnrollmentTypeWIthFlag(@Param("issuerId") Integer issuerId,@Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("enrollmentTypeLookupId") int enrollmentTypeLookupId,@Param("statusList") List<String> statusList,  @Param("resendFlag") String resendFlag);

	@Query(" SELECT distinct enrollment FROM Enrollment enrollment, EnrollmentEvent event " +
		   " WHERE enrollment.issuerId = :issuerId and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId " +
		   " and (event.createdOn BETWEEN :startDate AND :endDate) and event.enrollment = enrollment.id " +
		   " and enrollment.enrollmentStatusLkp.lookupValueCode <> :status and enrollment.enrollmentStatusLkp.lookupValueCode <>'ABORTED' ")
	List<Enrollment> findEnrollmentByIssuerIDAndEnrollmentType(@Param("issuerId") Integer issuerId,@Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("enrollmentTypeLookupId") int enrollmentTypeLookupId,@Param("status") String status );

	@Query(" SELECT distinct enrollment FROM Enrollment enrollment, EnrollmentEvent event " +
		   " WHERE enrollment.issuerId = :issuerId and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId " +
		   " AND (event.createdOn BETWEEN :startDate AND :endDate) and event.enrollment = enrollment.id " +
		   " AND enrollment.enrollmentStatusLkp.lookupValueCode <> :status and enrollment.enrollmentStatusLkp.lookupValueCode <>'ABORTED' " +
		   " AND enrollment.carrierResendFlag = :resendFlag")
	List<Enrollment> findEnrollmentByIssuerIDAndEnrollmentTypeWithFlag(@Param("issuerId") Integer issuerId,@Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("enrollmentTypeLookupId") int enrollmentTypeLookupId,@Param("status") String status,  @Param("resendFlag") String resendFlag );

	@Query(" SELECT distinct en.issuerId FROM Enrollment en, EnrollmentEvent event " +
		   " WHERE ((event.createdOn BETWEEN :startDate  and :endDate) OR (event.extractionStatus = 'RESEND' AND event.createdOn <= :endDate))" +
		   " AND event.enrollment = en.id " +
		   " AND (event.sendToCarrierFlag = 'true' OR event.sendToCarrierFlag IS NULL) " +
		   " AND en.enrollmentStatusLkp.lookupValueCode <>'ABORTED' " +
		   " AND en.enrollmentTypeLkp.lookupValueCode = :enrollmentType")
	List<Integer> findIssuerByupdateDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("enrollmentType") String enrollmentType );

	@Query(" SELECT distinct en.issuerId FROM Enrollment en, EnrollmentEvent event " +
		   " WHERE ((event.createdOn BETWEEN :startDate and :endDate) OR (event.extractionStatus = 'RESEND' AND event.createdOn <= :endDate)) " +
		   " and event.enrollment = en.id " +
		   " and (event.sendToCarrierFlag = 'true' OR event.sendToCarrierFlag IS NULL) " +
		   " and en.enrollmentStatusLkp.lookupValueCode <>'ABORTED' " +
		   " and en.carrierResendFlag = :resendFlag " +
		   " and en.enrollmentTypeLkp.lookupValueCode = :enrollmentType")
	List<Integer> findIssuerByResendFlag(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("resendFlag") String resendFlag, @Param("enrollmentType") String enrollmentType );


	@Query(" FROM Enrollment as en " +
		   " WHERE en.updatedOn >=  :lastRunDate " +
		   " and en.updatedBy.id = :updatedBy " +
		   " and en.enrollmentStatusLkp.lookupValueCode <>'PENDING'")
	List<Enrollment> getCarrierUpdatedEnrollments(@Param("lastRunDate") Date lastRunDate, @Param("updatedBy") Integer updatedBy);

	@Query("SELECT DISTINCT e FROM Enrollment e, Enrollee ee WHERE " +
			"e.id = ee.enrollment.id AND " +
			"((( :planNumber IS NOT NULL) AND (e.CMSPlanID =:planNumber)) OR (( :planNumber IS NULL) AND ((e.CMSPlanID IS NULL) OR (e.CMSPlanID IS NOT NULL))))  AND " +
			"((( :planMarket IS NOT NULL) AND (e.enrollmentTypeLkp.lookupValueId=cast(:planMarket as integer))) OR (( :planMarket IS NULL) AND ((e.enrollmentTypeLkp.lookupValueId IS NULL) OR (e.enrollmentTypeLkp.lookupValueId IS NOT NULL))))  AND " +
			"((( :planLevel IS NOT NULL) AND (e.planLevel in (:planLevel))) OR (( :planLevel IS NULL) AND ((e.planLevel IS NULL) OR (e.planLevel IS NOT NULL))))  AND " +
			"((( :regionName IS NOT NULL) AND (ee.ratingArea =:regionName)) OR (( :regionName IS NULL) AND ((ee.ratingArea IS NULL) OR (ee.ratingArea IS NOT NULL)))) AND" +
			"((( :startDate IS NOT NULL) AND ((TO_DATE(TO_CHAR(e.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE( :startDate,'MM/DD/YYYY')))) OR (( :startDate IS NULL) AND (e.createdOn IS NULL OR e.createdOn IS NOT NULL))) AND " +
			"((( :endDate IS NOT NULL) AND ((TO_DATE(TO_CHAR(e.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE( :endDate,'MM/DD/YYYY')))) OR (( :endDate IS NULL) AND (e.createdOn IS NULL OR e.createdOn IS NOT NULL))) AND " +
			"((( :status IS NOT NULL) AND (e.enrollmentStatusLkp.lookupValueCode = UPPER(:status))) OR (( :status IS NULL) AND ((e.enrollmentStatusLkp.lookupValueCode IS NULL) OR (e.enrollmentStatusLkp.lookupValueCode IS NOT NULL)))) " +
			"order by e.createdOn")
    List<Enrollment> getEnrollmentsByPlanData(@Param("planNumber") String planNumber, @Param("planLevel") String planLevel, @Param("planMarket") String planMarket, @Param("regionName") String regionName, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("status") String status);

	@Query("SELECT DISTINCT e FROM Enrollment e, Enrollee ee WHERE " +
			"e.id = ee.enrollment.id AND " +
			"((( :issuerName IS NOT NULL) AND (e.insurerName =:issuerName)) OR (( :issuerName IS NULL) AND ((e.insurerName IS NULL) OR (e.insurerName IS NOT NULL))))  AND " +
			"((( :planMarket IS NOT NULL) AND (e.enrollmentTypeLkp.lookupValueId = cast(:planMarket as integer))) OR (( :planMarket IS NULL) AND ((e.enrollmentTypeLkp.lookupValueId IS NULL) OR (e.enrollmentTypeLkp.lookupValueId IS NOT NULL))))  AND " +
			"((( :planLevel IS NOT NULL) AND (e.planLevel in (:planLevel))) OR (( :planLevel IS NULL) AND ((e.planLevel IS NULL) OR (e.planLevel IS NOT NULL))))  AND " +
			"((( :regionName IS NOT NULL) AND (ee.ratingArea =:regionName)) OR (( :regionName IS NULL) AND ((ee.ratingArea IS NULL) OR (ee.ratingArea IS NOT NULL)))) AND " +
			"((( :startDate IS NOT NULL) AND ((TO_DATE(TO_CHAR(e.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE( :startDate,'MM/DD/YYYY')))) OR (( :startDate IS NULL) AND (e.createdOn IS NULL OR e.createdOn IS NOT NULL))) AND " +
			"((( :endDate IS NOT NULL) AND ((TO_DATE(TO_CHAR(e.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE( :endDate,'MM/DD/YYYY')))) OR (( :endDate IS NULL) AND (e.createdOn IS NULL OR e.createdOn IS NOT NULL))) AND " +
			"((( :status IS NOT NULL) AND (e.enrollmentStatusLkp.lookupValueCode = UPPER(:status))) OR (( :status IS NULL) AND ((e.enrollmentStatusLkp.lookupValueCode IS NULL) OR (e.enrollmentStatusLkp.lookupValueCode IS NOT NULL)))) " +
			"order by e.createdOn")
    List<Enrollment> getEnrollmentsByIssuerData(@Param("issuerName") String issuerName, @Param("planLevel") String planLevel, @Param("planMarket") String planMarket, @Param("regionName") String regionName, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("status") String status);

	@Query(" FROM Enrollment as en  " +
		   " WHERE en.employer.externalId= :externalID " +
		   " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
	List<Enrollment> findEnrollmentByExternalID(@Param("externalID") String externalID);

	@Query(" FROM Enrollment as en  " +
		   " WHERE en.employeeAppId in (:employeeAppIDList) " +
		  // " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') " +
		   " AND (en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM') OR (en.enrollmentStatusLkp.lookupValueCode='TERM' " +
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') > TO_DATE(:terminationDate,'MM/DD/YYYY') ))")
	List<Enrollment> findActiveEnrollmentByEmployeeApplicationID(@Param("employeeAppIDList") List<Long> employeeAppIDList, @Param("terminationDate") String terminationDateString);

	@Query(" FROM  Enrollment as en " +
		   " WHERE ((en.enrollmentTypeLkp.lookupValueCode= 'FI' AND en.enrollmentStatusLkp.lookupValueCode = 'PENDING') OR (en.enrollmentStatusLkp.lookupValueCode = 'CONFIRM' AND en.enrollmentConfirmationDate < :cutOffDate )  OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND en.benefitEndDate >:termCondDate) ) " +
		   " AND en.createdOn < :cutOffDate "+
		   " AND en.enrollmentTypeLkp.lookupValueCode= :enrollmentType " +
		   " AND en.insurerTaxIdNumber= :insurerTaxId ")
	List<Enrollment> findEnrollmentByStatusAndUpdateDate( @Param("cutOffDate") Date cutOffDate, @Param("enrollmentType") String enrollmentType , @Param("insurerTaxId") String insurerTaxId,  @Param("termCondDate") Date termCondDate );
	
	@Query(" FROM  Enrollment as en " +
			   " WHERE ((en.enrollmentTypeLkp.lookupValueCode= 'FI' AND en.enrollmentStatusLkp.lookupValueCode = 'PENDING') OR (en.enrollmentStatusLkp.lookupValueCode = 'CONFIRM' AND en.enrollmentConfirmationDate < :cutOffDate )  OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND en.benefitEndDate >:termCondDate) ) " +
			   " AND en.createdOn < :cutOffDate "+
			   " AND en.enrollmentTypeLkp.lookupValueCode= :enrollmentType " +
			   " AND en.insurerTaxIdNumber= :insurerTaxId " +
			   " AND TO_CHAR(en.benefitEffectiveDate, 'YYYY') = :coverageYear")
	List<Enrollment> findEnrollmentByStatusAndUpdateDateAndCoverageYear( @Param("cutOffDate") Date cutOffDate, @Param("enrollmentType") String enrollmentType , @Param("insurerTaxId") String insurerTaxId,
			@Param("termCondDate") Date termCondDate, @Param("coverageYear") String coverageYear);

	@Query(" SELECT distinct en " +
		   " FROM  Enrollment as en " +
		   " WHERE ((en.enrollmentStatusLkp.lookupValueCode = 'CONFIRM'  ) OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND en.benefitEndDate >:termCondDate)) " +
		   " AND en.enrollmentConfirmationDate < :cutOffDate " +
		   " AND en.enrollmentTypeLkp.lookupValueCode= :enrollmentType ")
	List<Enrollment> findEnrollmentForCmsReconcilation( @Param("cutOffDate") Date cutOffDate, @Param("termCondDate") Date termCondDate, @Param("enrollmentType") String enrollmentType);


	@Query(" SELECT distinct en.insurerTaxIdNumber " +
			   " FROM  Enrollment as en "+
			   " WHERE ((en.enrollmentTypeLkp.lookupValueCode= 'FI' AND en.enrollmentStatusLkp.lookupValueCode = 'PENDING') OR (en.enrollmentStatusLkp.lookupValueCode = 'CONFIRM' AND en.enrollmentConfirmationDate < :cutOffDate )  OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND en.benefitEndDate >:termCondDate) ) " +
			   " AND en.createdOn < :cutOffDate "+
			   " AND en.enrollmentTypeLkp.lookupValueCode= :enrollmentType  ")
	List<String> getinsurerTaxIdNumbers( @Param("cutOffDate") Date cutOffDate, @Param("enrollmentType") String enrollmentType, @Param("termCondDate") Date termCondDate  );
	
	@Query(" SELECT distinct en.insurerTaxIdNumber " +
			   " FROM  Enrollment as en "+
			   " WHERE ((en.enrollmentTypeLkp.lookupValueCode= 'FI' AND en.enrollmentStatusLkp.lookupValueCode = 'PENDING') OR (en.enrollmentStatusLkp.lookupValueCode = 'CONFIRM' AND en.enrollmentConfirmationDate < :cutOffDate )  OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND en.benefitEndDate >:termCondDate) ) " +
			   " AND en.createdOn < :cutOffDate "+
			   " AND en.enrollmentTypeLkp.lookupValueCode= :enrollmentType "+
			   " AND TO_CHAR(en.benefitEffectiveDate, 'YYYY') = :coverageYear")
	List<String> getinsurerTaxIdNumbersByCoverageYear( @Param("cutOffDate") Date cutOffDate, @Param("enrollmentType") String enrollmentType, @Param("termCondDate") Date termCondDate, @Param("coverageYear") String coverageYear);

	@Query(" SELECT distinct en.insurerTaxIdNumber " +
			   " FROM  Enrollment as en "+
			   " WHERE ((en.enrollmentTypeLkp.lookupValueCode= 'FI' AND en.enrollmentStatusLkp.lookupValueCode = 'PENDING') OR (en.enrollmentStatusLkp.lookupValueCode = 'CONFIRM' AND en.enrollmentConfirmationDate < :cutOffDate )  OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND en.benefitEndDate >:termCondDate) ) " +
			   " AND en.createdOn < :cutOffDate "+
			   " AND en.enrollmentTypeLkp.lookupValueCode= :enrollmentType "+
			   " AND en.hiosIssuerId = :hiosIssuerId ")
	List<String> getinsurerTaxIdNumbersByIssuerId( @Param("cutOffDate") Date cutOffDate, @Param("enrollmentType") String enrollmentType, @Param("termCondDate") Date termCondDate, @Param("hiosIssuerId") String hiosIssuerId);
	
	@Query(" SELECT distinct en.insurerTaxIdNumber " +
			   " FROM  Enrollment as en "+
			   " WHERE ((en.enrollmentTypeLkp.lookupValueCode= 'FI' AND en.enrollmentStatusLkp.lookupValueCode = 'PENDING') OR (en.enrollmentStatusLkp.lookupValueCode = 'CONFIRM' AND en.enrollmentConfirmationDate < :cutOffDate )  OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND en.benefitEndDate >:termCondDate) ) " +
			   " AND en.createdOn < :cutOffDate "+
			   " AND en.enrollmentTypeLkp.lookupValueCode= :enrollmentType "+
			   " AND en.hiosIssuerId = :hiosIssuerId "+
			   " AND TO_CHAR(en.benefitEffectiveDate, 'YYYY') = :coverageYear ")
	List<String> getinsurerTaxIdNumbersByIssuerIdAndCoverageYear( @Param("cutOffDate") Date cutOffDate, @Param("enrollmentType") String enrollmentType, @Param("termCondDate") Date termCondDate,
			@Param("hiosIssuerId") String hiosIssuerId, @Param("coverageYear")String covergeYear);


	@Query(" SELECT e.planId, "
			+ "e.enrollmentStatusLkp.lookupValueCode, "
			+ "e.CMSPlanID,"
			+ "e.exchgSubscriberIdentifier, "
			+ "TO_CHAR(e.benefitEffectiveDate,'MM/DD/YYYY'), "
			+ "TO_CHAR(e.benefitEndDate,'MM/DD/YYYY'), "
			+ "e.grossPremiumAmt, "
			+ "e.netPremiumAmt, "
			+ "e.aptcAmt, "
			+ "e.planLevel, "
			+ "e.changePlanAllowed, "
			+ "e.stateSubsidyAmt "
		    + "FROM Enrollment as e where e.id = :enrollmentId")
	List<Object[]> getPlanIdAndOrderItemIdByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);

	@Query(" SELECT DISTINCT en FROM Enrollment as en JOIN FETCH en.enrollees " +
		   " WHERE en.id = :enrollmentId " +
		   " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	Enrollment getEnrollmentById(@Param("enrollmentId")Integer enrollmentId);

	@Query("SELECT en FROM Enrollment as en "
			+ "JOIN FETCH en.enrollees "
			+ "WHERE "
			+ "en.id = :enrollmentId "
			+ "AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL' , 'ABORTED')")
	Enrollment getEnrollmentForInbound(@Param("enrollmentId")Integer enrollmentId);

	@Query(" FROM Enrollment as en " +
		   " WHERE en.updatedOn BETWEEN :startDate " +
		   " AND :endDate and en.enrollmentTypeLkp.lookupValueCode <> '24' "+
		   " AND en.enrollmentStatusLkp.lookupValueCode <> 'ABORTED' "
		   //+" AND NOT (en.enrollmentStatusLkp.lookupValueCode='TERM' AND  TO_CHAR(en.benefitEndDate, 'dd-MM')='31-12') "+
		  // " AND NOT (en.enrollmentStatusLkp.lookupValueCode='PENDING' AND en.renewalFlag='A') "
		   )
	List<Enrollment> getEnrollmentsUpdatedAfter(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(" SELECT en.planLevel, count(*) as count " +
		   " FROM Enrollment as en " +
		   " WHERE en.assisterBrokerId = :assisterBrokerId " +
		   " AND en.brokerRole = :role " +
		   " AND (en.updatedOn BETWEEN :startDate AND :endDate ) " +
		   " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') group by en.planLevel ")
	List<Object[]> getEnrollmentPlanLevelCountByBroker(@Param ("assisterBrokerId") Integer assisterBrokerId, @Param("role") String role, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query("SELECT distinct en.employer FROM Enrollment en")
	List<Employer> getDistinctEmployers();

	@Query(" FROM  Enrollment as en  " +
		   " WHERE en.cmrHouseHoldId = :cmrHouseholdId " +
		   " AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','CONFIRM')")
	List<Enrollment> findEnrollmentByCmrHouseholdId(@Param("cmrHouseholdId") Integer cmrHouseholdId);

	@Query(" SELECT DISTINCT en.id, en.employer.id FROM Enrollment as en, EnrollmentEvent as ev"+
		   " WHERE ev.enrollment.id = en.id" +
		   " AND ev.createdOn <= :endDate"+
		   " AND ev.createdOn >= :startDate)")
	List<Object[]> findEmployerIdByStartDateAndEndate(@Param("startDate") Date startDate,@Param("endDate") Date endDate);

	@Query(" SELECT en.id, en.issuerSubscriberIdentifier, en.groupPolicyNumber, en.CMSPlanID, en.exchgSubscriberIdentifier," +
		   " ee.firstName, ee.lastName, ee.middleName, ee.suffix, en.employer.id, ee.healthCoveragePolicyNo "+
		   " FROM Enrollment as en,"+
		   " Enrollee as ee "+
		   " WHERE en.id = :enrollmentId "+
		   " AND ee.enrollment.id = en.id "+
		   " AND ee.personTypeLkp.lookupValueCode='SUBSCRIBER'")
	List<Object[]> getRemittanceDataByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);

	@Modifying
	@Query(" UPDATE Enrollment  SET carrierResendFlag = null  " +
		   " WHERE carrierResendFlag = :carrierResendFlagStatus " +
		   " AND (updatedOn BETWEEN :startDate AND :endDate)")
	@Transactional
	void clearCarrierSendFlag(@Param("carrierResendFlagStatus") String carrierResendFlagStatus, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(" SELECT MIN(benefitEffectiveDate) FROM Enrollment en " +
		   " WHERE en.employerEnrollmentId= :employerEnrollmentId " +
		   " AND en.enrollmentStatusLkp.lookupValueCode='CANCEL' ")
	Date findEarliestEffStartDate(@Param("employerEnrollmentId") Integer employerEnrollmentId);

	@Query(" FROM  Enrollment as en " +
		   " WHERE en.id =(SELECT MAX(id) from Enrollment enr " +
		                 " WHERE enr.employeeAppId= :employeeAppId and insuranceTypeLkp.lookupValueCode='HLT') ")
	Enrollment findLatestShopHealthEnrollment(@Param("employeeAppId") Long employeeAppId);

	@Query(" FROM  Enrollment as en " +
		   " WHERE en.id =(select MAX(id) from Enrollment enr WHERE enr.employeeAppId= :employeeAppId and insuranceTypeLkp.lookupValueCode='DEN') ")
	Enrollment findLatestShopDentalEnrollment(@Param("employeeAppId") Long employeeAppId);

	@Query(" FROM  Enrollment as en " +
			" WHERE en.id =(SELECT MAX(id) FROM Enrollment enr " +
			               " WHERE enr.ssapApplicationid= :ssapApplicationid and insuranceTypeLkp.lookupValueCode='HLT') ")
	Enrollment findLatestIndividualHealthEnrollment(@Param("ssapApplicationid") Long ssapApplicationid);

	@Query(" FROM  Enrollment as en " +
		   " where en.id =(select MAX(id) from Enrollment enr " +
		                 " where enr.ssapApplicationid= :ssapApplicationid and insuranceTypeLkp.lookupValueCode='DEN') ")
	Enrollment findLatestIndividualDentalEnrollment(@Param("ssapApplicationid") Long ssapApplicationid);
	
	
	@Query(" FROM Enrollment enr "
			+ " where enr.houseHoldCaseId= :houseHoldCaseId and enr.insuranceTypeLkp.lookupValueCode='DEN' "
			+ " and enr.benefitEffectiveDate <= :termDate "
			+ " and enr.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','ABORTED') "
			+ "	and enr.benefitEndDate >=:termDate and enr.aptcAmt IS NOT NULL) ")
		List<Enrollment> findIndividualDentalEnrollment(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("termDate") Date termDate);

	@Query(" SELECT e.employeeId,e.CMSPlanID " +
		   " FROM Enrollment as e "+
		   " where e.employer.id=:employerId "+
		   " AND e.insuranceTypeLkp.lookupValueCode = 'HLT' "+
		   " AND e.enrollmentStatusLkp.lookupValueCode in ('PAYMENT_RECEIVED','CONFIRM')")
	List<Object[]> findEmployeeIdAndPlanIdForHealth(@Param("employerId") Integer employerId);
	
	@Query(" SELECT e.employeeId,e.CMSPlanID " +
			   " FROM Enrollment as e "+
			   " where e.employer.id=:employerId "+
			   " AND e.insuranceTypeLkp.lookupValueCode = 'DEN' "+
			   " AND e.enrollmentStatusLkp.lookupValueCode in ('PAYMENT_RECEIVED','CONFIRM')")
		List<Object[]> findEmployeeIdAndPlanIdForDental(@Param("employerId") Integer employerId);

	@Query(" SELECT COUNT(en.id) FROM Enrollment en " +
		   " WHERE en.issuerId=:issuerID " +
		   " AND (en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM','ABORTED') OR (en.enrollmentStatusLkp.lookupValueCode='TERM' " +
		   " AND  en.benefitEndDate > :cutOffDate))")
	long getActiveEnrollmentCountForIssuer(@Param("issuerID") Integer issuerID, @Param("cutOffDate") Date cutOffDate);

	@Query(" SELECT en.id " +
		   " FROM Enrollment as en "+
		   " WHERE en.benefitEffectiveDate <= :currentDate "+
		   " AND (en.benefitEndDate >= :currentDate  OR en.benefitEndDate is null)"+
		   " AND en.enrollmentStatusLkp.lookupValueCode in :status "+
		   " AND en.employer.id = :employerid" )
	List<Integer> findActiveEnrollmentForEmployer(@Param("currentDate") Date currentDate, @Param("status") List<String> status, @Param("employerid") int employerid);

	@Query(" SELECT en.id,en.insuranceTypeLkp.lookupValueLabel ,en.benefitEffectiveDate,en.benefitEndDate," +
		   " en.enrollmentStatusLkp.lookupValueCode "+
		   " FROM Enrollment as en "+
		   " WHERE en.id = (select max(id) FROM Enrollment as enl where  enl.employeeId = :employeeId and insuranceTypeLkp.lookupValueCode = 'HLT') ")
	List<Object[]> findEnrollmentByEmployeeId(@Param("employeeId") Integer employeeId);


	@Query(" SELECT en.id,en.insuranceTypeLkp.lookupValueLabel ,en.benefitEffectiveDate,en.benefitEndDate,"+
			" en.enrollmentStatusLkp.lookupValueCode "+
			" FROM Enrollment as en "+
			" WHERE en.id in (:enrollmentId) ")
	List<Object[]> findEnrollmentByEnrollmentId(@Param("enrollmentId") List<Integer> enrollmentId);

	@Query(" FROM Enrollment as en "+
		   " WHERE en.id in (:enrollmentId) ")
	List<Enrollment> findEnrollmentByEnrollmentIds(@Param("enrollmentId") List<Integer> enrollmentId);

	@Query("FROM Enrollment as en  WHERE en.employeeId= :employeeID " +
			//" AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')"
			" AND (en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM') OR (en.enrollmentStatusLkp.lookupValueCode='TERM' " +
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') > TO_DATE(:terminationDate,'MM/DD/YYYY') ))"
			)
	List<Enrollment> findActiveEnrollmentByEmployeeID(@Param("employeeID") Integer employeeID,  @Param("terminationDate") String terminationDateString);

	@Query(" FROM Enrollment as en  WHERE en.ssapApplicationid =:ssapApplicationId " +
			//"AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
			" AND (en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM') OR (en.enrollmentStatusLkp.lookupValueCode='TERM' " +
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:terminationDate, 'MM/DD/YYYY')))"
			)
	List<Enrollment> findActiveEnrollmentBySsapApplicationID(@Param("ssapApplicationId") Long ssapApplicationId ,  @Param("terminationDate") String terminationDateString);

	@Query("FROM Enrollment as en WHERE en.houseHoldCaseId = :householdCaseId")
	List<Enrollment> findEnrollmentByHouseholdCaseId(@Param("householdCaseId") String householdCaseId);

	@Query("SELECT DISTINCT en.houseHoldCaseId from Enrollment as en " +
			" WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "+
			" AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') "+
			" AND en.planLevel NOT IN ('CATASTROPHIC') "+
			" AND en.insuranceTypeLkp.lookupValueCode = 'HLT' "+
			" AND en.enrollmentConfirmationDate IS NOT NULL")
	List<String> getUniqueHouseHoldIDList(@Param("startDate") String startDate, @Param("endDate") String endDateString);

	/*@Query(" SELECT DISTINCT en FROM Enrollment as en JOIN FETCH en.enrollees where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
			" AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING') " +
			" AND en.enrollmentTypeLkp.lookupValueCode IN ('FI')" +
			" AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate ")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForConfirm(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId,@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(" FROM Enrollment as en where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
		   " AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('PAYMENT_RECEIVED') " +
		   " AND en.enrollmentTypeLkp.lookupValueCode IN ('24') " +
		   " AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate ")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForConfirmShop(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId,@Param("startDate") Date startDate, @Param("endDate") Date endDate);*/


	/*@Query(" SELECT DISTINCT en FROM Enrollment as en JOIN FETCH en.enrollees where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
		   " AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING') " +
		   " AND en.benefitEffectiveDate =  :effectiveEndDate ")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForCancel(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId,@Param("effectiveEndDate") Date effectiveEndDate);

	@Query(" SELECT DISTINCT en FROM Enrollment as en JOIN FETCH en.enrollees where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
		   " AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') " +
		   " AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate ")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForTerm(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId,@Param("startDate") Date startDate, @Param("endDate") Date endDate);
*/
	@Query(" SELECT DISTINCT en FROM Enrollment as en JOIN FETCH en.enrollees where en.id = :id " +
		   " AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING') " +
		   " AND en.benefitEffectiveDate =  :effectiveEndDate ")
	List<Enrollment> getEnrollmentByEnrollmentIDForCancel(@Param("id")Integer id,@Param("effectiveEndDate") Date effectiveEndDate);

	@Query(" SELECT DISTINCT en FROM Enrollment as en JOIN FETCH en.enrollees where en.id = :id " +
		   " AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') " +
		   " AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate ")
	List<Enrollment> getEnrollmentByEnrollmentIdForTerm(@Param("id")Integer id,@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(" SELECT en FROM Enrollment as en JOIN FETCH en.enrollees where en.id = :id " +
		   " AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING') " +
		   " AND en.enrollmentTypeLkp.lookupValueCode IN ('FI')" +
		   " AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate ")
	List<Enrollment> getEnrollmentByEnrollmentIdForConfirm(@Param("id")Integer id,@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(" FROM Enrollment as en WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
		   " AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "+
		   //" AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE( TO_CHAR(:endDate,'MM/DD/YYYY'), 'MM/DD/YYYY') "+
		   " AND en.enrollmentStatusLkp.lookupValueCode IN ('PAYMENT_RECEIVED','CONFIRM','TERM') " +
		   " AND  en.planLevel NOT IN ('CATASTROPHIC') " +
		   " AND en.employerEnrollmentId = :employerEnrollmentId "+
		   " AND en.insuranceTypeLkp.lookupValueCode = 'HLT' ")
	List<Enrollment> getEnrollmentByEmployer(@Param("employerEnrollmentId")Integer employerEnrollmentId, @Param("startDate") String startDateString, @Param("endDate") String endDateString);

	@Query(" FROM Enrollment as en WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			   " AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "+
			   //" AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE( TO_CHAR(:endDate,'MM/DD/YYYY'), 'MM/DD/YYYY') "+
			   " AND en.enrollmentStatusLkp.lookupValueCode IN ('PAYMENT_RECEIVED','CONFIRM','TERM') " +
			   " AND  en.planLevel NOT IN ('CATASTROPHIC') " +
			   " AND en.employerEnrollmentId IN (:employerEnrollmentIdList) "+
			   " AND en.insuranceTypeLkp.lookupValueCode = 'HLT' ")
	List<Enrollment> getEnrollmentByEmployer(@Param("employerEnrollmentIdList")List<Integer> employerEnrollmentIdList, @Param("startDate") String startDateString, @Param("endDate") String endDateString);
	
	@Query(" SELECT DISTINCT en.employerEnrollmentId from Enrollment as en " +
		   " WHERE en.enrollmentStatusLkp.lookupValueCode IN ('PAYMENT_RECEIVED','CONFIRM','TERM') " +
		   " AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
		   " AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "+
		   " AND en.insuranceTypeLkp.lookupValueCode = 'HLT' ")
	List<Integer> getUniqueEmployerEnrollmentIDList(@Param("startDate") String startDateString, @Param("endDate") String endDateString);

	@Query(" Select count(en) " +
			   " FROM  Enrollment as en " +
			   " where ((en.enrollmentStatusLkp.lookupValueCode in ('CONFIRM', 'PENDING')) " +
			   " OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND en.benefitEndDate >= :termCondDate)) " +
			   " AND en.ssapApplicationid = :applicationID ")
	Long getActiveEnrollmentsForApplicationID(@Param("applicationID") Long applicationID, @Param("termCondDate") Date termCondDate);

	@Query(" Select count(en) " +
			   " FROM  Enrollment as en " +
			   " where en.ssapApplicationid = :applicationID " +
			   " AND en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND TO_CHAR(en.benefitEndDate,'MM/DD/YYYY') = :coverageYear ")
	Long getActiveEnrollmentsForApplicationID(@Param("applicationID") Long applicationID, @Param("coverageYear") String coverageYear);

	
	@Query(" SELECT DISTINCT en from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND en.employerEnrollmentId = :employerEnrollmentId " +
			   " AND en.brokerRole = :assisterBrokerRole "+
			   " AND en.assisterBrokerId = :assisterBrokerId "+
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate, 'MM/DD/YYYY') ")
	List<Enrollment> getShopEnrollmentToRemoveBroker(@Param("employerEnrollmentId") Integer employerEnrollmentId, @Param("endDate") String endDate,@Param("assisterBrokerRole") String assisterBrokerRole, @Param("assisterBrokerId") Integer assisterBrokerId);

	@Query(" SELECT DISTINCT en from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND en.employerEnrollmentId = :employerEnrollmentId " +
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate, 'MM/DD/YYYY') ")
	List<Enrollment> getShopEnrollmentToAddBroker(@Param("employerEnrollmentId") Integer employerEnrollmentId, @Param("endDate") String endDate);

	@Query(" SELECT DISTINCT en from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND en.houseHoldCaseId = :houseHoldCaseId " +
			   " AND en.brokerRole = :assisterBrokerRole "+
			   " AND en.assisterBrokerId = :assisterBrokerId "+
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate, 'MM/DD/YYYY') ")
	List<Enrollment> getIndivEnrollmentToRemoveBroker(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("endDate") String endDateString,@Param("assisterBrokerRole") String assisterBrokerRole, @Param("assisterBrokerId") Integer assisterBrokerId);

	@Query(" SELECT DISTINCT en from Enrollment as en " +
			   " WHERE en.id IN (SELECT DISTINCT(ee.enrollment.id) FROM Enrollee as ee WHERE ee.exchgIndivIdentifier =:exchgIndivIdentifier) " +
			   " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND en.brokerRole = :assisterBrokerRole "+
			   " AND en.assisterBrokerId = :assisterBrokerId "+
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate, 'MM/DD/YYYY') ")
	List<Enrollment> getIndivEnrollmentByExchgIdentifierToRemoveBroker(@Param("exchgIndivIdentifier") String exchgIndivIdentifier, @Param("endDate") String endDateString,@Param("assisterBrokerRole") String assisterBrokerRole, @Param("assisterBrokerId") Integer assisterBrokerId);
	
	@Query(" SELECT DISTINCT en from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND en.houseHoldCaseId = :houseHoldCaseId " +
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate, 'MM/DD/YYYY') ")
	List<Enrollment> getIndivEnrollmentToAddBroker(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("endDate") String endDateString);
	
	@Query(" SELECT DISTINCT en from Enrollment as en " +
			   " WHERE en.id IN (SELECT DISTINCT(ee.enrollment.id) FROM Enrollee as ee WHERE ee.exchgIndivIdentifier =:exchgIndivIdentifier) " +
			   " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate, 'MM/DD/YYYY') ")
	List<Enrollment> getIndivEnrollmentByExchgIdentiferToAddBroker(@Param("exchgIndivIdentifier") String exchgIndivIdentifier, @Param("endDate") String endDateString);

	@Query("FROM Enrollment as en where en.id = :enrollmentId")
	Enrollment getEnrollmentByEnrollmentId(@Param("enrollmentId")Integer enrollmentId);


	@Query("SELECT en.benefitEffectiveDate from Enrollment as en "
			+ "WHERE en.houseHoldCaseId = :houseHoldCaseId "
			+ "AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') "
			+ "AND en.insuranceTypeLkp.lookupValueCode = 'HLT' ")
	List<Date> findBenefitEffDateByHouseholdCaseID(@Param("houseHoldCaseId") String houseHoldCaseId);
	
	@Query("SELECT en.enrollmentConfirmationDate from Enrollment as en WHERE en.id = :enrollmentId ")
	List<Date> findEnrollmentConfirmationDateById(@Param("enrollmentId") Integer enrollmentId);

	@Query("SELECT DISTINCT en.id FROM Enrollment as en "
			+ "WHERE en.houseHoldCaseId = :householdId "
			+ "and TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ "and TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "
			+ "and en.insuranceTypeLkp.lookupValueCode= :insuranceType and en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') "
			+ "and en.planLevel NOT IN ('CATASTROPHIC') "
			+ "and en.enrollmentConfirmationDate IS NOT NULL")
	List<Integer> getDistinctEnrollmentsActiveForMonth(@Param("householdId") String householdId, @Param("endDate") String endDateString,  @Param("startDate") String startDate , @Param("insuranceType") String insuranceType);

	@Query("select e from Enrollment e " +
			"where TO_DATE(TO_CHAR(e.submittedToCarrierDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') " +
			"and TO_DATE(TO_CHAR(e.submittedToCarrierDate,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE(:endDate, 'MM/DD/YYYY') ")
	List<Enrollment> findEnrollmentBySubmittedToCarrierDateBetween(@Param("startDate") String startDateString, @Param("endDate") String endDateString);

    @Query("select e from Enrollment e " +
        "where TO_DATE(TO_CHAR(e.submittedToCarrierDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') " +
        "and TO_DATE(TO_CHAR(e.submittedToCarrierDate,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE(:endDate, 'MM/DD/YYYY') " +
        "and e.enrollmentStatusLkp.lookupValueCode in(:status)")
    List<Enrollment> findEnrollmentBySubmittedToCarrierDateBetweenAndStatusIn(@Param("startDate") String startDateString, @Param("endDate") String endDateString, @Param("status") List<String> status);

	@Query("select e from Enrollment e " +
			"where TO_DATE(TO_CHAR(e.submittedToCarrierDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') " +
			"and TO_DATE(TO_CHAR(e.submittedToCarrierDate,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE(:endDate, 'MM/DD/YYYY') " +
			"and e.enrollmentStatusLkp.lookupValueCode not in(:status)")
	List<Enrollment> findEnrollmentBySubmittedToCarrierDateBetweenAndStatusNotIn(@Param("startDate") String startDateString, @Param("endDate") String endDateString, @Param("status") List<String> status);


	@Query("select e from Enrollment e where e.createdOn >= :date")
	List<Enrollment> findByCreateTimestampAfter(@Param("date") Date date);

	@Query("select e from Enrollment e where e.submittedToCarrierDate > :date")
	List<Enrollment> findBySubmittedToCarrierDateAfter(@Param("date") Date date);
	
	@Query(" SELECT en.enrollmentStatusLkp.lookupValueLabel " +
		      " FROM Enrollment as en "+
		      " WHERE en.id = (select max(id) FROM Enrollment as enl where  enl.employeeId = :employeeId " +
		      " AND insuranceTypeLkp.lookupValueCode = 'HLT' " +
		      " AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM', 'TERM') )")
		 String findEnrollmentStatusByEmployeeId(@Param("employeeId") Integer employeeId);
	
	@Query("SELECT DISTINCT en.houseHoldCaseId from Enrollment as en " +
			" WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate,'MM/DD/YYYY') "+
			" AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM')"+
			" AND en.planLevel NOT IN ('CATASTROPHIC') "+
			" AND en.insuranceTypeLkp.lookupValueCode = 'HLT' ")
	List<String> getUniqueConfirmedHouseHoldIDList(@Param("startDate") String startDateString, @Param("endDate") String endDateString);
	
	@Query("SELECT COUNT(*) from Enrollment as en " +
			" WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "+
			" AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM')"+
			" AND en.planLevel NOT IN ('CATASTROPHIC') "+
			" AND en.insuranceTypeLkp.lookupValueCode = 'HLT'"+ 
			" AND en.houseHoldCaseId = :householdCaseId")
	Long getCountOfConfirmedEnrollments(@Param("householdCaseId") String householdCaseId, @Param("startDate") String startDateString, @Param("endDate") String endDateString);
	
	@Query(" SELECT en.enrollmentStatusLkp.lookupValueLabel, en.planName, en.benefitEndDate, en.employerContribution"
			+ " FROM Enrollment as en WHERE en.id = :enrollmentId ")
	List<Object[]> getEnrollmentDataForBroker(@Param("enrollmentId") Integer enrollmentId);
	
	@Query("Select distinct en.id FROM Enrollment as en where "+
			   " en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','PENDING' )"+
			   " and to_char(en.benefitEndDate, 'ddMMYYYY') = :dateYear ")
	List<Integer> getEnrollmentIdsForAutoTermination(@Param("dateYear") String dateYear);
	
	@Query("Select en.priorEnrollmentId FROM Enrollment as en where "+
		   " en.renewalFlag is not null " +
			" and to_char(en.benefitEffectiveDate, 'yyyy') = :dateYear " +
		    " and en.priorEnrollmentId is not null ")
	List<Long> getPriorEnrollmentId(@Param("dateYear") String dateYear);
	
	@Query("Select en.priorEnrollmentId FROM Enrollment as en where "+
			   " en.renewalFlag is not null " +
				" and to_char(en.benefitEffectiveDate, 'yyyy') = :dateYear " +
			    " and en.priorEnrollmentId is not null AND hiosIssuerId in (:hiosIssuerId)")
	List<Long> getPriorEnrollmentIdUsingHiosId(@Param("dateYear") String dateYear, @Param("hiosIssuerId") String hiosIssuerId);
	
	/**
	 * Jira ID: HIX-71256
	 * @param houseHoldCaseIdList List<String>
	 * @param status List<String>
	 * @param systemDate Date
	 * @return List<Object>
	 */
	@Query("SELECT en.id, en.planId, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate, en.grossPremiumAmt,"
			+ " en.netPremiumAmt, en.enrollmentStatusLkp.lookupValueCode, en.createdOn, en.insuranceTypeLkp.lookupValueLabel, en.planLevel,"
			+ " en.planName, en.insurerName, en.hiosIssuerId, en.aptcAmt, en.renewalFlag, en.priorEnrollmentId, en.CMSPlanID, en.updatedOn, "
			+ " en.updatedBy.id, en.submittedToCarrierDate, en.createdBy.id, en.csrAmt, en.paymentTxnId,en.ssapApplicationid, ssap.caseNumber,"
			+ " (lastUpdatedByUser.firstName || ' '|| lastUpdatedByUser.lastName)as LastUpdatedByUserName ,"
			+ " (createdByUser.firstName ||' '|| createdByUser.lastName) as SubmittedByUserName, "
			+ " en.issuerId as issuerId, en.enrollmentConfirmationDate, en.slcspAmt, en.ehbAmt, en.dntlEhbAmt, en.ehbPercent, en.dntlEssentialHealthBenefitPrmDollarVal, "
			+ " en.agentBrokerName, "
			+ " en.assisterBrokerId, "
			+ " en.brokerTPAAccountNumber1, "
			+ " en.priorSsapApplicationid, "
			+ "	en.changePlanAllowed, "
			+ "	en.externalHouseHoldCaseId, "
			+ " en.stateSubsidyAmt"
			+ " FROM Enrollment as en, SsapApplication as ssap, AccountUser lastUpdatedByUser, AccountUser createdByUser"
			+ " where en.houseHoldCaseId in (:houseHoldCaseIdList) AND"
			+ " en.ssapApplicationid = ssap.id AND"
			+ " en.createdBy.id = createdByUser.id AND"
			+ " en.updatedBy.id = lastUpdatedByUser.id AND"
			+ " en.enrollmentTypeLkp.lookupValueCode IN ('FI') AND"
			+" ((en.enrollmentStatusLkp.lookupValueCode not in (:status))"
			+ " OR ((en.enrollmentStatusLkp.lookupValueCode in ('TERM')) AND (:systemDate <= en.benefitEndDate)))")
	List<Object[]> getActiveEnrollmentDataByHouseHoldCaseId(@Param("houseHoldCaseIdList") List<String> houseHoldCaseIdList,
			@Param("status") List<String> status,
			@Param("systemDate") Date systemDate);
	
	
	/**
	 * Jira ID: HIX-71256	
	 * @param houseHoldCaseIdList List<String>
	 * @param statusList List<String>
	 * @param coverageYear String
	 * @return List<Object>
	 */
	@Query("SELECT en.id, en.planId, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate, en.grossPremiumAmt,"
			+ " en.netPremiumAmt, en.enrollmentStatusLkp.lookupValueCode, en.createdOn, en.insuranceTypeLkp.lookupValueLabel, en.planLevel,"
			+ " en.planName, en.insurerName, en.hiosIssuerId, en.aptcAmt, en.renewalFlag, en.priorEnrollmentId, en.CMSPlanID, en.updatedOn, "
			+ " en.updatedBy.id, en.submittedToCarrierDate, en.createdBy.id, en.csrAmt, en.paymentTxnId, en.ssapApplicationid, ssap.caseNumber,"
			+ " (lastUpdatedByUser.firstName || ' '|| lastUpdatedByUser.lastName)as LastUpdatedByUserName,"
			+ " (createdByUser.firstName ||' '|| createdByUser.lastName) as SubmittedByUserName, "
			+ " en.issuerId as issuerId, en.enrollmentConfirmationDate, en.slcspAmt, en.ehbAmt, en.dntlEhbAmt, en.ehbPercent, en.dntlEssentialHealthBenefitPrmDollarVal, "
			+ " en.agentBrokerName, "
			+ " en.assisterBrokerId, "
			+ " en.brokerTPAAccountNumber1, "
			+ " en.priorSsapApplicationid, "
			+ "	en.changePlanAllowed, "
			+ "	en.externalHouseHoldCaseId, "
			+ " en.stateSubsidyAmt "
			+ " FROM Enrollment as en, SsapApplication as ssap, AccountUser lastUpdatedByUser, AccountUser createdByUser"
			+ " where en.houseHoldCaseId in (:houseHoldCaseIdList) AND"
			+ " en.createdBy.id = createdByUser.id AND"
			+ " en.updatedBy.id = lastUpdatedByUser.id AND"
			+ " en.enrollmentTypeLkp.lookupValueCode IN ('FI') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode IN (:status) AND"
			+ " TO_CHAR(en.benefitEffectiveDate, 'yyyy') = :coverageYear AND"
			+ " en.ssapApplicationid = ssap.id")
	List<Object[]> getEnrollmentDataByHouseHoldCaseIdNCoverageYearNStatus(@Param("houseHoldCaseIdList") List<String> houseHoldCaseIdList,
			@Param("status") List<String> statusList,
			@Param("coverageYear") String coverageYear);
	
	/**
	 * Jira ID: HIX-71256
	 * @param houseHoldCaseIdList List<String>
	 * @param statusList List<String>
	 * @return List<Object[]>
	 */
	@Query("SELECT en.id, en.planId, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate, en.grossPremiumAmt,"
			+ " en.netPremiumAmt, en.enrollmentStatusLkp.lookupValueCode, en.createdOn, en.insuranceTypeLkp.lookupValueLabel, en.planLevel,"
			+ " en.planName, en.insurerName, en.hiosIssuerId, en.aptcAmt, en.renewalFlag, en.priorEnrollmentId, en.CMSPlanID, en.updatedOn, "
			+ " en.updatedBy.id, en.submittedToCarrierDate, en.createdBy.id, en.csrAmt, en.paymentTxnId, en.ssapApplicationid, ssap.caseNumber,"
			+ " (lastUpdatedByUser.firstName || ' '|| lastUpdatedByUser.lastName)as LastUpdatedByUserName,"
			+ " (createdByUser.firstName ||' '|| createdByUser.lastName) as SubmittedByUserName, "
			+ " en.issuerId as issuerId, en.enrollmentConfirmationDate, en.slcspAmt, en.ehbAmt, en.dntlEhbAmt, en.ehbPercent, en.dntlEssentialHealthBenefitPrmDollarVal, "
			+ " en.agentBrokerName, "
			+ " en.assisterBrokerId, "
			+ " en.brokerTPAAccountNumber1, "
			+ " en.priorSsapApplicationid, "
			+ "	en.changePlanAllowed, "
			+ "	en.externalHouseHoldCaseId, "
			+ " en.stateSubsidyAmt "
			+ " FROM Enrollment as en, SsapApplication as ssap, AccountUser lastUpdatedByUser, AccountUser createdByUser"
			+ " where en.houseHoldCaseId in (:houseHoldCaseIdList) AND"
			+ " en.ssapApplicationid = ssap.id AND"
			+ " en.createdBy.id = createdByUser.id AND"
			+ " en.updatedBy.id = lastUpdatedByUser.id AND"
			+ " en.enrollmentTypeLkp.lookupValueCode IN ('FI') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode IN (:status)")
	List<Object[]> getEnrollmentDataByHouseHoldCaseIdNStatus(@Param("houseHoldCaseIdList") List<String> houseHoldCaseIdList,
			@Param("status") List<String> statusList);
	
	@Query("SELECT en.id, en.planId, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate, en.grossPremiumAmt,"
			+ " en.netPremiumAmt, en.enrollmentStatusLkp.lookupValueCode, en.createdOn, en.insuranceTypeLkp.lookupValueLabel, en.planLevel,"
			+ " en.planName, en.insurerName, en.hiosIssuerId, en.aptcAmt, en.renewalFlag, en.priorEnrollmentId, en.CMSPlanID, en.updatedOn, "
			+ " en.updatedBy.id, en.submittedToCarrierDate, en.createdBy.id, en.csrAmt, en.paymentTxnId, en.ssapApplicationid, en.ssapApplicationid as caseNumber,"
			+ " (lastUpdatedByUser.firstName || ' '|| lastUpdatedByUser.lastName)as LastUpdatedByUserName,"
			+ " (createdByUser.firstName ||' '|| createdByUser.lastName) as SubmittedByUserName, "
			+ " en.issuerId as issuerId, en.enrollmentConfirmationDate, en.slcspAmt, en.ehbAmt, en.dntlEhbAmt, en.ehbPercent, en.dntlEssentialHealthBenefitPrmDollarVal, "
			+ " en.agentBrokerName, "
			+ " en.assisterBrokerId, "
			+ " en.brokerTPAAccountNumber1, "
			+ " en.priorSsapApplicationid, "
			+ "	en.changePlanAllowed, "
			+ "	en.externalHouseHoldCaseId, "
			+ " en.stateSubsidyAmt "
			+ " FROM Enrollment as en, AccountUser lastUpdatedByUser, AccountUser createdByUser"
			+ " where en.houseHoldCaseId in (:houseHoldCaseIdList) AND"
			+ " en.createdBy.id = createdByUser.id AND"
			+ " en.updatedBy.id = lastUpdatedByUser.id AND"
			+ " en.enrollmentTypeLkp.lookupValueCode IN ('FI') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode IN (:status)")
	List<Object[]> getEnrollmentDataByHouseHoldCaseIdNStatusCA(@Param("houseHoldCaseIdList") List<String> houseHoldCaseIdList,
			@Param("status") List<String> statusList);

	// TODO remove this after 19.3 data migration 
	@Query("SELECT en.id, en.planId, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate, en.grossPremiumAmt,"
			+ " en.netPremiumAmt, en.enrollmentStatusLkp.lookupValueCode, en.createdOn, en.insuranceTypeLkp.lookupValueLabel, en.planLevel,"
			+ " en.planName, en.insurerName, en.hiosIssuerId, en.aptcAmt, en.renewalFlag, en.priorEnrollmentId, en.CMSPlanID, en.updatedOn, "
			+ " en.updatedBy.id, en.submittedToCarrierDate, en.createdBy.id, en.csrAmt, en.paymentTxnId, en.ssapApplicationid, en.ssapApplicationid as caseNumber,"
			+ " (lastUpdatedByUser.firstName || ' '|| lastUpdatedByUser.lastName)as LastUpdatedByUserName,"
			+ " (createdByUser.firstName ||' '|| createdByUser.lastName) as SubmittedByUserName, "
			+ " en.issuerId as issuerId, en.enrollmentConfirmationDate, en.slcspAmt, en.ehbAmt, en.dntlEhbAmt, en.ehbPercent, en.dntlEssentialHealthBenefitPrmDollarVal, "
			+ " en.agentBrokerName, "
			+ " en.assisterBrokerId, "
			+ " en.brokerTPAAccountNumber1, "
			+ " en.priorSsapApplicationid, "
			+ "	en.changePlanAllowed, "
			+ "	en.externalHouseHoldCaseId, "
			+ " en.stateSubsidyAmt "
			+ " FROM Enrollment as en, AccountUser lastUpdatedByUser, AccountUser createdByUser"
			+ " where en.houseHoldCaseId in (:houseHoldCaseIdList) AND"
			+ " en.createdBy.id = createdByUser.id AND"
			+ " en.updatedBy.id = lastUpdatedByUser.id AND"
			+ " en.enrollmentTypeLkp.lookupValueCode IN ('FI') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode IN (:status) AND TO_CHAR(en.benefitEffectiveDate, 'yyyy') = :coverageYear")
	List<Object[]> getEnrollmentDataByHouseHoldCaseIdNCoverageYearNStatusCA(@Param("houseHoldCaseIdList") List<String> houseHoldCaseIdList,
			@Param("status") List<String> statusList,
			@Param("coverageYear") String coverageYear);
	
	
	/**
	 * HIX-70722 Repository to return enrollment list for a tenant request
	 * @param enrollmentId
	 * @param leadId 
	 * @param affiliateId
	 * @return Enrollment
	 */
	@Query("FROM Enrollment as en where en.id = :enrollmentId and en.cmrHouseHoldId = (Select hh.id from Household as hh where hh.eligLead.id = :leadId and hh.eligLead.affiliateId = :affiliateId)")
	Enrollment getEnrollmentForTenant(@Param("enrollmentId")Integer enrollmentId, @Param("leadId")Long leadId, @Param("affiliateId") Long affiliateId);
	
	/**
	 * Jira ID: HIX-71256
	 * @param houseHoldCaseIdList List<String>
	 * @return List<Object[]>
	 */
	@Query("SELECT en.id, en.planId, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate, en.grossPremiumAmt,"
			+ " en.netPremiumAmt, en.enrollmentStatusLkp.lookupValueCode, en.createdOn, en.insuranceTypeLkp.lookupValueLabel, en.planLevel,"
			+ " en.planName, en.insurerName, en.hiosIssuerId, en.aptcAmt, en.renewalFlag, en.priorEnrollmentId, en.CMSPlanID, en.updatedOn, "
			+ " en.updatedBy.id, en.submittedToCarrierDate, en.createdBy.id, en.csrAmt, en.paymentTxnId, en.ssapApplicationid, ssap.caseNumber,"
			+ " (lastUpdatedByUser.firstName || ' '|| lastUpdatedByUser.lastName)as LastUpdatedByUserName,"
			+ " (createdByUser.firstName ||' '|| createdByUser.lastName) as SubmittedByUserName, "
			+ " en.issuerId as issuerId, en.enrollmentConfirmationDate, en.slcspAmt, en.ehbAmt, en.dntlEhbAmt, en.ehbPercent, en.dntlEssentialHealthBenefitPrmDollarVal, "
			+ " en.agentBrokerName, "
			+ " en.assisterBrokerId, "
			+ " en.brokerTPAAccountNumber1, "
			+ " en.priorSsapApplicationid, "
			+ "	en.changePlanAllowed, "
			+ "	en.externalHouseHoldCaseId, "
			+ " en.stateSubsidyAmt "
			+ " FROM Enrollment as en, SsapApplication as ssap, AccountUser lastUpdatedByUser, AccountUser createdByUser"
			+ " where en.houseHoldCaseId in (:houseHoldCaseIdList) AND"
			+ " en.ssapApplicationid = ssap.id AND"
			+ " en.createdBy.id = createdByUser.id AND"
			+ " en.updatedBy.id = lastUpdatedByUser.id AND"
			+ " en.enrollmentTypeLkp.lookupValueCode IN ('FI') AND"
			+ " en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED')")
	List<Object[]> getEnrollmentDataByHouseHoldCaseId(@Param("houseHoldCaseIdList") List<String> houseHoldCaseIdList);
	
	/**
	 * Jira ID: HIX-72324 [Enrollment API for Renewal]
	 * @param ssapApplicationid Long
	 * @return List<Object[]>
	 */
	@Query("SELECT en.id, en.enrollmentStatusLkp.lookupValueCode, en.planId, en.CMSPlanID, en.benefitEffectiveDate, en.benefitEndDate, en.createdOn "
			+ " FROM Enrollment as en WHERE en.ssapApplicationid= :ssapApplicationid AND en.insuranceTypeLkp.lookupValueCode = 'HLT'")
	List<Object[]> getEnrollmentDataBySsapApplicationId(@Param("ssapApplicationid")Long ssapApplicationid);
	
	

	
	/**
	 * Jira : HIX-108666 - returns count and enrollment data for both health and dental based on ssap_application_id
	 * @param ssapApplicationid
	 * @return List<Object[]>
	 *
	 */
	
	@Query("SELECT count(*),en.insuranceTypeLkp.lookupValueLabel "
			+ "FROM Enrollment as en "
			+ "WHERE en.ssapApplicationid= :ssapApplicationid "
			+ "AND en.insuranceTypeLkp.lookupValueCode in ('HLT','DEN') "
			+ "AND ((en.enrollmentStatusLkp.lookupValueCode in ('CONFIRM','PENDING'))"
			+ "OR ((en.enrollmentStatusLkp.lookupValueCode in ('TERM')) AND (en.benefitEndDate>= :sixtyDayBackDate)))" 
			+ "group by en.insuranceTypeLkp.lookupValueLabel ")
	List<Object[]> getAllEnrollmentDataBySsapApplicationId(@Param("ssapApplicationid")Long ssapApplicationid ,@Param("sixtyDayBackDate")Date sixtyDayBackDate);
	

	@Query("SELECT DISTINCT en.id from Enrollment as en " +
			" WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:currentYearStartDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:nextYearStartDate, 'MM/DD/YYYY') "+
			" AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') "+
			" AND en.planLevel NOT IN ('CATASTROPHIC') "+
			" AND en.insuranceTypeLkp.lookupValueCode = 'HLT' "+
			" AND en.enrollmentConfirmationDate IS NOT NULL")
	List<Integer> getEnrollmentsForAnnualIrsReport(@Param("currentYearStartDate") String currentYearStartDateString, @Param("nextYearStartDate") String nextYearStartDateString);
	
	/**
	 * Jira Id: 
	 * @param houseHoldCaseIdList
	 * @return
	 */
	@Query("SELECT en.id, en.planId, en.insuranceTypeLkp.lookupValueLabel, en.netPremiumAmt, en.houseHoldCaseId, en.ssapApplicationid,"
			+ " en.grossPremiumAmt, en.aptcAmt, (SELECT COUNT(DISTINCT ee.exchgIndivIdentifier) FROM Enrollee ee WHERE "
			+ " ee.enrollment.id = en.id AND ee.personTypeLkp.lookupValueCode NOT IN (:personTypeList) AND ee.enrolleeLkpValue.lookupValueCode NOT IN('CANCEL', 'ABORTED'))"
			+ " FROM Enrollment as en "
			+ " WHERE en.houseHoldCaseId in (:houseHoldCaseIdList) AND en.enrollmentTypeLkp.lookupValueCode IN ('FI') AND"
			+ " ((en.enrollmentStatusLkp.lookupValueCode not in (:status))"
			+ " OR ((en.enrollmentStatusLkp.lookupValueCode in ('TERM')) AND (:systemDate <= en.benefitEndDate)))")
	List<Object[]>getEnrollmentDataForBrokerByHouseHoldCaseId(@Param("houseHoldCaseIdList") List<String> houseHoldCaseIdList, @Param("status") List<String> status,
			@Param("systemDate") Date systemDate, @Param("personTypeList") List<String> personTypeList);
	
	@Query("SELECT DISTINCT en.id from Enrollment as en " +
			" WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:currentYearStartDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:nextYearStartDate, 'MM/DD/YYYY') "+
			" AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') "+
			" AND en.planLevel NOT IN ('CATASTROPHIC') "+
			" AND en.insuranceTypeLkp.lookupValueCode = 'HLT' "+
			" AND en.enrollmentConfirmationDate IS NOT NULL"+
			" AND en.updatedOn >= :lastJobRunDate")
	List<Integer> getEnrollmentsForAnnualIrsReport(@Param("currentYearStartDate") String currentYearStartDateString, @Param("nextYearStartDate") String nextYearStartDateString, @Param("lastJobRunDate") Date lastJobRunDate);
	
	@Query("SELECT en.id FROM Enrollment as en "
			+ "WHERE en.houseHoldCaseId = :householdId "
			+ "and TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ "and TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "
			+ "and en.insuranceTypeLkp.lookupValueCode= :insuranceType and en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') "
			+ "and en.planLevel NOT IN ('CATASTROPHIC') "
			+ "and en.enrollmentConfirmationDate IS NOT NULL")
	List<Integer> getEnrollmentIdsForAnnualIRS(@Param("householdId") String householdId, @Param("endDate") String endDateString,  @Param("startDate") String startDateString, @Param("insuranceType") String insuranceType);
	
	/**
	 * 
	 * @param ssapApplicationId
	 * @return
	 */
	@Query(" FROM Enrollment as en WHERE en.ssapApplicationid =:ssapApplicationId " +
			" AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL', 'ABORTED')")
	List<Enrollment> getEnrollmentNotInCancelAndAbortBySsapApplicationId(@Param("ssapApplicationId") long ssapApplicationId);
	
	@Query(" FROM Enrollment as en WHERE en.ssapApplicationid =:ssapApplicationId " +
		   " AND en.insuranceTypeLkp.lookupValueCode= :insuranceType "+
		   " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL', 'ABORTED')")
	List<Enrollment> getEnrollmentNotInCancelAndAbortBySsapApplicationIdAndType(@Param("ssapApplicationId") long ssapApplicationId, 
			                                                                    @Param("insuranceType") String insuranceType);
	@Query(" SELECT DISTINCT en.employerEnrollmentId, en.employer.id from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode IN ('PAYMENT_RECEIVED','CONFIRM','TERM') " +
			   " AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			   " AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "+
			   " AND en.insuranceTypeLkp.lookupValueCode = 'HLT' ")
	List<Object[]> getEmployerIdEmployerEnrollmentIdMapList(@Param("startDate") String startDateString, @Param("endDate") String endDateString);
	
	@Query(" SELECT DISTINCT en.employerEnrollmentId, en.employer.id from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode IN ('PAYMENT_RECEIVED','CONFIRM','TERM') " +
			   " AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			   " AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "+
			   " AND en.insuranceTypeLkp.lookupValueCode = 'HLT' "+
			   " AND en.employerEnrollmentId IN (:empEnrlIdList)")
	List<Object[]> getEmployerIdEmployerEnrollmentIdMapList(@Param("startDate") String startDateString, @Param("endDate")String endDateString, @Param("empEnrlIdList") List<Integer> empEnrlIdList);
	
	@Query("SELECT MAX(en.employeeId) FROM Enrollment en where en.employeeAppId=:employeeAppId ")
	Integer getEmployerIdByEmployeeApplicationId(@Param("employeeAppId") Long employeeAppId);
	
	@Query("SELECT en.id FROM Enrollment as en "
			+" WHERE en.enrollmentStatusLkp.lookupValueCode <> 'ABORTED' "
			+" AND en.insuranceTypeLkp.lookupValueCode = 'HLT' "
			+" AND TO_CHAR(en.benefitEffectiveDate, 'YYYY') = :coverageYear")
	List<Integer> getUniqueHealthEnrollmentIds(@Param("coverageYear") String coverageYear);
	
	@Query(" FROM Enrollment as en WHERE en.CMSPlanID =:cmsPlanId " +
			" AND TO_CHAR(en.benefitEffectiveDate, 'YYYY') = :coverageYear " + 
			" AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') > TO_DATE(:terminationDate, 'MM/DD/YYYY')" +
			" AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL', 'ABORTED')")
	List<Enrollment> getEnrollmentByCmsPlanId(@Param("cmsPlanId") String cmsPlanId, 
			                               		@Param("coverageYear") String coverageYear, 
			                               			@Param("terminationDate") String terminationDate);
	
	@Query(" SELECT COUNT(*) from Enrollment as en " +
			" WHERE en.CMSPlanID =:cmsPlanId " +
			" AND TO_CHAR(en.benefitEffectiveDate, 'YYYY') = :coverageYear " + 
			" AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') > TO_DATE(:terminationDate, 'MM/DD/YYYY')" +
			" AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL', 'ABORTED')")
	Long getEnrollmentCountByCmsPlanId(@Param("cmsPlanId") String cmsPlanId, 
			                               		@Param("coverageYear") String coverageYear, 
			                               			@Param("terminationDate") String terminationDate);
	
	@Query("SELECT houseHoldCaseId, insurerName FROM Enrollment as en where en.id = :enrollmentId")
	List<Object[]> getHouseholdCaseIdAndInsurerName(@Param("enrollmentId") Integer enrollmentId);
	
	@Query( " FROM Enrollment as en WHERE en.houseHoldCaseId = :householdId " +
			" AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "+
			" AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL', 'ABORTED') ORDER BY en.createdOn DESC")
	List<Enrollment> getEnrollmentByByHouseHoldCaseId(@Param("householdId") String householdId, @Param("startDate") String startDate);
	
	@Query(" SELECT count(*) FROM Enrollment as en " +
			   " WHERE en.houseHoldCaseId = :houseHoldCaseId " +
			   " AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','CONFIRM','TERM')  "+
			   " AND en.id not in(:enrollmentId) "+
			   " AND en.insuranceTypeLkp.lookupValueCode= :insuranceType "+
			   " AND :startDateProposed <= en.benefitEndDate "+
			   " AND :endDateProposed >= en.benefitEffectiveDate "
			   )
	Long getEnrollmentOverlapCount(@Param("enrollmentId") Integer enrollmentId, 
			@Param("houseHoldCaseId") String houseHoldCaseId, 
			@Param("insuranceType") String insuranceType,  
			@Param("startDateProposed") Date startDateProposed, 
			@Param("endDateProposed") Date endDateProposed);
	
	@Query(" SELECT count(*) FROM Enrollment as en " +
			   " WHERE en.houseHoldCaseId = :houseHoldCaseId " +
			   " AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING', 'CONFIRM', 'TERM')  "+			   
			   " AND en.insuranceTypeLkp.lookupValueCode= :insuranceType "+
			   " AND SUBSTR(en.CMSPlanID , 1, 5) = :hoisIssuerId"+
			   " AND to_char(en.benefitEffectiveDate, 'yyyy') = :dateYear ")
	Long getEnrollmentCountByHouseHoldIdTypeAndIssuer(@Param("houseHoldCaseId") String houseHoldCaseId, 
			                                           @Param("insuranceType") String insuranceType,
			                                           @Param("hoisIssuerId") String hoisIssuerId,
			                                           @Param("dateYear") String dateYear);

	@Query("FROM Enrollment as en "+
            " where en.enrollmentStatusLkp.lookupValueCode IN( :statusList)"+
            " and en.id = :enrollmentId")
	List<Enrollment> getEnrollmentByIdStatus(@Param("enrollmentId") Integer enrollmentId,@Param("statusList") List<String> statusList);
	
	@Query("FROM Enrollment as en where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
			" AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING', 'CONFIRM', 'TERM') " +
			" AND en.houseHoldCaseId=:houseHoldCaseId "+
			" AND en.enrollmentTypeLkp.lookupValueCode IN ('FI')" +
			" AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate ")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForConfirm(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId, @Param("houseHoldCaseId")String houseHoldCaseId,@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	
	@Query("FROM Enrollment as en where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
			" AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING', 'CONFIRM', 'TERM') " +
			//" AND en.houseHoldCaseId=:houseHoldCaseId "+
			" AND en.enrollmentTypeLkp.lookupValueCode IN ('FI')" +
			" AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate ")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForConfirm(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId,@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query("FROM Enrollment as en where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
			" AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') " +
			" AND en.houseHoldCaseId=:houseHoldCaseId "+
			" AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate ORDER BY en.benefitEffectiveDate DESC")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForTerm(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId, @Param("houseHoldCaseId")String houseHoldCaseId,@Param("startDate") Date startDate, @Param("endDate") Date endDate);
		
	
	@Query("FROM Enrollment as en where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
			" AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') " +
			//" AND en.houseHoldCaseId=:houseHoldCaseId "+
			" AND en.benefitEffectiveDate >=  :startDate AND en.benefitEffectiveDate <= :endDate  ORDER BY en.benefitEffectiveDate DESC")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForTerm(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId,@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query("FROM Enrollment as en where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
			" AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING') " +
			" AND en.houseHoldCaseId=:houseHoldCaseId "+
			" AND en.benefitEffectiveDate =  :effectiveEndDate ")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForCancel(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId, @Param("houseHoldCaseId")String houseHoldCaseId,@Param("effectiveEndDate") Date effectiveEndDate);
	
	@Query("FROM Enrollment as en where en.exchgSubscriberIdentifier = :exchgSubscriberIdentifier " +
			" AND en.CMSPlanID = :cmsPlanId AND en.enrollmentStatusLkp.lookupValueCode IN ('PENDING') " +
			//" AND en.houseHoldCaseId=:houseHoldCaseId "+
			" AND en.benefitEffectiveDate =  :effectiveEndDate ")
	List<Enrollment> getEnrollmentByExchgSubscriberIdentifierForCancel(@Param("exchgSubscriberIdentifier")String exchgSubscriberIdentifier,@Param("cmsPlanId")String cmsPlanId,@Param("effectiveEndDate") Date effectiveEndDate);

	@Query("SELECT DISTINCT en.id from Enrollment as en " +
			" WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:currentYearStartDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:nextYearStartDate, 'MM/DD/YYYY') "+
			" AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM','PENDING','PAYMENT_RECEIVED') ")
	List<Integer> getEnrollmentsForPremiumUpdate(@Param("currentYearStartDate") String currentYearStartDateString, @Param("nextYearStartDate") String nextYearStartDateString);
	
	@Query("SELECT DISTINCT en.id from Enrollment as en " +
			" WHERE TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:currentYearStartDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:nextYearStartDate, 'MM/DD/YYYY') "+
			" AND en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM','PENDING','PAYMENT_RECEIVED') " +
		    " AND en.id NOT IN ( SELECT DISTINCT enrollment.id FROM EnrollmentPremium WHERE year = :year)")
	List<Integer> getDeltaEnrollmentsForPremiumUpdate(@Param("currentYearStartDate") String currentYearStartDateString, @Param("nextYearStartDate") String nextYearStartDateString, @Param("year") Integer year);
	
	@Query("Select distinct en.id FROM Enrollment as en where "+
			   " en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM', 'PENDING') "+
			   " and to_char(en.benefitEffectiveDate, 'yyyy') = :dateYear ")
	List<Integer> getEnrollmentIdsForAutoTerminationCA(@Param("dateYear") String dateYear);
	
	@Query("Select distinct en.id FROM Enrollment as en where "+
			   " en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM', 'PENDING') "+
			   " and to_char(en.benefitEffectiveDate, 'yyyy') = :dateYear AND hiosIssuerId in (:hiosIssuerId)")
	List<Integer> getEnrollmentIdsForAutoTerminationUsingHiosId(@Param("dateYear") String dateYear, @Param("hiosIssuerId") String hiosIssuerId);
	
	@Query("FROM Enrollment as en where en.id=:enrollmentId ")
	Enrollment getByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
	
	@Query(" FROM Enrollment as en "
			+ "WHERE en.houseHoldCaseId = :householdId "
			+ "and TO_DATE(TO_CHAR(en.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ "and TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "
			+ "and en.insuranceTypeLkp.lookupValueCode= :insuranceType and en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') "
			+ "and en.planLevel NOT IN ('CATASTROPHIC') "
			+ "and en.enrollmentConfirmationDate IS NOT NULL")
	List<Enrollment> getEnrollmentsForAnnualIRS(@Param("householdId") String householdId, @Param("endDate") String endDateString,  @Param("startDate") String startDateString, @Param("insuranceType") String insuranceType);

	@Query("SELECT grossPremEffDate, netPremEffDate, aptcEffDate, stateSubsidyEffDate FROM Enrollment as en where en.id = :enrollmentId")
	List<Object[]> getPremiumEffDates(@Param("enrollmentId") Integer enrollmentId);
	
	/**
	 * Jira Id: 
	 * @param houseHoldCaseIdList
	 * @return
	 */
	@Query("SELECT en.id, "
			+ " en.enrollmentStatusLkp.lookupValueCode, "
			+ " en.benefitEffectiveDate, "
			+ " en.benefitEndDate, "
			+ " en.insuranceTypeLkp.lookupValueLabel, "
			+ " en.enrollmentConfirmationDate, "
			+ " en.CMSPlanID, "
			+ " en.ehbPercent "
			+ " FROM Enrollment as en"
			+ " where en.id in (:enrollmentId) AND"
			+ " ((en.enrollmentStatusLkp.lookupValueCode not in ('CANCEL','ABORTED'))"
			+ " OR ((en.enrollmentStatusLkp.lookupValueCode in ('TERM')) AND (:systemDate <= en.benefitEndDate)))")
	List<Object[]>getEnrollmentDataForMonthlyAPTCAmount(@Param("enrollmentId") List<Integer> enrollmentId, @Param("systemDate") Date systemDate);
	

	@Query(nativeQuery = true, value = "Select PAYMENT_TXN_SEQ.nextval from dual")
	Long getPaymentSeqNextValOracle();
	
	@Query(nativeQuery = true, value = "select nextval ('PAYMENT_TXN_SEQ')")
	Long getPaymentSeqNextValPostgres();
	
	@Query(" SELECT distinct en.issuerId FROM Enrollment en "
			  + " WHERE to_char(en.benefitEffectiveDate, 'yyyy') = :dateYear "
			  + " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED','PENDING')")
	List<Integer> findIssuerByYear( @Param("dateYear") String dateYear);
	
	@Query(" SELECT en.id " 
			  + " FROM Enrollment as en "
			  + " WHERE to_char(en.benefitEffectiveDate, 'yyyy') = :year "
			  + " AND en.enrollmentStatusLkp.lookupValueCode NOT IN  ('ABORTED','PENDING','CANCEL') "
//			  + " AND en.enrollmentConfirmationDate IS NOT NULL"
			  + " AND en.issuerId = :issuerId" )
	List<Integer> getEnrollmentIdByIssuerAndYear(@Param("issuerId") Integer issuerId,@Param("year") String year);
	
	@Query(" SELECT en.id " 
			  + " FROM Enrollment as en "
			  + " WHERE to_char(en.benefitEffectiveDate, 'yyyy') = :year "
			  + " AND en.enrollmentStatusLkp.lookupValueCode NOT IN  ('ABORTED','PENDING','CANCEL') "
			  + " AND en.enrollmentConfirmationDate IS NOT NULL"
			  + " AND en.issuerId = :issuerId" )
	List<Integer> getEffectuatedEnrollmentIdByIssuerAndYear(@Param("issuerId") Integer issuerId,@Param("year") String year);
	
	@Query(" SELECT en.hiosIssuerId "
		 + " FROM Enrollment AS en "
		 + " WHERE en.id = :enrollmentId ")
	String getHiosIssuerId(@Param("enrollmentId") Integer enrollmentId);
	
	
	@Query("SELECT en.id, en.planId, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate, "
			+ " en.grossPremiumAmt, en.netPremiumAmt, en.enrollmentStatusLkp.lookupValueCode, en.createdOn, en.insuranceTypeLkp.lookupValueLabel,"
			+ " en.planLevel, en.planName, en.insurerName, en.hiosIssuerId, en.aptcAmt, "
			+ " en.renewalFlag, en.priorEnrollmentId, en.CMSPlanID, en.updatedOn, en.submittedToCarrierDate, "
			+ " en.csrAmt, en.paymentTxnId, en.issuerId as issuerId, en.enrollmentConfirmationDate, en.slcspAmt,"
			+ " en.ehbAmt, en.aptcEffDate, en.csrMultiplier, en.updatedBy.id, en.ehbPercent,"
			+ " en.dntlEssentialHealthBenefitPrmDollarVal, en.stateSubsidyAmt, en.stateSubsidyEffDate  "
			+ " FROM Enrollment as en"
			+ " WHERE en.houseHoldCaseId in (:houseHoldCaseIdList)"
			+ " AND en.enrollmentTypeLkp.lookupValueCode IN ('FI')"
			+ " AND en.enrollmentStatusLkp.lookupValueCode NOT IN (:excludeStatusList)")
	List<Object[]> getEnrollmentDataByHouseHoldCaseIdCA(@Param("houseHoldCaseIdList") List<String> houseHoldCaseIdList, @Param("excludeStatusList") List<String> excludeStatusList);
	
	@Query("SELECT en.id, en.planId, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate,"
			+ " en.grossPremiumAmt, en.netPremiumAmt, en.enrollmentStatusLkp.lookupValueCode, en.createdOn, en.insuranceTypeLkp.lookupValueLabel, "
			+ " en.planLevel, en.planName, en.insurerName, en.hiosIssuerId, en.aptcAmt,"
			+ " en.renewalFlag, en.priorEnrollmentId, en.CMSPlanID, en.updatedOn, en.submittedToCarrierDate,"
			+ " en.csrAmt, en.paymentTxnId, en.issuerId as issuerId, en.enrollmentConfirmationDate, en.slcspAmt,"
			+ " en.ehbAmt, en.aptcEffDate, en.csrMultiplier, en.updatedBy.id, en.ehbPercent,"
			+ " en.dntlEssentialHealthBenefitPrmDollarVal, en.stateSubsidyAmt, en.stateSubsidyEffDate "
			+ " FROM Enrollment as en"
			+ " WHERE en.id in (:enrollmentIdList)"
			+ " AND en.enrollmentTypeLkp.lookupValueCode IN ('FI')"
			+ " AND en.enrollmentStatusLkp.lookupValueCode NOT IN (:excludeStatusList)")
	List<Object[]> getEnrollmentDataByEnrollmentIdCA(@Param("enrollmentIdList") List<Integer> enrollmentIdList, @Param("excludeStatusList")List<String> excludeStatusList);

	
	/**
	 * Jira ID: HIX-98231
	 * @param enrollmentIdList List<Integer>
	 * @return List<Object>
	 */
	@Query("SELECT en.id, "
			   + " en.planId, "
			   + " en.houseHoldCaseId, "
			   + " en.benefitEffectiveDate, "
			   + " en.benefitEndDate, "
			   + " en.grossPremiumAmt,"
			   + " en.netPremiumAmt, "
			   + " en.enrollmentStatusLkp.lookupValueCode, "
			   + " en.createdOn, "
			   + " en.insuranceTypeLkp.lookupValueLabel, "
			   + " en.planLevel,"
			   + " en.planName, "
			   + " en.insurerName, "
			   + " en.hiosIssuerId, "
			   + " en.aptcAmt, "
			   + " en.renewalFlag, "
			   + " en.priorEnrollmentId, "
			   + " en.CMSPlanID, "
			   + " en.updatedOn, "
			   + " en.updatedBy.id, "
			   + " en.submittedToCarrierDate, "
			   + " en.createdBy.id, "
			   + " en.csrAmt, "
			   + " en.paymentTxnId, "
			   + " en.ssapApplicationid, "
			   + " ssap.caseNumber,"
			   + " (lastUpdatedByUser.firstName || ' '|| lastUpdatedByUser.lastName)as LastUpdatedByUserName ,"
			   + " (createdByUser.firstName ||' '|| createdByUser.lastName) as SubmittedByUserName, "
			   + " en.issuerId as issuerId, "
			   + " en.enrollmentConfirmationDate, "
			   + " en.slcspAmt, "
			   + " en.ehbAmt, "
			   + " en.dntlEhbAmt, "
			   + " en.ehbPercent, "
			   + " en.dntlEssentialHealthBenefitPrmDollarVal, "
			   + " en.agentBrokerName, "
			   + " en.assisterBrokerId, "
			   + " en.brokerTPAAccountNumber1, "
			   + " en.priorSsapApplicationid, "
			   + " en.changePlanAllowed, "
			   + " en.externalHouseHoldCaseId, "
			   + " en.stateSubsidyAmt"
			   + " FROM Enrollment as en, SsapApplication as ssap, AccountUser lastUpdatedByUser, AccountUser createdByUser"
			   + " WHERE en.id in (:enrollmentIdList) "
			   + " AND en.ssapApplicationid = ssap.id "
			   + " AND en.createdBy.id = createdByUser.id "
			   + " AND en.updatedBy.id = lastUpdatedByUser.id ")
	List<Object[]> getEnrollmentDataByld(@Param("enrollmentIdList") List<Integer> enrollmentIdList);
	
	 @Query(" SELECT en.id,en.insuranceTypeLkp, " +
		      " en.tenantId, en.submittedToCarrierDate FROM Enrollment as en "+
		      " WHERE en.id = :enrollmentId ")
	 Object[] findEnrollmentForSalesAndReversalBatch(@Param("enrollmentId") Integer enrollmentId);
	 
	 @Query("SELECT hltEn.id FROM Enrollment as hltEn WHERE hltEn.id IN (SELECT DISTINCT en1095.exchgAsignedPolicyId"
			 + " FROM Enrollment1095 as en1095, Enrollment as denEn WHERE en1095.houseHoldCaseId = denEn.houseHoldCaseId"
			 + " AND en1095.coverageYear = cast(:year as integer) AND TO_CHAR(denEn.benefitEffectiveDate, 'YYYY') = :year"
			 + "	AND denEn.insuranceTypeLkp.lookupValueCode = 'DEN' AND denEn.updatedOn >="
			 + " (SELECT MAX(bje.startTime) FROM BatchJobExecution as bje WHERE bje.status ='COMPLETED' AND bje.createdOn = "
			 + " (SELECT MAX(bj.createdOn) FROM BatchJobExecution as bj WHERE bj.status = 'COMPLETED' AND bj.batchJobInstance.id"
			 + " IN (SELECT id FROM BatchJobInstance WHERE jobName ='populate1095StagingJob')))) AND hltEn.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM')")
	 List<Integer> getEnrollmentsAfterDentalUpdate(@Param("year") String year);
	 
	@Query(" SELECT en.id " 
				  + " FROM Enrollment as en "
				  + " WHERE to_char(en.benefitEffectiveDate, 'yyyy') = :year "
				  + " AND en.enrollmentStatusLkp.lookupValueCode NOT IN  ('ABORTED') "
				  + " AND en.issuerId = :issuerId" )
	List<Integer> getEnrollmentIdByIssuerAndYearForTestData(@Param("issuerId") Integer issuerId, @Param("year") String year);
	
	@Query("SELECT DISTINCT TO_CHAR(en.benefitEffectiveDate, 'yyyy') FROM Enrollment as en "
			+ " WHERE en.enrollmentStatusLkp.lookupValueCode NOT IN  ('ABORTED') ORDER BY 1")
	List<String> getDistinctEnrollmentCoverageYear();
	
	@Query(" SELECT en.CMSPlanID "
			 + " FROM Enrollment AS en "
			 + " WHERE en.id = :enrollmentId ")
	String getCmsPlanId(@Param("enrollmentId") Integer enrollmentId);
	
	@Query(" SELECT  COUNT( DISTINCT e.id ) "
			+" FROM Enrollment e "
			+ "WHERE e.createdOn < :issuerCutoffDate AND TO_CHAR(e.benefitEffectiveDate, 'YYYY') = :year  AND e.hiosIssuerId = :hiosIssuerId AND e.enrollmentStatusLkp.lookupValueCode NOT IN  ('ABORTED') ")
	long enrollmentCountInHIX(@Param("hiosIssuerId") String hiosIssuerId,@Param("year") String year,@Param("issuerCutoffDate") Date issuerCutoffDate );
	
	@Query("SELECT TO_CHAR(en.benefitEffectiveDate, 'yyyy') FROM Enrollment as en WHERE en.id = :enrollmentId")
	String getEnrollmentCoverageYear(@Param("enrollmentId") Integer enrollmentId);

	@Query("FROM Enrollment as en WHERE en.id = :enrollmentId AND en.hiosIssuerId = :hiosIssuerId AND en.createdOn < :issuerCutoffDate")
	Enrollment getEnrollmentByIdAndHiosIssuerId(@Param("enrollmentId") Integer enrollmentId, @Param("hiosIssuerId") String hiosIssuerId, @Param("issuerCutoffDate") Date issuerCutoffDate);
		
	@Query(nativeQuery = true , value = "SELECT distinct en.HIOS_ISSUER_ID FROM ENROLLMENT en "
			 + " WHERE en.issuer_id = :issuerId and en.issuer_id is not null limit 1")
	String getHiosIssuerIdByIssuerIdPg(@Param("issuerId") Integer issuerId);
	
	@Query("SELECT distinct en.hiosIssuerId FROM Enrollment AS en "
			 + " WHERE en.issuerId = :issuerId and en.issuerId is not null and rownum = 1")
	String getHiosIssuerIdByIssuerIdOracle(@Param("issuerId") Integer issuerId);
	
	@Query(nativeQuery = true , value = "SELECT distinct en.issuer_id FROM ENROLLMENT en "
			 + " WHERE en.HIOS_ISSUER_ID = :hiosIssuerId and en.HIOS_ISSUER_ID is not null limit 1")
	Integer getIssuerIdByHiosIssuerIdPg(@Param("hiosIssuerId") String hiosIssuerId);
	
	@Query("SELECT distinct en.issuerId FROM Enrollment AS en "
			 + " WHERE en.hiosIssuerId = :hiosIssuerId and en.hiosIssuerId is not null and rownum = 1")
	Integer getIssuerIdByHiosIssuerIdOracle(@Param("hiosIssuerId") String hiosIssuerId);
	
	@Query("SELECT distinct en.issuerId FROM Enrollment AS en where en.hiosIssuerId in (:hiosIssuerIdList)")
	List<Integer> getIssuerIdByHiosIssuerIdList(@Param("hiosIssuerIdList") List<String> hiosIssuerIdList);
	

	@Query("SELECT DISTINCT en.id from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND en.assisterBrokerId = :brokerAssisterID " +
			   " AND createdOn >= :startDate "+
			   " AND createdOn <= :endDate  ORDER BY en.id ")
	List<Integer> getIndivEnrollmentForBOBTransfer(@Param("brokerAssisterID") Integer brokerAssisterID , @Param("startDate") Date startDate ,@Param("endDate") Date endDate);
	
	@Query("SELECT DISTINCT en.id from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND en.assisterBrokerId = :brokerAssisterID " +
			   " AND en.id IN (:enrollmentIds)  ORDER BY en.id ")
	List<Integer> getIndivEnrollmentForBOBTransfer(@Param("brokerAssisterID") Integer brokerAssisterID,  @Param("enrollmentIds") List<Integer> enrollmentIds);
	
	@Query("SELECT DISTINCT en.id from Enrollment as en " +
			   " WHERE en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED') " +
			   " AND en.assisterBrokerId = :brokerAssisterID  ORDER BY en.id " )
	List<Integer> getIndivEnrollmentForBOBTransfer(@Param("brokerAssisterID") Integer brokerAssisterID);

	/**
	 * 
	 * @param List<String> memberIdList
	 * @param List<String> coverageYearList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO(ee.exchgIndivIdentifier, en.id, en.ssapApplicationid, "
			+ " en.priorSsapApplicationid, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate,"
			+ " en.enrollmentStatusLkp.lookupValueCode, en.insuranceTypeLkp.lookupValueLabel, ee.effectiveStartDate, "
			+ " ee.effectiveEndDate, ee.enrolleeLkpValue.lookupValueCode, ee.personTypeLkp.lookupValueCode,"
			+ " en.CMSPlanID, SUBSTR(en.CMSPlanID, (LENGTH(en.CMSPlanID) -1), LENGTH(en.CMSPlanID)), en.planLevel, en.ehbPercent,en.hiosIssuerId,en.insurerName,en.planName,en.netPremiumAmt,en.changePlanAllowed ,en.createdOn) "
			+ " FROM Enrollment as en, Enrollee as ee "
			+ " WHERE en.id = ee.enrollment.id "
			+ " AND ee.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') "
			+ " AND (en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM', 'PENDING') OR (en.enrollmentStatusLkp.lookupValueCode = 'TERM' AND  TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'), 'MM/DD/YYYY') >= TO_DATE(:currentDate,'MM/DD/YYYY')))"
			+ " AND ee.enrolleeLkpValue.lookupValueCode NOT IN('CANCEL') "
			+ " AND ee.exchgIndivIdentifier IN (:memberIdList) "
			+ " AND ((:coverageYearList) IS NULL OR ((:coverageYearList) IS NOT NULL AND TO_CHAR(en.benefitEffectiveDate, 'YYYY') IN (:coverageYearList))) "
			+ " ORDER BY ee.exchgIndivIdentifier ASC, ee.effectiveStartDate ASC")
	List<EnrollmentMemberDataDTO> getEnrollmentMemberDataDTO(@Param("memberIdList") List<String> memberIdList, @Param("coverageYearList") List<String> coverageYearList,  @Param("currentDate") String currentDate);
	
	@Query(" SELECT en.id " 
			  + " FROM Enrollment as en "
			  + " WHERE to_char(en.benefitEffectiveDate, 'yyyy') = :year "
			  + " AND en.enrollmentStatusLkp.lookupValueCode = 'PENDING' "
			  + " AND en.issuerId = :issuerId" )
	List<Integer> getPendingEnrollmentIdByIssuerAndYear(@Param("issuerId") Integer issuerId, @Param("year") String year);
	
	@Modifying
	@Transactional
	@Query(" UPDATE Enrollment SET ssapApplicationid = :newSsapApplicationId  " +
		   " WHERE ssapApplicationid = :existingSsapApplicationId ")
	void updateSsapApplicationId(@Param("existingSsapApplicationId") Long existingSsapApplicationId, @Param("newSsapApplicationId") Long newSsapApplicationId);
	
	@Modifying
	@Transactional
	@Query(" UPDATE Enrollment SET ssapApplicationid = :newSsapApplicationId  " +
		   " WHERE houseHoldCaseId = :houseHoldCaseId "+
		   " AND EXTRACT(YEAR FROM benefitEffectiveDate) = :coverageYear " )
	void updateSsapApplicationIdForHousehold(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("newSsapApplicationId") Long newSsapApplicationId, @Param("coverageYear") Integer coverageYear);

	@Query("FROM Enrollment as en where en.lastEnrollmentId=:id and en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED')")
	List<Enrollment> findByLastEnrollmentId(@Param("id") Integer id);
	
	@Query("SELECT CASE WHEN en.priorSsapApplicationid = en.ssapApplicationid THEN 'true' else 'false' end FROM Enrollment as en "
			+ " where en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED', 'CANCEL') "
			+ " AND en.insuranceTypeLkp.lookupValueCode in ('DEN') "
			+ " AND en.houseHoldCaseId = :houseHoldCaseId "
			+ " AND en.ssapApplicationid = :ssapApplicationId "
			+ " AND to_char(en.benefitEffectiveDate, 'yyyy') = :coverageYear order by en.createdOn DESC")			
	List<String> getDentalPlanSelectionStatusByHousehold(@Param("houseHoldCaseId") String householdCaseId, @Param("coverageYear")  String coverageYear,  @Param("ssapApplicationId") Long ssapApplicationId, Pageable pageable);
	
	
	@Query("SELECT id, householdCaseId from Household WHERE householdCaseId IN (:householdCaseIdList)")
	List<Object[]> getHouseholdCaseIdFromExtHouseholdCaseId(@Param("householdCaseIdList") List<String> householdCaseIdList);

	@Query(" SELECT ssap.caseNumber "
	+ " FROM Enrollment as en, SsapApplication as ssap"
	+ " where en.ssapApplicationid = ssap.id "
	+ " AND en.ssapApplicationid = :ssapApplicationid "
	+ " AND en.enrollmentTypeLkp.lookupValueCode IN ('FI')" 
	+ " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED')")
	String getSsapCaseNumber(@Param("ssapApplicationid") Long ssapApplicationid);

	@Query("SELECT en.id, en.createdOn FROM Enrollment as en "
			+ " where en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED', 'CANCEL') "
			+ " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'), 'MM/DD/YYYY') >= TO_DATE(:currentDate,'MM/DD/YYYY') "
			+ " AND en.ssapApplicationid = :ssapApplicationId order by en.createdOn DESC")
	List<Object[]> fetchLatestEnrollmentCreationTimeBySsapId(@Param("ssapApplicationId") Long ssapApplicationid, @Param("currentDate") String currentDate, Pageable pageable);

	@Query("SELECT en.id, en.benefitEffectiveDate FROM Enrollment as en "
			+ " where en.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED', 'CANCEL') "
			+ " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'), 'MM/DD/YYYY') >= TO_DATE(:currentDate,'MM/DD/YYYY') "
			+ " AND en.ssapApplicationid = :ssapApplicationId order by en.benefitEffectiveDate DESC")
	List<Object[]> fetchLatestEnrollmentEffectiveDateBySsapId(@Param("ssapApplicationId") Long ssapApplicationid, @Param("currentDate") String currentDate, Pageable pageable);
	
	@Modifying
	@Transactional
	@Query(" UPDATE Enrollment SET changePlanAllowed = :changePlanAllowed  " +
		   " WHERE houseHoldCaseId = :houseHoldCaseId "+
		   " AND TO_CHAR(benefitEffectiveDate, 'YYYY') = :coverageYear " )
	void updatePlanChangeFlagForHousehold(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("changePlanAllowed") String changePlanAllowed, @Param("coverageYear") String coverageYear);
	
	@Query("SELECT ssap.caseNumber FROM SsapApplication as ssap where ssap.id = :ssapApplicationId")
	String getCaseNumberFromSsap(@Param("ssapApplicationId")Long ssapApplicationId);
	
	@Query(" FROM Enrollment as en  WHERE en.ssapApplicationid =:ssapApplicationId " +
			" AND en.insuranceTypeLkp.lookupValueCode = :planType" +
			" AND (en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM') OR (en.enrollmentStatusLkp.lookupValueCode='TERM' " +
			   " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:terminationDate, 'MM/DD/YYYY')))"
			)
	List<Enrollment> findActiveEnrollmentBySsapApplicationIdAndPlanType(@Param("ssapApplicationId") Long ssapApplicationId ,  @Param("terminationDate") String terminationDateString, @Param("planType")String planType);
	
	@Query( " SELECT DISTINCT en.hiosIssuerId"
			+ " FROM Enrollment as en, Enrollee as ee"
			+ " where en.id = ee.enrollment "
			+ " AND ee.externalIndivId = :externalIndivId "
			+ " AND TO_CHAR(en.benefitEffectiveDate, 'yyyy') = :coverageYear"
			+ " AND en.insuranceTypeLkp.lookupValueCode = 'HLT'"
			+ " AND (en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM') OR (en.enrollmentStatusLkp.lookupValueCode='TERM' "
			+ " AND TO_DATE(TO_CHAR(en.benefitEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:terminationDate, 'MM/DD/YYYY')))"
			)
	List<String> findHiosIdForEnrollment(@Param("externalIndivId") String externalIndivId ,@Param("terminationDate") String terminationDateString, @Param("coverageYear") String coverageYear);

	
	/**
	 * 
	 * @param List<String> memberIdList
	 * @param List<String> coverageYearList
	 * @param List<String> enrollmentStatusList
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO(ee.exchgIndivIdentifier, en.id, en.ssapApplicationid, "
			+ " en.priorSsapApplicationid, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate,"
			+ " en.enrollmentStatusLkp.lookupValueCode, en.insuranceTypeLkp.lookupValueLabel, ee.effectiveStartDate, "
			+ " ee.effectiveEndDate, ee.enrolleeLkpValue.lookupValueCode, ee.personTypeLkp.lookupValueCode,"
			+ " en.CMSPlanID, SUBSTR(en.CMSPlanID, (LENGTH(en.CMSPlanID) -1), LENGTH(en.CMSPlanID)), en.planLevel, "
			+ " en.ehbPercent,en.hiosIssuerId,en.insurerName,en.planName,en.netPremiumAmt,en.changePlanAllowed ,en.createdOn) "
			+ " FROM Enrollment as en, Enrollee as ee "
			+ " WHERE en.id = ee.enrollment.id "
			+ " AND ee.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') "
			+ " AND en.enrollmentStatusLkp.lookupValueCode IN (:statusList) "
			+ " AND ee.exchgIndivIdentifier IN (:memberIdList) "
			+ " AND TO_CHAR(en.benefitEffectiveDate, 'yyyy') = :coverageYear"
			+ " ORDER BY ee.exchgIndivIdentifier ASC, ee.effectiveStartDate ASC")
	List<EnrollmentMemberDataDTO> getEnrollmentMemberDataByMemberListAndCovergaeYearAndStatus(@Param("memberIdList") List<String> memberIdList, @Param("coverageYear") String coverageYear,@Param("statusList") List<String> statusList);

	@Query("SELECT e.id "
			+ "FROM Enrollment as e "
			+ "where TO_CHAR(e.benefitEffectiveDate, 'YYYY') = :coverageYear AND e.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM', 'PENDING') "
			+ "ORDER BY e.id ASC")
	List<Integer> getEnrollmentIdListByCoverageYearForQHPReports(@Param("coverageYear") String coverageYear);

	
	
	/**
	 * 
	 * @param String householdCaseId
	 * @param List<String> coverageYearList
	 * @param List<String> enrollmentStatusList
	 * @return 
	 */
	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO(ee.exchgIndivIdentifier, en.id, en.ssapApplicationid, "
			+ " en.priorSsapApplicationid, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate,"
			+ " en.enrollmentStatusLkp.lookupValueCode, en.insuranceTypeLkp.lookupValueLabel, ee.effectiveStartDate, "
			+ " ee.effectiveEndDate, ee.enrolleeLkpValue.lookupValueCode, ee.personTypeLkp.lookupValueCode,"
			+ " en.CMSPlanID, SUBSTR(en.CMSPlanID, (LENGTH(en.CMSPlanID) -1), LENGTH(en.CMSPlanID)), en.planLevel, "
			+ " en.ehbPercent,en.hiosIssuerId,en.insurerName,en.planName,en.netPremiumAmt,en.changePlanAllowed ,en.createdOn) "
			+ " FROM Enrollment as en, Enrollee as ee "
			+ " WHERE en.houseHoldCaseId IN ( :householdCaseIdList )"
			+ " AND en.id = ee.enrollment.id "
			+ " AND ee.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') "
			+ " AND en.enrollmentStatusLkp.lookupValueCode IN (:statusList) "
			+ " AND TO_CHAR(en.benefitEffectiveDate, 'yyyy') = :coverageYear "
			+ " ORDER BY ee.exchgIndivIdentifier ASC, ee.effectiveStartDate ASC ")
    List<EnrollmentMemberDataDTO> getEnrollmentMemberDataByHouseholdCaseIdAndCovergaeYearAndStatus(@Param("householdCaseIdList") List<String> householdCaseIdList, @Param("coverageYear") String coverageYear,@Param("statusList") List<String> statusList);
	
	@Modifying
	@Transactional
	@Query(" UPDATE Enrollment SET changePlanAllowed = :changePlanAllowed  " +
		   " WHERE houseHoldCaseId = :houseHoldCaseId "+
		   " AND TO_CHAR(benefitEffectiveDate, 'YYYY') = :coverageYear and id NOT IN (:exclusionList) " )
	void updatePlanChangeFlagForHousehold(@Param("houseHoldCaseId") String houseHoldCaseId,
			@Param("changePlanAllowed") String changePlanAllowed, @Param("coverageYear") String coverageYear,
			@Param("exclusionList") List<Integer> exclusionList);

	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO(ee.exchgIndivIdentifier, en.id, en.ssapApplicationid, "
	    + " en.priorSsapApplicationid, en.houseHoldCaseId, en.benefitEffectiveDate, en.benefitEndDate,"
	    + " en.enrollmentStatusLkp.lookupValueCode, en.insuranceTypeLkp.lookupValueLabel, ee.effectiveStartDate, "
	    + " ee.effectiveEndDate, ee.enrolleeLkpValue.lookupValueCode, ee.personTypeLkp.lookupValueCode,"
	    + " en.CMSPlanID, SUBSTR(en.CMSPlanID, (LENGTH(en.CMSPlanID) -1), LENGTH(en.CMSPlanID)), en.planLevel, en.ehbPercent,en.hiosIssuerId,en.insurerName,en.planName,en.netPremiumAmt,en.changePlanAllowed ,en.createdOn) "
	    + " FROM Enrollment as en, Enrollee as ee "
	    + " WHERE en.id = ee.enrollment.id "
	    + " AND ee.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') "
	    + " AND en.enrollmentStatusLkp.lookupValueCode IN (:statusList)"
	    + " AND ee.enrolleeLkpValue.lookupValueCode NOT IN('CANCEL') "
	    + " AND ee.exchgIndivIdentifier IN (:memberIdList) "
	    + " AND ((:coverageYearList) IS NULL OR ((:coverageYearList) IS NOT NULL AND TO_CHAR(en.benefitEffectiveDate, 'YYYY') IN (:coverageYearList))) "
	    + " ORDER BY ee.exchgIndivIdentifier ASC, ee.effectiveStartDate ASC")
	List<EnrollmentMemberDataDTO> getEnrollmentMemberDataDTO(@Param("memberIdList") List<String> memberIdList, @Param("coverageYearList") List<String> coverageYearList,  @Param("statusList") List<String> statusList);

	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO(e.id, e.ssapApplicationid) "
			+ "FROM Enrollment as e "
			+ "where TO_CHAR(e.benefitEffectiveDate, 'YYYY') = :coverageYear AND e.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM', 'PENDING') "
			+ "AND e.aptcAmt is NULL AND e.insuranceTypeLkp.lookupValueCode='HLT' " 
			+ "ORDER BY e.id ASC")
	List<EnrollmentMemberDataDTO> getSSAPApplicationIdsForNFEnrollment(@Param("coverageYear") String coverageYear);
	
	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO(e.id, e.ssapApplicationid) "
			+ "FROM Enrollment as e "
			+ "where e.ssapApplicationid IN (:ssapApplicationIdList) AND e.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM', 'PENDING') "
			+ "AND e.aptcAmt is NULL AND e.insuranceTypeLkp.lookupValueCode='HLT' AND TO_CHAR(e.benefitEffectiveDate, 'YYYY') = :coverageYear "
			+ "ORDER BY e.id ASC")
	List<EnrollmentMemberDataDTO> getSSAPandEnrlIdbySsapId(@Param("ssapApplicationIdList") List<Long> ssapApplicationIdList, @Param("coverageYear") String coverageYear);
	
	@Modifying
	@Transactional
	@Query(" UPDATE Enrollment SET slcspAmt = :slcspAmt WHERE id = :enrollmentId ")
	void updateSLCSPAmtForEnrollment(@Param("slcspAmt") Float slcspAmt, @Param("enrollmentId") Integer enrollmentId);

	@Query("SELECT e.id "
			+ "FROM Enrollment as e "
			+ "where TO_CHAR(e.benefitEffectiveDate, 'YYYY') = :coverageYear AND e.houseHoldCaseId = :houseHoldCaseId AND e.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','ABORTED') "
			+ "AND e.insuranceTypeLkp.lookupValueCode in (:insuranceTypeLkp) ORDER BY e.id ASC")
	List<Integer> getEnrollmentIdListByHouseholdCaseId(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("coverageYear") String coverageYear, @Param("insuranceTypeLkp") String insuranceTypeLkp);
}

