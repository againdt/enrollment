package com.getinsured.hix.enrollment.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class EnrollmentReconciliationInfo {
	
	private Integer totalNumberOfXMLFilesReceived;
	private Integer totalNumberOfEDIFilesReceived;
	private Integer totalNumberOfTrackingFilesReceived;
	
	private Integer totalNumberOfBadXMLFilesReceived;
	private Integer totalNumberOfBadEDIRecordReceived;
	private Integer totalNumberOfBadTrackingRecordReceived;
	
	private Map<String, List<EnrollmentReconciliationErrorInfo>> enrollmentReconciliationErrorInfoMap;
	private Map<String, String> generalFileFailureResponseMap;
	private String successFolderPath;
	private String failureFolderPath;
	private String serverURL;
	
	/**
	 * 
	 */
	public EnrollmentReconciliationInfo(){
		totalNumberOfXMLFilesReceived = 0;
		totalNumberOfEDIFilesReceived = 0;
		totalNumberOfTrackingFilesReceived = 0;
		
		totalNumberOfBadXMLFilesReceived = 0;
		totalNumberOfBadEDIRecordReceived = 0;
		totalNumberOfBadTrackingRecordReceived = 0;
		
		enrollmentReconciliationErrorInfoMap = new HashMap<String, List<EnrollmentReconciliationErrorInfo>>();
		generalFileFailureResponseMap = new HashMap<String, String>();
		successFolderPath = StringUtils.EMPTY;
		failureFolderPath = StringUtils.EMPTY;
	}
	
	public synchronized void clearEnrollmentReconciliationErrorInfoMap(){
		this.enrollmentReconciliationErrorInfoMap.clear();
	}

	public Integer getTotalNumberOfXMLFilesReceived() {
		return totalNumberOfXMLFilesReceived;
	}

	public synchronized void setTotalNumberOfXMLFilesReceived(Integer totalNumberOfXMLFiles) {
		this.totalNumberOfXMLFilesReceived = totalNumberOfXMLFilesReceived + totalNumberOfXMLFiles;
	}

	public Integer getTotalNumberOfEDIFilesReceived() {
		return totalNumberOfEDIFilesReceived;
	}

	public synchronized void setTotalNumberOfEDIFilesReceived(Integer totalNumberOfEDIFiles) {
		this.totalNumberOfEDIFilesReceived = totalNumberOfEDIFilesReceived + totalNumberOfEDIFiles;
	}

	public Integer getTotalNumberOfTrackingFilesReceived() {
		return totalNumberOfTrackingFilesReceived;
	}

	public synchronized void setTotalNumberOfTrackingFilesReceived(Integer totalNumberOfTrackingFiles) {
		this.totalNumberOfTrackingFilesReceived = totalNumberOfTrackingFilesReceived + totalNumberOfTrackingFiles;
	}

	public Integer getTotalNumberOfBadXMLFilesReceived() {
		return totalNumberOfBadXMLFilesReceived;
	}

	public synchronized void incrementTotalNumberOfBadXMLFilesReceived(int incrementBy) {
		this.totalNumberOfBadXMLFilesReceived = totalNumberOfBadXMLFilesReceived + incrementBy;
	}

	public Integer getTotalNumberOfBadEDIRecordReceived() {
		return totalNumberOfBadEDIRecordReceived;
	}

	public synchronized void incrementTotalNumberOfBadEDIRecordReceived(Integer incrementBy) {
		this.totalNumberOfBadEDIRecordReceived = totalNumberOfBadEDIRecordReceived + incrementBy;
	}

	public Integer getTotalNumberOfBadTrackingRecordReceived() {
		return totalNumberOfBadTrackingRecordReceived;
	}

	public synchronized void incrementTotalNumberOfBadTrackingRecordReceived(Integer incrementBy) {
		this.totalNumberOfBadTrackingRecordReceived = totalNumberOfBadTrackingRecordReceived + incrementBy;
	}

	public Map<String, List<EnrollmentReconciliationErrorInfo>> getEnrollmentReconciliationErrorInfoMap() {
		return enrollmentReconciliationErrorInfoMap;
	}

	public synchronized void putToEnrollmentReconciliationErrorInfoMap(String key, EnrollmentReconciliationErrorInfo enrollmentReconciliationErrorInfo){
		if (enrollmentReconciliationErrorInfoMap != null && enrollmentReconciliationErrorInfoMap.containsKey(key)) {
			enrollmentReconciliationErrorInfoMap.get(key).add(enrollmentReconciliationErrorInfo);
		} else {
			List<EnrollmentReconciliationErrorInfo> enrollmentReconciliationErrorInfoList = new ArrayList<EnrollmentReconciliationErrorInfo>();
			enrollmentReconciliationErrorInfoList.add(enrollmentReconciliationErrorInfo);
			enrollmentReconciliationErrorInfoMap.put(key, enrollmentReconciliationErrorInfoList);
		}
	}
	
	public synchronized void putAllToEnrollmentReconciliationErrorInfoMap(Map<String, List<EnrollmentReconciliationErrorInfo>> enrollmentReconciliationErrorInfoMap){
		enrollmentReconciliationErrorInfoMap.putAll(enrollmentReconciliationErrorInfoMap);
	}

	public Map<String, String> getGeneralFileFailureResponseMap() {
		return generalFileFailureResponseMap;
	}

	public synchronized void putGeneralFileFailureResponseMap(String key, String value) {
		this.generalFileFailureResponseMap.put(key, value);
	}

	public String getSuccessFolderPath() {
		return successFolderPath;
	}

	public synchronized void setSuccessFolderPath(String successFolderPath) {
		this.successFolderPath = successFolderPath;
	}

	public String getFailureFolderPath() {
		return failureFolderPath;
	}

	public synchronized void setFailureFolderPath(String failureFolderPath) {
		this.failureFolderPath = failureFolderPath;
	}
	
	public synchronized void incrementCounterByFileType(int incrementBy, String fileType){
		if (fileType.equalsIgnoreCase(EnrollmentConstants.FILETYPE_EDI)) {
			incrementTotalNumberOfBadEDIRecordReceived(incrementBy);
		} else if (fileType.equalsIgnoreCase(EnrollmentConstants.FILETYPE_TA1_TRACKING) || fileType.equalsIgnoreCase(EnrollmentConstants.FILETYPE_999_TRACKING)) {
			incrementTotalNumberOfBadTrackingRecordReceived(incrementBy);
		}
	}

	public String getServerURL() {
		return serverURL;
	}

	public synchronized void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}
}
