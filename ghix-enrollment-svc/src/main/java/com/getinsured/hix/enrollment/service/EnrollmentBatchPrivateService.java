/**
 * 
 */

package com.getinsured.hix.enrollment.service;

import java.util.Date;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author parhi_s
 * @since 18/06/2013
 */
public interface EnrollmentBatchPrivateService {
	
	/**
	 * @param extractMonth
	 * @param enrollmentType
	 * @throws GIException
	 * 
	 */
//	void serveEnrollmentReconciliationJob(String extractMonth, String enrollmentType) throws GIException;
	
	/**
	 * @param extractMonth
	 * @throws GIException
	 * 
	 */
//	void serveCMSReconciliationJob(String extractMonth) throws GIException;
	
	/**
	 * @author ajinkya_m
	 * @since 18 Jun 2013
	 *
	 * Used by "CMSDailyEnrollmentReport" batch service to generate XML
	 */
//	void cmsDailyEnrollmentReport() throws GIException;

	/**
	 * This Method is used to serve the IndividualEnrollmentXML Job and ShopEnrollmentXML Job
	 * 
	 * @param enrollmentType
	 * @throws Exception
	 */
//	void serveEnrollmentRecordJob(String enrollmentType,String startDateString, String endDateString) throws GIException;
	
	/**
	 * Update Enrollment with updates send by CARRIER
	 * 
	 *  @author meher_a
	 */
//	void updateEnrollmentByXML(String timeStamp) throws GIException;
	
	//void updateEnrollmentByCarrierExcelFeed(String timeStamp) throws GIException;
	
	/**
	 * Updates the enrollment commission information based on Carrier feed
	 * 
	 * @author Priya
	 * @param timeStamp
	 */
	/*void updateCommissionByCarrierFeed(String timeStamp) throws GIException;
	void updateEnrollmentByBobFeed(String timeStamp) throws GIException;
	*/
	/**
	 * Change status of Enrollment from given Employer to Cancelled
	 * 
	 * @author meher_a
	 * @param employerId
	 */
//	void updateShopEnrollemntToCancelled(int employerId);
	
	/**
	 * @author panda_p
	 * @since 29-Mar-2013
	 * 
	 * This Will called thru batch and used to send the carrier updated data to AHBX  
	 * 
	 * 
	 */
//	void sendCarrierUpdatedDataToAHBX() throws GIException;
	
	/**
	 * 
	 * @param jobName
	 * @returnfindEnrollmentAndEnrolleeDataByName
	 * @throws GIException
	 */
	Date getJOBLastRunDate(String jobName) throws GIException;
	
	/**
	 * This Method is used to serve the Canceled and Terminated Job
	 * @param extractMonth
	 * @throws GIException
	 */
//	void serveCmsReconCancelTermEnrollment(String extractMonth)throws GIException;
	
	/*void FtpInboundCarrierLocation()throws GIException;*/
	
	/**
	 * This method is used to get specific type file names in a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
  /* List<String> getCarrierFileNamesInAFolder(String folderName, String fileType) throws GIException;
	List<String> getFileNamesInAFolder(String folderName, String fileType) throws GIException;
	List<String> getExcelFileNamesInCarrierFolder(String folderName, String fileType) throws GIException;
	*/
	boolean setMissingEnrolleePhoneNumbers() throws GIException;
} 
