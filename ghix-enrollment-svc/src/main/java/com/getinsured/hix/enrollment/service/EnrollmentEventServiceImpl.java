package com.getinsured.hix.enrollment.service;

import java.util.Date;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



import com.getinsured.hix.enrollment.repository.IEnrollmentEventRepository;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;

@Service("enrollmentEventService")
@Transactional
public class EnrollmentEventServiceImpl implements EnrollmentEventService {

	
	@Autowired private IEnrollmentEventRepository enrollmentEventRepository;
	@Override
	public void saveEnrollmentEvent(EnrollmentEvent enrollmentEvent){
	
		enrollmentEventRepository.saveAndFlush(enrollmentEvent);
	
	}
	@Override
	public EnrollmentEvent findEnrollmentByeventIDAndplanID(Integer eventID,
			Integer planID){
		EnrollmentEvent enrollmentEvent=null;
		
		enrollmentEvent= enrollmentEventRepository.findEnrollmentByeventIDAndplanID(eventID, planID);
		
		return enrollmentEvent;
	}

	@Override
	public List<EnrollmentEvent> findEventByenrollmentIDAndEnrolleeIDAndCreatedDate(Integer enrollmentID, Integer enrolleeID, Date createdDate){
	
		return enrollmentEventRepository.findEventByenrollmentIDAndEnrolleeIDAndCreatedDate(enrollmentID, enrolleeID, createdDate);
	}
	@Override
	public Date getEnrollmentTerminationDate(Integer enrollmentId) {
		return enrollmentEventRepository.findTerminatedDateForEnrollment(enrollmentId);
	}
	
	
	@Override
	public List<EnrollmentEvent> findSubscriberEventsForEnrollment(Integer enrollmentId) {
		return enrollmentEventRepository.findSubscriberEventsForEnrollment(enrollmentId);
	}
}
