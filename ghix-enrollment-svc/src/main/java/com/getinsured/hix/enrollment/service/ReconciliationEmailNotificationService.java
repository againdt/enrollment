package com.getinsured.hix.enrollment.service;

import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

public interface ReconciliationEmailNotificationService {
	
	void sendReconAlertEmail(Map<String, String> requestMap) throws GIException;

}
