package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.enrollment.EdiResponseLog;

public interface IEdiResponseLogRepository  extends JpaRepository<EdiResponseLog, Integer>{

}
