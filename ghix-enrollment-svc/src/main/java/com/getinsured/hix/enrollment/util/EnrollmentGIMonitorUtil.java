/**
 * 
 */
package com.getinsured.hix.enrollment.util;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.gimonitor.service.GIErrorCodeService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.service.UserService;

/**
 * Utility class to log enrollment validations in the GI monitor table
 * @author negi_s
 * @since 17/01/2017
 */
@Component
public class EnrollmentGIMonitorUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentGIMonitorUtil.class);
	
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private GIErrorCodeService giErrorCodeService;
	@Autowired private UserService userService;
	
	/**
	 * Log into GI monitor table
	 * @param component
	 * @param giErrorCode
	 * @param summary
	 * @param description
	 */
	public void logToGiMonitor(String component, String giErrorCode, String summary, String description){
		AccountUser user =null;
		try{
			user = userService.getLoggedInUser();
			GIMonitor giMonitor = new GIMonitor();
			giMonitor.setComponent(null != component ? component.toUpperCase() : null);
			giMonitor.setEventTime(new TSDate());
			giMonitor.setException(summary);
			giMonitor.setExceptionStackTrace(description);
			giMonitor.setUser(user);
			giMonitor.setErrorCode(giErrorCodeService.getGIErrorCodeByErrorCode(giErrorCode));
			giMonitorService.saveOrUpdateGIMonitor(giMonitor);
		}catch(Exception ex){
			LOGGER.error("Exception occured while persisting to GI_MONITOR",ex);
		}
	}
}
