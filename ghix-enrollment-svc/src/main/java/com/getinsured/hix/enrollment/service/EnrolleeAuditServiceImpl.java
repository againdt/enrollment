package com.getinsured.hix.enrollment.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.history.Revision;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeAud;


@Service("enrolleeAuditService")
@Transactional
public class EnrolleeAuditServiceImpl implements EnrolleeAuditService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrolleeAuditServiceImpl.class);
	@Autowired	private IEnrolleeRepository enrolleeRepository;
	@Autowired	private IEnrolleeAudRepository enrolleeAudRepository;
	
	@Override
	public Revision<Integer, Enrollee> findRevisionByDate(int enrolleeId,  Date selectDate) throws ParseException {

		Revision<Integer, Enrollee> revision = enrolleeRepository.findRevisionByDate(enrolleeId, selectDate,EnrollmentConstants.UPDATED_ON);
		if (revision != null){
			return revision;
		}
		return null;
	}
	
	public Revision<Integer, Enrollee> findRevisionByEvent(int enrolleeId,  Integer lastEventID) throws ParseException {

		Revision<Integer, Enrollee> revision = enrolleeRepository.findRevisionByEvent(enrolleeId, lastEventID,"lastEventId");
		if (revision != null){
			return revision;
		}
		return null;
	}
	
	@Override
	public List<Integer> getEnrolleeIdsForCarrierUpdate(Date lastRunDate, AccountUser user) {
		
		if(user!=null){
			if(lastRunDate!=null){
				return enrolleeAudRepository.getEnrolleeIdForCarrierUpdatedIssuers(lastRunDate, user.getId(), EnrollmentConstants.ENROLLMENT_STATUS_PENDING);
			}else{
				return enrolleeAudRepository.getEnrolleeIdForCarrierUpdatedIssuers(user.getId(), EnrollmentConstants.ENROLLMENT_STATUS_PENDING);
			}
		}else{
			return null;
		}
	}
	
	@Override
	public List<EnrolleeAud> getEnrolleeAudForCarriersUpdatedEnrollments(Date lastRunDate, AccountUser user,Integer enrolleeID) {
		LOGGER.info("userService: "+user);
		LOGGER.info("lastRunDate: "+lastRunDate);
		if(lastRunDate!=null){
			return enrolleeAudRepository.getEnrolleeAudForCarrierUpdatedEnrollments(lastRunDate, user.getId(), enrolleeID);
		}else{
			return enrolleeAudRepository.getEnrolleeAudForCarrierUpdatedEnrollments(user.getId(), enrolleeID);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Integer> getEnrolleeAudIdForResendInd21(Integer enrollmentId, AccountUser user){
		return enrolleeAudRepository.getEnrolleeAudIdForResendInd21(enrollmentId, user.getId());
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<EnrolleeAud> getEnrolleeAudForCarrierUpdatedEnrollmentsByEnrolleeId(Integer enrollmentId,AccountUser user, Integer enrolleeAudId){
		return enrolleeAudRepository.getEnrolleeAudForCarrierUpdatedEnrollmentsByEnrolleeId(enrollmentId, user.getId(), enrolleeAudId);
	}
	
}
