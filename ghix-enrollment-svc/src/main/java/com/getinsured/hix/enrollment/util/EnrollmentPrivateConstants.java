/**
 * 
 */
package com.getinsured.hix.enrollment.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * @author Priya
 *
 * In this class we can define constants for Enrollment Module.
  * 
 */


@Component
public final class EnrollmentPrivateConstants {

	private EnrollmentPrivateConstants() {
	}
	
	
	
	public static final String ID = "id";
	public static final String DEFAULT_SORT_COLUMN = "id";
	public static final String ORDER_ID = "orderId";
	public static final String ORDER_ITEM_ID = "orderItemId";
	public static final String PLAN_ID = "planId";
	public static final String CASE_ID = "caseId";
	public static final String PLAN_TIER_ID = "planTierId";
	public static final String ENROLLMENT_ID = "enrollmentId";
	public static final String ENROLLMENT_ID_KEY = "EnrollmentID";
	public static final String EMPLOYEE_ID_KEY = "EmployeeID";
	public static final String EMPLOYER_ID = "employerid";
	public static final String MEMBER_ID = "memberId";
	public static final String MEMBER_ID_KEY = "MemberId";
	public static final String ASSISTER_BROKER_ID = "assisterBrokerId";
	public static final String EXISTING_QHP_ENROLLMENT_ID = "existingQhpEnrollmentId";
	public static final String EXISTING_SADP_ENROLLMENT_ID = "existingSadpEnrollmentId";
	public static final String EMPLOYEE_ID = "employeeId";
	public static final String CUSTODIAL_PARENT_ID = "custodialParentId";
	public static final String RESPONSIBLE_PERSON_ID = "responsiblePersonId";
	public static final String HOUSEHOLD_CONTACT_ID = "houseHoldContactId";
	public static final String ENROLLEE_ID = "enrolleeId";
	public static final String POLICY_ID = "policyId";
	
	public static final String QHP_DIS_ENROLLMENT_END_DATE = "QHPDisenrollmentEndDate";
	public static final String SADP_DIS_ENROLLMENT_END_DATE = "SADPDisenrollmentEndDate";
	public static final String TERMINATION_DATE = "TerminationDate";
	public static final String DEATH_DATE = "DeathDate";
	public static final String CURRENT_DATE = "currentDate";
	public static final String LAST_INVOICE_DATE = "lastInvoiceDate";
	public static final String EFFECTIVE_START_DATE = "effectiveStartDate";
	public static final String EFFECTIVE_END_DATE = "effectiveEndDate";
	public static final String LAST_PREMIUM_PAID_DATE = "lastPremiumPaidDate";
	
	
	
	public static final String EVENT_REASON = "EVENT_REASON";
	public static final String EVENT_TYPE = "EVENT_TYPE";
	public static final String LOOKUP_ENROLLMENT_TYPE = "ENROLLMENT_TYPE";
	public static final String LOOKUP_EXCHANGE_TYPE = "EXCHANGE_TYPE";
	public static final String EXCHANGE_TYPE_MANUAL = "Manual";
	public static final String LOOKUP_INSURANCE_TYPE = "INSURANCE_TYPE";
	public static final String LOOKUP_INSURANCE_TYPE_HEALTH = "HLT";
	public static final String LOOKUP_INSURANCE_TYPE_LIFE_CODE = "LIFE";
	public static final String LOOKUP_INSURANCE_TYPE_VISION_CODE = "VISION";
	public static final String LOOKUP_INSURANCE_TYPE_DENTAL_CODE = "DEN";
	public static final String LOOKUP_PREFERRED_TIME_TO_CONTACT = "PREFERRED_TIME_TO_CONTACT";
	public static final String LOOKUP_TYPE_RACE = "RACE";
	public static final String LOOKUP_PERSON_TYPE = "PERSON_TYPE";
	public static final String LOOKUP_PERSON_TYPE_ENROLLEE = "ENROLLEE";
	public static final String LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON = "RESPONSIBLE_PERSON";
	public static final String LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT = "CUSTODIAL_PARENT";
	public static final String LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT = "HOUSEHOLD_CONTACT";
	
	public static final String LOOKUP_EDE_TYPE = "EDE_FLOW";
	public static final String LOOKUP_EDE_SELF_CODE = "EDE_SELF";
	public static final String LOOKUP_EDE_AGENT_CODE = "EDE_AGENT";
	
	public static final String PERSON_TYPE_SUBSCRIBER = "SUBSCRIBER";
	public static final String ENROLLMENT_STATUS = "ENROLLMENT_STATUS";
	public static final String ENROLLMENT_STATUS_CONFIRM = "CONFIRM";
	public static final String ENROLLMENT_STATUS_CANCEL = "CANCEL";
	public static final String ENROLLMENT_STATUS_TERM = "TERM";
	public static final String ENROLLMENT_STATUS_PENDING = "PENDING";
	public static final String ENROLLMENT_STATUS_ECOMMITTED = "ECOMMITTED";
	public static final String ENROLLMENT_PAYMENT_RECEIVED = "PAYMENT_RECEIVED";
	public static final String ENROLLMENT_STATUS_ABORTED = "ABORTED";
	public static final String ENROLLMENT_STATUS_SOLD = "SOLD";
	public static final String ENROLLMENT_STATUS_INFORCE = "INFORCE";
	public static final String BATCH_JOB_STATUS= "COMPLETED";
	public static final String GENDER = "GENDER";
	public static final String MARITAL_STATUS = "MARITAL_STATUS";
	public static final String LANGUAGE_SPOKEN = "LANGUAGE_SPOKEN";
	public static final String LANGUAGE_WRITTEN = "LANGUAGE_WRITTEN";
	public static final String TOBACCO_USAGE = "TOBACCO_USAGE";
	public static final String RELATIONSHIP = "RELATIONSHIP";
	public static final String HIGH = "HIGH";
	public static final String CITIZENSHIP_STATUS = "CITIZENSHIP_STATUS";
	public static final String MAINTENANCE_REASON_CODE = "maintenanceReasonCode";
	public static final String EXCHANGE_TYPE_OFFEXCHANGE = "OFFEXCHANGE";
	public static final String EXCHANGE_TYPE_ONEXCHANGE = "ONEXCHANGE";
	public static final String EXCHANGE_TYPE_ONEXCHANGE_ECOMMITTED = "ONEXCHANGE_ECOMMITTED";
	public static final String EXCHANGE_TYPE_D2C = "D2C";
	public static final String STM_LOOKUP_VALUE_CODE = "STM";
	public static final String STM_LOOKUP_VALUE_LABEL = "Short Term";
	public static final String AME_LOOKUP_VALUE_CODE = "AME";
	public static final String AME_LOOKUP_VALUE_LABEL = "Accident Medical";
	public static final String DENTAL_LOOKUP_VALUE_CODE = "DEN";
	public static final String FFM_AGENT_SUBMIT = "FFM_AGENT_SUBMIT";
	public static final String MODALITY_FFM_REDIRECT = "FFM_REDIRECT";
	public static final String MODALITY_FFM_PROXY = "FFM_PROXY";
	public static final String MODALITY_FFM_ECOMMIT = "ECOMMIT";
	public static final String MODALITY_CA_PROXY = "CA_PROXY";
			
	public static final String YES = "YES";
	public static final String NO = "NO";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String ON = "ON";
	public static final String OFF = "OFF";
		
	public static final String STATUS = "status";
	public static final String ENROLLMENT_STATUS_KEY1 = "EnrollmentStatus";
	public static final String ENROLLMENT_STATUS_KEY = "enrollmentStatus";
	public static final String INDIVIDUAL_ENROLLMENT_BATCH_JOB="enrollmentXMLIndividualJob";
	public static final String CMS_DAILY_ENROLLMENT_BATCH_JOB="cmsDailyEnrollmentReportJob";
	public static final String CMS_RECONCILIATION_ENROLLMENT_REPORT_JOB="cmsReconciliationEnrollmentReportJob";
	public static final String SEND_CARRIER_UPDATED_ENROLLMENT_JOB= "sendCarrierUpdatedEnrollmentJob";
	public static final String SHOP_ENROLLMENT_BATCH_JOB="enrollmentXMLShopJob";
	public static final String DEPENDENT_CONTRIBUTION = "dependentContribution";
	public static final String IND_RECON_EXTRACT_MONTH_CURRENT="current";
	public static final String IND_RECON_EXTRACT_MONTH_PRIOR="prior";
	public static final String SUBSCRIBER_FLAG = "subscriberFlag";
	public static final String PROGRAM_TYPE = "programType";
	public static final String APPLICANT_ESIG = "applicant_esig";
	public static final String COVERAGE_START_DATE = "coverageStartDate";
	public static final String GROSS_PREMIUM_COST = "grossPremiumCost";
	public static final String GROSS_PREMIUM_AMT = "grossPremiumAmt";
	public static final String NET_PREMIUM_AMT = "netPremiumAmt";
	public static final String EMPLOYEE_CONTRIBUTION = "employeeContribution";
	public static final String EMPLOYER_CONTRIBUTION = "employerContribution";
	public static final String APTC_APPLIED_AMOUNT = "aptcAppliedAmount";
	public static final String NET_PREMIUM_AMOUNT = "netPremiumAmount";
	public static final String SADP = "sadp";
	public static final String ASSISTER_BROKER_ROLE = "assisterBrokerRole";
	public static final String EMPLOYER_CASE_ID = "employerCaseId";
	public static final String MEMBER_DETAILS = "memberDetails";
	public static final String PLAN_DETAILS = "planDetails";
	public static final String DIS_ENROLLMENT_TYPE = "DisEnrollmentType";
	public static final String TERMINATION_REASON_CODE = "TerminationReasonCode";
	public static final String CARRIER_STAT_UPDATE_JOB_NAME= "carrierStatusUpdate";
	public static final String EMPLOYER = "employer";
	public static final String HEALTH_COVERAGE_POLICY_NO = "healthCoveragePolicyNo";
	public static final String PLAN_TYPE = "planType";
	public static final String INSURANCE_APPLICANT_ID = "insuranceApplicantId";
	public static final String EXCH_ASIGN_APPLICANT_ID = "exchAssignedInsuApplicantId";
	public static final String FFM_PERSON_ID = "personId";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String MIDDLE_NAME = "middleName";
	public static final String DOB = "dob";
	public static final String PRIMARY_PHONE = "primaryPhone";
	public static final String SECONDARY_PHONE = "secondaryPhone";
	public static final String PREFERRED_PHONE = "preferredPhone";
	public static final String PREFERRED_EMAIL = "preferredEmail";
	public static final String SSN = "ssn";
	public static final String SUFFIX = "suffix";
	public static final String INDIVIDUAL_PREMIUM = "individualPremium";
	public static final String RATING_AREA = "ratingArea";
	public static final String GENDER_CODE = "genderCode";
	public static final String MARITAL_STATUS_CODE = "maritalStatusCode";
	public static final String SPOKEN_LANGUAGE_CODE = "spokenLanguageCode";
	public static final String WRITTEN_LANGUAGE_CODE = "writtenLanguageCode";
	public static final String RELATIONSHIP_CODE = "relationshipCode";
	public static final String TOBACCO = "tobacco";
	public static final String LAST_TOBACCO_USE_DATE = "lastTobaccoUseDate";
	public static final String HOUSEHOLD_CONTACT_RELATIONSHIP = "houseHoldContactRelationship";
	public static final String CITIZENSHIP_STATUS_CODE = "citizenshipStatusCode";
	public static final String DESCRIPTION = "description";
	public static final String APTC = "aptc";
	public static final String MARKET_TYPE = "marketType";
	public static final String UPDATED_ON = "updatedOn";
		
	public static final String SORT_BY = "sortBy";
	public static final String SORT_ORDER = "sortOrder";
	public static final String START_RECORD = "startRecord";
	public static final String PAGE_SIZE = "pageSize";
	public static final String PAGE_NUMBER = "pageNumber";
	public static final String ORDER_BY="ORDER BY ";
	public static final String DISENROLL_SADP_FLAG="disenrollSadpFlag";
	public static final String DISENROLL_QHP_FLAG="disenrollQhpFlag";
	 
	public static final String EVENT_TYPE_CHANGE="001";
	public static final Integer AGE_18=18;
	public static final String REASON_CODE_DEATH="03";
	public static final String RELATIONSHIP_LOOKUPVALUE_CODE = "18";
	public static final String EVENT_TYPE_ADDITION="021";
	public static final String EVENT_TYPE_CANCELLATION="024";
	public static final String ENROLLMENT_TYPE_SHOP = "24";
	public static final String EVENT_REASON_INITIAL_ENROLLMENT = "28";
	public static final String EVENT_REASON_NON_PAYMENT = "59";
	public static final String MODULE_STATUS_CODE_E000 = "E-000";
	public static final String MODULE_STATUS_CODE_E003 = "E-003";
	public static final Integer DEFAULT_START_RECORD = 0;
	public static final Integer DEFAULT_PAGE_NUMBER = 1;
	
	public static final String EVENT_REASON_BENEFIT_SELECTION = "EC";
	public static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
	public static final String RESPONSIBLE_PERSON_ID_CODE_QD = "QD";
	public static final String DEFAULT_SORT_ORDER = "ASC";
	
	
	public static final String FFM_ENROLLMENT_TXN_TYPE_E = "E";
	
	public static final String ENROLLMENT_TYPE_INITIAL = "I";
	public static final String ENROLLMENT_TYPE_SPECIAL = "S";
	public static final String ENROLLMENT_TYPE_NEW = "N";
	public static final String SUBSCRIBER_FLAG_YES = "Y";
	public static final String SUBSCRIBER_FLAG_NO = "N";
	public static final String ASSISTER_ROLE = "Assister";
	public static final String AGENT_ROLE = "Agent";
	public static final String CSR_ROLE = "CSR";
	public static final String CONSUMER_ROLE = "CONSUMER";
	
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String T = "T";
	public static final String F = "F";
	
	public static final String ERR_MSG_HOUSEHOLD_IS_NULL = "Invalid input: HouseHold is Null";
	public static final String ERR_MSG_ENROLLMENT_STATUS_IS_NULL = "Invalid input: Enrollment Status is Null";
	public static final String ERR_MSG_EMPLOYER_ID_IS_NULL = "Invalid input: EmployerId is Null";
	public static final String ERR_MSG_ENROLLMENT_ID_IS_NULL = "Invalid input: EnrollmentId is Null";
	public static final String ERR_MSG_EMPLOYEE_ID_IS_NULL = "Invalid input: EmployeeId is Null";
	
	public static final String ERR_MSG_ADMIN_UPDATE_INDIVIDUAL_REQUEST_IS_NULL = "Invalid input: AdminUpdateIndividualRequest is Null";
	public static final String ERR_MSG_ENROLLEE_REQUEST_IS_NULL = "Invalid input: Enrollee request is Null";
	
	public static final String ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY = "Invalid input: Search criteria is Null or empty";
	public static final String ERR_MSG_ORDER_ID_NULL_OR_EMPTY = "Invalid input: orderId is Null or empty";
	public static final String ERR_MSG_EMPLOYER_ID_NULL_OR_EMPTY = "Invalid input: Employer Id is Null or empty";
	public static final String ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY = "Invalid input: Enrollment ID is Null or Empty";
	public static final String ERR_MSG_EMPLOYEE_ID_IS_NULL_OR_EMPTY = "Invalid input: Employee ID is Null or Empty";
	public static final String ERR_MSG_TERMINATION_DATE_IS_NULL_OR_EMPTY = "Invalid input: Termination date is Null or Empty";
	public static final String ERR_MSG_TERMINATION_DATE_FORMAT_ERROR = "Invalid input: Termination date is required in MM/dd/yyyy format";
	public static final String ERR_MSG_MEMBER_ID_IS_NULL_OR_EMPTY = "Invalid input: MemberId ID is Null or Empty";
	public static final String ERR_MSG_CURRENT_DATE_NULL_OR_EMPTY = "Invalid input: currentDate is null  or empty";
	public static final String ERR_MSG_STATUS_NULL_OR_EMPTY = "Invalid input: status is null or empty";
	public static final String ERR_MSG_ENROLLMENT_STATUS_NULL_OR_EMPTY = "Invalid input: EnrollmentStatus is Null Or Empty";
	public static final String ERR_MSG_DIS_ENROLLMENT_TYPE_NULL_OR_EMPTY = "Invalid input: DisEnrollmentType is Null Or Empty";
	public static final String ERR_MSG_TERMINATION_REASON_CODE_NULL_OR_EMPTY = "Invalid input: TerminationReasonCode is Null Or Empty";
	public static final String ERR_MSG_DEATH_DATE_NULL_OR_EMPTY = "Invalid input: DeathDate is Null Or Empty";
	public static final String ERR_MSG_CARRIER_CONFIRMATION_NUMBER_NULL_OR_EMPTY = "Invalid input: carrierConfirmationNumber is Null Or Empty";
	
	public static final String ERR_MSG_ID_CANNOT_BE_ZERO = "Invalid input: Id cannot be zero";
	public static final String ERR_MSG_INVALID_ENROLLMENT_ID = "Invalid input: Invalid Enrollment ID";
	public static final String ERR_MSG_INVALID_TICKET_ID = "Invalid input: Invalid Ticket ID";
	
	public static final String ERR_MSG_ENROLLMENT_STATUS_TERM_OR_CANCEL = "Invalid input: EnrollmentStatus should be TERM / CANCEL";
		
	
	public static final String ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO = "Plan data list received from plan selection service is null or size is zero";
	public static final String ERR_MSG_EMPLOYEE_COUNT_DOES_NOT_MATCH_EMPLOYEE_ENROLLMENTS = "Employee count does not match employee enrollments";
	
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND = "No Enrollment found for given search criteria.";
	public static final String ERR_MSG_NO_PLAN_DATA_FOUND = "No plan data found";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_CURRENT_MONTH = "No Enrollment found for current month.";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_STATUS_AND_DATE = "No Enrollment found with given status and termination date.";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_STATUS_EMPLOYER_AND_DATE = "No enrollment found with given status, employer and termination date.";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_CARRIER_FEED_DATA = "No enrollment found with given Carrier AppID,UID,FirstName and LastName.";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_EMPLOYER_CASE_ID = "Employer has no associated employee enrollments ";
	public static final String ERR_MSG_NO_PLAN_DATA_FOUND_IN_PLAN_SELECTION_SERVICE_RESPONSE = "No Plan data found in Plan selection service response";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID = "No enrollment found for the given enrollment Id";
	public static final String ERR_MSG_NO_RESULTS_FOUND = "No results found for given input";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_EMPLOYEE_ID = "No Enrollment found for given Employee Id";	
	public static final String ERR_MSG_NO_ENROLLEE = "No Enrollee found with id:";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_TICKET_ID = "No Enrollment found for given Ticket Id";
	public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_ID = "No Enrollment found for given Id";
    public static final String ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL = "Invalid input: Enrollment request is Null";
	
	public static final String MSG_ECOMMITTED_ENROLLMENT_PROCESS_SUCCESS = "ECommitted Enrollment Processed Successfully";
	public static final String MSG_ENROLLMENT_PROCESS_SUCCESS = "Enrollment Processed Successfully";
	public static final String MSG_ENROLLMENT_UPDATE_SUCCESS = "Enrollment updated successfully";
	public static final String MSG_SHOP_ENROLLMENT_UPDATE_SUCCESS = "Shop enrollment updated successfully";
	public static final String MSG_RETRIEVE_ENROLLMENT_SUCCESS = "Fetching enrollment is successfully done.";
	public static final String MSG_RETRIEVE_ENROLLEE_SUCCESS = "Fetching enrollee is successfully done.";
	
	public static final String ERR_MSG_FAILED_TO_CALL_PLANDISPLAY_ENROLLMENT_DATA_SERVICE = "Failed to call 'Plan Display' findByOrderId service from enrollment";
	public static final String ERR_MSG_ENROLLMENT_CREATE_FAILURE = "Failed to create Enrollment";
	public static final String ERR_MSG_ECOMMITTED_ENROLLMENT_TICKET_CREATE_FAILURE = "Failed to create ticket for ECommitted Enrollment";
	public static final String ERR_MSG_ECOMMITTED_ENROLLMENT_CREATE_FAILURE = "Failed to create ECommitted Enrollment";
	public static final String ERR_MSG_ECOMMITTED_ESIGN_CREATE_FAILURE = "Failed to create Enrollment Esign";
	public static final String ERR_MSG_ENROLLMENT_UPDATE_FAIL = "Failed to update enrollment";
	public static final String ERR_MSG_DIS_ENROLLMENT_FAIL = "Dis-Enrollment has failed";
	public static final String ERR_MSG_SHOP_ENROLLMENT_UPDATE_FAIL = "Failed to update shop enrollment";
	public static final String ERR_MSG_ENROLLMENT_RETRIEVE_FAIL = "Failed to retrieve the enrollment";
	public static final String ERR_MSG_ENROLLEE_RETRIEVE_FAIL = "Failed to retrieve the enrollee";
	public static final String ERR_MSG_HOUSE_HOLD_CONTACT_RETRIEVE_FAIL = "Failed to retrieve the house hold contact for the given enrollment id";
	public static final String ERR_MSG_PLAN_DISPLAY_ORDER_CONFIRM_API_FAIL = "Plandisplay OrderConfirmApi Failed for Order id: ";
	public static final String ERR_MSG_ERROR_CREATING_D2C_ENROLLMENT="Error creating D2C Enrollment";
	public static final String EXT_SUCCESS_RESPONSE_MESSAGE = "Enrollment created successfully";
	
	public static final int ERROR_CODE_200 = 200;
	public static final int ERROR_CODE_201 = 201;
	public static final int ERROR_CODE_202 = 202;
	public static final int ERROR_CODE_203 = 203;
	public static final int ERROR_CODE_204 = 204;
	public static final int ERROR_CODE_205 = 205;
	public static final int ERROR_CODE_206 = 206;
	public static final int ERROR_CODE_207 = 207;
	public static final int ERROR_CODE_208 = 208;
	public static final int ERROR_CODE_209 = 209;
	public static final int ERROR_CODE_250 = 250;
	public static final int ERROR_CODE_299 = 299;
	public static final int ERROR_CODE_301 = 301;
	
	public static final String BATCH_DATE_FORMAT = "yyyyMMddHHmmss";
	public static final String DEFAULT_BATCH_START_DATE = "19000101010000";
	public static final String DEFAULT_TIMESTAMP_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
	
	public static final String RATING_AREA_PREFIX = "R-";
		
	//public static String STATE_CODE;
	
	public static final String GROUP_ENROLLMENT_STATUS_N = "N";
	public static final String GROUP_ENROLLMENT_STATUS_Y = "Y";
	public static final String GROUP_ENROLLMENT_BATCH_JOB="enrollmentXMLGroupJob";
	
	public static final String GROUP_ACTION_TYPE="EnrollmentAction";
	public static final String GROUP_ACTION_TYPE_TERM="Terminate";
	public static final String GROUP_ACTION_TYPE_INITIAL="OpenEnrollment";
	public static final String GROUP_ACTION_TYPE_MAIN="Maintenance";
	
	public static final String WIP_FOLDER_NAME="WIP";
	public static final String SUCCESS_FOLDER_NAME="SUCCESS";
	public static final String FAILURE_FOLDER_NAME="FAILURE";
	public static final String FILE_TYPE_XML=".xml";
	public static final String FILE_TYPE_EXCEL=".XLS";
	public static final String FILE_TYPE_CSV=".csv";
	public static final String CARRIER_FILE_NAME = "PFG_StatusFeed_ADA_";
	
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int TEN = 10;
	/*@Value("#{combinedConfig.getString('global.StateCode')}")
	public void setStateCode(String stateCode){
		STATE_CODE = stateCode;
	}*/
	public static final String RESEND_FLAG = "RESEND";
	public static final String HOLD_FLAG = "HOLD";
	public static final String SENT_FLAG = "SENT";
	
	/*************************************************************************/
	/********************  FFM CONFIG properties      ************************/
	/*************************************************************************/
	
	//public static String FFM_NPN;
	public static String FFM_INFORMATION_EXCHANGE_SYSTEM_ID;
	
	
	/*@Value("#{configProp['defaultNPN']}")
	public void setFFM_NPN(String fFM_NPN) {
		FFM_NPN = fFM_NPN;
	}*/	
	
	
	@Value("#{configProp['ffmPartnerId']}")
	public void setFFM_INFORMATION_EXCHANGE_SYSTEM_ID(String fFM_INFORMATION_EXCHANGE_SYSTEM_ID) {
		FFM_INFORMATION_EXCHANGE_SYSTEM_ID = fFM_INFORMATION_EXCHANGE_SYSTEM_ID;
	}
	
	/**
	 * 
	 * @param input
	 * @return
	 */
	public static String toTitleCase(String input) {
	    StringBuilder titleCase = new StringBuilder();
	    boolean nextTitleCase = true;

	    for (char c : input.toCharArray()) {
	        if (Character.isSpaceChar(c)) {
	            nextTitleCase = true;
	        } else if (nextTitleCase) {
	            c = Character.toTitleCase(c);
	            nextTitleCase = false;
	        }

	        titleCase.append(c);
	    }

	    return titleCase.toString();
	}	
}