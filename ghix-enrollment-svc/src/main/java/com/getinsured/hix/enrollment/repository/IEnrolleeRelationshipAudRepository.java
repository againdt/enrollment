package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.enrollment.EnrolleeRelationshipAud;

public interface IEnrolleeRelationshipAudRepository extends JpaRepository<EnrolleeRelationshipAud, AudId> {
	@Query("FROM EnrolleeRelationshipAud as en " +
			" WHERE en.sourceEnrollee.id = :sourceID " +
			" and en.targetEnrollee.id = :targetID and en.rev = :rev")
	EnrolleeRelationshipAud findrelationShipBySourceTargetRev(
			@Param("sourceID") Integer sourceID,
			@Param("targetID") Integer targetID, @Param("rev") Integer rev);
	
	@Query("FROM EnrolleeRelationshipAud as en " +
			" WHERE en.sourceEnrollee.id = :sourceID " +
			" and en.targetEnrollee.id = :targetID and en.rev = :rev")
	List<EnrolleeRelationshipAud> findRelationShipRevisionBySourceTargetRev(
			@Param("sourceID") Integer sourceID,
			@Param("targetID") Integer targetID, @Param("rev") Integer rev);

	@Query("FROM EnrolleeRelationshipAud as en " +
			" WHERE en.sourceEnrollee.id = :sourceID " +
			" and en.targetEnrollee.id = :targetID " +
			" and en.updatedOn = (select MAX(updatedOn) FROM EnrolleeRelationshipAud " +
			                     " WHERE sourceEnrollee.id = :sourceID and targetEnrollee.id = :targetID " +
			                     " and updatedOn <= :updatedDate ) ORDER BY updatedOn DESC")
	List<EnrolleeRelationshipAud> findLastRelationAudit(@Param("sourceID") Integer sourceID,
			@Param("targetID") Integer targetID, @Param("updatedDate") Date updatedDate);

	
	
	@Query("FROM EnrolleeRelationshipAud as en " +
			" WHERE en.sourceEnrollee.id = :sourceID " +
			" and en.targetEnrollee.id = :targetID " +
			" and en.updatedOn = (select MIN(updatedOn) FROM EnrolleeRelationshipAud " +
			                     " WHERE sourceEnrollee.id = :sourceID and targetEnrollee.id = :targetID " +
			                     " and updatedOn >= :updatedDate ) ORDER BY updatedOn DESC")
	List<EnrolleeRelationshipAud> findOldestRelationAudit(@Param("sourceID") Integer sourceID,
			@Param("targetID") Integer targetID, @Param("updatedDate") Date updatedDate);

}
