/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.EnrollmentAud;
import com.getinsured.hix.platform.repository.TenantAwareRepository;


public interface IEnrolleeAudRepository extends TenantAwareRepository<EnrolleeAud, AudId>{
	
	@Query("select count(en.id) FROM EnrolleeAud as en " +
			" WHERE en.enrollment.id = :enrollmentID and en.rev = :rev " +
			" AND en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT') " +
			" AND en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	long countActiveEnrolleeForRevision(@Param("enrollmentID") Integer enrollmentID, @Param("rev") Integer rev);
	
/*	@Query("SELECT ena1 FROM EnrolleeAud ena1 " +
			"WHERE ena1.enrollment.id = :enrollmentID "  +
			"AND ena1.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena1.rev = " +
			" (SELECT MAX(ena2.rev) " +
			"FROM EnrolleeAud ena2 " +
			"WHERE ena2.enrollment = :enrollmentID " +
			"AND ena2.rev <= :rev " +
			"AND ena2.personTypeLkp.lookupValueCode = 'SUBSCRIBER' )")	
	EnrolleeAud getEnrollSubscByRev(@Param("enrollmentID") Integer enrollmentID, @Param("rev") Integer rev);*/
	
	@Query("SELECT ena FROM EnrolleeAud ena " +
			"WHERE ena.enrollment.id = :enrollmentID "  +
			"AND ena.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena.rev = :rev")	
	List<EnrolleeAud> getEnrollSubscByRev(@Param("enrollmentID") Integer enrollmentID, @Param("rev") Integer rev);
	
	@Query("SELECT ena1 FROM EnrolleeAud ena1 " +
			"WHERE ena1.enrollment.id = :enrollmentID "  +
			"AND ena1.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena1.updatedOn = " +
			" (SELECT MAX(ena2.updatedOn) " +
			"FROM EnrolleeAud ena2 " +
			"WHERE ena2.enrollment = :enrollmentID " +
			"AND ena2.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena2.updatedOn <= :updatedDate)")
	List<EnrolleeAud> getEnrollSubscByDate(@Param("enrollmentID") Integer enrollmentID, @Param("updatedDate") Date updatedDate);
	
	
	@Query("SELECT ena1 FROM EnrolleeAud ena1 " +
			"WHERE ena1.enrollment.id = :enrollmentID "  +
			"AND ena1.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena1.updatedOn = " +
			" (SELECT MAX(ena2.updatedOn) " +
			"FROM EnrolleeAud ena2 " +
			"WHERE ena2.enrollment = :enrollmentID " +
			"AND ena2.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena2.effectiveStartDate < :coverageDate)")
	List<EnrolleeAud> getEnrollSubscByCovDate(@Param("enrollmentID") Integer enrollmentID, @Param("coverageDate") Date coverageDate);
	
	@Query("SELECT ena1 FROM EnrolleeAud ena1 " +
			"WHERE ena1.enrollment.id = :enrollmentID "  +
			"AND ena1.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena1.updatedOn = " +
			" (SELECT MAX(ena2.updatedOn) " +
			"FROM EnrolleeAud ena2 " +
			"WHERE ena2.enrollment = :enrollmentID " +
			"AND ena2.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena2.totIndvRespEffDate < :coverageDate)")
	List<EnrolleeAud> getEnrollSubscByAmtCovDate(@Param("enrollmentID") Integer enrollmentID, @Param("coverageDate") Date coverageDate);
	
	
	@Query("SELECT ena1.ratingArea FROM EnrolleeAud ena1 " +
			"WHERE ena1.enrollment.id = :enrollmentID "  +
			"AND ena1.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena1.updatedOn = " +
			" (SELECT MAX(ena2.updatedOn) " +
			"FROM EnrolleeAud ena2 " +
			"WHERE ena2.enrollment = :enrollmentID " +
			"AND ena2.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena2.ratingAreaEffDate < :coverageDate)")
	String getSubscRatingByCovDate(@Param("enrollmentID") Integer enrollmentID, @Param("coverageDate") Date coverageDate);
	
	
	@Query("SELECT ena1 FROM EnrolleeAud ena1 " +
			"WHERE ena1.enrollment.id = :enrollmentID "  +
			"AND ena1.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena1.updatedOn = " +
			" (SELECT MIN(ena2.updatedOn) " +
			"FROM EnrolleeAud ena2 " +
			"WHERE ena2.enrollment = :enrollmentID " +
			"AND ena2.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			"AND ena2.effectiveStartDate >= :coverageDate)")
	List<EnrolleeAud> geOldestSubscByDate(@Param("enrollmentID") Integer enrollmentID, @Param("coverageDate") Date coverageDate);
	
	@Query(" FROM EnrolleeAud as en " +
			" WHERE en.id = :enrolleeID and en.updatedOn = (select max(updatedOn) " +
			                                 " FROM  EnrolleeAud where id = :enrolleeID " +
			                                 " and updatedOn <= :updatedDate) ORDER BY updatedOn DESC")
	List<EnrolleeAud> findEnrolleeRevByDate(@Param("enrolleeID") Integer enrolleeID,  @Param("updatedDate") Date updatedDate);
	
	@Query(" FROM EnrolleeAud as en " +
			" WHERE en.id = :enrolleeID and en.updatedOn = (select max(updatedOn) " +
			                                 " FROM  EnrolleeAud where id = :enrolleeID " +
			                                 " and updatedOn < :updatedDate) ORDER BY updatedOn DESC")
	List<EnrolleeAud> findPreviousEnrolleeRevByDate(@Param("enrolleeID") Integer enrolleeID,  @Param("updatedDate") Date updatedDate);
	
	@Query(" FROM EnrolleeAud as en " +
			" WHERE en.id = :enrolleeID and en.lastEventId.id= :eventID " +
			" AND en.updatedOn = (select max(updatedOn) FROM  EnrolleeAud " +
			                    " where id = :enrolleeID and lastEventId.id = :eventID) ORDER BY en.updatedOn, en.rev ")
	List<EnrolleeAud> findRevisionByEvent(@Param("enrolleeID") Integer enrolleeID, @Param("eventID") Integer eventID);
	
	@Query(" FROM EnrolleeAud as en " +
			" WHERE en.id = :enrolleeID" +
			" AND en.updatedOn = (select max(updatedOn) FROM  EnrolleeAud " +
			                    " where id = :enrolleeID and updatedOn <= :updatedDate) ORDER BY en.updatedOn, en.rev DESC ")
	List<EnrolleeAud> findRevisionByEventDate(@Param("enrolleeID") Integer enrolleeID, @Param("updatedDate") Date updatedDate);
	
	@Query("FROM EnrolleeAud as en WHERE en.id = :enrolleeId and en.rev = :rev and en.lastEventId.id= :eventID ")
	List<EnrolleeAud> getEnrolleeAudByIdEventIdAndRev(
			@Param("enrolleeId") Integer enrolleeId,
			@Param("eventID") Integer eventID,
			@Param("rev") Integer rev);
	
	@Query("FROM EnrolleeAud as en WHERE en.id = :enrolleeId and en.rev = :rev ORDER BY en.updatedOn DESC")
	List<EnrolleeAud> getEnrolleeAudByIdAndRev(
			@Param("enrolleeId") Integer enrolleeId,
			@Param("rev") Integer rev);
	
/*	@Query(" FROM EnrolleeAud as en " +
			" WHERE en.id = :enrolleeAudID " +
			" and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','PENDING')")
	List<EnrolleeAud> getEnrolleeByIdRevAndNoPendingStatus(@Param("enrolleeAudID") Integer enrolleeAudID);*/
	
	/*@Query("select en.enrolleeLkpValue FROM EnrolleeAud as en where en.id = :enrolleeID " +
			" AND en.updatedOn = (select max(updatedOn) FROM  EnrolleeAud " +
			                     " where enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM') and id=:enrolleeID) " +
			                     " AND ROWNUM = 1 ")
	LookupValue getPrevStatusForReInstmt(@Param("enrolleeID") Integer enrolleeID);*/
	
	@Query("FROM EnrolleeAud as en WHERE en.enrollment.id = :enrollmentID "
			+ "and en.rev = :rev "
			+ "and TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ "and TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "
//			+ "and en.enrolleeLkpValue.lookupValueCode IN ('CONFIRM','TERM') "
			+ "and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT')")
	List<EnrolleeAud> getEnrolleesByEnrollmentIdAndRevisionNo(@Param("enrollmentID") Integer enrollmentID, @Param("rev") Integer rev, @Param("endDate") String endDateString, @Param("startDate") String startDateString);
	
	@Query("FROM EnrolleeAud as en "
			+ "WHERE en.id = :enrolleeId AND "
			+ "en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrolleeAud ena "
				+ "WHERE "
				+ "ena.id = :enrolleeId "
				+ "AND TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY')) ")
	EnrolleeAud getMaxEnrolleeAudPerMonth(@Param("enrolleeId") Integer enrolleeId, @Param("endDate") String endDateString);
			
	@Query(" FROM EnrolleeAud as en WHERE en.enrollment.id = :enrollmentID " +
		   " AND en.rev = :rev and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT') " +
		   " AND TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate,'MM/DD/YYYY')" +
		   " AND TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate,'MM/DD/YYYY') ")
	List<EnrolleeAud> getEnrolleesByEnrollmentIdAndRevisionNoIrsShop(@Param("enrollmentID") Integer enrollmentID, @Param("rev") Integer rev,@Param("endDate") String endDateString);
	
	@Query(" SELECT ee.id from EnrolleeAud ee "+
		   " WHERE ee.enrollment.id = :enrollmentID "+
		   " AND ee.rev = :rev "+
		   " AND ee.enrolleeLkpValue.lookupValueCode IN ('CANCEL','TERM') ")
	List<Integer> getEnrolleesForReInstateByRev(@Param("enrollmentID") Integer enrollmentID , @Param("rev") Integer rev);
	
	@Query("FROM EnrolleeAud as en "
			+ "WHERE en.id = :enrolleeId AND "
			+ "en.updatedOn = "
				+ "(SELECT MIN(ena.updatedOn) FROM EnrolleeAud ena "
				+ "WHERE "
				+ "ena.id = :enrolleeId ) ")
	EnrolleeAud getMinEnrolleeAud(@Param("enrolleeId") Integer enrolleeId);
	
	@Query("FROM EnrolleeAud as en1 "
			+ "WHERE en1.id = :enrolleeId AND "
			+ "en1.updatedOn = (select MAX(updatedOn) from EnrolleeAud as en2 WHERE en2.id = :enrolleeId AND en2.updatedOn <= :lastInvoiceDate)")
	EnrolleeAud getEnrolleeCoverageDetailsBeforeLastInvoice(@Param("enrolleeId") Integer enrolleeId , @Param("lastInvoiceDate") Date lastInvoiceDate);
	
	@Query("SELECT MIN(ena.updatedOn) FROM EnrolleeAud ena "
				+ "WHERE "
				+ "ena.id = :enrolleeId and ena.enrolleeLkpValue.lookupValueCode='CONFIRM'  ")
	Date getEnrolleeConfirmationDate(@Param("enrolleeId") Integer enrolleeId);
	
	@Query("FROM EnrolleeAud ee "+
			   " WHERE ee.enrollment.id = :enrollmentID "+
			   " AND ee.rev = :rev "+
			   " AND ee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT') "+
			   " AND ee.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM') ")
	List<EnrolleeAud> getEnrolleeAudByRev(@Param("enrollmentID") Integer enrollmentID , @Param("rev") Integer rev);
	
	@Query("SELECT DISTINCT ee FROM EnrolleeAud ee, EnrollmentEventAud ea "+
			   " WHERE ee.enrollment.id = :enrollmentID "+
			   " AND ee.rev = :rev "+
			   " AND ea.rev=:rev "+
			   " AND ee.lastEventId = ea.id "+
			   " AND ea.eventTypeLkp.lookupValueCode <>'024' "+
			   " AND ee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT') "
			   //+ " AND ee.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM') "
			)
	List<EnrolleeAud> getEnrolleeAudByRevRetro(@Param("enrollmentID") Integer enrollmentID , @Param("rev") Integer rev);
	
	@Query("FROM EnrolleeAud as en "
			+ "WHERE en.id = :enrolleeId AND "
			+ "en.updatedOn = "
				+ "(SELECT Max(ena.updatedOn) FROM EnrolleeAud ena "
				+ "WHERE "
				+ "ena.id = :enrolleeId  AND ena.updatedOn<:lastUpdateDate) ")
	EnrolleeAud getLastEnrolleeAud(@Param("enrolleeId") Integer enrolleeId, @Param("lastUpdateDate")Date lastUpdateDate);
	
	 @Query(" FROM EnrolleeAud as en WHERE en.id = :enrolleeId "
			   + " AND en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT')")
			 List<EnrolleeAud> getEnrolleeAudById(@Param("enrolleeId") Integer enrolleeId);
	 
	@Query("SELECT DISTINCT en.id FROM EnrolleeAud as en WHERE en.updatedOn >=  :lastRunDate and en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>:status")
	List<Integer> getEnrolleeIdForCarrierUpdatedIssuers(
			@Param("lastRunDate") Date lastRunDate,
			@Param("updatedBy") Integer updatedBy, @Param("status") String status);
	
	@Query("SELECT DISTINCT en.id FROM EnrolleeAud as en WHERE en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>:status")
	List<Integer> getEnrolleeIdForCarrierUpdatedIssuers(
			@Param("updatedBy") Integer updatedBy, @Param("status") String status);
	
	@Query("FROM EnrolleeAud as en WHERE en.id = :enrolleeID AND en.updatedOn >=  :lastRunDate and en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>'PENDING' ORDER BY en.id ASC, en.rev ASC")
	List<EnrolleeAud> getEnrolleeAudForCarrierUpdatedEnrollments(
			@Param("lastRunDate") Date lastRunDate,
			@Param("updatedBy") Integer updatedBy, @Param("enrolleeID") Integer enrolleeID);
	
	@Query("FROM EnrolleeAud as en WHERE en.id = :enrolleeID AND en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>'PENDING' ORDER BY en.id ASC, en.rev ASC")
	List<EnrolleeAud> getEnrolleeAudForCarrierUpdatedEnrollments(
			@Param("updatedBy") Integer updatedBy, @Param("enrolleeID") Integer enrolleeID);
	
	/**
	 * 
	 * @param enrollmentId
	 * @param updatedBy
	 * @return
	 */
	@Query("SELECT Distinct en.id FROM EnrolleeAud as en WHERE en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>'PENDING'  and en.enrollment.id = :enrollmentId")
	List<Integer> getEnrolleeAudIdForResendInd21(@Param("enrollmentId") Integer enrollmentId, @Param("updatedBy") Integer updatedBy);
	
	/**
	 * 
	 * @param enrollmentId
	 * @param updatedBy
	 * @param enrolleeId
	 * @return
	 */
	@Query("FROM EnrolleeAud as en WHERE en.id = :enrolleeId AND en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>'PENDING'  and en.enrollment.id = :enrollmentId ORDER BY en.id ASC, en.rev ASC")
	List<EnrolleeAud> getEnrolleeAudForCarrierUpdatedEnrollmentsByEnrolleeId(@Param("enrollmentId") Integer enrollmentId,@Param("updatedBy") Integer updatedBy, @Param("enrolleeId")Integer enrolleeId);		 
	 
	//@Query("FROM EnrolleeAud")
	//getOldestSubscriberForMonth(@Param("enrollmentId") Integer enrollmentId, @Param("monthStartDate") Date monthStartDate );
	
	@Query(" FROM EnrolleeAud as en " +
			" WHERE en.id = :enrolleeID and en.updatedOn = (select max(updatedOn) " +
			                                 " FROM  EnrolleeAud where id = :enrolleeID " +
			                                 " and lastTobaccoUseDate <= :updatedDate) ORDER BY updatedOn DESC")
	List<EnrolleeAud> findEnrolleeRevByTobacoDate(@Param("enrolleeID") Integer enrolleeID,  @Param("updatedDate") Date updatedDate);
	
	@Query(" FROM EnrolleeAud as en " +
			" WHERE en.id = :enrolleeID and revtype = 0 ")
	EnrolleeAud findInsertAudByEnrolleeId(@Param("enrolleeID") Integer enrolleeID);

	@Query("SELECT en.ratingAreaEffDate, en.ratingArea FROM EnrolleeAud as en "
			+ " WHERE en.enrollment.id = :enrollmentId AND en.ratingAreaEffDate IS NOT NULL "
			+ " AND en.ratingArea IS NOT NULL "
			+ " AND en.ratingAreaEffDate >= :effectiveStartDate AND en.ratingAreaEffDate <= :effectiveEndDate "
			+ " AND en.personTypeLkp.lookupValueCode='SUBSCRIBER' ORDER BY en.updatedOn DESC")
	List<Object[]> getRatingAreaWithEffectiveDates(@Param("enrollmentId") Integer enrollmentId,@Param("effectiveStartDate") Date effectiveStartDate,@Param("effectiveEndDate") Date effectiveEndDate);

	@Query(" SELECT en.rev, en.createdOn FROM EnrolleeAud as en " +
			" WHERE en.enrollment.id = :enrollmentId and en.lastEventId.id= :eventID " +
			" AND en.updatedOn = (select max(updatedOn) FROM  EnrolleeAud " +
			                    " where enrollment.id = :enrollmentId and lastEventId.id = :eventID) ORDER BY en.updatedOn, en.rev ")
	List<Object[]> findRevByIdAndEventId(@Param("enrollmentId") Integer enrollmentId, @Param("eventID") Integer eventID);
	
	
	@Query("SELECT en.id, en.firstName, en.lastName, en.exchgIndivIdentifier, en.effectiveEndDate, en.effectiveStartDate , en.personTypeLkp.lookupValueCode "
			+ "FROM EnrolleeAud as en WHERE en.enrollment.id = :enrollmentID "
			+ "and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT' ,'CUSTODIAL_PARENT')"
			+ "and en.rev = :rev ")
	List<Object[]> getEnrolleesByEnrollmentIdandRev(@Param("enrollmentID") Integer enrollmentID, @Param("rev") Integer rev);
	
	@Query("SELECT en.ratingArea FROM EnrolleeAud as en "
			+ " WHERE en.enrollment.id = :enrollmentId AND en.updatedOn = (SELECT MIN(ena.updatedOn) from EnrolleeAud as ena "
			+ " where ena.id = en.id and ena.effectiveStartDate = :effectiveStartDate) "
			+ " AND en.ratingArea IS NOT NULL "
			+ " AND en.personTypeLkp.lookupValueCode='SUBSCRIBER' ORDER BY en.updatedOn DESC")
	List<String> getFirstRatingAreaForEnrollment(@Param("enrollmentId") Integer enrollmentId, @Param("effectiveStartDate") Date effectiveStartDate);
}
