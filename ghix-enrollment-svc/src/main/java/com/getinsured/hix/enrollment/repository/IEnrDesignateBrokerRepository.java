package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;

public interface IEnrDesignateBrokerRepository extends JpaRepository<DesignateBroker, Integer> {
	
	/**
	 * Finds {@link DesignateBroker} based on  employer id and  status
	 * 
	 * @param employerId
	 * @param status
	 * @return {@link DesignateBroker}
	 */
	@Query("FROM DesignateBroker d where d.employerId = :employerId AND d.status = :status")
	DesignateBroker findDesigBrokerByEmployerIdAndStatus(@Param("employerId") int employerId, @Param("status") Status status);

}
