package com.getinsured.hix.enrollment.carrierfeed.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jabelardo on 7/15/14.
 */
public interface DataImporter<T> {

    //Parses out one row at a time.
    Iterator<T> importIterator() throws IOException;

    //Parses out all rows
    List<T> importAll() throws IOException;

    List<String> getHeader();
}
