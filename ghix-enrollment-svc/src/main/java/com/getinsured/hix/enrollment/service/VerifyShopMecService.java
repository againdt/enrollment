/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.VerifyNonESIMECRequest;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.VerifyNonESIMECResponse;;

/**
 * @author raja
 * @since 10/05/2013
 * This method prepares the update the input Enrollment object with received Household data.
 * 
 * @param adminUpdateIndividualRequest
 * @param enrollment
 * @return
 * @throws GIException
 */
public interface VerifyShopMecService {
	VerifyNonESIMECResponse verifyShopMec(VerifyNonESIMECRequest verifyNonESIMECRequest,AccountUser user)throws GIException;	
}
