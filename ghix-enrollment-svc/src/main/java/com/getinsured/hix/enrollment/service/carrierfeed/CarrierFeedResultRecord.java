package com.getinsured.hix.enrollment.service.carrierfeed;

import java.util.Date;

import com.getinsured.hix.model.LookupValue;

public class CarrierFeedResultRecord {

	int enrollmentId; //from enrollment table
//	int originalEnrollmentStatusLookupId;
	String carrierStatus;
	GIStatus newEnrollmentStatus;
//	GIStatus originalEnrollmentStatus;

	String originalCarrierStatus;

	//calculated values
	String rateUp;
	Date dateClosed;
	Date effectiveDate;
	String followupMessage;
	LookupValue currentEnrollmentStatusLookupValue;
	Date lastStatusDt;
	
	String applicationId;
	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public int getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(int enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public int getOriginalEnrollmentStatusLookupId() {
		return currentEnrollmentStatusLookupValue.getLookupValueId();
	}

	public LookupValue getCurrentEnrollmentStatusLookupValue() {
		return currentEnrollmentStatusLookupValue;
	}

	public void setCurrentEnrollmentStatusLookupValue(
			LookupValue currentEnrollmentStatusLookupValue) {
		this.currentEnrollmentStatusLookupValue = currentEnrollmentStatusLookupValue;
	}

	public String getCarrierStatus() {
		return carrierStatus;
	}

	public void setCarrierStatus(String carrierStatus) {
		this.carrierStatus = carrierStatus;
	}

	public GIStatus getNewEnrollmentStatus() {
		return newEnrollmentStatus;
	}
	
	public String getNewEnrollmentStatusStr() {
		return newEnrollmentStatus.toString();
	}

	public void setNewEnrollmentStatus(GIStatus newEnrollmentStatus) {
		this.newEnrollmentStatus = newEnrollmentStatus;
	}

	public GIStatus getOriginalEnrollmentStatus() {
		if(currentEnrollmentStatusLookupValue != null){
		 return GIStatus.valueOf(this.currentEnrollmentStatusLookupValue.getLookupValueCode());
		}
		return GIStatus.UNKNOWN;
	 }

	public String getRateUp() {
		return rateUp;
	}

	public void setRateUp(String rateUp) {
		this.rateUp = rateUp;
	}

	public Date getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getFollowupMessage() {
		return followupMessage;
	}

	public void setFollowupMessage(String followupMessage) {
		this.followupMessage = followupMessage;
	}

	public LookupValue getEnrollmentStatusLookupValue() {
		return currentEnrollmentStatusLookupValue;
	}

	public void setEnrollmentStatusLookupValue(
			LookupValue enrollmentStatusLookupValue) {
		this.currentEnrollmentStatusLookupValue = enrollmentStatusLookupValue;
	}
	
	public String getOriginalCarrierStatus() {
		return originalCarrierStatus;
	}

	public void setOriginalCarrierStatus(String originalCarrierStatus) {
		this.originalCarrierStatus = originalCarrierStatus;
	}
	
	public Date getLastStatusDt() {
		return lastStatusDt;
	}

	public void setLastStatusDt(Date lastStatusDt) {
		this.lastStatusDt = lastStatusDt;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append( String.format("APPID:%s,",applicationId));
		sb.append( String.format("CARRIERSTATUS:%s,",carrierStatus));
		if(getOriginalEnrollmentStatus() != null){
		sb.append( String.format("ORIGINAL_ENROLLMENT_STATUS:%s,",getOriginalEnrollmentStatus().toString()));
		}else{
			sb.append( String.format("ORIGINAL_ENROLLMENT_STATUS:%s,",""));
		}
		sb.append( String.format("NEW_ENROLLMENT_STATUS:%s,",newEnrollmentStatus));
		sb.append( String.format("ENROLLMENT_ID:%s,",enrollmentId));
		sb.append( String.format("RATEUP:%s,",rateUp));
		sb.append( String.format("DATECLOSED:%s,",dateClosed));
		sb.append( String.format("EFFECTIVE_DATE:%s,",effectiveDate));
		sb.append( String.format("FOLLOWUP_MESSAGE:%s",followupMessage));
		
		return sb.toString();
		
	}
	
}
