package com.getinsured.hix.enrollment.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.enrollment.service.EnrlAgentBobTransferService;

public class EnrollmenAgentBOBTransferThread implements Callable <Map<String,String>>{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmenAgentBOBTransferThread.class);
	
	List<Integer> enrollmentIdList;
	
	//Map<String, String> enrollmentSkipMap;
	EnrlAgentBobTransferService enrlAgentBobTransferService;
	EnrollmentBrokerUpdateDTO brokerReq;
	
	
	public EnrollmenAgentBOBTransferThread(List<Integer> enrollmentIdList, EnrlAgentBobTransferService enrlAgentBobTransferService, EnrollmentBrokerUpdateDTO brokerReq) {
		super();
		this.enrollmentIdList = enrollmentIdList;
		//this.enrollmentSkipMap = enrollmentSkipMap;
		this.enrlAgentBobTransferService = enrlAgentBobTransferService;
		this.brokerReq=brokerReq;
	}

	@Override
	public Map<String,String> call() throws Exception{
		 Map<String, String> enrollmentSkipMap = new HashMap<String, String>();
		try {
			 enrlAgentBobTransferService.processBOBTransferRequest(enrollmentIdList, enrollmentSkipMap, brokerReq);
		} catch (Exception e) {
			enrollmentSkipMap.put(enrollmentIdList.toString(), EnrollmentUtils.shortenedStackTrace(e, 4));
			LOGGER.error("Exception occurred in EnrollmenAgentBOBTransferThread: ", e);
		}
		return enrollmentSkipMap;
	}

}
