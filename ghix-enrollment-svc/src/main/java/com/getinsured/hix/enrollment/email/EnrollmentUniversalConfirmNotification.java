package com.getinsured.hix.enrollment.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.enrollment.EnrollmentOffExchangeEmailDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
@Component
public class EnrollmentUniversalConfirmNotification extends NotificationAgent {

//	private Map<String, String> singleData;
	private EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO;
	
	private static final String INSURENCE_TYPE = "insuranceType";
	private static final String PLAN_NAME = "planName";
	private static final String CUSTOMER_NAME = "customerName";
	private static final String HOST = "host";
	private static final String NET_PREMIUM_AMT = "netPremiumAmount";
	private static final String LINK = "link";
	private static final String LOGIN_POSTFIX = "account/user/login";
	private static final String INSURANCE_COMPANY = "insuranceCompany";
	private static final String COMPANY_LOGO = "companyLogo";
	private static final String PHONE_NUM = "phoneNum";
	private static final String lOGO_URL = "logoUrl";
	private static final String REDIRECT_URL = "redirectUrl";
	private static final String TO = "To";
	private static final String SUBJECT = "Subject";
	private static final String FROM = "From";
	private static final String DISCLAIMER_CONTENT = "disclaimerContent";
	private static final String CALL_CENTER_HOURS = "callCenterHours";
	
/*	public Map<String, String> getSingleData() {
		Map<String,String> bean = new HashMap<String, String>();

		bean.put(CUSTOMER_NAME, enrollmentOffExchangeEmailDTO.getRecipientName());
		bean.put(HOST, GhixEndPoints.GHIXWEB_SERVICE_URL );
		
		 * Put Plan Details into bean  
		 
		bean.put(NET_PREMIUM_AMT,enrollmentOffExchangeEmailDTO.getNetPremiumAmount());
		bean.put(PLAN_NAME,enrollmentOffExchangeEmailDTO.getPlanName());
		bean.put(INSURENCE_TYPE,enrollmentOffExchangeEmailDTO.getInsuranceType());
		bean.put(LINK,GhixEndPoints.GHIXWEB_SERVICE_URL+LOGIN_POSTFIX);
		bean.put(INSURANCE_COMPANY, enrollmentOffExchangeEmailDTO.getInsuranceCompany());
		bean.put(COMPANY_LOGO, enrollmentOffExchangeEmailDTO.getCompanyLogo());
		bean.put(PHONE_NUM, enrollmentOffExchangeEmailDTO.getPhoneNum());
		bean.put(lOGO_URL, enrollmentOffExchangeEmailDTO.getLogoUrl());
		bean.put(REDIRECT_URL, enrollmentOffExchangeEmailDTO.getRedirectUrl());
		bean.put(DISCLAIMER_CONTENT, enrollmentOffExchangeEmailDTO.getDisclaimerContent());
		bean.put(CALL_CENTER_HOURS, enrollmentOffExchangeEmailDTO.getCallCenterHours());
//		setTokens(bean);

		Map<String, String> data = new HashMap<String, String>();
		data.put(TO, enrollmentOffExchangeEmailDTO.getRecipient());
		data.put(SUBJECT, "Congratulations on the submission of your "+ enrollmentOffExchangeEmailDTO.getInsuranceType() + " insurance application!");
		data.put(FROM, enrollmentOffExchangeEmailDTO.getEmailFrom());
		
		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
	}
	public void setSingleData(Map<String, String> singleData) {
		this.singleData = singleData;
	}*/
	
	public EnrollmentOffExchangeEmailDTO getEnrollmentOffExchangeEmailDTO() {
		return enrollmentOffExchangeEmailDTO;
	}
	public void setEnrollmentOffExchangeEmailDTO(
			EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO) {
		this.enrollmentOffExchangeEmailDTO = enrollmentOffExchangeEmailDTO;
	}
	
	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		Map<String,String> bean = new HashMap<String, String>();
		if(null != enrollmentOffExchangeEmailDTO){
			bean.put(CUSTOMER_NAME, enrollmentOffExchangeEmailDTO.getRecipientName());
			bean.put(HOST, GhixEndPoints.GHIXWEB_SERVICE_URL );
			/*
			 * Put Plan Details into bean  
			 */
			bean.put(NET_PREMIUM_AMT,enrollmentOffExchangeEmailDTO.getNetPremiumAmount());
			bean.put(PLAN_NAME,enrollmentOffExchangeEmailDTO.getPlanName());
			bean.put(INSURENCE_TYPE,enrollmentOffExchangeEmailDTO.getInsuranceType());
			bean.put(LINK,GhixEndPoints.GHIXWEB_SERVICE_URL+LOGIN_POSTFIX);
			bean.put(INSURANCE_COMPANY, enrollmentOffExchangeEmailDTO.getInsuranceCompany());
			bean.put(COMPANY_LOGO, enrollmentOffExchangeEmailDTO.getCompanyLogo());
			bean.put(PHONE_NUM, enrollmentOffExchangeEmailDTO.getPhoneNum());
			bean.put(lOGO_URL, enrollmentOffExchangeEmailDTO.getLogoUrl());
			bean.put(REDIRECT_URL, enrollmentOffExchangeEmailDTO.getRedirectUrl());
			bean.put(DISCLAIMER_CONTENT, enrollmentOffExchangeEmailDTO.getDisclaimerContent());
			bean.put(CALL_CENTER_HOURS, enrollmentOffExchangeEmailDTO.getCallCenterHours());
			bean.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		}
		return bean;
	}
	
	@Override
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		Map<String, String> data = new HashMap<String, String>();
		if(null != enrollmentOffExchangeEmailDTO){
			data.put(TO, enrollmentOffExchangeEmailDTO.getRecipient());
			data.put(SUBJECT, "Congratulations on the submission of your "+ enrollmentOffExchangeEmailDTO.getInsuranceType() + " insurance application!");
			data.put(FROM, enrollmentOffExchangeEmailDTO.getEmailFrom());
		}
		return data;
	}
	
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}
