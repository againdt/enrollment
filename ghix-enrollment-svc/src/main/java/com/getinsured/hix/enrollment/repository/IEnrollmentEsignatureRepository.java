/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.enrollment.EnrollmentEsignature;



/**
 * @author panda_p
 *
 */
@Repository
public interface IEnrollmentEsignatureRepository extends JpaRepository<EnrollmentEsignature, Integer>{
	
/*	@Query("SELECT enrollesign FROM EnrollmentEsignature enrollesign WHERE enrollesign.enrollment.id = :enrollmentId")
    EnrollmentEsignature findByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);*/
}
