/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.dto.enrollment.EnrollmentListWrapperDTO;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;



/**
 * @author rajaramesh_g
 * @since 22/11/2013
 */
public interface EnrollmentOffExchangeService {
	
	Enrollment findEnrollmentById(Integer enrollmentId);
	Enrollment findEnrollmentByTicketId(String ticketId);
	Enrollment updateEcommittedEnrollment(String ticketId, String status, String carrierConfirmationNumber, String applicationId);
	EnrollmentResponse saveEnrollment(EnrollmentListWrapperDTO enrollmentListWrapperDTO);
	Enrollment updateEnrollmentWithTicketId(Enrollment enrollment);
	//	Integer getOrderIdFromIndvOrderItemId(int indOrderItemId);
	List<Enrollment> findEnrollmentBySsapApplicationId(Long ssapApplicationId);
}
