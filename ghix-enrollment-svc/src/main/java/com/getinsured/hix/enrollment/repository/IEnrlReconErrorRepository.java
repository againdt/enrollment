package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.getinsured.hix.model.enrollment.EnrlReconError;

public interface IEnrlReconErrorRepository  extends JpaRepository<EnrlReconError, Integer>{
	
	@Query(" SELECT DISTINCT enrollmentId FROM EnrlReconError "
		 + " WHERE fileId = :fileId ")
	List<Long> getEnrollmentIdByFileId(@Param("fileId") Integer  fileId);
	
	@Query(" SELECT COUNT(DISTINCT enrollmentId) FROM EnrlReconError "
			 + " WHERE fileId = :fileId ")
	Integer getEnrollmentCountByFileId(@Param("fileId") Integer  fileId);
}
