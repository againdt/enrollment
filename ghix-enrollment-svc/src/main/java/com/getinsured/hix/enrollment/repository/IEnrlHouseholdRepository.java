package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.consumer.Household;



public interface IEnrlHouseholdRepository extends JpaRepository<Household, Integer> {
	
	@Query("SELECT id, householdCaseId from Household WHERE householdCaseId IN (:householdCaseIdList)")
	List<Object[]> getCmrHouseholdIdFromHouseholdCaseId(@Param("householdCaseIdList") List<String> householdCaseIdList);
	
	@Query("SELECT householdCaseId from Household WHERE id IN (:householdCaseId)")
	String getIndividualCmrHouseholdIdFromHouseholdCaseId(@Param("householdCaseId") Integer householdCaseId);
	
}
