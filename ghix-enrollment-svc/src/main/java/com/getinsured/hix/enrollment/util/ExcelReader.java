package com.getinsured.hix.enrollment.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ExcelReader {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelReader.class);
	// private String fileName;
	private FileInputStream excelFile = null;
	//private XSSFWorkbook workbook = null;
	HSSFWorkbook workbook =null;//
	//private static final String PERCENT_FORMAT = "0%";
	//private static final String CURRENCY_FORMAT = "\"$\"";
	//private static final int HUNDRED = 100;
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

	public void loadFile(String fileNameGiven) throws IOException {
		// fileName = fileNameGiven;
		excelFile = new FileInputStream(new File(fileNameGiven));
		// Get the workbook instance for XLS file
		//workbook = new XSSFWorkbook(excelFile);
		//workbook = WorkbookFactory.create(excelFile);
		workbook = new HSSFWorkbook(excelFile);
	}
	
	public void loadFile(InputStream fileInputStream) throws IOException {
		//workbook = new XSSFWorkbook(fileInputStream);
	}

	public void closeFile() throws IOException {
		excelFile.close();
	}

	public XSSFSheet getSheet(int tab) throws IOException {
		// Get first sheet from the workbook
		return null;// workbook.getSheetAt(tab);
	}

	public XSSFSheet getSheet(String tabName) throws IOException {
		// Get first sheet from the workbook
		return null;//workbook.getSheetAt(workbook.getSheetIndex(tabName));
	}

	public String getCellValueTrim(Cell cell) {
		
		String returnValue = getCellValue(cell);
		
		if (StringUtils.isNotEmpty(returnValue)) {
			return returnValue.trim();
		}
		return returnValue;
	}

	public String getCellValue(Cell cell) {
		return getCellValue(cell, false); 
	}

	public String getCellValue(Cell cell, boolean ignoreDecimal) {
		
		String returnValue = null;
		
		if (null == cell) {
			return returnValue;
		}
		
		if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
			returnValue = getCellNumericValue(cell, ignoreDecimal);
		}
		else if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
			returnValue = cell.getStringCellValue();
		}
		else if (Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
			returnValue = cell.getStringCellValue();
		}
		return returnValue;
	}
	
	private String getCellNumericValue(Cell cell, boolean ignoreDecimal) {
		
		String returnValue = null;
		
		if (null == cell) {
			return returnValue;
		}

		if (DateUtil.isCellDateFormatted(cell)) {
			Date today = cell.getDateCellValue();
			returnValue = DATE_FORMAT.format(today);
		}
		else {
			
			double val = cell.getNumericCellValue();
			String cellFormat = cell.getCellStyle().getDataFormatString();
			
			/*if(cellFormat.equals(PERCENT_FORMAT)) {
				val = val * HUNDRED;
				
				if(ignoreDecimal) {
					returnValue = getWholeNumberValue(val) + PERCENTAGE;
				}
				else {
					returnValue = val + PERCENTAGE;
				}
			}
			else if (cellFormat.startsWith(CURRENCY_FORMAT)) {
			
				if(ignoreDecimal) {
					returnValue = DOLLARSIGN + getWholeNumberValue(val);
				}
				else {
					returnValue = DOLLARSIGN + val;
				}
			}
			else {*/
				
			if(ignoreDecimal) {
				returnValue = StringUtils.EMPTY + getWholeNumberValue(val);
			}
			else {
				returnValue = StringUtils.EMPTY + val;
			}
			//}
		}
		return returnValue;
	}
	
	private String getWholeNumberValue(double value) {
		return Integer.toString(Double.valueOf(value).intValue());
	}
	public static void main(String args[])
	{
		ExcelReader r = new ExcelReader();
		r.getDataFromFile("", "");
	}
	public  List<ExcelModel>  getDataFromFile(String path,String fileName)
	{
		List<ExcelModel> parsedData = null;
		try {
			//loadFile("C:\\aetnafeed\\PFG_StatusFeed_ADA_06202014.XLS");
			//loadFile("D:\\hsData\\Book1.xls");
			loadFile(fileName);
			HSSFSheet sheet = workbook.getSheetAt(0);
			parsedData = readRowwiseData(sheet, null);
			int i=0;
			for(ExcelModel model : parsedData)
			{
				System.out.println("Row " +i + " Data : "+model);
				i++;
				// Retrieve Enrollment record based on Application Id - Priya/Ravi
				// Apply Business Rules - Atul/Ravi
				// Update enrollment record - Ravi/Priya
				// Insert status report in feed_summary table - Ravi/Atul
			}
			
		} catch (IOException e) {
			LOGGER.debug("IOException in getDataFromFile()", e);
		}
		return parsedData;
	}
	private List<ExcelModel> readRowwiseData(HSSFSheet  sheet, ExcelModel model) {
		
		//LOGGER.debug("readAndPersistSTMData() Start");

		List<ExcelModel> stmList = new ArrayList<ExcelModel>();
		try {
			
			if (null == sheet) {
				//LOGGER.info("XSSFSheet is null.");
				return null;
			}
			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();
			boolean isFirstRow = true;
			
			while (rowIterator.hasNext()) {
				
				Row excelRow = rowIterator.next();
				
				if (isFirstRow) {
					isFirstRow = false;
					//stmList.add(getSTMExcelVOHeader(excelRow));
					continue;
				}
				stmList.add(readRowData(excelRow));
			}
			// persist STM plans from STMExcelVO object to database
			//status = stmPlanMgmtSerffService.populateAndPersistSTMPlans(stmList, trackingRecord);
			
			/*if (status && StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
				//trackingRecord.setPmResponseXml(MSG_STM_PLANS);
			}*/
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			//LOGGER.debug("readAndPersistSTMData() End");
		}
		return stmList;
	}
private String trimIfPossible(String str)
{
	if(!StringUtils.isEmpty(str))
	{
		return str.trim();
	}
	return str;
}
public static Date StringToDate(String dateStr, String format) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
    try {
		return dateFormat.parse(dateStr);
	} catch (ParseException e) {
		//LOGGER.error("ParseException",e);
	}
    catch(Exception ee)
    {
    	LOGGER.error("Exception in StringToDate() ", ee);
    }
    return null;
}
private ExcelModel readRowData(Row excelRow) {
	ExcelModel stmExcelVO = null;
	try{		
	
	if (null != excelRow) {//excelRow.getCell(0).getStringCellValue()
		stmExcelVO = new ExcelModel();
	if(excelRow.getCell(0) != null){
		stmExcelVO.setSubmissionDate(StringToDate(trimIfPossible(excelRow.getCell(0).getStringCellValue()), "MM/dd/yyyy"));
	}	
	if(excelRow.getCell(1) != null){
 	 stmExcelVO.setApplicationID(trimIfPossible(excelRow.getCell(1).getStringCellValue()));
	}
	if(excelRow.getCell(2) != null){
	 stmExcelVO.setApplicantID(trimIfPossible(excelRow.getCell(2).getStringCellValue()));
	}
	if(excelRow.getCell(3) != null){
	 stmExcelVO.setLegacyID(trimIfPossible(excelRow.getCell(3).getStringCellValue()));
	}
	//DO NOT suck in SSN - PII
	//stmExcelVO.setSSN(trimIfPossible(excelRow.getCell(4).getStringCellValue()));
	if(excelRow.getCell(5) != null){
	stmExcelVO.setLastName(trimIfPossible(excelRow.getCell(5).getStringCellValue()));
	}
	if(excelRow.getCell(6) != null){
	stmExcelVO.setFirstName(trimIfPossible(excelRow.getCell(6).getStringCellValue()));
	}
	if(excelRow.getCell(7) != null){
	 stmExcelVO.setMI(trimIfPossible(excelRow.getCell(7).getStringCellValue()));
	}
	//DO NOT suck in DOB - PII
	if(excelRow.getCell(8) != null){
	stmExcelVO.setDOB(StringToDate(trimIfPossible(excelRow.getCell(8).getStringCellValue()), "MM/dd/yyyy"));
	}
	if(excelRow.getCell(9) != null){
	stmExcelVO.setApplicationStatusDesc(trimIfPossible(excelRow.getCell(9).getStringCellValue()));
	}
	if(excelRow.getCell(10) != null){
 	stmExcelVO.setRatingLevelDesc(trimIfPossible(excelRow.getCell(10).getStringCellValue()));
	}
	if(excelRow.getCell(11) != null){
	 stmExcelVO.setApplicantBaseRate(trimIfPossible(excelRow.getCell(11).getStringCellValue()));
	}
	if(excelRow.getCell(12) != null){
	stmExcelVO.setApplicantRateTotal(trimIfPossible(excelRow.getCell(12).getStringCellValue()));
	}
	if(excelRow.getCell(13) != null){
	stmExcelVO.setApplicationBaseRate(excelRow.getCell(13).getNumericCellValue()+"");
	}
	if(excelRow.getCell(14) != null){
	stmExcelVO.setApplicationRateTotal(excelRow.getCell(14).getNumericCellValue()+"");
	}
	if(excelRow.getCell(15) != null){
	stmExcelVO.setLastStatusDt(StringToDate(trimIfPossible(excelRow.getCell(15).getStringCellValue()), "MM/dd/yyyy"));
	}
	if(excelRow.getCell(16) != null){
	stmExcelVO.setReqEffectiveDate(StringToDate(trimIfPossible(excelRow.getCell(16).getStringCellValue()), "MM/dd/yyyy"));
	}
	if(excelRow.getCell(17) != null){
	 stmExcelVO.setActualEffDate(StringToDate(trimIfPossible(excelRow.getCell(17).getStringCellValue()), "MM/dd/yyyy"));
	}
	if(excelRow.getCell(18) != null){
	 stmExcelVO.setParentPlanName(trimIfPossible(excelRow.getCell(18).getStringCellValue()));
	}
	if(excelRow.getCell(19) != null){
	stmExcelVO.setPPID(excelRow.getCell(19).getNumericCellValue()+"");
	}
	if(excelRow.getCell(20) != null){
 	stmExcelVO.setDental(trimIfPossible(excelRow.getCell(20).getStringCellValue()));
	}
	if(excelRow.getCell(21) != null){
	stmExcelVO.setuWComments(trimIfPossible(excelRow.getCell(21).getStringCellValue()));
	}
	//stmExcelVO.setLastStatusDate(excelRow.getCell(22).getDateCellValue());
	if(excelRow.getCell(23) != null){
	stmExcelVO.setnPN(trimIfPossible(excelRow.getCell(23).getStringCellValue()));
	}
	 //DO NOT SUCK IN - PII
	//stmExcelVO.setTaxID(excelRow.getCell(24).getStringCellValue());
	if(excelRow.getCell(25) != null){
	stmExcelVO.setSellingAgent(trimIfPossible(excelRow.getCell(25).getStringCellValue()));
	}
	if(excelRow.getCell(26) != null){
	stmExcelVO.setEmail(trimIfPossible(excelRow.getCell(26).getStringCellValue()));
	}
	if(excelRow.getCell(27) != null){
	stmExcelVO.setAddress1(trimIfPossible(excelRow.getCell(27).getStringCellValue()));
	}
	if(excelRow.getCell(28) != null){
	stmExcelVO.setAddress2(trimIfPossible(excelRow.getCell(28).getStringCellValue()));
	}
	if(excelRow.getCell(29) != null){
	stmExcelVO.setCity(trimIfPossible(excelRow.getCell(29).getStringCellValue()));
	}
	if(excelRow.getCell(30) != null){
	stmExcelVO.setState(trimIfPossible(excelRow.getCell(30).getStringCellValue()));
	}
	if(excelRow.getCell(31) != null){
	stmExcelVO.setZip(trimIfPossible(excelRow.getCell(31).getStringCellValue()));
	 }
	 }
	}
	catch(Exception ex){
		LOGGER.error(ex.getMessage(), ex);
	}
	return stmExcelVO;
}
}
