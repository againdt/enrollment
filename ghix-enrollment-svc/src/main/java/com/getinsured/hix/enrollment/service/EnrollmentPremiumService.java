/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.enrollment.EnrollmentResponse;

/**
 * Service class for the Enrollment Premium table
 * @author negi_s
 * @since 06/20/2016
 */
public interface EnrollmentPremiumService {
	
	/**
	 * Find enrollment premiums by enrollment Id
	 * @param enrollmentId
	 * @return EnrollmentResponse
	 */
	EnrollmentResponse findPremiumDetailsByEnrollmentId(Integer enrollmentId);
	
	/**
	 * Find enrollment premiums by enrollment Id month and year
	 * @param enrollmentId, month , year
	 * @return EnrollmentResponse
	 */
	EnrollmentResponse findPremiumDetailsByEnrollmentIdMonthAndYear(Integer enrollmentId, Integer month, Integer year);
}
