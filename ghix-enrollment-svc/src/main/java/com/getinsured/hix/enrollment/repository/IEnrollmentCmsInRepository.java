package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.getinsured.hix.model.enrollment.EnrollmentCmsIn;

public interface IEnrollmentCmsInRepository extends JpaRepository<EnrollmentCmsIn, Integer>{

}
