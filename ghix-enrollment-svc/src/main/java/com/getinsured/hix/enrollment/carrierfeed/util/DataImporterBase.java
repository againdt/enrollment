package com.getinsured.hix.enrollment.carrierfeed.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by jabelardo on 7/10/14.
 */
public abstract class DataImporterBase<T> implements DataImporter<T> {

    private FileReader fileReader;

    private Map<FileType, FileReader<T>> fileReaderCreators = new HashMap<>();

    public DataImporterBase(String dataFilename, FileType fileType, String mappingsFilename) throws IOException {
        registerFileReaderCreators();
        initializeFileReader(dataFilename, fileType, mappingsFilename);
    }

    protected void register(FileType fileType, FileReader<T> fileReader) {
        fileReaderCreators.put(fileType, fileReader);
    }

    protected abstract void registerFileReaderCreators();

    protected abstract Class getModelClass();

    private void initializeFileReader(String dataFilename, FileType fileType, String mappingsFilename) throws IOException {
        fileReader = fileReaderCreators.get(fileType);
        if (fileReader == null) {
            throw new RuntimeException(String.format("%s fileReader for fileType %s is not registered",
                    this.getClass().getCanonicalName(), fileType));
        }
        fileReader.initialize(dataFilename, mappingsFilename);
    }

    @Override
    public List<String> getHeader() {
        return fileReader.getHeader();
    }

    @Override
    public Iterator<T> importIterator() throws IOException {
        return fileReader.importIterator(getModelClass());
    }

    @Override
    public List<T> importAll() throws IOException {
        return fileReader.importAll(getModelClass());
    }
}
