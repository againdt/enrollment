package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.LocationAud;

public interface IEnrollmentLocationPrivateRepository extends JpaRepository<LocationAud, Integer>{
	@Query("FROM LocationAud as ln WHERE ln.id = :locationId and ln.rev = :rev")
	LocationAud getLocationAudByIdAndRev(
			@Param("locationId") Integer locationId,
			@Param("rev") Integer rev);
}