package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync.RecordType;

public interface EnrollmentAhbxSyncService {
	
	void saveEnrollmentAhbxSync(List<Enrollment> enrollmentList, String houseHoldCaseID, RecordType recordType);

}
