/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.getinsured.hix.model.enrollment.EnrolleeRaceAud;

public interface IEnrolleeRaceAudPrivateRepository extends JpaRepository<EnrolleeRaceAud, Integer> {
	
	@Query(" FROM EnrolleeRaceAud as en " +
		   " WHERE en.id = :enrolleeRaceAudID and en.rev = :rev")
	EnrolleeRaceAud getEnrolleeRaceAudByIdAndRev(@Param("enrolleeRaceAudID") Integer enrolleeRaceAudID,
			@Param("rev") int rev);
	
}
