package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.ExternalEnrollmentSummary;


public interface IExternalEnrollmentSummaryRepository extends JpaRepository<ExternalEnrollmentSummary, Integer> {
	
	@Transactional
	@Modifying
	@Query("UPDATE ExternalEnrollmentSummary SET status = 'PURGED'")
	void updateStatusToPurged();
}
