package com.getinsured.hix.enrollment.util;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.platform.util.JacksonUtils;

public class ExcelModel {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelModel.class);

	private Date submissionDate;
	private String applicationID;
	private String applicantID;
	private String legacyID;
	private String SSN;
	private String lastName;
	private String firstName;
	private String MI;
	private Date DOB;
	private String applicationStatusDesc;
	private String ratingLevelDesc;
	private String applicantBaseRate;
	private String applicantRateTotal;
	private String applicationBaseRate;
	private String applicationRateTotal;
	private Date lastStatusDt;
	private Date reqEffectiveDate;
	private Date actualEffDate;
	private String parentPlanName;
	private String PPID;
	private String dental;
	private String uWComments;
	private Date lastStatusDate;
	private String nPN;
	private String taxID;
	private String sellingAgent;
	private String email;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	
	public String toJson() {
		String aetnaAppJson = "";
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(ExcelModel.class);
			aetnaAppJson = writer.writeValueAsString(this);
		} catch (Exception excp) {
			LOGGER.error("Exception in converting toJSON" , excp);
			aetnaAppJson = excp.getMessage();
		}
		return aetnaAppJson;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}
	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}
	public String getApplicationID() {
		return applicationID;
	}
	public void setApplicationID(String applicationID) {
		if(applicationID!=null)
		{
			this.applicationID = applicationID.trim();
		}
	}
	public String getApplicantID() {
		return applicantID;
	}
	public void setApplicantID(String applicantID) {
		this.applicantID = applicantID;
	}
	public String getLegacyID() {
		return legacyID;
	}
	public void setLegacyID(String legacyID) {
		this.legacyID = legacyID;
	}
	public String getSSN() {
		return SSN;
	}
	public void setSSN(String sSN) {
		SSN = sSN;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMI() {
		return MI;
	}
	public void setMI(String mI) {
		MI = mI;
	}
	public Date getDOB() {
		return DOB;
	}
	public void setDOB(Date dOB) {
		DOB = dOB;
	}
	public String getApplicationStatusDesc() {
		return applicationStatusDesc;
	}

	public void setApplicationStatusDesc(String applicationStatusDesc) {
		// mappings on our side are lower case. Hence lowering the case
		if (applicationStatusDesc != null && !applicationStatusDesc.isEmpty()) {
			this.applicationStatusDesc = applicationStatusDesc.toLowerCase();
		}
	}
	public String getRatingLevelDesc() {
		return ratingLevelDesc;
	}
	public void setRatingLevelDesc(String ratingLevelDesc) {
		this.ratingLevelDesc = ratingLevelDesc;
	}
	public String getApplicantBaseRate() {
		return applicantBaseRate;
	}
	public void setApplicantBaseRate(String applicantBaseRate) {
		this.applicantBaseRate = applicantBaseRate;
	}
	public String getApplicantRateTotal() {
		return applicantRateTotal;
	}
	public void setApplicantRateTotal(String applicantRateTotal) {
		this.applicantRateTotal = applicantRateTotal;
	}
	public String getApplicationBaseRate() {
		return applicationBaseRate;
	}
	public void setApplicationBaseRate(String applicationBaseRate) {
		this.applicationBaseRate = applicationBaseRate;
	}
	public String getApplicationRateTotal() {
		return applicationRateTotal;
	}
	public void setApplicationRateTotal(String applicationRateTotal) {
		this.applicationRateTotal = applicationRateTotal;
	}
	public Date getLastStatusDt() {
		return lastStatusDt;
	}
	public void setLastStatusDt(Date lastStatusDt) {
		
		this.lastStatusDt = lastStatusDt;
		if(this.lastStatusDt == null)
		{
			this.lastStatusDt = new TSDate();
		}
	}
	public Date getReqEffectiveDate() {
		return reqEffectiveDate;
	}
	public void setReqEffectiveDate(Date reqEffectiveDate) {
		this.reqEffectiveDate = reqEffectiveDate;
	}
	public Date getActualEffDate() {
		return actualEffDate;
	}
	public void setActualEffDate(Date actualEffDate) {
		this.actualEffDate = actualEffDate;
	}
	public String getParentPlanName() {
		return parentPlanName;
	}
	public void setParentPlanName(String parentPlanName) {
		this.parentPlanName = parentPlanName;
	}
	public String getPPID() {
		return PPID;
	}
	public void setPPID(String pPID) {
		PPID = pPID;
	}
	public String getDental() {
		return dental;
	}
	public void setDental(String dental) {
		this.dental = dental;
	}
	public String getuWComments() {
		return uWComments;
	}
	public void setuWComments(String uWComments) {
		this.uWComments = uWComments;
	}
	//not being used
	public Date getLastStatusDate() {
		return lastStatusDate;
	}
	public void setLastStatusDate(Date lastStatusDate) {
		this.lastStatusDate = lastStatusDate;
	}
	public String getnPN() {
		return nPN;
	}
	public void setnPN(String nPN) {
		this.nPN = nPN;
	}
	public String getTaxID() {
		return taxID;
	}
	public void setTaxID(String taxID) {
		this.taxID = taxID;
	}
	public String getSellingAgent() {
		return sellingAgent;
	}
	public void setSellingAgent(String sellingAgent) {
		this.sellingAgent = sellingAgent;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	

	@Override
	public String toString() {
		return "ExcelModel [submissionDate=" + submissionDate
				+ ", applicationID=" + applicationID + ", applicantID="
				+ applicantID + ", legacyID=" + legacyID + ", SSN=" + SSN
				+ ", lastName=" + lastName + ", firstName=" + firstName
				+ ", MI=" + MI + ", DOB=" + DOB + ", applicationStatusDesc="
				+ applicationStatusDesc + ", ratingLevelDesc="
				+ ratingLevelDesc + ", applicantBaseRate=" + applicantBaseRate
				+ ", applicantRateTotal=" + applicantRateTotal
				+ ", applicationBaseRate=" + applicationBaseRate
				+ ", applicationRateTotal=" + applicationRateTotal
				+ ", lastStatusDt=" + lastStatusDt + ", reqEffectiveDate="
				+ reqEffectiveDate + ", actualEffDate=" + actualEffDate
				+ ", parentPlanName=" + parentPlanName + ", PPID=" + PPID
				+ ", dental=" + dental + ", uWComments=" + uWComments
				+ ", lastStatusDate=" + lastStatusDate + ", nPN=" + nPN
				+ ", taxID=" + taxID + ", sellingAgent=" + sellingAgent
				+ ", email=" + email + ", address1=" + address1 + ", address2="
				+ address2 + ", city=" + city + ", state=" + state + ", zip="
				+ zip + "]";
	}
}
