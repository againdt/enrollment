package com.getinsured.hix.enrollment.service;

import java.util.Date;

import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.exception.GIException;

public interface EnrollmentRequotingService {
	
	void populateMonthlyPremiumsForNewEnrollment(Enrollment enrollment) throws GIException;
	void updateMonthlyPremiumForEnrollment(Enrollment enrollment, boolean updateLastSliceDate, boolean updateAPTC, String fillOnlySLCSP, boolean updateEffectiveDates) throws GIException;
	void updateMonthlyPremiumsForAPTCSliderChange(Enrollment enrollment) throws GIException;
	void updateMonthlyPremiumForCancelTerm(Enrollment enrollment, Date retroDate) throws GIException;
	void updatePremiumAfterMonthlyAPTCChange(Enrollment enrollment, boolean isAdd, boolean isMonthlyAptcChange, boolean isMonthlyStateSubsidyChange) throws GIException;
	
}
