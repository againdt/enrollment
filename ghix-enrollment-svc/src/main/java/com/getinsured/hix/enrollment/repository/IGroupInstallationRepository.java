package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.GroupInstallation;
import com.getinsured.hix.model.enrollment.GroupPlan;

public interface IGroupInstallationRepository extends JpaRepository<GroupInstallation, Integer> {
	@Query(" FROM GroupInstallation as grinstallation " +
		   " where grinstallation.enrollmentActionLkp.lookupValueCode <> 'Deprecated'")
	List<GroupInstallation> findAllGroupInstallation();

	@Query(" FROM GroupInstallation as grinstallation " +
		   " WHERE grinstallation.employerId = :emp_id " +
		   " AND grinstallation.issuerId = :issuer_id " +
		   " AND grinstallation.employerEnrollmentId = :employer_enrollment_id " +
		   " AND grinstallation.enrollmentActionLkp.lookupValueCode NOT IN ('Terminate','Deprecated')")
	GroupInstallation getGroupInstallationByEmployerId(@Param("emp_id")Integer emp_id, @Param("issuer_id")Integer issuer_id, @Param("employer_enrollment_id")Integer employer_enrollment_id);
	
	@Query(" FROM GroupPlan as gp  " +
		   " WHERE gp.groupInstallation.id=:id AND gp.planId =:planId")
	GroupPlan getGroupInstallationByGroupAndPlan(@Param("id")Integer id, @Param("planId")Integer planId);
	
	@Query(" FROM GroupInstallation as grinstallation " +
		   " WHERE grinstallation.employerId = :emp_id " +
		   " AND grinstallation.employerEnrollmentId = :employer_enrollment_Id " +
		   " AND grinstallation.enrollmentActionLkp.lookupValueCode <> 'Deprecated'")
	List<GroupInstallation> getGroupInstallationByEmployerId(@Param("emp_id")Integer emp_id,@Param("employer_enrollment_Id")int employer_enrollment_Id);
	
	@Query(" FROM GroupInstallation as grinstallation " +
		   " WHERE grinstallation.employerEnrollmentId = :employer_enrollment_Id " +
		   " AND grinstallation.issuerId= :issuer_Id " +
		   " AND grinstallation.enrollmentActionLkp.lookupValueCode <> 'Deprecated'")
	GroupInstallation getGroupInstallationByEmployerIdAndIssuerId(@Param("employer_enrollment_Id")int employer_enrollment_Id,@Param("issuer_Id")int issuer_Id);
	
	@Query(" FROM GroupInstallation " +
		   " WHERE id =(SELECT MAX(gi.id) from GroupInstallation as gi " +
		               " WHERE gi.issuerId= :issuer_Id  AND gi.employerEnrollmentId = :employer_enrollment_Id " +
		               " AND gi.enrollmentActionLkp.lookupValueCode='Deprecated' AND gi.groupData IS NOT NULL )")
	GroupInstallation getLastTerminatedByEmployerIdAndIssuerId(@Param("employer_enrollment_Id")int employer_enrollment_Id, @Param("issuer_Id")int issuer_Id);
	
	@Query(" FROM GroupInstallation as grinstallation " +
		   " WHERE grinstallation.issuerId= :issuer_Id " +
		   " AND grinstallation.enrollmentActionLkp.lookupValueCode <> 'Deprecated' " +
		   " AND grinstallation.groupData IS NOT NULL")
	List<GroupInstallation> getGroupInstallationByIssuerId(@Param("issuer_Id")int issuer_Id);
	
	@Query(" FROM GroupInstallation as grinstallation " +
			   " WHERE grinstallation.employerEnrollmentId = :employer_enrollment_Id " +
			   " AND grinstallation.enrollmentActionLkp.lookupValueCode <> 'Deprecated'")
	List<GroupInstallation> getGroupByEmpEnrlId(@Param("employer_enrollment_Id")Integer employer_enrollment_Id);
	
	@Query(" FROM GroupInstallation as grinstallation " +
			   " WHERE grinstallation.employerEnrollmentId = :employer_enrollment_Id " +
			   " AND grinstallation.brokerId = :broker_Id "+
			   " AND grinstallation.enrollmentActionLkp.lookupValueCode <> 'Deprecated'")
	List<GroupInstallation> getGroupByEmpEnrlIdAndBrokerId(@Param("employer_enrollment_Id")Integer employer_enrollment_Id, @Param("broker_Id")Integer broker_Id);
}
