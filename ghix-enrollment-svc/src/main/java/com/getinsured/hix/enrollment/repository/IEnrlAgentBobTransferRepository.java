/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.getinsured.hix.model.enrollment.EnrlAgentBOBTransfer;
import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepository;
import com.getinsured.hix.platform.repository.TenantAwareRepository;
/**
 * @author panda_p
 *
 */

public interface IEnrlAgentBobTransferRepository extends TenantAwareRepository<EnrlAgentBOBTransfer , Integer>,  EnversRevisionRepository<EnrlAgentBOBTransfer, Integer, Integer> {
	@Query("FROM EnrlAgentBOBTransfer as ebt " +
			   " WHERE ebt.status='SUCCESS' and (ebt.batchProcessedStatus is null or ebt.batchProcessedStatus='REPROCESS' ) ORDER BY ebt.creationTimeStamp ASC ")
	List<EnrlAgentBOBTransfer> findAgentBOBTransferRequest();
}
