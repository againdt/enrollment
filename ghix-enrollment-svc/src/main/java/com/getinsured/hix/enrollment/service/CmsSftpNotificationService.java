/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public interface CmsSftpNotificationService {
	
	void sendYhiAlertEmail(Map<String, String> requestMap) throws GIException;
}
