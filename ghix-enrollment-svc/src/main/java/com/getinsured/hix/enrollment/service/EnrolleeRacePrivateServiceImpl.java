/**
 * 
 */
package com.getinsured.hix.enrollment.service;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrolleeRacePrivateRepository;
import com.getinsured.hix.model.enrollment.Enrollee;


/**
 * @author panda_p
 *
 */
@Service("enrolleeRacePrivateService")
@Transactional
public class EnrolleeRacePrivateServiceImpl implements EnrolleeRacePrivateService {

	@Autowired private IEnrolleeRacePrivateRepository enrolleeRaceRepository;
	
	@Override
	public void deleteEnrolleeRace(Enrollee enrollee){
		enrolleeRaceRepository.deleteByEnrollee(enrollee);
	}
	
	
}
