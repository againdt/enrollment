/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentOut1095;

/**
 * @author negi_s
 *
 */
public interface IEnrollmentOut1095Repository extends JpaRepository<EnrollmentOut1095, Integer> {
	
	@Query("FROM EnrollmentOut1095 as en WHERE en.documentFileName = :documentFileName")
	EnrollmentOut1095 findByDocumentFileName(@Param("documentFileName")String fileName);
	
	@Query("FROM EnrollmentOut1095 as en WHERE en.documentFileName = :documentFileName AND en.batchId = :batchId ")
	EnrollmentOut1095 findByDocumentFileNameAndBatchId(@Param("documentFileName")String fileName , @Param("batchId")String batchId);
	
	@Query(" SELECT en.enrollmentIds " +
		   " FROM EnrollmentOut1095 as en " +
		   " WHERE en.documentFileName like '%' || :documentFileName || '%' " +
		   " AND en.batchId = :batchId ")
	String getEnrollmentIds(@Param("batchId")String batchId, @Param("documentFileName")String fileName);
	@Query(" SELECT en.enrollmentIds " +
			   " FROM EnrollmentOut1095 as en " +
			   " WHERE en.batchId = :batchId ")
	List<String> getEnrollmentIdsByBatchId(@Param("batchId")String batchId);
	
	@Query("SELECT en.year FROM EnrollmentOut1095 en WHERE en.batchId = :batchId")
	String getCoverageYearFromBatchId(@Param("batchId") String batchId);
}
