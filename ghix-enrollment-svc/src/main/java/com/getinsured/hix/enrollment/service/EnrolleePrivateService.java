/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.SendUpdatedEnrolleeResponseDTO;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author panda_p
 *
 */
public interface EnrolleePrivateService {
	/**
	 * This methode return list of enrollees which are updated since last carrier update.
	 * 
	 * @return Enrollee List
	 */
	List<Enrollee> getCarrierUpdatedEnrollments();
	
	
	
	/**
	 * Return Enrollee for provided id
	 * @param id
	 * @return Enrollee
	 */
	Enrollee findById(Integer id);
	
	/**
	 * @author panda_p
	 * get Subscriber And Enrollees by Enrollment Id
	 * @param enrollmentID
	 * @return
	 */
	public List<Enrollee> getSubscriberAndEnrollees(Integer enrollmentID);
	/**
	 * 
	 * @param searchCriteria
	 * @return
	 */
	Map<String, Object> searchEnrollee(Map<String, Object> searchCriteria);
	
	/**
	 * 
	 * @param inputVal
	 */
	void saveCarrierUpdatedDataResponse(List<SendUpdatedEnrolleeResponseDTO> inputVal);
	
	/**
	 * This method is used to find Enrollee by passing parameters memberID, enrollmentID, planID
	 * 
	 * @author parhi_s
	 * @param enrollmentID
	 * @param planID
	 * @param memberID
	 * @return List of Enrollee
	 * @throws Exception
	 */
	List<Enrollee> findEnrolleeByEnrollmentIDAndMemberIDAndPlanID(Integer enrollmentID, Integer planID, String memberID) throws GIException;
	
	/**
	 * This method is used to Update Enrollee
	 * 
	 * @author parhi_s
	 * @param enrollee
	 * @throws Exception
	 */
	void updateEnrollee(Enrollee enrollee) throws GIException ;
	
	/**
	 * This method is used to find Enrollee By passing parameters memberID and enrollmentID
	 * 
	 * @author parhi_s
	 * @param memberID
	 * @param enrollmentID
	 * @return
	 * @throws Exception
	 */
	Enrollee findEnrolleeByEnrollmentIDAndMemberID(String memberID, Integer enrollmentID) throws GIException;
	
	/**
	 * 
	 * @author panda_p
	 * @param exchgIndivIdentifier
	 * @return
	 */
	Enrollee getEnrolleeByExchgIndivIdentifier(String exchgIndivIdentifier);
	
	/**
	 * @author parhi_s
	 * @param enrollmentID
	 * @return
	 * @throws GIException
	 */
	Enrollee findSubscriberbyEnrollmentIDId(Integer enrollmentID) ;
	Integer findSubscriberIdByEnrollmentId(Integer enrollmentID);
	
	List<Enrollee> getEnrolleeByStatusAndEnrollmentID(Integer enrollmentID, String status) throws GIException;
	List<Enrollee> getEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException;
	Enrollee getHouseholdContactByEnrollmentID(Integer enrollmentID)throws GIException;
	Enrollee getResponsiblePersonByEnrollmentID(Integer enrollmentID)throws GIException;
	List<Enrollee> findEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException;
	
	/**The getEnrollmentEmployeeIdAndEnrollmentStatus() takes the input parameters as 
	 * employeeId, enrollment status and return results as a list of enrollment objects  
	 * @author raja
	 * @since 08/30/2013
	 * @param employeeId, enrollmentStatus 
	 * @return List<Enrollment>
	 * @throws GIException
	 */
	List<Enrollment> getEnrollmentEmployeeIdAndEnrollmentStatus(Integer employeeId, List<String> enrollmentStatus)throws GIException;
	
	/**
	 * Returns List of Enrolled (Not Canceled or Terminated) Enrollees for given Enrollment ID
	 * @param enrollmentID
	 * @return
	 * @throws GIException
	 */
	List<Enrollee> getEnrolledEnrolleesForEnrollmentID(Integer enrollmentID)throws GIException;
	
	/**
	 * Returns the active Enrollment for employeeID and memberID
	 * @param employeeId
	 * @param memberID
	 * @return
	 */
	List<Enrollment> findActiveEnrollmentByEmployeeIDandMemberID(Integer employeeId, String memberID) throws GIException;
	
	/**
	 * @author panda_p
	 * @since	10/31/2013
	 * 
	 * 
	 * @param enrolleeId
	 * @param subscriberId
	 * @return
	 */
	EnrolleeRelationship getRelationshipBySourceEndTargetId(Integer enrolleeId,Integer subscriberId);
	
	/** The below methods checks the enrollee is HouseHoldContact or CustodialParent or ResponsiblePerson and also none of the those enrollee. 
	 * @author rajaramesh_g
	 * @param enrolleeObj
	 * @return Boolean
	 */
	Boolean isEnrolleeNotHouseHoldAndCustodialAndResponsible(Enrollee enrolleeObj);
	Boolean isEnrolleeHouseHoldContact(Enrollee enrolleeObj);
	Boolean isEnrolleeCustodialParent(Enrollee enrolleeObj);
	Boolean isEnrolleeResponsiblePerson(Enrollee enrolleeObj);

    Enrollee findEnrolleeByEnrollmentIdAndPersonTypeLkp_LookupValueCode(Integer enrollmentId, String lookupValueCode);

}
