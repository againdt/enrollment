package com.getinsured.hix.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.hix.dto.enrollment.ExternalEnrollmentDTO;
import com.getinsured.hix.model.enrollment.ExternalEnrollment;
import com.getinsured.hix.platform.util.exception.GIException;


public interface ExternalEnrollmentService {
	 
	List<ExternalEnrollmentDTO>  getExternalEnrollmentDataByApplicantExternalId(String applicantExternalId ) throws GIException;

	Map<String, List<EnrollmentWithMemberDataDTO>> getEnrollmentDataForMembers(List<String> externalMemberIdList) throws GIException;
	ExternalEnrollment saveExternalEnrollment(ExternalEnrollment enrollment);
	
}
