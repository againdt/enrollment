/**
 * 
 */

package com.getinsured.hix.enrollment.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author parhi_s
 * @since 18/06/2013
 */
public interface EnrollmentBatchService {
	
	/**
	 * @param extractMonth
	 * @param enrollmentType
	 * @throws GIException
	 * 
	 */
	void serveEnrollmentReconciliationJob(String extractMonth, String enrollmentType,String hiosIssuerID) throws GIException;
	
	
	void handleIRSInboundShopResponses () throws GIException;
    void generateFilePayloadResubmitTransmissionXML(int month,int year,String batchCategoryCode,String batchID) throws GIException;
	/**
	 * Change status of Enrollment from given Employer to Cancelled
	 * 
	 * @author meher_a
	 * @param employerId
	 */
	void updateShopEnrollemntToCancelled(int employerId);
	
	
	/**
	 * 
	 * @param jobName
	 * @return
	 * @throws GIException
	 */
	Date getJOBLastRunDate(String jobName) throws GIException;
	
	/**
	 * This method is used to get specific type file names in a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
	List<String> getFileNamesInAFolder(String folderName, String fileType) throws GIException;
	
	/**
	 * Used to clear CARRIER_SEND_FLAG to null, for provided status (HOLD, RESEND).
	 * Sets CARRIER_SEND_FLAG to null for provided date range or else all enrollments
	 * @param carrierFlagStatus
	 * @param startDateString
	 * @param endDateString
	 */
	void clearCarrierSendFlag(String carrierFlagStatus, String startDateString, String endDateString);

	boolean updateEnrollmentByXML2(Enrollment enrollment, String fileName, String timeStamp, Long jobExecutionId) throws GIException;
	
	List<String> serveEnrollmentRecordJob(List<Enrollment> enrollmentList, int issuerID, String enrollmentType, Date startDate, Date endDate, String carrierResendFlag,long jobExecutionId, String jobName,  List<String> outputPathList, int issuerSubList) throws GIException;
	
	/**
	 * @author Aditya_S
	 * @since 05/02/2015 (DD/MM/YYYY)
	 * 
	 * @param timeStamp
	 */
	void serveEnrollmentReonReportJob(String timeStamp) throws GIException;
	
	/**
	 * 
	 * @param dateYear
	 */
	void processAutoTermEnrollment(Integer dateYear);
	
	/**
	 * 
	 * @param errorMap
	 */
	void updateEnrolleeUpdateSendStatusForFailure(Map<Integer, String> errorMap);
	
	 boolean isEnrollmentFromPendingToCancel(Enrollment enrollment) throws GIException ;

	 /**
	  * Handle inbound CSV for TA1, 999 and Bad tracking
	  * @param dateTime
	 * @throws GIException 
	  */
	void monitorInboundEDIjob(String dateTime) throws GIException;
	
	/**
	 * Jira ID: HIX-92255
	 * @param extractMonth
	 * @param enrollmentType
	 * @param hiosIssuerID
	 * @param coverageYear
	 * @throws GIException
	 */
	void serveEnrollmentReconciliationJob(String extractMonth, String enrollmentType,String hiosIssuerID, String coverageYear) throws GIException;
}
