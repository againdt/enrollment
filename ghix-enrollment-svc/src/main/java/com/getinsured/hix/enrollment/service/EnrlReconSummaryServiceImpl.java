package com.getinsured.hix.enrollment.service;


import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrlDiscrepancyDataDTO;
import com.getinsured.hix.dto.enrollment.EnrlReconMonthlyActivityDTO;
import com.getinsured.hix.dto.enrollment.EnrlReconMonthlyActivityDTO.FileLevelSummary;
import com.getinsured.hix.dto.enrollment.EnrlReconMonthlyActivityDTO.HighLevelSummary;
import com.getinsured.hix.dto.enrollment.EnrlReconMonthlyActivityDTO.IssuerDetails;
import com.getinsured.hix.dto.enrollment.EnrlReconMonthlyActivityDTO.IssuerLevelSummary;
import com.getinsured.hix.dto.enrollment.EnrlReconMonthlyActivityDTO.TopDiscrepancy;
import com.getinsured.hix.dto.enrollment.EnrlReconciliationResponse;
import com.getinsured.hix.dto.enrollment.EnrlSearchDiscrepancyDTO;
import com.getinsured.hix.dto.enrollment.EnrlSearchDiscrepancyDTO.EnrlDiscrepancyLookup;
import com.getinsured.hix.dto.enrollment.EnrollmentReconRequest;
import com.getinsured.hix.dto.feeds.IssuerInfoDTO;
import com.getinsured.hix.dto.feeds.IssuerListDetailsDTO;
import com.getinsured.hix.enrollment.repository.IEnrlReconDiscrepancyRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconLkpRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSnapshotRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSummaryRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy.DiscrepancyStatus;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * 
 * @author sharma_k
 *
 */
@Service("enrlReconSummaryService")
public class EnrlReconSummaryServiceImpl implements EnrlReconSummaryService{

	private static final Logger LOGGER = Logger.getLogger(EnrlReconSummaryServiceImpl.class);
	@Autowired private IEnrlReconSummaryRepository iEnrlReconSummaryRepository;
	@Autowired private IEnrlReconDiscrepancyRepository iEnrlReconDiscrepancyRepository;
	@Autowired private IEnrlReconLkpRepository iEnrlReconLkpRepository;
	@Autowired private IEnrollmentRepository iEnrollmentRepository;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	@PersistenceUnit private EntityManagerFactory emf;
	@Autowired private IEnrlReconSnapshotRepository iEnrlReconSnapshotRepository;
	
	private static Map<Integer, String> monthDetailsMap;
	private static LinkedHashMap<String, IssuerDetails> issuerDetailsListMap;
	
	private static final String SEARCH_DISCREPANCY_SELECT_QUERY = "SELECT COUNT(DISTINCT(ers.ID)), ers.HIX_ENROLLMENT_ID, ers.ISSUER_ASSIGNED_POLICY_ID, ers.HIX_SUBSCRIBER_ID, "
			+ " ersu.HIOS_ISSUER_ID, ers.SUBSCRIBER_NAME, ers.BENEFIT_EFFECTIVE_DATE"
			+ " FROM ENRL_RECON_SUMMARY ersu, ENRL_RECON_SNAPSHOT ers";
	private static final String SEARCH_DISCREPANCY_COUNT_QUERY = "SELECT COUNT(*) FROM (";
	
	private static final String SEARCH_TOP_DISCREPANCY_QUERY = "SELECT COUNT(*), enrlLkp.code, enrlLkp.label,  enrlLkp.category "
			+" FROM ENRL_RECON_DISCREPANCY erd, ENRL_RECON_SUMMARY ers, ENRL_RECON_LKP enrlLkp"
			+" WHERE erd.FILE_ID = ers.id AND erd.ENRL_RECON_LKP_ID = enrlLkp.id"
			+" AND ers.month = :monthNum AND ers.year = :year AND ers.HIOS_ISSUER_ID IN (:hiosIssuerIdList)"
			+" AND ers.status IN ('Autofixed', 'Completed')"
			+" AND ers.status NOT IN ('Duplicate')"
			+" GROUP BY enrlLkp.code, enrlLkp.label, enrlLkp.category ORDER BY COUNT(*) DESC";
	
	
	@Override
	public EnrlReconciliationResponse getPopulatedMonthAndCarrierList(EnrlReconciliationResponse enrlReconciliationResponse) {
		
		EnrlReconMonthlyActivityDTO enrlReconMonthlyActivityDTO = new EnrlReconMonthlyActivityDTO();
		enrlReconMonthlyActivityDTO.setMonthDetailsMap(populateMonthDetails());
		enrlReconMonthlyActivityDTO.setYearList(populateYearList());
		enrlReconMonthlyActivityDTO.setIssuerDetailsList(populateIssuerDetails());
		enrlReconciliationResponse.setEnrlReconMonthlyActivityDTO(enrlReconMonthlyActivityDTO);
		
		return enrlReconciliationResponse;
	}
	
	
	/**
	 * 
	 * @param enrollmentRequest
	 * @param enrollmentResponse
	 * @return
	 */
	@Override
	public EnrlReconciliationResponse getMonthlyActivityData(final EnrollmentReconRequest enrollmentReconRequest, EnrlReconciliationResponse enrlReconciliationResponse){
		try{
			Integer iterationMonth = enrollmentReconRequest.getMonthNum();
			Integer iterationYear = enrollmentReconRequest.getYear();
			EnrlReconMonthlyActivityDTO enrlReconMonthlyActivityDTO = new EnrlReconMonthlyActivityDTO();
			List<String> hiosIdList = new ArrayList<String>();
			List<Object[]> fileLevelSummaryObjList = null;
			if(StringUtils.isNotBlank(enrollmentReconRequest.getHiosIssuerId())){
				hiosIdList.add(enrollmentReconRequest.getHiosIssuerId());
				fileLevelSummaryObjList = iEnrlReconSummaryRepository.getFileLevelSummary(enrollmentReconRequest.getMonthNum(), enrollmentReconRequest.getYear(), enrollmentReconRequest.getHiosIssuerId());
			}
			else if(issuerDetailsListMap != null && !issuerDetailsListMap.isEmpty()){
				hiosIdList.addAll(issuerDetailsListMap.keySet());
			}
			else{
				populateIssuerDetails();
				if(issuerDetailsListMap != null && !issuerDetailsListMap.isEmpty()){
					hiosIdList.addAll(issuerDetailsListMap.keySet());
				}
				else{
					enrlReconciliationResponse.setErrMsg("Unable to retreive HIOS_ISSUER_ID list or no Issuers present in DB");
					enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					return enrlReconciliationResponse;
				}
			}
						
			
			
			Date currentMonthEndDate = getPastDateByMonthAndYear(iterationMonth, iterationYear, EnrollmentConstants.ZERO);
			Date twelveMonthOldDate = getPastMonthStartDateByParam(currentMonthEndDate, EnrollmentConstants.TWELVE);
			
			
			List<Object[]> historySummaryDetailsObjList =  iEnrlReconSummaryRepository.getHistorySummaryDetails(hiosIdList, currentMonthEndDate, twelveMonthOldDate);
			List<Object[]> highLevelSummaryObjList = iEnrlReconSummaryRepository.getHighLevelSummary(enrollmentReconRequest.getMonthNum(), enrollmentReconRequest.getYear(), hiosIdList);
			List<Object[]> issuerLevelSummaryObjList = iEnrlReconSummaryRepository.getIssuerLevelSummary(enrollmentReconRequest.getMonthNum(), enrollmentReconRequest.getYear(), hiosIdList);
			//List<Object[]> topDiscrepancyObjList = iEnrlReconDiscrepancyRepository.getTopDiscrepencies(enrollmentReconRequest.getMonthNum(), enrollmentReconRequest.getYear(), hiosIdList);
			List<TopDiscrepancy> topDiscrepancyList = getTopDiscrepancy(enrollmentReconRequest.getMonthNum(), enrollmentReconRequest.getYear(), hiosIdList);
			//List<Object[]> erlReconSummaryMissingCountList = iEnrlReconMissingCountRepository.getHighLevelSummaryMissingCount(iterationMonth, iterationYear, hiosIdList);
			
			
			if(issuerLevelSummaryObjList != null && !issuerLevelSummaryObjList.isEmpty()){
				List<IssuerLevelSummary> issuerLevelSummaryList = new ArrayList<IssuerLevelSummary>();
				for(Object[] issuerLevelSummaryObj : issuerLevelSummaryObjList){
					
					IssuerLevelSummary issuerLevelSummary = new IssuerLevelSummary();
					issuerLevelSummary.setTotalNumberOfFiles((Long) issuerLevelSummaryObj[EnrollmentConstants.ZERO]);
					issuerLevelSummary.setHiosIssuerId((String) issuerLevelSummaryObj[EnrollmentConstants.ONE]);
					issuerLevelSummary.setIssuerName((String) issuerLevelSummaryObj[EnrollmentConstants.TWO]);
					issuerLevelSummary.setTotalEnrlCntInFile((Long) issuerLevelSummaryObj[EnrollmentConstants.THREE]);
					issuerLevelSummary.setTotalEnrlInHix((Long)issuerLevelSummaryObj[EnrollmentConstants.FOUR]);
					issuerLevelSummary.setEnrlSuccessInHixCount((Long) issuerLevelSummaryObj[EnrollmentConstants.FIVE]);
					issuerLevelSummary.setEnrollmentFailedCount((Long) issuerLevelSummaryObj[EnrollmentConstants.SIX]);
					issuerLevelSummary.setTotalDiscrepancyCount((Long) issuerLevelSummaryObj[EnrollmentConstants.SEVEN]);
					issuerLevelSummary.setTotalEnrlDiscrepancyCount((Long) issuerLevelSummaryObj[EnrollmentConstants.EIGHT]);
					issuerLevelSummary.setEnrlMissinginHixCount(issuerLevelSummaryObj[EnrollmentConstants.NINE] != null ? (Long) issuerLevelSummaryObj[EnrollmentConstants.NINE] : EnrollmentConstants.ZERO);
					issuerLevelSummary.setEnrlMissingInFileCount(issuerLevelSummaryObj[EnrollmentConstants.TEN] != null ? (Long) issuerLevelSummaryObj[EnrollmentConstants.TEN] : EnrollmentConstants.ZERO);//Newly Added
					
					
					//Percent Calculation	
					issuerLevelSummary.setEnrlSuccessInHixPercent(EnrollmentUtils.calculatePercentage(issuerLevelSummary.getEnrlSuccessInHixCount(), issuerLevelSummary.getTotalEnrlInHix()));
					issuerLevelSummary.setEnrollmentFailedPercent(EnrollmentUtils.calculatePercentage(issuerLevelSummary.getEnrollmentFailedCount(), issuerLevelSummary.getTotalEnrlInHix()));
					issuerLevelSummary.setTotalEnrlDiscrepancyPercent(EnrollmentUtils.calculatePercentage(issuerLevelSummary.getTotalEnrlDiscrepancyCount(), issuerLevelSummary.getTotalEnrlInHix()));
					
					Date lastReconDate = iEnrlReconSummaryRepository.getLastReconDate(issuerLevelSummary.getHiosIssuerId());
					issuerLevelSummary.setLastReconDate(lastReconDate!= null ? DateUtil.dateToString(lastReconDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY): null);
					issuerLevelSummaryList.add(issuerLevelSummary);
				}
				enrlReconMonthlyActivityDTO.setIssuerLevelSummaryList(issuerLevelSummaryList);
			}
			
			if(historySummaryDetailsObjList != null && !historySummaryDetailsObjList.isEmpty()){
				List<HighLevelSummary> historySummaryList = new ArrayList<HighLevelSummary>();
				for (int i = EnrollmentConstants.ONE; i <= EnrollmentConstants.TWELVE; i++) {
					Object[] highLevelSummaryObj = getObjectArray(iterationMonth, iterationYear, historySummaryDetailsObjList);
					HighLevelSummary historyLevelSummary = new HighLevelSummary();
					if(highLevelSummaryObj != null){
						historyLevelSummary.setMonth(iterationMonth);
						historyLevelSummary.setYear(iterationYear);
						historyLevelSummary.setTotalEnrlCntInFile(highLevelSummaryObj[EnrollmentConstants.TWO] != null ? (Long) highLevelSummaryObj[EnrollmentConstants.TWO] : EnrollmentConstants.ZERO);
						historyLevelSummary.setTotalEnrlInHix(highLevelSummaryObj[EnrollmentConstants.THREE] != null ? (Long) highLevelSummaryObj[EnrollmentConstants.THREE] : EnrollmentConstants.ZERO );
						historyLevelSummary.setTotalSuccess(highLevelSummaryObj[EnrollmentConstants.FOUR] != null ? (Long)highLevelSummaryObj[EnrollmentConstants.FOUR] : EnrollmentConstants.ZERO);
						historyLevelSummary.setTotalFailure(highLevelSummaryObj[EnrollmentConstants.FIVE] != null ? (Long)highLevelSummaryObj[EnrollmentConstants.FIVE] : EnrollmentConstants.ZERO);
						historyLevelSummary.setTotalDiscrepancyCount(highLevelSummaryObj[EnrollmentConstants.SIX] != null ? (Long)highLevelSummaryObj[EnrollmentConstants.SIX]: EnrollmentConstants.ZERO);
						historyLevelSummary.setTotalEnrlDiscrepancyCount(highLevelSummaryObj[EnrollmentConstants.SEVEN] != null ? (Long)highLevelSummaryObj[EnrollmentConstants.SEVEN]: EnrollmentConstants.ZERO);
						historyLevelSummary.setTotalMissingInHix(highLevelSummaryObj[EnrollmentConstants.EIGHT] != null ? (Long) highLevelSummaryObj[EnrollmentConstants.EIGHT]: EnrollmentConstants.ZERO);
						historyLevelSummary.setTotalMissingInFile(highLevelSummaryObj[EnrollmentConstants.NINE] != null ? (Long) highLevelSummaryObj[EnrollmentConstants.NINE] : EnrollmentConstants.ZERO);//Newly added
						
						//Percent calculation
						if(historyLevelSummary.getTotalEnrlInHix() != null){
							historyLevelSummary.setTotalEnrlCntInFilePercent(EnrollmentUtils.calculatePercentage(historyLevelSummary.getTotalEnrlCntInFile(), historyLevelSummary.getTotalEnrlInHix()));
							historyLevelSummary.setTotalMissingInHixPercent(EnrollmentUtils.calculatePercentage(historyLevelSummary.getTotalMissingInHix(), historyLevelSummary.getTotalEnrlCntInFile()));
							historyLevelSummary.setTotalMissingInFilePercent(EnrollmentUtils.calculatePercentage(historyLevelSummary.getTotalMissingInFile(), historyLevelSummary.getTotalEnrlInHix()));
							historyLevelSummary.setTotalSuccessPercent(EnrollmentUtils.calculatePercentage(historyLevelSummary.getTotalSuccess(), historyLevelSummary.getTotalEnrlInHix()));
							historyLevelSummary.setTotalFailurePercent(EnrollmentUtils.calculatePercentage(historyLevelSummary.getTotalFailure(), historyLevelSummary.getTotalEnrlInHix()));
							historyLevelSummary.setTotalEnrlDiscrepancyPercent(EnrollmentUtils.calculatePercentage(historyLevelSummary.getTotalEnrlDiscrepancyCount(), historyLevelSummary.getTotalEnrlInHix()));
						}
					}
					else{
						historyLevelSummary.setMonth(iterationMonth);
						historyLevelSummary.setYear(iterationYear);
						historyLevelSummary.setTotalEnrlCntInFile(EnrollmentConstants.ZERO.longValue());
						historyLevelSummary.setTotalEnrlInHix(EnrollmentConstants.ZERO.longValue());
						historyLevelSummary.setTotalMissingInHix(EnrollmentConstants.ZERO.longValue());
						historyLevelSummary.setTotalMissingInFile(EnrollmentConstants.ZERO.longValue());// Newly added
						historyLevelSummary.setTotalSuccess(EnrollmentConstants.ZERO.longValue());
						historyLevelSummary.setTotalFailure(EnrollmentConstants.ZERO.longValue());
						historyLevelSummary.setTotalDiscrepancyCount(EnrollmentConstants.ZERO.longValue());
						historyLevelSummary.setTotalEnrlDiscrepancyCount(EnrollmentConstants.ZERO.longValue());
						historyLevelSummary.setTotalEnrlCntInFilePercent(EnrollmentConstants.ZERO.floatValue());
						historyLevelSummary.setTotalMissingInHixPercent(EnrollmentConstants.ZERO.floatValue());
						historyLevelSummary.setTotalMissingInFilePercent(EnrollmentConstants.ZERO.floatValue());
						historyLevelSummary.setTotalSuccessPercent(EnrollmentConstants.ZERO.floatValue());
						historyLevelSummary.setTotalFailurePercent(EnrollmentConstants.ZERO.floatValue());
						historyLevelSummary.setTotalEnrlDiscrepancyPercent(EnrollmentConstants.ZERO.floatValue());
					}
					DateTime dateTime = new DateTime(getPastDateByMonthAndYear(iterationMonth, iterationYear, EnrollmentConstants.ONE));
					iterationMonth = dateTime.getMonthOfYear();
					iterationYear = dateTime.getYear();
					historySummaryList.add(historyLevelSummary);
				}
				Collections.reverse(historySummaryList); //sorting in Reverse order for UI graph
				enrlReconMonthlyActivityDTO.setLastTwelveMonthSummaryList(historySummaryList);
			}
			
			
			if(highLevelSummaryObjList != null && !highLevelSummaryObjList.isEmpty()){
				List<HighLevelSummary> highLevelSummaryList = new ArrayList<HighLevelSummary>();
				for(Object[] highLevelSummaryObj : highLevelSummaryObjList){
					HighLevelSummary highLevelSummary = new HighLevelSummary();
					highLevelSummary.setMonth((Integer) highLevelSummaryObj[EnrollmentConstants.ZERO]);
					highLevelSummary.setYear((Integer) highLevelSummaryObj[EnrollmentConstants.ONE]);
					highLevelSummary.setTotalEnrlCntInFile((Long) highLevelSummaryObj[EnrollmentConstants.TWO]);
					highLevelSummary.setTotalEnrlInHix((Long) highLevelSummaryObj[EnrollmentConstants.THREE]);
					highLevelSummary.setTotalSuccess((Long) highLevelSummaryObj[EnrollmentConstants.FOUR]);
					highLevelSummary.setTotalFailure((Long) highLevelSummaryObj[EnrollmentConstants.FIVE]);
					highLevelSummary.setTotalDiscrepancyCount((Long) highLevelSummaryObj[EnrollmentConstants.SIX]);
					highLevelSummary.setTotalEnrlDiscrepancyCount((Long) highLevelSummaryObj[EnrollmentConstants.SEVEN]);
					highLevelSummary.setTotalMissingInHix(highLevelSummaryObj[EnrollmentConstants.EIGHT] != null ? (Long)highLevelSummaryObj[EnrollmentConstants.EIGHT] : EnrollmentConstants.ZERO.longValue());
					highLevelSummary.setTotalMissingInFile(highLevelSummaryObj[EnrollmentConstants.NINE] != null ? (Long)highLevelSummaryObj[EnrollmentConstants.NINE] : EnrollmentConstants.ZERO.longValue());
					
					
					//Percent calculation
					highLevelSummary.setTotalEnrlCntInFilePercent(EnrollmentUtils.calculatePercentage(highLevelSummary.getTotalEnrlCntInFile(), highLevelSummary.getTotalEnrlInHix()));
					highLevelSummary.setTotalMissingInHixPercent(EnrollmentUtils.calculatePercentage(highLevelSummary.getTotalMissingInHix(), highLevelSummary.getTotalEnrlCntInFile()));
					highLevelSummary.setTotalMissingInFilePercent(EnrollmentUtils.calculatePercentage(highLevelSummary.getTotalMissingInFile(), highLevelSummary.getTotalEnrlInHix()));
					highLevelSummary.setTotalSuccessPercent(EnrollmentUtils.calculatePercentage(highLevelSummary.getTotalSuccess(), highLevelSummary.getTotalEnrlInHix()));
					highLevelSummary.setTotalFailurePercent(EnrollmentUtils.calculatePercentage(highLevelSummary.getTotalFailure(), highLevelSummary.getTotalEnrlInHix()));
					highLevelSummary.setTotalEnrlDiscrepancyPercent(EnrollmentUtils.calculatePercentage(highLevelSummary.getTotalEnrlDiscrepancyCount(), highLevelSummary.getTotalEnrlInHix()));
					
					
					highLevelSummaryList.add(highLevelSummary);
				}
				enrlReconMonthlyActivityDTO.setHighLevelSummaryList(highLevelSummaryList);
			}
			if(fileLevelSummaryObjList != null && !fileLevelSummaryObjList.isEmpty()){
				List<FileLevelSummary> fileLevelSummaryList = new ArrayList<FileLevelSummary>();
				for(Object[] fileLevelSummaryObj: fileLevelSummaryObjList){
					
					FileLevelSummary fileLevelSummary = new FileLevelSummary();
					fileLevelSummary.setFileName((String)  fileLevelSummaryObj[EnrollmentConstants.ZERO]);
					fileLevelSummary.setDateReceived(fileLevelSummaryObj[EnrollmentConstants.ONE] != null ? DateUtil.dateToString((Date) fileLevelSummaryObj[EnrollmentConstants.ONE], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY) : null);
					fileLevelSummary.setStatus((String) fileLevelSummaryObj[EnrollmentConstants.TWO]);
					if((StringUtils.isNotBlank(fileLevelSummary.getStatus()) && !fileLevelSummary.getStatus().equals("Duplicate"))
						|| StringUtils.isBlank(fileLevelSummary.getStatus())){
						fileLevelSummary.setTotalEnrlCntInFile((Integer) fileLevelSummaryObj[EnrollmentConstants.THREE]);
						fileLevelSummary.setTotalEnrlInHix((Integer) fileLevelSummaryObj[EnrollmentConstants.FOUR]);
						fileLevelSummary.setTotalSuccess((Integer) fileLevelSummaryObj[EnrollmentConstants.FIVE]);
						fileLevelSummary.setTotalFailure((Integer) fileLevelSummaryObj[EnrollmentConstants.SIX]);
						fileLevelSummary.setTotalDiscrepancyCount((Integer) fileLevelSummaryObj[EnrollmentConstants.NINE]);
						fileLevelSummary.setTotalEnrlDiscrepancyCount((Integer) fileLevelSummaryObj[EnrollmentConstants.TEN]);
					}
					fileLevelSummary.setDiscrepancyReportName((String) fileLevelSummaryObj[EnrollmentConstants.ELEVEN]);
					fileLevelSummary.setDiscrepancyReportCreationTimestamp(fileLevelSummaryObj[EnrollmentConstants.TWELVE] != null ? DateUtil.dateToString((Date) fileLevelSummaryObj[EnrollmentConstants.TWELVE], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY) : null);
					fileLevelSummary.setFileId((Integer) fileLevelSummaryObj[EnrollmentConstants.THIRTEEN]);
					fileLevelSummary.setIssuerDetails(getIssuerDetailsInfo(enrollmentReconRequest.getHiosIssuerId()));
					fileLevelSummaryList.add(fileLevelSummary);
					
				}
				enrlReconMonthlyActivityDTO.setFileLevelSummaryList(fileLevelSummaryList);
			}
			
			if(topDiscrepancyList != null && !topDiscrepancyList.isEmpty()){
				Long totalDiscrepancyCount = iEnrlReconDiscrepancyRepository.getSumOfTotalDiscrepency(enrollmentReconRequest.getMonthNum(), enrollmentReconRequest.getYear(), hiosIdList);
				enrlReconMonthlyActivityDTO.setTotalDiscrepancyCount(totalDiscrepancyCount);
				enrlReconMonthlyActivityDTO.setTopDiscrepancyList(topDiscrepancyList);
			}
			enrlReconciliationResponse.setEnrlReconMonthlyActivityDTO(enrlReconMonthlyActivityDTO);
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		}
		catch(Exception ex){
			LOGGER.error("EnrlReconSummaryServiceImpl:: MONTHLY_ACTIVITY :: Exception occurred in getMonthlyActivityData(-,-) ", ex);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		return enrlReconciliationResponse;
	}
	
	
	@SuppressWarnings("rawtypes")
	@Override
	public EnrlReconciliationResponse searchEnrlDiscrepancyData(EnrollmentReconRequest enrollmentReconRequest,
			EnrlReconciliationResponse enrlReconciliationResponse) {
		List<EnrlDiscrepancyDataDTO> enrlDiscrepancyDataDTOList= null;
		EntityManager entityManager = null;
		try{
			if(enrollmentReconRequest != null){
				enrlDiscrepancyDataDTOList = new ArrayList<EnrlDiscrepancyDataDTO>();
				Query selectQuery = null;
				Query countQuery = null; 
				entityManager = emf.createEntityManager();
				StringBuilder queryBuilder = new StringBuilder(300);
				
				queryBuilder.append(SEARCH_DISCREPANCY_SELECT_QUERY);
				if(!enrollmentReconRequest.isSearchOnlyDiscrepancy()){
					queryBuilder.append(" LEFT JOIN ENRL_RECON_DISCREPANCY erd ON ers.file_id = erd.file_id AND ers.hix_enrollment_id = erd.hix_enrollment_id "
							+ " LEFT JOIN ENRL_RECON_LKP erl ON erd.enrl_recon_lkp_id = erl.id "
							+ " WHERE ersu.id = ers.file_id ");
				}else{
					queryBuilder.append(" , ENRL_RECON_DISCREPANCY erd, ENRL_RECON_LKP erl"
							+ " WHERE ers.hix_enrollment_id = erd.hix_enrollment_id "
							+ " AND erd.enrl_recon_lkp_id = erl.id "
							+ " AND ersu.id = ers.file_id "
							+ " AND ers.file_id = erd.file_id ");
				}
				queryBuilder.append(" AND ers.hix_enrollment_id is not null ");
				queryBuilder.append(" AND ersu.status not in ('Duplicate') ");
				
				if(StringUtils.isNotBlank(enrollmentReconRequest.getHiosIssuerId())){
					queryBuilder.append(" AND ersu.hios_issuer_id = '");
					queryBuilder.append(enrollmentReconRequest.getHiosIssuerId());
					queryBuilder.append("'");
				}
				if(enrollmentReconRequest.getSubscriberId() != null && enrollmentReconRequest.getSubscriberId() != EnrollmentConstants.ZERO){
					queryBuilder.append(" AND ers.hix_subscriber_id = ");
					queryBuilder.append(enrollmentReconRequest.getSubscriberId());
				}
				if(enrollmentReconRequest.getReconMonth() != null && enrollmentReconRequest.getReconMonth() != EnrollmentConstants.ZERO){
					queryBuilder.append(" AND ersu.month = ");
					queryBuilder.append(enrollmentReconRequest.getReconMonth());
				}
				if(enrollmentReconRequest.getReconYear() != null && enrollmentReconRequest.getReconYear() != EnrollmentConstants.ZERO){
					queryBuilder.append(" AND ersu.year = ");
					queryBuilder.append(enrollmentReconRequest.getReconYear());
				}
				if(StringUtils.isNotBlank(enrollmentReconRequest.getDiscrepencyType())){
					queryBuilder.append(" AND erl.id = ");
					queryBuilder.append(enrollmentReconRequest.getDiscrepencyType());
				}
				if(StringUtils.isNotBlank(enrollmentReconRequest.getStatus())){
					queryBuilder.append(" AND erd.status = '");
					queryBuilder.append(DiscrepancyStatus.valueOf(enrollmentReconRequest.getStatus()));
					queryBuilder.append("'");
				}
				if(enrollmentReconRequest.getIssuerEnrollmentId() != null && enrollmentReconRequest.getIssuerEnrollmentId() != EnrollmentConstants.ZERO){
					queryBuilder.append(" AND ers.issuer_assigned_policy_id = ");
					queryBuilder.append(enrollmentReconRequest.getIssuerEnrollmentId());
				}
				if(enrollmentReconRequest.getHixEnrollmentId() != null && enrollmentReconRequest.getHixEnrollmentId() != EnrollmentConstants.ZERO){
					queryBuilder.append( " AND ers.hix_enrollment_id = ");
					queryBuilder.append(enrollmentReconRequest.getHixEnrollmentId());
				}
				if(enrollmentReconRequest.getCoverageYear() != null && enrollmentReconRequest.getCoverageYear() != EnrollmentConstants.ZERO){
					queryBuilder.append( " AND TO_CHAR(ers.benefit_effective_date, '");
					queryBuilder.append("YYYY') = '"); 
					queryBuilder.append(enrollmentReconRequest.getCoverageYear());
					queryBuilder.append("'");
				}
				if(enrollmentReconRequest.getFileId() != null && enrollmentReconRequest.getFileId() != EnrollmentConstants.ZERO){
					queryBuilder.append( " AND ers.file_id = ");
					queryBuilder.append(enrollmentReconRequest.getFileId());
				}
				
				queryBuilder.append(" GROUP BY ers.HIX_ENROLLMENT_ID, ers.ISSUER_ASSIGNED_POLICY_ID, ers.HIX_SUBSCRIBER_ID, ersu.HIOS_ISSUER_ID, ers.SUBSCRIBER_NAME, ers.BENEFIT_EFFECTIVE_DATE");
				//queryBuilder.append("as countSub ");
				String countQueryString = SEARCH_DISCREPANCY_COUNT_QUERY + queryBuilder.toString() + ") countSub ";
				
				queryBuilder.append(" ORDER BY ");
				if(StringUtils.isNotBlank(enrollmentReconRequest.getSortByField())){
					if(enrollmentReconRequest.getSortByField().equalsIgnoreCase("EnrollmentId")){
						queryBuilder.append(" ers.hix_enrollment_id ");
					}
					else if(enrollmentReconRequest.getSortByField().equalsIgnoreCase("IssuerEnrollmentId")){
						queryBuilder.append(" ers.issuer_assigned_policy_id ");
					}
					else if(enrollmentReconRequest.getSortByField().equalsIgnoreCase("SubscriberIdentifier")){
						queryBuilder.append(" ers.HIX_SUBSCRIBER_ID ");
					}
					else if(enrollmentReconRequest.getSortByField().equalsIgnoreCase("subscriberName")){
						queryBuilder.append(" ers.subscriber_name ");
					}
					else if(enrollmentReconRequest.getSortByField().equalsIgnoreCase("BenefitEffectiveDate")){
						queryBuilder.append(" ers.benefit_effective_date ");
					}
					else if(enrollmentReconRequest.getSortByField().equalsIgnoreCase("NumberOfTimeReconciled")){
						queryBuilder.append(" Count(distinct(ers.id)) ");
					}
					else{
						queryBuilder.append(" ers.hix_enrollment_id ");
					}
				}
				else{
					queryBuilder.append(" ers.hix_enrollment_id ");
				}
				if(StringUtils.isNotBlank(enrollmentReconRequest.getSortOrder())){
					queryBuilder.append(enrollmentReconRequest.getSortOrder());
				}
				else{
					queryBuilder.append(" ASC");
				}
				Integer pageSize = EnrollmentConstants.FIVE;
				if(enrollmentReconRequest.getPageSize() != null && enrollmentReconRequest.getPageSize() != EnrollmentConstants.ZERO){
					pageSize = enrollmentReconRequest.getPageSize();
				}
				
				Integer startPosition = EnrollmentConstants.ZERO;
				if(enrollmentReconRequest.getPageNumber() != null && enrollmentReconRequest.getPageNumber() != EnrollmentConstants.ZERO){
					startPosition = (enrollmentReconRequest.getPageNumber() * pageSize) - pageSize;
				}
								
				selectQuery = entityManager.createNativeQuery(queryBuilder.toString());
				countQuery = entityManager.createNativeQuery(countQueryString);
				
				Number totalRecordCount = (Number) countQuery.getSingleResult();
				selectQuery.setFirstResult(startPosition);
				selectQuery.setMaxResults(pageSize);
				
				List<?> objectList = selectQuery.getResultList();
				if(objectList != null && !objectList.isEmpty()){
					Iterator rsIterator = objectList.iterator();
					Object[] objArray = null;
					while (rsIterator.hasNext()) {
						objArray = (Object[]) rsIterator.next();				
						if(null != objArray[EnrollmentConstants.ONE]){
							EnrlDiscrepancyDataDTO enrlDiscrepancyDataDTO = new EnrlDiscrepancyDataDTO();
							if(objArray[EnrollmentConstants.ONE] != null){
								enrlDiscrepancyDataDTO.setEnrollmentId(Long.valueOf((objArray[EnrollmentConstants.ONE]).toString()));
								enrlDiscrepancyDataDTO.setNumberOfTimeReconciled(iEnrlReconSnapshotRepository.countNumberOfTimeEnrollmentReconciled(enrlDiscrepancyDataDTO.getEnrollmentId()));
							}
							enrlDiscrepancyDataDTO.setIssuerEnrollmentId(objArray[EnrollmentConstants.TWO] != null ? Integer.valueOf((objArray[EnrollmentConstants.TWO]).toString()) : null);
							enrlDiscrepancyDataDTO.setSubscriberIdentifier(objArray[EnrollmentConstants.THREE] != null ? (objArray[EnrollmentConstants.THREE]).toString() : null);
							enrlDiscrepancyDataDTO.setHiosIssuerId((String) objArray[EnrollmentConstants.FOUR]);
							enrlDiscrepancyDataDTO.setSubscriberName((String) objArray[EnrollmentConstants.FIVE]);
							enrlDiscrepancyDataDTO.setBenefitEffectiveDate(objArray[EnrollmentConstants.SIX] != null ? 
									DateUtil.dateToString(Timestamp.valueOf((objArray[EnrollmentConstants.SIX]).toString()), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY): null);
							Object[] lastreconDetailObj = 
									iEnrlReconSummaryRepository.getLastReconilicationFileAndReceivedDate(enrlDiscrepancyDataDTO.getEnrollmentId());
							
							if(lastreconDetailObj != null){
								Object[] arrayObject = (Object[]) lastreconDetailObj[EnrollmentConstants.ZERO];
								enrlDiscrepancyDataDTO.setLastReconFileName((String)arrayObject[EnrollmentConstants.ZERO]);
								enrlDiscrepancyDataDTO.setLastReconcileDate(arrayObject[EnrollmentConstants.ONE] != null ? 
										DateUtil.dateToString((Date)arrayObject[EnrollmentConstants.ONE], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY) : null) ;
							}
							
							
							List<Object[]> discrepancyCountObjList = iEnrlReconDiscrepancyRepository.getDiscrepancyCountByEnrollmentId(enrlDiscrepancyDataDTO.getEnrollmentId());
							Map<String, Object> discrepancyStatusDataMap = new LinkedHashMap<String, Object>();
							discrepancyStatusDataMap.put(DiscrepancyStatus.OPEN.toString(), EnrollmentConstants.ZERO);
							discrepancyStatusDataMap.put(DiscrepancyStatus.HOLD.toString(), EnrollmentConstants.ZERO);
							discrepancyStatusDataMap.put(DiscrepancyStatus.RESOLVED.toString(), EnrollmentConstants.ZERO);
							discrepancyStatusDataMap.put(DiscrepancyStatus.AUTOFIXED.toString(), EnrollmentConstants.ZERO);
							if(discrepancyCountObjList != null && !discrepancyCountObjList.isEmpty()){
								for(Object[] obj: discrepancyCountObjList){
									if(obj[EnrollmentConstants.ZERO] != null){
										discrepancyStatusDataMap.put(((DiscrepancyStatus) obj[EnrollmentConstants.ZERO]).toString(), obj[EnrollmentConstants.ONE]);
									}
								}
							}
							enrlDiscrepancyDataDTO.setDiscrepancyStatusDataMap(discrepancyStatusDataMap);
							enrlDiscrepancyDataDTOList.add(enrlDiscrepancyDataDTO);
						}
				}
			}
			enrlReconciliationResponse.setTotalRecordCount(totalRecordCount != null ? totalRecordCount.intValue() : EnrollmentConstants.ZERO);
			}else{
				enrlReconciliationResponse.setErrMsg(EnrollmentConstants.ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY);
				enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			}
		}
		catch(Exception ex){
			LOGGER.error("EnrlReconSummaryServiceImpl:: SEARCH_DISCREPANCY :: Exception occurred in searchEnrlDiscrepancyData(-,-) ", ex);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
		}
		finally{
			 if(entityManager !=null && entityManager.isOpen()){
				 try{
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
				 }
				 catch(IllegalStateException ex){
					 LOGGER.error("Exception caught while closing EntityManager: ", ex);
				 }
			 }
		}
		enrlReconciliationResponse.setEnrlDiscrepancyDataDTOList(enrlDiscrepancyDataDTOList);
		return enrlReconciliationResponse;
	}
	
	
	@Override
	public EnrlReconciliationResponse populateSearchDiscrepancyCriteria(
			EnrlReconciliationResponse enrlReconciliationResponse) {
		EnrlSearchDiscrepancyDTO enrlSearchDiscrepancyDTO = new EnrlSearchDiscrepancyDTO();
		enrlSearchDiscrepancyDTO.setDiscrepancyStatusList(Arrays.asList(EnrlReconDiscrepancy.DiscrepancyStatus.values()));
		
		List<Object[]> enrlReconDiscrepancyLookupList = iEnrlReconLkpRepository.getEnrlReconLkpData();
		List<EnrlDiscrepancyLookup> enrlDiscrepancyLookupList = null;
		if(enrlReconDiscrepancyLookupList != null && !enrlReconDiscrepancyLookupList.isEmpty()){
			enrlDiscrepancyLookupList = new ArrayList<EnrlDiscrepancyLookup>();
			for(Object[] enrlReconDiscrepancyObj : enrlReconDiscrepancyLookupList){
				EnrlDiscrepancyLookup enrlDiscrepancyLookup = new EnrlDiscrepancyLookup();
				enrlDiscrepancyLookup.setId((Integer) enrlReconDiscrepancyObj[EnrollmentConstants.ZERO]);
				enrlDiscrepancyLookup.setCode((String) enrlReconDiscrepancyObj[EnrollmentConstants.ONE]);
				enrlDiscrepancyLookup.setLabel((String) enrlReconDiscrepancyObj[EnrollmentConstants.TWO]);
				enrlDiscrepancyLookup.setCategory((String) enrlReconDiscrepancyObj[EnrollmentConstants.THREE]);
				enrlDiscrepancyLookupList.add(enrlDiscrepancyLookup);
			}
			enrlSearchDiscrepancyDTO.setEnrlDiscrepancyLookupList(enrlDiscrepancyLookupList);
		}
		enrlSearchDiscrepancyDTO.setIssuerDetailsList(populateIssuerDetails());
		enrlSearchDiscrepancyDTO.setReconMonthList(iEnrlReconSummaryRepository.getDistinctReconMonthList());
		enrlSearchDiscrepancyDTO.setReconYearList(iEnrlReconSummaryRepository.getDistinctReconYearList());
		enrlSearchDiscrepancyDTO.setCoverageYearList(iEnrollmentRepository.getDistinctEnrollmentCoverageYear());	
		
		enrlReconciliationResponse.setEnrlSearchDiscrepancyDTO(enrlSearchDiscrepancyDTO);
		
		return enrlReconciliationResponse;
	}
	
	/**
	 * Populate 12 month Details
	 * @param month
	 * @return
	 */
	private static Map<Integer, String> populateMonthDetails() {
		if(monthDetailsMap == null || (monthDetailsMap != null && monthDetailsMap.isEmpty())){
			monthDetailsMap = EnrollmentUtils.getMonthDetails();
		}
		return monthDetailsMap;
	}
	
	/**
	 * Populate Year List, [CurrentYear -1, CurrentYear, CurrentYear +1] 
	 * @return
	 */
	private static List<Integer> populateYearList(){
		List<Integer> initialYearList = new ArrayList<Integer>();
		Calendar calendar=TSCalendar.getInstance();
		calendar.setTime(new TSDate());
		initialYearList.add(calendar.get(Calendar.YEAR) - EnrollmentConstants.ONE);
		initialYearList.add(calendar.get(Calendar.YEAR));
		initialYearList.add(calendar.get(Calendar.YEAR) + EnrollmentConstants.ONE);
		
		return initialYearList;
	}
	
	/**
	 * Get Issuer List from Plan-mgmt
	 * @return
	 */
	private List<IssuerDetails> populateIssuerDetails(){
		List<IssuerDetails> issuerDetailsList = new ArrayList<IssuerDetails>();
		if(issuerDetailsListMap != null && !issuerDetailsListMap.isEmpty()){
			issuerDetailsList.addAll(issuerDetailsListMap.values());
		}
		else{
			try{
				issuerDetailsListMap = new LinkedHashMap<String, IssuerDetails>();
				IssuerListDetailsDTO issuerListDetailsDTO = new IssuerListDetailsDTO();
				issuerListDetailsDTO.setStateCode(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				issuerListDetailsDTO.setTenantCode("ALL");
				issuerListDetailsDTO.setInsuranceType("ALL");
				List<IssuerInfoDTO> issuerInfoDtoList = enrollmentExternalRestUtil.getIssuerDetails(issuerListDetailsDTO);
				if(issuerInfoDtoList != null && !issuerInfoDtoList.isEmpty()){
					Collections.sort(issuerInfoDtoList, (obj1, obj2) -> obj1.getIssuerName().compareTo(obj2.getIssuerName()));
					for(IssuerInfoDTO issuerInfoDTO : issuerInfoDtoList){
						IssuerDetails issuerDetails = new IssuerDetails();
						issuerDetails.setIssuerId(issuerInfoDTO.getIssuerId());
						issuerDetails.setIssuerName(issuerInfoDTO.getIssuerName());
						issuerDetails.setHiosIssuerId(issuerInfoDTO.getHiosIssuerId());
						issuerDetailsListMap.put(issuerInfoDTO.getHiosIssuerId(), issuerDetails);
					}
					issuerDetailsList.addAll(issuerDetailsListMap.values());
				}
			}
			catch(Exception ex){
				/*
				 * Suppressing exception since for initial run Carrier list is not required.
				 */
				LOGGER.error("EnrlReconSummaryServiceImpl:: MONTHLY_ACTIVITY :: Exception caught while fetching issuerDetails: ", ex);
			}
		}
		return issuerDetailsList;
	}
	
	private IssuerDetails getIssuerDetailsInfo(String hiosIssuerId){
		IssuerDetails issuerDetails = null;
		populateIssuerDetails();
		if(issuerDetailsListMap != null && !issuerDetailsListMap.isEmpty()){
			issuerDetails = issuerDetailsListMap.get(hiosIssuerId);
		}
		return issuerDetails;
	}
	
	private Date getPastMonthStartDateByParam(final Date requestDate, final Integer monthsToSubtract){
		DateTime dateTime = new DateTime(requestDate);
		
		return dateTime.minusMonths(monthsToSubtract).toDate();
	}
	
	private Date getPastDateByMonthAndYear(Integer monthNum, Integer year, final Integer monthsToSubtract){
		DateTime dateTime = new DateTime(year, monthNum, EnrollmentConstants.ONE, EnrollmentConstants.TWENTY_THREE, EnrollmentConstants.FIFTY_NINE);

		return dateTime.minusMonths(monthsToSubtract).dayOfMonth().withMaximumValue().toDate();
	}
	
	private Object[] getObjectArray(Integer monthFilter, Integer yearFilter, List<Object[]> objectArrayList){
		Object[] highLevelSummaryObj = objectArrayList.stream().filter(obj -> obj[EnrollmentConstants.ZERO].equals(monthFilter) && obj[EnrollmentConstants.ONE].equals(yearFilter)).findAny().orElse(null);
		return highLevelSummaryObj;
	}
	
	@SuppressWarnings("rawtypes")
	private List<TopDiscrepancy> getTopDiscrepancy(Integer month, Integer year, List<String> hiosIdList){
		List<TopDiscrepancy> topDiscrepancyList = new ArrayList<TopDiscrepancy>();
		EntityManager entityManager = null;
		try{
			entityManager = emf.createEntityManager();
			Query selectQuery = null;
			
			
			
			selectQuery = entityManager.createNativeQuery(SEARCH_TOP_DISCREPANCY_QUERY);
			selectQuery.setParameter("monthNum", month);
			selectQuery.setParameter("year", year);
			selectQuery.setParameter("hiosIssuerIdList", hiosIdList);
			selectQuery.setMaxResults(EnrollmentConstants.FIVE);
			
			List<?> objectList = selectQuery.getResultList();
			if(objectList != null && !objectList.isEmpty()){
				Iterator rsIterator = objectList.iterator();
				Object[] topDiscrepancyObj = null;
				while (rsIterator.hasNext()) {
					topDiscrepancyObj = (Object[]) rsIterator.next();				
					if(null != topDiscrepancyObj[EnrollmentConstants.ZERO]){
						TopDiscrepancy topDiscrepancy = new TopDiscrepancy();
						topDiscrepancy.setDiscrepancyCount(topDiscrepancyObj[EnrollmentConstants.ZERO] != null ? ((Number)topDiscrepancyObj[EnrollmentConstants.ZERO]).longValue(): EnrollmentConstants.ZERO);
						topDiscrepancy.setDiscrepancyTypeCode((String) topDiscrepancyObj[EnrollmentConstants.ONE]);
						topDiscrepancy.setDiscrepancyTypeLabel((String) topDiscrepancyObj[EnrollmentConstants.TWO]);
						topDiscrepancy.setDiscrepancyCategory((String) topDiscrepancyObj[EnrollmentConstants.THREE]);
						topDiscrepancyList.add(topDiscrepancy);
					}
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception caught while fetching TopDiscrepancy: ",ex);
		}
		finally{
			if(entityManager !=null && entityManager.isOpen()){
				 try{
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
				 }
				 catch(IllegalStateException ex){
					 LOGGER.error("Exception caught while closing EntityManager: ", ex);
				 }
			 }
		}
		return topDiscrepancyList;
	}
}
