package com.getinsured.hix.enrollment.util;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.PropertiesEnumMarker;

/**
 * @author Raja
 * @since 31/07/2013
 * 
 * This class set value for all enrollment constants from gi_app_config table  
 */

@Component
@DependsOn("dynamicPropertiesUtil")
public final class EnrollmentConfiguration {
   		public enum EnrollmentConfigurationEnum implements PropertiesEnumMarker
		{  
			SOURCE_EXCHANGE_ID ("enrollment.SourceExchangeId"),
			GENDER ("enrollment.Gender"),
			ERRMESSAGE_E000 ("enrollment.errMessage.E-000"),
			ERRMESSAGE_E001 ("enrollment.errMessage.E-001"),
			ERRMESSAGE_E002 ("enrollment.errMessage.E-002"),
			ERRMESSAGE_E003 ("enrollment.errMessage.E-003"),
			ERRMESSAGE_E004 ("enrollment.errMessage.E-004"),
			ERRMESSAGE_E005 ("enrollment.errMessage.E-005"),
			ERRMESSAGE_E006 ("enrollment.errMessage.E-006"),
			ERRMESSAGE_E007 ("enrollment.errMessage.E-007"),
			DISPLAY_PROPERTIES_DISPLAY_CONFIRMATION_DISCLAIMERS ("enrollment.displayProperties.displayConfirmationDisclaimers"),
			DISPLAY_PROPERTIES_DISPLAY_CONFIRMATION_MAKING_CHANGES_TO_YOUR_PLANS ("enrollment.displayProperties.displayConfirmationMakingChangesToYourPlans"),
			DISPLAY_PROPERTIES_DISPLAY_SIGNATURE_PIN ("enrollment.displayProperties.displaySignaturePIN"),
			DISPLAY_PROPERTIES_DISPLAY_FILE_TAX_RETURN ("enrollment.displayProperties.displayFileTaxReturn"),
			INBOUND_LOOKUPBY_ENROLLMENT_ID ("enrollment.InboundLookupByEnrollmentId"),
			
			XMLEXTRACTPATH ("enrollment.XMLExtractPath"),
			
			XMLINDRECONEXTRACTPATH ("enrollment.XMLINDReconExtractPath"),
			
			XMLSHOPRECONEXTRACTPATH ("enrollment.XMLShopReconExtractPath"),
			
			UPDATEENROLLMENTXMLFILEPATH ("enrollment.UpdateEnrollmentXMLFilePath"),
			
			XMLCMSRECONEXTRACTPATH ("enrollment.XMLCMSReconExtractPath"),
			
			INDRECONEXTRACTMONTH ("enrollment.INDReconExtractMonth"),
			
			XMLCMSDAILYFILEPATH ("enrollment.XMLCMSDailyFilePath"),
			
			INBOUND834XMLPATH ("enrollment.inbound834XMLPath"),
			
			INBOUND820XMLPATH ("enrollment.inbound820XMLPath"),
			
			INBOUNDTHREADPOOLSIZE ("enrollment.InboundThreadPoolSize"),
			
			INBOUNDTHREADTIMEOUT ("enrollment.inboundThreadTimeOut"),
			
			XMLCMSRECONCANCELTERMENROLLMENTPATH ("enrollment.xmlCmsReconCancelTermEnrollmentPath"),
			
			ISA05 ("enrollment.ISA05"),
			
			ISA06 ("enrollment.ISA06"),
			
			ISA07 ("enrollment.ISA07"),
			
			ISA08 ("enrollment.ISA08"),
			
			ISA15 ("enrollment.ISA15"),
			
			GS02 ("enrollment.GS02"),
			
			GS03 ("enrollment.GS03"),
			
			GS08 ("enrollment.GS08"),
			
			GS08_834 ("enrollment.GS08.834"),
			
			GS08_820 ("enrollment.GS08.820"),
			
			GS08_999 ("enrollment.GS08.999"),
			
			GS08_TA1 ("enrollment.GS08.TA1"),
			
			INPROGRESS_DIRECTORY ("enrollment.inprogress.directory"),
			
			ARCHIVE_DIRECTORY ("enrollment.archive.directory"),
			
			GROUPXMLEXTRACTPATH ("enrollment.GroupXMLExtractPath"),
			
			FINDADULTDENTALPLANLINK ("enrollment.findAdultDentalPlanLink"),
			
			ENROLLMENTBATCHFAILUREEMAILNOTIFICATIONTO ("enrollment.EnrollmentBatchFailureEmailNotificationTo"),
			
			ENROLLMENTBATCHFAILUREEMAILNOTIFICATIONFROM ("enrollment.EnrollmentBatchFailureEmailNotificationFrom"),
			
			AHBXWSDLCALLENABLE ("enrollment.ahbxWsdlCallEnable"),
			
			LDAPCALLENABLE ("enrollment.ldapCallEnable"),
			
			WSDLFFMENROLLMENT ("enrollment.wsdlffmenrollment"),		
			
			IRSREPORTCONTENTFILEXMLPATH("enrollment.IRSReportContentFileXMLPath"),
			
			IRSREPORTMANIFESTFILEPATH("enrollment.IRSReportManifestFilePath"),
			
			IRSREPORTZIPFOLDERPATH("enrollment.IRSReportZipFolderPath"),
			
			IRSREPORTHUBFTPPATH("enrollment.IRSReportHubFTPPath"),
			
			IRSREPORTENVIRONMENT("enrollment.IRSReportEnvironment"),
			
			RECONCILIATIONENVIRONMENT("enrollment.ReconciliationEnvironment"),
			
			TradingPartnerID("enrollment.TradingPartnerID"),
			
			IRS_CMS_PARTNERID("enrollment.irs.CmsPartnerID"),
			
		    IRS_HEALTH_EXCHANGE_ID("enrollment.irs.HealthExchangeId"), 
			
			IRS_REPORT_ARCHIVE_FOLDERPATH("enrollment.IRSReportArchiveXMLPath"),
			
			IRS_NACK_RESPONSE_HUBFILE_PATH("enrollment.IRSNackResponseHubFilePath"),
			IRS_NACK_RESPONSE_MARKETPLACE_PATH("enrollment.IRSNackResponseMarketPlacePath"),
			IRS_RESPONSE_HUBFILE_PATH("enrollment.IRSResponseHubFilePath"),
			IRS_RESPONSE_MARKETPLACE_PATH("enrollment.IRSResponseMarketPlacePath"),
			IRSXML_MAXHOUSEHOLD_LIMIT("enrollment.MaxHouseholdLimitInIRSXML"),
			EMAIL_GRP_IRS_REPORTING("enrollment.EmailGroupForIRSReporting"),
			
			IRS_EOM_ACK_FILEPATH ("enrollment.IRSReportEOMACKFilePath"),
			
			IRS_EOM_OUT_FILEPATH ("enrollment.IRSReportEOMOUTFilePath"),
			
			ENROLLMENT_RECONCILIATION_REPORT_PATH("enrollment.EnrollmentReconciliationReportPath"),
			
			IRS_REPORT_FAILURE_FOLDER_PATH("enrollment.IRSReportFailureFolderPath"),
			IRS_REPORT_DOCS_PATH("enrollment.IRSReportDocsPath"),
			
			IRS_1095_XML_COMMIT_INTERVAL("enrollment.1095.xml.commitInterval"),
			IRS_1095_XML_THREAD_WAIT("enrollment.1095.xml.threadWait"),
			IRS_1095_XML_THREAD_POOL_SIZE("enrollment.1095.xml.poolSize"),
			IRS_1095_XML_ENROLLMENT_COUNT("enrollment.1095.xml.enrollmentCount"),
			
			
			EFFECTUATE_NEWLY_ADDED_MEMBERS("enrollment.effectuate.new.members"),
			STATE_CONTINUATION("shop.StateContinuation"),
			
			UPDATE_INBOUND_EFFECTUATION_DATE("enrollment.UpdateInboundEffectuationDate"),
			
			NEW_ENROLLMENT_ON_SUBSCRIBER_CHANGE("enrollment.NewEnrollmentOnSubscriberChange"),
   			
			INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING("enrollment.InternalEmailGroupFor1095Reporting"),
   			
   			EXTERNAL_EMAIL_GROUP_FOR_1095_REPORTING("enrollment.ExternalEmailGroupFor1095Reporting"),
			
			ENROLLMENT_ADMIN_EFFECTUATION_CSV_PATH("enrollment.admin.effectuation.csvPath"),
			
			ENROLLMENT_ADMIN_EFFECTUATION_ARCHIVE_CSV_PATH("enrollment.admin.effectuation.csvArchivePath"),
			
			ENROLLMENT_OUT_JOB_PATH("enrollment.out.job.path"),
			
			ENROLLMENT_IRS_FOLDER_PATH("enrollment.IRSFolderPath"),
			
			ENROLLMENT_PLR_REPORT_ENVIRONMENT("enrollment.PLRReportEnvironment"),
			
			UPDATE_MAILING_ADDRESS_YEAR("enrollment.mailing.address.year"),
			
			ONLY_LATEST_MAILING_ADDRESS_834("enrollment.only_latest_mailing_address_834"),
			
			IRS_1095_XML_NAMESPACE_PREFIX_REQUIRED("enrollment.1095.xml.isNameSpacePrefixRequired"),
			
			IRS_MONTHLY_RESPONSE_ARCHIVE_PATH("enrollment.IRSMonthlyResponseArchivePath"),
			
			//IRS_MONTHLY_RESPONSE_PATH("enrollment.IRSMonthlyResponsePath"),
			
			JIRA_UTIL_ENROLLMENT_COMPONENT("enrollment.jiraUtil.component"),
			
			JIRA_UTIL_ENROLLMENT_FIX_VERSION("enrollment.jiraUtil.fixVersion"),
			
			MONTHLY_PREMIUM_POPULATION_FLAG("enrollment.monthlyPremiumPopulation"),
			
			POPULATE_PAYMENT_TXN_ID("enrollment.populatePaymentTXNId"),
			
			SHOW_834_PREMIUM("enrollment.show834Premium"),
			
			APPLY_ONE_DOLLAR_RULE("enrollment.enableOneDollarRule"),
			
			ENROLLMENT_PER_CMSXMLFILE("enrollment.enrollmentPerCmsXmlFile"),
   			
			ENROLLMENT_PER_CMSOUTXMLTHREAD("enrollment.enrollmentPerCmsOutXmlThread"),
   			
			ENROLLMENT_CMSOUTXML_THREAD_POOL("enrollment.enrollmentCmsOutXmlThreadPool"),
   			
			CMS_PREVIOUS_YEAR_REPORTING_MONTHS("enrollment.cmsPreviousYearReportingMonths"),
			
   			CMS_PREVIOUS_YEAR_REPORTING_REQUIRED("enrollment.cmsPreviousYearReportingRequired"),
   			ENROLLMENT_CMS_ENVIRONMENT("enrollment.cmsEnvironment"),
   			
   			CMS_SFTP_SWITCH("enrollment.cms.sftp.switch"),
   			
   			ENABLE_JIRA_CREATION("enrollment.enableJiraCreation"),
   			
   			RECON_SNAPSHOTGEN_THREAD_TIMEOUT ("enrollment.enrlReconGenerateSnapshot.threadTimeOut"),
   			
   			RECON_SNAPSHOTGEN_THREAD_SIZE ("enrollment.enrlReconGenerateSnapshot.threadSize"),
   			
   			RECON_SNAPSHOTGEN_THREAD_POOLSIZE ("enrollment.enrlReconGenerateSnapshot.threadPoolSize"),
   			
   			RECON_TEST_DATA_CREATION_ENRL_COUNT("enrollment.reconTestDataCreation.enrlCount"),
   			
   			RECON_TEST_DATA_CREATION_THREAD_POOLSIZE("enrollment.reconTestDataCreation.threadPoolSize"),
   			
   			PROCESS_RECONCILIATION_FILE_IN_PARALLEL("enrollment.processMultipleCarrierReconFile"),
   			
   			ENRL_RECON_BATCH_JOB_CHAINING("enrollment.carrierRecon.jobChaining"),
   			
   			RECON_DISCREPANCY_REPORT_MAX_DISCREPANCY_COUNT("enrollment.reconDiscrepancyReport.maxDiscrepancyCount"),
   			
   			RECON_DISCREPANCY_REPORT_THREAD_POOLSIZE("enrollment.reconDiscrepancyReport.threadPoolSize"),
   			
   			RECON_DISCREPANCY_REPORT_MAX_ENRL_IN_MEMORY("enrollment.reconDiscrepancyReport.maxEnrollmentsInMemory"),
   			
   			RECON_MISSING_ENROLLMENT_LOOKBACK_DAYS("enrollment.carrierRecon.lookBackDays"),
   			
   			RECON_ALERT_EMAIL_ADDRESS("enrollment.carrierRecon.emailDistibutionList"),
   				
   			RECON_AUTOFIX("enrollment.carrierRecon.reconAutoFix"),
   			
   			RECON_CONSIDER_PREVIOUS_YEAR_UPTO("enrollment.carrierRecon.considerPrevYearUpto"),
   			
   			RECON_SEND_REPORT_COPY_TO_EXCHG("enrollment.carrierRecon.sendReportCopyToExchg"),
   			
   			POPULATE_CSR_APTC_ZERO("enrollment.populateCSRForZeroAPTC"),
   			
   			ENABLE_CMS_VALIDATION_DECK("enrollment.cmsEnableValidationDeck"),
   			
   			ENROLLMENT_AGENT_BOB_TRANSFER_THREAD("enrollment.enrollmentAgentBOBTransfer.threadCount"),
   			
   			ENROLLMENT_AGENT_BOB_TRANSFER_THREAD_POOL("enrollment.enrollmentAgentBOBTransfer.threadPoolSize"),
   			
   			ENROLLMENT_AGE_BASED_REQUOTING("enrollment.ageBasedRequoting"),
   			
   			LOAD_CMS_820_FILE_IN_PARALLEL("enrollment.cms.820.loadFilesInParallel"),
   			
   			PROCESS_CMS_820_FILE_IN_PARALLEL("enrollment.cms.820.processFilesInParallel"),
   			
   			ENROLLMENTS_PER_CMS_820_THREAD("enrollment.cms.820.enrollmentsPerThread"),
   			
   			ENROLLMENT_CMS_820_THREAD_POOL("enrollment.cms.820.threadPoolSize"),
   			
   			APTC_PRORATION("enrollment.aptcProration"),
   			
   			CMS_INCLUDE_PENDING_ENROLLMENTS("enrollment.cmsIncludePendingEnrollments"),
   			
   			ENROLLMENT_UPDATE_BY_CSV_PATH("enrollment.update.by.csvPath"),
			
			ENROLLMENT__UPDATE_BY_CSV_ARCHIVE_PATH("enrollment.update.by.csvArchivePath"),

			EMAIL_GRP_ITPROD_REPORTING("enrollment.EmailGroupForItProdReporting"),
			
			SEND_1095_PDF_TO_INBOX("enrollment.1095.pdf.sendToInbox"),
			
   			PROCESS_EXT_ENROLLMENT_FILE_IN_PARALLEL("enrollment.ext.processMultipleFile"),
   			
   			UPDATE_SSAP_APPLICANTS("enrollment.ext.updateSsapApplicants"),
   			
   			SHOW_834_PAYMENT_TXN_ID("enrollment.showPaymentTxnId"),
   			
   			USE_MIGRATION_ENROLLMENTS("enrollment.api.useMigratedEnrollments"),
   			
   			AUTO_TERM_FOR_RENEWALS("enrollment.autoTermForRenewals"),
   			
   			USE_SAME_EFFECTIVE_DATE("enrollment.useSameEffectiveDate"),
   			
   			ALLOW_MID_MONTH_EFFECTIVE_DATE("enrollment.allowMidMonthEffectiveDate")
   			;
   			
			private final String value;	  
			
			@Override
			public String getValue(){return this.value;}
			
			EnrollmentConfigurationEnum(String value){
		        this.value = value;
		    }
		}
		
		private static final String STATE_CODE_CA = "CA";
		private static final String STATE_CODE_NM = "NM";
		private static final String STATE_CODE_MS = "MS";
		private static final String STATE_CODE_PHIX = "PHIX";
		private static final String STATE_CODE_ID = "ID";
		private static final String STATE_CODE_MN = "MN";
		private static final String STATE_CODE_NV = "NV";
		
		private static String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
   		
   		public static boolean isNmCall(){
   			boolean isNmCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_NM)){
   				isNmCall = true;
   			}
   			return isNmCall;
   		}

   		public static boolean isCaCall(){
   			boolean isCaCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_CA)){
   				isCaCall = true;
   			}
   			return isCaCall;
   		}
   		
   		public static boolean isMsCall(){
   			boolean isMsCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_MS)){
   				isMsCall = true;
   			}
   			return isMsCall;
   		}
   		
   		public static boolean isPhixCall(){
   			boolean isPhixCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_PHIX)){
   				isPhixCall = true;
   			}
   			return isPhixCall;
   		}
   		public static boolean isIdCall(){
   			boolean isIdCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_ID)){
   				isIdCall = true;
   			}
   			return isIdCall;
   		}
   		public static boolean isMnCall(){
   			boolean isMnCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_MN)){
   				isMnCall = true;
   			}
   			return isMnCall;
   		}
		public static boolean isNvCall(){
   			boolean isNvCall = false;
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			if(STATE_CODE.equalsIgnoreCase(STATE_CODE_NV)){
   				isNvCall = true;
   			}
   			return isNvCall;
   		}
   		
   		public static void getStateCode(){
   			STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
   		}
   		
   		public static String returnStateCode(){
   			if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
   				getStateCode();
   			}
   			return STATE_CODE; 
   		}
}