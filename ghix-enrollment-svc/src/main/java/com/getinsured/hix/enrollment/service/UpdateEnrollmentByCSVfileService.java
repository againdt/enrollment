package com.getinsured.hix.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.enrollment.EnrollmentCSVDto;
import com.getinsured.hix.platform.util.exception.GIException;

public interface UpdateEnrollmentByCSVfileService {

	boolean updateEnrollmentByCSV(List<EnrollmentCSVDto> enrollmentCSVList, String name, String wipFolderDir) throws Exception;
	
	void createFailureCSVFile(String failureFolderPath, StringBuilder csvData, String failureType);
	
	void sendFailNotificationEmail(Map<String, String> requestMap) throws GIException;
	
	void moveCSVFileToGoodBadFolder(String fileName, String fileDirectory, boolean contailsBadEnrollment);

}
