package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNull;
import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeOffExchangeDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentListWrapperDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentOffExchangeDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.repository.IEnrollmentIssuerCommisionRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPlanPrivateRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPrivateRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.EnrollmentUtilsForCap;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentIssuerCommision;
import com.getinsured.hix.model.enrollment.EnrollmentPlan;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.google.gson.Gson;

/**
 * @author rajaramesh_g
 * @since 13/02/2013
 * 
 */

@Service("enrollmentOffExchangeService")
@Transactional
public class EnrollmentOffExchangeServiceImpl implements EnrollmentOffExchangeService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentOffExchangeServiceImpl.class);
	
	@Autowired private IEnrollmentPrivateRepository ienrollmentRepository;
	@Autowired private LookupService lookupService;
	@Autowired private EnrollmentUserPrivateService enrollmentUserService;
	@Autowired private RestTemplate restTemplate;
	@Autowired private IEnrollmentPlanPrivateRepository enrollmentPlanRepository;
	@Autowired private EnrollmentEventPrivateService  enrollmentEventService;
	@Autowired private UserService userService;
	@Autowired private Gson platformGson;
	@Autowired private IEnrollmentIssuerCommisionRepository enrollmentIssuerCommisionRepository;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	

	@Override
	public Enrollment findEnrollmentById(Integer enrollmentId) {
		
		LOGGER.info("findEnrollmentById :: enrollmentId is -->"+enrollmentId);
		
		return ienrollmentRepository.findById(enrollmentId);
	} 
	
	@Override
	public Enrollment findEnrollmentByTicketId(String ticketId) {
		
		LOGGER.info("findEnrollmentByTicketId :: Ticket ID is -->"+ticketId);
		
		return ienrollmentRepository.findByTicketId(ticketId);
	}

	@Override
	public Enrollment updateEcommittedEnrollment(String ticketId, String status , String carrierConfirmationNumber ,String applicationId) {
		
		LOGGER.info("updateEcommiTtedEnrollment :: TICKET ID is -->"+ticketId +" STATUS -->"+status+" CARRIER CONFIRMATION NUMBER -->"+carrierConfirmationNumber);
		
		LOGGER.info("updateEcommiTtedEnrollment :: TICKET ID is -->"+ticketId +" STATUS -->"+status+" CARRIER APP ID -->"+applicationId);
		
		Enrollment enrollmentObj = findEnrollmentByTicketId(ticketId);
		if(isNotNullAndEmpty(enrollmentObj)){
			if(status.equals(enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode()) && !isNotNullAndEmpty(carrierConfirmationNumber)){
				return enrollmentObj;
			}
			if(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(status)){
				enrollmentObj.setSubmittedToCarrierDate(new TSDate());
			}
			enrollmentObj.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, status));
			enrollmentObj.setIssuerAssignPolicyNo(carrierConfirmationNumber);
			enrollmentObj.setCarrierAppId(applicationId);
			List<Enrollee> enrolleeList = enrollmentObj.getEnrollees();
			for(Enrollee enrolleeObj : enrolleeList){
				LookupValue lkpValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, status);
				enrolleeObj.setEnrolleeLkpValue(lkpValue);
				if(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(lkpValue.getLookupValueCode())){
					enrolleeObj.setDisenrollTimestamp(new TSDate());
				}
				createEventForEnrolleeAndEnrollment(enrollmentObj, enrolleeObj, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION, enrollmentUserService.getLoggedInUser());
			}
			//HIX-44501 -- Update ( ecommitted app )
			EnrollmentUtilsForCap.setCapAgentId(enrollmentObj, userService);
			enrollmentObj = ienrollmentRepository.save(enrollmentObj);
			updateConsumerApplicationById(enrollmentObj.getSsapApplicationid() , status);
		}			
		return enrollmentObj;
	}

	@Override
	public EnrollmentResponse saveEnrollment(EnrollmentListWrapperDTO enrollmentListWrapperDTO) {

		EnrollmentResponse enrollmentResponse= new EnrollmentResponse();
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		List<EnrollmentOffExchangeDTO> enrollmentOffExchangeDTOList=new ArrayList<EnrollmentOffExchangeDTO>();
		
		if(enrollmentListWrapperDTO.getEnrollmentOffExchangeDTOlist()!=null){
		enrollmentOffExchangeDTOList=enrollmentListWrapperDTO.getEnrollmentOffExchangeDTOlist();
		}
		
		for(EnrollmentOffExchangeDTO enrollmentOffExchangeDTO : enrollmentOffExchangeDTOList)
		{
			Enrollment enrollment = new Enrollment();

			AccountUser user = enrollmentUserService.getLoggedInUser();

			if(!isNotNull(enrollmentOffExchangeDTO.getUserRoleId())){
				Role role = enrollmentUserService.getRoleForUser(user);
				enrollmentOffExchangeDTO.setUserRoleId(role.getId());
				enrollmentOffExchangeDTO.setUserRoleType(role.getName());
			}


			try{
				// POPULATE ENROLLMENT DATA
				setEnrollmentData(enrollment, enrollmentOffExchangeDTO);
				// SAVE ENROLLMENT DATA
				enrollment = ienrollmentRepository.saveAndFlush(enrollment);
				if(null != enrollment.getEnrollmentIssuerCommision()) {
					enrollment.getEnrollmentIssuerCommision().setEnrollment(enrollment);
					enrollmentIssuerCommisionRepository.saveAndFlush(enrollment.getEnrollmentIssuerCommision());
				}
				//HIX-45304
				enrollmentEventService.saveConsumerErnrollmentEvent(enrollment,null,enrollment.getEnrollmentStatusLkp().getLookupValueLabel());
				// POPULATING THE ENROLLMENT E-SIGNATURE DATA 
				//saveEsignatureForEnrollment(enrollment,enrollmentOffExchangeDTO.getEsignature());
				updateConsumerApplicationById(enrollment.getSsapApplicationid() ,EnrollmentPrivateConstants.ENROLLMENT_STATUS_ECOMMITTED);
				// APPENDING THE SAVED ENROLLMENT TO THE ENROLLMENT RESPONSE OBJECT.
				enrollmentList.add(enrollment);

			}catch(Exception e){
				giExceptionHandler.recordFatalException(Component.ENROLLMENT, String.valueOf(EnrollmentConstants.ERROR_CODE_201), EnrollmentPrivateConstants.ERR_MSG_ECOMMITTED_ENROLLMENT_CREATE_FAILURE , e) ;
				LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_ECOMMITTED_ENROLLMENT_CREATE_FAILURE , e);
				enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ECOMMITTED_ENROLLMENT_CREATE_FAILURE);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setModuleStatusCode(EnrollmentPrivateConstants.MODULE_STATUS_CODE_E003);
				return enrollmentResponse;
			}
		}
		enrollmentResponse.setEnrollmentList(enrollmentList);
		enrollmentResponse.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_200);
		enrollmentResponse.setErrMsg(EnrollmentPrivateConstants.MSG_ECOMMITTED_ENROLLMENT_PROCESS_SUCCESS);
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);

		return enrollmentResponse;
	}

	private void setEnrollmentData(Enrollment enrollment, EnrollmentOffExchangeDTO enrollmentOffExchangeDTO) throws GIException{
		
		enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_ECOMMITTED));
		enrollment.setCsrLevel(enrollmentOffExchangeDTO.getCsrLevel());
		enrollment.setAptcAmt(enrollmentOffExchangeDTO.getAptcAmt());
		enrollment.setCmrHouseHoldId(Integer.parseInt(enrollmentOffExchangeDTO.getCmrHouseholdId()));
		if((EnrollmentPrivateConstants.ON).equalsIgnoreCase(enrollmentOffExchangeDTO.getExchangeType())){
			enrollment.setExchangeTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_ONEXCHANGE));
			enrollment.setEnrollmentModalityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.MODALITY_FFM_ECOMMIT));
		}else{
			enrollment.setExchangeTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_OFFEXCHANGE));
			enrollment.setEnrollmentModalityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.MODALITY_FFM_ECOMMIT));
		}
		enrollment.setHouseHoldCaseId(enrollmentOffExchangeDTO.getCmrHouseholdId());
		enrollment.setPriorSsapApplicationid(enrollmentOffExchangeDTO.getSsapApplicationId());
		enrollment.setSsapApplicationid(enrollmentOffExchangeDTO.getSsapApplicationId());
		if (isNotNullAndEmpty(enrollmentOffExchangeDTO.getPlanType())) {
			String lookupValueLabel = enrollmentOffExchangeDTO.getPlanType();
			if(lookupValueLabel.equalsIgnoreCase(EnrollmentPrivateConstants.STM_LOOKUP_VALUE_CODE)){
				lookupValueLabel = EnrollmentPrivateConstants.STM_LOOKUP_VALUE_LABEL;
			}else if(lookupValueLabel.equalsIgnoreCase(EnrollmentPrivateConstants.AME_LOOKUP_VALUE_CODE)){
				lookupValueLabel = EnrollmentPrivateConstants.AME_LOOKUP_VALUE_LABEL;
			}
			enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, lookupValueLabel.toLowerCase()));
		}
		
		//		if (isNotNullAndEmpty(enrollmentOffExchangeDTO.getOrderItemId())) {
		//			PldOrderItem pldOrderItem = new PldOrderItem();
		//			pldOrderItem.setId(enrollmentOffExchangeDTO.getOrderItemId());
		//			enrollment.setIndOrderItem(pldOrderItem);
		//		}
		enrollment.setPlanId(enrollmentOffExchangeDTO.getPlanId());
		enrollment.setPlanLevel(enrollmentOffExchangeDTO.getPlanType());
		if(isNotNullAndEmpty(enrollmentOffExchangeDTO.getEmployerId())){
			Employer employerObj = new Employer();
			employerObj.setId(enrollmentOffExchangeDTO.getEmployerId());
			enrollment.setEmployer(employerObj);
		}
		enrollment.setEmployeeId(enrollmentOffExchangeDTO.getEmployeeId());
		
		if(isNotNullAndEmpty(enrollmentOffExchangeDTO.getUserRoleType()) && enrollmentOffExchangeDTO.getUserRoleType().equalsIgnoreCase(EnrollmentPrivateConstants.ASSISTER_ROLE)){
			enrollment.setAssisterBrokerId(enrollmentOffExchangeDTO.getUserRoleId());
			enrollment.setBrokerRole(enrollmentOffExchangeDTO.getUserRoleType());
		}
		
		if (isNotNullAndEmpty(enrollmentOffExchangeDTO.getMarketType())) {
			enrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE, enrollmentOffExchangeDTO.getMarketType()));
		}
		if (isNotNullAndEmpty(enrollmentOffExchangeDTO.getOrderItemId())){
			enrollment.setPdOrderItemId(enrollmentOffExchangeDTO.getOrderItemId().longValue());	
		}
		enrollment.setBenefitEffectiveDate(enrollmentOffExchangeDTO.getCoverageStartDate());
		enrollment.setGrossPremiumAmt(enrollmentOffExchangeDTO.getGrossPremiumAmt());
		enrollment.setNetPremiumAmt(enrollmentOffExchangeDTO.getNetPremiumAmt());
		// enrollmentOffExchangeDTO.getDependentContribution(); ??
		enrollment.setEmployeeContribution(enrollmentOffExchangeDTO.getEmployeeContribution());
		if(isNotNullAndEmpty(enrollmentOffExchangeDTO.getSadpFlag())){
			enrollment.setSadp(enrollmentOffExchangeDTO.getSadpFlag().toString());
		}
		if(isNotNullAndEmpty(enrollmentOffExchangeDTO.getPreferdTimeOfContact())){
		enrollment.setPrefferedContactTime(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PREFERRED_TIME_TO_CONTACT,enrollmentOffExchangeDTO.getPreferdTimeOfContact().toUpperCase()));
		}
		//enrollmentOffExchangeDTO.getIsSubsidized(); ??
		
		//	POPULATING HOUSEHOLD CONTACT INFORMATION
		Enrollee householdContactObj = new Enrollee(); 
		
		if(isNotNullAndEmpty(enrollmentOffExchangeDTO.getHouseHoldContactId())){
		householdContactObj.setExchgIndivIdentifier(enrollmentOffExchangeDTO.getHouseHoldContactId());
		}
		householdContactObj.setFirstName(enrollmentOffExchangeDTO.getHouseHoldContactFirstName());     
		householdContactObj.setMiddleName(enrollmentOffExchangeDTO.getHouseHoldContactMiddleName());
		householdContactObj.setLastName(enrollmentOffExchangeDTO.getHouseHoldContactLastName());
		householdContactObj.setPreferredEmail(enrollmentOffExchangeDTO.getHouseHoldContactPreferredEmail());
		householdContactObj.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT));
		
		Location householdContactHomeAddress = new Location();
		householdContactHomeAddress.setZip(enrollmentOffExchangeDTO.getHouseHoldContactHomeZip());       
		householdContactHomeAddress.setCity(enrollmentOffExchangeDTO.getHouseHoldContactHomeCity());      
		householdContactHomeAddress.setState(enrollmentOffExchangeDTO.getHouseHoldContactHomeState());     
		householdContactHomeAddress.setAddress1(enrollmentOffExchangeDTO.getHouseHoldContactHomeAddress1());
		householdContactHomeAddress.setAddress2(enrollmentOffExchangeDTO.getHouseHoldContactHomeAddress2());
		householdContactObj.setHomeAddressid(householdContactHomeAddress);
		
		enrollment.setHouseholdContact(householdContactObj);
		
		// POPULATING RESPONSIBLE PERSON INFORMATION
		Enrollee responsiblePersonObj = new Enrollee();
		if(isNotNullAndEmpty(enrollmentOffExchangeDTO.getResponsiblePersonId())){
			responsiblePersonObj.setRespPersonIdCode(enrollmentOffExchangeDTO.getResponsiblePersonId());
		}
		responsiblePersonObj.setFirstName(enrollmentOffExchangeDTO.getResponsiblePersonFirstName());
		responsiblePersonObj.setMiddleName(enrollmentOffExchangeDTO.getResponsiblePersonMiddleName());
		responsiblePersonObj.setLastName(enrollmentOffExchangeDTO.getResponsiblePersonLastName());
		responsiblePersonObj.setPreferredEmail(enrollmentOffExchangeDTO.getResponsiblePersonPreferredEmail());
		responsiblePersonObj.setPrimaryPhoneNo(enrollmentOffExchangeDTO.getResponsiblePersonPrimaryPhone());
		//enrollmentOffExchangeDTO.getResponsiblePersonSsn() ??;
		responsiblePersonObj.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON));
		//responsiblePersonObj.setPersonTypeLkp(enrollmentOffExchangeDTO.getResponsiblePersonType()) ??;
		
		Location responsiblePersonHomeAddress = new Location();
		responsiblePersonHomeAddress.setAddress1(enrollmentOffExchangeDTO.getResponsiblePersonHomeAddress1());
		responsiblePersonHomeAddress.setAddress2(enrollmentOffExchangeDTO.getResponsiblePersonHomeAddress2());
		responsiblePersonHomeAddress.setCity(enrollmentOffExchangeDTO.getResponsiblePersonHomeCity());
		responsiblePersonHomeAddress.setState(enrollmentOffExchangeDTO.getResponsiblePersonHomeState());
		responsiblePersonHomeAddress.setZip(enrollmentOffExchangeDTO.getResponsiblePersonHomeZip());
		responsiblePersonObj.setHomeAddressid(responsiblePersonHomeAddress);
		
		enrollment.setResponsiblePerson(responsiblePersonObj);
		
		PlanRequest planRequest= new PlanRequest();
		planRequest.setPlanId(""+enrollmentOffExchangeDTO.getPlanId());
		planRequest.setCommEffectiveDate(DateUtil.dateToString(enrollmentOffExchangeDTO.getCoverageStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		if(isNotNullAndEmpty(enrollmentOffExchangeDTO.getMarketType())&& enrollmentOffExchangeDTO.getMarketType().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
			planRequest.setMarketType("INDIVIDUAL");
		}
		
		String planInfoResponse=null;
		try{
			//planInfoResponse = restTemplate.postForObject(ghixPlanMgmtPlanDataURL,planRequest,String.class);PlanMgmtEndPoints
			planInfoResponse = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL,planRequest,String.class);
			LOGGER.info("getting successfull response of the plan data from plan management.");
		}
		catch(Exception ex){
			LOGGER.error("exception in getting plan data from the plan management" , ex);
		}

		PlanResponse planResponse = new PlanResponse();
		planResponse = platformGson.fromJson(planInfoResponse, planResponse.getClass());

		if(planResponse==null || (planResponse.getStatus() == null || planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))){
		//if(planResponse==null || planResponse.getStatus() == null){
			LOGGER.info("No Plan data found " );
			throw new GIException(EnrollmentPrivateConstants.ERROR_CODE_301,"No Plan data found. Plan mgmt REST services returns null ","HIGH");
		}
		if (isNotNullAndEmpty(enrollmentOffExchangeDTO.getPlanId())) {
			enrollment.setHiosIssuerId(planResponse.getHiosIssuerId());
			enrollment.setPlanId(planResponse.getPlanId());
			enrollment.setCMSPlanID(planResponse.getIssuerPlanNumber());
			enrollment.setPlanName(planResponse.getPlanName());
			enrollment.setInsurerName(planResponse.getIssuerName());
			enrollment.setInsurerTaxIdNumber(planResponse.getTaxIdNumber());
			enrollment.setCsrAmt(planResponse.getCsrAmount());
			if(planResponse.getPlanLevel()!=null){
				enrollment.setPlanLevel(planResponse.getPlanLevel().trim().toUpperCase());
			}
			enrollment.setIssuerId(planResponse.getIssuerId());
			enrollment.setCarrierAppUrl(planResponse.getApplicationUrl());
			enrollment.setCoInsurance(planResponse.getCoinsurance());
			
			//set enrollment Plan
			setEnrollmentPlanData(enrollment, planResponse);
			setEnrollmentCommissionData(enrollment, planResponse);
		}
		enrollment.setCreatedBy(enrollmentUserService.getLoggedInUser());
		enrollment.setUpdatedBy(enrollmentUserService.getLoggedInUser());
		
		//HIX-44501 - create new enrollment after shopping, additional info page.
		EnrollmentUtilsForCap.setCapAgentId(enrollment, userService);
		// POPULATING THE ENROLLEE DATA		
		setEnrolleeData(enrollment, enrollmentOffExchangeDTO);
		
				
	}

	

 /**
 * setEnrolleeData()
 * @param enrollment
 * @param enrollmentOffExchangeDTO
 */
	private void setEnrolleeData(Enrollment enrollment, EnrollmentOffExchangeDTO enrollmentOffExchangeDTO) {
		
		// get the Logged-in user object
		AccountUser user = enrollmentUserService.getLoggedInUser();
		
		if(isNotNullAndEmpty(enrollmentOffExchangeDTO.getEnrolles()) && !enrollmentOffExchangeDTO.getEnrolles().isEmpty()){
			
			List<EnrolleeOffExchangeDTO> enrolleeList = enrollmentOffExchangeDTO.getEnrolles();
			List<Enrollee> enrollmentEnrollees=new ArrayList<Enrollee>();
			
			for(EnrolleeOffExchangeDTO enrolleeOffExchangeDTO : enrolleeList){
				
				Enrollee enrollee = new Enrollee();
				
				if(isNotNullAndEmpty(enrolleeOffExchangeDTO.getMemberId())){
					enrollee.setExchgIndivIdentifier(enrolleeOffExchangeDTO.getMemberId().toString());
				}
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_ECOMMITTED));
				enrollee.setFirstName(enrolleeOffExchangeDTO.getFirstName());
				enrollee.setMiddleName(enrolleeOffExchangeDTO.getMiddleName());
				enrollee.setLastName(enrolleeOffExchangeDTO.getLastName());
				enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.GENDER, enrolleeOffExchangeDTO.getGenderCode()));
				enrollee.setBirthDate(enrolleeOffExchangeDTO.getDob());
				enrollee.setPrimaryPhoneNo(enrolleeOffExchangeDTO.getPrimaryPhone());
				enrollee.setPreferredEmail(enrolleeOffExchangeDTO.getPreferredEmail());
				//enrolleeOffExchangeDTO.getSsn(); ??
				if(isNotNullAndEmpty(enrolleeOffExchangeDTO.getTobacco())) {
					if(enrolleeOffExchangeDTO.getTobacco()){
						enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, EnrollmentPrivateConstants.T));
					}
					else{
						enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, EnrollmentPrivateConstants.N));
					}
				}
				if ((isNotNull(enrolleeOffExchangeDTO
						.getHouseHoldContactRelationship()) && (isNotNull(enrolleeOffExchangeDTO
						.getHouseHoldContactRelationship()
						.getLookupValueLabel()) && enrolleeOffExchangeDTO
						.getHouseHoldContactRelationship()
						.getLookupValueLabel().equalsIgnoreCase("Self")))
						|| (isNotNull(enrolleeOffExchangeDTO
								.getSubscriberFlag()) && enrolleeOffExchangeDTO
								.getSubscriberFlag())) {
					
					enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER));
//					enrollee.setPrimaryPhoneNo(enrollmentOffExchangeDTO.getHouseHoldContactHomePhoneNumber());
					
					enrollment.setExchgSubscriberIdentifier(enrollee.getExchgIndivIdentifier());
					
					// HIX-62266 Setting subscriber and sponsor name in the enrollment
					StringBuilder subscriberName = new StringBuilder();
					subscriberName.append(enrolleeOffExchangeDTO.getFirstName());
					subscriberName.append(" ");
					if (null != enrolleeOffExchangeDTO.getMiddleName()) {
						subscriberName.append(enrolleeOffExchangeDTO.getMiddleName());
						subscriberName.append(" ");
					}
					subscriberName.append(enrolleeOffExchangeDTO.getLastName());

					enrollment.setSponsorName(subscriberName.toString());
					enrollment.setSubscriberName(subscriberName.toString());
					
					//HIX-62305 Set State Exchange Code
					enrollment.setStateExchangeCode(enrollmentOffExchangeDTO.getHouseHoldContactHomeState());
					
					//HOME ADDRESS FIELDS
					Location homeAddressObj = new Location();
					homeAddressObj.setAddress1(enrollmentOffExchangeDTO.getHouseHoldContactHomeAddress1());
					homeAddressObj.setAddress2(enrollmentOffExchangeDTO.getHouseHoldContactHomeAddress2());
					homeAddressObj.setCity(enrollmentOffExchangeDTO.getHouseHoldContactHomeCity());
					homeAddressObj.setCounty(enrollmentOffExchangeDTO.getHouseHoldContactCounty());
					homeAddressObj.setState(enrollmentOffExchangeDTO.getHouseHoldContactHomeState());
					homeAddressObj.setZip(enrollmentOffExchangeDTO.getHouseHoldContactHomeZip());
					
					enrollee.setHomeAddressid(homeAddressObj);
				}
				else{
					
					enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE));		 
					if(enrolleeOffExchangeDTO.getHomeAddress1()!=null && !enrolleeOffExchangeDTO.getHomeAddress1().equalsIgnoreCase("")){
						Location locationObj = new Location();
						locationObj.setAddress1(enrolleeOffExchangeDTO.getHomeAddress1());
						locationObj.setAddress2(enrolleeOffExchangeDTO.getHomeAddress2());
						locationObj.setCity(enrolleeOffExchangeDTO.getHomeCity());
						locationObj.setCounty(enrolleeOffExchangeDTO.getCounty());
						locationObj.setState(enrolleeOffExchangeDTO.getHomeState());
						locationObj.setZip(enrolleeOffExchangeDTO.getHomeZip());
						enrollee.setHomeAddressid(locationObj);
					}
				}
				
				enrollee.setTotalIndvResponsibilityAmt(enrolleeOffExchangeDTO.getIndividualPremium());
				if(isNotNull(enrolleeOffExchangeDTO.getHouseHoldContactRelationship())){
					enrollee.setRelationshipToHCPLkp(enrolleeOffExchangeDTO.getHouseHoldContactRelationship());
				}
				enrollee.setRatingArea(enrolleeOffExchangeDTO.getRatingArea());
				enrollee.setMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.MARITAL_STATUS, enrolleeOffExchangeDTO.getMaritalStatusCode()));
				enrolleeOffExchangeDTO.setMaintenanceReasonCode(enrolleeOffExchangeDTO.getMaintenanceReasonCode());
				enrollee.setCreatedBy(user);
				enrollee.setEnrollment(enrollment);
				
				// setting the enrollment event for the given enrollment, enrollee, event type, event reason code, user.
				createEventForEnrolleeAndEnrollment(enrollment, enrollee, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION, user);
				enrollmentEnrollees.add(enrollee);
			}
			enrollment.setEnrollees(enrollmentEnrollees);
		}
		
	}
	
	/**
	 * @since 02/12/2013
	 * @author raja
	 * @param enrollment
	 * @param eventType
	 * @param eventReasonCode
	 * @param user
	 * @return enrollmentEventList
	 * 
	 * This method is used to Create EnrollmentEvent object by taking input parameters are 
	 * enrollment,enrollee,eventType,eventReasonCode,user and returns the EnrollmentEvent list.
	 * */
	private List<EnrollmentEvent> createEventForEnrolleeAndEnrollment(Enrollment enrollmentObj, Enrollee enrolleeObj, String eventType ,String eventReasonCode, AccountUser user){
		
		    List<EnrollmentEvent> enrollmentEventList = new ArrayList<EnrollmentEvent>();
		
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
			enrollmentEvent.setEnrollment(enrollmentObj);
			enrollmentEvent.setEnrollee(enrolleeObj);
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE,eventType));
			//add null check
			if(isNotNullAndEmpty(eventReasonCode)){
				if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON, eventReasonCode))){
					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON, eventReasonCode));
				}else{
					throw new RuntimeException("Lookup value is null");
				}
			}else{
				throw new RuntimeException("Event Reason code  is null or empty");
			}
			if(user!=null){
				enrollmentEvent.setCreatedBy(user);
			}
			//added last event Id for Audit
			enrolleeObj.setLastEventId(enrollmentEvent);
			enrollmentEventList.add(enrollmentEvent);
		
	 return enrollmentEventList;
	}
    
	/**
     * saveEsignatureForEnrollment()
     * @param enrollmentObj
     * @param esignatureStr
     * @return Enrollment
     */
/*	private void saveEsignatureForEnrollment(Enrollment enrollmentObj , String esignatureStr) throws GIException{

		if(esignatureStr!=null){

			// POPULATING THE E-SIGATURE OBJECT
			Esignature esignature = new Esignature();
			esignature.setModuleName(ModuleUserService.ENROLLMENT_MODULE);
			esignature.seteSignature(esignatureStr);
			esignature.setEsignDate(new TSDate());

			esignature = esignatureService.saveEsignature(esignature);

			// POPULATING THE ENROLLMENT-ESIGATURE OBJECT
			EnrollmentEsignature enrollmentEsig = new EnrollmentEsignature();
			enrollmentEsig.setEnrollment(enrollmentObj);
			enrollmentEsig.setEsignature(esignature);
			enrollmentEsignatureRepository.save(enrollmentEsig);
		}
	}*/
	
	/**
	 * setEnrollmentPlanData()
	 * @param enrollment
	 * @param planResponse
	 */
	private void setEnrollmentPlanData(Enrollment enrollment, PlanResponse planResponse) {

		// Fetch Enrollment_Plan data for Plan
		EnrollmentPlan enrollmentPlan = enrollmentPlanRepository.findEnrollmentPlanByPlanID(planResponse.getPlanId());
		// Populate Enrollment Plan table with Plan data is it doesn't exists
		if(enrollmentPlan == null){
			enrollmentPlan = new EnrollmentPlan();
			enrollmentPlan.setPlanId(planResponse.getPlanId());
			
			if(isNotNullAndEmpty(planResponse.getDeductible()) && NumberUtils.isNumber(planResponse.getDeductible()) ){
				enrollmentPlan.setIndivDeductible(Double.valueOf(planResponse.getDeductible()));
			}
			
			if(isNotNullAndEmpty(planResponse.getOopMax()) && NumberUtils.isNumber(planResponse.getOopMax())){
				enrollmentPlan.setIndivOopMax(Double.valueOf(planResponse.getOopMax()));
			}
			
			if(isNotNullAndEmpty(planResponse.getOopMaxFamily()) && NumberUtils.isNumber(planResponse.getOopMaxFamily())){
				enrollmentPlan.setFamilyOopMax(Double.valueOf(planResponse.getOopMaxFamily()));
			}
			
			if(isNotNullAndEmpty(planResponse.getDeductibleFamily()) && NumberUtils.isNumber(planResponse.getDeductibleFamily())){
				enrollmentPlan.setFamilyDeductible(Double.valueOf(planResponse.getDeductibleFamily()));
			}
			
			enrollmentPlan.setGenericMedication(planResponse.getGenericMedications());
			enrollmentPlan.setOfficeVisit(planResponse.getOfficeVisit());
			enrollmentPlan.setNetworkType(planResponse.getNetworkType());
			enrollmentPlan.setPhoneNumber(planResponse.getIssuerPhone());
			enrollmentPlan.setIssuerSiteUrl(planResponse.getIssuerSite());
		}
		enrollment.setEnrollmentPlan(enrollmentPlan);
	}


	/**
	 * updateEnrollmentWithTicketId()
	 * @param enrollment
	 * @return updatedEnrollment
	 */
	@Override
	public Enrollment updateEnrollmentWithTicketId(Enrollment enrollment) {

		return ienrollmentRepository.save(enrollment);
	}

	//  HIX-67133 Remove DB FK Constraint and Model reference for PldOrderItem from Enrollment
	/**
	 * getOrderIdFromIndvOrderItemId()
	 * @param indOrderItemId
	 * @return orderID
	 */
	//	@Override
	//	public Integer getOrderIdFromIndvOrderItemId(int indOrderItemId) {
	//		return ienrollmentRepository.getPldOrderId(indOrderItemId);
	//	}

	/**
	 * Method to get a list of enrollments for a given ssap application ID
	 * @author negi_s
	 * @param ssapApplicationId 
	 * @return List<Enrollment> Returns a list of enrollments
	 */
	@Override
	public List<Enrollment> findEnrollmentBySsapApplicationId(Long ssapApplicationId) {
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_MANUAL);
		return ienrollmentRepository.findEnrollmentBySsapApplicationId(ssapApplicationId, lookupValue.getLookupValueId());
	}
	
	private void updateConsumerApplicationById(Long ssapApplicationId, String enrollmentStatus) {
		
		SsapApplicationDTO ssapApplicationDTO = new SsapApplicationDTO();
		ssapApplicationDTO.setId(ssapApplicationId);
		ssapApplicationDTO.setEnrollmentStatus(enrollmentStatus);
		
		try{
			restTemplate.postForObject(ConsumerServiceEndPoints.UPDATE_CONSUMER_APPLICATION_BY_ID,ssapApplicationDTO,String.class);
		}
		catch(Exception e){
			LOGGER.error("EnrollmentOffExchangeController.updateConsumerApplicationById - Error in updating enrollment status for SSAP Application Id="+ssapApplicationDTO.getId() , e);
		}
	}
	
	private void setEnrollmentCommissionData(Enrollment enrollment, PlanResponse planResponse) {
		if(null != planResponse) {
			EnrollmentIssuerCommision enrlCommission = new EnrollmentIssuerCommision();
			enrlCommission.setInitialCommDollarAmt(planResponse.getFirstYearCommDollarAmt());
			enrlCommission.setInitialCommissionPercent(planResponse.getFirstYearCommPercentageAmt());
			enrlCommission.setRecurringCommDollarAmt(planResponse.getSecondYearCommDollarAmt());
			enrlCommission.setRecurringCommissionPercent(planResponse.getSecondYearCommPercentageAmt());
			enrlCommission.setNonCommissionFlag(planResponse.getNonCommissionFlag());
			if(!EnrollmentConstants.Y.equalsIgnoreCase(enrlCommission.getNonCommissionFlag())) {
				if(null != enrlCommission.getInitialCommDollarAmt()) {
					enrlCommission.setInitialCommissionAmt(enrlCommission.getInitialCommDollarAmt());
				}else if(null != enrlCommission.getInitialCommissionPercent() && null != enrollment.getGrossPremiumAmt()) {
					enrlCommission.setInitialCommissionAmt(EnrollmentUtils.round(enrlCommission.getInitialCommissionPercent() * enrollment.getGrossPremiumAmt(), 2));
				}
				if(null != enrlCommission.getRecurringCommDollarAmt()) {
					enrlCommission.setRecurringCommissionAmt(enrlCommission.getRecurringCommDollarAmt());
				}else if(null != enrlCommission.getRecurringCommissionPercent() && null != enrollment.getGrossPremiumAmt()) {
					enrlCommission.setRecurringCommissionAmt(EnrollmentUtils.round(enrlCommission.getRecurringCommissionPercent() * enrollment.getGrossPremiumAmt(), 2));
				}
			}else {
				enrlCommission.setInitialCommissionAmt(0d);
				enrlCommission.setRecurringCommissionAmt(0d);
			}
			enrlCommission.setEnrollment(enrollment);
			enrollment.setEnrollmentIssuerCommision(enrlCommission);
		}
	}

}
	
