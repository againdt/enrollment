/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.dto.bb.BenefitBayRequestDTO;
import com.getinsured.hix.dto.cap.consumerapp.CapApplicationDTO;
import com.getinsured.hix.dto.cap.consumerapp.CapOnExchangeFFMAppDto;
import com.getinsured.hix.dto.cap.consumerapp.EffectiveApplicationDTO;
import com.getinsured.hix.dto.cap.consumerapp.PolicyLiteDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.enrollment.D2CEnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateHouseholdDTO;
import com.getinsured.hix.enrollment.dto.*;
import com.getinsured.hix.enrollment.dto.EnrollmentStarResponse.EnrollmentRenewalResponse;
import com.getinsured.hix.enrollment.service.carrierfeed.CarrierFeedResultRecord;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EnrollmentRenewals;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.*;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.platform.util.exception.GIException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author panda_p
 * @since 13/02/2013
 */
public interface EnrollmentPrivateService {
	/**
	 * Return Enrollment for provided id
	 * 
	 * @param id
	 * @return Enrollment
	 */
	Enrollment findById(Integer id);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	EnrollmentDTO findbyenrollmentid(Integer id) throws Exception;
	
	/**
	 * Save / Update Enrollment in database
	 * @param enrollment
	 * @return
	 */
	Enrollment saveEnrollment(Enrollment enrollment);
	
	/**
	 * This methode return list of Enrollment which are updated since last carrier update.
	 * 
	 * @return List of Enrollment
	 */
	List<Enrollment> getCarrierUpdatedEnrollments();
	
	/**
	 * 
	 * @param searchCriteria
	 * @return
	 */
	Map<String,Object> searchEnrollment(Map<String,Object> searchCriteria);
	
	
	Map<String,Object> findEnrollmentByAdmin(EnrollmentSearchFilter enrollmentSearchFilter,Long tenantId);
	
	/**
	 * Return List of Enrollment with given Employer
	 * 
	 * @param employer
	 * @return
	 */
	List<Enrollment> findEnrollmentByEmployer(Employer employer);
	
	/**
	 * 
	 * @param employerId
	 * @return
	 */
	List<Object[]> findEnrollmentByEmployerId(int employerId);
	
	/**
	 *
	 * @param inputval
	 * @return
	 */
	List<Object[]> findCurrentMonthEffectiveEnrollment(Map<String, Object> inputval);
	
	/**
	 * 
	 * @param inputval
	 * @return
	 */
	List<Enrollment> findEnrollmentByStatusAndEmployerAndTerminationdate(Map<String, Object> inputval);
	
	/**
	 * 
	 * @param inputval
	 * @return
	 */
	List<Enrollment> findEnrollmentByStatusAndTerminationdate(Map<String, Object> inputval);
	
	/**
	 * This method create enrollment for individual plan selection.
	 * 
	 * @author meher_a
	 * @param pldOrderResponse
	 * @param applicant_esig
	 * @param ssapApplicationId 
	 * @return
	 */
	EnrollmentResponse createEnrollment(PldOrderResponse pldOrderResponse,String applicant_esig, String taxFiler_esign, String cmrHouseholdId, Long ssapApplicationId);
	
	/**
	 * Change status of Enrollment from given Employer to Confirmed
	 * 
	 * @author meher_a, raja
	 * @param EnrollmentStarRequest
	 */
	void updateShopEnrollmentStatus(EnrollmentRequest enrollmentRequest);
	
	
	//	HIX-67133 Remove DB FK Constraint and Model reference for PldOrderItem from Enrollment
	/**
	 * Return Enrollment with indOrderItemId
	 * @param indOrderItemId
	 * @return Enrollment List
	 */
	//	List<Enrollment> findByIndOrderItemId(Integer indOrderItemId);
	
	/**
	 * Finds Enrollment with given "issuerSubscriberIdentifier" and Update with Map valus 
	 * 
	 * @author meher_a
	 * @param issuerSubscriberIdentifier
	 * @param values
	 */
	void updateEnrollment (String issuerSubscriberIdentifier, Map<String, Object> values);
	
	
	
	/**
	 * Return Issuer with given "insurerTaxIdNumber"
	 * 
	 * @param insurerTaxIdNumber
	 * @return
	 */
//	Issuer getIssuerByinsurerTaxIdNumber(String insurerTaxIdNumber);
	
	/**
	 * 
	 * @param eventID
	 * @param planID
	 * @param memberID
	 * @param effEndDate
	 * @param status
	 * @param maintReasonCode
	 * @throws Exception
	 */
	void disEnrollByEventID( Integer eventID, Integer planID, String memberID, Date effEndDate, String status, String maintReasonCode)  throws GIException ;
	
	/**
	 * 
	 * @param planName
	 * @param planLevel
	 * @param planMarket
	 * @param regionName
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Enrollment> findEnrollmentsByPlan(String planId, String planLevel, String planMarket, String regionName, String startDate, String endDate, String status);
	
	/**
	 * 
	 * @param issuerName
	 * @param planLevel
	 * @param planMarket
	 * @param regionName
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Enrollment> findEnrollmentsByIssuer(String issuerName, String planLevel, String planMarket, String regionName, String startDate, String endDate, String status);
	
	
	
	/**
	 * Used by Disenrollment IND-56 
	 * 
	 * @param disEnrollmentMap
	 * @throws GIException
	 * @throws Exception
	 */
	String disEnrollByEnrollmentID(Map<String,Object> disEnrollmentMap) throws GIException;
	
	/**
	 * Used bi IND-29
	 * @param externalID
	 * @return Enrollment List
	 */
	List<Enrollment> findEnrollmentByExternalID(String externalID);

	
	
	
	
	
	/**
	 * @author panda_p
	 * @since 10 MAY 2013
	 *
	 * This methods intercepts pldOrderResponse and creates the special enrollment 
	 * 
	 * @param pldOrderResponse
	 * @param applicant_esig
	 * @return it returns Enrollment response with list of enrollment, disEnrollment done on Success and returns error message and code in case of failure.
	 */
	EnrollmentResponse createSpecialEnrollment(
			PldOrderResponse pldOrderResponse, String applicant_esig, String taxFiler_esign, String cmrHouseholdId);
	
	/**
	 * @author raja
	 * @since 28 MAY 2013
	 *
	 * This methods takes the input as enrollment id and  returns the planId and ind_order_items_id for the enrollment 
	 * 
	 * @param enrollmentId
	 * @return it returns Map<String,Object>.
	 */
	Map<String,Object> getPlanIdAndOrderItemIdByEnrollmentId(Integer enrollmentId);
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to create event for a particular Enrollee
	 * @param enrollee
	 * @param eventType
	 * @param eventReasonCode
	 * @param user
	 */
	void createEventForEnrollee(Enrollee enrollee,String eventType ,String eventReasonCode, AccountUser user);
	
	/**
	 * @author parhi_s
	 * @since
	 * 
	 * This method is used to update the status of the Enrollment when any enrollee status gets changed
	 * 
	 * @param enrollment
	 * @param user
	 */
	void checkEnrollmentStatus(Enrollment enrollment, AccountUser user);
	
	/**
	 * 
	 * @param userName
	 * @return
	 */
	AccountUser getUserByName(String userName);
	
	/**
	 *  @author aditya_s
	 *  @since 22/07/2013
	 *  This method returns list of {@link EnrollmentCountDTO} for given assisterBrokerID, role, startDate and endDate.
	 * 
	 * @param assisterBrokerId
	 * @param role
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<EnrollmentCountDTO> findEnrollmentPlanLevelCountByBroker(Integer assisterBrokerId, String role, Date startDate, Date endDate);
	
	List<Employer> findDistinctEmployer();
	
	/**
	 * Used to abort created enrollments.
	 */
	void abortEnrollment(Enrollment enrollment);
	
	/**
	 * 
	 * @author Sharma_k
	 * @since 25th August 2013
	 * @param enrollmentId
	 * @return
	 */
	boolean isEnrollmentExists(int enrollmentId);

	/**
	 * Returns list of Enrollment for provided CMR_Household_Id.
	 * @param cmrHouseholdId
	 * @return
	 */
	List<Enrollment> findEnrollmentByCmrHouseholdId(int cmrHouseholdId);
	
	/**
	 * @author Sharma_k
	 * @since 27th September 2013
	 * @param enrollmentId
	 * @return
	 * service call written to fetch remittance related data.
	 */
	List<Object[]> getRemittanceDataByEnrollmentId(Integer enrollmentId);
	
	/**
	 * Returns the active Enrollments for a particular Employee
	 * @author parhi_s
	 * @since
	 * 
	 * @param employeeId
	 * @return
	 */
	List<Enrollment> findActiveEnrollmentByEmployeeID(Integer employeeId);
	
	/**
	 * Returns EnrollmentPlan for given plan id
	 * @param planId
	 * @return
	 */
	EnrollmentPlan findEnrollmentPlanByPlanId(Integer planId);
	
	/**@author Pratap
	 * @since 22/10/2013
	 * @param enrollmentResponse
	 * @return String
	 */
	BenefitBayRequestDTO populateBenefitBayRequestDTO(EnrollmentResponse enrollmentResponse);
	
	/**
	 *  @author Priya C
	 *  @since 01/21/2014
	 *  This method returns the Enrollment for the given cmr_household_id , exchange_type and Enrollment status
	 * 
	 * @param assisterBrokerId
	 * @param role
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	Enrollment findManualEnrollmentByCmrHouseholdId(Integer cmrHouseholdId);
	
	/**
	 * @author root
	 * @since 03/07/2014
	 * Returns latest enrollment record in pending status if exists
	 * @param cmrHouseholdId
	 * @return
	 */
	Enrollment findLatestPendingEnrollmentByCmrHouseholdId(Integer cmrHouseholdId);
	
	/**
	 * Returns list of Enrollment for a given ssapApplicationId.
	 * @param ssapApplicationId
	 * @return
	 */
	List<Enrollment> findEnrollmentBySsapApplicationId(long ssapApplicationId);
	
	/**
	 * Returns an Enrollment for a given appID.
	 * @param carrierAppId
	 * @return Enrollment
	 */
	Enrollment findEnrollmentByCarrierAppId(String carrierAppId);
	
	/* HIX-60785 */
	List<Enrollment> findEnrollmentByCarrierAppId(String carrierAppId, String issuerName, String productType);
	
	List<Enrollment> findEnrollmentByCarrierUID(String carrierAppUID, String issuerName, String productType);
	
	List<Enrollment> findEnrollmentByissuerPolicyId(String policyId, String issuerName, String productType);
	
	
	/**
	 * Returns a list of Enrollments for a given first and last name.
	 * @param firstName
	 * @param lastName
	 * @return List<Enrollment>
	 */
	List<Object[]> findEnrollmentAndEnrolleeDataByName(String firstName , String lastName);
	
	/**
	 * Returns a list of Enrollments for a given first and last name.
	 * @param firstName
	 * @param lastName
	 * @return List<Enrollment, HH>
	 */
	List<Object[]> findByFirstNameAndLastNameLowercase(String firstName , String lastName);
		
	
	/**
	 * Updates the status of the enrollment given the enrollment Id and app Id.
	 * @param enrollmentId
	 * @param carrierAppId
	 * @param newStatus the lookup value code
	 * @return Enrollment updated Enrollment
	 */
	Enrollment updateEnrollmentStatusById(CarrierFeedResultRecord result);
	
	/**
	 * 
	 * @param d2CEnrollmentRequest
	 * @return EnrollmentResponse
	 */
	EnrollmentResponse createD2CEnrollmentWithEnrollees(D2CEnrollmentRequest d2CEnrollmentRequest);
	
	/**
	 * 
	 * @param EnrollmentUpdateHouseholdDTO
	 * @return EnrollmentUpdateHouseholdDTO
	 */
	EnrollmentUpdateHouseholdDTO updateEnrollmentCmrHouseholdId(EnrollmentUpdateHouseholdDTO requestDTO);
	
	/**
	 * 
	 * @param d2CEnrollmentRequest
	 * @return EnrollmentResponse
	 */
	EnrollmentResponse updateD2CEnrollmentStatus(D2CEnrollmentRequest d2CEnrollmentRequest);
	
	List<Enrollment> findEnrollmentByCarrierFeedDetails(Map<String, Object> inputval);
	
	/**
	 * 
	 * @param enrollmentId
	 * @param enrlStatus
	 * @param strTerminationDate 
	 * @return
	 */
	
	Enrollment updateEnrollment(Integer enrollmentId, String enrlStatus, String strTerminationDate, String strDateClosed);
	Enrollment findEnrollmentByCarrierUID(String carrierAppUID);
	Enrollment findEnrollmentByissuerPolicyId(String policyId);

	void saveEnrollmentCommission(Enrollment enrollment,String commissionAmt,String commissionDate);

	// Added to get the latest NON E-COMMITTED enrollment record to show up in 
	// consumer screen after updating enrollment status 
	Enrollment findLatestNonEcommittedEnrollmentByCmrHouseholdId(Integer cmrHouseholdId);

    List<Enrollment> findEnrollmentById(List<Integer> ids);

    List<Enrollment> findEnrollmentIdByEnrollmentStatusLkp_lookupValueCodeAndCreatedOnBefore(List<String> lookupValueCodes,
                                                                                             Date createdOn);

	List<PolicyLiteDTO> getEnrollmentListByHouseholdId(int householdId);

	boolean saveManualAppEnrolleeInfo(List<SsapApplicantDTO> applicantDtoList,
			Enrollment savedEnrollment);

	int findCountByCmrIdAndPlanTypeForManual(String cmrId, String appType);
	CapApplicationDTO findBySsapApplicationId(long ssapApplicationId);
	boolean saveFfmAppData(CapOnExchangeFFMAppDto capFfmAppDto);

	Enrollment updateEnrollmentDetails(EffectiveApplicationDTO effectiveApplicationDTO, Enrollment enrollment, boolean isSubmitApplication);

	boolean saveManualAppEnrolleeInfo(List<SsapApplicantDTO> applicantDTOList,
			Enrollment enrollment, boolean pendingStatusOnly);

	List<Enrollment> findEnrollmentByStatus(String status);

	BigDecimal countEnrollmentsByAffiliateId(Long affiliateId);
	
    EnrollmentResponse getEnrollmentCapInfo(String enrollmentId);
    
    /**
     * Publishes partner event via the affiliate event notification url
     * @param enrollmentId
     * @param householdId
     */
    void publishPartnerEvent(Integer enrollmentId, Integer householdId);
    
    /**Get Household for the given householdId
     * @param householdId
     */
    Household getHousehold(Integer householdId)throws GIException;
    
    /**
     * 
     * @param enrollmentBobRequest Object<EnrollmentBobRequest>
     * @throws GIException
     */
    void updateEnrollmentForBOBSuccess(EnrollmentBobRequest enrollmentBobRequest)throws GIException;
    
    /**
     * 
     * @param enrollmentBobRequest Object<EnrollmentBobRequest>
     * @param enrollmentBobResponse Object<EnrollmentBobResponse>
     */
    void searchEnrollmentForBOBRequest(EnrollmentBobRequest enrollmentBobRequest, EnrollmentBobResponse enrollmentBobResponse);
    
    /**
     * 
     * @param enrollmentIDList
     * @return
     */
    List<EnrollmentSubscriberDetailsDTO> getEnrollmentSubscriberDetails(List<Integer> enrollmentIDList);
    
    /**
     * 
     * @param policyNumber
     * @return
     */
    List<EnrollmentSubscriberDetailsDTO> findEnrollmentByPolicyNumberHICN(EnrollmentStarRequest enrollmentStarRequest) throws GIException;
    
    /**
     * 
     * @param carrierName
     * @param enrollmentStatusList
     * @return
     */
    List<EnrollmentBobDTO> findBobExpectedEnrollmentRecord(String carrierName, List<String> enrollmentStatusList);
    
    /**
     * 
     * @param enrollmentCommisionRequest
     * @return
     */
    EnrollmentCommission saveEnrollmentCommision(EnrollmentCommissionRequest enrollmentCommissionRequest)throws GIException;
    
    /**
     * 
     * @param enrollmentId
     * @param EnrollmentStatus
     * @param enrollmentRenewalDTOList
     * @param updatedBy
     * @return
     * @throws GIException
     */
    EnrollmentRenewalResponse processSingleAndRollOverRowRenewal(Integer enrollmentId, String EnrollmentStatus, List<EnrollmentRenewalDTO> enrollmentRenewalDTOList, AccountUser updatedBy, String renewalType)throws GIException;
    
    
    /**
     * @param enrlMissingCommissionRequest
     * @return EnrlMissingCommissionResponse
     * @throws GIException
     */
    EnrlMissingCommissionResponse getMissingCommissions(EnrlMissingCommissionRequest enrlMissingCommissionRequest) throws GIException;
    
    /**
     * 		
     * @param enrollmentId
     * @return
     */
    public EnrollmentRenewals findByEnrollmentIdWithLatestBenefitStartDate(Integer enrollmentId);

	void updateEnrollmentForFeed(EnrollmentUpdateDTO enrollmentUpdateDTO) throws GIException;
    
	/**
	 * 
	 * @param enrollmentId
	 * @return
	 */
	Integer getEnrollmentStatusNSubmittedInfoById(Integer enrollmentId);
	
	/**
	 * 
	 * @param enrollmentRequest
	 * @return
	 */
	List<EnrolleeDTO> getEnrolleeAtributeById(EnrollmentRequest enrollmentRequest) throws GIException;

	Set<Integer> setMatchedIssuerIds(Set<Integer> issuerIds, String carrierName);

	void searchEnrollmentForBOBMultilineRequest(EnrollmentBobRequest enrollmentBobRequest, EnrollmentBobResponse enrollmentBobResponse);

	Map<Integer,String> getLookupValueIdsByNames(String lookupTypeName, List<String> lookupValueCodes);

	void updateEnrollmentRenewalsForFeed(List<EnrollmentRenewalDTO> enrollmentRenewalDTOs, Integer enrollmentId);
}
