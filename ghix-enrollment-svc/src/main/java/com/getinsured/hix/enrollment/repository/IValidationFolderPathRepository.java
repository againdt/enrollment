package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ExchgPartnerLookup;


public interface IValidationFolderPathRepository  extends JpaRepository<ExchgPartnerLookup, Integer> {
	
	@Query("FROM ExchgPartnerLookup expl WHERE (expl.hiosIssuerId = :hiosIssuerID AND expl.isa06 = :isa06 AND expl.isa05 = :isa05  AND expl.isa07 = :isa05  AND expl.st01 = '834' AND expl.gs08 = :gs08 AND expl.market = :market AND expl.direction = :direction)")
	ExchgPartnerLookup getDropOffLocatonFor834Outbound(@Param("hiosIssuerID") String hiosIssuerID,@Param("isa06") String isa06,@Param("isa05") String isa05,@Param("gs08") String gs08,@Param("market") String market,@Param("direction") String direction);
	
	@Query("FROM ExchgPartnerLookup expl WHERE (expl.hiosIssuerId = :hiosIssuerID AND expl.st01 = '834'AND expl.market = :market AND expl.direction = :direction)")
	ExchgPartnerLookup findExchgPartnerByHiosIssuerID(@Param("hiosIssuerID") String hiosIssuerID,@Param("market") String market,@Param("direction") String direction);
	
	@Query("FROM ExchgPartnerLookup expl WHERE (expl.hiosIssuerId = :hiosIssuerID AND expl.st01 = 'PAS'AND expl.market = :market AND expl.direction = :direction)")
	ExchgPartnerLookup findExchgPartnerforGroupInstallByHiosIssuerID(@Param("hiosIssuerID") String hiosIssuerID,@Param("market") String market,@Param("direction") String direction);
	
	@Query("FROM ExchgPartnerLookup expl WHERE (expl.hiosIssuerId = :hiosIssuerID AND expl.isa06 = :isa06 AND expl.isa05 = :isa05  AND expl.isa07 = :isa05  AND expl.st01 = '834' AND expl.gs08 = :gs08 AND expl.market = :market AND expl.direction =:direction)")
	ExchgPartnerLookup getPickUpLocatonFor834Inbound(@Param("hiosIssuerID") String hiosIssuerID,@Param("isa06") String isa06,@Param("isa05") String isa05,@Param("gs08") String gs08,@Param("market") String market,@Param("direction") String direction);
	
	@Query("FROM ExchgPartnerLookup expl WHERE (expl.hiosIssuerId = :hiosIssuerID AND expl.isa06 = :isa06 AND expl.isa05 = :isa05  AND expl.isa07 = :isa05  AND expl.st01 = 'TA1' AND expl.gs08 = :gs08 AND expl.market = :market AND expl.direction =:direction)")
	ExchgPartnerLookup getDropOffLocatonForTA1Inbound834(@Param("hiosIssuerID") String hiosIssuerID,@Param("isa06") String isa06,@Param("isa05") String isa05,@Param("gs08") String gs08,@Param("market") String market,@Param("direction") String direction);
    @Query("FROM ExchgPartnerLookup expl WHERE (expl.hiosIssuerId = :hiosIssuerID AND expl.isa06 = :isa06 AND expl.isa05 = :isa05  AND expl.isa07 = :isa05  AND expl.st01 = '999' AND expl.gs08 = :gs08 AND expl.market = :market AND expl.direction =:direction)")
	ExchgPartnerLookup getDropOffLocatonFor999Inbound834(@Param("hiosIssuerID") String hiosIssuerID,@Param("isa06") String isa06,@Param("isa05") String isa05,@Param("gs08") String gs08,@Param("market") String market,@Param("direction") String direction);
	
    @Query("FROM ExchgPartnerLookup expl WHERE (expl.direction = :direction AND expl.st01 = :st01)")
	List<ExchgPartnerLookup> findAllHiosIssuerIdForInbound(@Param("direction") String direction,@Param("st01") String st01);
    
    @Query("FROM ExchgPartnerLookup expl WHERE (expl.hiosIssuerId = :hiosIssuerId)")
	List<ExchgPartnerLookup> findByHiosIssuerId(@Param("hiosIssuerId") String hiosIssuerId);
	
			
}
