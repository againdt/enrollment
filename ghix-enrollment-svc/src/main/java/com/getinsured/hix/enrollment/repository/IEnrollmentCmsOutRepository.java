package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.EnrollmentCmsOut;


public interface IEnrollmentCmsOutRepository  extends JpaRepository<EnrollmentCmsOut, Integer>{
	@Query(" SELECT fileName FROM EnrollmentCmsOut WHERE fileId = :sourceFileId")
	String getOutFileName(@Param("sourceFileId") String sourceFileId);
	
	@Query(" SELECT hiosIssuerId FROM EnrollmentCmsOut WHERE fileId = :sourceFileId")
	String getHiosIssuerId(@Param("sourceFileId") String sourceFileId);
	
 /*	@Query(" FROM EnrollmentCmsOut WHERE fileName = :OutFileName")
	EnrollmentCmsOut getEnrollmentCmsOutByFileName(@Param("OutFileName") String OutFileName);*/
	
	/*@Modifying
	@Transactional
	@Audited
	@Query("UPDATE EnrollmentCmsOut enrlCmsOut "
			+ " SET enrlCmsOut.inboundStatus = :inboundStatus, "
			+ " enrlCmsOut.sbmsSbmrRcvd=:sbmsSbmrRcvd, "
			+ " enrlCmsOut.inboundAction=:inboundAction "
			+ " WHERE enrlCmsOut.fileName = :OutFileName ")
	void updateEnrollmentCmsOutByFileName(@Param("OutFileName") String OutFileName, @Param("inboundStatus") String inboundStatus, 
			                                    @Param("inboundAction") String inboundAction ,@Param("sbmsSbmrRcvd")String sbmsSbmrRcvd);*/
	
	@Query(" FROM EnrollmentCmsOut WHERE fileId = :sourceFileId")
	EnrollmentCmsOut getEnrollmentCmsOutBySourceFileId(@Param("sourceFileId") String sourceFileId);
	
	@Query("SELECT DISTINCT ec.hiosIssuerId FROM EnrollmentCmsOut as ec WHERE ec.year = :year AND ec.month = :month")
	List<String> findDistinctHiosIssuerIdsByYearAndMonth(@Param("year") Integer year, @Param("month") Integer month);

	@Query("SELECT DISTINCT ec.hiosIssuerId FROM EnrollmentCmsOut as ec WHERE ec.year = :year AND ec.month = :month AND (ec.inboundAction is NULL OR ec.inboundAction IN ('REGEN','INACTIVE'))")
	List<String> findHiosIssuerIdsWithRegen(@Param("year") Integer year, @Param("month") Integer month);
	
	@Query("SELECT ec.inboundAction FROM EnrollmentCmsOut as ec where ec.createdOn = "
			+ "(SELECT MAX(eco.createdOn) FROM EnrollmentCmsOut as eco WHERE eco.hiosIssuerId = :hiosIssuerId "
			+ "AND eco.year = :year AND eco.month = :month) "
			+ "AND ec.hiosIssuerId = :hiosIssuerId ")
	String getMaxRecordForHiosIssuerIdByYearAndMonth(@Param("hiosIssuerId") String hiosIssuerId, @Param("year") Integer year, @Param("month") Integer month);

	@Query("FROM EnrollmentCmsOut as ec WHERE ec.fileName = :fileName AND ec.batchExecutionId =:batchExecutionId")
	EnrollmentCmsOut findByNameAndBatchExecutionId(@Param("fileName") String fileName, @Param("batchExecutionId") Long batchExecutionId);
	
	@Query("FROM EnrollmentCmsOut as ec WHERE ec.fileName = :fileName")
	EnrollmentCmsOut findByFileName(@Param("fileName") String fileName);
	
	@Query("FROM EnrollmentCmsOut as ec WHERE ec.batchExecutionId = :jobId AND ec.fileName is NOT NULL")
	List<EnrollmentCmsOut> getEnrollmentCmsOutByJobId(@Param("jobId") Long jobId);
	
	@Modifying
	@Transactional	
	@Query("DELETE FROM EnrollmentCmsOut as ec WHERE  ec.batchExecutionId = :jobId")
	void deleteEnrollmentCmsOutByJobId(@Param("jobId") Long jobId );
}
