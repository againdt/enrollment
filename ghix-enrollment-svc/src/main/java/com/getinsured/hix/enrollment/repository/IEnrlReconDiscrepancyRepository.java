package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.dto.enrollment.DiscrepancyDetailDTO;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy.DiscrepancyStatus;

/**
 * 
 * @author sharma_k
 *
 */
public interface IEnrlReconDiscrepancyRepository extends JpaRepository<EnrlReconDiscrepancy, Integer>{
	
	/**
	 * 
	 * @param monthNum
	 * @param year
	 * @param hiosIssuerIdList
	 * @return
	 */
	@Query("SELECT COUNT(*), enrlLkp.code, enrlLkp.label,  enrlLkp.category"
			+ " FROM EnrlReconDiscrepancy as erd, EnrlReconSummary as ers, EnrlReconLkp as enrlLkp "
			+ " WHERE erd.fileId = ers.id "
			+ " AND erd.enrlReconLkpId = enrlLkp.id "
			+ " AND ers.month = :monthNum AND ers.year = :year AND ers.hiosIssuerId IN (:hiosIssuerIdList) "
			+ " AND ers.status IN ('Autofixed', 'Completed') "
			+ " AND ers.status NOT IN ('Duplicate')"
			+ " GROUP BY enrlLkp.code, enrlLkp.label, enrlLkp.category ORDER BY COUNT(*) DESC")
	public List<Object[]> getTopDiscrepencies(@Param("monthNum") Integer monthNum, @Param("year") Integer year, @Param("hiosIssuerIdList") List<String> hiosIssuerIdList);
	
	/**
	 * 
	 * @param monthNum
	 * @param year
	 * @param hiosIssuerIdList
	 * @return
	 */
	@Query("SELECT COUNT(DISTINCT erd.id)"
			+ " FROM EnrlReconDiscrepancy as erd, EnrlReconSummary as ers "
			+ " WHERE erd.fileId = ers.id "
			+ "	AND ers.hiosIssuerId IN (:hiosIssuerIdList) "
			+ " AND ers.month = :monthNum AND ers.year = :year"
			+ " AND ers.status NOT IN ('Duplicate')")
	public long getSumOfTotalDiscrepency(@Param("monthNum") Integer monthNum, @Param("year") Integer year, @Param("hiosIssuerIdList") List<String> hiosIssuerIdList);
	
	/**
	 * 
	 * @param newDiscrepancyLkpList
	 * @param hixEnrollmentId
	 * @param statuses
	 * @return
	 */
	@Query(" FROM EnrlReconDiscrepancy as erd WHERE erd.enrlReconLkpId.id not in (:newDiscrepancyLkpList)"
			+ " and erd.hixEnrollmentId = :hixEnrollmentId "
			+ " and erd.status in (:discrepancyStatusList) ")
	List<EnrlReconDiscrepancy> findByDiscrepancyLkpIdsAndHixEnrllmentIdAndStatus(@Param("newDiscrepancyLkpList") List<Integer> newDiscrepancyLkpList,
																		@Param("hixEnrollmentId") Long hixEnrollmentId,
																		@Param("discrepancyStatusList") List<DiscrepancyStatus> discrepancyStatusList);
	
	
	/**
	 * 
	 * @param fileId
	 * @param discrepancyCodeList
	 * @param discrepancyStatusList
	 * @return
	 */
	
	@Query(" FROM EnrlReconDiscrepancy as erd WHERE erd.enrlReconLkpId.code in (:discrepancyCodeList)"
			+ " and erd.fileId = :fileId"
			+ " and erd.status in (:discrepancyStatusList) ")
	List<EnrlReconDiscrepancy> getDiscrepancyByDiscrepancyLkpAndFileId(@Param("fileId") Integer fileId,@Param("discrepancyCodeList") List<String> discrepancyCodeList,  @Param("discrepancyStatusList") List<DiscrepancyStatus> discrepancyStatusList);

	/**
	 * 
	 * @param hixEnrollmentId
	 * @param statuses
	 * @return
	 */
	@Query(" FROM EnrlReconDiscrepancy as erd WHERE erd.hixEnrollmentId = :hixEnrollmentId  and erd.status in (:discrepancyStatusList) ")
	List<EnrlReconDiscrepancy> findByHixEnrllmentIdAndStatus(@Param("hixEnrollmentId") Long hixEnrollmentId,
															 @Param("discrepancyStatusList") List<DiscrepancyStatus> discrepancyStatusList);
	
	/**
	 * 
	 * @param fileId
	 * @return
	 */
	@Query("SELECT COUNT(erd.id) FROM EnrlReconDiscrepancy as erd  WHERE erd.fileId = :fileId ")
	long findDiscrepancyCountByFileId(@Param("fileId") Integer fileId);
	
	@Query("FROM EnrlReconDiscrepancy as erd  WHERE erd.fileId = :fileId ORDER BY erd.hixEnrollmentId ASC")
	List<EnrlReconDiscrepancy> getDiscrepanciesByFileId(@Param("fileId") Integer fileId);

	@Query("FROM EnrlReconDiscrepancy as erd  WHERE erd.fileId = :fileId AND erd.reportSentStatus = 'Y'")
	List<EnrlReconDiscrepancy> getReportedDiscrepanciesByFileId(@Param("fileId") Integer fileId);
	
	@Query("SELECT erd.status, COUNT(*) FROM EnrlReconDiscrepancy erd, EnrlReconSummary ers WHERE erd.fileId = ers.id"
			+ " AND erd.hixEnrollmentId = :hixEnrollmentId"
			+ " AND ers.status NOT IN ('Duplicate') "
			+ " GROUP BY erd.status")
	List<Object[]> getDiscrepancyCountByEnrollmentId(@Param("hixEnrollmentId") Long hixEnrollmentId);
	
	@Query("SELECT new com.getinsured.hix.dto.enrollment.DiscrepancyDetailDTO( "
			+ " erd.memberId, lkp.code, lkp.category, erd.hixEnrollmentId, lkp.fieldName, erd.issuerEnrollmentId, ers.fileName,"
			+ " erd.hixValue, erd.issuerValue) "
			+ " FROM EnrlReconDiscrepancy erd, EnrlReconLkp lkp, EnrlReconSummary ers WHERE erd.enrlReconLkpId = lkp.id "
			+ " AND erd.fileId = ers.id"
			+ " AND ers.status NOT IN ('Duplicate')"
			+ " AND erd.hixEnrollmentId = :hixEnrollmentId AND erd.status in (:discrepancyStatusList) ORDER BY lkp.category ASC, lkp.code ASC")
	List<DiscrepancyDetailDTO> getDiscrepancyDetailByEnrollmentIDAndStatus(@Param("hixEnrollmentId") Long hixEnrollmentId,
			 @Param("discrepancyStatusList") List<DiscrepancyStatus> discrepancyStatusList);
	
	/**
	 * 
	 * @param discrepancyCodeList
	 * @param discrepancyStatusList
	 * @param enrollmentId
	 * @return
	 */
	@Query(" FROM EnrlReconDiscrepancy as erd WHERE erd.enrlReconLkpId.code in (:discrepancyCodeList)"
			+ " and erd.hixEnrollmentId = :enrollmentId and erd.status in (:discrepancyStatusList) ")
	List<EnrlReconDiscrepancy> getDiscrepancyByDiscrepancyLkpAndEnrollmentId(
			@Param("discrepancyCodeList") List<String> discrepancyCodeList,
			@Param("discrepancyStatusList") List<DiscrepancyStatus> discrepancyStatusList,
			@Param("enrollmentId") Long enrollmentId);
}
