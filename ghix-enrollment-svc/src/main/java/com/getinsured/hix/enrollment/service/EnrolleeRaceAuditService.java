/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.text.ParseException;
import java.util.Date;

import org.springframework.data.history.Revision;


import com.getinsured.hix.model.enrollment.EnrolleeRace;

/**
 * @author liu_j
 * @since 23/04/2013
 */
public interface EnrolleeRaceAuditService {
	Revision<Integer, EnrolleeRace> findRaceRevisionByDate( int enrolleeRaceId, Date selectDate) throws ParseException;

}
