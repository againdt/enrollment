/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.enrollment.EnrolleeUpdateSendStatus;


/**
 * @author panda_p
 *
 */
public interface IEnrolleeUpdateSendStatusPrivateRepository extends JpaRepository<EnrolleeUpdateSendStatus, Integer> {

}
