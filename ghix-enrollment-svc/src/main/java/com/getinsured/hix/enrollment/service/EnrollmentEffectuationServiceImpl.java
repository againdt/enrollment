package com.getinsured.hix.enrollment.service;

import org.springframework.stereotype.Service;
/**
 * @author meher_a
 *
 */

@Service("enrollmenteffectuationservice")
public class EnrollmentEffectuationServiceImpl implements EnrollmentEffectuationService{

	@Override
	public void effectuateEnrollment() {
		
	}

}
