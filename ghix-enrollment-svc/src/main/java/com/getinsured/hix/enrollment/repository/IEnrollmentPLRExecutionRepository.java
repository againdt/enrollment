package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentPLRExecution;

public interface IEnrollmentPLRExecutionRepository extends JpaRepository<EnrollmentPLRExecution, Integer> {

	/*@Query("FROM EnrollmentPLRExecution as epe WHERE epe.enrollment1095.houseHoldCaseId = :houseHoldCaseId AND TO_DATE(TO_CHAR(epe.enrollment1095.policyStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(TO_CHAR(:startDate,'MM/DD/YYYY'), 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(epe.enrollment1095.policyStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE( TO_CHAR(:endDate,'MM/DD/YYYY'), 'MM/DD/YYYY')  AND epe.enrollment1095.isActive='Y' ")
	List<EnrollmentPLRExecution> getByHouseholdCaseId(@Param("houseHoldCaseId") String houseHoldCaseId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);*/
	
	@Query("FROM EnrollmentPLRExecution as epe WHERE epe.enrollment1095 IN (:enrollment1095Ids) ")
	List<EnrollmentPLRExecution> getByEnrollment1095Id(@Param("enrollment1095Ids") List<Integer> enrollment1095Ids);
}
