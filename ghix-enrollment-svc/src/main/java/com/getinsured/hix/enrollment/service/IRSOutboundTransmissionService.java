package com.getinsured.hix.enrollment.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.IRSOutboundTransmission;;



/**
 * Return List of Enrollment with given Employer
 * 
 * @param employer
 * @return
 */
public interface IRSOutboundTransmissionService {
	
	List<IRSOutboundTransmission> findIRSOutboundTransmissionByBatchId(String batchId);
	IRSOutboundTransmission findIRSOutboundTransmissionByDocumentSeqId(String documentSeqId,String batchId);
}

