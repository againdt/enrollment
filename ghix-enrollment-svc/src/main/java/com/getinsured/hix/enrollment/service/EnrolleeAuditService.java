/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.data.history.Revision;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeAud;



/**
 * @author liu_j
 * @since 23/04/2013
 */
public interface EnrolleeAuditService {
	Revision<Integer, Enrollee> findRevisionByDate( int enrolleeId, Date selectDate) throws ParseException;
	
	List<Integer> getEnrolleeIdsForCarrierUpdate(Date lastRunDate, AccountUser user);
	
	List<EnrolleeAud> getEnrolleeAudForCarriersUpdatedEnrollments(Date lastRunDate, AccountUser user,Integer enrolleeID);
	
	/**
	 * 
	 * @param enrollmentId
	 * @param user
	 * @return
	 */
	List<Integer> getEnrolleeAudIdForResendInd21(Integer enrollmentId, AccountUser user);
	
	/**
	 * 
	 * @param enrollmentId
	 * @param user
	 * @param enrolleeAudId
	 * @return
	 */
	List<EnrolleeAud> getEnrolleeAudForCarrierUpdatedEnrollmentsByEnrolleeId(Integer enrollmentId, AccountUser user, Integer enrolleeAudId);


}
