package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.LocationAud;

public interface IEnrollmentLocationRepository extends JpaRepository<LocationAud, Integer>{
/*	@Query("FROM LocationAud as ln WHERE ln.id = :locationId and ln.rev = (select max(rev) from LocationAud where id=:locationId and rev<=:rev)")
	LocationAud getLocationAudByIdAndRev(
			@Param("locationId") Integer locationId,
			@Param("rev") Integer rev);*/
}