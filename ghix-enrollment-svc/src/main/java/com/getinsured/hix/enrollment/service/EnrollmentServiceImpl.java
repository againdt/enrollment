package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.getinsured.hix.dto.bb.BenefitBayRequestDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeDataDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeDataUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeUpdateDto;
import com.getinsured.hix.dto.enrollment.Enrollment834ResendDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentAptcUpdateDto;
import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCoverageValidationRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentCoverageValidationRequest.InsuranceType;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentEventHistoryDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMailingAddressUpdateRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberCoverageDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMembersDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPremiumDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentStatusUpdateDto;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.hix.dto.enrollment.MonthlyAPTCAmountDTO;
import com.getinsured.hix.dto.enrollment.ShopEnrollmentLogDto;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.shop.EmployeeCoverageDTO;
import com.getinsured.hix.dto.shop.EmployerCoverageDTO;
import com.getinsured.hix.enrollment.repository.IEnrlHouseholdRepository;
import com.getinsured.hix.enrollment.repository.IEnrlLocationRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPlanRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.repository.IGroupInstallationRepository;
import com.getinsured.hix.enrollment.repository.IQhpReportRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.Plan.PlanLevelDental;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentDTO;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentMemberDTO;
import com.getinsured.hix.model.enrollment.EnrollmentPlan;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.GroupInstallation;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.PlanDisplayConfiguration;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.request.ssap.SsapApplicationRequest;
import com.getinsured.iex.request.ssap.SsapApplicationResponse;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

/**
 * @author panda_p
 * @since 13/02/2013
 * 
 */

@Service("enrollmentService")
public class EnrollmentServiceImpl implements EnrollmentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentServiceImpl.class);

	@Autowired
	private IEnrollmentRepository enrollmentRepository;
	@Autowired
	private IEnrolleeRepository enrolleeRepository;
	@Autowired
	private IEnrollmentPlanRepository enrollmentPlanRepository;
	@Autowired
	private GhixRestTemplate restTemplate;
	@SuppressWarnings("rawtypes")
	@Autowired
	private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private BatchJobExecutionService batchJobExecutionService;
	@Autowired
	private UserService userService;
	@Autowired
	private EnrolleeService enrolleeService;
	@Autowired
	private EnrollmentEventService enrollmentEventService;
	@Autowired
	private EnrollmentEmailNotificationService enrollmentEmailNotificationService;
	@Autowired
	private EnrollmentAsyncService enrollmentAsyncService;
	@Autowired
	private IGroupInstallationRepository groupInstallationRepository;
	@Autowired
	private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired
	private IEnrollmentEventAudRepository enrollmentEventAudRepository;
	@Autowired
	private IEnrollmentEventRepository enrollmentEventRepository;
	@Autowired
	private IEnrlLocationRepository locationRepository;
	@Autowired
	private IEnrollmentPremiumRepository enrollmentPremiumRepository;
	@Autowired
	private EnrollmentRequotingService enrollmentRequotingService;
	@Autowired
	private EnrollmentPremiumService enrollmentPremiumService;
	@Autowired
	private IEnrolleeRelationshipRepository iEnrolleeRelationshipRepository;
	@Autowired
	private Gson platformGson;
	@Autowired
	private IEnrolleeAudRepository iEnrolleeAudRepository;
	@Autowired
	private IEnrollmentAudRepository iEnrollmentAudRepository;
	@Autowired
	private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	@Autowired
	private EnrollmentAhbxSyncService enrollmentAhbxSyncService;
	@Autowired
	private IEnrlHouseholdRepository enrlHouseholdRepository;
	@Autowired
	private GIWSPayloadService giWSPayloadService;
	@Autowired
	private IQhpReportRepository iQhpReportRepository;
	@Autowired
	private EnrollmentCreationService enrollmentCreationService;

	public static final String ENROLLMENT_STATUS_CANCEL = "CANCEL";
	public static final String ENROLLMENT_STATUS_ABORTED = "ABORTED";

	/**
	 * @Since 26-Jul-2013
	 * @author panda_pratap
	 * @return Logged In AccountUser
	 */
	@Override
	public AccountUser getLoggedInUser() {
		AccountUser accountUser = null;
		try {
			accountUser = userService.getLoggedInUser();
			if (accountUser != null) {
				int id = accountUser.getId();
				if (id > 0) {
					accountUser = this.userService.findById(id);
				} else {
					accountUser = this.userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
				}
			}
		} catch (InvalidUserException invalidUserException) {
			LOGGER.error("Error occured in EnrollmentServiceImpl.getLoggedInUser", invalidUserException);
		}
		return accountUser;
	}

	@Override
	@Transactional(readOnly = true)
	public Enrollment findById(Integer id) {
//		return enrollmentRepository.findOne(id);
		return enrollmentRepository.findById(id); // Tenant Aware Implementation
	}

	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> findBySsapApplicationid(Long ssapAppId) {
		return enrollmentRepository.findActiveEnrollmentBySsapApplicationID(ssapAppId,
				DateUtil.dateToString(new TSDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> getCarrierUpdatedEnrollments() {
		Date lastRunDate = getLastRunDate(EnrollmentConstants.CARRIER_STAT_UPDATE_JOB_NAME);
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
		return enrollmentRepository.getCarrierUpdatedEnrollments(lastRunDate, user.getId());
	}

	@Override
	@Transactional
	public Enrollment saveEnrollment(Enrollment enrollment) {
		enrollment.setUpdatedOn(new TSDate());
		return enrollmentRepository.saveAndFlush(enrollment);
	}

	@Override
	public List<Enrollment> findEnrollmentByExternalID(String externalID) {
		return enrollmentRepository.findEnrollmentByExternalID(externalID);
	}

	@Override
	public List<Enrollment> findAllEnrollments(List<Integer> enrollmentIds) {
		return enrollmentRepository.findEnrollmentByEnrollmentIds(enrollmentIds);
	}

	@Override
	public Date findEarliestEffStartDate(Integer employerEnrollmentId) {
		return enrollmentRepository.findEarliestEffStartDate(employerEnrollmentId);
	}

	/**
	 * @author panda_p
	 * 
	 * @param jobName
	 * @return lastRunDate
	 */
	private Date getLastRunDate(String jobName) {
		Date lastRunDate = null;
		if (jobName != null) {
			lastRunDate = batchJobExecutionService.getLastJobRunDate(jobName);
		}
		return lastRunDate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> searchEnrollment(Map<String, Object> searchCriteria) {
		Map<String, Object> applicationListAndRecordCount = new HashMap<String, Object>();
//		List<String> joinTables = new ArrayList<String>();
//		joinTables.add("plan");
//		joinTables.add("issuer");

		QueryBuilder<Enrollment> applicationQuery = delegateFactory.getObject();
		applicationQuery.buildSelectQuery(Enrollment.class);

		if (searchCriteria != null) {
			if (searchCriteria.get("policynumber") != null) {
				applicationQuery.applyWhere("groupPolicyNumber", searchCriteria.get("policynumber"), DataType.STRING,
						ComparisonType.EQUALS);
			}
			if (searchCriteria.get(EnrollmentConstants.STATUS) != null) {
				applicationQuery.applyWhere("enrollmentStatusLkp.lookupValueCode",
						searchCriteria.get(EnrollmentConstants.STATUS), DataType.STRING, ComparisonType.LIKE);
			}
			if (searchCriteria.get("plantype") != null) {
				applicationQuery.applyWhere("insuranceTypeLkp.lookupValueCode", searchCriteria.get("plantype"),
						DataType.STRING, ComparisonType.LIKE);
			}
			if (searchCriteria.get("issuer") != null) {
				int issuer_id = Integer.parseInt((String) searchCriteria.get("issuer"));
				applicationQuery.applyWhere("issuerId", issuer_id, DataType.NUMERIC, ComparisonType.EQ);
			}
			if (searchCriteria.get("plannumber") != null) {
				applicationQuery.applyWhere("CMSPlanID", searchCriteria.get("plannumber"), DataType.STRING,
						ComparisonType.EQUALS);
			}
			if (searchCriteria.get("lookup") != null) { // to filter individual
				// and shop enrollment
				// records
				if (searchCriteria.get("lookup").equals("INDV")) {
					applicationQuery.applyWhere("employee", null, DataType.NUMERIC, ComparisonType.ISNULL);
				} else if (searchCriteria.get("lookup").equals("SHOP")) {
					applicationQuery.applyWhere("employee", 0, DataType.NUMERIC, ComparisonType.GT);
				}
			}

			String sortBy = (searchCriteria.get(EnrollmentConstants.SORT_BY) != null)
					? searchCriteria.get(EnrollmentConstants.SORT_BY).toString()
					: EnrollmentConstants.DEFAULT_SORT_COLUMN;
			String sortOrder = (searchCriteria.get(EnrollmentConstants.SORT_ORDER) != null)
					? searchCriteria.get(EnrollmentConstants.SORT_ORDER).toString()
					: EnrollmentConstants.DEFAULT_SORT_ORDER;
			applicationQuery.applySort(sortBy, SortOrder.valueOf(sortOrder));

			List<String> columns = new ArrayList<String>();
			columns.add("id");
			columns.add("groupPolicyNumber");// policy Number
			columns.add("enrollmentStatusLkp.lookupValueCode");// Status
			columns.add("insuranceTypeLkp.lookupValueCode");// Plan type
			columns.add("planName");
			columns.add("CMSPlanID");
			columns.add("insurerName");
			columns.add("benefitEffectiveDate");
			columns.add("benefitEndDate");
			applicationQuery.applySelectColumns(columns);
			try {
				applicationQuery.setFetchDistinct(true);

				Integer startRecord = (searchCriteria.get(EnrollmentConstants.START_RECORD) != null)
						? (Integer) searchCriteria.get(EnrollmentConstants.START_RECORD)
						: EnrollmentConstants.DEFAULT_START_RECORD;
				Integer pageSize = (searchCriteria.get(EnrollmentConstants.PAGE_SIZE) != null)
						? (Integer) searchCriteria.get(EnrollmentConstants.PAGE_SIZE)
						: GhixConstants.PAGE_SIZE;

				List<Map<String, Object>> applications = applicationQuery.getData(startRecord, pageSize);
				applicationListAndRecordCount.put("applicationlist", applications);
				applicationListAndRecordCount.put("recordCount", applicationQuery.getRecordCount());
			} catch (Exception e) {
				LOGGER.error("Error occured in searchEnrollment", e);
			}
		}

		return applicationListAndRecordCount;
	}

	@Override
	public List<Enrollment> findEnrollmentByEmployer(Employer employer) {
		// This overloaded method need to be remove after 1 Jan 2015
		return enrollmentRepository.findByEmployer(employer);
	}

	@Override
	public List<Enrollment> findByEmployerAndEmployerEnrollmentId(int employerId, Integer employerEnrollmentId) {
		return enrollmentRepository.findByEmployerAndEmployerEnrollmentId(employerId, employerEnrollmentId);
	}

	@Override
	public List<Enrollment> findByEmployerAndEmployerEnrollmentIdAndDate(int employerId, Integer employerEnrollmentId,
			Date terminationDate) {
		return enrollmentRepository.findByEmployerAndEmployerEnrollmentId(employerId, employerEnrollmentId,
				DateUtil.dateToString(terminationDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
	}

	@Override
	public List<Object[]> findEnrollmentByEmployerId(int employerId, Integer employerEnrollmentId) {
		return enrollmentRepository.findByEmployerId(employerId, employerEnrollmentId);
	}

	@Override
	public List<Enrollment> findActiveEnrollmentByEmployeeApplicationID(List<Integer> employeeAppIdList,
			Date terminationDate) {
		List<Long> employeeAppIDListLong = new ArrayList<Long>();
		for (Integer id : employeeAppIdList) {
			employeeAppIDListLong.add(id.longValue());
		}
		return enrollmentRepository.findActiveEnrollmentByEmployeeApplicationID(employeeAppIDListLong,
				DateUtil.dateToString(terminationDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Object[]> findCurrentMonthEffectiveEnrollment(Map<String, Object> inputval) {
		Date currentDate = null;
		List<Object[]> enrollmentList = null;
		List<Object[]> afterTermEnrollList = null;
		if (isNotNullAndEmpty(inputval.get(EnrollmentConstants.CURRENT_DATE))) {
			currentDate = DateUtil.StringToDate(inputval.get(EnrollmentConstants.CURRENT_DATE).toString(),
					GhixConstants.REQUIRED_DATE_FORMAT);
		}

		Date lastInvoiceDate = null;
		if (isNotNullAndEmpty(inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP))) {
			lastInvoiceDate = DateUtil.StringToDate(inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP).toString(),
					EnrollmentConstants.FINANCE_DATE_FORMAT);
		}

		int employerId = 0;
		if (isNotNullAndEmpty(inputval.get(EnrollmentConstants.EMPLOYER_ID))) {
			employerId = Integer.valueOf(inputval.get(EnrollmentConstants.EMPLOYER_ID).toString());
		}
		Integer employerEnrollmentId = null;
		if (isNotNullAndEmpty(inputval.get(EnrollmentConstants.EMPLOYER_ENROLLMENT_ID))) {
			employerEnrollmentId = Integer.valueOf(inputval.get(EnrollmentConstants.EMPLOYER_ENROLLMENT_ID).toString());
		}
		List<String> statusList = null;

		if (isNotNullAndEmpty(inputval.get(EnrollmentConstants.STATUS))
				&& (inputval.get(EnrollmentConstants.STATUS) instanceof List<?>)) {
			statusList = (List<String>) inputval.get(EnrollmentConstants.STATUS);
			enrollmentList = enrollmentRepository.findCurrentMonthEffectiveEnrollment(currentDate, lastInvoiceDate,
					statusList, employerId, employerEnrollmentId);
			afterTermEnrollList = enrollmentRepository.findCurrentMonthEffectiveEnrollmentSpecial(currentDate,
					lastInvoiceDate, statusList, employerId, employerEnrollmentId);
			if (afterTermEnrollList != null && !afterTermEnrollList.isEmpty()) {
				enrollmentList.addAll(afterTermEnrollList);
			}
		}
		return enrollmentList;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> findEnrollmentByStatusAndTerminationdate(Map<String, Object> inputval) {

		Date terminationdate = DateUtil.StringToDate(inputval.get("terminationdate").toString(),
				GhixConstants.REQUIRED_DATE_FORMAT);
		String status = inputval.get(EnrollmentConstants.STATUS).toString();

		return enrollmentRepository.findEnrollmentByStatusAndBenefitEndDate(status, terminationdate, new Sort(
				new Sort.Order(Sort.Direction.ASC, "employee.id"), new Sort.Order(Sort.Direction.ASC, "insurerName")));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> findEnrollmentByStatusAndEmployerAndTerminationdate(Map<String, Object> inputval) {

		String status = inputval.get(EnrollmentConstants.STATUS).toString();
		Employer employer = (Employer) inputval.get(EnrollmentConstants.EMPLOYER);
		Date terminationdate = DateUtil.StringToDate(inputval.get("terminationdate").toString(),
				GhixConstants.REQUIRED_DATE_FORMAT);

		return enrollmentRepository.findEnrollmentByStatusAndEmployerAndBenefitEndDate(status, employer.getId(),
				terminationdate, new Sort(new Sort.Order(Sort.Direction.ASC, "employee.id"),
						new Sort.Order(Sort.Direction.ASC, "insurerName")));
	}

	public boolean getBooleanFromPlanDisplayFlags(String planDisplayFlag) {
		boolean flag = false;
		if (isNotNullAndEmpty(planDisplayFlag) && (planDisplayFlag.equalsIgnoreCase(EnrollmentConstants.YES)
				|| planDisplayFlag.equalsIgnoreCase(EnrollmentConstants.Y))) {
			flag = true;
		}
		return flag;
	}

	/**
	 * Checks enrollment status. Changes Enrollment status to cancel/terminated if
	 * all enrollments are cancelled.
	 * 
	 * @author Aditya-S
	 * @since 21-03-2014
	 * 
	 * @param enrollmentList
	 */
	public void checkEnrollmentStatusSpecialEnrollment(List<Enrollment> enrollmentList) {
		for (Enrollment enrollment : enrollmentList) {
			List<Enrollee> enrolleeList = enrollment.getEnrollees();

			int totalEnrolleeCount = 0;
			Enrollee subscriber = checkForSubscriber(enrolleeList);
			int cancellledEnrolleeCount = 0;
			int terminatedEnrolleeCount = 0;

			for (Enrollee enrollee : enrolleeList) {
				if (enrollee.getPersonTypeLkp() != null
						&& enrollee.getPersonTypeLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)
						|| enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)) {
					totalEnrolleeCount++;
					if (enrollee.getEnrolleeLkpValue() != null) {
						if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
							cancellledEnrolleeCount++;
						} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
							terminatedEnrolleeCount++;
						}
					}
				}
			}

			if (totalEnrolleeCount == cancellledEnrolleeCount) {
				enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
						EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
				enrollment.setAbortTimestamp(new TSDate());
				if (subscriber != null) {
					enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
				}
			} else if (totalEnrolleeCount == (cancellledEnrolleeCount + terminatedEnrolleeCount)) {
				enrollment.setAbortTimestamp(new TSDate());
				enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
						EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_TERM));
				if (subscriber != null && subscriber.getEffectiveEndDate() != null) {
					enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
				}
			}
		}
	}

	@Override
	@Transactional
	public List<Enrollment> saveAllEnrollment(List<Enrollment> enrollmentList) {
		List<Enrollment> enrollments = new ArrayList<Enrollment>();
		for (Enrollment enrollment : enrollmentList) {
			Enrollment en = enrollmentRepository.saveAndFlush(enrollment);
			enrollments.add(en);
		}
		return enrollments;
	}

	@Override
	public PlanResponse getPlanInfo(String planId, String marketType) {
		PlanRequest planRequest = new PlanRequest();
		PlanResponse planResponse = new PlanResponse();
		planRequest.setPlanId(planId);

		if (isNotNullAndEmpty(marketType)
				&& marketType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
			planRequest.setMarketType("INDIVIDUAL");
		}

		String planInfoResponse = null;
		try {
			planInfoResponse = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL, planRequest,
					String.class);
//                LOGGER.info("getting successfull response of the plan data from plan management." + planInfoResponse);
			LOGGER.info("getting successfull response of the plan data from plan management.");
		} catch (Exception ex) {
			LOGGER.error("exception in getting plan data from the plan management", ex);
		}

		planResponse = platformGson.fromJson(planInfoResponse, planResponse.getClass());
		return planResponse;

	}

	@Override
	public EnrollmentResponse updateShopEnrollmentStatus(EnrollmentRequest enrollmentRequest) {

		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();

		int enrollmentTypeShopId = lookupService.getlookupValueIdByTypeANDLookupValueCode(
				EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE, EnrollmentConstants.ENROLLMENT_TYPE_SHOP);
		int enrollmentStatusPendingId = lookupService.getlookupValueIdByTypeANDLookupValueCode(
				EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_PENDING);
		List<Enrollment> enrollmentList = null;
		try {
			enrollmentList = enrollmentRepository.getPendingShopEnrollment(enrollmentRequest.getEmployerEnrollmentId(),
					enrollmentTypeShopId, enrollmentStatusPendingId);

			if (enrollmentList == null) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND);
				return enrollmentResponse;
			}

		} catch (Exception e) {
			LOGGER.error("Error occured in updateShopEnrollemntToEnrolled", e);
		}
		// EnrollmentStatus
		int enrollmentStatusId = lookupService.getlookupValueIdByTypeANDLookupValueCode(
				EnrollmentConstants.ENROLLMENT_STATUS, enrollmentRequest.getEnrollmentStatus().toString());
		LookupValue enrolledLookup = new LookupValue();
		enrolledLookup.setLookupValueId(enrollmentStatusId);

		// Getting user
		// AccountUser user =
		// (enrollmentRequest.getLoggedInUserName()!=null)?getUserByName(enrollmentRequest.getLoggedInUserName())
		// : null;

		// Iterating the enrollment
		if (null != enrollmentList) {
			for (Enrollment enrollment : enrollmentList) {
				boolean isEnrolleeStatusInPending = false;
				// fetching all the enrollees for that enrollment
				// List<Enrollee> enrolleeList = enrollment.getEnrollees();

				// fetch only members for the Enrollment
				List<Enrollee> enrolleeList = enrollment.getActiveEnrolleesForEnrollment();
				// checking the enrolleeList is not null
				if (enrolleeList != null) {
					// iterating the enrollee from the enrolleeList
					for (Enrollee enrollee : enrolleeList) {
						// check for the enrollee status is PENDING
						if (enrollee.getEnrolleeLkpValue().getLookupValueId().equals(enrollmentStatusPendingId)) {
							// setting the enrollee status
							enrollee.setEnrolleeLkpValue(enrolledLookup);
							// setting the enrollment event for the given enrollment, enrollee, event type,
							// event reason code, user.
							// enrollee.setEnrollmentEvents(createEventForEnrolleeAndEnrollment(enrollment,
							// enrollee,
							// EnrollmentConstants.EVENT_TYPE_ADDITION,EnrollmentConstants.EVENT_REASON_BENEFIT_SELECTION,
							// user));
							if (enrollee.getEnrollmentEvents() != null && !enrollee.getEnrollmentEvents().isEmpty()) {
								// Collections.sort(enrollee.getEnrollmentEvents());
								// Calendar eventCreationDate = TSCalendar.getInstance();
								for (EnrollmentEvent event : enrollee.getEnrollmentEvents()) {
									// Done to differentiate between two events, so as to assure there is minimum
									// some Mili-Second difference between two events
									// eventCreationDate.setTimeInMillis(eventCreationDate.getTimeInMillis() +
									// EnrollmentConstants.ONE_THOUSAND);
									// event.setCreatedOn(eventCreationDate.getTime());
									// enrollee.setLastEventId(event);
									event.setExtractionStatus(EnrollmentConstants.RESEND_FLAG);
								}
							}
							isEnrolleeStatusInPending = true;
						} // end of the enrollee status check.

					} // end of the enrollee for ...loop

					if (isEnrolleeStatusInPending) {
						// setting the enrollment status
						enrollment.setEnrollmentStatusLkp(enrolledLookup);

						// saving the enrollment
						enrollmentRepository.saveAndFlush(enrollment);

						enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					}
				} // end of the enrolleeList null check
			} // end of the enrollment for ...loop
		}

		return enrollmentResponse;
	}

	@Override
	public AccountUser getUserByName(String userName) {
		return userService.findByUserName(userName);
	}

	private Enrollee checkForSubscriber(List<Enrollee> enrolleeList) {
		if (enrolleeList != null && !enrolleeList.isEmpty()) {
			for (Enrollee enrollee : enrolleeList) {
				if (enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode()
						.equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)) {
					return enrollee;
				}
			}
		}
		return null;
	}

	/**
	 * @since
	 * @author parhi_s
	 * @param enrollment
	 * @param user
	 * @throws Exception
	 * 
	 *                   This method is used to set Enrollment Status after the
	 *                   changes to Enrollee has been done during disEnrollment
	 *                   Process
	 * 
	 */
	@Override
	public Enrollment checkEnrollmentStatus(AccountUser user, Enrollment enrollment) {
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		if (enrollment != null) {
			try {
				// enrolleeList = enrolleeService.getEnrolleeByEnrollmentID(enrollment.getId());
				enrolleeList = enrollment.getEnrolleesAndSubscriber();
			} catch (Exception e) {
				LOGGER.error("Error occured in getEnrolleesAndSubscriber : " + e);
			}
		}

		return checkEnrollmentStatus(user, enrollment, enrolleeList);
	}

	/**
	 * 
	 * @param user
	 * @param enrollment
	 * @param enrolleeList
	 * @return
	 */
	private Enrollment checkEnrollmentStatus(AccountUser user, Enrollment enrollment, List<Enrollee> enrolleeList) {
		// Find Subscriber
		Enrollee subscriber = checkForSubscriber(enrolleeList);
		int cancellledEnrolleeCount = 0;
		int terminatedEnrolleeCount = 0;
		int comfirmedEnrolleeCount = 0;
		int paymentReceivedEnrolleeCount = 0;
		int pendingEnrolleeCount = 0;
		int totalEnrolleeCount = 0;
		boolean sendEmailNotification = false;

		if (enrolleeList != null && !enrolleeList.isEmpty()) {
			totalEnrolleeCount = enrolleeList.size();
			for (Enrollee enrollee : enrolleeList) {
				if (enrollee.getEnrolleeLkpValue() != null) {
					if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
						cancellledEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
						terminatedEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) {
						comfirmedEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED)) {
						paymentReceivedEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)) {
						pendingEnrolleeCount++;
					}
				}
			}
		}
		if (totalEnrolleeCount == cancellledEnrolleeCount) {
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
			enrollment.setAbortTimestamp(new TSDate());
			if (subscriber != null) {
				enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
			}
		} else if (totalEnrolleeCount == paymentReceivedEnrolleeCount) {
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
		} else if (totalEnrolleeCount == (cancellledEnrolleeCount + terminatedEnrolleeCount)) {
			if (null == enrollment.getAbortTimestamp()) {
				enrollment.setAbortTimestamp(new TSDate());
			}
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_TERM));
			if (subscriber != null && subscriber.getEffectiveEndDate() != null) {
				enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
			}

		} else if (totalEnrolleeCount == (cancellledEnrolleeCount + pendingEnrolleeCount)) {
			enrollment.setAbortTimestamp(null);
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_PENDING));
		} else if (comfirmedEnrolleeCount > 0 && paymentReceivedEnrolleeCount == 0 && pendingEnrolleeCount == 0
				&& enrollment.getEnrollmentStatusLkp() != null
				&& (enrollment.getEnrollmentStatusLkp().getLookupValueCode()
						.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)
						|| enrollment.getEnrollmentStatusLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED))) {
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
			if (subscriber != null) {
				enrollment.setBenefitEffectiveDate(subscriber.getEffectiveStartDate());
			}
			enrollment.setEnrollmentConfirmationDate(new TSDate());
			sendEmailNotification = true;
		}
		enrollment.setUpdatedBy(user);
		// enrollmentRepository.saveAndFlush(enrollment);

		// SENDING EMAIL NOTIFICATION TO THE RESPECTIVE EMPLOYEE ON THE ENROLLMENT
		// STATUS CONFIRMATION
		if (sendEmailNotification && enrollment.getEnrollmentStatusLkp().getLookupValueCode()
				.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) {

			try {
				List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
				enrollmentList.add(enrollment);
				enrollmentEmailNotificationService.sendEmployeeEnrollmentStatusConfirmationNotification(enrollmentList);
			} catch (GIException e) {
				LOGGER.error(
						"Error occured on sending the enrollmet status confirmation notification to the employee.: "
								+ e.getCause(),
						e);
			}

		}
		return enrollment;

	}

	/**
	 * @since
	 * @author parhi_s
	 * @param enrollment
	 * @param user
	 * @throws Exception
	 * 
	 *                   This method is used to set Enrollment Status after the
	 *                   changes to Enrollee has been done during disEnrollment
	 *                   Process
	 * 
	 */
	@Override
	public void checkEnrollmentStatus(Enrollment enrollment, AccountUser user) {
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		if (enrollment != null) {
			try {
				enrolleeList = enrolleeService.getEnrolleeByEnrollmentID(enrollment.getId());
			} catch (GIException e) {
				LOGGER.error("Error occured in getEnrolleeByEnrollmentID", e);
			}
		}
		// Find Subscriber
		Enrollee subscriber = checkForSubscriber(enrolleeList);
		int cancellledEnrolleeCount = 0;
		int terminatedEnrolleeCount = 0;
		int comfirmedEnrolleeCount = 0;
		int paymentReceivedEnrolleeCount = 0;
		int pendingEnrolleeCount = 0;
		int totalEnrolleeCount = 0;
		boolean sendEmailNotification = false;

		if (enrolleeList != null && !enrolleeList.isEmpty()) {
			totalEnrolleeCount = enrolleeList.size();
			for (Enrollee enrollee : enrolleeList) {
				if (enrollee.getEnrolleeLkpValue() != null) {
					if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
						cancellledEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
						terminatedEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) {
						comfirmedEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED)) {
						paymentReceivedEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)) {
						pendingEnrolleeCount++;
					}
				}
			}
		}
		if (totalEnrolleeCount == cancellledEnrolleeCount) {
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
			enrollment.setAbortTimestamp(new TSDate());
			if (subscriber != null) {
				enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
			}
		} else if (totalEnrolleeCount == paymentReceivedEnrolleeCount) {
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
		} else if (totalEnrolleeCount == (cancellledEnrolleeCount + terminatedEnrolleeCount)) {
			enrollment.setAbortTimestamp(new TSDate());
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_TERM));
			if (subscriber != null && subscriber.getEffectiveEndDate() != null) {
				enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
			}

		} else if (totalEnrolleeCount == (cancellledEnrolleeCount + pendingEnrolleeCount)) {
			enrollment.setAbortTimestamp(null);
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_PENDING));
		} else if (comfirmedEnrolleeCount > 0 && paymentReceivedEnrolleeCount == 0 && pendingEnrolleeCount == 0
				&& enrollment.getEnrollmentStatusLkp() != null
				&& (enrollment.getEnrollmentStatusLkp().getLookupValueCode()
						.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)
						|| enrollment.getEnrollmentStatusLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED))) {
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
			if (subscriber != null) {
				enrollment.setBenefitEffectiveDate(subscriber.getEffectiveStartDate());
			}
			enrollment.setEnrollmentConfirmationDate(new TSDate());
			sendEmailNotification = true;
		}
		enrollment.setUpdatedBy(user);
		enrollmentRepository.saveAndFlush(enrollment);

		// SENDING EMAIL NOTIFICATION TO THE RESPECTIVE EMPLOYEE ON THE ENROLLMENT
		// STATUS CONFIRMATION
		if (sendEmailNotification && enrollment.getEnrollmentStatusLkp().getLookupValueCode()
				.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) {

			try {
				List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
				enrollmentList.add(enrollment);
				enrollmentEmailNotificationService.sendEmployeeEnrollmentStatusConfirmationNotification(enrollmentList);
			} catch (GIException e) {
				LOGGER.error(
						"Error occured on sending the enrollmet status confirmation notification to the employee.: "
								+ e.getCause(),
						e);
			}

		}
	}

	/**
	 * @since
	 * @author parhi_s
	 * @param enrollee
	 * @param maintReasonCode
	 * @param user
	 * 
	 *                        This method is Called to create EnrollmentEvent during
	 *                        disEnrollment
	 * 
	 */
	@Override
	public void createEventForEnrollee(Enrollee enrollee, String eventType, String eventReasonCode, AccountUser user,
			boolean isSpecial) {
		if (enrollee != null) {
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
			enrollmentEvent.setEnrollee(enrollee);
			enrollmentEvent.setEnrollment(enrollee.getEnrollment());
			enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.CANCEL_PENDING_BATCH.toString());
			enrollmentEvent.setEventTypeLkp(
					lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, eventType));
			// add null check
			if (isNotNullAndEmpty(eventReasonCode)) {
				if (isNotNullAndEmpty(lookupService
						.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode))) {
					enrollmentEvent.setEventReasonLkp(lookupService
							.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode));
				} else {
					throw new RuntimeException(EnrollmentConstants.ERR_MSG_LOOKUP_VALUE_IS_NULL);
				}
			} else {
				throw new RuntimeException(EnrollmentConstants.ERR_MSG_REASON_CODE_VALUE);
			}
			if (isSpecial) {
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService
						.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode));
			}
			if (user != null) {
				enrollmentEvent.setCreatedBy(user);
				enrollmentEvent.setUpdatedBy(user);
			}
			// added last event Id for Audit
			enrollee.setLastEventId(enrollmentEvent);
			enrollmentEventService.saveEnrollmentEvent(enrollmentEvent);
			enrolleeRepository.saveAndFlush(enrollee);
		}
	}

	/**
	 * @author panda_p
	 * @since 27-Jun-2016
	 * @param enrollee
	 * @param eventType
	 * @param eventReasonCode
	 * @param user
	 * @param sendToCarrier
	 * @param isSpecial
	 * @param txn_idfier
	 */
	@Override
	public void createEnrolleeEvent(Enrollee enrollee, String eventType, String eventReasonCode, final AccountUser user,
			boolean sendToCarrier, boolean isSpecial, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) {
		List<EnrollmentEvent> eventList = new ArrayList<EnrollmentEvent>();
		EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
		enrollmentEvent.setEnrollee(enrollee);
		enrollmentEvent.setEnrollment(enrollee.getEnrollment());
		enrollmentEvent.setEventTypeLkp(
				lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, eventType));
		// add null check
		if (isNotNullAndEmpty(eventReasonCode)) {
			if (isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,
					eventReasonCode))) {
				enrollmentEvent.setEventReasonLkp(lookupService
						.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode));
			} else {
				throw new RuntimeException(EnrollmentConstants.ERR_MSG_LOOKUP_VALUE_IS_NULL);
			}
		} else {
			throw new RuntimeException(EnrollmentConstants.ERR_MSG_REASON_CODE_VALUE);
		}

		if (isSpecial) {
			enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService
					.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode));
		}
		if (!sendToCarrier) {
			enrollmentEvent.setSendToCarrierFlag("false");
		}
		if (isNotNullAndEmpty(txn_idfier)) {
			enrollmentEvent.setTxnIdentifier(txn_idfier.toString());
		}
		if (user != null) {
			enrollmentEvent.setCreatedBy(user);
			enrollmentEvent.setUpdatedBy(user);
		}
		eventList.add(enrollmentEvent);
		enrollee.setUpdatedBy(user);
		enrollee.setLastEventId(enrollmentEvent);
	}

	/**
	 * @since 10-30-2014
	 * @author Aditya-S
	 * @param enrollee
	 * @param maintReasonCode
	 * @param user
	 * 
	 *                        This method is Called to create EnrollmentEvent during
	 *                        during Inbound Process
	 * 
	 */
	@Override
	public void createEnrolleeEvent(Enrollee enrollee, String eventType, String eventReasonCode, AccountUser user,
			boolean sendToCarrier, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) {
		createEnrolleeEvent(enrollee, eventType, eventReasonCode, user, sendToCarrier, false, txn_idfier);
	}

	/**
	 * @since 25-10-2014
	 * @author Aditya-S
	 * 
	 *         Used to Dis-Enroll List fo Enrollments.
	 * 
	 * @param disEnrollmentDTO
	 * @return
	 * @throws GIException
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public List<Enrollment> disEnrollEnrollment(EnrollmentDisEnrollmentDTO disEnrollmentDTO,
			EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) throws GIException {
		if (disEnrollmentDTO == null
				|| (disEnrollmentDTO.getEnrollmentList() == null || disEnrollmentDTO.getEnrollmentList().isEmpty())
				|| (disEnrollmentDTO.getTerminationReasonCode() == null
						|| disEnrollmentDTO.getTerminationReasonCode().isEmpty())) {

			throw new GIException(
					"Validation Error in disEnrollEnrollment() :: Any of the following mandatory fields is null or empty"
							+ "\n" + "EnrollmentID  or TerminationReasonCode");
		}
		boolean isAnyEnrolleeChanged = false;
		List<Enrollment> updatedDentalAPTCEnrollment = new ArrayList<Enrollment>();
		for (Enrollment enrollment : disEnrollmentDTO.getEnrollmentList()) {
			enrollment.setGiWsPayloadId(disEnrollmentDTO.getGiWsPayloadId());
			List<EnrollmentEvent> enrollmentEventList = new ArrayList<EnrollmentEvent>();
			AccountUser updatedBy = disEnrollmentDTO.getUpdatedBy();
			String oldStatus = enrollment.getEnrollmentStatusLkp().getLookupValueCode();
			for (Enrollee enrollee : enrollment.getEnrolleesAndSubscriber()) {
				String enrolleeStatus = null;
				String terminationDateStr = null;
				Date terminationDate = null;
				String enrolleeDbStatus = enrollee.getEnrolleeLkpValue().getLookupValueCode();
				if (disEnrollmentDTO.getTerminationDate() == null) {
					terminationDate = enrollee.getEffectiveStartDate();
				} else {
					terminationDateStr = disEnrollmentDTO.getTerminationDate();
					terminationDate = EnrollmentUtils.StringToEODDate(terminationDateStr,
							GhixConstants.REQUIRED_DATE_FORMAT);

					if (EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL
							.equalsIgnoreCase(enrollment.getEnrollmentTypeLkp().getLookupValueCode())
							&& !DateUtil.getYearFromDate(terminationDate)
									.equals(DateUtil.getYearFromDate(enrollment.getBenefitEffectiveDate()))) {
						throw new GIException(
								"Validation Error in disEnrollEnrollment() ::  Year mismatch for  TerminationDate "
										+ terminationDate + " and EnrollmentEffectiveDate "
										+ enrollment.getBenefitEffectiveDate());
					}
				}

				if (enrollee.getEffectiveStartDate() != null) {
					if (EnrollmentUtils.removeTimeFromDate(terminationDate).after(enrollee.getEffectiveStartDate())
							&& !DateUtils.isSameDay(terminationDate, enrollee.getEffectiveStartDate())) {

						enrolleeStatus = EnrollmentConstants.ENROLLMENT_STATUS_TERM;
						// HIX-110972 Remove cancellation logic for future termination for CA
						/*
						 * if(EnrollmentConfiguration.isCaCall()){ if(enrolleeDbStatus!=null &&
						 * (enrolleeDbStatus.equalsIgnoreCase(EnrollmentConstants.
						 * ENROLLMENT_STATUS_PENDING))){ enrolleeStatus =
						 * EnrollmentConstants.ENROLLMENT_STATUS_CANCEL; } }
						 */
					} else {
						enrolleeStatus = EnrollmentConstants.ENROLLMENT_STATUS_CANCEL;
					}
					// For the Enrollee which are added to enrollment after last invoice is paid set
					// status to cancel
					if (disEnrollmentDTO.getLastPaidInvoiceDate() != null
							&& enrolleeStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {

						Date lastPaidInvoiceDate = EnrollmentUtils.StringToEODDate(
								disEnrollmentDTO.getLastPaidInvoiceDate(),
								EnrollmentConstants.DATETIME_FORMAT_MM_DD_YYYY);

						if (null != lastPaidInvoiceDate) {
							if (enrollee.getEffectiveStartDate().after(lastPaidInvoiceDate)) {
								enrolleeStatus = EnrollmentConstants.ENROLLMENT_STATUS_CANCEL;
							}
						} else {
							throw new RuntimeException(
									"lastPaidInvoiceDate is null ( Error while parsing lastPaidInvoiceDate )");
						}
					}
				}
				String termCode = disEnrollmentDTO.getTerminationReasonCode();
				if (isNotNullAndEmpty(termCode)
						&& (EnrollmentConstants.DISENROLLMENT_REASON_14.equalsIgnoreCase(termCode)
								|| EnrollmentConstants.DISENROLLMENT_REASON_AA.equalsIgnoreCase(termCode)
								|| EnrollmentConstants.DISENROLLMENT_REASON_AB.equalsIgnoreCase(termCode)
								|| EnrollmentConstants.DISENROLLMENT_REASON_AD.equalsIgnoreCase(termCode)
								|| EnrollmentConstants.DISENROLLMENT_REASON_AE.equalsIgnoreCase(termCode)
								|| EnrollmentConstants.DISENROLLMENT_REASON_AF.equalsIgnoreCase(termCode)
								|| EnrollmentConstants.DISENROLLMENT_REASON_AG.equalsIgnoreCase(termCode))) {
					enrollment.setAppEventReason(
							EnrollmentConstants.EnrollmentAppEvent.VOLUNTARY_DISENROLLMENT.toString());
				} else {
					enrollment.setAppEventReason(EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString());
				}

				if (enrolleeDbStatus != null
						&& (enrolleeDbStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)
								|| (enrolleeDbStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)
										&& (!disEnrollmentDTO.isAllowRetroTermination() || (// terminationDate.before(enrollee.getEffectiveStartDate())
																							// ||
										(terminationDate.after(enrollee.getEffectiveEndDate()) || DateUtils
												.isSameDay(terminationDate, enrollee.getEffectiveEndDate()))))))) {
					LOGGER.info("disEnrollByEnrollmentID():: The member " + enrollee.getExchgIndivIdentifier()
							+ " is already disenrolled.");
				} else {
					isAnyEnrolleeChanged = true;
					// getEffectiveStartDate can be different for all enrollees/enrollment, hence
					// set this here per enrollee.
					if (enrolleeStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
						enrollee.setEffectiveEndDate(enrollee.getEffectiveStartDate());
						enrollee.setDisenrollTimestamp(new TSDate());
					}
					// terminationDate is already Set above and is same for all enrollees..
					else if (enrolleeStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
						enrollee.setEffectiveEndDate(terminationDate);
						enrollee.setDisenrollTimestamp(new TSDate());
					}

					enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
							EnrollmentConstants.ENROLLMENT_STATUS, enrolleeStatus));
					enrollee.setUpdatedBy(updatedBy);

					if (disEnrollmentDTO.getTerminationReasonCode().equals(EnrollmentConstants.REASON_CODE_DEATH)
							&& isNotNullAndEmpty(disEnrollmentDTO.getDeathDate())) {
						enrollee.setDeathDate(DateUtil.StringToDate(disEnrollmentDTO.getDeathDate(),
								GhixConstants.REQUIRED_DATE_FORMAT));
					}

					LookupValue eventReasonLKP = null;
					if (isNotNullAndEmpty(disEnrollmentDTO.getTerminationReasonCode())) {
						if (isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(
								EnrollmentConstants.EVENT_REASON, disEnrollmentDTO.getTerminationReasonCode()))) {
							eventReasonLKP = lookupService.getlookupValueByTypeANDLookupValueCode(
									EnrollmentConstants.EVENT_REASON, disEnrollmentDTO.getTerminationReasonCode());
						} else {
							throw new RuntimeException("Lookup value is null");
						}
					} else {
						throw new GIException("Termination Reason code  is null or empty");
					}
					// Create Event
					if (!enrollee.isEventCreated()) {
						EnrollmentEvent enrollmentEvent = null;
						if (EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL.equals(txn_idfier) || !(enrolleeDbStatus
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)
								&& EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL.equalsIgnoreCase(
										enrollee.getEnrollment().getEnrollmentTypeLkp().getLookupValueCode()))) {
							enrollmentEvent = new EnrollmentEvent();
							if (txn_idfier != null) {
								enrollmentEvent.setTxnIdentifier(txn_idfier.toString());
							}
							if (!disEnrollmentDTO.isSend834()) {
								enrollmentEvent.setSendToCarrierFlag(EnrollmentConstants.FALSE);
							}
							enrollmentEvent.setEnrollee(enrollee);
							enrollmentEvent.setEnrollment(enrollment);
							enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
									EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CANCELLATION));
							enrollmentEvent.setEventReasonLkp(eventReasonLKP);
							enrollee.setLastEventId(enrollmentEvent);
							enrollmentEvent.setCreatedBy(updatedBy);
							enrollmentEvent.setUpdatedBy(updatedBy);

							enrollmentEventList.add(enrollmentEvent);
						}

						EnrollmentEvent enrollmentChangeEvent = new EnrollmentEvent();
						if (!EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL.equals(txn_idfier)
								&& enrolleeDbStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
							if (enrollmentEvent != null) {
								enrollmentEvent.setSendToCarrierFlag(EnrollmentConstants.FALSE);
							}
							if (txn_idfier != null) {
								enrollmentChangeEvent.setTxnIdentifier(txn_idfier.toString());
							}
							if (!disEnrollmentDTO.isSend834()) {
								enrollmentChangeEvent.setSendToCarrierFlag(EnrollmentConstants.FALSE);
							}

							enrollmentChangeEvent.setEnrollee(enrollee);
							enrollmentChangeEvent.setEnrollment(enrollment);
							enrollmentChangeEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
									EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CHANGE));
							enrollmentChangeEvent
									.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
											EnrollmentConstants.EVENT_REASON, EnrollmentConstants.EVENT_REASON_AI));// Minor
																													// Improvement
																													// -
																													// HIX-68804
																													// Change
																													// transaction:
																													// Validation
																													// error
																													// in
																													// EDI
							enrollee.setLastEventId(enrollmentChangeEvent);
							enrollmentChangeEvent.setCreatedBy(updatedBy);
							enrollmentChangeEvent.setUpdatedBy(updatedBy);
							enrollmentEventList.add(enrollmentChangeEvent);
						}
						enrollee.setEventCreated(true);
					}
				}
				if (isAnyEnrolleeChanged) {
					enrollment.setEnrollmentEvents(enrollmentEventList);
					checkEnrollmentStatus(updatedBy, enrollee.getEnrollment());
				}
			} // End Of Enrollee loop

			String currentStatus = enrollment.getEnrollmentStatusLkp().getLookupValueCode();

			if (disEnrollmentDTO.isNullifyDentalAPTCOnHealthTerm()
					&& !EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(oldStatus)
					&& !EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(oldStatus)
					&& (EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(currentStatus)
							|| EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(currentStatus))) {

				// code to update corresponding dental APTC
				if (EnrollmentConfiguration.isIdCall() && EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE
						.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())) {
					List<Enrollment> dentalEnrollments = updateDentalAPTCOnHealthDisEnrollment(enrollment, updatedBy,
							EnrollmentEvent.TRANSACTION_IDENTIFIER.APTC_CH_HLT_DISENROLL);
					if (dentalEnrollments != null && dentalEnrollments.size() > 0) {
						updatedDentalAPTCEnrollment.addAll(dentalEnrollments);
					}
				}
			}

			boolean populateMonthlyPremium = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
					EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
			if (isAnyEnrolleeChanged) {
				if (populateMonthlyPremium) {
					enrollmentRequotingService.updateMonthlyPremiumForCancelTerm(enrollment, null);
				}
			}

		} // End Of enrollment Loop

		if (isAnyEnrolleeChanged) {
			updatedDentalAPTCEnrollment.addAll(disEnrollmentDTO.getEnrollmentList());
			updatedDentalAPTCEnrollment = saveAllEnrollment(updatedDentalAPTCEnrollment);

			if (EnrollmentConfiguration.isCaCall()) {
				EnrollmentAhbxSync.RecordType recordType = EnrollmentAhbxSync.RecordType.IND56_DISENROLLMENT;
				if (EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL.equals(txn_idfier)) {
					recordType = EnrollmentAhbxSync.RecordType.EDIT_TOOL;
				}
				final List<Enrollment> finalEnrollmentList = updatedDentalAPTCEnrollment;
				final EnrollmentAhbxSync.RecordType record = recordType;
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
					public void afterCommit() {
						enrollmentAhbxSyncService.saveEnrollmentAhbxSync(finalEnrollmentList,
								finalEnrollmentList.get(0).getHouseHoldCaseId(), record);
					}
				});
			}

			SsapApplicationResponse ssapApplicationResponse = new SsapApplicationResponse();
			ssapApplicationResponse = updateIndividualStatusForCancel(disEnrollmentDTO.getEnrollmentList(), false);
			
			if (ssapApplicationResponse == null || !ssapApplicationResponse.getErrorCode().equalsIgnoreCase(Integer.valueOf(EnrollmentConstants.ERROR_CODE_200).toString())) {
				throw new GIException("Error in Updating the Application Status in Enrollment disenrollment flow");
			}
		
			return updatedDentalAPTCEnrollment;			
		} else {
			return disEnrollmentDTO.getEnrollmentList();
		}
	}

	@Override
	public List<Enrollment> findEnrollmentsByPlan(String planNumber, String planLevel, String planMarket,
			String regionName, String startDate, String endDate, String status) {

		return enrollmentRepository.getEnrollmentsByPlanData(planNumber, planLevel, planMarket, regionName, startDate,
				endDate, status);
	}

	@Override
	public List<Enrollment> findEnrollmentsByIssuer(String issuerName, String planLevel, String planMarket,
			String regionName, String startDate, String endDate, String status) {

		return enrollmentRepository.getEnrollmentsByIssuerData(issuerName, planLevel, planMarket, regionName, startDate,
				endDate, status);
	}

	@Override
	public Map<String, Object> getPlanIdAndOrderItemIdByEnrollmentId(Integer enrollmentId) {

		Map<String, Object> enrollmentMap = new HashMap<String, Object>();
		List<Object[]> enrollmentList = enrollmentRepository.getPlanIdAndOrderItemIdByEnrollmentId(enrollmentId);
		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			for (Object[] enrollColData : enrollmentList) {
				enrollmentMap.put(EnrollmentConstants.PLAN_ID, enrollColData[EnrollmentConstants.ZERO]);
				enrollmentMap.put(EnrollmentConstants.ENROLLMENT_STATUS, enrollColData[EnrollmentConstants.ONE]);
				if (enrollColData.length >= EnrollmentConstants.SIX) {
					enrollmentMap.put(EnrollmentConstants.CMS_PLAN_ID, enrollColData[EnrollmentConstants.TWO]);
					enrollmentMap.put(EnrollmentConstants.SUBSCRIBER_MEMBER_ID,
							enrollColData[EnrollmentConstants.THREE]);
					enrollmentMap.put(EnrollmentConstants.EFFECTIVE_START_DATE,
							enrollColData[EnrollmentConstants.FOUR]);
					enrollmentMap.put(EnrollmentConstants.EFFECTIVE_END_DATE, enrollColData[EnrollmentConstants.FIVE]);
				}
				if (enrollColData.length >= EnrollmentConstants.EIGHT) {
					enrollmentMap.put(EnrollmentConstants.GROSS_PREMIUM_AMT, enrollColData[EnrollmentConstants.SIX]);
					enrollmentMap.put(EnrollmentConstants.NET_PREMIUM_AMT, enrollColData[EnrollmentConstants.SEVEN]);

				}
				if (enrollColData.length >= EnrollmentConstants.NINE) {
					enrollmentMap.put(EnrollmentConstants.APTC, enrollColData[EnrollmentConstants.EIGHT]);
				}
				if (enrollColData.length >= EnrollmentConstants.TEN) {
					enrollmentMap.put(EnrollmentConstants.PLAN_LEVEL, enrollColData[EnrollmentConstants.NINE]);
				}
				if (enrollColData.length >= EnrollmentConstants.ELEVEN) {
					enrollmentMap.put(EnrollmentConstants.CHANGE_PLAN_ALLOWED, enrollColData[EnrollmentConstants.TEN]);
				}
				if(enrollColData.length >= EnrollmentConstants.TWELVE){
					enrollmentMap.put(EnrollmentConstants.STATE_SUBSIDY, enrollColData[EnrollmentConstants.ELEVEN]);
				}


			}
			/**
			 * @since 06th August 2015 Jira Id: HIX-73359 Description: Pass subscriber zip
			 *        and county in getplanidandorderitemidbyenrollmentid API
			 */
			List<Object[]> subscriberHomeAddressDetailList = enrolleeRepository
					.getSubscribersHomeAddressDetails(enrollmentId);
			if (subscriberHomeAddressDetailList != null && !subscriberHomeAddressDetailList.isEmpty()) {
				for (Object[] enrolleeColData : subscriberHomeAddressDetailList) {
					enrollmentMap.put(EnrollmentConstants.SUBSCRIBER_HOME_ADD_ZIP,
							enrolleeColData[EnrollmentConstants.ZERO]);
					enrollmentMap.put(EnrollmentConstants.SUBSCRIBER_HOME_ADD_COUNTYCODE,
							enrolleeColData[EnrollmentConstants.ONE]);
					// Adding break statement to exit the for loop is multiple Subscriber found. As
					// in query we had added order by Last_Updated_timeStamp
					break;
				}
			}
			List<Object[]> enrolleeAgeMemberIdList = enrolleeRepository.getEnrolleeAgeAndMemberId(enrollmentId);
			if (enrolleeAgeMemberIdList != null && !enrolleeAgeMemberIdList.isEmpty()) {
				enrollmentMap.put(EnrollmentConstants.ENROLLEE_AGE_AND_MEMBER_DATA, enrolleeAgeMemberIdList);
			}

			List<EnrollmentPremium> enrollmentPremiumList = enrollmentPremiumRepository.findByEnrollmentId(enrollmentId);
			if (enrollmentPremiumList != null && !enrollmentPremiumList.isEmpty()) {
				Collections.sort(enrollmentPremiumList);
				for (EnrollmentPremium enrollmentPremium : enrollmentPremiumList) {
					if (enrollmentPremium.getMaxAptc() != null) {
						enrollmentMap.put(EnrollmentConstants.MAX_APTC, enrollmentPremium.getMaxAptc());
						break;
					}
				}
			}
		}
		return enrollmentMap;
	}

	@Override
	public List<EnrollmentCountDTO> findEnrollmentPlanLevelCountByBroker(Integer assisterBrokerId, String role,
			Date startDate, Date endDate) {
		List<String> enrollmentPlanLevels = new ArrayList<String>();
		List<EnrollmentCountDTO> enrollmentCountDTOList = new ArrayList<EnrollmentCountDTO>();
		List<Object[]> enrollmentPlanLevelCountByBrokerData = enrollmentRepository
				.getEnrollmentPlanLevelCountByBroker(assisterBrokerId, role, startDate, endDate);

		if (enrollmentPlanLevelCountByBrokerData != null) {
			for (Object[] planLevelData : enrollmentPlanLevelCountByBrokerData) {
				EnrollmentCountDTO enrollmentCountDTO = new EnrollmentCountDTO();
				enrollmentCountDTO.setPlanLevel((String) planLevelData[0]);
				enrollmentPlanLevels.add((String) planLevelData[0]);
				enrollmentCountDTO.setEnrollmentCount(new Integer(planLevelData[1].toString()));
				enrollmentCountDTOList.add(enrollmentCountDTO);
			}
		}

		// HIX-44357 and HIX-44365 :: Put the health plan levels which are not there in
		// the query result
		if (PlanLevel.values() != null) {
			for (PlanLevel plan : PlanLevel.values()) {
				if (!enrollmentPlanLevels.contains(plan.toString())) {
					EnrollmentCountDTO enrollmentCountDTO = new EnrollmentCountDTO();
					enrollmentCountDTO.setPlanLevel(plan.toString());
					enrollmentPlanLevels.add(plan.toString());
					enrollmentCountDTO.setEnrollmentCount(EnrollmentConstants.DEFAULT_START_RECORD);
					enrollmentCountDTOList.add(enrollmentCountDTO);
				}
			}
		}

		// HIX-44357 and HIX-44365 :: Put the Dental plan levels which are not there in
		// the query result
		if (PlanLevelDental.values() != null) {
			for (PlanLevelDental plan : PlanLevelDental.values()) {
				if (!enrollmentPlanLevels.contains(plan.toString())) {
					EnrollmentCountDTO enrollmentCountDTO = new EnrollmentCountDTO();
					enrollmentPlanLevels.add(plan.toString());
					enrollmentCountDTO.setPlanLevel(plan.toString());
					enrollmentCountDTO.setEnrollmentCount(EnrollmentConstants.DEFAULT_START_RECORD);
					enrollmentCountDTOList.add(enrollmentCountDTO);
				}
			}
		}

		return enrollmentCountDTOList;
	}

	@Override
	public List<Employer> findDistinctEmployer() {
		return enrollmentRepository.getDistinctEmployers();
	}

	@Override
	@Transactional
	public void abortEnrollment(Enrollment enrollment) {
		try {
			List<Enrollee> enrolleeList = enrolleeService.getEnrolleeByEnrollmentID(enrollment.getId());
			for (Enrollee enrollee : enrolleeList) {
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
						EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_ABORTED));
				enrollee.setDisenrollTimestamp(new TSDate());
				enrolleeRepository.saveAndFlush(enrollee);
			}
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_ABORTED));
			enrollment.setAbortTimestamp(new TSDate());
			enrollment.setBenefitEndDate(new TSDate());
			enrollmentRepository.saveAndFlush(enrollment);
		} catch (GIException e) {
			LOGGER.error("Error in aborting enrollment.", e);
		}
	}

	@Override
	public boolean isEnrollmentExists(int enrollmentId) {
		return enrollmentRepository.exists(enrollmentId);
	}

	@Override
	public List<Enrollment> findEnrollmentByCmrHouseholdId(int cmrHouseholdId) {
		return enrollmentRepository.findEnrollmentByCmrHouseholdId(cmrHouseholdId);
	}

	@Override
	public List<Object[]> getRemittanceDataByEnrollmentId(Integer enrollmentId) {
		return enrollmentRepository.getRemittanceDataByEnrollmentId(enrollmentId);
	}

	@Override
	public BenefitBayRequestDTO populateBenefitBayRequestDTO(EnrollmentResponse enrollmentResponse) {
		Enrollment enrollmentObj = enrollmentResponse.getEnrollmentList().get(EnrollmentConstants.ZERO);
		BenefitBayRequestDTO benefitBayRequestDTO = new BenefitBayRequestDTO();

		benefitBayRequestDTO.setEnrollmentID(enrollmentObj.getId().toString());
		benefitBayRequestDTO.setHouseholdID(enrollmentObj.getHouseHoldCaseId());

		benefitBayRequestDTO.setCarrierID(enrollmentObj.getHiosIssuerId());
		benefitBayRequestDTO.setCarrierName(enrollmentObj.getInsurerName());

		benefitBayRequestDTO.setPlanID(enrollmentObj.getPlanId().toString());
		benefitBayRequestDTO.setPlanName(enrollmentObj.getPlanName());
		benefitBayRequestDTO.setGrossMonthlyPremium(new BigDecimal(String.valueOf(enrollmentObj.getGrossPremiumAmt())));
		benefitBayRequestDTO.setPlanType(enrollmentObj.getInsuranceTypeLkp().getLookupValueCode());

		EnrollmentPlan enrollmentPlan = enrollmentPlanRepository.findEnrollmentPlanByPlanID(enrollmentObj.getPlanId());

		if (enrollmentPlan != null) {

			benefitBayRequestDTO.setDoctorVisitCost(enrollmentPlan.getOfficeVisit());
			benefitBayRequestDTO
					.setFamilyDeductible(BigInteger.valueOf(enrollmentPlan.getFamilyDeductible().intValue()));
			benefitBayRequestDTO
					.setFamilyMaximumOutOfPocket(BigInteger.valueOf(enrollmentPlan.getIndivOopMax().longValue()));
			benefitBayRequestDTO.setGenericPrescriptionCost(enrollmentPlan.getGenericMedication());
			benefitBayRequestDTO
					.setIndividualDeductible(BigInteger.valueOf(enrollmentPlan.getIndivDeductible().longValue()));
			benefitBayRequestDTO
					.setIndividualMaximumOutOfPocket(BigInteger.valueOf(enrollmentPlan.getIndivOopMax().longValue()));
			benefitBayRequestDTO.setPlanTier(enrollmentPlan.getNetworkType());
		}

		return benefitBayRequestDTO;
	}

	@Override
	public List<Object[]> findEmployersByStartDateAndEndDate(Date startDate, Date endDate) {
		return enrollmentRepository.findEmployerIdByStartDateAndEndate(startDate, endDate);
	}

	@Override
	public EnrollmentResponse getShopEnrollmentLog(EnrollmentRequest enrollmentRequest) {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		Date startDate = null;
		Date endDate = null;
		Integer employerId = enrollmentRequest.getEmployerId();
		List<Integer> enrollmentIds = null;
		String employeeName = enrollmentRequest.getEmployeeName();
		List<Object[]> enrollmentLogDataList = null;
		try {

			/** (HIX-59519) Modification in enrollment log rest API for EMPLOYEE ID */

			if (enrollmentRequest.getEmployeeId() != null && enrollmentRequest.getEmployeeId() > 0) {

				enrollmentLogDataList = enrolleeRepository.getShopEnrollmentLog(enrollmentRequest.getEmployeeId());
				List<Object[]> specialEnrollmentLogDataList = enrolleeRepository
						.getShopEnrollmentLogForSpecialEnrollment(enrollmentRequest.getEmployeeId());
				enrollmentLogDataList.addAll(specialEnrollmentLogDataList);
			} else {

				/** Calculating Start and End Date */
				if (isNotNullAndEmpty(enrollmentRequest.getEmployerEnrollmentStartDate())
						&& isNotNullAndEmpty(enrollmentRequest.getEmployerEnrollmentEndDate())) {
					startDate = enrollmentRequest.getEmployerEnrollmentStartDate();
					endDate = enrollmentRequest.getEmployerEnrollmentEndDate();
				} else {
					startDate = DateUtil.getMonthStartDate(new TSDate());
					endDate = new TSDate();
				}

				if (isNotNullAndEmpty(employeeName)) {
					enrollmentIds = enrolleeRepository.searchEmployee(employeeName);
				}

				if (enrollmentIds != null) {
					enrollmentLogDataList = enrolleeRepository.getShopEnrollmentLog(employerId, startDate, endDate,
							enrollmentIds);
					List<Object[]> specialEnrollmentLogDataList = enrolleeRepository
							.getShopEnrollmentLogForSpecialEnrollment(employerId, startDate, endDate, enrollmentIds);
					enrollmentLogDataList.addAll(specialEnrollmentLogDataList);
				} else {
					enrollmentLogDataList = enrolleeRepository.getShopEnrollmentLog(employerId, startDate, endDate);
					List<Object[]> specialEnrollmentLogDataList = enrolleeRepository
							.getShopEnrollmentLogForSpecialEnrollment(employerId, startDate, endDate);
					enrollmentLogDataList.addAll(specialEnrollmentLogDataList);
				}

			}

			List<ShopEnrollmentLogDto> shopEnrollmentLogDtoList = new ArrayList<ShopEnrollmentLogDto>();
			if (!enrollmentLogDataList.isEmpty()) {
				/** Sort in Descending order HIX-62782 **/
				Collections.sort(enrollmentLogDataList, new Comparator<Object[]>() {
					@Override
					public int compare(Object[] objArray1, Object[] objArray2) {
						return (-((Date) objArray1[EnrollmentConstants.ZERO])
								.compareTo(((Date) objArray2[EnrollmentConstants.ZERO])));
					}
				});

				Map<Integer, String> employeeNameMap = new HashMap<Integer, String>();
				for (Object[] enrollmentLogData : enrollmentLogDataList) {
					ShopEnrollmentLogDto shopEnrollmentLogDto = new ShopEnrollmentLogDto();
					if (enrollmentLogData[EnrollmentConstants.ZERO] != null) {
						shopEnrollmentLogDto
								.setEventDate(DateUtil.dateToString((Date) enrollmentLogData[EnrollmentConstants.ZERO],
										GhixConstants.REQUIRED_DATE_FORMAT));
					}
					shopEnrollmentLogDto.setEventType((String) enrollmentLogData[EnrollmentConstants.ONE]);
					if (enrollmentLogData.length >= EnrollmentConstants.ELEVEN
							&& enrollmentLogData[EnrollmentConstants.TEN] != null) {
						shopEnrollmentLogDto.setEventReason((String) enrollmentLogData[EnrollmentConstants.TEN]);
						shopEnrollmentLogDto.setSerc((String) enrollmentLogData[EnrollmentConstants.TEN]);
					} else {
						shopEnrollmentLogDto.setEventReason((String) enrollmentLogData[EnrollmentConstants.TWO]);
						shopEnrollmentLogDto.setSerc((String) enrollmentLogData[EnrollmentConstants.TWO]);
					}

					if (enrollmentLogData[EnrollmentConstants.THREE] != null) {
						shopEnrollmentLogDto.setCoverageStartDate(
								DateUtil.dateToString((Date) enrollmentLogData[EnrollmentConstants.THREE],
										GhixConstants.REQUIRED_DATE_FORMAT));
					}
					if (enrollmentLogData[EnrollmentConstants.FOUR] != null) {
						shopEnrollmentLogDto.setCoverageEndDate(
								DateUtil.dateToString((Date) enrollmentLogData[EnrollmentConstants.FOUR],
										GhixConstants.REQUIRED_DATE_FORMAT));
					}

					shopEnrollmentLogDto.setEnrolleeName((String) enrollmentLogData[EnrollmentConstants.FIVE]);

					employeeName = employeeNameMap.get(enrollmentLogData[EnrollmentConstants.SIX]);

					if (!isNotNullAndEmpty(employeeName)) {
						employeeName = enrolleeRepository
								.getEmployeeNameFromEnrollmentId((Integer) enrollmentLogData[EnrollmentConstants.SIX]);
						employeeNameMap.put((Integer) enrollmentLogData[EnrollmentConstants.SIX], employeeName);
					}

					if (enrollmentLogData[EnrollmentConstants.SEVEN] != null) {
						shopEnrollmentLogDto.setInsuranceType((String) enrollmentLogData[EnrollmentConstants.SEVEN]);
					}
					if (enrollmentLogData[EnrollmentConstants.EIGHT] != null) {
						shopEnrollmentLogDto.setCmsPlanId((String) enrollmentLogData[EnrollmentConstants.EIGHT]);
					}
					if (enrollmentLogData[EnrollmentConstants.NINE] != null) {
						shopEnrollmentLogDto.setPlanName((String) enrollmentLogData[EnrollmentConstants.NINE]);
					}

					shopEnrollmentLogDto.setEmployeeName(employeeName);
					shopEnrollmentLogDtoList.add(shopEnrollmentLogDto);
				}
			}
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setShopEnrollmentLogDto(shopEnrollmentLogDtoList);
		} catch (Exception exe) {
			LOGGER.error("Exception in getShopEnrollmentLog", exe);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return enrollmentResponse;
	}

	@Override
	public boolean isIssuerActive(Integer issuerID) {
		boolean isActive = false;
		// long
		// count=enrollmentRepository.getActiveEnrollmentCountForIssuer(issuerID,new
		// TSDate());
		if (enrollmentRepository.getActiveEnrollmentCountForIssuer(issuerID, new TSDate()) > EnrollmentConstants.ZERO) {
			isActive = true;
		}
		return isActive;
	}

	@Override
	public EnrollmentResponse findEmployeeIdAndPlanId(EnrollmentRequest enrollmentRequest) {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (enrollmentRequest != null) {
			if (enrollmentRequest.getEmployerId() != null) {
				EmployerCoverageDTO employerCvgDto = new EmployerCoverageDTO();
				List<EmployeeCoverageDTO> listEmployeeCvgDto = new ArrayList<EmployeeCoverageDTO>();

				Map<Integer, EmployeeCoverageDTO> employeeCoverageDtoMap = new HashMap<Integer, EmployeeCoverageDTO>();
				List<Object[]> employeeHealthPlanList = enrollmentRepository
						.findEmployeeIdAndPlanIdForHealth(enrollmentRequest.getEmployerId());
				List<Object[]> employeeDentalPlanList = enrollmentRepository
						.findEmployeeIdAndPlanIdForDental(enrollmentRequest.getEmployerId());

				if (employeeDentalPlanList != null && !employeeDentalPlanList.isEmpty()) {
					for (Object[] employeeAndPlanListData : employeeDentalPlanList) {
						EmployeeCoverageDTO employeeCvgDto = new EmployeeCoverageDTO();
						employeeCvgDto.setEmployeeId((Integer) employeeAndPlanListData[0]);
						employeeCvgDto.setDentalHiosPlanNumber((String) employeeAndPlanListData[1]);
						employeeCoverageDtoMap.put((Integer) employeeAndPlanListData[0], employeeCvgDto);
						// listEmployeeCvgDto.add(employeeCvgDto);
					}
				}
				if (employeeHealthPlanList != null && !employeeHealthPlanList.isEmpty()) {
					for (Object[] employeeAndPlanListData : employeeHealthPlanList) {
						EmployeeCoverageDTO employeeCvgDto = employeeCoverageDtoMap
								.get((Integer) employeeAndPlanListData[0]);
						if (employeeCvgDto == null) {
							employeeCvgDto = new EmployeeCoverageDTO();
						}
						employeeCvgDto.setEmployeeId((Integer) employeeAndPlanListData[0]);
						employeeCvgDto.setHealthHiosPlanNumber((String) employeeAndPlanListData[1]);
						employeeCoverageDtoMap.put((Integer) employeeAndPlanListData[0], employeeCvgDto);
						listEmployeeCvgDto.add(employeeCvgDto);
					}
					employerCvgDto.setEmployeeCoverage(listEmployeeCvgDto);
					employerCvgDto.setEmployerId(enrollmentRequest.getEmployerId());
					enrollmentResponse.setEmployerCvgDto(employerCvgDto);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				} else {
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				}
			} else {
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_IS_NULL);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} else {
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setStatus(EnrollmentConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
		}
		return enrollmentResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Integer> findActiveEnrollmentsForEmployer(Map<String, Object> inputval) {
		Date currentDate = null;
		List<Integer> enrollmentList = null;

		if (isNotNullAndEmpty(inputval.get(EnrollmentConstants.CURRENT_DATE))) {
			currentDate = DateUtil.StringToDate(inputval.get(EnrollmentConstants.CURRENT_DATE).toString(),
					GhixConstants.REQUIRED_DATE_FORMAT);
		}

		int employerId = EnrollmentConstants.ZERO;
		if (isNotNullAndEmpty(inputval.get(EnrollmentConstants.EMPLOYER_ID))) {
			employerId = Integer.valueOf(inputval.get(EnrollmentConstants.EMPLOYER_ID).toString());
		}

		List<String> statusList = null;
		if (inputval.get(EnrollmentConstants.STATUS) instanceof List<?>) {
			statusList = (List<String>) inputval.get(EnrollmentConstants.STATUS);
			enrollmentList = enrollmentRepository.findActiveEnrollmentForEmployer(currentDate, statusList, employerId);
		}

		return enrollmentList;
	}

	@Override
	public List<Object[]> findEnrollmentByEmployeeIdAndEnrollmentId(Integer employeeId,
			List<Integer> enrollmentIdList) {
		if (enrollmentIdList != null && !enrollmentIdList.isEmpty()) {
			return enrollmentRepository.findEnrollmentByEnrollmentId(enrollmentIdList);
		} else {
			return enrollmentRepository.findEnrollmentByEmployeeId(employeeId);
		}
	}

	@Override
	public String findCoverageEndDateByEmployeeApplicationID(Long employeeId) {
		Enrollment enrollment = enrollmentRepository.findLatestShopHealthEnrollment(employeeId);
		Date coverageEndDate = enrollment.getBenefitEndDate();
		if (coverageEndDate != null) {
			return DateUtil.dateToString(coverageEndDate, GhixConstants.REQUIRED_DATE_FORMAT);
		}
		return null;
	}

	@Override
	public List<Enrollment> findActiveEnrollmentByEmployeeID(Integer employeeId, Date terminationDate) {
		return enrollmentRepository.findActiveEnrollmentByEmployeeID(employeeId,
				DateUtil.dateToString(terminationDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
	}

	@Override
	public List<Enrollment> findActiveEnrollmentBySsapApplicationID(Long ssapApplicationId) {
		return enrollmentRepository.findActiveEnrollmentBySsapApplicationID(ssapApplicationId,
				DateUtil.dateToString(new TSDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
	}

	@Override
	public List<Enrollment> findActiveEnrollmentByHouseholdCaseId(Long householdCaseId) {
		return enrollmentRepository.findEnrollmentByHouseholdCaseId(String.valueOf(householdCaseId));
	}

	/**
	 * @Aditya-s @since 25-11-2014
	 * 
	 *           Changes Person_Type lookup of Terminated/Cancelled Subscribers to
	 *           Enrollee
	 * 
	 * @param enrollee
	 */
	public void changeTerminatedSubscriberToEnrollee(List<Enrollee> enrolleeList) {
		if (enrolleeList != null && !enrolleeList.isEmpty()) {
			Date highestSubscriberEndDate = null;
			List<Enrollee> subscriberEnrollees = new ArrayList<Enrollee>();
			for (Enrollee enrollee : enrolleeList) {
				if (enrollee.getPersonTypeLkp().getLookupValueCode() != null && enrollee.getPersonTypeLkp()
						.getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)) {
					// enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
					if (highestSubscriberEndDate == null) {
						highestSubscriberEndDate = enrollee.getEffectiveEndDate();
					} else if (enrollee.getEffectiveEndDate().after(highestSubscriberEndDate)) {
						highestSubscriberEndDate = enrollee.getEffectiveEndDate();
					}
					subscriberEnrollees.add(enrollee);
				}
			}
			if (subscriberEnrollees != null && subscriberEnrollees.size() > 1) {
				for (Enrollee enrollee : subscriberEnrollees) {
					if (enrollee.getEffectiveEndDate().before(highestSubscriberEndDate)
							&& (enrollee.getEnrolleeLkpValue().getLookupValueCode()
									.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)
									|| (enrollee.getEnrolleeLkpValue().getLookupValueCode()
											.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)))) {
						enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
								EnrollmentConstants.LOOKUP_PERSON_TYPE,
								EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
					}
				}
			}
		}
	}

	/**
	 * @author Aditya Shanbhag
	 * 
	 *         Jira HIX-57285 - Requred for ID Profile
	 * 
	 *         API accepts enrollment ID and elected aptc to update the existing
	 *         enrollment
	 * @throws GIException
	 * 
	 */

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EnrollmentResponse updateEnrollmentAptc(EnrollmentRequest enrollmentRequest) throws Exception {
		EnrollmentResponse enrlResp = new EnrollmentResponse();
		if (enrollmentRequest != null) {
			boolean useSameEffectiveDate = Boolean.parseBoolean(DynamicPropertiesUtil
					.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.USE_SAME_EFFECTIVE_DATE));
			List<EnrollmentAptcUpdateDto> enrollmentAptcUpdateDtoList = enrollmentRequest
					.getEnrollmentAptcUpdateDtoList();

			if (enrollmentAptcUpdateDtoList != null && !enrollmentAptcUpdateDtoList.isEmpty()) {
				Date lastJobRunDate = getJOBLastRunDate(EnrollmentConstants.INDIVIDUAL_ENROLLMENT_BATCH_JOB);
				if (lastJobRunDate == null) {
					lastJobRunDate = DateUtil.StringToDate(EnrollmentConstants.DEFAULT_BATCH_START_DATE,
							EnrollmentConstants.BATCH_DATE_FORMAT);
				}
				Map<Integer, String> skippedEnrollmentMap = new HashMap<>();
				Long newSsapApplicationId = enrollmentAptcUpdateDtoList.get(0).getSsapApplicationId();
				Long existingSsapApplicationId = null;
				LookupValue eventTypeLookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(
						EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CHANGE);
				LookupValue eventTypeReasonLookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(
						EnrollmentConstants.EVENT_REASON, EnrollmentConstants.EVENT_REASON_LOOKUP_NO_REASON);
				// LookupValue eventTypeSpclReasonLookupValue =
				// lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON,
				// EnrollmentConstants.EVENT_REASON_LOOKUP_APTC_CH);
				List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
				String householdCaseId = null;
				for (EnrollmentAptcUpdateDto enrollmentAptcUpdateDto : enrollmentAptcUpdateDtoList) {
					Integer enrollmentId = enrollmentAptcUpdateDto.getEnrollmentId();
					Float updatedAptcAmt = enrollmentAptcUpdateDto.getAptcAmt();
					Long ssapApplicationId = enrollmentAptcUpdateDto.getSsapApplicationId();
					Date aptcEffectiveDate = enrollmentAptcUpdateDto.getAptcEffectiveDate();
					boolean isConvertedToNonFinancialFlag = enrollmentAptcUpdateDto.isConvertedToNonFinancialFlag();
					if (!isNotNullAndEmpty(enrollmentId)) {
						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_204);
						enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
						LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
					} else if (!isConvertedToNonFinancialFlag && !isNotNullAndEmpty(updatedAptcAmt)) {
						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_205);
						enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_APTC_VALUE_NULL_EMPTY);
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
						LOGGER.error(EnrollmentConstants.ERR_MSG_APTC_VALUE_NULL_EMPTY);
					} else if (!isNotNullAndEmpty(aptcEffectiveDate)) {
						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_206);
						enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_APTC_DATE_VALUE_NULL_EMPTY);
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
						LOGGER.error(EnrollmentConstants.ERR_MSG_APTC_DATE_VALUE_NULL_EMPTY);
					} else if (isConvertedToNonFinancialFlag && isNotNullAndEmpty(updatedAptcAmt)) {
						enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_208);
						enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NON_FINANCIAL_INVALID_APTC_DATE_AND_AMT);
						LOGGER.error(EnrollmentConstants.ERR_MSG_NON_FINANCIAL_INVALID_APTC_DATE_AND_AMT);
					} else {
						Enrollment enrollment = enrollmentRepository.findById(enrollmentId);

						if (enrollment == null) {
							enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_207);
							enrlResp.setErrMsg(
									EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID);
							enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
							return enrlResp;
						}
						householdCaseId = (null == householdCaseId) ? enrollment.getHouseHoldCaseId() : householdCaseId;
						existingSsapApplicationId = (null == existingSsapApplicationId)
								? enrollment.getSsapApplicationid()
								: existingSsapApplicationId;
						if (aptcEffectiveDate.before(enrollment.getBenefitEffectiveDate())) {
							enrollmentAptcUpdateDto.setAptcEffectiveDate(enrollment.getBenefitEffectiveDate());
							aptcEffectiveDate = enrollment.getBenefitEffectiveDate();
						}
						Calendar cal = TSCalendar.getInstance();
						cal.setTime(aptcEffectiveDate);
						/*
						 * else if(aptcEffectiveDate.before(enrollment.getBenefitEffectiveDate()) ||
						 * aptcEffectiveDate.after(enrollment.getBenefitEndDate())){
						 * enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_208);
						 * enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DATE_OUT_COV);
						 * enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE); }else
						 * if(!DateUtils.isSameDay(enrollment.getBenefitEffectiveDate(),
						 * aptcEffectiveDate) && cal.get(Calendar.DATE)!=1) {
						 * enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_209);
						 * enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_DATE_APTC_MID_MONTH);
						 * enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE); }
						 */
						if (aptcEffectiveDate.after(enrollment.getBenefitEndDate()) || !EnrollmentUtils
								.isSameYear(enrollment.getBenefitEffectiveDate(), aptcEffectiveDate)) {
							skippedEnrollmentMap.put(enrollment.getId(), EnrollmentConstants.ERR_MSG_DATE_OUT_COV);
							continue;
						} else if (DateUtil.getDateOfMonth(aptcEffectiveDate) != 1
								&& DateUtil.getDateOfMonth(enrollment.getBenefitEffectiveDate()) == 1) {
							skippedEnrollmentMap.put(enrollment.getId(),
									EnrollmentConstants.ERR_MSG_DATE_APTC_MID_MONTH);
							continue;
						} else {// Validations for EnrollmentId and Aptc done, Proceed with Update
								// Before crating event or doing any update, we should check if the amount
								// coming is different from existing amount
							if (EnrollmentUtils.isFloatEqual(enrollment.getAptcAmt(), updatedAptcAmt)) {
								throw new GIException(EnrollmentConstants.ERR_MSG_INVALID_APTC);
							}
							//HIX-119577 Capping APTC if it is greater than gross premium * ehb percent
							updatedAptcAmt = Math.min(updatedAptcAmt,
									(enrollment.getEhbPercent() != null
											? (enrollment.getGrossPremiumAmt() * enrollment.getEhbPercent())
											: enrollment.getGrossPremiumAmt()));
							
							enrollmentList.add(enrollment);
							// Create Events for Enrollee
							// List<Enrollee> enrolleeList = enrollment.getActiveEnrolleesForEnrollment();
							List<Enrollee> enrollees = enrolleeRepository
									.getActiveEnrolleesForEnrollmentID(enrollment.getId(), new TSDate());// HIX-72250, HIX-116673(New Change)
							AccountUser loggedInUser = getLoggedInUser();

							if (enrollees != null && enrollees.size() > 0) {
								for(Enrollee enrollee : enrollees) {
								List<EnrollmentEvent> eventList = enrollee.getEnrollmentEvents();
								if (eventList != null && eventList.size() > 0) {
									Collections.sort(eventList);
									Collections.reverse(eventList);
									boolean isSpecialNext = false;
									for (EnrollmentEvent event : eventList) {
										if (event.getCreatedOn().after(lastJobRunDate) && (event
												.getSpclEnrollmentReasonLkp() != null
												|| (event.getTxnIdentifier() != null && event.getTxnIdentifier()
														.equalsIgnoreCase(EnrollmentEvent.TRANSACTION_IDENTIFIER.APTC_CH
																.toString())))) {
											if (!isSpecialNext && event.getTxnIdentifier() != null
													&& event.getTxnIdentifier().equalsIgnoreCase(
															EnrollmentEvent.TRANSACTION_IDENTIFIER.APTC_CH
																	.toString())) {
												event.setSendToCarrierFlag("false");
												event.setUpdatedBy(loggedInUser);
											}
											if (event.getSpclEnrollmentReasonLkp() != null) {
												isSpecialNext = true;
											} else {
												isSpecialNext = false;
											}

										}
									}
								}

								EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
								enrollmentEvent.setEnrollee(enrollee);
								enrollmentEvent.setEnrollment(enrollment);
								enrollmentEvent.setEventTypeLkp(eventTypeLookupValue);
								enrollmentEvent.setEventReasonLkp(eventTypeReasonLookupValue);
								enrollmentEvent
										.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.APTC_CH.toString());

								// Set Created User
								enrollmentEvent.setCreatedBy(loggedInUser);
								enrollmentEvent.setUpdatedBy(loggedInUser);
								eventList.add(enrollmentEvent);
								enrollee.setEnrollmentEvents(eventList);
								// added last event Id for Audit
								enrollee.setLastEventId(enrollmentEvent);
							}
							}
							enrollment.setChangePlanAllowed(EnrollmentConstants.N);
							enrollment.setAptcAmt(updatedAptcAmt);
							if (ssapApplicationId != null) {
								enrollment.setPriorSsapApplicationid(ssapApplicationId);
								enrollment.setSsapApplicationid(ssapApplicationId);
							}
							if (!isConvertedToNonFinancialFlag) {
							    Float netPremium = enrollment.getGrossPremiumAmt() - updatedAptcAmt;

							    if(enrollment.getStateSubsidyAmt() != null){
                                    netPremium = netPremium - enrollment.getStateSubsidyAmt().floatValue();
                                }
								enrollment.setNetPremiumAmt(netPremium);
								if (!aptcEffectiveDate.before(enrollment.getBenefitEffectiveDate()) && !DateUtils
										.isSameDay(aptcEffectiveDate, enrollment.getBenefitEffectiveDate())) {
									/**
									 * The 15 Day logic is implemented at Individual portal end, they pass
									 * calculated date as per 15 Day Logic HIX-48237
									 */
									enrollment.setAptcEffDate(aptcEffectiveDate);
									enrollment.setNetPremEffDate(aptcEffectiveDate);

									if(enrollment.getStateSubsidyEffDate() != null){
										enrollment.setStateSubsidyEffDate(aptcEffectiveDate);
									}

								} else {
									enrollment.setAptcEffDate(enrollment.getBenefitEffectiveDate());
									enrollment.setNetPremEffDate(enrollment.getBenefitEffectiveDate());

									if(enrollment.getStateSubsidyEffDate() != null){
										enrollment.setStateSubsidyEffDate(enrollment.getBenefitEffectiveDate());
									}
								}

							} else if (isConvertedToNonFinancialFlag) {
								enrollment.setAptcEffDate(aptcEffectiveDate);
								enrollment.setNetPremiumAmt(enrollment.getGrossPremiumAmt());
								enrollment.setNetPremEffDate(aptcEffectiveDate);

								if(enrollment.getStateSubsidyEffDate() != null){
									enrollment.setStateSubsidyEffDate(aptcEffectiveDate);
								}
							}
							//HIX-118147 Setting effective date to all if use same effective date configuration is true
							if(useSameEffectiveDate) {
								enrollment.setGrossPremEffDate(enrollment.getAptcEffDate());
								if (enrollees != null && enrollees.size() > 0) {
									for(Enrollee enrollee : enrollees) {
										enrollee.setTotIndvRespEffDate(enrollment.getAptcEffDate());
									}
								}
							}
							boolean populateMonthlyPremium = Boolean
									.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
											EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
							if (populateMonthlyPremium) {
								enrollmentRequotingService.updateMonthlyPremiumsForAPTCSliderChange(enrollment);
							}
							enrollment.setUpdatedBy(loggedInUser);
						}
					}
				}
				if (!skippedEnrollmentMap.isEmpty()) {
					enrlResp.setSkippedEnrollmentMap(skippedEnrollmentMap);
				}
				// Save all Enrollments at end in one Transaction
				if (enrollmentList != null && !enrollmentList.isEmpty()
						&& (enrlResp.getStatus() == null || (null != enrlResp.getStatus()
								&& !enrlResp.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)))) {

					enrollmentList = this.saveAllEnrollment(enrollmentList);
					updateEnrollmentSsapId(existingSsapApplicationId, newSsapApplicationId);
					if (EnrollmentConfiguration.isCaCall()) {
						LOGGER.info("Async Call to enrollmentAhbxSyncService saveEnrollmentAhbxSync APTC_CHANGE");
						enrollmentAhbxSyncService.saveEnrollmentAhbxSync(enrollmentList, householdCaseId,
								EnrollmentAhbxSync.RecordType.APTC_CHANGE);
					}
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
				} else if (!skippedEnrollmentMap.isEmpty()) {
					updateEnrollmentSsapId(existingSsapApplicationId, newSsapApplicationId);
					enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENTS_SKIPPED);
				} else {
					throw new GIException(enrlResp.getErrCode(), enrlResp.getErrMsg(), EnrollmentConstants.HIGH);
				}
			} else {
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLEMENT_APTC_UPDATE_DTO_IS_NULL);
				LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLEMENT_APTC_UPDATE_DTO_IS_NULL);
			}
		} else {
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
			LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
		}
		return enrlResp;
	}

	/**
	 * Validates to check if Enrollment/s has "0 < Subscriber < 1"
	 * 
	 * @author Aditya_S
	 * @since 08/01/2015
	 * 
	 * @param enrollmentList
	 * @return
	 * @throws GIException
	 */
	public boolean validateEnrollmentForSubscriber(List<Enrollment> enrollmentList) throws GIException {
		boolean isSubscriberValid = false;
		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			for (Enrollment enrollment : enrollmentList) {
				int subscriberCount = 0;
				for (Enrollee enrollee : enrollment.getEnrollees()) {
					if (enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)) {
						subscriberCount++;
					}
				}

				if (subscriberCount <= 0) {
					LOGGER.error(EnrollmentConstants.ERR_MSG_NO_SUBSCRIBERS_FOR_ENROLLMENT
							+ EnrollmentConstants.HOUSEHOLD_CASE_ID + enrollment.getHouseHoldCaseId());
					throw new GIException(EnrollmentConstants.ERR_MSG_NO_SUBSCRIBERS_FOR_ENROLLMENT
							+ EnrollmentConstants.HOUSEHOLD_CASE_ID + enrollment.getHouseHoldCaseId());
				} else if (subscriberCount > 1) {
					LOGGER.error(EnrollmentConstants.ERR_MSG_MULTIPLE_SUBSCRIBERS_FOR_ENROLLMENT
							+ EnrollmentConstants.HOUSEHOLD_CASE_ID + enrollment.getHouseHoldCaseId());
					throw new GIException(EnrollmentConstants.ERR_MSG_MULTIPLE_SUBSCRIBERS_FOR_ENROLLMENT
							+ EnrollmentConstants.HOUSEHOLD_CASE_ID + enrollment.getHouseHoldCaseId());
				} else {
					isSubscriberValid = true;
				}
			}
		}
		return isSubscriberValid;
	}

	/**
	 * @author Aditya-S
	 * @since 29-01-2015
	 * 
	 *        Updated for HIX-75208
	 * @author panda_p
	 * @since 03-Sept-2015
	 * 
	 * @param applicationId
	 * @return
	 */
	@Override
	public boolean isActiveEnrollmentForApplicationId(Long applicationId, boolean useLookBackLogic) {
		Long count = null;
		Date tillDate = null;
		try {
			String daysToSubtrct = DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_SEP_DENIAL_DAYS);
			if (isNotNullAndEmpty(daysToSubtrct) && useLookBackLogic) {
				tillDate = EnrollmentUtils.getNdayBeforeDate(Long.parseLong(daysToSubtrct), new TSDate());
			} else if (useLookBackLogic) {
				tillDate = EnrollmentUtils.getNdayBeforeDate(EnrollmentConstants.SEVENTY, new TSDate());
			} else {
				tillDate = EnrollmentUtils.removeTimeFromDate(new TSDate());
			}

			count = enrollmentRepository.getActiveEnrollmentsForApplicationID(applicationId, tillDate);
		} catch (Exception ex) {
			LOGGER.error(EnrollmentConstants.ERR_MSG_FECTHING_ACTIVE_ENROLLMENT_COUNT_FOR_APP_ID);
		}

		return (count != null && count > 0) ? true : false;
	}

	/**
	 * @author Pratap
	 * @since 31-07-2015
	 * 
	 * @param ssapApplicationRequest
	 * @return
	 */
	@Override
	public boolean isActiveEnrollmentForApplicationId(Long applicationId, String coverageYear) {
		Long count = null;
		try {
			coverageYear = "12/31/" + coverageYear;
			count = enrollmentRepository.getActiveEnrollmentsForApplicationID(applicationId, coverageYear);
		} catch (Exception ex) {
			LOGGER.error(EnrollmentConstants.ERR_MSG_FECTHING_ACTIVE_ENROLLMENT_COUNT_FOR_APP_ID);
		}

		return (count != null && count > 0) ? true : false;
	}

	private void addBrokerInfo(List<Enrollment> enrollmentList, EnrollmentBrokerUpdateDTO brokerReq,
			final boolean sendToCarrierFlag, AccountUser user) throws GIException {
		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			boolean carrierSendFlag;
			for (Enrollment enrollment : enrollmentList) {
				enrollment.setAgentBrokerName(brokerReq.getAgentBrokerName());
				enrollment.setAssisterBrokerId(brokerReq.getAssisterBrokerId());
				if (!EnrollmentConfiguration.isMnCall()) {
					enrollment.setBrokerFEDTaxPayerId(brokerReq.getStateEINNumber());
				} else {
					enrollment.setBrokerFEDTaxPayerId("999999999");
				}
				// enrollment.setBrokerTPAFlag(brokerReq.getBrokerTPAFlag());
				if (!brokerReq.getRoleType().equalsIgnoreCase(EnrollmentConstants.ASSISTER_ROLE)) {
					enrollment.setBrokerTPAFlag(EnrollmentConstants.Y);
				}
				if (EnrollmentConfiguration.isNvCall()) {
					if (isNotNullAndEmpty(brokerReq.getAgentNPN())) {
						enrollment.setBrokerTPAAccountNumber1(brokerReq.getAgentNPN());
					}
					if (isNotNullAndEmpty(brokerReq.getStateLicenseNumber())) {
						enrollment.setBrokerTPAAccountNumber2(brokerReq.getStateLicenseNumber());
					}
				} else {
					if (isNotNullAndEmpty(brokerReq.getStateLicenseNumber())) {
						enrollment.setBrokerTPAAccountNumber1(brokerReq.getStateLicenseNumber());
					}
				}
				if (!EnrollmentConfiguration.isMnCall()) {
					enrollment.setBrokerRole(brokerReq.getRoleType());
				} else {
					enrollment.setBrokerRole(EnrollmentConstants.AGENT_ROLE);
				}
				enrollment.setUpdatedBy(user);
				for (Enrollee enrollee : enrollment.getEnrolleesAndSubscriber()) {
					carrierSendFlag = sendToCarrierFlag;
					if ((carrierSendFlag) && !validateEnrolleeForBrokerUpdate(enrollee)) {
						carrierSendFlag = false;
					}
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
							EnrollmentConstants.EVENT_REASON_LOOKUP_NO_REASON, user, carrierSendFlag,
							EnrollmentEvent.TRANSACTION_IDENTIFIER.BROKER_DESIGNATE);
				}
			}
		} else {
			if (!EnrollmentConfiguration.isMnCall()) {
				throw new GIException("No Enrollment found for Adding broker info");
			}
		}
	}

	private void removeBrokerInfo(List<Enrollment> enrollmentList, final boolean sendToCarrierFlag, AccountUser user)
			throws GIException {
		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			boolean carrierSendFlag;
			for (Enrollment enrollment : enrollmentList) {
				enrollment.setAgentBrokerName(null);
				enrollment.setAssisterBrokerId(null);
				enrollment.setBrokerFEDTaxPayerId(null);
				enrollment.setBrokerTPAFlag(null);
				enrollment.setBrokerTPAAccountNumber1(null);
				enrollment.setBrokerTPAAccountNumber2(null);
				enrollment.setBrokerRole(null);
				enrollment.setUpdatedBy(user);
				for (Enrollee enrollee : enrollment.getEnrolleesAndSubscriber()) {
					carrierSendFlag = sendToCarrierFlag;
					if ((carrierSendFlag) && !validateEnrolleeForBrokerUpdate(enrollee)) {
						carrierSendFlag = false;
					}
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
							EnrollmentConstants.EVENT_REASON_LOOKUP_NO_REASON, user, carrierSendFlag,
							EnrollmentEvent.TRANSACTION_IDENTIFIER.BROKER_DEDESIGNATE);
				}
			}
		} else {
			if (!EnrollmentConfiguration.isMnCall()) {
				throw new GIException("No Enrollment found for Removing broker info");
			}
		}
	}

	@Override
	@Transactional
	public void updateBrokerDetails(EnrollmentBrokerUpdateDTO brokerReq, EnrollmentResponse response) {
		try {
			String todaysDateStr = DateUtil.dateToString(new TSDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);

			AccountUser user = null;
			if (brokerReq != null && brokerReq.getUser() != null) {
				user = brokerReq.getUser();
			} else {
				user = getLoggedInUser();
				if (user.getId() == 0) {
					user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
				}
			}
			List<Enrollment> enrollmentList = null;
			boolean sendToCarrier = false;
			if (!brokerReq.getRoleType().equalsIgnoreCase(EnrollmentConstants.ASSISTER_ROLE)) {
				sendToCarrier = true;
			}
			if (brokerReq.getMarketType().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
				if (brokerReq.getAddremoveAction().equalsIgnoreCase(EnrollmentConstants.ACTION_ADD)) {
					// code to add broker info
					if (StringUtils.isNotBlank(brokerReq.getHouseHoldCaseId())) {
						enrollmentList = enrollmentRepository
								.getIndivEnrollmentToAddBroker(brokerReq.getHouseHoldCaseId(), todaysDateStr);
					} else if (StringUtils.isNotBlank(brokerReq.getExchgIndividualIdentifier())) {
						enrollmentList = enrollmentRepository.getIndivEnrollmentByExchgIdentiferToAddBroker(
								brokerReq.getExchgIndividualIdentifier(), todaysDateStr);
					} else if (brokerReq.getEnrollmentId() != null) {
						Enrollment enrollment = enrollmentRepository.findById(brokerReq.getEnrollmentId());
						if (enrollment != null) {
							enrollment.setEnrollees(enrolleeRepository.getEnrolleesForEnrollmentID(enrollment.getId()));
							enrollmentList = new ArrayList<>();
							enrollmentList.add(enrollment);
						}

					}
					addBrokerInfo(enrollmentList, brokerReq, sendToCarrier, user);
				} else if (brokerReq.getAddremoveAction().equalsIgnoreCase(EnrollmentConstants.ACTION_REMOVE)) {
					// code to remove broker info

					if (StringUtils.isNotBlank(brokerReq.getHouseHoldCaseId())) {
						if (EnrollmentConfiguration.isMnCall()) {
							enrollmentList = enrollmentRepository
									.getIndivEnrollmentToAddBroker(brokerReq.getHouseHoldCaseId(), todaysDateStr);
						} else {
							enrollmentList = enrollmentRepository.getIndivEnrollmentToRemoveBroker(
									brokerReq.getHouseHoldCaseId(), todaysDateStr, brokerReq.getRoleType(),
									brokerReq.getAssisterBrokerId());
						}
					} else if (StringUtils.isNotBlank(brokerReq.getExchgIndividualIdentifier())) {
						enrollmentList = enrollmentRepository.getIndivEnrollmentByExchgIdentifierToRemoveBroker(
								brokerReq.getExchgIndividualIdentifier(), todaysDateStr, brokerReq.getRoleType(),
								brokerReq.getAssisterBrokerId());
					}
					removeBrokerInfo(enrollmentList, sendToCarrier, user);
				}
			} else if (brokerReq.getMarketType().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)) {
				if (brokerReq.getAddremoveAction().equalsIgnoreCase(EnrollmentConstants.ACTION_ADD)) {
					// code to add broker info
					enrollmentList = enrollmentRepository
							.getShopEnrollmentToAddBroker(brokerReq.getEmployerEnrollmentId(), todaysDateStr);
					addBrokerInfo(enrollmentList, brokerReq, sendToCarrier, user);
					List<GroupInstallation> groupList = groupInstallationRepository
							.getGroupByEmpEnrlId(brokerReq.getEmployerEnrollmentId());
					if (groupList != null && !groupList.isEmpty()) {
						for (GroupInstallation grp : groupList) {
							grp.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y);
							grp.setBrokerId(brokerReq.getAssisterBrokerId());
						}
						groupInstallationRepository.save(groupList);
					}
					// addBrokerToGrpInstall();
				} else if (brokerReq.getAddremoveAction().equalsIgnoreCase(EnrollmentConstants.ACTION_REMOVE)) {
					enrollmentList = enrollmentRepository.getShopEnrollmentToRemoveBroker(
							brokerReq.getEmployerEnrollmentId(), todaysDateStr, brokerReq.getRoleType(),
							brokerReq.getAssisterBrokerId());
					removeBrokerInfo(enrollmentList, sendToCarrier, user);
					List<GroupInstallation> groupList = groupInstallationRepository.getGroupByEmpEnrlIdAndBrokerId(
							brokerReq.getEmployerEnrollmentId(), brokerReq.getAssisterBrokerId());
					// removeBrokerFromGrpInstall();
					if (groupList != null && !groupList.isEmpty()) {
						for (GroupInstallation grp : groupList) {
							grp.setIsChanged(EnrollmentConstants.GROUP_ENROLLMENT_STATUS_Y);
							grp.setBrokerId(null);
						}
						groupInstallationRepository.save(groupList);
					}
				}
			}
			if (enrollmentList != null && !enrollmentList.isEmpty()) {
				enrollmentRepository.save(enrollmentList);
			}

			response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			response.setErrMsg(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);
			response.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} catch (GIException ex) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			response.setErrMsg(
					"Failed to update broker details request: " + EnrollmentUtils.shortenedStackTrace(ex, 4));
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		} catch (Exception e) {
			LOGGER.error("Exception caught in updateBrokerDetails: ", e);
			response.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
	}

	@Override
	public String findEnrollmentStatusByEmployeeId(Integer employeeId) {
		return enrollmentRepository.findEnrollmentStatusByEmployeeId(employeeId);
	}

	/**
	 * @author parhi_s
	 * 
	 *         Returns onlly Enrollees for Enrollment, Returns all Active and
	 *         Non-Active Enrolles.
	 * 
	 * @return
	 */
	@Override
	public List<Enrollee> getEnrolleesOnly(Enrollment enrollment) {
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		List<Enrollee> enrollees = enrollment.getEnrollees();
		if (enrollees != null && !enrollees.isEmpty()) {
			for (Enrollee enrollee : enrollees) {
				if (enrollee.getPersonTypeLkp() != null && (enrollee.getPersonTypeLkp().getLookupValueCode()
						.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE))) {
					enrolleeList.add(enrollee);
				}
			}
		}
		return enrolleeList;
	}

	/**
	 * @author parhi_s
	 * 
	 *         Returns Subscriber and Enrollees for Enrollment, Returns all Active
	 *         and Non-Active Enrolles.
	 * 
	 * @return
	 */
	@Override
	public List<Enrollee> getEnrolleesAndSubscriber(Enrollment enrollment) {
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		List<Enrollee> enrollees = enrollment.getEnrollees();
		if (enrollees != null && !enrollees.isEmpty()) {
			for (Enrollee enrollee : enrollees) {
				if (enrollee.getPersonTypeLkp() != null && (enrollee.getPersonTypeLkp().getLookupValueCode()
						.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)
						|| enrollee.getPersonTypeLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))) {
					enrolleeList.add(enrollee);
				}
			}
		}
		return enrolleeList;
	}

	@Override
	public EnrollmentResponse getEnrollmentDataForBroker(String enrollmentId) {
		List<Object[]> enrollmentInfoList = null;
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if (NumberUtils.isNumber(enrollmentId)) {
			try {
				enrollmentInfoList = enrollmentRepository.getEnrollmentDataForBroker(Integer.valueOf(enrollmentId));
				if (null != enrollmentInfoList && !enrollmentInfoList.isEmpty()) {
					Object[] enrollmentInfo = enrollmentInfoList.get(0);
					enrollmentResponse.setEnrollmentStatusLabel((String) enrollmentInfo[EnrollmentConstants.ZERO]);
					enrollmentResponse.setPlanName((String) enrollmentInfo[EnrollmentConstants.ONE]);
					enrollmentResponse.setEffectiveEndDate((Date) enrollmentInfo[EnrollmentConstants.TWO]);
					enrollmentResponse.setEmployerContribution((Float) enrollmentInfo[EnrollmentConstants.THREE]);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				} else {
					LOGGER.error("No data for enrollment Id :: " + enrollmentId);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrMsg("No data for enrollment Id :: " + enrollmentId);
				}
			} catch (Exception e) {
				LOGGER.error("Exception @ getEnrollmentDataForBroker :: " + e.getMessage(), e);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg(e.getMessage());
			}
		} else {
			LOGGER.error("Invalid enrollment Id received :: " + enrollmentId);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Invalid enrollment ID" + enrollmentId);
		}
		return enrollmentResponse;
	}

	@Override
	public List<Enrollment> updateDentalAPTCOnHealthDisEnrollment(Enrollment healthEnrollment, AccountUser loggedInUser,
			EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) throws GIException {
		List<Enrollment> dentalEnrollments = null;
		try {
			if (healthEnrollment != null) {
				Date effDate = null;
				if (DateUtils.isSameDay(healthEnrollment.getBenefitEndDate(),
						healthEnrollment.getBenefitEffectiveDate())) {
					effDate = healthEnrollment.getBenefitEffectiveDate();
				} else {
					effDate = EnrollmentUtils.getNextMonthStartDate(healthEnrollment.getBenefitEndDate());
				}
				dentalEnrollments = enrollmentRepository
						.findIndividualDentalEnrollment(healthEnrollment.getHouseHoldCaseId(), effDate);
				if (dentalEnrollments != null && dentalEnrollments.size() > 0) {
					LookupValue eventTypeLookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(
							EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CHANGE);
					LookupValue eventTypeReasonLookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(
							EnrollmentConstants.EVENT_REASON, EnrollmentConstants.EVENT_REASON_LOOKUP_NO_REASON);
					for (Enrollment enr : dentalEnrollments) {
						enr.setAptcAmt(null);
						enr.setAptcEffDate(effDate);
						enr.setNetPremiumAmt(enr.getGrossPremiumAmt());
						enr.setNetPremEffDate(effDate);
						Enrollee subscriber = enr.getSubscriberForEnrollment();
						if (subscriber != null && !subscriber.isEventCreated()) {
							EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
							enrollmentEvent.setEnrollee(subscriber);
							enrollmentEvent.setEnrollment(enr);
							enrollmentEvent.setEventTypeLkp(eventTypeLookupValue);
							enrollmentEvent.setEventReasonLkp(eventTypeReasonLookupValue);
							if (txn_idfier != null) {
								enrollmentEvent.setTxnIdentifier(txn_idfier.toString());
							}

							// Set Created User
							enrollmentEvent.setCreatedBy(loggedInUser);
							enrollmentEvent.setUpdatedBy(loggedInUser);
							subscriber.setLastEventId(enrollmentEvent);
							subscriber.setEventCreated(true);
						}
						enr.setAPTCUpdated(true);
						enrollmentRequotingService.updateMonthlyPremiumsForAPTCSliderChange(enr);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred in updating Dental APTC, updateDentalAPTCOnHealthDisEnrollment(-,-): ", e);
			throw new GIException("Failed to nullify dental APTC on Health Disenrollment of enrollment id :: "
					+ healthEnrollment.getId() + " :: " + e.getMessage());
		}
		return dentalEnrollments;
	}

	@Override
	public Map<String, List<EnrollmentDataDTO>> getEnrollmentDataForBrokerByHouseHoldCaseIds(
			EnrollmentRequest enrollmentRequest) throws GIException {
		Map<String, List<EnrollmentDataDTO>> listOfEnrollmentDataDtomap = new HashMap<String, List<EnrollmentDataDTO>>();
		List<String> personTypeList = new ArrayList<String>();
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON);
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT);
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT);
		Date systemDate = new java.util.Date();
		List<String> statusList = new ArrayList<String>();
		statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
		statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED);
		statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_TERM);
		Set<String> houseHoldIdSet = new HashSet<String>(enrollmentRequest.getHouseHoldCaseIdList());
		// Removing duplicate Household from Request
		enrollmentRequest.getHouseHoldCaseIdList().clear();
		enrollmentRequest.getHouseHoldCaseIdList().addAll(houseHoldIdSet);

		List<List<String>> houseHoldIdSplitList = EnrollmentUtils.splitListIntoSubList(
				enrollmentRequest.getHouseHoldCaseIdList(), EnrollmentConstants.HOUSEHOLD_CASE_ID_SPLIT_LIST_COUNT);

		if (houseHoldIdSplitList != null && !houseHoldIdSplitList.isEmpty()) {
			for (List<String> houseHoldCaseIdList : houseHoldIdSplitList) {
				List<Object[]> enrollmentData = enrollmentRepository.getEnrollmentDataForBrokerByHouseHoldCaseId(
						houseHoldCaseIdList, statusList, systemDate, personTypeList);
				if (enrollmentData != null && !enrollmentData.isEmpty()) {
					for (Object[] enrollmentColData : enrollmentData) {
						EnrollmentDataDTO enrollmentDataDto = new EnrollmentDataDTO();
						enrollmentDataDto.setEnrollmentId((Integer) enrollmentColData[EnrollmentConstants.ZERO]);
						enrollmentDataDto.setPlanId((Integer) enrollmentColData[EnrollmentConstants.ONE]);
						enrollmentDataDto.setPlanType((String) enrollmentColData[EnrollmentConstants.TWO]);
						enrollmentDataDto.setNetPremiumAmt((Float) enrollmentColData[EnrollmentConstants.THREE]);
						enrollmentDataDto.setHouseHoldCaseId((String) enrollmentColData[EnrollmentConstants.FOUR]);
						enrollmentDataDto.setSsapApplicationId((Long) enrollmentColData[EnrollmentConstants.FIVE]);
						enrollmentDataDto.setGrossPremiumAmt((Float) enrollmentColData[EnrollmentConstants.SIX]);
						enrollmentDataDto.setAptcAmount((Float) enrollmentColData[EnrollmentConstants.SEVEN]);
						enrollmentDataDto.setNumberOfEnrollees(enrollmentColData[EnrollmentConstants.EIGHT] != null
								? ((int) (long) enrollmentColData[EnrollmentConstants.EIGHT])
								: null);

						// enrollmentDataDtoMap.get((Integer)enrolleeColData[EnrollmentConstants.ONE]).setNumberOfEnrollees((int)(long)enrolleeColData[EnrollmentConstants.ZERO]);

						List<EnrolleeDataDTO> enrolleeDataDtoList = new ArrayList<EnrolleeDataDTO>();
						enrollmentDataDto.setEnrolleeDataDtoList(enrolleeDataDtoList);
						if (listOfEnrollmentDataDtomap.isEmpty()) {
							List<EnrollmentDataDTO> enrollmentDataDtoList = new ArrayList<EnrollmentDataDTO>();
							enrollmentDataDtoList.add(enrollmentDataDto);
							listOfEnrollmentDataDtomap.put(enrollmentDataDto.getHouseHoldCaseId(),
									enrollmentDataDtoList);
						} else {
							if (listOfEnrollmentDataDtomap.containsKey(enrollmentDataDto.getHouseHoldCaseId())) {
								boolean isContainsEnrollment = false;
								for (EnrollmentDataDTO enrollmentDataDtoObj : listOfEnrollmentDataDtomap
										.get(enrollmentDataDto.getHouseHoldCaseId())) {
									if (enrollmentDataDtoObj.getEnrollmentId() == enrollmentDataDto.getEnrollmentId()) {
										isContainsEnrollment = true;
									}
								}

								if (!isContainsEnrollment) {
									listOfEnrollmentDataDtomap.get(enrollmentDataDto.getHouseHoldCaseId())
											.add(enrollmentDataDto);
								}
							} else {
								List<EnrollmentDataDTO> enrollmentDataDtoList = new ArrayList<EnrollmentDataDTO>();
								enrollmentDataDtoList.add(enrollmentDataDto);
								listOfEnrollmentDataDtomap.put(enrollmentDataDto.getHouseHoldCaseId(),
										enrollmentDataDtoList);
							}
						}
					}
				} else {
					/*
					 * Throws GIException for no enrollment found.
					 */
					throw new GIException(EnrollmentConstants.ERROR_CODE_202,
							EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND, EnrollmentConstants.HIGH);
				}
			}
		}
		return listOfEnrollmentDataDtomap;
	}

	/**
	  * 
	  */
	@Override
	public Map<String, List<EnrollmentDataDTO>> getEnrollmentDataByHouseHoldCaseIds(EnrollmentRequest enrollmentRequest)
			throws GIException {

		Map<String, List<EnrollmentDataDTO>> listOfEnrollmentDataDtomap = new HashMap<String, List<EnrollmentDataDTO>>();
		Map<Integer, EnrollmentDataDTO> enrollmentDataDtoMap = new HashMap<Integer, EnrollmentDataDTO>();
		boolean activeEnrolleeRequired = Boolean.FALSE;
		List<String> statusList = new ArrayList<String>();
		List<String> personTypeList = new ArrayList<String>();
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON);
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT);
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT);
		Date systemDate = new java.util.Date();
		List<Object[]> enrollmentData = null;

		if (enrollmentRequest.getIdList() != null && !enrollmentRequest.getIdList().isEmpty()) {

			enrollmentData = enrollmentRepository.getEnrollmentDataByld(enrollmentRequest.getIdList());

		} else if (enrollmentRequest.getEnrollmentStatusList() != null
				&& !enrollmentRequest.getEnrollmentStatusList().isEmpty()) {

			if (enrollmentRequest.getEnrollmentStatusList().contains(EnrollmentRequest.EnrollmentStatus.TERM_ACTIVE)) {
				/*
				 * Term-Active Status
				 */
				activeEnrolleeRequired = Boolean.TRUE;
				statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
				statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED);
				statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_TERM);
				enrollmentData = enrollmentRepository.getActiveEnrollmentDataByHouseHoldCaseId(
						enrollmentRequest.getHouseHoldCaseIdList(), statusList, systemDate);
			} else {

				for (EnrollmentStatus status : enrollmentRequest.getEnrollmentStatusList()) {
					statusList.add(status.toString());
				}
				// TODO: Remove the CA block once data migration is done, i.e once we have
				// corresponding CMR households and SSAPs available in CA env also.
				if (EnrollmentConfiguration.isCaCall()) {

					if (StringUtils.isNotEmpty(enrollmentRequest.getCoverageYear())) {
						enrollmentData = enrollmentRepository.getEnrollmentDataByHouseHoldCaseIdNCoverageYearNStatusCA(
								enrollmentRequest.getHouseHoldCaseIdList(), statusList,
								enrollmentRequest.getCoverageYear());
					} else {
						enrollmentData = enrollmentRepository.getEnrollmentDataByHouseHoldCaseIdNStatusCA(
								enrollmentRequest.getHouseHoldCaseIdList(), statusList);
					}
				} else if (StringUtils.isNotEmpty(enrollmentRequest.getCoverageYear())) {

					// StatusList with CoverageYear
					enrollmentData = enrollmentRepository.getEnrollmentDataByHouseHoldCaseIdNCoverageYearNStatus(
							enrollmentRequest.getHouseHoldCaseIdList(), statusList,
							enrollmentRequest.getCoverageYear());
				} else {
					/*
					 * StatusList without CoverageYear
					 */
					enrollmentData = enrollmentRepository.getEnrollmentDataByHouseHoldCaseIdNStatus(
							enrollmentRequest.getHouseHoldCaseIdList(), statusList);
				}
			}

		} else if (StringUtils.isNotEmpty(enrollmentRequest.getCoverageYear())) {
			/*
			 * Since statusList is empty populating status
			 */
			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_PENDING);
			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM);
			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_TERM);
			enrollmentData = enrollmentRepository.getEnrollmentDataByHouseHoldCaseIdNCoverageYearNStatus(
					enrollmentRequest.getHouseHoldCaseIdList(), statusList, enrollmentRequest.getCoverageYear());
		} else {
			/*
			 * Coverage Year null or empty, fetch enrollment data using only
			 * householdcaseIDs
			 */
			enrollmentData = enrollmentRepository
					.getEnrollmentDataByHouseHoldCaseId(enrollmentRequest.getHouseHoldCaseIdList());
		}

		Map<String, Map<String, String>> sepEventDetailMap = new HashMap<>();
		if (null != enrollmentRequest.getHouseHoldCaseIdList() && !enrollmentRequest.getHouseHoldCaseIdList().isEmpty()
				&& enrollmentRequest.isSepDataRequired()) {
			for (String householdCaseId : enrollmentRequest.getHouseHoldCaseIdList()) {
				sepEventDetailMap.put(householdCaseId, enrollmentExternalRestUtil
						.fetchSepEventDetailsByHousehold(householdCaseId, enrollmentRequest.getCoverageYear()));
				String dentalStatus = "false";
				if (null != sepEventDetailMap.get(householdCaseId)
						&& NumberUtils.isNumber(sepEventDetailMap.get(householdCaseId).get("ssapApplicationId"))) {
					String ssapApplicationId = sepEventDetailMap.get(householdCaseId).get("ssapApplicationId");
					List<String> dentalSelection = enrollmentRepository.getDentalPlanSelectionStatusByHousehold(
							householdCaseId, enrollmentRequest.getCoverageYear(), Long.valueOf(ssapApplicationId),
							new PageRequest(0, 1));
					if (null != dentalSelection && !dentalSelection.isEmpty()) {
						dentalStatus = dentalSelection.get(0);
					}
					sepEventDetailMap.get(householdCaseId).put("dentalEnrollmentStatus", dentalStatus);
				}
			}
		}

		if (enrollmentData != null && !enrollmentData.isEmpty()) {

			for (Object[] enrollmentColData : enrollmentData) {

				EnrollmentDataDTO enrollmentDataDto = new EnrollmentDataDTO();
				enrollmentDataDto.setEnrollmentId((Integer) enrollmentColData[EnrollmentConstants.ZERO]);
				enrollmentDataDto.setPlanId((Integer) enrollmentColData[EnrollmentConstants.ONE]);
				enrollmentDataDto.setHouseHoldCaseId((String) enrollmentColData[EnrollmentConstants.TWO]);
				enrollmentDataDto
						.setEnrollmentBenefitEffectiveStartDate((Date) enrollmentColData[EnrollmentConstants.THREE]);
				enrollmentDataDto
						.setEnrollmentBenefitEffectiveEndDate((Date) enrollmentColData[EnrollmentConstants.FOUR]);
				enrollmentDataDto.setGrossPremiumAmt((Float) enrollmentColData[EnrollmentConstants.FIVE]);
				enrollmentDataDto.setNetPremiumAmt((Float) enrollmentColData[EnrollmentConstants.SIX]);
				enrollmentDataDto.setEnrollmentStatus((String) enrollmentColData[EnrollmentConstants.SEVEN]);
				enrollmentDataDto.setCreationTimestamp((Date) enrollmentColData[EnrollmentConstants.EIGHT]);
				enrollmentDataDto.setPlanType((String) enrollmentColData[EnrollmentConstants.NINE]);
				enrollmentDataDto.setPlanLevel((String) enrollmentColData[EnrollmentConstants.TEN]);
				enrollmentDataDto.setPlanName((String) enrollmentColData[EnrollmentConstants.ELEVEN]);
				enrollmentDataDto.setCarrierName((String) enrollmentColData[EnrollmentConstants.TWELVE]);

				if (null != enrollmentColData[EnrollmentConstants.THIRTEEN]
						&& NumberUtils.isNumber((String) enrollmentColData[EnrollmentConstants.THIRTEEN])) {
					enrollmentDataDto
							.setCarrierId(Integer.valueOf((String) enrollmentColData[EnrollmentConstants.THIRTEEN]));
				}

				enrollmentDataDto.setAptcAmount((Float) enrollmentColData[EnrollmentConstants.FOURTEEN]);

				if (enrollmentColData[EnrollmentConstants.FIFTEEN] != null) {
					enrollmentDataDto.setRenewalFlag((String) enrollmentColData[EnrollmentConstants.FIFTEEN]);
					enrollmentDataDto.setPriorEnrollmentId((Long) enrollmentColData[EnrollmentConstants.SIXTEEN]);
				}

				// HIX-77014
				String cmsPlanId = (String) enrollmentColData[EnrollmentConstants.SEVENTEEN];

				if (StringUtils.isNotEmpty(cmsPlanId)) {
					enrollmentDataDto.setCMSPlanID(cmsPlanId);
					enrollmentDataDto.setCsrLevel(cmsPlanId.substring(cmsPlanId.length() - 2, cmsPlanId.length()));
				}

				enrollmentDataDto.setLastUpdateDate((Date) enrollmentColData[EnrollmentConstants.EIGHTEEN]);
				Integer lastUpdatedById = (Integer) enrollmentColData[EnrollmentConstants.NINETEEN];
				enrollmentDataDto.setLastUpdatedBy(lastUpdatedById);
				enrollmentDataDto.setSubmittedToCarrierDate((Date) enrollmentColData[EnrollmentConstants.TWENTY]);
				Integer lastCreatedById = (Integer) enrollmentColData[EnrollmentConstants.TWENTY_ONE];
				enrollmentDataDto.setCreatedBy(lastCreatedById);
				// HIX-71635 Add additional fields for CAP
				enrollmentDataDto.setCsrAmount((Float) enrollmentColData[EnrollmentConstants.TWENTY_TWO]);
				enrollmentDataDto.setTransactionId((String) enrollmentColData[EnrollmentConstants.TWENTY_THREE]);
				if (isNotNullAndEmpty(enrollmentColData[EnrollmentConstants.TWENTY_FOUR])) {
					enrollmentDataDto.setSsapApplicationId((Long) enrollmentColData[EnrollmentConstants.TWENTY_FOUR]);
				}
				if (EnrollmentConfiguration.isCaCall() && null != enrollmentDataDto.getSsapApplicationId()) {
					enrollmentDataDto.setSsapCaseNumber(
							enrollmentRepository.getCaseNumberFromSsap(enrollmentDataDto.getSsapApplicationId()));
				} else {
					enrollmentDataDto.setSsapCaseNumber((String) enrollmentColData[EnrollmentConstants.TWENTY_FIVE]);
				}
				/** HIX-73087 Populate the data in enrollment detail API */
				enrollmentDataDto.setLastUpdatedUserName((String) enrollmentColData[EnrollmentConstants.TWENTY_SIX]);
				enrollmentDataDto.setSubmittedByUserName((String) enrollmentColData[EnrollmentConstants.TWENTY_SEVEN]);

				if ((int) enrollmentColData[EnrollmentConstants.TWENTY_EIGHT] > 0) {
					enrollmentDataDto.setIssuerId(String.valueOf(enrollmentColData[EnrollmentConstants.TWENTY_EIGHT]));
				}

				enrollmentDataDto
						.setEnrollmentConfirmationDate((Date) enrollmentColData[EnrollmentConstants.TWENTY_NINE]);
				enrollmentDataDto.setEhbSlcspAmt((Float) enrollmentColData[EnrollmentConstants.THIRTY]);
				enrollmentDataDto.setEsEhbPrmAmt((Float) enrollmentColData[EnrollmentConstants.THIRTY_ONE]);

				if (EnrollmentConstants.INSURANCE_TYPE_HEALTH.equalsIgnoreCase(enrollmentDataDto.getPlanType())) {
					enrollmentDataDto.setEhbPercentage((Float) enrollmentColData[EnrollmentConstants.THIRTY_THREE]);
				} else {
					enrollmentDataDto.setDntlEhbAmt((Float) enrollmentColData[EnrollmentConstants.THIRTY_TWO]);
					enrollmentDataDto.setEhbPercentage((Float) enrollmentColData[EnrollmentConstants.THIRTY_FOUR]);
				}
				// HIX-106484
				enrollmentDataDto.setAgentName((String) enrollmentColData[EnrollmentConstants.THIRTY_FIVE]);
				enrollmentDataDto.setAgentId((Integer) enrollmentColData[EnrollmentConstants.THIRTY_SIX]);
				enrollmentDataDto.setAgentTpaNumber((String) enrollmentColData[EnrollmentConstants.THIRTY_SEVEN]);
				if (isNotNullAndEmpty(enrollmentColData[EnrollmentConstants.THIRTY_EIGHT])) {
					enrollmentDataDto
							.setPriorSsapApplicationId((Long) enrollmentColData[EnrollmentConstants.THIRTY_EIGHT]);
					List<GIWSPayload> payloadList = giWSPayloadService.findByEndpointFunctionAndSSAPID(
							"ERP-TRANSFERACCOUNT", enrollmentDataDto.getPriorSsapApplicationId());
					if (null != payloadList && !payloadList.isEmpty()) {
						enrollmentDataDto.setCorrelationId(payloadList.get(0).getCorrelationId());
					}
				}
				if (isNotNullAndEmpty(enrollmentColData[EnrollmentConstants.THIRTY_NINE])) {
					enrollmentDataDto.setAllowChangePlan((String) enrollmentColData[EnrollmentConstants.THIRTY_NINE]);
					;
				}
				if (isNotNullAndEmpty(enrollmentColData[EnrollmentConstants.FORTY])) {
					enrollmentDataDto.setExternalHouseHoldCaseId((String) enrollmentColData[EnrollmentConstants.FORTY]);
					;
				}
				if(isNotNullAndEmpty(enrollmentColData[EnrollmentConstants.FORTY_ONE])){
					enrollmentDataDto.setStateSubsidyAmount((BigDecimal) enrollmentColData[EnrollmentConstants.FORTY_ONE]);
				}

				Enrollee responsiblePerson = enrolleeService
						.getResponsiblePersonByEnrollmentID(enrollmentDataDto.getEnrollmentId());

				if (isNotNullAndEmpty(responsiblePerson)) {
					enrollmentDataDto.setPtfFirstName(
							responsiblePerson.getFirstName() != null ? responsiblePerson.getFirstName() : null);
					enrollmentDataDto.setPtfLastName(
							responsiblePerson.getLastName() != null ? responsiblePerson.getLastName() : null);
					enrollmentDataDto.setPtfMiddleName(
							responsiblePerson.getMiddleName() != null ? responsiblePerson.getMiddleName() : null);
				}

				/**
				 * Fetching LastUpdatedBy RoleName
				 */
				/*
				 * AccountUser accountUserLastUpdatedBy = new AccountUser();
				 * accountUserLastUpdatedBy.setId(lastUpdatedById); List<String>
				 * lastUpdatedByUserRoleList =
				 * userRoleService.findUserRoleNames(accountUserLastUpdatedBy);
				 * if(lastUpdatedByUserRoleList != null && !lastUpdatedByUserRoleList.isEmpty())
				 * {
				 * enrollmentDataDto.setLastUpdatedRoleName(lastUpdatedByUserRoleList.toString()
				 * ); }
				 */

				AccountUser lastUpdatedByUser = userService.findById(lastUpdatedById);

				if (null != lastUpdatedByUser) {
					Role defauleLastUpdateRole = userService.getDefaultRole(lastUpdatedByUser);
					if (null != defauleLastUpdateRole && null != defauleLastUpdateRole.getName()) {
						enrollmentDataDto.setLastUpdatedRoleName(defauleLastUpdateRole.getName());
					}
				}

				/**
				 * Fetching LastCreatedBy RoleName
				 */
				/*
				 * AccountUser accountUserLastCreatedBy = new AccountUser();
				 * accountUserLastCreatedBy.setId(lastCreatedById); List<String>
				 * lastCreatedByUserRoleList =
				 * userRoleService.findUserRoleNames(accountUserLastCreatedBy);
				 * if(lastCreatedByUserRoleList != null && !lastCreatedByUserRoleList.isEmpty())
				 * {
				 * enrollmentDataDto.setSubmittedByRoleName(lastCreatedByUserRoleList.toString()
				 * ); }
				 */

				AccountUser submittedByUser = userService.findById(lastCreatedById);

				if (null != submittedByUser) {
					Role defauleSubmittedByUserRole = userService.getDefaultRole(submittedByUser);
					if (null != defauleSubmittedByUserRole && null != defauleSubmittedByUserRole.getName()) {
						enrollmentDataDto.setSubmittedByRoleName(defauleSubmittedByUserRole.getName());
					}
				}
				// HIX-109341 Set SEP Info
				setSepInfo(enrollmentDataDto, sepEventDetailMap);

				List<EnrolleeDataDTO> enrolleeDataDtoList = new ArrayList<EnrolleeDataDTO>();
				enrollmentDataDto.setEnrolleeDataDtoList(enrolleeDataDtoList);
				enrollmentDataDtoMap.put(enrollmentDataDto.getEnrollmentId(), enrollmentDataDto);

				if (listOfEnrollmentDataDtomap.isEmpty()) {

					List<EnrollmentDataDTO> enrollmentDataDtoList = new ArrayList<EnrollmentDataDTO>();
					enrollmentDataDtoList.add(enrollmentDataDto);
					listOfEnrollmentDataDtomap.put(enrollmentDataDto.getHouseHoldCaseId(), enrollmentDataDtoList);

				} else {

					if (listOfEnrollmentDataDtomap.containsKey(enrollmentDataDto.getHouseHoldCaseId())) {

						listOfEnrollmentDataDtomap.get(enrollmentDataDto.getHouseHoldCaseId()).add(enrollmentDataDto);
					} else {
						List<EnrollmentDataDTO> enrollmentDataDtoList = new ArrayList<EnrollmentDataDTO>();
						enrollmentDataDtoList.add(enrollmentDataDto);
						listOfEnrollmentDataDtomap.put(enrollmentDataDto.getHouseHoldCaseId(), enrollmentDataDtoList);
					}
				}

				List<Object[]> premiumEffDates = null;

				if (enrollmentDataDto.getEnrollmentId() != null && enrollmentDataDto.getEnrollmentId() > 0) {

					premiumEffDates = enrollmentRepository.getPremiumEffDates(enrollmentDataDto.getEnrollmentId());
					Date grossPremEffDate = null;
					Date netPremEffDate = null;
					Date aptcEffDate = null;
					Date maxPremEffDate = null;

					if (premiumEffDates != null) {

						for (Object[] dateData : premiumEffDates) {

							if (dateData != null) {
								grossPremEffDate = (Date) dateData[EnrollmentConstants.ZERO];
								netPremEffDate = (Date) dateData[EnrollmentConstants.ONE];
								if (dateData.length > 2) {
									aptcEffDate = (Date) dateData[EnrollmentConstants.TWO];
								}
							}

							if (grossPremEffDate != null) {
								maxPremEffDate = grossPremEffDate;
								if (netPremEffDate != null && netPremEffDate.after(grossPremEffDate)) {
									maxPremEffDate = netPremEffDate;
								}
								if (aptcEffDate != null && aptcEffDate.after(maxPremEffDate)) {
									maxPremEffDate = aptcEffDate;
								}
							} else if (netPremEffDate != null) {
								maxPremEffDate = netPremEffDate;
								if (aptcEffDate != null && aptcEffDate.after(netPremEffDate)) {
									maxPremEffDate = aptcEffDate;
								}
							}

							if (maxPremEffDate != null) {
								enrollmentDataDto.setPremiumEffDate(
										DateUtil.dateToString(maxPremEffDate, GhixConstants.REQUIRED_DATE_FORMAT));
							}
						}
					}
				} // End of premiumEffDates setting
			}

			if (enrollmentDataDtoMap != null && !enrollmentDataDtoMap.isEmpty()) {
				List<Integer> enrollmentIdList = new ArrayList<Integer>();
				enrollmentIdList.addAll(enrollmentDataDtoMap.keySet());
				List<Object[]> enrolleeData = null;
				if (activeEnrolleeRequired) {
					enrolleeData = enrolleeService.findActiveEnrolleeDataByListOfEnrollmentIds(enrollmentIdList,
							personTypeList, systemDate);

				} else {
					enrolleeData = enrolleeService.getEnrolleeDataByListOfEnrollmentIdAndPersonType(enrollmentIdList,
							personTypeList);
				}

				for (Object[] enrolleeColData : enrolleeData) {
					EnrolleeDataDTO enrolleeDataDto = new EnrolleeDataDTO();
					enrolleeDataDto.setFirstName((String) enrolleeColData[EnrollmentConstants.ZERO]);
					enrolleeDataDto.setLastName((String) enrolleeColData[EnrollmentConstants.ONE]);
					enrolleeDataDto.setEnrolleeEffectiveStartDate((Date) enrolleeColData[EnrollmentConstants.TWO]);
					enrolleeDataDto.setEnrolleeEffectiveEndDate((Date) enrolleeColData[EnrollmentConstants.THREE]);
					enrolleeDataDto.setEnrollmentId((Integer) enrolleeColData[EnrollmentConstants.FOUR]);
					enrolleeDataDto.setCoveragePolicyNumber((String) enrolleeColData[EnrollmentConstants.FIVE]);

					enrolleeDataDto.setRelationshipToSubscriber(enrolleeColData[EnrollmentConstants.SIX] != null
							? (String) enrolleeColData[EnrollmentConstants.SIX]
							: EnrollmentConstants.OTHER_RELATIONSHIP_LKP_VALUE);
					/*
					 * enrolleeDataDto.setRelationshipToHouseHoldContact(enrolleeColData[
					 * EnrollmentConstants.SIX] != null ?
					 * (String)enrolleeColData[EnrollmentConstants.SIX] :
					 * EnrollmentConstants.OTHER_RELATIONSHIP_LKP_VALUE);
					 */

					enrolleeDataDto.setEnrolleeAddress1((String) enrolleeColData[EnrollmentConstants.SEVEN]);
					enrolleeDataDto.setEnrolleeAddress2((String) enrolleeColData[EnrollmentConstants.EIGHT]);
					enrolleeDataDto.setEnrolleeCity((String) enrolleeColData[EnrollmentConstants.NINE]);
					enrolleeDataDto.setEnrolleeState((String) enrolleeColData[EnrollmentConstants.TEN]);
					enrolleeDataDto.setEnrolleeZipcode((String) enrolleeColData[EnrollmentConstants.ELEVEN]);
					enrolleeDataDto.setEnrolleeCounty((String) enrolleeColData[EnrollmentConstants.TWELVE]);
					enrolleeDataDto.setEnrolleeCountyCode((String) enrolleeColData[EnrollmentConstants.THIRTEEN]);
					// HIX-71635 Add additional fields for CAP
					enrolleeDataDto.setPhoneNumber((String) enrolleeColData[EnrollmentConstants.FOURTEEN]);
					enrolleeDataDto.setEmailAddress((String) enrolleeColData[EnrollmentConstants.FIFTEEN]);
					enrolleeDataDto.setMemberId((String) enrolleeColData[EnrollmentConstants.SIXTEEN]);
					// HIX-84835 Enhance getenrollmentdatabyhouseholdcaseids to include more fields
					enrolleeDataDto.setSuffix((String) enrolleeColData[EnrollmentConstants.SEVENTEEN]);
					enrolleeDataDto.setMiddleName((String) enrolleeColData[EnrollmentConstants.EIGHTEEN]);
					enrolleeDataDto.setTaxIdentificationNumber((String) enrolleeColData[EnrollmentConstants.NINETEEN]);
					enrolleeDataDto.setBirthDate((Date) enrolleeColData[EnrollmentConstants.TWENTY]);
					enrolleeDataDto.setGenderLookupCode((String) enrolleeColData[EnrollmentConstants.TWENTY_ONE]);
					enrolleeDataDto.setGenderLookupLabel((String) enrolleeColData[EnrollmentConstants.TWENTY_TWO]);
					enrolleeDataDto.setEnrolleeId((Integer) enrolleeColData[EnrollmentConstants.TWENTY_THREE]);
					// HIX-106484
					enrolleeDataDto.setTobaccoStatus((String) enrolleeColData[EnrollmentConstants.TWENTY_FOUR]);
					/*
					 * enrolleeDataDto.setMailingAddress1((String)
					 * enrolleeColData[EnrollmentConstants.TWENTY_FIVE]);
					 * enrolleeDataDto.setMailingAddress2((String)
					 * enrolleeColData[EnrollmentConstants.TWENTY_SIX]);
					 * enrolleeDataDto.setMailingCity((String)
					 * enrolleeColData[EnrollmentConstants.TWENTY_SEVEN]);
					 * enrolleeDataDto.setMailingState((String)
					 * enrolleeColData[EnrollmentConstants.TWENTY_EIGHT]);
					 * enrolleeDataDto.setMailingZipcode((String)
					 * enrolleeColData[EnrollmentConstants.TWENTY_NINE]);
					 * enrolleeDataDto.setMailingCounty((String)
					 * enrolleeColData[EnrollmentConstants.THIRTY]);
					 * enrolleeDataDto.setMailingCountyCode((String)
					 * enrolleeColData[EnrollmentConstants.THIRTY_ONE]);
					 * enrolleeDataDto.setRatingArea((String)
					 * enrolleeColData[EnrollmentConstants.THIRTY_TWO]);
					 * enrolleeDataDto.setRatingAreaEffectiveDate((Date)
					 * enrolleeColData[EnrollmentConstants.THIRTY_THREE]);
					 */
					enrolleeDataDto.setRelationshipToHouseHoldContact(
							(String) enrolleeColData[EnrollmentConstants.THIRTY_FOUR]);
					enrolleeDataDto.setAge((Integer) enrolleeColData[EnrollmentConstants.THIRTY_FIVE]);
					enrolleeDataDto
							.setRelationshipToSubscriberCode((String) enrolleeColData[EnrollmentConstants.THIRTY_SIX]);
					enrolleeDataDto.setTobaccoStatusCode((String) enrolleeColData[EnrollmentConstants.THIRTY_SEVEN]);
					enrolleeDataDto.setRelationshipToHouseHoldContactCode(
							(String) enrolleeColData[EnrollmentConstants.THIRTY_EIGHT]);
					enrolleeDataDto.setPersonType((String) enrolleeColData[EnrollmentConstants.THIRTY_NINE]);
					enrolleeDataDto.setEnrolleeStatus((String) enrolleeColData[EnrollmentConstants.FORTY]);
					enrolleeDataDto.setQuotingDate(null != enrolleeColData[EnrollmentConstants.FORTY_ONE]
							? (Date) enrolleeColData[EnrollmentConstants.FORTY_ONE]
							: null);

					if (EnrollmentConstants.RELATIONSHIP_SELF_LABEL
							.equalsIgnoreCase(enrolleeDataDto.getRelationshipToSubscriber())
							|| EnrollmentConstants.RELATIONSHIP_SELF_CODE
									.equalsIgnoreCase(enrolleeDataDto.getRelationshipToSubscriberCode())) {
						EnrollmentDataDTO enrollmentDataDto = enrollmentDataDtoMap
								.get(enrolleeDataDto.getEnrollmentId());
						enrollmentDataDto.setRatingArea(EnrollmentUtils
								.getFormatedRatingArea((String) enrolleeColData[EnrollmentConstants.THIRTY_TWO]));
						enrollmentDataDto
								.setRatingAreaEffectiveDate((Date) enrolleeColData[EnrollmentConstants.THIRTY_THREE]);

						enrollmentDataDto.setHomeAddress1((String) enrolleeColData[EnrollmentConstants.SEVEN]);
						enrollmentDataDto.setHomeAddress2((String) enrolleeColData[EnrollmentConstants.EIGHT]);
						enrollmentDataDto.setHomeCity((String) enrolleeColData[EnrollmentConstants.NINE]);
						enrollmentDataDto.setHomeState((String) enrolleeColData[EnrollmentConstants.TEN]);
						enrollmentDataDto.setHomeZipcode((String) enrolleeColData[EnrollmentConstants.ELEVEN]);
						enrollmentDataDto.setHomeCounty((String) enrolleeColData[EnrollmentConstants.TWELVE]);
						enrollmentDataDto.setHomeCountyCode((String) enrolleeColData[EnrollmentConstants.THIRTEEN]);

						enrollmentDataDto.setMailingAddress1((String) enrolleeColData[EnrollmentConstants.TWENTY_FIVE]);
						enrollmentDataDto.setMailingAddress2((String) enrolleeColData[EnrollmentConstants.TWENTY_SIX]);
						enrollmentDataDto.setMailingCity((String) enrolleeColData[EnrollmentConstants.TWENTY_SEVEN]);
						enrollmentDataDto.setMailingState((String) enrolleeColData[EnrollmentConstants.TWENTY_EIGHT]);
						enrollmentDataDto.setMailingZipcode((String) enrolleeColData[EnrollmentConstants.TWENTY_NINE]);
						enrollmentDataDto.setMailingCounty((String) enrolleeColData[EnrollmentConstants.THIRTY]);
						enrollmentDataDto
								.setMailingCountyCode((String) enrolleeColData[EnrollmentConstants.THIRTY_ONE]);
						enrollmentDataDto.setSubscriberPresent(true);
					}
					enrollmentDataDtoMap.get(enrolleeDataDto.getEnrollmentId()).getEnrolleeDataDtoList()
							.add(enrolleeDataDto);
				}

				// HIX-86244 :: Get Premium information from enrollment ID
				if (enrollmentRequest.isPremiumDataRequired()) {

					List<EnrollmentPremium> enrollmentPremiumList = null;

					for (Integer enrollmentId : enrollmentIdList) {
						enrollmentPremiumList = enrollmentPremiumRepository.findByEnrollmentId(enrollmentId);

						if (null != enrollmentPremiumList && !enrollmentPremiumList.isEmpty()) {
							List<EnrollmentPremiumDTO> enrollmentPremiumDtoList = new ArrayList<EnrollmentPremiumDTO>();

							for (EnrollmentPremium enrollmentPremium : enrollmentPremiumList) {
								EnrollmentPremiumDTO epDto = new EnrollmentPremiumDTO();
								epDto.setYear(enrollmentPremium.getYear());
								epDto.setMonth(enrollmentPremium.getMonth());
								epDto.setAptcAmount(enrollmentPremium.getAptcAmount());

								if(enrollmentPremium.getStateSubsidyAmount() != null){
									epDto.setStateSubsidyAmount(enrollmentPremium.getStateSubsidyAmount());
								}

								if (enrollmentPremium.getAptcAmount() != null || enrollmentPremium.getStateSubsidyAmount() != null) {
									epDto.setFinancial(true);
								}

								if(enrollmentPremium.getAptcAmount() != null){
									epDto.setAptcEligible(true);
								}

								if(enrollmentPremium.getStateSubsidyAmount() != null){
									epDto.setStateSubsidyEligible(true);
								}

								epDto.setGrossPremiumAmount(enrollmentPremium.getGrossPremiumAmount());
								epDto.setNetPremiumAmount(enrollmentPremium.getNetPremiumAmount());
								epDto.setSlcspPremiumAmount(enrollmentPremium.getSlcspPremiumAmount());
								epDto.setSlcspPlanid(enrollmentPremium.getSlcspPlanid());
								epDto.setMaxAptc(enrollmentPremium.getMaxAptc());

								if(enrollmentPremium.getMaxStateSubsidy() != null){
									epDto.setMaxStateSubsidy(enrollmentPremium.getMaxStateSubsidy());
									epDto.setStateSubsidyAmount(enrollmentPremium.getStateSubsidyAmount());
									epDto.setActualStateSubsidyAmount(enrollmentPremium.getActStateSubsidyAmount());
								}

								Enrollment e = enrollmentPremium.getEnrollment();
								List<EnrolleeDataDTO> activeEnrolleeList =enrollmentDataDtoMap.get(enrollmentId).getEnrolleeDataDtoList();

								setMinimumPremium(e, enrollmentPremium, epDto, activeEnrolleeList.size());

								enrollmentPremiumDtoList.add(epDto);
							}
							enrollmentDataDtoMap.get(enrollmentId)
									.setEnrollmentPremiumDtoList(enrollmentPremiumDtoList);
						}
					}
				}

				// HIX-112045 :: If subscriber not present, then add subscriber entry
				for (Integer enrollmentId : enrollmentIdList) {
					EnrollmentDataDTO enrollmentDataDto = enrollmentDataDtoMap.get(enrollmentId);
					if (!enrollmentDataDto.isSubscriberPresent()) {
						setSubscriberData(enrollmentId, enrollmentDataDto);
					}
				}

			}
			// EnrollmentDataCheck
		} else if (!enrollmentRequest.isSepDataRequired()) {
			/*
			 * Throws GIException for no enrollment found.
			 */
			throw new GIException(EnrollmentConstants.ERROR_CODE_202, EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND,
					EnrollmentConstants.HIGH);
		}
		if (null != listOfEnrollmentDataDtomap && enrollmentRequest.isSepDataRequired()) {
			for (String householdCaseId : enrollmentRequest.getHouseHoldCaseIdList()) {
				if (!listOfEnrollmentDataDtomap.containsKey(householdCaseId)) {
					EnrollmentDataDTO enrollmentDataDto = new EnrollmentDataDTO();
					List<EnrollmentDataDTO> dtoList = new ArrayList<>();
					enrollmentDataDto.setHouseHoldCaseId(householdCaseId);
					setSepInfo(enrollmentDataDto, sepEventDetailMap);
					dtoList.add(enrollmentDataDto);
					listOfEnrollmentDataDtomap.put(householdCaseId, dtoList);
				}
			}
		}
		return listOfEnrollmentDataDtomap;
	}

	private void setMinimumPremium(Enrollment enrollment, EnrollmentPremium enrollmentPremium, EnrollmentPremiumDTO epDto, int activeEnrolleeCount){
		if(null != enrollmentPremium.getGrossPremiumAmount()) {
			BigDecimal premiumBeforeCredit = BigDecimal.valueOf(enrollmentPremium.getGrossPremiumAmount()).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal applicablePremium = BigDecimal.ZERO;

			BigDecimal minPremiumPerHousehold = BigDecimal.ZERO;
			BigDecimal ehbPercentage = BigDecimal.ONE;

			boolean applyOneDollarRule= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.APPLY_ONE_DOLLAR_RULE));

			if(applyOneDollarRule  && enrollment.getInsuranceTypeLkp()!=null && EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())){
				String minPremiumPerMember = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.MIN_PREMIUM_PER_MEMBER);
				minPremiumPerHousehold = BigDecimal.valueOf(activeEnrolleeCount).multiply(new BigDecimal(minPremiumPerMember));
			}

			if(EnrollmentUtils.isNotNullAndEmpty(enrollment.getEhbPercent())) {
				ehbPercentage = new BigDecimal(enrollment.getEhbPercent());
			}

			applicablePremium = premiumBeforeCredit.multiply(ehbPercentage);

			BigDecimal ehbMinimum = premiumBeforeCredit.subtract(applicablePremium);

			BigDecimal actualMinimum = minPremiumPerHousehold.max(ehbMinimum);

			epDto.setMinimumPremium(actualMinimum);
		}
	}

	@Override
	public List<EnrollmentDataDTO> getEnrollmentDataBySsapApplicationId(Long ssapApplicationId) {
		List<EnrollmentDataDTO> enrollmentDataDTOList = null;
		List<Object[]> enrollmentDataList = enrollmentRepository
				.getEnrollmentDataBySsapApplicationId(ssapApplicationId);
		if (enrollmentDataList != null && !enrollmentDataList.isEmpty()) {
			enrollmentDataDTOList = new ArrayList<EnrollmentDataDTO>();
			for (Object[] enrollmentData : enrollmentDataList) {
				EnrollmentDataDTO enrollmentDataDTO = new EnrollmentDataDTO();
				enrollmentDataDTO.setEnrollmentId((Integer) enrollmentData[EnrollmentConstants.ZERO]);
				enrollmentDataDTO.setEnrollmentStatus((String) enrollmentData[EnrollmentConstants.ONE]);
				enrollmentDataDTO.setPlanId((Integer) enrollmentData[EnrollmentConstants.TWO]);
				enrollmentDataDTO.setCMSPlanID((String) enrollmentData[EnrollmentConstants.THREE]);
				enrollmentDataDTO
						.setEnrollmentBenefitEffectiveStartDate((Date) enrollmentData[EnrollmentConstants.FOUR]);
				enrollmentDataDTO.setEnrollmentBenefitEffectiveEndDate((Date) enrollmentData[EnrollmentConstants.FIVE]);
				enrollmentDataDTO.setCreationTimestamp((Date) enrollmentData[EnrollmentConstants.SIX]);
				enrollmentDataDTOList.add(enrollmentDataDTO);
			}

		}
		return enrollmentDataDTOList;
	}

	@Override
	public Map<String, Integer> getAllEnrollmentDataBySsapApplicationId(Long ssapApplicationId) {
		List<Object[]> countList = enrollmentRepository.getAllEnrollmentDataBySsapApplicationId(ssapApplicationId,
				EnrollmentUtils.getBeforeDayDate(new TSDate(), EnrollmentConstants.SIXTY));
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		if (countList != null && !countList.isEmpty()) {
			for (Object[] countData : countList) {
				countMap.put((String) countData[EnrollmentConstants.ONE],
						((Long) countData[EnrollmentConstants.ZERO]).intValue());
			}
		}
		return countMap;
	}

	@Override
	public List<Enrollment> getEnrollmentNotInCancelAndAbortBySsapApplicationId(Long ssapApplicationId) {
		return enrollmentRepository.getEnrollmentNotInCancelAndAbortBySsapApplicationId(ssapApplicationId);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEnrollmentResend(Enrollment834ResendDTO enrollment834ResendDTO,
			EnrollmentResponse enrollmentResponse) throws GIException {
		try {
			AccountUser loggedInUser = getLoggedInUser();
			Map<Integer, String> failedEnrollmentMap = new HashMap<>();
			if (enrollment834ResendDTO != null) {
				if (enrollment834ResendDTO.getEnrollmentIds() == null
						|| enrollment834ResendDTO.getEnrollmentIds().size() < 1) {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
				} else if (enrollment834ResendDTO.getEventType() == null) {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_EVENT_TYPE_IS_NULL_OR_EMPTY);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					LOGGER.error(EnrollmentConstants.ERR_MSG_EVENT_TYPE_IS_NULL_OR_EMPTY);
				} else {
					boolean onlyLastEvent = false;
					if (enrollment834ResendDTO.getEventType().toString()
							.equalsIgnoreCase(Enrollment834ResendDTO.EVENT_TYPE.LAST.toString())) {
						onlyLastEvent = true;
					}
					AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
					for (Integer enrollmentId : enrollment834ResendDTO.getEnrollmentIds()) {

						if (enrollmentId != null) {
							try {
								List<EnrollmentEvent> eventList = null;
								if (onlyLastEvent) {
									Date lastEventTime = enrollmentEventAudRepository
											.getLatestNonCarrierRevisionDate(enrollmentId, user.getId());
									if (lastEventTime != null) {

										Calendar calendar = TSCalendar.getInstance();
										calendar.setTime(lastEventTime);
										calendar.add(Calendar.SECOND, -EnrollmentConstants.FIVE);
										Date eventCutoffTime = calendar.getTime();
										eventList = (ArrayList<EnrollmentEvent>) enrollmentEventRepository
												.getNonCarrierEventsByDate(enrollmentId, user.getId(), lastEventTime,
														eventCutoffTime);
									}

								} else {
									eventList = enrollmentEventRepository.getNonCarrierEvents(enrollmentId,
											user.getId());
								}
								if (eventList == null || eventList.size() < 1) {
									throw new Exception(
											"No non carrier event found for the enrollment Id :: " + enrollmentId);
								}

								for (EnrollmentEvent event : eventList) {
									event.setExtractionStatus(EnrollmentConstants.RESEND_FLAG);
								}

								enrollmentEventRepository.save(eventList);
								try {
									/*
									 * HIX-82198 Enhance Resend API to trigger CAP events history
									 */
									if (onlyLastEvent) {
										enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(
												enrollmentRepository.findById(enrollmentId),
												EnrollmentConstants.EnrollmentAppEvent.RESEND_834_LAST_TXN.toString(),
												null, enrollment834ResendDTO.getComments(), loggedInUser);
									} else {
										enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(
												enrollmentRepository.findById(enrollmentId),
												EnrollmentConstants.EnrollmentAppEvent.RESEND_834_HISTORY.toString(),
												null, enrollment834ResendDTO.getComments(), loggedInUser);
									}
								} catch (Exception ex) {
									LOGGER.error("Exception occurred while logging RESEND_834 Event: ", ex);
								}

							} catch (Exception e) {
								failedEnrollmentMap.put(enrollmentId, EnrollmentUtils.shortenedStackTrace(e, 5));
							}
						}
					} // End of For
					enrollmentResponse.setFailedEnrollmentMap(failedEnrollmentMap);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Caught exception in updateEnrollmentResend(-) call ::" + e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + ":: "
					+ EnrollmentUtils.shortenedStackTrace(e, 5));
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEnrollmentStatus(EnrollmentStatusUpdateDto enrollmentStatusUpdateRequest,
			EnrollmentResponse enrollmentResponse, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier)
			throws GIException {
		try {
			if (enrollmentStatusUpdateRequest != null) {
				List<Enrollment> enrollmentList = new ArrayList<>();
				Integer enrollmentId = enrollmentStatusUpdateRequest.getEnrollmentId();
				if (!isNotNullAndEmpty(enrollmentId)) {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
				} else {
					Enrollment enrollmentDb = findById(enrollmentId);
					if (enrollmentDb != null) {
						String existingEnrollmentStatus = enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode();
						if (EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equals(existingEnrollmentStatus)) {
							enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
							enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
							enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_STATUS_ALREADY_CONFIRM);
						} else {
							AccountUser loggedInUser = enrollmentStatusUpdateRequest.getUpdatedByUser() != null
									? enrollmentStatusUpdateRequest.getUpdatedByUser()
									: getLoggedInUser();
							if (loggedInUser != null) {
								enrollmentDb.setEnrollmentConfirmationDate(new TSDate());
								enrollmentDb.setIssuerAssignPolicyNo(
										enrollmentStatusUpdateRequest.getIssuerAssignPolicyNo());
								// List<Enrollee> enrolleeList =
								// getEnrolleesByStatus(enrollmentDb.getEnrollees() , existingEnrollmentStatus);
								List<Enrollee> allEnrolleeList = enrolleeRepository
										.getEnrolleeByEnrollmentID(enrollmentId);
								List<Enrollee> enrolleeList = getEnrolleesByStatus(allEnrolleeList,
										existingEnrollmentStatus);
								if (enrolleeList != null && !enrolleeList.isEmpty()) {
									List<EnrolleeUpdateDto> memberDtoList = enrollmentStatusUpdateRequest
											.getEnrolleeUpdateDtoList();
									for (EnrolleeUpdateDto memberDto : memberDtoList) {
										Enrollee dbEnrollee = getEnrolleeFromList(enrolleeList,
												memberDto.getExchgIndivIdentifier());
										if (dbEnrollee != null) {
											dbEnrollee.setUpdatedBy(loggedInUser);
											if (dbEnrollee.getPersonTypeLkp() != null
													&& dbEnrollee.getPersonTypeLkp().getLookupValueCode()
															.equalsIgnoreCase(
																	EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)
													&& memberDto.getIssuerIndivIdentifier() != null) {
												enrollmentDb.setIssuerSubscriberIdentifier(
														memberDto.getIssuerIndivIdentifier());
											}
											if (isNotNullAndEmpty(memberDto.getIssuerIndivIdentifier())) {
												dbEnrollee
														.setIssuerIndivIdentifier(memberDto.getIssuerIndivIdentifier());
											}
											if (isNotNullAndEmpty(memberDto.getLastPremiumPaidDate())) {
												dbEnrollee.setLastPremiumPaidDate(
														DateUtil.StringToDate(memberDto.getLastPremiumPaidDate(),
																GhixConstants.REQUIRED_DATE_FORMAT));
											}
											if (isNotNullAndEmpty(
													enrollmentStatusUpdateRequest.getIssuerAssignPolicyNo())) {
												dbEnrollee.setHealthCoveragePolicyNo(
														enrollmentStatusUpdateRequest.getIssuerAssignPolicyNo());
											}
											if (EnrollmentConstants.ENROLLMENT_STATUS_PENDING
													.equals(existingEnrollmentStatus)) {
												dbEnrollee.setEnrolleeLkpValue(
														lookupService.getlookupValueByTypeANDLookupValueCode(
																EnrollmentConstants.ENROLLMENT_STATUS,
																EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
											} 
											dbEnrollee.setEnrollment(enrollmentDb);
											createEnrolleeEvent(dbEnrollee, EnrollmentConstants.EVENT_TYPE_ADDITION,
													EnrollmentConstants.EVENT_REASON_INITIAL_ENROLLMENT, loggedInUser,
													false, txn_idfier);
										}
									}
								}

								// checkEnrollmentStatus(loggedInUser,enrollmentDb);
								checkEnrollmentStatus(loggedInUser, enrollmentDb, allEnrolleeList);
								// enrolleeRepository.save(enrolleeList);
								enrollmentDb.setEnrollees(enrolleeList);
								enrollmentList.add(enrollmentDb);
								enrollmentList.addAll(synchronizeQuotingDates(enrollmentDb, loggedInUser,
										EnrollmentEvent.TRANSACTION_IDENTIFIER.ENRL_STATUS_CH_QUOTING_DATE_UPDATE));
								// Call re-quoting
								for (Enrollment enrollment : enrollmentList) {
									enrollment.setUpdatedBy(loggedInUser);
									enrollmentRequotingService.updateMonthlyPremiumForEnrollment(enrollment, true,
											false, null, true);
								}

								enrollmentList = saveAllEnrollment(enrollmentList);

								Boolean isCaCall = EnrollmentConstants.STATE_CODE_CA
										.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode());

								if (isCaCall) {
									// Async Call to EnrollmentAhbxSyncService for saving enrollment update Data
									// replacement of IND21
									LOGGER.info(
											"Async Call to enrollmentAhbxSyncService saveEnrollmentAhbxSync IND21_UPDATE");
									final List<Enrollment> finalEnrollmentList = enrollmentList;
									TransactionSynchronizationManager
											.registerSynchronization(new TransactionSynchronizationAdapter() {
												public void afterCommit() {
													enrollmentAhbxSyncService.saveEnrollmentAhbxSync(
															finalEnrollmentList, enrollmentDb.getHouseHoldCaseId(),
															EnrollmentAhbxSync.RecordType.IND21_UPDATE);
												}
											});
								}

								if (enrollmentList != null) {
									enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
									enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);

									try {
										/**
										 * HIX-100991 - trigger CAP events history
										 */
										enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollmentDb,
												EnrollmentConstants.EnrollmentAppEvent.CARRIER_CONFIRMATION_RECON
														.toString(),
												loggedInUser);

									} catch (Exception ex) {
										LOGGER.error(
												"Exception occurred while logging CARRIER_CONFIRMATION_RECON Event history: ",
												ex);
									}
								} else {
									throw new GIException(EnrollmentConstants.ERR_MSG_UPDATE_ENROLLMENT_STATUS_FAILED);
								}
							} else {
								enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_208);
								enrollmentResponse.setErrMsg(
										"No logged in user found in either request or session to update enrollment.");
								enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
							}

						}
					} else {
						enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_207);
						enrollmentResponse
								.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}
				}
			} else {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
			}
		} catch (Exception ex) {
			LOGGER.error("Caught exception in updateEnrollmentStatus(-) call ::" + ex);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_299);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_RETRIEVE_FAIL + ":: "
					+ EnrollmentUtils.shortenedStackTrace(ex, 5));
		}
	}

	@Override
	public EnrollmentResponse terminateEnrollmentByPlanId(String cmsPlanId, String coverageYear,
			String terminationDate) {
		Executors.newSingleThreadExecutor().execute(
				new EnrollmentTerminateThread(cmsPlanId, coverageYear, terminationDate, enrollmentAsyncService));

		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		return enrollmentResponse;
	}

	@Override
	public EnrollmentResponse countEnrollmentByPlanId(String cmsPlanId, String coverageYear, String terminationDate) {
		Long enrollmentCount = enrollmentRepository.getEnrollmentCountByCmsPlanId(cmsPlanId, coverageYear,
				terminationDate);

		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		if (enrollmentCount != null) {
			enrollmentResponse.setEnrollmentCount(enrollmentCount);
		} else {
			enrollmentResponse.setEnrollmentCount(0L);
		}

		return enrollmentResponse;
	}

	private List<Enrollee> getEnrolleesByStatus(List<Enrollee> enrollees, String status) {
		List<Enrollee> filteredEnrollees = new ArrayList<Enrollee>();
		if (enrollees != null) {
			for (Enrollee enrollee : enrollees) {
				if (StringUtils.isNotBlank(status) && enrollee.getPersonTypeLkp() != null
						&& ((enrollee.getPersonTypeLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)
								|| enrollee.getPersonTypeLkp().getLookupValueCode()
										.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))
						&& enrollee.getEnrolleeLkpValue() != null
						&& (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(status))) {
					filteredEnrollees.add(enrollee);
				}
			}
		}

		return filteredEnrollees;
	}

	private Enrollee getEnrolleeFromList(List<Enrollee> enrollees, String exchgIndivIdentifier) {
		Enrollee latestEnrollee = null;
		for (Enrollee enrollee : enrollees) {
			if (StringUtils.equals(exchgIndivIdentifier, enrollee.getExchgIndivIdentifier())) {
				if (latestEnrollee == null) {
					latestEnrollee = enrollee;
				} else {
					if (enrollee.getCreatedOn().compareTo(latestEnrollee.getCreatedOn()) > 0) {
						latestEnrollee = enrollee;
					}
				}
			}
		}

		return latestEnrollee;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public EnrollmentResponse updateMailByHouseHoldId(String householdCaseId, Long locationID) {
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		try {
			Location location = locationRepository.findOne(locationID.intValue());

			if (!isValidLocation(location)) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_LOCATION_ID);
				return enrollmentResponse;
			}

			int updateForYear = Integer.parseInt(DynamicPropertiesUtil
					.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.UPDATE_MAILING_ADDRESS_YEAR));
			Calendar calOne = TSCalendar.getInstance();
			calOne.setTime(new TSDate());

			calOne.set(Calendar.YEAR, calOne.get(Calendar.YEAR) - (updateForYear - EnrollmentConstants.ONE));

			Date startDate = DateUtil.getYearStartDate(calOne.getTime(), DateUtil.StartEndYearEnum.START);

			String startDateStr = DateUtil.dateToString(startDate, GhixConstants.REQUIRED_DATE_FORMAT);

			List<Enrollment> enrollmentList = enrollmentRepository.getEnrollmentByByHouseHoldCaseId(householdCaseId,
					startDateStr);

			boolean onlyLatestMailingAddress834 = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
					EnrollmentConfiguration.EnrollmentConfigurationEnum.ONLY_LATEST_MAILING_ADDRESS_834));

			if (null != enrollmentList && !enrollmentList.isEmpty()) {
				List<Integer> issuerList = new ArrayList<>();
				AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
				for (Enrollment enrollment : enrollmentList) {
					boolean send834Event = true;
					if (onlyLatestMailingAddress834) {
						if (!issuerList.contains(enrollment.getIssuerId())) {
							issuerList.add(enrollment.getIssuerId());
						} else {
							send834Event = false;
						}
					}

					List<Enrollee> enrolleeList = getAllActiveEnrollees(enrollment);
					for (Enrollee enrollee : enrolleeList) {
						Location enrolleeAddressObj = enrollee.getMailingAddressId();

						if (enrolleeAddressObj == null) {
							enrolleeAddressObj = new Location();
						}

						enrolleeAddressObj.setAddress1(location.getAddress1());
						enrolleeAddressObj.setAddress2(location.getAddress2());
						if (isNotNullAndEmpty(location.getCity())) {
							enrolleeAddressObj.setCity(location.getCity());
						}
						if (isNotNullAndEmpty(location.getState())) {
							enrolleeAddressObj.setState(location.getState());
						}
						if (isNotNullAndEmpty(location.getZip())) {
							enrolleeAddressObj.setZip(location.getZip());
						}
						enrollee.setMailingAddressId(enrolleeAddressObj);

						createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
								EnrollmentConstants.EVENT_REASON_CHANGE_OF_LOCATION, user, send834Event, false, // isSpecial
								EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_UPDATE_MAILING_ADDRESS);

						enrollee.setEventCreated(true);
					}
				}
			}

			for (Enrollment enr : enrollmentList) {
				enr.setUpdatedOn(new TSDate());
			}

			saveAllEnrollment(enrollmentList);

		} catch (Exception ge) {
			LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage()
					+ EnrollmentConstants.CAUSE + ge.getCause(), ge);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage()
					+ EnrollmentConstants.CAUSE + ge.getCause());
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			return enrollmentResponse;
		}

		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		enrollmentResponse.setErrMsg(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);

		return enrollmentResponse;

	}

	private void updateEnrolleeMailingAddress(Enrollee enrollee, Location location, boolean send834Event,
			AccountUser user) {
		if (enrollee != null && location != null) {
			Location enrolleeAddressObj = enrollee.getMailingAddressId();

			if (enrolleeAddressObj == null) {
				enrolleeAddressObj = new Location();
			}

			enrolleeAddressObj.setAddress1(location.getAddress1());
			enrolleeAddressObj.setAddress2(location.getAddress2());
			if (isNotNullAndEmpty(location.getCity())) {
				enrolleeAddressObj.setCity(location.getCity());
			}
			if (isNotNullAndEmpty(location.getState())) {
				enrolleeAddressObj.setState(location.getState());
			}
			if (isNotNullAndEmpty(location.getZip())) {
				enrolleeAddressObj.setZip(location.getZip());
			}
			enrollee.setMailingAddressId(enrolleeAddressObj);

			createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
					EnrollmentConstants.EVENT_REASON_CHANGE_OF_LOCATION, user, send834Event, false, // isSpecial
					EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_UPDATE_MAILING_ADDRESS);
		}
	}

	private void updateHouseholdContactDetails(Enrollee houseHoldContactEnrollee,
			EnrollmentMailingAddressUpdateRequest houseHoldPersonReq, AccountUser user) {
		if (houseHoldContactEnrollee != null && houseHoldPersonReq != null) {
			boolean isUpdated = false;
			if (isNotNullAndEmpty(houseHoldPersonReq.getHouseholdContactPreferredPhone())) {
				houseHoldContactEnrollee.setPrimaryPhoneNo(houseHoldPersonReq.getHouseholdContactPreferredPhone());
				isUpdated = true;
			}
			if (isNotNullAndEmpty(houseHoldPersonReq.getHouseholdContactSecondaryPhone())) {
				houseHoldContactEnrollee.setSecondaryPhoneNo(houseHoldPersonReq.getHouseholdContactSecondaryPhone());
				isUpdated = true;
			}
			if (isNotNullAndEmpty(houseHoldPersonReq.getHouseholdContactPreferredEmail())) {
				houseHoldContactEnrollee.setPreferredEmail(houseHoldPersonReq.getHouseholdContactPreferredEmail());
				houseHoldContactEnrollee.setPreferredSMS(null);
				isUpdated = true;
			} else if (isNotNullAndEmpty(houseHoldPersonReq.getHouseholdContactPreferredPhone())) {
				houseHoldContactEnrollee.setPreferredSMS(houseHoldPersonReq.getHouseholdContactPreferredPhone());
				houseHoldContactEnrollee.setPreferredEmail(null);
				isUpdated = true;
			}
			if (isNotNullAndEmpty(houseHoldPersonReq.getHouseholdContactSpokenLanguageCode())) {
				houseHoldContactEnrollee.setLanguageLkp(
						lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_SPOKEN,
								houseHoldPersonReq.getHouseholdContactSpokenLanguageCode()));
				isUpdated = true;
			}
			if (isNotNullAndEmpty(houseHoldPersonReq.getHouseholdContactWrittenLanguageCode())) {
				houseHoldContactEnrollee.setLanguageWrittenLkp(
						lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_WRITTEN,
								houseHoldPersonReq.getHouseholdContactWrittenLanguageCode()));
				isUpdated = true;
			}
			if (isUpdated) {
				createEnrolleeEvent(houseHoldContactEnrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
						EnrollmentConstants.EVENT_REASON_CHANGE_OF_LOCATION, user, false, false,
						EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_UPDATE_MAILING_ADDRESS);
			}

		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public EnrollmentResponse updateMailByHouseHoldId(EnrollmentMailingAddressUpdateRequest mailingUpdateRequest,
			EnrollmentResponse enrollmentResponse) {
		try {
			Location location = locationRepository
					.findOne(mailingUpdateRequest.getUpdatedMailingAddressLocationID().intValue());

			if (!isValidLocation(location)) {
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_LOCATION_ID);
				return enrollmentResponse;
			}

			int updateForYear = Integer.parseInt(DynamicPropertiesUtil
					.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.UPDATE_MAILING_ADDRESS_YEAR));
			Calendar calOne = TSCalendar.getInstance();
			calOne.setTime(new TSDate());

			calOne.set(Calendar.YEAR, calOne.get(Calendar.YEAR) - (updateForYear - EnrollmentConstants.ONE));

			Date startDate = DateUtil.getYearStartDate(calOne.getTime(), DateUtil.StartEndYearEnum.START);

			String startDateStr = DateUtil.dateToString(startDate, GhixConstants.REQUIRED_DATE_FORMAT);

			List<Enrollment> enrollmentList = enrollmentRepository.getEnrollmentByByHouseHoldCaseId(
					Long.toString(mailingUpdateRequest.getHouseholdCaseId()), startDateStr);

			boolean onlyLatestMailingAddress834 = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
					EnrollmentConfiguration.EnrollmentConfigurationEnum.ONLY_LATEST_MAILING_ADDRESS_834));

			if (null != enrollmentList && !enrollmentList.isEmpty()) {
				List<Integer> issuerList = new ArrayList<>();
				AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
				for (Enrollment enrollment : enrollmentList) {
					boolean send834Event = true;
					if (onlyLatestMailingAddress834) {
						if (!issuerList.contains(enrollment.getIssuerId())) {
							issuerList.add(enrollment.getIssuerId());
						} else {
							send834Event = false;
						}
					}
					List<Enrollee> householdContacts = getHouseholdContactPerson(enrollment.getEnrollees());
					if (householdContacts != null && householdContacts.size() > 0) {
						for (Enrollee household : householdContacts) {
							updateHouseholdContactDetails(household, mailingUpdateRequest, user);
						}
					}

					List<Enrollee> enrolleeList = getAllActiveEnrollees(enrollment);
					for (Enrollee enrollee : enrolleeList) {
						updateEnrolleeMailingAddress(enrollee, location, send834Event, user);
					}
				}
			}

			for (Enrollment enr : enrollmentList) {
				enr.setUpdatedOn(new TSDate());
			}

			saveAllEnrollment(enrollmentList);

		} catch (Exception ge) {
			LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage()
					+ EnrollmentConstants.CAUSE + ge.getCause(), ge);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_205);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage()
					+ EnrollmentConstants.CAUSE + ge.getCause());
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			return enrollmentResponse;
		}

		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		enrollmentResponse.setErrMsg(EnrollmentConstants.MSG_ENROLLMENT_UPDATE_SUCCESS);

		return enrollmentResponse;

	}

	public List<Enrollee> getAllActiveEnrollees(Enrollment enrollment) {
		List<Enrollee> allActiveEnrolleeList = new ArrayList<Enrollee>();
		if (enrollment.getEnrollees() != null) {
			for (Enrollee enrollee : enrollment.getEnrollees()) {
				if (enrollee.getEnrolleeLkpValue() != null) {
					if ((!enrollee.getEnrolleeLkpValue().getLookupValueCode()
							.equalsIgnoreCase(ENROLLMENT_STATUS_CANCEL))
							&& (!enrollee.getEnrolleeLkpValue().getLookupValueCode()
									.equalsIgnoreCase(ENROLLMENT_STATUS_ABORTED))) {
						allActiveEnrolleeList.add(enrollee);
					}
				} else {
					allActiveEnrolleeList.add(enrollee);
				}

			}
		}
		return allActiveEnrolleeList;
	}

	private boolean isValidLocation(Location location) {
		if (null == location) {
			return false;
		} else if (null == location.getAddress1()) {
			return false;
		} else if (null == location.getCity()) {
			return false;
		} else if (null == location.getZip()) {
			return false;
		}
		return true;
	}

	private void updateHouseholdResponsible(EnrolleeDataUpdateDTO enr, List<Enrollee> enrollees, AccountUser user,
			String exchgIndivIdentifier) {
		if (enr != null && enrollees != null && enrollees.size() > 0) {
			List<Enrollee> householdResponsible = getHouseholdContactResponsiblePerson(exchgIndivIdentifier, enrollees);
			if (householdResponsible != null && householdResponsible.size() > 0) {

				for (Enrollee enrollee : householdResponsible) {
					boolean change = false;
					if (isNotNullAndEmpty(enr.getFirstName())
							&& !enr.getFirstName().equalsIgnoreCase(enrollee.getFirstName())) {
						enrollee.setFirstName(enr.getFirstName());
						change = true;
					}
					if (isNotNullAndEmpty(enr.getLastName())
							&& !enr.getLastName().equalsIgnoreCase(enrollee.getLastName())) {
						enrollee.setLastName(enr.getLastName());
						change = true;
					}
					enrollee.setMiddleName(enr.getMiddleName());
					if ((isNotNullAndEmpty(enr.getSsn()) && !enr.getSsn().equalsIgnoreCase(enrollee.getTaxIdNumber()))
							|| (isNotNullAndEmpty(enrollee.getTaxIdNumber())
									&& !enrollee.getTaxIdNumber().equalsIgnoreCase(enr.getSsn()))) {
						enrollee.setTaxIdNumber(enr.getSsn());
						change = true;
					}

					if (change) {
						createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
								EnrollmentConstants.EVENT_REASON_IDENTITY_CHANGE, user, false,
								EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
					}
				}

			}
		}
	}

	private boolean updateMemberData(Enrollment enrollment, EnrollmentUpdateDTO enrollmentUpdateDto,
			final AccountUser user, EnrollmentResponse response, List<String> enrollmentAppEventList)
			throws GIException {
		boolean isMemberDataUpdate = false;
		if (enrollmentUpdateDto.getMembers() != null && enrollmentUpdateDto.getMembers().size() > 0) {
			for (EnrolleeDataUpdateDTO enr : enrollmentUpdateDto.getMembers()) {
				boolean nameEdit = Boolean.FALSE, genderEdit = Boolean.FALSE, ssnEdit = Boolean.FALSE;
				String enrollmentAppEvent = null;
				if (enr.getEnrolleeId() != null) {
					Enrollee enrollee = enrollment.getEnrolleeById(enr.getEnrolleeId());
					if (enrollee != null) {
						if (isNotNullAndEmpty(enr.getFirstName())
								&& !enr.getFirstName().equalsIgnoreCase(enrollee.getFirstName())) {
							enrollee.setFirstName(enr.getFirstName());
							nameEdit = Boolean.TRUE;
						}
						if (isNotNullAndEmpty(enr.getLastName())
								&& !enr.getLastName().equalsIgnoreCase(enrollee.getLastName())) {
							enrollee.setLastName(enr.getLastName());
							nameEdit = Boolean.TRUE;
						}
						if ((isNotNullAndEmpty(enr.getMiddleName())
								&& !enr.getMiddleName().equalsIgnoreCase(enrollee.getMiddleName()))
								|| (isNotNullAndEmpty(enrollee.getMiddleName())
										&& !enrollee.getMiddleName().equalsIgnoreCase(enr.getMiddleName()))) {
							nameEdit = Boolean.TRUE;
						}
						enrollee.setMiddleName(enr.getMiddleName());
						if ((isNotNullAndEmpty(enr.getSsn())
								&& !enr.getSsn().equalsIgnoreCase(enrollee.getTaxIdNumber()))
								|| (isNotNullAndEmpty(enrollee.getTaxIdNumber())
										&& !enrollee.getTaxIdNumber().equalsIgnoreCase(enr.getSsn()))) {
							ssnEdit = Boolean.TRUE;
							enrollmentAppEvent = EnrollmentConstants.EnrollmentAppEvent.MEMBER_SSN_EDIT.toString();
						}
						enrollee.setTaxIdNumber(enr.getSsn());
						if (isNotNullAndEmpty(enr.getGender())) {
							LookupValue gender = lookupService.getlookupValueByTypeANDLookupValueCode(
									EnrollmentConstants.GENDER, enr.getGender());
							if (gender == null) {
								gender = lookupService.getlookupValueByTypeANDLookupValueCode(
										EnrollmentConstants.GENDER, EnrollmentConstants.GENDER_UNKNOWN);
							}
							if (isNotNullAndEmpty(enrollee.getGenderLkp()) && !gender.getLookupValueCode()
									.equalsIgnoreCase(enrollee.getGenderLkp().getLookupValueCode())) {
								enrollee.setGenderLkp(gender);
								genderEdit = Boolean.TRUE;
								enrollmentAppEvent = EnrollmentConstants.EnrollmentAppEvent.MEMBER_GENDER_EDIT
										.toString();
							}
						}

						if ((nameEdit && ssnEdit) || (ssnEdit && genderEdit) || (genderEdit && nameEdit)) {
							enrollmentAppEventList
									.add(EnrollmentConstants.EnrollmentAppEvent.MEMBER_DATA_EDIT.toString());
						} else if (enrollmentAppEvent != null) {
							enrollmentAppEventList.add(enrollmentAppEvent);
						} else if (nameEdit) {
							enrollmentAppEventList
									.add(EnrollmentConstants.EnrollmentAppEvent.MEMBER_NAME_EDIT.toString());
						}

						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = enr.getTxnType();
//						if(memberTxnIdentifier!=null){
//							createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(), user, null, null);
//						}
						if (nameEdit || ssnEdit || genderEdit) {
							isMemberDataUpdate = true;
							if (!enrollee.isEventCreated()) {
								// enrollee.setEventCreated(true);
								// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
								// EnrollmentConstants.EVENT_REASON_IDENTITY_CHANGE, user, true,
								// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
										enrollmentUpdateDto.isSend834(), user, EnrollmentConstants.EVENT_TYPE_CHANGE,
										EnrollmentConstants.EVENT_REASON_IDENTITY_CHANGE, enr.getTxnReason());
							}

							updateHouseholdResponsible(enr, enrollment.getEnrollees(), user,
									enrollee.getExchgIndivIdentifier());

						} 
					} else {
						response.setErrCode(EnrollmentConstants.ERROR_CODE_205);
						response.setErrMsg(EnrollmentConstants.ERR_MSG_NO_MEMBER_FOUND);
						response.setStatus(GhixConstants.RESPONSE_FAILURE);
						throw new GIException(EnrollmentConstants.ERR_MSG_NO_MEMBER_FOUND);
					}
				}
			}
		}
		if (isMemberDataUpdate) {
			for (EnrolleeDataUpdateDTO enr : enrollmentUpdateDto.getMembers()) {
				if (enr.getEnrolleeId() != null) {
					Enrollee enrollee = enrollment.getEnrolleeById(enr.getEnrolleeId());
					if (enrollee != null && !enrollee.isEventCreated()) {
						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = enr.getTxnType();
						createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(),
								user, EnrollmentConstants.EVENT_TYPE_CHANGE,
								EnrollmentConstants.EVENT_REASON_IDENTITY_CHANGE, enr.getTxnReason());

					}
				}
			}
		}
		return isMemberDataUpdate;
	}

	/**
	 * 
	 * @param enrollee
	 * @return
	 * @throws GIException
	 */
	private boolean validateEnrolleeForBrokerUpdate(final Enrollee enrollee) throws GIException {
		boolean isSendBrokerUpdateValid = Boolean.TRUE;

		if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
				.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
			isSendBrokerUpdateValid = Boolean.FALSE;
		} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode()
				.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)
				&& EnrollmentUtils.removeTimeFromDate(enrollee.getEffectiveEndDate())
						.before(EnrollmentUtils.removeTimeFromDate(new TSDate()))) {
			isSendBrokerUpdateValid = Boolean.FALSE;
		}
		return isSendBrokerUpdateValid;
	}

	@Override
	public boolean validateEnrollmentOverlap(EnrollmentCoverageValidationRequest request) {
		boolean overlapFlag = false;
		if ((null != request && (null != request.getMemberList() && !request.getMemberList().isEmpty())
				&& null != request.getInsuranceType())
				&& (null != request.getEmployeeApplicationId() || null != request.getHouseholdCaseId())) {
			boolean isExistingEnrollment = (null != request.getEnrollmentId() && 0 != request.getEnrollmentId()) ? true
					: false;
			boolean isShopRequest = (null != request.getEmployeeApplicationId()
					&& 0 != request.getEmployeeApplicationId()) ? true : false;
			for (EnrollmentMemberCoverageDTO memberDto : request.getMemberList()) {

				// TODO we need to get memberID from provided Enrollee Id
				Long overlapCount = null;
				if (null != memberDto.getMemberStartDate() && null != memberDto.getMemberEndDate()) {
					if (isExistingEnrollment && isShopRequest) {
						overlapCount = enrolleeRepository.getEnrolleeOverLapCountForExistingShopEnrollment(
								request.getEmployeeApplicationId(), request.getInsuranceType().name(),
								memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(),
								request.getEnrollmentId());
					} else if (isExistingEnrollment && !isShopRequest) {
						overlapCount = enrolleeRepository.getEnrolleeOverLapCountForExistingEnrollment(
								request.getHouseholdCaseId(), request.getInsuranceType().name(),
								memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(),
								request.getEnrollmentId());
					} else if (!isExistingEnrollment && isShopRequest) {
						overlapCount = enrolleeRepository.getEnrolleeOverLapCountForNewShopEnrollment(
								request.getEmployeeApplicationId(), request.getInsuranceType().name(),
								memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId());
					} else {
						overlapCount = enrolleeRepository.getEnrolleeOverLapCountForNewEnrollment(
								request.getHouseholdCaseId(), request.getInsuranceType().name(),
								memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId());
					}
					if (isNotNullAndEmpty(overlapCount) && overlapCount > 0) {
						overlapFlag = true;
						break;
					}
				}
			}
			if (isExistingEnrollment && !overlapFlag) {
				for (EnrollmentMemberCoverageDTO memberDto : request.getMemberList()) {
					if (memberDto.isNewMember()) {
						Long overlapCount = null;
						if (null != memberDto.getMemberStartDate() && null != memberDto.getMemberEndDate()) {
							if (isShopRequest) {
								overlapCount = enrolleeRepository.getOverLapCountForExistingShopEnrollmentNewEnrollees(
										request.getEmployeeApplicationId(), request.getInsuranceType().name(),
										memberDto.getMemberStartDate(), memberDto.getMemberEndDate(),
										memberDto.getMemberId(), request.getEnrollmentId(),
										(null != memberDto.getEnrolleeId() ? memberDto.getEnrolleeId() : 0));
							} else if (!isShopRequest) {
								overlapCount = enrolleeRepository.getOverLapCountForExistingEnrollmentNewEnrollee(
										request.getHouseholdCaseId(), request.getInsuranceType().name(),
										memberDto.getMemberStartDate(), memberDto.getMemberEndDate(),
										memberDto.getMemberId(), request.getEnrollmentId(),
										(null != memberDto.getEnrolleeId() ? memberDto.getEnrolleeId() : 0));
							}
							if (isNotNullAndEmpty(overlapCount) && overlapCount > 0) {
								overlapFlag = true;
								break;
							}
						}
					}
				}

			}
		} else {
			LOGGER.error("Request validation failed. Please check the request :: " + request);
		}
		return overlapFlag;
	}

	@Override
	public List<MonthlyAPTCAmountDTO> getMonthlyAPTCAmount(List<Integer> idList) {

		List<Object[]> enrollmentDataList = enrollmentRepository.getEnrollmentDataForMonthlyAPTCAmount(idList,
				new TSDate());

		List<MonthlyAPTCAmountDTO> monthlyAPTCAmountDTOList = new ArrayList<>();
		MonthlyAPTCAmountDTO monthlyAPTCAmountDTO = null;
		Calendar nextMonthDate = null;
		boolean errorFlag = false;

		if (enrollmentDataList != null) {
			if (idList.size() != enrollmentDataList.size()) {
				errorFlag = true;
			}
		} else {
			errorFlag = true;
		}

		if (errorFlag) {
			return null;
		}

		for (Object[] enrlData : enrollmentDataList) {
			monthlyAPTCAmountDTO = new MonthlyAPTCAmountDTO();

			Integer id = (Integer) enrlData[0];
			String enrollmentStatusLkp = (String) enrlData[1];
			Date benefitEffectiveDate = (Date) enrlData[2];

			Date benefitEndDate = null;
			if (enrlData.length >= 4) {
				benefitEndDate = (Date) enrlData[3];
			}

			String insuranceType = null;
			if (enrlData.length >= 5) {
				insuranceType = (String) enrlData[4];
			}

			Date enrollmentConfirmationDate = null;
			if (enrlData.length >= 6) {
				enrollmentConfirmationDate = (Date) enrlData[5];
			}

			String cmsPlanID = null;
			if (enrlData.length >= 7) {
				cmsPlanID = (String) enrlData[6];
			}

			Float essentialHealthBenefitPrmPercent = null;
			if (enrlData.length >= 8) {
				essentialHealthBenefitPrmPercent = (Float) enrlData[7];
			}

			if (DateUtil.checkIfInBetween(benefitEffectiveDate, benefitEndDate, new TSDate())) {
				Calendar cal1 = TSCalendar.getInstance();
				cal1.setTime(new TSDate());

				Calendar cal2 = TSCalendar.getInstance();
				cal2.setTime(benefitEndDate);

				if (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)
						&& cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) {
					monthlyAPTCAmountDTO
							.setErrMsg("Enrollment EndDate is in Current Month ( No Next month data available )");
					monthlyAPTCAmountDTO.setAPIStatus(GhixConstants.RESPONSE_FAILURE);
					monthlyAPTCAmountDTOList.add(monthlyAPTCAmountDTO);
					return monthlyAPTCAmountDTOList;
				} else {
					monthlyAPTCAmountDTO.setAPIStatus(GhixConstants.RESPONSE_SUCCESS);
				}
			} else if ((benefitEffectiveDate).after(new TSDate())) {
				nextMonthDate = TSCalendar.getInstance();
				nextMonthDate.setTime(benefitEffectiveDate);
				nextMonthDate.set(Calendar.HOUR_OF_DAY, 00);
				nextMonthDate.set(Calendar.MINUTE, 00);
				nextMonthDate.set(Calendar.SECOND, 00);
				monthlyAPTCAmountDTO.setAPIStatus(GhixConstants.RESPONSE_SUCCESS);

			} else if ((benefitEndDate).before(new TSDate())) {
				monthlyAPTCAmountDTO
						.setErrMsg("Enrollment EndDate is before Current Date ( No Next month data available )");
				monthlyAPTCAmountDTO.setAPIStatus(GhixConstants.RESPONSE_FAILURE);

				monthlyAPTCAmountDTOList.add(monthlyAPTCAmountDTO);
				return monthlyAPTCAmountDTOList;
			}

			if (nextMonthDate == null) {
				nextMonthDate = TSCalendar.getInstance();
				nextMonthDate.setTime(new TSDate());
				nextMonthDate.set(Calendar.DAY_OF_MONTH, 1);
				nextMonthDate.add(Calendar.MONTH, 1);
				nextMonthDate.set(Calendar.HOUR_OF_DAY, 00);
				nextMonthDate.set(Calendar.MINUTE, 00);
				nextMonthDate.set(Calendar.SECOND, 00);
				monthlyAPTCAmountDTO.setAPIStatus(GhixConstants.RESPONSE_SUCCESS);
			}

			monthlyAPTCAmountDTO.setEnrollment_id(id);

			if (monthlyAPTCAmountDTO.getAPIStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				monthlyAPTCAmountDTO.setHealthOrDental(insuranceType); // Health / Dental
				monthlyAPTCAmountDTO.setEnrollment_Status(enrollmentStatusLkp);
				monthlyAPTCAmountDTO.setEnrollmentConfirmationDate(enrollmentConfirmationDate);
				monthlyAPTCAmountDTO.setEnrollmentCoverageStartDate(benefitEffectiveDate);
				monthlyAPTCAmountDTO.setEnrollmentCoverageEndDate(benefitEndDate);
				monthlyAPTCAmountDTO.setNextMonthStartDate(nextMonthDate.getTime());
				monthlyAPTCAmountDTO.setcMSPlanId(cmsPlanID);
				monthlyAPTCAmountDTO.setEhbPercentage(essentialHealthBenefitPrmPercent);

				EnrollmentResponse enrRes = enrollmentPremiumService.findPremiumDetailsByEnrollmentIdMonthAndYear(id,
						nextMonthDate.get(Calendar.MONTH), nextMonthDate.get(Calendar.YEAR));

				if (enrRes != null && (enrRes.getEnrollmentPremiumDTOList() != null
						&& !enrRes.getEnrollmentPremiumDTOList().isEmpty())) {
					List<EnrollmentPremiumDTO> enrollmentPremiumDtoList = enrRes.getEnrollmentPremiumDTOList();
					if (enrollmentPremiumDtoList.size() == 1) {
						monthlyAPTCAmountDTO
								.setNextMonthGrossPremium(enrollmentPremiumDtoList.get(0).getGrossPremiumAmount());
						monthlyAPTCAmountDTO
								.setNextMonthNetPremium(enrollmentPremiumDtoList.get(0).getNetPremiumAmount());
						monthlyAPTCAmountDTO.setNextMonthAPTCAmount(enrollmentPremiumDtoList.get(0).getAptcAmount());
					}
				}
			}

			monthlyAPTCAmountDTOList.add(monthlyAPTCAmountDTO);
			nextMonthDate = null;
		}

		return monthlyAPTCAmountDTOList;
	}

	@Override
	public AccountUser retreiveLoggedInUserByEmail(final String loggedInUserEmail,
			EnrollmentResponse enrollmentResponse) throws GIException {
		if (StringUtils.isBlank(loggedInUserEmail)) {
			// Rule 1 throw exception if no user passed in Request
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_221);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_USER_MISSING_IN_REQUEST);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERROR_CODE_221,
					EnrollmentConstants.ERR_MSG_USER_MISSING_IN_REQUEST, EnrollmentConstants.HIGH);
		} else if (loggedInUserEmail.equalsIgnoreCase(GhixConstants.USER_NAME_CARRIER)) {
			// Rule 2 : If user passed is carrier@getinsured.com, then reject the request.
			// Use specific error code for this as System User Not Allowed.
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_222);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_CARRIER_USER_NOT_ALLOWED);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERROR_CODE_222,
					EnrollmentConstants.ERR_MSG_CARRIER_USER_NOT_ALLOWED, EnrollmentConstants.HIGH);
		} else {
			AccountUser accountUser = userService.findByEmail(loggedInUserEmail);
			if (accountUser != null) {
				return accountUser;
			} else {
				// If the user is not present in the system then reject the request. Use
				// specific error code for this as Admin User Not Found
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_223);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_USER_FOUND + loggedInUserEmail);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERROR_CODE_223,
						EnrollmentConstants.ERR_MSG_NO_USER_FOUND + loggedInUserEmail, EnrollmentConstants.HIGH);
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void updateEnrollment(EnrollmentUpdateDTO enrollmentUpdateDto, EnrollmentResponse response,
			Integer giWsPayloadId) throws GIException {
		List<String> enrollmentAppEventList = new ArrayList<String>();
		Boolean isCaCall = EnrollmentConstants.STATE_CODE_CA
				.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode());
		if (enrollmentUpdateDto != null) {
			if (enrollmentUpdateDto.getId() == null) {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_NULL);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
			} else {
				Map<String, String> requestParam = new HashMap<String, String>();
				Enrollment enrollmentDb = enrollmentRepository.findById(enrollmentUpdateDto.getId());
				if (enrollmentDb != null) {
					if (enrollmentUpdateDto.getConfirmationDateStr() != null) {
						enrollmentUpdateDto.setConfirmationDate(DateUtil.StringToDate(
								enrollmentUpdateDto.getConfirmationDateStr(), GhixConstants.REQUIRED_DATE_FORMAT));
					}
					String oldStatus = enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode();

					AccountUser user = getLoggedInUser();

					if ((DateUtils.isSameDay(enrollmentDb.getBenefitEffectiveDate(), enrollmentDb.getBenefitEndDate())
							|| (enrollmentDb.getEnrollmentStatusLkp() != null
									&& enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode()
											.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)))
							&& enrollmentUpdateDto.getStatus() == null) {
						response.setErrCode(EnrollmentConstants.ERROR_CODE_214);
						response.setErrMsg(EnrollmentConstants.ERR_MSG_ENRL_CANCEL_EDIT);
						response.setStatus(GhixConstants.RESPONSE_FAILURE);
						throw new GIException(EnrollmentConstants.ERR_MSG_ENRL_CANCEL_EDIT);
					}
					boolean isOverlap = validateEnrollmentOverlap(
							prepareCoverageValidationRequest(enrollmentUpdateDto, enrollmentDb));
					if (isOverlap && !DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEffectiveDate(),
							enrollmentUpdateDto.getBenefitEndDate())) {
						response.setErrCode(EnrollmentConstants.ERROR_CODE_216);
						response.setErrMsg(EnrollmentConstants.ERR_MSG_ENRLMNT_OVRLAP);
						response.setStatus(GhixConstants.RESPONSE_FAILURE);
						throw new GIException(EnrollmentConstants.ERR_MSG_ENRLMNT_OVRLAP);
					} else {
						Enrollee subscriber = enrollmentDb.getSubscriberForEnrollment();
						Date subscriberStartDate = null;
						Date subscriberEndDate = null;
						if (null != enrollmentUpdateDto.getMembers() && !enrollmentUpdateDto.getMembers().isEmpty()) {
							for (EnrolleeDataUpdateDTO enrolleeDto : enrollmentUpdateDto.getMembers()) {
								if (subscriber.getId() == enrolleeDto.getEnrolleeId()) {
									subscriberStartDate = enrolleeDto.getEffectiveStartDate();
									subscriberEndDate = enrolleeDto.getEffectiveEndDate();
									if (subscriberStartDate == null || subscriberEndDate == null) {
										response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
										response.setErrMsg(EnrollmentConstants.ERR_MSG_DATE_NULL);
										response.setStatus(GhixConstants.RESPONSE_FAILURE);
										throw new GIException(EnrollmentConstants.ERR_MSG_DATE_NULL);
									}
									if (!DateUtils.isSameDay(subscriberStartDate,
											enrollmentUpdateDto.getBenefitEffectiveDate())
											|| !DateUtils.isSameDay(subscriberEndDate,
													enrollmentUpdateDto.getBenefitEndDate())) {
										// && !DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEffectiveDate(),
										// enrollmentDb.getBenefitEffectiveDate())
										// && !DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEndDate(),
										// enrollmentDb.getBenefitEndDate())
										response.setErrCode(EnrollmentConstants.ERROR_CODE_218);
										response.setErrMsg(EnrollmentConstants.ERR_MSG_SUBSCRIBER_COVERAGE_MISMATCH);
										response.setStatus(GhixConstants.RESPONSE_FAILURE);
										throw new GIException(EnrollmentConstants.ERR_MSG_SUBSCRIBER_COVERAGE_MISMATCH);
									}
									// Adding Condition to verify Existing Subscriber Date Matches with Enrollment
									// or not before update.
									if (!DateUtils.isSameDay(subscriber.getEffectiveStartDate(),
											enrollmentDb.getBenefitEffectiveDate())
											|| !DateUtils.isSameDay(subscriber.getEffectiveEndDate(),
													enrollmentDb.getBenefitEndDate())) {
										response.setErrCode(EnrollmentConstants.ERROR_CODE_218);
										response.setErrMsg(EnrollmentConstants.ERR_MSG_SUBSCRIBER_COVERAGE_MISMATCH);
										response.setStatus(GhixConstants.RESPONSE_FAILURE);
										throw new GIException(EnrollmentConstants.ERR_MSG_SUBSCRIBER_COVERAGE_MISMATCH);
									}
									//HIX-114767 Set last premium paid through end date
									if (null != enrollmentUpdateDto.getPremiumPaidToDateEnd()
											&& enrolleeDto.getTxnType().toString().equalsIgnoreCase(
													EnrolleeDataUpdateDTO.TRANSACTION_TYPE.TERM.toString())
											&& EnrollmentConstants.EVENT_REASON_NON_PAYMENT
													.equalsIgnoreCase(enrolleeDto.getTxnReason())) {
										enrollmentDb.setPremiumPaidToDateEnd(enrollmentUpdateDto.getPremiumPaidToDateEnd());
									}
									break;
								}
							}
						}
					}
					/*
					 * HIX-110907 Disabling SLCSP override from edit tool if(isCaCall){
					 * validateSlcspOverrideRequest(enrollmentDb, enrollmentUpdateDto, response); }
					 */
					boolean isEnrollmentStatusChange = updateEnrollmentStatus(enrollmentDb, enrollmentUpdateDto,
							response, user, enrollmentAppEventList);
					boolean isEnrollmentDateChange = updateEnrollmentCoverageDates(enrollmentDb, enrollmentUpdateDto,
							response, user, enrollmentAppEventList);
					boolean isEnrolleeDateChange = updateEnrolleeCoverageDates(enrollmentDb, enrollmentUpdateDto,
							response, user, enrollmentAppEventList, isEnrollmentDateChange);
					boolean isMemberDataUpdate = updateMemberData(enrollmentDb, enrollmentUpdateDto, user, response,
							enrollmentAppEventList);
					boolean isAptcChange = updateEnrollmentAptcEditTool(enrollmentDb, enrollmentUpdateDto, user,
							response, enrollmentAppEventList);

					boolean isStateSubsidyChange = false;
					String stateSubsidyEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);

					if("Y".equalsIgnoreCase(stateSubsidyEnabled)){
						isStateSubsidyChange = updateEnrollmentSSEditTool(enrollmentDb, enrollmentUpdateDto, user,
								response, enrollmentAppEventList);
					}
					if (isMemberDataUpdate) {
						updateSubscriberSponsorInfo(enrollmentDb);
					}
					createSubscriberLevelEvent(enrollmentDb, user, enrollmentUpdateDto);
					List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
					enrollmentList.add(enrollmentDb);
					if (isEnrollmentDateChange || isEnrolleeDateChange || isEnrollmentStatusChange) {
						enrollmentList.addAll(synchronizeQuotingDates(enrollmentDb, user,
								EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL_QUOTING_DATE_UPDATE));
						for (Enrollment enrollment : enrollmentList) {
							enrollment.setUpdatedBy(user);
							enrollmentRequotingService.updateMonthlyPremiumForEnrollment(enrollment, true, false, null, true);
						}
					} else if (isAptcChange || isStateSubsidyChange) {
						enrollmentRequotingService.updatePremiumAfterMonthlyAPTCChange(enrollmentDb, false, isAptcChange, isStateSubsidyChange);
					}

					if (null != enrollmentDb.getAptcAmt() && null != enrollmentDb.getGrossPremiumAmt()
							&& enrollmentDb.getAptcAmt().compareTo(enrollmentDb.getGrossPremiumAmt()) > 0) {
						capEnrollmentAptc(enrollmentDb);
					}

					if (!isMemberDataUpdate && !isEnrollmentDateChange && !isEnrolleeDateChange
							&& !isEnrollmentStatusChange && !isAptcChange && !isStateSubsidyChange) {
						response.setStatus(GhixConstants.RESPONSE_FAILURE);
						response.setErrCode(EnrollmentConstants.ERROR_CODE_230);
						response.setErrMsg(EnrollmentConstants.ERR_MSG_UNABLE_TO_PROCESS);
					}

					/*
					 * HIX-110907 if(isCaCall){ overrideSLCSPAmount(enrollmentDb,
					 * enrollmentUpdateDto); }
					 */
					if (response.getStatus() == null
							|| !response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
						// Do we need to update Enrollment table if no changes made ??
						enrollmentDb.setUpdatedOn(new TSDate());
						enrollmentDb.setUpdatedBy(user);
						enrollmentDb.setGiWsPayloadId(giWsPayloadId);

						String currentStatus = enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode();
						if (!EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(oldStatus)
								&& !EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(oldStatus)
								&& (EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(currentStatus)
										|| EnrollmentConstants.ENROLLMENT_STATUS_TERM
												.equalsIgnoreCase(currentStatus))) {
							if (EnrollmentConfiguration.isIdCall() && EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE
									.equalsIgnoreCase(enrollmentDb.getInsuranceTypeLkp().getLookupValueCode())) {
								List<Enrollment> dentalEnrollments = updateDentalAPTCOnHealthDisEnrollment(enrollmentDb,
										user, EnrollmentEvent.TRANSACTION_IDENTIFIER.APTC_CH_HLT_EDITTOOL_DISENROLL);
								if (dentalEnrollments != null && dentalEnrollments.size() > 0) {
									enrollmentList.addAll(dentalEnrollments);
								}
							}

						}

						// enrollmentRepository.save(enrollmentDb);
						enrollmentList = saveAllEnrollment(enrollmentList);
						requestParam.put(EnrollmentConstants.SEND_IN_834,
								String.valueOf(enrollmentUpdateDto.isSend834()).equalsIgnoreCase("true")
										? EnrollmentConstants.YES
										: EnrollmentConstants.NO);
						if (enrollmentAppEventList != null && !enrollmentAppEventList.isEmpty()) {
							for (String appEvent : enrollmentAppEventList) {
								enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(
										enrollmentRepository.findById(enrollmentDb.getId()), appEvent, requestParam,
										enrollmentUpdateDto.getComment(), user);
							}
						}
						if (isCaCall) {
							LOGGER.info("Async Call to enrollmentAhbxSyncService saveEnrollmentAhbxSync EDIT_TOOL");
							final List<Enrollment> finalEnrollmentList = enrollmentList;
							TransactionSynchronizationManager
									.registerSynchronization(new TransactionSynchronizationAdapter() {
										public void afterCommit() {
											enrollmentAhbxSyncService.saveEnrollmentAhbxSync(finalEnrollmentList,
													enrollmentDb.getHouseHoldCaseId(),
													EnrollmentAhbxSync.RecordType.EDIT_TOOL);
										}
									});
						}
					}

				} else {
					response.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_NOT_FOUND);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
				}

			}
		}
	}

	private void updateSubscriberSponsorInfo(Enrollment enrollmentDb) {
		if (enrollmentDb != null) {
			Enrollee sponsorEnrollee = enrollmentDb.getHouseholdContactForEnrollment();
			Enrollee subscriber = enrollmentDb.getSubscriberForEnrollment();

			if (EnrollmentConfiguration.isCaCall() && subscriber != null) {
				int subscriberAge = EnrollmentUtils.getAge(subscriber.getBirthDate(),
						subscriber.getEffectiveStartDate());
				if (subscriberAge >= EnrollmentConstants.AGE_18) {
					sponsorEnrollee = subscriber;
				}
			}
			if (sponsorEnrollee != null) {
				enrollmentDb.setSponsorName(EnrollmentUtils.concatEnrolleeName(sponsorEnrollee));
				enrollmentDb.setSponsorTaxIdNumber(sponsorEnrollee.getTaxIdNumber());
			}
			if (subscriber != null) {
				enrollmentDb.setSubscriberName(EnrollmentUtils.concatEnrolleeName(subscriber));
				enrollmentDb.setExchgSubscriberIdentifier(subscriber.getExchgIndivIdentifier());
			}

		}
	}

	private void createSubscriberLevelEvent(Enrollment enrollmentDb, AccountUser user) {
		if (enrollmentDb != null) {
			boolean enrollmentUpdated = false;
			Enrollee subscriber = enrollmentDb.getSubscriberForEnrollment();
			if (subscriber != null && !subscriber.isEventCreated()) {
				List<Enrollee> enrollees = enrollmentDb.getEnrolleesAndSubscriber();
				if (enrollees != null && enrollees.size() > 0) {
					for (Enrollee enrollee : enrollees) {
						if (enrollee.isEventCreated()) {
							enrollmentUpdated = true;
							break;
						}
					}

					if (enrollmentUpdated) {
						subscriber.setEventCreated(true);
						createEnrolleeEvent(subscriber, EnrollmentConstants.EVENT_TYPE_CHANGE,
								EnrollmentConstants.EVENT_REASON_AI, user, true,
								EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
					}
				}
			}

		}

	}

	private void validateEnrollmentStatusChange(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
			EnrollmentResponse response) throws GIException {

		String oldStatus = enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode();
		String newStatus = enrollmentUpdateDto.getStatus();
		/*
		 * if(!((
		 * EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(oldStatus) &&
		 * (EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(newStatus) ||
		 * EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus) ||
		 * EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(newStatus) ))
		 * || ((EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(oldStatus)
		 * || EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(oldStatus))
		 * &&EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(newStatus))
		 * ||(EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(oldStatus)
		 * && (EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus) ||
		 * EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(newStatus)) )
		 * ||(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(oldStatus)
		 * && EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus)) )
		 * ){ response.setErrCode(EnrollmentConstants.ERROR_CODE_217);
		 * response.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_STATUS_CHANGE
		 * +" Can not Change From "+oldStatus +" to "+newStatus);
		 * response.setStatus(GhixConstants.RESPONSE_FAILURE); throw new
		 * GIException(EnrollmentConstants.ERR_MSG_INVALID_STATUS_CHANGE
		 * +" Can not Change From "+oldStatus +" to "+newStatus);
		 * 
		 * }
		 */

		if (enrollmentUpdateDto.getBenefitEndDate() == null) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFF_END_NULL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFF_END_NULL);
		}

		if (!EnrollmentUtils.isSameYear(enrollmentUpdateDto.getBenefitEndDate(),
				enrollmentUpdateDto.getBenefitEffectiveDate())
				&& !EnrollmentUtils.isSameYear(enrollmentUpdateDto.getBenefitEndDate(),
						enrollmentDb.getBenefitEffectiveDate())) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_215);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
		}

		if (enrollmentUpdateDto.getBenefitEffectiveDate().after(enrollmentUpdateDto.getBenefitEndDate()) && !DateUtils
				.isSameDay(enrollmentUpdateDto.getBenefitEffectiveDate(), enrollmentUpdateDto.getBenefitEndDate())) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_212);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
		}

		if (EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(newStatus)
				&& !DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEndDate(),
						EnrollmentUtils.getYearEndDate(enrollmentDb.getBenefitEffectiveDate()))) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_218);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_PENDING_STATUS_DATE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_PENDING_STATUS_DATE);
		}

		if ((EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus)
				|| EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(newStatus))
				&& enrollmentUpdateDto.getConfirmationDate() == null) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_219);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_CONFIRMATION_DATE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_CONFIRMATION_DATE);
		}

		// validate Member End Date
		for (EnrolleeDataUpdateDTO member : enrollmentUpdateDto.getMembers()) {
			if (EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(newStatus)
					&& !DateUtils.isSameDay(member.getEffectiveEndDate(),
							EnrollmentUtils.getYearEndDate(enrollmentDb.getBenefitEffectiveDate()))) {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_218);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_PENDING_STATUS_DATE);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_PENDING_STATUS_DATE);
			}
		}

	}

	private boolean updateEnrollmentStatus(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
			EnrollmentResponse response, final AccountUser user, List<String> enrollmentAppEventList)
			throws GIException {
		boolean isEnrollmentStatusChange = false;
		if (enrollmentDb != null && enrollmentUpdateDto != null && isNotNullAndEmpty(enrollmentUpdateDto.getStatus())) {
			// validate Status Change Request

			String oldStatus = enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode();
			String newStatus = enrollmentUpdateDto.getStatus();
			validateEnrollmentStatusChange(enrollmentDb, enrollmentUpdateDto, response);
			Date effectiveEndDate = EnrollmentUtils.getEODDate(enrollmentUpdateDto.getBenefitEndDate());

			if (EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(oldStatus)
					&& (EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(newStatus)
							|| EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus)
							|| EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(newStatus))) {
				// Cancel to Pending/Term/Confirm

				enrollmentDb.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
						EnrollmentConstants.ENROLLMENT_STATUS, newStatus.toUpperCase()));
				enrollmentDb.setBenefitEndDate(effectiveEndDate);
				enrollmentDb.setAbortTimestamp(null); // HIX-108647 - Set abort timestamp to null

				if (!EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(newStatus)) {
					enrollmentDb.setEnrollmentConfirmationDate(enrollmentUpdateDto.getConfirmationDate());
				} else {
					enrollmentDb.setEnrollmentConfirmationDate(null);
				}
				isEnrollmentStatusChange = true;

				for (Enrollee enrollee : enrollmentDb.getEnrolleesAndSubscriber()) {
					enrollee.setEffectiveEndDate(effectiveEndDate);
					enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
							EnrollmentConstants.ENROLLMENT_STATUS, newStatus.toUpperCase()));
					newValidateEnrolleeDate(enrollee, enrollmentDb, response);
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_ADDITION,
							EnrollmentConstants.EVENT_REASON_REENROLLMENT, user, true, false,
							EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
					if (!EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(newStatus)
							&& null != enrollmentUpdateDto.getHealthCoveragePolicyNo()) {
						enrollee.setHealthCoveragePolicyNo(enrollmentUpdateDto.getHealthCoveragePolicyNo());
					}
					enrollee.setEventCreated(true);
					enrollee.setUpdatedBy(user);
				}

			} else if ((EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(oldStatus)
					|| EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(oldStatus))
					&& EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(newStatus)) {
				// TERM/Confirm to Pending
				isEnrollmentStatusChange = true;
				enrollmentDb.setBenefitEndDate(effectiveEndDate);
				enrollmentDb.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
						EnrollmentConstants.ENROLLMENT_STATUS, newStatus.toUpperCase()));
				enrollmentDb.setEnrollmentConfirmationDate(null);
				for (Enrollee enrollee : enrollmentDb.getEnrolleesAndSubscriber()) {
					enrollee.setEffectiveEndDate(effectiveEndDate);
					enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
							EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_PENDING));
					if (EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(oldStatus)) {
						// Create Reinstatement Event
						newValidateEnrolleeDate(enrollee, enrollmentDb, response);
						createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_ADDITION,
								EnrollmentConstants.EVENT_REASON_REENROLLMENT, user, true, false,
								EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
						enrollee.setEventCreated(true);
						enrollee.setUpdatedBy(user);
					}
				}
			} else if ((EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(oldStatus)
					|| EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(oldStatus))
					&& (EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus)
							|| EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(newStatus))) {
				// PENDING to CONFIRM/TERM
				isEnrollmentStatusChange = true;
				enrollmentDb.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
						EnrollmentConstants.ENROLLMENT_STATUS, newStatus.toUpperCase()));
				enrollmentDb.setBenefitEndDate(effectiveEndDate);
				if (EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(oldStatus)) {
					enrollmentDb.setEnrollmentConfirmationDate(enrollmentUpdateDto.getConfirmationDate());
				}
				for (Enrollee enrollee : enrollmentDb.getEnrolleesAndSubscriber()) {
					enrollee.setEffectiveEndDate(effectiveEndDate);
					enrollee.setEnrolleeLkpValue(lookupService
							.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, newStatus));
					newValidateEnrolleeDate(enrollee, enrollmentDb, response);
					if (EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus)) {
						// Create Reinstatement Event
						enrollee.setDisenrollTimestamp(new TSDate());
						createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
								EnrollmentConstants.EVENT_REASON_29, user, true, false,
								EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);

					} else {
						createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_ADDITION,
								EnrollmentConstants.EVENT_REASON_INITIAL_ENROLLMENT, user, false, false,
								EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
					}
					if (null != enrollmentUpdateDto.getHealthCoveragePolicyNo()) {
						enrollee.setHealthCoveragePolicyNo(enrollmentUpdateDto.getHealthCoveragePolicyNo());
					}
					enrollee.setEventCreated(true);
					enrollee.setUpdatedBy(user);
				}

			}
			if (isEnrollmentStatusChange) {
				enrollmentDb.setUpdatedBy(user);
				enrollmentAppEventList.add(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_STATUS_CHANGED.toString());
			}

		}
		return isEnrollmentStatusChange;
	}

	private boolean updateEnrollmentCoverageDates(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
			EnrollmentResponse response, final AccountUser user, List<String> enrollmentAppEventList)
			throws GIException {
		boolean isEnrollmentDateChange = false;
		Date effectiveStartDateDb = enrollmentDb.getBenefitEffectiveDate();
		Date effectiveEndDateDb = enrollmentDb.getBenefitEndDate();
		if (!DateUtils.isSameDay(effectiveStartDateDb, enrollmentUpdateDto.getBenefitEffectiveDate())) {
			// Enrollment Start Date Changes
			newUpdateEnrollmentEffectiveDate(enrollmentDb, enrollmentUpdateDto, response, user);
			if (enrollmentAppEventList != null && enrollmentAppEventList.isEmpty()) {
				enrollmentAppEventList
						.add(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_START_DATE_EDIT.toString());
			}
			isEnrollmentDateChange = true;
		}
		if (!DateUtils.isSameDay(effectiveEndDateDb, enrollmentUpdateDto.getBenefitEndDate())) {
			// Enrollment End Date Changes
			newUpdateEnrollmentEndDate(enrollmentDb, enrollmentUpdateDto, user, response);
			if (enrollmentAppEventList != null && enrollmentAppEventList.isEmpty()) {
				enrollmentAppEventList.add(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_END_DATE_EDIT.toString());
			}
			isEnrollmentDateChange = true;
		}
		return isEnrollmentDateChange;
	}

	@Override
	public EnrollmentCoverageValidationRequest prepareCoverageValidationRequest(EnrollmentUpdateDTO enrollmentUpdateDto,
			Enrollment enrollmentDb) {
		EnrollmentCoverageValidationRequest request = new EnrollmentCoverageValidationRequest();
		request.setEnrollmentId(enrollmentUpdateDto.getId());
		request.setHouseholdCaseId(enrollmentUpdateDto.getHouseholdId().toString());
		InsuranceType insuranceType = null;
		if (EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE
				.equalsIgnoreCase(enrollmentDb.getInsuranceTypeLkp().getLookupValueCode())) {
			insuranceType = InsuranceType.HLT;
		} else if (EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE
				.equalsIgnoreCase(enrollmentDb.getInsuranceTypeLkp().getLookupValueCode())) {
			insuranceType = InsuranceType.DEN;
		}
		request.setInsuranceType(insuranceType);
		List<EnrollmentMemberCoverageDTO> memberList = new ArrayList<EnrollmentMemberCoverageDTO>();
		if (null != enrollmentUpdateDto.getMembers() && !enrollmentUpdateDto.getMembers().isEmpty()) {
			for (EnrolleeDataUpdateDTO enrolleeDto : enrollmentUpdateDto.getMembers()) {
				EnrollmentMemberCoverageDTO memberDto = new EnrollmentMemberCoverageDTO();
				/*
				 * Enrollee enrollee =
				 * enrollmentDb.getEnrolleeByMemberId(enrolleeDto.getExchgIndivIdentifier());
				 * memberDto.setEnrolleeId((null != enrollee) ? enrollee.getId(): null);
				 */
				Enrollee enrollee = enrollmentDb.getEnrolleeById(enrolleeDto.getEnrolleeId());
				if (EnrollmentConstants.ENROLLMENT_STATUS_CANCEL
						.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode())
						&& !enrolleeDto.getEffectiveStartDateStr()
								.equalsIgnoreCase(enrolleeDto.getEffectiveEndDateStr())) {
					memberDto.setNewMember(true);
				}
				memberDto.setEnrolleeId(enrolleeDto.getEnrolleeId());
				memberDto.setMemberId(enrollee != null ? enrollee.getExchgIndivIdentifier() : null);
				memberDto.setMemberStartDate(enrolleeDto.getEffectiveStartDateStr());
				memberDto.setMemberEndDate(enrolleeDto.getEffectiveEndDateStr());
				memberList.add(memberDto);
			}
		}
		request.setMemberList(memberList);
		return request;
	}

	private boolean updateEnrollmentAptcEditTool(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
			final AccountUser user, EnrollmentResponse response, List<String> enrollmentAppEventList) {
		boolean isAptcChange = false;
		if (null != enrollmentUpdateDto.getEnrollmentPremiumDtoList()
				&& !enrollmentUpdateDto.getEnrollmentPremiumDtoList().isEmpty()) {
			Map<Integer, EnrollmentPremium> enrollmentPremiumMap = enrollmentDb.getEnrollmentPremiumMap();
			for (EnrollmentPremiumDTO enrlPremiumDto : enrollmentUpdateDto.getEnrollmentPremiumDtoList()) {
				EnrollmentPremium enrollmentPremiumDb = null;
				if (enrollmentPremiumMap.containsKey(enrlPremiumDto.getMonth())) {
					enrollmentPremiumDb = enrollmentDb.getEnrollmentPremiumForMonth(enrlPremiumDto.getMonth());
					if (enrlPremiumDto.isFinancial() && ((null != enrlPremiumDto.getAptcAmount()
							&& null != enrollmentPremiumDb.getAptcAmount()
							&& (enrlPremiumDto.getAptcAmount().compareTo(enrollmentPremiumDb.getAptcAmount()) != 0))
							|| (null == enrollmentPremiumDb.getAptcAmount()
									&& null != enrlPremiumDto.getAptcAmount()))) {
						isAptcChange = true;
						enrollmentPremiumDb.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
						enrollmentPremiumDb.setAptcAmount(enrlPremiumDto.getAptcAmount());
					} else if (!enrlPremiumDto.isFinancial() && null != enrollmentPremiumDb.getAptcAmount()) {
						isAptcChange = true;
						enrollmentPremiumDb.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
						enrollmentPremiumDb.setAptcAmount(null);
					} 
					//Group Max APTC updates
					if (enrlPremiumDto.isFinancial() && ((null != enrlPremiumDto.getMaxAptc()
							&& null != enrollmentPremiumDb.getMaxAptc()
							&& (enrlPremiumDto.getMaxAptc().compareTo(enrollmentPremiumDb.getMaxAptc()) != 0)))) {
						isAptcChange = true;
						enrollmentPremiumDb.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
						enrollmentPremiumDb.setMaxAptc(enrlPremiumDto.getMaxAptc());
					} else if (enrlPremiumDto.isFinancial()
							&& (null != enrlPremiumDto.getMaxAptc() && null == enrollmentPremiumDb.getMaxAptc())) {
						isAptcChange = true;
						enrollmentPremiumDb.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
						enrollmentPremiumDb.setMaxAptc(enrlPremiumDto.getMaxAptc());
					}
				} else {
					isAptcChange = true;
					enrollmentPremiumDb = new EnrollmentPremium();
					enrollmentPremiumDb.setAptcAmount(enrlPremiumDto.getAptcAmount());
					enrollmentPremiumDb.setMaxAptc(enrlPremiumDto.getMaxAptc());
					enrollmentPremiumDb.setAptcChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
					enrollmentDb.getEnrollmentPremiums().add(enrollmentPremiumDb);
				}
			}
			if (isAptcChange) {
				enrollmentAppEventList.add(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_APTC_EDIT.toString());
				List<Enrollee> enrollees = enrollmentDb.getEnrollees();
				for (Enrollee enrollee : enrollees) {
					if (!enrollee.isEventCreated()) {
						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = getMemberTxnIdentifier(
								enrollmentUpdateDto, enrollee);
						String txnReason = getMemberTxnReason(enrollmentUpdateDto, enrollee);
						createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(),
								user, EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_29,
								txnReason);
					}
				}
			}
		} else {
			LOGGER.error("No premium records associated with enrollment ID :: " + enrollmentDb.getId());
		}
		return isAptcChange;
	}

	private boolean updateEnrollmentSSEditTool(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
												 final AccountUser user, EnrollmentResponse response, List<String> enrollmentAppEventList) {
		boolean isStateSubsidyChange = false;
		if (null != enrollmentUpdateDto.getEnrollmentPremiumDtoList()
				&& !enrollmentUpdateDto.getEnrollmentPremiumDtoList().isEmpty()) {
			Map<Integer, EnrollmentPremium> enrollmentPremiumMap = enrollmentDb.getEnrollmentPremiumMap();
			for (EnrollmentPremiumDTO enrlPremiumDto : enrollmentUpdateDto.getEnrollmentPremiumDtoList()) {
				EnrollmentPremium enrollmentPremiumDb = null;
				if (enrollmentPremiumMap.containsKey(enrlPremiumDto.getMonth())) {
					enrollmentPremiumDb = enrollmentDb.getEnrollmentPremiumForMonth(enrlPremiumDto.getMonth());
					if (enrlPremiumDto.isFinancial() && ((null != enrlPremiumDto.getStateSubsidyAmount()
							&& null != enrollmentPremiumDb.getStateSubsidyAmount()
							&& (enrlPremiumDto.getStateSubsidyAmount().compareTo(enrollmentPremiumDb.getStateSubsidyAmount()) != 0))
							|| (null == enrollmentPremiumDb.getStateSubsidyAmount()
							&& null != enrlPremiumDto.getStateSubsidyAmount()))) {
						isStateSubsidyChange = true;
						enrollmentPremiumDb.setStateSubsidyChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
						enrollmentPremiumDb.setStateSubsidyAmount(enrlPremiumDto.getStateSubsidyAmount());
						enrollmentPremiumDb.setMaxStateSubsidy(enrlPremiumDto.getMaxStateSubsidy());
					} else if (!enrlPremiumDto.isFinancial() && null != enrollmentPremiumDb.getStateSubsidyAmount()) {
						isStateSubsidyChange = true;
						enrollmentPremiumDb.setStateSubsidyChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
						enrollmentPremiumDb.setStateSubsidyAmount(null);
					}
					if (enrlPremiumDto.isFinancial() && ((null != enrlPremiumDto.getMaxStateSubsidy()
							&& null != enrollmentPremiumDb.getMaxStateSubsidy()
							&& (enrlPremiumDto.getMaxStateSubsidy().compareTo(enrollmentPremiumDb.getMaxStateSubsidy()) != 0)))) {
						isStateSubsidyChange = true;
						enrollmentPremiumDb.setStateSubsidyChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
						enrollmentPremiumDb.setMaxStateSubsidy(enrlPremiumDto.getMaxStateSubsidy());
					} else if (enrlPremiumDto.isFinancial()
							&& (null != enrlPremiumDto.getMaxStateSubsidy() && null == enrollmentPremiumDb.getMaxStateSubsidy())) {
						isStateSubsidyChange = true;
						enrollmentPremiumDb.setStateSubsidyChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
						enrollmentPremiumDb.setMaxStateSubsidy(enrlPremiumDto.getMaxStateSubsidy());
					}
				} else {
					isStateSubsidyChange = true;
					enrollmentPremiumDb = new EnrollmentPremium();
					enrollmentPremiumDb.setStateSubsidyAmount(enrlPremiumDto.getStateSubsidyAmount());
					enrollmentPremiumDb.setMaxStateSubsidy(enrlPremiumDto.getMaxStateSubsidy());
					enrollmentPremiumDb.setStateSubsidyChangeLogic(EnrollmentPremium.APTC_CHANGE_LOGIC.EDITTOOL.toString());
					enrollmentDb.getEnrollmentPremiums().add(enrollmentPremiumDb);
				}
			}
			if (isStateSubsidyChange) {
				enrollmentAppEventList.add(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SS_EDIT.toString());
				List<Enrollee> enrollees = enrollmentDb.getEnrollees();
				for (Enrollee enrollee : enrollees) {
					if (!enrollee.isEventCreated()) {
						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = getMemberTxnIdentifier(
								enrollmentUpdateDto, enrollee);
						String txnReason = getMemberTxnReason(enrollmentUpdateDto, enrollee);
						createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(),
								user, EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_29,
								txnReason);
					}
				}
			}
		} else {
			LOGGER.error("No premium records associated with enrollment ID :: " + enrollmentDb.getId());
		}
		return isStateSubsidyChange;
	}

	private boolean overrideSLCSPAmount(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto) {
		boolean isSlcspChange = false;
		if (null != enrollmentUpdateDto.getEnrollmentPremiumDtoList()
				&& !enrollmentUpdateDto.getEnrollmentPremiumDtoList().isEmpty()) {
			Map<Integer, EnrollmentPremium> enrollmentPremiumMap = enrollmentDb.getEnrollmentPremiumMap();
			for (EnrollmentPremiumDTO enrlPremiumDto : enrollmentUpdateDto.getEnrollmentPremiumDtoList()) {
				EnrollmentPremium enrollmentPremiumDb = null;
				if (enrollmentPremiumMap.containsKey(enrlPremiumDto.getMonth())) {
					enrollmentPremiumDb = enrollmentDb.getEnrollmentPremiumForMonth(enrlPremiumDto.getMonth());
					if ((null != enrlPremiumDto.getSlcspPremiumAmount()
							&& null != enrollmentPremiumDb.getSlcspPremiumAmount()
							&& enrlPremiumDto.getSlcspPremiumAmount()
									.compareTo(enrollmentPremiumDb.getSlcspPremiumAmount()) != 0)
							|| (null == enrollmentPremiumDb.getSlcspPremiumAmount()
									&& null != enrlPremiumDto.getSlcspPremiumAmount())) {
						isSlcspChange = true;
						enrollmentPremiumDb.setSlcspPremiumAmount(enrlPremiumDto.getSlcspPremiumAmount());
						enrollmentPremiumDb.setSlcspPlanid(enrlPremiumDto.getSlcspPlanid());
					}
				} else {
					isSlcspChange = true;
					enrollmentPremiumDb = new EnrollmentPremium();
					enrollmentPremiumDb.setAptcAmount(enrlPremiumDto.getAptcAmount());
					enrollmentDb.getEnrollmentPremiums().add(enrollmentPremiumDb);
				}
			}
		} else {
			LOGGER.error("No premium records associated with enrollment ID :: " + enrollmentDb.getId());
		}
		return isSlcspChange;
	}

	private void validateSlcspOverrideRequest(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
			EnrollmentResponse response) throws GIException {
		Integer monthsBetween = Months.monthsBetween(new DateTime(enrollmentUpdateDto.getBenefitEffectiveDate()),
				new DateTime(enrollmentUpdateDto.getBenefitEndDate())).getMonths();
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(enrollmentUpdateDto.getBenefitEffectiveDate());
		int nullCount = 0;
		int startMonth = cal.get(Calendar.MONTH) + 1;
		for (int month = startMonth; month <= startMonth + monthsBetween; month++) {
			for (EnrollmentPremiumDTO epDto : enrollmentUpdateDto.getEnrollmentPremiumDtoList()) {
				if (epDto.getMonth() == month && null == epDto.getSlcspPremiumAmount()) {
					nullCount++;
					break;
				}
			}
		}
		if (!(nullCount == 0 || nullCount == (monthsBetween + 1))) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_220);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_NULL_SLCSP_OVERRIDE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_NULL_SLCSP_OVERRIDE);
		}

	}

	private boolean updateEnrolleeCoverageDates(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
			EnrollmentResponse response, final AccountUser user, List<String> enrollmentAppEventList,
			boolean isEnrollmentDateChange) throws GIException {
		// Enrollee Start Date Changes
		boolean isEnrolleeDateChange = false;
		if (newUpdateEnrolleeStartDate(enrollmentDb, enrollmentUpdateDto, response, user, isEnrollmentDateChange)
				&& !isEnrollmentDateChange) {
			enrollmentAppEventList.add(EnrollmentConstants.EnrollmentAppEvent.MEMBER_START_DATE_EDIT.toString());
			isEnrolleeDateChange = true;
		}
		// Enrollee End Date Changes
		if (newUpdateEnrolleeEndDate(enrollmentDb, enrollmentUpdateDto, response, user, isEnrollmentDateChange)
				&& !isEnrollmentDateChange) {
			enrollmentAppEventList.add(EnrollmentConstants.EnrollmentAppEvent.MEMBER_END_DATE_EDIT.toString());
			isEnrolleeDateChange = true;
		}
		if (isEnrolleeDateChange) {
			for (Enrollee enrollee : enrollmentDb.getEnrolleesAndSubscriber()) {
				newValidateEnrolleeDate(enrollee, enrollmentDb, response);
			}
		}

		return isEnrolleeDateChange;
	}

	private void newUpdateEnrollmentEffectiveDate(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
			EnrollmentResponse response, AccountUser user) throws GIException {

		newValidateEnrollmentStartDateChangeRequest(enrollmentDb, enrollmentUpdateDto, response,
				EnrollmentUtils.getPendingTermEnrollees(enrollmentDb.getEnrollees()));

		Date currentEnrlEffectiveDate = enrollmentDb.getBenefitEffectiveDate();
		Date effectiveStartDate = enrollmentUpdateDto.getBenefitEffectiveDate();
		boolean isUpdateMemberDate = false;
		if (!DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEffectiveDate(), currentEnrlEffectiveDate)) {
			if (effectiveStartDate.before(currentEnrlEffectiveDate)) {
				// say current date is 1 june and new date is 1 March

				// Update Enrollment
				enrollmentDb.setBenefitEffectiveDate(effectiveStartDate);
				if (enrollmentDb.getNetPremEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getNetPremEffDate())) {
					enrollmentDb.setNetPremEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getEmplContEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getEmplContEffDate())) {
					enrollmentDb.setEmplContEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getGrossPremEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getGrossPremEffDate())) {
					enrollmentDb.setGrossPremEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getEhbEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getEhbEffDate())) {
					enrollmentDb.setEhbEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getStateEhbEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getStateEhbEffDate())) {
					enrollmentDb.setStateEhbEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getDntlEhbEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getDntlEhbEffDate())) {
					enrollmentDb.setDntlEhbEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getFinancialEffectiveDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getFinancialEffectiveDate())) {
					enrollmentDb.setFinancialEffectiveDate(effectiveStartDate);
				}
				if (enrollmentDb.getAptcEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getAptcEffDate())) {
					enrollmentDb.setAptcEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getStateSubsidyEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getStateSubsidyEffDate())) {
					enrollmentDb.setStateSubsidyEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getSlcspEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getSlcspEffDate())) {
					enrollmentDb.setSlcspEffDate(effectiveStartDate);
				}

				if (enrollmentDb.getCsrEffDate() != null
						&& DateUtils.isSameDay(currentEnrlEffectiveDate, enrollmentDb.getCsrEffDate())) {
					enrollmentDb.setCsrEffDate(effectiveStartDate);
				}
				isUpdateMemberDate = true;

			} else if (effectiveStartDate.after(currentEnrlEffectiveDate)) {
				// say current date is 1 june and new date is 1 Aug

				enrollmentDb.setBenefitEffectiveDate(effectiveStartDate);

				if (enrollmentDb.getAptcEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getAptcEffDate())
						&& effectiveStartDate.after(enrollmentDb.getAptcEffDate())) {
					enrollmentDb.setAptcEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getStateSubsidyEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getStateSubsidyEffDate())
						&& effectiveStartDate.after(enrollmentDb.getStateSubsidyEffDate())) {
					enrollmentDb.setStateSubsidyEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getEmplContEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getEmplContEffDate())
						&& effectiveStartDate.after(enrollmentDb.getEmplContEffDate())) {
					enrollmentDb.setEmplContEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getEhbEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getStateEhbEffDate())
						&& effectiveStartDate.after(enrollmentDb.getStateEhbEffDate())) {
					enrollmentDb.setEhbEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getStateEhbEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getStateEhbEffDate())
						&& effectiveStartDate.after(enrollmentDb.getStateEhbEffDate())) {
					enrollmentDb.setStateEhbEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getDntlEhbEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getDntlEhbEffDate())
						&& effectiveStartDate.after(enrollmentDb.getDntlEhbEffDate())) {
					enrollmentDb.setDntlEhbEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getFinancialEffectiveDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getFinancialEffectiveDate())
						&& effectiveStartDate.after(enrollmentDb.getFinancialEffectiveDate())) {
					enrollmentDb.setFinancialEffectiveDate(effectiveStartDate);
				}
				if (enrollmentDb.getNetPremEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getNetPremEffDate())
						&& effectiveStartDate.after(enrollmentDb.getNetPremEffDate())) {
					enrollmentDb.setNetPremEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getGrossPremEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getGrossPremEffDate())
						&& effectiveStartDate.after(enrollmentDb.getGrossPremEffDate())) {
					enrollmentDb.setGrossPremEffDate(effectiveStartDate);
				}
				if (enrollmentDb.getSlcspEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getSlcspEffDate())
						&& effectiveStartDate.after(enrollmentDb.getSlcspEffDate())) {
					enrollmentDb.setSlcspEffDate(effectiveStartDate);
				}

				if (enrollmentDb.getCsrEffDate() != null
						&& !DateUtils.isSameDay(effectiveStartDate, enrollmentDb.getCsrEffDate())
						&& effectiveStartDate.after(enrollmentDb.getCsrEffDate())) {
					enrollmentDb.setCsrEffDate(effectiveStartDate);
				}
				isUpdateMemberDate = true;
			}

			if (isUpdateMemberDate) {
				enrollmentDb.setUpdatedBy(user);

				// Update Enrollee
				for (Enrollee enrollee : EnrollmentUtils.getPendingTermEnrollees(enrollmentDb.getEnrollees())) {
					String status = enrollee.getEnrolleeLkpValue().getLookupValueCode();
					if (!DateUtil.isEqual(effectiveStartDate, enrollee.getEffectiveStartDate())
							&& EnrollmentUtils.isEqualOrAfter(enrollee.getBirthDate(), effectiveStartDate)) {
						enrollee.setEffectiveStartDate(effectiveStartDate);
						enrollee.setTotIndvRespEffDate(effectiveStartDate);
						enrollee.setRatingAreaEffDate(effectiveStartDate);

						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = getMemberTxnIdentifier(
								enrollmentUpdateDto, enrollee);
						String txnReason = getMemberTxnReason(enrollmentUpdateDto, enrollee);
//							if(memberTxnIdentifier!=null){
//								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(), user);
//							}

						if (DateUtils.isSameDay(effectiveStartDate, enrollee.getEffectiveEndDate())
								&& !status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
							enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
									EnrollmentConstants.ENROLLMENT_STATUS,
									EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
							enrollee.setDisenrollTimestamp(new TSDate());
							enrollee.setEffectiveEndDate(enrollee.getEffectiveStartDate());
							if (!enrollee.isEventCreated()) {
								// enrollee.setEventCreated(true);
								// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
								// EnrollmentConstants.EVENT_REASON_29, user, true, false,
								// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
										enrollmentUpdateDto.isSend834(), user,
										EnrollmentConstants.EVENT_TYPE_CANCELLATION,
										EnrollmentConstants.EVENT_REASON_29, txnReason);
							}
						}

						if ((EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM
								.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode())
								|| EnrollmentConstants.ENROLLMENT_STATUS_TERM
										.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode()))
								&& null != enrollmentUpdateDto.getHealthCoveragePolicyNo()) {
							enrollee.setHealthCoveragePolicyNo(enrollmentUpdateDto.getHealthCoveragePolicyNo());
						}
						if (!enrollee.isEventCreated()) {
							// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
							// EnrollmentConstants.EVENT_REASON_29, user, true, false,
							// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
							createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
									enrollmentUpdateDto.isSend834(), user, EnrollmentConstants.EVENT_TYPE_CHANGE,
									EnrollmentConstants.EVENT_REASON_29, txnReason);
							// enrollee.setEventCreated(true);
						}
						enrollee.setUpdatedBy(user);
					} else {
						if (effectiveStartDate.before(enrollee.getBirthDate())
								&& EnrollmentConstants.PERSON_TYPE_SUBSCRIBER
										.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())) {
							throw new GIException(EnrollmentConstants.ERR_MSG_EFFDATE_BEFORE_SUB_DOB);
						}
					}
				}
				checkEnrollmentStatus(user, enrollmentDb);
				if (enrollmentDb.getEnrollmentConfirmationDate() == null
						&& (enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)
								|| enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode()
										.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM))
						&& enrollmentUpdateDto.isConfirmIfNotConfirmed()) {
					enrollmentDb.setEnrollmentConfirmationDate(new TSDate());
				}
			}
		}
	}

	private void newUpdateEnrollmentEndDate(Enrollment enrollmentDb, EnrollmentUpdateDTO enrollmentUpdateDto,
			AccountUser user, EnrollmentResponse response) throws GIException {

		Date currentEndDate = enrollmentDb.getBenefitEndDate();
		if (enrollmentUpdateDto.getBenefitEndDate() == null) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFF_END_NULL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFF_END_NULL);
		}

		if (!EnrollmentUtils.isSameYear(enrollmentUpdateDto.getBenefitEndDate(),
				enrollmentUpdateDto.getBenefitEffectiveDate())
				&& !EnrollmentUtils.isSameYear(enrollmentUpdateDto.getBenefitEndDate(),
						enrollmentDb.getBenefitEffectiveDate())) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_215);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
		}

		if (enrollmentUpdateDto.getBenefitEffectiveDate().after(enrollmentUpdateDto.getBenefitEndDate()) && !DateUtils
				.isSameDay(enrollmentUpdateDto.getBenefitEffectiveDate(), enrollmentUpdateDto.getBenefitEndDate())) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_212);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
		}

		/*
		 * if(enrollmentDb.getNetPremEffDate()!=null &&
		 * enrollmentDb.getNetPremEffDate().after(enrollmentUpdateDto.getBenefitEndDate(
		 * )) && !DateUtils.isSameDay(enrollmentDb.getNetPremEffDate(),
		 * enrollmentUpdateDto.getBenefitEndDate())){ //TODO Premium dates outside
		 * coverage response.setErrCode(EnrollmentConstants.ERROR_CODE_206);
		 * response.setErrMsg(EnrollmentConstants.ERR_MSG_FINANCIAL_DATE);
		 * response.setStatus(GhixConstants.RESPONSE_FAILURE); throw new
		 * GIException(EnrollmentConstants.ERR_MSG_FINANCIAL_DATE); }
		 */

		if (!DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEndDate(), currentEndDate)) {

			List<Enrollee> enrollees = enrollmentDb.getEnrolleesAndSubscriber();
			Date effectiveEndDate;
			if (DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEndDate(), enrollmentDb.getBenefitEffectiveDate())) {
				effectiveEndDate = enrollmentDb.getBenefitEffectiveDate();
			} else {
				effectiveEndDate = EnrollmentUtils.getEODDate(enrollmentUpdateDto.getBenefitEndDate());
			}

			if (enrollees != null && enrollees.size() > 0) {

				if (effectiveEndDate.after(currentEndDate)) {
					// taking the end date ahead
					for (Enrollee enrollee : enrollees) {
						String status = enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode();
						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = getMemberTxnIdentifier(
								enrollmentUpdateDto, enrollee);
						String txnReason = getMemberTxnReason(enrollmentUpdateDto, enrollee);
//							if(memberTxnIdentifier!=null){
//								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(), user);
//							}
						if ((DateUtils.isSameDay(currentEndDate, enrollee.getEffectiveEndDate())
								|| enrollee.getEffectiveEndDate().after(currentEndDate))
								&& (enrollee.getDeathDate() == null || enrollee.getDeathDate().after(effectiveEndDate)
										|| DateUtils.isSameDay(enrollee.getDeathDate(), effectiveEndDate))) {
							if (effectiveEndDate.after(enrollee.getEffectiveStartDate())
									&& !DateUtils.isSameDay(effectiveEndDate, enrollee.getEffectiveStartDate())) {
								if (!status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
									enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
											EnrollmentConstants.ENROLLMENT_STATUS,
											EnrollmentConstants.ENROLLMENT_STATUS_TERM));
									enrollee.setDisenrollTimestamp(new TSDate());
									if (!enrollee.isEventCreated()) {
										// enrollee.setEventCreated(true);
										// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
										// EnrollmentConstants.EVENT_REASON_29, user, true, false,
										// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
										createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
												enrollmentUpdateDto.isSend834(), user,
												EnrollmentConstants.EVENT_TYPE_CANCELLATION,
												EnrollmentConstants.EVENT_REASON_29, txnReason);
									}
								} else {
									if (!enrollee.isEventCreated()) {
										// enrollee.setEventCreated(true);
										// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
										// EnrollmentConstants.EVENT_REASON_29, user, true, false,
										// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
										createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
												enrollmentUpdateDto.isSend834(), user,
												EnrollmentConstants.EVENT_TYPE_CHANGE,
												EnrollmentConstants.EVENT_REASON_29, txnReason);
									}
								}
								if (null != enrollmentUpdateDto.getHealthCoveragePolicyNo()) {
									enrollee.setHealthCoveragePolicyNo(enrollmentUpdateDto.getHealthCoveragePolicyNo());
									// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_ADDITION,
									// EnrollmentConstants.EVENT_REASON_INITIAL_ENROLLMENT, user, true);
								}
							} else {
								if (!status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
									enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
											EnrollmentConstants.ENROLLMENT_STATUS,
											EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
									enrollee.setDisenrollTimestamp(new TSDate());
									if (!enrollee.isEventCreated()) {
										// enrollee.setEventCreated(true);
										// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
										// EnrollmentConstants.EVENT_REASON_29, user, true, false,
										// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
										createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
												enrollmentUpdateDto.isSend834(), user,
												EnrollmentConstants.EVENT_TYPE_CANCELLATION,
												EnrollmentConstants.EVENT_REASON_29, txnReason);
									}
								} else {
									if (!enrollee.isEventCreated()) {
										// enrollee.setEventCreated(true);
										// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
										// EnrollmentConstants.EVENT_REASON_29, user, true, false,
										// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
										createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
												enrollmentUpdateDto.isSend834(), user,
												EnrollmentConstants.EVENT_TYPE_CHANGE,
												EnrollmentConstants.EVENT_REASON_29, txnReason);
									}
								}
							}

							enrollee.setEffectiveEndDate(effectiveEndDate);
						} else if (EnrollmentConstants.PERSON_TYPE_SUBSCRIBER
								.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())
								&& !(enrollee.getDeathDate() == null || enrollee.getDeathDate().after(effectiveEndDate)
										|| DateUtils.isSameDay(enrollee.getDeathDate(), effectiveEndDate))) {
							response.setErrCode(EnrollmentConstants.ERROR_CODE_211);
							response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_DDATE + "subscriber "
									+ enrollee.getExchgIndivIdentifier());
							response.setStatus(GhixConstants.RESPONSE_FAILURE);
							throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_DDATE + "subscriber "
									+ enrollee.getExchgIndivIdentifier());
						}
					}
				} else {
					// taking the end date back
					// validate
					/*
					 * if(enrollmentDb.getNetPremEffDate()!=null &&
					 * enrollmentDb.getNetPremEffDate().after(effectiveEndDate) &&
					 * DateUtils.isSameDay(effectiveEndDate,enrollmentDb.getNetPremEffDate()) ){
					 * 
					 * //TODO throw Effective dates exist outside coverage
					 * response.setErrCode(EnrollmentConstants.ERROR_CODE_206);
					 * response.setErrMsg(EnrollmentConstants.ERR_MSG_FINANCIAL_DATE);
					 * response.setStatus(GhixConstants.RESPONSE_FAILURE); throw new
					 * GIException(EnrollmentConstants.ERR_MSG_FINANCIAL_DATE); }
					 */

					for (Enrollee enrollee : enrollees) {

						if (enrollee.getEffectiveStartDate().after(effectiveEndDate)
								&& !DateUtils.isSameDay(effectiveEndDate, enrollee.getEffectiveStartDate())) {

							// TO DO throw Orphan Error

							response.setErrCode(EnrollmentConstants.ERROR_CODE_207);
							response.setErrMsg(
									EnrollmentConstants.ERR_MSG_ORPHAN_MEMBER + enrollee.getExchgIndivIdentifier());
							response.setStatus(GhixConstants.RESPONSE_FAILURE);
							throw new GIException(
									EnrollmentConstants.ERR_MSG_ORPHAN_MEMBER + enrollee.getExchgIndivIdentifier());
						}
						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = getMemberTxnIdentifier(
								enrollmentUpdateDto, enrollee);
						String txnReason = getMemberTxnReason(enrollmentUpdateDto, enrollee);
//							if(memberTxnIdentifier!=null){
//								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(), user);
//							}

						String status = enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode();

						if (DateUtils.isSameDay(effectiveEndDate, enrollee.getEffectiveStartDate())) {
							enrollee.setEffectiveEndDate(effectiveEndDate);
							if (!status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
								enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
										EnrollmentConstants.ENROLLMENT_STATUS,
										EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
								enrollee.setDisenrollTimestamp(new TSDate());
								if (!enrollee.isEventCreated()) {
									// enrollee.setEventCreated(true);
									// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
									// EnrollmentConstants.EVENT_REASON_29, user, true, false,
									// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
									createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
											enrollmentUpdateDto.isSend834(), user,
											EnrollmentConstants.EVENT_TYPE_CANCELLATION,
											EnrollmentConstants.EVENT_REASON_29, txnReason);
								}
							} else {
								// create event for date change
								if (!enrollee.isEventCreated()) {
									// enrollee.setEventCreated(true);
									// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
									// EnrollmentConstants.EVENT_REASON_29, user, true, false,
									// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
									createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
											enrollmentUpdateDto.isSend834(), user,
											EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_29,
											txnReason);
								}
							}
						}

						if (enrollee.getEffectiveEndDate().after(effectiveEndDate)
								&& !DateUtils.isSameDay(effectiveEndDate, enrollee.getEffectiveEndDate())
								&& (enrollee.getDeathDate() == null || enrollee.getDeathDate().after(effectiveEndDate)
										|| DateUtils.isSameDay(enrollee.getDeathDate(), effectiveEndDate))) {
							enrollee.setEffectiveEndDate(effectiveEndDate);
							if (!status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
								if (null != enrollmentUpdateDto.getHealthCoveragePolicyNo()) {
									enrollee.setHealthCoveragePolicyNo(enrollmentUpdateDto.getHealthCoveragePolicyNo());
								}
								enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
										EnrollmentConstants.ENROLLMENT_STATUS,
										EnrollmentConstants.ENROLLMENT_STATUS_TERM));
								if (!enrollee.isEventCreated()) {
									// enrollee.setEventCreated(true);
									// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
									// EnrollmentConstants.EVENT_REASON_29, user, true, false,
									// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
									createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
											enrollmentUpdateDto.isSend834(), user,
											EnrollmentConstants.EVENT_TYPE_CANCELLATION,
											EnrollmentConstants.EVENT_REASON_29, txnReason);
								}
							} else {
								// create event for date change
								if (!enrollee.isEventCreated()) {
									// enrollee.setEventCreated(true);
									// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
									// EnrollmentConstants.EVENT_REASON_29, user, true, false,
									// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
									createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
											enrollmentUpdateDto.isSend834(), user,
											EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_29,
											txnReason);
								}
							}
						}
					}

				}

				enrollmentDb.setBenefitEndDate(effectiveEndDate);
				enrollmentDb.setUpdatedBy(user);
				checkEnrollmentStatus(user, enrollmentDb);
				if (enrollmentDb.getEnrollmentConfirmationDate() == null
						&& (enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)
								|| enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode()
										.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM))
						&& enrollmentUpdateDto.isConfirmIfNotConfirmed()) {
					enrollmentDb.setEnrollmentConfirmationDate(new TSDate());
				}
			}

		}

	}

	private boolean newUpdateEnrolleeStartDate(Enrollment enrollment, EnrollmentUpdateDTO enrollmentUpdateDto,
			EnrollmentResponse response, AccountUser user, boolean isEnrollmentDateChange) throws GIException {
		boolean enrolleeStartDateChange = false;
		boolean isEnrolleeCancelEvent = false;
		boolean isCancelledMemberStatusChanged = false;
		if (enrollmentUpdateDto != null && enrollment != null && enrollmentUpdateDto.getMembers() != null) {
			Enrollee subscriber = enrollment.getSubscriberForEnrollment();
			String subscriberStartDateStr = null;
			boolean subscriberUpdate = false;
			for (EnrolleeDataUpdateDTO enrolleeDto : enrollmentUpdateDto.getMembers()) {
				if (subscriber.getId() == enrolleeDto.getEnrolleeId() && !DateUtils
						.isSameDay(enrolleeDto.getEffectiveStartDate(), enrollment.getBenefitEffectiveDate())
						&& !isEnrollmentDateChange) {
					subscriberUpdate = true;
					subscriberStartDateStr = enrolleeDto.getEffectiveStartDateStr();
					if (subscriberStartDateStr == null) {
						response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
						response.setErrMsg(EnrollmentConstants.ERR_MSG_DATE_NULL);
						response.setStatus(GhixConstants.RESPONSE_FAILURE);
						throw new GIException(EnrollmentConstants.ERR_MSG_DATE_NULL);
					}
				}
			}
			if (subscriberUpdate && StringUtils.isNotBlank(subscriberStartDateStr)) {
				try {
					enrollmentUpdateDto.setBenefitEffectiveDateStr(subscriberStartDateStr);
				} catch (ParseException pe) {
					throw new GIException("Unable to Parse Received date format: " + pe.getMessage());
				}
				newUpdateEnrollmentEffectiveDate(enrollment, enrollmentUpdateDto, response, user);
			}

			for (EnrolleeDataUpdateDTO enrolleeDto : enrollmentUpdateDto.getMembers()) {
				if (enrolleeDto.getEffectiveStartDate() == null) {
					response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_DATE_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_DATE_NULL);
				}
				Enrollee enrollee = enrollment.getEnrolleeById(enrolleeDto.getEnrolleeId());
				if (enrollee != null) {
					String status = enrollee.getEnrolleeLkpValue().getLookupValueCode();
					boolean isSubscriber = (subscriber.getId() == enrolleeDto.getEnrolleeId());
					if (!DateUtils.isSameDay(enrollee.getEffectiveStartDate(), enrolleeDto.getEffectiveStartDate())
							&& (!isSubscriber || subscriberUpdate)) {
						if (enrollee.getBirthDate() != null
								&& enrollee.getBirthDate().after(enrolleeDto.getEffectiveStartDate())
								&& !DateUtils.isSameDay(enrollee.getBirthDate(), enrolleeDto.getEffectiveStartDate())
								&& isEnrollmentDateChange) {
							newValidateEnrolleeDate(enrollee, enrollment, response);
							continue;
						}

						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = enrolleeDto.getTxnType();
						String txnReason = enrolleeDto.getTxnReason();
//							if(memberTxnIdentifier!=null){
//								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(), user);
//							}

						if (DateUtils.isSameDay(enrolleeDto.getEffectiveStartDate(), enrollee.getEffectiveEndDate())
								&& !status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) && DateUtils
										.isSameDay(enrolleeDto.getEffectiveEndDate(), enrollee.getEffectiveEndDate())) {
							enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
									EnrollmentConstants.ENROLLMENT_STATUS,
									EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
							isEnrolleeCancelEvent = true;
							enrollee.setDisenrollTimestamp(new TSDate());
							enrollee.setEffectiveEndDate(enrolleeDto.getEffectiveStartDate());
							if (!isEnrollmentDateChange && !enrollee.isEventCreated()) {
								// enrollee.setEventCreated(true);
								// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
								// EnrollmentConstants.EVENT_REASON_29, user, true, false,
								// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
										enrollmentUpdateDto.isSend834(), user,
										EnrollmentConstants.EVENT_TYPE_CANCELLATION,
										EnrollmentConstants.EVENT_REASON_29, txnReason);
							}
						}
						// member level start Date Reinstatement
						else if (!DateUtils.isSameDay(enrolleeDto.getEffectiveStartDate(),
								enrollee.getEffectiveEndDate())
								&& status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) && DateUtils
										.isSameDay(enrolleeDto.getEffectiveEndDate(), enrollee.getEffectiveEndDate())) {
							// enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
							enrollee.setEffectiveEndDate(EnrollmentUtils.getEODDate(enrolleeDto.getEffectiveEndDate()));
							determineCancelledMembersNewStatus(enrollee, subscriber);
							isCancelledMemberStatusChanged = true;
							enrollee.setDisenrollTimestamp(new TSDate());
							if (!isEnrollmentDateChange && !enrollee.isEventCreated()) {
								// enrollee.setEventCreated(true);
								// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
								// EnrollmentConstants.EVENT_REASON_29, user, true, false,
								// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
										enrollmentUpdateDto.isSend834(), user,
										EnrollmentConstants.EVENT_TYPE_CANCELLATION,
										EnrollmentConstants.EVENT_REASON_29, txnReason);
							}
						}

						enrolleeStartDateChange = true;
						enrollee.setEffectiveStartDate(enrolleeDto.getEffectiveStartDate());
						enrollee.setTotIndvRespEffDate(enrolleeDto.getEffectiveStartDate());
						enrollee.setRatingAreaEffDate(enrolleeDto.getEffectiveStartDate());

					}
				} else {
					response.setErrCode(EnrollmentConstants.ERROR_CODE_205);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_NO_MEMBER_FOUND);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_NO_MEMBER_FOUND);
				}
			}
		}
		if (!isEnrollmentDateChange && enrolleeStartDateChange) {
			for (EnrolleeDataUpdateDTO enrolleeDto : enrollmentUpdateDto.getMembers()) {
				Enrollee enrollee = enrollment.getEnrolleeById(enrolleeDto.getEnrolleeId());
				EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = enrolleeDto.getTxnType();
				String txnReason = enrolleeDto.getTxnReason();
				if (!enrollee.isEventCreated()) {
					createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(), user,
							EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_29, txnReason);
				}
			}

		}
		
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		enrollmentList.add(enrollment);
		
		if (isEnrolleeCancelEvent || isCancelledMemberStatusChanged) {
			response = updateIndividualApplicationStatus(isEnrolleeCancelEvent, isCancelledMemberStatusChanged,
					enrollmentList, response);
		}
		
		return enrolleeStartDateChange;
	}

	private boolean newUpdateEnrolleeEndDate(Enrollment enrollment, EnrollmentUpdateDTO enrollmentUpdateDto,
			EnrollmentResponse response, AccountUser user, boolean isEnrollmentDateChange) throws GIException {
		boolean isEnrolleeEndDateChange = false;
		boolean isEnrolleeCancelEvent = false;
		boolean isCancelledMemberStatusChanged = true;
		if (enrollmentUpdateDto != null && enrollment != null && enrollmentUpdateDto.getMembers() != null) {
			Enrollee subscriber = enrollment.getSubscriberForEnrollment();
			String subscriberEndDateStr = null;
			boolean subscriberUpdate = false;
			for (EnrolleeDataUpdateDTO enrolleeDto : enrollmentUpdateDto.getMembers()) {
				if ((subscriber.getId() == enrolleeDto.getEnrolleeId())
						&& !DateUtils.isSameDay(enrolleeDto.getEffectiveEndDate(), enrollment.getBenefitEndDate())
						&& !isEnrollmentDateChange) {
					subscriberUpdate = true;
					subscriberEndDateStr = enrolleeDto.getEffectiveEndDateStr();
					if (subscriberEndDateStr == null) {
						response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
						response.setErrMsg(EnrollmentConstants.ERR_MSG_DATE_NULL);
						response.setStatus(GhixConstants.RESPONSE_FAILURE);
						throw new GIException(EnrollmentConstants.ERR_MSG_DATE_NULL);
					}
				}
			}
			if (subscriberUpdate && StringUtils.isNotBlank(subscriberEndDateStr)) {
				try {
					enrollmentUpdateDto.setBenefitEndDateStr(subscriberEndDateStr);
				} catch (ParseException pe) {
					throw new GIException("Unable to Parse Received date format: " + pe.getMessage());
				}
				newUpdateEnrollmentEndDate(enrollment, enrollmentUpdateDto, user, response);
			}

			for (EnrolleeDataUpdateDTO enrolleeDto : enrollmentUpdateDto.getMembers()) {
				if (enrolleeDto.getEffectiveEndDateStr() == null) {
					response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_DATE_NULL);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_DATE_NULL);
				}
				Enrollee enrollee = enrollment.getEnrolleeById(enrolleeDto.getEnrolleeId());
				if (enrollee != null) {

					String status = enrollee.getEnrolleeLkpValue().getLookupValueCode();
					boolean isSubscriber = (subscriber.getId() == enrolleeDto.getEnrolleeId());
					if (!DateUtils.isSameDay(enrolleeDto.getEffectiveEndDate(), enrollee.getEffectiveEndDate())
							&& (!isSubscriber || subscriberUpdate)) {
						if (enrollee.getDeathDate() != null
								&& enrollee.getDeathDate().before(enrolleeDto.getEffectiveEndDate())
								&& !DateUtils.isSameDay(enrollee.getDeathDate(), enrolleeDto.getEffectiveEndDate())
								&& isEnrollmentDateChange) {
							newValidateEnrolleeDate(enrollee, enrollment, response);
							continue;
						}
						isEnrolleeEndDateChange = true;
						// newValidateEnrolleeDate(enrollee, enrollment, response);
						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = enrolleeDto.getTxnType();
						String txnReason = enrolleeDto.getTxnReason();
//							if(memberTxnIdentifier!=null){
//								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(), user);
//							}
						if ((DateUtils.isSameDay(enrollee.getEffectiveStartDate(), enrolleeDto.getEffectiveEndDate())
								|| enrolleeDto.getEffectiveEndDate().before(enrollee.getEffectiveStartDate()))
								&& !status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) && !DateUtils
										.isSameDay(enrolleeDto.getEffectiveEndDate(), enrollee.getEffectiveEndDate())) {
							enrollee.setEffectiveEndDate(enrollee.getEffectiveStartDate());
							enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
									EnrollmentConstants.ENROLLMENT_STATUS,
									EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
							isEnrolleeCancelEvent = true;
							enrollee.setDisenrollTimestamp(new TSDate());
							if (!isEnrollmentDateChange && !enrollee.isEventCreated()) {
								// enrollee.setEventCreated(true);
								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
										enrollmentUpdateDto.isSend834(), user,
										EnrollmentConstants.EVENT_TYPE_CANCELLATION,
										EnrollmentConstants.EVENT_REASON_29, txnReason);
								// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
								// EnrollmentConstants.EVENT_REASON_29, user, true, false,
								// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
							}
						} else if (!(DateUtils.isSameDay(enrollee.getEffectiveStartDate(),
								enrolleeDto.getEffectiveEndDate())
								|| enrolleeDto.getEffectiveEndDate().before(enrollee.getEffectiveStartDate()))) {
							enrollee.setEffectiveEndDate(EnrollmentUtils.getEODDate(enrolleeDto.getEffectiveEndDate()));
							// enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
							determineCancelledMembersNewStatus(enrollee, subscriber);
							isCancelledMemberStatusChanged = true;
							enrollee.setDisenrollTimestamp(new TSDate());
							if (!isEnrollmentDateChange && !enrollee.isEventCreated()) {
								// enrollee.setEventCreated(true);
								createEditToolEventForEnrollee(enrollee, memberTxnIdentifier,
										enrollmentUpdateDto.isSend834(), user,
										EnrollmentConstants.EVENT_TYPE_CANCELLATION,
										EnrollmentConstants.EVENT_REASON_29, txnReason);
								// createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
								// EnrollmentConstants.EVENT_REASON_29, user, true, false,
								// EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
							}
						} 
					}
				} else {
					response.setErrCode(EnrollmentConstants.ERROR_CODE_205);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_NO_MEMBER_FOUND);
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_NO_MEMBER_FOUND);
				}
			}
			/*
			 * if(subscriber!=null && isEnrolleeEndDateChange && !isEnrollmentDateChange &&
			 * ! subscriber.isEventCreated()){
			 * 
			 * subscriber.setEventCreated(true); createEnrolleeEvent(subscriber,
			 * EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_AI,
			 * user, true);
			 * 
			 * }
			 */
		}
		if (!isEnrollmentDateChange && isEnrolleeEndDateChange) {
			for (EnrolleeDataUpdateDTO enrolleeDto : enrollmentUpdateDto.getMembers()) {
				Enrollee enrollee = enrollment.getEnrolleeById(enrolleeDto.getEnrolleeId());
				EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = enrolleeDto.getTxnType();
				String txnReason = enrolleeDto.getTxnReason();
				if (!enrollee.isEventCreated()) {
					createEditToolEventForEnrollee(enrollee, memberTxnIdentifier, enrollmentUpdateDto.isSend834(), user,
							EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_29, txnReason);
				}
			}
		}
		
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		enrollmentList.add(enrollment);
		
		if (isEnrolleeCancelEvent || isCancelledMemberStatusChanged) {
			response = updateIndividualApplicationStatus(isEnrolleeCancelEvent, isCancelledMemberStatusChanged,
					enrollmentList, response);
		}
		
		return isEnrolleeEndDateChange;
	}

	private void newValidateEnrolleeDate(Enrollee enrollee, Enrollment enrollment, EnrollmentResponse response)
			throws GIException {

		if (!DateUtils.isSameDay(enrollee.getEffectiveStartDate(), enrollee.getEffectiveEndDate())) {
			if (enrollee.getEffectiveStartDate().after(enrollee.getEffectiveEndDate())
					&& !DateUtils.isSameDay(enrollee.getEffectiveStartDate(), enrollee.getEffectiveEndDate())) {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_212);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_EFFDATE + enrollee.getExchgIndivIdentifier());
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_EFFDATE + enrollee.getExchgIndivIdentifier());
			}

			if (!EnrollmentUtils.isSameYear(enrollee.getEffectiveStartDate(), enrollee.getEffectiveStartDate())) {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_215);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR + enrollee.getExchgIndivIdentifier());
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR + enrollee.getExchgIndivIdentifier());
			}

			if ((enrollee.getEffectiveStartDate().before(enrollment.getBenefitEffectiveDate())
					&& !DateUtils.isSameDay(enrollee.getEffectiveStartDate(), enrollment.getBenefitEffectiveDate()))
					|| (enrollee.getEffectiveStartDate().after(enrollment.getBenefitEndDate()) && !DateUtils
							.isSameDay(enrollee.getEffectiveStartDate(), enrollment.getBenefitEndDate()))) {
				// date falls outside enrollment coverage
				response.setErrCode(EnrollmentConstants.ERROR_CODE_208);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_OUT_COV + enrollee.getExchgIndivIdentifier());
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_OUT_COV + enrollee.getExchgIndivIdentifier());

			}

			if ((enrollee.getEffectiveEndDate().before(enrollment.getBenefitEffectiveDate())
					&& !DateUtils.isSameDay(enrollee.getEffectiveEndDate(), enrollment.getBenefitEffectiveDate()))
					|| (enrollee.getEffectiveEndDate().after(enrollment.getBenefitEndDate())
							&& !DateUtils.isSameDay(enrollee.getEffectiveEndDate(), enrollment.getBenefitEndDate()))) {
				// Date falls outside enrollment coverage
				response.setErrCode(EnrollmentConstants.ERROR_CODE_208);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_OUT_COV + enrollee.getExchgIndivIdentifier());
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_OUT_COV + enrollee.getExchgIndivIdentifier());
			}

			if (enrollee.getBirthDate() != null && enrollee.getBirthDate().after(enrollee.getEffectiveStartDate())
					&& !DateUtils.isSameDay(enrollee.getBirthDate(), enrollee.getEffectiveStartDate())) {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_210);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_BDATE + enrollee.getExchgIndivIdentifier());
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_BDATE + enrollee.getExchgIndivIdentifier());
			}

			if (enrollee.getDeathDate() != null && enrollee.getDeathDate().before(enrollee.getEffectiveEndDate())
					&& !DateUtils.isSameDay(enrollee.getDeathDate(), enrollee.getEffectiveEndDate())) {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_211);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_DDATE + enrollee.getExchgIndivIdentifier());
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_DDATE + enrollee.getExchgIndivIdentifier());
			}
		}

	}

	private void newValidateEnrollmentStartDateChangeRequest(Enrollment enrollment,
			EnrollmentUpdateDTO enrollmentUpdateDto, EnrollmentResponse response, List<Enrollee> enrollees)
			throws GIException {

		if (enrollmentUpdateDto.getBenefitEffectiveDate() == null) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_213);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFFDATE_NULL);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFFDATE_NULL);
		}

		if (enrollmentUpdateDto.getBenefitEffectiveDate().after(enrollmentUpdateDto.getBenefitEndDate()) && !DateUtils
				.isSameDay(enrollmentUpdateDto.getBenefitEffectiveDate(), enrollmentUpdateDto.getBenefitEndDate())) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_212);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_EFF_DATE_LARGE);
		}

		if (!EnrollmentUtils.isSameYear(enrollmentUpdateDto.getBenefitEffectiveDate(),
				enrollmentUpdateDto.getBenefitEndDate())
				&& !EnrollmentUtils.isSameYear(enrollmentUpdateDto.getBenefitEffectiveDate(),
						enrollment.getBenefitEndDate())) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_215);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_NOT_SAME_YEAR);
		}

		if (enrollment.getEnrollmentStatusLkp() != null && enrollment.getEnrollmentStatusLkp().getLookupValueCode()
				.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
			response.setErrCode(EnrollmentConstants.ERROR_CODE_214);
			response.setErrMsg(EnrollmentConstants.ERR_MSG_ENRL_CANCEL_EDIT);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_ENRL_CANCEL_EDIT);
		}

		/*
		 * if(enrollment.getNetPremEffDate()!=null &&
		 * enrollment.getNetPremEffDate().before(enrollmentUpdateDto.
		 * getBenefitEffectiveDate()) &&
		 * !DateUtils.isSameDay(enrollment.getNetPremEffDate(),enrollmentUpdateDto.
		 * getBenefitEffectiveDate()) &&
		 * !DateUtils.isSameDay(enrollment.getNetPremEffDate(),
		 * enrollment.getBenefitEffectiveDate())){ //TODO Premium dates outside coverage
		 * response.setErrCode(EnrollmentConstants.ERROR_CODE_206);
		 * response.setErrMsg(EnrollmentConstants.ERR_MSG_FINANCIAL_DATE);
		 * response.setStatus(GhixConstants.RESPONSE_FAILURE); throw new
		 * GIException(EnrollmentConstants.ERR_MSG_FINANCIAL_DATE); }
		 */
		/*
		 * for(Enrollee enrollee: enrollees){
		 * if(!DateUtils.isSameDay(enrollment.getBenefitEffectiveDate(),
		 * enrollee.getEffectiveStartDate()) &&
		 * !DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEffectiveDate(),
		 * enrollee.getEffectiveStartDate()) &&
		 * enrollmentUpdateDto.getBenefitEffectiveDate().after(enrollee.
		 * getEffectiveStartDate())) { //TODO Leads to Orphan Scenario
		 * response.setErrCode(EnrollmentConstants.ERROR_CODE_207);
		 * response.setErrMsg(EnrollmentConstants.ERR_MSG_ORPHAN_MEMBER+enrollee.
		 * getExchgIndivIdentifier());
		 * response.setStatus(GhixConstants.RESPONSE_FAILURE); throw new
		 * GIException(EnrollmentConstants.ERR_MSG_ORPHAN_MEMBER+enrollee.
		 * getExchgIndivIdentifier()); } if(enrollee.getBirthDate()!=null &&
		 * enrollee.getBirthDate().after(enrollmentUpdateDto.getBenefitEffectiveDate())
		 * && !DateUtils.isSameDay(enrollee.getBirthDate(),
		 * enrollmentUpdateDto.getBenefitEffectiveDate())){ //TODO Enrollee birth date
		 * outside enrollment coverage
		 * response.setErrCode(EnrollmentConstants.ERROR_CODE_210);
		 * response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_BDATE+enrollee.
		 * getExchgIndivIdentifier());
		 * response.setStatus(GhixConstants.RESPONSE_FAILURE); throw new
		 * GIException(EnrollmentConstants.ERR_MSG_MEMBER_BDATE+enrollee.
		 * getExchgIndivIdentifier()); } }
		 */
		Enrollee subscriber = enrollment.getSubscriberForEnrollment();
		for (EnrolleeDataUpdateDTO memberDto : enrollmentUpdateDto.getMembers()) {
			if (!DateUtils.isSameDay(memberDto.getEffectiveStartDate(), memberDto.getEffectiveEndDate())) {
				if (!DateUtils.isSameDay(enrollmentUpdateDto.getBenefitEffectiveDate(),
						memberDto.getEffectiveStartDate())
						&& enrollmentUpdateDto.getBenefitEffectiveDate().after(memberDto.getEffectiveStartDate())) {
					// TODO Leads to Orphan Scenario
					response.setErrCode(EnrollmentConstants.ERROR_CODE_207);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_ORPHAN_MEMBER + memberDto.getExchgIndivIdentifier());
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(
							EnrollmentConstants.ERR_MSG_ORPHAN_MEMBER + memberDto.getExchgIndivIdentifier());
				}
				if (subscriber.getId() == memberDto.getEnrolleeId() && subscriber.getBirthDate() != null
						&& subscriber.getBirthDate().after(enrollmentUpdateDto.getBenefitEffectiveDate()) && !DateUtils
								.isSameDay(subscriber.getBirthDate(), enrollmentUpdateDto.getBenefitEffectiveDate())) {
					// TODO Enrollee birth date outside enrollment coverage
					response.setErrCode(EnrollmentConstants.ERROR_CODE_210);
					response.setErrMsg(EnrollmentConstants.ERR_MSG_MEMBER_BDATE + "subscriber "
							+ memberDto.getExchgIndivIdentifier());
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_BDATE + "subscriber "
							+ memberDto.getExchgIndivIdentifier());
				}
			}
		}
	}

	@Override
	public List<Enrollee> getHouseholdContactResponsiblePerson(String memberId, List<Enrollee> enrollees) {
		List<Enrollee> householdResponsibleEnrollees = new ArrayList<Enrollee>();
		if (enrollees != null && isNotNullAndEmpty(memberId)) {
			for (Enrollee enrollee : enrollees) {
				if (enrollee.getPersonTypeLkp() != null && enrollee.getExchgIndivIdentifier().equalsIgnoreCase(memberId)
						&& (enrollee.getPersonTypeLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT)
								|| enrollee.getPersonTypeLkp().getLookupValueCode()
										.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON))) {
					householdResponsibleEnrollees.add(enrollee);
				}
			}
		}
		return householdResponsibleEnrollees;
	}

	public List<Enrollee> getHouseholdContactPerson(List<Enrollee> enrollees) {
		List<Enrollee> householdResponsibleEnrollees = new ArrayList<Enrollee>();
		if (enrollees != null) {
			for (Enrollee enrollee : enrollees) {
				if (enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode()
						.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT)) {
					householdResponsibleEnrollees.add(enrollee);
				}
			}
		}
		return householdResponsibleEnrollees;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EnrollmentDTO getEnrollmentAtributeById(EnrollmentRequest enrollmentRequest) throws GIException {
		/*
		 * List<String> joinTables = new ArrayList<String>(); joinTables.add("plan");
		 * joinTables.add("issuer");
		 */

		QueryBuilder<Enrollment> applicationQuery = delegateFactory.getObject();
		applicationQuery.buildSelectQuery(Enrollment.class);

		applicationQuery.applySelectColumns(enrollmentRequest.getEnrollmentAttributeList().stream()
				.map(a -> a.toString()).collect(Collectors.toList()));

		applicationQuery.applyWhere("id", enrollmentRequest.getEnrollmentId(), DataType.NUMERIC, ComparisonType.EQ);

		List<Map<String, Object>> enrollmentRow = applicationQuery.getData(EnrollmentConstants.DEFAULT_START_RECORD,
				GhixConstants.PAGE_SIZE);
		EnrollmentDTO enrollmentDTO = null;
		Date tempDate = null;

		for (Map<String, Object> map : enrollmentRow) {
			enrollmentDTO = new EnrollmentDTO();
			for (Map.Entry<String, Object> coloumName_Data : map.entrySet()) {
				try {
					if (getFieldType(coloumName_Data.getKey()).equalsIgnoreCase("integerClass")) {

						Method setter = EnrollmentDTO.class.getMethod(getSetterName(coloumName_Data.getKey()),
								Integer.class);
						setter.invoke(enrollmentDTO, coloumName_Data.getValue());

					} else if (getFieldType(coloumName_Data.getKey()).equalsIgnoreCase("StringClass")) {

						Method setter = EnrollmentDTO.class.getMethod(getSetterName(coloumName_Data.getKey()),
								String.class);
						setter.invoke(enrollmentDTO, coloumName_Data.getValue());

					} else if (getFieldType(coloumName_Data.getKey()).equalsIgnoreCase("DateClass")) {
						tempDate = (Date) coloumName_Data.getValue();

						Method setter = EnrollmentDTO.class.getMethod(getSetterName(coloumName_Data.getKey()),
								String.class);
						setter.invoke(enrollmentDTO,
								DateUtil.dateToString(tempDate, GhixConstants.REQUIRED_DATE_FORMAT));

					} else if (getFieldType(coloumName_Data.getKey()).equalsIgnoreCase("floatClass")) {

						Method setter = EnrollmentDTO.class.getMethod(getSetterName(coloumName_Data.getKey()),
								Float.class);
						setter.invoke(enrollmentDTO, coloumName_Data.getValue());

					}

				} catch (Exception ex) {
					throw new GIException(ex);
				}
			}
		}

		return enrollmentDTO;
	}

	private String getSetterName(String key) {
		Map<String, String> methodMap = new HashMap<>();
		methodMap.put("id", "setId");
		methodMap.put("enrollmentStatusLkplookupValueLabel", "setEnrlStatusLabel");
		methodMap.put("sponsorTaxIdNumber", "setSponsorTaxIdNumber");
		methodMap.put("subscriberName", "setSubscriberName");
		methodMap.put("sponsorName", "setSponsorName");
		methodMap.put("benefitEffectiveDate", "setEnrollmentStartDate");
		methodMap.put("benefitEndDate", "setEnrollmentEndDate");
		methodMap.put("enrollmentStatusLkplookupValueCode", "setEnrlStatusCode");
		methodMap.put("sponsorEIN", "setSponsorEIN");
		methodMap.put("grossPremiumAmt", "setGrossPremiumAmt");
		methodMap.put("aptcAmt", "setAptcAmt");
		methodMap.put("slcspAmt", "setSlcspAmt");
		methodMap.put("netPremiumAmt", "setNetPremiumAmt");
		methodMap.put("insurerName", "setInsurerName");
		methodMap.put("carrierAppId", "setCarrierAppId");

		return methodMap.get(key);
	}

	private String getFieldType(String key) {
		List<String> integerPropertyList = Arrays.asList("id");
		List<String> stringPropertyList = Arrays.asList("sponsorName", "sponsorEIN", "sponsorTaxIdNumber",
				"subscriberName", "enrollmentStatusLkplookupValueLabel", "enrollmentStatusLkplookupValueCode",
				"insurerName", "carrierAppId");
		List<String> datePropertyList = Arrays.asList("benefitEffectiveDate", "benefitEndDate");
		List<String> floatPropertyList = Arrays.asList("grossPremiumAmt", "aptcAmt", "slcspAmt", "netPremiumAmt");

		if (integerPropertyList.contains(key)) {
			return "integerClass";

		} else if (stringPropertyList.contains(key)) {
			return "StringClass";

		} else if (datePropertyList.contains(key)) {
			return "DateClass";

		} else if (floatPropertyList.contains(key)) {
			return "floatClass";
		}

		return null;
	}

	@Override
	public Map<String, List<EnrollmentDataDTO>> getEnrollmentDataByIdAndHouseholdCaseIds(
			final EnrollmentRequest enrollmentRequest) throws GIException {
		Map<String, List<EnrollmentDataDTO>> listOfEnrollmentDataDtomap = new HashMap<String, List<EnrollmentDataDTO>>();
		Map<Integer, EnrollmentDataDTO> enrollmentDataDtoMap = new HashMap<Integer, EnrollmentDataDTO>();
		List<String> excludeStatusList = new ArrayList<String>();
		excludeStatusList.add(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED);
		List<Object[]> enrollmentData = null;

		String stateSubsidyEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);

		if (enrollmentRequest.getIdList() != null && !enrollmentRequest.getIdList().isEmpty()) {
			enrollmentData = enrollmentRepository.getEnrollmentDataByEnrollmentIdCA(enrollmentRequest.getIdList(),
					excludeStatusList);

		} else if (enrollmentRequest.getHouseHoldCaseIdList() != null
				&& !enrollmentRequest.getHouseHoldCaseIdList().isEmpty()) {
			enrollmentData = enrollmentRepository.getEnrollmentDataByHouseHoldCaseIdCA(
					enrollmentRequest.getHouseHoldCaseIdList(), excludeStatusList);
		}

		if (enrollmentData != null && !enrollmentData.isEmpty()) {
			for (Object[] enrollmentColData : enrollmentData) {
				EnrollmentDataDTO enrollmentDataDto = new EnrollmentDataDTO();
				enrollmentDataDto.setEnrollmentId((Integer) enrollmentColData[EnrollmentConstants.ZERO]);
				enrollmentDataDto.setPlanId((Integer) enrollmentColData[EnrollmentConstants.ONE]);
				enrollmentDataDto.setHouseHoldCaseId((String) enrollmentColData[EnrollmentConstants.TWO]);
				enrollmentDataDto
						.setEnrollmentBenefitEffectiveStartDate((Date) enrollmentColData[EnrollmentConstants.THREE]);
				enrollmentDataDto
						.setEnrollmentBenefitEffectiveEndDate((Date) enrollmentColData[EnrollmentConstants.FOUR]);
				enrollmentDataDto.setGrossPremiumAmt((Float) enrollmentColData[EnrollmentConstants.FIVE]);
				enrollmentDataDto.setNetPremiumAmt((Float) enrollmentColData[EnrollmentConstants.SIX]);
				enrollmentDataDto.setEnrollmentStatus((String) enrollmentColData[EnrollmentConstants.SEVEN]);
				enrollmentDataDto.setCreationTimestamp((Date) enrollmentColData[EnrollmentConstants.EIGHT]);
				enrollmentDataDto.setPlanType((String) enrollmentColData[EnrollmentConstants.NINE]);
				enrollmentDataDto.setPlanLevel((String) enrollmentColData[EnrollmentConstants.TEN]);
				enrollmentDataDto.setPlanName((String) enrollmentColData[EnrollmentConstants.ELEVEN]);
				enrollmentDataDto.setCarrierName((String) enrollmentColData[EnrollmentConstants.TWELVE]);
				if (null != enrollmentColData[EnrollmentConstants.THIRTEEN]
						&& NumberUtils.isNumber((String) enrollmentColData[EnrollmentConstants.THIRTEEN])) {
					enrollmentDataDto
							.setCarrierId(Integer.valueOf((String) enrollmentColData[EnrollmentConstants.THIRTEEN]));
				}
				enrollmentDataDto.setAptcAmount((Float) enrollmentColData[EnrollmentConstants.FOURTEEN]);
				if (enrollmentColData[EnrollmentConstants.FIFTEEN] != null) {
					enrollmentDataDto.setRenewalFlag((String) enrollmentColData[EnrollmentConstants.FIFTEEN]);
					enrollmentDataDto.setPriorEnrollmentId((Long) enrollmentColData[EnrollmentConstants.SIXTEEN]);
				}
				// HIX-77014
				String cmsPlanId = (String) enrollmentColData[EnrollmentConstants.SEVENTEEN];
				if (StringUtils.isNotBlank(cmsPlanId)) {
					enrollmentDataDto.setCMSPlanID(cmsPlanId);
					enrollmentDataDto.setCsrLevel(cmsPlanId.substring(cmsPlanId.length() - 2, cmsPlanId.length()));
				}
				enrollmentDataDto.setLastUpdateDate((Date) enrollmentColData[EnrollmentConstants.EIGHTEEN]);
				enrollmentDataDto.setSubmittedToCarrierDate((Date) enrollmentColData[EnrollmentConstants.NINETEEN]);
				enrollmentDataDto.setCsrAmount((Float) enrollmentColData[EnrollmentConstants.TWENTY]);
				enrollmentDataDto.setTransactionId((String) enrollmentColData[EnrollmentConstants.TWENTY_ONE]);

				if ((int) enrollmentColData[EnrollmentConstants.TWENTY_TWO] > 0) {
					enrollmentDataDto.setIssuerId(String.valueOf(enrollmentColData[EnrollmentConstants.TWENTY_TWO]));
				}
				enrollmentDataDto
						.setEnrollmentConfirmationDate((Date) enrollmentColData[EnrollmentConstants.TWENTY_THREE]);
				enrollmentDataDto.setEhbSlcspAmt((Float) enrollmentColData[EnrollmentConstants.TWENTY_FOUR]);
				enrollmentDataDto.setEsEhbPrmAmt((Float) enrollmentColData[EnrollmentConstants.TWENTY_FIVE]);
				enrollmentDataDto.setAptcEffectuveDate((Date) enrollmentColData[EnrollmentConstants.TWENTY_SIX]);
				enrollmentDataDto.setCsrMultiplier((Float) enrollmentColData[EnrollmentConstants.TWENTY_SEVEN]);

				AccountUser submittedByUser = null;

				if (enrollmentColData[EnrollmentConstants.TWENTY_EIGHT] != null) {
					submittedByUser = userService
							.findById((Integer) enrollmentColData[EnrollmentConstants.TWENTY_EIGHT]);
				}

				if (null != submittedByUser) {
					Role defauleSubmittedByUserRole = userService.getDefaultRole(submittedByUser);
					if (null != defauleSubmittedByUserRole && null != defauleSubmittedByUserRole.getName()) {
						enrollmentDataDto.setSubmittedByRoleName(defauleSubmittedByUserRole.getName());
					}

					enrollmentDataDto.setSubmittedByUserId(submittedByUser.getId());
				}

				if (EnrollmentConstants.INSURANCE_TYPE_HEALTH.equalsIgnoreCase(enrollmentDataDto.getPlanType())) {
					enrollmentDataDto.setEhbPercentage((Float) enrollmentColData[EnrollmentConstants.TWENTY_NINE]);
				} else {
					enrollmentDataDto.setEhbPercentage((Float) enrollmentColData[EnrollmentConstants.THIRTY]);
				}

				if("Y".equalsIgnoreCase(stateSubsidyEnabled)){
					enrollmentDataDto.setStateSubsidyAmount((BigDecimal) enrollmentColData[EnrollmentConstants.THIRTY_ONE]);
					enrollmentDataDto.setStateSubsidyEffectiveDate((Date) enrollmentColData[EnrollmentConstants.THIRTY_TWO]);
				}

				List<EnrolleeDataDTO> enrolleeDataDtoList = new ArrayList<EnrolleeDataDTO>();
				enrollmentDataDto.setEnrolleeDataDtoList(enrolleeDataDtoList);
				enrollmentDataDtoMap.put(enrollmentDataDto.getEnrollmentId(), enrollmentDataDto);
				if (listOfEnrollmentDataDtomap.isEmpty()) {
					List<EnrollmentDataDTO> enrollmentDataDtoList = new ArrayList<EnrollmentDataDTO>();
					enrollmentDataDtoList.add(enrollmentDataDto);
					listOfEnrollmentDataDtomap.put(enrollmentDataDto.getHouseHoldCaseId(), enrollmentDataDtoList);
				} else {
					if (listOfEnrollmentDataDtomap.containsKey(enrollmentDataDto.getHouseHoldCaseId())) {
						listOfEnrollmentDataDtomap.get(enrollmentDataDto.getHouseHoldCaseId()).add(enrollmentDataDto);
					} else {
						List<EnrollmentDataDTO> enrollmentDataDtoList = new ArrayList<EnrollmentDataDTO>();
						enrollmentDataDtoList.add(enrollmentDataDto);
						listOfEnrollmentDataDtomap.put(enrollmentDataDto.getHouseHoldCaseId(), enrollmentDataDtoList);
					}
				}
			}

			if (enrollmentDataDtoMap != null && !enrollmentDataDtoMap.isEmpty()) {
				List<Integer> enrollmentIdList = new ArrayList<Integer>();
				enrollmentIdList.addAll(enrollmentDataDtoMap.keySet());

				/** Fetch Enrollee Information **/
				List<String> personTypeList = new ArrayList<String>();
				personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE);
				personTypeList.add(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER);

				List<Object[]> enrolleeData = null;
				enrolleeData = enrolleeService.getEnrolleeDataByListOfEnrollmentIdAndPersonTypeCA(enrollmentIdList,
						personTypeList);
				for (Object[] enrolleeColData : enrolleeData) {
					EnrolleeDataDTO enrolleeDataDto = new EnrolleeDataDTO();
					enrolleeDataDto.setFirstName((String) enrolleeColData[EnrollmentConstants.ZERO]);
					enrolleeDataDto.setLastName((String) enrolleeColData[EnrollmentConstants.ONE]);
					enrolleeDataDto.setEnrolleeEffectiveStartDate((Date) enrolleeColData[EnrollmentConstants.TWO]);
					enrolleeDataDto.setEnrolleeEffectiveEndDate((Date) enrolleeColData[EnrollmentConstants.THREE]);
					enrolleeDataDto.setEnrollmentId((Integer) enrolleeColData[EnrollmentConstants.FOUR]);
					enrolleeDataDto.setCoveragePolicyNumber((String) enrolleeColData[EnrollmentConstants.FIVE]);
					String enrolleePersonType = (String) enrolleeColData[EnrollmentConstants.SIX];

					if (enrolleePersonType != null
							&& enrolleePersonType.equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)) {
						enrolleeDataDto.setSubscriberFlag(EnrollmentConstants.Y);
					} else {
						enrolleeDataDto.setSubscriberFlag(EnrollmentConstants.N);
					}
					enrolleeDataDto.setEnrolleeAddress1((String) enrolleeColData[EnrollmentConstants.SEVEN]);
					enrolleeDataDto.setEnrolleeAddress2((String) enrolleeColData[EnrollmentConstants.EIGHT]);
					enrolleeDataDto.setEnrolleeCity((String) enrolleeColData[EnrollmentConstants.NINE]);
					enrolleeDataDto.setEnrolleeState((String) enrolleeColData[EnrollmentConstants.TEN]);
					enrolleeDataDto.setEnrolleeZipcode((String) enrolleeColData[EnrollmentConstants.ELEVEN]);
					enrolleeDataDto.setEnrolleeCounty((String) enrolleeColData[EnrollmentConstants.TWELVE]);
					enrolleeDataDto.setEnrolleeCountyCode((String) enrolleeColData[EnrollmentConstants.THIRTEEN]);
					// HIX-71635 Add additional fields for CAP
					enrolleeDataDto.setPhoneNumber((String) enrolleeColData[EnrollmentConstants.FOURTEEN]);
					enrolleeDataDto.setEmailAddress((String) enrolleeColData[EnrollmentConstants.FIFTEEN]);
					enrolleeDataDto.setMemberId((String) enrolleeColData[EnrollmentConstants.SIXTEEN]);
					// HIX-84835 Enhance getenrollmentdatabyhouseholdcaseids to include more fields
					enrolleeDataDto.setSuffix((String) enrolleeColData[EnrollmentConstants.SEVENTEEN]);
					enrolleeDataDto.setMiddleName((String) enrolleeColData[EnrollmentConstants.EIGHTEEN]);
					enrolleeDataDto.setTaxIdentificationNumber((String) enrolleeColData[EnrollmentConstants.NINETEEN]);
					enrolleeDataDto.setBirthDate((Date) enrolleeColData[EnrollmentConstants.TWENTY]);
					enrolleeDataDto.setGenderLookupCode((String) enrolleeColData[EnrollmentConstants.TWENTY_ONE]);
					enrolleeDataDto.setGenderLookupLabel((String) enrolleeColData[EnrollmentConstants.TWENTY_TWO]);
					enrolleeDataDto.setEnrolleeId((Integer) enrolleeColData[EnrollmentConstants.TWENTY_THREE]);
					enrolleeDataDto.setEnrolleeStatus((String) enrolleeColData[EnrollmentConstants.TWENTY_FOUR]);

					EnrolleeRelationship enrolleeRelationship = iEnrolleeRelationshipRepository
							.getRelationshipWithSubsciber(enrolleeDataDto.getEnrolleeId(),
									enrolleeDataDto.getEnrollmentId());
					if (enrolleeRelationship != null) {
						enrolleeDataDto.setRelationshipToSubscriber(
								enrolleeRelationship.getRelationshipLkp().getLookupValueLabel());
					} else {
						if (enrolleeDataDto.getSubscriberFlag().equals(EnrollmentConstants.Y)) {
							enrolleeDataDto.setRelationshipToSubscriber(EnrollmentConstants.RELATIONSHIP_SELF_LABEL);
						} else {
							enrolleeDataDto
									.setRelationshipToSubscriber(EnrollmentConstants.OTHER_RELATIONSHIP_LKP_VALUE);
						}
					}
					enrollmentDataDtoMap.get(enrolleeDataDto.getEnrollmentId()).getEnrolleeDataDtoList()
							.add(enrolleeDataDto);
				}

				/** Fetch Premium Information **/
				if (enrollmentRequest.isPremiumDataRequired()) {
					List<EnrollmentPremium> enrollmentPremiumList = null;
					for (Integer enrollmentId : enrollmentIdList) {
						enrollmentPremiumList = enrollmentPremiumRepository.findByEnrollmentId(enrollmentId);
						if (null != enrollmentPremiumList && !enrollmentPremiumList.isEmpty()) {
							List<EnrollmentPremiumDTO> enrollmentPremiumDtoList = new ArrayList<EnrollmentPremiumDTO>();
							for (EnrollmentPremium enrollmentPremium : enrollmentPremiumList) {
								EnrollmentPremiumDTO epDto = new EnrollmentPremiumDTO();
								epDto.setYear(enrollmentPremium.getYear());
								epDto.setMonth(enrollmentPremium.getMonth());
								epDto.setAptcAmount(enrollmentPremium.getAptcAmount());

								if("Y".equalsIgnoreCase(stateSubsidyEnabled)){
									epDto.setStateSubsidyAmount(enrollmentPremium.getStateSubsidyAmount());
								}

								if (enrollmentPremium.getAptcAmount() != null || enrollmentPremium.getStateSubsidyAmount() != null) {
									epDto.setFinancial(true);
								}
								epDto.setGrossPremiumAmount(enrollmentPremium.getGrossPremiumAmount());
								epDto.setNetPremiumAmount(enrollmentPremium.getNetPremiumAmount());
								epDto.setSlcspPremiumAmount(enrollmentPremium.getSlcspPremiumAmount());
								epDto.setSlcspPlanid(enrollmentPremium.getSlcspPlanid());
								enrollmentPremiumDtoList.add(epDto);
							}
							enrollmentDataDtoMap.get(enrollmentId)
									.setEnrollmentPremiumDtoList(enrollmentPremiumDtoList);
						}
					}
				}

			}
		} else {
			throw new GIException(EnrollmentConstants.ERROR_CODE_201,
					"No enrollment record found with the given request criteria", "LOW");
		}
		return listOfEnrollmentDataDtomap;
	}

	private Date getJOBLastRunDate(String jobName) throws GIException {
		Date lastRunDate = null;
		// Integer highestJobID = null;
		try {
			if (jobName != null) {
				/*
				 * highestJobID = batchJobExecutionService
				 * .getHighestInstanceIDByJobName(jobName,
				 * EnrollmentConstants.BATCH_JOB_STATUS); if (highestJobID != null) {
				 * lastRunDate = batchJobExecutionService.getLastJobRunDate(highestJobID); }
				 */
				lastRunDate = batchJobExecutionService.getLastJobRunDate(jobName, EnrollmentConstants.BATCH_JOB_STATUS);
			}

		} catch (Exception e) {
			throw new GIException("Error in getJOBLastRunDate() ::" + e.getMessage(), e);
		}
		return lastRunDate;
	}

	private EnrolleeDataUpdateDTO.TRANSACTION_TYPE getMemberTxnIdentifier(EnrollmentUpdateDTO enrollmentUpdateDto,
			Enrollee enrollee) {
		EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnType = null;
		if (enrollmentUpdateDto != null && enrollee != null && enrollmentUpdateDto.getMembers() != null
				&& enrollmentUpdateDto.getMembers().size() > 0) {
			for (EnrolleeDataUpdateDTO member : enrollmentUpdateDto.getMembers()) {
				if ((member.getEnrolleeId() != null && member.getEnrolleeId() == enrollee.getId())
						|| (member.getExchgIndivIdentifier() != null && member.getExchgIndivIdentifier()
								.equalsIgnoreCase(enrollee.getExchgIndivIdentifier()))) {
					memberTxnType = member.getTxnType();
					break;
				}
			}
		}
		return memberTxnType;
	}

	private void createEditToolEventForEnrollee(Enrollee enrollee,
			EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier, boolean send834, AccountUser user,
			String eventType, String eventReasonCode, String txnReason) {
		if (enrollee != null && !enrollee.isEventCreated()) {
			if (memberTxnIdentifier != null && !StringUtils.isBlank(txnReason)) {
				if (memberTxnIdentifier.toString()
						.equalsIgnoreCase(EnrolleeDataUpdateDTO.TRANSACTION_TYPE.CHANGE.toString())) {
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE, txnReason, user, send834,
							false, EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
				} else if (memberTxnIdentifier.toString()
						.equalsIgnoreCase(EnrolleeDataUpdateDTO.TRANSACTION_TYPE.ADD.toString())) {
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_ADDITION, txnReason, user, send834,
							false, EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
				} else if (memberTxnIdentifier.toString()
						.equalsIgnoreCase(EnrolleeDataUpdateDTO.TRANSACTION_TYPE.TERM.toString())) {
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION, txnReason, user, send834,
							false, EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
				}
				enrollee.setEventCreated(true);
			} else if (memberTxnIdentifier != null) {
				if (memberTxnIdentifier.toString()
						.equalsIgnoreCase(EnrolleeDataUpdateDTO.TRANSACTION_TYPE.CHANGE.toString())) {
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
							EnrollmentConstants.EVENT_REASON_AI, user, send834, false,
							EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
				} else if (memberTxnIdentifier.toString()
						.equalsIgnoreCase(EnrolleeDataUpdateDTO.TRANSACTION_TYPE.ADD.toString())) {
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_ADDITION,
							EnrollmentConstants.EVENT_REASON_BENEFIT_SELECTION, user, send834, false,
							EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
				} else if (memberTxnIdentifier.toString()
						.equalsIgnoreCase(EnrolleeDataUpdateDTO.TRANSACTION_TYPE.TERM.toString())) {
					createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CANCELLATION,
							EnrollmentConstants.EVENT_REASON_AI, user, send834, false,
							EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
				}
				enrollee.setEventCreated(true);
			} else if (memberTxnIdentifier == null && StringUtils.isNotEmpty(eventType)
					&& StringUtils.isNotEmpty(eventReasonCode)) {
				createEnrolleeEvent(enrollee, eventType, eventReasonCode, user, send834, false,
						EnrollmentEvent.TRANSACTION_IDENTIFIER.EDIT_TOOL);
				enrollee.setEventCreated(true);
			}
		}
	}

	/**
	 * Cap enrollment APTC if APTC is higher than gross premium
	 * 
	 * @param enrollmentDb
	 */
	private void capEnrollmentAptc(Enrollment enrollmentDb) {
		Map<Integer, EnrollmentPremium> enrollmentPremiumMap = enrollmentDb.getEnrollmentPremiumMap();
//		int startMonth = EnrollmentUtils.getMonthFromDate(enrollmentDb.getBenefitEffectiveDate());
		int endMonth = EnrollmentUtils.getMonthFromDate(enrollmentDb.getBenefitEndDate());
		int aptcEffMonth = EnrollmentUtils.getMonthFromDate(enrollmentDb.getAptcEffDate());
		Float aptcAmount = enrollmentPremiumMap.get(aptcEffMonth).getAptcAmount();
		Float netPremiumAmount = enrollmentPremiumMap.get(aptcEffMonth).getNetPremiumAmount();
		Float grossPremiumAmount = enrollmentPremiumMap.get(aptcEffMonth).getGrossPremiumAmount();
		for (int month = aptcEffMonth; month <= endMonth; month++) {
			if (enrollmentPremiumMap.containsKey(month)) {
				enrollmentPremiumMap.get(month).setActAptcAmount(aptcAmount);
			}
		}
		enrollmentDb.setAptcAmt(aptcAmount);
		enrollmentDb.setNetPremiumAmt(netPremiumAmount);
		enrollmentDb.setGrossPremiumAmt(grossPremiumAmount);
	}

	/**
	 * Edit tool method to set the cancelled enrollee's new status.
	 * 
	 * @param cancelledEnrollee
	 * @param subscriberEnrollee
	 */
	private void determineCancelledMembersNewStatus(Enrollee cancelledEnrollee, final Enrollee subscriberEnrollee) {
		if (DateUtils.isSameDay(cancelledEnrollee.getEffectiveEndDate(), subscriberEnrollee.getEffectiveEndDate())) {
			cancelledEnrollee.setEnrolleeLkpValue(subscriberEnrollee.getEnrolleeLkpValue());
		} else {
			cancelledEnrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(
					EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_TERM));
		}
	}

	@Override
	public Map<String, List<EnrollmentMemberDataDTO>> getEnrollmentMemberDataDTO(EnrollmentRequest enrollmentRequest) {
		Map<String, List<EnrollmentMemberDataDTO>> enrollmentMemberDataDtoMap = null;
		List<EnrollmentMemberDataDTO> enrollmentMemberDataDTOList = null;
		List<String> statusList = null;
		boolean isTermEnrollmentRequired = true;

		if (enrollmentRequest.getEnrollmentStatusList() != null && !enrollmentRequest.getEnrollmentStatusList().isEmpty()) {
		  statusList = enrollmentRequest.getEnrollmentStatusList().stream().map(s -> String.valueOf(s)).collect(Collectors.toList());
		  for (String enrollmentStatus : statusList) {
		    if (!enrollmentStatus.equalsIgnoreCase("TERM")) {
		      isTermEnrollmentRequired = false;
		    } else if (enrollmentStatus.equalsIgnoreCase("TERM")) {
		      isTermEnrollmentRequired = true;
		      break;
		    }
		  }
		}
		if (isTermEnrollmentRequired) {
		  enrollmentMemberDataDTOList = enrollmentRepository.getEnrollmentMemberDataDTO(
		      enrollmentRequest.getMemberIdList(),
		      (enrollmentRequest.getCoverageYearList() != null
		          && !enrollmentRequest.getCoverageYearList().isEmpty())
		              ? enrollmentRequest.getCoverageYearList()
		              : null,
		      DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		} else {
		  enrollmentMemberDataDTOList = enrollmentRepository.getEnrollmentMemberDataDTO(
		      enrollmentRequest.getMemberIdList(),
		      (enrollmentRequest.getCoverageYearList() != null
		          && !enrollmentRequest.getCoverageYearList().isEmpty())
		              ? enrollmentRequest.getCoverageYearList()
		              : null,
		      statusList);
		}
		
		if (enrollmentMemberDataDTOList != null && !enrollmentMemberDataDTOList.isEmpty()) {
			enrollmentMemberDataDtoMap = new LinkedHashMap<String, List<EnrollmentMemberDataDTO>>();
			for (EnrollmentMemberDataDTO enrollmentMemberDataDTO : enrollmentMemberDataDTOList) {
				if (enrollmentMemberDataDTO.getExchgIndivIdentifier() != null
						&& enrollmentMemberDataDtoMap.containsKey(enrollmentMemberDataDTO.getExchgIndivIdentifier())) {
					enrollmentMemberDataDtoMap.get(enrollmentMemberDataDTO.getExchgIndivIdentifier())
							.add(enrollmentMemberDataDTO);
				} else {
					List<EnrollmentMemberDataDTO> enrollmentMemberDataDTOListResponse = new ArrayList<EnrollmentMemberDataDTO>();
					enrollmentMemberDataDTOListResponse.add(enrollmentMemberDataDTO);
					enrollmentMemberDataDtoMap.put(enrollmentMemberDataDTO.getExchgIndivIdentifier(),
							enrollmentMemberDataDTOListResponse);
				}
				// HIX-106926 - Setting monthly Max APTC from Enrollment Premium
				if (enrollmentMemberDataDTO.getEnrollmentID() != null
						&& enrollmentMemberDataDTO.getEnrollmentID() > 0) {
					List<EnrollmentPremium> premList = enrollmentPremiumRepository
							.findByEnrollmentId(enrollmentMemberDataDTO.getEnrollmentID());
					if (!premList.isEmpty()) {
						List<EnrollmentPremiumDTO> premDtoList = new ArrayList<EnrollmentPremiumDTO>();

						premList.forEach(objPrem -> {
							EnrollmentPremiumDTO objEnrollmentPremiumDTO = new EnrollmentPremiumDTO();
							objEnrollmentPremiumDTO.setAptcAmount(objPrem.getAptcAmount());
							objEnrollmentPremiumDTO.setActualAptcAmount(objPrem.getActAptcAmount());
							objEnrollmentPremiumDTO.setMaxAptc(objPrem.getMaxAptc());
							objEnrollmentPremiumDTO.setStateSubsidyAmount(objPrem.getStateSubsidyAmount());
							objEnrollmentPremiumDTO.setActualStateSubsidyAmount(objPrem.getActStateSubsidyAmount());
							objEnrollmentPremiumDTO.setMaxStateSubsidy(objPrem.getMaxStateSubsidy());
							objEnrollmentPremiumDTO.setMonth(objPrem.getMonth());
							objEnrollmentPremiumDTO.setYear(objPrem.getYear());
							premDtoList.add(objEnrollmentPremiumDTO);

						});
						enrollmentMemberDataDTO.setEnrollmentMonthlyPremium(premDtoList);
					}
				}
			}
		}
		return enrollmentMemberDataDtoMap;
	}

	private String getMemberTxnReason(EnrollmentUpdateDTO enrollmentUpdateDto, Enrollee enrollee) {
		String txnReason = null;
		if (enrollmentUpdateDto != null && enrollee != null && enrollmentUpdateDto.getMembers() != null
				&& enrollmentUpdateDto.getMembers().size() > 0) {
			for (EnrolleeDataUpdateDTO member : enrollmentUpdateDto.getMembers()) {
				if ((member.getEnrolleeId() != null && member.getEnrolleeId() == enrollee.getId())
						|| (member.getExchgIndivIdentifier() != null && member.getExchgIndivIdentifier()
								.equalsIgnoreCase(enrollee.getExchgIndivIdentifier()))) {
					txnReason = member.getTxnReason();
					break;
				}
			}
		}
		return txnReason;
	}

	private void updateEnrollmentSsapId(Long existingSsapApplicationId, Long newSsapApplicationId) throws GIException {
		if (null != existingSsapApplicationId && null != newSsapApplicationId
				&& existingSsapApplicationId.compareTo(newSsapApplicationId) != 0) {
			try {
				enrollmentRepository.updateSsapApplicationId(existingSsapApplicationId, newSsapApplicationId);
			} catch (Exception e) {
				throw new GIException(EnrollmentConstants.ERR_MSG_SSAP_UPDATE, e);
			}
		}
	}

	@Override
	public List<Enrollment> synchronizeQuotingDates(Enrollment enrollment, AccountUser user,
			EnrollmentEvent.TRANSACTION_IDENTIFIER txnIdentifier) {
		List<Enrollment> linkedEnrollmentList = new ArrayList<>();
		if (!enrollment.getBenefitEffectiveDate().equals(enrollment.getBenefitEndDate())
				&& !EnrollmentConstants.ENROLLMENT_STATUS_CANCEL
						.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())) {
			Enrollment baseEnrollment = null;
			boolean isBaseEnrollment = false;
			if (null == enrollment.getLastEnrollmentId()) {
				baseEnrollment = enrollment;
				isBaseEnrollment = true;
			} else {
				baseEnrollment = fetchBaseEnrollment(enrollment.getLastEnrollmentId(), baseEnrollment);
			}

			Map<String, Date> quotingDateMap = baseEnrollment.getNonCancelEnrollees().stream()
					.filter(e -> null != e.getExchgIndivIdentifier() && null != e.getQuotingDate()).collect(Collectors
							.toMap(Enrollee::getExchgIndivIdentifier, Enrollee::getQuotingDate, (q1, q2) -> q1));

			if (isBaseEnrollment) {
				for (Enrollee enrollee : enrollment.getEnrolleesAndSubscriber()) {
					if (null != enrollee.getQuotingDate() && null != enrollee.getEffectiveStartDate()
							&& !enrollee.getQuotingDate().equals(enrollee.getEffectiveStartDate())) {
						enrollee.setQuotingDate(enrollee.getEffectiveStartDate());
						enrollee.setAge(EnrollmentUtils.getEnrolleeAge(enrollee));
						quotingDateMap.put(enrollee.getExchgIndivIdentifier(), enrollee.getEffectiveStartDate());
					}
				}
			} else {
				for (Enrollee enrollee : enrollment.getEnrolleesAndSubscriber()) {
					if (null != enrollee.getQuotingDate() && null != enrollee.getEffectiveStartDate()
							&& !enrollee.getQuotingDate().equals(enrollee.getEffectiveStartDate())) {
						Date baseEnrolleeQuotingDate = quotingDateMap.get(enrollee.getExchgIndivIdentifier());
						boolean isQuotingDateSame = null != baseEnrolleeQuotingDate && null != enrollee.getQuotingDate()
								&& baseEnrolleeQuotingDate.equals(enrollee.getQuotingDate());
						if (null == baseEnrolleeQuotingDate || (!isQuotingDateSame
								&& baseEnrolleeQuotingDate.after(enrollee.getEffectiveStartDate()))) {
							enrollee.setQuotingDate(enrollee.getEffectiveStartDate());
							enrollee.setAge(EnrollmentUtils.getEnrolleeAge(enrollee));
							quotingDateMap.put(enrollee.getExchgIndivIdentifier(), enrollee.getEffectiveStartDate());
						}
					}
				}
			}

			linkedEnrollmentList = fetchForwardLinkedEnrollments(enrollment.getId(), linkedEnrollmentList);
			for (Enrollment linkedEnrollment : linkedEnrollmentList) {
				for (Enrollee linkedEnrollee : linkedEnrollment.getEnrolleesAndSubscriber()) {
					if (quotingDateMap.containsKey(linkedEnrollee.getExchgIndivIdentifier())) {
						Date baseEnrolleeQuotingDate = quotingDateMap.get(linkedEnrollee.getExchgIndivIdentifier());
						boolean isQuotingDateSame = null != baseEnrolleeQuotingDate
								&& null != linkedEnrollee.getQuotingDate()
								&& baseEnrolleeQuotingDate.equals(linkedEnrollee.getQuotingDate());
						if (!isQuotingDateSame && baseEnrolleeQuotingDate.after(linkedEnrollee.getEffectiveStartDate())
								&& !linkedEnrollee.getQuotingDate().equals(linkedEnrollee.getEffectiveStartDate())) {
							linkedEnrollee.setQuotingDate(linkedEnrollee.getEffectiveStartDate());
							linkedEnrollee.setAge(EnrollmentUtils.getEnrolleeAge(linkedEnrollee));
							createEnrolleeEvent(linkedEnrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
									EnrollmentConstants.EVENT_REASON_AI, user, true, false, txnIdentifier);
						} else if (!isQuotingDateSame
								&& baseEnrolleeQuotingDate.before(linkedEnrollee.getEffectiveStartDate())) {
							linkedEnrollee.setQuotingDate(baseEnrolleeQuotingDate);
							linkedEnrollee.setAge(EnrollmentUtils.getEnrolleeAge(linkedEnrollee));
							createEnrolleeEvent(linkedEnrollee, EnrollmentConstants.EVENT_TYPE_CHANGE,
									EnrollmentConstants.EVENT_REASON_AI, user, true, false, txnIdentifier);
						}
					}
				}
				createSubscriberLevelEvent(linkedEnrollment, user);
			}
		}
		return linkedEnrollmentList;
	}

	private List<Enrollment> fetchForwardLinkedEnrollments(Integer id, List<Enrollment> affectedEnrollmentList) {
		List<Enrollment> enrollmentList = enrollmentRepository.findByLastEnrollmentId(id);
		for (Enrollment enrollment : enrollmentList) {
			if (!EnrollmentConstants.ENROLLMENT_STATUS_CANCEL
					.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())
					&& !EnrollmentConstants.ENROLLMENT_STATUS_ABORTED
							.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())) {
				affectedEnrollmentList.add(enrollment);
			}
			affectedEnrollmentList = fetchForwardLinkedEnrollments(enrollment.getId(), affectedEnrollmentList);
		}
		return affectedEnrollmentList;
	}

	private Enrollment fetchBaseEnrollment(Integer lastEnrollmentId, Enrollment baseEnrollment) {
		Enrollment enrollment = enrollmentRepository.findById(lastEnrollmentId);
		if (null == enrollment.getLastEnrollmentId()) {
			return enrollment;
		}
		baseEnrollment = fetchBaseEnrollment(enrollment.getLastEnrollmentId(), baseEnrollment);

		return baseEnrollment;
	}

	@Override
	public List<EnrollmentDataDTO> getEnrollmentSnapshot(EnrollmentRequest enrollmentRequest) {
		List<EnrollmentDataDTO> enrollmentDataDTOList = new ArrayList<EnrollmentDataDTO>();
		List<Object[]> enrolleeAudList = iEnrolleeAudRepository
				.findRevByIdAndEventId(enrollmentRequest.getEnrollmentId(), enrollmentRequest.getSubscriberEventId());
		Integer revisionId = (Integer)enrolleeAudList.get(0)[EnrollmentConstants.ZERO];
		Date revCreatedOn = (Date) enrolleeAudList.get(0)[EnrollmentConstants.ONE];
		List<Object[]> snapshotList = iEnrollmentAudRepository
				.getEnrollmentSnapshotByld(enrollmentRequest.getEnrollmentId(), revisionId);
		EnrollmentDataDTO enrollmentDataDto = new EnrollmentDataDTO();
		if (snapshotList == null || snapshotList.isEmpty()) {
			snapshotList = iEnrollmentAudRepository.getEnrollmentAudByEnrollmentIdAndDate(
					enrollmentRequest.getEnrollmentId(), revCreatedOn);
		}
		for (Object[] enrollmentData : snapshotList) {
			enrollmentDataDto.setEnrollmentId((Integer) enrollmentData[EnrollmentConstants.ZERO]);
			String cmsPlanId = (String) enrollmentData[EnrollmentConstants.ONE];
			if (StringUtils.isNotEmpty(cmsPlanId)) {
				enrollmentDataDto.setCMSPlanID(cmsPlanId);
			}
			enrollmentDataDto.setCarrierName((String) enrollmentData[EnrollmentConstants.TWO]);
			enrollmentDataDto.setPlanName((String) enrollmentData[EnrollmentConstants.THREE]);
			enrollmentDataDto.setEnrollmentBenefitEffectiveStartDate((Date) enrollmentData[EnrollmentConstants.FOUR]);
			enrollmentDataDto.setEnrollmentBenefitEffectiveEndDate((Date) enrollmentData[EnrollmentConstants.FIVE]);
			enrollmentDataDto.setPlanLevel((String) enrollmentData[EnrollmentConstants.SIX]);
			enrollmentDataDto.setGrossPremiumAmt((Float) enrollmentData[EnrollmentConstants.SEVEN]);
			enrollmentDataDto.setAptcAmount((Float) enrollmentData[EnrollmentConstants.EIGHT]);
			enrollmentDataDto.setNetPremiumAmt((Float) enrollmentData[EnrollmentConstants.NINE]);
			enrollmentDataDto.setTransactionId((String) enrollmentData[EnrollmentConstants.TEN]);
			enrollmentDataDto.setStateSubsidyAmount((BigDecimal) enrollmentData[EnrollmentConstants.ELEVEN]);
		}
		List<Object[]> enrolleeList = iEnrolleeAudRepository
				.getEnrolleesByEnrollmentIdandRev(enrollmentRequest.getEnrollmentId(), revisionId);
		List<EnrolleeDataDTO> enrolleeDataDtoList = new ArrayList<EnrolleeDataDTO>();
		if (enrolleeList != null && !enrolleeList.isEmpty()) {
			for (Object[] enrolleeData : enrolleeList) {
				List<Object[]> enrolleeEventList = enrollmentEventAudRepository
						.findEnrolleeEventsbyEnrolleeIdAndRev((Integer) enrolleeData[EnrollmentConstants.ZERO], revisionId);
				if (enrolleeEventList != null && !enrolleeEventList.isEmpty()) {
					EnrolleeDataDTO enrolleeDataDto = new EnrolleeDataDTO();
					enrolleeDataDto.setFirstName((String) enrolleeData[EnrollmentConstants.ONE]);
					enrolleeDataDto.setLastName((String) enrolleeData[EnrollmentConstants.TWO]);
					enrolleeDataDto.setMemberId((String) enrolleeData[EnrollmentConstants.THREE]);
					enrolleeDataDto.setEffectiveEndDate((Date) enrolleeData[EnrollmentConstants.FOUR] != null ? DateUtil.dateToString(
							(Date) enrolleeData[EnrollmentConstants.FOUR], EnrollmentConstants.DATETIME_FORMAT_MM_DD_YYYY) : null);
					enrolleeDataDto.setEffectiveStartDate((Date) enrolleeData[EnrollmentConstants.FIVE] != null
							? DateUtil.dateToString((Date) enrolleeData[EnrollmentConstants.FIVE],
									EnrollmentConstants.DATETIME_FORMAT_MM_DD_YYYY)
							: null);
					if (EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase((String) enrolleeData[EnrollmentConstants.SIX])) {
						enrolleeDataDto.setSubscriberFlag(EnrollmentConstants.YES);
					}
					enrolleeDataDto.setEventType((String)enrolleeEventList.get(0)[EnrollmentConstants.ZERO]);
					enrolleeDataDto.setEventReason((String)enrolleeEventList.get(0)[EnrollmentConstants.ONE]);
					enrolleeDataDtoList.add(enrolleeDataDto);
					enrollmentDataDto.setEnrolleeDataDtoList(enrolleeDataDtoList);
				}
			}
		}
		List<Object[]> eventList = enrollmentEventAudRepository.findSubscriberEventsbyEnrollmentIdAndRev(
				enrollmentRequest.getEnrollmentId(), revisionId);
		if (eventList != null && !eventList.isEmpty()) {
			enrollmentDataDto.setEventType((String) eventList.get(0)[EnrollmentConstants.ZERO]);
			enrollmentDataDto.setEventReason((String) eventList.get(0)[EnrollmentConstants.ONE]);
			enrollmentDataDto.setEventCreationDate((Date) eventList.get(0)[EnrollmentConstants.TWO]);
			AccountUser createdByUser = userService.findById((Integer) eventList.get(0)[EnrollmentConstants.THREE]);
			if (null != createdByUser) {
				enrollmentDataDto.setEventCreatedBy(createdByUser.getFullName());
				Role createdByRole = userService.getDefaultRole(createdByUser);
				if (null != createdByRole && null != createdByRole.getName()) {
					enrollmentDataDto.setUserRole(createdByRole.getName());
				}
			}
		}
		enrollmentDataDTOList.add(enrollmentDataDto);
		return enrollmentDataDTOList;
	}

	@Override
	public List<EnrollmentEventHistoryDTO> getAllSubscriberEvents(EnrollmentRequest enrollmentRequest) {
		List<EnrollmentEventHistoryDTO> enrollmentEventHistoryDTOList = new ArrayList<EnrollmentEventHistoryDTO>();
		List<Object[]> subscriberEventList = enrollmentEventAudRepository
				.findSubscriberEventsbyEnrollmentId(enrollmentRequest.getEnrollmentId());
		List<Object[]> enrollmentEventList = new ArrayList<Object[]>();
		if (subscriberEventList != null && !subscriberEventList.isEmpty()) {
			enrollmentEventList.addAll(subscriberEventList);
		}
		if (subscriberEventList != null && subscriberEventList != null) {
			/*
			 * Sorting in ascending order by Created On date
			 */
			Collections.sort(enrollmentEventList, new Comparator<Object[]>() {
				@Override
				public int compare(Object[] objArray1, Object[] objArray2) {
					return (((Date) objArray1[EnrollmentConstants.ZERO])
							.compareTo(((Date) objArray2[EnrollmentConstants.ZERO])));
				}
			});
		}
		if (enrollmentEventList != null && !enrollmentEventList.isEmpty()) {
			for (Object[] eventData : enrollmentEventList) {
				EnrollmentEventHistoryDTO enrollmentEventHistoryDTO = new EnrollmentEventHistoryDTO();
				enrollmentEventHistoryDTO.setEnrollmentEvent((String) eventData[EnrollmentConstants.TWO]);
				enrollmentEventHistoryDTO.setEnrollmentEventReason((String) eventData[EnrollmentConstants.ONE]);
				enrollmentEventHistoryDTO.setEventCreationDate(
						DateUtil.dateToString((java.util.Date) eventData[EnrollmentConstants.ZERO],
								EnrollmentConstants.DATETIME_FORMAT_MM_DD_YYYY));
				enrollmentEventHistoryDTO
						.setEventCreatedBy(EnrollmentUtils.getFullName((String) eventData[EnrollmentConstants.THREE],
								null, (String) eventData[EnrollmentConstants.FOUR]));
				enrollmentEventHistoryDTO
						.setSubscriberEventId(Integer.parseInt(eventData[EnrollmentConstants.NINE].toString()));
				AccountUser createdByUser = userService
						.findById(Integer.parseInt(eventData[EnrollmentConstants.TEN].toString()));
				if (null != createdByUser) {
					Role createdByRole = userService.getDefaultRole(createdByUser);
					if (null != createdByRole && null != createdByRole.getName()) {
						enrollmentEventHistoryDTO.setUserRole(createdByRole.getName());
					}
				}
				enrollmentEventHistoryDTOList.add(enrollmentEventHistoryDTO);
			}
		}
		return enrollmentEventHistoryDTOList;
	}

	private void setSepInfo(EnrollmentDataDTO enrollmentDataDto, Map<String, Map<String, String>> sepEventDetailMap) {
		Map<String, String> response = sepEventDetailMap.get(enrollmentDataDto.getHouseHoldCaseId());
		if (null != response && "200".equalsIgnoreCase(response.get("errCode"))) {
			enrollmentDataDto.setHealthPlanSelectionComplete(
					"NO_HEALTH".equalsIgnoreCase(response.get("healthEnrollmentStatus")));
			enrollmentDataDto.setEnrollmentWindowOpen(Boolean.parseBoolean(response.get("validEventsExist")));
			enrollmentDataDto
					.setDentalPlanSelectionComplete(Boolean.parseBoolean(response.get("dentalEnrollmentStatus")));
		}
	}

	@Override
	public Map<String, List<EnrollmentDataDTO>> getEnrollmentDataByExternalCaseIds(EnrollmentRequest enrollmentRequest)
			throws GIException {
		Map<String, List<EnrollmentDataDTO>> externalCaseIdMap = new HashMap<>();
		List<Object[]> householdObjList = enrollmentRepository
				.getHouseholdCaseIdFromExtHouseholdCaseId(enrollmentRequest.getExternalCaseIdList());
		Map<Integer, String> householdMap = new HashMap<>();
		householdObjList.forEach(o -> householdMap.put(Integer.parseInt(o[0].toString()), (String) (o[1])));
		if (null != householdMap && !householdMap.isEmpty()) {
			enrollmentRequest.setHouseHoldCaseIdList(
					householdMap.keySet().stream().map(s -> String.valueOf(s)).collect(Collectors.toList()));
			Map<String, List<EnrollmentDataDTO>> enrollmentData = getEnrollmentDataByHouseHoldCaseIds(
					enrollmentRequest);
			for (String key : enrollmentData.keySet()) {
				externalCaseIdMap.put(householdMap.get(Integer.valueOf(key)), enrollmentData.get(key));
			}
		}
		return externalCaseIdMap;
	}

	@Override
	public Set<String> updateSsapApplicationId(EnrollmentMemberDTO enrollmentMemberDTO) throws GIException {
		Enrollment enrollment = enrollmentRepository.findById(enrollmentMemberDTO.getEnrollmentId());
		enrollment.setSsapApplicationid(enrollmentMemberDTO.getSsapApplicationId());
		enrollment = enrollmentRepository.saveAndFlush(enrollment);
		List<String> memberIds = enrolleeRepository.getDistinctMemberIdBySsapIdAndType(
				enrollmentMemberDTO.getSsapApplicationId(), enrollment.getInsuranceTypeLkp().getLookupValueCode());
		Set<String> memberSet = new HashSet<>(memberIds);
		return memberSet;
	}

	@Override
	public EnrollmentResponse fetchLatestEnrollmentCreationTimeBySsapId(Long ssapApplicationId) throws GIException {
		EnrollmentResponse response = new EnrollmentResponse();
		try {
			List<Object[]> enrollmentData = enrollmentRepository.fetchLatestEnrollmentCreationTimeBySsapId(
					ssapApplicationId, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT),
					new PageRequest(0, 1));
			if (null != enrollmentData && !enrollmentData.isEmpty()) {
				response.setEnrollmentId((Integer) enrollmentData.get(0)[0]);
				response.setCreationDate(
						DateUtil.dateToString((Date) enrollmentData.get(0)[1], GhixConstants.REQUIRED_DATE_FORMAT));
				response.setEnrollmentExists(true);
				response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				response.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} else {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_205);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
			}
		} catch (Exception ex) {
			throw new GIException(EnrollmentConstants.ERROR_CODE_204, EnrollmentUtils.shortenedStackTrace(ex, 4),
					EnrollmentConstants.HIGH);
		}
		return response;
	}

	@Override
	public EnrollmentResponse fetchLatestEnrollmentEffectiveDateBySsapId(Long ssapApplicationId) throws GIException {
		EnrollmentResponse response = new EnrollmentResponse();
		try {
			List<Object[]> enrollmentData = enrollmentRepository.fetchLatestEnrollmentEffectiveDateBySsapId(
					ssapApplicationId, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT),
					new PageRequest(0, 1));
			if (null != enrollmentData && !enrollmentData.isEmpty()) {
				response.setEnrollmentId((Integer) enrollmentData.get(0)[0]);
				response.setBenefitEffectiveDate(
						DateUtil.dateToString((Date) enrollmentData.get(0)[1], GhixConstants.REQUIRED_DATE_FORMAT));
				response.setEnrollmentExists(true);
				response.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				response.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} else {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_205);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_NO_RECORDS_FOUND);
			}
		} catch (Exception ex) {
			throw new GIException(EnrollmentConstants.ERROR_CODE_204, EnrollmentUtils.shortenedStackTrace(ex, 4),
					EnrollmentConstants.HIGH);
		}
		return response;
	}

	private void createSubscriberLevelEvent(Enrollment enrollmentDb, AccountUser user,
			EnrollmentUpdateDTO enrollmentUpdateDto) {
		if (enrollmentDb != null) {
			boolean enrollmentUpdated = false;
			Enrollee subscriber = enrollmentDb.getSubscriberForEnrollment();
			if (subscriber != null && !subscriber.isEventCreated()) {
				List<Enrollee> enrollees = enrollmentDb.getEnrolleesAndSubscriber();
				if (enrollees != null && enrollees.size() > 0) {
					for (Enrollee enrollee : enrollees) {
						if (enrollee.isEventCreated()) {
							enrollmentUpdated = true;
							break;
						}
					}
					if (enrollmentUpdated) {
						EnrolleeDataUpdateDTO.TRANSACTION_TYPE memberTxnIdentifier = getMemberTxnIdentifier(
								enrollmentUpdateDto, subscriber);
						String txnReason = getMemberTxnReason(enrollmentUpdateDto, subscriber);
						createEditToolEventForEnrollee(subscriber, memberTxnIdentifier, enrollmentUpdateDto.isSend834(),
								user, EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_29,
								txnReason);
					}
				}
			}
		}
	}

	@Override
	public List<Enrollment> findActiveEnrollmentBySsapApplicationIdAndPlanType(Long ssapApplicationId,
			String planType) {
		return enrollmentRepository.findActiveEnrollmentBySsapApplicationIdAndPlanType(ssapApplicationId,
				DateUtil.dateToString(new TSDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), planType);
	}

	@Override
	public EnrollmentResponse findHiosIdForEnrollment(String coverageYear, String externalIndivId) throws GIException {
		EnrollmentResponse response = new EnrollmentResponse();
		try {
			List<String> hiosIssuerIdList = enrollmentRepository.findHiosIdForEnrollment(externalIndivId,
					DateUtil.dateToString(new TSDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), coverageYear);
			response.setHiosIssuerIdList(hiosIssuerIdList);
			response.setStatus(EnrollmentConstants.SUCCESS);
		} catch (Exception ex) {
			throw new GIException(EnrollmentConstants.ERROR_CODE_208, EnrollmentUtils.shortenedStackTrace(ex, 4),
					EnrollmentConstants.HIGH);
		}
		return response;
	}

	private void setSubscriberData(Integer enrollmentId, EnrollmentDataDTO enrollmentDataDto) {
		List<Object[]> enrolleeData = enrolleeService.getSubscriberDataByEnrollmentId(enrollmentId);
		if (null != enrolleeData && !enrolleeData.isEmpty()) {
			EnrolleeDataDTO enrolleeDataDto = new EnrolleeDataDTO();
			Object[] enrolleeColData = enrolleeData.get(0);
			enrolleeDataDto.setFirstName((String) enrolleeColData[EnrollmentConstants.ZERO]);
			enrolleeDataDto.setLastName((String) enrolleeColData[EnrollmentConstants.ONE]);
			enrolleeDataDto.setEnrolleeEffectiveStartDate((Date) enrolleeColData[EnrollmentConstants.TWO]);
			enrolleeDataDto.setEnrolleeEffectiveEndDate((Date) enrolleeColData[EnrollmentConstants.THREE]);
			enrolleeDataDto.setEnrollmentId((Integer) enrolleeColData[EnrollmentConstants.FOUR]);
			enrolleeDataDto.setCoveragePolicyNumber((String) enrolleeColData[EnrollmentConstants.FIVE]);

			enrolleeDataDto.setRelationshipToSubscriber(
					enrolleeColData[EnrollmentConstants.SIX] != null ? (String) enrolleeColData[EnrollmentConstants.SIX]
							: EnrollmentConstants.OTHER_RELATIONSHIP_LKP_VALUE);

			enrolleeDataDto.setEnrolleeAddress1((String) enrolleeColData[EnrollmentConstants.SEVEN]);
			enrolleeDataDto.setEnrolleeAddress2((String) enrolleeColData[EnrollmentConstants.EIGHT]);
			enrolleeDataDto.setEnrolleeCity((String) enrolleeColData[EnrollmentConstants.NINE]);
			enrolleeDataDto.setEnrolleeState((String) enrolleeColData[EnrollmentConstants.TEN]);
			enrolleeDataDto.setEnrolleeZipcode((String) enrolleeColData[EnrollmentConstants.ELEVEN]);
			enrolleeDataDto.setEnrolleeCounty((String) enrolleeColData[EnrollmentConstants.TWELVE]);
			enrolleeDataDto.setEnrolleeCountyCode((String) enrolleeColData[EnrollmentConstants.THIRTEEN]);
			enrolleeDataDto.setPhoneNumber((String) enrolleeColData[EnrollmentConstants.FOURTEEN]);
			enrolleeDataDto.setEmailAddress((String) enrolleeColData[EnrollmentConstants.FIFTEEN]);
			enrolleeDataDto.setMemberId((String) enrolleeColData[EnrollmentConstants.SIXTEEN]);
			enrolleeDataDto.setSuffix((String) enrolleeColData[EnrollmentConstants.SEVENTEEN]);
			enrolleeDataDto.setMiddleName((String) enrolleeColData[EnrollmentConstants.EIGHTEEN]);
			enrolleeDataDto.setTaxIdentificationNumber((String) enrolleeColData[EnrollmentConstants.NINETEEN]);
			enrolleeDataDto.setBirthDate((Date) enrolleeColData[EnrollmentConstants.TWENTY]);
			enrolleeDataDto.setGenderLookupCode((String) enrolleeColData[EnrollmentConstants.TWENTY_ONE]);
			enrolleeDataDto.setGenderLookupLabel((String) enrolleeColData[EnrollmentConstants.TWENTY_TWO]);
			enrolleeDataDto.setEnrolleeId((Integer) enrolleeColData[EnrollmentConstants.TWENTY_THREE]);
			enrolleeDataDto.setTobaccoStatus((String) enrolleeColData[EnrollmentConstants.TWENTY_FOUR]);
			enrolleeDataDto
					.setRelationshipToHouseHoldContact((String) enrolleeColData[EnrollmentConstants.THIRTY_FOUR]);
			enrolleeDataDto.setAge((Integer) enrolleeColData[EnrollmentConstants.THIRTY_FIVE]);
			enrolleeDataDto.setRelationshipToSubscriberCode((String) enrolleeColData[EnrollmentConstants.THIRTY_SIX]);
			enrolleeDataDto.setTobaccoStatusCode((String) enrolleeColData[EnrollmentConstants.THIRTY_SEVEN]);
			enrolleeDataDto
					.setRelationshipToHouseHoldContactCode((String) enrolleeColData[EnrollmentConstants.THIRTY_EIGHT]);
			enrolleeDataDto.setPersonType((String) enrolleeColData[EnrollmentConstants.THIRTY_NINE]);
			enrolleeDataDto.setEnrolleeStatus((String) enrolleeColData[EnrollmentConstants.FORTY]);
			enrolleeDataDto.setQuotingDate(null != enrolleeColData[EnrollmentConstants.FORTY_ONE]
					? (Date) enrolleeColData[EnrollmentConstants.FORTY_ONE]
					: null);
			enrollmentDataDto.setRatingArea(
					EnrollmentUtils.getFormatedRatingArea((String) enrolleeColData[EnrollmentConstants.THIRTY_TWO]));
			enrollmentDataDto.setRatingAreaEffectiveDate((Date) enrolleeColData[EnrollmentConstants.THIRTY_THREE]);

			enrollmentDataDto.setHomeAddress1((String) enrolleeColData[EnrollmentConstants.SEVEN]);
			enrollmentDataDto.setHomeAddress2((String) enrolleeColData[EnrollmentConstants.EIGHT]);
			enrollmentDataDto.setHomeCity((String) enrolleeColData[EnrollmentConstants.NINE]);
			enrollmentDataDto.setHomeState((String) enrolleeColData[EnrollmentConstants.TEN]);
			enrollmentDataDto.setHomeZipcode((String) enrolleeColData[EnrollmentConstants.ELEVEN]);
			enrollmentDataDto.setHomeCounty((String) enrolleeColData[EnrollmentConstants.TWELVE]);
			enrollmentDataDto.setHomeCountyCode((String) enrolleeColData[EnrollmentConstants.THIRTEEN]);

			enrollmentDataDto.setMailingAddress1((String) enrolleeColData[EnrollmentConstants.TWENTY_FIVE]);
			enrollmentDataDto.setMailingAddress2((String) enrolleeColData[EnrollmentConstants.TWENTY_SIX]);
			enrollmentDataDto.setMailingCity((String) enrolleeColData[EnrollmentConstants.TWENTY_SEVEN]);
			enrollmentDataDto.setMailingState((String) enrolleeColData[EnrollmentConstants.TWENTY_EIGHT]);
			enrollmentDataDto.setMailingZipcode((String) enrolleeColData[EnrollmentConstants.TWENTY_NINE]);
			enrollmentDataDto.setMailingCounty((String) enrolleeColData[EnrollmentConstants.THIRTY]);
			enrollmentDataDto.setMailingCountyCode((String) enrolleeColData[EnrollmentConstants.THIRTY_ONE]);
			enrollmentDataDto.setSubscriberPresent(true);
			enrollmentDataDto.getEnrolleeDataDtoList().add(enrolleeDataDto);
		}
	}

	@Override
	public Map<String, List<EnrollmentWithMemberDataDTO>> getEnrollmentDataForMembers(
			EnrollmentRequest enrollmentRequest) {

		Map<String, List<EnrollmentWithMemberDataDTO>> enrollmentMemberDataDtoMap = new HashMap<String, List<EnrollmentWithMemberDataDTO>>();

		List<EnrollmentWithMemberDataDTO> enrollmentMemberDataDtoList = new ArrayList<EnrollmentWithMemberDataDTO>();

		Map<String, EnrollmentWithMemberDataDTO> enrollmentMap = null;

		// Check Enrollment status List and convert to string list
		List<String> statusList = new ArrayList<String>();
		for (EnrollmentStatus status : enrollmentRequest.getEnrollmentStatusList()) {
			statusList.add(status.toString());
		}
		List<EnrollmentMemberDataDTO> enrollmentMemberDataDTOList=null;
		
		if(null!=enrollmentRequest.getHouseHoldCaseIdList() && enrollmentRequest.getHouseHoldCaseIdList().size()>0)
		{
			// Get Enrollments by Household case Id a.k.a cmr household Id
		 enrollmentMemberDataDTOList = enrollmentRepository.getEnrollmentMemberDataByHouseholdCaseIdAndCovergaeYearAndStatus(enrollmentRequest.getHouseHoldCaseIdList(),
					enrollmentRequest.getCoverageYear(), statusList);
		}
		else
		{
			//Get Enrollments by MemberIds
		 enrollmentMemberDataDTOList = enrollmentRepository
				.getEnrollmentMemberDataByMemberListAndCovergaeYearAndStatus(enrollmentRequest.getMemberIdList(),
						enrollmentRequest.getCoverageYear(), statusList); 
		}
		
		 

		if (enrollmentMemberDataDTOList != null && !enrollmentMemberDataDTOList.isEmpty()) {

			enrollmentMap = new LinkedHashMap<String, EnrollmentWithMemberDataDTO>();

			for (EnrollmentMemberDataDTO enrollmentMemberDataDTO : enrollmentMemberDataDTOList) {

				// See we have an entry for the enrollment in the enrollment Map
				if (enrollmentMap.get(enrollmentMemberDataDTO.getEnrollmentID().toString()) != null) {
					// Get the Enrollment entry and add the Member to the existing member list
					EnrollmentWithMemberDataDTO existingEnrollmentEntry = enrollmentMap
							.get(enrollmentMemberDataDTO.getEnrollmentID().toString());
					EnrollmentMembersDTO enrollmentMembersDTO = new EnrollmentMembersDTO(
							enrollmentMemberDataDTO.getExchgIndivIdentifier(),
							enrollmentMemberDataDTO.getExchgIndivIdentifier(),
							enrollmentMemberDataDTO.getEnrollmentID().toString());
					existingEnrollmentEntry.getMembers().add(enrollmentMembersDTO);
				} else {
					// Create an Entry for enrollmentId
					EnrollmentWithMemberDataDTO enrollmentEntry = new EnrollmentWithMemberDataDTO();
					enrollmentEntry.setEnrollmentID(enrollmentMemberDataDTO.getEnrollmentID().toString());
					enrollmentEntry.setEnrollmentStatus(enrollmentMemberDataDTO.getEnrollmentStatus());
					enrollmentEntry.setInsuranceType(enrollmentMemberDataDTO.getInsuranceType());
					enrollmentEntry.setPlanID(enrollmentMemberDataDTO.getCMSPlanID());
					// Create a new member list and add the member
					List<EnrollmentMembersDTO> memberListDto = new ArrayList<EnrollmentMembersDTO>();
					EnrollmentMembersDTO enrollmentMembersDTO = new EnrollmentMembersDTO(
							enrollmentMemberDataDTO.getExchgIndivIdentifier(),
							"NA",
							enrollmentMemberDataDTO.getEnrollmentID().toString());
					memberListDto.add(enrollmentMembersDTO);
					enrollmentEntry.setMembers(memberListDto);
					enrollmentMap.put(enrollmentMemberDataDTO.getEnrollmentID().toString(), enrollmentEntry);

				}

			}
			enrollmentMemberDataDtoList = new ArrayList(enrollmentMap.values());
		}

		enrollmentMemberDataDtoMap.put("enrollments", enrollmentMemberDataDtoList);
		return enrollmentMemberDataDtoMap;
	}

	/**
	 * Method is used to get List of Enrollment-ID to generate QHP Reports by Coverage Year.
	 */
	@Override
	public List<Integer> getEnrollmentIdListByCoverageYearForQHPReports(Integer coverageYear) {

		List<Integer> enrollmentIdList = null;

		try {

			if (null == coverageYear || 0 >= coverageYear) {
				LOGGER.error("Invalid Coverage Year: {}", coverageYear);
				return enrollmentIdList;
			}
			// Populates Enrollment data by Coverage Year
			enrollmentIdList = enrollmentRepository.getEnrollmentIdListByCoverageYearForQHPReports(String.valueOf(coverageYear));

			if (CollectionUtils.isEmpty(enrollmentIdList)) {
				LOGGER.error("Enrollment data is not found to generate QHP Reports.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Failed to generate QHP Reports data: ", ex);
		}
		finally {

			if (CollectionUtils.isEmpty(enrollmentIdList)) {
				LOGGER.error("Enrollment data is not found to generate QHP Reports.");
			}
			else {
				LOGGER.info("Successfully get List of {} Enrollment-ID to generate QHP Reports.", enrollmentIdList.size());
			}
		}
		return enrollmentIdList;
	}
	
	@Override
	public Map<String, String> getRenewalStatusMapByExternalCaseIds(List<String> externalHouseholCaseIdList, String year) {
		Map<String, String> renewalStatusMap = new HashMap<>();
		String renewalStatus = null;
		List<Object[]> houseHoldCaseIdList = enrollmentRepository.getHouseholdCaseIdFromExtHouseholdCaseId(externalHouseholCaseIdList);
		if (null != houseHoldCaseIdList && !houseHoldCaseIdList.isEmpty()) {
			for (Object[] houseHoldCaseId : houseHoldCaseIdList) {
				renewalStatus = enrollmentExternalRestUtil.getRenewalStatus((Integer) houseHoldCaseId[0], year);
				if (renewalStatus != null) {
					renewalStatusMap.put((String) houseHoldCaseId[1], renewalStatus);
				}
			}
		}
		return renewalStatusMap;
	}
	
	private EnrollmentResponse updateIndividualApplicationStatus(boolean isEnrolleeCancelEvent, boolean isCancelledMemberStatusChanged, List<Enrollment> enrollmentList, EnrollmentResponse response)
			throws GIException {
		
		if (isEnrolleeCancelEvent) {
			try {
				updateIndividualStatusForCancel(enrollmentList, true);
			} catch (GIException gi) {
				LOGGER.error(gi.getErrorMsg());
				response.setErrMsg(EnrollmentConstants.ERR_MSG_FAIL_TO_UPDATE_APP_STATUS);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_FAIL_TO_UPDATE_APP_STATUS);
			}
		}

		if (isCancelledMemberStatusChanged) {
			response = updateIndividualStatusForReinstatementOrAdd(enrollmentList, response);
			if (response != null && response.getStatus() != GhixConstants.RESPONSE_SUCCESS && response.getErrCode() == EnrollmentConstants.ERROR_CODE_204) {
				response.setErrCode(EnrollmentConstants.ERROR_CODE_1004);
				response.setErrMsg(EnrollmentConstants.ERR_MSG_ACT_ON_NEW_APP);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(EnrollmentConstants.ERR_MSG_ACT_ON_NEW_APP);
			} else if (response != null && response.getStatus() != GhixConstants.RESPONSE_SUCCESS) {
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				throw new GIException(response.getErrMsg());
			}
		}
		return response;
	}
	
	
	@Override
	public SsapApplicationResponse updateIndividualStatusForCancel(List<Enrollment> enrollmentList, boolean isEnrolleeUpdateByEditTool) throws GIException {

		SsapApplicationResponse ssapApplicationResponse = new SsapApplicationResponse();
		boolean updateApplicaitonStatus = false;

		for (Enrollment enrollment : enrollmentList) {
			
			SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
			String householdCaseID = enrollment.getHouseHoldCaseId();
			Long ssapApplicationId = enrollment.getSsapApplicationid();
			String coverageYear = DateUtil.getYearFromDate(enrollment.getBenefitEffectiveDate()).toString();

			if (!isEnrolleeUpdateByEditTool) {
				List<Integer> enrollmentIdList = enrollmentRepository.getEnrollmentIdListByHouseholdCaseId(householdCaseID, coverageYear, enrollment.getInsuranceTypeLkp().getLookupValueCode());
				if (enrollmentIdList != null && !enrollmentIdList.isEmpty()) {
					updateApplicaitonStatus = true;
					ssapApplicationRequest.setEnrollmentCanceledOrTerminated(true);
					if (enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE)) {
						ssapApplicationRequest.setApplicationStatus(EnrollmentConstants.APPLICAITON_TYPE_PN);
					} else {
						ssapApplicationRequest.setApplicationDentalStatusUpdate(true);
						ssapApplicationRequest.setApplicationDentalStatus(null);
					}
				} else {
					updateApplicaitonStatus = true;
					ssapApplicationRequest.setEnrollmentCanceledOrTerminated(true);
					if (enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE)) {
						ssapApplicationRequest.setApplicationStatus(EnrollmentConstants.APPLICATION_TYPE_ER);
					} else {
						ssapApplicationRequest.setApplicationDentalStatusUpdate(true);
						ssapApplicationRequest.setApplicationDentalStatus(null);
					}
				}
			} else {
				if (enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE)) {
					ssapApplicationRequest.setApplicationStatus(EnrollmentConstants.APPLICAITON_TYPE_PN);
					updateApplicaitonStatus = true;
				} else {
					updateApplicaitonStatus = false;
				}
			}

			AccountUser loggedInUser = this.getLoggedInUser();
			ssapApplicationRequest.setUserId(new BigDecimal(loggedInUser.getId()));
			ssapApplicationRequest.setSsapApplicationId(ssapApplicationId);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Disenrollment application status update request for enrollment ID : {} is {}",
						enrollment.getId(), ssapApplicationRequest.toString());
			}
			if (updateApplicaitonStatus) {
				ssapApplicationResponse = enrollmentExternalRestUtil.updateSsapApplicationById(ssapApplicationRequest);
			}

			if (ssapApplicationResponse != null && !ssapApplicationResponse.getErrorCode()
					.equalsIgnoreCase(Integer.valueOf(EnrollmentConstants.ERROR_CODE_200).toString())) {
				throw new GIException("Error in Updating the Application Status in Enrollment disenrollment flow");
			}
		}
		return ssapApplicationResponse;

	}
	
	@Override
	public EnrollmentResponse updateIndividualStatusForReinstatementOrAdd(List<Enrollment> enrollmentList, EnrollmentResponse enrollmentResponse) {

		enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);

		for (Enrollment enrollment : enrollmentList) {
			Long ssapApplicationId = enrollment.getSsapApplicationid();
			List<Enrollment> enrollments = new ArrayList<Enrollment>();
			enrollments.add(enrollment);
			if (ssapApplicationId != null) {
				enrollmentResponse = enrollmentCreationService.updateIndividualStatus(enrollmentResponse, ssapApplicationId, enrollments, true);
				if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && enrollmentResponse.getErrCode() == EnrollmentConstants.ERROR_CODE_204) {
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ACT_ON_NEW_APP);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				} else if (enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_FAIL_TO_UPDATE_APP_STATUS);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			} else {
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_APP_ID_FOUND);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		return enrollmentResponse;
	}

}
