package com.getinsured.hix.enrollment.util;

import java.util.Date;

/**
 * @author Ansari_i
 * 
 */
public class AetnaBob {

	private String applicationID;
	private String lastPaidPremAmt;
	private Date memEffectiveDate;
	private Date memTerminationDate;
	private String memberName;
	private String termDescription;

	public String getApplicationID() {
		return applicationID;
	}

	public void setApplicationID(String applicationID) {
		this.applicationID = applicationID;
	}

	public String getLastPaidPremAmt() {
		return lastPaidPremAmt;
	}

	public void setLastPaidPremAmt(String lastPaidPremAmt) {
		this.lastPaidPremAmt = lastPaidPremAmt;
	}

	public Date getMemEffectiveDate() {
		return memEffectiveDate;
	}

	public void setMemEffectiveDate(Date memEffectiveDate) {
		this.memEffectiveDate = memEffectiveDate;
	}

	public Date getMemTerminationDate() {
		return memTerminationDate;
	}

	public void setMemTerminationDate(Date memTerminationDate) {
		this.memTerminationDate = memTerminationDate;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getTermDescription() {
		return termDescription;
	}

	public void setTermDescription(String termDescription) {
		this.termDescription = termDescription;
	}

}
