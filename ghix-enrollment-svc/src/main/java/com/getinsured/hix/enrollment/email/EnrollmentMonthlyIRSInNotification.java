package com.getinsured.hix.enrollment.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.notification.NotificationAgent;

@Component
public class EnrollmentMonthlyIRSInNotification extends NotificationAgent {
	private Map<String, String> singleData;
	private Map<String, Object> emailData;
	private Map<String, String> requestData;
	
	/**
	 * 
	 * @return
	 */
	public Map<String, Object> getEmailData() {
		return emailData;
	}

	/**
	 * 
	 * @param emailData
	 */
	public void setEmailData(Map<String, Object> emailData) {
		this.emailData = emailData;
	}

	/**
	 * 
	 * @param notificationData
	 */
	public void setNotificationData(Map<String, Object> notificationData){
		this.notificationData = notificationData;
	}
	
	/**
	 * 
	 * @return
	 */
	public Map<String, Object> getNotificationData(){
		return this.notificationData;
	}
	
	/**
	 * @return the requestData
	 */
	public Map<String, String> getRequestData() {
		return requestData;
	}

	/**
	 * @param requestData the requestData to set
	 */
	public void setRequestData(Map<String, String> requestData) {
		this.requestData = requestData;
	}

	/**
	 * 
	 */
	@Override
	public Map<String, String> getSingleData() {
		
		Map<String,String> bean = new HashMap<String, String>();
		if(requestData != null && !requestData.isEmpty()){
			bean.putAll(requestData);
		}
		bean.put("redirectUrl", "");
		setTokens(bean);
		
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", (String)emailData.get("recipient"));
		
		if (singleData != null){
			data.putAll(singleData);
		}
		return data;
	}
	
	/**
	 * 
	 * @param singleData
	 */
	public void updateSingleData(Map<String, String> singleData){
		this.singleData = singleData;
	}

}
