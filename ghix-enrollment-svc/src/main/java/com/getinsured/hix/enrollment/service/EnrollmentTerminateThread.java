package com.getinsured.hix.enrollment.service;
/**
 * 
 * @author meher_a
 *
 */
public class EnrollmentTerminateThread implements Runnable {
	
	private EnrollmentAsyncService asyncService;
	private String cmsPlanId; 
	private String coverageYear;
	private String terminationDate;
	
	public EnrollmentTerminateThread(String cmsPlanId,String coverageYear, String terminationDate, EnrollmentAsyncService asyncService) {
		super();
		this.cmsPlanId = cmsPlanId;
		this.coverageYear = coverageYear;
		this.terminationDate = terminationDate;
		this.asyncService = asyncService;
	}

	@Override
	public void run() {
		asyncService.enrollmentAsyncTerminatorTask(cmsPlanId, coverageYear, terminationDate);
	}
}
