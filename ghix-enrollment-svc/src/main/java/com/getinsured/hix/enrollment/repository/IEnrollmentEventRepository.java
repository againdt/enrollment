package com.getinsured.hix.enrollment.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.EnrollmentEvent;
public interface IEnrollmentEventRepository extends JpaRepository<EnrollmentEvent, Integer> {

	@Query(" FROM EnrollmentEvent as ev " +
		   " WHERE ev.id =  :eventID and ev.enrollment.planId = :planID")
	EnrollmentEvent findEnrollmentByeventIDAndplanID(@Param("eventID") Integer eventID, @Param("planID") Integer planID);
	
	@Query(" FROM EnrollmentEvent as ev " +
		   " WHERE ev.enrollment.id =  :enrollmentID " +
		   " AND ev.enrollee.id = :enrolleeID " +
		   " AND ev.createdOn >= :createdDate ORDER BY ev.createdOn ASC")
	List<EnrollmentEvent> findEventByenrollmentIDAndEnrolleeIDAndCreatedDate(@Param("enrollmentID") Integer enrollmentID, @Param("enrolleeID") Integer enrolleeID,@Param("createdDate") Date createdDate );

	@Query(" SELECT max(ev.createdOn) FROM EnrollmentEvent as ev , Enrollee as ee " +
		   " WHERE ev.enrollment.id = :enrollmentId " +
		   " AND ev.eventTypeLkp.lookupValueCode = '024' " +
		   " AND ee.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
		   " AND ev.enrollee = ee.id")
	Date findTerminatedDateForEnrollment(@Param ("enrollmentId") Integer enrollmentId);
	
	@Query(" SELECT ev from EnrollmentEvent as ev "+
		   " WHERE ev.enrollment.id = :enrollmentId "+
		   " AND ev.enrollee.personTypeLkp.lookupValueCode = 'SUBSCRIBER' ")
	List<EnrollmentEvent> findSubscriberEventsForEnrollment(@Param ("enrollmentId") Integer enrollmentId);
	
	@Query(" SELECT count(*) from EnrollmentEvent  "+
		   " WHERE enrollment.id = :enrollmentId "+
		   " AND extractionStatus = 'FAILED' ")
	long getFailedEventsCountForEnrollment(@Param("enrollmentId") Integer enrollmentId);
	
	@Modifying
	@Transactional
	@Query(" UPDATE EnrollmentEvent SET extractionStatus = 'FAILED'  "+
		   " WHERE enrollment.id in (:enrollmentIdList) "+
		   " AND ((createdOn >= :startDate AND createdOn <= :endDate) OR (extractionStatus = 'RESEND') )")
	void updateEventsToFailed(@Param("enrollmentIdList") List<Integer> enrollmentIdList, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Modifying
	@Transactional
    @Query(" UPDATE EnrollmentEvent SET extractionStatus = NULL "+
           " WHERE enrollment.id in (:enrollmentIdList) "+
           " AND (extractionStatus = 'RESEND' AND createdOn <= :endDate) )")
	void updateEventsToSuccess(@Param("enrollmentIdList") List<Integer> enrollmentIdList, @Param("endDate") Date endDate);

	@Query(" SELECT ev.eventReasonLkp.lookupValueCode " +
			   " FROM EnrollmentEvent ev, " +
			   " Enrollee ee " +
			   " WHERE ev.enrollment.id = :enrollmentId " +
			   " AND ee.enrollment.id   = :enrollmentId " +
			   " AND ee.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			   " AND ev.enrollee.id     = ee.id " +
			   " AND ev.eventTypeLkp.lookupValueCode = '024' " +
			   " ORDER BY ev.createdOn DESC ")
	List<String> getEnrollmentCancelTermReason(@Param ("enrollmentId") Integer enrollmentId);
	
	/**
	 * Jira Id: HIX-70721
	 * @param enrollmentId
	 * @return
	 */
	@Query(" SELECT ev.eventReasonLkp.lookupValueLabel,  ev.createdOn" +
			   " FROM EnrollmentEvent ev, " +
			   " Enrollee ee " +
			   " WHERE ev.enrollment.id = :enrollmentId " +
			   " AND ee.enrollment.id   = :enrollmentId " +
			   " AND ee.personTypeLkp.lookupValueCode = 'SUBSCRIBER' " +
			   " AND ev.enrollee.id  = ee.id " +
			   " AND ev.eventTypeLkp.lookupValueCode = '024' ORDER BY ev.createdOn DESC")
	List<Object[]> getEnrollmentTermReasonAndDate(@Param ("enrollmentId") Integer enrollmentId);
	
	@Query(" SELECT ev from EnrollmentEvent as ev "+
			   " WHERE ev.enrollment.id = :enrollmentId "+
			   " AND (ev.sendToCarrierFlag is NULL OR ev.sendToCarrierFlag = 'true') AND (ev.createdBy IS NULL OR ev.createdBy.id <> :carrierId) ")
		List<EnrollmentEvent> getNonCarrierEvents(@Param ("enrollmentId") Integer enrollmentId, @Param("carrierId") Integer carrierId);
	
	@Query(" SELECT ev from EnrollmentEvent as ev "+
			   " WHERE ev.enrollment.id = :enrollmentId "+
			   " AND (ev.sendToCarrierFlag is NULL OR ev.sendToCarrierFlag = 'true') AND (ev.createdBy IS NULL OR ev.createdBy.id <> :carrierId) AND (createdOn BETWEEN :eventCutoffDate AND :lastEventDate) ")
		List<EnrollmentEvent> getNonCarrierEventsByDate(@Param ("enrollmentId") Integer enrollmentId,@Param("carrierId") Integer carrierId, @Param("lastEventDate") Date lastEventDate, @Param("eventCutoffDate") Date eventCutoffDate);
	
	@Query(   " FROM EnrollmentEvent ev " + 
			   " WHERE ev.enrollment.id = :enrollmentId " +
			   " AND ev.txnIdentifier= :txnIdStr" +
			   " AND ev.createdOn > :lastRunDate " )
	List<EnrollmentEvent> findAPTCUpdateEvents(@Param ("lastRunDate") Date lastRunDate, @Param ("enrollmentId") int enrollmentId,@Param ("txnIdStr") String txnIdStr );

	@Query(    " FROM EnrollmentEvent ev " +
			   " WHERE ev.enrollee.id  = :enrolleeId " +
			   " AND ev.eventTypeLkp.lookupValueCode = '024' ORDER BY ev.createdOn DESC")
	List<EnrollmentEvent> getCancelTermEventByEnrolleeId(@Param ("enrolleeId") Integer enrolleeId);
	
	@Query(" SELECT MAX(ev.id) from EnrollmentEvent as ev "+
			   " WHERE ev.enrollment.id = :enrollmentId "+
			   " AND ev.enrollee.personTypeLkp.lookupValueCode = 'SUBSCRIBER' ")
	Integer findMaxSubscriberEventIdForEnrollment(@Param ("enrollmentId") Integer enrollmentId);
}
