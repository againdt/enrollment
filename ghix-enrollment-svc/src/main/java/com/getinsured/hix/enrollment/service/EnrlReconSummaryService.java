package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.dto.enrollment.EnrlReconciliationResponse;
import com.getinsured.hix.dto.enrollment.EnrollmentReconRequest;

/**
 * 
 * @author sharma_k
 *
 */
public interface EnrlReconSummaryService {
	
	/**
	 * 
	 * @return
	 */
	public EnrlReconciliationResponse getPopulatedMonthAndCarrierList(EnrlReconciliationResponse enrlReconciliationResponse);
	
	/**
	 * 
	 * @param enrollmentReconRequest
	 * @param enrlReconciliationResponse
	 * @return
	 */
	public EnrlReconciliationResponse getMonthlyActivityData(EnrollmentReconRequest enrollmentReconRequest, EnrlReconciliationResponse enrlReconciliationResponse);
	
	/**
	 * 
	 * @param enrollmentReconRequest
	 * @param enrlReconciliationResponse
	 * @return
	 */
	public EnrlReconciliationResponse searchEnrlDiscrepancyData(EnrollmentReconRequest enrollmentReconRequest, EnrlReconciliationResponse enrlReconciliationResponse);

	/**
	 * 
	 * @param enrlReconciliationResponse
	 * @return
	 */
	public EnrlReconciliationResponse populateSearchDiscrepancyCriteria(EnrlReconciliationResponse enrlReconciliationResponse);
}
