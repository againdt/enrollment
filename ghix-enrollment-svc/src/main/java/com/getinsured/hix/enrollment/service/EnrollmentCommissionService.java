/**
 *
 */
package com.getinsured.hix.enrollment.service;

import java.util.Date;
import java.util.List;

import com.getinsured.hix.dto.enrollment.EnrollmentCommissionDTO;
import com.getinsured.hix.enrollment.dto.EnrollmentCommissionRequest;
import com.getinsured.hix.model.enrollment.CommissionConfigMapping;
import com.getinsured.hix.model.enrollment.EnrollmentCommission;
import com.getinsured.hix.platform.util.exception.GIException;


/**
 * @author Priya C
 * @since 13/02/2013
 */
public interface EnrollmentCommissionService {

	CommissionConfigMapping getcommissionConfigMappingByName(String mappingName);
	List<EnrollmentCommission> findEnrollmentCommissionByEnrollmentId(Integer enrollmentId);
	EnrollmentCommission saveEnrollmentCommission(EnrollmentCommissionDTO enrollmentCommissionDTO);
	EnrollmentCommission updateEnrollmentCommission(EnrollmentCommissionDTO enrollmentCommissionDTO);
	boolean existsByEnrollmentIdAndCommissionDateBetween(Integer enrollmentId, Date begDate, Date endDate);
	Float sumCommissionAmtByEnrollmentIdAndCommissionDateLT(Integer enrollmentId, Date commissionDate, Date premiumMonth);
	EnrollmentCommission saveEnrollmentCommission(EnrollmentCommission enrollmentCommission);
	List<EnrollmentCommission> findByEnrollmentIdAndCommissionDateGE(Integer enrollmentId, Date commissionDate, Date premiumMonth);
	Float sumCommissionAmtByEnrollmentIdAndCommissionDateLT(Integer enrollmentId, Date commissionDate, Date premiumMonth, Integer id);
	List<EnrollmentCommission> findByEnrollmentIdAndCommissionDateGE(Integer enrollmentId, Date commissionDate, Date premiumMonth, Integer id);
	
	/**
	 * 
	 * @param enrollmentCommissionRequest
	 * @return
	 */
	EnrollmentCommission saveEnrollmentCommission(EnrollmentCommissionRequest enrollmentCommissionRequest)throws GIException;
	
}
