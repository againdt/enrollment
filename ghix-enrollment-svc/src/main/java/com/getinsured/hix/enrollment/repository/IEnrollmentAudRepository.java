/**
 *
 */
package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.enrollment.EnrollmentAud;
import com.getinsured.hix.platform.repository.TenantAwareRepository;
public interface IEnrollmentAudRepository extends
TenantAwareRepository<EnrollmentAud, AudId> {
	
	@Query(" FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentId and en.rev = :rev ORDER BY en.updatedOn DESC")
	List<EnrollmentAud> getEnrollmentAudByIdAndRev(@Param("enrollmentId") Integer enrollmentId, @Param("rev") Integer rev);
	
	@Query(" FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentAudID "
			+ " AND en.updatedOn = (SELECT MAX(updatedOn) FROM EnrollmentAud ena WHERE ena.id=:enrollmentAudID and ena.updatedOn<=:eventDate)")
	List<EnrollmentAud> getEnrollmentAudByIdAndDate(
			@Param("enrollmentAudID") Integer enrollmentAudID,
			@Param("eventDate") Date eventDate);

	@Query(" FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentId "
			+ " AND en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','PENDING')")
	List<EnrollmentAud> getEnrollmentByIdAndNoPendingStatus(
			@Param("enrollmentId") Integer enrollmentId);

	@Query(" FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentId and en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','PENDING') and en.updatedOn<=:eventDate")
	List<EnrollmentAud> getEnrollmentByIdAndNoPendingStatus(
			@Param("enrollmentId") Integer enrollmentId,
			@Param("eventDate") Date eventDate);
	
	@Query(" FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentId and en.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','PENDING') and en.rev =:rev")
	List<EnrollmentAud> getEnrollmentByIdRevAndNoPendingStatus(
			@Param("enrollmentId") Integer enrollmentId,
			@Param("rev") Integer rev);

	/*@Query(" SELECT en.enrollmentStatusLkp FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentID "
			+ " AND en.updatedOn = (select max(updatedOn) FROM  EnrollmentAud "
			+ " WHERE id = :enrollmentID AND enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM') ) AND ROWNUM = 1")
	LookupValue getPrevStatusForReInstmt(
			@Param("enrollmentID") Integer enrollmentID);*/


	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.houseHoldCaseId = :householdId "
				+ "and TO_DATE(TO_CHAR(ena.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and ena.insuranceTypeLkp.lookupValueCode='HLT' and ena.rev IN (SELECT enrla.rev FROM EnrolleeAud as enrla where enrla.enrollment.id = ena.id))")
	EnrollmentAud getMaxEnrollmentForHouseHold(@Param("householdId") String householdId, @Param("endDate") String endDateString);

	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.houseHoldCaseId = :householdId "
				+ "AND TO_DATE(TO_CHAR(ena.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and ena.insuranceTypeLkp.lookupValueCode='DEN' and ena.rev in (SELECT enrla.rev FROM EnrolleeAud as enrla where enrla.enrollment.id = ena.id))")
	EnrollmentAud getMaxDentalEnrollmentForHouseHold(@Param("householdId") String householdId, @Param("endDate") String endDateString);


    @Query(nativeQuery = true,
            value = "select e_aud.id enrollment_id, "
            		+ "                      e_aud.LAST_UPDATE_TIMESTAMP update_on, "
            		+ "                      e_aud.CAP_AGENT_ID cap_agent_id, "
            		+ "			          e_aud.submitted_to_carrier_date submitted_to_carrier_date "
            		+ "                from ENROLLMENT_AUD e_aud where e_aud.rev in ( select min(e.rev) "
            		+ "               FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) " 
            		+ "               WHERE  "
            		+ "               CAP_AGENT_ID is not null and l.lookup_value_code = :status "
            		+ "               and e.LAST_UPDATE_TIMESTAMP between TO_TIMESTAMP(:startDate, 'YYYY-MM-DD HH24:MI:SS.FF') and TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF') "
            		+ "                  group by  e.id "
            		+ "                   ) "
    )
    List<Object[]> findByStatusCreatedAndCreateTimestampBetween(@Param("status") String status, @Param("startDate") String startDate, @Param("endDate") String endDate);

    @Query(nativeQuery = true, value = "select e_aud.id enrollment_id, "
    		+ "                      e_aud.LAST_UPDATE_TIMESTAMP update_on, "
    		+ "                      e_aud.CAP_AGENT_ID cap_agent_id, "
    		+ "			          e_aud.submitted_to_carrier_date submitted_to_carrier_date "
    		+ "                from ENROLLMENT_AUD e_aud where e_aud.rev in ( select min(e.rev) "
    		+ "               FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) " 
    		+ "               WHERE  "
    		+ "               CAP_AGENT_ID is not null and l.lookup_value_code = :status "
    		+ "                   and e.LAST_UPDATE_TIMESTAMP < TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF') "
    		+ "                  group by  e.id "
    		+ "                   ) ")
    List<Object[]> findByStatusCreatedAndCreateTimestampBefore(
            @Param("status") String status, @Param("endDate") String endDate);
    
    @Query(nativeQuery = true, value = "select e_aud.id enrollment_id, "
    		+ "                      e_aud.LAST_UPDATE_TIMESTAMP update_on, "
    		+ "                      e_aud.CAP_AGENT_ID cap_agent_id, "
    		+ "			          e_aud.submitted_to_carrier_date submitted_to_carrier_date "
    		+ "                from ENROLLMENT_AUD e_aud where e_aud.rev in ( select min(e.rev) "
    		+ "               FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) " 
    		+ "               WHERE  "
    		+ "               CAP_AGENT_ID is not null and l.lookup_value_code = :status "
    		+ "                   and e.LAST_UPDATE_TIMESTAMP < TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF') "
    		+ "                  group by  e.id ) "
    		+ "           and e_aud.tenant_id = :tenant_id order by e_aud.rev asc offset :offset limit :limit ")
    List<Object[]> findByStatusCreatedAndCreateTimestampBeforeByTenantAndLimit(
            @Param("status") String status, @Param("endDate") String endDate, @Param("tenant_id") Long tenant_id,@Param("offset") Long offset,@Param("limit") Long limit);

	/*@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_status, update_on, cap_agent_id, submitted_to_carrier_date from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(l.lookup_value_code) OVER (PARTITION BY e.id ORDER BY e.last_update_timestamp) prev_status, "
			+ "          l.lookup_value_code status, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   WHERE e.revtype = 1 and e.LAST_UPDATE_TIMESTAMP between :startDate and :endDate "
			+ "   ORDER BY e.id, e.rev "
			+ ") where (prev_status is null or prev_status <> :status) and status = :status")
	List<Object[]> findByStatusUpdatedToAndUpdateOnBetween(
			@Param("status") String status, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate);*/

	/*@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_status, update_on, cap_agent_id, submitted_to_carrier_date from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(l.lookup_value_code) OVER (PARTITION BY e.id ORDER BY e.last_update_timestamp) prev_status, "
			+ "          l.lookup_value_code status, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   WHERE e.revtype = 1 and e.LAST_UPDATE_TIMESTAMP < :endDate "
			+ "   ORDER BY e.id, e.rev "
			+ ") where (prev_status is null or prev_status <> :status) and status = :status")
	List<Object[]> findByStatusUpdatedToAndUpdateOnBefore(
			@Param("status") String status, @Param("endDate") Date endDate);*/

	/*
	 * @Query(nativeQuery = true, value =
	 * "select distinct enrollment_id, prev_status, update_on, cap_agent_id from ( "
	 * + "   SELECT e.id enrollment_id, " +
	 * "          LAG(l.lookup_value_code) OVER (PARTITION BY e.id ORDER BY e.last_update_timestamp) prev_status, "
	 * + "          l.lookup_value_code status, " +
	 * "          e.LAST_UPDATE_TIMESTAMP update_on, " +
	 * "          e.CAP_AGENT_ID cap_agent_id " +
	 * "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
	 * +
	 * "   WHERE e.revtype = 1 and e.LAST_UPDATE_TIMESTAMP between :startDate and :endDate "
	 * + "   ORDER BY e.id, e.rev " +
	 * ") where prev_status = :status and (prev_status is null or prev_status <> :status)"
	 * ) List<Object[]>
	 * findByStatusUpdatedFromAndUpdateOnBetween(@Param("status") String status,
	 *
	 * @Param("startDate") Date startDate, @Param("endDate") Date endDate);
	 */

	/*@Query(nativeQuery = true, value = "select distinct enrollment_id from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(l.lookup_value_code) OVER (PARTITION BY e.id ORDER BY e.LAST_UPDATE_TIMESTAMP) prev_status, "
			+ "          l.lookup_value_code status, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   WHERE e.revtype = 1 and e.LAST_UPDATE_TIMESTAMP < :thresholdDate "
			+ "   ORDER BY e.id, e.rev "
			+ ") where (prev_status is null or prev_status <> :status) and status = :status")
	List<BigDecimal> findEnrollmentIdByCurrentStatusUpdatedToBefore(
			@Param("status") String status,
			@Param("thresholdDate") Date thresholdDate);*/

	/*@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_cap_agent_id, cap_agent_id, update_on from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(e.CAP_AGENT_ID) OVER (PARTITION BY e.id ORDER BY e.last_update_timestamp) prev_cap_agent_id, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   WHERE e.revtype = 1 and e.LAST_UPDATE_TIMESTAMP between :startDate and :endDate "
			+ "   and l.lookup_value_code = :status "
			+ "   ORDER BY e.id, e.rev "
			+ ") where (prev_cap_agent_id is null and cap_agent_id is not null) "
			+ "  or (prev_cap_agent_id is not null and cap_agent_id is null) "
			+ "  or prev_cap_agent_id <> cap_agent_id")
	List<Object[]> findByCapAgentIdUpdatedAndStatusAndUpdateOnBetween(
			@Param("status") String status, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate);*/

	/*@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_cap_agent_id, cap_agent_id, update_on from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(e.CAP_AGENT_ID) OVER (PARTITION BY e.id ORDER BY e.last_update_timestamp) prev_cap_agent_id, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   WHERE e.revtype = 1 and e.LAST_UPDATE_TIMESTAMP < :endDate "
			+ "   and l.lookup_value_code = :status "
			+ "   ORDER BY e.id, e.rev "
			+ ") where (prev_cap_agent_id is null and cap_agent_id is not null) "
			+ "  or (prev_cap_agent_id is not null and cap_agent_id is null) "
			+ "  or prev_cap_agent_id <> cap_agent_id")
	List<Object[]> findByCapAgentIdUpdatedAndStatusAndUpdateOnBefore(
			@Param("status") String status, @Param("endDate") Date endDate);
*/
	@Query(" FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentId "
			+ " AND en.updatedOn = (SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
			+ " WHERE ena.id = :enrollmentId "
			+ " AND TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') <= TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ " AND ena.enrollmentStatusLkp.lookupValueCode IN ('PAYMENT_RECEIVED','CONFIRM','TERM') )")
	List<EnrollmentAud> getMaxEnrollmentAudForEnrollment(
			@Param("enrollmentId") Integer enrollmentId,
			@Param("endDate") String endDateString, Pageable pageable);

//    @Query(" FROM EnrollmentAud as en "
//            + " WHERE en.id = :enrollmentId "
//            + " AND en.rev = (SELECT MAX(ena.rev) FROM EnrollmentAud ena "
//            + " WHERE ena.id = :enrollmentId "
//            + " AND ena.updatedOn < :endDate "
//            + " and ena.enrollmentStatusLkp.lookupValueCode IN ('PAYMENT_RECEIVED','CONFIRM','TERM'))")
//    EnrollmentAud getMaxEnrollmentAudForEnrollment(@Param("enrollmentId") Integer enrollmentId, @Param("endDate") Date endDate);

	@Query(" SELECT ea.rev From EnrollmentAud ea "
			+ " WHERE ea.enrollmentStatusLkp.lookupValueCode IN ('CANCEL','TERM') "
			+ " AND ea.id = :enrollmentID  AND ea.updatedOn = ( SELECT MAX(eaa.updatedOn) From EnrollmentAud eaa  WHERE eaa.enrollmentStatusLkp.lookupValueCode IN ('CANCEL','TERM') and eaa.id= :enrollmentID)"
			)
	Integer getFirstDisEnrollRevForEnrollment(
			@Param("enrollmentID") Integer enrollmentID);

	@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_cap_agent_id, cap_agent_id, update_on, status, submitted_to_carrier_date from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(e.CAP_AGENT_ID, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_cap_agent_id, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on, "
			+ "          l.lookup_value_code status, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   WHERE l.lookup_value_code in :status "
			+ "   ORDER BY e.id, e.rev "
			+ ") as enrollmentId where ((prev_cap_agent_id is null and cap_agent_id is not null) "
			+ "  or (prev_cap_agent_id is not null and prev_cap_agent_id <> -1 and cap_agent_id is null) "
			+ "  or (prev_cap_agent_id is not null and prev_cap_agent_id <> -1 and cap_agent_id is not null and prev_cap_agent_id <> cap_agent_id))"
			+ "  and update_on < TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF')")
	List<Object[]> findByCapAgentIdUpdatedAndStatusInAndUpdateOnBefore(
			@Param("status") List<String> status,
			@Param("endDate") String endDate);
	
	@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_cap_agent_id, cap_agent_id, update_on, status, submitted_to_carrier_date from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(e.CAP_AGENT_ID, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_cap_agent_id, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on, "
			+ "          l.lookup_value_code status, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date ,"
			+ "			 e.tenant_id tenantId "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   WHERE l.lookup_value_code in :status "
			+ "   ORDER BY e.id, e.rev "
			+ ") EN_AUD where ((prev_cap_agent_id is null and cap_agent_id is not null) "
			+ "  or (prev_cap_agent_id is not null and prev_cap_agent_id <> -1 and cap_agent_id is null) "
			+ "  or (prev_cap_agent_id is not null and prev_cap_agent_id <> -1 and cap_agent_id is not null and prev_cap_agent_id <> cap_agent_id))"
			+ "  and update_on < TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF') "
			+ "  and tenantId = :tenant_id offset :offset limit :limit ")
	List<Object[]> findByCapAgentIdUpdatedAndStatusInAndUpdateOnBeforeByTenantAndLimit(
			@Param("status") List<String> status,
			@Param("endDate") String endDate, @Param("tenant_id") Long tenant_id,@Param("offset") Long offset,@Param("limit") Long limit);	

	@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_cap_agent_id, cap_agent_id, update_on, status, submitted_to_carrier_date from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(e.CAP_AGENT_ID, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_cap_agent_id, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on, "
			+ "          l.lookup_value_code status, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   WHERE l.lookup_value_code in :status "
			+ "   ORDER BY e.id, e.rev "
			+ ") EN_AUD where ((prev_cap_agent_id is null and cap_agent_id is not null) "
			+ "  or (prev_cap_agent_id is not null and prev_cap_agent_id <> -1 and cap_agent_id is null) "
			+ "  or (prev_cap_agent_id is not null and prev_cap_agent_id <> -1 and cap_agent_id is not null and prev_cap_agent_id <> cap_agent_id))"
			+ "  and update_on between TO_TIMESTAMP(:startDate, 'YYYY-MM-DD HH24:MI:SS.FF') and TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF')")
	List<Object[]> findByCapAgentIdUpdatedAndStatusInAndUpdateOnBetween(
			@Param("status") List<String> status,
			@Param("startDate") String startDate,
			@Param("endDate") String endDate);

	@Query(nativeQuery = true,
            value = "select distinct enrollment_id, prev_status, update_on, cap_agent_id, submitted_to_carrier_date from ( " +
                "   SELECT e.id enrollment_id, " +
                "          LAG(l.lookup_value_code, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_status, " +
                "          l.lookup_value_code status, " +
                "          e.LAST_UPDATE_TIMESTAMP update_on, " +
                "          e.CAP_AGENT_ID cap_agent_id, " +
				"          e.submitted_to_carrier_date submitted_to_carrier_date " +
                "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) " +
				"  WHERE e.last_update_timestamp BETWEEN TO_TIMESTAMP(:startDate, 'YYYY-MM-DD HH24:MI:SS.FF') AND TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF') "+
                "   ORDER BY e.id, e.rev " +
                ") EN_AUD where (prev_status is null or (prev_status <> '-1' and prev_status <> status)) and cap_agent_id is not null and status = :status" 
    )
    List<Object[]> findByStatusUpdatedToAndUpdateOnBetween(@Param("status") String status, @Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_status, update_on, cap_agent_id, submitted_to_carrier_date from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(l.lookup_value_code, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_status, "
			+ "          l.lookup_value_code status, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   ORDER BY e.id, e.rev "
			+ ") enrollmentId where (prev_status is null or (prev_status <> '-1' and prev_status <> status)) and cap_agent_id is not null and status = :status"
			+ "  and update_on < TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF')")
	List<Object[]> findByStatusUpdatedToAndUpdateOnBefore(
			@Param("status") String status, @Param("endDate") String endDate);
	
	@Query(nativeQuery = true, value = "select distinct enrollment_id, prev_status, update_on, cap_agent_id, submitted_to_carrier_date from ( "
			+ "   SELECT e.id enrollment_id, "
			+ "          LAG(l.lookup_value_code, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_status, "
			+ "          l.lookup_value_code status, "
			+ "          e.LAST_UPDATE_TIMESTAMP update_on, "
			+ "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date, "
			+ "			 e.tenant_id tenantid "
			+ "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
			+ "   ORDER BY e.id, e.rev "
			+ ") EN_AUD where (prev_status is null or (prev_status <> '-1' and prev_status <> status)) and cap_agent_id is not null and status = :status"
			+ "  and update_on < TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF')"
			+ "  and tenantid = :tenant_id offset :offset limit :limit ")
	List<Object[]> findByStatusUpdatedToAndUpdateOnBeforeByTenantAndLimit(
			@Param("status") String status, @Param("endDate") String endDate, @Param("tenant_id") Long tenant_id,@Param("offset") Long offset,@Param("limit") Long limit);

	/* @Query(nativeQuery = true,
            value = "select count(*) from (select * from ( " +
                    "   SELECT LAG(l.lookup_value_code, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_status, " +
                    "          l.lookup_value_code status, e.LAST_UPDATE_TIMESTAMP update_on " +
                    "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) " +
                    "   WHERE e.id = :enrollmentId " +
                    "   ORDER BY e.rev " +
                    ") where (prev_status is null or (prev_status <> '-1' and prev_status <> status)) and status = :status" +
                    "  and update_on between TO_TIMESTAMP(:startDate, 'YYYY-MM-DD HH24:MI:SS.FF') and TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF'))")
    BigDecimal countByEnrollmentIdAndStatusUpdatedToAndUpdateOnBetween(@Param("enrollmentId") Integer enrollmentId,
                                                                       @Param("status") String status,
                                                                       @Param("startDate") String startDate,
                                                                       @Param("endDate") String endDate);*/

    @Query("Select en.aptcAmt FROM EnrollmentAud as en "
			+ "WHERE en.id = :enrollmentID "
			+ " AND en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.id = :enrollmentID "
				+ "and  TO_DATE(TO_CHAR(ena.aptcEffDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY'))")
	Float getLatestAptcBeforeDate(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") String endDateString );
    
    
    @Query("Select en.csrAmt FROM EnrollmentAud as en "
			+ "WHERE en.id = :enrollmentID "
			+ " AND en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.id = :enrollmentID "
				+ "and  ena.csrEffDate < :endDate)")
	Float getLatestCSRBeforeDate(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") Date endDate);

    @Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.updatedOn = "
				+ "(SELECT MIN(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.houseHoldCaseId = :householdId "
				+ "and TO_DATE(TO_CHAR(ena.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and ena.insuranceTypeLkp.lookupValueCode='HLT')")
	EnrollmentAud getMinEnrollmentAudForHouseHold(@Param("householdId") String householdId, @Param("endDate") String endDateString);

	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.updatedOn = "
				+ "(SELECT MIN(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.houseHoldCaseId = :householdId "
				+ "and TO_DATE(TO_CHAR(ena.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and ena.insuranceTypeLkp.lookupValueCode='DEN')")
	EnrollmentAud getMinDentalEnrollmentAudForHouseHold(@Param("householdId") String householdId, @Param("endDate") Date endDateString);

    @Query(nativeQuery = true,
            value = "select distinct enrollment_id, status, update_on, cap_agent_id, submitted_to_carrier_date from ( " +
                    "   SELECT e.id enrollment_id, " +
                    "          LAG(l.lookup_value_code, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_status, " +
                    "          l.lookup_value_code status, " +
                    "          e.LAST_UPDATE_TIMESTAMP update_on, " +
                    "          LAG(e.CAP_AGENT_ID, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_cap_agent_id, " +
                    "          e.CAP_AGENT_ID cap_agent_id, " +
					"          e.submitted_to_carrier_date submitted_to_carrier_date " +
                    "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) " +
                    "   ORDER BY e.id, e.rev " +
                    ") EN_AUD where cap_agent_id <> -1 and cap_agent_id = prev_cap_agent_id and status in (:status)" +
                    "  and update_on between TO_TIMESTAMP(:startDate, 'YYYY-MM-DD HH24:MI:SS.FF') and TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF')"
    )
    List<Object[]> findByStatusUpdatedFromToAndUpdateOnBetween(@Param("status") List<String> status, @Param("startDate") String startDate, @Param("endDate") String endDate);

    @Query(nativeQuery = true, value = "select distinct enrollment_id, status, update_on, cap_agent_id, submitted_to_carrier_date from ( "
            + "   SELECT e.id enrollment_id, "
            + "          LAG(l.lookup_value_code, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_status, "
            + "          l.lookup_value_code status, "
            + "          e.LAST_UPDATE_TIMESTAMP update_on, "
            + "          LAG(e.CAP_AGENT_ID, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_cap_agent_id, "
            + "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date "
            + "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
            +
            "   ORDER BY e.id, e.rev "
            + ") as enrollmentId where cap_agent_id <> -1 and cap_agent_id = prev_cap_agent_id and cap_agent_id is not null and status in (:status)"
            + "  and update_on < TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF')")
    List<Object[]> findByStatusUpdatedFromToAndUpdateOnBefore(@Param("status") List<String> status, @Param("endDate") String endDate);
    
    @Query(nativeQuery = true, value = "select distinct enrollment_id, status, update_on, cap_agent_id, submitted_to_carrier_date from ( "
            + "   SELECT e.id enrollment_id, "
            + "          LAG(l.lookup_value_code, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_status, "
            + "          l.lookup_value_code status, "
            + "          e.LAST_UPDATE_TIMESTAMP update_on, "
            + "          LAG(e.CAP_AGENT_ID, 1, '-1') OVER (PARTITION BY e.id ORDER BY e.rev) prev_cap_agent_id, "
            + "          e.CAP_AGENT_ID cap_agent_id, "
			+ "          e.submitted_to_carrier_date submitted_to_carrier_date, "
			+ "			 e.tenant_id tenantId"
            + "   FROM ENROLLMENT_AUD e JOIN lookup_value l ON (e.ENROLLMENT_STATUS_LKP = l.lookup_value_id) "
            + "   ORDER BY e.id, e.rev "
            + ") EN_AUD where cap_agent_id <> -1 and cap_agent_id = prev_cap_agent_id and cap_agent_id is not null and status in (:status)"
            + "  and update_on < TO_TIMESTAMP(:endDate, 'YYYY-MM-DD HH24:MI:SS.FF') "
            + "  and tenantId = :tenant_id offset :offset limit :limit ")
    List<Object[]> findByStatusUpdatedFromToAndUpdateOnBeforeByTenantAndLimit(@Param("status") List<String> status, @Param("endDate") String endDate, @Param("tenant_id") Long tenant_id,@Param("offset") Long offset,@Param("limit") Long limit);

	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :id and "
			+ "en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.houseHoldCaseId = :householdId "
				+ "and ena.id = :id "
				+ "and TO_DATE(TO_CHAR(ena.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and ena.insuranceTypeLkp.lookupValueCode= :insuranceType and ena.rev IN (SELECT enrla.rev FROM EnrolleeAud as enrla where enrla.enrollment.id = ena.id))")
	EnrollmentAud getMaxEnrollmentForId(@Param("householdId") String householdId, @Param("id") Integer id, @Param("endDate") String endDateString, @Param("insuranceType") String insuranceType);

	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :id and "
			+ "en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				//+ "ena.houseHoldCaseId = :householdId "
				+ "ena.id = :id "
				+ "and TO_DATE(TO_CHAR(ena.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and ena.rev IN (SELECT enrla.rev FROM EnrolleeAud as enrla where enrla.enrollment.id = ena.id))")
	EnrollmentAud getMaxEnrollmentForId( @Param("id") Integer id, @Param("endDate") String endDateString);

	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :id and "
			+ "en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				//+ "ena.houseHoldCaseId = :householdId "
				+ "ena.id = :id "
				+ "and TO_DATE(TO_CHAR(ena.aptcEffDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				//+ "and TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ ")")
	List<EnrollmentAud> getMaxAPTCEnrollmentForId( @Param("id") Integer id, @Param("endDate") String endDateString);
	
	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :id and "
			+ "en.updatedOn = "
				+ "(SELECT MIN(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				//+ "ena.houseHoldCaseId = :householdId "
				+ "ena.id = :id "
				+ "and ena.aptcEffDate is not null and TO_DATE(TO_CHAR(ena.aptcEffDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ ")")
	List<EnrollmentAud> getOldestAPTCEnrollmentForId( @Param("id") Integer id, @Param("endDate") String endDate, Pageable pageable);

	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :id and "
			+ "en.updatedOn = "
			+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
			+ "WHERE "
			//+ "ena.houseHoldCaseId = :householdId "
			+ "ena.id = :id "
			+ "and TO_DATE(TO_CHAR(ena.stateSubsidyEffDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
			//+ "and TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ ")")
	List<EnrollmentAud> getMaxStateSubsidyEnrollmentForId( @Param("id") Integer id, @Param("endDate") String endDateString);

	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :id and "
			+ "en.updatedOn = "
			+ "(SELECT MIN(ena.updatedOn) FROM EnrollmentAud ena "
			+ "WHERE "
			//+ "ena.houseHoldCaseId = :householdId "
			+ "ena.id = :id "
			+ "and TO_DATE(TO_CHAR(ena.updatedOn,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ ")")
	List<EnrollmentAud> getOldestStateSubsidyEnrollmentForId( @Param("id") Integer id, @Param("endDate") String endDate, Pageable pageable);
	
	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :id and "
			+ "en.updatedOn = "
				+ "(SELECT MIN(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.houseHoldCaseId = :householdId "
				+ "and ena.id = :id "
				+ "and TO_DATE(TO_CHAR(ena.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
				+ "and ena.insuranceTypeLkp.lookupValueCode= :insuranceType)")
	EnrollmentAud getMinEnrollmentAudForHouseHold(@Param("householdId") String householdId, @Param("id") Integer id , @Param("endDate") String endDateString, @Param("insuranceType") String insuranceType);

	@Query("Select DISTINCT en.aptcEffDate FROM EnrollmentAud as en "
			+ "WHERE en.id = :enrollmentID and TO_DATE(TO_CHAR(en.aptcEffDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ "and en.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM') order by en.aptcEffDate ASC  ")
	List<Date> getDistinctAptcEffDateForMonth(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") String endDateString);

	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :enrollmentID and "
			+ "en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE "
				+ "ena.id = :enrollmentID "
				+ "and TO_DATE(TO_CHAR(ena.aptcEffDate,'MM/DD/YYYY'),'MM/DD/YYYY') = TO_DATE(:aptcEffDate, 'MM/DD/YYYY')) ")
	EnrollmentAud getMaxAuditForAptcEffDate(@Param("enrollmentID") Integer enrollmentID , @Param("aptcEffDate") String aptcEffDateString);

	@Query(" FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentId AND "
			+ " en.updatedOn = (SELECT MIN(ena.updatedOn) FROM EnrollmentAud ena "
			+ " WHERE ena.id = :enrollmentId "
			+ " )")
	List<EnrollmentAud> getMaxEnrollmentAudForEnrollment(@Param("enrollmentId") Integer enrollmentId,  Pageable  pageable);

    @Query(nativeQuery = true, value = "select * from ENROLLMENT_AUD where id = :enrollmentId and revtype = 0")
    EnrollmentAud findInsertAudByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);

	@Query("select e from EnrollmentAud e where e.id = :enrollmentId order by e.rev")
	List<EnrollmentAud> findByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);

    @Query("select e from EnrollmentAud e where e.createdOn >= :date")
    List<EnrollmentAud> findByCreateTimestampAfter(@Param("date") Date date);

	@Query("select e from EnrollmentAud e where e.submittedToCarrierDate > :date")
	List<EnrollmentAud> findBySubmittedToCarrierDateAfter(@Param("date") Date date);

    @Query("select e from EnrollmentAud e where e.rev = (select max(e1.rev) FROM EnrollmentAud e1 where e1.id = :enrollmentId)")
    EnrollmentAud findLastEnrollmentAudForEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
    
	@Query("SELECT DISTINCT ena.houseHoldCaseId from EnrollmentAud as ena " +
			" WHERE ena.houseHoldCaseId in "
			+ "(SELECT DISTINCT e.houseHoldCaseId from Enrollment as e where "
			+ "TO_DATE(TO_CHAR(e.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "
			+ "AND TO_DATE(TO_CHAR(e.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
			+ "AND e.enrollmentStatusLkp.lookupValueCode IN ('TERM')"
			+ "AND e.planLevel NOT IN ('CATASTROPHIC') "
			+ "AND e.insuranceTypeLkp.lookupValueCode = 'HLT') "
			+ "and ena.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM')")
	List<String> getUniqueTerminatedHouseHoldIDList(@Param("startDate") String startDateString, @Param("endDate") String endDateString);
	
	@Query("Select en.slcspAmt FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :enrollmentId and "
			+ "en.updatedOn = "
			+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
			+ "WHERE "
			+ "ena.id = :enrollmentId "
			+ "and TO_DATE(TO_CHAR(ena.slcspEffDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY'))")
	Float getLatestSlcspAmtBeforeDate(@Param("enrollmentId") Integer enrollmentId, @Param("endDate") String endDateString);
	
	/**
	 * HIX-70722 Returns the initial carrier submission date, when the enrollment went to PENDING status for the first time
	 * @param enrollmentId
	 * @return Date carrier submission date
	 */
	@Query("Select en.submittedToCarrierDate FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :enrollmentId and "
			+ "en.updatedOn = "
			+ "(SELECT MIN(ena.updatedOn) FROM EnrollmentAud ena "
			+ "WHERE "
			+ "ena.id = :enrollmentId "
			+ "and ena.enrollmentStatusLkp.lookupValueCode IN ('PENDING'))")
	Date getCarrierSubmissionDate(@Param("enrollmentId") Integer enrollmentId);
	
	/**
	 * Jira ID: HIX-73086
	 * Set last status update date in EnrollmentCapDto
	 * sorting Fetched record in ascending order by UpdatedOn
	 * @param enrollmentId Integer
	 * @return Date
	 */
	@Query("SELECT MIN(aud.updatedOn) FROM EnrollmentAud as aud, Enrollment en"
			+ " WHERE aud.id = en.id AND en.id = :enrollmentId AND aud.enrollmentStatusLkp = en.enrollmentStatusLkp")
	List<Date> getEnrollmentLastStatusUpdateTimeStamp(@Param("enrollmentId") Integer enrollmentId, Pageable pageable);
	
	/**
	 * Jira ID: HIX-73086
	 * Set last status update date in EnrollmentCapDto
	 * sorting Fetched record in ascending order by UpdatedOn
	 * @param enrollmentId Integer
	 * @return Date
	 */
	@Query(nativeQuery = true, value="SELECT MIN(enrollment0_.LAST_UPDATE_TIMESTAMP) AS col_0_0_ FROM ENROLLMENT_AUD enrollment0_ CROSS JOIN ENROLLMENT enrollment1_"
			  +" WHERE enrollment0_.id=enrollment1_.id AND enrollment0_.TENANT_ID = :tenant_id AND enrollment1_.TENANT_ID = :tenant_id "
			  +" AND enrollment1_.id= :enrollmentId AND enrollment0_.ENROLLMENT_STATUS_LKP=enrollment1_.ENROLLMENT_STATUS_LKP")
	Date getEnrollmentLastStatusUpdateTimeStampForPostgres(@Param("tenant_id") Long tenant_id,@Param("enrollmentId") Integer enrollmentId);
	
	/**
	 * List of SSAP Application IDs from Audit except the current
	 * @param enrollmentId
	 * @param ssapApplicationId
	 * @return List of SSAP Application IDs
	 */
	@Query("SELECT ena.ssapApplicationid FROM EnrollmentAud as ena"
			+ " WHERE ena.id = :enrollmentId AND ena.ssapApplicationid <> :ssapApplicationId"
			+ " ORDER BY ena.updatedOn DESC")
	List<Long> getSsapApplicationIdForStagingJob(@Param("enrollmentId") Integer enrollmentId, @Param("ssapApplicationId") Long ssapApplicationId);
	
	@Query(" FROM EnrollmentAud as en "
			+ " WHERE en.id = :enrollmentId "
			+ " ORDER BY en.updatedOn ASC")
	List<EnrollmentAud> getEnrollmentAudbyId(
			@Param("enrollmentId") Integer enrollmentId);
	
	@Query("FROM EnrollmentAud as en "
			+ "WHERE "
			+ "en.id = :enrollmentId and "
			+ "en.updatedOn = "
				+ "(SELECT MAX(ena.updatedOn) FROM EnrollmentAud ena "
				+ "WHERE ena.id = :enrollmentId)")
	EnrollmentAud getMaxAuditRecordForEnrollment(@Param("enrollmentId") Integer enrollmentId);
	
	@Query(" SELECT en.id, "
			   + " en.CMSPlanID, "
			   + " en.insurerName, "
			   + " en.planName, "
			   + " en.benefitEffectiveDate, "
			   + " en.benefitEndDate, "
			   + " en.planLevel,"
			   + " en.grossPremiumAmt,"
			   + " en.aptcAmt, "
			   + " en.netPremiumAmt, "
			   + " en.paymentTxnId, "
			   + " en.stateSubsidyAmt "
			   + " FROM EnrollmentAud as en"
			   + " WHERE en.id= :enrollmentId"
			   + " AND en.rev= :rev")
	List<Object[]> getEnrollmentSnapshotByld(@Param("enrollmentId") Integer enrollmentId , @Param("rev") Integer rev);
	
	
	@Query(" SELECT en.id, "
			   + " en.CMSPlanID, "
			   + " en.insurerName, "
			   + " en.planName, "
			   + " en.benefitEffectiveDate, "
			   + " en.benefitEndDate, "
			   + " en.planLevel,"
			   + " en.grossPremiumAmt,"
			   + " en.aptcAmt, "
			   + " en.netPremiumAmt, "
			   + " en.paymentTxnId, "
			   + " en.stateSubsidyAmt "
			   + " FROM EnrollmentAud as en"
			   + " WHERE en.id = :enrollmentAudID "
			   + " AND en.updatedOn = (SELECT MAX(updatedOn) FROM EnrollmentAud ena WHERE ena.id=:enrollmentAudID and ena.updatedOn<=:eventDate)")
	List<Object[]> getEnrollmentAudByEnrollmentIdAndDate(@Param("enrollmentAudID") Integer enrollmentAudID,@Param("eventDate") Date eventDate);
	
	@Query("SELECT en.rev FROM EnrollmentAud as en "
			+ " where en.id =:enrollmentId"
			+ " order by en.updatedOn DESC")
	List<Integer> getLatestRevByEnrollmentId(@Param("enrollmentId") Integer enrollmentId, Pageable pageable);
}
