package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentPlan;


public interface IEnrollmentPlanRepository extends JpaRepository<EnrollmentPlan, Integer>{
	@Query("FROM EnrollmentPlan as enrlPlan where enrlPlan.planId = :planId")
	EnrollmentPlan findEnrollmentPlanByPlanID(@Param("planId") Integer planId);
}
