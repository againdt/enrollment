/**
 * 
 */
package com.getinsured.hix.enrollment.util;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author negi_s
 * SAX error handler for IRS schema validations
 */
public class EnrollmentIrsSaxErrorHandler implements ErrorHandler {
	
	private List<String> errorList;
	private Boolean isValid;

	public EnrollmentIrsSaxErrorHandler(){
		errorList = new ArrayList<String>();
		isValid = Boolean.TRUE;
	}
	
	/**
	 * @return the errorList
	 */
	public List<String> getErrorList() {
		return errorList;
	}

	/**
	 * @return the isValid
	 */
	public Boolean isValid() {
		return isValid;
	}
	
	/**
	 * Reset handler parameters
	 */
	public void resetHandler(){
		errorList.clear();
		isValid = Boolean.TRUE;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
	 */
	@Override
	public void warning(SAXParseException exception) throws SAXException {
		errorList.add("Line Number : " + exception.getLineNumber() + " Column Number : " + exception.getColumnNumber()+ " Message : " + exception.getMessage() + System.lineSeparator());
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
	 */
	@Override
	public void error(SAXParseException exception) throws SAXException {
		errorList.add("Line Number : " + exception.getLineNumber() + " Column Number : " + exception.getColumnNumber()+ " Message : " + exception.getMessage() + System.lineSeparator());
		isValid = Boolean.FALSE;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
	 */
	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		errorList.add("Line Number : " + exception.getLineNumber() + " Column Number : " + exception.getColumnNumber()+ " Message : " + exception.getMessage() + System.lineSeparator());
		isValid = Boolean.FALSE;
		throw exception;
	}

}
