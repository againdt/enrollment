package com.getinsured.hix.enrollment.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.EmployerEnrollment;

public interface IEnrlEmployerEnrollmentRepository extends JpaRepository<EmployerEnrollment, Integer> {
	
/*	@Query("select employerEnrollment from EmployerEnrollment employerEnrollment where " +
			"employer.id = :employerId and status = 'ACTIVE'")
	EmployerEnrollment checkStatusActive(@Param("employerId") Integer employerId);*/
	
	@Query("SELECT coverageDateStart FROM EmployerEnrollment employerEnrollment where id = :employer_enrollment_Id ")
	Date getCoverageStartDateByEmployerId(@Param("employer_enrollment_Id")int employer_enrollment_Id);
	
	@Query("SELECT coverageDateStart FROM EmployerEnrollment employerEnrollment where id = :employer_enrollment_Id ")
	Date getTermCoverageStartDateByEmployerId(@Param("employer_enrollment_Id")int employer_enrollment_Id);

}
