/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Enrollment Monthly IRS Notification Interface
 * @author negi_s
 *
 */
public interface EnrollmentMonthlyIRSNotificationService {
	
	/**
	 * Create an inbound batch notification for processing monthly IRS Errors from CMS - Part 1
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendEnrollmentMonthlyIRSInEmail(Map<String, String> requestMap) throws GIException;
	
	/**
	 * Create an inbound batch notification for processing monthly IRS Errors from CMS - Part 1
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendEnrollmentMonthlyInProcessEmail(Map<String, String> emailDataMap) throws GIException;
	
	/**
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendDoNothingEmail(Map<String, String> emailDataMap) throws GIException;
}
