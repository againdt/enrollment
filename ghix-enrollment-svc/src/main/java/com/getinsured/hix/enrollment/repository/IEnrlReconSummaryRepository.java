package com.getinsured.hix.enrollment.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrlReconSummary;

public interface IEnrlReconSummaryRepository extends JpaRepository<EnrlReconSummary, Integer> {
	
	@Query("FROM EnrlReconSummary ers " +
			"WHERE ers.id = :fileId ")
	EnrlReconSummary getReconSummaryByFileId(@Param("fileId") Integer  fileId);
	
	/**
	 * 
	 * @param monthNum
	 * @param year
	 * @param hiosIssuerId
	 * @return
	 */
	@Query("SELECT ers.fileName, ers.dateReceived, ers.status, ers.totalEnrlCntInFile, "
			+ " ers.totalEnrlCntInHix, ers.enrlCntSuccess, ers.recCntFailed, ers.enrlCntMissingInHix, ers.enrlCntMissingInFile,"
			+ " ers.cntDiscrepancies, ers.enrlCntDiscrepancies, ers.discrepancyReportName, ers.discrepancyReportCreationTimestamp,"
			+ " ers.id"
			+ " FROM EnrlReconSummary as ers "
			+ " WHERE ers.month =:monthNum AND ers.year =:year AND ers.hiosIssuerId = :hiosIssuerId ")
	public List<Object[]> getFileLevelSummary(@Param("monthNum") Integer monthNum, @Param("year") Integer year, @Param("hiosIssuerId") String hiosIssuerId);
	
	/**
	 * 
	 * @param monthNum
	 * @param year
	 * @param hiosIssuerIdList
	 * @return
	 */
	@Query("SELECT ers.month, ers.year, sum(ers.totalEnrlCntInFile), SUM(ers.totalEnrlCntInHix), "
			+ " SUM(ers.enrlCntSuccess), SUM(ers.recCntFailed), SUM(ers.cntDiscrepancies), SUM(ers.enrlCntDiscrepancies),"
			+ " SUM(ers.enrlCntMissingInHix), SUM(ers.enrlCntMissingInFile)"
			+ " FROM EnrlReconSummary as ers WHERE ers.month =:monthNum AND ers.year =:year AND ers.hiosIssuerId IN (:hiosIssuerIdList)"
			+ "	AND ers.status IN ('Autofixed', 'Completed')"
			+ "	AND ers.status NOT IN ('Duplicate')"
			+ " GROUP BY ers.month, ers.year")
	public List<Object[]> getHighLevelSummary(@Param("monthNum") Integer monthNum, @Param("year") Integer year, @Param("hiosIssuerIdList") List<String> hiosIssuerIdList);
	
	@Query("SELECT ers.month, ers.year, sum(ers.totalEnrlCntInFile), SUM(ers.totalEnrlCntInHix), "
			+ " SUM(ers.enrlCntSuccess), SUM(ers.recCntFailed), SUM(ers.cntDiscrepancies), SUM(ers.enrlCntDiscrepancies),"
			+ " SUM(ers.enrlCntMissingInHix), SUM(ers.enrlCntMissingInFile)"
			+ " FROM EnrlReconSummary as ers WHERE ers.hiosIssuerId IN (:hiosIssuerIdList)"
			+ " AND TO_DATE(TO_CHAR(ers.dateReceived,'MM/DD/YYYY'),'MM/DD/YYYY') >= :requestStartDate AND TO_DATE(TO_CHAR(ers.dateReceived,'MM/DD/YYYY'),'MM/DD/YYYY') <= :requestEndDate "
			+ " AND ers.status IN ('Autofixed', 'Completed')"
			+ "	AND ers.status NOT IN ('Duplicate')"
			+ " GROUP BY ers.month, ers.year "
			+ " ORDER by ers.year DESC, ers.month DESC")
	public List<Object[]> getHistorySummaryDetails(@Param("hiosIssuerIdList") List<String> hiosIssuerIdList,
			@Param("requestEndDate") Date requestEndDate, @Param("requestStartDate") Date requestStartDate);
	
	
	/**
	 * 
	 * @param monthNum
	 * @param year
	 * @param hiosIssuerIdList
	 * @return
	 */
	@Query("SELECT COUNT(ers.fileName), ers.hiosIssuerId, iss.name, SUM(ers.totalEnrlCntInFile),"
			+ " SUM(ers.totalEnrlCntInHix), SUM(ers.enrlCntSuccess), SUM(ers.recCntFailed),"
			+ " SUM(ers.cntDiscrepancies), SUM(ers.enrlCntDiscrepancies), SUM(ers.enrlCntMissingInHix), SUM(ers.enrlCntMissingInFile)"
			+ " FROM EnrlReconSummary as ers, Issuer iss"
			+ " WHERE ers.hiosIssuerId = iss.hiosIssuerId"
			+ " AND ers.month = :monthNum AND ers.year = :year AND ers.hiosIssuerId IN (:hiosIssuerIdList)"
			+ " AND ers.status IN ('Autofixed', 'Completed')"
			+ "	AND ers.status NOT IN ('Duplicate')"
			+ " GROUP BY ers.hiosIssuerId, iss.name")
	public List<Object[]> getIssuerLevelSummary(@Param("monthNum") Integer monthNum, @Param("year") Integer year, @Param("hiosIssuerIdList") List<String> hiosIssuerIdList);
	
	/**
	 * 
	 * @param hiosIssuerId
	 * @return
	 */
	@Query("SELECT MAX(ers.dateReceived) FROM EnrlReconSummary as ers WHERE ers.hiosIssuerId =:hiosIssuerId")
	public Date getLastReconDate(@Param("hiosIssuerId")String hiosIssuerId);
	
	@Query("SELECT DISTINCT ers.month from EnrlReconSummary as ers ORDER BY ers.month")
	public List<Integer> getDistinctReconMonthList();
	
	@Query("SELECT DISTINCT ers.year FROM EnrlReconSummary as ers ORDER BY ers.year")
	public List<Integer> getDistinctReconYearList();
	
	@Query("SELECT ers.id FROM EnrlReconSummary as ers WHERE ers.hiosIssuerId =:hiosIssuerId AND ers.year = :year"
			+ " AND ers.discrepancyReportName is NULL AND ers.discrepancyReportCreationTimestamp is NULL AND ers.status = 'Autofixed'")
	List<Integer> getFileIdsForReportGeneration(@Param("hiosIssuerId")String hiosIssuerId,  @Param("year") Integer year);

	@Query("SELECT ers.fileName FROM EnrlReconSummary ers " +
			"WHERE ers.id = :fileId ")
	String getFileNameFromFileId(@Param("fileId") Integer  fileId);

	@Query("SELECT DISTINCT(ers.hiosIssuerId) FROM EnrlReconSummary as ers WHERE  ers.year = :year"
			+ " AND ers.discrepancyReportName is NULL AND ers.discrepancyReportCreationTimestamp is NULL AND ers.status = 'Summarized'")
	List<String> getHiosIdListForReportGeneration(@Param("year") Integer year);
	
	@Query("SELECT ers.fileName, ers.dateReceived from EnrlReconSummary as ers "
			+ " WHERE ers.id = (SELECT max(er.fileId) FROM EnrlReconSnapshot as er WHERE er.hixEnrollmentId = :hixEnrollmentId)")
	public Object[] getLastReconilicationFileAndReceivedDate(@Param("hixEnrollmentId") Long hixEnrollmentId);
	
	@Query(" FROM EnrlReconSummary as ers WHERE ers.hiosIssuerId =:hiosIssuerId AND ers.year = :year"
			+ " AND ers.month =:month")
	public List<EnrlReconSummary> getSummaryByIssuerAndMonth(@Param("month") Integer month,@Param("year") Integer year, @Param("hiosIssuerId")String hiosIssuerId);
	
	@Query("SELECT DISTINCT ers.hiosIssuerId FROM EnrlReconSummary as ers WHERE ers.year = :year AND ers.month =:month" )
	public List<String> getHiosIssuerIdsForMonthYear(@Param("month") Integer month,@Param("year") Integer year);
	
	@Query(" FROM EnrlReconSummary ers " +
			"WHERE ers.id = :fileId ")
	EnrlReconSummary getByFileId(@Param("fileId") Integer  fileId);
	
	/**
	 * @param date
	 * @return
	 */
	@Query(" SELECT iss.name, ers.fileName, ers.dateReceived, ers.status, ers.discrepancyReportName, ers.discrepancyReportCreationTimestamp "
			+ " FROM EnrlReconSummary as ers, Issuer iss"
			+ " WHERE ers.hiosIssuerId = iss.hiosIssuerId"
			+ " AND ers.dateProcessed >= :lastRunDate")
	public List<Object[]> getUpdateSummaryByDate(@Param("lastRunDate") Date lastRunDate);

	@Query("SELECT COUNT(ers.id) FROM EnrlReconSummary as ers WHERE ers.hiosIssuerId =:hiosIssuerId AND ers.year = :year"
			+ " AND ers.month =:month AND ers.coverageYear = :coverageYear AND ers.status <> 'Duplicate'")
	Long getFileCountByIssuerAndProcessingDate(@Param("hiosIssuerId") String hiosIssuerId, @Param("coverageYear") Integer coverageYear, @Param("month") Integer month, @Param("year") Integer year);
	
	@Query("SELECT ers.hiosIssuerId FROM EnrlReconSummary ers WHERE ers.id = :fileId ")
	String getHiosIssuerIdFromFileId(@Param("fileId") Integer  fileId);
	
	@Query("SELECT ers.id FROM EnrlReconSummary as ers WHERE ers.id = :fileId "
			+ " AND ers.discrepancyReportName is NULL AND ers.discrepancyReportCreationTimestamp is NULL AND ers.status = 'Autofixed'")
	List<Integer> inspectFileIdForReportGeneration(@Param("fileId") Integer  fileId);
}
