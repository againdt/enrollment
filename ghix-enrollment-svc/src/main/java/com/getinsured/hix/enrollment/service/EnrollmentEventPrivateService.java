package com.getinsured.hix.enrollment.service;



import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;

public interface EnrollmentEventPrivateService {

	void saveEnrollmentEvent(EnrollmentEvent enrollmentEvent);
	
	EnrollmentEvent findEnrollmentByeventIDAndplanID( Integer eventID,  Integer planID);

	/**
	 * @param enrollmentID
	 * @param enrolleeID
	 * @param createdDate
	 * @return List<EnrollmentEvent>
	 * @throws Exception
	 */
	List<EnrollmentEvent> findEventByenrollmentIDAndEnrolleeIDAndCreatedDate(
			Integer enrollmentID, Integer enrolleeID, Date createdDate);
	
	String saveConsumerErnrollmentEvent(Enrollment manualEnroll, String eventName , String prevEnrollmentStatus);
	
}
