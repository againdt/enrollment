package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.getinsured.hix.model.enrollment.EnrollmentInDtl1095;

public interface IEnrollmentInDtl1095Repository extends JpaRepository<EnrollmentInDtl1095, Integer> {

}
