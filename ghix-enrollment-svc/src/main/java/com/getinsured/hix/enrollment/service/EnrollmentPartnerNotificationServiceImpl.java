package com.getinsured.hix.enrollment.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.affiliate.service.AffiliateConfigService;
import com.getinsured.hix.platform.events.PartnerEventDataFieldEnum;
import com.getinsured.hix.platform.events.PartnerEventService;
import com.getinsured.hix.platform.events.PartnerEventTypeEnum;

@Component
public class EnrollmentPartnerNotificationServiceImpl implements EnrollmentPartnerNotificationService {
	
	private static final Logger LOGGER = Logger.getLogger(EnrollmentPartnerNotificationServiceImpl.class);
	
	@Autowired private PartnerEventService partnerEventService;
	
	@Autowired private RestTemplate restTemplate;
	
	@Autowired private AffiliateConfigService affiliateConfigService;
	
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	
	public void publishPartnerEvent(Integer enrollmentId, Integer householdId) {
		try {
			LOGGER.info("Event Notifications publish event for enrollment id:" + enrollmentId + " Household Id: " + householdId);
			if(enrollmentId != null && householdId != null) {
				Household household = enrollmentExternalRestUtil.getHousehold(householdId);
				if(household != null && household.getEligLead() != null && BooleanUtils.isTrue(household.getEligLead().getDataSharingConsent()) && household.getEligLead().getAffiliateId() != 0L) {
					callAndLogEnrollmentApiDetails(enrollmentId, household);
					Map<PartnerEventDataFieldEnum, String> eventData = new HashMap<>();
					eventData.put(PartnerEventDataFieldEnum.AFFILIATE_ID, String.valueOf(household.getEligLead().getAffiliateId()));
					eventData.put(PartnerEventDataFieldEnum.LEAD_ID, String.valueOf(household.getEligLead().getId()));
					eventData.put(PartnerEventDataFieldEnum.ENROLLMENT_ID, enrollmentId.toString());
					eventData.put(PartnerEventDataFieldEnum.EVENT_NAME, PartnerEventTypeEnum.ENROLL.name());
					LOGGER.info("Event Notifications publish event data: " + eventData);
					partnerEventService.publishEvent(eventData);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error publishing enrollment partner event for enrollment id:" + enrollmentId 
					+ " and householdId: " + householdId, e);
		}
	}

	private void callAndLogEnrollmentApiDetails(Integer enrollmentId, Household household) {
		try {
			Affiliate affiliate = affiliateConfigService.getAffiliate(household.getEligLead().getAffiliateId());
			HttpHeaders headers = new HttpHeaders();
			headers.set("apiKey", affiliate.getApiKey());
			@SuppressWarnings({ "rawtypes", "unchecked" })
			HttpEntity entity = new HttpEntity(headers);
			String enrollmentApiUrl = getEnrollmentApiUrl(affiliate, enrollmentId, household.getEligLead().getId());
			HttpEntity<String> response = restTemplate.exchange(enrollmentApiUrl, HttpMethod.GET, entity, String.class);
			LOGGER.info("Partner Event Notification: Enrollment API Response for url:" + enrollmentApiUrl + " with apiKey:" + affiliate.getApiKey() + ": " + response.getBody());
		} catch (RestClientException e) {
			LOGGER.error("Partner Event Notification: Enrollment API Error", e);
		}catch (Exception e) {
			LOGGER.error("Partner Event Notification: Error ", e);
		}
	}

	private String getEnrollmentApiUrl(Affiliate affiliate, Integer enrollmentId, long eligLeadId) throws Exception {
		if(affiliate.getUrl() !=null &&  !affiliate.getUrl().equalsIgnoreCase("")){
		return "https://" + affiliate.getUrl() + "/ghix-enrollment/api/enrollment/leads/" + eligLeadId + "/enrollments/" + enrollmentId;
		}else{
			throw new Exception(" affiliate Url is null or Empty: Affiliate Id =  " +affiliate.getAffiliateId());
		}
	}

	/*@Override
	public Household getHousehold(Integer householdId) {
		Household household = null;
		if(householdId != null) {
			String getResponse = restTemplate.postForObject(ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, householdId.toString(), String.class, String.class);
			XStream xstream = GhixUtils.getXStreamStaxObject();
			ConsumerResponse consumerResponse = (ConsumerResponse) xstream.fromXML(getResponse);
			if (consumerResponse != null && GhixConstants.RESPONSE_SUCCESS.equals(consumerResponse.getStatus()))
			{
				household = consumerResponse.getHousehold();
			}
		}
		return household;
	}*/

}
