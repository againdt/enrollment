package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentMembersDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.hix.dto.enrollment.ExternalEnrollmentDTO;
import com.getinsured.hix.enrollment.repository.IExternalEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.enrollment.ExternalEnrollment;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("ExternalEnrollmentService")
public class ExternalEnrollmentServiceImpl implements ExternalEnrollmentService {

	@Autowired
	private IExternalEnrollmentRepository externalEnrollmentRepo;
	
	 public static final String INSURANCE_TYPE_DENTAL = "Dental";
     public static final String INSURANCE_TYPE_HEALTH = "Health";
	 private static final Logger LOGGER = Logger.getLogger(ExternalEnrollmentServiceImpl.class);

	@Override
	public List<ExternalEnrollmentDTO> getExternalEnrollmentDataByApplicantExternalId(String applicantExternalId)
			throws GIException {

		List<ExternalEnrollmentDTO> externalEnrollments = new ArrayList<ExternalEnrollmentDTO>();

		ExternalEnrollment externalEnrollment = externalEnrollmentRepo.findByApplicantExternalId(applicantExternalId);

		if (externalEnrollment != null && externalEnrollment.getHealthExchangeAssignedPolicyId() != null) {
			// Prepare Health Enrollment
			ExternalEnrollmentDTO healthEnrollment = new ExternalEnrollmentDTO();
			healthEnrollment.setEnrollmentId(externalEnrollment.getHealthExchangeAssignedPolicyId());
			healthEnrollment.setPlanId(externalEnrollment.getHealthPlanId());
			healthEnrollment.setPlanType(INSURANCE_TYPE_HEALTH);

			externalEnrollments.add(healthEnrollment);
		}
		if (externalEnrollment != null && externalEnrollment.getDentalExchangeAssignedPolicyId() != null) {
			// Prepare Dental Enrollment
			ExternalEnrollmentDTO dentalEnrollment = new ExternalEnrollmentDTO();
			dentalEnrollment.setEnrollmentId(externalEnrollment.getDentalExchangeAssignedPolicyId());
			dentalEnrollment.setPlanId(externalEnrollment.getDentalPlanId());
			dentalEnrollment.setPlanType(INSURANCE_TYPE_DENTAL);
			externalEnrollments.add(dentalEnrollment);
		}
		return externalEnrollments;
	}

	public Map<String, List<EnrollmentWithMemberDataDTO>> getEnrollmentDataForMembers(List<String> externalMemberIdList)
			throws GIException {
		Map<String, List<EnrollmentWithMemberDataDTO>> enrollmentMemberDataDtoMap = new HashMap<String, List<EnrollmentWithMemberDataDTO>>();

		List<EnrollmentWithMemberDataDTO> enrollmentMemberDataDtoList = new ArrayList<EnrollmentWithMemberDataDTO>();

		Map<String, EnrollmentWithMemberDataDTO> enrollmentMap = null;

		List<ExternalEnrollment> enrollmentMemberDataDTOList = externalEnrollmentRepo
				.findByApplicantExternalIdIn(externalMemberIdList);
		
		// Get the list of all enrollmentId's (make sure list doesn't contains nulls)
	    List<String> applicantHealthEnrollmentIds =
                		   enrollmentMemberDataDTOList.stream()
                		   .map(ExternalEnrollment::getHealthExchangeAssignedPolicyId)
                		   .filter(StringUtils::isNotEmpty)
                           .collect(Collectors.toList());
	    LOGGER.debug(applicantHealthEnrollmentIds.toString());     
	    
	    List<String> applicantDetalEnrollmentIds =
     		   enrollmentMemberDataDTOList.stream()
     		   			 .map(ExternalEnrollment::getDentalExchangeAssignedPolicyId)
                         .filter(StringUtils::isNotEmpty)
                         .collect(Collectors.toList());
	    LOGGER.debug(applicantDetalEnrollmentIds.toString());    
		// Get the Enrollments based on  Health Enrollment Id's and Dental EnrollmentId's
		 enrollmentMemberDataDTOList = externalEnrollmentRepo
				.findByhealthExchangeAssignedPolicyIdInOrDentalExchangeAssignedPolicyIdIn(applicantHealthEnrollmentIds,applicantDetalEnrollmentIds);
		 

		if (enrollmentMemberDataDTOList != null && !enrollmentMemberDataDTOList.isEmpty()) {

			enrollmentMap = new LinkedHashMap<String, EnrollmentWithMemberDataDTO>();

			for (ExternalEnrollment enrollmentMemberDataDTO : enrollmentMemberDataDTOList) {

				if (StringUtils.isNotBlank(enrollmentMemberDataDTO.getHealthExchangeAssignedPolicyId())) {
					// Create Health Entry
					EnrollmentMembersDTO member = new EnrollmentMembersDTO("",
							enrollmentMemberDataDTO.getApplicantExternalId(),
							enrollmentMemberDataDTO.getHealthExchangeAssignedPolicyId());
					if (enrollmentMap.containsKey(enrollmentMemberDataDTO.getHealthExchangeAssignedPolicyId()+INSURANCE_TYPE_HEALTH)) {
						enrollmentMap.get(enrollmentMemberDataDTO.getHealthExchangeAssignedPolicyId()+INSURANCE_TYPE_HEALTH).getMembers()
								.add(member);
					} else {
						EnrollmentWithMemberDataDTO enrollmentEntry = new EnrollmentWithMemberDataDTO();
						enrollmentEntry.setEnrollmentID(enrollmentMemberDataDTO.getHealthExchangeAssignedPolicyId());
						enrollmentEntry.setEnrollmentStatus(EnrollmentStatus.CONFIRM.toString());
						enrollmentEntry.setInsuranceType(INSURANCE_TYPE_HEALTH);

						enrollmentEntry.setPlanID(enrollmentMemberDataDTO.getHealthPlanId());
						List<EnrollmentMembersDTO> memberListDto = new ArrayList<EnrollmentMembersDTO>();
						memberListDto.add(member);
						enrollmentEntry.setMembers(memberListDto);

						enrollmentMap.put(enrollmentMemberDataDTO.getHealthExchangeAssignedPolicyId()+INSURANCE_TYPE_HEALTH, enrollmentEntry);

					}

				}
				if (StringUtils.isNotBlank(enrollmentMemberDataDTO.getDentalExchangeAssignedPolicyId())) {
					EnrollmentMembersDTO member = new EnrollmentMembersDTO("",
							enrollmentMemberDataDTO.getApplicantExternalId(),
							enrollmentMemberDataDTO.getDentalExchangeAssignedPolicyId());
					if (enrollmentMap.containsKey(enrollmentMemberDataDTO.getDentalExchangeAssignedPolicyId()+INSURANCE_TYPE_DENTAL)) {
						enrollmentMap.get(enrollmentMemberDataDTO.getDentalExchangeAssignedPolicyId()+INSURANCE_TYPE_DENTAL).getMembers()
								.add(member);
					} else {
						EnrollmentWithMemberDataDTO enrollmentEntry = new EnrollmentWithMemberDataDTO();
						enrollmentEntry.setEnrollmentID(enrollmentMemberDataDTO.getDentalExchangeAssignedPolicyId());
						enrollmentEntry.setEnrollmentStatus(EnrollmentStatus.CONFIRM.toString());
						enrollmentEntry.setInsuranceType(INSURANCE_TYPE_DENTAL);

						enrollmentEntry.setPlanID(enrollmentMemberDataDTO.getDentalPlanId());
						List<EnrollmentMembersDTO> memberListDto = new ArrayList<EnrollmentMembersDTO>();
						memberListDto.add(member);
						enrollmentEntry.setMembers(memberListDto);

						enrollmentMap.put(enrollmentMemberDataDTO.getDentalExchangeAssignedPolicyId()+INSURANCE_TYPE_DENTAL, enrollmentEntry);

					}
				}
			}

			enrollmentMemberDataDtoList = new ArrayList(enrollmentMap.values());
		}
		enrollmentMemberDataDtoMap.put("enrollments", enrollmentMemberDataDtoList);
		return enrollmentMemberDataDtoMap;

	}

	@Override
	public ExternalEnrollment saveExternalEnrollment(ExternalEnrollment enrollment) {
		return externalEnrollmentRepo.save(enrollment);
	}

}
