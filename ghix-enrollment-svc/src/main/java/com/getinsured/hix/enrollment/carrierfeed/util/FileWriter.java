package com.getinsured.hix.enrollment.carrierfeed.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jabelardo on 7/9/14.
 */
public abstract class FileWriter<T> {

    private String dataFilename;
    private String templateFilename;
    private JsonNode mappings;

    public void initialize(String dataFilename, String mappingsFilename, String templateFilename) throws IOException {
        this.dataFilename = dataFilename;
        this.templateFilename = templateFilename;
        ObjectMapper mapper = new ObjectMapper();
        mappings = mapper.readTree(new File(mappingsFilename));
    }

    protected abstract void write(OutputStream outputStream, Iterator<T> inputIterator) throws IOException;

    public void export(Iterator<T> inputIterator) throws IOException {

        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(dataFilename);
            write(outputStream, inputIterator);
            outputStream.flush();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    protected boolean hasTemplate() {
        return templateFilename != null;
    }

    protected String getTemplateFilename() {
        return templateFilename;
    }

    public void exportAll(List<T> inputList) throws IOException {
        Iterator<T> inputIterator = inputList.iterator();
        export(inputIterator);
    }

    public JsonNode getMappings() {
        return mappings;
    }
}
