/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * @author raja
 * @since 10/05/2013
 * This method prepares the update the input Enrollment object with received Household data.
 * 
 * @param adminUpdateIndividualRequest
 * @param enrollment
 * @return
 * @throws GIException
 */
public interface AdminUpdateService {
	Enrollment adminUpdateEnrollment(AdminUpdateIndividualRequest adminUpdateIndividualRequest, Enrollment enrollment,AccountUser user,  boolean isInternal)throws GIException;
	void adminUpdateService(AdminUpdateIndividualRequest adminUpdateIndividualRequest, Long updatedLocationId, Boolean isLocationIdUpdated, Boolean isOnlySsapAppIdUpdated, EnrollmentResponse enrlResp) throws GIException;
	String updateAddressTwo(Long householdCaseId, List<SsapApplicant> ssapApplicantList);
	public String updateExternalAppID(Long ssapApplicationId, List<String> issuerIds);
}
