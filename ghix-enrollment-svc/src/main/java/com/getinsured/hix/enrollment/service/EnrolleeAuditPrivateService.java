/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.text.ParseException;
import java.util.Date;

import org.springframework.data.history.Revision;

import com.getinsured.hix.model.enrollment.Enrollee;



/**
 * @author liu_j
 * @since 23/04/2013
 */
public interface EnrolleeAuditPrivateService {
	Revision<Integer, Enrollee> findRevisionByDate( int enrolleeId, Date selectDate) throws ParseException;

}
