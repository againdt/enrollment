/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
/*import org.hibernate.tool.hbm2x.StringUtils;*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrolleeDetailsDto;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberRequestDTO;
import com.getinsured.hix.dto.enrollment.QhpReportDTO;
import com.getinsured.hix.dto.enrollment.SendUpdatedEnrolleeResponseDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeUpdateSendStatusRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.repository.IQhpReportRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.EnrolleeUpdateSendStatus;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.QhpReport;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.timeshift.util.TSDate;
import com.thoughtworks.xstream.XStream;

/**
 * @author panda_p
 *
 */

@Service("enrolleeService")
@Transactional
public class EnrolleeServiceImpl implements EnrolleeService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrolleeServiceImpl.class);
	@Autowired private IEnrolleeUpdateSendStatusRepository enrolleeUpdateSendStatusRepository;
	@Autowired private IEnrolleeRepository enrolleeRepository;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private UserService userService;
	@Autowired private LookupService lookupService;
	@Autowired private IEnrolleeAudRepository enrolleeAudRepository;
	@Autowired private IEnrollmentEventRepository enrollmentEventRepository;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IQhpReportRepository iQhpReportRepository;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;

	private static final String SUBSCRIBER_ENROLLMENT_QUERY = "SELECT en.ID, en.ISSUER_ID, en.CMS_PLAN_ID, en.PLAN_NAME, en.PLAN_LEVEL, en.INSURER_NAME, "
			+ " en.BENEFIT_EFFECTIVE_DATE, en.BENEFIT_END_DATE, enrollmentStatusLkp.LOOKUP_VALUE_CODE, enrollmentStatusLkp.LOOKUP_VALUE_LABEL, "
			+ " ee.FIRST_NAME, ee.MIDDLE_NAME, ee.LAST_NAME, ee.FIRST_NAME || ' ' || ee.MIDDLE_NAME || ' ' || ee.LAST_NAME as FULLNAME, ee.BIRTH_DATE, SUBSTR(ee.TAX_ID_NUMBER, 6) as SSN,"
			+ " en.EXCHG_SUBSCRIBER_IDENTIFIER, TO_CHAR(en.BENEFIT_EFFECTIVE_DATE, 'YYYY'),  insuranceTypeLkp.LOOKUP_VALUE_CODE as INS_LOOKUP_VALUE_CODE, insuranceTypeLkp.LOOKUP_VALUE_LABEL as INS_LOOKUP_VALUE_LABEL "
			+ " FROM ENROLLMENT en, ENROLLEE ee, LOOKUP_VALUE enrollmentStatusLkp, LOOKUP_VALUE insuranceTypeLkp "
			+ " WHERE en.ID = ee.ENROLLMENT_ID "
			+ " AND en.ENROLLMENT_STATUS_LKP = enrollmentStatusLkp.LOOKUP_VALUE_ID "
			+ " AND en.INSURANCE_TYPE_LKP = insuranceTypeLkp.LOOKUP_VALUE_ID ";
	
	//@Value("#{configProp['enrollment.sendCarrierUpdatedDataUrl']}")
	//private String sendCarrierUpdatedDataUrl;

	
	@Override
	@Transactional(readOnly = true)
	public List<Enrollee> getCarrierUpdatedEnrollments() {
		Date lastRunDate=getLastRunDate(EnrollmentConstants.SEND_CARRIER_UPDATED_ENROLLMENT_JOB);
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
		LOGGER.info("userService: "+user);
		LOGGER.info("lastRunDate: "+lastRunDate);
		if(lastRunDate!=null){
			return enrolleeRepository.getCarrierUpdatedEnrollments(lastRunDate, user.getId());
		}else{
			return enrolleeRepository.getCarrierUpdatedEnrollments(user.getId());
		}

	}

	/**
	 * @author panda_p
	 * 
	 * @param jobName
	 * @return lastRunDate
	 */
	private Date getLastRunDate(String jobName){
		Date lastRunDate=null;
		if(jobName!=null){
			lastRunDate = batchJobExecutionService.getLastJobRunDate(jobName);
		}

		return lastRunDate;
	}

	/**
	 * 
	 */
	@Override	
	@Transactional
	public void saveCarrierUpdatedDataResponse(List<SendUpdatedEnrolleeResponseDTO> inputVal){

		LOGGER.info(" inside saveCarrierUpdatedDataResponse  : inputVal =  " +inputVal);
		for(SendUpdatedEnrolleeResponseDTO sendUpdatedEnrolleeResponseDTO: inputVal){
			//SendUpdatedEnrolleeResponseDTO sendUpdatedEnrolleeResponseDTO  = (SendUpdatedEnrolleeResponseDTO) objPlan;
			EnrolleeUpdateSendStatus enrolleeUpdateSendStatus = new EnrolleeUpdateSendStatus();

			enrolleeUpdateSendStatus.setAhbxStatusCode(sendUpdatedEnrolleeResponseDTO.getAhbxStatusCode());
			enrolleeUpdateSendStatus.setAhbxStatusDesc(sendUpdatedEnrolleeResponseDTO.getAhbxStatusDescription());
			enrolleeUpdateSendStatus.setExchgIndivIdentifier(sendUpdatedEnrolleeResponseDTO.getExchgIndivIdentifier());
			enrolleeUpdateSendStatus.setEnrolleeStatus(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, sendUpdatedEnrolleeResponseDTO.getEnrolleeStatus()));
			if(sendUpdatedEnrolleeResponseDTO.getEnrolleeId()!=null){
				Enrollee en = new Enrollee();
				en.setId(Integer.parseInt(sendUpdatedEnrolleeResponseDTO.getEnrolleeId()));
				enrolleeUpdateSendStatus.setEnrollee(en);
			}
			try{
				enrolleeUpdateSendStatusRepository.save(enrolleeUpdateSendStatus);
			}catch(Exception e){
				LOGGER.error("Exception occured while saving the response for IND21 from AHBX ");
				LOGGER.error("Exception occured while saving the response for IND21 from AHBX" , e );
			}
		}
	}

	
	@Override	
	@Transactional(readOnly=true)
	public Enrollee findSubscriberbyEnrollmentIDId(Integer enrollmentID) {	
		
			return enrolleeRepository.findSubscriberByEnrollmentID(enrollmentID);
	}
	
	@Override	
	@Transactional(readOnly=true)
	public Integer findSubscriberIdByEnrollmentId(Integer enrollmentID) {	
		return enrolleeRepository.findSubscriberIdByEnrollmentId(enrollmentID);
	}
	

	@Override	
	@Transactional(readOnly=true)
	public Enrollee findById(Integer id) {		
//		return enrolleeRepository.findOne(id);
		return enrolleeRepository.findById(id); //Tenant Aware Implementation	
	}
	
	@Override
	@Transactional(readOnly=true)
	public Map<String, Object> searchEnrollee(Map<String, Object> searchCriteria) {
		
		boolean responseAsPage = true;
		
		if(isNotNullAndEmpty(searchCriteria.get("pageable"))){
			responseAsPage = Boolean.valueOf((String)searchCriteria.get("pageable"));
		}
		
		Pageable pageable = null;
		
		if(responseAsPage){
			int pageNumber = (searchCriteria.get(EnrollmentConstants.PAGE_NUMBER)!=null) ?
					          Integer.parseInt(searchCriteria.get(EnrollmentConstants.PAGE_NUMBER).toString()):
					          EnrollmentConstants.DEFAULT_PAGE_NUMBER;
			int pageSize = (searchCriteria.get(EnrollmentConstants.PAGE_SIZE)!=null) ?
					       Integer.parseInt(searchCriteria.get(EnrollmentConstants.PAGE_SIZE).toString()):
						   GhixConstants.PAGE_SIZE;
					       if (pageNumber < 1) {
								pageNumber = 1;
							}
			
	        Sort sortObject = null;
			String sortBy = (searchCriteria.get(EnrollmentConstants.SORT_BY)!=null) ?
					                searchCriteria.get(EnrollmentConstants.SORT_BY).toString() :
					                EnrollmentConstants.DEFAULT_SORT_COLUMN;
			String sortOrder = (searchCriteria.get(EnrollmentConstants.SORT_ORDER)!=null) ? 
					                searchCriteria.get(EnrollmentConstants.SORT_ORDER).toString(): 
					                EnrollmentConstants.DEFAULT_SORT_ORDER;

			Sort.Direction direction = null;
			if(sortOrder.equalsIgnoreCase(EnrollmentConstants.DEFAULT_SORT_ORDER)){
				direction = Sort.Direction.ASC;
			}else{
				direction = Sort.Direction.DESC;
			}

			sortObject = new Sort(new Sort.Order(direction, sortBy));					       

		   pageable = new PageRequest(pageNumber - 1, pageSize, sortObject);					       
		}
		
		String policynumber = null;
		String statusLkp = null;
		List<String> statusLkpList = new ArrayList<>();
		String plantype = null;
		String cmsPlanId = null;
		int issuer = 0;
		String name = null;
		String enrollmentTypeLkp = null;
		String insuranceType = null;
		String employerName = null;
		 
		//HIX-93635 Only For ID
		String firstName = null; 
		String lastName = null;  
		String subscriberId = null;  
		String last4DigitSSN = null;  
		String dOBofSubscriber = null;  
		String applicableYear = null;
		boolean isSubscriber = false;
		
		int policyId = 0;

		if(isNotNullAndEmpty(searchCriteria.get("policynumber"))){
			policynumber = (String)searchCriteria.get("policynumber");
		}
		
		if(isNotNullAndEmpty(searchCriteria.get(EnrollmentConstants.STATUS))){
			statusLkp = (String)searchCriteria.get(EnrollmentConstants.STATUS);
			statusLkpList.add(statusLkp);
		}
		
		if(statusLkpList.size() < 1){
			statusLkpList = Arrays.asList("PENDING", "CONFIRM", "PAYMENT_RECEIVED", "TERM");
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("plantype"))){
			plantype = (String)searchCriteria.get("plantype");
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("plannumber"))){
			cmsPlanId = (String)searchCriteria.get("plannumber");
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("issuer"))){
			issuer = Integer.parseInt(searchCriteria.get("issuer").toString());
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("name"))){
			name = (String)searchCriteria.get("name");
			name = name.trim();
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("lookup"))){ 
			enrollmentTypeLkp = (String)searchCriteria.get("lookup");			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("insuranceType"))){ 
			insuranceType = (String)searchCriteria.get("insuranceType");			
		}
		
		if(null != enrollmentTypeLkp && enrollmentTypeLkp.equals(EnrollmentConstants.ENROLLMENT_TYPE_SHOP) && isNotNullAndEmpty(searchCriteria.get("employerName"))){ 
			employerName = (String)searchCriteria.get("employerName");			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("firstName"))){ 
			firstName = (String)searchCriteria.get("firstName");			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("lastName"))){ 
			lastName = (String)searchCriteria.get("lastName");			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("subscriberId"))){ 
			subscriberId = (String)searchCriteria.get("subscriberId");			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("last4DigitSSN"))){ 
			last4DigitSSN = (String)searchCriteria.get("last4DigitSSN");			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("dOBofSubscriber"))){ 
			dOBofSubscriber = (String)searchCriteria.get("dOBofSubscriber");			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("applicableYear"))){ 
			applicableYear = (String)searchCriteria.get("applicableYear");			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("policyId"))){ 
			policyId = Integer.parseInt(searchCriteria.get("policyId").toString());			
		}
		
		if(isNotNullAndEmpty(searchCriteria.get("isSubscriber"))){ 
			String isSubStr = (String)searchCriteria.get("isSubscriber");
			if(isSubStr.equalsIgnoreCase("true")){
				isSubscriber = Boolean.valueOf(isSubStr);
			}
		}
		
		
		if(name == null){
			if(firstName!= null ){
				name = firstName;
			}
			
			if(lastName!= null ){
				name = lastName;
			}
			
			if(firstName!= null  && lastName!= null){
				name = firstName+" "+lastName;
			}
			
			if(name != null){
				name = name.trim();
			}
			
		}

		Page<Object[]> data = null;
		List<Map<String, Object>> dataset = null;

		if((EnrollmentConstants.ENROLLMENT_TYPE_SHOP).equals(enrollmentTypeLkp)){
				
			if(isSubscriber){
				
				data = enrolleeRepository.getShopSubscriberByIssuerData(policynumber, statusLkpList, plantype, 
						cmsPlanId, issuer, employerName, 
                        name, enrollmentTypeLkp,
                        subscriberId, last4DigitSSN,
                        dOBofSubscriber, applicableYear, policyId, pageable);
					
				}else{
					
				data = enrolleeRepository.getShopEnrolleeByIssuerData(policynumber, statusLkpList, plantype, 
						cmsPlanId, issuer, employerName, 
					                                              name, enrollmentTypeLkp,
					                                              subscriberId, last4DigitSSN,
					                                              dOBofSubscriber, applicableYear, policyId, pageable);
				}
			
			dataset = getShopDataSet(data);
		}

		if((EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL).equals(enrollmentTypeLkp)){
			
			if(isSubscriber){
				
				data = enrolleeRepository.getIndividualSubscriberByIssuerData(policynumber, statusLkpList, plantype, 
						cmsPlanId, issuer, name, 
                        enrollmentTypeLkp,
                        subscriberId, last4DigitSSN,
                        dOBofSubscriber, applicableYear, policyId, insuranceType, pageable);
				
			}else{
				
				data = enrolleeRepository.getIndividualEnrolleeByIssuerData(policynumber, statusLkpList, plantype, 
						cmsPlanId, issuer, name, 
					                                                    enrollmentTypeLkp,
							                                            subscriberId, last4DigitSSN,
                        dOBofSubscriber, applicableYear, policyId, insuranceType, pageable);
			}
			
			dataset = getIndividualDataSet(data);
			
		}


		Map<String,Object> enroleesAndRecordCount = new HashMap<String,Object>();
		enroleesAndRecordCount.put("enroleelist", dataset);
		if(null != data){
			enroleesAndRecordCount.put("recordCount", data.getTotalElements());
		}

		return enroleesAndRecordCount;
	}

	private List<Map<String, Object>> getShopDataSet(Page<Object[]> data) {
		List<Map<String, Object>> dataset = new ArrayList<Map<String, Object>>();
		
		List<String> selectColumns = getShopColumnList();
		
		String columnName = null;
		for (Object[] tuple : data) {
			Map<String, Object> temp = new HashMap<String, Object>();
			
			for (int i = 0; i < selectColumns.size(); i++) {
				columnName = selectColumns.get(i);
				
				if (columnName.contains(".")) {
					columnName = columnName.replace(".", "");
				}
				
				temp.put(columnName, tuple[i]);
			}
			dataset.add(temp);
		}
		return dataset;
	}
	
	private List<Map<String, Object>> getIndividualDataSet(Page<Object[]> data) {
		List<Map<String, Object>> dataset = new ArrayList<Map<String, Object>>();
		
		List<String> selectColumns = getIndividualColumnList();
		
		String columnName = null;
		for (Object[] tuple : data) {
			Map<String, Object> temp = new HashMap<String, Object>();
			
			for (int i = 0; i < selectColumns.size(); i++) {
				columnName = selectColumns.get(i);
				
				if (columnName.contains(".")) {
					columnName = columnName.replace(".", "");
				}
				
				temp.put(columnName, tuple[i]);
			}
			dataset.add(temp);
		}
		return dataset;
	}

	private List<String> getShopColumnList() {
		List<String> selectColumns = new ArrayList<String>();
		selectColumns.add(EnrollmentConstants.ID);
		selectColumns.add(EnrollmentConstants.HEALTH_COVERAGE_POLICY_NO);
		selectColumns.add("enrollment.enrollmentStatusLkp.lookupValueCode");
		selectColumns.add("enrollment.enrollmentStatusLkp.lookupValueLabel");
		selectColumns.add("enrollment.insuranceTypeLkp.lookupValueCode");
		selectColumns.add("enrollment.enrollmentTypeLkp.lookupValueCode");
		selectColumns.add("enrollment.planName");
		selectColumns.add("enrollment.CMSPlanID");
		selectColumns.add("enrollment.insurerName");
		selectColumns.add(EnrollmentConstants.FIRST_NAME);
		selectColumns.add(EnrollmentConstants.MIDDLE_NAME);
		selectColumns.add(EnrollmentConstants.LAST_NAME);
		selectColumns.add("enrlee.birthDate");
		selectColumns.add("ssn");
		selectColumns.add("subscriberId");
		selectColumns.add("policy_id");
		selectColumns.add(EnrollmentConstants.EFFECTIVE_START_DATE);
		selectColumns.add("enrollment.employer.id");
		selectColumns.add("enrollment.employer.name");
		selectColumns.add("enrollment.benefitEffectiveDate");
		selectColumns.add("enrollment.benefitEndDate");		
		return selectColumns;
	}
	
	private List<String> getIndividualColumnList() {
		List<String> selectColumns = new ArrayList<String>();
		selectColumns.add(EnrollmentConstants.ID);
		selectColumns.add(EnrollmentConstants.HEALTH_COVERAGE_POLICY_NO);
		selectColumns.add("enrollment.enrollmentStatusLkp.lookupValueCode");
		selectColumns.add("enrollment.enrollmentStatusLkp.lookupValueLabel");
		selectColumns.add("enrollment.insuranceTypeLkp.lookupValueLabel");
		selectColumns.add("enrollment.enrollmentTypeLkp.lookupValueCode");
		selectColumns.add("enrollment.planName");
		selectColumns.add("enrollment.CMSPlanID");
		selectColumns.add("enrollment.insurerName");
		selectColumns.add(EnrollmentConstants.FIRST_NAME);
		selectColumns.add(EnrollmentConstants.MIDDLE_NAME);
		selectColumns.add(EnrollmentConstants.LAST_NAME);
		selectColumns.add("enrlee.birthDate");
		selectColumns.add("ssn");
		selectColumns.add("subscriberId");
		selectColumns.add("policy_id");
		selectColumns.add(EnrollmentConstants.EFFECTIVE_START_DATE);
		selectColumns.add("enrollment.benefitEffectiveDate");
		selectColumns.add("enrollment.benefitEndDate");		
		return selectColumns;
	}


	/**
	 * @since
	 * @author parhi_s
	 * 
	 * This method is used to find Enrollee by passing parameters memberID, enrollmentID, planID
	 */
	@Override
	public List<Enrollee> findEnrolleeByEnrollmentIDAndMemberIDAndPlanID(Integer enrollmentID,
			Integer planID, String memberID) throws GIException  {
		List<Enrollee> enrolleeList= new ArrayList<Enrollee>();
		try{
			enrolleeList=enrolleeRepository.findEnrolleeByEnrollmentIDAndMemberIDAndPlanID(memberID, enrollmentID, planID);
		}
		catch(Exception e){
			LOGGER.error("Error in findEnrolleeByOrderIDAndMemberIDAndPlanID() == "+e.getMessage());
			LOGGER.error("Error in findEnrolleeByOrderIDAndMemberIDAndPlanID : "+ e);
			throw new GIException("Error in findEnrolleeByOrderIDAndMemberIDAndPlanID() == "+e.getMessage(),e);
		}
		return enrolleeList;
	}
	/**
	 * @since
	 * @author parhi_s
	 * This method is used to Update Enrollee
	 */

	@Override
	public void updateEnrollee(Enrollee enrollee) throws GIException {
		try{
			enrolleeRepository.saveAndFlush(enrollee);
		}catch(Exception e){
			LOGGER.error("Error in updateEnrollee() == "+e.getMessage());
			LOGGER.error("Exception in updateEnrollee : "+ e);
			throw new GIException("Error in updateEnrollee() == "+e.getMessage(),e);
		}
	}

	/**
	 * @since
	 * @author parhi_s
	 * 
	 * This method is used to find Enrollee By passing parameters memberID and enrollmentID
	 */

	@Override
	public List<Enrollee> findEnrolleeByEnrollmentIDAndMemberID(
			String memberID, Integer enrollmentID) throws GIException{
		try{
			return enrolleeRepository.findEnrolleeByEnrollmentIDAndMemberID(memberID, enrollmentID);
		}
		catch(Exception e){
			LOGGER.error("Error in findEnrolleeByEnrollmentIDAndMemberID() == "+e.getMessage());
			LOGGER.error("Exception in findEnrolleeByEnrollmentIDAndMemberID : "+ e);
			throw new GIException("Error in findEnrolleeByEnrollmentIDAndMemberID() == "+e.getMessage(),e);
		}

	}

	/**
	 * @author panda_p
	 * 
	 * @param exchgIndivIdentifier
	 * @return
	 */
	@Override
	public Enrollee getEnrolleeByExchgIndivIdentifier(String exchgIndivIdentifier){
		return enrolleeRepository.getEnrolleeByExchgIndivIdentifier(exchgIndivIdentifier);
	}

	/* (non-Javadoc)
	 * @author parhi_s
	 * @param enrollmentID
	 * @param statusID
	 * @see com.getinsured.hix.enrollment.service.EnrolleeService#getEnrolleeByStatusAndEnrollmentID(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Enrollee> getEnrolleeByStatusAndEnrollmentID(Integer enrollmentID, String status) throws GIException {
		try{
			return enrolleeRepository.getEnrolleeByStatusAndEnrollmentID(enrollmentID, status);
		}catch(Exception e){
			LOGGER.error("Error in  getEnrolleeByStatusAndEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception getEnrolleeByStatusAndEnrollmentID : "+ e);
			throw new GIException("Error in  getEnrolleeByStatusAndEnrollmentID() == "+e.getMessage(),e);
		}

	}

	/* (non-Javadoc)
	 * @author priya
	 * @param enrollmentID
	 */
	@Override
	public List<Enrollee> getEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException {

		try{
			return enrolleeRepository.getEnrolleeByEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error("Error in  getEnrolleeByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  getEnrolleeByEnrollmentID : "+ e);
			throw new GIException("Error in  getEnrolleeByEnrollmentID() == "+e.getMessage(),e);
		}

	}
	
	@Override
	public List<Enrollee> getEnrolledEnrolleesForEnrollmentID(Integer enrollmentID)throws GIException {
		try{
			List<Enrollee> enrolleeList = enrolleeRepository.getEnrolledEnrolleesForEnrollmentID(enrollmentID);
			
			List<Enrollee> sortedEnrolleeList = new ArrayList<Enrollee>();
			
			for (Enrollee enrollee : enrolleeList) {
				if(enrollee.getRelationshipToHCPLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.RELATIONSHIP_SELF_CODE)){
					sortedEnrolleeList.add(enrollee);
					break;
				}
			}
			
			for (Enrollee enrollee : enrolleeList) {
				if(enrollee.getRelationshipToHCPLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.RELATIONSHIP_SPOUSE_CODE)){
					sortedEnrolleeList.add(enrollee);
					break;
				}
			}
			
			for (Enrollee enrollee : enrolleeList) {
				if(enrollee.getRelationshipToHCPLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.RELATIONSHIP_CHILD_CODE)){
					sortedEnrolleeList.add(enrollee);
				}
			}
			
			// add any other remaining enrollee to sorted list
			String relationCode = null;
			for (Enrollee enrollee : enrolleeList) {
				relationCode = enrollee.getRelationshipToHCPLkp().getLookupValueCode();
				
				if(!relationCode.equalsIgnoreCase(EnrollmentConstants.RELATIONSHIP_SELF_CODE) && !relationCode.equalsIgnoreCase(EnrollmentConstants.RELATIONSHIP_SPOUSE_CODE) && !relationCode.equalsIgnoreCase(EnrollmentConstants.RELATIONSHIP_CHILD_CODE)){
					sortedEnrolleeList.add(enrollee);
				}
			}

			return sortedEnrolleeList;
		}catch(Exception e){
			LOGGER.error("Error in  getEnrolleeByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  getEnrolleeByEnrollmentID : "+ e);
			throw new GIException("Error in  getEnrolleeByEnrollmentID() == "+e.getMessage(),e);
		}

	}
	
	/* (non-Javadoc)
	 * @author parhi_s
	 * @param enrollmentID
	 */
	@Override
	public List<Enrollee> findEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException {
		try{
			return enrolleeRepository.findEnrolleesByEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error(EnrollmentConstants.ERR_MSG_LOGGER+e.getMessage());
			LOGGER.error(EnrollmentConstants.ERR_MSG_LOGGER+ e);
			throw new GIException(EnrollmentConstants.ERR_MSG_LOGGER+e.getMessage(),e);
		}

	}
	
	@Override
	public List<Enrollee> findEnrolledEnrolleeByEnrollmentID(Integer enrollmentID)throws GIException {
		try{
			return enrolleeRepository.getEnrolledEnrolleesForEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error(EnrollmentConstants.ERR_MSG_LOGGER+e.getMessage());
			LOGGER.error(EnrollmentConstants.ERR_MSG_LOGGER+ e);
			throw new GIException(EnrollmentConstants.ERR_MSG_LOGGER+e.getMessage(),e);
		}

	}

	/* (non-Javadoc)
	 * @author priya
	 * @param enrollmentID
	 */
	@Override
	public Enrollee getHouseholdContactByEnrollmentID(Integer enrollmentID)throws GIException{
		try{
			return enrolleeRepository.getHouseholdContactByEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error("Error in  getHouseholdContactByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  getHouseholdContactByEnrollmentID : "+ e);
			throw new GIException("Error in  getHouseholdContactByEnrollmentID() == "+e.getMessage(),e);
		}

	}

	/* (non-Javadoc)
	 * @author priya
	 * @param enrollmentID
	 */
	@Override
	public Enrollee getResponsiblePersonByEnrollmentID(Integer enrollmentID)throws GIException{
		try{
			return enrolleeRepository.getResponsiblePersonByEnrollmentID(enrollmentID);
		}catch(Exception e){
			LOGGER.error("Error in  getResponsiblePersonByEnrollmentID() == "+e.getMessage());
			LOGGER.error("Exception in  getResponsiblePersonByEnrollmentID : "+ e);
			throw new GIException("Error in  getResponsiblePersonByEnrollmentID() == "+e.getMessage(),e);
		}

	}
	

	@Override
	public List<Enrollment> getEnrollmentEmployeeIdAndEnrollmentStatus(Integer employeeId, Long employeeAppId,List<String> enrollmentStatus) throws GIException {
	
	 return enrolleeRepository.getEnrollmentEmployeeIdAndEnrollmentStatus(employeeId, employeeAppId,enrollmentStatus, new TSDate());
	}
	
	@Override
	public List<Enrollment> getEnrollmentBySsapApplictionIdAndEnrollmentStatus(Long ssapAppId,List<String> enrollmentStatus) throws GIException {
	
	 return enrolleeRepository.getEnrollmentBySsapApplictionIdAndEnrollmentStatus(ssapAppId);
	}

	@Override
	public List<Enrollee> getSpecialEnrolleeByEnrollmentID(Integer enrollmentID)
			throws GIException {
		try {
			return enrolleeRepository.getSpecialEnrolleeByEnrollmentID(enrollmentID);
			
		} catch (Exception e) {
			LOGGER.error(EnrollmentConstants.ERR_MSG_LOGGER+ e.getMessage());
			throw new GIException(EnrollmentConstants.ERR_MSG_LOGGER+ e.getMessage(), e);
		}
	}
	@Override
	public List<Enrollment> findActiveEnrollmentBySSNAndCoverageDates(String ssn,Date coverateStartDate,Date coverageEndDate){
		return enrolleeRepository.findActiveEnrollmentBySSNAndCoverageDates(ssn,coverateStartDate,coverageEndDate);
	}
	@Override
	public List<Enrollment> findActiveEnrollmentBySSN(String ssn){
		return enrolleeRepository.findActiveEnrollmentBySSN(ssn);
	}
	
	@Override
	public List<Enrollee> getSpecialEnrolleeByEnrollmentID(Map<String, Object> inputval, Integer enrollmentID)throws GIException {
		
		Date currentDate = null;
		if (isNotNullAndEmpty(inputval.get(EnrollmentConstants.CURRENT_DATE))) {
			currentDate = DateUtil.StringToDate(inputval.get(EnrollmentConstants.CURRENT_DATE).toString(), GhixConstants.REQUIRED_DATE_FORMAT);
		}
		
		
		Date lastInvoiceTimestamp = null;
		if(isNotNullAndEmpty(inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP))){
			lastInvoiceTimestamp =  DateUtil.StringToDate((String) inputval.get(EnrollmentConstants.LAST_INVOICE_TIMESTAMP), EnrollmentConstants.FINANCE_DATE_FORMAT); 
		}
		try {
			return enrolleeRepository.getSpecialEnrolleeByEnrollmentID(currentDate, lastInvoiceTimestamp,enrollmentID);
			
		} catch (Exception e) {
			LOGGER.error(EnrollmentConstants.ERR_MSG_LOGGER+ e.getMessage());
			throw new GIException(EnrollmentConstants.ERR_MSG_LOGGER+ e.getMessage(), e);
		}
	}
	
	@Override
	public List<Enrollee> getEnrolleeExchgIndivIdentifierAndCoverageStartDateByEnrollmentId(Integer enrollmentId) {
		
		List<Object[]> enrolleeList = enrolleeRepository.getExchgIndivIdentifierAndEffectiveStartDateByEnrollmentID(enrollmentId);
		List<Enrollee> responseEnrolleeList = new ArrayList<Enrollee>();
		Enrollee responseEnrollee;
		
		for (Object[] enrollColData : enrolleeList) {
			
			responseEnrollee = new Enrollee();
			responseEnrollee.setId((Integer)enrollColData[0]);
			responseEnrollee.setExchgIndivIdentifier((String)enrollColData[1]);
			responseEnrollee.setEffectiveStartDate((Date)enrollColData[2]);
			if(enrollColData.length>3){//
				responseEnrollee.setQuotingDate((Date)enrollColData[3]);
			}
			//HIX-105005, HIX-105932
			if((enrollColData.length>4)&&
					Boolean.parseBoolean(
							DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGE_BASED_REQUOTING))) {
				responseEnrollee.setAge((Integer)enrollColData[4]);
			}
			
			responseEnrolleeList.add(responseEnrollee);
		}
		
		return responseEnrolleeList;
	}
	
	@Override
	public Enrollment getHealthEnrollmentEmployeeIdAndEnrollmentStatus(Integer employeeId,Long employeeAppId,List<String> status){
		return enrolleeRepository.getHealthEnrollmentEmployeeIdAndEnrollmentStatus(employeeId,employeeAppId,status);
	}
	@Override
	public Enrollment getDentalEnrollmentEmployeeIdAndEnrollmentStatus(Integer employeeId,Long employeeAppId,List<String> status){
		return enrolleeRepository.getDentalEnrollmentEmployeeIdAndEnrollmentStatus(employeeId,employeeAppId,status);
	}
	
	
	/**
	 * 
	 * @author Aditya_S
	 * 
	 * HIX-66734
	 * Returns Enrollee Aud information for given Enrollee Id and Aud record before Last Invoice Timestamp.
	 * 
	 * @param enrolleeRequestStr
	 * @return
	 */
	@Override
	public String getEnrolleeCoverageDetailsBeforeLastInvoice(String enrolleeRequestStr){
		LOGGER.info("*************** Inside getEnrolleeCoverageDetailsBeforeLastInvoice ***************");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrolleeRequest enrolleeRequest = (EnrolleeRequest) xstream.fromXML(enrolleeRequestStr);
		
		EnrolleeResponse enrolleeResponse = new EnrolleeResponse();
		
		if(enrolleeRequest != null){
			if(enrolleeRequest.getEnrolleeId() == null || enrolleeRequest.getEnrolleeId() <= 0){
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrolleeResponse.setErrMsg("Invalid Enrollee Id :: " + enrolleeRequest.getEnrolleeId());
				LOGGER.error("Invalid Enrollee Id :: " + enrolleeRequest.getEnrolleeId());
			}else if(enrolleeRequest.getLastInvoiceTimestamp() == null){
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
				enrolleeResponse.setErrMsg("Invalid LastInvoiceTime");
				LOGGER.error("Invalid LastInvoiceTime");
			}else{
				EnrolleeAud enrolleeAudRecord = enrolleeAudRepository.getEnrolleeCoverageDetailsBeforeLastInvoice(enrolleeRequest.getEnrolleeId(), enrolleeRequest.getLastInvoiceTimestamp());
				if(enrolleeAudRecord != null){
					EnrolleeDetailsDto enrolleeDetailsDtoObj = new EnrolleeDetailsDto();
					enrolleeDetailsDtoObj.setEnrolleeId(enrolleeAudRecord.getId());
					enrolleeDetailsDtoObj.setEffectiveStartDate(enrolleeAudRecord.getEffectiveStartDate());
					enrolleeDetailsDtoObj.setEffectiveEndDate(enrolleeAudRecord.getEffectiveEndDate());
					enrolleeDetailsDtoObj.setTotalIndividualResponsibilityAmount(enrolleeAudRecord.getTotalIndvResponsibilityAmt());
					if(enrolleeAudRecord.getEnrolleeLkpValue() != null){
						enrolleeDetailsDtoObj.setEnrolleeStatus(enrolleeAudRecord.getEnrolleeLkpValue().getLookupValueLabel());
					}
					enrolleeResponse.setEnrolleeDetailsDto(enrolleeDetailsDtoObj);
					enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				}else{
					enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrolleeResponse.setErrMsg("No Audit record found for Enrollee id :: " + enrolleeRequest.getEnrolleeId());
					LOGGER.error("No Audit record found for Enrollee id :: " + enrolleeRequest.getEnrolleeId());
				}
			}
		}else{
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrolleeResponse.setErrMsg("EnrolleeRequest is NULL");
			LOGGER.error("EnrolleeRequest is NULL");
		}
		return xstream.toXML(enrolleeResponse);
	}

	@Override
	public void findEnrolleeByApplicationid(final EnrolleeRequest enrolleeRequest, EnrolleeResponse enrolleeResponse) {
		try{
			/** populating the list of enrollment status*/
			List<String> statusList = new ArrayList<String>();
			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
			//statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_TERM);
			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED);

			/** populating Enrollee person type*/
			List<String> personTypeList = new ArrayList<String>();
			personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON);
			personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT);
			personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT);
			
			List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
			List<EnrollmentShopDTO> enrollmentShopDtoList = null;
			Enrollment healthEnrollment=null;
			Enrollment dentalEnrollment=null;
			if(isNotNullAndEmpty(enrolleeRequest.getSsapApplicationId())){
				enrollmentShopDtoList = enrolleeRepository.getEnrollmentShopDto(enrolleeRequest.getSsapApplicationId());
				if(enrollmentShopDtoList != null && !enrollmentShopDtoList.isEmpty())
				{
					for(EnrollmentShopDTO enrollmentShopDto : enrollmentShopDtoList)
					{
						List<EnrolleeShopDTO> enrolleeShopDTOList = new ArrayList<EnrolleeShopDTO>();
						if(isNotNullAndEmpty(enrolleeRequest.getEmployeeApplicationId())){
							enrolleeShopDTOList = enrolleeRepository.getEnrolleeShopDtoList(enrollmentShopDto.getEnrollmentId(), new TSDate());
						}
						else{
							enrolleeShopDTOList = enrolleeRepository.getEnrolleeIndividualDtoList(enrollmentShopDto.getEnrollmentId());
						}
						if(!enrolleeShopDTOList.isEmpty())
						{
							for(EnrolleeShopDTO enrolleeShopDto : enrolleeShopDTOList){
								
								if(!Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
									EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGE_BASED_REQUOTING))) {//HIX-105932
									enrolleeShopDto.setAge(null);
								}
								
								if(StringUtils.isNotEmpty(enrolleeShopDto.getPersonType()) && 
										enrolleeShopDto.getPersonType().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))
								{
									enrollmentShopDto.setCoveragePolicyNumber(enrolleeShopDto.getPolicyNumber());
									if(StringUtils.isNotEmpty(enrolleeShopDto.getEnrolleeLookUpValue())&&
											enrolleeShopDto.getEnrolleeLookUpValue().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
									{
										List<Object[]> enrollmentEventData = enrollmentEventRepository.getEnrollmentTermReasonAndDate(enrollmentShopDto.getEnrollmentId());
										if(enrollmentEventData != null && !enrollmentEventData.isEmpty())
										{
											for(Object[] enrollmentEventDataCol : enrollmentEventData)
											{
												enrollmentShopDto.setReasonForTermination((String) enrollmentEventDataCol[0]);
												enrollmentShopDto.setTerminationDate((Date) enrollmentEventDataCol[1]);
												break;
											}
										}
									}
								}	
							}//Enrollee loop ends here
							
						}
						enrollmentShopDto.setEnrolleeShopDTOList(enrolleeShopDTOList);
						//TODO Exception if no enrollee founds
						
						if(enrollmentShopDto.getEnrollmentStatusValue().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) 
								|| enrollmentShopDto.getEnrollmentStatusLabel().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
							List<String> cancelTermReason = enrollmentEventRepository.getEnrollmentCancelTermReason(enrollmentShopDto.getEnrollmentId());
							if((null!= cancelTermReason && !cancelTermReason.isEmpty())  && EnrollmentConstants.EVENT_REASON_NON_PAYMENT.equalsIgnoreCase(cancelTermReason.get(0))){
								enrollmentShopDto.setEnrollmentCancelTermForNonPayment(true);
							}
						}
						/** Max PremiumEffeciveDate*/
						List<Object[]> premiumEffDates = null;
						if(enrollmentShopDto.getEnrollmentId()!=null && enrollmentShopDto.getEnrollmentId()>0){
							premiumEffDates = enrollmentRepository.getPremiumEffDates(enrollmentShopDto.getEnrollmentId());
							Date grossPremEffDate = null;
							Date netPremEffDate = null;
							Date aptcEffDate = null;
							Date maxPremEffDate =null;
							Date stateSubsidyEffDate = null;
							if(premiumEffDates !=null){
								for(Object[] dateData : premiumEffDates){
									if(dateData !=null){
										grossPremEffDate = (Date) dateData[EnrollmentConstants.ZERO];
										netPremEffDate = (Date)dateData[EnrollmentConstants.ONE];
										if(dateData.length>2){
											aptcEffDate = (Date)dateData[EnrollmentConstants.TWO];
										}
										if(dateData.length>3){
											stateSubsidyEffDate = (Date)dateData[EnrollmentConstants.THREE];
										}
									}
									if(grossPremEffDate!=null){
										maxPremEffDate = grossPremEffDate;
										if(netPremEffDate!=null && netPremEffDate.after(grossPremEffDate)){
											maxPremEffDate = netPremEffDate;
										}
										if(aptcEffDate!=null && aptcEffDate.after(maxPremEffDate)){
											maxPremEffDate = aptcEffDate;
										}
										if(stateSubsidyEffDate!=null && stateSubsidyEffDate.after(maxPremEffDate)){
											maxPremEffDate = stateSubsidyEffDate;
										}
									}else if(netPremEffDate!=null){
										maxPremEffDate = netPremEffDate;
										if(aptcEffDate!=null && aptcEffDate.after(netPremEffDate)){
											maxPremEffDate = aptcEffDate;
										}
										if(stateSubsidyEffDate!=null && stateSubsidyEffDate.after(netPremEffDate)){
											maxPremEffDate = stateSubsidyEffDate;
										}
									}
									if(maxPremEffDate !=null){
										enrollmentShopDto.setPremiumEffDate(DateUtil.dateToString( maxPremEffDate, GhixConstants.REQUIRED_DATE_FORMAT));
									}
								}
							}
						}//End of premiumEffDates setting
						/** Max PremiumEffeciveDate*/
					}//Enrollment loop ends here
					enrolleeResponse.setEnrollmentShopDTOList(enrollmentShopDtoList);
					enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrolleeResponse.setErrMsg(EnrollmentConstants.MSG_RETRIEVE_ENROLLEE_SUCCESS);
				}
				else
				{
					enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrolleeResponse.setErrMsg(
							String.format(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_SSAP_APP_ID,
									enrolleeRequest.getSsapApplicationId()));
				}
			}else if(isNotNullAndEmpty(enrolleeRequest.getEmployeeApplicationId())){
				healthEnrollment=this.getHealthEnrollmentEmployeeIdAndEnrollmentStatus(enrolleeRequest.getEmployeeId(),enrolleeRequest.getEmployeeApplicationId().longValue(), statusList);
				dentalEnrollment=this.getDentalEnrollmentEmployeeIdAndEnrollmentStatus(enrolleeRequest.getEmployeeId(),enrolleeRequest.getEmployeeApplicationId().longValue(), statusList);
				if(isNotNullAndEmpty(healthEnrollment)){
					if(isNotNullAndEmpty(dentalEnrollment)){
						if(healthEnrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(dentalEnrollment.getEnrollmentStatusLkp().getLookupValueCode())){
							if(healthEnrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
								if(dentalEnrollment.getBenefitEffectiveDate().after(healthEnrollment.getBenefitEndDate())){
									enrollmentList.add(healthEnrollment);
								}else{
									enrollmentList.add(healthEnrollment);
									enrollmentList.add(dentalEnrollment);
								}
							}
							else{
								enrollmentList.add(healthEnrollment);
								enrollmentList.add(dentalEnrollment);
							}
						}else{
							enrollmentList.add(healthEnrollment);
							enrollmentList.add(dentalEnrollment);
						}
					}else{
						enrollmentList.add(healthEnrollment);
					}
				}
				if(isNotNullAndEmpty(enrollmentList) && !enrollmentList.isEmpty()){
					
					/** populating the EnrollmentShopDTO */
					List<EnrollmentShopDTO> enrollmentShopDTOList = new ArrayList<EnrollmentShopDTO>();
					for(Enrollment enrollmentObj: enrollmentList){
						if(isNotNullAndEmpty(enrollmentObj)){
							EnrollmentShopDTO enrollmentShopObj = new EnrollmentShopDTO();
							enrollmentShopObj.setPlanId(enrollmentObj.getPlanId());
							enrollmentShopObj.setPlanName(enrollmentObj.getPlanName());
							enrollmentShopObj.setPlanLevel(enrollmentObj.getPlanLevel());
							enrollmentShopObj.setEnrollmentId(enrollmentObj.getId());
							enrollmentShopObj.setMonthlyPremium(enrollmentObj.getGrossPremiumAmt());
							enrollmentShopObj.setEmployerContribution(enrollmentObj.getEmployerContribution());
							enrollmentShopObj.setMonthlyPayrollDeduction(enrollmentObj.getEmployeeContribution());
							enrollmentShopObj.setHouseHoldCaseId(enrollmentObj.getHouseHoldCaseId());
							enrollmentShopObj.setEmployeeApplicationId(enrollmentObj.getEmployeeAppId());
							enrollmentShopObj.setEmployerEnrollmentId(enrollmentObj.getEmployerEnrollmentId());
							enrollmentShopObj.setAptcAmt(enrollmentObj.getAptcAmt());
							enrollmentShopObj.setStateSubsidyAmt(enrollmentObj.getStateSubsidyAmt());
							enrollmentShopObj.setNetPremiumAmt(enrollmentObj.getNetPremiumAmt());
							enrollmentShopObj.setEnrollmentCreationTimestamp(enrollmentObj.getCreatedOn());
							if(isNotNullAndEmpty(enrollmentObj.getInsuranceTypeLkp())){
									enrollmentShopObj.setPlanType(enrollmentObj.getInsuranceTypeLkp().getLookupValueLabel());
							}
							enrollmentShopObj.setIssuerId(enrollmentObj.getIssuerId());
							
							enrollmentShopObj.setCoverageStartDate(enrollmentObj.getBenefitEffectiveDate());
							enrollmentShopObj.setCoverageEndDate(enrollmentObj.getBenefitEndDate());
							
							
							// populating the EnrolleeShopDTO
							List<Enrollee> enrolleeList = enrollmentObj.getEnrolleesAndSubscriber();
							List<EnrolleeShopDTO> enrolleeShopDTOList = new ArrayList<EnrolleeShopDTO>();
							 
							for(Enrollee enrolleeObj: enrolleeList){
								if((isNotNullAndEmpty(enrolleeObj) && isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp())) && 
										!(personTypeList.contains(enrolleeObj.getPersonTypeLkp().getLookupValueCode()))){
									/** Don't send Enrollees in cancelled state or Terminated enrollees with effective end date exceeded. */
									if(isNotNullAndEmpty(enrolleeRequest.getEmployeeApplicationId()) && (enrolleeObj.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))
												|| (enrolleeObj.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) && enrolleeObj.getEffectiveEndDate().before(new TSDate())))
									{
											continue;
									}
										EnrolleeShopDTO enrolleeShopObj = new EnrolleeShopDTO();
										if(enrolleeObj.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) &&
												enrolleeObj.getHealthCoveragePolicyNo() != null)
										{
											enrollmentShopObj.setCoveragePolicyNumber(enrolleeObj.getHealthCoveragePolicyNo());
										}
										enrolleeShopObj.setPolicyNumber(enrolleeObj.getHealthCoveragePolicyNo());
										enrolleeShopObj.setFirstName(enrolleeObj.getFirstName());
										enrolleeShopObj.setLastName(enrolleeObj.getLastName());
										enrolleeShopObj.setEffectiveStartDate(enrolleeObj.getEffectiveStartDate());
										enrolleeShopObj.setEffectiveEndDate(enrolleeObj.getEffectiveEndDate());
										enrolleeShopObj.setExchgIndivIdentifier(enrolleeObj.getExchgIndivIdentifier());
										
										enrolleeShopObj.setPersonType(isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp()) ? enrolleeObj.getPersonTypeLkp().getLookupValueCode() : null);
										
										if((isNotNullAndEmpty(enrolleeObj.getEnrolleeLkpValue()) && enrolleeObj.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
											 && (isNotNullAndEmpty(enrolleeObj.getPersonTypeLkp()) && enrolleeObj.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){
												List<EnrollmentEvent> eventList = enrolleeObj.getEnrollmentEvents();
												for(EnrollmentEvent eventObj : eventList){
													if(eventObj.getEventTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.EVENT_TYPE_TERMINATION)){
														enrollmentShopObj.setReasonForTermination(eventObj.getEventReasonLkp().getLookupValueLabel());
														enrollmentShopObj.setTerminationDate(eventObj.getCreatedOn());
														break;
													}
												}
										}
										enrolleeShopDTOList.add(enrolleeShopObj);
									
								}
							}
							//old method enrollee Loops end here
							
							/** populating the enrollment status*/
							if(isNotNullAndEmpty(enrollmentObj.getEnrollmentStatusLkp())){
								enrollmentShopObj.setEnrollmentStatusValue(enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode());
								enrollmentShopObj.setEnrollmentStatusLabel(enrollmentObj.getEnrollmentStatusLkp().getLookupValueLabel());
								
								if(enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
									List<String> cancelTermReason = enrollmentEventRepository.getEnrollmentCancelTermReason(enrollmentObj.getId());
									if((null!= cancelTermReason && !cancelTermReason.isEmpty())  && EnrollmentConstants.EVENT_REASON_NON_PAYMENT.equalsIgnoreCase(cancelTermReason.get(0))){
										enrollmentShopObj.setEnrollmentCancelTermForNonPayment(true);
									}
								}
							}
							enrollmentShopObj.setEnrolleeShopDTOList(enrolleeShopDTOList);
							enrollmentShopDTOList.add(enrollmentShopObj);
						}
					}
					enrolleeResponse.setEnrollmentShopDTOList(enrollmentShopDTOList);
					enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrolleeResponse.setErrMsg(EnrollmentConstants.MSG_RETRIEVE_ENROLLEE_SUCCESS);
				}//Code till here	
				else{
					enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
					enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_EMP_APP_ID);
				}		
			}else{
				enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_INVALID_INPUT);
			}
		}
		catch(Exception ge){
			LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLEE_RETRIEVE_FAIL , ge);
			enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLEE_RETRIEVE_FAIL+"::"+ge.getCause());
		}
	}

	@Override
	public List<Object[]> findActiveEnrolleeDataByListOfEnrollmentIds(List<Integer> enrollmentIdList, List<String> personTypeList, Date systemDate) 
	{
		return enrolleeRepository.getActiveEnrolleeDataByListOfEnrollmentId(enrollmentIdList, personTypeList,  systemDate);
	}

	@Override
	public List<Object[]> getEnrolleeDataByListOfEnrollmentIdAndPersonType(
			List<Integer> enrollmentIdList, List<String> personTypeList)
	{
			return enrolleeRepository.getEnrolleeDataByListOfEnrollmentIdAndPersonType(enrollmentIdList, personTypeList);
		}

	@Override
	public List<Object[]> getEnrolleeDataByListOfEnrollmentIdAndPersonTypeCA(List<Integer> enrollmentIdList, List<String> personTypeList){
		return enrolleeRepository.getEnrolleeDataByListOfEnrollmentIdAndPersonTypeCA(enrollmentIdList, personTypeList);
	}

	@Override
	public List<Object[]> getEnrolleesDataByListOfEnrollmentId(List<Integer> enrollmentIdList, List<String> personTypeList) {
		// TODO Auto-generated method stub
		return enrolleeRepository.getEnrolleesDataByListOfEnrollmentId(enrollmentIdList, personTypeList);
	}

	/**
	 * 
	 */
	@Override	
	@Transactional
	public void saveCarrierUpdatedDataResponse(List<SendUpdatedEnrolleeResponseDTO> inputVal, EnrolleeUpdateSendStatus enrolleeUpdateSendStatusObject){

		LOGGER.info(" inside saveCarrierUpdatedDataResponse  : inputVal =  " +inputVal);
		for(SendUpdatedEnrolleeResponseDTO sendUpdatedEnrolleeResponseDTO: inputVal){
			EnrolleeUpdateSendStatus enrolleeUpdateSendStatus=null;
			//SendUpdatedEnrolleeResponseDTO sendUpdatedEnrolleeResponseDTO  = (SendUpdatedEnrolleeResponseDTO) objPlan;
			if(enrolleeUpdateSendStatusObject==null){
				enrolleeUpdateSendStatus = new EnrolleeUpdateSendStatus();
				enrolleeUpdateSendStatus.setExchgIndivIdentifier(sendUpdatedEnrolleeResponseDTO.getExchgIndivIdentifier());
				enrolleeUpdateSendStatus.setEnrolleeStatus(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, sendUpdatedEnrolleeResponseDTO.getEnrolleeStatus()));
				
				if(sendUpdatedEnrolleeResponseDTO.getEnrolleeId()!=null){
					Enrollee en = new Enrollee();
					en.setId(Integer.parseInt(sendUpdatedEnrolleeResponseDTO.getEnrolleeId()));
					enrolleeUpdateSendStatus.setEnrollee(en);
				}
				
			}else{
				enrolleeUpdateSendStatus=enrolleeUpdateSendStatusObject;
			}
			enrolleeUpdateSendStatus.setAhbxStatusCode(sendUpdatedEnrolleeResponseDTO.getAhbxStatusCode());
			enrolleeUpdateSendStatus.setAhbxStatusDesc(sendUpdatedEnrolleeResponseDTO.getAhbxStatusDescription());
			
			try{
				enrolleeUpdateSendStatusRepository.save(enrolleeUpdateSendStatus);
			}catch(Exception e){
				LOGGER.info("Exception occured while saving the response for IND21 from AHBX ");
				LOGGER.error("Exception occured while saving the response for IND21 from AHBX : "+ e );
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<EnrollmentSubscriberDTO> fetchEnrollmentSubscriberData(final EnrollmentSubscriberRequestDTO enrollmentSubscriberRequestDTO, EnrolleeResponse enrolleeResponse) {
		List<EnrollmentSubscriberDTO> enrollmentSubscriberDTOList = new ArrayList<EnrollmentSubscriberDTO>();
		EntityManager entityManager = null;
		try{
			String countQueryString = "SELECT COUNT(*) FROM (";
			Query selectQuery = null;
			Query countQuery = null;
			StringBuilder queryBuilder = new StringBuilder(300);
			entityManager = entityManagerFactory.createEntityManager();
			queryBuilder.append(SUBSCRIBER_ENROLLMENT_QUERY);
			queryBuilder.append( " AND ee.PERSON_TYPE_LKP = ");
			queryBuilder.append(lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE, EnrollmentConstants.PERSON_TYPE_SUBSCRIBER));
			
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTO.getEnrollmentTypeLookupCode())){
				queryBuilder.append( " AND en.ENROLLMENT_TYPE_LKP = ");
				queryBuilder.append(lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE, enrollmentSubscriberRequestDTO.getEnrollmentTypeLookupCode()));
			}
			
			
			if(enrollmentSubscriberRequestDTO.getIssuerID() != null && enrollmentSubscriberRequestDTO.getIssuerID() != EnrollmentConstants.ZERO){
				queryBuilder.append(" AND en.ISSUER_ID = ");
				queryBuilder.append(enrollmentSubscriberRequestDTO.getIssuerID());
			}
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTO.getCoverageYear())){
				queryBuilder.append(" AND TO_CHAR(en.BENEFIT_EFFECTIVE_DATE, 'YYYY') = '");
				queryBuilder.append(enrollmentSubscriberRequestDTO.getCoverageYear());
				queryBuilder.append("'");
			}
			if(enrollmentSubscriberRequestDTO.getSubscriberName() != null){
				queryBuilder.append(" AND LOWER(ee.FIRST_NAME ||CASE WHEN (ee.MIDDLE_NAME IS NOT NULL) THEN ' '||ee.MIDDLE_NAME ELSE '' END|| ' '||ee.LAST_NAME) like '%");
				queryBuilder.append(enrollmentSubscriberRequestDTO.getSubscriberName().toLowerCase());
				queryBuilder.append("%'");
			}
			if(enrollmentSubscriberRequestDTO.getEnrollmentID() != null){
				queryBuilder.append(" AND en.ID = ");
				queryBuilder.append(enrollmentSubscriberRequestDTO.getEnrollmentID());
			}
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTO.getCMSPlanID())){
				queryBuilder.append(" AND en.CMS_PLAN_ID = '");
				queryBuilder.append(enrollmentSubscriberRequestDTO.getCMSPlanID());
				queryBuilder.append("'");
				
			}
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTO.getEnrollmentStatus())){
				queryBuilder.append(" AND en.ENROLLMENT_STATUS_LKP = ");
				queryBuilder.append(lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, enrollmentSubscriberRequestDTO.getEnrollmentStatus().toUpperCase()));
			}
			else{
				queryBuilder.append(" AND en.ENROLLMENT_STATUS_LKP NOT IN (");
				queryBuilder.append(lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_ABORTED));
				queryBuilder.append(")");
			}
			
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTO.getExchgSubscriberIdentifier())){
				queryBuilder.append(" AND en.EXCHG_SUBSCRIBER_IDENTIFIER = '");
				queryBuilder.append(enrollmentSubscriberRequestDTO.getExchgSubscriberIdentifier());
				queryBuilder.append("'");
			}
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTO.getSSNLast4Digit())){
				queryBuilder.append(" AND SUBSTR(ee.TAX_ID_NUMBER, 6) = '");
				queryBuilder.append(enrollmentSubscriberRequestDTO.getSSNLast4Digit());
				queryBuilder.append("'");
			}
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTO.getSubscriberDateOfBirth())){
				queryBuilder.append(" AND TO_CHAR(ee.BIRTH_DATE, 'MM/DD/YYYY') = '");
				queryBuilder.append(enrollmentSubscriberRequestDTO.getSubscriberDateOfBirth());
				queryBuilder.append("'");
			}
			if(enrollmentSubscriberRequestDTO.getInsuranceTypeLookupCode() != null){
				queryBuilder.append(" AND en.INSURANCE_TYPE_LKP = ");
				queryBuilder.append(lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_INSURANCE_TYPE, enrollmentSubscriberRequestDTO.getInsuranceTypeLookupCode()));
			}
			
			countQueryString = countQueryString + queryBuilder.toString() + ") subscSub ";
			
			queryBuilder.append(" ORDER BY ");
			if(StringUtils.isNotBlank(enrollmentSubscriberRequestDTO.getSortByField())){
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("enrollmentID")){
					queryBuilder.append(" en.ID ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("issuerID")){
					queryBuilder.append(" en.ISSUER_ID ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("CMSPlanID")){
					queryBuilder.append(" en.CMS_PLAN_ID ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("planName")){
					queryBuilder.append(" en.PLAN_NAME ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("planLevel")){
					queryBuilder.append(" en.PLAN_LEVEL ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("insurerName")){
					queryBuilder.append(" en.INSURER_NAME ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("enrollmentBenefitStartDate")){
					queryBuilder.append(" en.BENEFIT_EFFECTIVE_DATE ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("enrollmentBenefitEndDate")){
					queryBuilder.append(" en.BENEFIT_END_DATE ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("enrollmentStatusLookupCode")){
					queryBuilder.append("enrollmentStatusLkp.LOOKUP_VALUE_CODE ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("subscriberFullName")){
					queryBuilder.append(" FULLNAME ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("birthDate")){
					queryBuilder.append(" ee.BIRTH_DATE ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("lastFourDigitSSN")){
					queryBuilder.append(" SSN ");
				}
				if(enrollmentSubscriberRequestDTO.getSortByField().equalsIgnoreCase("exchgSubscriberIdentifier")){
					queryBuilder.append(" en.EXCHG_SUBSCRIBER_IDENTIFIER ");
				}
			}
			else{
				queryBuilder.append(" en.id ");
			}
			if(enrollmentSubscriberRequestDTO.getSortOrder() != null){
				queryBuilder.append(enrollmentSubscriberRequestDTO.getSortOrder().toString());
			}
			else{
				queryBuilder.append(EnrollmentSubscriberRequestDTO.SortOrder.DESC.toString());
			}
			selectQuery = entityManager.createNativeQuery(queryBuilder.toString());
			countQuery = entityManager.createNativeQuery(countQueryString);
			
			Integer pageSize = EnrollmentConstants.ZERO;
			Integer startPosition = EnrollmentConstants.ZERO;
			if(enrollmentSubscriberRequestDTO.getPageSize() != null && enrollmentSubscriberRequestDTO.getPageSize() != EnrollmentConstants.ZERO){
				pageSize = enrollmentSubscriberRequestDTO.getPageSize();
				selectQuery.setMaxResults(pageSize);
			}
			
			if(enrollmentSubscriberRequestDTO.getPageNumber() != null && enrollmentSubscriberRequestDTO.getPageNumber() != EnrollmentConstants.ZERO
					&& pageSize != EnrollmentConstants.ZERO){
				startPosition = (enrollmentSubscriberRequestDTO.getPageNumber() * pageSize) - pageSize;
				selectQuery.setFirstResult(startPosition);
			}
			Number totalRecordCount = (Number) countQuery.getSingleResult();
			
			List<?> objectList = selectQuery.getResultList();
			
			
			if(objectList != null && !objectList.isEmpty()){
				Iterator rsIterator = objectList.iterator();
				Object[] objArray = null;
				while (rsIterator.hasNext()) {
					objArray = (Object[]) rsIterator.next();
					EnrollmentSubscriberDTO enrollmentSubscriberDTO = new EnrollmentSubscriberDTO();
					enrollmentSubscriberDTO.setEnrollmentID(objArray[EnrollmentConstants.ZERO] != null ? Integer.valueOf(objArray[EnrollmentConstants.ZERO].toString()): null);
					enrollmentSubscriberDTO.setIssuerID(objArray[EnrollmentConstants.ONE] != null ? Integer.valueOf(objArray[EnrollmentConstants.ONE].toString()): null);
					enrollmentSubscriberDTO.setCMSPlanID((String) objArray[EnrollmentConstants.TWO]);
					enrollmentSubscriberDTO.setPlanName((String) objArray[EnrollmentConstants.THREE]);
					enrollmentSubscriberDTO.setPlanLevel((String) objArray[EnrollmentConstants.FOUR]);
					enrollmentSubscriberDTO.setInsurerName((String) objArray[EnrollmentConstants.FIVE]);
					enrollmentSubscriberDTO.setEnrollmentBenefitStartDate((Date) objArray[EnrollmentConstants.SIX]);
					enrollmentSubscriberDTO.setEnrollmentBenefitEndDate((Date) objArray[EnrollmentConstants.SEVEN]);
					enrollmentSubscriberDTO.setEnrollmentStatusLookupCode((String) objArray[EnrollmentConstants.EIGHT]);
					enrollmentSubscriberDTO.setEnrollmentStatusLookupLabel((String) objArray[EnrollmentConstants.NINE]);
					
					enrollmentSubscriberDTO.setSubscriberFirstName((String) objArray[EnrollmentConstants.TEN]);
					enrollmentSubscriberDTO.setSubscriberMiddleName((String) objArray[EnrollmentConstants.ELEVEN]);
					enrollmentSubscriberDTO.setSubscriberLastName((String) objArray[EnrollmentConstants.TWELVE]);
					enrollmentSubscriberDTO.setSubscriberFullName(EnrollmentUtils.getFullName(enrollmentSubscriberDTO.getSubscriberFirstName(), enrollmentSubscriberDTO.getSubscriberMiddleName(), enrollmentSubscriberDTO.getSubscriberLastName()));
					enrollmentSubscriberDTO.setBirthDate((Date) objArray[EnrollmentConstants.FOURTEEN]);
					enrollmentSubscriberDTO.setLastFourDigitSSN((String) objArray[EnrollmentConstants.FIFTEEN]);
					enrollmentSubscriberDTO.setExchgSubscriberIdentifier((String) objArray[EnrollmentConstants.SIXTEEN]);
					enrollmentSubscriberDTO.setCoverageYear((String) objArray[EnrollmentConstants.SEVENTEEN]);
					enrollmentSubscriberDTO.setInsuranceTypeLookupCode((String) objArray[EnrollmentConstants.EIGHTEEN]);
					enrollmentSubscriberDTO.setInsuranceTypeLookupLabel((String) objArray[EnrollmentConstants.NINETEEN]);
					
					enrollmentSubscriberDTOList.add(enrollmentSubscriberDTO);
				}
				enrolleeResponse.setEnrollmentSubscriberDTOList(enrollmentSubscriberDTOList);
				enrolleeResponse.setTotalRecordCount(totalRecordCount != null ? totalRecordCount.longValue(): EnrollmentConstants.ZERO);
			}
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred in fetchEnrollmentSubscriberData(-, -) method: ", ex);
			enrolleeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrolleeResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			enrolleeResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
		}
		finally{
			if(entityManager != null && entityManager.isOpen()){
				try {
					entityManager.clear();
					entityManager.close();
					entityManager = null;
				} catch (IllegalStateException ex) {
					LOGGER.error("Exception caught while closing EntityManager: ", ex);
				}
			}
		}
		return enrollmentSubscriberDTOList;
	}

	@Override
	public List<Object[]> getSubscriberDataByEnrollmentId(Integer enrollmentId) {
		return enrolleeRepository.getSubscriberDataByEnrollmentId(enrollmentId);
	}

	/**
	 * Method is used to get Enrollment and Enrollee data to generate QHP Reports.
	 */
	@Override
	public List<QhpReportDTO> getEnrollmentAndEnrolleeDataToGenerateQHPReports(Integer enrollmentId) {

		List<QhpReportDTO> qhpReportList = null;

		if (null == enrollmentId || 0 >= enrollmentId) {
			LOGGER.error("Invalid Enrollment-ID.");
			return qhpReportList;
		}
		qhpReportList = enrolleeRepository.getEnrollmentAndEnrolleeDataToGenerateQHPReports(enrollmentId);

		if (CollectionUtils.isEmpty(qhpReportList)) {
			LOGGER.error("Enrollment data is not found to generate QHP Reports.");
		}
		return qhpReportList;
	}

	/**
	 * Method is used to store QHP Report data using QhpReportDTO class.
	 */
	@Override
	@Transactional
	public boolean storeQHPReportData(List<QhpReportDTO> qhpReportList, Integer batchJobExecutionId) {

		boolean qhpReportsStatus = false;

		try {

			if (CollectionUtils.isEmpty(qhpReportList)) {
				LOGGER.error("There is no data found to store in QHP Reports table.");
				return qhpReportsStatus;
			}

			// Insert data into QHP_REPORT table
			for (QhpReportDTO qhpReportDTO : qhpReportList) {

				QhpReport qhpReportModel = setQhpReportModel(qhpReportDTO, batchJobExecutionId);
				iQhpReportRepository.saveAndFlush(qhpReportModel);

				if (!qhpReportsStatus) {
					qhpReportsStatus = true;
				}
			}
		}
		catch (Exception ex) {
			qhpReportsStatus = false;
			LOGGER.error("Failed to store new QHP Reports data in DB: ", ex);
		}
		finally {

			if (qhpReportsStatus) {
				LOGGER.debug("Successfully load QHP Reports data in database.");
			}
		}
		return qhpReportsStatus;
	}

	/**
	 * Method is used to set QhpReport Model members.
	 */
	private QhpReport setQhpReportModel(QhpReportDTO qhpReportDTO, Integer batchJobExecutionId) {

		QhpReport qhpReportModel = new QhpReport();
		qhpReportModel.setBatchJobExecutionId(batchJobExecutionId);

		// Set Enrollment Data
		qhpReportModel.setExternalCaseId(qhpReportDTO.getExternalCaseId());
		qhpReportModel.setHiosPlanId(qhpReportDTO.getHiosPlanId());
		qhpReportModel.setPlanType(qhpReportDTO.getPlanType());
		qhpReportModel.setYearOfEnrollment(qhpReportDTO.getYearOfEnrollment());
		qhpReportModel.setEnrollmentId(qhpReportDTO.getEnrollmentId());
		qhpReportModel.setEnrollmentStatus(qhpReportDTO.getEnrollmentStatus());
		qhpReportModel.setPlanName(qhpReportDTO.getPlanName());
		qhpReportModel.setPlanLevel(qhpReportDTO.getPlanLevel());
		qhpReportModel.setIssuerName(qhpReportDTO.getIssuerName());
		qhpReportModel.setBrokerAssisterId(qhpReportDTO.getBrokerAssisterId());
		qhpReportModel.setDelegationType(qhpReportDTO.getDelegationType());

		// Set Enrollee(Member) Data
		qhpReportModel.setMemberId(qhpReportDTO.getMemberId());
		qhpReportModel.setMemberBenefitStartDate(qhpReportDTO.getMemberBenefitStartDate());
		qhpReportModel.setMemberBenefitEndDate(qhpReportDTO.getMemberBenefitEndDate());
		qhpReportModel.setMemberStatus(qhpReportDTO.getMemberStatus());
		qhpReportModel.setSubscriberFlag(qhpReportDTO.getSubscriberFlag());
		return qhpReportModel;
	}

	/**
	 * Method is used to clean old QHP Report data from DB by Job ID.
	 */
	@Override
	@Transactional
	public boolean cleanOldQHPReportDataByJobId(Integer batchJobExecutionId) {

		boolean hasCleanedOldData = false;

		try {
			iQhpReportRepository.deleteAllQhpRptByNotInBatchJobExecId(batchJobExecutionId);
			LOGGER.debug("Successfully delete all old data from QHP Report.");
			hasCleanedOldData = true;
		}
		catch (Exception ex) {
			LOGGER.error("Failed to delete old QHP Reports data from DB: ", ex);
		}
		return hasCleanedOldData;
	}

	@Override
	public List<Enrollee> getEnrolleeExchgIndivIdentifierAndCoverageStartDateByEnrollmentIdAndChangeDate(
			Integer enrollmentId, Date changeEffectiveDate) {
		List<Object[]> enrolleeList = enrolleeRepository.getExchgIndivIdentifierAndEffectiveStartDateByEnrollmentIDAndChangeDate(enrollmentId, changeEffectiveDate);
		List<Enrollee> responseEnrolleeList = new ArrayList<Enrollee>();
		Enrollee responseEnrollee;
		
		for (Object[] enrollColData : enrolleeList) {
			
			responseEnrollee = new Enrollee();
			responseEnrollee.setId((Integer)enrollColData[0]);
			responseEnrollee.setExchgIndivIdentifier((String)enrollColData[1]);
			responseEnrollee.setEffectiveStartDate((Date)enrollColData[2]);
			if(enrollColData.length>3){//
				responseEnrollee.setQuotingDate((Date)enrollColData[3]);
			}
			//HIX-105005, HIX-105932
			if((enrollColData.length>4)&&
					Boolean.parseBoolean(
							DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGE_BASED_REQUOTING))) {
				responseEnrollee.setAge((Integer)enrollColData[4]);
			}
			
			responseEnrolleeList.add(responseEnrollee);
		}
		
		return responseEnrolleeList;
	}
}
