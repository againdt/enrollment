package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.platform.util.exception.GIException;
import java.util.Date;
public interface GroupInstallationService {	
	
	void getGroupInstallationXML(String regenerate,String employer_enrollment_Id, String issuer_Id, String groupAction) throws GIException;
	
	void addOrUpdateEmployerGroup(Integer empId, Integer issuerId, Integer planId,Integer employerEnrollmentId);
	
	/**
	 * @author parhi_s
	 * This method is used to terminate the group when the employer gets terminated
	 * @param employerId
	 * @throws GIException
	 */
	void terminateGroupForEmployer(Integer employerId, Integer employer_enrollment_Id,Date terminationDate) throws GIException;

	/**
	 * @author parhi_s
	 * This method returns ID of all the groups which are either terminated or created or for which the Employer or Broker or Location is updated. 
	 * @param lastRunDate
	 * @return List<Integer>
	 * @throws GIException
	 */
	/*List<Integer> getGroups(java.util.Date lastRunDate) throws GIException;*/
}
