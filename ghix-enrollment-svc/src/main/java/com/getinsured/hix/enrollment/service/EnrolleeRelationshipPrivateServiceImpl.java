/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipPrivateRepository;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.enrollment.service.EnrolleeRelationshipPrivateService;
/**
 * @author panda_p
 *
 */



@Service("enrolleeRelationshipPrivateService")
public class EnrolleeRelationshipPrivateServiceImpl implements EnrolleeRelationshipPrivateService {
	
	@Autowired private IEnrolleeRelationshipPrivateRepository enrolleeRelationshipRepository;
	@Override
	public EnrolleeRelationship findBySourceEnrolleeAndTargetEnrollee(Enrollee sourceEnrollee, Enrollee targetEnrollee) {
		if (sourceEnrollee != null && targetEnrollee != null) {
			
			return enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(sourceEnrollee.getId(),targetEnrollee.getId());
		} else {
			
			return null;
		}
	}

}
