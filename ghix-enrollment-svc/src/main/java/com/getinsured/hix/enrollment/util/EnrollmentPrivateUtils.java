package com.getinsured.hix.enrollment.util;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;

/**
 * 
 * @author rajaramesh_g
 *
 */
@Component
public class EnrollmentPrivateUtils {
	
		@Autowired private AppEventService appEventService;
		@Autowired private LookupService lookupService;
		private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentPrivateUtils.class);	

        /**@param e
         * @return boolean
		 * the below is takes input as object type (e) like (Integer, String, Character, List<String>, List<Integer>...etc)
		 * and return true when the input object is not null/empty/"null" else return false"
	  	 **/
		public static <T> boolean isNotNullAndEmpty(T e){
	 	   boolean isNotNUll=false;
	 	   if((e!=null) && !(e.toString().isEmpty()) && !(e.toString().equalsIgnoreCase("null"))){
				isNotNUll=true;
	 	   }
		  return isNotNUll;
	 	}
		
		/**@param e
         * @return boolean
		 * the below is takes input as object type (e) like (Integer, String, Character, List<String>, List<Integer>...etc)
		 * and return true when the input object is not null else return false"
	  	 **/
		public static <T> boolean isNotNull(T e){
	 	   boolean isNotNUll=false;
			if((e!=null)){
				isNotNUll=true;
	 	   }
		  return isNotNUll;
	 	}
		
		/**@param <V>
		 * @param <K>
		 * @param e
         * @return boolean
		 * the below is takes input as object type (<K,V>) like (Map<Integer, String>, Map<String, String>.... etc)
		 * and return true when the input object (<K,V>) is not null/empty/"null" else return false"
	  	 **/
		public static <K,V> boolean isMapNotNullAndEmpty(Map<K,V> e){
	 	   boolean isNotNUll=false;
	 	  if((e!=null) && !(e.isEmpty())){
				isNotNUll=true;
		   }
		  return isNotNUll;
	 	}
		
		/**@param <V>
		 * @param <K>
		 * @param e
         * @return boolean
		 * the below is takes input as object type (<K,V>) like (Map<Integer, String>, Map<String, String>.... etc)
		 * and return true when the input object is not null else return false"
	  	 **/
		public static <K,V> boolean isMapNotNull(Map<K,V> e){
	 	   boolean isNotNUll=false;
	 	  if((e!=null)){
				isNotNUll=true;
		  }
		  return isNotNUll;
	 	}
		
		/**
		 * @author raja
		 * @since 10/05/2013
		 * 
		 * This method validates the date of birth with current date and 
		 * also it converts the date format from String to java util date
		 * 
		 * @param date
		 * @return
		 * @throws GIException
		 */
		public static Date validateDOB(String date) throws GIException 
		{
			Date currentDate = new TSDate();
			Date dob=null;
			try{
				if(!DateUtil.isValidDate(date, GhixConstants.REQUIRED_DATE_FORMAT)){
					throw new GIException(GhixErrors.INVALID_DATE_OF_BIRTH);
				}
			 dob= DateUtil.StringToDate(date, GhixConstants.REQUIRED_DATE_FORMAT);
			if (!(currentDate.compareTo(dob) > 0))
			{
				throw new GIException(GhixErrors.INVALID_DATE_OF_BIRTH);
			}
			}catch(Exception e){
				LOGGER.error("Invalid date of birth" , e);
				throw new GIException(GhixErrors.INVALID_DATE_OF_BIRTH);
			}
			return dob;
		}
		
		/**
		 * @author raja
		 * @since 20/08/2013
		 * 
		 * This method checks the given date with the in start date and 
		 * and the end date.
		 * 
		 * @param startDate, endDate, givenDate
		 * @return true/false
		 * 
		 */
		
		public static boolean isDateInBetweenStartDateAndEndDate(Date startDate, Date endDate, Date givenDate) {
			
			boolean isDateBetween=false;
	        if (givenDate.after(startDate) && givenDate.before(endDate)) {
	        	isDateBetween= true;
	        }
	       else if (givenDate.equals(startDate) || givenDate.equals(endDate)) {
	    	   isDateBetween= true;
	        }
	        return isDateBetween;
	    }

		/**
		 * @author shinde_dh
		 * @since 01/06/2016
		 * 
		 * @param enrollment
		 * @param appEnrollmentEventLkpValueCode
		 * @param prevStatus
		 */
		public void recordApplicationEnrollmentPrivateEvent(final Enrollment enrollment, final String appEnrollmentEventLkpValueCode , String prevStatus)
		{
			try
			{
				LOGGER.info("ENROLLMENT_APP_EVENT:: Received event: "+appEnrollmentEventLkpValueCode);
				Map<String, String> mapParameters = null;
				if(enrollment != null && isNotNullAndEmpty(appEnrollmentEventLkpValueCode))
				{
					if(isNotNullAndEmpty(enrollment.getCmrHouseHoldId()))
					{
						EventInfoDto eventInfoDto = new EventInfoDto();
						mapParameters = populateEnrollmentMapData(enrollment,appEnrollmentEventLkpValueCode,prevStatus);
						
						eventInfoDto.setModuleId(enrollment.getCmrHouseHoldId());
						eventInfoDto.setModuleName("INDIVIDUAL");//HardCoded as no Enum found for this role found
						LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.APP_ENROLLMENT_EVENT,appEnrollmentEventLkpValueCode);
						if(lookupValue != null) {
							eventInfoDto.setEventLookupValue(lookupValue);
							LOGGER.info("ENROLLMENT_APP_EVENT:: Prepared EventInfoDTO : "+eventInfoDto.toString()+ " lookupValue: "+lookupValue.getLookupValueCode());
							LOGGER.info("ENROLLMENT_APP_EVENT:: Map Parameters: "+mapParameters);
							appEventService.record(eventInfoDto, mapParameters);
						}
						else {
							LOGGER.info("NO Lookup Found for Lookup_value_code :"+ appEnrollmentEventLkpValueCode);
						}
					}
					else
					{
						LOGGER.warn("ENROLLMENT_APP_EVENT:: No CMR HouseHold ID found.");
					}
				}
				else
				{
					LOGGER.warn("ENROLLMENT_APP_EVENT:: Invalid or empty request received for AppEventLog");
				}
			}
			catch(Exception ex)
			{
				/**
				 * Suppressing Exception, to avoid existing service layer abrupt
				 */
				LOGGER.error("ENROLLMENT_APP_EVENT:: Exception caught in recordApplicationEnrollmentPrivateEvent(-) method::"+ex);
			}
		}
		
		/**
		 * 
		 * @param enrollment
		 * @return
		 */
		private Map<String, String> populateEnrollmentMapData(final Enrollment enrollment, String appEnrollmentEventLkpValueCode , String prevStatus)
		{
			Map<String, String> responseDataMap = new HashMap<String, String>();
			if(enrollment != null)
			{	
				
				if(enrollment.getId() != null){
					responseDataMap.put("Enrollment Id", Integer.toString(enrollment.getId()));
				}
					
				if(isNotNullAndEmpty(enrollment.getInsurerName())){
					responseDataMap.put("Insurer Name", enrollment.getInsurerName());
				}
				if(isNotNullAndEmpty(enrollment.getPlanName())){
					responseDataMap.put("Plan Name", enrollment.getPlanName());
				}
				if(isNotNullAndEmpty(enrollment.getInsuranceTypeLkp())){
					responseDataMap.put("Plan Type", enrollment.getInsuranceTypeLkp().getLookupValueLabel());
				}
				if(isNotNullAndEmpty(enrollment.getExchangeTypeLkp())){
					responseDataMap.put("On/Off Exchange", enrollment.getExchangeTypeLkp().getLookupValueLabel());
				}
				if(isNotNullAndEmpty(enrollment.getEnrollmentModalityLkp())){
					responseDataMap.put("Enrollment Modality", enrollment.getEnrollmentModalityLkp().getLookupValueLabel());
				}
				if(isNotNullAndEmpty(enrollment.getGrossPremiumAmt())){
					responseDataMap.put("Monthly Premium", enrollment.getGrossPremiumAmt().toString());
				}
				if(isNotNullAndEmpty(enrollment.getAptcAmt()) 
						&& (isNotNullAndEmpty(enrollment.getExchangeTypeLkp()) && enrollment.getExchangeTypeLkp().getLookupValueCode().equalsIgnoreCase("ONEXCHANGE"))
						&& ( EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString().equals(appEnrollmentEventLkpValueCode))
				  )
				{
					responseDataMap.put("APTC", enrollment.getAptcAmt().toString());
				}
				if(isNotNullAndEmpty(enrollment.getBenefitEffectiveDate())){
					responseDataMap.put("Benefit Effective Date", enrollment.getBenefitEffectiveDate().toString());
				}
				if(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_TERMINATED.toString().equals(appEnrollmentEventLkpValueCode)){
					if(isNotNullAndEmpty(enrollment.getBenefitEndDate())){
						responseDataMap.put("Termination Date", enrollment.getBenefitEndDate().toString());
					}
				}
				if(isNotNullAndEmpty(enrollment.getUpdatedBy())){
					responseDataMap.put("Updated By", String.valueOf(enrollment.getUpdatedBy().getFullName()));
				}
				if(isNotNullAndEmpty(enrollment.getUpdatedOn())){
					responseDataMap.put("Updated On", enrollment.getUpdatedOn().toString());
				}
				if( EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_STATUS_CHANGED.toString().equals(appEnrollmentEventLkpValueCode)){
					responseDataMap.put("Previous Enrollment Status", prevStatus);
					responseDataMap.put("Current Enrollment Status", enrollment.getEnrollmentStatusLkp().getLookupValueLabel());
				}else{
					if(enrollment.getEnrollmentStatusLkp() != null){
						responseDataMap.put("Enrollment Status", enrollment.getEnrollmentStatusLkp().getLookupValueCode());
				}
			}
				
			}
			return responseDataMap;
		}
}
