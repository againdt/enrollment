/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.dto.enrollment.EnrollmentUpdateDTO;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public interface EnrollmentEditToolService {
	/**
	 * Enrollment Edit Tool Add Transaction
	 * @param enrollmentUpdateDTO
	 * @param response
	 * @param giWsPayloadId
	 * @throws GIException
	 */
	void updateEnrollmentAddTxnEditTool(EnrollmentUpdateDTO enrollmentUpdateDTO, EnrollmentResponse response, Integer giWsPayloadId) throws GIException;
}
