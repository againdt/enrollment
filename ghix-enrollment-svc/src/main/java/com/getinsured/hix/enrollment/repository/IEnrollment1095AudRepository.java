package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.enrollment.Enrollment1095Aud;

public interface IEnrollment1095AudRepository extends JpaRepository<Enrollment1095Aud, AudId> {
	
	@Query(" SELECT DISTINCT en.exchgAsignedPolicyId FROM Enrollment1095Aud as en WHERE en.correctedRecordSeqNum = :correctedRecordSeqNum AND en.isActive='Y' AND en.isObsolete='N' ")
	List<Integer> getEnrollmentId(@Param("correctedRecordSeqNum") String correctedRecordSeqNum);


}
