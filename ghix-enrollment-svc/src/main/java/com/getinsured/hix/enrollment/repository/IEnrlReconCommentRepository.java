package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.enrollment.EnrlReconComment;

public interface IEnrlReconCommentRepository extends JpaRepository<EnrlReconComment, Integer>{

}
