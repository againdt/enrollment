/**
 * 
 */
package com.getinsured.hix.enrollment.repository;


import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepository;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

/**
 * @author panda_p
 * 
 */
public interface IEnrolleePrivateRepository extends TenantAwareRepository<Enrollee, Integer>,
		EnversRevisionRepository<Enrollee, Integer, Integer> {

	Enrollee findById(int enrolleeId);

	@Query("FROM Enrollee as en WHERE en.updatedOn >=  :lastRunDate and en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>'PENDING'")
	List<Enrollee> getCarrierUpdatedEnrollments(
			@Param("lastRunDate") Date lastRunDate,
			@Param("updatedBy") Integer updatedBy);

	@Query("FROM Enrollee as en WHERE en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>'PENDING'")
	List<Enrollee> getCarrierUpdatedEnrollments(
			@Param("updatedBy") Integer updatedBy);

	@Query("FROM Enrollee as en WHERE en.exchgIndivIdentifier = :memberID and en.enrollment.id = :enrollmentID and en.enrollment.planId = :planID")
	List<Enrollee> findEnrolleeByEnrollmentIDAndMemberIDAndPlanID(
			@Param("memberID") String memberID,
			@Param("enrollmentID") Integer enrollmentID,
			@Param("planID") Integer planID);

	@Query("FROM Enrollee as en WHERE en.exchgIndivIdentifier = :memberID and en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'")
	Enrollee findEnrolleeByEnrollmentIDAndMemberID(
			@Param("memberID") String memberID,
			@Param("enrollmentID") Integer enrollmentID);

	@Query("FROM Enrollee as en WHERE en.exchgIndivIdentifier = :exchgIndivIdentifier")
	Enrollee getEnrolleeByExchgIndivIdentifier(
			@Param("exchgIndivIdentifier") String exchgIndivIdentifier);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' and en.enrolleeLkpValue.lookupValueCode = :status")
	List<Enrollee> getEnrolleeByStatusAndEnrollmentID(@Param("enrollmentID") Integer enrollmentID, @Param("status") String status);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' and en.enrolleeLkpValue.lookupValueCode IN ('CANCEL','TERM')")
	List<Enrollee> getSubscriberAndEnrollees(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'")
	List<Enrollee> getEnrolleeByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	List<Enrollee> getEnrolledEnrolleesForEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' and (en.createdOn >= :lastRunDate or en.updatedOn >=  :lastRunDate)")
	List<Enrollee> getEnrolleeByLastRunDate(@Param("enrollmentID") Integer enrollmentID, @Param("lastRunDate") Date lastRunDate);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode = 'RESPONSIBLE_PERSON'")
	Enrollee getResponsiblePersonByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	

	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode = 'HOUSEHOLD_CONTACT'")
	Enrollee getHouseholdContactByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("FROM Enrollee as en WHERE en.exchgIndivIdentifier = :memberID and en.enrollment.id = :enrollmentId AND en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	Enrollee findEnrolleeByEnrollmentIdAndMemberId(@Param("memberID") String memberID,@Param("enrollmentId") Integer enrollmentId);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentId and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' and (en.enrolleeLkpValue.lookupValueCode = 'PENDING' or en.enrolleeLkpValue.lookupValueCode='CONFIRM') ")
	List<Enrollee> getPenddingOrEnrollEnrollee(@Param("enrollmentId") Integer enrollmentId);

	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID")
	List<Enrollee> findEnrolleesByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("SELECT COUNT(en) FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	long totalActiveMembersByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode='SUBSCRIBER'")
	Enrollee findSubscriberByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("select en FROM Enrollment as en " +
			          "WHERE " +
						   "en.employeeId=:employeeId and "+
						   "en.enrollmentStatusLkp.lookupValueCode not in (:status)")
	List<Enrollment> getEnrollmentEmployeeIdAndEnrollmentStatus(@Param("employeeId") Integer employeeId, @Param("status") List<String> status);
	
	@Query("SELECT id FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode='SUBSCRIBER'")
	Integer findSubscriberIdByEnrollmentId(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("SELECT en.enrollment FROM Enrollee as en  WHERE en.exchgIndivIdentifier=:memberID and en.enrollment.employeeId= :employeeID and en.enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
	List<Enrollment> findActiveEnrollmentByEmployeeIDandMemberID(@Param("employeeID") Integer employeeID,@Param("memberID") String memberID );
	
	@Query("SELECT DISTINCT enrlee.id, " +
			   "enrlee.healthCoveragePolicyNo, " +
			   "enrlee.enrollment.enrollmentStatusLkp.lookupValueCode, " +
			   "enrlee.enrollment.enrollmentStatusLkp.lookupValueLabel, " +
			   "enrlee.enrollment.insuranceTypeLkp.lookupValueCode, " +
			   "enrlee.enrollment.enrollmentTypeLkp.lookupValueCode, " +
			   "enrlee.enrollment.planName, " +
			   "enrlee.enrollment.CMSPlanID, " +
			   "enrlee.enrollment.insurerName, " +
			   "enrlee.firstName, " +
			   "enrlee.middleName, " +
			   "enrlee.lastName, " +
			   "enrlee.effectiveStartDate, " +
			   "enrlee.enrollment.employer.id, " +
			   "enrlee.enrollment.employer.name, " +
			   "enrlee.enrollment.benefitEffectiveDate, " +
			   "enrlee.enrollment.benefitEndDate " +
	   "FROM Enrollee enrlee " +
	   "WHERE " +
	   " enrlee.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' AND " +
	   " enrlee.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' AND " +
	   " enrlee.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' AND "+
	   " ( ((:healthCoveragePolicyNo IS NOT NULL) AND (enrlee.healthCoveragePolicyNo =:healthCoveragePolicyNo)) OR ( ( :healthCoveragePolicyNo IS NULL) AND ((enrlee.healthCoveragePolicyNo IS NULL) OR (enrlee.healthCoveragePolicyNo IS NOT NULL)) ) ) AND " +
	   " ( ((:enrollmentStatusLkp IS NOT NULL) AND (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode =:enrollmentStatusLkp)) OR ( ( :enrollmentStatusLkp IS NULL) AND ((enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NOT NULL)) ) ) AND " +
	   " ( ((:plantype IS NOT NULL) AND (enrlee.enrollment.insuranceTypeLkp.lookupValueCode =:plantype)) OR ( ( :plantype IS NULL) AND ((enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NOT NULL)) ) ) AND " +
	   " ( ((:plannumber IS NOT NULL) AND (enrlee.enrollment.CMSPlanID =:plannumber)) OR ( ( :plannumber IS NULL) AND ((enrlee.enrollment.CMSPlanID IS NULL) OR (enrlee.enrollment.CMSPlanID IS NOT NULL)) ) ) AND " +
	   " ( ((:issuer <> 0) AND (enrlee.enrollment.issuerId =:issuer)) OR ( ( :issuer = 0) AND (enrlee.enrollment.issuerId > 0) )) AND " +
	   " ( ((:employerName IS NOT NULL) AND (enrlee.enrollment.employer.name =:employerName)) OR ( ( :employerName IS NULL) AND ((enrlee.enrollment.employer.name IS NULL) OR (enrlee.enrollment.employer.name IS NOT NULL)) ) ) AND " +
	   " ((  (:name IS NOT NULL) AND ( LOWER(enrlee.firstName) ||' '|| LOWER(enrlee.middleName) ||' '||LOWER(enrlee.lastName)  LIKE '%' || LOWER(:name) || '%')  OR ( ( :name IS NULL) AND ((enrlee.firstName IS NULL) OR (enrlee.firstName IS NOT NULL)) )) OR " +
	   " ( (:name IS NOT NULL) AND (LOWER(enrlee.firstName) ||' '|| LOWER(enrlee.lastName) LIKE '%' || LOWER(:name) || '%')  ) OR ( ( :name IS NULL) AND ((enrlee.lastName IS NULL) OR (enrlee.lastName IS NOT NULL)) )) AND " +
		" ( ((:enrollmentTypeLkp IS NOT NULL) AND (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode =:enrollmentTypeLkp)) OR ((:enrollmentTypeLkp IS NULL) AND ((enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NOT NULL) )) )")
Page<Object[]> getShopEnrolleeByIssuerData(@Param("healthCoveragePolicyNo") String healthCoveragePolicyNo,
										   @Param("enrollmentStatusLkp") String enrollmentStatusLkp,
										   @Param("plantype") String plantype,
										   @Param("plannumber") String plannumber,
										   @Param("issuer") int issuer,
										   @Param("employerName") String employerName, 
										   @Param("name") String name,
										   @Param("enrollmentTypeLkp") String enrollmentTypeLkp,
										   Pageable pageable);

@Query("SELECT DISTINCT enrlee.id, " +
		   "enrlee.healthCoveragePolicyNo, " +
		   "enrlee.enrollment.enrollmentStatusLkp.lookupValueCode, " +
		   "enrlee.enrollment.enrollmentStatusLkp.lookupValueLabel, " +
		   "enrlee.enrollment.insuranceTypeLkp.lookupValueCode, " +
		   "enrlee.enrollment.enrollmentTypeLkp.lookupValueCode, " +
		   "enrlee.enrollment.planName, " +
		   "enrlee.enrollment.CMSPlanID, " +
		   "enrlee.enrollment.insurerName, " +
		   "enrlee.firstName, " +
		   "enrlee.middleName, " +
		   "enrlee.lastName, " +
		   "enrlee.effectiveStartDate, " +
		   "enrlee.enrollment.benefitEffectiveDate, " +
		   "enrlee.enrollment.benefitEndDate " +
"FROM Enrollee enrlee " +
"WHERE " +
" enrlee.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' AND " +
" enrlee.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' AND " +
" enrlee.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' AND "+
" ( ((:healthCoveragePolicyNo IS NOT NULL) AND (enrlee.healthCoveragePolicyNo =:healthCoveragePolicyNo)) OR ( ( :healthCoveragePolicyNo IS NULL) AND ((enrlee.healthCoveragePolicyNo IS NULL) OR (enrlee.healthCoveragePolicyNo IS NOT NULL)) ) ) AND " +
" ( ((:enrollmentStatusLkp IS NOT NULL) AND (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode =:enrollmentStatusLkp)) OR ( ( :enrollmentStatusLkp IS NULL) AND ((enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NOT NULL)) ) ) AND " +
" ( ((:plantype IS NOT NULL) AND (enrlee.enrollment.insuranceTypeLkp.lookupValueCode =:plantype)) OR ( ( :plantype IS NULL) AND ((enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NOT NULL)) ) ) AND " +
" ( ((:plannumber IS NOT NULL) AND (enrlee.enrollment.CMSPlanID =:plannumber)) OR ( ( :plannumber IS NULL) AND ((enrlee.enrollment.CMSPlanID IS NULL) OR (enrlee.enrollment.CMSPlanID IS NOT NULL)) ) ) AND " +
" ( ((:issuer <> 0) AND (enrlee.enrollment.issuerId =:issuer)) OR ( ( :issuer = 0) AND (enrlee.enrollment.issuerId > 0) )) AND " +
" ((  (:name IS NOT NULL) AND ( LOWER(enrlee.firstName) ||' '|| LOWER(enrlee.middleName) ||' '||LOWER(enrlee.lastName)  LIKE '%' || LOWER(:name) || '%')  OR ( ( :name IS NULL) AND ((enrlee.firstName IS NULL) OR (enrlee.firstName IS NOT NULL)) )) OR " +
" ( (:name IS NOT NULL) AND (LOWER(enrlee.firstName) ||' '|| LOWER(enrlee.lastName) LIKE '%' || LOWER(:name) || '%')  ) OR ( ( :name IS NULL) AND ((enrlee.lastName IS NULL) OR (enrlee.lastName IS NOT NULL)) )) AND " +
	" ( ((:enrollmentTypeLkp IS NOT NULL) AND (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode =:enrollmentTypeLkp)) OR ((:enrollmentTypeLkp IS NULL) AND ((enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NOT NULL) )) )")
Page<Object[]> getIndividualEnrolleeByIssuerData(@Param("healthCoveragePolicyNo") String healthCoveragePolicyNo,
									   @Param("enrollmentStatusLkp") String enrollmentStatusLkp,
									   @Param("plantype") String plantype,
									   @Param("plannumber") String plannumber,
									   @Param("issuer") int issuer, 
									   @Param("name") String name, 
									   @Param("enrollmentTypeLkp") String enrollmentTypeLkp,
									   Pageable pageable);

    @Query("select e from Enrollee e where e.enrollment.id = :enrollmentId and e.personTypeLkp.lookupValueCode = :lookupValueCode")
    Enrollee findEnrolleeByEnrollmentIdAndPersonTypeLkp_LookupValueCode(@Param("enrollmentId") Integer enrollmentId, @Param("lookupValueCode") String lookupValueCode);

    
    @Query("SELECT enrlee.firstName || ' ' || enrlee.lastName, "
    		+ "enrlee.preferredEmail, "
    		+ "enrlee.birthDate, "
    		+ "enrlee.primaryPhoneNo, "
    		+ "enrlee.personTypeLkp.lookupValueCode "
    		+ "FROM Enrollee as enrlee "
    		+ "WHERE "
    		+ "enrlee.enrollment.id = :enrollmentId "
    		+ "AND enrlee.personTypeLkp.lookupValueCode NOT IN  ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT', 'CUSTODIAL_PARENT') ")
    List<Object[]> getEnrolleeCapDetails(@Param("enrollmentId") Integer enrollmentId);
    
    @Query("SELECT en.enrollment.id  FROM Enrollee as en WHERE en.enrollment.id in (:enrollmentIDs) and en.personTypeLkp.lookupValueCode='SUBSCRIBER' and en.homeAddressid.state = :stateCode")
	List<Integer> getMatchedSubscriberByState(@Param("stateCode") String stateCode, @Param("enrollmentIDs") Set<Integer> enrollmentIDs);
}

