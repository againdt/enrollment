/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepository;

/**
 * @author panda_p
 *
 */
public interface IEnrolleeRacePrivateRepository extends JpaRepository<EnrolleeRace, Integer>, EnversRevisionRepository<EnrolleeRace, Integer, Integer> {
	@Modifying
	@Transactional
	@Query ("DELETE FROM EnrolleeRace en " +
			" WHERE en.enrollee = :enrollee")
	void deleteByEnrollee(@Param("enrollee") Enrollee enrollee);
}
