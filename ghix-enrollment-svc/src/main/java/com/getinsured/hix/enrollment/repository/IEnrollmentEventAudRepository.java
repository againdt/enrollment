package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.enrollment.EnrollmentEventAud;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

/**
 * @since 20th July 2015
 * @author Sharma_k
 *
 */
public interface IEnrollmentEventAudRepository extends
TenantAwareRepository<EnrollmentEventAud, AudId>
{
	/**
	 * Fetching EnrollmentEvent where No SpecialEnrollment event happened.
	 * @param enrollmentId
	 * @param personTypeList
	 * @return
	 */
	@Query("SELECT ev.rev, ev.createdOn, ev.eventReasonLkp.lookupValueLabel,"
			+ " ev.eventTypeLkp.lookupValueLabel, ev.createdBy.firstName,"
			+ " ev.createdBy.lastName, ee.firstName, ee.middleName,"
			+ " ee.lastName"
			+ " FROM EnrollmentEventAud as ev, Enrollee as ee "
			+ " WHERE ev.enrollee.id = ee.id AND ev.enrollment.id = :enrollmentId AND"
			+ " ev.spclEnrollmentReasonLkp IS NULL AND ev.revType = 0 AND"
			+ " ev.enrollee.personTypeLkp.lookupValueCode NOT IN (:personTypeList) ORDER BY ev.createdOn")
	List<Object[]> getEnrollmentEventHistoryData(@Param("enrollmentId") Integer enrollmentId, @Param("personTypeList")List<String> personTypeList);
	
	/**
	 * Fetching EnrollmentEvent where SpecialEnrollment event happened.
	 * @param enrollmentId
	 * @param personTypeList
	 * @return
	 */
	@Query("SELECT ev.rev, ev.createdOn, ev.eventReasonLkp.lookupValueLabel,"
			+ " ev.eventTypeLkp.lookupValueLabel, ev.createdBy.firstName,"
			+ " ev.createdBy.lastName, ee.firstName, ee.middleName,"
			+ " ee.lastName, ev.spclEnrollmentReasonLkp.lookupValueLabel"
			+ " FROM EnrollmentEventAud as ev, Enrollee as ee "
			+ " WHERE ev.enrollee.id = ee.id AND ev.enrollment.id = :enrollmentId AND"
			+ " ev.spclEnrollmentReasonLkp IS NOT NULL AND ev.revType = 0 AND"
			+ " ev.enrollee.personTypeLkp.lookupValueCode NOT IN (:personTypeList) ORDER BY ev.createdOn")
	List<Object[]> getSpclEnrollmentEventHistoryData(@Param("enrollmentId") Integer enrollmentId, @Param("personTypeList")List<String> personTypeList);
	
	/*@Query("SELECT MAX(ev.rev) FROM EnrollmentEventAud as ev WHERE ev.eventTypeLkp.lookupValueCode='024' and ev.enrollment.id = :enrollmentId and ev.createdOn = (SELECT MAX(eve.createdOn) FROM EnrollmentEventAud as eve WHERE eve.eventTypeLkp.lookupValueCode='024' and eve.enrollment.id = :enrollmentId)")*/
	
	@Query("SELECT MIN(evAud.rev) FROM EnrollmentEventAud as evAud "
			+ " WHERE evAud.id = (SELECT MAX(ev.id) FROM EnrollmentEvent as ev WHERE ev.eventTypeLkp.lookupValueCode='024' and ev.enrollment.id = :enrollmentId "
			+ " AND ev.enrollee.id = (SELECT ee.id FROM Enrollee ee WHERE ee.enrollment.id = :enrollmentId AND ee.personTypeLkp.lookupValueCode IN('SUBSCRIBER') ))"
			+ " AND evAud.updatedOn = (SELECT MIN(eve.updatedOn) FROM EnrollmentEventAud as eve WHERE eve.id = evAud.id)")
	Integer getLatestSubscriberDisEnrollmentRevision(@Param("enrollmentId") Integer enrollmentId);
	
	@Query("SELECT MIN(evAud.rev) FROM EnrollmentEventAud as evAud "
			+ " WHERE evAud.enrollment.id = :enrollmentId AND evAud.eventTypeLkp.lookupValueCode='024' AND evAud.rev > :rev "
			+ " AND evAud.createdOn > (SELECT max(ev.createdOn) FROM EnrollmentEventAud as ev WHERE ev.enrollment.id = :enrollmentId AND ev.rev = :rev) ")
	Integer getMemberDisenrolledAfterSubscriber(@Param("rev") Integer rev, @Param("enrollmentId") Integer enrollmentId);
	
	
	@Query("SELECT COUNT(*) FROM EnrollmentEventAud as ev WHERE ev.spclEnrollmentReasonLkp IS NOT NULL AND ev.enrollment.id = :enrollmentId AND ev.rev= :revId ")
	long getSpecialEventCount(@Param("enrollmentId") Integer enrollmentId, @Param("revId") Integer revId);
	
	@Query("SELECT MAX(eve.createdOn) FROM EnrollmentEventAud as eve WHERE (eve.createdBy IS NULL OR eve.createdBy.id <> :carrierId) and eve.enrollment.id = :enrollmentId and (eve.sendToCarrierFlag is NULL OR eve.sendToCarrierFlag = 'true') ")
	Date getLatestNonCarrierRevisionDate(@Param("enrollmentId") Integer enrollmentId, @Param("carrierId") Integer carrierId);
	
	@Query("SELECT ev.rev FROM EnrollmentEventAud as ev WHERE ev.id = :eventId AND revType = 0")
	Integer getRevIdByEventId(@Param("eventId")Integer eventId);
	
	@Query("SELECT ev.createdOn, ev.eventReasonLkp.lookupValueLabel,"
			+ " ev.eventTypeLkp.lookupValueLabel, ev.createdBy.firstName,"
			+ " ev.createdBy.lastName, ee.firstName, ee.middleName,"
			+ " ee.lastName,"
			+ " ev.rev,"
			+ " ev.id,"
			+ " ev.createdBy.id"
			+ " FROM EnrollmentEventAud as ev, Enrollee as ee "
			+ " WHERE ev.enrollee.id = ee.id AND ev.enrollment.id = :enrollmentId AND"
			+ " ev.enrollee.personTypeLkp.lookupValueCode = 'SUBSCRIBER' ")
	List<Object[]> findSubscriberEventsbyEnrollmentId(@Param("enrollmentId") Integer enrollmentId);

	@Query(" SELECT ev.eventTypeLkp.lookupValueLabel, ev.eventReasonLkp.lookupValueLabel, ev.createdOn, ev.createdBy.id"
			+ " from EnrollmentEventAud as ev "
			+ " WHERE ev.enrollment.id = :enrollmentId "
			+ " AND ev.rev = :rev AND"
			+ " ev.enrollee.personTypeLkp.lookupValueCode = 'SUBSCRIBER' ")
	List<Object[]> findSubscriberEventsbyEnrollmentIdAndRev(@Param("enrollmentId") Integer enrollmentId , @Param("rev") Integer rev);
	
	
	@Query(" SELECT ev.eventTypeLkp.lookupValueLabel, ev.eventReasonLkp.lookupValueLabel from EnrollmentEventAud as ev "
			+ " WHERE ev.enrollee.id = :enrolleeId "
			+ " AND ev.rev = :rev "
			+ " ORDER BY ev.createdOn DESC ")
	List<Object[]> findEnrolleeEventsbyEnrolleeIdAndRev(@Param("enrolleeId") Integer enrolleeId , @Param("rev") Integer rev);
}
