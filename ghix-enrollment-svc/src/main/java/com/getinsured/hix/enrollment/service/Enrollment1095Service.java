package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.AccountUser;

public interface Enrollment1095Service {
	
	/**
	 * 
	 * @param String householdCaseID
	 * @param Integer coverageYear
	 * @param Object<AccountUser.class> accountUser
	 * @param String phoneNumber
	 * @return
	 */
	boolean resendEnrollment1095(String householdCaseID, Integer coverageYear, AccountUser accountUser, String phoneNumber);
	
	/**
	 * 
	 * @param String householdCaseID
	 * @return
	 */
	String fetchEnrollment1095DataByHouseholdCaseId(String householdCaseID);

}
