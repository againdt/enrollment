package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.consumer.history.ConsumerEnrollEventHistory;
import com.getinsured.hix.consumer.history.ConsumerEventHistory;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventPrivateRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.platform.util.GhixEndPoints;

@Service("enrollmentEventPrivateService")
@Transactional
public class EnrollmentEventPrivateServiceImpl implements EnrollmentEventPrivateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEventPrivateServiceImpl.class);
	@Autowired private IEnrollmentEventPrivateRepository enrollmentEventRepository;
	@Autowired private RestTemplate restTemplate;

	@Override
	public void saveEnrollmentEvent(EnrollmentEvent enrollmentEvent){
	
		enrollmentEventRepository.saveAndFlush(enrollmentEvent);
	
	}
	@Override
	public EnrollmentEvent findEnrollmentByeventIDAndplanID(Integer eventID,
			Integer planID){
		EnrollmentEvent enrollmentEvent=null;
		
		enrollmentEvent= enrollmentEventRepository.findEnrollmentByeventIDAndplanID(eventID, planID);
		
		return enrollmentEvent;
	}

	@Override
	public List<EnrollmentEvent> findEventByenrollmentIDAndEnrolleeIDAndCreatedDate(Integer enrollmentID, Integer enrolleeID, Date createdDate){
	
		return enrollmentEventRepository.findEventByenrollmentIDAndEnrolleeIDAndCreatedDate(enrollmentID, enrolleeID, createdDate);
	}
	
	
	@Override
	public String saveConsumerErnrollmentEvent(Enrollment manualEnroll, String eventName , String prevEnrollmentStatus) {
//		ObjectMapper mapper = new ObjectMapper();
		ConsumerEnrollEventHistory enrollEventHistory = new ConsumerEnrollEventHistory();
		enrollEventHistory.setEffectiveDate(manualEnroll.getBenefitEffectiveDate());
		enrollEventHistory.setEnrollmentId(manualEnroll.getId());
		enrollEventHistory.setInsureanceCompany(manualEnroll.getInsurerName());
		enrollEventHistory.setPlanName(manualEnroll.getPlanName());
		enrollEventHistory.setPolicyId(manualEnroll.getPlanId());
		enrollEventHistory.setPolicyNumber(manualEnroll.getIssuerAssignPolicyNo());
		
		if(isNotNullAndEmpty(eventName)){
			String event = "";
			if(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString().equals(eventName)){
				event = EnrollmentConstants.ENROLLMENT_SUBMITTED;
			}
			else if(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_TERMINATED.toString().equals(eventName)){
				event = EnrollmentConstants.ENROLLMENT_TERMINATED;
			}
			else if(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_INFORCE.toString().equals(eventName)){
				event = EnrollmentConstants.ENROLLMENT_INFORCE;
			}
			else if(EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_STATUS_CHANGED.toString().equals(eventName)){
				event = EnrollmentConstants.ENROLLMENT_STATUS_CHANGED;
			}
			enrollEventHistory.setEventName(event);
		}
		
		if(isNotNullAndEmpty( manualEnroll.getInsuranceTypeLkp().getLookupValueLabel())){
			enrollEventHistory.setPlanType( manualEnroll.getInsuranceTypeLkp().getLookupValueLabel());
		}
		if(isNotNullAndEmpty(manualEnroll.getEnrollmentModalityLkp())){
			enrollEventHistory.setExchangeType(manualEnroll.getEnrollmentModalityLkp().getLookupValueLabel());
		}
		if(isNotNullAndEmpty(manualEnroll.getEnrollmentModalityLkp())){
			enrollEventHistory.setMonthlyPremium(manualEnroll.getGrossPremiumAmt().toString());
		}
		if(isNotNullAndEmpty(manualEnroll.getAptcAmt()) 
				&& (isNotNullAndEmpty(manualEnroll.getExchangeTypeLkp()) && manualEnroll.getExchangeTypeLkp().getLookupValueCode().equalsIgnoreCase("ONEXCHANGE"))
				&& (isNotNullAndEmpty(eventName) && EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString().equals(eventName))
		  ){
			enrollEventHistory.setAptc(manualEnroll.getAptcAmt().toString());
		}
		if(isNotNullAndEmpty(manualEnroll.getUpdatedBy())){
			enrollEventHistory.setUpdatedBy(manualEnroll.getUpdatedBy().getId());
		}
		if(isNotNullAndEmpty(manualEnroll.getUpdatedOn())){
			enrollEventHistory.setUpdatedOn(manualEnroll.getUpdatedOn());
		}
		if( isNotNullAndEmpty(eventName) && EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_STATUS_CHANGED.toString().equals(eventName)){
			enrollEventHistory.setPreviousEnrollmentStatus(prevEnrollmentStatus);
			if(isNotNullAndEmpty(manualEnroll.getEnrollmentStatusLkp())){
				enrollEventHistory.setCurrentEnrollmentStatus(manualEnroll.getEnrollmentStatusLkp().getLookupValueLabel());
				enrollEventHistory.setEnrollmentStatus(manualEnroll.getEnrollmentStatusLkp().getLookupValueLabel());
			}
		}
		
		return callConsumerEventLogAPI(manualEnroll.getCreatedOn(),manualEnroll.getCreatedBy().getId(),manualEnroll.getCmrHouseHoldId(), enrollEventHistory);
	}
	
	
	/**
	 * @author shinde_dh
	 * @since 31/12/2015
	 * @param enrollCreatedOn
	 * @param userId
	 * @param cmrHouseHoldId
	 * @param enrollEventHistory
	 * @return callResponse String
	 */
	private String callConsumerEventLogAPI(Date enrollCreatedOn,Integer userId,Integer cmrHouseHoldId, ConsumerEnrollEventHistory enrollEventHistory)
	{
		String callResponse = null;
		try 
		{
			ConsumerEventHistory consumerEventHistory = new ConsumerEventHistory(enrollCreatedOn);
			
			if(isNotNullAndEmpty(userId)){
				consumerEventHistory.setCreatedBy(userId);
			}
			consumerEventHistory.setDatetime(enrollCreatedOn);
			consumerEventHistory.setEventMetaData(enrollEventHistory);
	
			consumerEventHistory.setEventLevel("Enrollment Service");
			consumerEventHistory.setEventSource("Enrollment");
			if(isNotNullAndEmpty(cmrHouseHoldId)){
				consumerEventHistory.setHouseholdId(cmrHouseHoldId);
			}
			consumerEventHistory.setDescription("Desc");
			callResponse = restTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.SAVE_CONSUMER_EVENT_LOG,consumerEventHistory, String.class);
		} catch (RestClientException e) {
			LOGGER.error("Exception in calling Consumer Service :: saveConsumerErnrollmentEvent () in enrollmentEventPrivateService",e);
		}
		return callResponse;	
	}
}
