package com.getinsured.hix.enrollment.service.carrierfeed;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

enum BatchProcessingStatus {
	INPROGRESS, FAILED
}

enum RecordProcessingStatus {
	FAIL, SUCCESS, IGNORED
}

public class CarrierFeedUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(CarrierFeedUtils.class);

	public static final Long dateMatchingRange = 7l;

	public static enum Match {
		YES, NO, LNOTAVAILABLE, RNOTAVAILABLE
	}

	public static final SimpleDateFormat DATE_FORMAT_YYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");

	public static Match isMatch(float left, float right) {
		if (Float.compare(left, right) == 0) {
			return Match.YES;
		}
		return Match.NO;
	}

	public static Match isMatchDateWithoutTime(Date left, Date right) {

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date leftDate = sdf.parse(sdf.format(left));
			Date rightDate = sdf.parse(sdf.format(right));
			if (leftDate == null) {
				return Match.LNOTAVAILABLE;
			}
			if (rightDate == null) {
				return Match.RNOTAVAILABLE;
			}
			if (isDateMatch(leftDate, rightDate)) {
				return Match.YES;
			}
		} catch (Exception e) {
			LOGGER.error("Error Parsing Date", e);
			return Match.NO;
		}
		return Match.NO;

	}

	public static float getFloatOrNull(String val) {
		try {

			return Float.parseFloat(val);
		} catch (Exception e) {
			LOGGER.error("Error Parsing Float", e);
			return 0;
		}
	}

	@SuppressWarnings("deprecation")
	public static boolean isDateMatch(Date date1, Date date2) {
		if (date1 == null || date2 == null) {
			return false;
		}

		return date1.getYear() == date2.getYear() && date1.getMonth() == date2.getMonth()
				&& date1.getDay() == date2.getDay();
	}

	public static Match isMatchDateRangeWithoutTime(Date leftDate, Date rightDate) {

		try {
			if (leftDate == null)
				return Match.LNOTAVAILABLE;
			if (rightDate == null)
				return Match.RNOTAVAILABLE;
			if (isDateRangeMatch(leftDate, rightDate)) {
				return Match.YES;
			}
		} catch (Exception e) {
			return Match.NO;
		}
		return Match.NO;

	}

	public static boolean isDateRangeMatch(Date date1, Date date2) {
		if (date1 == null || date2 == null) {
			return false;
		}

		LocalDate localDate1 = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(date1));
		LocalDate localDate2 = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(date2));

		LocalDate localDate1RangeFrom = localDate1.minus(dateMatchingRange, ChronoUnit.DAYS);
		LocalDate localDate1RangeTo = localDate1.plus(dateMatchingRange, ChronoUnit.DAYS);

		return (!localDate2.isBefore(localDate1RangeFrom)) && (!localDate2.isAfter(localDate1RangeTo));
	}

	public static boolean checkDateWithRange(Date date1, Date date2, int days) {
		if (date1 == null || date2 == null) {
			return false;
		}

		LocalDate localDate1 = LocalDate.parse(DATE_FORMAT_YYYYMMDD.format(date1));
		LocalDate localDate2 = LocalDate.parse(DATE_FORMAT_YYYYMMDD.format(date2));

		if (localDate1.isAfter(localDate2)) {
			return false;
		}

		localDate1 = localDate1.plus(days, ChronoUnit.DAYS);

		return !localDate1.isBefore(localDate2);
	}

	public static void copyNonNullProperties(Object src, Object target) {
		BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
	}

	public static String[] getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		for (java.beans.PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null)
				emptyNames.add(pd.getName());
		}
		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}

}
