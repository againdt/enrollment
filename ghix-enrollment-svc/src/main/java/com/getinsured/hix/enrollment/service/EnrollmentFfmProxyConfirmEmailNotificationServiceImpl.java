/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentOffExchangeEmailDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.email.EnrollmentFfmProxyConfirmEmailNotification;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentPlan;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.notification.ConfigurationService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixUtils.EMAIL_STATS;
import com.google.gson.Gson;

/**
 * @author negi_s
 *
 */
@Service("enrollmentFfmProxyConfirmEmailNotificationService")
public class EnrollmentFfmProxyConfirmEmailNotificationServiceImpl implements
		EnrollmentFfmProxyConfirmEmailNotificationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentFfmProxyConfirmEmailNotificationServiceImpl.class);
	@Autowired private EnrollmentFfmProxyConfirmEmailNotification emailNotification;
	@Autowired private GhixRestTemplate restTemplate;
	@Autowired private ConfigurationService configurationService;
	@Autowired private Gson platformGson;
	
	private static final int ASCII_SPACE = 32;
	private static final int ASCII_NINE = 57;
	private static final int ASCII_PERCENTAGE = 37;
	private static final int ASCII_DOLLAR = 36;
	private static final String EMPTY_STRING = "";
	

	/* (non-Javadoc)
	 * @see com.getinsured.hix.enrollment.service.EnrollmentFfmProxyConfirmEmailNotificationService#sendEmailNotification(com.getinsured.hix.model.consumer.Household, com.getinsured.hix.model.enrollment.Enrollment)
	 */
	@Override
	public void sendEmailNotification(Household household, Enrollment enrollment)
			throws NotificationTypeNotFound {
		
		LOGGER.info("Email Process :: Start");
		EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO = null;
		Notice noticeObj =null;
		String phoneNum = null;
		String logoUrl = null;
		String redirectUrl = null;
		String emailFrom = null;
		Long tenantId = null;
		Integer flowId = null;
		Long affiliateId = null;
		String customerCompanyName = null;
		String disclaimerContent = null;

		PlanResponse planResponse = getPlanInfo(enrollment);
		
		tenantId = enrollment.getTenantId();
		EligLead eligLead = household.getEligLead();
		if(null != eligLead){
			flowId = eligLead.getFlowId();
			affiliateId = eligLead.getAffiliateId();
		}else{
			LOGGER.error("ENROLLMENT-SendEmailNotification setCommonData: eligLead is null for HOUSEHOLD_ID = " + household.getId());
		}
		
		if(null != flowId && 0 == flowId){
			flowId = eligLead.getFlowId() > 0 ? eligLead.getFlowId() : null;
			LOGGER.error("ENROLLMENT-SendEmailNotification setCommonData: Flow ID is null for CMR_HOUSEHOLD_ID = " + household.getId());
		}
		if(null != affiliateId && 0 == affiliateId){
			affiliateId = eligLead.getAffiliateId() > 0 ? eligLead.getAffiliateId() : null;
			LOGGER.error("ENROLLMENT-SendEmailNotification setCommonData: Affiliate ID is null for CMR_HOUSEHOLD_ID = " + household.getId());
		}
		
		//Creating tracking attributes for the email
		EnumMap<EMAIL_STATS, String> notificationTrackingAttributes = new EnumMap<EMAIL_STATS, String>(EMAIL_STATS.class);
		notificationTrackingAttributes.put(EMAIL_STATS.FLOW_ID, String.valueOf(flowId));
		notificationTrackingAttributes.put(EMAIL_STATS.AFFILIATE_ID, String.valueOf(affiliateId));
		notificationTrackingAttributes.put(EMAIL_STATS.TENANT_ID, String.valueOf(tenantId));
		
		phoneNum = configurationService.customerCareNum(flowId, affiliateId, tenantId);
		logoUrl = configurationService.logoUrl(flowId, affiliateId, tenantId);
		redirectUrl = configurationService.baseUrl(flowId, affiliateId, tenantId);
		emailFrom = configurationService.fromEmailAddress(affiliateId, tenantId);
		customerCompanyName = configurationService.getCompanyName(flowId, affiliateId, tenantId);
		disclaimerContent = configurationService.getDisclaimerTextForEmailFooter(flowId, affiliateId, tenantId);
		
		if(null != phoneNum && phoneNum.length() == 10){
			String phoneFormat = "(%s) %s-%s";
			phoneNum = String.format(phoneFormat, phoneNum.substring(0, 3), phoneNum.substring(3, 6), phoneNum.substring(6, 10));
		}
		
		EnrollmentPlan enrollmentPlan = enrollment.getEnrollmentPlan();
		if(null != enrollmentPlan){
		 enrollmentOffExchangeEmailDTO=setCommonData(enrollmentPlan, household,  enrollment);	
		}

		if(null != enrollmentOffExchangeEmailDTO){
			enrollmentOffExchangeEmailDTO.setPhoneNum(phoneNum);
			enrollmentOffExchangeEmailDTO.setLogoUrl(logoUrl);
			enrollmentOffExchangeEmailDTO.setRedirectUrl(redirectUrl);
			enrollmentOffExchangeEmailDTO.setEmailFrom(emailFrom);
			enrollmentOffExchangeEmailDTO.setCompanyName(customerCompanyName);
			enrollmentOffExchangeEmailDTO.setDisclaimerContent(disclaimerContent);

			String insuranceType = enrollment.getInsuranceTypeLkp().getLookupValueLabel().toLowerCase();
			if(null != enrollmentOffExchangeEmailDTO){
				enrollmentOffExchangeEmailDTO.setInsuranceType(insuranceType);
			}
			enrollmentOffExchangeEmailDTO.setCompanyLogo((null != planResponse.getIssuerLogo()) ? planResponse.getIssuerLogo() : EMPTY_STRING);
		}
		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("enrollmentOffExchangeEmailDTO", enrollmentOffExchangeEmailDTO);
		emailNotification.setEnrollmentOffExchangeEmailDTO(enrollmentOffExchangeEmailDTO);	
		noticeObj=emailNotification.generateEmail(emailNotification.getClass().getSimpleName(), null, emailNotification.getEmailData(notificationContext), emailNotification.getTokens(notificationContext));
		emailNotification.sendEmail(noticeObj, notificationTrackingAttributes);
		
		LOGGER.info("Email Process :: End");

	}
	private EnrollmentOffExchangeEmailDTO setCommonData(EnrollmentPlan enrollmentPlan, Household household, Enrollment enrollment) {
		
		EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO = new EnrollmentOffExchangeEmailDTO();
		
		String recipientName=household.getFirstName()+" "+household.getLastName();
		enrollmentOffExchangeEmailDTO.setRecipient(household.getEmail());
		enrollmentOffExchangeEmailDTO.setRecipientName(recipientName);
		enrollmentOffExchangeEmailDTO.setInsuranceCompany(enrollment.getInsurerName());
		enrollmentOffExchangeEmailDTO.setNetPremiumAmount("$"+String.format("%.2f", enrollment.getNetPremiumAmt()));
		/*
		 * Set Plan Related Data here
		 */
		enrollmentOffExchangeEmailDTO.setPlanName(enrollment.getPlanName());
		enrollmentOffExchangeEmailDTO.setDeductible("$0");
		enrollmentOffExchangeEmailDTO.setMaxOop("$0");
		if(enrollmentPlan!=null){
			enrollmentOffExchangeEmailDTO.setPlanType(enrollmentPlan.getNetworkType());
			if(null != enrollmentPlan.getIndivDeductible()){
				enrollmentOffExchangeEmailDTO.setDeductible("$"+enrollmentPlan.getIndivDeductible().intValue());
			}
			if(null != enrollmentPlan.getIndivOopMax()){
				enrollmentOffExchangeEmailDTO.setMaxOop("$"+enrollmentPlan.getIndivOopMax().intValue());
			}
			String officeVisit = getNumericPart(enrollmentPlan.getOfficeVisit());
			enrollmentOffExchangeEmailDTO.setOfficeVisit("$"+(( officeVisit != null) ? officeVisit : "0"));
			enrollmentOffExchangeEmailDTO.setGenericMedication("$"+((getNumericPart(enrollmentPlan.getGenericMedication()) != null) ? getNumericPart(enrollmentPlan.getGenericMedication()) : "0"));
		}
		return enrollmentOffExchangeEmailDTO;
	}

	/**
	 * Fetch numeric portion of string
	 * @param input
	 * @return
	 */
	private String getNumericPart(String input){
		if(input==null || input.trim().length()<1){
			return null;
		}
		StringBuilder sb = new StringBuilder();
		char[] inpArray = input.toCharArray();
		for(char c : inpArray){
			if(c==ASCII_SPACE || c>ASCII_NINE || c==ASCII_PERCENTAGE ) {
				break;
			}else if(c == ASCII_DOLLAR){
				continue;
			}
			sb.append(c);
		}
		try{
			Float.parseFloat(sb.toString());
		}catch(NumberFormatException ex){
			LOGGER.error(ex.getMessage() + sb.toString());
			return null;
		}
		return sb.toString();
	}
	
	/**
	 * Fetch plan related information from Plan Management API
	 * @param enrollment
	 * @return planDuration
	 */
	private PlanResponse getPlanInfo(Enrollment enrollment){
		
		PlanRequest planRequest = new PlanRequest();
		String planId =  ""+enrollment.getPlanId();
		if(!EnrollmentPrivateUtils.isNotNullAndEmpty(planId) && null != enrollment.getEnrollmentPlan()){
			planId = ""+enrollment.getEnrollmentPlan().getPlanId();
		}
		LOGGER.info("Fetching Plan Data from Plan Management API :: " + planId);
		
		planRequest.setPlanId(planId);
		planRequest.setMarketType(GhixConstants.INDIVIDUAL.toUpperCase());
        
		String planResponseStr = "";
		PlanResponse planResponse = null;
		try {
			planResponseStr = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL, planRequest,String.class);
		} catch (Exception e) {
			LOGGER.error("Error in fetching plan details", e);
		}
		if(!"".equalsIgnoreCase(planResponseStr)){
			planResponse = (PlanResponse) platformGson.fromJson(planResponseStr, PlanResponse.class);	
		}
		return planResponse; 
	}
}
