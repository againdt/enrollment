package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.bb.BenefitBayRequestDTO;
import com.getinsured.hix.dto.cap.consumerapp.CapApplicationDTO;
import com.getinsured.hix.dto.cap.consumerapp.CapOnExchangeFFMAppDto;
import com.getinsured.hix.dto.cap.consumerapp.EffectiveApplicationDTO;
import com.getinsured.hix.dto.cap.consumerapp.EnrollmentDTO;
import com.getinsured.hix.dto.cap.consumerapp.PlanInfoDTO;
import com.getinsured.hix.dto.cap.consumerapp.PolicyLiteDTO;
import com.getinsured.hix.dto.cap.consumerapp.mapper.CapEnrollmentMapper;
import com.getinsured.hix.dto.cap.consumerapp.mapper.CapSSapApplicantMapper;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.enrollment.D2CEnrolleeDTO;
import com.getinsured.hix.dto.enrollment.D2CEnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateHouseholdDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.IssuerBrandNameResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.dto.shop.EmployerDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.enrollment.carrierfeed.util.CarrierFeedQueryUtil;
import com.getinsured.hix.enrollment.dto.BOBMultiRowProcessDetails;
import com.getinsured.hix.enrollment.dto.BOBMultipleRowRenewalModel;
import com.getinsured.hix.enrollment.dto.EnrlMissingCommissionDTO;
import com.getinsured.hix.enrollment.dto.EnrlMissingCommissionRequest;
import com.getinsured.hix.enrollment.dto.EnrlMissingCommissionResponse;
import com.getinsured.hix.enrollment.dto.EnrollmentBobDTO;
import com.getinsured.hix.enrollment.dto.EnrollmentBobRequest;
import com.getinsured.hix.enrollment.dto.EnrollmentBobResponse;
import com.getinsured.hix.enrollment.dto.EnrollmentBobResponse.MatchType;
import com.getinsured.hix.enrollment.dto.EnrollmentCommissionRequest;
import com.getinsured.hix.enrollment.dto.EnrollmentRenewalDTO;
import com.getinsured.hix.enrollment.dto.EnrollmentStarRequest;
import com.getinsured.hix.enrollment.dto.EnrollmentStarRequest.RenewalType;
import com.getinsured.hix.enrollment.dto.EnrollmentStarResponse.EnrollmentRenewalResponse;
import com.getinsured.hix.enrollment.dto.EnrollmentSubscriberDetailsDTO;
import com.getinsured.hix.enrollment.dto.EnrollmentUpdateDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleePrivateRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentCommissionRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEsignaturePrivateRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentIssuerCommisionRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPlanPrivateRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPrivateRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRenewalRepository;
import com.getinsured.hix.enrollment.service.carrierfeed.CarrierFeedResultRecord;
import com.getinsured.hix.enrollment.service.carrierfeed.CarrierFeedUtils;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentConstants.InsuranceTypeMappingEnum;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.EnrollmentUtilsForCap;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerEnrollment;
import com.getinsured.hix.model.EmployerEnrollment.Status;
import com.getinsured.hix.model.EmployerLocation;
import com.getinsured.hix.model.EnrollmentRenewals;
import com.getinsured.hix.model.Esignature;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.PldOrderItem;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeCapDto;
import com.getinsured.hix.model.enrollment.EnrolleeDTO;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentCapDto;
import com.getinsured.hix.model.enrollment.EnrollmentCommission;
import com.getinsured.hix.model.enrollment.EnrollmentEsignature;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentIssuerCommision;
import com.getinsured.hix.model.enrollment.EnrollmentPlan;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.EnrollmentSearchFilter;
import com.getinsured.hix.model.plandisplay.PldMemberData;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.model.plandisplay.PldPlanData;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.repository.TenantRepository;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.EsignatureService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.BrokerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * @author panda_p
 * @since 13/02/2013
 * 
 */

@Service("enrollmentPrivateService")
@Transactional
public class EnrollmentPrivateServiceImpl implements EnrollmentPrivateService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentPrivateServiceImpl.class);
	
	@Autowired	private IEnrollmentPrivateRepository enrollmentRepository;
	@Autowired	private IEnrollmentCommissionRepository enrollmentCommissionRepository;
	@Autowired	private IEnrollmentEsignaturePrivateRepository enrollmentEsignatureRepository;
	@Autowired	private IEnrolleePrivateRepository enrolleeRepository;
	@Autowired	private IEnrollmentPlanPrivateRepository enrollmentPlanRepository;
	@Autowired private RestTemplate restTemplate;
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired private LookupService lookupService;
	@Autowired private EsignatureService esignatureService;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private UserService userService;
	@Autowired private EnrolleePrivateService enrolleeService;
	@Autowired private EnrollmentEventPrivateService  enrollmentEventService;
	@Autowired private EnrollmentPartnerNotificationService enrollmentPartnerNotificationService;
	@Autowired private EnrollmentUserPrivateService enrollmentUserService;
	@Autowired	private IEnrollmentPrivateRepository ienrollmentRepository;
	@Autowired private IEnrollmentAudRepository iEnrollmentAudRepository;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	@Autowired private GIExceptionHandler giExceptionHandler;
	@Autowired private EnrollmentPrivateUtils enrollmentPrivateUtils;
	@Autowired private EnrollmentCommissionService enrollmentCommissionService;
	@Autowired private IEnrollmentRenewalRepository iEnrollmentRenewalRepository;
	@Autowired private Gson platformGson;
	@Autowired private IEnrollmentIssuerCommisionRepository enrollmentIssuerCommisionRepository;
	
	@Value("#{configProp['database.type']}")
	private String DB_TYPE;
	
	private static AccountUser STARRModule_defaultUser = null;
	
	static{
		STARRModule_defaultUser = new AccountUser();
		STARRModule_defaultUser.setId(1);
	}
	
	private static final String DATE_FORMAT = "MM/dd/yyyy";
	private static final String AGING_COMMISSION = "Aging Commissions";
	private static final String STR_EFFECTIVE_DATE = "effectiveDate"; 
	
	@Value("#{configProp['tenant.bb.userName']}")
	private String BENEFIT_BAY_USER;
	@PersistenceUnit private EntityManagerFactory  entityManagerFactory ;
	@Autowired private TenantRepository tenantRepository;
	
	/**
	 * @Since 26-Jul-2013
	 * @author panda_pratap
	 * @return Logged In AccountUser
	 */
	private AccountUser getLoggedInUser(){
		AccountUser accountUser =null;
		try {
			 accountUser = userService.getLoggedInUser();
		} catch (InvalidUserException invalidUserException) {
			LOGGER.error("Error occured in EnrollmentServiceImpl.getLoggedInUser" , invalidUserException);
		}
		return accountUser;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Enrollment findById(Integer id) {
//		return enrollmentRepository.findOne(id);
		return enrollmentRepository.findById(id); //Tenant Aware Implementation
	}
	@Override
	public Enrollment findEnrollmentByissuerPolicyId(String policyId) {
		return enrollmentRepository.findByissuerAssignPolicyNo(policyId);
	}

	@Override
	public Enrollment findEnrollmentByCarrierUID(String carrierAppUID) {
		return enrollmentRepository.findBycarrierAppUID(carrierAppUID);
	}
	@Override
	public void saveEnrollmentCommission(Enrollment enrollment,String commissionAmt,String commissionDate){
		EnrollmentCommission enrollmentCommission = new EnrollmentCommission();
		if(enrollment.getInsurerName() != null){
		enrollmentCommission.setCarrier(enrollment.getInsurerName());
		}
		if(commissionAmt != null){
		enrollmentCommission.setCommissionAmt(Float.valueOf(commissionAmt));
		}
		if(enrollment.getId() != null){
		enrollmentCommission.setEnrollmentId(enrollment.getId());
		}
		if(enrollment.getStateExchangeCode() != null){
		enrollmentCommission.setState(enrollment.getStateExchangeCode());
		}
		if(commissionDate != null){
		enrollmentCommission.setCommissionDate(DateUtil.StringToDate(commissionDate, DATE_FORMAT));
		}
		enrollmentCommission.setAmtPaidToDate(new Float(0));
		try {
			enrollmentCommissionRepository.save(enrollmentCommission);
		} catch (Exception e) {
			LOGGER.error("Error in saveEnrollmentCommission() == "+ e.getMessage());
			LOGGER.error("Exception in saveEnrollmentCommission" , e);

		}
	}
	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> getCarrierUpdatedEnrollments() {
		Date lastRunDate=getLastRunDate(EnrollmentPrivateConstants.CARRIER_STAT_UPDATE_JOB_NAME);
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
		return enrollmentRepository.getCarrierUpdatedEnrollments(lastRunDate, user.getId());
	}
	
	@Override
	@Transactional
	public Enrollment saveEnrollment(Enrollment enrollment) {
		//set Enrollment Submitted By
		setEnrollmentSubmittedBy(enrollment);
		return enrollmentRepository.save(enrollment);
	}
	
	@Override
	public List<Enrollment>  findEnrollmentByExternalID(String externalID){
		return enrollmentRepository.findEnrollmentByExternalID(externalID);
	}
	
	/**
	 * @author panda_p
	 * 
	 * @param jobName
	 * @return lastRunDate
	 */
	private Date getLastRunDate(String jobName){
		Date lastRunDate= null;
		if (jobName != null) {
			lastRunDate = batchJobExecutionService.getLastJobRunDate(jobName);
		}
		return lastRunDate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> searchEnrollment(
			Map<String, Object> searchCriteria) {
		Map<String, Object> applicationListAndRecordCount = new HashMap<String, Object>();
/*		List<String> joinTables = new ArrayList<String>();
		joinTables.add("plan");
		joinTables.add("issuer");*/

		QueryBuilder<Enrollment> applicationQuery = delegateFactory.getObject();
		applicationQuery.buildSelectQuery(Enrollment.class);

		if (searchCriteria != null) {
			if (searchCriteria.get("policynumber") != null) {
				applicationQuery.applyWhere("groupPolicyNumber",
						searchCriteria.get("policynumber"), DataType.STRING,
						ComparisonType.EQUALS);
			}
			if (searchCriteria.get(EnrollmentPrivateConstants.STATUS) != null) {
				applicationQuery.applyWhere(
						"enrollmentStatusLkp.lookupValueCode",
						searchCriteria.get(EnrollmentPrivateConstants.STATUS), DataType.STRING,
						ComparisonType.LIKE);
			}
			if (searchCriteria.get("plantype") != null) {
				applicationQuery.applyWhere("insuranceTypeLkp.lookupValueCode",
						searchCriteria.get("plantype"), DataType.STRING,
						ComparisonType.LIKE);
			}
			if (searchCriteria.get("issuer") != null) {
				int issuer_id = Integer.parseInt((String) searchCriteria
						.get("issuer"));
				applicationQuery.applyWhere("issuerId", issuer_id,
						DataType.NUMERIC, ComparisonType.EQ);
			}
			if (searchCriteria.get("plannumber") != null) {
				applicationQuery.applyWhere("CMSPlanID",
						searchCriteria.get("plannumber"), DataType.STRING,
						ComparisonType.EQUALS);
			}
			if (searchCriteria.get("lookup") != null) { // to filter individual
														// and shop enrollment
														// records
				if (searchCriteria.get("lookup").equals("INDV")) {
					applicationQuery.applyWhere("employee", null,
							DataType.NUMERIC, ComparisonType.ISNULL);
				} else if (searchCriteria.get("lookup").equals("SHOP")) {
					applicationQuery.applyWhere("employee", 0,
							DataType.NUMERIC, ComparisonType.GT);
				}
			}
			
			String sortBy = (searchCriteria.get(EnrollmentPrivateConstants.SORT_BY)!=null)?searchCriteria.get(EnrollmentPrivateConstants.SORT_BY).toString() : EnrollmentPrivateConstants.DEFAULT_SORT_COLUMN;
			String sortOrder = (searchCriteria.get(EnrollmentPrivateConstants.SORT_ORDER)!=null)?searchCriteria.get(EnrollmentPrivateConstants.SORT_ORDER).toString() : EnrollmentPrivateConstants.DEFAULT_SORT_ORDER;
			applicationQuery.applySort(sortBy, SortOrder.valueOf(sortOrder));

			List<String> columns = new ArrayList<String>();
			columns.add("id");
			columns.add("groupPolicyNumber");// policy Number
			columns.add("enrollmentStatusLkp.lookupValueCode");// Status
			columns.add("insuranceTypeLkp.lookupValueCode");// Plan type
			columns.add("planName");
			columns.add("CMSPlanID");
			columns.add("insurerName");
			columns.add("benefitEffectiveDate");
			columns.add("benefitEndDate");
			applicationQuery.applySelectColumns(columns);
			try {
				applicationQuery.setFetchDistinct(true);
				
				Integer startRecord = (searchCriteria.get(EnrollmentPrivateConstants.START_RECORD)!=null)?(Integer)searchCriteria.get(EnrollmentPrivateConstants.START_RECORD) : EnrollmentPrivateConstants.DEFAULT_START_RECORD;
				Integer pageSize = (searchCriteria.get(EnrollmentPrivateConstants.PAGE_SIZE)!=null)?(Integer)searchCriteria.get(EnrollmentPrivateConstants.PAGE_SIZE) : GhixConstants.PAGE_SIZE;
				
				List<Map<String, Object>> applications = applicationQuery.getData(startRecord,pageSize);
				applicationListAndRecordCount.put("applicationlist", applications);
				applicationListAndRecordCount.put("recordCount", applicationQuery.getRecordCount());
			} catch (Exception e) {
				LOGGER.error("Error occured in searchEnrollment" , e);
			}
		}
		return applicationListAndRecordCount;
	}
	
	@Override
	public Map<String,Object> findEnrollmentByAdmin(EnrollmentSearchFilter enrollmentSearchFilter,Long tenantId){
		Map<String, Object> mapTicketRecCount = new HashMap<String, Object>();
		Map<String, Object> mapQueryParam = new HashMap<>();
		/*Integer subscriberEnrolleeTypeId = 0;
	 	String getSubscriberEnrolleTypeId = "select lookupValueId  from LookupValue where lookupValueCode='SUBSCRIBER'";*/
		
	 	//subscriberEnrolleeTypeId = (Integer)em.createQuery(getSubscriberEnrolleTypeId).getSingleResult();
	 	
	 	String selectClause = " SELECT en.id as enrollmentId," +
	 			" (case when ee.id >0 then ee.first_Name else (hh.FIRST_NAME) end) as firstName,"+
	 			" (case when ee.id >0 then ee.last_Name else (hh.LAST_NAME || ' [HH]') end) as lastName,"+
	 			" en.CARRIER_APP_ID as applicationId, en.ISSUER_ASSIGN_POLICY_NO as policyNo,"+
	 			"  en.INSURER_NAME as carrier, en.PLAN_NAME, homeAddressid.state, enrollmentStatusLkp.lookup_Value_Code as enrollmentStatus,"+
	 			"  en.LAST_UPDATE_TIMESTAMP as lastUpdated , en.benefit_Effective_Date as effectiveDate,"+
	 			"  en.exchange_Assign_Policy_No,"+
	 			"  exchangeTypeLkp.lookup_Value_Code,insuranceTypeLkp.lookup_Value_Label as insuranceType, en.renewal_Flag, ee.id as enrolleeId ";
	 	String fromClause =   " FROM  Enrollment as  en " +
	 			" inner join CMR_HOUSEHOLD hh on en.cmr_HouseHold_Id = hh.id " +
	 			" left outer join ENROLLEE as ee on en.id = ee.ENROLLMENT_ID " + 
	 			" left outer join locations as homeAddressid on ee.home_address_id = homeAddressid.id " +
	 			" left outer join lookup_value as enrollmentStatusLkp " + 
	 			" on  en.ENROLLMENT_STATUS_LKP = enrollmentStatusLkp.lookup_value_id  " +
	 			" left outer join lookup_value as exchangeTypeLkp " + 
	 			" on  en.EXCHANGE_TYPE = exchangeTypeLkp.lookup_value_id " +
	 			" left outer join lookup_value as insuranceTypeLkp " + 
	 			" on  en.INSURANCE_TYPE_LKP = insuranceTypeLkp.lookup_value_id ";
	 	
	 	String whereClause =" where enrollmentStatusLkp.lookup_Value_Code not in "
	 			+ "('"+EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED +"','"+EnrollmentPrivateConstants.ENROLLMENT_STATUS_ECOMMITTED+"') ";
	 	
	 	if(tenantId > 0){
	 		whereClause = whereClause + " and en.tenant_Id = "+tenantId+" ";
	 	}
	 	
	 	boolean enableMedicare = enrollmentSearchFilter.isEnableMedicare();
		String medicareJoin = "";
		String medicareWhere = "";
	 	if(!enableMedicare){
			medicareJoin = " LEFT OUTER JOIN CMR_MEMBER P ON P.CMR_HOUSEHOLD_ID=HH.ID and lower(p.RELATIONSHIP)='self' ";
		
			medicareWhere = " AND (P.MEMBER_EXTENSION ->> 'medicareElgible' = 'false' OR P.MEMBER_EXTENSION IS NULL) ";
		}
	 	
	 	String hicn = enrollmentSearchFilter.getHicn();
	 	String hicnWhere = "";
	 	
	 	if(StringUtils.isNotBlank(hicn) && !hicn.equalsIgnoreCase("null")){
	 		if(StringUtils.isEmpty(medicareJoin)){
	 			medicareJoin = " LEFT OUTER JOIN CMR_MEMBER P ON P.CMR_HOUSEHOLD_ID=HH.ID and lower(p.RELATIONSHIP)='self' ";
	 		}
	 		
	 		hicnWhere = " AND (P.MEMBER_EXTENSION -> 'medicare' ->> 'hicn' = '"+hicn+"') ";
	 	}
	 	
	 	//left outer join ENROLLMENT_RENEWALS er 
	 	 //on en.id = er.ENROLLMENT_ID
	 	//
	 	if(StringUtils.isNotEmpty(enrollmentSearchFilter.getRenewal())) {
	 		if(StringUtils.equalsIgnoreCase("Yes", enrollmentSearchFilter.getRenewal())){
		 		fromClause = fromClause + " inner join ENROLLMENT_RENEWALS er  on en.id = er.ENROLLMENT_ID ";
		 		
	 		} else if(StringUtils.equalsIgnoreCase("No", enrollmentSearchFilter.getRenewal())){
		 		whereClause = whereClause + " and not exists (select er.id from ENROLLMENT_RENEWALS er where er.enrollment_id = en.id ) ";
	 		} 
	 	}
	 	fromClause =  fromClause + medicareJoin;
	 	
	 	whereClause = whereClause + medicareWhere + hicnWhere;
	 	
	 	String selectCountClause = "SELECT count(*) as recordCount, count(distinct en.id) as enrollmentCount";
	 	 	
		StringBuilder query = new StringBuilder(selectClause);
		
		query.append(fromClause);
		
		query.append(whereClause);
		// apply the filters and create parameter map
		applyGeneralFilters(query, enrollmentSearchFilter);
					
		// apply sort filters
		addAndSetSortFilters(query, enrollmentSearchFilter);
		
		addPagination(query, enrollmentSearchFilter);
		
		LOGGER.info("Search Enrollments Query: " + query.toString());
		EntityManager em = null;
		try {
			// Execute the query, get the records as per search criteria.
			em = entityManagerFactory.createEntityManager();

			// Get results start
			Query listQuery = em.createNativeQuery(query
					.toString());
			@SuppressWarnings("unchecked")
			List<Object> resultList = listQuery.getResultList();

			mapTicketRecCount.put("applicationlist", resultList);

			// Get results end
			
			// Get counts start
			query.replace(query.indexOf(selectClause), selectClause.length(),
					selectCountClause);
			
			query.replace(query.indexOf(EnrollmentPrivateConstants.ORDER_BY), query.length(),"");
			
			LOGGER.info("Search Enrollment countQuery: " + query.toString());
			Query countQuery = em.createNativeQuery(query
					.toString());
			Object[] dataCount = (Object[])countQuery.getSingleResult();
			mapTicketRecCount.put("recordCount", dataCount[0]);
			mapTicketRecCount.put("enrollmentCount", dataCount[1]);
			// Get counts ends
			
		} catch (Exception e) {
			LOGGER.error("Error occured in searchEnrollment" , e);
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		return mapTicketRecCount;
	}

	private void applyGeneralFilters(StringBuilder query,EnrollmentSearchFilter searchCriteria) {
		String firstName = searchCriteria.getFirstName();
		if (StringUtils.isNotEmpty(firstName)) {
			if (!firstName.contains("'")) {
				query.append(" AND upper(ee.first_Name) like  '%").append(firstName.toUpperCase() + "%'");
			} else {
				query.append(" AND upper(ee.first_Name) like  '%").append(firstName.toUpperCase().replace("'", "''") + "%'");
			}
		}
		String lastName = searchCriteria.getLastName();
		if (!StringUtils.isEmpty(lastName)) {
			if (!lastName.contains("'")) {
				query.append(" AND  upper(ee.last_Name) like '%").append(lastName.toUpperCase() + "%'");
			} else {
				query.append(" AND  upper(ee.last_Name) like '%").append(lastName.toUpperCase().replace("'", "''") + "%'");
			}
		}
		String  applicationId = searchCriteria.getUniqueId();
		if (applicationId != null && !applicationId.isEmpty()) {
			query.append(" AND  upper(en.carrier_App_Id) like '%").append( applicationId.toUpperCase()+ "%'");
		}
		String insurerName = searchCriteria.getCarrier();
		if (insurerName != null && !insurerName.isEmpty()) {
	 	 	query.append(" AND  upper(en.insurer_Name) like '%").append( insurerName.toUpperCase()+ "%'");
		}
		String state = searchCriteria.getState();
		if (StringUtils.isNotEmpty(state)&& !state.contains("Any")) {
	 	 	query.append(" AND  homeAddressid.state='" + state + "'");			
		}
		String enrollmentStatus = searchCriteria.getEnrollmentStatus();
		if (StringUtils.isNotEmpty(enrollmentStatus)&& !enrollmentStatus.contains("Any")) {
			query.append(" AND  enrollmentStatusLkp.lookup_Value_Code='" + enrollmentStatus+"'");	
		}
		String exchangeType = searchCriteria.getExchangeType();
		if (StringUtils.isNotEmpty(exchangeType)&& !exchangeType.contains("Any")) {
			query.append(" AND  exchangeTypeLkp.lookup_Value_Code='" + exchangeType+"'");			
		}
		String capAgentId = searchCriteria.getServicedBy();
		if(!StringUtils.isEmpty(capAgentId)) {
			query.append(" AND  en.cap_Agent_Id=" + Integer.parseInt(capAgentId));			
		}
		String lastUpdateDateFrom = searchCriteria.getLastUpdatedFrom();
		if(!StringUtils.isEmpty(lastUpdateDateFrom)) {
			query.append(" AND  en.LAST_UPDATE_TIMESTAMP>='").append(getDateFromString(lastUpdateDateFrom)+"'");	
			
		}
		String lastUpdateDateFromTo = searchCriteria.getLastUpdatedTo();
		if(!StringUtils.isEmpty(lastUpdateDateFromTo)) {
			query.append(" AND en.LAST_UPDATE_TIMESTAMP <='").append(getDateFromString(lastUpdateDateFromTo) + "'");
		}
		String policyEffectiveFrom = searchCriteria.getPolicyEffectiveFrom();
		if(!StringUtils.isEmpty(policyEffectiveFrom)) {
			query.append(" AND en.BENEFIT_EFFECTIVE_DATE>='").append(getDateFromString(policyEffectiveFrom)+"'");
		}
		String policyEffectiveTo = searchCriteria.getPolicyEffectiveTo();
		if(!StringUtils.isEmpty(policyEffectiveTo)) {
			query.append(" AND en.BENEFIT_EFFECTIVE_DATE <='").append(getDateFromString(policyEffectiveTo)+"'");
		}
		String policySubmitFrom = searchCriteria.getPolicySubmitFrom();
		String policySubmitTo = searchCriteria.getPolicySubmitTo();
		
		if(StringUtils.isNotEmpty(policySubmitFrom) && StringUtils.isNotEmpty(policySubmitTo)){
			query.append(" AND (en.SUBMITTED_TO_CARRIER_DATE BETWEEN '"+this.getFormattedDateString(policySubmitFrom,0)+"' AND '"+this.getFormattedDateString(policySubmitTo,1)+"') ");
		}else{
			if(StringUtils.isNotEmpty(policySubmitFrom)){
				query.append(" AND en.SUBMITTED_TO_CARRIER_DATE>='").append(getDateFromString(policySubmitFrom)+"'");
			}
			
			if(StringUtils.isNotEmpty(policySubmitTo)){
				query.append(" AND en.SUBMITTED_TO_CARRIER_DATE<='").append(getDateFromString(policySubmitTo) + "'"); 
			}
		}
	/*	if(StringUtils.isNotEmpty(policySubmitFrom) && StringUtils.isNotEmpty(policySubmitFrom)) {
		   query.append(" AND ((trunc(en.submittedToCarrierDate) BETWEEN ").append("trunc(:policySubmitFrom)").append(" AND ").append("trunc(:policySubmitTo)").append("))");
		   addpolicySubmitDateToSearchQuery(policySubmitFrom, policySubmitTo, mapQueryParam);
		}*/
		String insuranceType = searchCriteria.getInsuranceType();
		if (StringUtils.isNotEmpty(insuranceType)&& !insuranceType.contains("Any")) {
			query.append(" AND insuranceTypeLkp.lookup_Value_Code='" + insuranceType + "'");
		}
		String enrollmentID = searchCriteria.getEnrollmentID();
		if (StringUtils.isNotEmpty(enrollmentID)) {
			query.append(" AND en.id=" + Integer.parseInt(enrollmentID));
		}
		String policyNo = searchCriteria.getPolicyNo();
		if(!StringUtils.isEmpty(policyNo)) { 
	 	 	query.append(" AND upper(en.ISSUER_ASSIGN_POLICY_NO) like '" + "%" + policyNo.toUpperCase() + "%'");
		}
		String verificationEvent = searchCriteria.getVerificationEvent();
		if(!StringUtils.isEmpty(verificationEvent)) { 
			String condition = null;
			if("Not Verified".equals(verificationEvent)){
				condition = "is null";
			}else{
				condition = "= '" +verificationEvent + "' ";
		}
			query.append("AND en.VERIFICATION_REASON ").append(condition).append(" ");
		}
	}
	
	private void addpolicySubmitDateToSearchQuery(String policySubmitFrom,String policySubmitTo, Map<String, Object> mapQueryParam) {
		  
		  //Input date format
		  SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		  
		
		  String dbPolicySubmitFrom = null;
		  String dbPolicySubmitTo = null;
		  
		try {
			if (policySubmitTo != null) {
				Date policySubmitToDate = new TSDate(dateFormat.parse(
						policySubmitTo).getTime());
				dbPolicySubmitTo = dateFormat.format(policySubmitToDate);
				mapQueryParam.put("policySubmitTo", getDateFromString(dbPolicySubmitTo));
			}
		} catch (ParseException e) {
			LOGGER.error("Error while parsing policy submit To date "
					+ e.getMessage());
		}
		try {
			if (policySubmitFrom != null) {
				Date policySubmitFromDate = dateFormat.parse(policySubmitFrom);
				dbPolicySubmitFrom = dateFormat.format(policySubmitFromDate);
				mapQueryParam.put("policySubmitFrom", getDateFromString(dbPolicySubmitFrom));
			}
		} catch (ParseException e) {
			LOGGER.error("Error while parsing policy submit From date "
					+ e.getMessage());
		}

	 }
	private Date getDateFromString(String date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date newDate = null;
			newDate = sdf.parse(date);
			return newDate;
			//return sdf.parse(sdf.format(newDate));

		} catch (ParseException e) {
			LOGGER.error("Inside EnrollmentListController:: Can not parse date "
					+ e.getMessage());
		}
		return null;
	}
	
	private String getFormattedDateString(String date, int addDays){
		Date queryDate = this.getDateFromString(date);
		String formattedDate = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(queryDate != null){
			
			if(addDays > 0){
				LocalDateTime localDateTime = queryDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				localDateTime = localDateTime.plusDays(addDays);
				
				queryDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			}
			
			formattedDate = sdf.format(queryDate);
		}
				
		return formattedDate;
	}

	private void addPagination(StringBuilder query, EnrollmentSearchFilter searchCriteria) {
		Integer pageSize = (Integer) searchCriteria.getPageSize();
		Integer startIndex = (Integer) searchCriteria.getStartRecord() ;
		query.append("OFFSET " + startIndex.intValue() + " LIMIT " + pageSize.intValue());
	}

	/**
	 * @param query
	 * @param searchCriteria
	 * This method will add the sorting criteria on the ticket list query.
	 */
	private void addAndSetSortFilters(StringBuilder query, EnrollmentSearchFilter searchCriteria) {		
		String columnName = searchCriteria.getSortColumn();				
		Boolean sortOrder = searchCriteria.isSortDirection();
		query.append(" ");
		if(columnName.equals("firstName")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("ee.first_Name,ee.last_Name").append(" ");
		}else if (columnName.equals("applicationId")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.carrier_App_Id").append(" ");
		}else if (columnName.equals("planId")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.issuer_Assign_Policy_No").append(" ");
		}else if (columnName.equals("carrierName")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.insurer_Name").append(" ");
		}else if (columnName.equals("planName")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.PLAN_NAME").append(" ");
		}else if (columnName.equals("enrollmentStatus")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("enrollmentStatusLkp.lookup_Value_Code").append(" ");
		}else if (columnName.equals("lastUpdated")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.LAST_UPDATE_TIMESTAMP").append(" ");
		}else if (columnName.equals("benefitEffectiveDate")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.benefit_Effective_Date").append(" ");
		}
		else if (columnName.equals("state")){
			//query.append("ORDER BY ").append("ee.homeAddressid.state").append(" ");
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("homeAddressid.state").append(" ");
		}else if (columnName.equals("insuranceType")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("insuranceType").append(" ");
		}else if (columnName.equals("enrollmentId")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.id").append(" ");
		}else if (columnName.equals("issuerAssignPolicyNo")){
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.issuer_Assign_Policy_No").append(" ");
		}
		else{
			query.append(EnrollmentPrivateConstants.ORDER_BY).append("en.LAST_UPDATE_TIMESTAMP").append(" ");
		}
	
//		String order  = sortOrder?"DESC" : "ASC";
		if(sortOrder){
			query.append("DESC");
		}
		else{
			query.append("ASC");
		}
		
		query.append(" , en.id, ee.id ");
		
	}

	@Override
	public List<Enrollment> findEnrollmentByEmployer(Employer employer) {
		return enrollmentRepository.findByEmployer(employer);
	}
	
	@Override
	public List<Object[]> findEnrollmentByEmployerId(int employerId) {
		return enrollmentRepository.findByEmployerId(employerId);
	}
	
	@Override
	public List<Enrollment> findActiveEnrollmentByEmployeeID(Integer employeeId) {
		return enrollmentRepository.findActiveEnrollmentByEmployeeID(employeeId);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Object[]> findCurrentMonthEffectiveEnrollment(
			Map<String, Object> inputval) {

		Date currentDate = null;
		if (isNotNullAndEmpty(inputval.get(EnrollmentPrivateConstants.CURRENT_DATE))) {
			currentDate = DateUtil.StringToDate(inputval.get(EnrollmentPrivateConstants.CURRENT_DATE)
					.toString(), GhixConstants.REQUIRED_DATE_FORMAT);
		}

		Date lastInvoiceDate = null;
		if (isNotNullAndEmpty(inputval.get(EnrollmentPrivateConstants.LAST_INVOICE_DATE))) {
			lastInvoiceDate = DateUtil.StringToDate(
					inputval.get(EnrollmentPrivateConstants.LAST_INVOICE_DATE).toString(), GhixConstants.REQUIRED_DATE_FORMAT);
		}

		String status = null;
		if (isNotNullAndEmpty(inputval.get(EnrollmentPrivateConstants.STATUS))) {
			status = inputval.get(EnrollmentPrivateConstants.STATUS).toString();
		}

		int employerId = 0;
		if (isNotNullAndEmpty(inputval.get("employerid"))) {
			employerId = Integer.valueOf(inputval.get("employerid").toString());
		}
		
		return enrollmentRepository.findCurrentMonthEffectiveEnrollment(currentDate, lastInvoiceDate, status, employerId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> findEnrollmentByStatusAndTerminationdate(
			Map<String, Object> inputval) {

		Date terminationdate = DateUtil.StringToDate(
				inputval.get("terminationdate").toString(), GhixConstants.REQUIRED_DATE_FORMAT);
		String status = inputval.get(EnrollmentPrivateConstants.STATUS).toString();

		return enrollmentRepository.findEnrollmentByStatusAndBenefitEndDate(
				status, terminationdate, new Sort(new Sort.Order(
						Sort.Direction.ASC, "employee.id"), new Sort.Order(
						Sort.Direction.ASC, "insurerName")));
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> findEnrollmentByCarrierFeedDetails(
			Map<String, Object> inputval) {
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		String applicationId = inputval.get("applicationId").toString();
		String uid = inputval.get("uid").toString();
		String firstName = inputval.get("firstName").toString();
		String lastName = inputval.get("lastName").toString();
		Enrollment enrollment = null;
		if(isNotNullAndEmpty(applicationId)){
			enrollment = enrollmentRepository.findByCarrierAppId(applicationId);		
		}
		if(enrollment == null){
			enrollment = enrollmentRepository.findBycarrierAppUID(uid);	
			
		}
//		if(enrollment == null){
//			enrollment = enrollmentRepository.findBycarrierAppUID(uid);	
//		}
		
		if(enrollment == null){
			List<Object[]> possibleEnrollments = enrollmentRepository
					.findEnrollmentAndEnrolleeDataByName(
							firstName.trim(), lastName.trim());
		
			
			if (possibleEnrollments == null || possibleEnrollments.isEmpty()) {
				return null;
								
			}
			List<Enrollment> reallyPossibleEnrollments = new  ArrayList<Enrollment>();
			for(Object[] x : possibleEnrollments)
			{
				reallyPossibleEnrollments.add((Enrollment)x[0]);
			}
			return reallyPossibleEnrollments;
		}
		if(enrollment != null){
			
			enrollmentList.add(enrollment);
		}
				
   return enrollmentList;
		//return enrollmentRepository.findEnrollmentByCarrierFeedDetails(applicationId,uid,firstName,lastName);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> findEnrollmentByStatusAndEmployerAndTerminationdate(
			Map<String, Object> inputval) {

		String status = inputval.get(EnrollmentPrivateConstants.STATUS).toString();
		Employer employer = (Employer) inputval.get(EnrollmentPrivateConstants.EMPLOYER);
		Date terminationdate = DateUtil.StringToDate(
				inputval.get("terminationdate").toString(), GhixConstants.REQUIRED_DATE_FORMAT);

		return enrollmentRepository
				.findEnrollmentByStatusAndEmployerAndBenefitEndDate(status,
						employer.getId(), terminationdate, new Sort(
								new Sort.Order(Sort.Direction.ASC,
										"employee.id"), new Sort.Order(
										Sort.Direction.ASC, "insurerName")));
	}
	
	
	/**
	 * @author panda_p
	 * @since 10-May-2013
	 * 
	 * This method creates special enrollment for individuals.
	 * 
	 *  
	 * @param pldOrderResponse received from plan display module 
	 * @param applicant_esig
	 * @return EnrollmentResponse
	 */
	@Override
	@Transactional
	public EnrollmentResponse createSpecialEnrollment(PldOrderResponse pldOrderResponse,String applicant_esig, String taxFiler_esign,  String cmrHouseholdId) {
		LOGGER.info("Reached Special Enrollment Creation method inside service impl class");
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		List<PldPlanData> planDataList = new ArrayList<PldPlanData>();
		planDataList = pldOrderResponse.getPlanDataList();
		Character enrollmentReason = pldOrderResponse.getEnrollmentType();
		String houseHoldCaseID = pldOrderResponse.getHouseholdCaseId();
		String sadp_flag = pldOrderResponse.getSadpFlag();
		String brokerId = pldOrderResponse.getUserRoleId();
		String brokerType = pldOrderResponse.getUserRoleType();
		String employerId = pldOrderResponse.getEmployerId();
		String employeeId = pldOrderResponse.getEmployeeId();
		String partnerAssignedConsumerId = pldOrderResponse.getHouseholdId();
		
		Map<String, String> responsiblePerson = null;
		if (isNotNullAndEmpty(pldOrderResponse.getResponsiblePerson())) {
			responsiblePerson = pldOrderResponse.getResponsiblePerson();
		}
		Map<String, String> householdContact = null;
		if (isNotNullAndEmpty(pldOrderResponse.getHouseHoldContact())) {
			householdContact = pldOrderResponse.getHouseHoldContact();
		}
		
	    if(!isNotNullAndEmpty(planDataList)){
	    	enrollmentResponse.setErrMsg("planDataList received is null");
	    	return enrollmentResponse;
	    }
	    
	    List<Enrollment> enrollmentList =  new ArrayList<Enrollment>();
	    Map<String,List<Map<String, String>>> finalDisEnrollmentMemberMap = new HashMap<String, List<Map<String, String>>>();
	    
	    for (PldPlanData pdlPlan : planDataList) {
			Map<String, String> plan = pdlPlan.getPlan();
			String existingMemberPlanId = plan.get(EnrollmentPrivateConstants.PLAN_ID);
			
//			Float grossPremAmt =null;
//			Float netPremAmt = null;
//			Float aptcAmt = null;
//			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT))) {
//				grossPremAmt = Float.parseFloat(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT));
//			}
//			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.NET_PREMIUM_AMT))) {
//				netPremAmt = Float.parseFloat(plan.get(EnrollmentConstants.NET_PREMIUM_AMT));
//			}
//			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.APTC))) {
//				aptcAmt = Float.parseFloat(plan.get(EnrollmentConstants.APTC));
//			}
			
			List<PldMemberData> memberInfoList = pdlPlan.getMemberInfoList();
			
			if(!isNotNullAndEmpty(memberInfoList)){
		    	enrollmentResponse.setErrMsg("memberInfoList received is null");
		    	return enrollmentResponse;
		    }
			
			List<PldMemberData> newMemberList = new ArrayList<PldMemberData>();
			List<PldMemberData> noChangeMemberList = new ArrayList<PldMemberData>();
			
			/**
			 * disEnrollMemberList needs to set in the EnrollmentResponse, This would be displayed in the order confirm page. 
			 */
			List<Map<String, String>> disEnrollMemberList = new ArrayList<Map<String, String>>();
			
			
			for (PldMemberData pldMemberData : memberInfoList) {
				Map<String, String> member = pldMemberData.getMemberInfo();
				
				if(isNotNullAndEmpty(member.get("pldNewPersonFlag")) && member.get("pldNewPersonFlag").equalsIgnoreCase(EnrollmentPrivateConstants.YES)){
					newMemberList.add(pldMemberData);
					//=====================================================================
					if((isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.DISENROLL_QHP_FLAG)) && member.get(EnrollmentPrivateConstants.DISENROLL_QHP_FLAG).equalsIgnoreCase(EnrollmentPrivateConstants.YES))
							|| (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.DISENROLL_SADP_FLAG)) && member.get(EnrollmentPrivateConstants.DISENROLL_SADP_FLAG).equalsIgnoreCase(EnrollmentPrivateConstants.YES))){
						
						Map<String, String> disMemberMap =  new HashMap<String, String>();
						Map<String,Object> disEnrollmentMap = new HashMap<String, Object>();
						/**
						 * disEnrollmentMap can take the following keys
						 *
						 * MemberId , EnrollmentID,TerminationDate , EnrollmentStatus , DisEnrollmentType , TerminationReasonCode , DeathDate
						 */
						disEnrollmentMap.put(EnrollmentPrivateConstants.DIS_ENROLLMENT_TYPE, GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
						disEnrollmentMap.put(EnrollmentPrivateConstants.TERMINATION_REASON_CODE,member.get(EnrollmentPrivateConstants.MAINTENANCE_REASON_CODE)); // This will come from planDisply. needs to discuss with Vijay
						
						//		Calculating termination Date and setting it to disEnrollmentMap
						String coverageStartDate = plan.get(EnrollmentPrivateConstants.COVERAGE_START_DATE);
						Date effectiveDate = null;
						if (isNotNullAndEmpty(coverageStartDate)) {
							String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd hh:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
							effectiveDate =  DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT); 
						}
						Date terminationDate = null;
						if(isNotNullAndEmpty(effectiveDate)){
							terminationDate = this.getBeforeDayDate(effectiveDate);
						}
						if(isNotNullAndEmpty(terminationDate)){
						disEnrollmentMap.put(EnrollmentPrivateConstants.TERMINATION_DATE, terminationDate.toString());
						}
						
						if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.MEMBER_ID))){
							disEnrollmentMap.put(EnrollmentPrivateConstants.MEMBER_ID_KEY, member.get(EnrollmentPrivateConstants.MEMBER_ID));
						}
						if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID))){
							disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_ID_KEY, Integer.parseInt(member.get(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID)));
							try{
								Enrollment enrl = this.findById(Integer.valueOf(member.get(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID)));
								
								if(enrl!=null &&enrl.getEnrollmentStatusLkp()!=null && enrl.getBenefitEffectiveDate() !=null){
									
									if(enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM) &&enrl.getBenefitEffectiveDate().compareTo(new TSDate())<=0){
										disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1, EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM);
									} else if(enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING) || (enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM) && enrl.getBenefitEffectiveDate().compareTo(new TSDate())>0)){
										disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1, EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL);
									}
								}
								//		Calling for DisEnrollment
								String disEnrollResponse = disEnrollByEnrollmentID(disEnrollmentMap);
								if(disEnrollResponse.equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
									disMemberMap.put("fName", member.get(EnrollmentPrivateConstants.FIRST_NAME));
									disMemberMap.put("lName", member.get(EnrollmentPrivateConstants.LAST_NAME));
									disMemberMap.put(EnrollmentPrivateConstants.PLAN_TYPE, plan.get(EnrollmentPrivateConstants.PLAN_TYPE));
									disMemberMap.put(EnrollmentPrivateConstants.MEMBER_ID, member.get(EnrollmentPrivateConstants.MEMBER_ID));
									disMemberMap.put(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID, member.get(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID));
									//disMemberMap.put("existingSadpEnrollmentId", member.get("existingSadpEnrollmentId"));
									disMemberMap.put(EnrollmentPrivateConstants.QHP_DIS_ENROLLMENT_END_DATE,DateUtil.dateToString(terminationDate, GhixConstants.REQUIRED_DATE_FORMAT));
									disEnrollMemberList.add(disMemberMap);
								}
							}catch(GIException gie){
								LOGGER.error(gie.getErrorMsg() , gie);
								enrollmentResponse.setErrMsg(gie.getErrorMsg());
								enrollmentResponse.setErrCode(gie.getErrorCode());
								return enrollmentResponse;
							}
						}
						else if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID))){
							disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_ID_KEY, Integer.parseInt(member.get(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID)));
							
							try{
								Enrollment enrl = this.findById(Integer.valueOf(member.get(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID)));
								
								if(enrl!=null &&enrl.getEnrollmentStatusLkp()!=null && enrl.getBenefitEffectiveDate() !=null){
									
									if(enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM) &&enrl.getBenefitEffectiveDate().compareTo(new TSDate())<=0){
										disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1, EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM);
									} else if(enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING) || (enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM) && enrl.getBenefitEffectiveDate().compareTo(new TSDate())>0)){
										disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1, EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL);
									}
								}
								
								//		Calling for DisEneollment							
								String disEnrollResponse = disEnrollByEnrollmentID(disEnrollmentMap);
								if(disEnrollResponse.equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
									disMemberMap.put("fName", member.get(EnrollmentPrivateConstants.FIRST_NAME));
									disMemberMap.put("lName", member.get(EnrollmentPrivateConstants.LAST_NAME));
									disMemberMap.put(EnrollmentPrivateConstants.PLAN_TYPE, plan.get(EnrollmentPrivateConstants.PLAN_TYPE));
									disMemberMap.put(EnrollmentPrivateConstants.MEMBER_ID, member.get(EnrollmentPrivateConstants.MEMBER_ID));
									//disMemberMap.put("existingQhpEnrollmentId", member.get("existingQhpEnrollmentId"));
									disMemberMap.put(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID, member.get(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID));
									disMemberMap.put(EnrollmentPrivateConstants.SADP_DIS_ENROLLMENT_END_DATE, DateUtil.dateToString(terminationDate, GhixConstants.REQUIRED_DATE_FORMAT));
									disEnrollMemberList.add(disMemberMap);
								}
							}catch(GIException gie){
								LOGGER.error(gie.getErrorMsg() , gie);
								enrollmentResponse.setErrMsg(gie.getErrorMsg());
								enrollmentResponse.setErrCode(gie.getErrorCode());
								return enrollmentResponse;
							}
						}
					}//End of DisEnrollloop
					//=====================================================================
				}else if((isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.DISENROLL_QHP_FLAG)) && member.get(EnrollmentPrivateConstants.DISENROLL_QHP_FLAG).equalsIgnoreCase(EnrollmentPrivateConstants.NO))
						|| (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.DISENROLL_SADP_FLAG)) && member.get(EnrollmentPrivateConstants.DISENROLL_SADP_FLAG).equalsIgnoreCase(EnrollmentPrivateConstants.NO))){
					noChangeMemberList.add(pldMemberData);
				}else if((isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.DISENROLL_QHP_FLAG)) && member.get(EnrollmentPrivateConstants.DISENROLL_QHP_FLAG).equalsIgnoreCase(EnrollmentPrivateConstants.YES))
						|| (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.DISENROLL_SADP_FLAG)) && member.get(EnrollmentPrivateConstants.DISENROLL_SADP_FLAG).equalsIgnoreCase(EnrollmentPrivateConstants.YES))){
					
					Map<String, String> disMemberMap =  new HashMap<String, String>();
					Map<String,Object> disEnrollmentMap = new HashMap<String, Object>();
					/**
					 * disEnrollmentMap can take the following keys
					 *
					 * MemberId , EnrollmentID,TerminationDate , EnrollmentStatus , DisEnrollmentType , TerminationReasonCode , DeathDate
					 */
					disEnrollmentMap.put(EnrollmentPrivateConstants.DIS_ENROLLMENT_TYPE, GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
					disEnrollmentMap.put(EnrollmentPrivateConstants.TERMINATION_REASON_CODE,""); // This will come from planDisply. needs to discuss with Vijay
					
					//		Calculating termination Date and setting it to disEnrollmentMap
					String coverageStartDate = plan.get(EnrollmentPrivateConstants.COVERAGE_START_DATE);
					Date effectiveDate = null;
					if (isNotNullAndEmpty(coverageStartDate)) {
						String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd hh:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
						effectiveDate =  DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT); 
					}
					Date terminationDate = null;
					if(isNotNullAndEmpty(effectiveDate)){
						terminationDate = this.getBeforeDayDate(effectiveDate);
					}
					if(isNotNullAndEmpty(terminationDate)){
					disEnrollmentMap.put(EnrollmentPrivateConstants.TERMINATION_DATE, terminationDate.toString());
					}
					
					if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.MEMBER_ID))){
						disEnrollmentMap.put(EnrollmentPrivateConstants.MEMBER_ID_KEY, member.get(EnrollmentPrivateConstants.MEMBER_ID));
					}
					if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID))){
						disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_ID_KEY, Integer.parseInt(member.get(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID)));
						try{
							Enrollment enrl = this.findById(Integer.valueOf(member.get(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID)));
							
							if(enrl!=null &&enrl.getEnrollmentStatusLkp()!=null && enrl.getBenefitEffectiveDate() !=null){
								
								if(enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM) &&enrl.getBenefitEffectiveDate().compareTo(new TSDate())<=0){
									disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1, EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM);
								} else if(enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING) || (enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM) && enrl.getBenefitEffectiveDate().compareTo(new TSDate())>0)){
									disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1, EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL);
								}
							}
							//		Calling for DisEnrollment
							String disEnrollResponse = disEnrollByEnrollmentID(disEnrollmentMap);
							if(disEnrollResponse.equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
								disMemberMap.put("fName", member.get(EnrollmentPrivateConstants.FIRST_NAME));
								disMemberMap.put("lName", member.get(EnrollmentPrivateConstants.LAST_NAME));
								disMemberMap.put(EnrollmentPrivateConstants.PLAN_TYPE, plan.get(EnrollmentPrivateConstants.PLAN_TYPE));
								disMemberMap.put(EnrollmentPrivateConstants.MEMBER_ID, member.get(EnrollmentPrivateConstants.MEMBER_ID));
								disMemberMap.put(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID, member.get(EnrollmentPrivateConstants.EXISTING_QHP_ENROLLMENT_ID));
								//disMemberMap.put("existingSadpEnrollmentId", member.get("existingSadpEnrollmentId"));
								disMemberMap.put(EnrollmentPrivateConstants.QHP_DIS_ENROLLMENT_END_DATE,DateUtil.dateToString(terminationDate, GhixConstants.REQUIRED_DATE_FORMAT));
								disEnrollMemberList.add(disMemberMap);
							}
						}catch(GIException gie){
							LOGGER.error(gie.getErrorMsg() , gie);
							enrollmentResponse.setErrMsg(gie.getErrorMsg());
							enrollmentResponse.setErrCode(gie.getErrorCode());
							return enrollmentResponse;
						}
					}
					else if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID))){
						disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_ID_KEY, Integer.parseInt(member.get(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID)));
						
						try{
							Enrollment enrl = this.findById(Integer.valueOf(member.get(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID)));
							
							if(enrl!=null &&enrl.getEnrollmentStatusLkp()!=null && enrl.getBenefitEffectiveDate() !=null){
								
								if(enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM) &&enrl.getBenefitEffectiveDate().compareTo(new TSDate())<=0){
									disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1, EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM);
								} else if(enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING) || (enrl.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM) && enrl.getBenefitEffectiveDate().compareTo(new TSDate())>0)){
									disEnrollmentMap.put(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1, EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL);
								}
							}
							
							//		Calling for DisEneollment							
							String disEnrollResponse = disEnrollByEnrollmentID(disEnrollmentMap);
							if(disEnrollResponse.equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
								disMemberMap.put("fName", member.get(EnrollmentPrivateConstants.FIRST_NAME));
								disMemberMap.put("lName", member.get(EnrollmentPrivateConstants.LAST_NAME));
								disMemberMap.put(EnrollmentPrivateConstants.PLAN_TYPE, plan.get(EnrollmentPrivateConstants.PLAN_TYPE));
								disMemberMap.put(EnrollmentPrivateConstants.MEMBER_ID, member.get(EnrollmentPrivateConstants.MEMBER_ID));
								//disMemberMap.put("existingQhpEnrollmentId", member.get("existingQhpEnrollmentId"));
								disMemberMap.put(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID, member.get(EnrollmentPrivateConstants.EXISTING_SADP_ENROLLMENT_ID));
								disMemberMap.put(EnrollmentPrivateConstants.SADP_DIS_ENROLLMENT_END_DATE, DateUtil.dateToString(terminationDate, GhixConstants.REQUIRED_DATE_FORMAT));
								disEnrollMemberList.add(disMemberMap);
							}
						}catch(GIException gie){
							LOGGER.error(gie.getErrorMsg() , gie);
							enrollmentResponse.setErrMsg(gie.getErrorMsg());
							enrollmentResponse.setErrCode(gie.getErrorCode());
							return enrollmentResponse;
						}
					}
				}//End of DisEnrollloop
			}//End of member checking
			
			enrollmentResponse.setDisEnrollmentList(disEnrollMemberList);
			finalDisEnrollmentMemberMap.put(houseHoldCaseID, disEnrollMemberList);
			
			
			Enrollment existingEnrlmnt = new Enrollment();
			if(!noChangeMemberList.isEmpty()){
				// 	add member to existing Enrollment.
				if(isNotNullAndEmpty(existingMemberPlanId) && isNotNullAndEmpty(houseHoldCaseID)){
					existingEnrlmnt = this.enrollmentRepository.findByHouseHoldCaseIdAndPlanId(houseHoldCaseID,Integer.parseInt( existingMemberPlanId));
					if(existingEnrlmnt==null){
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg("No Enrollment found for the existing member PlanId : " +existingMemberPlanId+"And houseHoldCaseID : "+houseHoldCaseID );
						return enrollmentResponse;
					}
//					if(grossPremAmt != null){
//						existingEnrlmnt.setGrossPremiumAmt(grossPremAmt);
//					}
//					if(netPremAmt != null){
//						existingEnrlmnt.setNetPremiumAmt(netPremAmt);
//					}
//					if(aptcAmt != null){
//						existingEnrlmnt.setAptcAmt(aptcAmt);
//					}
					// Update Existing Enrollees and Enrollment
					updateExistingEnrollee(existingEnrlmnt , noChangeMemberList);
					updateexistingEnrollmentForSpecial(existingEnrlmnt, pdlPlan, responsiblePerson, householdContact, sadp_flag, brokerId, brokerType, employerId, employeeId, enrollmentReason);
					
					if(!newMemberList.isEmpty()){
						try {
							existingEnrlmnt = addMemberToExixtingEnrollment(existingEnrlmnt, newMemberList,noChangeMemberList,enrollmentReason);
						} catch (GIException gie) {
							LOGGER.error("Failed to add new member enrollment" , gie);
						}
					}
				}else{
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrMsg("existing Member PlanId or houseHoldCaseID not found");
					return enrollmentResponse;
				}
			}else{
				//	create new enrollment
				if(!newMemberList.isEmpty()){
					List<PldPlanData> newPlanDataList = new ArrayList<PldPlanData>();
					pdlPlan.setMemberInfoList(newMemberList);
					newPlanDataList.add(pdlPlan);
					List<Enrollment> enrlList = new ArrayList<Enrollment>();
					try{
						enrlList = getEnrollmentFromplanDataList(newPlanDataList,houseHoldCaseID,responsiblePerson,householdContact,sadp_flag,brokerId,brokerType,employerId,employeeId,enrollmentReason,partnerAssignedConsumerId,cmrHouseholdId, null);	
					}catch (GIException gie) {
						LOGGER.error("Failed to create enrollment" , gie);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrCode(gie.getErrorCode());
						enrollmentResponse.setErrMsg(gie.getErrorMsg());
						return enrollmentResponse;
					}
					existingEnrlmnt = enrlList.get(0);
				}
			}
			enrollmentList.add(existingEnrlmnt);
	    }//End of planData loop
	    enrollmentResponse.setFinalDisEnrollmentMemberMap(finalDisEnrollmentMemberMap);
	    List<Enrollment> enrollments =null;
	    try {
			enrollments = saveAllEnrollment(enrollmentList);
		} catch (Exception e) {
			LOGGER.error(" Enrollment Error" , e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(e.getMessage());
			return enrollmentResponse;
		}
		
		try{
			saveEsignatureForEnrollment(enrollments,applicant_esig, taxFiler_esign);
		}catch (Exception e) {
			LOGGER.error(e.getMessage() + e);
			LOGGER.error(" Enrollment Error" , e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Failed to save Esignature For Enrollment:"+e.getMessage());
			return enrollmentResponse;
		}
			
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);	
		enrollmentResponse.setEnrollmentList(enrollments);
		enrollmentResponse.setEnrolleeNameMap(getEnrolleeNameMap(enrollments));
	    
		return enrollmentResponse;
	}
	
	/**
	 * @author panda_p
	 * @since 10-May-2013
	 * 
	 * Adds member (enrollee) to the existing enrollment for special enrollment
	 * 
	 * @param existingEnrlmnt
	 * @param newMemberList
	 * @return
	 */
	private Enrollment addMemberToExixtingEnrollment(Enrollment existingEnrlmnt, List<PldMemberData> newMemberList ,List<PldMemberData>oldMemberList, Character enrollmentReason) throws GIException{
		
		List<Enrollee> enrolleeList= getEnrolleeFromMemberInfoList(newMemberList, existingEnrlmnt.getBenefitEffectiveDate(), enrollmentReason);
		//existingEnrlmnt.setEnrollees(enrolleeList);
		
		List<EnrollmentEvent> enrollmentEventList= new ArrayList<EnrollmentEvent>();
		
		for (Enrollee tempenrollee : enrolleeList) {

			tempenrollee.setEnrollment(existingEnrlmnt);
			
			//creating EnrollmentEvent
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent(); 
			enrollmentEvent.setEnrollee(tempenrollee);
			enrollmentEvent.setEnrollment(existingEnrlmnt);
			enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION));
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION));
			//added last event Id for Audit
			tempenrollee.setLastEventId(enrollmentEvent);
			
			enrollmentEventList.add(enrollmentEvent);
			
		}
		List<Enrollee> existingEnrolleeList = null;
		try {
			existingEnrolleeList = enrolleeService.getEnrolleeByEnrollmentID(existingEnrlmnt.getId());
		} catch (GIException e) {
			LOGGER.error("Error occured in getEnrolleeByEnrollmentID" , e);
		}
		if(isNotNullAndEmpty(existingEnrolleeList)){
			existingEnrolleeList.addAll(enrolleeList);
			existingEnrlmnt.setEnrollees(existingEnrolleeList);
		}
		saveEnrolleeRelationship(newMemberList,existingEnrolleeList);

		// Adding Event Event Reason and Event Type for Existing Enrollee list
//		String	existingMemberEventReason = "";
		
//	      Enrollee existingEnrollee = null;
//			for (PldMemberData pldMemberData : oldMemberList) {
//				Map<String, String> existingmember = pldMemberData.getMemberInfo();
				// Already happening in Update Existing Enrollee loop for special enrollment.
	//				if(isNotNullAndEmpty(existingmember.get("maintenanceReasonCode"))){
	//					existingMemberEventReason =  existingmember.get("maintenanceReasonCode").toString();
	//					EnrollmentEvent enrollmentEvent = new EnrollmentEvent(); 
	//					enrollmentEvent.setEnrollment(existingEnrlmnt);
	//					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,existingMemberEventReason));
	//					enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CHANGE));
	//					if(isNotNullAndEmpty(existingmember.get("memberId"))){
	//					 try{
	//						 existingEnrollee = enrolleeService.findEnrolleeByEnrollmentIDAndMemberID(existingmember.get("memberId"),existingEnrlmnt.getId());
	//						 enrollmentEvent.setEnrollee(existingEnrollee);
	//					} 
	//					catch (Exception e){
	//						LOGGER.error("Failed to getEnrollee By ExchgIndivIdentifier : " + e);
	//					}
	//					//added last event Id for Audit
	//					//existingEnrollee.setLastEventId(enrollmentEvent);
	//					enrollmentEventList.add(enrollmentEvent);
	//				 }
	//			  }
//		}
		existingEnrlmnt.setEnrollmentEvents(enrollmentEventList);
		
		return existingEnrlmnt; 
	}
	/**
	 * 
	 * @param date
	 * @return
	 */
	private Date getBeforeDayDate(Date date) {
		Date prevDay = null;
		if (date != null) {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, -1);
			prevDay = cal.getTime();
		}
		return prevDay;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	private Date getMonthEndDate(Date date) {
		Date lastDay = null;
		if (date != null) {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.DATE, cal.getMaximum(Calendar.DATE));
			lastDay = cal.getTime();
		}
		return lastDay;
	}
	
	
	private EnrollmentResponse validateD2CEnrollmentCreationRequest(D2CEnrollmentRequest d2CEnrollmentRequest){
		EnrollmentResponse response= null;
		if(d2CEnrollmentRequest==null){
			response=new EnrollmentResponse();
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
			response.setErrMsg("Request not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		if(d2CEnrollmentRequest.getCmrHouseholdId()==null){
			response=new EnrollmentResponse();
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_202);
			response.setErrMsg("CMR Household ID not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		if(d2CEnrollmentRequest.getPlanId()==null){
			response=new EnrollmentResponse();
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_203);
			response.setErrMsg("Plan ID not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		if(d2CEnrollmentRequest.getSsapApplicationId()==null){
			response=new EnrollmentResponse();
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_204);
			response.setErrMsg("SSAP Application ID not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		
		if(d2CEnrollmentRequest.getD2cEnrollmentId()==null){
			response=new EnrollmentResponse();
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_205);
			response.setErrMsg("D2C Enrollment ID not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		if(d2CEnrollmentRequest.getIssuerId()==null){
			response=new EnrollmentResponse();
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_206);
			response.setErrMsg("Issuer ID not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		return response;
	}
	
	
	
	
	@Override
	public EnrollmentResponse updateD2CEnrollmentStatus(D2CEnrollmentRequest d2CEnrollmentRequest){
		EnrollmentResponse response=new EnrollmentResponse();
	
		if(d2CEnrollmentRequest==null){
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
			response.setErrMsg("Request not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		if(d2CEnrollmentRequest.getEnrollmentStatus()==null ||d2CEnrollmentRequest.getEnrollmentStatus().equals("") ){
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_202);
			response.setErrMsg("Enrollment Status not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		if(d2CEnrollmentRequest.getD2cEnrollmentId()==null){
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_203);
			response.setErrMsg(" D2C Enrollment ID not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		
		Enrollment enrollment=enrollmentRepository.findD2CEnrollment(d2CEnrollmentRequest.getD2cEnrollmentId(), EnrollmentPrivateConstants.EXCHANGE_TYPE_D2C);
		
		if(enrollment==null){
			LOGGER.error(" updateD2CEnrollmentStatus :: No Enrollment exist for provided D2C Enrollment ID = "+d2CEnrollmentRequest.getD2cEnrollmentId());
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_204);
			response.setErrMsg("No Enrollment exist for provided D2C Enrollment ID = "+d2CEnrollmentRequest.getD2cEnrollmentId());
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}else{
			try{
				enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, d2CEnrollmentRequest.getEnrollmentStatus()));
				enrollment.setUpdatedBy(getLoggedInUser());
				//HIX-77342
				if(d2CEnrollmentRequest.getEnrollmentStatus().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)){
					setEnrollmentSubmittedBy(enrollment);
					//enrollment.setSubmittedBy(getLoggedInUser());
				}
				enrollmentRepository.save(enrollment);
				response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_200);
				response.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}catch(Exception e){
				response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_299);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrMsg("Error Updating D2C Enrollment" + e.getMessage());
				LOGGER.error("Error Updating D2C Enrollment" + e.getMessage(), e);
			}
		}
		
		return response;
	}
	
	@Override
	public EnrollmentResponse createD2CEnrollmentWithEnrollees(D2CEnrollmentRequest d2CEnrollmentRequest){
		
		LOGGER.info("createD2CEnrollmentWithEnrollees createD2CEnrollment START -->");
		long startTime = TimeShifterUtil.currentTimeMillis();		
		Enrollment d2cEnrollment=null;
		EnrollmentResponse response = null;
		if(d2CEnrollmentRequest!=null 
				&& d2CEnrollmentRequest.getD2CEnrolleeDTOList() != null 
				&& !d2CEnrollmentRequest.getD2CEnrolleeDTOList().isEmpty()){
			response= new EnrollmentResponse();
			try{
				d2cEnrollment= new Enrollment();
				if(isNotNullAndEmpty(d2CEnrollmentRequest.getCmrHouseholdId())){
					d2cEnrollment.setCmrHouseHoldId(d2CEnrollmentRequest.getCmrHouseholdId());
				}
				if(isNotNullAndEmpty(d2CEnrollmentRequest.getPlanId())){
					d2cEnrollment.setPlanId(d2CEnrollmentRequest.getPlanId());
					//Setting Plan Level and other plan related fields
					setPlanInfoFromPlanManagement(d2CEnrollmentRequest.getPlanId()+"", d2cEnrollment);
				}
				if(isNotNullAndEmpty(d2CEnrollmentRequest.getIssuerId())){
					d2cEnrollment.setIssuerId(d2CEnrollmentRequest.getIssuerId());
				}
				d2cEnrollment.setBenefitEffectiveDate(d2CEnrollmentRequest.getBenefitEffectiveDate());
				d2cEnrollment.setBenefitEndDate(d2CEnrollmentRequest.getBenefitEndDate());
				d2cEnrollment.setAptcAmt(d2CEnrollmentRequest.getAptcAmt());
				d2cEnrollment.setCsrAmt(d2CEnrollmentRequest.getCsrAmt());
				d2cEnrollment.setCapAgentId(d2CEnrollmentRequest.getCapAgentId());
				if(isNotNullAndEmpty(d2CEnrollmentRequest.getEmployerId())){
					Employer employer= new Employer();
					employer.setId(d2CEnrollmentRequest.getEmployerId());
					d2cEnrollment.setEmployer(employer);
				}
				if(isNotNullAndEmpty(d2CEnrollmentRequest.getEnrollmentStatus())){
					d2cEnrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, d2CEnrollmentRequest.getEnrollmentStatus()));
					if(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(d2CEnrollmentRequest.getEnrollmentStatus())){
						d2cEnrollment.setSubmittedToCarrierDate(new TSDate());
						//Jira ID: HIX-77342
						setEnrollmentSubmittedBy(d2cEnrollment);
					}
				}
				if(isNotNullAndEmpty(d2CEnrollmentRequest.getEnrollmentType())){
					d2cEnrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE, d2CEnrollmentRequest.getEnrollmentType()));
				}
				d2cEnrollment.setGrossPremiumAmt(d2CEnrollmentRequest.getGrossPremiumAmt());
				d2cEnrollment.setNetPremiumAmt(d2CEnrollmentRequest.getNetPremiumAmt());
				if(d2CEnrollmentRequest.getAssisterBrokerId() != null){
					d2cEnrollment.setAssisterBrokerId(d2CEnrollmentRequest.getAssisterBrokerId());
				}
				d2cEnrollment.setCmrHouseHoldId(d2CEnrollmentRequest.getCmrHouseholdId());
				d2cEnrollment.setEmployeeId(d2CEnrollmentRequest.getEmployeeId());
				d2cEnrollment.setTicketId(d2CEnrollmentRequest.getTicketId());
				d2cEnrollment.setPriorSsapApplicationid(d2CEnrollmentRequest.getSsapApplicationId());
				d2cEnrollment.setSsapApplicationid(d2CEnrollmentRequest.getSsapApplicationId());
				if(isNotNullAndEmpty(d2CEnrollmentRequest.getExchangeType())){
					d2cEnrollment.setExchangeTypeLkp((lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, d2CEnrollmentRequest.getExchangeType())));
				}
				d2cEnrollment.setGroupPolicyNumber(d2CEnrollmentRequest.getGroupPolicyNumber());
				d2cEnrollment.setPlanName(d2CEnrollmentRequest.getPlanName());
				d2cEnrollment.setExchangeAssignPolicyNo(d2CEnrollmentRequest.getExchangeAssignPolicyNo());
				d2cEnrollment.setIssuerAssignPolicyNo(d2CEnrollmentRequest.getIssuerAssignPolicyNo());
				d2cEnrollment.setConfirmationNumber(d2CEnrollmentRequest.getConfirmationNo());
				d2cEnrollment.setCarrierAppId(d2CEnrollmentRequest.getCarrierAppId());
				d2cEnrollment.setCarrierAppUID(d2CEnrollmentRequest.getCarrierAppUid());
				d2cEnrollment.setD2cEnrollmentId(d2CEnrollmentRequest.getD2cEnrollmentId());
				d2cEnrollment.setEnrollmentModalityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_D2C));
				d2cEnrollment.setExchangeTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_OFFEXCHANGE));
				if(d2cEnrollment.getInsuranceTypeLkp() == null){
					if(StringUtils.isNotBlank(d2CEnrollmentRequest.getInsuranceType())){
						d2cEnrollment.setInsuranceTypeLkp(getInsuranceTypeLkpFromPlanResponse(d2CEnrollmentRequest.getInsuranceType()));
					}
					else{
						d2cEnrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE_HEALTH));
					}
				}
				d2cEnrollment.setCreatedBy(getLoggedInUser());
				d2cEnrollment.setUpdatedBy(getLoggedInUser());
				
				List<EnrollmentEvent> d2cEnrollmentEvents = new ArrayList<EnrollmentEvent>();
				d2cEnrollment.setEnrollmentEvents(d2cEnrollmentEvents);
				List<Enrollee> d2cEnrolleeList = new ArrayList<Enrollee>();
				Enrollee d2cEnrollee = null;
				for(D2CEnrolleeDTO d2cEnolleeDTO : d2CEnrollmentRequest.getD2CEnrolleeDTOList()){
					d2cEnrollee = createD2CEnrolleeAndEvent(d2CEnrollmentRequest, d2cEnrollment, d2cEnolleeDTO);
					d2cEnrollee.setEnrollment(d2cEnrollment);
					d2cEnrolleeList.add(d2cEnrollee);
				}
				if(!d2CEnrollmentRequest.isExistSubscriber()){
					//No subscriber found, return ERROR
					response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
					response.setErrMsg("No SUBSCRIBER found.");
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					return response;
				}
				
				d2cEnrollment.setEnrollees(d2cEnrolleeList);
				setEnrollmentSubmittedBy(d2cEnrollment);
				d2cEnrollment=enrollmentRepository.saveAndFlush(d2cEnrollment);
				if(null != d2cEnrollment.getEnrollmentIssuerCommision()) {
					d2cEnrollment.getEnrollmentIssuerCommision().setEnrollment(d2cEnrollment);
					enrollmentIssuerCommisionRepository.saveAndFlush(d2cEnrollment.getEnrollmentIssuerCommision());
				}
				enrollmentPrivateUtils.recordApplicationEnrollmentPrivateEvent(d2cEnrollment,EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString(),d2cEnrollment.getEnrollmentStatusLkp().getLookupValueLabel());
				if(isNotNullAndEmpty(d2cEnrollment.getId())){
					LOGGER.info("D2CEnrollment ID: " + d2cEnrollment.getId());
				}else{
					LOGGER.info("D2CEnrollment ID is null");
				}
				
				 // HIX-38637 save enrollment event
				response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_200);
				response.setStatus(GhixConstants.RESPONSE_SUCCESS);
				response.setEnrollmentId(d2cEnrollment.getId());
				
			}catch(Exception e){
				giExceptionHandler.recordFatalException(Component.ENROLLMENT, String.valueOf(EnrollmentConstants.ERROR_CODE_299), EnrollmentPrivateConstants.ERR_MSG_ERROR_CREATING_D2C_ENROLLMENT , e) ;
				response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_299);
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrMsg(EnrollmentPrivateConstants.ERR_MSG_ERROR_CREATING_D2C_ENROLLMENT + e.getMessage());
				LOGGER.error(EnrollmentPrivateConstants.ERR_MSG_ERROR_CREATING_D2C_ENROLLMENT + e.getMessage(), e);
			}
		}else{
			response=new EnrollmentResponse();
			response.setErrCode(EnrollmentPrivateConstants.ERROR_CODE_201);
			response.setErrMsg("Request or EnrolleeList not provided.");
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			return response;
		}
		LOGGER.info("createD2CEnrollmentWithEnrollees createD2CEnrollment END in ***" + (TimeShifterUtil.currentTimeMillis() - startTime));
		return response;
	}
	
	private Enrollee createD2CEnrolleeAndEvent(D2CEnrollmentRequest d2CEnrollmentRequest, Enrollment d2cEnrollment, D2CEnrolleeDTO d2cEnrolleeDTO){
		LOGGER.info("EnrollmentPrivateServiceImpl createD2CEnrolleeAndEvent START -->");
		Enrollee d2cEnrollee = new Enrollee();
		d2cEnrollee.setCreatedBy(getLoggedInUser());
		d2cEnrollee.setUpdatedBy(getLoggedInUser());
		
		d2cEnrollee.setLastName(d2cEnrolleeDTO.getLastName());
		d2cEnrollee.setFirstName(d2cEnrolleeDTO.getFirstName());
		d2cEnrollee.setMiddleName(d2cEnrolleeDTO.getMiddleName());
		d2cEnrollee.setHomeAddressid(d2cEnrolleeDTO.getHomeAddress());
		d2cEnrollee.setBirthDate(d2cEnrolleeDTO.getBirthDate());
		if(isNotNullAndEmpty(d2cEnrolleeDTO.getGender())){
			d2cEnrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.GENDER, d2cEnrolleeDTO.getGender()));
		}
		d2cEnrollee.setMailingAddressId(d2cEnrolleeDTO.getMailingAddress());
		if(isNotNullAndEmpty(d2cEnrolleeDTO.getTobaccoUsage())){
			d2cEnrollee.setTobaccoUsageLkp((lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, d2cEnrolleeDTO.getTobaccoUsage())));
		}
		if(isNotNullAndEmpty(d2cEnrolleeDTO.getMaritalStatus())){
			d2cEnrollee.setMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.MARITAL_STATUS, d2cEnrolleeDTO.getMaritalStatus()));
		}
		if(isNotNullAndEmpty(d2cEnrolleeDTO.getRaceEthnicity())){
			List<EnrolleeRace> enrolleeRaces = new ArrayList<EnrolleeRace>();
			EnrolleeRace enrolleeRace = new EnrolleeRace();
			enrolleeRace.setRaceEthnicityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_TYPE_RACE, d2cEnrolleeDTO.getRaceEthnicity()));
			enrolleeRace.setRaceDescription(lookupService.getLookupValueLabel(EnrollmentPrivateConstants.LOOKUP_TYPE_RACE, d2cEnrolleeDTO.getRaceEthnicity()));
			enrolleeRace.setEnrollee(d2cEnrollee);
			enrolleeRaces.add(enrolleeRace);
			d2cEnrollee.setEnrolleeRace(enrolleeRaces);
		}
		d2cEnrollee.setEffectiveStartDate(d2cEnrolleeDTO.getEffectiveStartDate());
		d2cEnrollee.setEffectiveEndDate(d2cEnrolleeDTO.getEffectiveEndDate());
		if(isNotNullAndEmpty(d2cEnrolleeDTO.getPersonType())){
			if(d2cEnrolleeDTO.getPersonType().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
				//Enrollee PersonType is SUBSCRIBER
				d2CEnrollmentRequest.setExistSubscriber(Boolean.TRUE);
			}
			d2cEnrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE, d2cEnrolleeDTO.getPersonType()));
		}
		if(isNotNullAndEmpty(d2cEnrolleeDTO.getRelationshipHcp())){
			d2cEnrollee.setRelationshipToHCPLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.RELATIONSHIP, d2cEnrolleeDTO.getRelationshipHcp()));	
		}
//		d2cEnrollee.setEnrollmentReason(d2cEnrolleeDTO.getEnrollmentReason());
		d2cEnrollee.setEnrollmentReason(new Character('I'));
		d2cEnrollee.setExchgIndivIdentifier(d2cEnrolleeDTO.getExchgIndivIdentifier());
		d2cEnrollee.setPrimaryPhoneNo(d2cEnrolleeDTO.getPrimaryPhoneNo());
		d2cEnrollee.setPreferredEmail(d2cEnrolleeDTO.getPrefEmail());
		d2cEnrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
		
		EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
		enrollmentEvent.setCreatedBy(getLoggedInUser());
		enrollmentEvent.setEnrollee(d2cEnrollee);
		enrollmentEvent.setEnrollment(d2cEnrollment);
		enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION));
		enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION));

//		d2cEnrollee.setLastEventId(enrollmentEvent);
		d2cEnrollment.getEnrollmentEvents().add(enrollmentEvent);
		
		LOGGER.info("EnrollmentPrivateServiceImpl createD2CEnrolleeAndEvent END ***");
		
		return d2cEnrollee;
	}
	
	/**
	 * @author meher_a
	 * @since 
	 * 
	 * This method create enrollment for individual plan selection.
	 */
	@Override
	@Transactional
	public EnrollmentResponse createEnrollment(PldOrderResponse pldOrderResponse,String applicant_esig, String taxFiler_esign, String cmrHouseholdId, Long ssapApplicationId) {
		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		List<Enrollment> enrollmentList = null;

		List<PldPlanData> planDataList = new ArrayList<PldPlanData>();
		List<Enrollment> enrollments =null;

		planDataList = pldOrderResponse.getPlanDataList();
		
		//Added for FFM
		String partnerAssignedConsumerId = pldOrderResponse.getHouseholdId();
		
		
		Character enrollmentResaon = pldOrderResponse.getEnrollmentType();
		String houseHoldCaseID = pldOrderResponse.getHouseholdCaseId();
		String sadp_flag = pldOrderResponse.getSadpFlag();
		String brokerId = pldOrderResponse.getUserRoleId();
		String brokerType = pldOrderResponse.getUserRoleType();
		String employerId = pldOrderResponse.getEmployerId();
		String employeeId = pldOrderResponse.getEmployeeId();
		Map<String, String> responsiblePerson = null;
		if (isNotNullAndEmpty(pldOrderResponse.getResponsiblePerson())) {
			responsiblePerson = pldOrderResponse.getResponsiblePerson();
		}
		Map<String, String> householdContact = null;
		if (isNotNullAndEmpty(pldOrderResponse.getHouseHoldContact())) {
			householdContact = pldOrderResponse.getHouseHoldContact();
		}
		try {
			enrollmentList = getEnrollmentFromplanDataList(planDataList,houseHoldCaseID,responsiblePerson,householdContact,sadp_flag,brokerId,brokerType, employerId, employeeId, enrollmentResaon, partnerAssignedConsumerId, cmrHouseholdId ,ssapApplicationId);
			enrollments = saveAllEnrollment(enrollmentList);
			
			/*if((!isPHIXBBUser()) && enrollments != null){
				for (Enrollment enrollment : enrollments) {
					if(enrollment.getEnrollmentTypeLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){

						if(enrollment.getEmployer() != null && enrollment.getIssuer() != null && enrollment.getPlan() != null){
							groupInstallationService.addOrUpdateEmployerGroup(enrollment.getEmployer().getId(), enrollment.getIssuer().getId(), enrollment.getAssisterBrokerId(), enrollment.getPlan().getId());
						}
					}
				}
			}*/
		} catch (GIException gie) {
			LOGGER.error("Failed to create enrollment" , gie);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(gie.getErrorCode());
			enrollmentResponse.setErrMsg(gie.getMessage());
			return enrollmentResponse;
		}
		catch (Exception e) {
			LOGGER.error("Failed to create enrollment" , e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(e.getMessage());
			return enrollmentResponse;
		}
		
		try{
			saveEsignatureForEnrollment(enrollments,applicant_esig, taxFiler_esign);
		}catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Failed to save Esignature For Enrollment  -----   "+e.getMessage());
			return enrollmentResponse;
		}
			
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);	
		enrollmentResponse.setEnrollmentList(enrollments);
		enrollmentResponse.setEnrolleeNameMap(getEnrolleeNameMap(enrollments));
		return enrollmentResponse;
	}
	private Map<Integer,String> getEnrolleeNameMap(List<Enrollment> enrollmentList){
		Map<Integer,String> enrolleeNameMap= new HashMap<Integer, String>();
		List<Enrollee> enrolleelist= null;
		for(Enrollment enrollment : enrollmentList){
			try{
			enrolleelist = enrolleeService.getEnrolledEnrolleesForEnrollmentID(enrollment.getId());
			
			StringBuilder combinedEnroleeName=new StringBuilder();
			if(enrolleelist != null){			
			 for(Enrollee enrollee: enrolleelist){
				combinedEnroleeName.append(enrollee.getFirstName());
				if(enrollee.getLastName()!=null){
					combinedEnroleeName.append(" ").append(enrollee.getLastName());
				}
				combinedEnroleeName.append(", ");
			  }
			}
			enrolleeNameMap.put(enrollment.getId(), combinedEnroleeName.substring(0,(combinedEnroleeName.length()-2)));
			}
			catch(GIException e){
			LOGGER.error("Error occured in getting getEnrolleeNameMap" , e);
			}
		}
		return enrolleeNameMap;
	}
	private List<Enrollment> saveAllEnrollment(List<Enrollment> enrollmentList) throws GIException {
		List<Enrollment> enrollments = new ArrayList<Enrollment>();

		for (Enrollment enrollment : enrollmentList) {
			enrollment.setCreatedBy(getLoggedInUser());
			enrollment.setUpdatedBy(getLoggedInUser());
			//Setting Enrollment submitted By
			setEnrollmentSubmittedBy(enrollment);
			enrollment.setSubmittedToCarrierDate(new TSDate());//HIX-55747
			try{
				EnrollmentUtilsForCap.setCapAgentId(enrollment, userService);
				enrollmentPrivateUtils.recordApplicationEnrollmentPrivateEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString(), enrollment.getEnrollmentStatusLkp().getLookupValueLabel());
				Enrollment en = enrollmentRepository.save(enrollment);
				enrollments.add(en);
			}catch(Exception e){
				LOGGER.error("Exception in saveAllEnrollment : "+ e);
				throw new GIException(e.getMessage(), e);
			}
		}
		return enrollments;		
	}
	
	
	private List<Enrollee> getEnrolleeFromMemberInfoList(List<PldMemberData> memberInfoList, Date effectiveStartDate, Character enrollmentReason) {
		boolean custIndicator =false;
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		Enrollee enrollee;
		for (PldMemberData pldMemberData : memberInfoList) {
			Map<String, String> member = pldMemberData.getMemberInfo();
			enrollee = new Enrollee();

			if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.DOB))){
				enrollee.setBirthDate(DateUtil.StringToDate(member.get(EnrollmentPrivateConstants.DOB), GhixConstants.REQUIRED_DATE_FORMAT));
			}
			
			if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.INSURANCE_APPLICANT_ID))){
				enrollee.setInsuranceAplicantId(member.get(EnrollmentPrivateConstants.INSURANCE_APPLICANT_ID));
			}
			if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.FFM_PERSON_ID))){
				enrollee.setFfmPersonId(member.get(EnrollmentPrivateConstants.FFM_PERSON_ID));
			}
			
			enrollee.setEnrollmentReason(enrollmentReason);
			enrollee.setEffectiveStartDate(effectiveStartDate);
			enrollee.setExchgIndivIdentifier(member.get(EnrollmentPrivateConstants.MEMBER_ID));
			enrollee.setFirstName(member.get(EnrollmentPrivateConstants.FIRST_NAME));
			enrollee.setLastName(member.get(EnrollmentPrivateConstants.LAST_NAME));
			if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.MIDDLE_NAME))){
				enrollee.setMiddleName(member.get(EnrollmentPrivateConstants.MIDDLE_NAME));
			}
			enrollee.setPrimaryPhoneNo(member.get(EnrollmentPrivateConstants.PRIMARY_PHONE));
			enrollee.setSecondaryPhoneNo(member.get(EnrollmentPrivateConstants.SECONDARY_PHONE));
			enrollee.setPreferredEmail(member.get(EnrollmentPrivateConstants.PREFERRED_EMAIL));
			enrollee.setPreferredSMS(member.get(EnrollmentPrivateConstants.PREFERRED_PHONE));
			enrollee.setTaxIdNumber(member.get(EnrollmentPrivateConstants.SSN));
			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.SUFFIX))) {
				enrollee.setSuffix(member.get(EnrollmentPrivateConstants.SUFFIX));
			}
			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.INDIVIDUAL_PREMIUM))) {
				enrollee.setTotalIndvResponsibilityAmt(Float.parseFloat(member.get(EnrollmentPrivateConstants.INDIVIDUAL_PREMIUM)));
			}
			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.RATING_AREA))) {
				enrollee.setRatingArea(member.get(EnrollmentPrivateConstants.RATING_AREA));
			}
			
			/*if (isNotNullAndEmpty(member.get(EnrollmentConstants.EMPLOYEE_ID))) {
				enrollee.setEmployeeId(Integer.parseInt(member.get(EnrollmentConstants.EMPLOYEE_ID)));
			}*/

			enrollee.setEffectiveStartDate(effectiveStartDate);

			// setting Resp_Person_Id_Code to QD for EDI outbound 834
			// enrollee.setRespPersonIdCode(EnrollmentConstants.RESPONSIBLE_PERSON_ID_CODE_QD);

			enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));

			if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.SUBSCRIBER_FLAG)) && member.get(EnrollmentPrivateConstants.SUBSCRIBER_FLAG).equalsIgnoreCase(EnrollmentPrivateConstants.YES)){
				enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER));
			}
			else{
				enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
			}

			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.GENDER_CODE))) {
				enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.GENDER, member.get(EnrollmentPrivateConstants.GENDER_CODE)));
			}
			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.MARITAL_STATUS_CODE))) {
				enrollee.setMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.MARITAL_STATUS, member.get(EnrollmentPrivateConstants.MARITAL_STATUS_CODE)));
			}

			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.SPOKEN_LANGUAGE_CODE))) {
				enrollee.setLanguageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LANGUAGE_SPOKEN, member.get(EnrollmentPrivateConstants.SPOKEN_LANGUAGE_CODE)));	
			}

			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.WRITTEN_LANGUAGE_CODE))) {
				enrollee.setLanguageWrittenLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LANGUAGE_WRITTEN, member.get(EnrollmentPrivateConstants.WRITTEN_LANGUAGE_CODE)));	
			}

			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.TOBACCO))) {
				if(member.get(EnrollmentPrivateConstants.TOBACCO).toString().equalsIgnoreCase(EnrollmentPrivateConstants.Y)){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, EnrollmentPrivateConstants.T));
				}
				else{
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, member.get(EnrollmentPrivateConstants.TOBACCO)));
				}

			}
			//LAST_TOBACCO_USE_DATE field is added for FFM flow
			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.LAST_TOBACCO_USE_DATE))) {
				enrollee.setLastTobaccoUseDate(DateUtil.StringToDate( member.get(EnrollmentPrivateConstants.LAST_TOBACCO_USE_DATE),GhixConstants.REQUIRED_DATE_FORMAT));	
			}

			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.HOUSEHOLD_CONTACT_RELATIONSHIP))) {
				enrollee.setRelationshipToHCPLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.RELATIONSHIP, member.get(EnrollmentPrivateConstants.HOUSEHOLD_CONTACT_RELATIONSHIP)));	
			}

			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.CITIZENSHIP_STATUS_CODE))) {
				enrollee.setCitizenshipStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.CITIZENSHIP_STATUS, member.get(EnrollmentPrivateConstants.CITIZENSHIP_STATUS_CODE)));	
			}


			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.CUSTODIAL_PARENT_ID))) {
				boolean createNewCustInd = true;
				if(custIndicator){
					for (Enrollee enrollee2 : enrolleeList) {
						Enrollee tempCustodialParent = enrollee2.getCustodialParent();
						if(tempCustodialParent != null &&
							member.get(EnrollmentPrivateConstants.CUSTODIAL_PARENT_ID).equalsIgnoreCase(tempCustodialParent.getExchgIndivIdentifier())) {
								enrollee.setCustodialParent(tempCustodialParent);
								createNewCustInd=false;
								break;
						}
					}//End of for
				}
				if(createNewCustInd){
					Enrollee custodialParent= new Enrollee();
					custodialParent.setExchgIndivIdentifier(member.get(EnrollmentPrivateConstants.CUSTODIAL_PARENT_ID));
					custodialParent.setFirstName(member.get("custodialParentFirstName"));
					custodialParent.setMiddleName(member.get("custodialParentMiddleName"));
					custodialParent.setLastName(member.get("custodialParentLastName"));
					//					custodialParent.setSsn(member.get("custodialParentSsn"));
					custodialParent.setTaxIdNumber(member.get("custodialParentSsn"));
					custodialParent.setSuffix(member.get("custodialParentSuffix"));
					custodialParent.setPreferredEmail(member.get("custodialParentPreferredEmail"));
					custodialParent.setPreferredSMS(member.get("custodialParentPreferredPhone"));
					custodialParent.setPrimaryPhoneNo(member.get("custodialParentPrimaryPhone"));
					custodialParent.setSecondaryPhoneNo(member.get("custodialParentSecondaryPhone"));

					Location custParentAddress = new Location();
					custParentAddress.setAddress1(member.get("custodialParentHomeAddress1"));
					custParentAddress.setAddress2(member.get("custodialParentHomeAddress2"));
					custParentAddress.setCity(member.get("custodialParentHomeCity"));
					custParentAddress.setState(member.get("custodialParentHomeState"));
					if(isNotNullAndEmpty(member.get("custodialParentHomeZip"))){
						custParentAddress.setZip(member.get("custodialParentHomeZip").toString());	
					}

					custodialParent.setHomeAddressid(custParentAddress);

					custodialParent.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT));
					
					custodialParent.setCreatedBy(getLoggedInUser());
					custodialParent.setUpdatedBy(getLoggedInUser());
					enrollee.setCustodialParent(custodialParent);
					custIndicator=true;
				}
			}
			if(isNotNullAndEmpty(member.get("mailingAddress1"))){
				Location mailingAddress = new Location();
				mailingAddress.setAddress1(member.get("mailingAddress1"));
				mailingAddress.setAddress2(member.get("mailingAddress2"));
				mailingAddress.setCity(member.get("mailingCity"));
				mailingAddress.setState(member.get("mailingState"));
				if (isNotNullAndEmpty(member.get("mailingZip"))) {
					mailingAddress.setZip(member.get("mailingZip"));
				}
				enrollee.setMailingAddressId(mailingAddress);
			}
			
			if(isNotNullAndEmpty(member.get("homeAddress1"))){
				Location homeAddress = new Location();
				homeAddress.setAddress1(member.get("homeAddress1"));
				homeAddress.setAddress2(member.get("homeAddress2"));
				homeAddress.setCity(member.get("homeCity"));
				homeAddress.setState(member.get("homeState"));
				homeAddress.setCountycode(member.get("countyCode"));
				if (isNotNullAndEmpty(member.get("homeZip"))) {
					homeAddress.setZip(member.get("homeZip"));
				}
				enrollee.setHomeAddressid(homeAddress);
			}
			

			List<EnrolleeRace> enrolleeRace = new ArrayList<EnrolleeRace>();
			List<Map<String, String>> raceList = pldMemberData.getRaceInfo();
			if(isNotNullAndEmpty(raceList)){
				for (Map<String,String> raceMap: raceList){
					EnrolleeRace tmpenrolleeRace = new EnrolleeRace();
					tmpenrolleeRace.setRaceEthnicityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_TYPE_RACE, raceMap.get("raceEthnicityCode")));
					tmpenrolleeRace.setRaceDescription(raceMap.get(EnrollmentPrivateConstants.DESCRIPTION));

					tmpenrolleeRace.setEnrollee(enrollee);
					enrolleeRace.add(tmpenrolleeRace);
				}
				enrollee.setEnrolleeRace(enrolleeRace);
			}
			

			//enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
			enrollee.setCreatedBy(getLoggedInUser());
			enrollee.setUpdatedBy(getLoggedInUser());
			enrolleeList.add(enrollee);

		}

		return enrolleeList;
	}
	
	
	 private Enrollee findEnrolleeByMemberId(String memberId, List<Enrollee> enrolleeList) {
		 for (Enrollee enrollee : enrolleeList) { 
			 if (enrollee.getExchgIndivIdentifier() != null && enrollee.getExchgIndivIdentifier().equals(memberId)){
				 return enrollee;
			 }
		 }
		 return null;
	 }
	 
	 
	 private void saveEnrolleeRelationship(List<PldMemberData> memberInfoList,List<Enrollee> enrolleeList)
	 
	 {
		 for (PldMemberData pldMemberData : memberInfoList) {
			 Enrollee sourceEnrollee = findEnrolleeByMemberId(pldMemberData.getMemberInfo().get(EnrollmentPrivateConstants.MEMBER_ID),enrolleeList );
			 List<Map<String,String>> relationshipList  = pldMemberData.getRelationshipInfo();
			 List<EnrolleeRelationship> enrolleeRelationshipList = new ArrayList<EnrolleeRelationship>();
			 for (Map<String,String> relationship:relationshipList){
				 if(!relationship.isEmpty()){
					 EnrolleeRelationship tmpEnrolleeRelationship = new EnrolleeRelationship();
					 tmpEnrolleeRelationship.setRelationshipLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.RELATIONSHIP, relationship.get(EnrollmentPrivateConstants.RELATIONSHIP_CODE)));
					 tmpEnrolleeRelationship.setSourceEnrollee(sourceEnrollee);
					 Enrollee targetEnrollee = findEnrolleeByMemberId(relationship.get(EnrollmentPrivateConstants.MEMBER_ID),enrolleeList );


					 tmpEnrolleeRelationship.setTargetEnrollee(targetEnrollee);

					 enrolleeRelationshipList.add(tmpEnrolleeRelationship);
				 }	
			 }
			 sourceEnrollee.setEnrolleeRelationship( enrolleeRelationshipList);
		 }
	 }
	
	@SuppressWarnings("unused")
	private List<Enrollment> getEnrollmentFromplanDataList(List<PldPlanData> planDataList, String houseHoldCaseID,Map<String, String> responsiblePerson, Map<String, String> householdContact,String sadp_flag ,String brokerId, String brokerType, String employerId, String employeeId, Character enrollmentReason, String partnerAssignedConsumerId, String cmrHouseholdId, Long ssapApplicationId) throws GIException {
		//Map<String, Enrollment> enrollmentMap = new HashMap<String, Enrollment>();
		Enrollment enrollment;
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		for (PldPlanData pdlPlan : planDataList) {
			Map<String, String> plan = pdlPlan.getPlan();


			enrollment = new Enrollment();
			String coverageStartDate = plan.get(EnrollmentPrivateConstants.COVERAGE_START_DATE);
			Date effectiveDate = null;
			if (isNotNullAndEmpty(coverageStartDate)) {
				String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd hh:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
				effectiveDate =  DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT); 
				enrollment.setBenefitEffectiveDate(effectiveDate);	
			}
			
			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.APTC))) {
				enrollment.setAptcAmt(Float.parseFloat(plan.get(EnrollmentPrivateConstants.APTC)));	
			}
			
			enrollment.setHouseHoldCaseId(houseHoldCaseID);
			enrollment.setCarrierAppId(houseHoldCaseID);
			
			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.GROSS_PREMIUM_AMT))) {
				enrollment.setGrossPremiumAmt(Float.parseFloat(plan.get(EnrollmentPrivateConstants.GROSS_PREMIUM_AMT)));	
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.NET_PREMIUM_AMT))) {
				enrollment.setNetPremiumAmt(Float.parseFloat(plan.get(EnrollmentPrivateConstants.NET_PREMIUM_AMT)));	
			}
			if(isNotNullAndEmpty(sadp_flag) && sadp_flag.equalsIgnoreCase(EnrollmentPrivateConstants.YES)){
				enrollment.setSadp(EnrollmentPrivateConstants.Y);
			}
			else{
				enrollment.setSadp(EnrollmentPrivateConstants.N);
			}

			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));

			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.MARKET_TYPE))) {
				enrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE, plan.get(EnrollmentPrivateConstants.MARKET_TYPE)));
			}
			
			//Setting Employee Id::Start
			/*if (isNotNullAndEmpty(plan.get(EnrollmentConstants.EMPLOYEE_ID))) {
				enrollment.setEmployeeId(Integer.parseInt(plan.get(EnrollmentConstants.EMPLOYEE_ID)));
			}*/
			if (!isPHIXBBUser() && isNotNullAndEmpty(employeeId)) {
				enrollment.setEmployeeId(Integer.valueOf(employeeId));
			}
			//Setting Employee Id::End

			// setting broker information

			if (isNotNullAndEmpty(brokerId)) {
				if(isNotNullAndEmpty(brokerType) && brokerType.equalsIgnoreCase(EnrollmentPrivateConstants.ASSISTER_ROLE)){
					enrollment.setAssisterBrokerId(Integer.valueOf(brokerId));
					enrollment.setBrokerRole(brokerType);
				}else{
					LOGGER.info("Calling broker service to get broker info for brokerid ==");
					//LOGGER.info("Broker service URL == "+BrokerServiceEndPoints.GET_BROKER_DETAIL+brokerId);
					String brokerInfoResponse=null;
					try{
						brokerInfoResponse=restTemplate.getForObject(BrokerServiceEndPoints.GET_BROKER_DETAIL+brokerId, String.class);
					}catch(Exception e){
						LOGGER.error("exception in getting broker data from the ghix-broker" , e);
					}

					if(brokerInfoResponse==null){
						LOGGER.error("No Broker data found ");
						//throw new GIException(302,"No Broker data found. Broker module REST services returns null ","HIGH");
					}
					else{
						EntityResponseDTO brokerResponse = (EntityResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(brokerInfoResponse);
						if(isNotNullAndEmpty(brokerResponse.getResponseCode()) && brokerResponse.getResponseCode() == EnrollmentPrivateConstants.ERROR_CODE_200){
							enrollment.setBrokerTPAFlag(EnrollmentPrivateConstants.Y);
							enrollment.setAssisterBrokerId(Integer.valueOf(brokerId));
							enrollment.setBrokerRole(brokerType);
							if(isNotNullAndEmpty(brokerResponse.getStateLicenseNumber())){
								enrollment.setBrokerTPAAccountNumber1(brokerResponse.getStateLicenseNumber());
							}
							if(isNotNullAndEmpty(brokerResponse.getBrokerName())){
								enrollment.setAgentBrokerName(brokerResponse.getBrokerName());
							}
							if(isNotNullAndEmpty(brokerResponse.getStateEINNumber())){
								enrollment.setBrokerFEDTaxPayerId(brokerResponse.getStateEINNumber());
							}
						}
						else{
							LOGGER.error("Not getting Success  response code data from ghix-broker. ");
							//throw new GIException(brokerResponse.getResponseCode(),"No Broker data found. "+ brokerResponse.getResponseDescription() ,"HIGH");
						}
					}
				}
			}

			PlanRequest planRequest= new PlanRequest();
			planRequest.setPlanId(plan.get("planId"));
			if(null != enrollment.getBenefitEffectiveDate()) {
				planRequest.setCommEffectiveDate(DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT));
			}
			if(isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.MARKET_TYPE))&& plan.get(EnrollmentPrivateConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
				planRequest.setMarketType("INDIVIDUAL");
			}
			
			String planInfoResponse=null;
			try{
				//planInfoResponse = restTemplate.postForObject(ghixPlanMgmtPlanDataURL,planRequest,String.class);PlanMgmtEndPoints
				planInfoResponse = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL,planRequest,String.class);
//				LOGGER.info("getting successfull response of the plan data from plan management."+planInfoResponse);
				LOGGER.info("Successfull response of the plan data from plan management.");
			}
			catch(Exception ex){
				LOGGER.error("exception in getting plan data from the plan management" , ex);
			}

			PlanResponse planResponse = new PlanResponse();
			planResponse = platformGson.fromJson(planInfoResponse, planResponse.getClass());

			if(planResponse==null || (planResponse.getStatus() == null || planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))){
			//if(planResponse==null || planResponse.getStatus() == null){
				LOGGER.info("No Plan data found " );
				throw new GIException(EnrollmentPrivateConstants.ERROR_CODE_301,"No Plan data found. Plan mgmt REST services returns null ","HIGH");
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.PLAN_ID))) {
				enrollment.setHiosIssuerId(planResponse.getHiosIssuerId());
				enrollment.setPlanId(planResponse.getPlanId());
				enrollment.setCMSPlanID(planResponse.getIssuerPlanNumber());
				enrollment.setPlanName(planResponse.getPlanName());
				enrollment.setInsurerName(planResponse.getIssuerName());
				enrollment.setInsurerTaxIdNumber(planResponse.getTaxIdNumber());
				if(planResponse.getPlanLevel()!=null){
					enrollment.setPlanLevel(planResponse.getPlanLevel().trim().toUpperCase());
				}
				enrollment.setIssuerId(planResponse.getIssuerId());
				enrollment.setCarrierAppUrl(planResponse.getApplicationUrl());
				enrollment.setIssuerLogo(planResponse.getIssuerLogo());
				
				if(EnrollmentPrivateConfiguration.isPhixCall() || isPHIXBBUser()){
					
					//Folloing fields are FFM only
					if(!isPHIXBBUser()){
						enrollment.setExchangeTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_ONEXCHANGE));
						enrollment.setEnrollmentModalityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.MODALITY_FFM_REDIRECT));
					}
					enrollment.setPartnerAssignedConsumerId(partnerAssignedConsumerId);
					if(isNotNullAndEmpty(cmrHouseholdId)){
						enrollment.setCmrHouseHoldId(Integer.valueOf(cmrHouseholdId));
					}
					if(isNotNullAndEmpty(ssapApplicationId)){
						enrollment.setPriorSsapApplicationid(ssapApplicationId);
						enrollment.setSsapApplicationid(ssapApplicationId);
					}
					
					if(isNotNullAndEmpty(plan.get("aptcPercentage"))){
						enrollment.setAptcPercentage(Float.parseFloat(plan.get("aptcPercentage")));	
					}
					if(isNotNullAndEmpty(plan.get("csr"))){
						String csr = plan.get("csr");
						String csrLevel = "0"+csr.substring(csr.length()-1);
						//LOGGER.info("csrLevel : " + csrLevel);
						enrollment.setCsrLevel(csrLevel);
					}
					if(isNotNullAndEmpty(plan.get("stateExchangeCode"))){
						enrollment.setStateExchangeCode(plan.get("stateExchangeCode"));
					}
					
					
					// Fetch Enrollment_Plan data for Plan
					EnrollmentPlan enrollmentPlan = enrollmentPlanRepository.findEnrollmentPlanByPlanID(planResponse.getPlanId());
					// Populate Enrollment Plan table with Plan data is it doesn't exists
					if(enrollmentPlan == null){
						enrollmentPlan = new EnrollmentPlan();
						enrollmentPlan.setPlanId(planResponse.getPlanId());
						
						if(isNotNullAndEmpty(planResponse.getDeductible())){
							enrollmentPlan.setIndivDeductible(Double.valueOf(planResponse.getDeductible()));
						}
						
						if(isNotNullAndEmpty(planResponse.getOopMax())){
							enrollmentPlan.setIndivOopMax(Double.valueOf(planResponse.getOopMax()));
						}
						
						if(isNotNullAndEmpty(planResponse.getOopMaxFamily())){
							enrollmentPlan.setFamilyOopMax(Double.valueOf(planResponse.getOopMaxFamily()));
						}
						
						if(isNotNullAndEmpty(planResponse.getDeductibleFamily())){
							enrollmentPlan.setFamilyDeductible(Double.valueOf(planResponse.getDeductibleFamily()));
						}
						
						enrollmentPlan.setGenericMedication(planResponse.getGenericMedications());
						enrollmentPlan.setOfficeVisit(planResponse.getOfficeVisit());
						enrollmentPlan.setNetworkType(planResponse.getNetworkType());
						enrollmentPlan.setPhoneNumber(planResponse.getIssuerPhone());
						enrollmentPlan.setIssuerSiteUrl(planResponse.getIssuerSite());
					}
					enrollment.setEnrollmentPlan(enrollmentPlan);
				}
				setEnrollmentCommissionData(enrollment, planResponse);
			}
			else{
				throw new GIException("No Plans found");
			}

			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.PLAN_TYPE))) {
				enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueLabel("INSURANCE_TYPE", plan.get(EnrollmentPrivateConstants.PLAN_TYPE).toString().toLowerCase()));
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.ORDER_ITEM_ID))) {
//				PldOrderItem pldOrderItem = new PldOrderItem();
//				pldOrderItem.setId(Integer.parseInt(plan.get(EnrollmentPrivateConstants.ORDER_ITEM_ID)));
				enrollment.setPdOrderItemId(Long.valueOf(plan.get(EnrollmentConstants.ORDER_ITEM_ID).trim()));
			}

			Enrollee responsiblePersonEnrollee = null;
			Map<String, String> responsiblePersonMap = responsiblePerson;
			if (responsiblePersonMap != null && isNotNullAndEmpty(responsiblePersonMap.get(EnrollmentPrivateConstants.RESPONSIBLE_PERSON_ID))) {
				responsiblePersonEnrollee = new Enrollee();
				responsiblePersonEnrollee.setExchgIndivIdentifier(responsiblePersonMap.get(EnrollmentPrivateConstants.RESPONSIBLE_PERSON_ID));
				responsiblePersonEnrollee.setFirstName(responsiblePersonMap.get("responsiblePersonFirstName"));
				responsiblePersonEnrollee.setMiddleName(responsiblePersonMap.get("responsiblePersonMiddleName"));
				responsiblePersonEnrollee.setLastName(responsiblePersonMap.get("responsiblePersonLastName"));
				//				responsiblePersonEnrollee.setSsn(responsiblePersonMap.get("responsiblePersonSsn"));
				responsiblePersonEnrollee.setTaxIdNumber(responsiblePersonMap.get("responsiblePersonSsn"));
				responsiblePersonEnrollee.setSuffix(responsiblePersonMap.get("responsiblePersonSuffix"));
				responsiblePersonEnrollee.setPreferredEmail(responsiblePersonMap.get("responsiblePersonPreferredEmail"));
				responsiblePersonEnrollee.setPreferredSMS(responsiblePersonMap.get("responsiblePersonPreferredPhone"));
				responsiblePersonEnrollee.setPrimaryPhoneNo(responsiblePersonMap.get("responsiblePersonPrimaryPhone"));
				responsiblePersonEnrollee.setSecondaryPhoneNo(responsiblePersonMap.get("responsiblePersonSecondaryPhone"));
				responsiblePersonEnrollee.setRespPersonIdCode(EnrollmentPrivateConstants.RESPONSIBLE_PERSON_ID_CODE_QD);
				if(isNotNullAndEmpty(responsiblePersonMap.get("responsiblePersonHomeAddress1"))){
					Location responsiblePersonAddress = new Location();
					responsiblePersonAddress.setAddress1(responsiblePersonMap.get("responsiblePersonHomeAddress1"));
					responsiblePersonAddress.setAddress2(responsiblePersonMap.get("responsiblePersonHomeAddress2"));
					responsiblePersonAddress.setCity(responsiblePersonMap.get("responsiblePersonHomeCity"));
					responsiblePersonAddress.setState(responsiblePersonMap.get("responsiblePersonHomeState"));

					if(isNotNullAndEmpty(responsiblePersonMap.get("responsiblePersonHomeZip"))){
						responsiblePersonAddress.setZip(responsiblePersonMap.get("responsiblePersonHomeZip"));	
					}

					responsiblePersonEnrollee.setHomeAddressid(responsiblePersonAddress);

				}
				responsiblePersonEnrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON));
				responsiblePersonEnrollee.setCreatedBy(getLoggedInUser());
				//enrollment.setResponsiblePerson(responsiblePerson);
			}

			Enrollee houseHoldContactEnrollee = null;
			Map<String, String> householdContactMap = householdContact;
			if (isNotNullAndEmpty(householdContactMap) && isNotNullAndEmpty(householdContactMap.get(EnrollmentPrivateConstants.HOUSEHOLD_CONTACT_ID))) {
				houseHoldContactEnrollee = new Enrollee();
				houseHoldContactEnrollee.setExchgIndivIdentifier(householdContactMap.get(EnrollmentPrivateConstants.HOUSEHOLD_CONTACT_ID));
				houseHoldContactEnrollee.setFirstName(householdContactMap.get("houseHoldContactFirstName"));
				houseHoldContactEnrollee.setMiddleName(householdContactMap.get("houseHoldContactMiddleName"));
				houseHoldContactEnrollee.setLastName(householdContactMap.get("houseHoldContactLastName"));
				houseHoldContactEnrollee.setSuffix(householdContactMap.get("houseHoldContactSuffix"));
				houseHoldContactEnrollee.setPreferredEmail(householdContactMap.get("houseHoldContactPreferredEmail"));
				houseHoldContactEnrollee.setPreferredSMS(householdContactMap.get("houseHoldContactPreferredPhone"));
				houseHoldContactEnrollee.setPrimaryPhoneNo(householdContactMap.get("houseHoldContactPrimaryPhone"));
				houseHoldContactEnrollee.setSecondaryPhoneNo(householdContactMap.get("houseHoldContactSecondaryPhone"));
				houseHoldContactEnrollee.setTaxIdNumber(householdContactMap.get("houseHoldContactFederalTaxIdNumber"));
				houseHoldContactEnrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.GENDER, householdContactMap.get(EnrollmentPrivateConstants.GENDER_CODE)));
				if(isNotNullAndEmpty(householdContactMap.get(EnrollmentPrivateConstants.FFM_PERSON_ID))){
					houseHoldContactEnrollee.setFfmPersonId(householdContactMap.get(EnrollmentPrivateConstants.FFM_PERSON_ID));
				}
				
				if(isNotNullAndEmpty(householdContactMap.get("houseHoldContactHomeAddress1"))){
					Location houseHoldContactAddress = new Location();
					houseHoldContactAddress.setAddress1(householdContactMap.get("houseHoldContactHomeAddress1"));
					houseHoldContactAddress.setAddress2(householdContactMap.get("houseHoldContactHomeAddress2"));
					houseHoldContactAddress.setCity(householdContactMap.get("houseHoldContactHomeCity"));
					houseHoldContactAddress.setState(householdContactMap.get("houseHoldContactHomeState"));
					
					//houseHoldContactEnrollee.setGenderLkp(householdContactMap.get("houseHoldContactGender"));
					if(isNotNullAndEmpty(householdContactMap.get("houseHoldContactHomeZip"))){
						houseHoldContactAddress.setZip(householdContactMap.get("houseHoldContactHomeZip"));	
					}
					houseHoldContactEnrollee.setHomeAddressid(houseHoldContactAddress);
				}
				
				houseHoldContactEnrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT));
				//	enrollment.setHouseholdContact(houseHoldContact);
			}

			List<PldMemberData> memberInfoList = pdlPlan.getMemberInfoList();
			List<Enrollee> enrolleeList = getEnrolleeFromMemberInfoList(memberInfoList, effectiveDate , enrollmentReason);
			if (houseHoldContactEnrollee != null){
				houseHoldContactEnrollee.setCreatedBy(getLoggedInUser());
				houseHoldContactEnrollee.setUpdatedBy(getLoggedInUser());
				enrolleeList.add(houseHoldContactEnrollee);
			}
			if (responsiblePersonEnrollee != null){
				responsiblePersonEnrollee.setCreatedBy(getLoggedInUser());
				responsiblePersonEnrollee.setUpdatedBy(getLoggedInUser());
				enrolleeList.add(responsiblePersonEnrollee);
			}

			// II Sprint HIX-7680
			//			enrollment.setCsrAmt(planResponse.getCsrAmount()*enrolleeList.size());
			enrollment.setCsrAmt(planResponse.getCsrAmount());		
			saveEnrolleeRelationship(memberInfoList,enrolleeList);

			

			//Find subscriber
			Enrollee subscriber = null;
			for (Enrollee enrollee:enrolleeList){
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().equals(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER))){
					//Found the subscriber
					subscriber = enrollee;
					StringBuilder subscriberName = new StringBuilder();

					if(isNotNullAndEmpty(subscriber.getFirstName())){
						subscriberName.append(subscriber.getFirstName());
						subscriberName.append(" ");
					}
					if(isNotNullAndEmpty(subscriber.getMiddleName())){
						subscriberName.append(subscriber.getMiddleName());
						subscriberName.append(" ");	
					}
					if(isNotNullAndEmpty(subscriber.getLastName())){
						subscriberName.append(subscriber.getLastName());
						subscriberName.append(" ");
					}
					if(isNotNullAndEmpty(subscriber.getSuffix())){
						subscriberName.append(subscriber.getSuffix());	
					}
					enrollment.setExchgSubscriberIdentifier(subscriber.getExchgIndivIdentifier());
					enrollment.setSubscriberName(subscriberName.toString().trim());
					break;
				}
			}

			if(subscriber != null){
				int subscriberAge = getAge(subscriber.getBirthDate());
				if(subscriberAge < EnrollmentPrivateConstants.AGE_18 && houseHoldContactEnrollee != null && 
						(isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.MARKET_TYPE))&& plan.get(EnrollmentPrivateConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)))
				{
					subscriber = houseHoldContactEnrollee;
				}

			/*	StringBuffer subscriberName = new StringBuffer();*/
				StringBuilder subscriberName = new StringBuilder();

				if(isNotNullAndEmpty(subscriber.getFirstName())){
					subscriberName.append(subscriber.getFirstName());
					subscriberName.append(" ");
				}
				if(isNotNullAndEmpty(subscriber.getMiddleName())){
					subscriberName.append(subscriber.getMiddleName());
					subscriberName.append(" ");	
				}
				if(isNotNullAndEmpty(subscriber.getLastName())){
					subscriberName.append(subscriber.getLastName());
					subscriberName.append(" ");
				}
				if(isNotNullAndEmpty(subscriber.getSuffix())){
					subscriberName.append(subscriber.getSuffix());	
				}
				enrollment.setSponsorName(subscriberName.toString().trim());
				
				if(isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.MARKET_TYPE))){
					if (plan.get(EnrollmentPrivateConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)) {
						enrollment.setSponsorEIN(subscriber.getTaxIdNumber());
					}
					else if (plan.get(EnrollmentPrivateConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
						enrollment.setSponsorTaxIdNumber(subscriber.getTaxIdNumber());
					}
				}
			}
			else {
				LOGGER.info("Could not find Subscriber for the Enrollment ID:" + enrollment.getId());
			}

			boolean isEmployerStatusActiveAndEnrollmentReasonNew=false;
			// HIX-6649, HIX-6695 :: the below snippets is to populate the employeeId, employee contribution, employer contribution in enrollment if the enrollment type is "SHOP" 
			if(!isPHIXBBUser() && (enrollment.getEnrollmentTypeLkp()!=null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP))){
				if (isNotNullAndEmpty(employerId)) {
					Employer employer = new Employer();
					employer.setId(Integer.parseInt(employerId));
					enrollment.setEmployer(employer);
					
					LOGGER.info("Calling shop service to get employer info for employerId ");
					//LOGGER.info("shop service URL == "+ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId);
//					String shopInfoResponse=null;
					EmployerDTO employerDTO = null;

					String postResponse =null;
					try{
						postResponse = restTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId, String.class);

					}catch(RestClientException re){
						LOGGER.error("Message : "+re.getMessage());
						LOGGER.error("Cause : "+re.getCause());
						LOGGER.error(" ",re);
					}

					XStream xstream = GhixUtils.getXStreamStaxObject();
					ShopResponse shopResponse= new ShopResponse();
					shopResponse = (ShopResponse) xstream.fromXML(postResponse);
					if(shopResponse != null){
						
						employerDTO = (EmployerDTO) shopResponse.getResponseData().get("Employer");
												
						if(employerDTO != null){
							
							if(employerDTO.getName() != null){			            	
								enrollment.setSponsorName(employerDTO.getName());
							}
							if(employerDTO.getExternalId() != null){
								enrollment.setEmployerCaseId(employerDTO.getExternalId());
							}
							
							
							if(employerDTO.getFederalEIN() != null){	
								enrollment.setSponsorEIN(employerDTO.getFederalEIN().toString());
							}
							
							if(employerDTO.getExternalId()!= null){	
								enrollment.setExternalEmployerId(employerDTO.getExternalId());
							}
												
						}
						
						// setting the enrollment status to PAYMENT_RECEIVED when the employer enrollment status is ACTIVE & enrollment reason is 'N' 
						EmployerEnrollment.Status employerEnrollmentStatus = (EmployerEnrollment.Status) shopResponse.getResponseData().get(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1); 
						if((employerEnrollmentStatus!=null && enrollmentReason!=null) && (employerEnrollmentStatus.equals(Status.ACTIVE) && enrollmentReason.toString().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_NEW))){
							enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_PAYMENT_RECEIVED));
							isEmployerStatusActiveAndEnrollmentReasonNew=true;
						}
						
					}
				}
				else{
					throw new GIException("No Employer found");
				}

				if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.EMPLOYEE_CONTRIBUTION))) {
					enrollment.setEmployerContribution(Float.parseFloat(plan.get(EnrollmentPrivateConstants.EMPLOYEE_CONTRIBUTION)));
				}

				if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.DEPENDENT_CONTRIBUTION))) {
					enrollment.setEmployerContribution(enrollment.getEmployerContribution() + Float.parseFloat(plan.get(EnrollmentPrivateConstants.DEPENDENT_CONTRIBUTION)));
				}

				if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.NET_PREMIUM_AMT))) {
					enrollment.setEmployeeContribution(Float.parseFloat(plan.get(EnrollmentPrivateConstants.NET_PREMIUM_AMT)));	
				}

			}
			
			List<EnrollmentEvent> enrollmentEventList= new ArrayList<EnrollmentEvent>();

			for (Enrollee tempenrollee : enrolleeList) {

				tempenrollee.setEnrollment(enrollment);
				if(tempenrollee.getCustodialParent()!=null){
					tempenrollee.getCustodialParent().setEnrollment(enrollment);
				}
				// setting the enrollee status to PAYMENT_RECEIVED when the employer enrollment status is ACTIVE & enrollment reason is 'N' or isEmployerStatusActiveAndEnrollmentReasonNew is true)
				if(isEmployerStatusActiveAndEnrollmentReasonNew &&
					(tempenrollee.getPersonTypeLkp() != null && ! tempenrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON) && ! tempenrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT))){
						tempenrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_PAYMENT_RECEIVED));
				}
				
				//creating EnrollmentEvent
				EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
				enrollmentEvent.setCreatedBy(getLoggedInUser());
				enrollmentEvent.setEnrollee(tempenrollee);
				enrollmentEvent.setEnrollment(enrollment);
				enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION));
				enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION));

				//added last event Id for Audit
				tempenrollee.setLastEventId(enrollmentEvent);
				enrollmentEventList.add(enrollmentEvent);
			}
			enrollment.setEnrollees(enrolleeList);
			enrollment.setCreatedBy(getLoggedInUser());
			enrollment.setEnrollmentEvents(enrollmentEventList);
			enrollmentList.add(enrollment);
		}
		return enrollmentList;
	}
	
	 
	private int getAge(Date dateOfBirth) {
	    int age = 0;
	    Calendar born = TSCalendar.getInstance();
	    Calendar now = TSCalendar.getInstance();
	    if(dateOfBirth!= null) {
	        now.setTime(new TSDate());
	        born.setTime(dateOfBirth);  
	        if(born.after(now)) {
	            throw new IllegalArgumentException("Can't be born in the future");
	        }
	        age = now.get(Calendar.YEAR) - born.get(Calendar.YEAR);             
	        if(now.get(Calendar.DAY_OF_YEAR) < born.get(Calendar.DAY_OF_YEAR))  {
	            age-=1;
	        }
	    }  
	    return age;
	}
	@Override
	public void updateShopEnrollmentStatus(EnrollmentRequest enrollmentRequest){
		
		int enrollmentTypeShopId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE,EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP);
		int enrollmentStatusPendingId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING);
		List<Enrollment> enrollmentList = null;
		try{
		enrollmentList = enrollmentRepository.getPendingShopEnrollment(enrollmentRequest.getEmployerId(),enrollmentTypeShopId,enrollmentStatusPendingId);
		}catch(Exception e)
			{
			LOGGER.error("Error occured in updateShopEnrollemntToEnrolled" , e);
			}
		
		//EnrollmentStatus 
		int enrollmentStatusId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,enrollmentRequest.getEnrollmentStatus().toString());
		LookupValue enrolledLookup = new LookupValue();
		enrolledLookup.setLookupValueId(enrollmentStatusId);
		
		// Getting user
		AccountUser user = (enrollmentRequest.getLoggedInUserName()!=null)?getUserByName(enrollmentRequest.getLoggedInUserName()) : null;
		
		if(null != enrollmentList){
			// Iterating the enrollment 
			for (Enrollment enrollment : enrollmentList) {
				
				boolean isEnrolleeStatusInPending = false;
				
				// fetching all the enrollees for that enrollment 
				//List<Enrollee> enrolleeList = enrollment.getEnrollees();
				
				//fetch only members for the Enrollment 
				List<Enrollee> enrolleeList= enrolleeRepository.getEnrolleeByEnrollmentID(enrollment.getId()) ;
				// checking the enrolleeList is not null 
				if(enrolleeList !=null){
					
					// iterating the enrollee from the enrolleeList
					for(Enrollee enrollee : enrolleeList) {
					
						// check for the enrollee status is PENDING
					   if(enrollee.getEnrolleeLkpValue().getLookupValueId().equals(enrollmentStatusPendingId)){
						
						   // setting the enrollee status  
						   enrollee.setEnrolleeLkpValue(enrolledLookup);
						   if(enrolledLookup.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
							   enrollee.setDisenrollTimestamp(new TSDate());
						   }
						   
						   // setting the enrollment event for the given enrollment, enrollee, event type, event reason code, user.
						   enrollee.setEnrollmentEvents(createEventForEnrolleeAndEnrollment(enrollment, enrollee, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION, user));
						   
						   isEnrolleeStatusInPending =true;
						  					   
					   } // end of the enrollee status check. 
					 
					} // end of the enrollee for ...loop
					
					if(isEnrolleeStatusInPending){
						// setting the enrollment status 
						enrollment.setEnrollmentStatusLkp(enrolledLookup);
			
					   // saving the enrollment
						enrollmentRepository.saveAndFlush(enrollment);
					}	
							
				} // end of the enrolleeList null check
				
			 } // end of the enrollment for ...loop. 
		}
	}
	
	/**
	 * @since 06/03/2013
	 * @author raja
	 * @param enrollment
	 * @param eventType
	 * @param eventReasonCode
	 * @param user
	 * @return enrollmentEventList
	 * 
	 * This method is used to Create EnrollmentEvent object by taking input parameters are 
	 * enrollment,enrollee,eventType,eventReasonCode,user and returns the EnrollmentEvent list.
	 * */
	private List<EnrollmentEvent> createEventForEnrolleeAndEnrollment(Enrollment enrollmentObj, Enrollee enrolleeObj, String eventType ,String eventReasonCode, AccountUser user){
		List<EnrollmentEvent> enrollmentEventList = enrolleeObj.getEnrollmentEvents();
		if(enrollmentEventList != null){
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
			enrollmentEvent.setEnrollment(enrollmentObj);
			enrollmentEvent.setEnrollee(enrolleeObj);
			//add null check
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE,eventType));
			if(isNotNullAndEmpty(eventReasonCode)){
					if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON, eventReasonCode))){
						enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON, eventReasonCode));
					}else{
						throw new RuntimeException(EnrollmentConstants.ERR_MSG_LOOKUP_VALUE_IS_NULL);
					}
			}else{
				throw new RuntimeException(EnrollmentConstants.ERR_MSG_REASON_CODE_VALUE);
			}
			if(user!=null){
				enrollmentEvent.setCreatedBy(user);
			}
			//added last event Id for Audit
			enrolleeObj.setLastEventId(enrollmentEvent);
			enrollmentEventList.add(enrollmentEvent);
		}
	 return enrollmentEventList;
	}
	
	@Override
	public AccountUser getUserByName(String userName){
	  return userService.findByUserName(userName);
	}
	
	
	//  HIX-67133 Remove DB FK Constraint and Model reference for PldOrderItem from Enrollment
	//	@Override
	//	public List<Enrollment> findByIndOrderItemId(Integer indOrderItemId) {
	//		return enrollmentRepository.findByIndOrderItemId(indOrderItemId);
	//	}
	
	private void saveEsignatureForEnrollment(List<Enrollment> enrollments , String signature,  String taxFiler_esign){
		Esignature esignature = null;
		if(signature!=null){
			for (Enrollment enrollment : enrollments) {
				esignature = new Esignature();
				esignature.setModuleName(ModuleUserService.ENROLLMENT_MODULE);
				esignature.setRefId(enrollment.getEnrollmentEvents().get(0).getId());
				esignature.seteSignature(signature);
				esignature.setEsignDate(new TSDate());
			
				// saving the esignature object in ESignature table.
				esignature = esignatureService.saveEsignature(esignature);
				
				EnrollmentEsignature enrollmentEsig = new EnrollmentEsignature();
				enrollmentEsig.setEnrollment(enrollment);
				enrollmentEsig.setEsignature(esignature);
				enrollmentEsignatureRepository.save(enrollmentEsig);
			}
		}
		
		if(taxFiler_esign!=null){
			for (Enrollment enrollment : enrollments) {
				esignature = new Esignature();
				esignature.setModuleName(ModuleUserService.ENROLLMENT_MODULE);
				esignature.setRefId(enrollment.getEnrollmentEvents().get(0).getId());
				esignature.seteSignature(taxFiler_esign);
				esignature.setEsignDate(new TSDate());
			
				// saving the esignature object in ESignature table.
				
				esignature = esignatureService.saveEsignature(esignature);
				
				EnrollmentEsignature enrollmentEsig = new EnrollmentEsignature();
				enrollmentEsig.setEnrollment(enrollment);
				enrollmentEsig.setEsignature(esignature);
				enrollmentEsig.setTaxFillerEsignIndicator("Y");
				enrollmentEsignatureRepository.save(enrollmentEsig);
			}
		}
	}
 	
	@Override
	public void updateEnrollment (String issuerSubscriberIdentifier, Map<String, Object> enrollmentValues){
		
		//Get Enrollment for given "issuerSubscriberIdentifier"
		Enrollment enrollment = enrollmentRepository.findByIssuerSubscriberIdentifier(issuerSubscriberIdentifier);
		
		// Update Enrollment with given Map values
		enrollment.setAgentBrokerName((String)enrollmentValues.get("AGENT_BROKER_NAME"));
		enrollment.setAptcAmt((Float)enrollmentValues.get("APTC_AMT"));
		enrollment.setBenefitEffectiveDate((Date)enrollmentValues.get("BENEFIT_EFFECTIVE_DATE"));
		enrollment.setBenefitEndDate((Date)enrollmentValues.get("BENEFIT_END_DATE"));
		enrollment.setBrokerFEDTaxPayerId((String)enrollmentValues.get("BROKER_FED_TAX_PAYER_ID"));
		enrollment.setBrokerTPAAccountNumber1((String)enrollmentValues.get("BROKER_TPA_ACCOUNT_NUMBER_1"));
		enrollment.setBrokerTPAAccountNumber2((String)enrollmentValues.get("BROKER_TPA_ACCOUNT_NUMBER_2"));
		enrollment.setBrokerTPAFlag((String)enrollmentValues.get("BROKER_TPA_FLAG"));
		enrollment.setCMSPlanID((String)enrollmentValues.get("CMS_PLAN_ID"));
		enrollment.setCsrAmt((Float)enrollmentValues.get("CSR_AMT"));
		
		enrollment.setEmployeeContribution((Float)enrollmentValues.get("EMPLOYEE_CONTRIBUTION"));
				
		Employer employer = new Employer();
		employer.setId((Integer)enrollmentValues.get("EMPLOYER_ID"));
		enrollment.setEmployer(employer);
		
		enrollment.setEmployerContribution((Float)enrollmentValues.get("EMPLOYER_CONTRIBUTION"));
		//While using this method try to set the value of house hold case id to 
		//enrollment.setExchgSubscriberIdentifier((String)enrollmentValues.get("EXCHG_SUBSCRIBER_IDENTIFIER"));
		
		LookupValue enrollmentStLk = new LookupValue();
		enrollmentStLk.setLookupValueId((Integer)enrollmentValues.get("ENROLLMENT_STATUS_LKP"));
		enrollment.setEnrollmentStatusLkp(enrollmentStLk);
		
		if(enrollmentStLk != null && enrollmentStLk.getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)){
			enrollment.setAbortTimestamp(new TSDate());
		}
		
		LookupValue enrollmentTyLk = new LookupValue();
		enrollmentTyLk.setLookupValueId((Integer)enrollmentValues.get("ENROLLMENT_TYPE_LKP"));
		enrollment.setEnrollmentTypeLkp(enrollmentTyLk);
		
		enrollment.setFamilyCoverageTier((String)enrollmentValues.get("FAMILY_COVERAGE_TIER"));
		
		FinancialInfo financialInfo = new FinancialInfo();
		financialInfo.setId((Integer)enrollmentValues.get("FINANCIAL_ID"));
		enrollment.setFinancialInfo(financialInfo);
		
		enrollment.setGroupPolicyNumber((String)enrollmentValues.get("GROUP_POLICY_NUMBER"));
		
		enrollment.setGrossPremiumAmt((Float)enrollmentValues.get(EnrollmentPrivateConstants.GROSS_PREMIUM_AMT));
		
		PldOrderItem pldOrderItem = new PldOrderItem();
		pldOrderItem.setId((Integer)enrollmentValues.get("IND_ORDER_ITEMS_ID"));
//		enrollment.setIndOrderItem(pldOrderItem);
		
		LookupValue insuranceLk = new LookupValue();
		insuranceLk.setLookupValueId((Integer)enrollmentValues.get("INSURANCE_TYPE_LKP"));
		enrollment.setInsuranceTypeLkp(insuranceLk);
		
		enrollment.setInsurerName((String)enrollmentValues.get("INSURER_NAME"));
		enrollment.setInsurerTaxIdNumber((String)enrollmentValues.get("INSURER_TAX_ID_NUMBER"));
			
		enrollment.setIssuerSubscriberIdentifier((String)enrollmentValues.get("ISSUER_SUBSCRIBER_IDENTIFIER"));
		
		PlanRequest planRequest= new PlanRequest();
		planRequest.setPlanId(enrollmentValues.get("PLAN_ID").toString());
		String planInfoResponse = null;
		try{
			//planInfoResponse = restTemplate.postForObject(ghixPlanMgmtPlanDataURL,planRequest,String.class);
			planInfoResponse = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL,planRequest,String.class);
//			LOGGER.info("getting successfull response of the plan data from plan management."+planInfoResponse);
			LOGGER.info("Successfull response of the plan data from plan management.");
		}
		catch(Exception ex){
			//	ex.printStackTrace();
			LOGGER.error("exception in getting plan data from the plan management" , ex);
		}
		
		PlanResponse planResponse = new PlanResponse();
		planResponse = platformGson.fromJson(planInfoResponse, planResponse.getClass());
		
		enrollment.setPlanId(planResponse.getPlanId());
		enrollment.setHiosIssuerId(planResponse.getHiosIssuerId());
		enrollment.setIssuerId(planResponse.getIssuerId());
		
		// Fetch Enrollment_Plan data for Plan
		EnrollmentPlan enrollmentPlan = enrollmentPlanRepository.findEnrollmentPlanByPlanID(planResponse.getPlanId());
		// Populate Enrollment Plan table with Plan data is it doesn't exists
		if(enrollmentPlan == null){
			enrollmentPlan = new EnrollmentPlan();
			enrollmentPlan.setPlanId(planResponse.getPlanId());
			
			if(isNotNullAndEmpty(planResponse.getDeductible())){
				try{
					enrollmentPlan.setIndivDeductible(Double.valueOf(planResponse.getDeductible()));
				}
				catch (Exception e){
					LOGGER.error("Error parsing value of double,setting value to 0.",e);
					enrollmentPlan.setIndivDeductible(Double.valueOf(0));
				}
			}
			
			if(isNotNullAndEmpty(planResponse.getOopMax())){
				try{
					enrollmentPlan.setIndivOopMax(Double.valueOf(planResponse.getOopMax()));
				}
				catch (Exception e){
					LOGGER.error("Error parsing value of double,setting value to 0.",e);
					enrollmentPlan.setIndivOopMax(Double.valueOf(0));
				}
			}
			
			if(isNotNullAndEmpty(planResponse.getOopMaxFamily())){
				try{
					enrollmentPlan.setFamilyOopMax(Double.valueOf(planResponse.getOopMaxFamily()));
				}
				catch (Exception e){
					LOGGER.error("Error parsing value of double,setting value to 0.",e);
					enrollmentPlan.setFamilyOopMax(Double.valueOf(0));
				}
			}
			
			if(isNotNullAndEmpty(planResponse.getDeductibleFamily())){
				try{
					enrollmentPlan.setFamilyDeductible(Double.valueOf(planResponse.getDeductibleFamily()));
				}
				catch (Exception e){
					LOGGER.error("Error parsing value of double,setting value to 0.",e);
					enrollmentPlan.setFamilyDeductible(Double.valueOf(0));
				}
			}
			
			enrollmentPlan.setGenericMedication(planResponse.getGenericMedications());
			enrollmentPlan.setOfficeVisit(planResponse.getOfficeVisit());
			enrollmentPlan.setNetworkType(planResponse.getNetworkType());
			enrollmentPlan.setPhoneNumber(planResponse.getIssuerPhone());
			enrollmentPlan.setIssuerSiteUrl(planResponse.getIssuerSite());
			enrollment.setEnrollmentPlan(enrollmentPlan);
		}
		
		enrollment.setPlanCoverageLevel((String)enrollmentValues.get("PLAN_COVERAGE_LEVEL"));
		enrollment.setPlanName((String)enrollmentValues.get("PLAN_NAME"));
		enrollment.setSponsorEIN((String)enrollmentValues.get("SPONSOR_EIN"));
		enrollment.setSponsorName((String)enrollmentValues.get("SPONSOR_NAME"));
		enrollment.setSponsorTaxIdNumber((String)enrollmentValues.get("SPONSOR_TAX_ID_NUMBER"));
		enrollment.setSubscriberName((String)enrollmentValues.get("SUBSCRIBER_NAME"));
		//enrollment.setUpdatedBy(?);
		enrollment.setUpdatedOn(new TSDate());
		
		EmployerLocation employerLocation = new EmployerLocation();
		employerLocation.setId((Integer)enrollmentValues.get("WORKSITE_ID"));
		enrollment.setWorksite(employerLocation);
		setEnrollmentSubmittedBy(enrollment);
		//Saving Updated Enrollment
		enrollmentRepository.saveAndFlush(enrollment);
	}


	/*@Override
	public Issuer getIssuerByinsurerTaxIdNumber(String insurerTaxIdNumber) {
		LOGGER.info(" getIssuerByinsurerTaxIdNumber SERVICE ");
		return enrollmentRepository.getIssuerByinsurerTaxIdNumber(insurerTaxIdNumber);
	}*/

	private Enrollee checkForSubscriber(List<Enrollee> enrolleeList) {
		if(enrolleeList !=null && !enrolleeList.isEmpty()){
			for(Enrollee enrollee:enrolleeList){
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER)){
					return enrollee;
				}
			}
		}
		return null;	
	}
	 
	/**
	 * @since
	 * @author parhi_s
	 * @param enrollment
	 * @param user
	 * @throws Exception
	 * 
	 * This method is used to set Enrollment Status after the
	 * changes to Enrollee has been done during disEnrollment
	 * Process
	 * 
	 */
	@Override
	public void checkEnrollmentStatus(Enrollment enrollment, AccountUser user){
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		if (enrollment != null) {
			try {
				enrolleeList = enrolleeService.getEnrolleeByEnrollmentID(enrollment.getId());
				// Find Subscriber
				Enrollee subscriber = checkForSubscriber(enrolleeList);
				int cancellledEnrolleeCount = 0;
				int terminatedEnrolleeCount = 0;
				int comfirmedEnrolleeCount = 0;
				int paymentReceivedEnrolleeCount=0;
				int pendingEnrolleeCount = 0;
				int totalEnrolleeCount = 0;
				if (enrolleeList != null && !enrolleeList.isEmpty()) {
					totalEnrolleeCount = enrolleeList.size();
					for (Enrollee enrollee : enrolleeList) {
						if (enrollee.getEnrolleeLkpValue() != null) {
							if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)) {
								cancellledEnrolleeCount++;
							} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM)) {
								terminatedEnrolleeCount++;
							} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM)) {
								comfirmedEnrolleeCount++;
							} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_PAYMENT_RECEIVED)) {
								paymentReceivedEnrolleeCount++;
							} else if(enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING)){
								pendingEnrolleeCount++;
							}
						}
					}
				}
				if (totalEnrolleeCount == cancellledEnrolleeCount) {
					enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL));
					enrollment.setAbortTimestamp(new TSDate());
					if(subscriber != null){
						enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
					}
				}else if (totalEnrolleeCount == paymentReceivedEnrolleeCount) {
					enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,EnrollmentPrivateConstants.ENROLLMENT_PAYMENT_RECEIVED));
				}else if (totalEnrolleeCount == (cancellledEnrolleeCount + terminatedEnrolleeCount)) {
					enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM));
					if(subscriber != null && subscriber.getEffectiveStartDate() != null){
				        // set the enrollment BenefitEndDate to previous day of subscriber benefit start date
						Date terminationDate = null;
						if(enrollment.getEnrollmentTypeLkp()!=null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
							terminationDate=subscriber.getEffectiveEndDate();
						}
						else if(isNotNullAndEmpty(subscriber.getEffectiveStartDate())){
							terminationDate = this.getBeforeDayDate(subscriber.getEffectiveStartDate());
						}
						enrollment.setBenefitEndDate(terminationDate);
					}
					
				}else if(comfirmedEnrolleeCount>0 &&paymentReceivedEnrolleeCount==0 && pendingEnrolleeCount==0 && enrollment.getEnrollmentStatusLkp()!=null && (enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING) || enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_PAYMENT_RECEIVED )) ){
					enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM));
					enrollment.setEnrollmentConfirmationDate(new TSDate());
				}
				enrollment.setUpdatedBy(user);
				enrollment.setUpdatedOn(new TSDate());
				setEnrollmentSubmittedBy(enrollment);
				enrollmentRepository.saveAndFlush(enrollment);
			} catch (GIException e) {
				LOGGER.error("Error occured in getEnrolleeByEnrollmentID " , e);
			}
		}
	}
	/**
	 * @since
	 * @author parhi_s
	 * @param enrollee
	 * @param maintReasonCode
	 * @param user
	 * 
	 * This method is Called to create EnrollmentEvent during disEnrollment
	 * 
	 */
	@Override
	public void createEventForEnrollee(Enrollee enrollee,String eventType ,String eventReasonCode, AccountUser user){
		
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
			
			enrollmentEvent.setEnrollee(enrollee);
			enrollmentEvent.setEnrollment(enrollee.getEnrollment());
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE,eventType));
			//add null check
			if(isNotNullAndEmpty(eventReasonCode)){
					if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON, eventReasonCode))){
						enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON, eventReasonCode));
					}else{
						throw new RuntimeException(EnrollmentConstants.ERR_MSG_LOOKUP_VALUE_IS_NULL);
					}
			}else{
				throw new RuntimeException(EnrollmentConstants.ERR_MSG_REASON_CODE_VALUE);
			}
			if(user!=null){
			enrollmentEvent.setCreatedBy(user);
			}
			//added last event Id for Audit
			enrollee.setLastEventId(enrollmentEvent);
			enrollmentEventService.saveEnrollmentEvent(enrollmentEvent);
	}

	/**
	 * @since
	 * @author ajinkya_m
	 * 
	 * This method is called to disEnroll an Enrollee by passing
	 * parameters enrollmentID, planID, memberID, effEndDate, status,
	 * maintReasonCode, User
	 */
	@Transactional
	@Override
	public String disEnrollByEnrollmentID(Map<String,Object> disEnrollmentMap) throws GIException{
		
		/* Excepts map with following keys
		 * MemberId , EnrollmentID, TerminationDate , EnrollmentStatus , DisEnrollmentType , TerminationReasonCode , DeathDate, User
		 * 
		 * */
		if(disEnrollmentMap.get(EnrollmentPrivateConstants.MEMBER_ID_KEY)==null || disEnrollmentMap.get(EnrollmentPrivateConstants.ENROLLMENT_ID_KEY)==null ||disEnrollmentMap.get(EnrollmentPrivateConstants.TERMINATION_DATE)==null ||disEnrollmentMap.get(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1)==null || disEnrollmentMap.get(EnrollmentPrivateConstants.TERMINATION_REASON_CODE)==null ){
			throw new GIException("Validation Error in disEnrollByEnrollmentID() :: Any of the following mandatory fields is null"+ 
																				"\n"+ "MemberId = "+disEnrollmentMap.get(EnrollmentPrivateConstants.MEMBER_ID_KEY) + 
																				"\n"+ "EnrollmentID ="+disEnrollmentMap.get(EnrollmentPrivateConstants.ENROLLMENT_ID_KEY)+
																				"\n"+ "TerminationDate ="+disEnrollmentMap.get(EnrollmentPrivateConstants.TERMINATION_DATE)+
																				"\n"+ "EnrollmentStatus ="+disEnrollmentMap.get(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1)+
																				"\n"+ "TerminationReasonCode ="+disEnrollmentMap.get(EnrollmentPrivateConstants.TERMINATION_REASON_CODE));
		}
		
		String memberId = (String) disEnrollmentMap.get(EnrollmentPrivateConstants.MEMBER_ID_KEY);
		Integer enrollmentID=(Integer)disEnrollmentMap.get(EnrollmentPrivateConstants.ENROLLMENT_ID_KEY);
		//Integer enrollmentID = Integer.parseInt((String) disEnrollmentMap.get(EnrollmentConstants.ENROLLMENT_ID_KEY));
		Enrollee enrollee = enrolleeService.findEnrolleeByEnrollmentIDAndMemberID(memberId, enrollmentID);
		
		if(enrollee != null){
			String enrolleeStatus=enrollee.getEnrolleeLkpValue().getLookupValueCode();
			if(enrolleeStatus!=null && (enrolleeStatus.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL) || enrolleeStatus.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM))){
				LOGGER.info("disEnrollByEnrollmentID():: The member "+ enrollee.getExchgIndivIdentifier()+ " is already disenrolled.");
			}else{
			AccountUser user = (AccountUser)disEnrollmentMap.get("User");
			String enrollmentStatus = (String)disEnrollmentMap.get(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1);
			if(enrollmentStatus.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)){
				enrollee.setEffectiveEndDate(enrollee.getEffectiveStartDate());
			}
			String disEnrollmentType = (String)disEnrollmentMap.get(EnrollmentPrivateConstants.DIS_ENROLLMENT_TYPE);
			if(enrollmentStatus.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM)){
				Date terminationDate = null;
				if(disEnrollmentType!=null && disEnrollmentType.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
					terminationDate= this.getMonthEndDate(DateUtil.StringToDate((String)disEnrollmentMap.get(EnrollmentPrivateConstants.TERMINATION_DATE), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				else if(isNotNullAndEmpty(enrollee.getEffectiveStartDate())){
					terminationDate = this.getBeforeDayDate(enrollee.getEffectiveStartDate());
				}
				enrollee.setEffectiveEndDate(terminationDate);
			}
			
			LookupValue lkpValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, (String)disEnrollmentMap.get(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1));
			if(lkpValue!= null)
			{
				enrollee.setEnrolleeLkpValue(lkpValue);
				if(lkpValue.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || lkpValue.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
					enrollee.setDisenrollTimestamp(new TSDate());
				}
			}
			enrollee.setUpdatedBy(user);
			
			if(disEnrollmentType!=null && disEnrollmentType.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
				String terminationReasonCode = disEnrollmentMap.get(EnrollmentPrivateConstants.TERMINATION_REASON_CODE).toString();
				
				if(terminationReasonCode != null && terminationReasonCode.equals(EnrollmentPrivateConstants.REASON_CODE_DEATH)){
					enrollee.setDeathDate(DateUtil.StringToDate((String)disEnrollmentMap.get(EnrollmentPrivateConstants.DEATH_DATE), GhixConstants.REQUIRED_DATE_FORMAT));
				}
			}
			// 	Save enrollee
			enrolleeRepository.saveAndFlush(enrollee);
			//	Create Event
			createEventForEnrollee(enrollee,EnrollmentPrivateConstants.EVENT_TYPE_CANCELLATION,disEnrollmentMap.get(EnrollmentPrivateConstants.TERMINATION_REASON_CODE).toString(), user);
			// 	Check if all the enrollee of given enrollment are cancel/terminated
			checkEnrollmentStatus(enrollee.getEnrollment() ,user);
			}
			return GhixConstants.RESPONSE_SUCCESS;
			
		}else{
			LOGGER.info("disEnrollByenrollmentID() ::"+ " No Enrollee Found for given memberID" );
			throw new GIException(EnrollmentPrivateConstants.ERROR_CODE_201,"No member found for given Member id : "+memberId + " and Enrollment ID : "+enrollmentID ,"HIGH");
		}
	}

	/**
	 * @since
	 * @author parhi_s
	 * 
	 *         This method is called to disEnroll an Enrollee by passing
	 *         parameters eventID, planID, memberID, effEndDate, status,
	 *         maintReasonCode
	 */
	@Transactional
	@Override
	public void disEnrollByEventID(Integer eventID, Integer planID,
			String memberID, Date effEndDate, String status,
			String maintReasonCode) throws GIException {

		try {
			EnrollmentEvent enrollmentEvent = enrollmentEventService
					.findEnrollmentByeventIDAndplanID(eventID, planID);
			if (enrollmentEvent != null
					&& enrollmentEvent.getEnrollment() != null) {

				Enrollee enrollee = enrolleeService
						.findEnrolleeByEnrollmentIDAndMemberID(memberID,
								enrollmentEvent.getEnrollment().getId());

				if (enrollee != null) {

					AccountUser user = userService
							.findByUserName(GhixConstants.USER_NAME_CARRIER);

					
						LookupValue lkpValue = lookupService
								.getlookupValueByTypeANDLookupValueCode(
										EnrollmentPrivateConstants.ENROLLMENT_STATUS, status);
						if(lkpValue != null){
							enrollee.setEnrolleeLkpValue(lkpValue);
							if(lkpValue.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || lkpValue.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
								enrollee.setDisenrollTimestamp(new TSDate());
							}
							
						}
						enrollee.setEffectiveEndDate(effEndDate);
						enrollee.setUpdatedBy(user);

						enrolleeService.updateEnrollee(enrollee);
						createEventForEnrollee(enrollee,EnrollmentPrivateConstants.EVENT_TYPE_CANCELLATION, maintReasonCode, user);
						if (enrollee.getEnrollment() != null){
							checkEnrollmentStatus(enrollee
									.getEnrollment(), user);
					
					}

				} else {

					LOGGER.info("disEnrollByEventID() ::"
							+ " No Enrollee Found for memberID=" + memberID
							+ "and enrollmentID="
							+ enrollmentEvent.getEnrollment().getId());
				}
			} else {
				LOGGER.info("disEnrollByEventID() ::"
						+ " No Event Found for eventID=" + eventID
						+ "and planID=" + planID);
			}
		} catch (Exception e) {
			LOGGER.error("Exception In disEnrollByEventID() : " + e);
			throw new GIException("Exception for disEnrollByEventID()"+e.getMessage(),e);
		}

	}

	@Override
	public List<Enrollment> findEnrollmentsByPlan(String planId, String planLevel, String planMarket, String regionName, String startDate, String endDate, String status) {
	  
	 return enrollmentRepository.getEnrollmentsByPlanData(planId, planLevel, planMarket, regionName, startDate, endDate, status);
	}

	@Override
	public List<Enrollment> findEnrollmentsByIssuer(String issuerName, String planLevel, String planMarket, String regionName, String startDate, String endDate, String status) {
	
	  return enrollmentRepository.getEnrollmentsByIssuerData(issuerName, planLevel, planMarket, regionName, startDate, endDate, status);
	}
		
	@Override
	public Map<String, Object> getPlanIdAndOrderItemIdByEnrollmentId(Integer enrollmentId) {
		
		Map<String, Object> enrollmentMap = new HashMap<String, Object>();
		List<Object[]> enrollment = enrollmentRepository.getPlanIdAndOrderItemIdByEnrollmentId(enrollmentId);
		
		if(enrollment!=null){
		   for(Object[] enrollColData : enrollment){
			   enrollmentMap.put(EnrollmentPrivateConstants.PLAN_ID, enrollColData[0]);
		   }
		}
		return enrollmentMap;
	}
	
	@Override
	public List<EnrollmentCountDTO> findEnrollmentPlanLevelCountByBroker(Integer assisterBrokerId, String role, Date startDate, Date endDate) {
		List<EnrollmentCountDTO> enrollmentCountDTOList = new ArrayList<EnrollmentCountDTO>();
		
		List<String> enrollmentPlanLevelCountByBrokerList = enrollmentRepository.getEnrollmentPlanLevelCountByBroker(assisterBrokerId, role, startDate,endDate);
		if(enrollmentPlanLevelCountByBrokerList != null && !enrollmentPlanLevelCountByBrokerList.isEmpty()){
			for(PlanLevel planLevel: PlanLevel.values()){
				EnrollmentCountDTO enrollmentCountDTO = new EnrollmentCountDTO();
				enrollmentCountDTO.setPlanLevel(planLevel.toString());
				enrollmentCountDTO.setEnrollmentCount(Collections.frequency(enrollmentPlanLevelCountByBrokerList, planLevel.toString()));
				enrollmentCountDTOList.add(enrollmentCountDTO);
			}
		}

		return enrollmentCountDTOList;
	}

	@Override
	public List<Employer> findDistinctEmployer()
	{
		return enrollmentRepository.getDistinctEmployers();
	}
	
	@Override
	@Transactional
	public void abortEnrollment(Enrollment enrollment){
		try {
			List<Enrollee> enrolleeList = enrolleeService.getEnrolleeByEnrollmentID(enrollment.getId());
			for(Enrollee enrollee : enrolleeList){
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED));
				enrollee.setDisenrollTimestamp(new TSDate());
				enrolleeRepository.saveAndFlush(enrollee);
			}
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED));
			enrollment.setAbortTimestamp(new TSDate());
			//enrollment.setBenefitEndDate(new TSDate());
			enrollmentRepository.saveAndFlush(enrollment);
		} catch (GIException e) {
			LOGGER.error("Error in aborting enrollment.",e);
			LOGGER.error(e.getMessage());
		}
	}

	@Override
	public boolean isEnrollmentExists(int enrollmentId)
	{
		return enrollmentRepository.exists(enrollmentId);
	}
		/**
	 * Used for Special enrollment. To update existing {@link Enrollee} using Plan Display data response data
	 * 
	 * Since 24-08-2013
	 * 
	 * @param enrollee
	 * @param member
	 */
	// 
	public void updateExistingEnrollee(Enrollment enrollment, List<PldMemberData> pldMemberList){

		if(enrollment == null || pldMemberList == null || pldMemberList.isEmpty()){
			return;
		}

		for(PldMemberData pldMemberData: pldMemberList){
			Map<String, String> member = pldMemberData.getMemberInfo();
			// Find Member from Existing enrollment based on ExchangeIndividualIdentifier/MemberID
			Enrollee enrollee  = getEnrolleeByExchangeIdentifierFromEnrolleeList(enrollment.getEnrollees(), member.get(EnrollmentPrivateConstants.MEMBER_ID));
			if(enrollee == null){
				continue;
			}
			enrollee.setUpdatedOn(new TSDate());
			enrollee.setUpdatedBy(getLoggedInUser());
			if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.DOB))){
				enrollee.setBirthDate(DateUtil.StringToDate(member.get(EnrollmentPrivateConstants.DOB), GhixConstants.REQUIRED_DATE_FORMAT));
			}
			enrollee.setExchgIndivIdentifier(member.get(EnrollmentPrivateConstants.MEMBER_ID));
			enrollee.setSuffix(member.get(EnrollmentPrivateConstants.SUFFIX));
			enrollee.setFirstName(member.get(EnrollmentPrivateConstants.FIRST_NAME));
			enrollee.setLastName(member.get(EnrollmentPrivateConstants.LAST_NAME));
			enrollee.setMiddleName(member.get(EnrollmentPrivateConstants.MIDDLE_NAME));
			enrollee.setPrimaryPhoneNo(member.get(EnrollmentPrivateConstants.PRIMARY_PHONE));
			enrollee.setSecondaryPhoneNo(member.get(EnrollmentPrivateConstants.SECONDARY_PHONE));
			enrollee.setPreferredEmail(member.get(EnrollmentPrivateConstants.PREFERRED_EMAIL));
			enrollee.setPreferredSMS(member.get(EnrollmentPrivateConstants.PREFERRED_PHONE));
			enrollee.setPreferredSMS(member.get(EnrollmentPrivateConstants.PREFERRED_PHONE));

			/*----- These cannot change for enrollee -------*/

			//		enrollee.setTaxIdNumber(member.get(EnrollmentConstants.SSN));
			//		enrollee.setExchgIndivIdentifier(member.get(EnrollmentConstants.MEMBER_ID));

			/*----- These cannot change for enrollee -------*/

			enrollee.setMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.MARITAL_STATUS, member.get(EnrollmentPrivateConstants.MARITAL_STATUS_CODE)));
			enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.GENDER, member.get(EnrollmentPrivateConstants.GENDER_CODE)));
			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.TOBACCO))) {
				if(member.get(EnrollmentPrivateConstants.TOBACCO).toString().equalsIgnoreCase(EnrollmentPrivateConstants.Y)){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, EnrollmentPrivateConstants.T));
				}
				else{
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, member.get(EnrollmentPrivateConstants.TOBACCO)));
				}
			}
			enrollee.setLanguageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LANGUAGE_SPOKEN, member.get(EnrollmentPrivateConstants.SPOKEN_LANGUAGE_CODE)));	
			enrollee.setLanguageWrittenLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LANGUAGE_WRITTEN, member.get(EnrollmentPrivateConstants.WRITTEN_LANGUAGE_CODE)));	
			// HIX-27935
			// HIX-26622
			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.INDIVIDUAL_PREMIUM))) {
				enrollee.setTotalIndvResponsibilityAmt(Float.parseFloat(member.get(EnrollmentPrivateConstants.INDIVIDUAL_PREMIUM)));
			}
			Location mailingAddress = enrollee.getMailingAddressId();
			if(mailingAddress == null){
				mailingAddress = new Location();
				enrollee.setMailingAddressId(mailingAddress);
			}
			mailingAddress.setAddress1(member.get("mailingAddress1"));
			mailingAddress.setAddress2(member.get("mailingAddress2"));
			mailingAddress.setCity(member.get("mailingCity"));
			mailingAddress.setState(member.get("mailingState"));
			if (isNotNullAndEmpty(member.get("mailingZip"))) {
				mailingAddress.setZip(member.get("mailingZip"));
			}


			Location homeAddress = enrollee.getHomeAddressid();
			if(homeAddress == null){
				homeAddress = new Location();
				enrollee.setHomeAddressid(mailingAddress);
			}
			homeAddress.setAddress1(member.get("homeAddress1"));
			homeAddress.setAddress2(member.get("homeAddress2"));
			homeAddress.setCity(member.get("homeCity"));
			homeAddress.setState(member.get("homeState"));
			homeAddress.setCountycode(member.get("countyCode"));
			if (isNotNullAndEmpty(member.get("homeZip"))) {
				homeAddress.setZip(member.get("homeZip"));
			}

			// Setting Custodial Parent
			if (isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.CUSTODIAL_PARENT_ID))) {
				Enrollee custodialParent = enrollee.getCustodialParent();
				if(custodialParent == null){
					custodialParent= new Enrollee();
					enrollee.setCustodialParent(custodialParent);
					custodialParent.setCreatedBy(getLoggedInUser());
					custodialParent.setCreatedOn(new TSDate());
				}
				custodialParent.setExchgIndivIdentifier(member.get(EnrollmentPrivateConstants.CUSTODIAL_PARENT_ID));
				custodialParent.setFirstName(member.get("custodialParentFirstName"));
				custodialParent.setMiddleName(member.get("custodialParentMiddleName"));
				custodialParent.setLastName(member.get("custodialParentLastName"));
				//					custodialParent.setSsn(member.get("custodialParentSsn"));
				custodialParent.setTaxIdNumber(member.get("custodialParentSsn"));
				custodialParent.setSuffix(member.get("custodialParentSuffix"));
				custodialParent.setPreferredEmail(member.get("custodialParentPreferredEmail"));
				custodialParent.setPreferredSMS(member.get("custodialParentPreferredPhone"));
				custodialParent.setPrimaryPhoneNo(member.get("custodialParentPrimaryPhone"));
				custodialParent.setSecondaryPhoneNo(member.get("custodialParentSecondaryPhone"));

				Location custParentAddress = custodialParent.getHomeAddressid();
				if(custParentAddress == null){
					custParentAddress = new Location();
					custodialParent.setHomeAddressid(custParentAddress);
				}
				custParentAddress.setAddress1(member.get("custodialParentHomeAddress1"));
				custParentAddress.setAddress2(member.get("custodialParentHomeAddress2"));
				custParentAddress.setCity(member.get("custodialParentHomeCity"));
				custParentAddress.setState(member.get("custodialParentHomeState"));
				if(isNotNullAndEmpty(member.get("custodialParentHomeZip"))){
					custParentAddress.setZip(member.get("custodialParentHomeZip").toString());	
				}
			}

			List<EnrolleeRace> enrolleeRace = enrollee.getEnrolleeRace();
			if(enrolleeRace == null){
				enrolleeRace = new ArrayList<EnrolleeRace>();
				enrollee.setEnrolleeRace(enrolleeRace);
			}

			// Setting enrolleeEvent
			List<EnrollmentEvent> enrolleeEventList= new ArrayList<EnrollmentEvent>();
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent(); 
			enrollmentEvent.setEnrollee(enrollee);
			enrollmentEvent.setEnrollment(enrollment);
			//add null check
			if(isNotNullAndEmpty(member.get(EnrollmentPrivateConstants.MAINTENANCE_REASON_CODE))){
					if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON,  member.get(EnrollmentPrivateConstants.MAINTENANCE_REASON_CODE)))){
						enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON,  member.get(EnrollmentPrivateConstants.MAINTENANCE_REASON_CODE)));
					}else{
						throw new RuntimeException(EnrollmentConstants.ERR_MSG_LOOKUP_VALUE_IS_NULL);
					}
			}else{
				throw new RuntimeException(EnrollmentConstants.ERR_MSG_REASON_CODE_VALUE);
			}
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE, EnrollmentPrivateConstants.EVENT_TYPE_CHANGE));

			enrollee.setLastEventId(enrollmentEvent);
			enrolleeEventList.add(enrollmentEvent);
			enrollee.setEnrollmentEvents(enrolleeEventList);
		} // End of Pld Member Loop
		
	}
	
	/**
	 * Used to fetch enrollee from enrollment based on ExchgIndivIdentifier
	 * 
	 * Since 24-08-2013
	 * 
	 * @param enrolleeList
	 * @param exchgIndivIdentifier
	 * @return
	 */
	public Enrollee getEnrolleeByExchangeIdentifierFromEnrolleeList(List<Enrollee> enrolleeList, String exchgIndivIdentifier){
		if(enrolleeList != null && exchgIndivIdentifier != null){
			for(Enrollee enrollee : enrolleeList){
				if(enrollee.getExchgIndivIdentifier().equalsIgnoreCase(exchgIndivIdentifier)){
					return enrollee;
				}
			}
		}
		return null;
	}
	
	/**
	 * Used to update existing enrollment in case of special enrollment
	 * @param enrollment
	 * @param pdlPlan
	 * @param responsiblePerson
	 * @param householdContact
	 * @param sadp_flag
	 * @param brokerId
	 * @param brokerType
	 * @param employerId
	 * @param enrollmentReason
	 */
	public void updateexistingEnrollmentForSpecial (Enrollment enrollment, PldPlanData pdlPlan, Map<String, String> responsiblePerson, Map<String, String> householdContact, String sadp_flag, String brokerId, String brokerType, String employerId, String employeeId, Character enrollmentReason){
			Map<String, String> plan = pdlPlan.getPlan();

			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.APTC))) {
				enrollment.setAptcAmt(Float.parseFloat(plan.get(EnrollmentPrivateConstants.APTC)));	
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.GROSS_PREMIUM_AMT))) {
				enrollment.setGrossPremiumAmt(Float.parseFloat(plan.get(EnrollmentPrivateConstants.GROSS_PREMIUM_AMT)));	
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.NET_PREMIUM_AMT))) {
				enrollment.setNetPremiumAmt(Float.parseFloat(plan.get(EnrollmentPrivateConstants.NET_PREMIUM_AMT)));	
			}
			if(sadp_flag.equalsIgnoreCase(EnrollmentPrivateConstants.YES)){
				enrollment.setSadp(EnrollmentPrivateConstants.Y);
			}
			else{
				enrollment.setSadp(EnrollmentPrivateConstants.N);
			}

			/*************************************************** This does not change ***********************************************************************************************************/
			//				enrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE, plan.get(EnrollmentConstants.MARKET_TYPE)));
			//				enrollment.setPlan(planObj);
			//				enrollment.setCMSPlanID(planResponse.getIssuerPlanNumber());
			//				enrollment.setPlanName(planResponse.getPlanName());
			//				enrollment.setInsurerName(planResponse.getIssuerName());
			//				enrollment.setInsurerTaxIdNumber(planResponse.getTaxIdNumber());
			//				enrollment.setIssuer(issuerObj);
			//				enrollment.setCsrAmt(planResponse.getCsrAmount());
			//				enrollment.setExchgSubscriberIdentifier(houseHoldCaseID);
			
			/**************************************************************************************************************************************************************/

			// setting broker information only if Broker Id is different from existing broker
			if (isNotNullAndEmpty(brokerId) && (enrollment.getAssisterBrokerId() == null && !enrollment.getAssisterBrokerId().equals(Integer.valueOf(brokerId)))) {
				if(isNotNullAndEmpty(brokerType) && brokerType.equalsIgnoreCase(EnrollmentPrivateConstants.ASSISTER_ROLE)){
					enrollment.setAssisterBrokerId(Integer.valueOf(brokerId));
					enrollment.setBrokerRole(brokerType);
				}else{
					LOGGER.info("Calling broker service to get broker info for brokerid ");
					//LOGGER.info("Broker service URL == "+BrokerServiceEndPoints.GET_BROKER_DETAIL+brokerId);
					String brokerInfoResponse=null;
					try{
						brokerInfoResponse = restTemplate.getForObject(BrokerServiceEndPoints.GET_BROKER_DETAIL+brokerId, String.class);
					}catch(Exception e){
						LOGGER.error("exception in getting broker data from the ghix-broker", e);
					}

					if(brokerInfoResponse == null){
						LOGGER.error("No Broker data found for broker ID ");
					}
					else{
						EntityResponseDTO brokerResponse = (EntityResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(brokerInfoResponse);
						if(isNotNullAndEmpty(brokerResponse.getResponseCode()) && brokerResponse.getResponseCode() == EnrollmentPrivateConstants.ERROR_CODE_200){
							enrollment.setBrokerTPAFlag(EnrollmentPrivateConstants.Y);
							enrollment.setAssisterBrokerId(Integer.valueOf(brokerId));
							enrollment.setBrokerRole(brokerType);
							if(isNotNullAndEmpty(brokerResponse.getStateLicenseNumber())){
								enrollment.setBrokerTPAAccountNumber1(brokerResponse.getStateLicenseNumber());
							}
							if(isNotNullAndEmpty(brokerResponse.getBrokerName())){
								enrollment.setAgentBrokerName(brokerResponse.getBrokerName());
							}
							if(isNotNullAndEmpty(brokerResponse.getStateEINNumber())){
								enrollment.setBrokerFEDTaxPayerId(brokerResponse.getStateEINNumber());
							}
						}
						else{
							LOGGER.error("Not getting Success  response code data from ghix-broker. : brokerId ");
						}
					}
				}
			}		

			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.PLAN_TYPE))) {
				enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueLabel("INSURANCE_TYPE", plan.get(EnrollmentPrivateConstants.PLAN_TYPE).toString().toLowerCase()));
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.ORDER_ITEM_ID))) {
//				PldOrderItem pldOrderItem = new PldOrderItem();
//				pldOrderItem.setId(Integer.parseInt(plan.get(EnrollmentPrivateConstants.ORDER_ITEM_ID)));
				enrollment.setPdOrderItemId(Long.valueOf(plan.get(EnrollmentConstants.ORDER_ITEM_ID).trim()));
			}
			String coverageStartDate = plan.get(EnrollmentPrivateConstants.COVERAGE_START_DATE);
			Date effectiveDate = null;
			if (isNotNullAndEmpty(coverageStartDate)) {
				String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd hh:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
				effectiveDate =  DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT); 
				enrollment.setBenefitEffectiveDate(effectiveDate);	
			}
			
			Enrollee responsiblePersonEnrollee = null;
			Map<String, String> responsiblePersonMap = responsiblePerson;
			if (responsiblePersonMap != null && isNotNullAndEmpty(responsiblePersonMap.get(EnrollmentPrivateConstants.RESPONSIBLE_PERSON_ID))) {
				responsiblePersonEnrollee = enrollment.getResponsiblePerson();
				if(responsiblePersonEnrollee == null){
					responsiblePersonEnrollee = new Enrollee();
					enrollment.setResponsiblePerson(responsiblePersonEnrollee);
					responsiblePersonEnrollee.setCreatedBy(getLoggedInUser());
					responsiblePersonEnrollee.setUpdatedBy(getLoggedInUser());
				}
				responsiblePersonEnrollee.setExchgIndivIdentifier(responsiblePersonMap.get(EnrollmentPrivateConstants.RESPONSIBLE_PERSON_ID));
				responsiblePersonEnrollee.setFirstName(responsiblePersonMap.get("responsiblePersonFirstName"));
				responsiblePersonEnrollee.setMiddleName(responsiblePersonMap.get("responsiblePersonMiddleName"));
				responsiblePersonEnrollee.setLastName(responsiblePersonMap.get("responsiblePersonLastName"));
				responsiblePersonEnrollee.setTaxIdNumber(responsiblePersonMap.get("responsiblePersonSsn"));
				responsiblePersonEnrollee.setSuffix(responsiblePersonMap.get("responsiblePersonSuffix"));
				responsiblePersonEnrollee.setPreferredEmail(responsiblePersonMap.get("responsiblePersonPreferredEmail"));
				responsiblePersonEnrollee.setPreferredSMS(responsiblePersonMap.get("responsiblePersonPreferredPhone"));
				responsiblePersonEnrollee.setPrimaryPhoneNo(responsiblePersonMap.get("responsiblePersonPrimaryPhone"));
				responsiblePersonEnrollee.setSecondaryPhoneNo(responsiblePersonMap.get("responsiblePersonSecondaryPhone"));
				responsiblePersonEnrollee.setRespPersonIdCode(EnrollmentPrivateConstants.RESPONSIBLE_PERSON_ID_CODE_QD);
				
				Location responsiblePersonAddress = responsiblePersonEnrollee.getHomeAddressid();
				if(responsiblePersonAddress == null){
					responsiblePersonAddress = new Location();
					responsiblePersonEnrollee.setHomeAddressid(responsiblePersonAddress);
				}
				responsiblePersonAddress.setAddress1(responsiblePersonMap.get("responsiblePersonHomeAddress1"));
				responsiblePersonAddress.setAddress2(responsiblePersonMap.get("responsiblePersonHomeAddress2"));
				responsiblePersonAddress.setCity(responsiblePersonMap.get("responsiblePersonHomeCity"));
				responsiblePersonAddress.setState(responsiblePersonMap.get("responsiblePersonHomeState"));
				responsiblePersonAddress.setZip(responsiblePersonMap.get("responsiblePersonHomeZip"));	
			}

			Enrollee houseHoldContactEnrollee = null;
			Map<String, String> householdContactMap = householdContact;
			if (isNotNullAndEmpty(householdContactMap) && isNotNullAndEmpty(householdContactMap.get(EnrollmentPrivateConstants.HOUSEHOLD_CONTACT_ID))) {
				houseHoldContactEnrollee = enrollment.getHouseholdContact();
				if(houseHoldContactEnrollee == null){
					houseHoldContactEnrollee = new Enrollee();
					enrollment.setHouseholdContact(houseHoldContactEnrollee);
				}
				
				houseHoldContactEnrollee.setExchgIndivIdentifier(householdContactMap.get(EnrollmentPrivateConstants.HOUSEHOLD_CONTACT_ID));
				houseHoldContactEnrollee.setFirstName(householdContactMap.get("houseHoldContactFirstName"));
				houseHoldContactEnrollee.setMiddleName(householdContactMap.get("houseHoldContactMiddleName"));
				houseHoldContactEnrollee.setLastName(householdContactMap.get("houseHoldContactLastName"));
				houseHoldContactEnrollee.setSuffix(householdContactMap.get("houseHoldContactSuffix"));
				houseHoldContactEnrollee.setPreferredEmail(householdContactMap.get("houseHoldContactPreferredEmail"));
				houseHoldContactEnrollee.setPreferredSMS(householdContactMap.get("houseHoldContactPreferredPhone"));
				houseHoldContactEnrollee.setPrimaryPhoneNo(householdContactMap.get("houseHoldContactPrimaryPhone"));
				houseHoldContactEnrollee.setSecondaryPhoneNo(householdContactMap.get("houseHoldContactSecondaryPhone"));
				houseHoldContactEnrollee.setTaxIdNumber(householdContactMap.get("houseHoldContactFederalTaxIdNumber"));

				Location houseHoldContactAddress = houseHoldContactEnrollee.getHomeAddressid();
				if(houseHoldContactAddress == null){
					houseHoldContactAddress = new Location();
					houseHoldContactEnrollee.setHomeAddressid(houseHoldContactAddress);
					houseHoldContactEnrollee.setCreatedBy(getLoggedInUser());
					houseHoldContactEnrollee.setUpdatedBy(getLoggedInUser());
				}
				houseHoldContactAddress.setAddress1(householdContactMap.get("houseHoldContactHomeAddress1"));
				houseHoldContactAddress.setAddress2(householdContactMap.get("houseHoldContactHomeAddress2"));
				houseHoldContactAddress.setCity(householdContactMap.get("houseHoldContactHomeCity"));
				houseHoldContactAddress.setState(householdContactMap.get("houseHoldContactHomeState"));
				if(isNotNullAndEmpty(householdContactMap.get("houseHoldContactHomeZip"))){
					houseHoldContactAddress.setZip(householdContactMap.get("houseHoldContactHomeZip"));	
				}
			}
			
			//Find subscriber
			Enrollee subscriber = null;
			List<Enrollee> enrolleeList = enrollment.getEnrollees();
			if(enrolleeList != null){
				for (Enrollee enrollee:enrolleeList){
					if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().equals(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER))){
						//Found the subscriber
						subscriber = enrollee;
						break;
					}
				}
			}

			if(subscriber != null){
				int subscriberAge = getAge(subscriber.getBirthDate());
				if(subscriberAge < EnrollmentPrivateConstants.AGE_18 && houseHoldContactEnrollee != null)
				{
					subscriber = houseHoldContactEnrollee;
				}
				StringBuilder subscriberName = new StringBuilder();

				if(isNotNullAndEmpty(subscriber.getFirstName())){
					subscriberName.append(subscriber.getFirstName());
					subscriberName.append(" ");
				}
				if(isNotNullAndEmpty(subscriber.getMiddleName())){
					subscriberName.append(subscriber.getMiddleName());
					subscriberName.append(" ");	
				}
				if(isNotNullAndEmpty(subscriber.getLastName())){
					subscriberName.append(subscriber.getLastName());
					subscriberName.append(" ");
				}
				if(isNotNullAndEmpty(subscriber.getSuffix())){
					subscriberName.append(subscriber.getSuffix());	
				}

				enrollment.setSubscriberName(subscriberName.toString().trim());
				
				if(isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.MARKET_TYPE))){
					if (plan.get(EnrollmentPrivateConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)) {
						enrollment.setSponsorEIN(subscriber.getTaxIdNumber());
					}
					else if (plan.get(EnrollmentPrivateConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
						enrollment.setSponsorTaxIdNumber(subscriber.getTaxIdNumber());
					}
				}
				
				enrollment.setSponsorName(subscriberName.toString().trim());
			}
			else {
				LOGGER.info("Could not find Subscriber for the Enrollment ID:" + enrollment.getId());
			}
			
			// For getting employers/employee details
			if(enrollment.getEnrollmentTypeLkp()!=null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
				if (isNotNullAndEmpty(employerId) && (enrollment.getEmployer() == null || enrollment.getEmployer().equals(Integer.valueOf(employerId)))) {
					Employer employer = new Employer();
					employer.setId(Integer.parseInt(employerId));
					enrollment.setEmployer(employer);
					
					LOGGER.info("Calling shop service to get employer info for employerId ");
					//LOGGER.info("shop service URL == "+ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId);
				//	String shopInfoResponse=null;
					EmployerDTO employerDTO = null;

					String postResponse =null;
					try{
						postResponse = restTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId, String.class);

					}catch(RestClientException re){
						LOGGER.error("Message : "+re.getMessage());
						LOGGER.error("Cause : "+re.getCause());
						LOGGER.error(" " , re);
					}

					XStream xstream = GhixUtils.getXStreamStaxObject();
					ShopResponse shopResponse= new ShopResponse();
					shopResponse = (ShopResponse) xstream.fromXML(postResponse);
					if(shopResponse != null){
						
						employerDTO = (EmployerDTO) shopResponse.getResponseData().get("Employer");
												
						if(employerDTO != null){
							
							if(employerDTO.getName() != null){			            	
								enrollment.setSponsorName(employerDTO.getName());
							}
							if(employerDTO.getExternalId() != null){
								enrollment.setEmployerCaseId(employerDTO.getExternalId());
							}
							
							
							if(employerDTO.getFederalEIN() != null){	
								enrollment.setSponsorEIN(employerDTO.getFederalEIN().toString());
							}
							
							if(employerDTO.getExternalId()!= null){	
								enrollment.setExternalEmployerId(employerDTO.getExternalId());
							}
						}
						
						// setting the enrollment status to PAYMENT_RECEIVED when the employer enrollment status is ACTIVE & enrollment reason is 'N' 
						EmployerEnrollment.Status employerEnrollmentStatus = (EmployerEnrollment.Status) shopResponse.getResponseData().get(EnrollmentPrivateConstants.ENROLLMENT_STATUS_KEY1); 
						if((employerEnrollmentStatus!=null && enrollmentReason!=null) && (employerEnrollmentStatus.equals(Status.ACTIVE) && enrollmentReason.toString().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_NEW))){
							enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_PAYMENT_RECEIVED));
						}
					}
				}
				else{
					LOGGER.info("No Employer Found");
				}

				if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.EMPLOYEE_CONTRIBUTION))) {
					enrollment.setEmployerContribution(Float.parseFloat(plan.get(EnrollmentPrivateConstants.EMPLOYEE_CONTRIBUTION)));
				}

				if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.DEPENDENT_CONTRIBUTION))) {
					enrollment.setEmployerContribution(enrollment.getEmployerContribution() + Float.parseFloat(plan.get(EnrollmentPrivateConstants.DEPENDENT_CONTRIBUTION)));
				}

				if (isNotNullAndEmpty(plan.get(EnrollmentPrivateConstants.NET_PREMIUM_AMT))) {
					enrollment.setEmployeeContribution(Float.parseFloat(plan.get(EnrollmentPrivateConstants.NET_PREMIUM_AMT)));	
				}
			}
			
			enrollment.setUpdatedBy(getLoggedInUser());
			enrollment.setUpdatedOn(new TSDate());
			setEnrollmentSubmittedBy(enrollment);
			enrollmentRepository.saveAndFlush(enrollment);
	}
	
	
	@Override
	public List<Enrollment> findEnrollmentByCmrHouseholdId(int cmrHouseholdId){
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_MANUAL);
		return enrollmentRepository.findEnrollmentByCmrHouseholdId(cmrHouseholdId,lookupValue.getLookupValueId());
	}
	
	@Override
	public List<Enrollment> findEnrollmentBySsapApplicationId(long ssapApplicationId){
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_MANUAL);
		return enrollmentRepository.findEnrollmentBySsapApplicationId(ssapApplicationId,lookupValue.getLookupValueId());
	}
	
	@Override
	public List<Object[]> getRemittanceDataByEnrollmentId(Integer enrollmentId)
	{
		return enrollmentRepository.getRemittanceDataByEnrollmentId(enrollmentId);
	}
	
	@Override
	public EnrollmentPlan findEnrollmentPlanByPlanId(Integer planId){
		return enrollmentPlanRepository.findEnrollmentPlanByPlanID(planId);
	}
	
	@Override
	public BenefitBayRequestDTO populateBenefitBayRequestDTO(EnrollmentResponse enrollmentResponse){
		Enrollment enrollmentObj =  enrollmentResponse.getEnrollmentList().get(0);
		BenefitBayRequestDTO benefitBayRequestDTO = new BenefitBayRequestDTO();
		EnrollmentPlan enrollmentPlan = null;
		
		benefitBayRequestDTO.setEnrollmentID(""+enrollmentObj.getId());
		benefitBayRequestDTO.setHouseholdID(enrollmentObj.getHouseHoldCaseId());
		if(isNotNullAndEmpty(enrollmentObj.getHiosIssuerId())){
			benefitBayRequestDTO.setCarrierID(enrollmentObj.getHiosIssuerId());
		}else{
			LOGGER.debug("HIOS ISSUER ID not found");
			LOGGER.error("HIOS ISSUER ID not found ");
		}
		benefitBayRequestDTO.setCarrierName(enrollmentObj.getInsurerName());
		benefitBayRequestDTO.setPlanID(enrollmentObj.getCMSPlanID());
		benefitBayRequestDTO.setPlanName(enrollmentObj.getPlanName());
		if(isNotNullAndEmpty(enrollmentObj.getGrossPremiumAmt())){
			benefitBayRequestDTO.setGrossMonthlyPremium(new BigDecimal(String.valueOf(enrollmentObj.getGrossPremiumAmt())));
		}
		
		benefitBayRequestDTO.setPlanType(enrollmentObj.getInsuranceTypeLkp().getLookupValueCode());
		if(isNotNullAndEmpty(enrollmentObj.getPlanId())){
			enrollmentPlan = enrollmentPlanRepository.findEnrollmentPlanByPlanID(enrollmentObj.getPlanId());
		}
		if(enrollmentPlan != null){
			
			benefitBayRequestDTO.setDoctorVisitCost(enrollmentPlan.getOfficeVisit());
			benefitBayRequestDTO.setGenericPrescriptionCost(enrollmentPlan.getGenericMedication());
			benefitBayRequestDTO.setPlanTier(enrollmentPlan.getNetworkType());
			
			if(isNotNullAndEmpty(enrollmentPlan.getFamilyDeductible())){
				benefitBayRequestDTO.setFamilyDeductible(BigInteger.valueOf(enrollmentPlan.getFamilyDeductible().longValue()));
			}
			if(isNotNullAndEmpty(enrollmentPlan.getFamilyOopMax())){
				benefitBayRequestDTO.setFamilyMaximumOutOfPocket(BigInteger.valueOf(enrollmentPlan.getFamilyOopMax().longValue()));
			}
			if(isNotNullAndEmpty(enrollmentPlan.getIndivDeductible())){
				benefitBayRequestDTO.setIndividualDeductible(BigInteger.valueOf(enrollmentPlan.getIndivDeductible().longValue()));
			}
			if(isNotNullAndEmpty(enrollmentPlan.getIndivOopMax())){
				benefitBayRequestDTO.setIndividualMaximumOutOfPocket(BigInteger.valueOf(enrollmentPlan.getIndivOopMax().longValue()));
			}
		}
		
	return benefitBayRequestDTO;
	}
	
	@Override
	public Enrollment findManualEnrollmentByCmrHouseholdId(Integer cmrHouseholdId)
	{
		return enrollmentRepository.findManualEnrollmentByCmrHouseholdId(cmrHouseholdId);
		
	}
	
	@Override
	public Enrollment findLatestPendingEnrollmentByCmrHouseholdId(Integer cmrHouseholdId){
		return enrollmentRepository.findPendingEnrollmentByCmrHouseholdId(cmrHouseholdId);
	}
	
	@Override
	public Enrollment findLatestNonEcommittedEnrollmentByCmrHouseholdId(Integer cmrHouseholdId){
		return enrollmentRepository.findNonEcommittedEnrollmentByCmrHouseholdId(cmrHouseholdId);
	}
	
	
	@Override
	public Enrollment findEnrollmentByCarrierAppId(String carrierAppId) {
		return enrollmentRepository.findByCarrierAppId(carrierAppId);
	}
	
	@Override
	public List<Object[]> findEnrollmentAndEnrolleeDataByName(
			String firstName, String lastName) {
		return enrollmentRepository.findEnrollmentAndEnrolleeDataByName(firstName.trim(), lastName.trim());
	}

	@Override
	public List<Object[]> findByFirstNameAndLastNameLowercase(
			String firstName, String lastName) {
		return enrollmentRepository.findByFirstNameAndLastNameLowercase(firstName, lastName);
	}
	
	@Override
	public Enrollment updateEnrollmentStatusById(CarrierFeedResultRecord result){
		
		Enrollment enrollment = enrollmentRepository.findById(result.getEnrollmentId());
		LookupValue newEnrollmentStatusLkp = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS , result.getNewEnrollmentStatusStr());
		enrollment.setEnrollmentStatusLkp(newEnrollmentStatusLkp);
		enrollment.setCarrierAppId(result.getApplicationId());
		enrollment.setCarrierLastUpdatedDate(result.getLastStatusDt());
		if(result.getCarrierStatus() != null){
			enrollment.setCarrierStatus(result.getCarrierStatus());
		}
		if(result.getFollowupMessage() != null){
			enrollment.setFollowUpMessage(result.getFollowupMessage());
		}
		if(result.getDateClosed() != null){
			enrollment.setDateClosed(result.getDateClosed());
		}
		if(result.getRateUp() != null){
			enrollment.setRateUp(result.getRateUp());
		}
		setEnrollmentSubmittedBy(enrollment);
		return enrollmentRepository.saveAndFlush(enrollment);
	}
	
	private boolean isPHIXBBUser(){
		boolean isBBUser=false;
		try{
			if(BENEFIT_BAY_USER!=null){
				AccountUser actUser=userService.getLoggedInUser();
				if(actUser!=null && actUser.getUsername()!=null &&actUser.getUsername().equalsIgnoreCase(BENEFIT_BAY_USER) ){
					isBBUser=true;
				}
			}
		}catch(Exception e){
			LOGGER.error("Exception while trying to get logged in user in isBBUser()",e);
		}
		return isBBUser;
	}
	
	
	@Override
	public Enrollment updateEnrollment(Integer enrollmentId, String enrlStatus, String strTerminationDate, String strDateClosed) {
		
		LOGGER.info("updateEnrollment :: ENROLLMENT ID is -->"+enrollmentId +" STATUS -->"+enrlStatus);  
		
		Enrollment enrollmentObj = findById(enrollmentId);
		if(isNotNullAndEmpty(enrollmentObj)){
			if(null!=enrollmentObj.getEnrollmentStatusLkp() && enrlStatus.equals(enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode()) 
					&& StringUtils.isEmpty(strTerminationDate) && StringUtils.isEmpty(strDateClosed)){
				return enrollmentObj;
			}
			//HIX-60327 : Set the termination date
			Date benefitEndDate=null;
			if(!StringUtils.isEmpty(strTerminationDate)) {			
				try {
					benefitEndDate = new SimpleDateFormat("MM/dd/yyyy").parse(strTerminationDate);
					enrollmentObj.setBenefitEndDate(benefitEndDate);
				} catch (ParseException e) {
					LOGGER.error("Unable to parse the benefit end date: ", e);
				}	
			}			
			
			//END: HIX-60327
			
			Date dateClosed=null;
			if(!StringUtils.isEmpty(strDateClosed)) {			
				try {
					dateClosed = new SimpleDateFormat("MM/dd/yyyy").parse(strDateClosed);
					enrollmentObj.setDateClosed(dateClosed);
				} catch (ParseException e) {
					LOGGER.error("Unable to parse the Date Closed : ", e);
				}	
			}	
			
			String appEnrollmentEventLkpValueCode = null;
			String prevStatus = enrollmentObj.getEnrollmentStatusLkp().getLookupValueLabel();
					
			enrollmentObj.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, enrlStatus));
			
			if(!enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM)){
				enrollmentObj.setDateClosed(null);
			}
			
			List<Enrollee> enrolleeList = enrollmentObj.getEnrollees();
			for(Enrollee enrolleeObj : enrolleeList){
				if(benefitEndDate != null) {	
					enrolleeObj.setEffectiveEndDate(benefitEndDate);
				}
				LookupValue lkpValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, enrlStatus);
				if(lkpValue != null){
					enrolleeObj.setEnrolleeLkpValue(lkpValue);
					if(lkpValue.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || lkpValue.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
						enrolleeObj.setDisenrollTimestamp(new TSDate());
					}
				}
				
				createEventForEnrolleeAndEnrollment(enrollmentObj, enrolleeObj, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION, enrollmentUserService.getLoggedInUser());
			}
			enrollmentObj.setUpdatedBy(getLoggedInUser());
			enrollmentObj = ienrollmentRepository.saveAndFlush(enrollmentObj);
			
			if(enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM)){
				appEnrollmentEventLkpValueCode = EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_TERMINATED.toString();
			}
			else if(enrollmentObj.getEnrollmentStatusLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_INFORCE)){
				appEnrollmentEventLkpValueCode = EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_INFORCE.toString();
			}
			else{
				appEnrollmentEventLkpValueCode = EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_STATUS_CHANGED.toString();
			}
			
			enrollmentPrivateUtils.recordApplicationEnrollmentPrivateEvent(enrollmentObj, appEnrollmentEventLkpValueCode, prevStatus);
			//enrollmentEventPrivateService.saveConsumerErnrollmentEvent(enrollmentObj , appEnrollmentEventLkpValueCode ,prevStatus);
		}			
		return enrollmentObj;
	}
	

    @Override
    public List<Enrollment> findEnrollmentById(List<Integer> ids) {
        return enrollmentRepository.findEnrollmentById(ids);
    }

    @Override
    public List<Enrollment> findEnrollmentIdByEnrollmentStatusLkp_lookupValueCodeAndCreatedOnBefore(List<String> lookupValueCodes, Date createdOn) {
        return enrollmentRepository.findEnrollmentIdByEnrollmentStatusLkp_lookupValueCodeAndCreatedOnBefore(lookupValueCodes, createdOn);
    }
    
    @Override
    public List<PolicyLiteDTO> getEnrollmentListByHouseholdId(int householdId){
    	return ienrollmentRepository.findEnrollmentListByCmrHouseholdId(householdId);
    }

    @Override
    public boolean saveManualAppEnrolleeInfo(List<SsapApplicantDTO> applicantDtoList, Enrollment savedEnrollment) {
    	return saveManualAppEnrolleeInfo(applicantDtoList, savedEnrollment, true);
    }
    
    /**
	 * Fixes for HIX-57436 
	 * Set the status of the enrollment as 'Pending' only if the flow comes from manual application or application submission.
	 * This method is also called when enrollees are updated.
	 */
    @Override
	public boolean saveManualAppEnrolleeInfo(List<SsapApplicantDTO> applicantDtoList, Enrollment savedEnrollment, boolean pendingStatusOnly) {
    	boolean bStatus = false;
    	List<Enrollee> enrollees = new ArrayList<>();
		
		if (null != applicantDtoList && !applicantDtoList.isEmpty()) {
		try {
				for (SsapApplicantDTO dto : applicantDtoList) {
					Enrollee enrollee = null;
					if (dto.getId() > 0) {
					enrollee = enrolleeRepository.findById((int) dto.getId());
						if (enrollee == null) {
							throw new GIRuntimeException("Could not find existing enrolleeById:"+ dto.getId());
					}
					} else {
						enrollee = new Enrollee();
					}
					createManualEnrolleeAndEvent(dto, savedEnrollment, enrollee);
					
					// HIX-62266 Setting subscriber and sponsor name in the enrollment
					if(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())){
						StringBuilder subscriberName = new StringBuilder();
						subscriberName.append(enrollee.getFirstName());
						subscriberName.append(" ");
						if (null != enrollee.getMiddleName()) {
							subscriberName.append(enrollee.getMiddleName());
							subscriberName.append(" ");
						}
						subscriberName.append(enrollee.getLastName());
						savedEnrollment.setSponsorName(subscriberName.toString());
						savedEnrollment.setSubscriberName(subscriberName.toString());
						
						//HIX-62305 Set State Exchange Code
						if(null != enrollee.getHomeAddressid()){
							savedEnrollment.setStateExchangeCode(enrollee.getHomeAddressid().getState());
						}
					}
					
					enrollee.setEnrollment(savedEnrollment);
					enrollees.add(enrollee);
				}

				if (pendingStatusOnly) {
					savedEnrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
											EnrollmentPrivateConstants.ENROLLMENT_STATUS,
											EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
				}

				EnrollmentUtilsForCap.setCapAgentId(savedEnrollment,userService);
				savedEnrollment.setEnrollees(enrollees);
				setEnrollmentSubmittedBy(savedEnrollment);
				savedEnrollment = enrollmentRepository.save(savedEnrollment);
				bStatus = true;
			} catch (Exception e) {
				LOGGER.error("Unable to save the enrollees for the record" , e);
			}
		}

		return bStatus;
	}

	private Enrollee createManualEnrolleeAndEvent(SsapApplicantDTO enrolleeDTO, Enrollment enrollment, Enrollee enrollee){
		LOGGER.info("EnrollmentPrivateServiceImpl createManualEnrolleeAndEvent START -->");
		enrollee.setCreatedBy(getLoggedInUser());
		enrollee.setUpdatedBy(getLoggedInUser());
		
		enrollee.setLastName(enrolleeDTO.getLastName());
		enrollee.setFirstName(enrolleeDTO.getFirstName());
		enrollee.setMiddleName(enrolleeDTO.getMiddleName());
		enrollee.setPreferredEmail(enrolleeDTO.getPrefEmail());
		enrollee.setPrimaryPhoneNo(enrolleeDTO.getPrimaryPhoneNo());
		enrollee.setTaxIdNumber(enrolleeDTO.getSsn());
		if(null!=enrolleeDTO.getHomeAddress() ){
			Location location=null;
			if(null!=enrollee.getHomeAddressid()){
				location   =  enrollee.getHomeAddressid();
			}
			else{
				location = new Location();
			}

			location.setAddress1(enrolleeDTO.getHomeAddress().getAddress1());
			location.setAddress2(enrolleeDTO.getHomeAddress().getAddress2());
			location.setCity(enrolleeDTO.getHomeAddress().getCity());
			location.setCounty(enrolleeDTO.getHomeAddress().getCounty());
			if(location.getId()>0){
				location.setId(location.getId());	 
			}
			location.setState(enrolleeDTO.getHomeAddress().getState());
			location.setZip(enrolleeDTO.getHomeAddress().getZip());

			enrollee.setHomeAddressid(location);
		}
		
		if(isNotNullAndEmpty(enrolleeDTO.getBirthDate())){
			enrollee.setBirthDate(enrolleeDTO.getBirthDate());
		}else{
			LOGGER.info("incomming Enrollee DOB is : " + enrolleeDTO.getBirthDate());
		}
		
		/*if(isNotNullAndEmpty(enrolleeDTO.getBirthDateString())){
			enrollee.setBirthDate(DateUtil.StringToDate(enrolleeDTO.getBirthDateString(), GhixConstants.REQUIRED_DATE_FORMAT));			
		}else{
			LOGGER.info("incomming Enrollee DOB is : " + enrolleeDTO.getBirthDateString());
		}*/
		
		if(isNotNullAndEmpty(enrolleeDTO.getGender())){
			enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentPrivateConstants.GENDER, enrolleeDTO.getGender().toLowerCase()));
		}
		if(isNotNullAndEmpty(enrolleeDTO.getTobaccoUser())){
			if(enrolleeDTO.getTobaccoUser().equalsIgnoreCase("Yes")){
			 enrollee.setTobaccoUsageLkp((lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentPrivateConstants.TOBACCO_USAGE, "Tobacco Use".toLowerCase())));
			}
			else{
				enrollee.setTobaccoUsageLkp((lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentPrivateConstants.TOBACCO_USAGE, "No Tobacco Use".toLowerCase())));
			}
		}
		else{
			enrollee.setTobaccoUsageLkp((lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentPrivateConstants.TOBACCO_USAGE, "Unknown Tobacco Use".toLowerCase())));
		}
		
		enrollee.setEffectiveStartDate( enrollment.getBenefitEffectiveDate());
		enrollee.setEffectiveEndDate(enrollment.getBenefitEndDate());
		
		if (isNotNullAndEmpty(enrolleeDTO.getRelationship())) {
			enrollee.setRelationshipToHCPLkp(lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentPrivateConstants.RELATIONSHIP,enrolleeDTO.getRelationship().toLowerCase()));
		}
		
		if (isNotNullAndEmpty(enrolleeDTO.getPersonType())) {
			enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,enrolleeDTO.getPersonType().toLowerCase()));
		}
		
		enrollee.setEnrollmentReason(new Character('I'));
		
		if(isNotNullAndEmpty(enrolleeDTO.getExchgIndivIdentifier())){
			enrollee.setExchgIndivIdentifier(enrolleeDTO.getExchgIndivIdentifier());
		}
		
		if(isNotNullAndEmpty(enrolleeDTO.getPrimaryPhoneNo())){
			enrollee.setPrimaryPhoneNo(enrolleeDTO.getPrimaryPhoneNo());
		}
		
		if(isNotNullAndEmpty(enrolleeDTO.getPrefEmail())){
			enrollee.setPreferredEmail(enrolleeDTO.getPrefEmail());			
		}

		enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
		
		if(NumberUtils.isNumber(enrolleeDTO.getHeight())){
			enrollee.setHeight(Float.valueOf(enrolleeDTO.getHeight()));
		}
		if(NumberUtils.isNumber(enrolleeDTO.getWeight())){
			enrollee.setWeight(Float.valueOf(enrolleeDTO.getWeight()));
		}

		if (enrollee.getId() <= 0) {
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
			enrollmentEvent.setCreatedBy(getLoggedInUser());
			enrollmentEvent.setEnrollee(enrollee);
			enrollmentEvent.setEnrollment(enrollment);
			enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION));
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE,EnrollmentPrivateConstants.EVENT_TYPE_ADDITION));
			if(null!=enrollment.getEnrollmentEvents()){
				enrollment.getEnrollmentEvents().add(enrollmentEvent);
			}
			else{
				List<EnrollmentEvent> enrollmentEvents = new ArrayList<EnrollmentEvent>();
				enrollmentEvents.add(enrollmentEvent);
				enrollment.setEnrollmentEvents(enrollmentEvents);
			}
		}
		LOGGER.info("EnrollmentPrivateServiceImpl createManualEnrolleeAndEvent END ***");
		return enrollee;

		//return enrolleeRepository.save(enrollee);
	}

    @Override
    public int findCountByCmrIdAndPlanTypeForManual(String cmrId, String appType) {
    	int countOfEnrollment=0;
    	if(org.apache.commons.lang.StringUtils.isNotEmpty(cmrId)){
    		Long count =  ienrollmentRepository.findCountByCmrIdAndPlanTypeForManual(Integer.valueOf(cmrId), appType);
    		if(null!=count) {
				countOfEnrollment = count.intValue();
			}    	
    }
    	return countOfEnrollment;
    }

    /**
	 * Get the enrollment record by the ssap-id
	 */
    @Override
	public CapApplicationDTO findBySsapApplicationId(long ssapApplicationId) {
		CapApplicationDTO capAppDto = new CapApplicationDTO();
		Enrollment enrollment = ienrollmentRepository.findBySsapAppIdNotAborted(ssapApplicationId);
		
		// Cap application DTO
		if (null != enrollment) {
			SingleIssuerResponse issuerResponse  = enrollmentExternalRestUtil.getIssuerInfoById(enrollment.getIssuerId());
			CapEnrollmentMapper mapper = new CapEnrollmentMapper();
			EnrollmentDTO enrollmentDto = mapper.getDtoFromObject(enrollment);
			enrollmentDto.setIssuerPassword(issuerResponse.getProducerPassword());
			enrollmentDto.setIssuerUserName(issuerResponse.getProducerUserName());
			enrollmentDto.setCarrierAppUrl(issuerResponse.getApplicationUrl());
			capAppDto.setEnrollmentDTO(enrollmentDto);

			// list of applicants
			List<SsapApplicantDTO> applicants = new ArrayList<>();
			if (null != enrollment) {
				List<Enrollee> enrollees = enrollment.getEnrollees();
				for (Enrollee enrollee : enrollees) {
					SsapApplicantDTO appl = getSsapApplicantDto(enrollee);
					applicants.add(appl);
    }
//				plan = enrollment.getPlan();
			}
			capAppDto.setApplicantDTOList(applicants);
    
			// get the plan data filled

			PlanInfoDTO planInfoDTO = getPlanInfoDto(enrollment, issuerResponse);
			// set the metal tier from enrollment plan level
			planInfoDTO.setMetalTier(enrollment.getPlanLevel());
			capAppDto.setPlanInfoDTO(planInfoDTO);
		}
		return capAppDto;
	}
    /**
     * @author panda_p
     * @since 22-11-2016
     * 
     * @param id - Enrollment id
     * @return Enrollment DTO object
     */
    @Override
	public com.getinsured.hix.model.enrollment.EnrollmentDTO findbyenrollmentid(Integer id) throws Exception{
    	com.getinsured.hix.model.enrollment.EnrollmentDTO objEnrollmentDTO = null;
    	Enrollment enrollment = ienrollmentRepository.findById(id);
    	if(enrollment!=null){
    		objEnrollmentDTO = new com.getinsured.hix.model.enrollment.EnrollmentDTO();
    		
    		objEnrollmentDTO.setCmrHouseHoldId(enrollment.getCmrHouseHoldId());
        	objEnrollmentDTO.setCapAgentId(enrollment.getCapAgentId());
        	objEnrollmentDTO.setId(enrollment.getId());
        	if(enrollment.getInsuranceTypeLkp()!=null){
            	objEnrollmentDTO.setInsuranceTypeLabel(enrollment.getInsuranceTypeLkp().getLookupValueLabel());
        	}
        	objEnrollmentDTO.setPlanName(enrollment.getPlanName());
        	objEnrollmentDTO.setInsurerName(enrollment.getInsurerName());
        	objEnrollmentDTO.setIssuerId(enrollment.getIssuerId());
        	
        	objEnrollmentDTO.setPlanLevel(enrollment.getPlanLevel());
        	if(enrollment.getExchangeTypeLkp()!=null){
        		objEnrollmentDTO.setExchangeTypeLabel(enrollment.getExchangeTypeLkp().getLookupValueLabel());
        	}
        	objEnrollmentDTO.setGrossPremiumAmt(enrollment.getGrossPremiumAmt());
        	objEnrollmentDTO.setAptcAmt(enrollment.getAptcAmt());
        	objEnrollmentDTO.setBenefitEffectiveDate(enrollment.getBenefitEffectiveDate());
        	objEnrollmentDTO.setBenefitEndDate(enrollment.getBenefitEndDate());
        	if(isNotNullAndEmpty(enrollment.getBenefitEffectiveDate())){
        		objEnrollmentDTO.setEnrollmentStartDate(DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT));
        	}
        	
        	if(isNotNullAndEmpty(enrollment.getBenefitEndDate())){
        		objEnrollmentDTO.setEnrollmentEndDate(DateUtil.dateToString(enrollment.getBenefitEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
        	}
        	
        	objEnrollmentDTO.setSubmittedToCarrierDate(enrollment.getSubmittedToCarrierDate());
        	objEnrollmentDTO.setCarrierAppId(enrollment.getCarrierAppId());
        	objEnrollmentDTO.setIssuerAssignPolicyNo(enrollment.getIssuerAssignPolicyNo());
        	objEnrollmentDTO.setExchangeAssignPolicyNo(enrollment.getExchangeAssignPolicyNo());
        	objEnrollmentDTO.setCapAgentId(enrollment.getCapAgentId());
        	objEnrollmentDTO.setConfirmationNumber(enrollment.getConfirmationNumber());
        	
        	objEnrollmentDTO.setUpdatedOn(enrollment.getUpdatedOn());
        	if(enrollment.getSubmittedBy()!=null){
            	objEnrollmentDTO.setFullName(enrollment.getSubmittedBy().getFullName());
        	}
        	objEnrollmentDTO.setVerificationEvent(enrollment.getVerificationEvent());
        	if(enrollment.getEnrollmentStatusLkp()!=null){
            	objEnrollmentDTO.setEnrlStatusLabel(enrollment.getEnrollmentStatusLkp().getLookupValueLabel());
        	}
        	objEnrollmentDTO.setDateClosed(enrollment.getDateClosed());
        	objEnrollmentDTO.setDuplicateEnrollmentId(enrollment.getDuplicateEnrollmentId());
        	objEnrollmentDTO.setRenewalFlag(enrollment.getRenewalFlag());
        	objEnrollmentDTO.setSsapApplicationid(enrollment.getSsapApplicationid());
        	if(enrollment.getEnrollmentModalityLkp()!=null){
        		objEnrollmentDTO.setEnrlModalityLabel(enrollment.getEnrollmentModalityLkp().getLookupValueLabel());
        	}
        	
        	List<Enrollee> enrolleeList  = enrolleeService.getSubscriberAndEnrollees(enrollment.getId());
        	
    		
    		List<EnrolleeDTO> objEnrolleeDTOList= new ArrayList<EnrolleeDTO>();
    		EnrolleeDTO objEnrolleeDTO =null;
    		for (Enrollee enrollee : enrolleeList) {
    			objEnrolleeDTO = new EnrolleeDTO();
    			if(null!=enrollee.getPersonTypeLkp() && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase("SUBSCRIBER")){
    				objEnrolleeDTO.setPersonTypeCode(enrollee.getPersonTypeLkp().getLookupValueCode());
    			}
    			if(enrollee.getRelationshipToHCPLkp()!=null){
    				objEnrolleeDTO.setRelationshipToHCPCode(enrollee.getRelationshipToHCPLkp().getLookupValueLabel());
    			}
    			
    			//objEnrolleeDTO.setEnrolleeStatusCode(enrolleeStatusCode)
    			
    			objEnrolleeDTO.setId(enrollee.getId());
    			objEnrolleeDTO.setPreferredEmail(enrollee.getPreferredEmail());
    			objEnrolleeDTO.setFirstName(enrollee.getFirstName());
    			objEnrolleeDTO.setLastName(enrollee.getLastName());
    			objEnrolleeDTO.setMiddleName(enrollee.getMiddleName());
    			objEnrolleeDTO.setTaxIdNumber(enrollee.getTaxIdNumber());
    			
    			objEnrolleeDTO.setBirthDate(enrollee.getBirthDate());
    			objEnrolleeDTO.setPrimaryPhoneNo(enrollee.getPrimaryPhoneNo());
    			objEnrolleeDTO.setTaxIdNumber(enrollee.getTaxIdNumber());
    			objEnrolleeDTO.setId(enrollee.getId());
    			objEnrolleeDTO.setId(enrollee.getId());
    			objEnrolleeDTO.setCreatedOn(enrollee.getCreatedOn());
    			objEnrolleeDTO.setHomeAddressid(enrollee.getHomeAddressid());
    			if(enrollee.getHomeAddressid()!=null){
    				objEnrolleeDTO.setHomeAddress1(enrollee.getHomeAddressid().getAddress1());
    				objEnrolleeDTO.setHomeAddress2(enrollee.getHomeAddressid().getAddress2());
    				objEnrolleeDTO.setHomeAddressState(enrollee.getHomeAddressid().getState());
    				objEnrolleeDTO.setHomeAddressCity(enrollee.getHomeAddressid().getCity());
    				objEnrolleeDTO.setHomeAddressZip(enrollee.getHomeAddressid().getZip());
    			}
    			
    			objEnrolleeDTOList.add(objEnrolleeDTO);
    			
    		}
    		
    		objEnrollmentDTO.setEnrolleeDTOs(objEnrolleeDTOList);
    	}
		
    	return objEnrollmentDTO;
    }
/*	private PlanInfoDTO getPlanInfoDto(Plan plan) {
//		PlanInfoDTO planInfoDTO = new CapPlanInfoMapper().getDtoFromObject(plan);
		return new CapPlanInfoMapper().getDtoFromObject(plan);
	}*/
	
	private PlanInfoDTO getPlanInfoDto(Enrollment enrollment, SingleIssuerResponse issuerResponse) {
		PlanInfoDTO dto = null;
		
		if(enrollment !=null) {
			dto =  new PlanInfoDTO();
			
			dto.setId(enrollment.getPlanId());
//			dto.setMetalTier(plan.getIssuerPlanNumber());
//			dto.setMonthlyPremium(plan.getPlanRate())
			dto.setName(enrollment.getPlanName());
			dto.setType(InsuranceTypeMappingEnum.getValue(enrollment.getInsuranceTypeLkp().getLookupValueCode()));
			
			if(issuerResponse != null) {
				dto.setCarrierName(issuerResponse.getName());
				dto.setCarrierUrl(issuerResponse.getApplicationUrl());
				dto.setProducerUserName(issuerResponse.getProducerUserName());
				if(!StringUtils.isEmpty(issuerResponse.getProducerPassword())) {
					dto.setProducerPassword(issuerResponse.getProducerPassword());
				}
			}
		}
				
		return dto;
	}

	private SsapApplicantDTO getSsapApplicantDto(Enrollee enrollee) {
//		SsapApplicantDTO dto = new CapSSapApplicantMapper().getDtoFromObject(enrollee);
		return new CapSSapApplicantMapper().getDtoFromObject(enrollee);
	}
	
    @Override
	public boolean saveFfmAppData(CapOnExchangeFFMAppDto capFfmAppDto) {
		boolean bStatus = false;

		Enrollment enrollment = ienrollmentRepository
				.findBySsapApplicationId(capFfmAppDto.getSsapAppId());
		if (enrollment != null) {

			String applicationId = capFfmAppDto.getEnrollmentAppId();
			if (!StringUtils.isEmpty(applicationId)) {
				enrollment.setCarrierAppId(applicationId);
			}

			String confirmationNumber = capFfmAppDto
					.getEnrollmentConfirmationId();
			if (!StringUtils.isEmpty(confirmationNumber)) {
				enrollment.setConfirmationNumber(confirmationNumber);
			}
			
			enrollment.setUpdatedBy(getLoggedInUser());
			
			try {
				Enrollment savedEnrollment = ienrollmentRepository
						.save(enrollment);
				bStatus = (savedEnrollment != null);
			} catch (Exception e) {
				LOGGER.error("Error saving ffm app data in enrollment" , e);
				bStatus = false;
			}
		}
		return bStatus;
	}
    
    @Override
    public Enrollment updateEnrollmentDetails(EffectiveApplicationDTO effectiveApplicationDTO, Enrollment enrollment, boolean isSubmitApplication){
    	
    	if(org.apache.commons.lang3.StringUtils.isNotEmpty(effectiveApplicationDTO.getAptcAmount())){
    		float effectiveAptc = Float.valueOf(effectiveApplicationDTO.getAptcAmount()).floatValue();
    		float enrollmentAptc = null!=enrollment.getAptcAmt()?enrollment.getAptcAmt().floatValue():-1f;
    		if(Float.compare(enrollmentAptc,effectiveAptc)!=0){
    			enrollment.setAptcAmt(effectiveAptc);
    		}
    		
    	}
    	
    	if(org.apache.commons.lang3.StringUtils.isNotEmpty(effectiveApplicationDTO.getNetPremiumAmount())){
    		float effectiveNetPremium = Float.valueOf(effectiveApplicationDTO.getNetPremiumAmount()).floatValue();
    		float enrollmentNetPremium = null!=enrollment.getNetPremiumAmt()?enrollment.getNetPremiumAmt().floatValue():-1f;
    		if(Float.compare(effectiveNetPremium,enrollmentNetPremium)!=0){
    			enrollment.setNetPremiumAmt(effectiveNetPremium);
    		}
    	}
    	
    	if(org.apache.commons.lang3.StringUtils.isNotEmpty(effectiveApplicationDTO.getGrossPremiumAmount()) && !Float.valueOf(effectiveApplicationDTO.getGrossPremiumAmount()).equals(enrollment.getGrossPremiumAmt())){
    		float effectiveGrossPremium = Float.valueOf(effectiveApplicationDTO.getGrossPremiumAmount()).floatValue();
    		float enrollmentGrossPremium = null!=enrollment.getGrossPremiumAmt()?enrollment.getGrossPremiumAmt().floatValue():-1f;
    		if(Float.compare(effectiveGrossPremium,enrollmentGrossPremium)!=0){
    			enrollment.setGrossPremiumAmt(effectiveGrossPremium);
    		}
    		
    	}
    	
    	if(StringUtils.isNotEmpty(effectiveApplicationDTO.getEffectiveDate())){
			Date date=null;
			try {
				date = new SimpleDateFormat("MM/dd/yyyy").parse(effectiveApplicationDTO.getEffectiveDate());
			} catch (ParseException e) {
				LOGGER.error("Unable to set the effective date for the offexchange application: ", e);
			}
			
			if(date != null ) {
					enrollment.setBenefitEffectiveDate(new Timestamp(date.getTime()));
			}
		}
    	
    	if(org.apache.commons.lang3.StringUtils.isNotEmpty(effectiveApplicationDTO.getApplicationId()) && !effectiveApplicationDTO.getApplicationId().equals(enrollment.getCarrierAppId())){
    		enrollment.setCarrierAppId(effectiveApplicationDTO.getApplicationId());
    	}
    	
    	if(org.apache.commons.lang3.StringUtils.isNotEmpty(effectiveApplicationDTO.getVerificationEvent())){
    		enrollment.setVerificationEvent(effectiveApplicationDTO.getVerificationEvent());
    	}
    	
    	if(null == enrollment.getPrefferedContactTime() &&
    		org.apache.commons.lang.StringUtils.isNotEmpty(effectiveApplicationDTO.getBestTimeToContact())){
        		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueLabel("PREFERRED_TIME_TO_CONTACT", effectiveApplicationDTO.getBestTimeToContact().toLowerCase());
        		enrollment.setPrefferedContactTime(lookupValue);
        		
    	}

    	boolean sendPartnerEvent = false;
		if (isSubmitApplication) {
			if (null == enrollment.getSubmittedToCarrierDate()) {
				enrollment.setSubmittedToCarrierDate(new TSDate());
			}

			if (isNotNullAndEmpty(enrollment.getEnrollmentStatusLkp())
					&& EnrollmentPrivateConstants.ENROLLMENT_STATUS_ECOMMITTED
							.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())) {
				EnrollmentUtilsForCap.setCapAgentId(enrollment,userService);
				enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
				sendPartnerEvent = true;
			}
		}
		
		enrollment.setUpdatedBy(getLoggedInUser());
    	Enrollment savedEnrollment = saveEnrollment(enrollment);
    	if(savedEnrollment != null && sendPartnerEvent) {
    		publishPartnerEvent(savedEnrollment.getId(), savedEnrollment.getCmrHouseHoldId());
    		enrollmentPrivateUtils.recordApplicationEnrollmentPrivateEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString(), EnrollmentPrivateConstants.ENROLLMENT_STATUS_ECOMMITTED);
    	}
    	return savedEnrollment;
    }
    
    /**
     * Setting enrollment object with plan related information
     * @param planId
     * @param enrollment
     */
    private void setPlanInfoFromPlanManagement(String planId , Enrollment enrollment){
    	PlanResponse  planResponse = getPlanResponse(planId, enrollment.getBenefitEffectiveDate());
		if (null != planResponse) {
			enrollment.setHiosIssuerId(planResponse.getHiosIssuerId());
			enrollment.setCMSPlanID(planResponse.getIssuerPlanNumber());
			enrollment.setPlanName(planResponse.getPlanName());
			enrollment.setInsurerName(planResponse.getIssuerName());
			enrollment.setInsurerTaxIdNumber(planResponse.getTaxIdNumber());
			if (planResponse.getPlanLevel() != null) {
				enrollment.setPlanLevel(planResponse.getPlanLevel().trim().toUpperCase());
			}
			if(StringUtils.isNotBlank(planResponse.getInsuranceType())){
				enrollment.setInsuranceTypeLkp(getInsuranceTypeLkpFromPlanResponse(planResponse.getInsuranceType()));
			}
			setEnrollmentCommissionData(enrollment, planResponse);
		}
    }
	@Override
	@Transactional(readOnly = true)
	public List<Enrollment> findEnrollmentByStatus(String status) {
		return enrollmentRepository.findEnrollmentByStatus(status);
	}

    /**
     * Method to call Plan Management API to get the plan response
     * @param planId
     * @param effectiveDate 
     * @return PlanResponse
     */
	private PlanResponse getPlanResponse(String planId, Date effectiveDate) {
		
		PlanRequest planRequest= new PlanRequest();
		planRequest.setPlanId(planId);
		planRequest.setMarketType("INDIVIDUAL");
		if(null != effectiveDate) {
			planRequest.setCommEffectiveDate(DateUtil.dateToString(effectiveDate, GhixConstants.REQUIRED_DATE_FORMAT));
		}
		
		String planInfoResponse=null;
		try{
			planInfoResponse = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL,planRequest,String.class);
			LOGGER.info("getting successfull response of the plan data from plan management");
		}
		catch(Exception ex){
			LOGGER.error("exception in getting plan data from the plan management" , ex);
		}
		PlanResponse planResponse = null;
		try {
			planResponse = platformGson.fromJson(planInfoResponse,PlanResponse.class);
		} catch (Exception ex) {
			LOGGER.error("Error in unmarshalling planResponse" , ex);
		}
		return planResponse;
		
	}

	@Override
	public BigDecimal countEnrollmentsByAffiliateId(Long affiliateId) {
		return enrollmentRepository.countEnrollmentsByAffiliateId(affiliateId);
	}

	/* HIX-60785 */
	/**
		1. Enrollment try to Match with Product Type
		2. if Enrollment> 1 throw multi match
		3. Enrollment match with out Product Type  - 0, 1,2
	*/
	CarrierFeedQueryUtil feedQueryUtil = new CarrierFeedQueryUtil();

	@Override
	public List<Enrollment> findEnrollmentByCarrierAppId(String carrierAppId, String issuerName, String giProductType) {
		EntityManager em = null;
		try {
			em = entityManagerFactory.createEntityManager();
			if(!em.getTransaction().isActive()){
				em.getTransaction().begin();
			}
			String enrollemntQuery =  feedQueryUtil.getEnrollmentByCarrierAppIdQuery(carrierAppId, issuerName, giProductType);
			Query hqlQuery = em.createQuery(enrollemntQuery);
			LOGGER.info("FindEnrollmentByCarrierAppId HQL QUERY: "+hqlQuery.toString());
	
			List<Enrollment> resultList = hqlQuery.getResultList();
			
			return resultList;//enrollmentRepository.findByCarrierAppId(carrierAppId);
		} catch (Exception e) {
			throw new GIRuntimeException("Could not generate findEnrollmentByCarrierAppId and  Error Message : "+e.getMessage() , e);
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
	}
	
	@Override
	public List<Enrollment> findEnrollmentByCarrierUID(String carrierAppUID, String issuerName, String giProductType) {
		List<Enrollment> resultList =null;
		EntityManager em = null;
		try {
			em = entityManagerFactory.createEntityManager();
			if(!em.getTransaction().isActive()){
				em.getTransaction().begin();
			}
			String enrollemntQuery = feedQueryUtil.getEnrollmentByCarrierUIDQuery(carrierAppUID, issuerName, giProductType);
			Query hqlQuery = em.createQuery(enrollemntQuery);
			LOGGER.info("FindEnrollmentByCarrierUID HQL QUERY: "+hqlQuery.toString());

			resultList = hqlQuery.getResultList();
			
		} catch (Exception e) {
			throw new GIRuntimeException("Could not findEnrollmentByCarrierUID "+e.getMessage() , e);
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		return resultList;// enrollmentRepository.findBycarrierAppUID(carrierAppUID);
	}
	
	//TODO:
	@Override
	public List<Enrollment> findEnrollmentByissuerPolicyId(String policyId, String issuerName, String giProductType) {
		List<Enrollment> resultList =null;
		EntityManager em = null;
		try {
			em = entityManagerFactory.createEntityManager();
			if(!em.getTransaction().isActive()){
				em.getTransaction().begin();
			}
			String enrollmentQuery = feedQueryUtil.getEnrollmentByissuerPolicyIdQuery(policyId, issuerName, giProductType);
			Query hqlQuery = em.createQuery(enrollmentQuery);
			LOGGER.info("FindEnrollmentByissuerPolicyId HQL QUERY: "+hqlQuery.toString());
			resultList = hqlQuery.getResultList();
		} catch (Exception e) {
			throw new GIRuntimeException("Could not findEnrollmentByissuerPolicyId "+e.getMessage() , e);
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		return resultList;//enrollmentRepository.findByissuerAssignPolicyNo(policyId);
	}
	
	/**
	 * @author Aditya-S
	 * 
	 * Returns Enrollment and Enrollee Info for Given enrollment ID.
	 * HIX-64906
	 */

	@Override
	public EnrollmentResponse getEnrollmentCapInfo(String enrollmentId){
		LOGGER.info("getEnrollmentCapInfo Service");
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		if(enrollmentId == null || enrollmentId.isEmpty()){
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
			LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY);
			return enrollmentResponse;
		}
		try{
			EnrollmentCapDto enrollmentCapDtoObj = new EnrollmentCapDto();
			Enrollment enrollment = enrollmentRepository.findById(Integer.valueOf(enrollmentId));
			if (enrollment != null)
			{
				enrollmentCapDtoObj.setSsapId(String.valueOf(enrollment.getSsapApplicationid()));
				enrollmentCapDtoObj.setEnrollmentId(Integer.valueOf(enrollmentId));	
				enrollmentCapDtoObj.setApplicationId(enrollment.getCarrierAppId());
				enrollmentCapDtoObj.setExchangeAssignPolicyNo(enrollment.getExchangeAssignPolicyNo());
				enrollmentCapDtoObj.setIssuerAssignPolicyNo(enrollment.getIssuerAssignPolicyNo());
				if(enrollment.getEnrollmentStatusLkp() != null){
					enrollmentCapDtoObj.setEnrollmentStatus(enrollment.getEnrollmentStatusLkp().getLookupValueCode());
				}
				if(enrollment.getInsuranceTypeLkp() != null){
					enrollmentCapDtoObj.setPlanType(enrollment.getInsuranceTypeLkp().getLookupValueLabel());
				}
				
				if(enrollment.getSubmittedToCarrierDate() != null){
					enrollmentCapDtoObj.setSubmitDate(DateUtil.dateToString(enrollment.getSubmittedToCarrierDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}

				if(enrollment.getBenefitEffectiveDate() != null){
					enrollmentCapDtoObj.setEffectiveDate(DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}

				enrollmentCapDtoObj.setGrossPremiumAmt(enrollment.getGrossPremiumAmt());
				enrollmentCapDtoObj.setAptcAmt(enrollment.getAptcAmt());
				
				//HIX-72874 Add LastTimestamp attribute to DTO
				if(enrollment.getUpdatedOn() != null){
					enrollmentCapDtoObj.setLastUpdatedTimestamp(DateUtil.dateToString(enrollment.getUpdatedOn(), GhixConstants.DISPLAY_DATE_FORMAT1));
				}
				
				if(isNotNullAndEmpty(enrollment.getCapAgentId())){
					enrollmentCapDtoObj.setCapAgentId(enrollment.getCapAgentId());
					enrollmentCapDtoObj.setCapAgentName(userService.getUserName(enrollment.getCapAgentId()));
				}
				else{
					LOGGER.info("Cap Agent ID is null or empty for Enrollment Id: "+enrollmentId);
				}
				
				if(enrollment.getExchangeTypeLkp() != null){
					enrollmentCapDtoObj.setExchangeType(enrollment.getExchangeTypeLkp().getLookupValueLabel());
				}
				
				enrollmentCapDtoObj.setPlanTier(enrollment.getPlanLevel());
				
			    if(isNotNullAndEmpty(enrollment.getPlanName())){
					enrollmentCapDtoObj.setPlanName(enrollment.getPlanName());
				}
				
				enrollmentCapDtoObj.setPlanId(enrollment.getPlanId());
				enrollmentCapDtoObj.setIssuerName(enrollment.getInsurerName());
				List<Date> lastStatusUpdateTimeStampList = null;
				Date lastStatusUpdateTimeStamp = null;
				
				Sort sortObject = new Sort(new Sort.Order(Sort.Direction.ASC, "updatedOn"));
				Pageable pageable = new PageRequest(0, 1, sortObject);
				
				if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase("POSTGRESQL")) {
					long tenantId = 0;
					if(TenantContextHolder.getTenant() != null) {
						tenantId = TenantContextHolder.getTenant().getId();
					}
					lastStatusUpdateTimeStamp = iEnrollmentAudRepository
							.getEnrollmentLastStatusUpdateTimeStampForPostgres(tenantId,enrollment.getId());
				} else {
					lastStatusUpdateTimeStampList = iEnrollmentAudRepository.getEnrollmentLastStatusUpdateTimeStamp(enrollment.getId(), pageable);
					
					if(lastStatusUpdateTimeStampList != null && !lastStatusUpdateTimeStampList.isEmpty() ) {
						lastStatusUpdateTimeStamp = lastStatusUpdateTimeStampList.get(0);
					}
					
				}
				
				if(lastStatusUpdateTimeStamp != null)
				{
					enrollmentCapDtoObj.setEnrollmentLastStatusUpdateTimeStamp(DateUtil.dateToString(lastStatusUpdateTimeStamp, GhixConstants.DISPLAY_DATE_FORMAT1));
				}
				else
				{
					/**
					 * As per CAP team Enrollment last status update time is required for billing.
					 * so in any case if we found this data as null we will be sending them error Code 203
					 * Reference Jira Id: HIX-76961
					 */
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_203);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrMsg("lastStatusUpdateTimeStamp is null in Enrollment_Aud for: Enrollment id: " + enrollmentId);
					return enrollmentResponse;
				}
				
				/**
				 * Added getEnrolleeByEnrollmentID 
				 */
				List<Enrollee> enrolleeList = enrolleeRepository.getEnrolleeByEnrollmentID(enrollment.getId());
				if(enrolleeList != null  && !enrolleeList.isEmpty()){
					enrollmentCapDtoObj.setNoOfEnrollees(enrolleeList.size());
					List<EnrolleeCapDto> enrolleeCapDtoList = new ArrayList<EnrolleeCapDto>();
					
					for(Enrollee enrollee : enrolleeList){
						EnrolleeCapDto enrolleeCapDtoObj = new EnrolleeCapDto();
						enrolleeCapDtoObj.setName(enrollee.getFirstName() + " "+ enrollee.getLastName());
						enrolleeCapDtoObj.setEmail(enrollee.getPreferredEmail());

						if(enrollee.getBirthDate() != null){
							enrolleeCapDtoObj.setDob(DateUtil.dateToString(enrollee.getBirthDate(), GhixConstants.REQUIRED_DATE_FORMAT));
						}

						enrolleeCapDtoObj.setPhone(enrollee.getPrimaryPhoneNo());

						if(enrollee.getPersonTypeLkp() != null && EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())){
							enrolleeCapDtoObj.setSubscriber(true);
						}
						enrolleeCapDtoList.add(enrolleeCapDtoObj);
					}
					enrollmentCapDtoObj.setEnrolleeCapDtoList(enrolleeCapDtoList);
					enrollmentResponse.setEnrollmentCapDto(enrollmentCapDtoObj);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}else {
					LOGGER.error("GetEnrollmentCapInfo :: Error getEnrollmentCapInfo : Enrollment id: " + enrollmentId + " No Enrollee Found.");
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrMsg("Error getEnrollmentCapInfo : Enrollment id: " + enrollmentId + " No Enrollee Found.");
				}
			}else{
				LOGGER.error("GetEnrollmentCapInfo :: Error getEnrollmentCapInfo : Enrollment id: " + enrollmentId + " is not found.");
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Error getEnrollmentCapInfo : Enrollment id: " + enrollmentId + " is not found.");
			}
		} catch (Exception ge) {
			LOGGER.error("Error getEnrollmentCapInfo Enrollment ID: " + enrollmentId, ge);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("GetEnrollmentCapInfo :: Error fetching Enrollment Data for Enrollment ID: " + enrollmentId);
		}
		return enrollmentResponse;
	}
	
	@Override
	public void publishPartnerEvent(Integer enrollmentId, Integer householdId) {
		enrollmentPartnerNotificationService.publishPartnerEvent(enrollmentId, householdId);
	}

	@Override
	public Household getHousehold(Integer householdId)throws GIException {
		return enrollmentExternalRestUtil.getHousehold(householdId);
	}

	@Override
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public void updateEnrollmentForBOBSuccess(EnrollmentBobRequest enrollmentBobRequest) throws GIException {
		boolean isUpdateEnrollment = Boolean.FALSE;
		Enrollment enrollment = enrollmentRepository.findOne(enrollmentBobRequest.getEnrollmentId());
		if(enrollment != null){
			AccountUser lastUpdateByUser = null;
			if(StringUtils.isEmpty(enrollmentBobRequest.getLastUpdatedByUserEmail())){
				throw new GIException("Received Null or Empty LastUpdatedBy User Email Address");
			}
			else{
				lastUpdateByUser = userService.findByEmail(enrollmentBobRequest.getLastUpdatedByUserEmail());
				if(lastUpdateByUser == null){
					throw new GIException("No AccountUser found with the given UserEmail");
				}
			}
			if(enrollmentBobRequest.getEnrollmentStatus() != null &&
					!StringUtils.equals(enrollment.getEnrollmentStatusLkp().getLookupValueCode(), enrollmentBobRequest.getEnrollmentStatus().toString())) {
				enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, enrollmentBobRequest.getEnrollmentStatus().toString()));
				isUpdateEnrollment = Boolean.TRUE;
			}
			if(StringUtils.isNotEmpty(enrollmentBobRequest.getCarrierAppId()) && !StringUtils.equals(enrollment.getCarrierAppId(), enrollmentBobRequest.getCarrierAppId()) ){
				enrollment.setCarrierAppId(enrollmentBobRequest.getCarrierAppId());
				isUpdateEnrollment = Boolean.TRUE;
			}
			if(StringUtils.isNotEmpty(enrollmentBobRequest.getCarrierAppUid()) && !StringUtils.equals(enrollment.getCarrierAppUID(), enrollmentBobRequest.getCarrierAppUid())){
				enrollment.setCarrierAppUID(enrollmentBobRequest.getCarrierAppUid());
				isUpdateEnrollment = Boolean.TRUE;
			}
			/*
			 * Adding implementation as per jira id HIX-82763
			 */
			if(StringUtils.isNotEmpty(enrollmentBobRequest.getPolicyId())){
				if(!StringUtils.equals(enrollment.getIssuerAssignPolicyNo(), enrollmentBobRequest.getPolicyId())){
					enrollment.setIssuerAssignPolicyNo(enrollmentBobRequest.getPolicyId());
					isUpdateEnrollment = Boolean.TRUE;
				}
				if(!StringUtils.equals(enrollment.getExchangeAssignPolicyNo(), enrollmentBobRequest.getPolicyId()))
				{
					enrollment.setExchangeAssignPolicyNo(enrollmentBobRequest.getPolicyId());
					isUpdateEnrollment = Boolean.TRUE;
				}
			}
			if(enrollmentBobRequest.getDateClosed() != null && enrollmentBobRequest.getDateClosed() != enrollment.getDateClosed()){
				enrollment.setDateClosed(enrollmentBobRequest.getDateClosed());
				isUpdateEnrollment = Boolean.TRUE;
			}
			if(enrollmentBobRequest.getDuplicateEnrollmentId() != null && enrollmentBobRequest.getDuplicateEnrollmentId() != enrollment.getDuplicateEnrollmentId()){
				enrollment.setDuplicateEnrollmentId(enrollmentBobRequest.getDuplicateEnrollmentId());
				isUpdateEnrollment = Boolean.TRUE;
			}
			
			if(isUpdateEnrollment){
				//Added Last_Updated_bu User Info
				//For Unsecured layer removing LoggedIn User changes 
				enrollment.setUpdatedBy(lastUpdateByUser);
				setEnrollmentSubmittedBy(enrollment);
				enrollmentRepository.saveAndFlush(enrollment);
			}
			/*else{
				throw new GIException("Null or Empty request found for EnrollmentBOBSuccesss update");
			}*/
		}
		else{
			throw new GIException("Unable to find enrollment Record for EnrollmentId: "+enrollmentBobRequest.getEnrollmentId());
		}
	}

	@Override
	public EnrollmentUpdateHouseholdDTO updateEnrollmentCmrHouseholdId(EnrollmentUpdateHouseholdDTO requestDTO) {
		
		if(null == requestDTO.getCmrHouseholdId()){
			requestDTO.setStatus(EnrollmentUpdateHouseholdDTO.Status.FAILURE);
			requestDTO.setErrorMsg("CMR Household cannot be Null");
			return requestDTO;
		}else if(null == requestDTO.getSsapIdList() || requestDTO.getSsapIdList().isEmpty()){
			requestDTO.setStatus(EnrollmentUpdateHouseholdDTO.Status.FAILURE);
			requestDTO.setErrorMsg("SSAP ID list cannot be empty");
			return requestDTO;
		}
		
		int updateCount = enrollmentRepository.updateEnrollmentCmrHouseholdBySsapId(requestDTO.getCmrHouseholdId(), requestDTO.getSsapIdList());
		requestDTO.setUpdateCount(updateCount);
		requestDTO.setStatus(EnrollmentUpdateHouseholdDTO.Status.SUCCESS);
		return requestDTO;
	}
	
	private List<EnrollmentBobDTO> generateEnrollmentBobDTOs(List<Object[]> hicnEnrollments){
		List<EnrollmentBobDTO> enrollmentBobDTOs = new ArrayList<EnrollmentBobDTO>();
		
		EnrollmentBobDTO bobDTO = null;
		
		for(Object[] enrollmentData:hicnEnrollments){
			bobDTO = new EnrollmentBobDTO();
			
			bobDTO.setEnrollmentId(((BigDecimal)enrollmentData[EnrollmentConstants.ZERO]).intValueExact());
			bobDTO.setBenefitEffectiveDate((Date)enrollmentData[EnrollmentConstants.ONE]);
			bobDTO.setInsuranceType((String)enrollmentData[EnrollmentConstants.TWO]);
			bobDTO.setIssuerAssignPolicyNo((String)enrollmentData[EnrollmentConstants.THREE]);
			bobDTO.setExchangeAssignPolicyNo((String)enrollmentData[EnrollmentConstants.FOUR]);
			bobDTO.setCarrierAppId((String)enrollmentData[EnrollmentConstants.FIVE]);
			bobDTO.setCarrierAppUID((String)enrollmentData[EnrollmentConstants.SIX]);
			bobDTO.setEnrollmentStatus((String)enrollmentData[EnrollmentConstants.SEVEN]);
			bobDTO.setRenewalFlag((String)enrollmentData[EnrollmentConstants.EIGHT]);
			bobDTO.setIssuerId(enrollmentData[EnrollmentConstants.NINE] != null ? ((BigDecimal)enrollmentData[EnrollmentConstants.NINE]).intValueExact() : null);
			enrollmentBobDTOs.add(bobDTO);
		}
		
		return enrollmentBobDTOs;
	}
	
	@Override
	public void searchEnrollmentForBOBRequest(final EnrollmentBobRequest enrollmentBobRequest, EnrollmentBobResponse enrollmentBobResponse){
		
		/*Long tenantId = enrollmentBobRequest.getTenantId() != null ? enrollmentBobRequest.getTenantId()
				: (TenantContextHolder.getTenant() != null) ? TenantContextHolder.getTenant().getId() : null;*/
		String carrierName = enrollmentBobRequest.getCarrierName();
		
		List<EnrollmentBobDTO> enrollmentBobDTOList = null;
		
		/*
		 * HIC Number Match
		 */
		if(StringUtils.isNotEmpty(enrollmentBobRequest.getHicn())){
			LOGGER.info("Searching By HICN : "+enrollmentBobRequest.getHicn());
			Long tenantId = null;
			
			if(TenantContextHolder.getTenant() != null){
				tenantId = TenantContextHolder.getTenant().getId();
			}
			
			List<Object[]> hicnEnrollments = enrollmentRepository.getEnrollmentByHICN(carrierName, enrollmentBobRequest.getHicn(), tenantId);
			enrollmentBobDTOList = this.generateEnrollmentBobDTOs(hicnEnrollments);
			 
			if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
				hicnEnrollments = enrollmentRepository.getEnrollmentByHICN(enrollmentBobRequest.getHicn(), tenantId);
				enrollmentBobDTOList = this.generateEnrollmentBobDTOs(hicnEnrollments);
				
				if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,carrierName);
				}
			}
			
			if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
				enrollmentBobResponse.setMatchType(MatchType.HICN);
				enrollmentBobResponse.setEnrollmentBobDTOList(enrollmentBobDTOList);
				return;
			}
			
		}
		
		/*
		 * Policy ID Match
		 */
		if(StringUtils.isNotEmpty(enrollmentBobRequest.getPolicyId())){
			LOGGER.info("Searching By PolicyID : "+enrollmentBobRequest.getPolicyId());
			
			enrollmentBobDTOList = enrollmentRepository.getEnrollmentByissuerPolicyId(carrierName, enrollmentBobRequest.getPolicyId());
			
			if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
				enrollmentBobDTOList = enrollmentRepository.getEnrollmentByissuerPolicyId(enrollmentBobRequest.getPolicyId());
				
				if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,carrierName);
				}
			}
			
			if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
				enrollmentBobResponse.setMatchType(MatchType.POLICY_ID);
				enrollmentBobResponse.setEnrollmentBobDTOList(enrollmentBobDTOList);
				return;
			}
		}
		
		/*
		 * Carrier APP ID Match
		 */
		if(StringUtils.isNotEmpty(enrollmentBobRequest.getCarrierAppId())){
			LOGGER.info("Searching By CarrierAppId : "+enrollmentBobRequest.getCarrierAppId());
			enrollmentBobDTOList = enrollmentRepository.getEnrollmentByCarrierAppId(carrierName, enrollmentBobRequest.getCarrierAppId());
			
			if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
				enrollmentBobDTOList = enrollmentRepository.getEnrollmentByCarrierAppId(enrollmentBobRequest.getCarrierAppId());
				
				if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,carrierName);
				}
			}
			if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
				enrollmentBobResponse.setMatchType(MatchType.APP_ID);
				enrollmentBobResponse.setEnrollmentBobDTOList(enrollmentBobDTOList);
				return;
			}
		}
		
		/*
		 * Carrier APP Uid match
		 */
		if(StringUtils.isNotEmpty(enrollmentBobRequest.getCarrierAppUid())){
			LOGGER.info("Searching By CarrierAppUid : "+enrollmentBobRequest.getCarrierAppUid());
			enrollmentBobDTOList = enrollmentRepository.getEnrollmentBycarrierAppUId(carrierName, enrollmentBobRequest.getCarrierAppUid());
			
			if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
				enrollmentBobDTOList = enrollmentRepository.getEnrollmentBycarrierAppUId(enrollmentBobRequest.getCarrierAppUid());
				if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,carrierName);
				}
			}
			if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
				enrollmentBobResponse.setMatchType(MatchType.UID);
				enrollmentBobResponse.setEnrollmentBobDTOList(enrollmentBobDTOList);
				return;
			}	
		}
		
		
		/*
		 * Fuzzy Match
		 */
		if(StringUtils.isNotEmpty(enrollmentBobRequest.getFirstName()) && StringUtils.isNotEmpty(enrollmentBobRequest.getLastName())){
			String firstName = enrollmentBobRequest.getFirstName().toLowerCase();
			String lastName = enrollmentBobRequest.getLastName().toLowerCase();
			
			if (!(StringUtils.isEmpty(enrollmentBobRequest.getBenefitEffDateMatchingRangeFrom()) || StringUtils.isEmpty(enrollmentBobRequest.getBenefitEffDateMatchingRangeTo()))){
				enrollmentBobDTOList = enrollmentRepository.findEnrollmentByEnrolleeAndEffectiveDateQuery(carrierName, firstName, lastName, enrollmentBobRequest.getBenefitEffDateMatchingRangeFrom(), enrollmentBobRequest.getBenefitEffDateMatchingRangeTo());
				
				if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = enrollmentRepository.findEnrollmentByEnrolleeAndEffectiveDateQuery(firstName, lastName, enrollmentBobRequest.getBenefitEffDateMatchingRangeFrom(), enrollmentBobRequest.getBenefitEffDateMatchingRangeTo());
					if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
						enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,carrierName);
					}
				}
			}
			else{
				enrollmentBobDTOList = enrollmentRepository.findEnrollmentByEnrolleeByFirstNameAndLastName(carrierName, firstName, lastName);
				
				if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = enrollmentRepository.findEnrollmentByEnrolleeByFirstNameAndLastName(firstName, lastName);
					if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
						enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,carrierName);
					}
				}
			}
			
			if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
				enrollmentBobResponse.setMatchType(MatchType.FUZZY_MATCH);
				enrollmentBobResponse.setEnrollmentBobDTOList(enrollmentBobDTOList);
			}
			
			if(enrollmentBobDTOList != null && enrollmentBobDTOList.size() > 1){
        		LOGGER.info("-------- Multiple Fuzzy Match --------");
        		Map<Integer,EnrollmentBobDTO> enrollmentMap = new HashMap<Integer,EnrollmentBobDTO>();
        		List<EnrollmentBobDTO> enrollmentBobs = new ArrayList<EnrollmentBobDTO>();
        		
        		for (EnrollmentBobDTO enrollment : enrollmentBobDTOList) {
        			enrollmentMap.put(enrollment.getEnrollmentId(),enrollment);
				}
        		
        		List<Integer> newEnrollList = enrolleeRepository.getMatchedSubscriberByState(enrollmentBobRequest.getState(),enrollmentMap.keySet());
        		
        		if(newEnrollList == null || newEnrollList.isEmpty()){
        			enrollmentBobResponse.setMatchType(null);
    				enrollmentBobResponse.setEnrollmentBobDTOList(null);
        		}else{
        			enrollmentBobResponse.setMatchType(MatchType.FUZZY_MATCH);
        			for(Integer enId: newEnrollList){
        				enrollmentBobs.add(enrollmentMap.get(enId));
        			}
        			enrollmentBobResponse.setEnrollmentBobDTOList(enrollmentBobs);
        		}
        	}
		}
	}
	
	private String generateMultiMatchReason(List<EnrollmentBobDTO> enrollmentBobDTOList){
		int counter = 1;
		StringBuilder sb = new StringBuilder();

		for (EnrollmentBobDTO e : enrollmentBobDTOList) {
			if (enrollmentBobDTOList.size() - counter > 0)
				sb.append(e.getEnrollmentId()).append(",");
			else
				sb.append(e.getEnrollmentId());
			counter++;
		}
		return sb.toString();
	}

	@Override
	public void searchEnrollmentForBOBMultilineRequest(EnrollmentBobRequest enrollmentBobRequest, EnrollmentBobResponse enrollmentBobResponse){
		
		BOBMultiRowProcessDetails processDetails = new BOBMultiRowProcessDetails();
		
		List<BOBMultipleRowRenewalModel> bobMultipleRowRenewalModels = enrollmentBobRequest.getBobMultipleRowRenewalModels();  // will get from EnrollmentBobRequest 
		Collections.sort(bobMultipleRowRenewalModels);
		List<EnrollmentRenewals> dbRenewals = new ArrayList<EnrollmentRenewals>();
		
		try{
			
			BOBMultipleRowRenewalModel bobModel = bobMultipleRowRenewalModels.get(0);

			List<EnrollmentBobDTO> enrollmentBobDTOs = this.bobExactMatching(enrollmentBobRequest, bobModel, processDetails);

			if (enrollmentBobDTOs != null && enrollmentBobDTOs.size() > 0) { // PolicyId, ProductType match success

				List<EnrollmentBobDTO> enrollmentBobDTOs_edm = this.matchBenefitEffectiveDate(enrollmentBobDTOs, bobModel); // Match {Effective Date}

				if (enrollmentBobDTOs_edm.size() > 1) {
					throw new Exception("FAIL_MULTIMATCH:" + this.generateMultiMatchReason(enrollmentBobDTOs_edm));
				} else if (enrollmentBobDTOs_edm.size() == 1) { // Effective Date Range-Match Success

					EnrollmentBobDTO enrollmentBobDTO = enrollmentBobDTOs_edm.get(0);
					processDetails.setLatestStatus(bobModel.getEnrollmentStatus());
					processDetails.setEnrollmentId(enrollmentBobDTO.getEnrollmentId());
					processDetails.setEnrollmentBobDTO(enrollmentBobDTO);
					processDetails.setOrgEffectiveDate(enrollmentBobDTO.getBenefitEffectiveDate());
					
					List<BOBMultipleRowRenewalModel> renewalModels = processDetails.getRenewalRecords();
					if (renewalModels == null) {
						renewalModels = new ArrayList<BOBMultipleRowRenewalModel>();
						processDetails.setRenewalRecords(renewalModels);
					}

					renewalModels.add(bobModel);
				} else { // Else (No Effective Date Match)
					if (enrollmentBobDTOs.size() > 1) { // throw FAIL_MULTIMATCH
						throw new Exception("FAIL_MULTIMATCH:" + this.generateMultiMatchReason(enrollmentBobDTOs));
					} else {
						// TODO Isexception = Match Renewals Records()
						EnrollmentBobDTO enrollmentBobDTO = enrollmentBobDTOs.get(0);
						List<EnrollmentRenewals> eRenewals = iEnrollmentRenewalRepository.findEnrollmentRenwalsByEnrollmentId(enrollmentBobDTO.getEnrollmentId());
						if(eRenewals != null && eRenewals.size() > 0) {
							dbRenewals.addAll(eRenewals);
						}
						matchRenewalRecords(dbRenewals, bobModel, enrollmentBobDTO.getEnrollmentId(), enrollmentBobDTO.getBenefitEffectiveDate(),processDetails,false);
						
						processDetails.setLatestStatus(bobModel.getEnrollmentStatus());
						processDetails.setEnrollmentId(enrollmentBobDTO.getEnrollmentId());
						processDetails.setEnrollmentBobDTO(enrollmentBobDTO);
						processDetails.setOrgEffectiveDate(enrollmentBobDTO.getBenefitEffectiveDate());
						
						List<BOBMultipleRowRenewalModel> renewalModels = processDetails.getRenewalRecords();
						if (renewalModels == null) {
							renewalModels = new ArrayList<BOBMultipleRowRenewalModel>();
							processDetails.setRenewalRecords(renewalModels);
						}
						renewalModels.add(bobModel);
					}

				}
			} else { // Else: PolicyId, ProductType match Unsuccessful in
						// Enrollment:

				List<Integer> renewalEnrollmentIDs = iEnrollmentRenewalRepository.getEnrollmentIdByRenewalPolicyId(
						enrollmentBobRequest.getPolicyId(), enrollmentBobRequest.getProductType());

				if (renewalEnrollmentIDs == null || renewalEnrollmentIDs.size() == 0) {
					throw new Exception("BOB Fail No Enrollment Exception");
				}

				List<EnrollmentBobDTO> bobEnDTOs = enrollmentRepository
						.getEnrollmentBobDTOByIdAndCarrierName(enrollmentBobRequest.getCarrierName(), renewalEnrollmentIDs);
				renewalEnrollmentIDs = null;

				if (bobEnDTOs == null || bobEnDTOs.size() == 0) {
					throw new Exception("BOB Fail No Enrollment Exception");
				} else if (bobEnDTOs.size() > 1) {
					throw new Exception("FAIL_MULTIMATCH:" + this.generateMultiMatchReason(bobEnDTOs));
				}

				EnrollmentBobDTO bobDTO = bobEnDTOs.get(0);

				List<EnrollmentRenewals> eRenewals = iEnrollmentRenewalRepository.findEnrollmentRenwalsByEnrollmentId(bobDTO.getEnrollmentId());
				if(eRenewals != null && eRenewals.size() > 0) {
					dbRenewals.addAll(eRenewals);
				}
				matchRenewalRecords(dbRenewals, bobModel, bobDTO.getEnrollmentId(), bobDTO.getBenefitEffectiveDate(),processDetails,false);

				processDetails.setLatestStatus(bobModel.getEnrollmentStatus());
				processDetails.setEnrollmentId(bobDTO.getEnrollmentId());
				processDetails.setEnrollmentBobDTO(bobDTO);
				processDetails.setOrgEffectiveDate(bobDTO.getBenefitEffectiveDate());
				
				List<BOBMultipleRowRenewalModel> renewalModels = processDetails.getRenewalRecords();
				if (renewalModels == null) {
					renewalModels = new ArrayList<BOBMultipleRowRenewalModel>();
					processDetails.setRenewalRecords(renewalModels);
				}
				renewalModels.add(bobModel);

			}
			
			if(processDetails.getEnrollmentId() == null){
				throw new Exception("BOB Fail No Enrollment Exception");
			}
			
			// for iter > 0
			
			if(dbRenewals.size() == 0) {
				List<EnrollmentRenewals> eRenewals = iEnrollmentRenewalRepository.findEnrollmentRenwalsByEnrollmentId(processDetails.getEnrollmentId());
				if(eRenewals != null && eRenewals.size() > 0) {
					dbRenewals.addAll(eRenewals);
				}
			}
			
			int iter = 0;
			boolean isSkipThresholdCheck = true; // skip Threshold check for first feed renewal record
			for(BOBMultipleRowRenewalModel renewalModel:bobMultipleRowRenewalModels){
				if(iter>0){
					isSkipThresholdCheck = false;
				}
				matchRenewalRecords(dbRenewals, renewalModel, processDetails.getEnrollmentId(), processDetails.getOrgEffectiveDate(),processDetails, isSkipThresholdCheck);
				iter++;
			}
			
			/* Sort the EnrollmentRenewals in chronological order starting with the oldest using Benefit Effective Date */
			Collections.sort(dbRenewals, new Comparator<EnrollmentRenewals>(){	 
	            @Override
	            public int compare(EnrollmentRenewals o1, EnrollmentRenewals o2) {
	                return (((Date)o1.getBenefitStartDate()).compareTo(((Date)o2.getBenefitStartDate())));
	            }
	        });
			
			EnrollmentRenewals last_ER = dbRenewals.get(dbRenewals.size() - 1);
			if(EnrollmentConstants.ENROLLMENT_STATUS_INFORCE.equals(last_ER.getEnrollmentStatus()) || EnrollmentConstants.ENROLLMENT_STATUS_TERM.equals(last_ER.getEnrollmentStatus()) || EnrollmentConstants.ENROLLMENT_STATUS_WITHDRAWN.equals(last_ER.getEnrollmentStatus())) {
				for(int i=0;i<dbRenewals.size()-1;i++) {	
					EnrollmentRenewals erDB = dbRenewals.get(i);
					if(!EnrollmentConstants.ENROLLMENT_STATUS_WITHDRAWN.equals(erDB.getEnrollmentStatus())) {
						erDB.setEnrollmentStatus(EnrollmentConstants.ENROLLMENT_STATUS_TERM);
					}
				}
			}
			
			
			List<EnrollmentRenewalDTO> enrollmentRenewalDTOs = new ArrayList<EnrollmentRenewalDTO>();
			
			for(EnrollmentRenewals eeRenewals:dbRenewals){
				EnrollmentRenewalDTO renewalDTO = new EnrollmentRenewalDTO();
				CarrierFeedUtils.copyNonNullProperties(eeRenewals, renewalDTO);
				
				enrollmentRenewalDTOs.add(renewalDTO);
			}
			
			enrollmentBobResponse.setEnrollmentRenewalDTOs(enrollmentRenewalDTOs);
			enrollmentBobResponse.setEnrollmentId(String.valueOf(processDetails.getEnrollmentId()));
			enrollmentBobResponse.setMatchedEnrollmentBobDTO(processDetails.getEnrollmentBobDTO());
			enrollmentBobResponse.setEnrollmentStatus(processDetails.getLatestStatus());
			enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentBobResponse.setErrMsg(null);
		
		}catch(Exception e){
			enrollmentBobResponse.setEnrollmentId(String.valueOf(processDetails.getEnrollmentId()));
			enrollmentBobResponse.setErrCode(EnrollmentConstants.ERROR_CODE_202);
			enrollmentBobResponse.setStatus(GhixConstants.RESPONSE_PARTIAL_SUCCESS);
			enrollmentBobResponse.setErrMsg(e.getMessage());
		}
		
	}
	
	private void matchRenewalRecords(List<EnrollmentRenewals> dbRenewals, BOBMultipleRowRenewalModel bobMultipleRowRenewal, Integer enrollmentId, Date orgEffectiveStartDate, BOBMultiRowProcessDetails processDetails, boolean isSkipThresholdCheck) throws Exception{
		boolean isRenewalFound = false;
		for(EnrollmentRenewals enRenewal: dbRenewals){
			
			if(Enrollment.ENROLLMENT_STATUS_PENDING.equals(enRenewal.getEnrollmentStatus())){
				throw new Exception("BOB GI Renewals Pending Status");
			}
			
			if(CarrierFeedUtils.isMatchDateRangeWithoutTime(bobMultipleRowRenewal.getBenefitStartDate(), enRenewal.getBenefitStartDate()) == CarrierFeedUtils.Match.YES){
				enRenewal.setBenefitEndDate(bobMultipleRowRenewal.getBenefitEndDate() != null ? bobMultipleRowRenewal.getBenefitEndDate() : enRenewal.getBenefitEndDate());
				enRenewal.setPolicyId(bobMultipleRowRenewal.getPolicyId());
				enRenewal.setProductType(bobMultipleRowRenewal.getProductType());
				enRenewal.setPremium(bobMultipleRowRenewal.getPremium());
				enRenewal.setEnrollmentStatus(bobMultipleRowRenewal.getEnrollmentStatus());
				processDetails.setLatestStatus(bobMultipleRowRenewal.getEnrollmentStatus());
				isRenewalFound = true;
				break;
			}
		}
		
		if(!isRenewalFound){
			if(CarrierFeedUtils.checkDateWithRange(orgEffectiveStartDate, bobMultipleRowRenewal.getBenefitStartDate(), 90) && !isSkipThresholdCheck){
				throw new Exception("BOB Renewal Effective Date in Threshold Range");
			}
			
			// create new record in Enrollment Renewals table
			EnrollmentRenewals eRenewal = new EnrollmentRenewals();
			eRenewal.setEnrollmentId(enrollmentId);
			eRenewal.setBenefitStartDate(bobMultipleRowRenewal.getBenefitStartDate());
			eRenewal.setBenefitEndDate(bobMultipleRowRenewal.getBenefitEndDate());
			eRenewal.setEnrollmentStatus(bobMultipleRowRenewal.getEnrollmentStatus());
			eRenewal.setPolicyId(bobMultipleRowRenewal.getPolicyId());
			eRenewal.setPremium(bobMultipleRowRenewal.getPremium());
			eRenewal.setProductType(bobMultipleRowRenewal.getProductType());
			processDetails.setLatestStatus(bobMultipleRowRenewal.getEnrollmentStatus());
			
			dbRenewals.add(eRenewal);
		}
		
	}
	
	private List<EnrollmentBobDTO> bobExactMatching(EnrollmentBobRequest enrollmentBobRequest, BOBMultipleRowRenewalModel multipleRowRenewalModel, BOBMultiRowProcessDetails processDetails) throws Exception{
		List<EnrollmentBobDTO> enrollmentBobDTOList = null;
		
		Long tenantId = null;
		
		if(TenantContextHolder.getTenant() != null){
			tenantId = TenantContextHolder.getTenant().getId();
		}
		
		/*
		 * HIC Number Match
		 */
		if(StringUtils.isNotEmpty(enrollmentBobRequest.getHicn())){
			LOGGER.info("Searching By HICN : "+enrollmentBobRequest.getHicn());
			
			List<Object[]> hicnEnrollments = enrollmentRepository.getEnrollmentByHICN(enrollmentBobRequest.getCarrierName(), enrollmentBobRequest.getHicn(), tenantId);
			enrollmentBobDTOList = this.generateEnrollmentBobDTOs(hicnEnrollments);
			
			if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
				hicnEnrollments = enrollmentRepository.getEnrollmentByHICN(enrollmentBobRequest.getHicn(), tenantId);
				enrollmentBobDTOList = this.generateEnrollmentBobDTOs(hicnEnrollments);
				
				if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,enrollmentBobRequest.getCarrierName());
				}
				
			}
			
			if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
				processDetails.setMatchType(MatchType.HICN.toString());
				return this.matchProductType(enrollmentBobDTOList,multipleRowRenewalModel);
			}
		}
		
		/*
		 * Policy ID Match
		 */
		if(StringUtils.isNotEmpty(enrollmentBobRequest.getPolicyId())){
			LOGGER.info("Searching By PolicyID : "+enrollmentBobRequest.getPolicyId());
			
			enrollmentBobDTOList = enrollmentRepository.getEnrollmentByissuerPolicyId(enrollmentBobRequest.getCarrierName(), enrollmentBobRequest.getPolicyId());
			
			if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
				enrollmentBobDTOList = enrollmentRepository.getEnrollmentByissuerPolicyId(enrollmentBobRequest.getPolicyId());
				
				if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,enrollmentBobRequest.getCarrierName());
				}
			}
			
			if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
				processDetails.setMatchType(MatchType.POLICY_ID.toString());
				return this.matchProductType(enrollmentBobDTOList,multipleRowRenewalModel);
			}
		}
		
		/*
		 * Fuzzy Match
		 */
		if(StringUtils.isNotEmpty(enrollmentBobRequest.getFirstName()) && StringUtils.isNotEmpty(enrollmentBobRequest.getLastName())){
			String firstName = enrollmentBobRequest.getFirstName().toLowerCase();
			String lastName = enrollmentBobRequest.getLastName().toLowerCase();
			String benefitEffDateMatchingRangeFrom = null, benefitEffDateMatchingRangeTo = null;
			
			if(multipleRowRenewalModel.getBenefitStartDate() != null){
	        	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
	        	
        		LocalDate effectiveLocalDate = LocalDate.parse( new SimpleDateFormat("yyyy-MM-dd").format(multipleRowRenewalModel.getBenefitStartDate()) );
        		
        		LocalDate effectiveLocalDateRangeFrom = effectiveLocalDate.minus(CarrierFeedUtils.dateMatchingRange , ChronoUnit.DAYS);
        		LocalDate effectiveLocalDateRangeTo = effectiveLocalDate.plus(CarrierFeedUtils.dateMatchingRange, ChronoUnit.DAYS);
        		
        		benefitEffDateMatchingRangeFrom = effectiveLocalDateRangeFrom.format(dateTimeFormatter);
        		benefitEffDateMatchingRangeTo = effectiveLocalDateRangeTo.format(dateTimeFormatter);
        	}
			
			if (!(StringUtils.isEmpty(benefitEffDateMatchingRangeFrom) || StringUtils.isEmpty(benefitEffDateMatchingRangeTo))){
				enrollmentBobDTOList = enrollmentRepository.findEnrollmentByEnrolleeAndEffectiveDateQuery(enrollmentBobRequest.getCarrierName(), firstName, lastName, benefitEffDateMatchingRangeFrom, benefitEffDateMatchingRangeTo);

				if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = enrollmentRepository.findEnrollmentByEnrolleeAndEffectiveDateQuery(firstName, lastName, benefitEffDateMatchingRangeFrom, benefitEffDateMatchingRangeTo);
					if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
						enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,enrollmentBobRequest.getCarrierName());
					}
				}

			}
			else{
				enrollmentBobDTOList = enrollmentRepository.findEnrollmentByEnrolleeByFirstNameAndLastName(enrollmentBobRequest.getCarrierName(), firstName, lastName);
				
				if(enrollmentBobDTOList == null || enrollmentBobDTOList.isEmpty()){
					enrollmentBobDTOList = enrollmentRepository.findEnrollmentByEnrolleeByFirstNameAndLastName(firstName, lastName);
					if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
						enrollmentBobDTOList = this.matchWithIssuerBrandName(enrollmentBobDTOList,enrollmentBobRequest.getCarrierName());
					}
				}
			}
			
			if(enrollmentBobDTOList != null && !enrollmentBobDTOList.isEmpty()){
				processDetails.setMatchType(MatchType.FUZZY_MATCH.toString());
			}
			
			if(enrollmentBobDTOList != null && enrollmentBobDTOList.size() > 1){
        		LOGGER.info("-------- Multiple Fuzzy Match --------");
        		Map<Integer,EnrollmentBobDTO> enrollmentMap = new HashMap<Integer,EnrollmentBobDTO>();
        		List<EnrollmentBobDTO> enrollmentBobs = new ArrayList<EnrollmentBobDTO>();
        		
        		for (EnrollmentBobDTO enrollment : enrollmentBobDTOList) {
        			enrollmentMap.put(enrollment.getEnrollmentId(),enrollment);
				}
        		
        		List<Integer> newEnrollList = enrolleeRepository.getMatchedSubscriberByState(enrollmentBobRequest.getState(),enrollmentMap.keySet());
        		
        		if(newEnrollList == null || newEnrollList.isEmpty()){
        			processDetails.setMatchType(null);
    				return null;
        		}else{
        			processDetails.setMatchType(MatchType.FUZZY_MATCH.toString());
        			for(Integer enId: newEnrollList){
        				enrollmentBobs.add(enrollmentMap.get(enId));
        			}
        			return this.matchProductType(enrollmentBobDTOList,multipleRowRenewalModel);
        		}
        	}
		}

		return null;
		
	}
	
	 private List<EnrollmentBobDTO> matchBenefitEffectiveDate(List<EnrollmentBobDTO> enrollmentBobDTOList,BOBMultipleRowRenewalModel multipleRowRenewalModel) throws Exception{
		 List<EnrollmentBobDTO> matchedEnrollments= new ArrayList<EnrollmentBobDTO>();
		 Date feedBenefitStartDate = multipleRowRenewalModel.getBenefitStartDate();
	    	if(feedBenefitStartDate == null){
	    		throw new Exception("BOB GI Benefit Date Not Present");
	    	}
	    	
	    	for(EnrollmentBobDTO enrollment: enrollmentBobDTOList){
	    		if(enrollment.getBenefitEffectiveDate() == null){
	    			matchedEnrollments.add(enrollment);
	    		}else if(CarrierFeedUtils.isMatchDateRangeWithoutTime(feedBenefitStartDate, enrollment.getBenefitEffectiveDate()) == CarrierFeedUtils.Match.YES){
	    			matchedEnrollments.add(enrollment);
	    		}
	    	}
	    	
	    	return matchedEnrollments;
	    }
	
    private List<EnrollmentBobDTO> matchProductType(List<EnrollmentBobDTO> enrollmentBobDTOList,BOBMultipleRowRenewalModel multipleRowRenewalModel){
    	List<EnrollmentBobDTO> matchedEnrollments= new ArrayList<EnrollmentBobDTO>();
    	String productTypeFromFeed = multipleRowRenewalModel.getProductType();
    	
    	if(StringUtils.isEmpty(productTypeFromFeed)){
    		return enrollmentBobDTOList;
    	}
    	
    	for(EnrollmentBobDTO enrollment: enrollmentBobDTOList){
    		
    		if(enrollment.getInsuranceType() == null){
    			continue;
    		}
    		
		  String productTypeOfGI =  enrollment.getInsuranceType();
      	  if(productTypeFromFeed.equals(productTypeOfGI)){
      		matchedEnrollments.add(enrollment);
      	  }
     
    	}
    	
    	return matchedEnrollments;
    }

	@Override
	public List<EnrollmentSubscriberDetailsDTO> getEnrollmentSubscriberDetails(List<Integer> enrollmentIdList) {
		List<EnrollmentSubscriberDetailsDTO> enrollmentSubscriberDetailsDTOList = new ArrayList<EnrollmentSubscriberDetailsDTO>();
		List<Object[]> enrollmentSubscriberData = enrollmentRepository.getEnrollmentAndSubscriberDetails(enrollmentIdList);
		if(enrollmentSubscriberData != null && !enrollmentSubscriberData.isEmpty()){
			for(Object[] enrollmentSubsciberObject : enrollmentSubscriberData){
				EnrollmentSubscriberDetailsDTO enrollmentSubscriberDetailsDTO = new EnrollmentSubscriberDetailsDTO();
				
				enrollmentSubscriberDetailsDTO.setEnrollmentId((Integer)enrollmentSubsciberObject[EnrollmentConstants.ZERO]);
				enrollmentSubscriberDetailsDTO.setCarrierName((String)enrollmentSubsciberObject[EnrollmentConstants.ONE]);
				enrollmentSubscriberDetailsDTO.setPlanName((String)enrollmentSubsciberObject[EnrollmentConstants.TWO]);
				enrollmentSubscriberDetailsDTO.setInsuranceTypeLkpCode((String)enrollmentSubsciberObject[EnrollmentConstants.THREE]);
				enrollmentSubscriberDetailsDTO.setInsuranceTypeLkpType((String)enrollmentSubsciberObject[EnrollmentConstants.FOUR]);
				enrollmentSubscriberDetailsDTO.setIssuerAssignPolicyNo((String)enrollmentSubsciberObject[EnrollmentConstants.FIVE]);
				enrollmentSubscriberDetailsDTO.setCarrierAppId((String)enrollmentSubsciberObject[EnrollmentConstants.SIX]);
				enrollmentSubscriberDetailsDTO.setBenefitEffectiveDate((Date)enrollmentSubsciberObject[EnrollmentConstants.SEVEN]);
				enrollmentSubscriberDetailsDTO.setEnrollmentStatus((String)enrollmentSubsciberObject[EnrollmentConstants.EIGHT]);
				enrollmentSubscriberDetailsDTO.setNetPremiumAmount((Float)enrollmentSubsciberObject[EnrollmentConstants.NINE]);
				enrollmentSubscriberDetailsDTO.setSubscriberName((String)enrollmentSubsciberObject[EnrollmentConstants.TEN]);
				enrollmentSubscriberDetailsDTO.setSubscriberDob((Date)enrollmentSubsciberObject[EnrollmentConstants.ELEVEN]);
				enrollmentSubscriberDetailsDTO.setSubscriberPrimaryPhoneNo((String)enrollmentSubsciberObject[EnrollmentConstants.TWELVE]);
				enrollmentSubscriberDetailsDTO.setSubscriberHomeAddress1((String)enrollmentSubsciberObject[EnrollmentConstants.THIRTEEN]);
				enrollmentSubscriberDetailsDTO.setSubscriberHomeAddress2((String)enrollmentSubsciberObject[EnrollmentConstants.FOURTEEN]);
				enrollmentSubscriberDetailsDTO.setSubscriberHomeCity((String)enrollmentSubsciberObject[EnrollmentConstants.FIFTEEN]);
				enrollmentSubscriberDetailsDTO.setSubscriberHomeState((String)enrollmentSubsciberObject[EnrollmentConstants.SIXTEEN]);
				enrollmentSubscriberDetailsDTO.setSubscriberHomeZipCode((String)enrollmentSubsciberObject[EnrollmentConstants.SEVENTEEN]);
				enrollmentSubscriberDetailsDTO.setRenewalFlag((String)enrollmentSubsciberObject[EnrollmentConstants.EIGHTEEN]);
				enrollmentSubscriberDetailsDTO.setHomeAddress(null);
				
				enrollmentSubscriberDetailsDTOList.add(enrollmentSubscriberDetailsDTO);
				
			}
		}
		return enrollmentSubscriberDetailsDTOList;
	}

	@Override
	public List<EnrollmentSubscriberDetailsDTO> findEnrollmentByPolicyNumberHICN(EnrollmentStarRequest enrollmentStarRequest) throws GIException {
		
		List<EnrollmentSubscriberDetailsDTO> enrollmentSubscriberDetailsDTOList = new ArrayList<EnrollmentSubscriberDetailsDTO>();
		List<Object[]> enrollmentSubscriberData = null;
		
		Long tenantId = null;
		
		if(TenantContextHolder.getTenant() != null) {
			tenantId = TenantContextHolder.getTenant().getId();
		}else {
			tenantId = 1l; // Default tenant GINS
		}
		
		if(StringUtils.isNotEmpty(enrollmentStarRequest.getPolicyNumber()) || StringUtils.isNotEmpty(enrollmentStarRequest.getCarrierUID()) || StringUtils.isNotEmpty(enrollmentStarRequest.getAppID())) {
			enrollmentSubscriberData = enrollmentRepository.findEnrollmentByPolicyNo(enrollmentStarRequest.getAppID(),enrollmentStarRequest.getPolicyNumber(), enrollmentStarRequest.getCarrierUID(), enrollmentStarRequest.getEnrollmentStatusIds(), tenantId);
		}
		
		if(StringUtils.isNotEmpty(enrollmentStarRequest.getHicn()) && (enrollmentSubscriberData == null || enrollmentSubscriberData.isEmpty())){
			enrollmentSubscriberData = enrollmentRepository.findEnrollmentByHICN(enrollmentStarRequest.getHicn(), enrollmentStarRequest.getEnrollmentStatusIds(), tenantId);
		}
		
		if(enrollmentSubscriberData != null && !enrollmentSubscriberData.isEmpty()){
			for(Object[] enrollmentSubsciberObject : enrollmentSubscriberData){
				EnrollmentSubscriberDetailsDTO enrollmentSubscriberDetailsDTO = new EnrollmentSubscriberDetailsDTO();
				
				enrollmentSubscriberDetailsDTO.setEnrollmentId(((BigDecimal)enrollmentSubsciberObject[EnrollmentConstants.ZERO]).intValueExact());
				enrollmentSubscriberDetailsDTO.setCarrierName((String)enrollmentSubsciberObject[EnrollmentConstants.ONE]);
				enrollmentSubscriberDetailsDTO.setCarrierAppId((String)enrollmentSubsciberObject[EnrollmentConstants.TWO]);
				enrollmentSubscriberDetailsDTO.setEnrollmentStatusLkpID(((BigDecimal)enrollmentSubsciberObject[EnrollmentConstants.THREE]).intValueExact());
				enrollmentSubscriberDetailsDTO.setIssuerId(((BigDecimal)enrollmentSubsciberObject[EnrollmentConstants.FOUR]).intValueExact());
				enrollmentSubscriberDetailsDTO.setIssuerAssignPolicyNo((String)enrollmentSubsciberObject[EnrollmentConstants.FIVE]);
				
				enrollmentSubscriberDetailsDTOList.add(enrollmentSubscriberDetailsDTO);
			}
		}
		return enrollmentSubscriberDetailsDTOList;
	}

	@Override
	public List<EnrollmentBobDTO> findBobExpectedEnrollmentRecord(String carrierName, List<String> enrollmentStatusList) {
		return enrollmentRepository.findBobExpectedEnrollmentRecord(carrierName, enrollmentStatusList);
	}

	@Override
	public EnrollmentCommission saveEnrollmentCommision(EnrollmentCommissionRequest enrollmentCommissionRequest)throws GIException {
		Float amtPaidToDate = null;
		amtPaidToDate = (enrollmentCommissionRequest.getCommissionAmt() != null
				? Float.valueOf(enrollmentCommissionRequest.getCommissionAmt())
				: 0 ) + enrollmentCommissionService.sumCommissionAmtByEnrollmentIdAndCommissionDateLT(
						enrollmentCommissionRequest.getEnrollmentId(), enrollmentCommissionRequest.getCommissionDate(),
						enrollmentCommissionRequest.getDueMonthYear());
		
		if(amtPaidToDate != null){
			enrollmentCommissionRequest.setAmtPaidToDate(String.valueOf(CarrierFeedQueryUtil.getFormattedFloatValue(amtPaidToDate)));
		}
		return enrollmentCommissionService.saveEnrollmentCommission(enrollmentCommissionRequest);
	}

	
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	@Override
	public EnrollmentRenewalResponse processSingleAndRollOverRowRenewal(Integer enrollmentId, String EnrollmentStatus,
			List<EnrollmentRenewalDTO> enrollmentRenewalDTOList, AccountUser updatedBy, String renewalType)throws GIException {
		try{
			Enrollment enrollment = enrollmentRepository.findById(enrollmentId);
			if(enrollment != null){
				
				//Step1: Check if GI Enrollment is marked with "Renewal Flag"
				if((StringUtils.isEmpty(enrollment.getRenewalFlag()) || (StringUtils.isNotEmpty(enrollment.getRenewalFlag())
						&& !enrollment.getRenewalFlag().equalsIgnoreCase("Y"))) || renewalType.equals(RenewalType.ROLL_OVER.toString())){
					//Step2.0 Mark "Renewal Flag" with "yes"
					
					enrollment.setRenewalFlag("Y");
					enrollment.setUpdatedBy(updatedBy);
					enrollment = enrollmentRepository.save(enrollment);
				}
				
				/* Sort the BOB rows in chronological order
				starting with the oldest using Benefit
				Effective Date */
				Collections.sort(enrollmentRenewalDTOList, new Comparator<EnrollmentRenewalDTO>(){	 
		            @Override
		            public int compare(EnrollmentRenewalDTO o1, EnrollmentRenewalDTO o2) {
		                return (((Date)o1.getBenefitStartDate()).compareTo(((Date)o2.getBenefitStartDate())));
		            }
		        });
				
				//Step2.0.1: Add new entries into the Enrollment_Renewals table for each row in the BOB feed.
				createEnrollmentRenewal(enrollment, updatedBy, enrollmentRenewalDTOList);
				
				if(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentStatus)){
					//BOB & GI Status Match
					return EnrollmentRenewalResponse.BOB_NO_STATUS_UPDATE;
				}
				else{
					//BOB & GI Status Different
					return EnrollmentRenewalResponse.BOB_SUCCESS;
				}
			}
			else{
				throw new GIException("No Enrollment Record found with Enrollment Id: "+enrollmentId);
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception caught while processing multi line renewal",ex);
			throw new GIException(ex);
		}
	}
		
	/**
	 * 
	 * @param enrollment
	 * @param updatedBy
	 * @param enrollmentRenewalDTOList
	 */
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	private void createEnrollmentRenewal(Enrollment enrollment, AccountUser updatedBy, List<EnrollmentRenewalDTO> enrollmentRenewalDTOList){
		try{
			List<EnrollmentRenewals> enrollmentRenewalsList = new ArrayList<EnrollmentRenewals>();
			for(EnrollmentRenewalDTO enrollmentRenewalDTO : enrollmentRenewalDTOList){
				
				LocalDate localDate1 = LocalDate.parse( new SimpleDateFormat("yyyy-MM-dd").format(enrollmentRenewalDTO.getBenefitStartDate()) );
		        
		        LocalDate localDate1RangeFrom = localDate1.minus(CarrierFeedUtils.dateMatchingRange, ChronoUnit.DAYS);
				LocalDate localDate1RangeTo = localDate1.plus(CarrierFeedUtils.dateMatchingRange, ChronoUnit.DAYS);
				
				long isRecordsExists = iEnrollmentRenewalRepository.checkRenewalExists(enrollment.getId(), java.sql.Date.valueOf(localDate1RangeFrom),java.sql.Date.valueOf(localDate1RangeTo));

				if(isRecordsExists > 0){
					continue; // skip if effective date is already present in defined range
				}
				
				EnrollmentRenewals enrollmentRenewals = new EnrollmentRenewals();
				enrollmentRenewals.setEnrollmentId(enrollment.getId());
				enrollmentRenewals.setBenefitStartDate(enrollmentRenewalDTO.getBenefitStartDate());
				enrollmentRenewals.setBenefitEndDate(enrollmentRenewalDTO.getBenefitEndDate());
				enrollmentRenewals.setCreatedBy(updatedBy.getId());
				enrollmentRenewals.setLastUpdatedBy(updatedBy.getId());
				enrollmentRenewals.setPremium(enrollment.getGrossPremiumAmt());
				enrollmentRenewals.setPolicyId(enrollmentRenewalDTO.getPolicyId());
				enrollmentRenewals.setEnrollmentStatus(enrollmentRenewalDTO.getEnrollmentStatus());
				enrollmentRenewals.setProductType(enrollmentRenewalDTO.getProductType());
				
				enrollmentRenewalsList.add(enrollmentRenewals);
			}
			if(!enrollmentRenewalsList.isEmpty()){
				iEnrollmentRenewalRepository.save(enrollmentRenewalsList);
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred while saving Enrollment Renewal: ",ex);
		}
	}
	
	@Override
	public EnrlMissingCommissionResponse getMissingCommissions(EnrlMissingCommissionRequest request) throws GIException {
		EnrlMissingCommissionResponse response = new EnrlMissingCommissionResponse();
		EntityManager em = null;
		try{
			em = entityManagerFactory.createEntityManager();

			Calendar cal = TSCalendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, -90);
			
			Date eventDate = cal.getTime();
			
			String eventDateValue = new SimpleDateFormat("yyyy-MM-dd").format(eventDate);
			
			if(isNotNullAndEmpty(request.getTenantCode())){
				Tenant tenant = tenantRepository.findByCode(request.getTenantCode());
				request.setTenantId(tenant.getId());
			}
			
			if(null == request.getTenantId()) {
				request.setTenantId((TenantContextHolder.getTenant() != null) ? TenantContextHolder.getTenant().getId() : null);
			}
			if(!isNotNullAndEmpty(request.getOrderBy())) {
				request.setOrderBy(STR_EFFECTIVE_DATE);
			}
			if(!isNotNullAndEmpty(request.getPageSize())) {
				request.setPageSize(Integer.valueOf(EnrollmentPrivateConstants.TEN));
			}
			if(request.isExportQuery() == true){
			    request.setPageSize(Integer.valueOf(5000));
			}
			if(!isNotNullAndEmpty(request.getStartRow())) {
				request.setStartRow(Integer.valueOf(EnrollmentPrivateConstants.ZERO));
			}
			
			Map<Integer,String> lookupMap = new HashMap<Integer,String>();
			List<String> lookupString = new ArrayList<String>();
			lookupString.add("SUBSCRIBER");
			
			List<LookupValue> lookupValues = lookupService.getLookupValueListForBOBFeed("PERSON_TYPE", lookupString);
			
			String subscriberLookupId = String.valueOf(lookupValues.get(0).getLookupValueId());
			String enrollmentStatusAgingIds = "";
			String enrollmentStatusIds = "";
			
			lookupValues = lookupService.getLookupValueList("ENROLLMENT_STATUS");
			
			for(LookupValue lv:lookupValues) {
				lookupMap.put(lv.getLookupValueId(), lv.getLookupValueLabel());
				
				if(lv.getLookupValueCode().equals("INFORCE") || lv.getLookupValueCode().equals("TERM")) {
					if (AGING_COMMISSION.equalsIgnoreCase(request.getExceptionType()) && lv.getLookupValueCode().equals("INFORCE")){
						enrollmentStatusAgingIds = String.valueOf(lv.getLookupValueId());
					}else {
						enrollmentStatusIds = enrollmentStatusIds + lv.getLookupValueId() + ",";
					}
				}
			}
			
			if (AGING_COMMISSION.equalsIgnoreCase(request.getExceptionType())){
				enrollmentStatusIds = enrollmentStatusAgingIds;
			}else {
				enrollmentStatusIds = enrollmentStatusIds.substring(0, enrollmentStatusIds.length()-1);
			}
			
			
			lookupValues = lookupService.getLookupValueList("INSURANCE_TYPE");
			String insuranceTypeIds = "";
			for(LookupValue lv:lookupValues) {
				lookupMap.put(lv.getLookupValueId(), lv.getLookupValueLabel());
				
				if("MEDICARE".equals(request.getInsuranceType())){
					if(lv.getLookupValueCode().startsWith("MC_") || lv.getLookupValueCode().equals("MEDICARE")) {
						insuranceTypeIds = insuranceTypeIds + lv.getLookupValueId() + ",";
					}
				}else if(lv.getLookupValueCode().equals(request.getInsuranceType())) {
					insuranceTypeIds = String.valueOf(lv.getLookupValueId());
				}
			}
			
			if(insuranceTypeIds.contains(",")) {
				insuranceTypeIds = insuranceTypeIds.substring(0, insuranceTypeIds.length()-1);
			}

			
			String query = buildMissingCommissionNativeQuery(request, subscriberLookupId, enrollmentStatusIds, insuranceTypeIds, eventDateValue, Boolean.FALSE);
			Query hqlQuery = em.createNativeQuery(query);
			
			hqlQuery.setFirstResult(request.getStartRow());
			hqlQuery.setMaxResults(request.getPageSize());
			
			List<Object[]> resultList = hqlQuery.getResultList();

			response.setMissingCommissionDTOList(getMissingCommissionDtos(resultList, request.getExceptionType(), lookupMap, request.isExportQuery()));
			
			if(!request.isExportQuery()){
				query = buildMissingCommissionNativeQuery(request, subscriberLookupId, enrollmentStatusIds, insuranceTypeIds, eventDateValue, Boolean.TRUE);
				Query hqlCountQuery = em.createNativeQuery(query);
				response.setRecordCount(((BigInteger)hqlCountQuery.getSingleResult()).longValue());
			}
		}catch (Exception ex){
			throw new GIException(ex.getMessage(), ex);
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		return response;
	}
	
/*private String buildMissingCommissionQuery(EnrlMissingCommissionRequest request, Boolean isCountQuery){
		
		StringBuilder queryStr = new StringBuilder();
		queryStr.append(" SELECT ");
		if (isCountQuery){
			queryStr.append(" COUNT(*) ");
		}else{
			queryStr.append("  e.id, e.issuerAssignPolicyNo, concat(concat(ee.firstName, ' '), ee.lastName), e.insurerName, to_char(e.submittedToCarrierDate, 'MM/dd/yyyy'), "
					+ " to_char(e.benefitEffectiveDate, 'MM/dd/yyyy'), e.insuranceTypeLkp.lookupValueLabel, e.enrollmentStatusLkp.lookupValueLabel, e.capAgentId " );
			if(request.isExportQuery()) {
				queryStr.append(" , e.carrierAppId, e.planName, ee.primaryPhoneNo, to_char(ee.birthDate, 'MM/dd/yyyy'), hd.zip ");
			}
		}
		queryStr.append(" FROM Enrollment e, Enrollee ee ");
		if(request.isExportQuery()){
			queryStr.append(" left outer join ee.homeAddressid as hd ");
		}
		queryStr.append(" WHERE ");
		queryStr.append(" e.tenantId = :tenantId and e.id = ee.enrollment.id ");
		 if (AGING_COMMISSION.equalsIgnoreCase(request.getExceptionType()))
        {
                queryStr.append(" and e.enrollmentStatusLkp.lookupValueCode = 'INFORCE' ");
        }
        else
        {
                queryStr.append(" and e.enrollmentStatusLkp.lookupValueCode IN ('INFORCE', 'TERM') ");
        }
		queryStr.append(" and ee.personTypeLkp.lookupValueCode = 'SUBSCRIBER' ");

		if(StringUtils.isNotBlank(request.getEnrollmentId()) && !request.getEnrollmentId().equalsIgnoreCase("ALL")) {
			queryStr.append(" and e.id = " + request.getEnrollmentId().trim());
		}
		if(StringUtils.isNotBlank(request.getCarrierName()) && !request.getCarrierName().equalsIgnoreCase("ALL")) {
			queryStr.append(" e.insurerName like '%"+request.getCarrierName()+"%' ");
		}
		if(StringUtils.isNotBlank(request.getPolicyId()) && !request.getPolicyId().equalsIgnoreCase("ALL")) {
			queryStr.append(" e.insurerName like '%"+request.getCarrierName()+"%' ");
		}
		
		queryStr.append(" and ( :policyId = 'ALL' or TRIM(LEADING '0' FROM e.issuerAssignPolicyNo) = TRIM(LEADING '0' FROM :policyId) ) ");
		queryStr.append(" and ( :insuranceTypeALL = 'ALL' or e.insuranceTypeLkp.lookupValueCode in (:insuranceType)) ");
		queryStr.append(" and ( :effectiveDate = 'ALL' or to_char(e.benefitEffectiveDate, 'MM/YYYY') = :effectiveDate) ");
		queryStr.append(" and ( :submittedDate = 'ALL' or to_char(e.submittedToCarrierDate, 'MM/YYYY') = :submittedDate) ");
		
		if (AGING_COMMISSION.equalsIgnoreCase(request.getExceptionType())){
			queryStr.append(" and exists ( select ec.enrollmentId from EnrollmentCommission ec where ec.enrollmentId = e.id and ");
			queryStr.append(" :eventDate >= (select max(ec1.commissionDate) from EnrollmentCommission ec1 where ec1.enrollmentId = ec.enrollmentId group by ec1.enrollmentId) )");
		}else{
			queryStr.append(" and e.benefitEffectiveDate <= :eventDate ");
			queryStr.append(" and not exists ( select enrollmentId from EnrollmentCommission ec where ec.enrollmentId = e.id) ");
		}
		
		if (isNotNullAndEmpty(request.getOrderBy()) && !isCountQuery){
			queryStr.append(" ORDER BY ");
			String orderBy = (request.getOrderBy() == null) ? "" : request.getOrderBy();
			
			if(orderBy.equalsIgnoreCase("policyId")){
				queryStr.append(" e.issuerAssignPolicyNo ");
			}else if(orderBy.equalsIgnoreCase("carrierName")){
				queryStr.append(" e.insurerName ");
			}else if(orderBy.equalsIgnoreCase("enrollmentId")){
				queryStr.append(" e.id ");
			}else if(orderBy.equalsIgnoreCase("effectiveDate")){
				queryStr.append(" e.benefitEffectiveDate ");
			}else if(orderBy.equalsIgnoreCase("submittedDate")){
				queryStr.append(" e.submittedToCarrierDate ");
			}else if(orderBy.equalsIgnoreCase("name")){
				queryStr.append(" ee.firstName ");
				if (request.getSortDir()){
					queryStr.append(" desc ");
				}
				queryStr.append(" , ee.lastName ");
			}else if(orderBy.equalsIgnoreCase("insuranceType")){
				queryStr.append(" e.insuranceTypeLkp.lookupValueLabel ");
			}else if(orderBy.equalsIgnoreCase("enrollmentStatus")){
				queryStr.append(" e.enrollmentStatusLkp.lookupValueLabel ");
			}else{
				queryStr.append(" e.benefitEffectiveDate ");
			}
			if (request.getSortDir()){
				queryStr.append(" desc ");
			}
			if(!orderBy.equalsIgnoreCase("enrollmentId")){
				queryStr.append(" , e.id desc ");
			}
		}
		return queryStr.toString();
	}
*/
private String buildMissingCommissionNativeQuery(EnrlMissingCommissionRequest request, String subscriberLookupId, String enrollmentStatusIds, String insuranceTypeIds, String eventDateValue, Boolean isCountQuery){
	
	StringBuilder queryStr = new StringBuilder();
	queryStr.append(" SELECT ");
	if (isCountQuery){
		queryStr.append(" COUNT(*) ");
	}else{
		queryStr.append("  en.id, en.ISSUER_ASSIGN_POLICY_NO,((el.FIRST_NAME  ||' ')  || el.LAST_NAME), en.INSURER_NAME,  TO_CHAR(en.SUBMITTED_TO_CARRIER_DATE, 'MM/dd/yyyy') as carrierName, "
				+ " TO_CHAR(en.BENEFIT_EFFECTIVE_DATE, 'MM/dd/yyyy') as effDate, en.INSURANCE_TYPE_LKP, en.ENROLLMENT_STATUS_LKP, en.CAP_AGENT_ID " );
		if(request.isExportQuery()) {
			queryStr.append(" , en.CARRIER_APP_ID, en.PLAN_NAME, el.PRIMARY_PHONE_NO, to_char(el.BIRTH_DATE, 'MM/dd/yyyy'), hl.zip ");
		}
	}
	queryStr.append(" FROM Enrollment en INNER JOIN ENROLLEE el ON el.ENROLLMENT_ID = en.id  ");
	if(request.isExportQuery()){
		queryStr.append(" LEFT JOIN locations hl ON hl.id = el.HOME_ADDRESS_ID ");
	}
	queryStr.append(" WHERE en.TENANT_ID = " + request.getTenantId());
	
	if (AGING_COMMISSION.equalsIgnoreCase(request.getExceptionType())){
            queryStr.append(" and en.ENROLLMENT_STATUS_LKP = " + enrollmentStatusIds ); 
    }else{
            queryStr.append(" and en.ENROLLMENT_STATUS_LKP IN ("+enrollmentStatusIds+") "); 
    }
	queryStr.append(" and el.PERSON_TYPE_LKP = " + subscriberLookupId );

	if(StringUtils.isNotBlank(request.getEnrollmentId()) && !request.getEnrollmentId().equalsIgnoreCase("ALL")) {
		queryStr.append(" and en.id = " + request.getEnrollmentId().trim());
	}
	if(StringUtils.isNotBlank(request.getCarrierName()) && !request.getCarrierName().equalsIgnoreCase("ALL")) {
		queryStr.append(" and en.INSURER_NAME like '%"+request.getCarrierName()+"%' ");
	}
	if(StringUtils.isNotBlank(request.getPolicyId()) && !request.getPolicyId().equalsIgnoreCase("ALL")) {
		queryStr.append(" and en.ISSUER_ASSIGN_POLICY_NO = '"+request.getPolicyId()+"' ");
	}
	if(StringUtils.isNotBlank(request.getInsuranceType()) && !request.getInsuranceType().equalsIgnoreCase("ALL")) {
		queryStr.append(" and en.INSURANCE_TYPE_LKP IN ("+insuranceTypeIds+") "); 
	}
	
	if(StringUtils.isNotBlank(request.getEffectiveDate()) && !request.getEffectiveDate().equalsIgnoreCase("ALL")) {
		queryStr.append(" and TO_CHAR(en.BENEFIT_EFFECTIVE_DATE, 'MM/YYYY') IN ('"+request.getEffectiveDate()+"') "); 
	}
	
	if(StringUtils.isNotBlank(request.getSubmittedDate()) && !request.getSubmittedDate().equalsIgnoreCase("ALL")) {
		queryStr.append(" and TO_CHAR(en.SUBMITTED_TO_CARRIER_DATE, 'MM/YYYY') IN ('"+request.getSubmittedDate()+"') "); 
	}

	if (AGING_COMMISSION.equalsIgnoreCase(request.getExceptionType())){
		queryStr.append(" and EXISTS ( SELECT ec.ENROLLMENT_ID FROM ENROLLMENT_COMMISSION ec WHERE ec.ENROLLMENT_ID=en.id and ");
		queryStr.append(" TO_DATE('"+eventDateValue+"','YYYY-MM-DD') >= (select max(ec1.COMMISSION_DATE) from ENROLLMENT_COMMISSION ec1 where ec1.ENROLLMENT_ID = ec.ENROLLMENT_ID group by ec1.ENROLLMENT_ID) )");
	}else{
		queryStr.append(" AND en.BENEFIT_EFFECTIVE_DATE <= TO_DATE('"+eventDateValue+"','YYYY-MM-DD') ");
		queryStr.append(" AND NOT (EXISTS (SELECT ec.ENROLLMENT_ID FROM ENROLLMENT_COMMISSION ec WHERE ec.ENROLLMENT_ID=en.id  ))");
	}
	
	if (isNotNullAndEmpty(request.getOrderBy()) && !isCountQuery){
		queryStr.append(" ORDER BY ");
		String orderBy = (request.getOrderBy() == null) ? "" : request.getOrderBy();
		
		if(orderBy.equalsIgnoreCase("policyId")){
			queryStr.append(" en.ISSUER_ASSIGN_POLICY_NO ");
		}else if(orderBy.equalsIgnoreCase("carrierName")){
			queryStr.append(" en.INSURER_NAME ");
		}else if(orderBy.equalsIgnoreCase("enrollmentId")){
			queryStr.append(" en.id ");
		}else if(orderBy.equalsIgnoreCase("effectiveDate")){
			queryStr.append(" en.BENEFIT_EFFECTIVE_DATE ");
		}else if(orderBy.equalsIgnoreCase("submittedDate")){
			queryStr.append(" en.SUBMITTED_TO_CARRIER_DATE ");
		}else if(orderBy.equalsIgnoreCase("name")){
			queryStr.append(" el.FIRST_NAME ");
			if (request.getSortDir()){
				queryStr.append(" desc ");
			}
			queryStr.append(" , el.LAST_NAME ");
		}/*else if(orderBy.equalsIgnoreCase("insuranceType")){
			queryStr.append(" e.insuranceTypeLkp.lookupValueLabel ");
		}else if(orderBy.equalsIgnoreCase("enrollmentStatus")){
			queryStr.append(" e.enrollmentStatusLkp.lookupValueLabel ");
		}*/else{
			queryStr.append(" en.BENEFIT_EFFECTIVE_DATE ");
		}
		if (request.getSortDir()){
			queryStr.append(" desc ");
		}
		if(!orderBy.equalsIgnoreCase("enrollmentId")){
			queryStr.append(" , en.id desc ");
		}
	}
	return queryStr.toString();
}
	
	
	private List<EnrlMissingCommissionDTO> getMissingCommissionDtos(List<Object[]> resultList, String exceptionType, Map<Integer,String> lookupMap, boolean isExportQuery){
		List<EnrlMissingCommissionDTO> missingCommissionList = new ArrayList<EnrlMissingCommissionDTO>();
		for (Object[] value: resultList){	
			EnrlMissingCommissionDTO missingCommissionDto = new EnrlMissingCommissionDTO();
			missingCommissionDto.setEnrollmentId(value[0] != null ? value[0]+"" : "");
			missingCommissionDto.setPolicyId(value[1] != null ? value[1]+"" : "" );
			missingCommissionDto.setName(value[2] != null ? value[2]+"" : "");
			missingCommissionDto.setCarrierName(value[3] != null ? value[3]+"" : "");
			missingCommissionDto.setSubmittedDate(value[4] != null ? value[4]+"" : "");
			missingCommissionDto.setEffectiveDate(value[5] != null ? value[5]+"" : "");
			if(value[6] != null){
				Integer insuranceId = ((BigDecimal) value[6]).intValue();
				missingCommissionDto.setInsuranceType(lookupMap.get(insuranceId));
			}
			
			if(value[7] != null){
				Integer enStatusId = ((BigDecimal) value[7]).intValue();
				missingCommissionDto.setEnrollmentStatus(lookupMap.get(enStatusId));
			}

			if(value[8] != null){
				Integer capAgentId = ((BigDecimal) value[8]).intValue();
				this.getCapAgentDetails(capAgentId, missingCommissionDto );
			}else{
				missingCommissionDto.setAgentName("");
				missingCommissionDto.setUserNPN("");
			}
			missingCommissionDto.setExceptionType(exceptionType);
			if (isExportQuery){
				missingCommissionDto.setCarrierAppId(value[9] != null ? value[9]+"" : "");
				missingCommissionDto.setPlanName(value[10] != null ? value[10]+"" : "");
				missingCommissionDto.setPrimaryPhoneNo(value[11] != null ? value[11]+"" : "");
				missingCommissionDto.setBirthDate(value[12] != null ? value[12]+"" : "");
				missingCommissionDto.setZipcode(value[13] != null ? value[13]+"" : "");
			}
			missingCommissionList.add(missingCommissionDto);
		}
		return missingCommissionList;
	}
	
	private void getCapAgentDetails(Integer capAgentId, EnrlMissingCommissionDTO missingCommissionDto ){
		AccountUser user = userService.findById(capAgentId);
		String agentName = user.getFirstName() + " " + user.getLastName();
		String userNpn = user.getUserNPN();
		missingCommissionDto.setAgentName(agentName);
		missingCommissionDto.setUserNPN(userNpn);
	}
	
	@Override
	public void updateEnrollmentForFeed(EnrollmentUpdateDTO enrollmentUpdateDTO) throws GIException {
		
		Enrollment enrollment = findById(enrollmentUpdateDTO.getEnrollmentId());

		if(enrollmentUpdateDTO.getBenefitEffectiveDate() != null){
			enrollment.setBenefitEffectiveDate(enrollmentUpdateDTO.getBenefitEffectiveDate());
		}
		
		if(StringUtils.isNotEmpty(enrollmentUpdateDTO.getVerificationEvent())){
			enrollment.setVerificationEvent(enrollmentUpdateDTO.getVerificationEvent());
		}
		
		enrollment.setUpdatedBy(STARRModule_defaultUser);
		
		//Saving Updated Enrollment
		enrollmentRepository.saveAndFlush(enrollment);
	}

	@Override
	@Transactional(readOnly = true)
	public Integer getEnrollmentStatusNSubmittedInfoById(Integer enrollmentId) {
		return enrollmentRepository.getEnrollmentStatusNSubmittedInfoById(enrollmentId);
	}
	
	/**
	 * Set Agent Details to enrollment in submitted  by field
	 * 
	 * @param enrollment
	 */
	private void setEnrollmentSubmittedBy(Enrollment enrollment) {
		if (enrollment.getId() != null 
				&& enrollment.getEnrollmentStatusLkp() != null 
				&& enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)) {

			/* If Enrollment Already exists and New status is Pending and no previous Submitted_by Present*/

			Integer submittedBy = getEnrollmentStatusNSubmittedInfoById(enrollment.getId());
			if (submittedBy == null) {
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("Before setting Submitted_by for enrollment :" + enrollment.getId());
				}
				enrollment.setSubmittedBy(getLoggedInUser());
				enrollment.setCarrierResendFlag(EnrollmentPrivateConstants.RESEND_FLAG);
			}
		} else if (enrollment.getEnrollmentStatusLkp() != null 
				&& enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)) {

			/* New Enrollment with new status as Pending so setting the Submitted_by*/

			enrollment.setSubmittedBy(getLoggedInUser());
			enrollment.setCarrierResendFlag(EnrollmentPrivateConstants.RESEND_FLAG);
		}
	}

	@Override
	public EnrollmentRenewals findByEnrollmentIdWithLatestBenefitStartDate(Integer enrollmentId) {
		Page<EnrollmentRenewals> enrollmentRenewalsPage = null;
		  enrollmentRenewalsPage = iEnrollmentRenewalRepository.findByEnrollmentIdWithLatestBenefitStartDate(enrollmentId,new   PageRequest(0, 1));
		  
		  if(enrollmentRenewalsPage != null && enrollmentRenewalsPage.getContent().size() > 0){
			return enrollmentRenewalsPage.getContent().get(0);
		  }
		 
		return null;
	}

	@Override
	public List<EnrolleeDTO> getEnrolleeAtributeById(EnrollmentRequest enrollmentRequest) throws GIException{

		QueryBuilder<Enrollment> applicationQuery = delegateFactory.getObject();
		applicationQuery.buildSelectQuery(Enrollee.class);
		
		applicationQuery.applySelectColumns( enrollmentRequest.getEnrolleeAttributeList()
															  .stream()
															  .map(a -> a.toString())
														      .collect(Collectors.toList()));
		
		applicationQuery.applyWhere("enrollment.id", enrollmentRequest.getEnrollmentId(), DataType.NUMERIC, ComparisonType.EQ);
		List<Map<String, Object>> enrolleeRow = applicationQuery.getData(EnrollmentConstants.DEFAULT_START_RECORD, GhixConstants.PAGE_SIZE);
		List<EnrolleeDTO> enrolleeDTOList = new ArrayList<>();
		EnrolleeDTO enrolleeDTO = null;
		Date tempDate = null;
		
		for (Map<String, Object> map : enrolleeRow) {
			enrolleeDTO = new EnrolleeDTO();
			String fieldType = null;
			
			for (Map.Entry<String, Object> coloumName_Data : map.entrySet()) {
				
				try {
					
					fieldType = getFieldType(coloumName_Data.getKey());
					
					if(fieldType.equalsIgnoreCase("integerClass")){
						
						Method setter=EnrolleeDTO.class.getMethod(getSetterName(coloumName_Data.getKey()), Integer.class);
						setter.invoke(enrolleeDTO, coloumName_Data.getValue());
						
					}else if(fieldType.equalsIgnoreCase("intClass")){
						
						Method setter=EnrolleeDTO.class.getMethod(getSetterName(coloumName_Data.getKey()), int.class);
						setter.invoke(enrolleeDTO, coloumName_Data.getValue());
						
					}else if(fieldType.equalsIgnoreCase("StringClass")){
						
						Method setter=EnrolleeDTO.class.getMethod(getSetterName(coloumName_Data.getKey()), String.class);
						setter.invoke(enrolleeDTO, coloumName_Data.getValue());
						
					}else if(fieldType.equalsIgnoreCase("DateClass")){
						tempDate = (Date)coloumName_Data.getValue();
						
						Method setter=EnrolleeDTO.class.getMethod(getSetterName(coloumName_Data.getKey()), String.class);
						setter.invoke(enrolleeDTO, DateUtil.dateToString(tempDate, GhixConstants.REQUIRED_DATE_FORMAT));
					}
					
				} catch (Exception  ex) {
					throw new GIException(ex);
				}
			}
			
			enrolleeDTOList.add(enrolleeDTO);
		}
		
		return enrolleeDTOList;
	}
	
	@Override
	public Set<Integer> setMatchedIssuerIds(Set<Integer> issuerIds, String carrierName){
		Set<Integer> matchedIssuerIds = new HashSet<Integer>();
		try{
			IssuerBrandNameResponseDTO issuerBrandNameResponseDTO = enrollmentExternalRestUtil.getIssuerBrandNameByIssuerIdList(Lists.newArrayList(issuerIds));
			if(issuerBrandNameResponseDTO != null && issuerBrandNameResponseDTO.getIssuerBrandNameDtoList() != null){
				List<IssuerBrandNameDTO> issuerBrandNameDtoList = issuerBrandNameResponseDTO.getIssuerBrandNameDtoList();
				for (Integer issuerId : issuerIds) {
					if(isMatchExistsWithIssueridAndName(issuerId, carrierName, issuerBrandNameDtoList))
					{ 
						matchedIssuerIds.add(issuerId);
					}
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception caught while matching brandname", ex);
		}
		return matchedIssuerIds;
	}
	
	private String getSetterName(String key){
		Map<String, String> methodMap = new HashMap<>();
		methodMap.put("id", "setId");
		methodMap.put("enrollmentid", "setEnrollmentId");
		methodMap.put("enrolleeLkpValuelookupValueCode", "setEnrolleeStatusCode");
		methodMap.put("firstName", "setFirstName");
		methodMap.put("lastName", "setLastName");
		methodMap.put("homeAddressidstate", "setHomeAddressState");
		methodMap.put("primaryPhoneNo", "setPrimaryPhoneNo");
		methodMap.put("personTypeLkplookupValueLabel", "setPersonTypeLabel");
		methodMap.put("createdOn", "setCreatedDate");
		
		return methodMap.get(key);
	}
	
	private String getFieldType(String key){
		List<String> integerPropertyList = Arrays.asList("enrollmentid");
		List<String> stringPropertyList = Arrays.asList("firstName","lastName","homeAddressidstate","primaryPhoneNo",
				                                        "enrolleeLkpValuelookupValueCode","personTypeLkplookupValueLabel");
		List<String> datePropertyList = Arrays.asList("createdOn");
		List<String> intPropertyList = Arrays.asList("id");
		
		if(integerPropertyList.contains(key)){
			return "integerClass";
			
		}else if(intPropertyList.contains(key)){
			return "intClass";
			
		}else if(stringPropertyList.contains(key)){
			return "StringClass";
			
		}else if(datePropertyList.contains(key)){
			return "DateClass";
		}

		return null;
	}
	
	private LookupValue getInsuranceTypeLkpFromPlanResponse(String insuranceType){
		LookupValue insuranceTypeLkp = null;
		if(StringUtils.isNotBlank(insuranceType)){
			
			if(insuranceType.equalsIgnoreCase("HEALTH")){
				insuranceTypeLkp = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE_HEALTH);
			}
			else if(insuranceType.equalsIgnoreCase("AME")){
				insuranceTypeLkp = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentPrivateConstants.AME_LOOKUP_VALUE_CODE);
			}
			else if(insuranceType.equalsIgnoreCase("LIFE")){
				insuranceTypeLkp = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE_LIFE_CODE);
			}
			else if(insuranceType.equalsIgnoreCase("VISION")){
				insuranceTypeLkp = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE_VISION_CODE);
			}
			else if(insuranceType.equalsIgnoreCase("DENTAL")){
				insuranceTypeLkp = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE_DENTAL_CODE);
			}
			else if(insuranceType.equalsIgnoreCase("STM")){
				insuranceTypeLkp = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentPrivateConstants.STM_LOOKUP_VALUE_CODE);
			}		
		}
		return insuranceTypeLkp;
	}
	
	private void setEnrollmentCommissionData(Enrollment enrollment, PlanResponse planResponse) {
		if(null != planResponse) {
			EnrollmentIssuerCommision enrlCommission = new EnrollmentIssuerCommision();
			enrlCommission.setInitialCommDollarAmt(planResponse.getFirstYearCommDollarAmt());
			enrlCommission.setInitialCommissionPercent(planResponse.getFirstYearCommPercentageAmt());
			enrlCommission.setRecurringCommDollarAmt(planResponse.getSecondYearCommDollarAmt());
			enrlCommission.setRecurringCommissionPercent(planResponse.getSecondYearCommPercentageAmt());
			enrlCommission.setNonCommissionFlag(planResponse.getNonCommissionFlag());
			if(!EnrollmentConstants.Y.equalsIgnoreCase(enrlCommission.getNonCommissionFlag())) {
				if(null != enrlCommission.getInitialCommDollarAmt()) {
					enrlCommission.setInitialCommissionAmt(enrlCommission.getInitialCommDollarAmt());
				}else if(null != enrlCommission.getInitialCommissionPercent() && null != enrollment.getGrossPremiumAmt()) {
					enrlCommission.setInitialCommissionAmt(EnrollmentUtils.round(enrlCommission.getInitialCommissionPercent() * enrollment.getGrossPremiumAmt(), 2));
				}
				if(null != enrlCommission.getRecurringCommDollarAmt()) {
					enrlCommission.setRecurringCommissionAmt(enrlCommission.getRecurringCommDollarAmt());
				}else if(null != enrlCommission.getRecurringCommissionPercent() && null != enrollment.getGrossPremiumAmt()) {
					enrlCommission.setRecurringCommissionAmt(EnrollmentUtils.round(enrlCommission.getRecurringCommissionPercent() * enrollment.getGrossPremiumAmt(), 2));
				}
			}else {
				enrlCommission.setInitialCommissionAmt(0d);
				enrlCommission.setRecurringCommissionAmt(0d);
			}
			enrlCommission.setEnrollment(enrollment);
			enrollment.setEnrollmentIssuerCommision(enrlCommission);
		}
	}
	
	private List<EnrollmentBobDTO> matchWithIssuerBrandName(List<EnrollmentBobDTO> enrollmentBobDTOList, String carrierName)
	{
			List<EnrollmentBobDTO> matchedEnrollmentBobDTOList = new ArrayList<EnrollmentBobDTO>();
			
	        Set<Integer> issuerIdSet = new HashSet<Integer>();
	        for(EnrollmentBobDTO enrollmentBobDTO : enrollmentBobDTOList){
	                issuerIdSet.add(enrollmentBobDTO.getIssuerId());
	        }
	        if(issuerIdSet != null  && !issuerIdSet.isEmpty()){
	                try{
	                        IssuerBrandNameResponseDTO issuerBrandNameResponseDTO = enrollmentExternalRestUtil
	                                        .getIssuerBrandNameByIssuerIdList(issuerIdSet.stream().collect(Collectors.toList()));
	                        if(issuerBrandNameResponseDTO != null && issuerBrandNameResponseDTO.getIssuerBrandNameDtoList() != null){
	                                List<IssuerBrandNameDTO> issuerBrandNameDtoList = issuerBrandNameResponseDTO.getIssuerBrandNameDtoList();
	                                for(EnrollmentBobDTO enrollmentBobDTO : enrollmentBobDTOList){
	                                        if(isMatchExistsWithIssueridAndName(enrollmentBobDTO.getIssuerId(), carrierName, issuerBrandNameDtoList))
	                                        { 
	                                        	matchedEnrollmentBobDTOList.add(enrollmentBobDTO);
	                                        }
	                                }
	                        }
	                }
	                catch(Exception ex){
	                		LOGGER.error("Exception caught while matching brandname", ex);
		                        }
		                }
	              return matchedEnrollmentBobDTOList;
	  }
	  
	  private boolean isMatchExistsWithIssueridAndName(final int issuerId, final String carrierName, List<IssuerBrandNameDTO> issuerBrandNameDtoList){
	      return (boolean) CollectionUtils.exists(issuerBrandNameDtoList, new Predicate() {
	          public boolean evaluate(Object arg0) {
	              return ((IssuerBrandNameDTO) arg0).getIssuerId()==issuerId &&
	                          ((IssuerBrandNameDTO) arg0).getIssuerBrandName().contains(carrierName);
	          }
	      });
	 }
	  
	 @Override
	 public Map<Integer,String> getLookupValueIdsByNames(String lookupTypeName, List<String> lookupValueCodes){
		 
		 Map<Integer,String> lookupValueDetails = new HashMap<Integer,String>();
		 
		 List<LookupValue> statusLookupList = lookupService.getLookupValueListForBOBFeed(lookupTypeName, lookupValueCodes);
		 
		 for(LookupValue lookupValue:statusLookupList){
			 lookupValueDetails.put(lookupValue.getLookupValueId(), lookupValue.getLookupValueCode());
		 }
		 
		 return lookupValueDetails;
	 }

	@Override
	public void updateEnrollmentRenewalsForFeed(List<EnrollmentRenewalDTO> enrollmentRenewalDTOs,
			Integer enrollmentId) {
		String enRenewalFlag = ienrollmentRepository.isRenewalFlagSet(enrollmentId);
		
		if(enRenewalFlag == null || !enRenewalFlag.equals("Y")){
			ienrollmentRepository.updateEnrollmentisRenewalFlag(enrollmentId, "Y");
		}
		
		List<EnrollmentRenewals> enrollmentRenewals = new ArrayList<EnrollmentRenewals>();
		EnrollmentRenewals dbRenewal = null;
		for(EnrollmentRenewalDTO renewalDTO: enrollmentRenewalDTOs){
			if(renewalDTO.getId() > 0){
				dbRenewal = iEnrollmentRenewalRepository.findOne(renewalDTO.getId());
			}else{
				dbRenewal = new EnrollmentRenewals();
			}
			
			CarrierFeedUtils.copyNonNullProperties(renewalDTO, dbRenewal);
			enrollmentRenewals.add(dbRenewal);
		}
		iEnrollmentRenewalRepository.save(enrollmentRenewals);
	}
}
