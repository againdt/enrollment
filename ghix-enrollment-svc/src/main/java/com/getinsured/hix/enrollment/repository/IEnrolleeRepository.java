/**
 * 
 */
package com.getinsured.hix.enrollment.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.QhpReportDTO;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepository;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

/**
 * @author panda_p
 * 
 */
public interface IEnrolleeRepository extends TenantAwareRepository<Enrollee, Integer>,
		EnversRevisionRepository<Enrollee, Integer, Integer> {

	Enrollee findById(int enrolleeId);

	@Query("FROM Enrollee as en WHERE en.updatedOn >=  :lastRunDate and en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>'PENDING'")
	List<Enrollee> getCarrierUpdatedEnrollments(
			@Param("lastRunDate") Date lastRunDate,
			@Param("updatedBy") Integer updatedBy);

	@Query("FROM Enrollee as en WHERE en.updatedBy.id = :updatedBy and en.enrolleeLkpValue.lookupValueCode <>'PENDING'")
	List<Enrollee> getCarrierUpdatedEnrollments(
			@Param("updatedBy") Integer updatedBy);

	@Query("FROM Enrollee as en WHERE en.exchgIndivIdentifier = :memberID and en.enrollment.id = :enrollmentID and en.enrollment.planId = :planID")
	List<Enrollee> findEnrolleeByEnrollmentIDAndMemberIDAndPlanID(
			@Param("memberID") String memberID,
			@Param("enrollmentID") Integer enrollmentID,
			@Param("planID") Integer planID);

	/*@Query("FROM Enrollee as en WHERE en.exchgIndivIdentifier = :memberID and en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'") */
	@Query("FROM Enrollee as en WHERE en.exchgIndivIdentifier = :memberID"
			+ " and en.enrollment.id = :enrollmentID"
			+ " and en.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') ORDER BY en.createdOn DESC")
	List<Enrollee> findEnrolleeByEnrollmentIDAndMemberID(
			@Param("memberID") String memberID,
			@Param("enrollmentID") Integer enrollmentID);

	@Query("FROM Enrollee as en WHERE en.exchgIndivIdentifier = :exchgIndivIdentifier")
	Enrollee getEnrolleeByExchgIndivIdentifier(
			@Param("exchgIndivIdentifier") String exchgIndivIdentifier);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID "
			+ " AND en.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') "
			+ " AND en.enrolleeLkpValue.lookupValueCode = :status")
	List<Enrollee> getEnrolleeByStatusAndEnrollmentID(@Param("enrollmentID") Integer enrollmentID, @Param("status") String status);
	
/*	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' and en.enrolleeLkpValue.lookupValueCode IN ('CANCEL','TERM')")
	List<Enrollee> getInActiveEnrollees(@Param("enrollmentID") Integer enrollmentID);*/
	
	@Query(" FROM Enrollee as en " +
			" WHERE en.enrollment.id = :enrollmentID " +
			" AND en.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') ORDER BY en.effectiveStartDate, en.exchgIndivIdentifier, en.createdOn DESC")
	List<Enrollee> getEnrolleeByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query(" SELECT en.id, en.exchgIndivIdentifier, en.effectiveStartDate, en.quotingDate, en.age " +
			" FROM Enrollee as en " +
			" WHERE en.enrollment.id = :enrollmentID " +
			" AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
			" AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT'  " +
			" AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'")
	List<Object[]> getExchgIndivIdentifierAndEffectiveStartDateByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +
			" AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
			" AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
			" AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' " +
			" AND en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	List<Enrollee> getEnrolledEnrolleesForEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	
	@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +
			" AND en.personTypeLkp.lookupValueCode IN('ENROLLEE', 'SUBSCRIBER') " +
			" AND (en.enrolleeLkpValue.lookupValueCode IN ('PENDING', 'CONFIRM', 'PAYMENT_RECEIVED')  "+
					" OR (en.enrolleeLkpValue.lookupValueCode IN ('TERM') AND en.effectiveEndDate > :date )) ")
	List<Enrollee> getActiveEnrolleesForEnrollmentID(@Param("enrollmentID") Integer enrollmentID,@Param("date") Date date);
	
	/*@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +
			" AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
			" AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
			" AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' " +
			" AND (en.createdOn >= :lastRunDate or en.updatedOn >=  :lastRunDate)")
	List<Enrollee> getEnrolleeByLastRunDate(@Param("enrollmentID") Integer enrollmentID, @Param("lastRunDate") Date lastRunDate);*/
	
	@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +
			" AND en.personTypeLkp.lookupValueCode = 'RESPONSIBLE_PERSON'")
	Enrollee getResponsiblePersonByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	

	@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +
			" AND en.personTypeLkp.lookupValueCode = 'HOUSEHOLD_CONTACT'")
	Enrollee getHouseholdContactByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query(" FROM Enrollee as en WHERE en.exchgIndivIdentifier = :memberID " +
			" AND en.enrollment.id = :enrollmentId " +
			" AND en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') " +
			" AND en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT')")
	Enrollee findEnrolleeByEnrollmentIdAndMemberId(@Param("memberID") String memberID,@Param("enrollmentId") Integer enrollmentId);
	
/*	@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentId " +
			" AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
			" AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
			" AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' " +
			" AND (en.enrolleeLkpValue.lookupValueCode = 'PENDING' or en.enrolleeLkpValue.lookupValueCode='CONFIRM') ")
	List<Enrollee> getPenddingOrEnrollEnrollee(@Param("enrollmentId") Integer enrollmentId);*/

	@Query(" SELECT en.enrollment FROM Enrollee as en  " +
			" WHERE en.taxIdNumber=:ssn and en.enrollment.benefitEffectiveDate >= :coverageStartDate " +
			" and en.enrollment.benefitEndDate <= :coverageEndDate " +
			" and en.enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') " +
			" and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " +
			" and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
	List<Enrollment> findActiveEnrollmentBySSNAndCoverageDates(@Param("ssn")String ssn,@Param("coverageStartDate") Date coverageStartDate,@Param("coverageEndDate") Date coverageEndDate);
	
	@Query("SELECT en.enrollment FROM Enrollee as en  " +
			" WHERE en.taxIdNumber=:ssn " +
			" and en.enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') " +
			" and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " +
			" and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
	List<Enrollment> findActiveEnrollmentBySSN(@Param("ssn")String ssn);
	
	@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID")
	List<Enrollee> findEnrolleesByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query(" SELECT COUNT(en) FROM Enrollee as en " +
			" WHERE en.enrollment.id = :enrollmentID " +
			" and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
			" and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
			" and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' " +
			" and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
	long totalActiveMembersByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	@Query(" SELECT COUNT(en) FROM Enrollee as en " +
			" WHERE en.enrollment.id = :enrollmentID " +
			" and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
			" and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
			" and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' " +
			" and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','ABORTED')")
	long totalActiveMembersByEnrollmentIDForIRS(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode='SUBSCRIBER'")
	Enrollee findSubscriberByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	/**
	 * Query for Shop
	 * @param enrollmentID
	 * @param systemDate
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrolleeShopDTO(en.healthCoveragePolicyNo, en.firstName,"
			+ " en.lastName, en.effectiveStartDate, en.effectiveEndDate, en.exchgIndivIdentifier,"
			+ " en.personTypeLkp.lookupValueCode, en.enrolleeLkpValue.lookupValueCode, en.age, en.totalIndvResponsibilityAmt, "
			+ " en.quotingDate, en.birthDate, en.relationshipToHCPLkp.lookupValueCode)"
			+ " FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID "
			+ " AND en.personTypeLkp.lookupValueCode in('ENROLLEE', 'SUBSCRIBER')"
			+" AND ((en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM'))"
			+ " OR ((en.enrolleeLkpValue.lookupValueCode IN ('TERM')) AND (:systemDate <= en.effectiveEndDate)))")
	List<EnrolleeShopDTO> getEnrolleeShopDtoList(@Param("enrollmentID") Integer enrollmentID, @Param("systemDate")Date systemDate);
	
	/**
	 * Query for Individual
	 * @param employeeId
	 * @param employeeAppId
	 * @param status
	 * @return
	 */
	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrolleeShopDTO(en.healthCoveragePolicyNo, en.firstName,"
			+ " en.lastName, en.effectiveStartDate, en.effectiveEndDate, en.exchgIndivIdentifier,"
			+ " en.personTypeLkp.lookupValueCode, en.enrolleeLkpValue.lookupValueCode, en.age, en.totalIndvResponsibilityAmt, en.quotingDate, "
			+ " en.birthDate, en.relationshipToHCPLkp.lookupValueCode, loc.address1, loc.address2, loc.city, loc.state, loc.county, loc.countycode, loc.zip)"
			+ " FROM Enrollee as en "
			+ " LEFT JOIN en.homeAddressid as loc "
			+ "	WHERE en.enrollment.id = :enrollmentID "
			+ " AND en.personTypeLkp.lookupValueCode in('ENROLLEE', 'SUBSCRIBER')"
			+" AND en.enrolleeLkpValue.lookupValueCode NOT IN ('ABORTED')")
	List<EnrolleeShopDTO> getEnrolleeIndividualDtoList(@Param("enrollmentID") Integer enrollmentID);
	
	@Query("select en FROM Enrollment as en " +
			          "WHERE " +
						   "en.employeeId=:employeeId and en.employeeAppId= :employeeAppId and "+
						   "((en.enrollmentStatusLkp.lookupValueCode not in (:status)) " +
						   "OR ((en.enrollmentStatusLkp.lookupValueCode in ('TERM')) AND (:currentDate <= en.benefitEndDate)))")
	List<Enrollment> getEnrollmentEmployeeIdAndEnrollmentStatus(@Param("employeeId") Integer employeeId,@Param("employeeAppId") Long employeeAppId, @Param("status") List<String> status, @Param("currentDate") Date currentDate);
	
	@Query(" select en FROM Enrollment as en " +
	          "WHERE " +
				   "en.ssapApplicationid= :ssapAppId and en.enrollmentStatusLkp.lookupValueCode not in ('ABORTED')")
	List<Enrollment> getEnrollmentBySsapApplictionIdAndEnrollmentStatus(@Param("ssapAppId") Long ssapAppId);
	
	@Query("SELECT new com.getinsured.hix.dto.enrollment.EnrollmentShopDTO(en.planId, en.planName, en.planLevel, en.id, en.grossPremiumAmt, "
			+ "en.employerContribution, en.employeeContribution, en.houseHoldCaseId, en.employeeAppId, en.employerEnrollmentId, en.aptcAmt, en.stateSubsidyAmt, "
			+ "en.netPremiumAmt, en.createdOn, en.insuranceTypeLkp.lookupValueLabel, en.issuerId, en.benefitEffectiveDate, en.benefitEndDate,"
			+ "en.enrollmentStatusLkp.lookupValueCode, en.enrollmentStatusLkp.lookupValueLabel, en.CMSPlanID, en.ehbAmt, en.dntlEhbAmt, "
			+ "en.ehbPercent, en.dntlEssentialHealthBenefitPrmDollarVal, en.enrollmentConfirmationDate, "
			+ "case when en.priorEnrollmentId is not null then 'Y' else 'N' end ) "
			+ "FROM Enrollment as en WHERE en.ssapApplicationid= :ssapAppId and en.enrollmentStatusLkp.lookupValueCode not in ('ABORTED')")
	List<EnrollmentShopDTO> getEnrollmentShopDto(@Param("ssapAppId") Long ssapAppId);
	
	@Query(" SELECT id FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID and en.personTypeLkp.lookupValueCode='SUBSCRIBER'")
	Integer findSubscriberIdByEnrollmentId(@Param("enrollmentID") Integer enrollmentID);
	
	/*@Query("SELECT en.enrollment FROM Enrollee as en  " +
			" WHERE en.exchgIndivIdentifier=:memberID " +
			" and en.enrollment.employeeId= :employeeID " +
			" and en.enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') " +
			" and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " +
			" and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
	List<Enrollment> findActiveEnrollmentByEmployeeIDandMemberID(@Param("employeeID") Integer employeeID,@Param("memberID") String memberID );*/
	
	/*@Query("SELECT en.enrollment FROM Enrollee as en  " +
			" WHERE en.exchgIndivIdentifier=:memberID " +
			" and en.enrollment.employeeId= :employeeID " +
			" and en.enrollment.employeeAppId= :employeeAppId " +
			" and  en.enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') " +
			" and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " +
			" and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
	List<Enrollment> findActiveEnrollmentByEmployeeIDandMemberID(@Param("employeeID") Integer employeeID,@Param("memberID") String memberID, @Param("employeeAppId") Integer employeeAppId );*/
	
	@Query("   SELECT DISTINCT enrlee.id, " 
		   + " enrlee.healthCoveragePolicyNo, "  
			   + " enrlee.enrollment.enrollmentStatusLkp.lookupValueCode, "  
			   + " enrlee.enrollment.enrollmentStatusLkp.lookupValueLabel, "  
			   + " enrlee.enrollment.insuranceTypeLkp.lookupValueCode, " 
			   + " enrlee.enrollment.enrollmentTypeLkp.lookupValueCode, " 
			   + " enrlee.enrollment.planName, "
			   + " enrlee.enrollment.CMSPlanID, " 
			   + " enrlee.enrollment.insurerName, "  
			   + " enrlee.firstName, "
			   + " enrlee.middleName, " 
			   + " enrlee.lastName, "
			   + " enrlee.birthDate, "
			   + " SUBSTR(enrlee.taxIdNumber, 6), "
			   + " enrlee.enrollment.exchgSubscriberIdentifier,"
			   + " enrlee.enrollment.id,"
			   + " enrlee.effectiveStartDate, "  
			   + " enrlee.enrollment.employer.id, "  
			   + " enrlee.enrollment.employer.name, "  
			   + " enrlee.enrollment.benefitEffectiveDate, "  
			   + " enrlee.enrollment.benefitEndDate "
	+ " FROM Enrollee enrlee " 
	+ " WHERE enrlee.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " 
	
	+ " AND enrlee.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT'  "
	
	+ " AND enrlee.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' "
	
	+ " AND (    ((:healthCoveragePolicyNo IS NOT NULL) AND (enrlee.healthCoveragePolicyNo =:healthCoveragePolicyNo)) "
	+ "       OR (( :healthCoveragePolicyNo IS NULL) AND ((enrlee.healthCoveragePolicyNo IS NULL) "
	+ "       OR (enrlee.healthCoveragePolicyNo IS NOT NULL)) ) )   "
	
	+ " AND(     (( (:statusLkpList) IS NOT NULL) AND (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IN (:statusLkpList) )) " 
	+ "       OR (( (:statusLkpList) IS NULL) AND ((enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NULL) "
	+ "       OR (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NOT NULL) ) ) )  "
	
	+ " AND (    ((:plantype IS NOT NULL) AND (enrlee.enrollment.insuranceTypeLkp.lookupValueCode =:plantype)) "
	+ "       OR (( :plantype IS NULL) AND ((enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NOT NULL)))) "
	
	+ " AND (    ((:cmsPlanId IS NOT NULL) AND ( enrlee.enrollment.CMSPlanID LIKE '%'||:cmsPlanId||'%' ) ) "
	+ "       OR (( :cmsPlanId IS NULL) AND ((enrlee.enrollment.CMSPlanID IS NULL) OR (enrlee.enrollment.CMSPlanID IS NOT NULL)))) "
	
	+ " AND (    ((:issuer <> 0) AND (enrlee.enrollment.issuerId =:issuer)) "
	+ "       OR (( :issuer = 0) AND (enrlee.enrollment.issuerId > 0))) "
	
	+ " AND (    ((:employerName IS NOT NULL) AND (enrlee.enrollment.employer.name =:employerName)) " 
	+ "       OR (( :employerName IS NULL) AND ((enrlee.enrollment.employer.name IS NULL) OR (enrlee.enrollment.employer.name IS NOT NULL)))) "
	
	+ " AND (    ((:name IS NOT NULL) AND ( LOWER(enrlee.firstName) ||' '||LOWER(enrlee.lastName)  LIKE '%' || LOWER(:name) || '%')  "
	+ "       OR ( ( :name IS NULL) AND ((enrlee.firstName IS NULL) OR (enrlee.firstName IS NOT NULL)) )) " 
    + "       OR (( :name IS NULL) AND ((enrlee.lastName IS NULL) OR (enrlee.lastName IS NOT NULL))) ) "
	
	+ " AND (    ((:enrollmentTypeLkp IS NOT NULL) AND (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode =:enrollmentTypeLkp)) " 
	+ "       OR ((:enrollmentTypeLkp IS NULL) AND ((enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NULL) "
    + "       OR (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NOT NULL) ))) "
	
	+ " AND (    ((:subscriberId IS NOT NULL) AND ( (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') AND (enrlee.exchgIndivIdentifier  =:subscriberId) ) "
	+ "       OR (( :subscriberId IS NULL ) AND ( (enrlee.exchgIndivIdentifier IS NULL) OR (enrlee.exchgIndivIdentifier IS NOT NULL))) ))"
	
	+ " AND (    ((:last4DigitSSN IS NOT NULL) AND (SUBSTR(enrlee.taxIdNumber, 6)  =:last4DigitSSN)) "
	+ "       OR (( :last4DigitSSN IS NULL ) AND ( (enrlee.taxIdNumber IS NULL) OR (enrlee.taxIdNumber IS NOT NULL))) )"
	
	+ " AND (    ((:dOBofSubscriber IS NOT NULL) AND ( (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') AND TO_CHAR(enrlee.birthDate, 'MM/DD/YYYY')  =:dOBofSubscriber) ) "
	+ "       OR (( :dOBofSubscriber IS NULL ) AND ( (enrlee.birthDate IS NULL) OR (enrlee.birthDate IS NOT NULL))) )"
	
	+ " AND (    ((:applicableYear IS NOT NULL) AND (TO_CHAR(enrlee.effectiveStartDate, 'YYYY')  =:applicableYear)) "
	+ "       OR (( :applicableYear IS NULL ) AND ( (enrlee.effectiveStartDate IS NULL) OR (enrlee.effectiveStartDate IS NOT NULL))) )"
	
	+" AND    (((:policyId <> 0) AND (enrlee.enrollment.id =:policyId)) "
	+"     OR ( ( :policyId = 0) AND (enrlee.enrollment.id > 0) )) ")
Page<Object[]> getShopEnrolleeByIssuerData(@Param("healthCoveragePolicyNo") String healthCoveragePolicyNo,
										   @Param("statusLkpList") List<String> statusLkpList,
										   @Param("plantype") String plantype,
										   @Param("cmsPlanId") String cmsPlanId,
										   @Param("issuer") int issuer,
										   @Param("employerName") String employerName, 
										   @Param("name") String name,
										   @Param("enrollmentTypeLkp") String enrollmentTypeLkp,										  
										   @Param("subscriberId") String subscriberId, 
										   @Param("last4DigitSSN") String last4DigitSSN,
										   @Param("dOBofSubscriber") String dOBofSubscriber, 
										   @Param("applicableYear") String applicableYear,
										   @Param("policyId") int policyId,
										   Pageable pageable);

@Query(" SELECT DISTINCT enrlee.id, " 
		  +" enrlee.healthCoveragePolicyNo, " 
		  +" enrlee.enrollment.enrollmentStatusLkp.lookupValueCode, " 
		  +" enrlee.enrollment.enrollmentStatusLkp.lookupValueLabel, " 
		  +" enrlee.enrollment.insuranceTypeLkp.lookupValueLabel, " 
		  +" enrlee.enrollment.enrollmentTypeLkp.lookupValueCode, " 
		  +" enrlee.enrollment.planName, " 
		  +" enrlee.enrollment.CMSPlanID, " 
		  +" enrlee.enrollment.insurerName, " 
		  +" enrlee.firstName, " 
		  +" enrlee.middleName, " 
		  +" enrlee.lastName, "
		  +" enrlee.birthDate, "
		  +" SUBSTR(enrlee.taxIdNumber, 6), "
		  +" enrlee.enrollment.exchgSubscriberIdentifier,"
		  +" enrlee.enrollment.id,"
		  +" enrlee.effectiveStartDate, " 
		  +" enrlee.enrollment.benefitEffectiveDate, " 
		  +" enrlee.enrollment.benefitEndDate " 
	+" FROM Enrollee enrlee " 
	+" WHERE enrlee.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' "
	
	+" AND enrlee.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' "
	
	+" AND enrlee.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' "
	
	+" AND    (((:healthCoveragePolicyNo IS NOT NULL) AND (enrlee.healthCoveragePolicyNo =:healthCoveragePolicyNo)) " 
	+"     OR (( :healthCoveragePolicyNo IS NULL) AND ((enrlee.healthCoveragePolicyNo IS NULL) OR (enrlee.healthCoveragePolicyNo IS NOT NULL)) ) ) "
	
	+" AND    ((( (:statusLkpList) IS NOT NULL) AND (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IN (:statusLkpList) )) " 
	+"     OR ((  (:statusLkpList) IS NULL) AND ((enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NULL) "
	+"     OR (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NOT NULL)) ) ) "
	
	+" AND    (((:plantype IS NOT NULL) AND (enrlee.enrollment.insuranceTypeLkp.lookupValueCode =:plantype)) " 
	+"     OR (( :plantype IS NULL) AND ((enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NOT NULL)) ) ) "
	
	+ " AND (    ((:cmsPlanId IS NOT NULL) AND (enrlee.enrollment.CMSPlanID LIKE '%'||:cmsPlanId||'%') ) "
	+ "       OR (( :cmsPlanId IS NULL) AND ((enrlee.enrollment.CMSPlanID IS NULL) OR (enrlee.enrollment.CMSPlanID IS NOT NULL)))) "
	
	+" AND    (((:issuer <> 0) AND (enrlee.enrollment.issuerId =:issuer)) "
	+"     OR ( ( :issuer = 0) AND (enrlee.enrollment.issuerId > 0) )) "
	
	+" AND    (((:name IS NOT NULL) AND ( LOWER(enrlee.firstName) ||' '||LOWER(enrlee.lastName)  LIKE '%' || LOWER(:name) || '%') "  
	+"     OR (( :name IS NULL) AND ((enrlee.firstName IS NULL) OR (enrlee.firstName IS NOT NULL)) )) "
	+"     OR (( :name IS NULL) AND ((enrlee.lastName IS NULL) OR (enrlee.lastName IS NOT NULL)) )) "
	
	+" AND    (((:enrollmentTypeLkp IS NOT NULL) AND (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode =:enrollmentTypeLkp)) " 
	+"     OR ((:enrollmentTypeLkp IS NULL) AND ((enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NULL) "
	+"     OR (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NOT NULL) )) )"
	
	+ " AND (    ((:subscriberId IS NOT NULL) AND ( (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') AND (enrlee.exchgIndivIdentifier  =:subscriberId) ) "
	+ "       OR (( :subscriberId IS NULL ) AND ( (enrlee.exchgIndivIdentifier IS NULL) OR (enrlee.exchgIndivIdentifier IS NOT NULL))) ))"
	
	+ " AND (    ((:last4DigitSSN IS NOT NULL) AND (SUBSTR(enrlee.taxIdNumber, 6)  =:last4DigitSSN)) "
	+ "       OR (( :last4DigitSSN IS NULL ) AND ( (enrlee.taxIdNumber IS NULL) OR (enrlee.taxIdNumber IS NOT NULL))) )"
	
	+ " AND (    ((:dOBofSubscriber IS NOT NULL) AND ( (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') AND TO_CHAR(enrlee.birthDate, 'MM/DD/YYYY')  =:dOBofSubscriber) ) "
	+ "       OR (( :dOBofSubscriber IS NULL ) AND ( (enrlee.birthDate IS NULL) OR (enrlee.birthDate IS NOT NULL))) )"
	
	+ " AND (    ((:applicableYear IS NOT NULL) AND (TO_CHAR(enrlee.effectiveStartDate, 'YYYY')  =:applicableYear)) "
	+ "       OR (( :applicableYear IS NULL ) AND ( (enrlee.effectiveStartDate IS NULL) OR (enrlee.effectiveStartDate IS NOT NULL))) )"
	
	+" AND    (((:insuranceType IS NOT NULL) AND (enrlee.enrollment.insuranceTypeLkp.lookupValueCode =:insuranceType)) " 
	+"     OR ((:insuranceType IS NULL) AND ((enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NULL) "
	+"     OR (enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NOT NULL) )) )"
	
	+" AND    (((:policyId <> 0) AND (enrlee.enrollment.id =:policyId)) "
	+"     OR ( ( :policyId = 0) AND (enrlee.enrollment.id > 0) )) ")
Page<Object[]> getIndividualEnrolleeByIssuerData(@Param("healthCoveragePolicyNo") String healthCoveragePolicyNo,
									   @Param("statusLkpList") List<String> statusLkpList,
									   @Param("plantype") String plantype,
									   @Param("cmsPlanId") String cmsPlanId,
									   @Param("issuer") int issuer, 
									   @Param("name") String name, 
									   @Param("enrollmentTypeLkp") String enrollmentTypeLkp,
									   @Param("subscriberId") String subscriberId, 
									   @Param("last4DigitSSN") String last4DigitSSN,
									   @Param("dOBofSubscriber") String dOBofSubscriber, 
									   @Param("applicableYear") String applicableYear,
									   @Param("policyId") int policyId,
									   @Param("insuranceType") String insuranceType,
									   Pageable pageable);

@Query("FROM Enrollee as en " +
	  " WHERE en.enrollment.id = :enrollmentID " +
	  " and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
	  " and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
	  " and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'" +
	  " and en.enrollmentReason = 'S'" +
	  " and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')")
List<Enrollee> getSpecialEnrolleeByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);

/**
 * Returns data for newly added or deleted member during special enrollment
 * @param currentDate
 * @param lastInvoiceDate
 * @param enrollmentID
 * @return
 */
@Query("SELECT DISTINCT en FROM Enrollee as en, EnrollmentEvent event" +
		  " WHERE en.enrollment.id = :enrollmentID " +
		  " and event.enrollee.id = en.id" +
		  " and event.spclEnrollmentReasonLkp IS NOT NULL" +
		  " and en.enrollmentReason = 'S' " +
		  " and event.updatedOn <= :currentDate " +
		  " and event.updatedOn > :lastInvoiceDate " +
		  " and (event.eventTypeLkp.lookupValueCode IN ('021','024', '025')) " +
		  " and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
		  " and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
		  " and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'")
List<Enrollee> getSpecialEnrolleeByEnrollmentID(@Param("currentDate") Date currentDate,@Param("lastInvoiceDate") Date lastInvoiceDate, @Param("enrollmentID") Integer enrollmentID);

@Query("SELECT DISTINCT enrlee.enrollment.id "
		+ "FROM Enrollee as enrlee "
		+ "WHERE "
		+ "(((LOWER(enrlee.firstName) ||' '|| LOWER(enrlee.middleName) ||' '|| LOWER(enrlee.lastName)  LIKE '%' || LOWER(:employeeName) || '%')) OR "
		+ "((LOWER(enrlee.firstName) ||' '|| LOWER(enrlee.lastName) LIKE '%' || LOWER(:employeeName) || '%'))) AND "
		+ "(enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER' OR enrlee.personTypeLkp.lookupValueCode='RESPONSIBLE_PERSON') ")
List<Integer> searchEmployee(@Param("employeeName") String employeeName);


@Query("SELECT "
        + "event.createdOn, "
        + "event.eventTypeLkp.lookupValueLabel, "
        + "event.eventReasonLkp.lookupValueLabel, "
        + "enrlee.effectiveStartDate, "
        + "enrlee.effectiveEndDate, "
        + "enrlee.firstName || ' ' || enrlee.lastName, "
        + "enrlee.enrollment.id, "
        + "enrlee.enrollment.insuranceTypeLkp.lookupValueLabel, "
        + "enrlee.enrollment.CMSPlanID, "
        + "enrlee.enrollment.planName "
        + "FROM "
        + "EnrollmentEvent as event, "
        + "Enrollee as enrlee "
        + "WHERE "
        + "enrlee.enrollment.employeeId = :employeeId "
        + "AND event.enrollee.id = enrlee.id "
        + "AND event.spclEnrollmentReasonLkp IS NULL "
        + "AND event.eventTypeLkp.lookupValueCode <> '001' "
        + "AND enrlee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " 
        + "ORDER BY event.createdOn desc")
List<Object[]> getShopEnrollmentLog(@Param("employeeId") Integer employeeId );


@Query("SELECT "
        + "event.createdOn, "
        + "event.eventTypeLkp.lookupValueLabel, "
        + "event.eventReasonLkp.lookupValueLabel, "
        + "enrlee.effectiveStartDate, "
        + "enrlee.effectiveEndDate, "
        + "enrlee.firstName || ' ' || enrlee.lastName, "
        + "enrlee.enrollment.id, "
        + "enrlee.enrollment.insuranceTypeLkp.lookupValueLabel, "
        + "enrlee.enrollment.CMSPlanID, "
        + "enrlee.enrollment.planName, "
        + "event.spclEnrollmentReasonLkp.lookupValueLabel "
        + "FROM "
        + "EnrollmentEvent as event, "
        + "Enrollee as enrlee "
        + "WHERE "
        + "enrlee.enrollment.employeeId = :employeeId "
        + "AND event.enrollee.id = enrlee.id "
        + "AND event.spclEnrollmentReasonLkp IS NOT NULL "
        + "AND event.eventTypeLkp.lookupValueCode <> '001' "
        + "AND enrlee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " 
        + "ORDER BY event.createdOn desc")
List<Object[]> getShopEnrollmentLogForSpecialEnrollment(@Param("employeeId") Integer employeeId);


@Query("SELECT "
        + "event.createdOn, "
        + "event.eventTypeLkp.lookupValueLabel, "
        + "event.eventReasonLkp.lookupValueLabel, "
        + "enrlee.effectiveStartDate, "
        + "enrlee.effectiveEndDate, "
        + "enrlee.firstName || ' ' || enrlee.lastName, "
        + "enrlee.enrollment.id, "
        + "enrlee.enrollment.insuranceTypeLkp.lookupValueLabel, "
        + "enrlee.enrollment.CMSPlanID, "
        + "enrlee.enrollment.planName "
        + "FROM "
        + "EnrollmentEvent as event, "
        + "Enrollee as enrlee "
        + "WHERE "
        + "enrlee.enrollment.employer.id = :employerId "
        + "AND event.createdOn BETWEEN :startDate AND :endDate "
        + "AND event.enrollee.id = enrlee.id "
        + "AND event.spclEnrollmentReasonLkp IS NULL "
        + "AND event.eventTypeLkp.lookupValueCode <> '001' "
        + "AND enrlee.enrollment.id in (:enrollmentIds) "
        + "AND enrlee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " 
        + "ORDER BY event.createdOn desc")
List<Object[]> getShopEnrollmentLog(@Param("employerId") Integer employerId ,@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("enrollmentIds") List<Integer> enrollmentIds);


@Query("SELECT "
        + "event.createdOn, "
        + "event.eventTypeLkp.lookupValueLabel, "
        + "event.eventReasonLkp.lookupValueLabel, "
        + "enrlee.effectiveStartDate, "
        + "enrlee.effectiveEndDate, "
        + "enrlee.firstName || ' ' || enrlee.lastName, "
        + "enrlee.enrollment.id, "
        + "enrlee.enrollment.insuranceTypeLkp.lookupValueLabel, "
        + "enrlee.enrollment.CMSPlanID, "
        + "enrlee.enrollment.planName, "
        + "event.spclEnrollmentReasonLkp.lookupValueLabel "
        + "FROM "
        + "EnrollmentEvent as event, "
        + "Enrollee as enrlee "
        + "WHERE "
        + "enrlee.enrollment.employer.id = :employerId "
        + "AND event.createdOn BETWEEN :startDate AND :endDate "
        + "AND event.enrollee.id = enrlee.id "
        + "AND event.spclEnrollmentReasonLkp IS NOT NULL "
        + "AND event.eventTypeLkp.lookupValueCode <> '001' "
        + "AND enrlee.enrollment.id in (:enrollmentIds) "
        + "AND enrlee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " 
        + "ORDER BY event.createdOn desc")
List<Object[]> getShopEnrollmentLogForSpecialEnrollment(@Param("employerId") Integer employerId ,@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("enrollmentIds") List<Integer> enrollmentIds);


@Query("SELECT "
        + "event.createdOn, "
        + "event.eventTypeLkp.lookupValueLabel, "
        + "event.eventReasonLkp.lookupValueLabel, "
        + "enrlee.effectiveStartDate, "
        + "enrlee.effectiveEndDate, "
        + "enrlee.firstName || ' ' || enrlee.lastName, "
        + "enrlee.enrollment.id, "
        + "enrlee.enrollment.insuranceTypeLkp.lookupValueLabel, "
        + "enrlee.enrollment.CMSPlanID, "
        + "enrlee.enrollment.planName "
        + "FROM "
        + "EnrollmentEvent as event, "
        + "Enrollee as enrlee "
        + "WHERE "
        + "enrlee.enrollment.employer.id = :employerId "
        + "AND event.createdOn BETWEEN :startDate AND :endDate "
        + "AND event.enrollee.id = enrlee.id "
        + "AND event.spclEnrollmentReasonLkp IS NULL "
        + "AND event.eventTypeLkp.lookupValueCode <> '001' "
        + "AND enrlee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " 
        + "ORDER BY event.createdOn desc")
List<Object[]> getShopEnrollmentLog(@Param("employerId") Integer employerId ,@Param("startDate") Date startDate, @Param("endDate") Date endDate);

@Query("SELECT "
        + "event.createdOn, "
        + "event.eventTypeLkp.lookupValueLabel, "
        + "event.eventReasonLkp.lookupValueLabel, "
        + "enrlee.effectiveStartDate, "
        + "enrlee.effectiveEndDate, "
        + "enrlee.firstName || ' ' || enrlee.lastName, "
        + "enrlee.enrollment.id, "
        + "enrlee.enrollment.insuranceTypeLkp.lookupValueLabel, "
        + "enrlee.enrollment.CMSPlanID, "
        + "enrlee.enrollment.planName, "
        + "event.spclEnrollmentReasonLkp.lookupValueLabel "
        + "FROM "
        + "EnrollmentEvent as event, "
        + "Enrollee as enrlee "
        + "WHERE "
        + "enrlee.enrollment.employer.id = :employerId "
        + "AND event.createdOn BETWEEN :startDate AND :endDate "
        + "AND event.enrollee.id = enrlee.id "
        + "AND event.spclEnrollmentReasonLkp IS NOT NULL "
        + "AND event.eventTypeLkp.lookupValueCode <> '001' "
        + "AND enrlee.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " 
        + "ORDER BY event.createdOn desc")
List<Object[]> getShopEnrollmentLogForSpecialEnrollment(@Param("employerId") Integer employerId ,@Param("startDate") Date startDate, @Param("endDate") Date endDate);

@Query("SELECT enrlee.firstName || ' ' || enrlee.lastName "
		+ "FROM Enrollee as enrlee "
		+ "WHERE "
		+ "enrlee.enrollment.id = :enrollmentId "
		+ "AND enrlee.personTypeLkp.lookupValueCode='RESPONSIBLE_PERSON'")
String getEmployeeNameFromEnrollmentId(@Param("enrollmentId") Integer enrollmentId);

/*@Query("SELECT "
		+ "enrlee.firstName, "
		+ "enrlee.lastName "
		+ "FROM "
		+ "Enrollee as enrlee "
		+ "WHERE "
		+ "enrlee.exchgIndivIdentifier = :memberId "
		+ "AND enrlee.enrollment.id = :enrollmentId "
		+ "AND enrlee.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' "
		+ "AND enrlee.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' "
		+ "AND enrlee.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'")
List<Object[]> getEnrolleeName(@Param("memberId") String memberId,@Param("enrollmentId") Integer enrollmentId);*/

/*@Query("SELECT en.enrollment FROM Enrollee as en  " +
		" WHERE en.enrollment.employeeId= :employeeID " +
		" and en.enrollment.employeeAppId= :employeeAppId " +
		" and  en.enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED') " +
		" and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') " +
		" and en.enrolleeLkpValue.lookupValueCode NOT IN ('CANCEL','TERM', 'ABORTED')" )
List<Enrollment> findActiveEnrollmentByEmployeeIDandEmployeeAppId(@Param("employeeID") Integer employeeID, @Param("employeeAppId") Integer employeeAppId );*/

@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentId " +
		" and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
		" and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
		" and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'  " +
		" and en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM','TERM') ")
List<Enrollee> getPenddingOrTermEnrollee(@Param("enrollmentId") Integer enrollmentId);


@Query("select max(en) FROM Enrollment as en " +
       " WHERE en.employeeId=:employeeId " +
       " and en.employeeAppId= :employeeAppId and en.insuranceTypeLkp.lookupValueCode = 'HLT' "+
       " and en.enrollmentStatusLkp.lookupValueCode not in (:status)")
Enrollment getHealthEnrollmentEmployeeIdAndEnrollmentStatus(@Param("employeeId") Integer employeeId,@Param("employeeAppId") Long employeeAppId, @Param("status") List<String> status);

@Query(" select max(en) FROM Enrollment as en " +
       " WHERE en.employeeId=:employeeId " +
       " and en.employeeAppId= :employeeAppId and en.insuranceTypeLkp.lookupValueCode = 'DEN' "+
       " and en.enrollmentStatusLkp.lookupValueCode not in (:status)")
Enrollment getDentalEnrollmentEmployeeIdAndEnrollmentStatus(@Param("employeeId") Integer employeeId,@Param("employeeAppId") Long employeeAppId, @Param("status") List<String> status);


@Query("SELECT en.id FROM Enrollee as en WHERE en.id NOT IN (:existingEnrolleeList) and en.enrollment.id = :enrollmentID "
//		+ "and TO_DATE(TO_CHAR(en.createdOn,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE( TO_CHAR(:endDate,'MM/DD/YYYY'), 'MM/DD/YYYY') "
		+ "and TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
		+ "and TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "
		+ "and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') and en.enrolleeLkpValue.lookupValueCode IN ('CONFIRM','TERM')")
List<Integer> getRemainingEnrolleesForIRS(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") String endDateString, @Param("existingEnrolleeList") List<Integer> enrolleeIds, @Param("startDate") String startDateString);

/*@Query(" SELECT en.id FROM Enrollee as en WHERE " +
		   " en.enrollment.id = :enrollmentID " +
		   " and en.createdOn < :endDate " +
		   " and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON'" +
		   " and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
		   " and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' " +
		   " and en.enrolleeLkpValue.lookupValueCode IN ('CONFIRM','TERM')")
	List<Integer> getRemainingEnrolleesForIRS(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") Date endDate);*/

/*@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +
	   " and en.createdOn < :endDate " +
	   " and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
	   " and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
	   " and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' " +
	   " and en.enrolleeLkpValue.lookupValueCode IN ('CONFIRM','TERM')")
List<Enrollee> getEnrolleesByEnrollmentIdAndCreatedOn(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") Date endDate);*/

@Query(" SELECT en.id FROM Enrollee as en WHERE en.id NOT IN (:existingEnrolleeList) " +
		   " AND en.enrollment.id = :enrollmentID " +
		   " AND TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate,'MM/DD/YYYY') "+
		   " AND TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate,'MM/DD/YYYY') "+
		   " AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON'" +
		   " AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
		   " AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' ")
	List<Integer> getRemainingEnrolleesForIRSShop(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") String endDateString, @Param("existingEnrolleeList") List<Integer> enrolleeIds);

@Query(" SELECT en.id FROM Enrollee as en WHERE  " +
		   " en.enrollment.id = :enrollmentID " +
		   " and TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate,'MM/DD/YYYY') "+
		   " and TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:endDate,'MM/DD/YYYY') "+
		   " and en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON'" +
		   " and en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
		   " and en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' ")
	List<Integer> getRemainingEnrolleesForIRSShop(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") String endDateString);

@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID "
		+ "and TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
		+ "and TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "
		+ "and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') and en.enrolleeLkpValue.lookupValueCode IN ('CONFIRM','TERM')")
List<Enrollee> getConfirmedAndTermEnrolleesForIRS(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") String endDateString, @Param("startDate") String startDateString);


/**
 * Jira ID: HIX-71256
 * @since 25th June 2015
 * @param enrollmentIdList List<Integer>
 * @param personTypeList List<String>
 * @param systemDate Date
 * @return
 * HQL query to fetch List of Active Enrollee by ListOfEnrollmentIds
 */
@Query("SELECT ee.firstName, ee.lastName, ee.effectiveStartDate, ee.effectiveEndDate, ee.enrollment.id, ee.healthCoveragePolicyNo,"
		+ " erLkp.lookupValueLabel, loc.address1, loc.address2, loc.city, loc.state, loc.zip, loc.county, loc.countycode,"
		+ " ee.primaryPhoneNo, ee.preferredEmail, ee.exchgIndivIdentifier, ee.suffix, ee.middleName, ee.taxIdNumber,"
		+ " ee.birthDate, genLkp.lookupValueCode, genLkp.lookupValueLabel, ee.id, "
		+ " tobaccoLkp.lookupValueLabel, "
		+ " mailingLoc.address1, mailingLoc.address2, mailingLoc.city, mailingLoc.state, mailingLoc.zip, mailingLoc.county, mailingLoc.countycode, "
		+ " ee.ratingArea, "
		+ " ee.ratingAreaEffDate, "
		+ " hcpLkp.lookupValueLabel, "
		+ " ee.age, "
		+ " erLkp.lookupValueCode, "
		+ " tobaccoLkp.lookupValueCode, "
		+ " hcpLkp.lookupValueCode, "
		+ " ee.personTypeLkp.lookupValueCode, ee.enrolleeLkpValue.lookupValueCode, "
		+ " ee.quotingDate "
		+ " FROM Enrollee as ee"
		+ " LEFT JOIN ee.enrollment as en"
		+ " LEFT JOIN ee.homeAddressid as loc "
		+ " LEFT JOIN ee.mailingAddressId as mailingLoc "
		+ " LEFT JOIN ee.genderLkp as genLkp "
		+ " LEFT JOIN ee.relationshipToHCPLkp as hcpLkp "
		+ " LEFT JOIN ee.tobaccoUsageLkp as tobaccoLkp "
		+ " LEFT JOIN ee.enrolleeRelationship as eer LEFT JOIN eer.relationshipLkp erLkp "
		+ " WHERE "
		+ " eer.sourceEnrollee.id = ee.id "
		+ " AND eer.targetEnrollee.id = (SELECT MAX(eeIn.id) FROM Enrollee eeIn Where eeIn.enrollment.id = ee.enrollment.id AND eeIn.personTypeLkp.lookupValueCode='SUBSCRIBER' AND eeIn.exchgIndivIdentifier = en.exchgSubscriberIdentifier)"
		+ " AND ee.enrollment.id IN (:enrollmentIdList) "
		+ " AND ee.personTypeLkp.lookupValueCode NOT IN (:personTypeList) "
		+ " AND ee.enrolleeLkpValue.lookupValueCode NOT IN ('ABORTED') "
		+ " AND :systemDate <= ee.effectiveEndDate")
List<Object[]> getActiveEnrolleeDataByListOfEnrollmentId(@Param("enrollmentIdList") List<Integer> enrollmentIdList,
		@Param("personTypeList") List<String> personTypeList, @Param("systemDate") Date systemDate);

/**
 * Jira ID: HIX-71256
 * @since 25th June 2015
 * @param enrollmentIdList List<Integer>
 * @param personTypeList List<String>
 * @return Object[]
 * HQL query to fetch List of Enrollee by ListOfEnrollmentIds
 */
@Query("SELECT ee.firstName, ee.lastName, ee.effectiveStartDate, ee.effectiveEndDate, ee.enrollment.id, ee.healthCoveragePolicyNo,"
		+ " ee.personTypeLkp.lookupValueCode, loc.address1, loc.address2, loc.city, loc.state, loc.zip, loc.county, loc.countycode,"
		+ " ee.primaryPhoneNo, ee.preferredEmail, ee.exchgIndivIdentifier, ee.suffix, ee.middleName, ee.taxIdNumber,"
		+ " ee.birthDate, genLkp.lookupValueCode, genLkp.lookupValueLabel, ee.id, ee.enrolleeLkpValue.lookupValueCode  "
		+ " FROM Enrollee as ee, Location as loc "
		+ " LEFT JOIN ee.genderLkp as genLkp"
		+ " WHERE ee.homeAddressid.id = loc.id "
		+ " AND ee.enrollment.id IN (:enrollmentIdList) "
		+ " AND ee.enrolleeLkpValue.lookupValueCode NOT IN ('ABORTED') "
		+ " AND ee.personTypeLkp.lookupValueCode IN (:personTypeList)")
List<Object[]> getEnrolleeDataByListOfEnrollmentIdAndPersonTypeCA(@Param("enrollmentIdList") List<Integer> enrollmentIdList,
		@Param("personTypeList") List<String> personTypeList);

@Query("SELECT ee.firstName, ee.lastName, ee.effectiveStartDate, ee.effectiveEndDate, ee.enrollment.id, ee.healthCoveragePolicyNo,"
		+ " erLkp.lookupValueLabel, loc.address1, loc.address2, loc.city, loc.state, loc.zip, loc.county, loc.countycode,"
		+ " ee.primaryPhoneNo, ee.preferredEmail, ee.exchgIndivIdentifier, ee.suffix, ee.middleName, ee.taxIdNumber,"
		+ " ee.birthDate, genLkp.lookupValueCode, genLkp.lookupValueLabel, ee.id, "
		+ " tobaccoLkp.lookupValueLabel, "
		+ " mailingLoc.address1, mailingLoc.address2, mailingLoc.city, mailingLoc.state, mailingLoc.zip, mailingLoc.county, mailingLoc.countycode, "
		+ " ee.ratingArea, "
		+ " ee.ratingAreaEffDate, "
		+ " hcpLkp.lookupValueLabel, "
		+ " ee.age, "
		+ " erLkp.lookupValueCode, "
		+ " tobaccoLkp.lookupValueCode, "
		+ " hcpLkp.lookupValueCode, "
		+ " ee.personTypeLkp.lookupValueCode, ee.enrolleeLkpValue.lookupValueCode, "
		+ " ee.quotingDate "
		+ " FROM Enrollee as ee"
		+ " LEFT JOIN ee.enrollment as en"
		+ " LEFT JOIN ee.homeAddressid as loc "
		+ " LEFT JOIN ee.mailingAddressId as mailingLoc "
		+ " LEFT JOIN ee.genderLkp as genLkp "
		+ " LEFT JOIN ee.relationshipToHCPLkp as hcpLkp "
		+ " LEFT JOIN ee.tobaccoUsageLkp as tobaccoLkp "
		+ " LEFT JOIN ee.enrolleeRelationship as eer LEFT JOIN eer.relationshipLkp erLkp "
		+ " WHERE "
		+ " eer.sourceEnrollee.id = ee.id "
		+ " AND eer.targetEnrollee.id = (SELECT MAX(eeIn.id) FROM Enrollee eeIn Where eeIn.enrollment.id = ee.enrollment.id AND eeIn.personTypeLkp.lookupValueCode='SUBSCRIBER' AND eeIn.exchgIndivIdentifier = en.exchgSubscriberIdentifier)"
		+ " AND ee.enrollment.id IN (:enrollmentIdList) "
		+ " AND ee.enrolleeLkpValue.lookupValueCode NOT IN ('ABORTED') "
		+ " AND ee.personTypeLkp.lookupValueCode NOT IN (:personTypeList) ")
List<Object[]> getEnrolleeDataByListOfEnrollmentIdAndPersonType(@Param("enrollmentIdList") List<Integer> enrollmentIdList,
		@Param("personTypeList") List<String> personTypeList);

@Query(" SELECT en.exchgIndivIdentifier" +
		" FROM Enrollee as en " +
		" WHERE en.enrollment.id = :enrollmentID " +
		" AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
		" AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT'  " +
		" AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' "+
		" AND en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM', 'PAYMENT_RECEIVED')"
		)
List<String> getActiveExchgIndivIdentifierByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);

@Query(" SELECT en.exchgIndivIdentifier" +
		" FROM Enrollee as en " +
		" WHERE en.enrollment.id = :enrollmentID " +
		" AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
		" AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT'  " +
		" AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' "+
		" AND en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM','TERM', 'PAYMENT_RECEIVED')"
		)
List<String> getExchgIndivIdentifierByEnrollmentID(@Param("enrollmentID") Integer enrollmentID);

/**
 * Fetching ZipCode and Countycode details of Subscriber.
 * @param enrollmentId Integer
 * @return List<Object[]>
 */
@Query("SELECT loc.zip, loc.countycode FROM Enrollee as enr, Location as loc"
		+ " WHERE enr.homeAddressid.id = loc.id AND enr.enrollment.id = :enrollmentId"
		+ " AND enr.personTypeLkp.lookupValueCode='SUBSCRIBER' ORDER BY enr.updatedOn DESC")
List<Object[]> getSubscribersHomeAddressDetails(@Param("enrollmentId") Integer enrollmentId);

/**
 * Fetching enrollee age and exchgIndivIdentifier details of memebrs.
 * @param enrollmentId Integer
 * @return List<Object[]>
 */
@Query("SELECT enr.exchgIndivIdentifier, enr.age FROM Enrollee as enr"
  + " WHERE enr.enrollment.id = :enrollmentId"
  + " AND enr.personTypeLkp.lookupValueCode in ('SUBSCRIBER','ENROLLEE' ) ORDER BY enr.updatedOn DESC")
List<Object[]> getEnrolleeAgeAndMemberId(@Param("enrollmentId") Integer enrollmentId);

/**
 * Jira ID: HIX-71256
 * @since 25th June 2015
 * @param enrollmentIdList List<Integer>
 * @param personTypeList List<String>
 * @return Object[]
 * HQL query to fetch List of Enrollee by ListOfEnrollmentIds
 */
@Query("SELECT COUNT(DISTINCT en.exchgIndivIdentifier), en.enrollment.id FROM Enrollee as en"
		+ " WHERE en.enrollment.id IN (:enrollmentIdList) AND"
		+ " en.personTypeLkp.lookupValueCode NOT IN (:personTypeList) group by en.enrollment.id")
List<Object[]> getEnrolleesDataByListOfEnrollmentId(@Param("enrollmentIdList") List<Integer> enrollmentIdList,
		@Param("personTypeList") List<String> personTypeList);


/**
 * Get accountable enrollees for IRS annual calculations
 * @param enrollmentId
 * @param endDate
 * @param startDate
 * @return Enrollee count
 */
@Query("SELECT COUNT(en.id) FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID "
		+ "and TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:endDate, 'MM/DD/YYYY') "
		+ "and TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:startDate, 'MM/DD/YYYY') "
		+ "and en.totalIndvResponsibilityAmt IS NOT NULL and en.totalIndvResponsibilityAmt > 0"
		+ "and en.personTypeLkp.lookupValueCode NOT IN ('RESPONSIBLE_PERSON', 'HOUSEHOLD_CONTACT','CUSTODIAL_PARENT') and en.enrolleeLkpValue.lookupValueCode IN ('CONFIRM','TERM')")
Long getAccountableEnrolleeCount(@Param("enrollmentID") Integer enrollmentID , @Param("endDate") String endDateString, @Param("startDate") String startDateString);



@Query("FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +
		" AND en.personTypeLkp.lookupValueCode IN('SUBSCRIBER') " +
		" AND (en.enrolleeLkpValue.lookupValueCode IN ('PENDING', 'CONFIRM', 'PAYMENT_RECEIVED')  "+
				" OR (en.enrolleeLkpValue.lookupValueCode IN ('TERM') AND en.effectiveEndDate > :date )) ORDER BY en.createdOn DESC")
List<Enrollee> getActiveSubscriberForEnrollmentID(@Param("enrollmentID") Integer enrollmentID,@Param("date") Date date);

@Query(" SELECT count(*) FROM Enrollee as en " +
		   " WHERE en.enrollment.houseHoldCaseId = :houseHoldCaseId " +
		   " AND en.enrollment.insuranceTypeLkp.lookupValueCode= :insuranceType "+
		   " AND en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM','TERM','PAYMENT_RECEIVED') "+
		   " AND TO_DATE(:startDateProposed,'MM/DD/YYYY')  <= TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND TO_DATE(:endDateProposed,'MM/DD/YYYY')  >= TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND en.exchgIndivIdentifier = :memberId"+
		   " AND en.enrollment.id <> :enrollmentId"
		   )
Long getEnrolleeOverLapCountForExistingEnrollment(@Param("houseHoldCaseId") String houseHoldCaseId, 
			@Param("insuranceType") String insuranceType,  
			@Param("startDateProposed") String startDateProposed, 
			@Param("endDateProposed") String endDateProposed,
			@Param("memberId") String memberId,
			@Param("enrollmentId") Integer enrollmentId);

@Query(" SELECT count(*) FROM Enrollee as en " +
		   " WHERE en.enrollment.houseHoldCaseId = :houseHoldCaseId " +
		   " AND en.enrollment.insuranceTypeLkp.lookupValueCode= :insuranceType "+
		   " AND en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM','TERM','PAYMENT_RECEIVED') "+
		   " AND TO_DATE(:startDateProposed,'MM/DD/YYYY')  <= TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND TO_DATE(:endDateProposed,'MM/DD/YYYY')  >= TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND en.exchgIndivIdentifier = :memberId"+
		   " AND en.enrollment.id = :enrollmentId"+
		   " AND en.id <> :enrolleeId"
		   )
Long getOverLapCountForExistingEnrollmentNewEnrollee(@Param("houseHoldCaseId") String houseHoldCaseId, 
			@Param("insuranceType") String insuranceType,  
			@Param("startDateProposed") String startDateProposed, 
			@Param("endDateProposed") String endDateProposed,
			@Param("memberId") String memberId,
			@Param("enrollmentId") Integer enrollmentId,
			@Param("enrolleeId") Integer enrolleeId);

@Query(" SELECT count(*) FROM Enrollee as en " +
		   " WHERE en.enrollment.houseHoldCaseId = :houseHoldCaseId " +
		   " AND en.enrollment.insuranceTypeLkp.lookupValueCode= :insuranceType "+
		   " AND en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM','TERM','PAYMENT_RECEIVED') "+
		   " AND TO_DATE(:startDateProposed,'MM/DD/YYYY')  <= TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND TO_DATE(:endDateProposed,'MM/DD/YYYY')  >= TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND en.exchgIndivIdentifier = :memberId"
		   )
Long getEnrolleeOverLapCountForNewEnrollment(@Param("houseHoldCaseId") String houseHoldCaseId, 
			@Param("insuranceType") String insuranceType,  
			@Param("startDateProposed") String startDateProposed, 
			@Param("endDateProposed") String endDateProposed,
			@Param("memberId") String memberId
			);

@Query(" SELECT count(*) FROM Enrollee as en " +
		   " WHERE en.enrollment.employeeAppId = :employeeAppId " +
		   " AND en.enrollment.insuranceTypeLkp.lookupValueCode= :insuranceType "+
		   " AND en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM','TERM','PAYMENT_RECEIVED') "+
		   " AND TO_DATE(:startDateProposed,'MM/DD/YYYY')  <= TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND TO_DATE(:endDateProposed,'MM/DD/YYYY')  >= TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND en.exchgIndivIdentifier = :memberId"+
		   " AND en.enrollment.id <> :enrollmentId"
		   )
Long getEnrolleeOverLapCountForExistingShopEnrollment(@Param("employeeAppId") Long employeeAppId, 
			@Param("insuranceType") String insuranceType,  
			@Param("startDateProposed") String startDateProposed, 
			@Param("endDateProposed") String endDateProposed,
			@Param("memberId") String memberId,
			@Param("enrollmentId") Integer enrollmentId);

@Query(" SELECT count(*) FROM Enrollee as en " +
		   " WHERE en.enrollment.employeeAppId = :employeeAppId " +
		   " AND en.enrollment.insuranceTypeLkp.lookupValueCode= :insuranceType "+
		   " AND en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM','TERM','PAYMENT_RECEIVED') "+
		   " AND TO_DATE(:startDateProposed,'MM/DD/YYYY')  <= TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND TO_DATE(:endDateProposed,'MM/DD/YYYY')  >= TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND en.exchgIndivIdentifier = :memberId"+
		   " AND en.enrollment.id = :enrollmentId"+
		   " AND en.id <> :enrolleeId"
		   )
Long getOverLapCountForExistingShopEnrollmentNewEnrollees(@Param("employeeAppId") Long employeeAppId, 
			@Param("insuranceType") String insuranceType,  
			@Param("startDateProposed") String startDateProposed, 
			@Param("endDateProposed") String endDateProposed,
			@Param("memberId") String memberId,
			@Param("enrollmentId") Integer enrollmentId,
			@Param("enrolleeId") Integer enrolleeId);

@Query(" SELECT count(*) FROM Enrollee as en " +
		   " WHERE en.enrollment.employeeAppId = :employeeAppId " +
		   " AND en.enrollment.insuranceTypeLkp.lookupValueCode= :insuranceType "+
		   " AND en.enrolleeLkpValue.lookupValueCode IN ('PENDING','CONFIRM','TERM','PAYMENT_RECEIVED') "+
		   " AND TO_DATE(:startDateProposed,'MM/DD/YYYY')  <= TO_DATE(TO_CHAR(en.effectiveEndDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND TO_DATE(:endDateProposed,'MM/DD/YYYY')  >= TO_DATE(TO_CHAR(en.effectiveStartDate,'MM/DD/YYYY'),'MM/DD/YYYY')  "+
		   " AND en.exchgIndivIdentifier = :memberId"
		   )
Long getEnrolleeOverLapCountForNewShopEnrollment(@Param("employeeAppId") Long employeeAppId, 
			@Param("insuranceType") String insuranceType,  
			@Param("startDateProposed") String startDateProposed, 
			@Param("endDateProposed") String endDateProposed,
			@Param("memberId") String memberId
			);

@Query("   SELECT DISTINCT enrlee.id, " 
		   + " enrlee.healthCoveragePolicyNo, "  
			   + " enrlee.enrollment.enrollmentStatusLkp.lookupValueCode, "  
			   + " enrlee.enrollment.enrollmentStatusLkp.lookupValueLabel, "  
			   + " enrlee.enrollment.insuranceTypeLkp.lookupValueCode, " 
			   + " enrlee.enrollment.enrollmentTypeLkp.lookupValueCode, " 
			   + " enrlee.enrollment.planName, "
			   + " enrlee.enrollment.CMSPlanID, " 
			   + " enrlee.enrollment.insurerName, "  
			   + " enrlee.firstName, "
			   + " enrlee.middleName, " 
			   + " enrlee.lastName, "
			   + " enrlee.birthDate, "
			   + " SUBSTR(enrlee.taxIdNumber, 6), "
			   + " enrlee.enrollment.exchgSubscriberIdentifier,"
			   + " enrlee.enrollment.id,"
			   + " enrlee.effectiveStartDate, "  
			   + " enrlee.enrollment.employer.id, "  
			   + " enrlee.enrollment.employer.name, "  
			   + " enrlee.enrollment.benefitEffectiveDate, "  
			   + " enrlee.enrollment.benefitEndDate "
	+ " FROM Enrollee enrlee " 
	+ " WHERE enrlee.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " 
	
	+ " AND enrlee.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT'  "
	
	+ " AND enrlee.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' "
	
	+ " AND (    ((:healthCoveragePolicyNo IS NOT NULL) AND (enrlee.healthCoveragePolicyNo =:healthCoveragePolicyNo)) "
	+ "       OR (( :healthCoveragePolicyNo IS NULL) AND ((enrlee.healthCoveragePolicyNo IS NULL) "
	+ "       OR (enrlee.healthCoveragePolicyNo IS NOT NULL)) ) )   "
	
	+ " AND(     (( (:statusLkpList) IS NOT NULL) AND (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IN (:statusLkpList) )) " 
	+ "       OR (( (:statusLkpList) IS NULL) AND ((enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NULL) "
	+ "       OR (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NOT NULL) ) ) )  "
	
	+ " AND (    ((:plantype IS NOT NULL) AND (enrlee.enrollment.insuranceTypeLkp.lookupValueCode =:plantype)) "
	+ "       OR (( :plantype IS NULL) AND ((enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NOT NULL)))) "
	
	+ " AND (    ((:cmsPlanId IS NOT NULL) AND (enrlee.enrollment.CMSPlanID LIKE '%'||:cmsPlanId||'%' ) ) "
	+ "       OR (( :cmsPlanId IS NULL) AND ((enrlee.enrollment.CMSPlanID IS NULL) OR (enrlee.enrollment.CMSPlanID IS NOT NULL)))) "
	
	+ " AND (    ((:issuer <> 0) AND (enrlee.enrollment.issuerId =:issuer)) "
	+ "       OR (( :issuer = 0) AND (enrlee.enrollment.issuerId > 0))) "
	
	+ " AND (    ((:employerName IS NOT NULL) AND (enrlee.enrollment.employer.name =:employerName)) " 
	+ "       OR (( :employerName IS NULL) AND ((enrlee.enrollment.employer.name IS NULL) OR (enrlee.enrollment.employer.name IS NOT NULL)))) "
	
	+ " AND (    ((:name IS NOT NULL) AND ( LOWER(enrlee.firstName) ||' '||LOWER(enrlee.lastName)  LIKE '%' || LOWER(:name) || '%')  "
	+ "       OR ( ( :name IS NULL) AND ((enrlee.firstName IS NULL) OR (enrlee.firstName IS NOT NULL)) )) " 
 + "       OR (( :name IS NULL) AND ((enrlee.lastName IS NULL) OR (enrlee.lastName IS NOT NULL))) ) "
	
	+ " AND (    ((:enrollmentTypeLkp IS NOT NULL) AND (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode =:enrollmentTypeLkp)) " 
	+ "       OR ((:enrollmentTypeLkp IS NULL) AND ((enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NULL) "
 + "       OR (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NOT NULL) ))) "
	
	+ " AND (    ((:subscriberId IS NOT NULL) AND ( (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') AND (enrlee.exchgIndivIdentifier  =:subscriberId) ) "
	+ "       OR (( :subscriberId IS NULL ) AND ( (enrlee.exchgIndivIdentifier IS NULL) OR (enrlee.exchgIndivIdentifier IS NOT NULL))) ))"
	
	+ " AND (    ((:last4DigitSSN IS NOT NULL) AND (SUBSTR(enrlee.taxIdNumber, 6)  =:last4DigitSSN)) "
	+ "       OR (( :last4DigitSSN IS NULL ) AND ( (enrlee.taxIdNumber IS NULL) OR (enrlee.taxIdNumber IS NOT NULL))) )"
	
	+ " AND (    ((:dOBofSubscriber IS NOT NULL) AND ( (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') AND TO_CHAR(enrlee.birthDate, 'MM/DD/YYYY')  =:dOBofSubscriber) ) "
	+ "       OR (( :dOBofSubscriber IS NULL ) AND ( (enrlee.birthDate IS NULL) OR (enrlee.birthDate IS NOT NULL))) )"
	
	+ " AND (    ((:applicableYear IS NOT NULL) AND (TO_CHAR(enrlee.effectiveStartDate, 'YYYY')  =:applicableYear)) "
	+ "       OR (( :applicableYear IS NULL ) AND ( (enrlee.effectiveStartDate IS NULL) OR (enrlee.effectiveStartDate IS NOT NULL))) )"
	
	+" AND    (((:policyId <> 0) AND (enrlee.enrollment.id =:policyId)) "
	+"     OR ( ( :policyId = 0) AND (enrlee.enrollment.id > 0) )) "
	
	+ "AND (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') " )
Page<Object[]> getShopSubscriberByIssuerData(@Param("healthCoveragePolicyNo") String healthCoveragePolicyNo,
										   @Param("statusLkpList") List<String> statusLkpList,
										   @Param("plantype") String plantype,
										   @Param("cmsPlanId") String cmsPlanId,
										   @Param("issuer") int issuer,
										   @Param("employerName") String employerName, 
										   @Param("name") String name,
										   @Param("enrollmentTypeLkp") String enrollmentTypeLkp,										  
										   @Param("subscriberId") String subscriberId, 
										   @Param("last4DigitSSN") String last4DigitSSN,
										   @Param("dOBofSubscriber") String dOBofSubscriber, 
										   @Param("applicableYear") String applicableYear,
										   @Param("policyId") int policyId,
										   Pageable pageable);

@Query(" SELECT DISTINCT enrlee.id, " 
		  +" enrlee.healthCoveragePolicyNo, " 
		  +" enrlee.enrollment.enrollmentStatusLkp.lookupValueCode, " 
		  +" enrlee.enrollment.enrollmentStatusLkp.lookupValueLabel, " 
		  +" enrlee.enrollment.insuranceTypeLkp.lookupValueLabel, " 
		  +" enrlee.enrollment.enrollmentTypeLkp.lookupValueCode, " 
		  +" enrlee.enrollment.planName, " 
		  +" enrlee.enrollment.CMSPlanID, " 
		  +" enrlee.enrollment.insurerName, " 
		  +" enrlee.firstName, " 
		  +" enrlee.middleName, " 
		  +" enrlee.lastName, "
		  +" enrlee.birthDate, "
		  +" SUBSTR(enrlee.taxIdNumber, 6), "
		  +" enrlee.enrollment.exchgSubscriberIdentifier,"
		  +" enrlee.enrollment.id,"
		  +" enrlee.effectiveStartDate, " 
		  +" enrlee.enrollment.benefitEffectiveDate, " 
		  +" enrlee.enrollment.benefitEndDate " 
	+" FROM Enrollee enrlee " 
	+" WHERE enrlee.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' "
	
	+" AND enrlee.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' "
	
	+" AND enrlee.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' "
	
	+" AND    (((:healthCoveragePolicyNo IS NOT NULL) AND (enrlee.healthCoveragePolicyNo =:healthCoveragePolicyNo)) " 
	+"     OR (( :healthCoveragePolicyNo IS NULL) AND ((enrlee.healthCoveragePolicyNo IS NULL) OR (enrlee.healthCoveragePolicyNo IS NOT NULL)) ) ) "
	
	+" AND    ((( (:statusLkpList) IS NOT NULL) AND (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IN (:statusLkpList) )) " 
	+"     OR (( (:statusLkpList) IS NULL) AND ((enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NULL) "
	+"     OR (enrlee.enrollment.enrollmentStatusLkp.lookupValueCode IS NOT NULL)) ) ) "
	
	+" AND    (((:plantype IS NOT NULL) AND (enrlee.enrollment.insuranceTypeLkp.lookupValueCode =:plantype)) " 
	+"     OR (( :plantype IS NULL) AND ((enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NULL) OR (enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NOT NULL)) ) ) "
	
	+ " AND (    ((:cmsPlanId IS NOT NULL) AND (enrlee.enrollment.CMSPlanID LIKE '%'||:cmsPlanId||'%') ) "
	+ "       OR (( :cmsPlanId IS NULL) AND ((enrlee.enrollment.CMSPlanID IS NULL) OR (enrlee.enrollment.CMSPlanID IS NOT NULL)))) "
	
	+" AND    (((:issuer <> 0) AND (enrlee.enrollment.issuerId =:issuer)) "
	+"     OR ( ( :issuer = 0) AND (enrlee.enrollment.issuerId > 0) )) "
	
	+" AND    (((:name IS NOT NULL) AND ( LOWER(enrlee.firstName) ||' '||LOWER(enrlee.lastName)  LIKE '%' || LOWER(:name) || '%') "  
	+"     OR (( :name IS NULL) AND ((enrlee.firstName IS NULL) OR (enrlee.firstName IS NOT NULL)) )) "
	+"     OR (( :name IS NULL) AND ((enrlee.lastName IS NULL) OR (enrlee.lastName IS NOT NULL)) )) "
	
	+" AND    (((:enrollmentTypeLkp IS NOT NULL) AND (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode =:enrollmentTypeLkp)) " 
	+"     OR ((:enrollmentTypeLkp IS NULL) AND ((enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NULL) "
	+"     OR (enrlee.enrollment.enrollmentTypeLkp.lookupValueCode IS NOT NULL) )) )"
	
	+ " AND (    ((:subscriberId IS NOT NULL) AND ( (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') AND (enrlee.exchgIndivIdentifier  =:subscriberId) ) "
	+ "       OR (( :subscriberId IS NULL ) AND ( (enrlee.exchgIndivIdentifier IS NULL) OR (enrlee.exchgIndivIdentifier IS NOT NULL))) ))"
	
	+ " AND (    ((:last4DigitSSN IS NOT NULL) AND (SUBSTR(enrlee.taxIdNumber, 6)  =:last4DigitSSN)) "
	+ "       OR (( :last4DigitSSN IS NULL ) AND ( (enrlee.taxIdNumber IS NULL) OR (enrlee.taxIdNumber IS NOT NULL))) )"
	
	+ " AND (    ((:dOBofSubscriber IS NOT NULL) AND ( (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') AND TO_CHAR(enrlee.birthDate, 'MM/DD/YYYY')  =:dOBofSubscriber) ) "
	+ "       OR (( :dOBofSubscriber IS NULL ) AND ( (enrlee.birthDate IS NULL) OR (enrlee.birthDate IS NOT NULL))) )"
	
	+ " AND (    ((:applicableYear IS NOT NULL) AND (TO_CHAR(enrlee.effectiveStartDate, 'YYYY')  =:applicableYear)) "
	+ "       OR (( :applicableYear IS NULL ) AND ( (enrlee.effectiveStartDate IS NULL) OR (enrlee.effectiveStartDate IS NOT NULL))) )"
	
	+" AND    (((:insuranceType IS NOT NULL) AND (enrlee.enrollment.insuranceTypeLkp.lookupValueCode =:insuranceType)) " 
	+"     OR ((:insuranceType IS NULL) AND ((enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NULL) "
	+"     OR (enrlee.enrollment.insuranceTypeLkp.lookupValueCode IS NOT NULL) )) )"
	
	+" AND    (((:policyId <> 0) AND (enrlee.enrollment.id =:policyId)) "
	+"     OR ( ( :policyId = 0) AND (enrlee.enrollment.id > 0) ) ) "
	
	+ "AND (enrlee.personTypeLkp.lookupValueCode='SUBSCRIBER') " )
Page<Object[]> getIndividualSubscriberByIssuerData(@Param("healthCoveragePolicyNo") String healthCoveragePolicyNo,
									   @Param("statusLkpList") List<String> statusLkpList,
									   @Param("plantype") String plantype,
									   @Param("cmsPlanId") String cmsPlanId,
									   @Param("issuer") int issuer, 
									   @Param("name") String name, 
									   @Param("enrollmentTypeLkp") String enrollmentTypeLkp,
									   @Param("subscriberId") String subscriberId, 
									   @Param("last4DigitSSN") String last4DigitSSN,
									   @Param("dOBofSubscriber") String dOBofSubscriber, 
									   @Param("applicableYear") String applicableYear,
									   @Param("policyId") int policyId,
									   @Param("insuranceType") String insuranceType,
									   Pageable pageable);

	@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +
		" AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
		" AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT' " +
		" AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT' " +
		" AND en.enrolleeLkpValue.lookupValueCode NOT IN ('ABORTED')")
	List<Enrollee> getEnrolleesForEnrollmentID(@Param("enrollmentID") Integer enrollmentID);
	
	
	@Query(" SELECT distinct exchgIndivIdentifier FROM Enrollee as ee WHERE ee.enrollment.ssapApplicationid = :ssapId AND ee.enrollment.insuranceTypeLkp.lookupValueCode='HLT' "
			+ " AND ee.enrollment.priorSsapApplicationid = :ssapId "
			+ " AND ee.personTypeLkp.lookupValueCode IN ('ENROLLEE', 'SUBSCRIBER') "
			+ " AND (ee.enrolleeLkpValue.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM') "
			+ "			OR (ee.enrolleeLkpValue.lookupValueCode IN ('TERM')"
			+ "					AND ee.effectiveEndDate>= :sixtyDayBackDate))")
	List<String> getMemberIdBySsapId(@Param("ssapId") Long ssapId, @Param("sixtyDayBackDate") Date sixtyDayBackDate);
	
	@Query(" FROM Enrollee as en WHERE en.enrollment.id = :enrollmentID " +			
			" AND en.personTypeLkp.lookupValueCode = :personType " )
	List<Enrollee> getEnrolleesByPersonTypeForEnrollment(@Param("enrollmentID") Integer enrollmentID, @Param("personType") String personType);

	@Query(" SELECT distinct exchgIndivIdentifier FROM Enrollee as ee WHERE ee.enrollment.ssapApplicationid = :ssapId AND ee.enrollment.insuranceTypeLkp.lookupValueCode= :insuranceType "
			+ " AND ee.personTypeLkp.lookupValueCode IN ('ENROLLEE', 'SUBSCRIBER') "
			+ " AND ee.enrolleeLkpValue.lookupValueCode IN ('PENDING','PAYMENT_RECEIVED','CONFIRM', 'TERM') ")
	List<String> getDistinctMemberIdBySsapIdAndType(@Param("ssapId") Long ssapApplicationId, @Param("insuranceType") String insuranceType);
	
	@Query("SELECT ee.firstName, ee.lastName, ee.effectiveStartDate, ee.effectiveEndDate, ee.enrollment.id, ee.healthCoveragePolicyNo,"
			+ " 'Self', loc.address1, loc.address2, loc.city, loc.state, loc.zip, loc.county, loc.countycode,"
			+ " ee.primaryPhoneNo, ee.preferredEmail, ee.exchgIndivIdentifier, ee.suffix, ee.middleName, ee.taxIdNumber,"
			+ " ee.birthDate, genLkp.lookupValueCode, genLkp.lookupValueLabel, ee.id, "
			+ " tobaccoLkp.lookupValueLabel, "
			+ " mailingLoc.address1, mailingLoc.address2, mailingLoc.city, mailingLoc.state, mailingLoc.zip, mailingLoc.county, mailingLoc.countycode, "
			+ " ee.ratingArea, "
			+ " ee.ratingAreaEffDate, "
			+ " hcpLkp.lookupValueLabel, "
			+ " ee.age, "
			+ " '18', "
			+ " tobaccoLkp.lookupValueCode, "
			+ " hcpLkp.lookupValueCode, "
			+ " ee.personTypeLkp.lookupValueCode, ee.enrolleeLkpValue.lookupValueCode, "
			+ " ee.quotingDate "
			+ " FROM Enrollee as ee "
			+ " LEFT JOIN ee.homeAddressid as loc "
			+ " LEFT JOIN ee.mailingAddressId as mailingLoc "
			+ " LEFT JOIN ee.genderLkp as genLkp "
			+ " LEFT JOIN ee.relationshipToHCPLkp as hcpLkp "
			+ " LEFT JOIN ee.tobaccoUsageLkp as tobaccoLkp "
			+ " WHERE "
			+ " ee.enrollment.id = :enrollmentId "
			+ " AND ee.enrolleeLkpValue.lookupValueCode NOT IN ('ABORTED') "
			+ " AND ee.personTypeLkp.lookupValueCode = 'SUBSCRIBER'")
	List<Object[]> getSubscriberDataByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);

	@Query("SELECT new com.getinsured.hix.dto.enrollment.QhpReportDTO(ee.enrollment.externalHouseHoldCaseId, ee.enrollment.CMSPlanID, "
			+ "ee.enrollment.insuranceTypeLkp.lookupValueLabel, cast(TO_CHAR(ee.enrollment.benefitEffectiveDate, 'YYYY') as integer), ee.enrollment.id, "
			+ "ee.enrollment.enrollmentStatusLkp.lookupValueCode, ee.enrollment.planName, ee.enrollment.planLevel, ee.enrollment.insurerName, "
			+ "ee.enrollment.assisterBrokerId, ee.enrollment.brokerRole, ee.exchgIndivIdentifier, ee.effectiveStartDate, ee.effectiveEndDate, "
			+ "ee.enrolleeLkpValue.lookupValueCode, ee.personTypeLkp.lookupValueCode) "
			+ "FROM Enrollee as ee "
			+ "WHERE ee.enrollment.id IN (:enrollmentId) "
			+ "AND ee.enrolleeLkpValue.lookupValueCode IN ('CONFIRM', 'PENDING') "
			+ "AND ee.enrollment.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM', 'PENDING') "
			+ "AND ee.personTypeLkp.lookupValueCode IN ('ENROLLEE', 'SUBSCRIBER') "
			+ "ORDER BY ee.enrollment.id, ee.id ASC")
	List<QhpReportDTO> getEnrollmentAndEnrolleeDataToGenerateQHPReports(@Param("enrollmentId") Integer enrollmentId);

	@Query(" SELECT en.id, en.exchgIndivIdentifier, en.effectiveStartDate, en.quotingDate, en.age " +
			" FROM Enrollee as en " +
			" WHERE en.enrollment.id = :enrollmentID " +
			" AND en.effectiveEndDate >= :changeEffectiveDate " +
			" AND en.personTypeLkp.lookupValueCode <>'RESPONSIBLE_PERSON' " +
			" AND en.personTypeLkp.lookupValueCode <>'HOUSEHOLD_CONTACT'  " +
			" AND en.personTypeLkp.lookupValueCode <>'CUSTODIAL_PARENT'")
	List<Object[]> getExchgIndivIdentifierAndEffectiveStartDateByEnrollmentIDAndChangeDate(@Param("enrollmentID") Integer enrollmentID, @Param("changeEffectiveDate") Date changeEffectiveDate);

}
