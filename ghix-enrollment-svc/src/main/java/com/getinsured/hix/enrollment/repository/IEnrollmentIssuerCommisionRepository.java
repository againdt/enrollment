package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentIssuerCommision;
import com.getinsured.hix.platform.repository.TenantAwareRepository;

public interface IEnrollmentIssuerCommisionRepository
		extends TenantAwareRepository<EnrollmentIssuerCommision, Integer> {

	@Query("SELECT en.nonCommissionFlag "
			+ " FROM EnrollmentIssuerCommision as en "
			+ " WHERE en.enrollment.id = :enrollmentId")
	String getNonComFlagByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
}
