package com.getinsured.hix.enrollment.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.SecurableTarget;
import com.getinsured.hix.model.SecurableTarget.TargetName;
import com.getinsured.hix.model.Securables;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.ISecurableTargetRepository;
import com.getinsured.hix.platform.security.service.UserService;

/**
 * 
 * @author rajaramesh_g
 *
 */
public final class EnrollmentUtilsForCap {
	
	/**
	 * Private constructor.
	 * Utility classes should not have public constructor, Added to hide implicit public constructor.
	 * 
	 */
	private EnrollmentUtilsForCap(){

	}
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentUtilsForCap.class);
	
	
	//rename to setCapAgentId
		public static void setCapAgentId(Enrollment enrollment, UserService userService) {
			AccountUser user;
			LookupValue enrollmentStatus  = null==enrollment?null:enrollment.getEnrollmentStatusLkp();
			try {
				//If CAP_AGENT_ID exists, bail
				if((enrollment == null) || (userService == null) || (null != enrollment.getCapAgentId() && enrollment.getCapAgentId() > 0 ))
				{
					if(null!=enrollmentStatus && !enrollmentStatus.getLookupValueCode().equalsIgnoreCase("ECOMMITTED")){
						return;	
					}
					
				}
	 			user = userService.getLoggedInUser();
			
				//Ensure that the CAP_AGENT_ID is not already set.
				if (null != user ) {
					Role role = userService.getDefaultRole(user);
					if (null != role && (role.getName().equalsIgnoreCase("CSR") || role.getName().equalsIgnoreCase("REMOTE_AGENT"))) {
						enrollment.setCapAgentId(user.getId());
					}
				}
			} catch (InvalidUserException e) {
				LOGGER.error("Could not set CAP_AGENT_ID for enrollment Id:"+enrollment.getId() +", Exception:"+e.getMessage());
			}
		}
		
		/*public static void setCapAgentId(Enrollment enrollment, UserService userService, ISecurablesRepository iSecurablesRepository) {
			AccountUser user = null;
			if((enrollment == null) || (userService == null) ||(iSecurablesRepository == null)|| (null != enrollment.getCapAgentId() && enrollment.getCapAgentId() > 0 ))
			{
				return;
			}
			try {
				user = userService.getLoggedInUser();
				if(null != user){
					Securables securable = iSecurablesRepository.findExistedFfmUserInfoByTargetId(""+user.getId());
					if(null != securable && EnrollmentConstants.N.equalsIgnoreCase(securable.getApplicationData())){
						enrollment.setCapAgentId(user.getId());
					}
				}
			} catch (InvalidUserException e) {
				LOGGER.error("Could not set CAP_AGENT_ID for enrollment Id:"+enrollment.getId() +", Exception:"+e.getMessage() , e);
			}
		}*/
		
		public static void setCapAgentId(Enrollment enrollment, UserService userService, ISecurableTargetRepository iSecurableTargetRepository) {
			AccountUser user = null;
			if((enrollment == null) || (userService == null) ||(iSecurableTargetRepository == null)|| (null != enrollment.getCapAgentId() && enrollment.getCapAgentId() > 0 ))
			{
				return;
			}
			try {
				user = userService.getLoggedInUser();
				if(null != user){
					SecurableTarget securableTarget = iSecurableTargetRepository.findByTargetIdAndTargetType(user.getId(), TargetName.USER);
					if(null != securableTarget && null != securableTarget.getSecurables()){
						Securables securable = securableTarget.getSecurables().get(0);
						if(null != securable && EnrollmentConstants.N.equalsIgnoreCase(securable.getApplicationData())){
							enrollment.setCapAgentId(user.getId());
						}
					}
					
				}
			} catch (InvalidUserException e) {
				LOGGER.error("Could not set CAP_AGENT_ID for enrollment Id:"+enrollment.getId() +", Exception:"+e.getMessage() , e);
			}
		}
		
		public static void setCapAgentId(Enrollment enrollment, Integer ffmLoggedInUserId, ISecurableTargetRepository iSecurableTargetRepository) {

			LOGGER.info("Setting Cap Agent Id : Logged in UserID : " + ffmLoggedInUserId);
			
			if((enrollment == null) || (ffmLoggedInUserId == null) ||(iSecurableTargetRepository == null)|| (null != enrollment.getCapAgentId() && enrollment.getCapAgentId() > 0 ))
			{
				LOGGER.info("One or more required input parameters are are null");
				return;
			}
			try {
					SecurableTarget securableTarget = iSecurableTargetRepository.findByTargetIdAndTargetType(ffmLoggedInUserId, TargetName.USER);
					if(null != securableTarget && null != securableTarget.getSecurables()){
						Securables securable = securableTarget.getSecurables().get(0);
						if(null != securable && !(EnrollmentConstants.Y.equalsIgnoreCase(securable.getApplicationData()))){
							enrollment.setCapAgentId(ffmLoggedInUserId);
						} else {
							LOGGER.info("Application Data is Y :: "+ securableTarget.getId());
						}
					} else {
						LOGGER.info("Securable Target does not exist for the given user ID :: " + ffmLoggedInUserId);
					}
					
			} catch (Exception e) {
				LOGGER.error("Could not set CAP_AGENT_ID for enrollment Id:"+enrollment.getId() +", Exception:"+e.getMessage() , e);
			}
		}
}
