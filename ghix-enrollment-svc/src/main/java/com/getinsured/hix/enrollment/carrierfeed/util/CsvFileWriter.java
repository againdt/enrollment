package com.getinsured.hix.enrollment.carrierfeed.util;

import com.fasterxml.jackson.databind.JsonNode;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jabelardo on 7/10/14.
 */
public class CsvFileWriter<T> extends FileWriter<T> {

    @Override
    protected void write(OutputStream outputStream, Iterator<T> inputIterator) throws IOException {
        ICsvBeanWriter beanWriter = null;
        try {
            beanWriter = new CsvBeanWriter(new OutputStreamWriter(outputStream), CsvPreference.STANDARD_PREFERENCE);

            String[] headers = getHeaders();

            int firstDataRow = getMappings().get("firstDataRow").asInt();
            if (firstDataRow > 0) {
                int skipRowCount = 1;
                while (skipRowCount++ < firstDataRow) {
                    beanWriter.writeComment("");
                }
                beanWriter.writeHeader(headers);
            }
            while (inputIterator.hasNext()) {
                T model = inputIterator.next();
                beanWriter.write(model, headers);
            }
        } finally {
            if (beanWriter != null) {
                beanWriter.close();
            }
        }
    }

    private String[] getHeaders() {
        List<String> headers = new ArrayList<>();
        JsonNode fieldNodes = getMappings().get("fields");
        for (JsonNode fieldNode : fieldNodes) {
            String fieldName = fieldNode.get("fieldName").asText();
            headers.add(fieldName);
        }
        return headers.toArray(new String[headers.size()]);
    }
}
