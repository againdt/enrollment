package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentCustomGroupDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemPersonDTO;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GEnrollment;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GHouseHoldContact;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GMember;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GRace;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GRelationship;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GResponsiblePerson;
import com.getinsured.hix.dto.plandisplay.ind71g.Ind71GRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PdOrderResponse;
import com.getinsured.hix.model.plandisplay.PldMemberData;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.model.plandisplay.PldPlanData;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;



@Service("enrollmentCustomGroupingService")
public class EnrollmentCustomGroupingServiceImpl implements EnrollmentCustomGroupingService {
	@Autowired private IEnrolleeRepository enrolleeRepository;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCustomGroupingServiceImpl.class);
	@Override
	public PldOrderResponse getPDOrderData(EnrollmentCustomGroupDTO customGroupingDTO, EnrollmentResponse response) throws Exception{
		PldOrderResponse pldOrderResponse= null;
		
		if( customGroupingDTO.getInd71GRequest()!=null && customGroupingDTO.getInd71GRequest().getEnrollments()!=null && customGroupingDTO.getInd71GRequest().getEnrollments().size()>0){
			pldOrderResponse=new PldOrderResponse();
			PdOrderResponse pdResp= customGroupingDTO.getPdOrderResponse();
			Ind71GRequest customGroupingRequest=customGroupingDTO.getInd71GRequest();
			populateHouseholdLevelData(pldOrderResponse,customGroupingRequest);
			Set<String> planIds= new HashSet<>();
			String marketType=EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL;// FI passed on assumption 
			ArrayList<PldPlanData> planDataList = new ArrayList<PldPlanData>();
			for(PdOrderItemDTO pdOrderItemDTO:pdResp.getPdOrderItemDTOList()){
				String enrollmentId=pdOrderItemDTO.getPreviousEnrollmentId();
				IND71GEnrollment householdEnr= find71GEnrollment(customGroupingRequest, enrollmentId);
				
				if(householdEnr== null){
					populateEnrollmentResponse(response, EnrollmentConstants.ERROR_CODE_211,"Enrollment Id "+enrollmentId +" provided in PDOrderItem is not present in Ind71GRequest" , GhixConstants.RESPONSE_FAILURE);
					LOGGER.error("Enrollment Id "+enrollmentId +" provided in PDOrderItem is not present in Ind71GRequest");
					throw new Exception("EnrollmentCustomGroupingServiceImpl :: Enrollment Id "+enrollmentId +" provided in PDOrderItem is not present in Ind71GRequest");
					
				}

				EnrollmentResponse existingEnrollment= getPlanDetailsByEnrollmentId(Integer.parseInt(householdEnr.getEnrollmentId())); 
				if(existingEnrollment==null){
					populateEnrollmentResponse(response, EnrollmentConstants.ERROR_CODE_212,"Enrollment Id "+enrollmentId +" provided in PDOrderItem is not present in Enrollment"  , GhixConstants.RESPONSE_FAILURE);
					LOGGER.error("Enrollment Id "+enrollmentId +" provided in PDOrderItem is not present in Enrollment" );
					throw new Exception("EnrollmentCustomGroupingServiceImpl :: Enrollment Id "+enrollmentId +" provided in PDOrderItem is not present in Enrollment" );
				}
				
				if(pdOrderItemDTO.getDisenrollAllMembers()) {
					addAllMemberToDisenrollMemberList(existingEnrollment, pldOrderResponse, householdEnr.getMembers());
					continue;
				}
				
				PldPlanData pldPlanData=new PldPlanData();
				Map<String,String> plan = getPlanData(pdOrderItemDTO, householdEnr.getCoverageDate(), marketType);
				pldPlanData.setPlan(plan);
				if(isNotNullAndEmpty(householdEnr.getFinancialEffectiveDate())){
					pldPlanData.setFinancialEffectiveDate(DateUtil.StringToDate(householdEnr.getFinancialEffectiveDate(),"MM/dd/yyyy"));
				}
				pldPlanData.setTerminationFlag(existingEnrollment.isTerminationFlag());
				formPldPlanData(pdOrderItemDTO, pldOrderResponse,   pldPlanData, householdEnr, existingEnrollment);
				planIds.add(pdOrderItemDTO.getPlanId()+"");
				//if(!(isNotNullAndEmpty(pldPlanData.getExistingMemberInfoList())&& isNotNullAndEmpty(pldPlanData.getNewMemberInfoList()))) {
				planDataList.add(pldPlanData);
				//}
			}
			pldOrderResponse.setPlans(planIds);
			pldOrderResponse.setMarketType(marketType);
			pldOrderResponse.setPlanDataList(planDataList);
		}
		
		return pldOrderResponse;
	}
	
	private IND71GEnrollment find71GEnrollment(Ind71GRequest customGroupingRequest, String existingEnrollmentID){
		for(IND71GEnrollment enrRequest:customGroupingRequest.getEnrollments()){
			if(enrRequest.getEnrollmentId().equals(existingEnrollmentID)){
				return enrRequest;
			}
		}
		return null;
		
	}
	
	private void populateHouseholdLevelData(PldOrderResponse pldOrderResponse, Ind71GRequest customGroupingRequest){
		pldOrderResponse.setApplicationId(Long.valueOf(customGroupingRequest.getApplicationId()));
		pldOrderResponse.setApplicationType(customGroupingRequest.getEnrollmentType());
		pldOrderResponse.setHouseholdCaseId(customGroupingRequest.getHouseholdCaseId());
		pldOrderResponse.setUserRoleId(customGroupingRequest.getUserRoleId());
		pldOrderResponse.setUserRoleType(customGroupingRequest.getUserRoleType());
		pldOrderResponse.setEnrollmentType(customGroupingRequest.getEnrollmentType().charAt(0));
		//Populate HouseholdContact And ResponsiblePerson
		populateResponsiblePersonInfo( pldOrderResponse,  customGroupingRequest);
		populateHouseholdContactInfo(pldOrderResponse,customGroupingRequest);
		pldOrderResponse.setSadpFlag("NO");// as copied from PDHandshakeService
		if(isNotNullAndEmpty(customGroupingRequest.getAllowChangePlan())) {
			pldOrderResponse.setAllowChangePlan(customGroupingRequest.getAllowChangePlan());
		}else {
			pldOrderResponse.setAllowChangePlan(EnrollmentConstants.N);
		}

		if((null != customGroupingRequest.getEhbAmount() && customGroupingRequest.getEhbAmount() != 0.0f)
				&& null == pldOrderResponse.getEhbAmount()) {
			pldOrderResponse.setEhbAmount(customGroupingRequest.getEhbAmount());					
		}
		
	}

	private void populateResponsiblePersonInfo(PldOrderResponse pldOrderResponse, Ind71GRequest customGroupingRequest) {
		if (customGroupingRequest.getResponsiblePerson() != null) {
			IND71GResponsiblePerson customResponsible = customGroupingRequest.getResponsiblePerson();
			Map<String, String> responsiblePersonMap = new HashMap<>();
			
			responsiblePersonMap.put(EnrollmentConstants.RESPONSIBLE_PERSON_ID, customResponsible.getResponsiblePersonId());
			responsiblePersonMap.put("responsiblePersonFirstName", customResponsible.getResponsiblePersonFirstName());
			responsiblePersonMap.put("responsiblePersonMiddleName", customResponsible.getResponsiblePersonMiddleName());
			responsiblePersonMap.put("responsiblePersonLastName", customResponsible.getResponsiblePersonLastName());
			responsiblePersonMap.put("responsiblePersonSsn", customResponsible.getResponsiblePersonSsn());
			responsiblePersonMap.put("responsiblePersonSuffix", customResponsible.getResponsiblePersonSuffix());
			responsiblePersonMap.put("responsiblePersonPreferredEmail", customResponsible.getResponsiblePersonPreferredEmail());
			responsiblePersonMap.put("responsiblePersonPreferredPhone", customResponsible.getResponsiblePersonPreferredPhone());
			responsiblePersonMap.put("responsiblePersonPrimaryPhone", customResponsible.getResponsiblePersonPrimaryPhone());
			responsiblePersonMap.put("responsiblePersonSecondaryPhone", customResponsible.getResponsiblePersonSecondaryPhone());

			responsiblePersonMap.put("responsiblePersonHomeAddress1", customResponsible.getResponsiblePersonHomeAddress1());
			responsiblePersonMap.put("responsiblePersonHomeAddress2", customResponsible.getResponsiblePersonHomeAddress2());
			responsiblePersonMap.put("responsiblePersonHomeCity", customResponsible.getResponsiblePersonHomeCity());
			responsiblePersonMap.put("responsiblePersonHomeState", customResponsible.getResponsiblePersonHomeState());
			responsiblePersonMap.put("responsiblePersonHomeZip", customResponsible.getResponsiblePersonHomeZip());	
			
			pldOrderResponse.setResponsiblePerson(responsiblePersonMap);
		}

	}
		

	private void populateHouseholdContactInfo(PldOrderResponse pldOrderResponse, Ind71GRequest customGroupingRequest) {
		if(customGroupingRequest.getHouseHoldContact()!=null){
			IND71GHouseHoldContact customHousehold=customGroupingRequest.getHouseHoldContact();
			Map<String, String> householdContactMap= new HashMap<>();
			householdContactMap.put(EnrollmentConstants.HOUSEHOLD_CONTACT_ID, customHousehold.getHouseHoldContactId());
			householdContactMap.put("houseHoldContactFirstName", customHousehold.getHouseHoldContactFirstName());
			householdContactMap.put("houseHoldContactMiddleName", customHousehold.getHouseHoldContactMiddleName());
			householdContactMap.put("houseHoldContactLastName", customHousehold.getHouseHoldContactLastName());
			householdContactMap.put("houseHoldContactSuffix", customHousehold.getHouseHoldContactSuffix());
			householdContactMap.put("houseHoldContactPreferredEmail", customHousehold.getHouseHoldContactPreferredEmail());
			householdContactMap.put("houseHoldContactPreferredPhone", customHousehold.getHouseHoldContactPreferredPhone());
			householdContactMap.put("houseHoldContactPrimaryPhone", customHousehold.getHouseHoldContactPrimaryPhone());
			householdContactMap.put("houseHoldContactSecondaryPhone", customHousehold.getHouseHoldContactSecondaryPhone());
			householdContactMap.put("houseHoldContactFederalTaxIdNumber", customHousehold.getHouseHoldContactFederalTaxIdNumber());
			
			householdContactMap.put("houseHoldContactHomeAddress1", customHousehold.getHouseHoldContactHomeAddress1());
			householdContactMap.put("houseHoldContactHomeAddress2", customHousehold.getHouseHoldContactHomeAddress2());
			householdContactMap.put("houseHoldContactHomeCity", customHousehold.getHouseHoldContactHomeCity());
			householdContactMap.put("houseHoldContactHomeState", customHousehold.getHouseHoldContactHomeState());
			householdContactMap.put("houseHoldContactHomeZip", customHousehold.getHouseHoldContactHomeZip());	
			pldOrderResponse.setHouseHoldContact(householdContactMap);
			}
			
		}
		
	private Map<String,String> getPlanData(PdOrderItemDTO pdOrderItemDTO, String coverageStartDate, String marketType) {
		Map<String,String> plan = new HashMap<String,String>();
		
		plan.put("orderItemId", ""+pdOrderItemDTO.getId());
		plan.put("planId", ""+pdOrderItemDTO.getPlanId());
		plan.put("planType", pdOrderItemDTO.getInsuranceType().toString());
		plan.put("coverageStartDate", DateUtil.changeFormat(coverageStartDate, "MM/dd/yyyy", "yyyy-MM-dd HH:mm:ss.S"));
		plan.put("netPremiumAmt", ""+pdOrderItemDTO.getPremiumAfterCredit());
		plan.put("grossPremiumAmt", ""+pdOrderItemDTO.getPremiumBeforeCredit());
		plan.put("marketType", marketType);
		plan.put(EnrollmentConstants.MAX_APTC,""+ pdOrderItemDTO.getAggrAptc());
		plan.put(EnrollmentConstants.MAX_STATE_SUBSIDY,""+pdOrderItemDTO.getMaxStateSubsidy());
		if(isNotNullAndEmpty(pdOrderItemDTO.getPdHousehold()) && isNotNullAndEmpty(pdOrderItemDTO.getPdHousehold().getCostSharing())){
			plan.put(EnrollmentConstants.CSR, pdOrderItemDTO.getPdHousehold().getCostSharing().toString());	
		}
		if(pdOrderItemDTO.getMonthlySubsidy() != null) {
			 plan.put("aptc", ""+pdOrderItemDTO.getMonthlySubsidy());
		}

		if(pdOrderItemDTO.getMonthlyStateSubsidy() != null) {
			plan.put("stateSubsidy", ""+pdOrderItemDTO.getMonthlyStateSubsidy());
		}
		
		return plan;
	}
	
	private void formPldPlanData(PdOrderItemDTO pdOrderItemDTO,PldOrderResponse pldOrderResponse, PldPlanData pldPlanData, IND71GEnrollment householdEnr, EnrollmentResponse existingEnrollmentData) throws Exception{
		
		String enrollmentType=pldOrderResponse.getEnrollmentType().toString();
		
		String subscriberId= getSubscriberId(pdOrderItemDTO.getPdOrderItemPersonDTOList());
		String newEnrollmentOnSubscriberChangeFlag =DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.NEW_ENROLLMENT_ON_SUBSCRIBER_CHANGE);
		//check for CS level change
		PlanResponse planResponse= null;
		if( isNotNullAndEmpty(pdOrderItemDTO.getPlanId())){
			planResponse=  enrollmentService.getPlanInfo(pdOrderItemDTO.getPlanId().toString(), EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL);
			pldPlanData.setPlanResponse(planResponse);
		}
		
		if(!existingEnrollmentData.getCmsPlanId().substring(Math.max(existingEnrollmentData.getCmsPlanId().length() - 2, 0)).equalsIgnoreCase(planResponse.getIssuerPlanNumber().substring(Math.max(planResponse.getIssuerPlanNumber().length() - 2, 0)))){
			pldPlanData.setCreateNewEnrollment(EnrollmentConstants.YES);
			pldPlanData.setNewEnrollmentReason(EnrollmentConstants.EnrollmentAppEvent.CS_LEVEL_CHANGE_ENROLLMENT.toString());
			pldPlanData.setQleReason(EnrollmentConstants.QLE_REASON_CS_CHANGE);
			pldPlanData.setExistingEnrollmentId(existingEnrollmentData.getEnrollmentId().toString());
			Date coverageStartDate= DateUtil.StringToDate(householdEnr.getCoverageDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
			String coverageEndDateStr= DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(coverageStartDate), GhixConstants.REQUIRED_DATE_FORMAT);
			addMemberToDisenrollMemberList(existingEnrollmentData.getMemberIds(), existingEnrollmentData, pldOrderResponse, coverageEndDateStr, householdEnr.getMembers(), EnrollmentConstants.EnrollmentAppEvent.CS_LEVEL_CHANGE_DISENROLLMENT.toString());
			//add all members into memberdetails
			List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
			for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()){
				
				PldMemberData pldMemberData=formMemberData(pdOrderItemDTO,  enrollmentType, pdOrderItemPersonDTO, householdEnr.getMembers());
				if(pldMemberData!=null){
					memberInfoList.add(pldMemberData);
				}
			}
			if(!memberInfoList.isEmpty()){
				pldPlanData.setNewMemberInfoList(memberInfoList);
			}
		}else if(EnrollmentUtils.getBooleanFromYesOrYFlag(newEnrollmentOnSubscriberChangeFlag)&&!pldPlanData.isTerminationFlag() && !existingEnrollmentData.getSubscriberMemberId().equalsIgnoreCase(subscriberId)){
			//subscriber changed
			// disenroll all members with termination date as a day before to termination loop
			pldPlanData.setCreateNewEnrollment(EnrollmentConstants.YES);
			pldPlanData.setNewEnrollmentReason(EnrollmentConstants.EnrollmentAppEvent.SUBSCRIBER_CHANGE_ENROLLMENT.toString());
			pldPlanData.setQleReason(EnrollmentConstants.QLE_REASON_SUB_CHANGE);
			pldPlanData.setExistingEnrollmentId(existingEnrollmentData.getEnrollmentId().toString());
			Date coverageStartDate= DateUtil.StringToDate(householdEnr.getCoverageDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
			String coverageEndDateStr= DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(coverageStartDate), GhixConstants.REQUIRED_DATE_FORMAT);
			addMemberToDisenrollMemberList(existingEnrollmentData.getMemberIds(), existingEnrollmentData, pldOrderResponse, coverageEndDateStr, householdEnr.getMembers(), EnrollmentConstants.EnrollmentAppEvent.PLAN_CHANGE_DISENROLLMENT.toString());
			//add all members into memberdetails
			List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
			for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemDTO.getPdOrderItemPersonDTOList()){
				
				PldMemberData pldMemberData=formMemberData(pdOrderItemDTO,  enrollmentType, pdOrderItemPersonDTO, householdEnr.getMembers());
				if(pldMemberData!=null){
					memberInfoList.add(pldMemberData);
				}
			}
			if(!memberInfoList.isEmpty()){
				pldPlanData.setNewMemberInfoList(memberInfoList);
			}
		}else{
			pldPlanData.setCreateNewEnrollment(EnrollmentConstants.NO);
			pldPlanData.setExistingEnrollmentId(existingEnrollmentData.getEnrollmentId().toString());
			//no change plan so compare all members
			//new members add to new member list
			List<PldMemberData> newMemberInfoList = new ArrayList<PldMemberData>();
			List<PldMemberData> existingMemberInfoList = new ArrayList<PldMemberData>();
			for(IND71GMember member :householdEnr.getMembers()){
				if(EnrollmentConstants.Y.equalsIgnoreCase(member.getDisenrollMemberFlag())){
					addMemberToDisenrollMemberList(member.getMemberId(), existingEnrollmentData, pldOrderResponse, member.getTerminationDate(), member, member.getTerminationReasonCode());
				}else{
					PdOrderItemPersonDTO pdOrderItemPersonDTO= getMemberFromPDMemberList(member, pdOrderItemDTO.getPdOrderItemPersonDTOList());
					PldMemberData pldMemberData=formMemberData(pdOrderItemDTO,enrollmentType, pdOrderItemPersonDTO, member);
					if(EnrollmentConstants.Y.equalsIgnoreCase(member.getNewPersonFlag())){
						newMemberInfoList.add(pldMemberData);
						
					}else{
						existingMemberInfoList.add(pldMemberData);
					}
				}
				
			}
			
			if(!newMemberInfoList.isEmpty()){
				pldPlanData.setNewMemberInfoList(newMemberInfoList);
			}
			if(!existingMemberInfoList.isEmpty()){
				pldPlanData.setExistingMemberInfoList(existingMemberInfoList);
			}
			validateIdahoFinancialEffDate( existingEnrollmentData, pldPlanData);
		}
		
			
	}
	private EnrollmentResponse getPlanDetailsByEnrollmentId(Integer enrollmentId){
		EnrollmentResponse enrlResp= null;
			boolean termFlag= false;
			Map<String, Object> enrollmentMap = enrollmentService.getPlanIdAndOrderItemIdByEnrollmentId(enrollmentId);
			if(enrollmentMap!=null && !enrollmentMap.isEmpty()){
				enrlResp= new EnrollmentResponse();
				enrlResp.setEnrollmentId(enrollmentId);
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.ENROLLMENT_STATUS))){
				enrlResp.setEnrollmentStatusLabel((String)enrollmentMap.get(EnrollmentConstants.ENROLLMENT_STATUS));
				if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrlResp.getEnrollmentStatusLabel())){
					termFlag=true;
					enrlResp.setTerminationFlag(termFlag);
				}
			}
			enrlResp.setPlanId((Integer) enrollmentMap.get(EnrollmentConstants.PLAN_ID));
			enrlResp.setOrderItemId((Integer) enrollmentMap.get(EnrollmentConstants.ORDER_ITEM_ID));
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.CMS_PLAN_ID))){
				enrlResp.setCmsPlanId((String)enrollmentMap.get(EnrollmentConstants.CMS_PLAN_ID));
			}
			enrlResp.setSubscriberMemberId((String)enrollmentMap.get(EnrollmentConstants.SUBSCRIBER_MEMBER_ID));
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.APTC))){
				enrlResp.setAptcAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.APTC).toString()));	
			}
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.STATE_SUBSIDY))){
				enrlResp.setStateSubsidyAmt(new BigDecimal(enrollmentMap.get(EnrollmentConstants.STATE_SUBSIDY).toString()));
			}
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.GROSS_PREMIUM_AMT))){
				enrlResp.setGrossPremiumAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.GROSS_PREMIUM_AMT).toString()));	
			}
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.NET_PREMIUM_AMT))){
				enrlResp.setNetPremiumAmt(Float.valueOf(enrollmentMap.get(EnrollmentConstants.NET_PREMIUM_AMT).toString()));	
			}
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.EFFECTIVE_START_DATE))){
				enrlResp.setEffectiveStartDate(DateUtil.StringToDate(enrollmentMap.get(EnrollmentConstants.EFFECTIVE_START_DATE).toString(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
			}
			
			if(isNotNullAndEmpty(enrollmentMap.get(EnrollmentConstants.EFFECTIVE_END_DATE))){
				enrlResp.setEffectiveEndDate(DateUtil.StringToDate(enrollmentMap.get(EnrollmentConstants.EFFECTIVE_END_DATE).toString(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
			}
			
			List<String> memberIds= null;
			if(termFlag){
				memberIds=enrolleeRepository.getExchgIndivIdentifierByEnrollmentID(enrollmentId);
			}else{
				memberIds=enrolleeRepository.getActiveExchgIndivIdentifierByEnrollmentID(enrollmentId);
			}
			
			if(memberIds!=null && !memberIds.isEmpty()){
				enrlResp.setMemberIds(memberIds);
			}
			
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_200);
			enrlResp.setErrMsg(EnrollmentConstants.MSG_RETRIEVE_ENROLLMENT_SUCCESS);
			enrlResp.setStatus(GhixConstants.RESPONSE_SUCCESS);
			LOGGER.info(EnrollmentConstants.MSG_RETRIEVE_ENROLLMENT_SUCCESS);
			}
		return enrlResp;
	}
	private String getSubscriberId(List<PdOrderItemPersonDTO> pdPersonList){
		String subscriberId=null;
			
		if(pdPersonList!=null && !pdPersonList.isEmpty()){
			for(PdOrderItemPersonDTO personDto: pdPersonList){
				if(YorN.Y.equals(personDto.getSubscriberFlag())){
					subscriberId= personDto.getExternalId();
					break;
				}
			}
		}
		return subscriberId;
	}
	
	private void addAllMemberToDisenrollMemberList(EnrollmentResponse existingenrollmentData, PldOrderResponse pldResponse, List<IND71GMember> householdMemberList) {
		if(existingenrollmentData!=null &&pldResponse!=null &&householdMemberList!=null ) {
			for (IND71GMember householdMember:householdMemberList ){
				//IND71GMember householdMember = getMemberFromMemberList(householdMemberList, memberId);
				addMemberToDisenrollMemberList(householdMember.getMemberId() ,  existingenrollmentData,  pldResponse, householdMember.getTerminationDate() ,  householdMember,  householdMember.getTerminationReasonCode());
			}
		}
	}
	
	private void addMemberToDisenrollMemberList(List<String> memberIds , EnrollmentResponse existingenrollmentData, PldOrderResponse pldResponse, String coverageEndDate, List<IND71GMember> householdMemberList, String appEventReason) {
		for (String memberId:memberIds ){
			IND71GMember householdMember = getMemberFromMemberList(householdMemberList, memberId);
			addMemberToDisenrollMemberList(memberId ,  existingenrollmentData,  pldResponse,  coverageEndDate,  householdMember,  appEventReason);
		}

	}
	private void addMemberToDisenrollMemberList(String memberId , EnrollmentResponse existingenrollmentData, PldOrderResponse pldResponse, String coverageEndDate, IND71GMember householdMember, String appEventReason) {
		List<PldMemberData> disenrollMemberInfoList = pldResponse.getDisenrollMemberInfoList();
		if(disenrollMemberInfoList== null ){
			disenrollMemberInfoList= new ArrayList<PldMemberData>();
		}
		boolean found=false;
		if(EnrollmentUtils.isNotNullAndEmpty(memberId)){
			for(PldMemberData disMemberData: disenrollMemberInfoList){
				if(disMemberData!=null&& memberId.equalsIgnoreCase(disMemberData.getMemberInfo().get(EnrollmentConstants.MEMBER_ID)) ){
					found=true;
					Map<String,String> disenrolMbr=disMemberData.getMemberInfo();
					String oldDisEnrollmentDate=disenrolMbr.get("terminationDate");
					if(oldDisEnrollmentDate !=null && coverageEndDate!=null &&  DateUtil.StringToDate(coverageEndDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY).before(DateUtil.StringToDate(oldDisEnrollmentDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY))) {
						disenrolMbr.put("terminationDate", coverageEndDate);
					}
					disMemberData.setMemberInfo(disenrolMbr);
					List<String> enrollmentIds=disMemberData.getEnrollmentIds();
					if(!enrollmentIds.contains(existingenrollmentData.getEnrollmentId().toString())){
						if(isNotNullAndEmpty(appEventReason)){
							disMemberData.setAppEventReason(appEventReason);
						}
						enrollmentIds.add(existingenrollmentData.getEnrollmentId().toString());
						disMemberData.setEnrollmentIds(enrollmentIds);
						if(disMemberData.getRelationshipInfo()==null || disMemberData.getRelationshipInfo().isEmpty()){
							if(householdMember!=null){
								List<Map<String,String>> relationInfoList = createMemberRelationshipInfoList(householdMember.getRelationships());
								disMemberData.setRelationshipInfo(relationInfoList);
							}
						}
					}
					break;
				}
			}
			if(!found){
				List<String> enrollmentIds= new ArrayList<String>();
				PldMemberData pldMemberData= new PldMemberData();
				if(isNotNullAndEmpty(appEventReason)){
					pldMemberData.setAppEventReason(appEventReason);
				}
				Map<String,String> disenrolMbr = new HashMap<String, String>();
				disenrolMbr.put("memberId", memberId);
				
				disenrolMbr.put("terminationDate", coverageEndDate);
				if(householdMember!=null){
					disenrolMbr.put("deathDate", householdMember.getDeathDate());
					List<Map<String,String>> relationInfoList = createMemberRelationshipInfoList(householdMember.getRelationships());
					pldMemberData.setRelationshipInfo(relationInfoList);
					if(isNotNullAndEmpty( householdMember.getMaintenanceReasonCode())){
						disenrolMbr.put("terminationReasonCode", householdMember.getMaintenanceReasonCode());
					}
				}else{
					disenrolMbr.put("terminationReasonCode", EnrollmentConstants.EVENT_REASON_AI);
				}
				
				pldMemberData.setMemberInfo(disenrolMbr);
				
				enrollmentIds.add(existingenrollmentData.getEnrollmentId().toString());
				pldMemberData.setEnrollmentIds(enrollmentIds);
				 
				disenrollMemberInfoList.add(pldMemberData);
			}
		}
		pldResponse.setDisenrollMemberInfoList(disenrollMemberInfoList);
	}
	
	
	private IND71GMember getMemberFromMemberList(List<IND71GMember> householdMembers, String memberId) {
		IND71GMember currentMember = null;
		for(IND71GMember householdMember : householdMembers){
			if(householdMember.getMemberId().equals(memberId)){
				currentMember = householdMember;
			}
		}
		return currentMember;
	}
	public List<Map<String,String>> createMemberRelationshipInfoList(List<IND71GRelationship>  relationshipList) {
		List<Map<String,String>> relationInfoList = new ArrayList<Map<String,String>>();
		if(relationshipList != null && !relationshipList.isEmpty()) {
		for(IND71GRelationship relationship: relationshipList){
				 Map<String,String> relationInfo = new HashMap<String, String>();
				 relationInfo.put("memberId",relationship.getMemberId());
	 	 	 	 relationInfo.put("relationshipCode",relationship.getRelationshipCode());
	 	 	 	 relationInfoList.add(relationInfo);
 	 	}
		return relationInfoList;
		}
		return null;
	}
	
	private PldMemberData formMemberData(PdOrderItemDTO pdOrderItemDTO, String enrollmentType,  PdOrderItemPersonDTO pdOrderItemPersonDTO, List<IND71GMember> householdMemberList){
		IND71GMember householdMember = getMemberFromMemberList(householdMemberList, pdOrderItemPersonDTO);
		if(householdMember ==null || EnrollmentConstants.Y.equalsIgnoreCase(householdMember.getDisenrollMemberFlag())) {
			return null;
		}
		return formMemberData( pdOrderItemDTO,  enrollmentType,   pdOrderItemPersonDTO,  householdMember);
	}
	
	
	private PldMemberData formMemberData(PdOrderItemDTO pdOrderItemDTO, String enrollmentType,  PdOrderItemPersonDTO pdOrderItemPersonDTO, IND71GMember householdMember){
		Map<String,Long> enrollmentPlanMap = null;
		/*if(EnrollmentVal.S.equals(enrollmentType)){
			enrollmentPlanMap = formEnrollmentPlanMap(householdMember, existingQhpPlanId, existingSadpPlanId);
		}*/
		
		Map<String,String> memberInfo = createMemberInfoMapFromPersonData(householdMember, pdOrderItemDTO.getInsuranceType(), enrollmentType, enrollmentPlanMap, pdOrderItemDTO.getPlanId());				
		String subscriberFlag = YorN.Y.equals(pdOrderItemPersonDTO.getSubscriberFlag()) ? "YES" : "NO";
		memberInfo.put("subscriberFlag", subscriberFlag);
		memberInfo.put("ratingArea",""+pdOrderItemPersonDTO.getRegion());
		memberInfo.put("individualPremium",""+pdOrderItemPersonDTO.getPremium());
		memberInfo.put("contribution",""+pdOrderItemPersonDTO.getAppliedSubsidy());
		if(pdOrderItemPersonDTO.getAge()!=null) {
			memberInfo.put("age",""+pdOrderItemPersonDTO.getAge());
		}
		if(pdOrderItemPersonDTO.getPersonCoverageStartDate()!=null) {
			memberInfo.put("quotingDate",DateUtil.dateToString(pdOrderItemPersonDTO.getPersonCoverageStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		}
		
		List<Map<String,String>> raceInfoList = new ArrayList<Map<String,String>>();
		List<IND71GRace> raceList = householdMember.getRace();
		if(raceList!=null && raceList.size()>0){
			for(IND71GRace race: raceList){
				Map<String,String> raceInfo = new HashMap<String,String>();
				raceInfo.put("raceEthnicityCode",race.getRaceEthnicityCode());
				raceInfo.put("description",race.getDescription()); // Optional
				raceInfoList.add(raceInfo);
			}
		}
		List<Map<String,String>> relationInfoList = createMemberRelationshipInfoList(householdMember.getRelationships());        		
		PldMemberData pldMemberData = new PldMemberData();
		pldMemberData.setMemberInfo(memberInfo);
		pldMemberData.setRaceInfo(raceInfoList);
		pldMemberData.setRelationshipInfo(relationInfoList);
		return pldMemberData;
	}
	
	
	private IND71GMember getMemberFromMemberList(List<IND71GMember> householdMembers, PdOrderItemPersonDTO pdOrderItemPersonDTO) {
		IND71GMember currentMember = new IND71GMember();
		for(IND71GMember householdMember : householdMembers){
			if(householdMember.getMemberId().equals(pdOrderItemPersonDTO.getExternalId().toString())){
				currentMember = householdMember;
			}
		}
		return currentMember;
	}
	private PdOrderItemPersonDTO getMemberFromPDMemberList(IND71GMember householdMember, List<PdOrderItemPersonDTO> pdOrderItemPersonList) {
		for(PdOrderItemPersonDTO pdOrderItemPerson  :pdOrderItemPersonList){
			if(householdMember.getMemberId().equals(pdOrderItemPerson.getExternalId().toString())){
				return pdOrderItemPerson;
			}
		}
		return null;
	}
	
	private Map<String,String> createMemberInfoMapFromPersonData(IND71GMember personDataObj, InsuranceType insuranceType, String enrollmentType, Map<String,Long> enrollmentPlanMap, Long planId) {
		Map<String,String> memberInfo = new HashMap<String,String>();
		memberInfo.put("memberId",personDataObj.getMemberId());
		if(EnrollmentConstants.ENROLLMENT_TYPE_SPECIAL.equalsIgnoreCase(enrollmentType)){
   			memberInfo.put("maintenanceReasonCode", personDataObj.getMaintenanceReasonCode());
   		}
   		memberInfo.put("firstName",personDataObj.getFirstName());
   		memberInfo.put("middleName",personDataObj.getMiddleName()); // Optional
   		memberInfo.put("lastName",personDataObj.getLastName());
   		memberInfo.put("suffix",personDataObj.getSuffix()); // Optional
   		memberInfo.put("dob",""+personDataObj.getDob());
   		if(personDataObj.getTobacco() != null){
   			memberInfo.put("tobacco",personDataObj.getTobacco().toString()); // Optional
   			memberInfo.put("lastTobaccoUseDate","");
   		}
    	memberInfo.put("ssn",personDataObj.getSsn()); // Optional
    	memberInfo.put("primaryPhone",personDataObj.getPrimaryPhone());
    	memberInfo.put("secondaryPhone",personDataObj.getSecondaryPhone()); // Optional
    	memberInfo.put("preferredPhone",personDataObj.getPreferredPhone()); // Optional
    	memberInfo.put("preferredEmail",personDataObj.getPreferredEmail()); // Optional
    	memberInfo.put("homeAddress1",personDataObj.getHomeAddress1());
    	memberInfo.put("homeAddress2",personDataObj.getHomeAddress2()); // Optional
    	memberInfo.put("homeCity",personDataObj.getHomeCity());
    	memberInfo.put("homeState",personDataObj.getHomeState());
    	memberInfo.put("homeZip",personDataObj.getHomeZip());
    	memberInfo.put("genderCode",personDataObj.getGenderCode());
    	memberInfo.put("maritalStatusCode",personDataObj.getMaritalStatusCode());
    	memberInfo.put("citizenshipStatusCode",personDataObj.getCitizenshipStatusCode()); // Optional
    	memberInfo.put("spokenLanguageCode",personDataObj.getSpokenLanguageCode()); // Optional
    	memberInfo.put("writtenLanguageCode",personDataObj.getWrittenLanguageCode()); // Optional
    	memberInfo.put("mailingAddress1",personDataObj.getMailingAddress1());
    	memberInfo.put("mailingAddress2",personDataObj.getMailingAddress2()); // Optional
    	memberInfo.put("mailingCity",personDataObj.getMailingCity());
    	memberInfo.put("mailingState",personDataObj.getMailingState());
    	memberInfo.put("mailingZip",personDataObj.getMailingZip());
    	memberInfo.put("custodialParentId",personDataObj.getCustodialParentId()); // Optional
    	memberInfo.put("custodialParentFirstName",personDataObj.getCustodialParentFirstName()); // Optional
    	memberInfo.put("custodialParentMiddleName",personDataObj.getCustodialParentMiddleName()); // Optional
    	memberInfo.put("custodialParentLastName",personDataObj.getCustodialParentLastName()); // Optional
    	memberInfo.put("custodialParentSuffix",personDataObj.getCustodialParentSuffix()); // Optional
    	memberInfo.put("custodialParentSsn",personDataObj.getCustodialParentSsn()); // Optional
    	memberInfo.put("custodialParentPrimaryPhone",personDataObj.getCustodialParentPrimaryPhone()); // Optional
    	memberInfo.put("custodialParentSecondaryPhone",personDataObj.getCustodialParentSecondaryPhone()); // Optional
    	memberInfo.put("custodialParentPreferredPhone",personDataObj.getCustodialParentPreferredPhone()); // Optional
    	memberInfo.put("custodialParentPreferredEmail",personDataObj.getCustodialParentPreferredEmail()); // Optional
    	memberInfo.put("custodialParentHomeAddress1",personDataObj.getCustodialParentHomeAddress1()); // Optional
    	memberInfo.put("custodialParentHomeAddress2",personDataObj.getCustodialParentHomeAddress2()); // Optional
    	memberInfo.put("custodialParentHomeCity",personDataObj.getCustodialParentHomeCity()); // Optional
    	memberInfo.put("custodialParentHomeState",personDataObj.getCustodialParentHomeState()); // Optional
    	memberInfo.put("custodialParentHomeZip",personDataObj.getCustodialParentHomeZip()); // Optional
    	memberInfo.put("houseHoldContactRelationship",personDataObj.getResponsiblePersonRelationship());
    	memberInfo.put("financialHardshipExemption",personDataObj.getFinancialHardshipExemption().toString());
    	if(personDataObj.getExternalMemberId() != null) {
    		memberInfo.put("externalMemberId", personDataObj.getExternalMemberId());
    	}
    	if(personDataObj.getCatastrophicEligible() != null){
    		memberInfo.put("catastrophicEligible",personDataObj.getCatastrophicEligible().toString());
    	}
    	memberInfo.put("countyCode",personDataObj.getCountyCode());
    	return memberInfo;
	}
	
	private void validateIdahoFinancialEffDate(EnrollmentResponse existingEnrollmentData,PldPlanData pldPlanData) {
		Date financialEffectiveDate=pldPlanData.getFinancialEffectiveDate();
		String validationFailureDesc=null;
		if(EnrollmentConfiguration.isIdCall() && financialEffectiveDate!=null){
			if(!EnrollmentUtils.isFloatEqual(pldPlanData.getPlan().get(EnrollmentConstants.GROSS_PREMIUM_AMT), existingEnrollmentData.getGrossPremiumAmt())){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_GROSS_CHANGE + " for Enrollment Id :: "+ existingEnrollmentData.getEnrollmentId() +" for financial Effective Date"+ financialEffectiveDate ;
				pldPlanData.setFinancialEffectiveDate(null);
			}else if(existingEnrollmentData.getEffectiveEndDate().before(DateUtils.truncate(financialEffectiveDate, Calendar.DAY_OF_MONTH))||
					!EnrollmentUtils.isSameYear(existingEnrollmentData.getEffectiveStartDate(), financialEffectiveDate)
					){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_OUT_COV + " for Enrollment Id :: "+ existingEnrollmentData.getEnrollmentId() +" for financial Effective Date"+ financialEffectiveDate ;
				pldPlanData.setFinancialEffectiveDate(null);
			}else if(financialEffectiveDate.before(existingEnrollmentData.getEffectiveStartDate())){
				pldPlanData.setFinancialEffectiveDate(existingEnrollmentData.getEffectiveStartDate());
			}else if(DateUtil.getDateOfMonth(financialEffectiveDate)!=1 && DateUtil.getDateOfMonth(existingEnrollmentData.getEffectiveStartDate())==1){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_APTC_MID_MONTH + " for Enrollment Id :: "+ existingEnrollmentData.getEnrollmentId() +" for financial Effective Date"+ financialEffectiveDate ;
				pldPlanData.setFinancialEffectiveDate(null);
			}
			if(EnrollmentUtils.isNotNullAndEmpty(validationFailureDesc)){
				String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
				enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.FINANCIALEFFDATE, "Financial Effective Date Validation Failure", validationFailureDesc);
			}
		}
	}
	private void populateEnrollmentResponse(EnrollmentResponse response, int responseCode, String responseMessage, String status){
		if(response!=null){
			response.setStatus(status);
			response.setErrCode(responseCode);
			response.setErrMsg(responseMessage);
		}
			
	}
}

