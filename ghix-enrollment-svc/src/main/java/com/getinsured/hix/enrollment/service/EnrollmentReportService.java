package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.dto.enrollment.EnrollmentEventHistoryDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;

/**
 * @since 20th July 2015
 * @author Sharma_k
 * Jira ID: HIX-72107
 * Create a new Enrollment Events API for CAP Enrollment History functionality
 */
public interface EnrollmentReportService 
{
	/**
	 * 
	 * @param enrollmentRequest Object<EnrollmentRequest>
	 * @return List<Object<EnrollmentEventHistoryDTO>>
	 */
	List<EnrollmentEventHistoryDTO> getEnrollmentEventHistoryDTOList(EnrollmentRequest enrollmentRequest);
}
