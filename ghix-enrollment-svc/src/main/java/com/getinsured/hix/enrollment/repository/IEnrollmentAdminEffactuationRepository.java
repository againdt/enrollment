package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.enrollment.EnrollmentAdminEffactuation;


public interface IEnrollmentAdminEffactuationRepository  extends JpaRepository<EnrollmentAdminEffactuation, Integer>{
	
}
