package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentIRSMonthlyIn;


public interface IEnrollmentIRSMonthlyInRepository extends JpaRepository<EnrollmentIRSMonthlyIn, Integer> {
	@Query(" FROM EnrollmentIRSMonthlyIn as en " +
		   " WHERE en.jobExecutionId = :jobExecutionId " +
		   " AND en.errorCodeLkp.lookupValueCode in (:errorCode)")
	List<EnrollmentIRSMonthlyIn> getbyJobExecutionIdAndReSubZip(@Param("jobExecutionId") String jobExecutionId, @Param("errorCode") List<String> errorCode);
	
	@Query(" FROM EnrollmentIRSMonthlyIn as en " +
		   " WHERE en.jobExecutionId = :jobExecutionId ")
	List<EnrollmentIRSMonthlyIn> getbyJobExecutionId(@Param("jobExecutionId") String jobExecutionId);
}
