package com.getinsured.hix.enrollment.carriefeed.model;

/**
 * Created by jabelardo on 7/10/14.
 */
public class CsvModel {
    private String sellingBrokerNpn;
    private String sellingBrokerTin;
    private String sellingBrokerFirstName;
    private String sellingBrokerLastName;
    private String sellingBrokerFirmName;
    private String servicingBrokerNpn;
    private String servicingBrokerTin;
    private String servicingBrokerFirstName;
    private String servicingBrokerLastName;
    private String servicingBrokerFirmName;
    private String groupId;
    private String groupName;
    private String grpEffDt;
    private String grpTrmDt;
    private String memberId;
    private String UID;
    private String memberName;
    private String memberLastName;
    private String memberFirstName;
    private String memberMiddleInitial;
    private String memberSuffix;
    private String memEffDt;
    private String memTrmDt;
    private String termDescription;
    private String lastPremDt;
    private String lastPremMonth;
    private String lastPaidPremAmt;
    private String lastSubPaidPremAmt;
    private String gaNpn;
    private String gaTin;
    private String gaFirstName;
    private String gaLastName;
    private String gaFirmName;
    private String letterDesc;
    private String printDt;
    private String planName;
    private String CPID;
    private String PPID;
    private String HIPPA;
    private String RC;
    private String applctnId;
    private String apctId;
    private String applReceivedDate;
    private String UW;
    private String dentalind;
    private String DOB;
    private String phone;
    private String add1;
    private String add2;
    private String city;
    private String st;
    private String zip;
    private String enrlmntOnOffExchInd;
    private String mbrExchMbrId;
    private String totalPrem;
    private String medOnlyPrem;
    private String denOnlyPrem;
    private String medBasePrem;
    private String type1;
    private String method1;
    private String date1;
    private String amt1;
    private String type2;
    private String method2;
    private String date2;
    private String amt2;
    private String type3;
    private String method3;
    private String date3;
    private String amt3;
    private String type4;
    private String method4;
    private String date4;
    private String amt4;
    private String type5;
    private String method5;
    private String date5;
    private String amt5;
    private String type6;
    private String method6;
    private String date6;
    private String amt6;
    private String note1;
    private String note2;
    private String note3;
    private String note4;
    private String note5;
    private String note6;
    private String subsidyIndicator;

    public String getSellingBrokerNpn() {
        return sellingBrokerNpn;
    }

    public void setSellingBrokerNpn(String sellingBrokerNpn) {
        this.sellingBrokerNpn = sellingBrokerNpn;
    }

    public String getSellingBrokerTin() {
        return sellingBrokerTin;
    }

    public void setSellingBrokerTin(String sellingBrokerTin) {
        this.sellingBrokerTin = sellingBrokerTin;
    }

    public String getSellingBrokerFirstName() {
        return sellingBrokerFirstName;
    }

    public void setSellingBrokerFirstName(String sellingBrokerFirstName) {
        this.sellingBrokerFirstName = sellingBrokerFirstName;
    }

    public String getSellingBrokerLastName() {
        return sellingBrokerLastName;
    }

    public void setSellingBrokerLastName(String sellingBrokerLastName) {
        this.sellingBrokerLastName = sellingBrokerLastName;
    }

    public String getSellingBrokerFirmName() {
        return sellingBrokerFirmName;
    }

    public void setSellingBrokerFirmName(String sellingBrokerFirmName) {
        this.sellingBrokerFirmName = sellingBrokerFirmName;
    }

    public String getServicingBrokerNpn() {
        return servicingBrokerNpn;
    }

    public void setServicingBrokerNpn(String servicingBrokerNpn) {
        this.servicingBrokerNpn = servicingBrokerNpn;
    }

    public String getServicingBrokerTin() {
        return servicingBrokerTin;
    }

    public void setServicingBrokerTin(String servicingBrokerTin) {
        this.servicingBrokerTin = servicingBrokerTin;
    }

    public String getServicingBrokerFirstName() {
        return servicingBrokerFirstName;
    }

    public void setServicingBrokerFirstName(String servicingBrokerFirstName) {
        this.servicingBrokerFirstName = servicingBrokerFirstName;
    }

    public String getServicingBrokerLastName() {
        return servicingBrokerLastName;
    }

    public void setServicingBrokerLastName(String servicingBrokerLastName) {
        this.servicingBrokerLastName = servicingBrokerLastName;
    }

    public String getServicingBrokerFirmName() {
        return servicingBrokerFirmName;
    }

    public void setServicingBrokerFirmName(String servicingBrokerFirmName) {
        this.servicingBrokerFirmName = servicingBrokerFirmName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGrpEffDt() {
        return grpEffDt;
    }

    public void setGrpEffDt(String grpEffDt) {
        this.grpEffDt = grpEffDt;
    }

    public String getGrpTrmDt() {
        return grpTrmDt;
    }

    public void setGrpTrmDt(String grpTrmDt) {
        this.grpTrmDt = grpTrmDt;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberLastName() {
        return memberLastName;
    }

    public void setMemberLastName(String memberLastName) {
        this.memberLastName = memberLastName;
    }

    public String getMemberFirstName() {
        return memberFirstName;
    }

    public void setMemberFirstName(String memberFirstName) {
        this.memberFirstName = memberFirstName;
    }

    public String getMemberMiddleInitial() {
        return memberMiddleInitial;
    }

    public void setMemberMiddleInitial(String memberMiddleInitial) {
        this.memberMiddleInitial = memberMiddleInitial;
    }

    public String getMemberSuffix() {
        return memberSuffix;
    }

    public void setMemberSuffix(String memberSuffix) {
        this.memberSuffix = memberSuffix;
    }

    public String getMemEffDt() {
        return memEffDt;
    }

    public void setMemEffDt(String memEffDt) {
        this.memEffDt = memEffDt;
    }

    public String getMemTrmDt() {
        return memTrmDt;
    }

    public void setMemTrmDt(String memTrmDt) {
        this.memTrmDt = memTrmDt;
    }

    public String getTermDescription() {
        return termDescription;
    }

    public void setTermDescription(String termDescription) {
        this.termDescription = termDescription;
    }

    public String getLastPremDt() {
        return lastPremDt;
    }

    public void setLastPremDt(String lastPremDt) {
        this.lastPremDt = lastPremDt;
    }

    public String getLastPremMonth() {
        return lastPremMonth;
    }

    public void setLastPremMonth(String lastPremMonth) {
        this.lastPremMonth = lastPremMonth;
    }

    public String getLastPaidPremAmt() {
        return lastPaidPremAmt;
    }

    public void setLastPaidPremAmt(String lastPaidPremAmt) {
        this.lastPaidPremAmt = lastPaidPremAmt;
    }

    public String getLastSubPaidPremAmt() {
        return lastSubPaidPremAmt;
    }

    public void setLastSubPaidPremAmt(String lastSubPaidPremAmt) {
        this.lastSubPaidPremAmt = lastSubPaidPremAmt;
    }

    public String getGaNpn() {
        return gaNpn;
    }

    public void setGaNpn(String gaNpn) {
        this.gaNpn = gaNpn;
    }

    public String getGaTin() {
        return gaTin;
    }

    public void setGaTin(String gaTin) {
        this.gaTin = gaTin;
    }

    public String getGaFirstName() {
        return gaFirstName;
    }

    public void setGaFirstName(String gaFirstName) {
        this.gaFirstName = gaFirstName;
    }

    public String getGaLastName() {
        return gaLastName;
    }

    public void setGaLastName(String gaLastName) {
        this.gaLastName = gaLastName;
    }

    public String getGaFirmName() {
        return gaFirmName;
    }

    public void setGaFirmName(String gaFirmName) {
        this.gaFirmName = gaFirmName;
    }

    public String getLetterDesc() {
        return letterDesc;
    }

    public void setLetterDesc(String letterDesc) {
        this.letterDesc = letterDesc;
    }

    public String getPrintDt() {
        return printDt;
    }

    public void setPrintDt(String printDt) {
        this.printDt = printDt;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getCPID() {
        return CPID;
    }

    public void setCPID(String CPID) {
        this.CPID = CPID;
    }

    public String getPPID() {
        return PPID;
    }

    public void setPPID(String PPID) {
        this.PPID = PPID;
    }

    public String getHIPPA() {
        return HIPPA;
    }

    public void setHIPPA(String HIPPA) {
        this.HIPPA = HIPPA;
    }

    public String getRC() {
        return RC;
    }

    public void setRC(String RC) {
        this.RC = RC;
    }

    public String getApplctnId() {
        return applctnId;
    }

    public void setApplctnId(String applctnId) {
        this.applctnId = applctnId;
    }

    public String getApctId() {
        return apctId;
    }

    public void setApctId(String apctId) {
        this.apctId = apctId;
    }

    public String getApplReceivedDate() {
        return applReceivedDate;
    }

    public void setApplReceivedDate(String applReceivedDate) {
        this.applReceivedDate = applReceivedDate;
    }

    public String getUW() {
        return UW;
    }

    public void setUW(String UW) {
        this.UW = UW;
    }

    public String getDentalind() {
        return dentalind;
    }

    public void setDentalind(String dentalind) {
        this.dentalind = dentalind;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdd1() {
        return add1;
    }

    public void setAdd1(String add1) {
        this.add1 = add1;
    }

    public String getAdd2() {
        return add2;
    }

    public void setAdd2(String add2) {
        this.add2 = add2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getEnrlmntOnOffExchInd() {
        return enrlmntOnOffExchInd;
    }

    public void setEnrlmntOnOffExchInd(String enrlmntOnOffExchInd) {
        this.enrlmntOnOffExchInd = enrlmntOnOffExchInd;
    }

    public String getMbrExchMbrId() {
        return mbrExchMbrId;
    }

    public void setMbrExchMbrId(String mbrExchMbrId) {
        this.mbrExchMbrId = mbrExchMbrId;
    }

    public String getTotalPrem() {
        return totalPrem;
    }

    public void setTotalPrem(String totalPrem) {
        this.totalPrem = totalPrem;
    }

    public String getMedOnlyPrem() {
        return medOnlyPrem;
    }

    public void setMedOnlyPrem(String medOnlyPrem) {
        this.medOnlyPrem = medOnlyPrem;
    }

    public String getDenOnlyPrem() {
        return denOnlyPrem;
    }

    public void setDenOnlyPrem(String denOnlyPrem) {
        this.denOnlyPrem = denOnlyPrem;
    }

    public String getMedBasePrem() {
        return medBasePrem;
    }

    public void setMedBasePrem(String medBasePrem) {
        this.medBasePrem = medBasePrem;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public String getMethod1() {
        return method1;
    }

    public void setMethod1(String method1) {
        this.method1 = method1;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getAmt1() {
        return amt1;
    }

    public void setAmt1(String amt1) {
        this.amt1 = amt1;
    }

    public String getType2() {
        return type2;
    }

    public void setType2(String type2) {
        this.type2 = type2;
    }

    public String getMethod2() {
        return method2;
    }

    public void setMethod2(String method2) {
        this.method2 = method2;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public String getAmt2() {
        return amt2;
    }

    public void setAmt2(String amt2) {
        this.amt2 = amt2;
    }

    public String getType3() {
        return type3;
    }

    public void setType3(String type3) {
        this.type3 = type3;
    }

    public String getMethod3() {
        return method3;
    }

    public void setMethod3(String method3) {
        this.method3 = method3;
    }

    public String getDate3() {
        return date3;
    }

    public void setDate3(String date3) {
        this.date3 = date3;
    }

    public String getAmt3() {
        return amt3;
    }

    public void setAmt3(String amt3) {
        this.amt3 = amt3;
    }

    public String getType4() {
        return type4;
    }

    public void setType4(String type4) {
        this.type4 = type4;
    }

    public String getMethod4() {
        return method4;
    }

    public void setMethod4(String method4) {
        this.method4 = method4;
    }

    public String getDate4() {
        return date4;
    }

    public void setDate4(String date4) {
        this.date4 = date4;
    }

    public String getAmt4() {
        return amt4;
    }

    public void setAmt4(String amt4) {
        this.amt4 = amt4;
    }

    public String getType5() {
        return type5;
    }

    public void setType5(String type5) {
        this.type5 = type5;
    }

    public String getMethod5() {
        return method5;
    }

    public void setMethod5(String method5) {
        this.method5 = method5;
    }

    public String getDate5() {
        return date5;
    }

    public void setDate5(String date5) {
        this.date5 = date5;
    }

    public String getAmt5() {
        return amt5;
    }

    public void setAmt5(String amt5) {
        this.amt5 = amt5;
    }

    public String getType6() {
        return type6;
    }

    public void setType6(String type6) {
        this.type6 = type6;
    }

    public String getMethod6() {
        return method6;
    }

    public void setMethod6(String method6) {
        this.method6 = method6;
    }

    public String getDate6() {
        return date6;
    }

    public void setDate6(String date6) {
        this.date6 = date6;
    }

    public String getAmt6() {
        return amt6;
    }

    public void setAmt6(String amt6) {
        this.amt6 = amt6;
    }

    public String getNote1() {
        return note1;
    }

    public void setNote1(String note1) {
        this.note1 = note1;
    }

    public String getNote2() {
        return note2;
    }

    public void setNote2(String note2) {
        this.note2 = note2;
    }

    public String getNote3() {
        return note3;
    }

    public void setNote3(String note3) {
        this.note3 = note3;
    }

    public String getNote4() {
        return note4;
    }

    public void setNote4(String note4) {
        this.note4 = note4;
    }

    public String getNote5() {
        return note5;
    }

    public void setNote5(String note5) {
        this.note5 = note5;
    }

    public String getNote6() {
        return note6;
    }

    public void setNote6(String note6) {
        this.note6 = note6;
    }

    public String getSubsidyIndicator() {
        return subsidyIndicator;
    }

    public void setSubsidyIndicator(String subsidyIndicator) {
        this.subsidyIndicator = subsidyIndicator;
    }
}
