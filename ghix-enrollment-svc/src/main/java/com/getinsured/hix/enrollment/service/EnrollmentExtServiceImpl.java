/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrolleeExtDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeExtRelationshipDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentExtDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentExtResponseDTO;
import com.getinsured.hix.enrollment.repository.IEnrlHouseholdRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPrivateRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.timeshift.util.TSDate;

/**
 * Enrollment ACA Express service implementation
 * @author negi_s
 * @since 12/09/2017
 *
 */
@Service("enrollmentAcaService")
public class EnrollmentExtServiceImpl implements EnrollmentExtService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentExtServiceImpl.class);

	@Autowired private LookupService lookupService;
	@Autowired private IEnrollmentPrivateRepository enrollmentPrivateRepository;
	@Autowired private EnrollmentUserPrivateService enrollmentUserService;
	@Autowired private EnrollmentPrivateUtils enrollmentPrivateUtils;
	@Autowired private IEnrlHouseholdRepository enrlHouseholdRepository;
	
	private static final String ACADATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String ACA_DATE_OF_BIRTH_FORMAT = "dd/MM/yyyy h:mm:ss a";

	private static final String ERR_MSG_ENRL_FAILURE = "Failure in creating enrollment";
	
	public EnrollmentExtResponseDTO createAcaEnrollment(EnrollmentExtDTO dto, final Integer giWsPayloadId) throws GIException {
		LOGGER.info("Initiating enrollment");
		EnrollmentExtResponseDTO enrollmentResponse = new EnrollmentExtResponseDTO();
		//AccountUser user = enrollmentUserService.getLoggedInUser();
		AccountUser user = null;
		if(StringUtils.isNotBlank(dto.getCapAgentId())) {
			user = new AccountUser(Integer.parseInt(dto.getCapAgentId()), "Michael", "Daugherty");
		}
		try {
			Enrollment enrollment = populateEnrollmentData(dto, user);
			populateEnrolleeData(enrollment, dto, user);
			populateEnrolleeRelationship(enrollment.getEnrollees(), dto.getMembers(), user);
			enrollment.setEnrollmentEvents(createEnrollmentEvents(enrollment,enrollment.getEnrollees() ,user));
			enrollment.setGiWsPayloadId(giWsPayloadId);
			enrollment = saveEnrollment(enrollment);
			//enrollmentPrivateUtils.recordApplicationEnrollmentPrivateEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString(), enrollment.getEnrollmentStatusLkp().getLookupValueLabel());
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			enrollmentResponse.setResponseCode(EnrollmentPrivateConstants.ERROR_CODE_200);
			enrollmentResponse.setResponseMsg(EnrollmentPrivateConstants.EXT_SUCCESS_RESPONSE_MESSAGE);
			enrollmentResponse.setUrl(DynamicPropertiesUtil.getPropertyValue(EnrollmentPrivateConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_EXT_RESPONSE_URL));
		}catch(Exception e){
			if(StringUtils.isBlank(e.getMessage()))
				throw new GIException(ERR_MSG_ENRL_FAILURE, e);
			else
				throw new GIException(e.getMessage(), e);
		}
		return enrollmentResponse;
	}

	/**
	 * Populate enrollment data using request DTO
	 * @param dto
	 * @param user
	 * @return enrollment
	 */
	private Enrollment populateEnrollmentData(EnrollmentExtDTO dto, AccountUser user) {
		Enrollment enrollment = new Enrollment();
		enrollment.setPriorSsapApplicationid(Long.valueOf(dto.getSsapApplicationId()));
		enrollment.setSsapApplicationid(Long.valueOf(dto.getSsapApplicationId()));
		enrollment.setGrossPremiumAmt(dto.getPremium());
		enrollment.setAptcAmt(dto.getAptc());
		enrollment.setNetPremiumAmt(enrollment.getGrossPremiumAmt());
		if(null != enrollment.getGrossPremiumAmt() && null != enrollment.getAptcAmt()) {
			enrollment.setNetPremiumAmt(enrollment.getGrossPremiumAmt() - enrollment.getAptcAmt());
		}
		enrollment.setBenefitEffectiveDate(DateUtil.StringToDate(dto.getBenefitEffectiveDate(), ACADATEFORMAT));
		enrollment.setInsurerName(dto.getIssuerName());
		enrollment.setPlanName(dto.getPlanName());
		if(StringUtils.isNotBlank(dto.getPlanCsLevel()) && dto.getPlanCsLevel().length() == EnrollmentConstants.TWO){
			enrollment.setCsrLevel(EnrollmentConstants.CSR_LEVEL_APPENDER + dto.getPlanCsLevel().substring(dto.getPlanCsLevel().length() - EnrollmentConstants.ONE));
		}
		else{
			enrollment.setCsrLevel(dto.getPlanCsLevel());
		}
		enrollment.setCMSPlanID(dto.getHiosPlanId());
		enrollment.setPlanId(dto.getPlanId());
		enrollment.setIssuerId(dto.getIssuerId());
		enrollment.setPlanLevel(dto.getPlanLevel());
		
		
		enrollment.setCapAgentId(NumberUtils.isNumber(dto.getCapAgentId()) ? Integer.valueOf(dto.getCapAgentId()) : null);
		enrollment.setExchangeAssignPolicyNo(dto.getExchangeAssignPolicyNo());
		enrollment.setIssuerAssignPolicyNo(dto.getIssuerAssignPolicyNo());
		enrollment.setSubmittedToCarrierDate(new TSDate());
		enrollment.setHouseHoldCaseId(dto.getHouseholeCaseId());
		if(NumberUtils.isNumber(dto.getHouseholeCaseId())) {
			enrollment.setCmrHouseHoldId(Integer.valueOf(dto.getHouseholeCaseId()));
		}
		enrollment.setStateExchangeCode(dto.getStateCode());
		enrollment.setExchangeTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_ONEXCHANGE));
		enrollment.setEnrollmentModalityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.LOOKUP_EDE_SELF_CODE));
		enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
		enrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE, EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL));
		
		/**Setting Insurance type*/
		if(dto.getInsuranceTypeLabel()!=null && dto.getInsuranceTypeLabel().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH)) {
			enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE));
		}else if(dto.getInsuranceTypeLabel()!=null && dto.getInsuranceTypeLabel().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL)) {
			enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE));
		}
		
		
		enrollment.setCreatedBy(user);
		enrollment.setUpdatedBy(user);

		return enrollment;
	}

	/**
	 * Populate enrollees using request DTO
	 * @param enrollment
	 * @param dto
	 * @param user
	 */
	private void populateEnrolleeData(Enrollment enrollment, EnrollmentExtDTO dto, AccountUser user) {
		List<Enrollee> enrollees = new ArrayList<>();
		Location location = getLocation(dto);
		if(null != dto.getMembers()) {
			for(EnrolleeExtDTO member : dto.getMembers()) {
				Enrollee enrollee = new Enrollee();
				enrollee.setExchgIndivIdentifier(member.getMemberId());
				enrollee.setFirstName(member.getFirstName());
				enrollee.setMiddleName(member.getMiddleName());
				enrollee.setLastName(member.getLastName());
				enrollee.setSuffix(member.getSuffix());
				enrollee.setBirthDate(DateUtil.StringToDate(member.getDob(), ACADATEFORMAT));
				enrollee.setEffectiveStartDate(enrollment.getBenefitEffectiveDate());
				enrollee.setEffectiveEndDate(enrollment.getBenefitEndDate());
				enrollee.setTaxIdNumber(member.getSsn());
				
				enrollee.setPreferredEmail(member.getPreferredEmail());
				enrollee.setRatingArea(member.getRatingArea());
				if(member.getRelationshipToHcp()!=null) {
					enrollee.setRelationshipToHCPLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.RELATIONSHIP, member.getRelationshipToHcp()));
				}
				
				enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.GENDER, member.getGender()));
				if(EnrollmentPrivateConstants.Y.equalsIgnoreCase(member.getTobacco())){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, EnrollmentPrivateConstants.T));
				}
				else{
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, EnrollmentPrivateConstants.N));
				}
				if(EnrollmentPrivateConstants.Y.equalsIgnoreCase(member.getSubscriberIndicator())) {
					enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER));
					enrollee.setPrimaryPhoneNo(member.getPrimaryPhoneNumber());
					enrollment.setSubscriberName(EnrollmentUtils.concatEnrolleeName(enrollee));
					enrollment.setExchgSubscriberIdentifier(enrollee.getExchgIndivIdentifier());
					enrollment.setSponsorName(EnrollmentUtils.concatEnrolleeName(enrollee));
				}else {
					enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
				}
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
				enrollee.setHomeAddressid(location);
				enrollee.setMailingAddressId(location);
				enrollee.setRatingArea(dto.getRatingArea());
				enrollee.setCreatedBy(user);
				enrollee.setUpdatedBy(user);
				enrollee.setEnrollment(enrollment);

				enrollees.add(enrollee);
			}
			enrollment.setEnrollees(enrollees);
		}
	}

	/**
	 * Create enrollee relationships using request data
	 * @param enrolleeList
	 * @param memberList
	 * @param user
	 */
	private void populateEnrolleeRelationship(List<Enrollee> enrolleeList, List<EnrolleeExtDTO> memberList, AccountUser user) {
		for (EnrolleeExtDTO member : memberList) {
			Enrollee sourceEnrollee = findEnrolleeByMemberId(member.getMemberId(), enrolleeList);
			List<EnrolleeExtRelationshipDTO> relationshipList  = member.getRelationships();
			List<EnrolleeRelationship> enrolleeRelationshipList = new ArrayList<EnrolleeRelationship>();
			if(null != relationshipList && !relationshipList.isEmpty()){
				for (EnrolleeExtRelationshipDTO relationship : relationshipList){
					EnrolleeRelationship tmpEnrolleeRelationship = new EnrolleeRelationship();
					tmpEnrolleeRelationship.setRelationshipLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.RELATIONSHIP, relationship.getRelationshipCode()));
					tmpEnrolleeRelationship.setSourceEnrollee(sourceEnrollee);
					Enrollee targetEnrollee = findEnrolleeByMemberId(relationship.getMemberId(),enrolleeList );
					tmpEnrolleeRelationship.setTargetEnrollee(targetEnrollee);
					tmpEnrolleeRelationship.setCreatedBy(user);
					tmpEnrolleeRelationship.setUpdatedBy(user);
					enrolleeRelationshipList.add(tmpEnrolleeRelationship);
				}
				sourceEnrollee.setEnrolleeRelationship(enrolleeRelationshipList);
			}
		}
	}

	private Enrollee findEnrolleeByMemberId(String memberId, List<Enrollee> enrolleeList)  {
		for (Enrollee enrollee : enrolleeList) { 
			if (enrollee.getExchgIndivIdentifier() != null && enrollee.getExchgIndivIdentifier().equals(memberId)){
				return enrollee;
			}
		}
		return null;
	}

	private Location getLocation(EnrollmentExtDTO dto) {
		Location location = new Location();
		location.setAddress1(dto.getAddressLine1());
		location.setAddress2(dto.getAddressLine2());
		location.setCity(dto.getCity());
		location.setZip(String.valueOf(dto.getZip()));
		location.setCountycode(dto.getCountyCode());
		location.setState(dto.getStateCode());
		return location;
	}
	
	private List<EnrollmentEvent> createEnrollmentEvents(Enrollment enrollment,
			List<Enrollee> enrolleeList, AccountUser user) {
		
		List<EnrollmentEvent> enrollmentEventList= new ArrayList<EnrollmentEvent>();

		for (Enrollee tempenrollee : enrolleeList) {

			if(tempenrollee.getCustodialParent()!=null){
				tempenrollee.getCustodialParent().setEnrollment(enrollment);
			}
			//creating EnrollmentEvent
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
			enrollmentEvent.setCreatedBy(user);
			enrollmentEvent.setEnrollee(tempenrollee);
			enrollmentEvent.setEnrollment(enrollment);
			enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION));
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION));

			//added last event Id for Audit
			tempenrollee.setLastEventId(enrollmentEvent);
			enrollmentEventList.add(enrollmentEvent);
		}
		return enrollmentEventList;
	}
	
	@Transactional
	private Enrollment saveEnrollment(Enrollment enrollment) throws GIException {
		return 	enrollmentPrivateRepository.saveAndFlush(enrollment);
	}
}
