package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Broker;

public interface IEnrlBrokerRepository extends JpaRepository<Broker, Integer> {

}
