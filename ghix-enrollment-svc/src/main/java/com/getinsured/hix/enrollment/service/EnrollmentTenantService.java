package com.getinsured.hix.enrollment.service;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentTenantResponse;

/**
 * HIX-70722 Enrollment Tenant related services
 * @author negi_s
 * @since 08/07/2015
 */
public interface EnrollmentTenantService {
	
	/**
	 * HIX-70722 Service to get enrollment tenant API response 
	 * @author negi_s
	 * @since 15/07/2015
	 * @param leadId
	 * @param enrollmentId
	 * @param affiliateId 
	 * @return EnrollmentTenantResponse
	 */
	EnrollmentTenantResponse getEnrollmentTenantResponse(Long leadId, Integer enrollmentId, Long affiliateId);

	/**
	 * HIX-70722 Service to return enrollment list for a tenant request
	 * @author negi_s
	 * @since 08/07/2015
	 * @param leadId
	 * @param enrollmentId
	 * @param affiliateId
	 * @return Enrollment
	 */
	Enrollment findEnrollmentForTenant(Long leadId, Integer enrollmentId, Long affiliateId);

}
