package com.getinsured.hix.enrollment.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class EnrollmentEmployeeHouseHoldInfoUpdateNotification extends  EnrollmentNotificationAgent {
	private Map<String, String> enrollmentEmailDetails;
	private Map<String, String> singleData;
	
	@Override
	public void setEnrollmentEmailDetails(Map<String, String> enrollmentEmailDetails) {
		this.enrollmentEmailDetails = enrollmentEmailDetails;
	}
	
	@Override
	public void setNotificationData(Map<String, Object> notificationData){
		this.notificationData = notificationData;
	}
	
	@Override
	public Map<String, Object> getNotificationData(){
		return this.notificationData;
	}
	
	@Override
	public Map<String, String> getSingleData() {
		Map<String, String> data = new HashMap<String, String>();
		
		if(this.enrollmentEmailDetails != null && !enrollmentEmailDetails.isEmpty()){
			setTokens(this.enrollmentEmailDetails);
			data.putAll(this.enrollmentEmailDetails);
		}

		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
		
	}
	
	@Override
	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}

}
