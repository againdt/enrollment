package com.getinsured.hix.enrollment.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.dto.enrollment.EnrollmentSftpFileTransferDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.JiraUtil;
import com.google.gson.Gson;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;

/**
 * Java Utility for SFTP file transfers
 * 
 * @author negi_s
 * @since 06/10/2016
 *
 */
public class EnrollmentSftpUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentSftpUtil.class);

	/**
	 * Key for SFTP parameter map denoting the SFTP host url.
	 */
	public static final String SFTP_HOST = "sftpHost";
	/**
	 * Key for SFTP parameter map denoting the SFTP username.
	 */
	public static final String SFTP_USERNAME = "sftpUsername";
	/**
	 * Key for SFTP parameter map denoting the SFTP password.
	 */
	public static final String SFTP_PASSWORD = "sftpPassword";
	/**
	 * Key for SFTP parameter map denoting the SFTP port number.
	 */
	public static final String SFTP_PORT = "sftpPort";

	private static final int MAX_POOL_SIZE = 5;
	private static final int MAX_CHANNEL_OPEN = 5; // Max Value 10

	private static final String UPLOAD = "UPLOAD";
	private static final String DOWNLOAD = "DOWNLOAD";
	private static final String DELETE = "DELETE";
	private static final String SESSION = "session";
	private static final String CHANNEL = "channel";
	
	private static final String CONNECTION_ERR_MSG = "Unable to establish connection with the host";

	private static Gson gson = new Gson();
	
	/**
	 * Private constructor.
	 * Utility classes should not have public constructor, Added to hide implicit public constructor.
	 * 
	 */
	private EnrollmentSftpUtil(){
	}

	/**
	 * Send a file to a remote location
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param fileName
	 *            Location of the file to be sent
	 * @param hostLocation
	 *            Destination on the remote location
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String uploadFile(Map<String, String> sftpParameters, String fileName, String hostLocation, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			fileTransferList.add(uploadFile(fileName, hostLocation, channelSftp));
		} catch (Exception ex) {
			LOGGER.error("Exception found while transferring the file :: " + ex.getMessage(), ex);
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Send a file to a remote location (Part of thread implementation)
	 * 
	 * @param fileName
	 *            Location of the file to be sent
	 * @param hostLocation
	 *            Destination on the remote location
	 * @return a <code>EnrollmentSftpFileTransferDTO</code>
	 */
	protected static EnrollmentSftpFileTransferDTO uploadFile(String fileName, String hostLocation,
			ChannelSftp channelSftp) {
		EnrollmentSftpFileTransferDTO fileDto = new EnrollmentSftpFileTransferDTO();
		try {
			channelSftp.cd(hostLocation);
			File f = new File(fileName);
			channelSftp.put(new FileInputStream(f), f.getName());
			setFileInfo(fileDto, f, UPLOAD);
			LOGGER.info("File transfered successfully to host.");
			fileDto.setTransferStatus(EnrollmentConstants.SUCCESS);
		} catch (Exception ex) {
			fileDto.setFileName(fileName);
			try {
				fileDto.setFileSize(String.valueOf(new File(fileName).length()));
				fileDto.setFileName(new File(fileName).getName());
			} catch (Exception e) {
				LOGGER.error("Error opening file" + e.getMessage());
			}
			fileDto.setTransferType(UPLOAD);
			fileDto.setTransferStatus(EnrollmentConstants.FAILURE);
			fileDto.setErrorMsg(ex.getMessage());
			LOGGER.error("Exception found while transferring the file :: " + ex.getMessage(), ex);
		} finally {
			channelSftp.exit();
			LOGGER.info("SFTP Channel exited.");
		}
		return fileDto;
	}

	/**
	 * Send a files of a particular extension to a remote location
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param fileNameList
	 *            Location of the files
	 * @param hostLocation
	 *            Destination on the remote location
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table         
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String uploadFilesByFileNames(Map<String, String> sftpParameters, List<String> fileNameList,
			String hostLocation, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		if (null != fileNameList && !fileNameList.isEmpty() && null != hostLocation) {
			try {
				sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
				initiateThreadUpload(fileNameList, null, hostLocation, fileTransferList,
						getSession(sftpSessionAndChannelMap));
				LOGGER.info("File transfered successfully to host.");
			} catch (Exception ex) {
				LOGGER.error("Exception found while transferring the file :: " + ex.getMessage(), ex);
			} finally {
				closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), null);
			}
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Send a files of a particular extension to a remote location
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param sourceLocation
	 *            Location of the source folder
	 * @param hostLocation
	 *            Destination on the remote location
	 * @param extension
	 *            File extension .IN,.zip etc.
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table         
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String uploadFilesByExtension(Map<String, String> sftpParameters, String sourceLocation,
			String hostLocation, String extension, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		if (null != sourceLocation && null != hostLocation) {
			try {
				sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
				File sourceFolder = new File(sourceLocation);
				if (sourceFolder.isDirectory()) {
					List<String> filesToUpload = Arrays.asList(sourceFolder.list(new FilenameFilter() {
						@Override
						public boolean accept(File dir, String name) {
							if (name.endsWith(extension)) {
								return true;
							}
							return false;
						}
					}));
					initiateThreadUpload(filesToUpload, sourceLocation, hostLocation, fileTransferList,
							getSession(sftpSessionAndChannelMap));
				}
				LOGGER.info("File transfered successfully to host.");
			} catch (Exception ex) {
				LOGGER.error("Exception found while transferring the file :: " + ex.getMessage(), ex);
			} finally {
				closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), null);
			}
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Send a files of a particular pattern to a remote location
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param sourceLocation
	 *            Location of the source folder
	 * @param hostLocation
	 *            Destination on the remote location
	 * @param pattern
	 *            Filename pattern
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String uploadFilesByPattern(Map<String, String> sftpParameters, String sourceLocation,
			String hostLocation, String pattern, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		if (null != sourceLocation && null != hostLocation) {
			try {
				sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
				File sourceFolder = new File(sourceLocation);
				if (sourceFolder.isDirectory()) {
					List<String> filesToUpload = Arrays.asList(sourceFolder.list(new FilenameFilter() {
						@Override
						public boolean accept(File dir, String name) {
							if (name.contains(pattern)) {
								return true;
							}
							return false;
						}
					}));
					initiateThreadUpload(filesToUpload, sourceLocation, hostLocation, fileTransferList,
							getSession(sftpSessionAndChannelMap));
				}
				LOGGER.info("File transfered successfully to host.");
			} catch (Exception ex) {
				LOGGER.error("Exception found while transferring the file :: " + ex.getMessage(), ex);
			} finally {
				closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), null);
			}
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Send a files of a particular extension and pattern to a remote location
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param sourceLocation
	 *            Location of the source folder
	 * @param hostLocation
	 *            Destination on the remote location
	 * @param extension
	 *            File extension .IN,.zip etc.
	 * @param pattern
	 *            Filename pattern
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String uploadFilesByExtensionAndPattern(Map<String, String> sftpParameters, String sourceLocation,
			String hostLocation, String extension, String pattern, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		if (null != sourceLocation && null != hostLocation) {
			try {
				sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
				File sourceFolder = new File(sourceLocation);
				if (sourceFolder.isDirectory()) {
					List<String> filesToUpload = Arrays.asList(sourceFolder.list(new FilenameFilter() {
						@Override
						public boolean accept(File dir, String name) {
							if (name.endsWith(extension) && name.contains(pattern)) {
								return true;
							}
							return false;
						}
					}));
					initiateThreadUpload(filesToUpload, sourceLocation, hostLocation, fileTransferList,
							getSession(sftpSessionAndChannelMap));
				}
				LOGGER.info("File transfered successfully to host.");
			} catch (Exception ex) {
				LOGGER.error("Exception found while transferring the file :: " + ex.getMessage(), ex);
			} finally {
				closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), null);
			}
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Get a file from a remote location
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param fileName
	 *            File to be fetched
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param targetLocation
	 *            Download location
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String downloadFile(Map<String, String> sftpParameters, String fileName, String hostLocation,
			String targetLocation, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			fileTransferList.add(downloadFile(fileName, hostLocation, targetLocation, channelSftp));
		} catch (Exception ex) {
			LOGGER.error("Exception found while tranfer the file :: " + ex.getMessage(), ex);
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Get a file from a remote location (Part of thread implementation)
	 * 
	 * @param fileName
	 *            File to be fetched
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param targetLocation
	 *            Download location
	 * @param channelSftp
	 *            JSch channel
	 * @return a <code>EnrollmentSftpFileTransferDTO</code>
	 */
	protected static EnrollmentSftpFileTransferDTO downloadFile(String fileName, String hostLocation,
			String targetLocation, ChannelSftp channelSftp) {
		EnrollmentSftpFileTransferDTO fileDto = new EnrollmentSftpFileTransferDTO();
		fileDto.setFileName(fileName);
		fileDto.setTransferType(DOWNLOAD);
		try {
			channelSftp.cd(hostLocation);
			setRemoteFileInfo(fileDto, channelSftp.lstat(fileName));
			channelSftp.get(fileName, new FileOutputStream(new File(targetLocation + File.separator + fileName)));
			fileDto.setTransferStatus(EnrollmentConstants.SUCCESS);
			LOGGER.info("File transfered successfully from host.");
		} catch (Exception ex) {
			fileDto.setTransferStatus(EnrollmentConstants.FAILURE);
			fileDto.setErrorMsg(ex.getMessage());
			LOGGER.error("Exception found while transferring the file :: " + ex.getMessage(), ex);
		} finally {
			channelSftp.exit();
			LOGGER.info("sftp Channel exited.");
		}
		return fileDto;
	}

	/**
	 * Get files of the a particular extension from a remote location (Thread
	 * implementation)
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * 
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param targetLocation
	 *            Download location
	 * @param extension
	 *            File extension .IN,.zip etc.
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String downloadFilesByExtension(Map<String, String> sftpParameters, String hostLocation,
			String targetLocation, String extension, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			List<String> fileListToDownload = filterByExtension(getFileList(channelSftp, hostLocation), extension);
			initiateThreadDownload(fileListToDownload, hostLocation, targetLocation,
					getSession(sftpSessionAndChannelMap), fileTransferList);
			LOGGER.info("File transfered successfully from host.");
		} catch (Exception ex) {
			LOGGER.error("Exception found while tranfer the file :: " + ex.getMessage(), ex);
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Get files of a particular pattern from a remote location (Thread
	 * implementation)
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param targetLocation
	 *            Download location
	 * @param pattern
	 *            Filename pattern
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String downloadFilesByPattern(Map<String, String> sftpParameters, String hostLocation,
			String targetLocation, String pattern, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			List<String> fileListToDownload = filterByPattern(getFileList(channelSftp, hostLocation), pattern);
			initiateThreadDownload(fileListToDownload, hostLocation, targetLocation,
					getSession(sftpSessionAndChannelMap), fileTransferList);
			LOGGER.info("File transfered successfully from host.");
		} catch (Exception ex) {
			LOGGER.error("Exception found while transferring the file :: " + ex.getMessage(), ex);
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Get files of the a particular extension and filename pattern from a remote location (Thread
	 * implementation)
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param targetLocation
	 *            Download location
	 * @param extension
	 *            File extension .IN,.zip etc.
	 * @param pattern
	 *            Filename pattern
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String downloadFilesByExtensionAndPattern(Map<String, String> sftpParameters, String hostLocation,
			String targetLocation, String extension, String pattern, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			List<String> fileListToDownload = filterByPattern(
					filterByExtension(getFileList(channelSftp, hostLocation), extension), pattern);
			initiateThreadDownload(fileListToDownload, hostLocation, targetLocation,
					getSession(sftpSessionAndChannelMap), fileTransferList);
			LOGGER.info("File transfered successfully from host.");
		} catch (Exception ex) {
			LOGGER.error("Exception found while transferring the file :: " + ex.getMessage());
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}
	
	/**
	 * Delete a file from a remote location (Part of thread implementation)
	 * 
	 * @param fileName
	 *            File to be fetched
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param channelSftp
	 *            JSch channel
	 * @return a <code>EnrollmentSftpFileTransferDTO</code>
	 */
	protected static EnrollmentSftpFileTransferDTO deleteFile(String fileName, String hostLocation,
			ChannelSftp channelSftp) {
		EnrollmentSftpFileTransferDTO fileDto = new EnrollmentSftpFileTransferDTO();
		fileDto.setFileName(fileName);
		fileDto.setTransferType(DELETE);
		try {
			channelSftp.cd(hostLocation);
			setRemoteFileInfo(fileDto, channelSftp.lstat(fileName));
			channelSftp.rm(fileName);
			fileDto.setTransferStatus(EnrollmentConstants.SUCCESS);
			LOGGER.info("File deleted successfully from host.");
		} catch (Exception ex) {
			fileDto.setTransferStatus(EnrollmentConstants.FAILURE);
			fileDto.setErrorMsg(ex.getMessage());
			LOGGER.error("Exception found while deleting the file :: " + ex.getMessage(), ex);
		} finally {
			channelSftp.exit();
			LOGGER.info("sftp Channel exited.");
		}
		return fileDto;
	}

	/**
	 * Delete a file from a remote location
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param fileName
	 *            File to be fetched
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String deleteFile(Map<String, String> sftpParameters, String fileName, String hostLocation, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			fileTransferList.add(deleteFile(fileName, hostLocation, channelSftp));
		} catch (Exception ex) {
			LOGGER.error("Exception found while deleting the file :: " + ex.getMessage(), ex);
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Delete files by filename from a remote location
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param fileListToDelete
	 *            Files to be deleted
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String deleteByFilename(Map<String, String> sftpParameters, List<String> fileListToDelete,
			String hostLocation, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			initiateThreadDelete(fileListToDelete, hostLocation, getSession(sftpSessionAndChannelMap),
					fileTransferList);
		} catch (Exception ex) {
			LOGGER.error("Exception found while deleting the file :: " + ex.getMessage(), ex);
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Get files of the a particular extension from a remote location (Thread
	 * implementation)
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * 
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param targetLocation
	 *            Download location
	 * @param extension
	 *            File extension .IN,.zip etc.
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String deleteFilesByExtension(Map<String, String> sftpParameters, String hostLocation,
			String extension, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			List<String> fileListToDownload = filterByExtension(getFileList(channelSftp, hostLocation), extension);
			initiateThreadDelete(fileListToDownload, hostLocation, getSession(sftpSessionAndChannelMap),
					fileTransferList);
			LOGGER.info("File deleted successfully from host.");
		} catch (Exception ex) {
			LOGGER.error("Exception found while deleting the file :: " + ex.getMessage(), ex);
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Delete files of a particular pattern from a remote location (Thread
	 * implementation)
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param pattern
	 *            Filename pattern
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String deleteFilesByPattern(Map<String, String> sftpParameters, String hostLocation, String pattern, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			List<String> fileListToDelete = filterByPattern(getFileList(channelSftp, hostLocation), pattern);
			initiateThreadDelete(fileListToDelete, hostLocation, getSession(sftpSessionAndChannelMap),
					fileTransferList);
			LOGGER.info("File deleted successfully from host.");
		} catch (Exception ex) {
			LOGGER.error("Exception found while deleting the file :: " + ex.getMessage(), ex);
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Delete files of the a particular extension and filename pattern from a
	 * remote location (Thread implementation)
	 * 
	 * @param sftpParameters
	 *            Required host, username, password and port number
	 * @param hostLocation
	 *            Location of the file on the remote location
	 * @param extension
	 *            File extension .IN,.zip etc.
	 * @param pattern
	 *            Filename pattern
	 * @param enrollmentGIMonitorUtil
	 *            Util to log errors into the GI Monitor table
	 * @return a Json
	 * @see #SFTP_HOST
	 * @see #SFTP_USERNAME
	 * @see #SFTP_PASSWORD
	 * @see #SFTP_PORT
	 */
	public static String deleteFilesByExtensionAndPattern(Map<String, String> sftpParameters, String hostLocation,
			String extension, String pattern, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		List<EnrollmentSftpFileTransferDTO> fileTransferList = new ArrayList<EnrollmentSftpFileTransferDTO>();
		ChannelSftp channelSftp = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			channelSftp.cd(hostLocation);
			List<String> fileListToDelete = filterByPattern(
					filterByExtension(getFileList(channelSftp, hostLocation), extension), pattern);
			initiateThreadDelete(fileListToDelete, hostLocation, getSession(sftpSessionAndChannelMap),
					fileTransferList);
			LOGGER.info("File deleted successfully from host.");
		} catch (Exception ex) {
			LOGGER.error("Exception found while deleting the file :: " + ex.getMessage());
		} finally {
			closeResources(getSession(sftpSessionAndChannelMap), getChannel(sftpSessionAndChannelMap), channelSftp);
		}
		return gson.toJson(fileTransferList);
	}

	/**
	 * Initiates file delete from the remote location
	 * 
	 * @param fileListToDelete
	 * @param hostLocation
	 * @param session
	 * @param fileTransferList
	 * @throws InterruptedException
	 */
	private static void initiateThreadDelete(List<String> fileListToDelete, String hostLocation, Session session,
			List<EnrollmentSftpFileTransferDTO> fileTransferList) throws InterruptedException {
		if (!fileListToDelete.isEmpty()) {
			int size = fileListToDelete.size();
			int numberOfPartitions = size / MAX_CHANNEL_OPEN;
			if (size % MAX_CHANNEL_OPEN != 0) {
				numberOfPartitions++;
			}
			int firstIndex = 0;
			int lastIndex = 0;
			for (int i = 0; i < numberOfPartitions; i++) {
				ExecutorService executor = Executors.newFixedThreadPool(MAX_POOL_SIZE);
				List<Callable<EnrollmentSftpFileTransferDTO>> taskList = new ArrayList<>();
				firstIndex = i * MAX_CHANNEL_OPEN;
				lastIndex = (i + 1) * MAX_CHANNEL_OPEN;
				if (lastIndex > size) {
					lastIndex = size;
				}
				List<String> subList = fileListToDelete.subList(firstIndex, lastIndex);
				for (String downloadFile : subList) {
					taskList.add(new EnrollmentSftpDeleteThread(downloadFile, hostLocation, session));
				}
				executor.invokeAll(taskList).stream().map(future -> {
					try {
						return future.get();
					} catch (Exception ex) {
						LOGGER.error("Exception occurred in deleteByFilenameThread: " + ex.getMessage());
					}
					return null;
				}).filter(p -> p != null).forEach(e -> fileTransferList.add(e));
				executor.shutdown();
			}
		}

	}
	
	/**
	 * Initiates file dowload from the remote location
	 * @param fileListToDownload
	 * @param hostLocation
	 * @param targetLocation
	 * @param session
	 * @param fileTransferList
	 * @throws InterruptedException
	 */
	private static void initiateThreadDownload(List<String> fileListToDownload, String hostLocation,
			String targetLocation, Session session, List<EnrollmentSftpFileTransferDTO> fileTransferList)
			throws InterruptedException {
		if (!fileListToDownload.isEmpty()) {
			if (!new File(targetLocation).exists()) {
				EnrollmentUtils.createDirectory(targetLocation);
			}
			int size = fileListToDownload.size();
			int numberOfPartitions = size / MAX_CHANNEL_OPEN;
			if (size % MAX_CHANNEL_OPEN != 0) {
				numberOfPartitions++;
			}
			int firstIndex = 0;
			int lastIndex = 0;
			for (int i = 0; i < numberOfPartitions; i++) {
				ExecutorService executor = Executors.newFixedThreadPool(MAX_POOL_SIZE);
				List<Callable<EnrollmentSftpFileTransferDTO>> taskList = new ArrayList<>();
				firstIndex = i * MAX_CHANNEL_OPEN;
				lastIndex = (i + 1) * MAX_CHANNEL_OPEN;
				if (lastIndex > size) {
					lastIndex = size;
				}
				List<String> subList = fileListToDownload.subList(firstIndex, lastIndex);
				for (String downloadFile : subList) {
					taskList.add(new EnrollmentSftpDownloadThread(downloadFile, hostLocation, targetLocation, session));
				}
				executor.invokeAll(taskList).stream().map(future -> {
					try {
						return future.get();
					} catch (Exception ex) {
						LOGGER.error("Exception occurred in getFilesByExtensionThread: " + ex.getMessage());
					}
					return null;
				}).filter(p -> p != null).forEach(e -> fileTransferList.add(e));
				executor.shutdown();
			}
		}

	}
	
	/**
	 * Initiates file upload to the remote location
	 * @param filesToUpload
	 * @param sourceLocation
	 * @param hostLocation
	 * @param fileTransferList
	 * @param session
	 * @throws InterruptedException
	 */
	private static void initiateThreadUpload(List<String> filesToUpload, String sourceLocation, String hostLocation,
			List<EnrollmentSftpFileTransferDTO> fileTransferList, Session session) throws InterruptedException {
		if (!filesToUpload.isEmpty()) {
			int size = filesToUpload.size();
			int numberOfPartitions = size / MAX_CHANNEL_OPEN;
			if (size % MAX_CHANNEL_OPEN != 0) {
				numberOfPartitions++;
			}
			int firstIndex = 0;
			int lastIndex = 0;
			for (int i = 0; i < numberOfPartitions; i++) {
				ExecutorService executor = Executors.newFixedThreadPool(MAX_POOL_SIZE);
				List<Callable<EnrollmentSftpFileTransferDTO>> taskList = new ArrayList<>();
				firstIndex = i * MAX_CHANNEL_OPEN;
				lastIndex = (i + 1) * MAX_CHANNEL_OPEN;
				if (lastIndex > size) {
					lastIndex = size;
				}
				List<String> subList = filesToUpload.subList(firstIndex, lastIndex);
				for (String uploadFile : subList) {
					if(null != sourceLocation){
					taskList.add(new EnrollmentSftpUploadThread(sourceLocation + File.separator + uploadFile,
							hostLocation, session));
					}else{
						taskList.add(new EnrollmentSftpUploadThread(uploadFile,
								hostLocation, session));
					}
				}
				executor.invokeAll(taskList).stream().map(future -> {
					try {
						return future.get();
					} catch (Exception ex) {
						LOGGER.error("Exception occurred in getFilesByExtensionThread: " + ex.getMessage());
					}
					return null;
				}).filter(p -> p != null).forEach(e -> fileTransferList.add(e));
				executor.shutdown();
			}
		}

	}

	/**
	 * Filter file name by naming pattern
	 * 
	 * @param fileList
	 *            File name list
	 * @param pattern
	 *            Pattern to be matched
	 * @return Filtered list
	 */
	private static List<String> filterByPattern(List<String> fileList, String pattern) {
		List<String> filteredFileList = new ArrayList<String>();
		for (String fileName : fileList) {
			if (fileName.trim().toLowerCase().contains(pattern.toLowerCase())) {
				filteredFileList.add(fileName);
			}
		}
		return filteredFileList;
	}

	/**
	 * Get file list of a particular remote location
	 * 
	 * @param hostLocation
	 *            Base directory on the remote location
	 * @return File list
	 */
	public static List<String> getFileList(Map<String, String> sftpParameters, String hostLocation, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil ) {
		List<String> fileList = new ArrayList<String>();
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		ChannelSftp channelSftp = null;
		try {
			sftpSessionAndChannelMap = getSessionAndChannel(sftpParameters, enrollmentGIMonitorUtil);;
			channelSftp = (ChannelSftp) getChannel(sftpSessionAndChannelMap);
			return getFileList(channelSftp, hostLocation);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while getting file list", ex);
		} finally {
			closeResources(null, null, channelSftp);
		}
		return fileList;
	}

	/**
	 * Filter files by extension
	 * 
	 * @param extension
	 *            File extension .IN,.zip etc.
	 * @param fileList
	 *            File list
	 * @return Filtered list
	 */
	private static List<String> filterByExtension(List<String> fileList, String extension) {
		List<String> filteredFileList = new ArrayList<String>();
		for (String fileName : fileList) {
			if (fileName.trim().toLowerCase().endsWith(extension.toLowerCase())) {
				filteredFileList.add(fileName);
			}
		}
		return filteredFileList;
	}

	/**
	 * Get file list of a remote location
	 * 
	 * @param channelSftp
	 *            JSch channel
	 * @param hostLocation
	 * @return File list
	 */
	private static List<String> getFileList(ChannelSftp channelSftp, String hostLocation) {
		List<String> fileList = new ArrayList<String>();
		try {
			channelSftp.cd(hostLocation);
			@SuppressWarnings("unchecked")
			Vector<Object> vectorFileList = channelSftp.ls(hostLocation);
			for (Object entry : vectorFileList) {
				if (entry.getClass() == LsEntry.class) {
					LsEntry lsEntry = (LsEntry) entry;
					LOGGER.debug(lsEntry.getFilename());
					fileList.add(lsEntry.getFilename());
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while getting file list", ex);
		}
		return fileList;
	}

	private static void setFileInfo(EnrollmentSftpFileTransferDTO fileDto, File f, String transferType) {
		fileDto.setFileName(f.getName());
		fileDto.setTransferType(transferType);
		fileDto.setFileSize(String.valueOf(f.length()));
	}

	private static void setRemoteFileInfo(EnrollmentSftpFileTransferDTO fileDto, SftpATTRS lstat) {
		fileDto.setFileSize(String.valueOf(lstat.getSize()));
		fileDto.setFilePermissions(lstat.getPermissionsString());
	}

	/**
	 * Initialize a SFTP session and channel
	 * 
	 * @param channel
	 * @param session
	 * @param
	 * @throws Exception
	 */
	private static Map<String, Object> getSessionAndChannel(Map<String, String> sftpParameterMap,  EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) throws Exception {
		final JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		Map<String, Object> sftpSessionAndChannelMap = new HashMap<String, Object>();
		if (validateParameterMap(sftpParameterMap)) {
			try {
				LOGGER.info("Preparing the host information for SFTP.");
				session = jsch.getSession(sftpParameterMap.get(SFTP_USERNAME).trim(), sftpParameterMap.get(SFTP_HOST).trim(),
						Integer.parseInt(sftpParameterMap.get(SFTP_PORT).trim()));
				session.setPassword(sftpParameterMap.get(SFTP_PASSWORD).trim());
				java.util.Properties config = new java.util.Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
				session.connect();
				LOGGER.info("Host connected.");
				channel = session.openChannel("sftp");
				channel.connect();
				sftpSessionAndChannelMap.put(SESSION, session);
				sftpSessionAndChannelMap.put(CHANNEL, channel);
				LOGGER.info("SFTP channel opened and connected.");
			} catch (JSchException e) {
				LOGGER.error("JSchException encountered while initialiazing" + e.getMessage(), e);
				logBug(CONNECTION_ERR_MSG +" :: " + sftpParameterMap.get(SFTP_HOST) , e.getMessage(), enrollmentGIMonitorUtil);
				throw new Exception("JSchException encountered while initialiazing" + e.getMessage(), e);
			}
		} else {
			logBug(CONNECTION_ERR_MSG +" :: " + sftpParameterMap.get(SFTP_HOST) , "Invalid SFTP parameter map.", enrollmentGIMonitorUtil);
			throw new Exception("Invalid SFTP parameter map.");
		}
		return sftpSessionAndChannelMap;
	}

	private static boolean validateParameterMap(Map<String, String> sftpParameters) {
		return null != sftpParameters && !sftpParameters.isEmpty() && sftpParameters.containsKey(SFTP_USERNAME)
				&& sftpParameters.containsKey(SFTP_PASSWORD) && sftpParameters.containsKey(SFTP_HOST)
				&& sftpParameters.containsKey(SFTP_PORT) && null != sftpParameters.get(SFTP_USERNAME)
				&& null != sftpParameters.get(SFTP_PASSWORD) && null != sftpParameters.get(SFTP_HOST)
				&& null != sftpParameters.get(SFTP_PORT);

	}

	private static Channel getChannel(Map<String, Object> sftpSessionAndChannelMap) {
		if (null != sftpSessionAndChannelMap && !sftpSessionAndChannelMap.isEmpty()) {
			return (Channel) sftpSessionAndChannelMap.get(CHANNEL);
		}
		return null;
	}

	private static Session getSession(Map<String, Object> sftpSessionAndChannelMap) {
		if (null != sftpSessionAndChannelMap && !sftpSessionAndChannelMap.isEmpty()) {
			return (Session) sftpSessionAndChannelMap.get(SESSION);
		}
		return null;
	}

	private static void closeResources(Session session, Channel channel, ChannelSftp channelSftp) {
		if (null != channelSftp && channelSftp.isConnected()) {
			channelSftp.exit();
			LOGGER.info("SFTP Channel exited.");
		}
		if (null != channel) {
			channel.disconnect();
			LOGGER.info("Channel disconnected.");
		}
		if (null != session) {
			session.disconnect();
			LOGGER.info("Host Session disconnected.");
		}
	}
	
	/**
	 * Log JIRA
	 * @param summary
	 * @param description
	 */
	private static void logBug(String summary, String description, EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
		String ghixEnvironment = "PROD";
		if (!EnrollmentConstants.ENROLLMENT_ENVIRONMENT_PRODUCTION.equalsIgnoreCase(DynamicPropertiesUtil
				.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_CMS_ENVIRONMENT))
				&& !"R".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_CMS_ENVIRONMENT))) {
			ghixEnvironment = "QA";
		}
		summary = ghixEnvironment+" : "+summary;
		if(isJiraEnabled){
		JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
				Arrays.asList(fixVersion), 
				description,
				summary,
				null);
	}
		 enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.SFTP,
				 summary, description);
	}
	
	/**
	 * Util method to check SFTP connection
	 * @param sftpParameterMap
	 * @throws Exception
	 */
	public static void checkConnection(Map<String, String> sftpParameterMap) throws Exception {
		final JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		if (validateParameterMap(sftpParameterMap)) {
			try {
				LOGGER.info("Preparing the host information for SFTP.");
				session = jsch.getSession(sftpParameterMap.get(SFTP_USERNAME).trim(), sftpParameterMap.get(SFTP_HOST).trim(),
						Integer.parseInt(sftpParameterMap.get(SFTP_PORT).trim()));
				session.setPassword(sftpParameterMap.get(SFTP_PASSWORD).trim());
				java.util.Properties config = new java.util.Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
				session.connect();
				LOGGER.info("Host connected.");
				channel = session.openChannel("sftp");
				channel.connect();
				LOGGER.info("SFTP channel opened and connected.");
			} catch (JSchException e) {
				LOGGER.error("JSchException encountered while initialiazing" + e.getMessage(), e);
				throw new Exception("JSchException encountered while initialiazing" + e.getMessage(), e);
			}  finally {
				closeResources(session, channel, null);
			}
		}
	}
}
