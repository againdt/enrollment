package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

public interface ExitSurveyEmailNotificationService {

	void sendEmailNotification(Household household) throws NotificationTypeNotFound;
}
