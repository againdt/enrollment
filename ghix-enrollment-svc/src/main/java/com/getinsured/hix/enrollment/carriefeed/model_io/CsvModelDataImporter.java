package com.getinsured.hix.enrollment.carriefeed.model_io;

import com.getinsured.hix.enrollment.carriefeed.model.CsvModel;
import com.getinsured.hix.enrollment.carrierfeed.util.CsvFileReader;
import com.getinsured.hix.enrollment.carrierfeed.util.DataImporterBase;
import com.getinsured.hix.enrollment.carrierfeed.util.ExcelClassicFileReader;
import com.getinsured.hix.enrollment.carrierfeed.util.FileType;

import java.io.IOException;

/**
 * Created by jabelardo on 7/10/14.
 */
public class CsvModelDataImporter extends DataImporterBase<CsvModel> {

    public CsvModelDataImporter(String dataFilename, FileType fileType, String mappingsFilename) throws IOException {
        super(dataFilename, fileType, mappingsFilename);
    }

    @Override
    protected void registerFileReaderCreators() {
        register(FileType.EXCEL_CLASSIC, new ExcelClassicFileReader<CsvModel>());
        register(FileType.CSV, new CsvFileReader<CsvModel>());
    }

    @Override
    protected Class getModelClass() {
        return CsvModel.class;
    }
}
