package com.getinsured.hix.enrollment.carrierfeed.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by jabelardo on 7/14/14.
 */
public abstract class ExcelFileReaderBase<T> extends FileReader<T> {

    private static final Logger logger = LoggerFactory.getLogger(ExcelFileReaderBase.class);

    protected abstract Workbook createWorkbook(InputStream inStream) throws IOException;

    @Override
    protected List<T> createImportList(Class<T> modelClass) throws IOException {
        List<T> importList = new ArrayList<>();
        InputStream inStream = null;
        try {
            inStream = new FileInputStream(getDataFilename());
            Workbook wb = createWorkbook(inStream);
            Sheet sheet = wb.getSheetAt(0);
            int firstDataRow = getMappings().get("firstDataRow").asInt();
            int lastRowNum = sheet.getLastRowNum();
            for (int rowNum = firstDataRow; rowNum <= lastRowNum; rowNum++) {
                Row row = sheet.getRow(rowNum);
                if (row == null) {
                    continue;
                }
                T model = importModel(modelClass, getMappings().get("fields"), row);
                importList.add(model);
            }
        } finally {
            if (inStream != null) {
                inStream.close();
            }
        }
        return importList;
    }

    private T importModel(Class<T> modelClass, JsonNode fieldNodes, Row row) {
        T model = null;
        try {
            model = modelClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        int cellNum = 0;
        for (JsonNode fieldNode : fieldNodes) {
            Cell cell = row.getCell(cellNum);
            cellNum++;
            if (cell == null) {
                continue;
            }
            String fieldName = fieldNode.get("fieldName").asText();
            JsonNode fieldFormatNode = fieldNode.get("fieldFormat");
            String fieldFormat = fieldFormatNode == null ? null : fieldFormatNode.asText();
            Field field = null;
            try {
                field = model.getClass().getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                logger.error(String.format("Can't find in class %s field %s", modelClass.getCanonicalName(), fieldName) , e);
                continue;
            }
            field.setAccessible(true);
            Class<?> fieldType = field.getType();

            int cellType = cell.getCellType();
            Object value = null;
            if (cellType != Cell.CELL_TYPE_BLANK) {

                if (fieldType.equals(Date.class)) {
                    value = readDateValue(cell, fieldFormat, cellType);

                } else if (fieldType.equals(String.class)) {
                    value = readStringValue(cell, fieldFormat, cellType);

                } else if (fieldType.equals(Byte.class)) {
                    value = readByteValue(cell, cellType);

                } else if (fieldType.equals(Short.class)) {
                    value = readShortValue(cell, cellType);

                } else if (fieldType.equals(Integer.class)) {
                    value = readIntegerValue(cell, cellType);

                } else if (fieldType.equals(Long.class)) {
                    value = readLongValue(cell, cellType);

                } else if (fieldType.equals(Float.class)) {
                    value = readByteValue(cell, cellType);

                } else if (fieldType.equals(Double.class)) {
                    value = readDoubleValue(cell, cellType);

                } else if (fieldType.equals(Character.class)) {
                    value = readCharacterValue(cell, cellType);

                } else if (fieldType.equals(Boolean.class)) {
                    value = readBooleanValue(cell, cellType);

                } else {
                    logger.warn(String.format("Don't know how to read in class %s field %s type %s",
                            modelClass.getCanonicalName(), fieldName, fieldType.toString()));
                }
            }
            try {
                field.set(model, value);
            } catch (IllegalAccessException e) {
                logger.error(String.format("Can't set field value in class %s field %s",
                        modelClass.getCanonicalName(), fieldName) , e);
            }
        }
        return model;
    }

    private Boolean readBooleanValue(Cell cell, int cellType) {
        Boolean value = null;
        if (cellType == Cell.CELL_TYPE_BOOLEAN) {
            value = cell.getBooleanCellValue();

        } else if (cellType == Cell.CELL_TYPE_NUMERIC) {
            value = (int) cell.getNumericCellValue() != 0;

        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String stringCellValue = cell.getStringCellValue();
            if (Arrays.binarySearch(booleanTrueValues, stringCellValue.toLowerCase()) > -1) {
                value = true;
            }
            if (Arrays.binarySearch(booleanFalseValues, stringCellValue.toLowerCase()) > -1) {
                value = false;
            }
        }
        return value;
    }

    private Character readCharacterValue(Cell cell, int cellType) {
        Character value = null;
        if (cellType == Cell.CELL_TYPE_NUMERIC) {
            value = (char) cell.getNumericCellValue();

        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String stringCellValue = cell.getStringCellValue().trim();
            if (!stringCellValue.isEmpty()) {
                value = stringCellValue.charAt(0);
            }
        } else if (cellType == Cell.CELL_TYPE_BOOLEAN) {
            value = cell.getBooleanCellValue() ? 't' : 'f';
        }
        return value;
    }

    private Short readShortValue(Cell cell, int cellType) {
        Short value = null;
        if (cellType == Cell.CELL_TYPE_NUMERIC) {
            value = (short) cell.getNumericCellValue();

        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String stringCellValue = cell.getStringCellValue().trim();
            try {
                if (!stringCellValue.isEmpty()) {
                    value = Short.valueOf(stringCellValue);
                }
            } catch (NumberFormatException e) {
                logger.error(String.format("Can't parse numeric value %s ", stringCellValue));
            }
        } else if (cellType == Cell.CELL_TYPE_BOOLEAN) {
            value = (short) (cell.getBooleanCellValue() ? 1 : 0);
        }
        return value;
    }

    private Integer readIntegerValue(Cell cell, int cellType) {
        Integer value = null;
        if (cellType == Cell.CELL_TYPE_NUMERIC) {
            value = (int) cell.getNumericCellValue();

        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String stringCellValue = cell.getStringCellValue().trim();
            try {
                if (!stringCellValue.isEmpty()) {
                    value = Integer.valueOf(stringCellValue);
                }
            } catch (NumberFormatException e) {
                logger.error(String.format("Can't parse numeric value %s ", stringCellValue));
            }
        } else if (cellType == Cell.CELL_TYPE_BOOLEAN) {
            value = cell.getBooleanCellValue() ? 1 : 0;
        }
        return value;
    }

    private Long readLongValue(Cell cell, int cellType) {
        Long value = null;
        if (cellType == Cell.CELL_TYPE_NUMERIC) {
            value = (long) cell.getNumericCellValue();

        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String stringCellValue = cell.getStringCellValue().trim();
            try {
                if (!stringCellValue.isEmpty()) {
                    value = Long.valueOf(stringCellValue);
                }
            } catch (NumberFormatException e) {
                logger.error(String.format("Can't parse numeric value %s ", stringCellValue));
            }
        } else if (cellType == Cell.CELL_TYPE_BOOLEAN) {
            value = cell.getBooleanCellValue() ? 1L : 0L;
        }
        return value;
    }

    private Byte readByteValue(Cell cell, int cellType) {
        Byte value = null;
        if (cellType == Cell.CELL_TYPE_NUMERIC) {
            value = (byte) cell.getNumericCellValue();

        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String stringCellValue = cell.getStringCellValue().trim();
            try {
                if (!stringCellValue.isEmpty()) {
                    value = Byte.valueOf(stringCellValue);
                }
            } catch (NumberFormatException e) {
                logger.error(String.format("Can't parse numeric value %s ", stringCellValue));
            }
        } else if (cellType == Cell.CELL_TYPE_BOOLEAN) {
            value = (byte) (cell.getBooleanCellValue() ? 1 : 0);
        }
        return value;
    }


    private Double readDoubleValue(Cell cell, int cellType) {
        Double value = null;
        if (cellType == Cell.CELL_TYPE_NUMERIC) {
            value = cell.getNumericCellValue();

        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String stringCellValue = cell.getStringCellValue().trim();
            try {
                if (!stringCellValue.isEmpty()) {
                    value = Double.valueOf(stringCellValue);
                }
            } catch (NumberFormatException e) {
                logger.error(String.format("Can't parse numeric value %s ", stringCellValue));
            }
        } else if (cellType == Cell.CELL_TYPE_BOOLEAN) {
            value = (double) (cell.getBooleanCellValue() ? 1 : 0);
        }
        return value;
    }

    private Date readDateValue(Cell cell, String fieldFormat, int cellType) {
        Date value = null;
        if (cellType == Cell.CELL_TYPE_NUMERIC) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            if (DateUtil.isCellDateFormatted(cell)) {
                value = cell.getDateCellValue();
            } else {
                value = TSDate.getNoOffsetTSDate((long) cell.getNumericCellValue());
            }
        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String format = fieldFormat == null ? "MM/dd/yyyy" : fieldFormat;
            String stringCellValue = cell.getStringCellValue().trim();
            try {
                if (!stringCellValue.isEmpty()) {
                    SimpleDateFormat formatter = new SimpleDateFormat(format);
                    value = formatter.parse(stringCellValue);
                }
            } catch (ParseException e) {
                logger.error(String.format("Can't parse date value with format %s value %s ",
                        format, stringCellValue));
            }
        } else {
            logger.warn(String.format("Don't know how to get %s values from %s cells", "Date", getCellTypeString(cellType)));
        }
        return value;
    }

    private static String getCellTypeString(int cellType) {
        switch (cellType) {
            case Cell.CELL_TYPE_NUMERIC:
                return "CELL_TYPE_NUMERIC";
            case Cell.CELL_TYPE_STRING:
                return "CELL_TYPE_STRING";
            case Cell.CELL_TYPE_FORMULA:
                return "CELL_TYPE_FORMULA";
            case Cell.CELL_TYPE_BLANK:
                return "CELL_TYPE_BLANK";
            case Cell.CELL_TYPE_BOOLEAN:
                return "CELL_TYPE_BOOLEAN";
            case Cell.CELL_TYPE_ERROR:
                return "CELL_TYPE_ERROR";
        }
        throw new RuntimeException("Unknown cell type " + cellType);
    }

    private String readStringValue(Cell cell, String fieldFormat, int cellType) {
        String value = null;
        if (cellType == Cell.CELL_TYPE_NUMERIC) {
            if (DateUtil.isCellDateFormatted(cell)) {
                String format = fieldFormat == null ? "MM/dd/yyyy" : fieldFormat;
                SimpleDateFormat formatter = new SimpleDateFormat(format);
                value = formatter.format(cell.getDateCellValue());
            } else {
                String format = fieldFormat == null ? "#" : fieldFormat;
                NumberFormat formatter = new DecimalFormat(format);
                value = formatter.format(cell.getNumericCellValue());
            }
        } else if (cellType == Cell.CELL_TYPE_STRING) {
            String stringCellValue = cell.getStringCellValue().trim();
            if (!stringCellValue.isEmpty()) {
                value = stringCellValue;
            }
        } else if (cellType == Cell.CELL_TYPE_BOOLEAN) {
            value = cell.getBooleanCellValue() ? "t" : "f";
        }
        return value;
    }
}
