package com.getinsured.hix.enrollment.service;

import java.io.File;
import java.io.FileFilter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrollmentPrivateRepository;
import com.getinsured.hix.enrollment.util.CommissionExcelReader;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.ExcelReader;
import com.getinsured.hix.model.consumer.FFMHouseholdResponse;
import com.getinsured.hix.model.consumer.FFMMemberResponse;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints.PlandisplayEndpoints;
import com.getinsured.hix.platform.util.exception.GIException;
/**
 * 
 * @author parhi_s
 * @since 18/06/2013
 */
@Service("enrollmentBatchPrivateService")
@Transactional
public class EnrollmentBatchPrivateServiceImpl implements EnrollmentBatchPrivateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentBatchPrivateServiceImpl.class);
//	private final SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.FILENAME_DATE_FORMAT);
	public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	public static final String ERROR_MOVE_FILES="Error in moveFiles()";
//	@Autowired private RestTemplate restTemplate;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
//	@Autowired	private IEnrolleePrivateRepository enrolleeRepository;
	@Autowired	private IEnrollmentPrivateRepository enrollmentRepository;
//	@Autowired	private IEnrolleeAudPrivateRepository enrolleeAudRepository;
//	@Autowired	private IEnrollmentEsignaturePrivateRepository enrollmentEsignatureRepository;
//	@Autowired private IEnrolleeRelationshipPrivateRepository enrolleeRelationshipRepository;
//	@Autowired	private IEnrolleeRaceAudPrivateRepository enrolleeRaceAudRepository;
//	@Autowired private IEnrolleeRelationshipAudPrivateRepository enrolleeRelationshipAudRepository;
//	@Autowired private IEnrolleeRacePrivateRepository enrolleeRaceRepository;
//	@Autowired private IEnrollmentLocationPrivateRepository locationRepository;
	/*@Autowired private ICarrierFeedSummaryRepository carrierFeedSummaryRepository;*/
	/*@Autowired private ICarrierFeedDetailsRepository carrierFeedDetailsRepository;*/
//	@Autowired private UserService userService;
//	@Autowired private EnrolleePrivateService enrolleeService;
	@Autowired private EnrollmentPrivateService enrollmentService;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private LookupService lookupService;
	//@Autowired private EnrollmentEventService  enrollmentEventService;
//	@Autowired private EnrolleeRaceAuditPrivateService enrolleeRaceAuditService;
//	@Autowired private EnrolleeAuditPrivateService enrolleeAuditService;
//	@Autowired private ValidationFolderPathPrivateService validationFolderPathService;
	@Autowired private ExcelReader excelReader;
	@Autowired private CommissionExcelReader commissionExcelReader;
	
/*	@Value("#{configProp['enrollment.XMLCMSReconExtractPath']}")
	private String xmlCMSReconExtractPath;
	
	@Value("#{configProp['enrollment.XMLCmsReconCancelTermEnrollmentPath']}")
	private String xmlCmsReconCancelTermEnrollmentPath;
	
	@Value("#{configProp['enrollment.XMLCMSDailyFilePath']}")
	private String xmlCMSDailyFilePath;*/
	
//	@Value("#{configProp['enrollment.XMLExtractPath']}")
//	private String xmlExtractPath;
	
//	@Value("#{configProp['enrollment.inbound834XMLPath']}")
//	private String inbound834XMLPath;
	
	@Value("#{configProp['enrollment.inboundFeedImporterPath']}")
	private String inboundFeedImporterPath;
	
	@Value("#{configProp['enrollment.CommissionFeedInboundPath']}")
	private String commissionFeedInboundPath;
	
	@Value("#{configProp['enrollment.BobFeedInboundPath']}")
	private String bobFeedInboundPath;
	
	/**
	 * Returns the start time of the Last Month u
	 * 
	 * @return Date
	 */
	/*private Date getLastMonthStartDateTime() throws GIException{
		Date startDate=null;
		try{
			Calendar today = Calendar.getInstance();
			today.add(Calendar.MONTH, -1);
			today.set(Calendar.DATE, today.getMinimum(Calendar.DATE));
			today.set(Calendar.HOUR_OF_DAY,today.getMinimum(Calendar.HOUR_OF_DAY));
			today.set(Calendar.MINUTE, today.getMinimum(Calendar.MINUTE));
			today.set(Calendar.SECOND, today.getMinimum(Calendar.SECOND));
			
			startDate= today.getTime();
		}catch(Exception e){
			throw new GIException("Error in getLastMonthStartDateTime()::"+e.getMessage(),e);
		}
		return startDate;
	}*/
	

	/**
	 * Returns the start time of the current Month 
	 * 
	 * 
	 * @return Date
	 */
	/*private  Date getCurrentMonthStartDateTime() throws GIException{
		Date startDate=null;
		try{
		Calendar today = Calendar.getInstance();
		today.set(Calendar.DATE, today.getMinimum(Calendar.DATE));
		today.set(Calendar.HOUR_OF_DAY,today.getMinimum(Calendar.HOUR_OF_DAY));
		today.set(Calendar.MINUTE, today.getMinimum(Calendar.MINUTE));
		today.set(Calendar.SECOND, today.getMinimum(Calendar.SECOND));
		
		startDate= today.getTime();
		}catch(Exception e){
			throw new GIException("Error in getCurrentMonthStartDateTime()::"+e.getMessage(),e);
		}
		return startDate;
	}
	*//**
	 * @author parhi_s
	 * @since
	 * This method is used by reconciliation jobs to set enrollments object
	 * @param enrollmentList
	 * @return
	 * @throws GIException
	 *//*
	
	private Enrollments setEnrollmentsFor834Reconciliation(List<Enrollment> enrollmentList, String statusType) throws GIException{
		Enrollments enrollments=null;
		List<Enrollment> updatedEnrollmentList=null;
		try{
			if(enrollmentList!=null && !enrollmentList.isEmpty()){
				updatedEnrollmentList= new ArrayList<Enrollment>();
				for(Enrollment enrollment:enrollmentList){
					int totalEnrollees=0;
					List<Enrollee> enrolleeList=null;
					if(statusType!=null && statusType.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)){
						enrolleeList=enrolleeRepository.getInActiveEnrollees(enrollment.getId());
						if(enrolleeList!=null && !enrolleeList.isEmpty() && enrollment.getEnrollmentTypeLkp()!=null &&enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
							enrolleeList= filterOutPendingToCancelEnrollees(enrolleeList);
						}
					}else{
						enrolleeList=enrolleeRepository.getEnrolleeByStatusAndEnrollmentID(enrollment.getId(), EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM);
					}
					
					EnrollmentEsignature enrollesig = enrollmentEsignatureRepository.findByEnrollmentId(enrollment.getId());
					if(enrolleeList!=null && !enrolleeList.isEmpty()){
						enrollment.setEnrollees(enrolleeList);
						updatedEnrollmentList.add(enrollment);
						totalEnrollees=enrolleeList.size();
						enrollment.setQTYt(totalEnrollees);
						enrollment.setQTYy(1);
						enrollment.setQTYn(totalEnrollees-1);
						
						
						//removing '-' from  InsurerTaxIdNumber
						if(enrollment.getInsurerTaxIdNumber()!=null){
							enrollment.setInsurerTaxIdNumber(enrollment.getInsurerTaxIdNumber().replaceAll("[-]", ""));
						}
						
						Enrollee subscriber = null;
						for (Enrollee enrollee:enrolleeList){
							if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals((EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER))){
								//Found the subscriber
								subscriber = enrollee;
								break;
							}
						}
						for(Enrollee enrollee:enrolleeList){
							//check whether to display mailing address or not 
							enrollee.setShowMailingAddress(EnrollmentPrivateConstants.TRUE);
							if(compareAddress(enrollee.getHomeAddressid(), enrollee.getMailingAddressId())){
								enrollee.setShowMailingAddress(EnrollmentPrivateConstants.FALSE);
							}
							
							//set relationship to subscriber
							if(subscriber!=null){
								EnrolleeRelationship enroleeRelationship = enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrollee.getId(), subscriber.getId());
									if(enroleeRelationship!=null){
									enrollee.setRelationshipToSubscriberLkp(enroleeRelationship.getRelationshipLkp());
									}
								}
							
							if(enrollesig != null)
							{
								enrollee.setFormatSignatureDate(enrollesig.getEsignature().getEsignDate());
							}
						}
					
						//code to set total CSR amount
						if(enrollment.getCsrAmt()!=null){
							enrollment.setTotalCSRAmt(totalEnrollees*enrollment.getCsrAmt());
						}
					
						Enrollee responsibleEnrollee = enrolleeService.getResponsiblePersonByEnrollmentID(enrollment.getId());
						if(responsibleEnrollee != null){
						enrollment.setResponsiblePerson(responsibleEnrollee);
						}
					
					}
					
					if(updatedEnrollmentList!=null && !updatedEnrollmentList.isEmpty()){
						enrollments=new Enrollments();
						enrollments.setEnrollmentList(updatedEnrollmentList);
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("setEnrollmentsFor834Reconciliation()::"+e.getMessage(),e);
			throw new GIException("Error in setEnrollmentsFor834Reconciliation()::"+e.getMessage(),e);
			
		}
		return enrollments;
	}*/
	
	/**
	 * @author parhi_s
	 * 
	 * @param enrolleeList
	 * @return
	 * @throws GIException
	 */
	
	/*private List<Enrollee> filterOutPendingToCancelEnrollees(
			List<Enrollee> enrolleeList) throws GIException {
		List<Enrollee> inactEnrollees=null;
		try{
			if(enrolleeList!=null && !enrolleeList.isEmpty()){
				inactEnrollees=new ArrayList<Enrollee>();
				for(Enrollee enrollee:enrolleeList){
					if(enrollee.getEnrolleeLkpValue() != null && 
							enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)){
						List<EnrolleeAud> enrolleeAudList = enrolleeAudRepository.getEnrolleeByIdRevAndNoPendingStatus(enrollee.getId());
						if(enrolleeAudList == null || enrolleeAudList.isEmpty()){
							continue;
						}
						inactEnrollees.add(enrollee);
					}
				}
			}
			return inactEnrollees;
		}catch(Exception e){
			LOGGER.error("filterOutPenfindEnrollmentAndEnrolleeDataByNamedingToCancelEnrollees()::"+e.getMessage(),e);
			throw new GIException("Error in filterOutPendingToCancelEnrollees()::"+e.getMessage(),e);
		}
	}
	
	*//**
	 * @since
	 * @author parhi_s
	 * @param enrollments
	 * @param outputPath
	 * @throws Exception
	 * 
	 * This method is used to Create XML report by taking
	 *  Enrollments record for enrollmentXML job
	 *//*

	private void generateEnrollmentsXMLReport(Enrollments enrollments,String outputPath, String xsltPath) throws GIException {	
		try {
			LOGGER.info("generateEnrollmentsXMLReport outputPath = "+ outputPath);
			if(enrollments!=null && enrollments.getEnrollmentList()!=null && !enrollments.getEnrollmentList().isEmpty()){			
				String strXML = null;
				InputStream strXSLPath = EnrollmentPrivateServiceImpl.class.getClassLoader().getResourceAsStream(xsltPath);
				InputStream strEmptyTagXSLPath = EnrollmentPrivateServiceImpl.class.getClassLoader().getResourceAsStream("removeEmptyTags.xsl");
				JAXBContext context = JAXBContext.newInstance(Enrollments.class, Enrollment.class, ArrayList.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

				StringWriter writerEnroll = new StringWriter();
				StreamResult resultEnroll = new StreamResult(writerEnroll);
				for(Enrollment enrollment : enrollments.getEnrollmentList()){
					for(Enrollee enrollee : enrollment.getEnrollees()){
						getFormatedRatingArea(enrollee);
					}
				}
				
				m.marshal(enrollments, resultEnroll);
				String strEnrollTrans = new String(writerEnroll.toString());
				TransformerFactory factory = null;
				factory = TransformerFactory.newInstance();
				Source xslSource = new StreamSource(strXSLPath);
				StreamSource xmlJAXB = new StreamSource(new StringReader(strEnrollTrans));
				Templates template = factory.newTemplates(xslSource);
				Transformer transformer = template.newTransformer();
				StringWriter writerXSLT = new StringWriter();
				StreamResult resultXSLT = new StreamResult(writerXSLT);
				transformer.transform(xmlJAXB, resultXSLT);

				strXML = new String(writerXSLT.toString());
				StreamSource xmlWithEmpty = new StreamSource(new StringReader(strXML));
				Source xslEmptyTagSource = new StreamSource(strEmptyTagXSLPath);
				Templates templateEmpty = factory.newTemplates(xslEmptyTagSource);
				Transformer transformerEmpty = templateEmpty.newTransformer();
				transformerEmpty.transform(xmlWithEmpty, new StreamResult(new File(outputPath)));
			}	

		} catch (Exception exception) {

			LOGGER.error(" EnrollmentServiceImpl.generateEnrollmentsXMLReport Exception "+ exception.getMessage());
			throw new GIException(" EnrollmentServiceImpl.generateEnrollmentsXMLReport Exception ",exception);

		}
	}
*/

/*	@Override
	public void serveEnrollmentReconciliationJob(String extractMonth, String enrollmentType) throws GIException{
		LOGGER.info("ServeEnrollmentReconciliationJob SERVICE : START");
		try{
			Date startDate=null;
			Date endDate=null;
			String outboundReconXmlExtractPath = "";
			String hiosIssuerId ="";
			
			if(extractMonth!=null && extractMonth.equalsIgnoreCase(EnrollmentPrivateConstants.IND_RECON_EXTRACT_MONTH_PRIOR)){
				startDate=getLastMonthStartDateTime();
				endDate=getCurrentMonthStartDateTime();
			}else{
				startDate=getCurrentMonthStartDateTime();
				endDate=new TSDate();
			}
			
			LOGGER.info("ServeEnrollmentReconciliationJob SERVICE : startDate = "+startDate +" || endDate = "+endDate+" || enrollmentType = "+enrollmentType);
			
			List<String> taxIDList= enrollmentRepository.getinsurerTaxIdNumbers( EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM, startDate, endDate, enrollmentType);
			if(taxIDList!=null && !taxIDList.isEmpty()){
				String outputfolder=null;
				LOGGER.info("ServeEnrollmentReconciliationJob SERVICE : outputfolder = "+outputfolder);
				
				for(String insurerTaxId :taxIDList ){
					if(insurerTaxId!=null){
						String outputPath=null;
						StringBuilder outputPathBuilder = new StringBuilder();
						
						List<Enrollment>  enrollmentList= new ArrayList<Enrollment>();
						enrollmentList= enrollmentRepository.findEnrollmentByStatusAndUpdateDate(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM, startDate, endDate, enrollmentType, insurerTaxId);
						Enrollments enrollments= setEnrollmentsFor834Reconciliation(enrollmentList,EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM);				
						// setting the source exchange id 
						Issuer issuer= enrollmentList.get(0).getIssuer();
						enrollments.setSourceExchgId(DynamicPropertiesUtil.getPropertyValue(EnrollmentPrivateConfiguration.EnrollmentConfigurationEnum.SOURCE_EXCHANGE_ID));
						if(isNotNullAndEmpty(issuer.getHiosIssuerId())){	
							
							LOGGER.info("calling FindExchgPartnerByHiosIssuerID to create ISA section in XML");
							LOGGER.info("HIOS_ISSUER_ID =" + issuer.getHiosIssuerId());
							LOGGER.info("GhixConstants.ISA06 =" + GhixConstants.ISA06);
							LOGGER.info("GhixConstants.ISA05 =" + GhixConstants.ISA05);
							LOGGER.info("GhixConstants.GS08 =" + GhixConstants.GS08);
							ExchgPartnerLookup exchgPartnerLookup = null;
							if(enrollmentType.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
							 exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(issuer.getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_SHOP,GhixConstants.OUTBOUND);
							}
							if(enrollmentType.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
								 exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(issuer.getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL,GhixConstants.OUTBOUND);
							}
								
							if(exchgPartnerLookup != null){
									enrollments.setISA05(exchgPartnerLookup.getIsa05());
									enrollments.setISA06(exchgPartnerLookup.getIsa06());
									enrollments.setISA07(exchgPartnerLookup.getIsa07());
									enrollments.setISA08(exchgPartnerLookup.getIsa08());
               				        enrollments.setISA15(exchgPartnerLookup.getIsa15());
									enrollments.setGS02(exchgPartnerLookup.getGs02());
									enrollments.setGS03(exchgPartnerLookup.getGs03());
									if(exchgPartnerLookup.getSourceDir() != null){
										  outboundReconXmlExtractPath = exchgPartnerLookup.getSourceDir();
										}
								}
							}
						hiosIssuerId = issuer.getHiosIssuerId() != null ? issuer.getHiosIssuerId().toString() : "hiosIssuerId";
						outputfolder = outboundReconXmlExtractPath;
						
						
						if(outputfolder!=null && !("".equals(outputfolder.trim()))){
							if (!(new File(outputfolder).exists())) {
								throw new GIException("ServeEnrollmentReconciliationJob SERVICE : No Source Directory Exists defined in ExchangePartner Lookup Table");
							}
						}else{
							throw new GIException("ServeEnrollmentReconciliationJob SERVICE :No Source Directory defined in ExchangePartner Lookup Table");
						}
						
						outputPathBuilder.append(outputfolder);
						outputPathBuilder.append(File.separator);
						outputPathBuilder.append("to_");
						outputPathBuilder.append(hiosIssuerId);
						outputPathBuilder.append("_");
					
						if (enrollmentType.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
							if(EnrollmentPrivateConfiguration.isCaCall()){	
								outputPathBuilder.append(GhixConstants.CA_834_RECONSHOP);
//								outputPath = outputfolder + File.separator +"to_" + hiosIssuerId + "_"+ GhixConstants.CA_834_RECONSHOP + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
							else if(EnrollmentPrivateConfiguration.isNmCall()){
								outputPathBuilder.append(GhixConstants.NM_834_RECONSHOP );
//								outputPath = outputfolder + File.separator +"to_" + hiosIssuerId + "_"+ GhixConstants.NM_834_RECONSHOP + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
						}
						if (enrollmentType.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
							if(EnrollmentPrivateConfiguration.isCaCall()){
								outputPathBuilder.append(GhixConstants.CA_834_RECONINDIV);
//								outputPath = outputfolder + File.separator +"to_" + hiosIssuerId + "_"+ GhixConstants.CA_834_RECONINDIV + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
							else if(EnrollmentPrivateConfiguration.isNmCall()){
								outputPathBuilder.append(GhixConstants.NM_834_RECONINDIV);
//								outputPath = outputfolder + File.separator +"to_" + hiosIssuerId + "_"+ GhixConstants.NM_834_RECONINDIV + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
						}
						
						outputPathBuilder.append("_");
						outputPathBuilder.append(dateFormat.format(new TSDate()));
						outputPathBuilder.append(".xml");
						outputPath = outputPathBuilder.toString();
					
						generateEnrollmentsXMLReport(enrollments, outputPath,"INDEnrollmentReconXMLTrans.xsl");
					
					
					}
				}
			}
			
		LOGGER.info("ServeEnrollmentReconciliationJob SERVICE : END");
		}catch(Exception e){
			LOGGER.error("Error in serveEnrollmentReconciliationJob() ::"+e.getMessage(),e);		
			throw new GIException("Error in serveEnrollmentReconciliationJob() ::"+e.getMessage(),e);
		}
	}
	
	
	
	@Override
	public void serveCMSReconciliationJob(String extractMonth) throws GIException{
		try{
			LOGGER.info(" serveCMSReconciliationJob Service : START");
			Date startDate=null;
			Date endDate=null;
			
			if(extractMonth!=null && extractMonth.equalsIgnoreCase(EnrollmentPrivateConstants.IND_RECON_EXTRACT_MONTH_PRIOR)){
				startDate=getLastMonthStartDateTime();
				endDate=getCurrentMonthStartDateTime();
			}else{
				LOGGER.info(" serveCMSReconciliationJob Service ExtractMonth is NULL ");
				startDate=getCurrentMonthStartDateTime();
				endDate=new TSDate();
			}		
			
				
			String outputfolder=null;
			if(xmlCMSReconExtractPath!=null && !("".equals(xmlCMSReconExtractPath.trim()))){
				outputfolder = xmlCMSReconExtractPath;
			}else {
				throw new GIException("Error in serveCMSReconciliationJob() :: No Output Path defined in the configuration file");
			}

			if (!(new File(outputfolder).exists())) {
				new File(outputfolder).mkdirs();
				LOGGER.info(" serveCMSReconciliationJob Service : outputfolder Created Path ="+outputfolder);
			}
			String outputPath=null;
			List<Enrollment>  enrollmentList= new ArrayList<Enrollment>();
			enrollmentList= enrollmentRepository.findEnrollmentByStatusAndUpdateDate(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM, startDate, endDate);
		
			Enrollments enrollments= setEnrollmentsFor834Reconciliation(enrollmentList,EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM);				
			outputPath = outputfolder + File.separator + dateFormat.format(new TSDate()) + ".xml";				
			generateEnrollmentsXMLReport(enrollments, outputPath,"INDEnrollmentReconXMLTrans.xsl");
					
			
		}catch(Exception e){
			LOGGER.error("Error in serveCMSReconciliationJob() ::"+e.getMessage(),e);		
			throw new GIException("Error in serveCMSReconciliationJob() ::"+e.getMessage(),e);
		}
		LOGGER.info(" serveCMSReconciliationJob Service : END");
	}
	
	@Override
	public void cmsDailyEnrollmentReport() throws GIException{
		Date lastRunDate = null;
		Date startDate=null;
		Date endDate=new TSDate();
		List<Enrollment> enrollmentList = null;
		Enrollments enrollments = null;
		Issuer issuer=new Issuer();
		String outputPath ="";
			try {
			LOGGER.info(" cmsDailyEnrollmentReport Service Start ");
			lastRunDate=getJOBLastRunDate(EnrollmentPrivateConstants.CMS_DAILY_ENROLLMENT_BATCH_JOB);
			if(lastRunDate!=null){
				startDate=lastRunDate;
			}else{
				startDate= DateUtil.StringToDate(EnrollmentPrivateConstants.DEFAULT_BATCH_START_DATE, EnrollmentPrivateConstants.BATCH_DATE_FORMAT);
			}
			LOGGER.info(" cmsDailyEnrollmentReport Service : Get list of enrollment");
			if(startDate!=null && endDate!=null){
				enrollmentList = enrollmentRepository.getEnrollmentsUpdatedAfter(startDate,endDate);
			}
			if(enrollmentList != null && !enrollmentList.isEmpty()){
				if(xmlCMSDailyFilePath!=null && !("".equals(xmlCMSDailyFilePath.trim()))){
					if (!(new File(xmlCMSDailyFilePath).exists())) {
						new File(xmlCMSDailyFilePath).mkdirs();
					}
				}else{
					throw new GIException("Error in cmsDailyEnrollmentReport Service :: No Output Path defined in the configuration file");
				}
				issuer=enrollmentList.get(0).getIssuer();
				enrollments=setEnrollmentsFor834Daily(enrollmentList, startDate, endDate, null);
				enrollments.setHiosIssuerId(issuer.getHiosIssuerId());
				enrollments.setSourceExchgId(DynamicPropertiesUtil.getPropertyValue(EnrollmentPrivateConfiguration.EnrollmentConfigurationEnum.SOURCE_EXCHANGE_ID));
				
				outputPath = xmlCMSDailyFilePath + File.separator+ dateFormat.format(new TSDate()) + ".xml";
				generateEnrollmentsXMLReport(enrollments, outputPath,"EnrollmentXMLTrans.xsl");
			}
			
		} catch (Exception e) {
			LOGGER.error("Exception in EnrollmentServiceImpl.enrollmentMarshalling() "+ e.getMessage());
			throw new GIException("Error in cmsDailyEnrollmentReport() ::"+e.getMessage(),e);
		}
		
		LOGGER.info(" cmsDailyEnrollmentReport Service END ");
	}*/
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to get the end date of the job, than ran successfully
	 * @param jobName
	 * @return
	 * @throws GIException
	 */
	@Override
	public Date getJOBLastRunDate(String jobName) throws GIException{
		Date lastRunDate=null;
		try{
			if(jobName!=null){
				lastRunDate = batchJobExecutionService.getLastJobRunDate(jobName);
			}

		}catch(Exception e){
			throw new GIException("Error in getJOBLastRunDate() ::"+e.getMessage(),e);
		}
		return lastRunDate;
	}
	
	
	/**
	 * This Method is used to serve the IndividualEnrollmentXML Job and
	 * ShopEnrollmentXML Job
	 */

	/*@Override
	public void serveEnrollmentRecordJob(String enrollmentType,String startDateString, String endDateString) throws GIException {
		LOGGER.info("serveEnrollmentRecordJob SERVICE : START ");
		List<Integer> issuerList = new ArrayList<Integer>();
		Date lastRunDate = null;
		Date startDate=null;
		Date endDate=null;
		LOGGER.info("serveEnrollmentRecordJob SERVICE : enrollmentType = "+enrollmentType);
		String jobName=null;
		if (enrollmentType.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
			jobName=EnrollmentPrivateConstants.INDIVIDUAL_ENROLLMENT_BATCH_JOB;
		} else if (enrollmentType.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)) {
			jobName=EnrollmentPrivateConstants.SHOP_ENROLLMENT_BATCH_JOB;
		}
		lastRunDate=getJOBLastRunDate(jobName);
		LOGGER.info("serveEnrollmentRecordJob SERVICE : lastRunDate = "+lastRunDate);
		if(startDateString==null && endDateString==null){
			if(lastRunDate!=null){
				startDate=lastRunDate;
			}else{
				startDate= DateUtil.StringToDate(EnrollmentPrivateConstants.DEFAULT_BATCH_START_DATE, EnrollmentPrivateConstants.BATCH_DATE_FORMAT);
			}
			endDate=new TSDate();
		}else if(startDateString!=null && endDateString!=null){
			startDate= DateUtil.StringToDate(startDateString, EnrollmentPrivateConstants.BATCH_DATE_FORMAT);
			endDate=DateUtil.StringToDate(endDateString, EnrollmentPrivateConstants.BATCH_DATE_FORMAT);
		}
		
		if(startDate!=null && endDate!=null ){
			issuerList = enrollmentRepository.findIssuerByupdateDate(startDate, endDate);
		}
		if (issuerList!=null && !issuerList.isEmpty()) {
			LOGGER.info("serveEnrollmentRecordJob SERVICE : issuerList Size = "+issuerList.size());
			 Map<Integer, String> issuerXmlFileMap = new HashMap<Integer, String>();
			for (Integer issuerID : issuerList) {
				try {
					String hiosIssuerId = null;
					Issuer issuer=null;
					String outputPath  = "";
					StringBuilder outputPathBuilder = new StringBuilder();
				    String outboundXmlExtractPath = "";
					List<Enrollment> enrollmentList = findEnrollmentsForEDI834(issuerID, startDate,endDate, enrollmentType);
					
					if(enrollmentList!=null && !enrollmentList.isEmpty()){
						
						issuer= enrollmentList.get(0).getIssuer();
						
						Enrollments enrollments=setEnrollmentsFor834Daily(enrollmentList,startDate, endDate, enrollmentType);
						
						enrollments.setHiosIssuerId(issuer.getHiosIssuerId());
						
						// setting the source exchange id 
								
						enrollments.setSourceExchgId(DynamicPropertiesUtil.getPropertyValue(EnrollmentPrivateConfiguration.EnrollmentConfigurationEnum.SOURCE_EXCHANGE_ID));
						
						if(isNotNullAndEmpty(issuer.getHiosIssuerId())){	
				
						LOGGER.info("calling FindExchgPartnerByHiosIssuerID to create ISA section in XML");
						LOGGER.info("HIOS_ISSUER_ID =" + issuer.getHiosIssuerId());
						LOGGER.info("GhixConstants.ISA06 =" + GhixConstants.ISA06);
						LOGGER.info("GhixConstants.ISA05 =" + GhixConstants.ISA05);
						LOGGER.info("GhixConstants.GS08 =" + GhixConstants.GS08);
						ExchgPartnerLookup exchgPartnerLookup = null;
						if(jobName.equals(EnrollmentPrivateConstants.SHOP_ENROLLMENT_BATCH_JOB)){
						 exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(issuer.getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_SHOP,GhixConstants.OUTBOUND);
						}
						if(jobName.equals(EnrollmentPrivateConstants.INDIVIDUAL_ENROLLMENT_BATCH_JOB)){
							 exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(issuer.getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL,GhixConstants.OUTBOUND);
						}
							
						if(exchgPartnerLookup != null){
								enrollments.setISA05(exchgPartnerLookup.getIsa05());
								enrollments.setISA06(exchgPartnerLookup.getIsa06());
								enrollments.setISA07(exchgPartnerLookup.getIsa07());
								enrollments.setISA08(exchgPartnerLookup.getIsa08());
								enrollments.setISA15(exchgPartnerLookup.getIsa15());
								enrollments.setGS02(exchgPartnerLookup.getGs02());
								enrollments.setGS03(exchgPartnerLookup.getGs03());
								if(exchgPartnerLookup.getSourceDir() != null){
									  outboundXmlExtractPath = exchgPartnerLookup.getSourceDir();
								}
							}
						}
						
						hiosIssuerId = issuer.getHiosIssuerId() != null ? issuer.getHiosIssuerId().toString() : "hiosIssuerId";
						if(outboundXmlExtractPath==null || "".equals(outboundXmlExtractPath.trim())){
							throw new GIException("serveEnrollmentRecordJob SERVICE : No Source Directory defined in ExchangePartner Lookup Table");
						}
						outputPathBuilder.append(outboundXmlExtractPath);
						outputPathBuilder.append(File.separator);
						outputPathBuilder.append("to_");
						outputPathBuilder.append(hiosIssuerId);
						outputPathBuilder.append("_");
//						outputPath = outboundXmlExtractPath;
						
						if (!(new File(outputPath).exists())) {
							
							throw new GIException("serveEnrollmentRecordJob SERVICE :Source Directory defined in ExchangePartner Lookup Table doesn't Exists.");
						}
					
						if (enrollmentType.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
							if(EnrollmentPrivateConfiguration.isCaCall()){
								outputPathBuilder.append(GhixConstants.CA_834_SHOP);
//								outputPath = outputPath + File.separator +"to_" + hiosIssuerId + "_"+ GhixConstants.CA_834_SHOP + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
							else if(EnrollmentPrivateConfiguration.isNmCall()){
								outputPathBuilder.append(GhixConstants.NM_834_SHOP);
//								outputPath = outputPath + File.separator +"to_" + hiosIssuerId + "_"+ GhixConstants.NM_834_SHOP + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
						}
						if (enrollmentType.equals(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
							if(EnrollmentPrivateConfiguration.isCaCall()){
								outputPathBuilder.append(GhixConstants.CA_834_INDV);
//								outputPath = outputPath + File.separator +"to_" + hiosIssuerId + "_"+ GhixConstants.CA_834_INDV + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
							else if(EnrollmentPrivateConfiguration.isNmCall()){	
								outputPathBuilder.append(GhixConstants.NM_834_INDV);
//								outputPath = outputPath + File.separator +"to_" + hiosIssuerId + "_"+ GhixConstants.NM_834_INDV + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
						}
						outputPathBuilder.append("_");
						outputPathBuilder.append(dateFormat.format(new TSDate()));
						outputPathBuilder.append(".xml");
						outputPath = outputPathBuilder.toString();
						LOGGER.info("serveEnrollmentRecordJob SERVICE : outputPath = "+outputPath);
						generateEnrollmentsXMLReport(enrollments, outputPath,"EnrollmentXMLTrans.xsl");
						issuerXmlFileMap.put(issuerID, outputPath);
					}
					
				}catch (Exception exception) {
					for (Map.Entry<Integer, String> entry : issuerXmlFileMap.entrySet()){
						File tempFile = new File(entry.getValue());
						tempFile.delete();
					}
					LOGGER.error("Exception in EnrollmentServiceImpl.serveEnrollmentRecordJob while processing Issuer Id :"+ issuerID+ "Exception Message:"+ exception.getMessage());
					throw new GIException("Error in serveEnrollmentRecordJob()::"+exception.getMessage(),exception);
				}
			}
			
			for (Map.Entry<Integer, String> entry : issuerXmlFileMap.entrySet())
			{
				File tempFile = new File(entry.getValue());
				if(tempFile != null){
					// Rename back from .tmp to .xml So that files get picked up for EDI.
					String newXmlFileName = tempFile.getAbsolutePath().replaceAll(".tmp", ".xml");
					File newXmlFile = new File(newXmlFileName);
					tempFile.renameTo(newXmlFile);
				}
			}
		}
	LOGGER.info("serveEnrollmentRecordJob SERVICE : END ");
	}*/
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to  fetch the enrollments based upon their enrollment type for a specific issuer
	 * @param issuerID
	 * @param lastRunDate
	 * @param enrollmentType
	 * @return
	 * @throws GIException
	 */
	
	/*private List<Enrollment> findEnrollmentsForEDI834(Integer issuerID, Date startDate, Date enddate, String enrollmentType) throws GIException{
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		List<Enrollment> enrollmentOldList = null;
		try{
			int enrollmentTypeLookupId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE,enrollmentType);
			
			if(enrollmentType.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
				
				enrollmentOldList = enrollmentRepository.findEnrollmentByIssuerIDAndEnrollmentType(issuerID,startDate, enddate, enrollmentTypeLookupId);
				
			}else if(enrollmentType.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
				
				enrollmentOldList = enrollmentRepository.findEnrollmentByIssuerIDAndEnrollmentType(issuerID,startDate, enddate, enrollmentTypeLookupId, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING);
				
			}
			for(Enrollment enrObj :enrollmentOldList){
				// skip enrollments with aborted status. (Currently Enrollments are aborted in case of IND-20 Failure)
				if(!enrObj.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_ABORTED)){
					// Fetch enrollments not updated by carrier.
					if(enrObj.getUpdatedBy()==null || enrObj.getUpdatedBy().getUserName()==null || !(enrObj.getUpdatedBy().getUserName().equalsIgnoreCase(GhixConstants.USER_NAME_CARRIER))){
						enrollmentList.add(enrObj);
					}
				}
			}
		}catch(Exception e){
			
			throw new GIException("Error in findEnrollmentsForEDI834()::"+e.getMessage(),e);
		}
		return enrollmentList;
	}
	*/
	/**
	 * 
	 * @param allEnrollees
	 * @param lastRunDate
	 * @param enrollmentType
	 * @return
	 * @throws GIException
	 */
	/*private List<Enrollee> filterEnrolleesForEDIDaily(List<Enrollee> allEnrollees, Date startDate,Date endDate,  String enrollmentType) throws GIException{
		List<Enrollee> updatedEnrolleeList = null;
		try{
			if(allEnrollees!=null ){
				AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
				updatedEnrolleeList = new ArrayList<Enrollee>();
				for(Enrollee enrollee:allEnrollees){
					// Filter Enrollees which are cancelled from pending state. We don't need them to be sent in extracts in case of Shop.
					if(isNotNullAndEmpty(enrollmentType) && (
						enrollmentType.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP) && 
						enrollee.getEnrolleeLkpValue() != null && 
						enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL))){
						List<EnrolleeAud> enrolleeAudList = enrolleeAudRepository.getEnrolleeByIdRevAndNoPendingStatus(enrollee.getId());
						if(enrolleeAudList == null || enrolleeAudList.isEmpty()){
							continue;
						}
					}
					
					Date enrolleeUpdateDate=enrollee.getUpdatedOn();
					if(enrollee.getPersonTypeLkp()!=null && (!(enrolleeUpdateDate.before(startDate))&&!(enrolleeUpdateDate.after(endDate)) )&&(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER) )
						&& (enrollmentType==null ||enrollee.getUpdatedBy()==null || !(enrollee.getUpdatedBy().getUserName().equalsIgnoreCase(GhixConstants.USER_NAME_CARRIER)))){
							List<EnrollmentEvent> allEvents= enrollee.getEnrollmentEvents();
							List<EnrollmentEvent> createdEvents= new ArrayList<EnrollmentEvent>();
							Date eventCreationDate=null;
							for(EnrollmentEvent event:allEvents){
								eventCreationDate=event.getCreatedOn();
								if((!(eventCreationDate.before(startDate)) && !(eventCreationDate.after(endDate))) &&
									 (event.getCreatedBy() == null || ((isNotNullAndEmpty(user) && isNotNullAndEmpty(event.getCreatedBy())) && (event.getCreatedBy().getId()!=user.getId())))){
										createdEvents.add(event);
									}
							}
							enrollee.setEnrollmentEvents(createdEvents);
							updatedEnrolleeList.add(enrollee);
						}
				}
			}
		}catch(Exception e){
			LOGGER.error("Exception in filterEnrolleesForEDIDaily()"+e.getMessage());
			throw new GIException("Exception in filterEnrolleesForEDIDaily()"+e.getMessage(),e);
		}
		return updatedEnrolleeList;
	}*/
	/*private Enrollee setReferenceFieldsFromAudit(Enrollee eventEnrollee, EnrolleeAud enrolleeAud) throws GIException{
		try{
			if(eventEnrollee!=null && enrolleeAud!=null){
				eventEnrollee.setEnrolleeLkpValue(enrolleeAud.getEnrolleeLkpValue());
				eventEnrollee.setLanguageLkp(enrolleeAud.getLanguageLkp());
				eventEnrollee.setLanguageWrittenLkp(enrolleeAud.getLanguageWrittenLkp());
				eventEnrollee.setHomeAddressid(enrolleeAud.getHomeAddressid());
				eventEnrollee.setGenderLkp(enrolleeAud.getGenderLkp());
				eventEnrollee.setMaritalStatusLkp(enrolleeAud.getMaritalStatusLkp());
				eventEnrollee.setCitizenshipStatusLkp(enrolleeAud.getCitizenshipStatusLkp());
				eventEnrollee.setTobaccoUsageLkp(enrolleeAud.getTobaccoUsageLkp());
				eventEnrollee.setMailingAddressId(enrolleeAud.getMailingAddressId());
				eventEnrollee.setPersonTypeLkp(enrolleeAud.getPersonTypeLkp());
			}
		}catch(Exception e){
			//e.printStackTrace();
			LOGGER.error("Exception in setReferenceFieldsFromAudit()"+e.getMessage(),e);
			throw new GIException("Exception in setReferenceFieldsFromAudit()"+e.getMessage(),e);
		}
		return eventEnrollee;
	}*/
	
	/*private Enrollee setNormalFieldsFromAudit(Enrollee localEnrollee, Enrollee eventEnrollee) throws GIException{
		try{
			if(localEnrollee!=null && eventEnrollee!=null){
				localEnrollee.setId(eventEnrollee.getId());
				localEnrollee.setTaxIdNumber(eventEnrollee.getTaxIdNumber());
				localEnrollee.setExchgIndivIdentifier(eventEnrollee.getExchgIndivIdentifier());
				localEnrollee.setIssuerIndivIdentifier(eventEnrollee.getIssuerIndivIdentifier());
				localEnrollee.setHealthCoveragePolicyNo(eventEnrollee.getHealthCoveragePolicyNo());
				localEnrollee.setEffectiveStartDate(eventEnrollee.getEffectiveStartDate());
				localEnrollee.setEffectiveEndDate(eventEnrollee.getEffectiveEndDate());
				localEnrollee.setLastName(eventEnrollee.getLastName());
				localEnrollee.setFirstName(eventEnrollee.getFirstName());
				localEnrollee.setMiddleName(eventEnrollee.getMiddleName());
				localEnrollee.setSuffix(eventEnrollee.getSuffix());
				localEnrollee.setNationalIndividualIdentifier(eventEnrollee.getNationalIndividualIdentifier());
				localEnrollee.setPrimaryPhoneNo(eventEnrollee.getPrimaryPhoneNo());
				localEnrollee.setSecondaryPhoneNo(eventEnrollee.getSecondaryPhoneNo());
				localEnrollee.setPreferredSMS(eventEnrollee.getPreferredSMS());
				localEnrollee.setPreferredEmail(eventEnrollee.getPreferredEmail());
				localEnrollee.setBirthDate(eventEnrollee.getBirthDate());
				localEnrollee.setTotalIndvResponsibilityAmt(eventEnrollee.getTotalIndvResponsibilityAmt());
				localEnrollee.setMaintenanceEffectiveDate(eventEnrollee.getMaintenanceEffectiveDate());
				localEnrollee.setRatingArea(eventEnrollee.getRatingArea());
				localEnrollee.setLastPremiumPaidDate(eventEnrollee.getLastPremiumPaidDate());
				localEnrollee.setTotEmpResponsibilityAmt(eventEnrollee.getTotEmpResponsibilityAmt());
				localEnrollee.setRespPersonIdCode(eventEnrollee.getRespPersonIdCode());
				localEnrollee.setCreatedOn(eventEnrollee.getCreatedOn());
				localEnrollee.setUpdatedOn(eventEnrollee.getUpdatedOn());
				localEnrollee.setEmployerGroupNo(eventEnrollee.getEmployerGroupNo());
				localEnrollee.setIssuerSubscriberIdentifier(eventEnrollee.getIssuerSubscriberIdentifier());
				localEnrollee.setId(eventEnrollee.getId());
				//localEnrollee.setHomeAddressid(eventEnrollee.getHomeAddressid());
				//localEnrollee.setMailingAddressId(eventEnrollee.getMailingAddressId());	
			}
			
		}catch(Exception e){
			//e.printStackTrace();
			LOGGER.error("Exception in setNormalFieldsFromAudit()"+e.getMessage(),e);
			throw new GIException("Exception in setNormalFieldsFromAudit()"+e.getMessage(),e);
		}
		return localEnrollee;
		
	}*/
	
	/*private Location setLocationFieldsFromAudit(Location localLocation, LocationAud revLocation) throws GIException {
		if(localLocation != null && revLocation!= null){
			try{
				localLocation.setId(revLocation.getId());
				localLocation.setAddOnZip(revLocation.getAddOnZip());
				localLocation.setAddress1(revLocation.getAddress1());
				localLocation.setAddress2(revLocation.getAddress2());
				localLocation.setCity(revLocation.getCity());
				localLocation.setCounty(revLocation.getCounty());
				localLocation.setCountycode(revLocation.getCountycode());
				localLocation.setLat(revLocation.getLat());
				localLocation.setLon(revLocation.getLon());
				localLocation.setRdi(revLocation.getRdi());
				localLocation.setState(revLocation.getState());
				localLocation.setZip(revLocation.getZip());
			}
			catch (Exception e){
				LOGGER.error("Exception in setNormalFieldsFromAudit()"+e.getMessage(),e);
				throw new GIException("Exception in setLocationFieldsFromAudit() :: " ,e);
			}
		}
		return localLocation;
	}*/
	
	protected boolean isSubscriberCancelled(Enrollee subscriber){
		boolean isSubscriberCancelled = false;
		if(subscriber != null &&
			subscriber.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)){
				isSubscriberCancelled = true;
		}
		return isSubscriberCancelled;
	}
	
	/*private List<Enrollee> setPerEventEnrolleeFromEnrollee(List<Enrollee> enrolleeList, Enrollee subscriber, Integer subscriberId) throws GIException{
		List<Enrollee> perEventEnrollee = null;
		List<Enrollee> perEventCancelEnrollee = null;
		try{
			if(enrolleeList!=null && !enrolleeList.isEmpty()){
				perEventEnrollee = new ArrayList<Enrollee>();
				perEventCancelEnrollee = new ArrayList<Enrollee>();
				for(Enrollee enrollee:enrolleeList){
					List<EnrollmentEvent> eventList= enrollee.getEnrollmentEvents();
					//					if(eventList != null && eventList.size()<1){
					//						//Write the code to find relation to subscriber here itself 
					//						if(enrollee.getPersonTypeLkp()!=null &&  !(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) ) && subscriber!=null){
					//							for(EnrolleeRelationship er: enrollee.getEnrolleeRelationship()){
					//								if(er.getTargetEnrollee()!=null && (er.getTargetEnrollee().getId()== subscriber.getId())){
					//									enrollee.setRelationshipToSubscriberLkp(er.getRelationshipLkp());
					//									break;
					//								}
					//							}
					//						}
					//						setDemographicChanges(enrollee);
					//						perEventEnrollee.add(enrollee);
					//					}
					//					else{
					Collections.sort(eventList);
					for(EnrollmentEvent event: eventList){
						//Code to fetch the Enrollee from Audit per event Id
						Revision<Integer, Enrollee> myEnrollee=null;
						Enrollee eventEnrollee=null;
						EnrolleeAud enrolleeAud=null;
						Enrollee enrolleeLocal= new Enrollee();
						myEnrollee = enrolleeRepository.findRevisionByEvent(enrollee.getId(), event.getId(), "lastEventId");
						Integer enrolleeRevisionNumber=null;
						if(myEnrollee!= null){
							enrolleeRevisionNumber=myEnrollee.getRevisionNumber();
							eventEnrollee = myEnrollee.getEntity();
							enrolleeAud  = enrolleeAudRepository.getEnrolleeAudByIdAndRev(eventEnrollee.getId(),enrolleeRevisionNumber );

							// Check from Audit if person is Enrollee
							// If Enrollee filter out cancel events form enrollee. As those are not needed in Extracts
							// Also only filter out cancel events from enrollee only if subscriber is canclled, else these are needed in extracts
							if(isSubscriberCancelled(subscriber) && event.getEventTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.EVENT_TYPE_CANCELLATION)
								 && enrolleeAud != null && enrolleeAud.getPersonTypeLkp() != null && enrolleeAud.getPersonTypeLkp().getLookupValueCode() != null
									&& enrolleeAud.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE)){
										continue;
							}

							enrolleeLocal= setNormalFieldsFromAudit(enrolleeLocal,eventEnrollee);
							enrolleeLocal= setReferenceFieldsFromAudit(enrolleeLocal,enrolleeAud );
							//Code to Set Relationship
							Integer relRevisionNumber= enrolleeRelationshipRepository.findLastRelationRevision(enrollee.getId(), subscriberId, enrolleeRevisionNumber);
							if(relRevisionNumber!=null){
								EnrolleeRelationshipAud enrolleeRelationAud= enrolleeRelationshipAudRepository.findrelationShipBySourceTargetRev(enrollee.getId(), subscriberId, relRevisionNumber);
								if(enrolleeRelationAud!=null){
									//eventEnrollee.setRelationshipToSubscriberLkp(enrolleeRelationAud.getRelationshipLkp());
									enrolleeLocal.setRelationshipToSubscriberLkp(enrolleeRelationAud.getRelationshipLkp());
								}
							}

							//write code to set EnrolleeRace list From history
							Integer erRevisionNumber=enrolleeRaceRepository.findLastRaceRevision(enrollee.getId(), enrolleeRevisionNumber);
							List<EnrolleeRace> raceList= null;
							if(erRevisionNumber!=null){
								raceList=new ArrayList<EnrolleeRace>();
								List<Object> objList = enrolleeRaceRepository.findRacesCreatedForEnrollee(enrollee.getId(), enrolleeRevisionNumber);
								int size=objList.size();
								EnrolleeRace raceEntity=null;

								for(int i=0;i<size;i++){
									Object[] objArray = (Object[])objList.get(i);
									if(objArray!=null){
										raceEntity = (EnrolleeRace)objArray[0];
									}
									if(raceEntity!=null){
										EnrolleeRaceAud enrolleeRaceAud=enrolleeRaceAudRepository.getEnrolleeRaceAudByIdAndRev(raceEntity.getId(),erRevisionNumber);
										if(enrolleeRaceAud!=null){
											raceEntity.setRaceEthnicityLkp(enrolleeRaceAud.getRaceEthnicityLkp());
										}
										raceList.add(raceEntity);
									}

								}
							}
							enrolleeLocal.setEnrolleeRace(raceList);
							//Code to get Address from History
							if(enrollee.getHomeAddressid()!=null){
								LocationAud revLocationAud = locationRepository.getLocationAudByIdAndRev(enrollee.getHomeAddressid().getId(), enrolleeRevisionNumber);
								if(revLocationAud != null){
									Location localLocation = new Location();
									localLocation = setLocationFieldsFromAudit(localLocation, revLocationAud);
									enrolleeLocal.setHomeAddressid(localLocation);
								}
							}

							//Mailing Address
							if(enrollee.getMailingAddressId()!=null){
								LocationAud revLocationAud = locationRepository.getLocationAudByIdAndRev(enrollee.getMailingAddressId().getId(), enrolleeRevisionNumber);
								if(revLocationAud != null){
									Location localLocation = new Location();
									localLocation = setLocationFieldsFromAudit(localLocation, revLocationAud);
									enrolleeLocal.setMailingAddressId(localLocation);
								}
							}

							//Code to get and set Custodial parent from revision
							if(enrollee.getCustodialParent()!=null){
								Revision<Integer,Enrollee> lastSubscriberRevision = enrolleeRepository.findLastSubscriberChangeRevision(enrollee.getCustodialParent().getId(), enrolleeRevisionNumber);
								if(lastSubscriberRevision!=null){
									Enrollee custodialParent=lastSubscriberRevision.getEntity();
									if(custodialParent!=null){
										Enrollee localCustodial= new Enrollee();
										localCustodial=setNormalFieldsFromAudit(localCustodial, custodialParent);
										//eventEnrollee.setCustodialParent(custodialParent);
										//Code to get and set Custodial parent Address 
										if(enrollee.getCustodialParent().getHomeAddressid()!=null){
											LocationAud revLocationAud = locationRepository.getLocationAudByIdAndRev(enrollee.getCustodialParent().getHomeAddressid().getId(), enrolleeRevisionNumber);
											if(revLocationAud != null){
												Location localLocation = new Location();
												localLocation = setLocationFieldsFromAudit(localLocation, revLocationAud);
												localCustodial.setHomeAddressid(localLocation);
											}
										}

										if(enrollee.getCustodialParent().getMailingAddressId()!=null){
											LocationAud revLocationAud = locationRepository.getLocationAudByIdAndRev(enrollee.getCustodialParent().getHomeAddressid().getId(), enrolleeRevisionNumber);
											if(revLocationAud != null){
												Location localLocation = new Location();
												localLocation = setLocationFieldsFromAudit(localLocation, revLocationAud);
												localCustodial.setMailingAddressId(localLocation);
											}
										}
										enrolleeLocal.setCustodialParent(localCustodial);
									}
								}
							}
						}

						List<EnrollmentEvent> perEnrolleeEventList= new ArrayList<EnrollmentEvent>();
						perEnrolleeEventList.add(event);
						//eventEnrollee.setEnrollmentEvents(perEnrolleeEventList);
						enrolleeLocal.setEnrollmentEvents(perEnrolleeEventList);
						setDemographicChanges(enrolleeLocal);

						// Implemented to have all Adding
						if(event.getEventTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.EVENT_TYPE_CANCELLATION)){
							perEventCancelEnrollee.add(enrolleeLocal);
						}
						else{
							perEventEnrollee.add(enrolleeLocal);
						}
					}

				}
				//					}
			}
		}catch(Exception e){
			
			LOGGER.error("Exception in setPerEventEnrolleeFromEnrollee"+e.getMessage(),e);
			throw new GIException("Exception in setPerEventEnrolleeFromEnrollee"+e.getMessage(),e);
		}
		if((perEventCancelEnrollee != null && !perEventCancelEnrollee.isEmpty()) && perEventEnrollee != null){
			perEventEnrollee.addAll(perEventCancelEnrollee);
		}

		return perEventEnrollee;
	}*/
	
	/**
	 * @since
	 * @author parhi_s
	 * @param issuer
	 * @param lastRunDate
	 * @param enrollmentType
	 * @throws Exception
	 * This method is used to create Enrollments object for the XML
	 * generation purpose
	 */
	/*private Enrollments setEnrollmentsFor834Daily(List<Enrollment> enrollmentList, Date startDate, Date endDate,String enrollmentType) throws GIException {

		Enrollments enrollments = new Enrollments();

		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			List<Enrollment> updatedEnrollmentList = new ArrayList<Enrollment>();
			//			enrollments.setEnrollmentList(enrollmentList);

			for (Enrollment enrollment : enrollmentList) {

				try {
					long activeEnrolleeCount=0;
					List<Enrollee> enrolleesList=null;

					List<Enrollee> allEnrolleeList = enrollment.getEnrollees();

					activeEnrolleeCount=enrolleeRepository.totalActiveMembersByEnrollmentID(enrollment.getId());

					enrolleesList=filterEnrolleesForEDIDaily(allEnrolleeList, startDate,endDate, enrollmentType);
					enrollment.setEnrollees(enrolleesList);

					if(enrollment.getInsurerTaxIdNumber()!=null){
						enrollment.setInsurerTaxIdNumber(enrollment.getInsurerTaxIdNumber().replaceAll("[-]", ""));
					}

					if(enrolleesList!=null && !enrolleesList.isEmpty()){
						//Find subscriber
						Enrollee subscriber = checkForSubscriber(enrolleesList);


						// Subscriber here could be null, as subscriber may not be needed for extracts, in that case pass
						// subscriber Id from ellEnrollees list. Subscriber Id is needed to query EnrolleeAud repository for relationship and other stuff.
						if(subscriber == null){
							Enrollee noExtractSubscriber = checkForSubscriber(allEnrolleeList);
							enrolleesList=setPerEventEnrolleeFromEnrollee(enrolleesList,subscriber,noExtractSubscriber.getId());
						}
						else{
							enrolleesList=setPerEventEnrolleeFromEnrollee(enrolleesList,subscriber,subscriber.getId());
						}

						// Revisit the code to set QTY values
						// Code to set QTY values

						// calculate subscriber event count before resetting subscriber from all enrollee list of enrollment.
						int subscriberEvent = 0;
						if(subscriber!=null){
							// Sort the list, so that subscriber is first member. Order of other members don't matter.
							enrolleesList = sortEnrolleeListBySubscriberFirst(enrolleesList);
							subscriberEvent = subscriber.getEnrollmentEvents().size();
						}

						// If subscriber is null, fetch form all enrollees list, after consedering all events anc count required for extracts.
						if(subscriber==null){
							subscriber=checkForSubscriber(allEnrolleeList);
						}

						int totalEnrolleeEvent = getEnrollmentEvent(enrollment.getEnrollees(), subscriber);
						enrollment.setQTYy(subscriberEvent);
						enrollment.setQTYn(totalEnrolleeEvent-subscriberEvent);
						enrollment.setQTYt(totalEnrolleeEvent);

						EnrollmentEsignature enrollesig = enrollmentEsignatureRepository.findByEnrollmentId(enrollment.getId());

						for (Enrollee enrollee: enrolleesList){

							enrollee.setShowMailingAddress(EnrollmentPrivateConstants.TRUE);

							if(compareAddress(enrollee.getHomeAddressid(), enrollee.getMailingAddressId())){

								enrollee.setShowMailingAddress(EnrollmentPrivateConstants.FALSE);

							}

							if(subscriber!=null){
									EnrolleeRelationship enroleeRelationship = enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrollee.getId(), subscriber.getId());
									if(enroleeRelationship!=null){
										enrollee.setRelationshipToSubscriberLkp(enroleeRelationship.getRelationshipLkp());
									}
								}

							if(enrollesig != null)
							{
								enrollee.setFormatSignatureDate(enrollesig.getEsignature().getEsignDate());
							}
							//setDemographicChanges(enrollee);
						}
					}
					// As no enrollees, we don't need to consider this in extract.
					// Might occur in case of shop when we filter enrollees cancelled directly from pending state.
					else{
						continue;
					}
					enrollment.setEnrollees(enrolleesList);

					//code to set total CSR amount
					if(enrollment.getCsrAmt()!=null){
						enrollment.setTotalCSRAmt(activeEnrolleeCount*enrollment.getCsrAmt());
					}
					Enrollee responsibleEnrollee = enrolleeService.getResponsiblePersonByEnrollmentID(enrollment.getId());

					if(responsibleEnrollee != null){
						enrollment.setResponsiblePerson(responsibleEnrollee);
					}

				} catch (Exception e) {
					LOGGER.error("Error occured in setEnrollmentsFor834Daily. ", e);
					throw new GIException("Error occured in setEnrollmentsFor834Daily()::"+e.getMessage(),e);
				}
				updatedEnrollmentList.add(enrollment);
			}
			enrollments.setEnrollmentList(updatedEnrollmentList);
		}
		return enrollments;
	}*/
	
	protected List<Enrollee> sortEnrolleeListBySubscriberFirst(List<Enrollee> enrolleeList){
		List<Enrollee> sortedEnrolleeList = new ArrayList<Enrollee>();
		List<Enrollee> nonSubscriberEnrolleeList = new ArrayList<Enrollee>();
		for(Enrollee enrollee : enrolleeList){
			if(enrollee != null && enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode() != null){
				if(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER)){
					// Sort to add enrollees to top of extracts list.
					if(enrollee.getEnrolleeLkpValue() != null && !enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)){
						sortedEnrolleeList.add(enrollee);
					}
					
					// If enrollee status is cancel it should go at end in extracts.
					else{
						nonSubscriberEnrolleeList.add(enrollee);
					}
				}
				// add other non subscriber enrollees to different list.
				else{
					nonSubscriberEnrolleeList.add(enrollee);
				}
			}
		}
		// add all enrollees, to get subscriber at top, followed by other enrollees.
		sortedEnrolleeList.addAll(nonSubscriberEnrolleeList);
		return sortedEnrolleeList;
	}
	
	/*private int getEnrollmentEvent(List<Enrollee> enrollees, Enrollee subscriber) {
		int totalEvent = 0;
		for (Enrollee enrollee : enrollees) {
			if(enrollee.getEnrollmentEvents() != null){
				for(EnrollmentEvent event : enrollee.getEnrollmentEvents()){
					if(event.getEventTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.EVENT_TYPE_CANCELLATION)){
						if((enrollee.getPersonTypeLkp().getLookupValueCode() != null && 
								enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE)) && 
								isSubscriberCancelled(subscriber)){
							// Skip cancel event for Enrollees if Subscriber is cancelled. We don't need in Extract.
							// In this case we only need subscriber level cancellation in extract
							continue;
						}
							// Consider only if its a subscriber or there is only member/enrollee level disenrollment.
						else{
							totalEvent++;
						}
					}		// If event is not cancelletion consider event for extracts.
					else{
						totalEvent++;
					}
				}
			}
		}
		return totalEvent;
	}*/
			


	/**
	 * @author parhi_s
	 * @since
	 * Method used by batch services to check if any enrollee has undergone demographic changes and if so then set the incorrect details
	 * @param enrollee
	 * @param lastRunDate
	 * @throws GIException
	 */
	/*private void setDemographicChanges(Enrollee enrollee) throws GIException{
		try{
			if(enrollee != null && enrollee.getEnrollmentEvents() !=null){
				List<EnrollmentEvent> enrollmentEventList = enrollee.getEnrollmentEvents();
				boolean isEnrolleeRaceChanged = false; 
				for(EnrollmentEvent event : enrollmentEventList){
					if(event.getEventTypeLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.EVENT_TYPE_CHANGE)){
						Revision<Integer, Enrollee> myEnrollee=null;
						Enrollee oldEnrollee=null;
						EnrolleeAud enrolleeAud=null;
						myEnrollee = enrolleeAuditService.findRevisionByDate(enrollee.getId(),event.getCreatedOn());
						if(myEnrollee!= null){
							oldEnrollee = myEnrollee.getEntity();
							enrolleeAud  = enrolleeAudRepository.getEnrolleeAudByIdAndRev(oldEnrollee.getId(), myEnrollee.getRevisionNumber());
						}
						// use enrolleeAud for lookup values
						if(oldEnrollee != null)	{
							if(!compareString(enrollee.getFirstName() , oldEnrollee.getFirstName())){
								enrollee.setIncorrectMemberFirstName(oldEnrollee.getFirstName());
							}
							if(!compareString(enrollee.getLastName() , oldEnrollee.getLastName())){
								enrollee.setIncorrectMemberLastName(oldEnrollee.getLastName());
							}
							if(!compareString(enrollee.getMiddleName(), oldEnrollee.getMiddleName())){
								enrollee.setIncorrectMemberMiddleName(oldEnrollee.getMiddleName());
							}
							if(!compareString(enrollee.getSuffix(), oldEnrollee.getSuffix())){
								enrollee.setIncorrectMemberNameSuffix(oldEnrollee.getSuffix());
							}
							if(!compareString(enrollee.getNationalIndividualIdentifier(), oldEnrollee.getNationalIndividualIdentifier())){
								enrollee.setIncorrectMemberNationalIndivID(oldEnrollee.getNationalIndividualIdentifier());
							}
							if(!(DateUtil.isEqual(enrollee.getBirthDate(), oldEnrollee.getBirthDate()))){
								enrollee.setIncorrectMemberBirthDate(oldEnrollee.getBirthDate());
							}
							if(enrolleeAud!=null){
								if(!compareLookup(enrollee.getGenderLkp(), enrolleeAud.getGenderLkp())){
									enrollee.setIncorrectMemberGenderLkp(enrolleeAud.getGenderLkp());
								}
								if(!compareLookup(enrollee.getMaritalStatusLkp(), enrolleeAud.getMaritalStatusLkp())){
									enrollee.setIncorrectMemberMaritalStatusLkp(enrolleeAud.getMaritalStatusLkp());
								}
								if(!compareLookup(enrollee.getCitizenshipStatusLkp(), enrolleeAud.getCitizenshipStatusLkp())){
									enrollee.setIncorrectMemberCitizenshipStatusLkp(enrolleeAud.getCitizenshipStatusLkp());
								}
							}
							if(enrollee.getEnrolleeRace() != null)
							{
								for(EnrolleeRace newEnrolleeRace : enrollee.getEnrolleeRace()){
									Revision<Integer, EnrolleeRace> myER = enrolleeRaceAuditService.findRaceRevisionByDate(enrollee.getId(),event.getCreatedOn());
									EnrolleeRaceAud enrolleeRaceAud=null;
									if(myER!=null){
										EnrolleeRace latestEnrolleeRace = myER.getEntity();
										enrolleeRaceAud = enrolleeRaceAudRepository.getEnrolleeRaceAudByIdAndRev(latestEnrolleeRace.getId(), myER.getRevisionNumber());
									}
									if(enrolleeRaceAud != null && (enrolleeRaceAud.getRaceEthnicityLkp().getLookupValueCode() != newEnrolleeRace.getRaceEthnicityLkp().getLookupValueCode())){ 
									  if(enrolleeRaceAud != null && (!enrolleeRaceAud.getRaceEthnicityLkp().getLookupValueCode().equals(newEnrolleeRace.getRaceEthnicityLkp().getLookupValueCode()))) {
										isEnrolleeRaceChanged  = true;

									}
								}
							}
							if(isEnrolleeRaceChanged){
								List<EnrolleeRace> oldEnrolleeRaceList = new ArrayList<EnrolleeRace>();
								for(EnrolleeRace enrolleeRace : enrollee.getEnrolleeRace()){
									EnrolleeRace oldEnrolleeRace = new EnrolleeRace();
									oldEnrolleeRace = enrolleeRaceAuditService.findRaceRevisionByDate(enrolleeRace.getId(),event.getCreatedOn()).getEntity();
									oldEnrolleeRaceList.add(oldEnrolleeRace);
								}
								enrollee.setIncorrectMemberRaceEthnicityLkp(oldEnrolleeRaceList);
							}
						}
					}
				}
			}
		}catch(Exception e){
			throw new GIException("Error in setDemographicChanges()::"+e.getMessage(),e);
		}
	}
	*/
	//Method to compare lookup by lookup value
  /*  private boolean compareLookup(LookupValue firstLKP, LookupValue secondLKP){
    	boolean isEqual = false;
    	if(firstLKP==null && secondLKP==null){
    		isEqual=true;
    	}else if(firstLKP !=null && secondLKP!=null ){
    		isEqual=compareString(firstLKP.getLookupValueCode(), secondLKP.getLookupValueCode());
    	}
    	return isEqual;
    }	*/
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to compare two strings.
	 * @param firstString
	 * @param secondString
	 * @return
	 */
	
/*	private boolean compareString(String firstString,String secondString) {
		boolean isEqual = false;
		if(firstString ==null &&secondString==null){
			isEqual=true;
		}
		else if(firstString !=null && secondString!=null){
			firstString=firstString.trim();
			secondString=secondString.trim();
			isEqual=firstString.equals(secondString);
		}
		
		return isEqual;
	}
	*/
	/**
	 * @author parhi_s
	 * @since
	 * Method used to compare two address objects for some specific fields
	 * @param firstAddress
	 * @param secondAddress
	 * @return
	 */
	/*private boolean compareAddress(Location firstAddress, Location secondAddress)  {

		boolean isEqual = false;
		if (firstAddress == null && secondAddress == null) {
			isEqual = true;
		} else if (firstAddress != null && secondAddress != null 
				&& compareString(firstAddress.getAddress1(),secondAddress.getAddress1()) 
				&& compareString(firstAddress.getAddress2(), secondAddress.getAddress2())
				&& compareString(firstAddress.getCity(), secondAddress.getCity())
				&& compareString(firstAddress.getState(), secondAddress.getState()) 
				&& compareString(firstAddress.getZip(),secondAddress.getZip())){	
					
					isEqual=true;				
				  }

		return isEqual;
	}*/
	/*@Transactional
	private boolean updateEnrollmentsForXML(Enrollments enrollments, String fileName, String timeStamp) throws GIException{
		boolean containsBadEnrollments = false;
		StringBuilder badEnrollmentDetailsBuffer = new StringBuilder();
		if(enrollments != null){
			List<Enrollment> enrollmentList = enrollments.getEnrollmentList();
			AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
			for (Enrollment enrollment : enrollmentList) {
				try{
					Enrollment enrollmentDb = enrollmentRepository.getEnrollmentByExchgSubscriberIdentifier(enrollment.getExchgSubscriberIdentifier(), enrollment.getCMSPlanID());
					if(enrollmentDb !=null){
						String enrollmentType=null;
						if(enrollmentDb.getEnrollmentTypeLkp()!=null){
							enrollmentType=enrollmentDb.getEnrollmentTypeLkp().getLookupValueCode();
						}
						List<Enrollee> enrolleeList = enrollment.getEnrollees();
						Enrollee  subscriber = checkForSubscriber(enrolleeList);
						String subscriberStatus=null;
						if(subscriber!=null && subscriber.getEnrolleeLkpValue()!=null){
							subscriberStatus = subscriber.getEnrolleeLkpValue().getLookupValueCode();
						}

						if(subscriberStatus!=null &&  (
								subscriberStatus.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM)
								|| subscriberStatus.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL))){
							if(enrollmentType!=null && !enrollmentType.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
								if(subscriberStatus.equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL)){
									enrollmentDb.setIssuerSubscriberIdentifier(enrollment.getIssuerSubscriberIdentifier());
									enrollmentDb.setBenefitEndDate(subscriber.getEffectiveEndDate());
									List<Enrollee> cancelEnrolleeList = enrolleeRepository.getEnrolleeByStatusAndEnrollmentID(enrollmentDb.getId(), EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING);

									for (Enrollee enrolleeDb : cancelEnrolleeList) {
										enrolleeDb.setEffectiveEndDate(subscriber.getEffectiveEndDate());
										enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL));

										if(enrolleeDb.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER) &&subscriber.getIssuerIndivIdentifier()!=null){
											enrolleeDb.setIssuerIndivIdentifier(subscriber.getIssuerIndivIdentifier());
										}
										enrolleeDb.setUpdatedBy(user);
										enrolleeRepository.saveAndFlush(enrolleeDb);

										//Method call to create Event
										enrollmentService.createEventForEnrollee(enrolleeDb, EnrollmentPrivateConstants.EVENT_TYPE_CANCELLATION, EnrollmentPrivateConstants.EVENT_REASON_NON_PAYMENT, user);
									}

								}else if(subscriberStatus.equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM)){
									List<Enrollee> termEnrolleeList = enrolleeRepository.getPenddingOrEnrollEnrollee(enrollmentDb.getId());
									enrollmentDb.setBenefitEndDate(subscriber.getEffectiveEndDate());
									for (Enrollee enrolleeDb : termEnrolleeList) {
										enrolleeDb.setEffectiveEndDate(subscriber.getEffectiveEndDate());
										enrolleeDb.setLastPremiumPaidDate(subscriber.getLastPremiumPaidDate());
										if(enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING)){

											enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL));

										}else if(enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM)){

											enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM));
										}

										enrolleeDb.setUpdatedBy(user);
										enrolleeRepository.saveAndFlush(enrolleeDb);
										//Method call to create Event
										enrollmentService.createEventForEnrollee(enrolleeDb, EnrollmentPrivateConstants.EVENT_TYPE_CANCELLATION, EnrollmentPrivateConstants.EVENT_REASON_NON_PAYMENT, user);
									}
								}
							}
						}else{
							//XML Update at a individual level Confirmation only
							if(subscriber!=null && subscriber.getEmployerGroupNo()!=null){
								enrollmentDb.setGroupPolicyNumber(subscriber.getEmployerGroupNo());
							}

							for (Enrollee enrollee : enrolleeList) {
								String enrolleeStatus = enrollee.getEnrolleeLkpValue().getLookupValueCode();
								Enrollee enrolleeDb = enrolleeRepository.findEnrolleeByEnrollmentIdAndMemberId(enrollee.getExchgIndivIdentifier(), enrollmentDb.getId());
								if(enrolleeDb!=null && enrolleeStatus.equals(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM)){
									enrolleeDb.setIssuerIndivIdentifier(enrollee.getIssuerIndivIdentifier());
									enrolleeDb.getEnrollment().setIssuerSubscriberIdentifier(enrollment.getIssuerSubscriberIdentifier());
									if(enrollmentType!=null && !enrollmentType.equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP)){
										enrolleeDb.setEffectiveStartDate(enrollee.getEffectiveStartDate());
										//enrolleeDb.setEffectiveEndDate(enrollee.getEffectiveEndDate());
										enrolleeDb.setLastPremiumPaidDate(enrollee.getLastPremiumPaidDate());
									}
									enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_CONFIRM));
									enrolleeDb.setHealthCoveragePolicyNo(enrollee.getHealthCoveragePolicyNo());
									enrolleeDb.setUpdatedBy(user);
									enrolleeRepository.saveAndFlush(enrolleeDb);
									//Method call to create Event
									enrollmentService.createEventForEnrollee(enrolleeDb, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION, EnrollmentPrivateConstants.EVENT_REASON_INITIAL_ENROLLMENT, user);
								}
							}
						}
						//check Enrollment Status
						enrollmentService.checkEnrollmentStatus(enrollmentDb,user);
					}else{
						containsBadEnrollments = true;
						createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment);
						LOGGER.info("No Active Enrollment found with Exchange Subscriber Identifier = "+enrollment.getExchgSubscriberIdentifier()+"and CMS Plan ID = "+enrollment.getCMSPlanID());
					}
				}
				catch(Exception exception){
					LOGGER.error("Bad Enrollments :: updateEnrollmentsForXML",exception);
					containsBadEnrollments = true;
					createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment);
				}
			}
		}
		
		// Log failing enrollment data to file.
		if(containsBadEnrollments){
			String failureFolderPath = inbound834XMLPath + File.separator + EnrollmentPrivateConstants.FAILURE_FOLDER_NAME + File.separator + timeStamp;
			fileName = fileName.replaceAll(".xml", "");
			File failureFile = new File(failureFolderPath + File.separator + fileName + "_failedRecords.txt");
			File failureFolder = new File(failureFolderPath);
			try {
				if(!failureFolder.exists()){
					failureFolder.mkdirs();
				}
				if(!failureFile.exists()){
					failureFile.createNewFile();
				}
				FileWriter writer = new FileWriter(failureFile);
				BufferedWriter bufferedWriter = new BufferedWriter(writer, 1024);
				bufferedWriter.write(badEnrollmentDetailsBuffer.toString());
				bufferedWriter.flush();
				bufferedWriter.close();
			} catch (IOException e) {
				LOGGER.error("Error Writing failed records file",e);
			}
		}
		return containsBadEnrollments;
	}*/
	
	
/*	public void createContentForBadEnrollmentFile(StringBuilder badEnrollmentDetailsBuffer, Enrollment enrollment){
		if(badEnrollmentDetailsBuffer != null && enrollment != null){
			badEnrollmentDetailsBuffer.append("Exchange_Subscriber_Identifier=");
			badEnrollmentDetailsBuffer.append(enrollment.getExchgSubscriberIdentifier());
			badEnrollmentDetailsBuffer.append(",CMS_Plan_Id=");
			badEnrollmentDetailsBuffer.append(enrollment.getCMSPlanID());
			badEnrollmentDetailsBuffer.append("\n");
		}
	}*/
	
    //Method to move files from one folder to another folder. If lof is null then it will move all files from source to target folder else it moves the list of files
    private void moveFiles(String sourceFolder, String targetFolder, String fileType, List<File> lof) throws GIException{
          try{
        	  if (sourceFolder!=null && !("".equals(sourceFolder.trim())) &&!(new File(sourceFolder).exists())) {
                  return;
             }
             if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
                  new File(targetFolder).mkdirs();
             }
             
             if(lof!=null && !lof.isEmpty()){
                  for(File file:lof){
                       file.renameTo(new File(targetFolder+File.separator+file.getName()));
                  }
             }else{
                  //move all files from source to target folder
                  File[] files=getFilesInAFolder(sourceFolder,fileType);
                  if(files!=null && files.length>0){
                	  List<File> loFiles=Arrays.asList(files);
                	  moveFiles(sourceFolder,targetFolder,fileType, loFiles);
                  }
             }
          }catch(Exception e){
        	  LOGGER.error(ERROR_MOVE_FILES+e.toString());
        	  throw new GIException(ERROR_MOVE_FILES+e.toString(),e);
          }
         
    }
  //Method to move files from one folder to another folder. If lof is null then it will move all files from source to target folder else it moves the list of files
    private void moveCommissionFeedFiles(String sourceFolder, String targetFolder, String fileType, List<File> lof) throws GIException{
          try{
        	  if (sourceFolder!=null && !("".equals(sourceFolder.trim())) &&!(new File(sourceFolder).exists())) {
                  return;
             }
             if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
                  new File(targetFolder).mkdirs();
             }
             
             if(lof!=null && !lof.isEmpty()){
                  for(File file:lof){
                       file.renameTo(new File(targetFolder+File.separator+file.getName()));
                  }
             }else{
                  //move all files from source to target folder
                  File[] files=getFilesInAFolder(sourceFolder,fileType);
                  if(files!=null && files.length>0){
                	  List<File> loFiles=Arrays.asList(files);
                	  moveFiles(sourceFolder,targetFolder,fileType, loFiles);
                  }
             }
          }catch(Exception e){
        	  LOGGER.error(ERROR_MOVE_FILES+e.toString());
        	  throw new GIException(ERROR_MOVE_FILES+e.toString(),e);
          }
         
    }
    /*@Override
	public void updateEnrollmentByCarrierExcelFeed(String timeStamp) throws GIException{
		List<String> lof = null;
		String wipFolderPath = null;
		if(inboundFeedImporterPath != null && timeStamp != null ){
			//PFG_StatusFeed_ADA_07032014
			String currentFileName = EnrollmentPrivateConstants.CARRIER_FILE_NAME + getFormattedDateTime() +EnrollmentPrivateConstants.FILE_TYPE_EXCEL;
			wipFolderPath = inboundFeedImporterPath + File.separator +EnrollmentPrivateConstants.WIP_FOLDER_NAME;
			moveOnlyCurrentFiles(inboundFeedImporterPath,wipFolderPath,currentFileName,EnrollmentPrivateConstants.FILE_TYPE_EXCEL,null);
			lof = this.getCarrierFileNamesInAFolder(wipFolderPath, EnrollmentPrivateConstants.FILE_TYPE_EXCEL);
		}else{
			LOGGER.error("Error in updateEnrollmentByCarrierExcelFeed : No inboundFeedImporterPath defined ");
			throw new GIException("Error in updateEnrollmentByCarrierExcelFeed : No inboundFeedImporterPath defined "); 
		}

		if(lof != null && !lof.isEmpty()){
			String succesFolderPath= inboundFeedImporterPath + File.separator + EnrollmentPrivateConstants.SUCCESS_FOLDER_NAME + File.separator + timeStamp;
			String failureFolderPath = inboundFeedImporterPath + File.separator + EnrollmentPrivateConstants.FAILURE_FOLDER_NAME + File.separator + timeStamp;
			List<String> failedFileNames = new ArrayList<String>();
			for(String fileName : lof){
				boolean fileContainsBadEnrollments = false;
				List<File> currentFileList = new ArrayList<File>();
				File currentFile = new File(fileName);
				try{
					currentFileList.add(currentFile);
					List<ExcelModel> aetnaAppList = excelReader.getDataFromFile("",fileName);
					CarrierFeedSummary summary = new CarrierFeedSummary();
			        summary.setCarrierName("Aetna");
			        summary.setCarrierStatus("IN_PROGRESS");
			        summary.setFileName(fileName);
			        summary = carrierFeedSummaryRepository.saveAndFlush(summary);
			        
			        int failedUpdateCnt = 0;
			        int ignoredUpdateCnt = 0;
			        int successUpdateCnt = 0;
					for(ExcelModel aetnaApp : aetnaAppList){
						String processingStatus = processApplication(aetnaApp, summary);
						if(processingStatus.equals("FAILED")){
							failedUpdateCnt++;
						}else if(processingStatus.equals("IGNORED")){
							ignoredUpdateCnt++;
						}else if(processingStatus.equals("SUCCESS")){
							successUpdateCnt++;
						}
						if("FAILED".equals(processingStatus)){
							failedUpdateCnt++;
						}else if("IGNORED".equals(processingStatus)){
							ignoredUpdateCnt++;
						}else if("SUCCESS".equals(processingStatus)){
							successUpdateCnt++;
						}
					}
					summary.setCarrierStatus("COMPLETED");
					carrierFeedSummaryRepository.saveAndFlush(summary);
					
					LOGGER.info("updateEnrollmentByXML SERVICE :The batch executed succesfully for the file :: " + currentFile.getName());
					if(fileContainsBadEnrollments){
						failedFileNames.add(fileName);
						moveFiles(wipFolderPath, failureFolderPath, EnrollmentPrivateConstants.FILE_TYPE_EXCEL, currentFileList);
					}
					else{
						moveFiles(wipFolderPath, succesFolderPath, EnrollmentPrivateConstants.FILE_TYPE_EXCEL, currentFileList);
					}
				}
				catch(Exception e){
					LOGGER.error("updateEnrollmentByXML SERVICE :The batch did not execute succesfully for the file :: " + currentFile.getName() , e);
					failedFileNames.add(fileName);
					moveFiles(wipFolderPath, failureFolderPath, EnrollmentPrivateConstants.FILE_TYPE_EXCEL, currentFileList);
				}
			}
		}else{
			LOGGER.info("updateEnrollmentByCarrierExcelFeed  : Either the folder" + inboundFeedImporterPath +" does not exist or No Excel file found in the folder : "+ inboundFeedImporterPath);
		}
}*/

  /*  @Override
	@SuppressWarnings("deprecation")
	public void updateCommissionByCarrierFeed(String timeStamp) throws GIException{    
	List<String> lof = null;
	String wipFolderPath = null;
	if(commissionFeedInboundPath != null && timeStamp != null ){
		//PFG_StatusFeed_ADA_07032014
		//String currentFileName = EnrollmentConstants.CARRIER_FILE_NAME + getFormattedDateTime() +EnrollmentConstants.FILE_TYPE_EXCEL;
		//String currentFileName = "Vimo June 2014 Comp"+EnrollmentConstants.FILE_TYPE_EXCEL;
	 	wipFolderPath = commissionFeedInboundPath + File.separator +EnrollmentPrivateConstants.WIP_FOLDER_NAME;
 		moveCommissionFeedFiles(commissionFeedInboundPath,wipFolderPath,EnrollmentPrivateConstants.FILE_TYPE_EXCEL,null);
		lof = this.getCarrierFileNamesInAFolder(wipFolderPath, EnrollmentPrivateConstants.FILE_TYPE_EXCEL);
	}else{
		LOGGER.error("Error in updateCommissionByCarrierFeed : No commissionFeedInboundPath defined ");
		throw new GIException("Error in updateCommissionByCarrierFeed : No commissionFeedInboundPath defined "); 
	}

	if(lof != null && !lof.isEmpty()){
		String succesFolderPath= commissionFeedInboundPath + File.separator + EnrollmentPrivateConstants.SUCCESS_FOLDER_NAME + File.separator + timeStamp;
		String failureFolderPath = commissionFeedInboundPath + File.separator + EnrollmentPrivateConstants.FAILURE_FOLDER_NAME + File.separator + timeStamp;
		List<String> failedFileNames = new ArrayList<String>();
		for(String fileName : lof){
			boolean fileContainsBadEnrollments = false;
			List<File> currentFileList = new ArrayList<File>();
			File currentFile = new File(fileName);
			try{
				currentFileList.add(currentFile);
//				List<Object> resultList = null;			
							
				List<CommissionExcelModel> commisionAppList = commissionExcelReader.getDataFromFile("",fileName);
				 CarrierFeedSummary summary = new CarrierFeedSummary();
		        summary.setCarrierName("Aetna");
		        summary.setCarrierStatus("IN_PROGRESS");
		        summary.setFileName(fileName);
		        summary = carrierFeedSummaryRepository.saveAndFlush(summary);
		        
		        int failedUpdateCnt = 0;
		        int ignoredUpdateCnt = 0;
		        int successUpdateCnt = 0;
				for(CommissionExcelModel commissionApp : commisionAppList){
					String processingStatus = processCommissionApplication(commissionApp, summary);
					if("FAILED".equals(processingStatus)){
						failedUpdateCnt++;
					}else if("IGNORED".equals(processingStatus)){
						ignoredUpdateCnt++;
					}else if("SUCCESS".equals(processingStatus)){
						successUpdateCnt++;
					}
				}
				//summary.setCarrierStatus("COMPLETED");
			//	carrierFeedSummaryRepository.saveAndFlush(summary);
				
				LOGGER.info("updateCommissionByCarrierFeed SERVICE :The batch executed succesfully for the file :: " + currentFile.getName());
				if(fileContainsBadEnrollments){
					failedFileNames.add(fileName);
					moveFiles(wipFolderPath, failureFolderPath, EnrollmentPrivateConstants.FILE_TYPE_EXCEL, currentFileList);
				}
				else{
					moveFiles(wipFolderPath, succesFolderPath, EnrollmentPrivateConstants.FILE_TYPE_EXCEL, currentFileList);
				}
			}
			catch(Exception e){
				LOGGER.error("updateCommissionByCarrierFeed SERVICE :The batch did not execute succesfully for the file :: " + currentFile.getName(),e);
				failedFileNames.add(fileName);
				moveFiles(wipFolderPath, failureFolderPath, EnrollmentPrivateConstants.FILE_TYPE_EXCEL, currentFileList);
			}
		}
	}else{
		LOGGER.info("updateCommissionByCarrierFeed  : Either the folder" + commissionFeedInboundPath +" does not exist or No Excel file found in the folder : "+ commissionFeedInboundPath);
	}
}
*/    
  /*  @Override
	@SuppressWarnings("deprecation")
   	public void updateEnrollmentByBobFeed(String timeStamp) throws GIException{    
    List<String> lof = null;
   	String wipFolderPath = null;
   	if(bobFeedInboundPath != null && timeStamp != null ){
   	  	 	wipFolderPath = bobFeedInboundPath + File.separator +EnrollmentPrivateConstants.WIP_FOLDER_NAME;
    		moveCommissionFeedFiles(bobFeedInboundPath,wipFolderPath,EnrollmentPrivateConstants.FILE_TYPE_CSV,null);
   		lof = this.getCarrierFileNamesInAFolder(wipFolderPath, EnrollmentPrivateConstants.FILE_TYPE_CSV);
   	}else{
   		LOGGER.error("Error in updateEnrollmentByBobFeed : No bobFeedInboundPath defined ");
   		throw new GIException("Error in updateEnrollmentByBobFeed : No bobFeedInboundPath defined "); 
   	}

    	if(lof != null && !lof.isEmpty()){
   		String succesFolderPath= bobFeedInboundPath + File.separator + EnrollmentPrivateConstants.SUCCESS_FOLDER_NAME + File.separator + timeStamp;
   		String failureFolderPath = bobFeedInboundPath + File.separator + EnrollmentPrivateConstants.FAILURE_FOLDER_NAME + File.separator + timeStamp;
   		List<String> failedFileNames = new ArrayList<String>();
   		for(String fileName : lof){
   			boolean fileContainsBadEnrollments = false;
   			List<File> currentFileList = new ArrayList<File>();
   			File currentFile = new File(fileName);
   			try{
   				currentFileList.add(currentFile);   				 			
   				String mappingsFilename = bobFeedInboundPath + File.separator + "VIMO_TermVoid_20140704.csv.conf";
       		    FileType fileType = FileType.CSV;
    	        DataImporter<CsvModel> importer = new CsvModelDataImporter(fileName, fileType, mappingsFilename);
     	        List<CsvModel> bobFeedlist = importer.importAll();    	      	 				
   				 CarrierFeedSummary summary = new CarrierFeedSummary();
   		        summary.setCarrierName("Aetna");
   		        summary.setCarrierStatus("IN_PROGRESS");
   		        summary.setFileName(fileName);
   		        summary = carrierFeedSummaryRepository.saveAndFlush(summary);
   		        int failedUpdateCnt = 0;
   		        int ignoredUpdateCnt = 0;
   		        int successUpdateCnt = 0;
   				for(CsvModel bobFeedModel : bobFeedlist){
   					String processingStatus = processApplicationBob(bobFeedModel, summary);
   					if("FAILED".equals(processingStatus)){
   						failedUpdateCnt++;
   					}else if("IGNORED".equals(processingStatus)){
   						ignoredUpdateCnt++;
   					}else if( "SUCCESS".equals(processingStatus)){
   						successUpdateCnt++;
   					}
   				}
   						
   				LOGGER.info("updateEnrollmentByBobFeed SERVICE :The batch executed succesfully for the file :: " + currentFile.getName());
   				if(fileContainsBadEnrollments){
   					failedFileNames.add(fileName);
   					moveFiles(wipFolderPath, failureFolderPath, EnrollmentPrivateConstants.FILE_TYPE_CSV, currentFileList);
   				}
   				else{
   					moveFiles(wipFolderPath, succesFolderPath, EnrollmentPrivateConstants.FILE_TYPE_CSV, currentFileList);
   				}
   			}
   			catch(Exception e){
   				LOGGER.error("updateEnrollmentByBobFeed SERVICE :The batch did not execute succesfully for the file :: " + currentFile.getName(),e);
   				failedFileNames.add(fileName);
   				moveFiles(wipFolderPath, failureFolderPath, EnrollmentPrivateConstants.FILE_TYPE_CSV, currentFileList);
   			}
   		}
   	}else{
   		LOGGER.info("updateEnrollmentByBobFeed  : Either the folder" + bobFeedInboundPath +" does not exist or No CSV file found in the folder : "+ bobFeedInboundPath);
   	 }
   }
       private String getFormattedDateTime() {
		DateFormat df = new SimpleDateFormat("MMddyyyy");
		return df.format(new TSDate());
	}

   private String processApplication(ExcelModel aetnaApp, CarrierFeedSummary summary) {
    	
    	AetnaCarrierFeedSingleRecordProcessor processor = new AetnaCarrierFeedSingleRecordProcessor();
    	return processor.processRecord(aetnaApp, summary, enrollmentService, enrollmentRepository, carrierFeedDetailsRepository);
    }
  private String processCommissionApplication(CommissionExcelModel commissionApp, CarrierFeedSummary summary) {
    	
	  CommissionFeedProcessor processor = new CommissionFeedProcessor();
    	return processor.processCommissionRecord(commissionApp, summary, enrollmentService, enrollmentRepository, carrierFeedDetailsRepository);
    }*/
    public static void moveOnlyCurrentFiles(String sourceFolder, String targetFolder, String fileName,String fileType, List<File> lof) throws GIException{
        try{
      	  if (sourceFolder!=null && !("".equals(sourceFolder.trim())) &&!(new File(sourceFolder).exists())) {
                return;
           }
           if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
                new File(targetFolder).mkdirs();
           }
           
           if(lof!=null && !lof.isEmpty()){
                for(File file:lof){
              	   file.renameTo(new File(targetFolder+File.separator+file.getName()));
              	  
                }
           }else{
          	 
          	 
                //move all files from source to target folder
                File[] fileList =getAllCarrierFilesInAFolder(sourceFolder,fileName,fileType);
                
                if(fileList!=null && fileList.length>0){
                    for(File file:fileList){
                  	  if(file.getName().equalsIgnoreCase(fileName)){
                  	   file.renameTo(new File(targetFolder+File.separator+file.getName()));
                  	  }
                  	  
                    }
                }
                 
//                if(files!=null && files.length>0){
//              	  List<File> loFiles=Arrays.asList(files);
//              	   moveOnlyCurrentFiles(sourceFolder,targetFolder,fileName,fileType, loFiles);
//              	  
//                }
           }
        }catch(Exception e){
      	 // LOGGER.error(ERROR_MOVE_FILES+e.toString());
      	  throw new GIException(ERROR_MOVE_FILES+e.toString(),e);
        }
       
  }
    

	public static File[] getAllCarrierFilesInAFolder(String folderName,
			final String fileName, String fileType) throws GIException {
		File[] listOFFiles;
		try {
//			final String fileExtn = fileType;
//			final String carrierFileName = fileName;
			File folder = new File(folderName);
			listOFFiles = folder.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return true;
				}			});
			for (File pathname : listOFFiles) {
				if (pathname.isFile() && pathname.getName().equalsIgnoreCase(fileName)) {
					return listOFFiles;
				}
			}
		} catch (Exception e) {
			// LOGGER.error("Error in getFilesInAFolder"+e.getMessage(),e);
			throw new GIException(
					"Error in getFilesInAFolder" + e.getMessage(), e);
		}
		return listOFFiles;

	}
    		
 
	/*private String processApplicationBob(CsvModel aetnaBob, CarrierFeedSummary summary) {
		String CarrierAppID = aetnaBob.getApplctnId();
		String CarrierAppUID = aetnaBob.getUID();
		Integer enrollmentId = 0;
		Enrollment enrollment =  enrollmentService.findEnrollmentByCarrierAppId(CarrierAppID);
		String reason = "";
        String aetnaAppJson = "";	
    	String newStatusToUpdate = "N/A"; 
//    	enrollmentId = enrollment.getId();
        try{
        	ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(CsvModel.class);
        	aetnaAppJson = writer.writeValueAsString(aetnaBob);
        }catch(Exception excp){
        	LOGGER.error("Exception in processApplicationBob" , excp );
        	aetnaAppJson = excp.getMessage();
        }
        if(enrollment != null){
        	enrollmentId = enrollment.getId();
        }else{
        	reason = "Policy in Error: NO GI Enrollment Found for APP ID : "+CarrierAppID;
			return createCarrierFeedDetails("FAILED",newStatusToUpdate,enrollmentId, reason, summary, aetnaAppJson);
        }

		String currentEnrollmentStatus = enrollment.getEnrollmentStatusLkp().getLookupValueCode();
    
    	
    	if("SOLD".equals(currentEnrollmentStatus) || "DECLINED".equals(currentEnrollmentStatus) || "KICKBACK".equals(currentEnrollmentStatus)) {
			reason = "Policy in Error: BO Status is not permitting an Update.";
           	return createCarrierFeedDetails("FAILED",newStatusToUpdate,enrollmentId, reason, summary, aetnaAppJson);
        }
    	if (aetnaBob.getMemTrmDt() != null)
    	{
    		if("APPROVED".equals(currentEnrollmentStatus) || "INFORCE".equals(currentEnrollmentStatus)) {
    			newStatusToUpdate = "TERM";
            }
    	}
		else
		{
			if("LAPSED".equals(currentEnrollmentStatus) || "WITHDRAWN".equals(currentEnrollmentStatus)) {
				reason = "Policy in Error: BO Status is not permitting an Update.";
	           	return createCarrierFeedDetails("FAILED",newStatusToUpdate, enrollmentId,reason, summary, aetnaAppJson);
            }
			else if("APPROVED".equals(currentEnrollmentStatus) || "PENDING".equals(currentEnrollmentStatus) || "TERM".equals(currentEnrollmentStatus)) {
    			newStatusToUpdate = "INFORCE";
            }
		}
    	
    	if(!"N/A".equals(newStatusToUpdate) && !newStatusToUpdate.equalsIgnoreCase(currentEnrollmentStatus))
    	{
    		enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, newStatusToUpdate));
    		reason = "Status Changed : "+newStatusToUpdate;
    		createCarrierFeedDetails("UPDATE", newStatusToUpdate,enrollmentId,reason, summary, aetnaAppJson);
    		
    		if("TERM".equals(newStatusToUpdate))
    		{   Date memTermDate = DateUtil.StringToDate(aetnaBob.getMemTrmDt(),REQUIRED_DATE_FORMAT);
    			enrollment.setDateClosed(memTermDate);
    			reason = "Termination Reason : " + aetnaBob.getTermDescription() + ". Termination Date : " +aetnaBob.getMemTrmDt();
    			createCarrierFeedDetails("UPDATE",newStatusToUpdate,enrollmentId, reason, summary, aetnaAppJson);
    		}
    		if("INFORCE".equals(newStatusToUpdate))
    		{
    			enrollment.setDateClosed(null);
    		}
    		Date memEffDate = DateUtil.StringToDate(aetnaBob.getMemEffDt(),REQUIRED_DATE_FORMAT);
    		enrollment.setBenefitEffectiveDate(memEffDate);//is this correct? BenefitEffectiveDate = MemEffectiveDate?
    		enrollment.setCarrierAppUID(CarrierAppUID);
    		enrollmentRepository.save(enrollment);
    	}
    	else
    	{
			reason =  "Policies skipped (possibly processed earlier)";
			createCarrierFeedDetails("FAILED", newStatusToUpdate,enrollmentId, reason, summary, aetnaAppJson);
    	}
    	
    	return "SUCCESS";
	}*/
	
/*	private String createCarrierFeedDetails(String status,String CarrierStatus,Integer enrollmentId, String reason, CarrierFeedSummary summary, String aetnaAppJson) {

		CarrierFeedDetails cfd = new CarrierFeedDetails();
		cfd.setGiStatus(status);
		cfd.setCarrierStatus(CarrierStatus);
    	cfd.setStatus(status);
    	if(enrollmentId != 0){
    	 cfd.setEnrollmentId(enrollmentId);
    	}
    	cfd.setReason(reason);
    	cfd.setCarrierFeedSummaryId(summary.getId());
    	cfd.setFeedRecordJson(aetnaAppJson);
       	carrierFeedDetailsRepository.save(cfd);
       	return "FAILED";
	}*/


	/*@Override
	public void updateEnrollmentByXML(String timeStamp) throws GIException{
		List<String> lof = null;
		String wipFolderPath = null;
		if(inbound834XMLPath != null && timeStamp != null ){
			wipFolderPath = inbound834XMLPath + File.separator +EnrollmentPrivateConstants.WIP_FOLDER_NAME;
			moveFiles(inbound834XMLPath,wipFolderPath,EnrollmentPrivateConstants.FILE_TYPE_XML,null);
			lof = this.getFileNamesInAFolder(wipFolderPath, EnrollmentPrivateConstants.FILE_TYPE_XML);
		}else{
			LOGGER.error("Error in updateEnrollmentByXML : No updateEnrollmentXMLFilePath defined ");
			throw new GIException("Error in updateEnrollmentByXML : No updateEnrollmentXMLFilePath defined "); 
		}

		if(lof != null && !lof.isEmpty()){
			String succesFolderPath= inbound834XMLPath + File.separator + EnrollmentPrivateConstants.SUCCESS_FOLDER_NAME + File.separator + timeStamp;
			String failureFolderPath = inbound834XMLPath + File.separator + EnrollmentPrivateConstants.FAILURE_FOLDER_NAME + File.separator + timeStamp;
			List<String> failedFileNames = new ArrayList<String>();
			for(String fileName : lof){
				boolean fileContainsBadEnrollments = false;
				List<File> currentFileList = new ArrayList<File>();
				File currentFile = new File(fileName);
				try{
					currentFileList.add(currentFile);
					Enrollments enrollments = getEnrollmentsFromXML(currentFile);
					fileContainsBadEnrollments = updateEnrollmentsForXML(enrollments,currentFile.getName(),timeStamp);
					LOGGER.info("updateEnrollmentByXML SERVICE :The batch executed succesfully for the file :: " + currentFile.getName());
					if(fileContainsBadEnrollments){
						failedFileNames.add(fileName);
						moveFiles(wipFolderPath, failureFolderPath, EnrollmentPrivateConstants.FILE_TYPE_XML, currentFileList);
					}
					else{
						moveFiles(wipFolderPath, succesFolderPath, EnrollmentPrivateConstants.FILE_TYPE_XML, currentFileList);
					}
				}
				catch(Exception e){
					LOGGER.error("updateEnrollmentByXML SERVICE :The batch did not execute succesfully for the file :: " + currentFile.getName() , e);
					failedFileNames.add(fileName);
					moveFiles(wipFolderPath, failureFolderPath, EnrollmentPrivateConstants.FILE_TYPE_XML, currentFileList);
				}
			}
			//			if(failedFileNames != null && failedFileNames.size() > 0){
			//				try{
			//					enrollmentEmailNotificationService.sendEnrollmentStatusNotification(failedFileNames);
			//				}
			//				catch(GIException giException){
			//					
			//				}
			//			}
		}else{
			LOGGER.info("updateEnrollmentByXML SERVICE : Either the folder" +inbound834XMLPath +" does not exist or No XML file found in the folder : "+ inbound834XMLPath);
		}
}*/
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to get all the files of a specific type from a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
	/*@Override
	public List<String> getCarrierFileNamesInAFolder(String folderName, String fileType) throws GIException{
		try{
		final String fileExtn=fileType;
		File folder= new File(folderName);
		List<String> fileNames= null;
		File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn.toLowerCase()) || pathname.getName().endsWith(fileExtn.toUpperCase())){
					return true;
				}
				return false;
			}
		});
		if(listOFFiles!=null){
			fileNames= new ArrayList<String>();
			for(int i=0;i<listOFFiles.length;i++){
				fileNames.add(listOFFiles[i].getAbsolutePath());
			}
		}
		return fileNames;
		}catch(Exception e){
			throw new GIException("Error in getFilesInAFolder"+e.getMessage(),e);
		}
		
	}
	
	@Override
	public List<String> getFileNamesInAFolder(String folderName, String fileType) throws GIException{
		try{
		final String fileExtn=fileType;
		File folder= new File(folderName);
		List<String> fileNames= null;
		File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn)){
					return true;
				}
				return false;
			}
		});
		if(listOFFiles!=null){
			fileNames= new ArrayList<String>();
			for(int i=0;i<listOFFiles.length;i++){
				fileNames.add(listOFFiles[i].getAbsolutePath());
			}
		}
		return fileNames;
		}catch(Exception e){
			throw new GIException("Error in getFilesInAFolder"+e.getMessage(),e);
		}
		
	}


	*//**
	 * @author Priya
	 * @since
	 * This method is used to get all the Excel files of a specific type from a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 *//*
	@Override
	public List<String> getExcelFileNamesInCarrierFolder(String folderName, String fileType) throws GIException{
		try{
		final String fileExtn=fileType;
		File folder= new File(folderName);
		List<String> fileNames= null;
		File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn.toLowerCase()) ||  pathname.getName().endsWith(fileExtn.toUpperCase())){
					return true;
				}
				return false;
			}
		});
		if(listOFFiles!=null){
			fileNames= new ArrayList<String>();
			for(int i=0;i<listOFFiles.length;i++){
				fileNames.add(listOFFiles[i].getAbsolutePath());
			}
		}
		return fileNames;
		}catch(Exception e){
			throw new GIException("Error in getFilesInAFolder"+e.getMessage(),e);
		}
		
	}

	*//**
	 * @author parhi_s
	 * @since
	 * This method is used to get all the files of a specific type from a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
	private File[] getFilesInAFolder(String folderName, String fileType) throws GIException{
		try{
		final String fileExtn=fileType;
		File folder= new File(folderName);
		File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn.toLowerCase()) ||  pathname.getName().endsWith(fileExtn.toUpperCase())){
					return true;
				}
				return false;
			}
		});
		return listOFFiles;
		}catch(Exception e){
			LOGGER.error("Error in getFilesInAFolder"+e.getMessage(),e);
			throw new GIException("Error in getFilesInAFolder"+e.getMessage(),e);
		}
		
	}
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to transform an xml using a xsl file and then unmarshal to enrollments object
	 * @param xmlFile
	 * @return
	 * @throws GIException
	 */
	/*private Enrollments getEnrollmentsFromXML(File xmlFile) throws GIException{
		Enrollments enrollments = null;	
		try{			
			if(xmlFile!=null){
				JAXBContext context = JAXBContext.newInstance(Enrollments.class);
				Unmarshaller unMarshaller = context.createUnmarshaller();
				String strXML=null;
		   	 	TransformerFactory factory = null;
		   	
				InputStream strXSLPath = EnrollmentPrivateServiceImpl.class.getClassLoader().getResourceAsStream("RevEnrollmentTrans.xsl");
		
				factory = TransformerFactory.newInstance();
				Source xslSource = new StreamSource(strXSLPath);
				StreamSource xmlJAXB = new StreamSource(xmlFile);
				Templates template = factory.newTemplates(xslSource);
				Transformer transformer = template.newTransformer();
		
				StringWriter writerXSLT = new StringWriter();
				StreamResult resultXSLT = new StreamResult(writerXSLT);
				transformer.transform(xmlJAXB, resultXSLT);
		
				strXML = writerXSLT.toString();
				
				LOGGER.info("getEnrollmentsFromXML XML TO UPDATE :"+strXML);
				
				if(strXML==null){
		           LOGGER.error("Error in updateEnrollmentByXML():: XSLT Transformation Returns NULL");
		           throw new GIException("Error in updateEnrollmentByXML():: XSLT Transformation Returns NULL");                          
				}
				
				enrollments = (Enrollments) unMarshaller.unmarshal(new ByteArrayInputStream(strXML.getBytes()));
			}
			
		}catch (JAXBException e) {
			LOGGER.error("JAXBException at getEnrollmentsFromXML ::"+ e.getMessage());
			throw new GIException("JAXBException in getEnrollmentsFromXML():: "+e.getMessage(),e);       
		}catch(Exception e){
			LOGGER.error("Exception at getEnrollmentsFromXML ::"+ e.getMessage());
			throw new GIException("JAXBException in getEnrollmentsFromXML():: "+e.getMessage(),e);  
		}
			return enrollments;
	}*/
	
	/**
	 * @author ajinkya_m
	 * @param enrolleeList
	 * @return true if List has only one Enrollee and of type SUBSCRIBER else false
	 */
/*	private Enrollee checkForSubscriber(List<Enrollee> enrolleeList) {
		if(enrolleeList !=null && !enrolleeList.isEmpty()){
			for(Enrollee enrollee:enrolleeList){
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER)){
					return enrollee;
				}
			}
		}
		return null;	
	}*/
	
	/*@Override
	public void updateShopEnrollemntToCancelled(int employerId){
		LOGGER.info(" updateShopEnrollemntToCancelled Service START");
		int enrollmentTypeShopId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE,EnrollmentPrivateConstants.ENROLLMENT_TYPE_SHOP);
		int enrollmentStatusPendingId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING);
		Date currentDate = new TSDate();
		List<Enrollment> enrollmentList = enrollmentRepository.getPendingShopEnrollment(employerId,currentDate,enrollmentTypeShopId,enrollmentStatusPendingId);
		
		if(enrollmentList != null && !enrollmentList.isEmpty())
			{
				// Getting user
				AccountUser user = enrollmentService.getUserByName(GhixConstants.USER_NAME_EXCHANGE);
			
				//EnrollmentStatus = Cancelled = CANCEL
				int enrollmentStatusCancelledId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS,"CANCEL");
				LookupValue cancelledLookup = new LookupValue();
				cancelledLookup.setLookupValueId(enrollmentStatusCancelledId);
				
				// Set enrollment status to Cancelled
				for (Enrollment enrollment : enrollmentList) {
					List<Enrollee> enrolleeList = null;
					try {
						enrolleeList = enrolleeService.getEnrolleeByEnrollmentID(enrollment.getId());
					} catch (GIException e) {
						LOGGER.error("Error occured in getEnrolleeByEnrollmentID.", e);
					}
					if(enrolleeList != null){
					for (Enrollee enrollee : enrolleeList) {
						
						enrollee.setEnrolleeLkpValue(cancelledLookup);
						
						//	Save enrollee
						enrolleeRepository.saveAndFlush(enrollee);
						
						//	Create Event
						enrollmentService.createEventForEnrollee(enrollee,EnrollmentPrivateConstants.EVENT_TYPE_CANCELLATION,EnrollmentPrivateConstants.EVENT_REASON_NON_PAYMENT, user);
					 }
					}
					// Check if all the enrollee of given enrollment are cancel/terminated
					enrollmentService.checkEnrollmentStatus(enrollment ,user);
					LOGGER.info(" updateShopEnrollemntToCancelled Service END");
				}
			
			}
		}*/
	
	/**
	 * @author panda_p
	 * @since 29-Mar-2013
	 * 
	 * This Will called thru batch and used to send the carrier updated data to AHBX  
	 * 
	 * 
	 */
	/*@Override	
	@Transactional(readOnly=true)
	public void sendCarrierUpdatedDataToAHBX() throws GIException{
		LOGGER.info("sendCarrierUpdatedDataToAHBX SERVICE : START");
		Map<String,Object> reqParam = new HashMap<String,Object>();

		//getting Enrollee list updated by carrier
		List<Enrollee> enrolleeList= enrolleeService.getCarrierUpdatedEnrollments();

		if(enrolleeList!=null && !enrolleeList.isEmpty()){
			List<Map<String,Object>> planList = new ArrayList<Map<String,Object>>();
			
			LOGGER.info("sendCarrierUpdatedDataToAHBX SERVICE : enrolleeList.size = "+enrolleeList.size());
			
			for(Enrollee enrollee : enrolleeList){
				Map<String,Object> planMap = new HashMap<String,Object>();

				if(enrollee.getEnrollment()!=null){
					Enrollment enrollment= enrollee.getEnrollment();
					planMap.put(EnrollmentPrivateConstants.CASE_ID, enrollment.getHouseHoldCaseId());
					planMap.put(EnrollmentPrivateConstants.ENROLLMENT_ID, enrollment.getId());
					planMap.put(EnrollmentPrivateConstants.PLAN_ID, enrollment.getPlan().getId());
				}else{
					LOGGER.info("sendCarrierUpdatedDataToAHBX SERVICE : Enrollment not found for the enrolle updatedby carrier");
					throw new GIException("Enrollment not found for the enrolle updatedby carrier");
				}

				planMap.put(EnrollmentPrivateConstants.ENROLLEE_ID, enrollee.getId());
				planMap.put(EnrollmentPrivateConstants.POLICY_ID, enrollee.getHealthCoveragePolicyNo());
				
				if(enrollee.getEffectiveStartDate()!=null){
					planMap.put(EnrollmentPrivateConstants.EFFECTIVE_START_DATE, DateUtil.dateToString(enrollee.getEffectiveStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
				}else{
					planMap.put(EnrollmentPrivateConstants.EFFECTIVE_START_DATE,"");
				}
				if(enrollee.getEnrolleeLkpValue()!=null 
						&& (enrollee.getEnrolleeLkpValue().getLookupValueCode()!=null 
						&& (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL) 
								|| enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentPrivateConstants.ENROLLMENT_STATUS_TERM)))){
					if(enrollee.getEffectiveEndDate()!=null){
						planMap.put(EnrollmentPrivateConstants.EFFECTIVE_END_DATE, DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
					}else{
						planMap.put(EnrollmentPrivateConstants.EFFECTIVE_END_DATE,"");
					}
				}
				
				//planMap.put(EnrollmentConstants.EFFECTIVE_END_DATE, enrollee.getEffectiveEndDate());
				
				planMap.put(EnrollmentPrivateConstants.STATUS, enrollee.getEnrolleeLkpValue().getLookupValueCode());

				planMap.put(EnrollmentPrivateConstants.MEMBER_ID, enrollee.getExchgIndivIdentifier());
				
				if(enrollee.getLastPremiumPaidDate()!=null){
					planMap.put(EnrollmentPrivateConstants.LAST_PREMIUM_PAID_DATE, DateUtil.dateToString(enrollee.getLastPremiumPaidDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
				}else{
					planMap.put(EnrollmentPrivateConstants.LAST_PREMIUM_PAID_DATE,"");
				}
				
				//planMap.put(EnrollmentConstants.LAST_PREMIUM_PAID_DATE, enrollee.getLastPremiumPaidDate());

				LOGGER.info(" sendCarrierUpdatedDataToAHBX SERVICE : IND 21 INPUT : "+planMap.toString());
				planList.add(planMap);

			}

			reqParam.put("planList", planList);

			LOGGER.info(" sendCarrierUpdatedDataToAHBX SERVICE : Calling IND21 ");
			String verifyResponse = restTemplate.postForObject(GhixEndPoints.AHBXEndPoints.ENROLLMENT_IND21_CALL_URL, reqParam, String.class);
			LOGGER.info(" sendCarrierUpdatedDataToAHBX SERVICE : verifyResponse from IND21 = "+verifyResponse);
			
			XStream xStream = GhixUtils.getXStreamStaxObject();
			EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xStream.fromXML(verifyResponse);
			if(enrollmentResponse!=null && (enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList() !=null && !enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList().isEmpty())){
				List<SendUpdatedEnrolleeResponseDTO> sendUpdatedEnrolleeResponseDTOList =enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList();
				enrolleeService.saveCarrierUpdatedDataResponse(sendUpdatedEnrolleeResponseDTOList);
			}
		}
		LOGGER.info("sendCarrierUpdatedDataToAHBX SERVICE : END");	
	}*/


	/*@Override
	public void serveCmsReconCancelTermEnrollment(String extractMonth)throws GIException {
		try{
			LOGGER.info(" serveCmsReconCancelTermEnrollment Service : START");
			Date startDate = null;
			Date endDate = null;

			if(extractMonth!=null && extractMonth.equalsIgnoreCase(EnrollmentPrivateConstants.IND_RECON_EXTRACT_MONTH_PRIOR)){
				startDate=getLastMonthStartDateTime();
				endDate=getCurrentMonthStartDateTime();
			}else{
				LOGGER.info(" serveCmsReconCancelTermEnrollment Service ExtractMonth is NULL ");
				startDate = getJOBLastRunDate(EnrollmentPrivateConstants.CMS_RECONCILIATION_ENROLLMENT_REPORT_JOB);
				if(startDate == null){
					startDate=getCurrentMonthStartDateTime();
				}
				endDate=new TSDate();
			}		

			String outputfolder=null;
			if(xmlCmsReconCancelTermEnrollmentPath != null && !("".equals(xmlCmsReconCancelTermEnrollmentPath.trim()))){
				outputfolder = xmlCmsReconCancelTermEnrollmentPath;
			}else {
				throw new GIException("Error in serveCmsReconCancelTermEnrollment() :: No Output Path defined in the configuration file");
			}

			if (!(new File(outputfolder).exists())) {
				new File(outputfolder).mkdirs();
				LOGGER.info(" serveCmsReconCancelTermEnrollment Service : outputfolder Created Path ="+outputfolder);
			}
			String outputPath=null;
			List<Enrollment>  enrollmentList= new ArrayList<Enrollment>();
			enrollmentList= enrollmentRepository.findCancelTerminatedEnrollmentByUpdateDate(startDate, endDate);

			Enrollments enrollments = setEnrollmentsFor834Reconciliation(enrollmentList,EnrollmentPrivateConstants.ENROLLMENT_STATUS_CANCEL);				
			outputPath = outputfolder + File.separator + dateFormat.format(new TSDate()) + ".xml";				
			generateEnrollmentsXMLReport(enrollments, outputPath,"INDEnrollmentReconXMLTrans.xsl");


		}catch(Exception e){
			LOGGER.error("Error in serveCmsReconCancelTermEnrollment() ::"+e.getMessage(),e);		
			throw new GIException("Error in serveCmsReconCancelTermEnrollment() ::"+e.getMessage(),e);
		}
		LOGGER.info(" serveCmsReconCancelTermEnrollment Service : END");
	}
	*/
	public void getFormatedRatingArea(Enrollee enrollee){
		if(enrollee != null && enrollee.getRatingArea() != null){
			String ratingArea = enrollee.getRatingArea();
			try{
				StringBuilder formatedRatingArea = new StringBuilder();
				//String stateCode = EnrollmentPrivateConstants.STATE_CODE;
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
				formatedRatingArea.append(EnrollmentPrivateConstants.RATING_AREA_PREFIX);
				formatedRatingArea.append(stateCode);

				NumberFormat numberFormater = NumberFormat.getInstance();
				numberFormater.setMinimumIntegerDigits(EnrollmentPrivateConstants.THREE);
				numberFormater.setMaximumFractionDigits(0);
				numberFormater.setGroupingUsed(false);
				formatedRatingArea.append(numberFormater.format(Integer.parseInt(ratingArea)));
				enrollee.setRatingArea(formatedRatingArea.toString());
			}
			catch (Exception e){
				LOGGER.error("Error in formatting Rating Area for Enrollee with id:: " +  enrollee.getId() +  " -- " + e.getMessage() , e);
			}
		}
	}


	/**
	 * @author negi_s
	 * @since 05/03/2015
	 * 
	 *        One time job to the add missing phone numbers in the enrollee
	 *        table with the numbers received from the plan display API
	 * 
	 */
	@Override
	public boolean setMissingEnrolleePhoneNumbers() throws GIException {

		//List<Integer> indOrderItemIdList = enrollmentRepository.getIndOrderItemIdForMissingPhNo();
		List<Long> ssapIdList = enrollmentRepository.getSsapApplicationIdForMissingPhNo();
		if(null != ssapIdList && !ssapIdList.isEmpty()){
			LOGGER.info("Ssap Application Ids received :: " + ssapIdList.size());
			for(Long ssapId : ssapIdList){
				List<FFMMemberResponse> memberList = getMemberListFromPld(ssapId);
				if(!memberList.isEmpty()){
					List<Enrollment> enrollmentList = enrollmentRepository.findBySsapApplicationIdActive(ssapId);
					Enrollment enrollment = null;
					if(null != enrollmentList && !enrollmentList.isEmpty()){
						enrollment = enrollmentList.get(0);
						for(Enrollee  enrollee : enrollment.getEnrollees()){
							for(FFMMemberResponse member : memberList){
								if((null !=  member.getFfeApplicantId()) && member.getFfeApplicantId().toString().equalsIgnoreCase(enrollee.getExchgIndivIdentifier())){
									enrollee.setPrimaryPhoneNo(member.getPhoneNumber());
									break;
								}
							}
						}
						enrollmentService.saveEnrollment(enrollment);
					}
				}else{
					LOGGER.info("No plan display member data found for given ssapId :: " + ssapId);
				}
			}
		}else{
			throw new RuntimeException("No ssapId found with empty phone number member data");
		}
		return true;
	}

	/**
	 * Calls the plan display API to get the memberList for the given ssapId.
	 * @author negi_s 
	 * @param ssapId
	 * @return List<FFMMemberResponse> memberList
	 */
	@SuppressWarnings("deprecation")
	private List<FFMMemberResponse> getMemberListFromPld(Long ssapId) {
		// Insert Rest Call
	
		List<FFMMemberResponse> memberList = new ArrayList<FFMMemberResponse>();
		FFMHouseholdResponse pldHouseholdResponse = null;
		Map<String, String> params = new HashMap<String, String>();
		params.put("ssapId", ssapId+"");
		try{
			pldHouseholdResponse = ghixRestTemplate.getForObject(PlandisplayEndpoints.EXTRACT_PERSONID_BY_SSAP_ID ,FFMHouseholdResponse.class, params);
		}catch(Exception e){
			LOGGER.error("Error calling Plan Display API for ssap Id :: "+ssapId , e);
		}
		if(null != pldHouseholdResponse){
			memberList = pldHouseholdResponse.getMembers();
		}
//		FFMMemberResponse ffmMemberResponse = new FFMMemberResponse(); //314458714102528274;
//		ffmMemberResponse.setFfeApplicantId(314458714102528274l);
//		ffmMemberResponse.setPhoneNumber("123232323");
//		memberList.add(ffmMemberResponse);
		return memberList;
	}
	
}
