package com.getinsured.hix.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentCoverageValidationRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateRequest;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync.RecordType;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.platform.util.exception.GIException;

public interface EnrollmentCreationService {

	/**
	 * 
	 * @param pldOrderResponse
	 * @param applicant_esig
	 * @param onBehalfActionCheckbox
	 * @return
	 * @throws GIException
	 */
	EnrollmentResponse createSpecialEnrollment(PldOrderResponse pldOrderResponse, String applicant_esig,
			String onBehalfActionCheckbox, RecordType recordType) throws GIException;

	/**
	 * 
	 * @param pldOrderResponse
	 * @param applicant_esig
	 * @param onBehalfActionCheckbox
	 * @return
	 * @throws GIException
	 */
	EnrollmentResponse createEnrollment(PldOrderResponse pldOrderResponse, String applicant_esig,
			String onBehalfActionCheckbox, RecordType recordType) throws GIException;
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws GIException
	 */
	void automateSpecialEnrollment(EnrollmentUpdateRequest request, EnrollmentResponse response) throws GIException;
	
	/**
	 * 
	 * @param newMemberMapList
	 * @throws GIException
	 */
	void terminateNewlyAddedmembers(EnrollmentResponse response, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) throws GIException;
	
	/**
	 * 
	 * @param caseId
	 * @param orderId
	 * @param enrollmentList
	 * @param disEnrollMemberList
	 * @param enroll_type
	 * @param autoRenewalFlag
	 * @param cancelTermEnrollmentIds
	 * @return
	 */
	Map<String,Object> getIndvPSDetails(String caseId, String orderId, List<Enrollment> enrollmentList, List<Map<String, String>> disEnrollMemberList, Character enroll_type, String autoRenewalFlag, List<String> cancelTermEnrollmentIds);
	
	boolean validateEnrollmentOverlap(EnrollmentCoverageValidationRequest request);
	
	/**
	 * Overlap check utility method
	 * @param enrollmentList
	 * @param disEnrollmentList
	 * @return
	 * @throws GIException
	 */
	boolean validateEnrollmentMemberOverlapping(List<Enrollment> enrollmentList, final List<Enrollment> disEnrollmentList) throws GIException;
	
	EnrollmentResponse updateIndividualStatus(EnrollmentResponse enrollmentResponse, Long app_id, List<Enrollment> enrollmentList, boolean isAppUpdateByReInstatement);
	
}
