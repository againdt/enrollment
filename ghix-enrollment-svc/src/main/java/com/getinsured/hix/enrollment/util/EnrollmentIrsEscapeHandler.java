package com.getinsured.hix.enrollment.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.lang3.math.NumberUtils;

import com.sun.xml.bind.marshaller.CharacterEscapeHandler;

/**
 * @author negi_s
 * @since 09-03-2015
 * 
 * Custom CharacterEscapeHandler to remove special characters from named
 * fields in the IRS report
 * 
 */
public class EnrollmentIrsEscapeHandler implements CharacterEscapeHandler {

	/**
	 * 
	 */
	public EnrollmentIrsEscapeHandler() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sun.xml.bind.marshaller.CharacterEscapeHandler#escape(char[],
	 * int, int, boolean, java.io.Writer)
	 */
	@Override
	public void escape(char[] ch, int start, int length, boolean isAttVal,
			Writer out) throws IOException {

		StringWriter buffer = new StringWriter();
		for (int i = start; i < start + length; i++) {
			buffer.write(ch[i]);
		}
		String str = buffer.toString();
		if(!NumberUtils.isNumber(str)){
			 for (int i = start; i < start + length; i++) {
		            char c = ch[i];
				if (c == '&' || c == '"' || c == '\'' || c == '#' || c == '<'
						|| c == '>' || c == '\u2014') {
					out.write("");
					continue;
				}
		            // otherwise print normally
		            out.write(c);
		        }
		}else{
			out.write(str); //Number Field
		}

	}

}
