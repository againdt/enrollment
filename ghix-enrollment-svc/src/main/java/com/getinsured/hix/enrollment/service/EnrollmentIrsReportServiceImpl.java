package com.getinsured.hix.enrollment.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.shop.RestEmployerEnrollmentDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.repository.IIRSOutboundTransmissionRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentIrsEscapeHandler;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrlEligibleEmployeeIRSDTO;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.EnrolleeRelationshipAud;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentAud;
import com.getinsured.hix.model.enrollment.IRSOutboundTransmission;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

import us.gov.treasury.irs.common.BusinessAddressGrpType;
import us.gov.treasury.irs.common.BusinessNameType;
import us.gov.treasury.irs.common.CompletePersonNameType;
import us.gov.treasury.irs.common.EmployeeSHOPPolicyType;
import us.gov.treasury.irs.common.SHOPEmployeeType;
import us.gov.treasury.irs.common.SHOPEmployerInfoType;
import us.gov.treasury.irs.common.SHOPEmployerType;
import us.gov.treasury.irs.common.SHOPExchangePersonType;
import us.gov.treasury.irs.common.SHOPExchangeType;
import us.gov.treasury.irs.common.USAddressGrpType;
import us.gov.treasury.irs.msg.monthlyexchangeperiodicdata.HealthExchangeType;
import us.gov.treasury.irs.msg.monthlyexchangeperiodicdata.ObjectFactory;


/**
 * Provides service level implementation and processing for IRS Reports.
 * Mostly used in ghix-batch to generate IRS reports.
 * 
 * @author Aditya-S
 * @since 06/19/2014
 *
 */

@Service("enrollmentIrsReportService")
public class EnrollmentIrsReportServiceImpl implements EnrollmentIrsReportService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentIrsReportServiceImpl.class);

	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IEnrollmentAudRepository enrollmentAudRepository;
	@Autowired private IEnrolleeAudRepository enrolleeAudRepository;
	@Autowired private IEnrolleeRepository enrolleeRepository;
	@Autowired private IIRSOutboundTransmissionRepository irsOutboundTransmissionRepo;
	@Autowired private IEnrolleeRelationshipRepository enrolleeRelationshipRepository;
	@Autowired private IEnrolleeRelationshipAudRepository enrolleeRelationshipAudRepository;
	@Autowired private RestTemplate restTemplate;
	@Autowired private EnrollmentEmailNotificationService enrollmentEmailNotificationService;
	
	private static final String SKIPPED_HOUSEHOLD_ACTION_MSG = "Manually fix the data for the skipped household case IDs based on the accompanying error messages logged in the above text file.";

	//private static final int MAX_HOUSEHOLDS_PER_FILE = 50;

	private void formEmployerAndEmployerEnrlIdsMap(List<Integer> employerEnrIdList,List<Integer> employerIdList,Map<Integer, List<Integer>>  employerAndEmployerEnrlIdsMap, List<Object[]> employerAndEmployerEnrlIdMapList ){
		employerEnrIdList.clear();
		employerIdList.clear();
			if(employerAndEmployerEnrlIdMapList!=null && !employerAndEmployerEnrlIdMapList.isEmpty()){
				for (Object[] row: employerAndEmployerEnrlIdMapList){
					if(employerIdList.contains((Integer)row[1])){
						List<Integer> emplEnrlIdList=employerAndEmployerEnrlIdsMap.get((Integer)row[1]);
						if(!employerEnrIdList.contains((Integer)row[0])){
							employerEnrIdList.add((Integer)row[0]);
						}
						if(!emplEnrlIdList.contains((Integer)row[0])){
							emplEnrlIdList.add((Integer)row[0]);
						}
						employerAndEmployerEnrlIdsMap.put((Integer)row[1], emplEnrlIdList);
						
					}else{
						employerIdList.add((Integer)row[1]);
						if(!employerEnrIdList.contains((Integer)row[0])){
							employerEnrIdList.add((Integer)row[0]);
						}
						List<Integer> emplEnrlIdList= new ArrayList<>();
						emplEnrlIdList.add((Integer)row[0]);
						employerAndEmployerEnrlIdsMap.put((Integer)row[1], emplEnrlIdList);
					}
				}
			}
		}
	
	@Override
	public void generateShopIrsReport(int month,int year) throws GIException {
		LOGGER.info("******* Inside generateShopIrsReport ********/");
		EnrollmentUtils.cleanDirectory(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH));
		String batchDate = getDateTime();
		Map<String, String> skippedEnrollment =  new HashMap<String,String>();
		List<Integer> employerEnrIdList= new ArrayList<>();
		List<Integer> employerIdList= new ArrayList<>();
		Map<Integer, List<Integer>> employerAndEmployerEnrlIdsMap= new HashMap<>();
		//List<Integer> employerEnrIdList = getUniqueEmployerEnrIdList(month, year);
		
		List<Object[]> employerAndEmployerEnrlIdMapList= getEmployerIdEmployerEnrollmentIdMap(month, year);
		formEmployerAndEmployerEnrlIdsMap(employerEnrIdList,employerIdList, employerAndEmployerEnrlIdsMap,employerAndEmployerEnrlIdMapList );
		if(employerEnrIdList != null && !employerEnrIdList.isEmpty()){
			// Call Shop Api for employer data
			SHOPExchangeType shopExchangetype = getEmployerInfoFromShop(employerEnrIdList);
			
			// Call Shop Api for Employee eligibility data
			Map<Integer,List<EnrlEligibleEmployeeIRSDTO>> employeeEligibilityMap = getEmployeeEligibilityFromShop(employerEnrIdList,month, year);
			
			int maxHouseEmployerPerFile = 1;
			if(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSXML_MAXHOUSEHOLD_LIMIT) != null){
				maxHouseEmployerPerFile = Integer.valueOf(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSXML_MAXHOUSEHOLD_LIMIT));
			}
			LOGGER.info("GenerateShopIrsReport maxHouseEmployerPerFile "+ maxHouseEmployerPerFile);
			
			if(shopExchangetype != null && shopExchangetype.getSHOPEmployer() != null){

				List<SHOPEmployerType> shopEmployerList = shopExchangetype.getSHOPEmployer();
				//List<SHOPEmployerType> subList = null;
				List<Integer> subList = null;
					int size = shopEmployerList.size();
					int numberOfFiles = size / maxHouseEmployerPerFile;
					if (size % maxHouseEmployerPerFile != 0) {
						numberOfFiles++;
					}
					int firstIndex = 0;
					int lastIndex = 0;
					for (int fileNumber = 0; fileNumber < numberOfFiles; fileNumber++) {
						firstIndex = fileNumber * maxHouseEmployerPerFile;
						lastIndex = (fileNumber + 1) * maxHouseEmployerPerFile;
						if (lastIndex > size) {
							lastIndex = size;
						}
						
					//subList = shopEmployerList.subList(firstIndex, lastIndex);
						subList=employerIdList.subList(firstIndex, lastIndex);
					
					if(subList != null && !subList.isEmpty()){
						LOGGER.info("GenerateShopIrsReport processing for "+ subList.toString());
						
						try{
							mounthWiseShopEmployerList(shopEmployerList,employeeEligibilityMap,month, year,fileNumber,skippedEnrollment, subList, employerAndEmployerEnrlIdsMap);
						
						}catch(Exception e){
							LOGGER.info("Exception While Processing Sub_List :: " + subList.toString(), e);
							skippedEnrollment.put("EmployerEnrollmentIds: "+subList.toString(),shortenedStackTrace(e, EnrollmentConstants.THREE));
						}
						
					}else{
						LOGGER.info("GenerateShopIrsReport subList is empty or null ");
					}
				}
			//Write Error log File		
			logSkippedHouseholds(skippedEnrollment , batchDate);
					
			}else{
				throw new GIException("No shopExchangetype Records found @ generateShopIrsReport GET_EMPLOYER_INFO_IRS API return Null ");
			}
		}
	}
	
	private void mounthWiseShopEmployerList(List<SHOPEmployerType> shopEmployerList, Map<Integer, List<EnrlEligibleEmployeeIRSDTO>> employeeEligibilityMap,
			int month, int year, int fileNumber, Map<String, String> skippedEnrollment, List<Integer> employerIds, Map<Integer,List<Integer>> enplAndEmplEnrlMap)  throws GIException{
		List<SHOPEmployerType> mounthWiseShopEmployerList = new ArrayList<SHOPEmployerType>();
		List<EnrlEligibleEmployeeIRSDTO> enrlEligibleEmployeeIRSDTOList = null;

		for (Integer employerId: employerIds) {
			List<Integer> employerEnrollmentIdList= enplAndEmplEnrlMap.get(employerId);
			if(employeeEligibilityMap != null && employerEnrollmentIdList!=null && !employerEnrollmentIdList.isEmpty()){
				enrlEligibleEmployeeIRSDTOList= new ArrayList<>();
				for(Integer employerEnrollmentId: employerEnrollmentIdList){
					enrlEligibleEmployeeIRSDTOList.addAll(employeeEligibilityMap.get(employerEnrollmentId));
				}
				
			}
			// call this for each month.
			// Create new ShopEmployer Type for each month.
			//LOGGER.info(" MounthWiseShopEmployerList EmployerEnrollmentId : "+ shopEmployerType.getEmployerEnrollmentId());
			List<SHOPEmployerType> employerSHOPTypeList= getshopEmployerTypeByEmpEnrlId(employerEnrollmentIdList, shopEmployerList);
			mounthWiseShopEmployerList.add(getMonthWiseShopEmployerList(employerSHOPTypeList, month, year,enrlEligibleEmployeeIRSDTOList, skippedEnrollment, employerEnrollmentIdList));
		}

		// get XML from SHOPExchangeType
		sendShopEmployerListForReportProcessing(mounthWiseShopEmployerList,month,year,fileNumber);
	}

	List<SHOPEmployerType> getshopEmployerTypeByEmpEnrlId(List<Integer> empEnrlIdList, List<SHOPEmployerType>  shopEmployerEnrlList){
		List<SHOPEmployerType> employerSHOPTypeList=null;
		if(shopEmployerEnrlList!=null && !shopEmployerEnrlList.isEmpty()){
			employerSHOPTypeList= new ArrayList<>();
			for (SHOPEmployerType shopEmployerType:shopEmployerEnrlList){
				if(empEnrlIdList.contains(shopEmployerType.getEmployerEnrollmentId())){
					employerSHOPTypeList.add(shopEmployerType);
				}
			}
		}
		return employerSHOPTypeList;
	}

	@Override
	public Map<Integer, List<EnrlEligibleEmployeeIRSDTO>> getEmployeeEligibilityFromShop(List<Integer> employerEnrIdList, int month, int year) throws GIException {
		RestEmployerEnrollmentDTO restEmployerEnrollmentDTO = new RestEmployerEnrollmentDTO();
		Map<Integer, List<EnrlEligibleEmployeeIRSDTO>> employeeEligibilityMap = null;
		String monthStartDate = null;
		ShopResponse shopResponse = null;
		String shopResponseStr = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();

		try {
			monthStartDate = DateUtil.dateToString(EnrollmentUtils.getMonthStartDateTime(month,year),GhixConstants.REQUIRED_DATE_FORMAT);
		} catch (GIException e) {
			throw new GIException("Error getting currentMonthStartDate", e);
		}
		
		restEmployerEnrollmentDTO.setEmployerEnrollmentList(employerEnrIdList);
		restEmployerEnrollmentDTO.setCoverageEligibilityDate(monthStartDate);
		
		LOGGER.info("Calling Shop service to get EmployeeEligibility data");
		shopResponseStr = restTemplate.postForObject(ShopEndPoints.GET_ELIGIBLE_EMPLOYEE_DETAILS, restEmployerEnrollmentDTO, String.class); 
		LOGGER.info("shopExchangeTypeResponse (EmployeeEligibility) : " );

		if(shopResponseStr != null){
			shopResponse = (ShopResponse)xstream.fromXML(shopResponseStr);
			if(shopResponse != null && !shopResponse.getStatus().equalsIgnoreCase("FAILURE")){
				Map<String, Object> responseData = shopResponse.getResponseData();
				employeeEligibilityMap = (Map<Integer, List<EnrlEligibleEmployeeIRSDTO>>) responseData.get("EligibleEmployeeDetails"); 
			}
		}	
		return employeeEligibilityMap;
	}
	
	@Override
	public SHOPEmployerType getMonthWiseShopEmployerList(List<SHOPEmployerType> shopEmployerTypeList,int reportingMonth, int applicableYear, 
			List<EnrlEligibleEmployeeIRSDTO> enrlEligibleEmployeeIRSDTOList,
			Map<String, String> skippedEnrollment, List<Integer> employerEnrollmentIdList) throws GIException {

		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		SHOPEmployerType shopEmployerTypeNew = null;
		if(shopEmployerTypeList!=null && !shopEmployerTypeList.isEmpty()){




			List<SHOPEmployerInfoType> shopEmployerInfoTypeList = null;
			SHOPEmployerInfoType shopNewEmployerInfoType = null;

			if(reportingMonth == 0){
				reportingMonth = 12;
				applicableYear--;
			}

			Date yearStartDate = EnrollmentUtils.getCurrentYearStartDate(applicableYear);

			shopEmployerTypeNew = new SHOPEmployerType();
			shopEmployerTypeNew.setEIN(removeNonDigitCharShop(shopEmployerTypeList.get(0).getEIN()));


			shopEmployerInfoTypeList = shopEmployerTypeNew.getSHOPEmployerInfo();
			//SHOPEmployerInfoType shopEmployerInfoType = shopEmployerTypeList.get(0).getSHOPEmployerInfo().get(0);
			/**
			 * Schema Change Start
			 */
			// Get Distinct Enrollment for this Employer_enrollment
			// For Each Enrollment create <SHOPEmployee> tag Add it to <SHOPEmployer>
			// Within Each <SHOPEmployee> Tag create <EmployeeCoverage> for each Month.
			Date reportingEndDate = EnrollmentUtils.getMonthEndDate(reportingMonth-1,applicableYear);

			// Get Enrollment List for Employer Enrollment.
			enrollmentList = enrollmentRepository.getEnrollmentByEmployer(employerEnrollmentIdList, DateUtil.dateToString(yearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY),  DateUtil.dateToString(reportingEndDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));

			List<Long> employeeAppIdList= new ArrayList<>();
			List<Integer> employeeIdList= new ArrayList<>();
			//= getEmployeeAppIdList(enrollmentList);
			Map<Integer,List<Enrollment>> employeeEmployeeEnrollmentMap= new HashMap<>();
			setEmployeeAndEmployeeAppId(employeeIdList, employeeAppIdList, enrollmentList,employeeEmployeeEnrollmentMap);

			//*********************END**********************************

			for(int month = 0; month < reportingMonth ; month++){
				// create <SHOPEmployerInfo> per month and ADD to <SHOPEmployer>
				Date endDate = EnrollmentUtils.getMonthEndDate(month,applicableYear);
				for(SHOPEmployerType shopEmployerType: shopEmployerTypeList){
					SHOPEmployerInfoType shopEmployerInfoType=shopEmployerType.getSHOPEmployerInfo().get(0);
					if(isEnrollmentActive(shopEmployerInfoType.getEmployerQHPCoverageStartDt().toGregorianCalendar().getTime(), 
							shopEmployerInfoType.getEmployerQHPCoverageTermDt().toGregorianCalendar().getTime(), endDate)){
						shopNewEmployerInfoType = new SHOPEmployerInfoType();
						shopNewEmployerInfoType.setApplicableCoverageMonthNum(formatForTwoDigit(month+1));
						shopNewEmployerInfoType.setBusinessAddressGrp(getBusinessAddressGroup(shopEmployerInfoType.getBusinessAddressGrp()));

						BusinessNameType businessNameType = shopEmployerInfoType.getBusinessName();
						businessNameType.setBusinessNameLine1(removeSpecialCharShop(businessNameType.getBusinessNameLine1()));
						businessNameType.setBusinessNameLine2(removeSpecialCharShop(businessNameType.getBusinessNameLine2()));
						shopNewEmployerInfoType.setBusinessName(businessNameType);

						shopNewEmployerInfoType.setEmployerQHPCoverageStartDt(shopEmployerInfoType.getEmployerQHPCoverageStartDt());
						shopNewEmployerInfoType.setEmployerQHPCoverageTermDt(shopEmployerInfoType.getEmployerQHPCoverageTermDt());

						//ShopEmployee Schema Change
						setShopEmployeeList(shopNewEmployerInfoType.getSHOPEmployee(), employeeIdList,
								employeeEmployeeEnrollmentMap, shopEmployerTypeNew, endDate,
								enrlEligibleEmployeeIRSDTOList, skippedEnrollment, month, applicableYear);

						shopEmployerInfoTypeList.add(shopNewEmployerInfoType);
					}
				}
			}
		}
		return shopEmployerTypeNew;
	}
	
	/**
	 * Schema Change
	 * Set list of employee for the current month
	 * @param shopEmployee
	 * @param employeeIdList
	 * @param employeeEmployeeEnrollmentMap
	 * @param shopEmployerTypeNew
	 * @param endDate
	 * @param enrlEligibleEmployeeIRSDTOList
	 * @param skippedEnrollment
	 * @param month
	 * @param applicableYear
	 * @throws GIException
	 */
	private void setShopEmployeeList(List<SHOPEmployeeType> shopEmployee, List<Integer> employeeIdList,
			Map<Integer, List<Enrollment>> employeeEmployeeEnrollmentMap, SHOPEmployerType shopEmployerTypeNew,
			Date endDate, List<EnrlEligibleEmployeeIRSDTO> enrlEligibleEmployeeIRSDTOList,
			Map<String, String> skippedEnrollment, int month, int applicableYear) throws GIException {

		for(Integer employeeId: employeeIdList){
			List<Enrollment> empEnrollmentList=employeeEmployeeEnrollmentMap.get(employeeId);
			if(empEnrollmentList!=null && !empEnrollmentList.isEmpty()){
				List<Integer> empEnrlIdList = shopEmployerTypeNew.getEmployerEnrollmentIdList();
				//							Date endDate = EnrollmentUtils.getMonthEndDate(month,applicableYear);
				boolean monthEnrollmentFound=false;
				for(Enrollment enrollment : empEnrollmentList){
					if(!empEnrlIdList.contains(enrollment.getEmployerEnrollmentId())){
						empEnrlIdList.add(enrollment.getEmployerEnrollmentId());
					}
					if(isEnrollmentActive(enrollment.getBenefitEffectiveDate(), enrollment.getBenefitEndDate(), endDate)){
						//Get Enrollment Aud record
						monthEnrollmentFound=true;
							Pageable pageable = new PageRequest(0, 1);
							List<EnrollmentAud> enrollmentAudList = enrollmentAudRepository.getMaxEnrollmentAudForEnrollment(enrollment.getId(), DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), pageable);
						if(enrollmentAudList != null && enrollmentAudList.size() == 0){
							enrollmentAudList = enrollmentAudRepository.getMaxEnrollmentAudForEnrollment(enrollment.getId(), pageable);
						}
						SHOPEmployeeType shopEmployeeType = new SHOPEmployeeType();

						try{
							EnrollmentAud enrollmentAud = enrollmentAudList.get(0);
							List<EnrolleeAud> EnrolleeAudList = getEnrolleeAudList(enrollmentAud.getId(), enrollmentAud.getRev(),endDate);
							Date startDate = EnrollmentUtils.getMonthStartDate(endDate);

							Map<String,Float> amountMap = retroAmountCalculationsShop(EnrolleeAudList, endDate , startDate);
							EnrolleeAud subscriberEnrolleeAud= getSubscriberEnrolleeAud(EnrolleeAudList);
							shopEmployeeType.setCompletePersonName(getCompletePersonNameType(subscriberEnrolleeAud));

							shopEmployeeType.setEmployeeSHOPPolicy(getEmployeeSHOPPolicyType(enrollmentAud,endDate,amountMap.get("EmployeeQHPShareOfPremiumAmt")));

							shopEmployeeType.setEmployerQHPPremiumContriAmt(getBigDecimalFromFloat(amountMap.get("EmployerQHPPremiumContriAmt")));

							shopEmployeeType.setSSN(removeNonDigitCharShop(subscriberEnrolleeAud.getTaxIdNumber()));

							shopEmployee.add(shopEmployeeType);
						}catch(Exception e){
							LOGGER.info("Exception While Processing Enrollment ID :: " + enrollment.getId(), e);
							skippedEnrollment.put("Enrollment_ID : "+enrollment.getId(), shortenedStackTrace(e, EnrollmentConstants.THREE));
							throw new GIException("Exception While Processing Enrollment ID :: " + enrollment.getId(), e);
						}
						break;
					}

				}
				if(!monthEnrollmentFound){
					for(Enrollment enrollment : empEnrollmentList){
						Map<Integer,List<EnrlEligibleEmployeeIRSDTO>> employeeEligibilityMap = getEmployeeEligibilityFromShop(Arrays.asList(enrollment.getEmployerEnrollmentId()),month, applicableYear);
						if(employeeEligibilityMap != null  && employeeEligibilityMap.get(enrollment.getEmployerEnrollmentId()) != null ){

							List<EnrlEligibleEmployeeIRSDTO> monthwiseEligibleList = employeeEligibilityMap.get(enrollment.getEmployerEnrollmentId());

							for (EnrlEligibleEmployeeIRSDTO enrlEligibleEmployeeIRSDTO : monthwiseEligibleList) {
								if(enrlEligibleEmployeeIRSDTO.getEmployeeAppId().equals(enrollment.getEmployeeAppId())){
									SHOPEmployeeType shopEmployeeType = new SHOPEmployeeType();
									//												List<EmployeeCoverageType> employeeEligbleCoverageList = shopEmployeeType.getEmployeeCoverage();
									setShopEmployeeType(enrlEligibleEmployeeIRSDTO, shopEmployeeType, skippedEnrollment, month);
									shopEmployee.add(shopEmployeeType);
								}
							}
							break;
						}
					}

				}
			}
		}

		List<String> ssnMonthList= new ArrayList<>();
		if (enrlEligibleEmployeeIRSDTOList != null && !enrlEligibleEmployeeIRSDTOList.isEmpty()) {
			for (EnrlEligibleEmployeeIRSDTO enrlEligibleEmployeeIRSDTO : enrlEligibleEmployeeIRSDTOList) {
				Integer employeeId=enrollmentRepository.getEmployerIdByEmployeeApplicationId(enrlEligibleEmployeeIRSDTO.getEmployeeAppId());
				if (employeeId==null ||!employeeIdList.contains(employeeId)) {
					if(!ssnMonthList.contains(enrlEligibleEmployeeIRSDTO.getSSN()+month)){
						SHOPEmployeeType shopEligibleEmployeeType  = new SHOPEmployeeType();
						setShopEmployeeType(enrlEligibleEmployeeIRSDTO, shopEligibleEmployeeType, skippedEnrollment, month);
						ssnMonthList.add(enrlEligibleEmployeeIRSDTO.getSSN()+month);
						shopEmployee.add(shopEligibleEmployeeType);
					}
				}
			}
		}

		//*******************Employee Section*********************************

	}

	private void setShopEmployeeType(EnrlEligibleEmployeeIRSDTO enrlEligibleEmployeeIRSDTO,	SHOPEmployeeType shopEmployeeType, Map<String, String> skippedEnrollment, int month) throws GIException {
		try{
			shopEmployeeType.setCompletePersonName(getCompletePersonNameType(enrlEligibleEmployeeIRSDTO));
			shopEmployeeType.setSSN(enrlEligibleEmployeeIRSDTO.getSSN());
			shopEmployeeType.setEmployerQHPPremiumContriAmt(getBigDecimalFromFloat(new Float(0)));
		}catch(Exception e){
			LOGGER.info("Exception While Processing Enrollment EligibleEmployee EmployeeApp ID :: " + enrlEligibleEmployeeIRSDTO.getEmployeeAppId(), e);
			skippedEnrollment.put("EmployeeApplication_ID : "+enrlEligibleEmployeeIRSDTO.getEmployeeAppId(), shortenedStackTrace(e, EnrollmentConstants.THREE));
			throw new GIException("Exception While Processing Enrollment EligibleEmployee EmployeeApp ID :: " + enrlEligibleEmployeeIRSDTO.getEmployeeAppId(), e);
		}
	}

	private BusinessAddressGrpType getBusinessAddressGroup(BusinessAddressGrpType businessAddressGrp) {
		BusinessAddressGrpType updatedBusinessAddressGrp = businessAddressGrp;
		if(null != updatedBusinessAddressGrp && null != updatedBusinessAddressGrp.getUSAddressGrp()){
			USAddressGrpType uSAddressGrpType  = updatedBusinessAddressGrp.getUSAddressGrp();
			uSAddressGrpType.setAddressLine1Txt(removePeriodAndExtraSpaces(uSAddressGrpType.getAddressLine1Txt()));
			uSAddressGrpType.setAddressLine2Txt(removePeriodAndExtraSpaces(uSAddressGrpType.getAddressLine2Txt()));
			uSAddressGrpType.setCityNm(removePeriodAndExtraSpaces(uSAddressGrpType.getCityNm()));
			updatedBusinessAddressGrp.setUSAddressGrp(uSAddressGrpType);
		}
		return updatedBusinessAddressGrp;
	}
	
	private boolean isEnrollmentActive(Date benefitEffectiveDate,Date benefitEndDate, Date endDate) throws GIException {
		Calendar benifit_start = EnrollmentUtils.dateToCalendar(EnrollmentUtils.removeTimeFromDate(benefitEffectiveDate));
		Calendar benifit_end = EnrollmentUtils.dateToCalendar(EnrollmentUtils.removeTimeFromDate(benefitEndDate));
		benifit_end.add(GregorianCalendar.DATE, 1);
		Calendar month_End = EnrollmentUtils.dateToCalendar(EnrollmentUtils.removeTimeFromDate(endDate));
		
		return ( benifit_start.before(month_End) && month_End.before(benifit_end) );
		
	}

/*	private EmployeeCoverageType getEmployeeCoverageType(EnrlEligibleEmployeeIRSDTO enrlEligibleEmployeeIRSDTO , Map<String, String> skippedEnrollment , int month) throws GIException {
		EmployeeCoverageType employeeCoverageType = new EmployeeCoverageType();
		employeeCoverageType.setApplicableCoverageMonthNum(formatForTwoDigit(month+1));
		
		try{
			employeeCoverageType.setCompletePersonName(getCompletePersonNameType(enrlEligibleEmployeeIRSDTO));
			employeeCoverageType.setSSN(enrlEligibleEmployeeIRSDTO.getSSN());
			employeeCoverageType.setEmployerQHPPremiumContriAmt(getBigDecimalFromFloat(new Float(0)));

			if(employeeCoverageType.getCompletePersonName() != null && 
					(employeeCoverageType.getCompletePersonName().getPersonFirstName() != null 
					&& employeeCoverageType.getCompletePersonName().getPersonLastName() != null )){
				
				return employeeCoverageType;
			}
			
		}catch(Exception e){
			LOGGER.info("Exception While Processing Enrollment EligibleEmployee EmployeeApp ID :: " + enrlEligibleEmployeeIRSDTO.getEmployeeAppId(), e);
			skippedEnrollment.put("EmployeeApplication_ID : "+enrlEligibleEmployeeIRSDTO.getEmployeeAppId(), shortenedStackTrace(e, EnrollmentConstants.THREE));
			throw new GIException("Exception While Processing Enrollment EligibleEmployee EmployeeApp ID :: " + enrlEligibleEmployeeIRSDTO.getEmployeeAppId(), e);
		}
		
		return null;
		
	}*/

	private List<Long> getEmployeeAppIdList(List<Enrollment> enrollmentList) {
		List<Long> employeeAppIdList = new ArrayList<>();
		for (Enrollment enrollment : enrollmentList) {
			employeeAppIdList.add(enrollment.getEmployeeAppId());
		}
		return employeeAppIdList;
	}

	private void setEmployeeAndEmployeeAppId(List<Integer> employeeIdList,List<Long> employeeAppIdList, List<Enrollment> enrollmentList,Map<Integer,List<Enrollment>> employeeIdAndEnrollmentMap){
		for (Enrollment enrollment : enrollmentList) {
			if(!employeeAppIdList.contains(enrollment.getEmployeeAppId())){
				employeeAppIdList.add(enrollment.getEmployeeAppId());
			}
			if(!employeeIdList.contains(enrollment.getEmployeeId())){
				employeeIdList.add(enrollment.getEmployeeId());
			}
			List<Enrollment> empEnrollmentList=employeeIdAndEnrollmentMap.get(enrollment.getEmployeeId());
			if(empEnrollmentList==null){
				empEnrollmentList= new ArrayList<>();
			}
			empEnrollmentList.add(enrollment);
			employeeIdAndEnrollmentMap.put(enrollment.getEmployeeId(), empEnrollmentList);
		}
	}
	private Map<String, Float> retroAmountCalculationsShop(
			List<EnrolleeAud> enrolleeAuditList, Date endDate, Date startDate) {
        Float total_employee_PremiumAmount = 0f;
        Float total_employer_PremiumAmount = 0f;
        
        for(EnrolleeAud enrollee : enrolleeAuditList ){
        	
               Date coverageStartDate = DateUtils.truncate(enrollee.getEffectiveStartDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
               Date coverageEndDate = DateUtils.truncate(enrollee.getEffectiveEndDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
               Float totEmpResponsibilityAmt = Float.valueOf(0);
               
               if(enrollee.getTotEmpResponsibilityAmt() != null){
            	   totEmpResponsibilityAmt = enrollee.getTotEmpResponsibilityAmt();
               }
               
               Float employee_contribution = enrollee.getTotalIndvResponsibilityAmt() - totEmpResponsibilityAmt;
               Float employer_contribution = (float)totEmpResponsibilityAmt;
               Float retro_employee_Premium = 0f;
               Float retro_employer_Premium = 0f;
               
               if(coverageStartDate != null && coverageEndDate !=null ){
                     boolean isCoverageAfterMonthStart = coverageStartDate.after(startDate);
                     boolean isCoverageTermBeforeMonthEnd = coverageEndDate.before(endDate);
                     
                     int daysElapsed = EnrollmentUtils.daysInMonth(startDate) ;
                     
                     if(isCoverageAfterMonthStart && isCoverageTermBeforeMonthEnd){
                            // Calculate number of days between coverage start date and coverage end date
                            daysElapsed = EnrollmentUtils.getDaysBetween(coverageStartDate, coverageEndDate) + 1;
                     
                     }else if(isCoverageAfterMonthStart){
                            // Calculate number of days between coverage start date and month end date
                            daysElapsed = EnrollmentUtils.getDaysBetween(coverageStartDate, endDate) + 1;
                     }else if(isCoverageTermBeforeMonthEnd){
                            // Calculate number of days between month start date and coverage term date
                            daysElapsed = EnrollmentUtils.getDaysBetween(startDate, coverageEndDate) + 1;
                     }
                     retro_employee_Premium = employee_contribution / EnrollmentUtils.daysInMonth(startDate) * daysElapsed;
                     retro_employer_Premium = employer_contribution / EnrollmentUtils.daysInMonth(startDate) * daysElapsed;
               }
               total_employee_PremiumAmount += retro_employee_Premium;
               total_employer_PremiumAmount += retro_employer_Premium;
        }
        Map<String, Float> amountMap = new HashMap<String, Float>();
        amountMap.put("EmployeeQHPShareOfPremiumAmt", total_employee_PremiumAmount);
        amountMap.put("EmployerQHPPremiumContriAmt", total_employer_PremiumAmount);
        
        return amountMap;
	}

	private void setEmpEnrollmentIdNull(List<SHOPEmployerType> shopEmployerList) {
		for (SHOPEmployerType shopEmployerType : shopEmployerList) {
			shopEmployerType.setEmployerEnrollmentId(null);
			shopEmployerType.getEmployerEnrollmentIdList().clear();
		}
	}
	
	@Override
	public void sendShopEmployerListForReportProcessing(List<SHOPEmployerType> shopEmployerList,int month, int year , int i) throws GIException{

		if (EnrollmentUtils.isNotNullAndEmpty(shopEmployerList)) {
				
				HealthExchangeType healthExchangeTypeObj = new HealthExchangeType();	
			    Set<String> employerEnrollmentIdSet = new HashSet<String>();
				StringBuilder employerEnrollmentId = new StringBuilder();
				String generatedFileName = null;
				
				for(SHOPEmployerType shopEmployerType : shopEmployerList){
					if(shopEmployerType.getEmployerEnrollmentIdList() != null && !shopEmployerType.getEmployerEnrollmentIdList().isEmpty()){
						for(Integer id: shopEmployerType.getEmployerEnrollmentIdList()){
							employerEnrollmentIdSet.add(id.toString());
						}
					}
				}
				
				for(String id : employerEnrollmentIdSet){
					employerEnrollmentId.append(id);
					employerEnrollmentId.append(",");
				}
				
				setEmpEnrollmentIdNull(shopEmployerList);
				
				healthExchangeTypeObj.setSubmissionYr(getSubmissionYear(year));
				healthExchangeTypeObj.setSubmissionMonthNum(formatForTwoDigit(month + 1));
				healthExchangeTypeObj.setApplicableCoverageYr(getSubmissionYear((month == 0)? year-1 : year));
				healthExchangeTypeObj.setSHOPExchange(getShopExchangeType(shopEmployerList));

				generatedFileName = generateIrsXml(healthExchangeTypeObj, i);

				logIRSOutBoundTransmissionInformation(employerEnrollmentId.toString(),generatedFileName,"S",month,year);

			}
	}

	private SHOPExchangeType getShopExchangeType(List<SHOPEmployerType> subList) {
		SHOPExchangeType shopExchangeTypeObj = new SHOPExchangeType();

		// Confirm HealthExchangeId 
		shopExchangeTypeObj.setHealthExchangeId(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_HEALTH_EXCHANGE_ID));
		List<SHOPEmployerType> shopEmployerTypeList = shopExchangeTypeObj.getSHOPEmployer();
		shopEmployerTypeList.addAll(subList);
		return shopExchangeTypeObj;
	}

	private List<EnrolleeAud> getEnrolleeAudList(Integer id, int rev, Date endDate) {
		List<Integer> remainingEnrolleeIdList = null;
		List<Integer> enrolleeIDfromAud = new ArrayList<Integer>();

		List<EnrolleeAud> enrolleeAuditList = enrolleeAudRepository.getEnrolleesByEnrollmentIdAndRevisionNoIrsShop(id, rev, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));

		if(enrolleeAuditList != null && !enrolleeAuditList.isEmpty()){
			for(EnrolleeAud enrolleeAud : enrolleeAuditList){
				enrolleeIDfromAud.add(enrolleeAud.getId());
			}

			remainingEnrolleeIdList = enrolleeRepository.getRemainingEnrolleesForIRSShop(id, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), enrolleeIDfromAud);
		}else{
			remainingEnrolleeIdList = enrolleeRepository.getRemainingEnrolleesForIRSShop(id, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		}


		if(remainingEnrolleeIdList != null && !remainingEnrolleeIdList.isEmpty()){
			for(Integer enrolleeId : remainingEnrolleeIdList){
				EnrolleeAud enrolleeAudRecord= enrolleeAudRepository.getMaxEnrolleeAudPerMonth(enrolleeId, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
				if(enrolleeAudRecord!=null){
					enrolleeAuditList.add(enrolleeAudRecord);
				}
			}
		}

		return enrolleeAuditList;
	}

	private EnrolleeAud getSubscriberEnrolleeAud(List<EnrolleeAud> enrolleeAuditList) {
		EnrolleeAud subscriberAud =  null;
		if(enrolleeAuditList != null && !enrolleeAuditList.isEmpty()){
			for(EnrolleeAud enrolleeAud : enrolleeAuditList){
				if(enrolleeAud.getPersonTypeLkp().getLookupValueCode() != null  && enrolleeAud.getPersonTypeLkp().getLookupValueCode().equals("SUBSCRIBER")){
					subscriberAud = enrolleeAud;
					break;
				}
			}
		}

		return subscriberAud;
	}

	private EmployeeSHOPPolicyType getEmployeeSHOPPolicyType(EnrollmentAud enrollmentAud, Date endDate,Float emplyee_contribution) {
		EmployeeSHOPPolicyType employeeSHOPPolicyType = new EmployeeSHOPPolicyType();
		employeeSHOPPolicyType.setEmployeeQHPShareOfPremiumAmt(getBigDecimalFromFloat(emplyee_contribution));
		
		employeeSHOPPolicyType.setPolicyCoverageEndDt(getXmlGregorianCalendarDate(enrollmentAud.getBenefitEndDate()));
		employeeSHOPPolicyType.setPolicyCoverageStartDt(getXmlGregorianCalendarDate(enrollmentAud.getBenefitEffectiveDate()));

		Long enrolleeCount = enrolleeRepository.totalActiveMembersByEnrollmentIDForIRS(enrollmentAud.getId());
		if(enrolleeCount.intValue() > 1){
			employeeSHOPPolicyType.setQHPFamilyOrIndivCoverageCd("Family");
		}else{

			employeeSHOPPolicyType.setQHPFamilyOrIndivCoverageCd("Individual");
		}
		List<EnrolleeAud> enrolleeAudList=getEnrolleeAudList(enrollmentAud.getId(), enrollmentAud.getRev(),endDate);

		setSHOPExchangePersonType(enrolleeAudList, employeeSHOPPolicyType);
		return employeeSHOPPolicyType;
	}

	private SHOPExchangePersonType getSHOPExchangePersonType(EnrolleeAud enrolleeAud) {
		SHOPExchangePersonType shopExchangePersonType  = new SHOPExchangePersonType();

		shopExchangePersonType.setCompletePersonName(getCompletePersonNameType(enrolleeAud));
		shopExchangePersonType.setSSN(removeNonDigitCharShop(enrolleeAud.getTaxIdNumber()));

		return shopExchangePersonType;
	}

	private void setSHOPExchangePersonType(List<EnrolleeAud> enrolleeAudList,EmployeeSHOPPolicyType employeeSHOPPolicyType) {
		EnrolleeAud subscriberEnrolleeAud = getSubscriberEnrolleeAud(enrolleeAudList);
		
		List<SHOPExchangePersonType> shopPersonList= new ArrayList<>();
		boolean spouseFound=false;
		for (EnrolleeAud enrolleeAud : enrolleeAudList) {
			if(subscriberEnrolleeAud.getId()!=enrolleeAud.getId()){
				if(!spouseFound  ){
					Integer relRevisionNumber = enrolleeRelationshipRepository.findLastRelationRevision(enrolleeAud.getId(), subscriberEnrolleeAud.getId(), enrolleeAud.getRev());

					if(relRevisionNumber != null){
						EnrolleeRelationshipAud enrolleeRelationAud= enrolleeRelationshipAudRepository.findrelationShipBySourceTargetRev(enrolleeAud.getId(), subscriberEnrolleeAud.getId(), relRevisionNumber);
						if(enrolleeRelationAud != null &&
							enrolleeRelationAud.getRelationshipLkp().getLookupValueCode().equalsIgnoreCase("01")){
							employeeSHOPPolicyType.setSHOPSpouse(getSHOPExchangePersonType(enrolleeAud));// spouse
								 spouseFound=true;
							}else{
								shopPersonList.add(getSHOPExchangePersonType(enrolleeAud));
							}
					}
				}else{
					shopPersonList.add(getSHOPExchangePersonType(enrolleeAud));
				}
			}
			
		}
		if(shopPersonList!=null && shopPersonList.size()>0){
			List<SHOPExchangePersonType> shopExchangePersonList = employeeSHOPPolicyType.getSHOPDependent();
			shopExchangePersonList.addAll(shopPersonList);
		}
	}
	
	private SHOPExchangePersonType getSHOPExchangePersonTypeSpouse(List<EnrolleeAud> enrolleeAudList) {
		EnrolleeAud subscriberEnrolleeAud = getSubscriberEnrolleeAud(enrolleeAudList);

		for (EnrolleeAud enrolleeAud : enrolleeAudList) {
			Integer relRevisionNumber = enrolleeRelationshipRepository.findLastRelationRevision(enrolleeAud.getId(), subscriberEnrolleeAud.getId(), enrolleeAud.getRev());

			if(relRevisionNumber != null){
				EnrolleeRelationshipAud enrolleeRelationAud= enrolleeRelationshipAudRepository.findrelationShipBySourceTargetRev(enrolleeAud.getId(), subscriberEnrolleeAud.getId(), relRevisionNumber);
				if(enrolleeRelationAud != null &&
					enrolleeRelationAud.getRelationshipLkp().getLookupValueCode().equalsIgnoreCase("01")){
						return getSHOPExchangePersonType(enrolleeAud);
					}
			}
		}
		return null;
	}
	
	private CompletePersonNameType getCompletePersonNameType(EnrlEligibleEmployeeIRSDTO enrlEligibleEmployeeIRSDTO) {
		CompletePersonNameType completePersonNameType = new CompletePersonNameType();

		completePersonNameType.setPersonFirstName(enrlEligibleEmployeeIRSDTO.getFirstName());
		completePersonNameType.setPersonMiddleName(enrlEligibleEmployeeIRSDTO.getMiddleName());
		completePersonNameType.setPersonLastName(enrlEligibleEmployeeIRSDTO.getLastName());
		completePersonNameType.setSuffixName(enrlEligibleEmployeeIRSDTO.getSuffixName());

		return completePersonNameType;
	}

	private CompletePersonNameType getCompletePersonNameType(EnrolleeAud subscriberEnrollee) {
		CompletePersonNameType completePersonNameType = new CompletePersonNameType();

		completePersonNameType.setPersonFirstName(subscriberEnrollee.getFirstName());
		completePersonNameType.setPersonMiddleName(subscriberEnrollee.getMiddleName());
		completePersonNameType.setPersonLastName(subscriberEnrollee.getLastName());
		completePersonNameType.setSuffixName(subscriberEnrollee.getSuffix());

		return completePersonNameType;
	}

	@Override
	public SHOPExchangeType getEmployerInfoFromShop(List<Integer> employerEnrIdList) {
		SHOPExchangeType shopExchangeType = null;
		String shopExchangeTypeRequest = null;
		ShopResponse shopResponse = null;
		String shopRes = null;

		XStream xstream = GhixUtils.getXStreamStaxObject();
		shopExchangeTypeRequest = xstream.toXML(employerEnrIdList);

		LOGGER.info("Calling Shop service to get SHOPExchangeType data");
		shopRes = restTemplate.postForObject(ShopEndPoints.GET_EMPLOYER_INFO_IRS, shopExchangeTypeRequest, String.class); 
		LOGGER.info("shopExchangeTypeResponse : ");

		if(shopRes != null){
			shopResponse = (ShopResponse)xstream.fromXML(shopRes);
			if(shopResponse != null && !shopResponse.getStatus().equalsIgnoreCase("FAILURE")){
				Map<String, Object> responseData = shopResponse.getResponseData();
				shopExchangeType = (SHOPExchangeType) responseData.get(SHOPExchangeType.class.getName()); 
			}
		}
		return shopExchangeType;
	}

	@Override
	public List<Integer> getUniqueEmployerEnrIdList(int month, int year) throws GIException{
		Date currentMonthStartDate = null;
		Date yearStartDate = null;
		int intMonth = month;
		int intYear = year;
		try {
			currentMonthStartDate = EnrollmentUtils.getMonthStartDateTime(intMonth,intYear);
		} catch (GIException e) {
			throw new GIException("Error getting currentMonthStartDate", e);
		}
		//To handle boundary condition when month passed is January
		int applicableYear = intYear;
		if(intMonth == 0){
			applicableYear--;
		}
		yearStartDate = EnrollmentUtils.getCurrentYearStartDate(applicableYear);

		if(EnrollmentUtils.isNotNullAndEmpty(currentMonthStartDate) && EnrollmentUtils.isNotNullAndEmpty(yearStartDate)){
			return enrollmentRepository.getUniqueEmployerEnrollmentIDList(DateUtil.dateToString(yearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(currentMonthStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		}else{
			return null;
		}
	}

	@Override
	public List<Object[]> getEmployerIdEmployerEnrollmentIdMap(int month, int year) throws GIException{
		 
		Date currentMonthStartDate = null;
		Date yearStartDate = null;
		int intMonth = month;
		int intYear = year;
		try {
			currentMonthStartDate = EnrollmentUtils.getMonthStartDateTime(intMonth,intYear);
		} catch (GIException e) {
			throw new GIException("Error getting currentMonthStartDate", e);
		}
		//To handle boundary condition when month passed is January
		int applicableYear = intYear;
		if(intMonth == 0){
			applicableYear--;
		}
		yearStartDate = EnrollmentUtils.getCurrentYearStartDate(applicableYear);

		if(EnrollmentUtils.isNotNullAndEmpty(currentMonthStartDate) && EnrollmentUtils.isNotNullAndEmpty(yearStartDate)){
			return enrollmentRepository.getEmployerIdEmployerEnrollmentIdMapList(DateUtil.dateToString(yearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(currentMonthStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		}else{
			return null;
		}
	}
	
	@Override
	public List<Object[]> getEmployerIdEmployerEnrollmentIdMap(int month, int year, List<Integer> employerEnrollmentIds) throws GIException{
		 
		Date currentMonthStartDate = null;
		Date yearStartDate = null;
		int intMonth = month;
		int intYear = year;
		try {
			currentMonthStartDate = EnrollmentUtils.getMonthStartDateTime(intMonth,intYear);
		} catch (GIException e) {
			throw new GIException("Error getting currentMonthStartDate", e);
		}
		//To handle boundary condition when month passed is January
		int applicableYear = intYear;
		if(intMonth == 0){
			applicableYear--;
		}
		yearStartDate = EnrollmentUtils.getCurrentYearStartDate(applicableYear);

		if(EnrollmentUtils.isNotNullAndEmpty(currentMonthStartDate) && EnrollmentUtils.isNotNullAndEmpty(yearStartDate)){
			return enrollmentRepository.getEmployerIdEmployerEnrollmentIdMapList(DateUtil.dateToString(yearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(currentMonthStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), employerEnrollmentIds);
		}else{
			return null;
		}
	}
	
	@Override
	public void generateShopIrsReportbByListParam(List<Integer> employerEnrollmentIdlist,int month,int year) throws GIException {
		LOGGER.info("******* Inside generateShopIrsReportbByListParam ********/");
		Map<String, String> skippedEnrollment =  new HashMap<String,String>();
		String batchDate = getDateTime();
		EnrollmentUtils.cleanDirectory(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH));
		
		if(employerEnrollmentIdlist != null && !employerEnrollmentIdlist.isEmpty()){
			// CALL SHOP API
			SHOPExchangeType shopExchangetype = getEmployerInfoFromShop(employerEnrollmentIdlist);
			List<Integer> employerIdList= new ArrayList<>();
			Map<Integer, List<Integer>> employerAndEmployerEnrlIdsMap= new HashMap<>();
			List<Object[]> employerAndEmployerEnrlIdMapList= getEmployerIdEmployerEnrollmentIdMap(month, year, employerEnrollmentIdlist);
			formEmployerAndEmployerEnrlIdsMap(employerEnrollmentIdlist,employerIdList, employerAndEmployerEnrlIdsMap,employerAndEmployerEnrlIdMapList );
			// Call Shop Api for Employee eligibility data
			Map<Integer,List<EnrlEligibleEmployeeIRSDTO>> employeeEligibilityMap = getEmployeeEligibilityFromShop(employerEnrollmentIdlist,month, year);
			
			int maxHouseEmployerPerFile = 1;
			if(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSXML_MAXHOUSEHOLD_LIMIT) != null){
				maxHouseEmployerPerFile = Integer.valueOf(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSXML_MAXHOUSEHOLD_LIMIT));
			}
			LOGGER.info("GenerateShopIrsReport maxHouseEmployerPerFile "+ maxHouseEmployerPerFile);
			
			if(shopExchangetype != null && shopExchangetype.getSHOPEmployer() != null){

				List<SHOPEmployerType> shopEmployerList = shopExchangetype.getSHOPEmployer();
				//List<SHOPEmployerType> subList = null;
				List<Integer> subList = null;
				int size = employerIdList.size();
				int numberOfFiles = size / maxHouseEmployerPerFile;
				if (size % maxHouseEmployerPerFile != 0) {
					numberOfFiles++;
				}
				int firstIndex = 0;
				int lastIndex = 0;
				for (int fileNumber = 0; fileNumber < numberOfFiles; fileNumber++) {
					firstIndex = fileNumber * maxHouseEmployerPerFile;
					lastIndex = (fileNumber + 1) * maxHouseEmployerPerFile;
					if (lastIndex > size) {
						lastIndex = size;
					}
					
				//subList = shopEmployerList.subList(firstIndex, lastIndex);
				subList= employerIdList.subList(firstIndex, lastIndex);
				if(subList != null && !subList.isEmpty()){
					LOGGER.info("GenerateShopIrsReport processing for "+ subList.toString());
					
					try{
						//mounthWiseShopEmployerList(subList,employeeEligibilityMap,month, year,fileNumber,skippedEnrollment);
						mounthWiseShopEmployerList(shopEmployerList,employeeEligibilityMap,month, year,fileNumber,skippedEnrollment, subList, employerAndEmployerEnrlIdsMap);
					}catch(Exception e){
						LOGGER.info("Exception While Processing Sub_List :: " + subList.toString(), e);
						skippedEnrollment.put("EmployerEnrollmentIds: "+subList.toString(),shortenedStackTrace(e, EnrollmentConstants.THREE));
					}
					
				}else{
					LOGGER.info("GenerateShopIrsReport subList is empty or null ");
				}
				
				}
				//Write Error log File		
				logSkippedHouseholds(skippedEnrollment , batchDate);
				
			}else{
				throw new GIException("No shopExchangetype Records found @ generateShopIrsReport GET_EMPLOYER_INFO_IRS API return Null ");
			}
		}
	}

	/**
	 * 
	 * Returns Current Year
	 * 
	 * @author Aditya-S
	 * @since 30-07-2014
	 * 
	 * @return
	 */
	private XMLGregorianCalendar getSubmissionYear(int year){
		
		//Calendar calender = TSCalendar.getInstance();
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(GregorianCalendar.YEAR, year);
		XMLGregorianCalendar xmlGregorianCalendar = null;
		try {
			xmlGregorianCalendar = DatatypeFactory.newInstance()
					.newXMLGregorianCalendarDate(year,
							DatatypeConstants.FIELD_UNDEFINED,
							DatatypeConstants.FIELD_UNDEFINED,
							DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException dex) {
			LOGGER.error("DatatypeConfigurationException @ getSubmissionYear", dex);
		}
		return xmlGregorianCalendar;
	}

	/**
	 * Convert normal date to XMLGregorianCalendar date
	 * @param date
	 * @return XMLGregorianCalendar 
	 */
	private XMLGregorianCalendar getXmlGregorianCalendarDate(
			Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		XMLGregorianCalendar xmlGregorianDate = null;
		cal.setTime(date);
		try {
			xmlGregorianDate = DatatypeFactory.newInstance()
					.newXMLGregorianCalendarDate(cal.get(Calendar.YEAR),
							cal.get(Calendar.MONTH) + 1,
							cal.get(Calendar.DAY_OF_MONTH),
							DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			LOGGER.error("Error converting to XMLGregorianCalendar Date", e);
		}
		return xmlGregorianDate;
	}
	


	/**
	 * Converts float to BigDecimal
	 * @param amount
	 * @return
	 */
	private BigDecimal getBigDecimalFromFloat(Float amount) {

		BigDecimal bigDecimalAmount = null;
		if(amount !=null){
			try{
				bigDecimalAmount = new BigDecimal(Float.toString(amount)).setScale(2, BigDecimal.ROUND_HALF_UP);
			}catch(Exception e){
				LOGGER.error("Error parsing float amount" , e);
			}
		}
		return bigDecimalAmount;
	}

	/**
	 * Generates XML from {@link HealthExchangeType} Object Provided
	 * 
	 * @author Aditya-S
	 * @since 30-07-2014
	 * @param healthExchangeType
	 * @param fileNumber 
	 * @return 
	 * @throws GIException 
	 */
	private String generateIrsXml(HealthExchangeType healthExchangeType, int fileNumber) throws GIException{
		ObjectFactory objectFactory = new ObjectFactory();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmssSSS'Z'");
		String fileName = null;
		try {
			fileName = "EOM_Request_"+String.format("%05d", fileNumber+1)+"_"+dateFormat.format(new TSDate())+".xml";
			StringBuilder irsFileNameBuilder = new StringBuilder();
			irsFileNameBuilder.append(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH));
			irsFileNameBuilder.append("/");
			irsFileNameBuilder.append(fileName);

			JAXBContext jaxbContext = JAXBContext.newInstance(HealthExchangeType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
            jaxbMarshaller.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new EnrollmentIrsEscapeHandler());

			JAXBElement<HealthExchangeType> je =  objectFactory.createHealthExchange(healthExchangeType);
			jaxbMarshaller.marshal(je, new File(irsFileNameBuilder.toString()));

		} catch (JAXBException e) {
			LOGGER.error("EnrollmentIrsReportServiceImpl @ generateIrsXml  ", e);
			throw new GIException("Error generating XML @ generateIrsXml", e);
		}
		return fileName;
	}

	/**
	 * Sets the IRS file details into the IRS Outbound Transmission table
	 * @param householdIds
	 * @param generatedFileName
	 */
	private void logIRSOutBoundTransmissionInformation(String householdIds, String generatedFileName,String submissionType,int submissionMonth,int submissionYear) {
		
		IRSOutboundTransmission irsOutBoundTransmission = new IRSOutboundTransmission();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		irsOutBoundTransmission.setDocumentFileName(generatedFileName);
		irsOutBoundTransmission.setHouseholdCaseID(householdIds);
		irsOutBoundTransmission.setSubmissionType(submissionType);
		irsOutBoundTransmission.setMonth(""+(submissionMonth+1));
		irsOutBoundTransmission.setYear(""+submissionYear);
		irsOutBoundTransmission.setBatchId(sdf.format(new TSDate()));

		try{
			irsOutboundTransmissionRepo.save(irsOutBoundTransmission);
		}catch(Exception e){
			LOGGER.error("Error saving data in IRSOutBoundTransmission table @ logIRSOutBoundTransmissionInformation" , e);
		}
	}

	/**
	 * Function to check the Individual Portal API
	 */
	@Override
	public void testPortalApi() {
		List<String> houseHoldIdList = null;
		List<String> success = new ArrayList<String>();
		List<String> failure = new ArrayList<String>();
		try {
			houseHoldIdList = getUniqueHouseHolds(1, 2014);
			if (EnrollmentUtils.isNotNullAndEmpty(houseHoldIdList)) {
				for (String householdId : houseHoldIdList) {
					// Calling Indiv Portal API
					try {
//						getHouseInfoFromIndivPortal(householdId, 0, 2014);
						success.add(householdId);
					} catch (Exception e) {
						failure.add(householdId);
						LOGGER.debug("Error in Portal ", e);
					}
				}
			}
		} catch (GIException e1) {
			LOGGER.error("Error in Portal ", e1);
		}
		LOGGER.info("Successful :: " + success);
		LOGGER.info("Failed :: " + failure);
	}
	
	/**
	 * @author negi_s
	 * @since 19-01-2015
	 * 
	 * Returns unique household ids for current year from 1st Jan to Previous month Last Date.
	 * 
	 * @return List<String> unique household ids
	 */
	@Override
	public List<String> getUniqueHouseHolds(int month, int year) throws GIException{
		Date currentMonthStartDate = null;
		List<String> houseHoldIdList = null; 
		Date yearStartDate = null;
		int intMonth = month;
		int intYear = year;
		try {
			currentMonthStartDate = EnrollmentUtils.getMonthStartDateTime(intMonth,intYear);
		} catch (GIException e) {
			throw new GIException("Error getting currentMonthStartDate", e);
		}
		//To handle boundary condition when month passed is January
		int applicableYear = intYear;
		if(intMonth == 0){
			applicableYear--;
		}
		yearStartDate = EnrollmentUtils.getCurrentYearStartDate(applicableYear);

	/*	List<String> confirmedHouseHoldIdList = null;
		List<String> terminatedHouseHoldIdList = null;
		Set<String> uniqueSetHousholds = new HashSet<String>();
		if(EnrollmentUtils.isNotNullAndEmpty(currentMonthStartDate) && EnrollmentUtils.isNotNullAndEmpty(yearStartDate)){
			confirmedHouseHoldIdList = enrollmentRepository.getUniqueConfirmedHouseHoldIDList(yearStartDate, currentMonthStartDate);
			terminatedHouseHoldIdList = enrollmentAudRepository.getUniqueTerminatedHouseHoldIDList(yearStartDate, currentMonthStartDate);
			//			houseHoldIdList =  enrollmentRepository.getUniqueHouseHoldIDList(yearStartDate, currentMonthStartDate);
		}
		if(null != confirmedHouseHoldIdList && !confirmedHouseHoldIdList.isEmpty()){
			uniqueSetHousholds.addAll(confirmedHouseHoldIdList);
		}
		if(null != terminatedHouseHoldIdList && !terminatedHouseHoldIdList.isEmpty()){
			uniqueSetHousholds.addAll(terminatedHouseHoldIdList);
		}*/
		if(EnrollmentUtils.isNotNullAndEmpty(currentMonthStartDate) && EnrollmentUtils.isNotNullAndEmpty(yearStartDate)){
			houseHoldIdList = enrollmentRepository.getUniqueHouseHoldIDList(DateUtil.dateToString(yearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(currentMonthStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		}
		return houseHoldIdList;
	}
	
	/**
	 * Log the skipped household IDs
	 * 
	 * @author negi_s
	 * @since 02-04-2015
	 * @param skippedHousholds
	 * @param batchDate
	 */
	@Override
	public void logSkippedHouseholds(Map<String, String> skippedHousholds, String batchDate) {
		if(null !=skippedHousholds && !skippedHousholds.isEmpty()){
			Writer writer = null;
//			String irsFailureFolderPath = "C:/Data/IRSFailureFolderPath";// + File.separator + getDateTime() ;
			String irsFailureFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_FAILURE_FOLDER_PATH);
			try{
				if(null != irsFailureFolderPath && new File(irsFailureFolderPath).exists()){
					String timeStampFailureFolderPath = irsFailureFolderPath + File.separator + batchDate;
					String skipListFilePath = timeStampFailureFolderPath+File.separator + "SkippedHousehold.txt";
					if(StringUtils.isNotEmpty(timeStampFailureFolderPath))
					{
						if(! new File(timeStampFailureFolderPath).exists()){
							new File(timeStampFailureFolderPath).mkdirs();
						}
					}
					/**/
					writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(skipListFilePath), "utf-8"));
					writer.write(skippedHousholds.toString());
					// Email Trigger to email group
					enrollmentEmailNotificationService.sendIRSOutboundFailureNotification("SkippedHousehold.txt", skipListFilePath, SKIPPED_HOUSEHOLD_ACTION_MSG);
				}
			}catch(Exception e){
				LOGGER.debug("Exception in creating SkippedHousehold log :: " + e.getMessage(), e);
			}finally{
				if(null != writer){
					try {
						writer.close();
					} catch (IOException e) {
						LOGGER.debug("Exception closing writer :: " + e.getMessage(), e);
					}
				}
			}
		}
	}
	
	/**
	 * Get date and time suffix
	 * 
	 * @since 02-04-2015
	 * @return String
	 */
	@Override
	public String getDateTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
		return df.format(new TSDate());
	}
	
	/**
	 * 
	 * @param e
	 * @param maxLines
	 * @return
	 */
	public static String shortenedStackTrace(Exception e, int maxLines) {
	    StringWriter writer = new StringWriter();
	    e.printStackTrace(new PrintWriter(writer));
	    String[] lines = writer.toString().split("\n");
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < Math.min(lines.length, maxLines); i++) {
	        sb.append(lines[i]).append("\n");
	    }
	    return sb.toString();
	}
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	private static String removeSpecialCharShop(String str){
		String result = null;
		if(null != str){
			result = str.replaceAll("[^a-zA-Z0-9()\\s-]+","").trim();
		}
		return result;
	}
	
 	/**
	 * 
	 * @param ein
	 * @return string containing only digits
	 */
	private String removeNonDigitCharShop(String ein) {
		String result = null;
		if(null != ein){
			result = ein.replaceAll("[^0-9()]+","").trim();
		}
		return result;
	}
	
	/**
	 * 
	 * @param month int
	 * @return string with length 2, left padding 0 
	 */
	private String formatForTwoDigit(int month) {
		return String.format("%02d", month);
	}
	
	/**
	 * Remove occurrences of period and extra spaces
	 * @param str
	 * @return String null if input string is null
	 */
	private String removePeriodAndExtraSpaces(String str) {
		String result = null;
		if(null != str){
			result = EnrollmentUtils.removeExtraSpaces(str).replaceAll("\\.", "");
		}
		return result;
	}

	
}
