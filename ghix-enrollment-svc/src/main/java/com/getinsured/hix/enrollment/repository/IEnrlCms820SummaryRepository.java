package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.EnrlCms820Summary;


public interface IEnrlCms820SummaryRepository extends JpaRepository<EnrlCms820Summary, Integer> {
	
	@Query("FROM EnrlCms820Summary ers WHERE ers.id = :summaryId ")
	EnrlCms820Summary getCms820SummaryBySummaryId(@Param("summaryId") Integer summaryId);

	@Query("SELECT ers.id FROM EnrlCms820Summary ers WHERE ers.status in ('Loaded')")
	List<Integer> getCms820ForProcessing();
	
	@Transactional
	@Modifying
	@Query("UPDATE EnrlCms820Summary ers SET ers.status = :status WHERE ers.id =:summaryId")
	void updateSummaryStatus(@Param("summaryId") Integer summaryId, @Param("status") String status);
	
}
