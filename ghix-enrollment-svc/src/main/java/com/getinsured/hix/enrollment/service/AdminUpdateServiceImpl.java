package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.enrollment.repository.IEnrlLocationRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
/**
 * @author raja
 * @since 10/05/2013
 * 
 */

@Service("adminUpdateService")
//@Transactional
public class AdminUpdateServiceImpl implements AdminUpdateService {	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminUpdateServiceImpl.class);

	@Autowired private LookupService lookupService;
	@Autowired private EnrolleeService enrolleeService;
	@Autowired private EnrolleeRaceService enrolleeRaceService;
	@Autowired private UserService userService;
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired private EnrolleeRelationshipService enrolleeRelationshipService;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentRequotingService enrollmentRequotingService;
	@Autowired private EnrollmentEmailNotificationService enrollmentEmailNotificationService;
	@Autowired private EnrollmentAhbxSyncService enrollmentAhbxSyncService;
	@Autowired private IEnrolleeRepository iEnrolleeRepository;
	@Autowired private IEnrlLocationRepository iEnrlLocationRepository;
	@Override
	@Transactional(rollbackFor={ GIException.class, Exception.class},propagation=Propagation.REQUIRED)
	public Enrollment adminUpdateEnrollment(AdminUpdateIndividualRequest adminUpdateIndividualRequest, Enrollment enrollment,AccountUser user, boolean isInternal)throws GIException{

		Long updatedssapId = adminUpdateIndividualRequest.getHousehold().getUpdatedSsapApplicationid();
		Boolean isOnlySsapAppIdUpdated = adminUpdateIndividualRequest.getHousehold().isIsOnlySsapAppIdUpdated();
		Boolean isLocationIdUpdated = adminUpdateIndividualRequest.getHousehold().isIsMailingAddressChanged();
		boolean householdContactPassed =false;
		boolean responsiblePersonPassed =false;
		boolean doRequote=false;
		boolean eventLog =false;
		
		if((isOnlySsapAppIdUpdated != null && isOnlySsapAppIdUpdated) && (updatedssapId != null && updatedssapId > 0)){
			enrollment.setPriorSsapApplicationid(updatedssapId);
			enrollment.setSsapApplicationid(updatedssapId);
		}
		else{
			List<EnrollmentEvent> enrollmentEventList = enrollment.getEnrollmentEvents();
			List<EnrollmentEvent> newEnrollmentEventList = new ArrayList<EnrollmentEvent>();		
			if(enrollmentEventList == null){
				enrollmentEventList = new ArrayList<EnrollmentEvent>();
			}
			LOGGER.info("Start admin update for enrollment :: " + enrollment.getId() + " event list size before update:: " + enrollmentEventList.size());
			AdminUpdateIndividualRequest.Household household = adminUpdateIndividualRequest.getHousehold();
			enrollment.setUpdatedBy(user);
			enrollment.setUpdatedOn(new TSDate());
			enrollment.setChangePlanAllowed(EnrollmentConstants.N);
			AdminUpdateIndividualRequest.Household.HouseHoldContact houseHoldPersonReq = household.getHouseHoldContact();
			AdminUpdateIndividualRequest.Household.ResponsiblePerson responsiblePersonReq = household.getResponsiblePerson();

			//  GETTING RESPONSIBLE PERSON  DATA
			Enrollee responsiblePersonEnrollee = enrolleeService.getResponsiblePersonByEnrollmentID(enrollment.getId());

			//  GETTING HOUSEHOLD CONTACT DATA
			Enrollee houseHoldContactEnrollee = enrolleeService.getHouseholdContactByEnrollmentID(enrollment.getId());

			if(responsiblePersonReq != null){
				responsiblePersonPassed=true;
				if(responsiblePersonEnrollee == null){
					responsiblePersonEnrollee = new Enrollee();
					enrollment.getEnrollees().add(responsiblePersonEnrollee);
				}

				if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonId())){
					responsiblePersonEnrollee.setExchgIndivIdentifier(responsiblePersonReq.getResponsiblePersonId());
				}

				if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonFirstName())){
					responsiblePersonEnrollee.setFirstName(responsiblePersonReq.getResponsiblePersonFirstName());
					responsiblePersonEnrollee.setMiddleName(responsiblePersonReq.getResponsiblePersonMiddleName());
				}

				/*if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonMiddleName())){
					responsiblePersonEnrollee.setMiddleName(responsiblePersonReq.getResponsiblePersonMiddleName());
				}*/

				if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonLastName())){
					responsiblePersonEnrollee.setLastName(responsiblePersonReq.getResponsiblePersonLastName());
				}

				//HIX-114986 Directly set suffix to request value
				responsiblePersonEnrollee.setSuffix(responsiblePersonReq.getResponsiblePersonSuffix());

				if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonSsn())){
					responsiblePersonEnrollee.setTaxIdNumber(responsiblePersonReq.getResponsiblePersonSsn());
				}


				if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonPreferredEmail())){
					responsiblePersonEnrollee.setPreferredEmail(responsiblePersonReq.getResponsiblePersonPreferredEmail());
					responsiblePersonEnrollee.setPreferredSMS(null);
				}
				else if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonPreferredPhone())){
					responsiblePersonEnrollee.setPreferredSMS(responsiblePersonReq.getResponsiblePersonPreferredPhone());
					responsiblePersonEnrollee.setPreferredEmail(null);
				}

				if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonPrimaryPhone())){
					responsiblePersonEnrollee.setPrimaryPhoneNo(responsiblePersonReq.getResponsiblePersonPrimaryPhone());
				}

				if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonSecondaryPhone())){
					responsiblePersonEnrollee.setSecondaryPhoneNo(responsiblePersonReq.getResponsiblePersonSecondaryPhone());
				}


				if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeAddress1())){
					Location responsiblePersonAddress = responsiblePersonEnrollee.getHomeAddressid();
					if(responsiblePersonAddress == null &&  isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeAddress1())){
						responsiblePersonAddress = new Location();
					}
					if(null != responsiblePersonAddress){
						responsiblePersonAddress.setAddress1(responsiblePersonReq.getResponsiblePersonHomeAddress1());
						responsiblePersonAddress.setAddress2(responsiblePersonReq.getResponsiblePersonHomeAddress2());
						if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeCity())){
							responsiblePersonAddress.setCity(responsiblePersonReq.getResponsiblePersonHomeCity());
						}
						if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeState())){
							responsiblePersonAddress.setState(responsiblePersonReq.getResponsiblePersonHomeState());
						}
						if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeZip())){
							responsiblePersonAddress.setZip(responsiblePersonReq.getResponsiblePersonHomeZip());
						}

					}
					responsiblePersonEnrollee.setHomeAddressid(responsiblePersonAddress);
				}


				responsiblePersonEnrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON));
				responsiblePersonEnrollee.setUpdatedOn(new TSDate());
				responsiblePersonEnrollee.setUpdatedBy(user);

				// Setting enrolleeEvent
				
				populateEnrollmentEvents(enrollment, responsiblePersonEnrollee, household.getMaintenanceReasonCode(), user, newEnrollmentEventList);
				eventLog=true;
				
				enrollment.setResponsiblePerson(responsiblePersonEnrollee);
			}

			//  Getting houseHold contact data

			if(houseHoldPersonReq != null){
				householdContactPassed=true;
				if(houseHoldContactEnrollee == null){
					houseHoldContactEnrollee = new Enrollee();
					enrollment.getEnrollees().add(houseHoldContactEnrollee);
				}
				if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactId())){
					houseHoldContactEnrollee.setExchgIndivIdentifier(houseHoldPersonReq.getHouseHoldContactId());
				}
				if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactFirstName())){
					houseHoldContactEnrollee.setFirstName(houseHoldPersonReq.getHouseHoldContactFirstName());
					houseHoldContactEnrollee.setMiddleName(houseHoldPersonReq.getHouseHoldContactMiddleName());
				}
				/*if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactMiddleName())){
					houseHoldContactEnrollee.setMiddleName(houseHoldPersonReq.getHouseHoldContactMiddleName());
				}*/
				if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactLastName())){
					houseHoldContactEnrollee.setLastName(houseHoldPersonReq.getHouseHoldContactLastName());
				}
				
				//HIX-114986 Directly set suffix to request value
				houseHoldContactEnrollee.setSuffix(houseHoldPersonReq.getHouseHoldContactSuffix());
				
				if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactPreferredEmail())){
					houseHoldContactEnrollee.setPreferredEmail(houseHoldPersonReq.getHouseHoldContactPreferredEmail());
					houseHoldContactEnrollee.setPreferredSMS(null);
				}
				else if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactPreferredPhone())){
					houseHoldContactEnrollee.setPreferredSMS(houseHoldPersonReq.getHouseHoldContactPreferredPhone());
					houseHoldContactEnrollee.setPreferredEmail(null);
				}
				if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactPrimaryPhone())){
					houseHoldContactEnrollee.setPrimaryPhoneNo(houseHoldPersonReq.getHouseHoldContactPrimaryPhone());
				}
				if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactSecondaryPhone())){
					houseHoldContactEnrollee.setSecondaryPhoneNo(houseHoldPersonReq.getHouseHoldContactSecondaryPhone());
				}
				if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactFederalTaxIdNumber())){
					houseHoldContactEnrollee.setTaxIdNumber(houseHoldPersonReq.getHouseHoldContactFederalTaxIdNumber());
				}

				if(isNotNullAndEmpty(houseHoldPersonReq.getHouseHoldContactHomeAddress1())){
					Location houseHoldContactAddress = houseHoldContactEnrollee.getHomeAddressid();
					if(houseHoldContactAddress == null &&  isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeAddress1())){
						houseHoldContactAddress = new Location();
					}
					if(null != houseHoldContactAddress){
						houseHoldContactAddress.setAddress1(responsiblePersonReq.getResponsiblePersonHomeAddress1());
						houseHoldContactAddress.setAddress2(responsiblePersonReq.getResponsiblePersonHomeAddress2());
						if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeCity())){
							houseHoldContactAddress.setCity(responsiblePersonReq.getResponsiblePersonHomeCity());
						}

						if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeState())){
							houseHoldContactAddress.setState(responsiblePersonReq.getResponsiblePersonHomeState());
						}

						if(isNotNullAndEmpty(responsiblePersonReq.getResponsiblePersonHomeZip())){
							houseHoldContactAddress.setZip(responsiblePersonReq.getResponsiblePersonHomeZip());
						}

					}
					responsiblePersonEnrollee.setHomeAddressid(houseHoldContactAddress);
				}

				houseHoldContactEnrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT));
				houseHoldContactEnrollee.setUpdatedOn(new TSDate());
				houseHoldContactEnrollee.setUpdatedBy(user);

				// Setting enrolleeEvent
				populateEnrollmentEvents(enrollment, houseHoldContactEnrollee, household.getMaintenanceReasonCode(), user, newEnrollmentEventList);
				eventLog=true;
				
				String householdContactName = concatEnrolleeName(houseHoldContactEnrollee);
				if(isNotNullAndEmpty(householdContactName)){
					enrollment.setSponsorName(householdContactName);
				}

				if(isNotNullAndEmpty(houseHoldContactEnrollee.getTaxIdNumber())){
					enrollment.setSponsorTaxIdNumber(houseHoldContactEnrollee.getTaxIdNumber());
				}
				enrollment.setHouseholdContact(houseHoldContactEnrollee);
			}

			if(updatedssapId != null && updatedssapId > 0){
				enrollment.setSsapApplicationid(Long.valueOf(household.getUpdatedSsapApplicationid()));
			}
			/*if(isNotNullAndEmpty(household.getProposedStartDate()) && EnrollmentConfiguration.isCaCall()){
				Date updatedEffectiveStartDate = DateUtil.StringToDate(household.getProposedStartDate(), GhixConstants.REQUIRED_DATE_FORMAT);;
				enrollment.setBenefitEffectiveDate(updatedEffectiveStartDate);
				for(Enrollee enrollee : enrollment.getEnrolleesAndSubscriber()){
					if(enrollee.getEnrolleeLkpValue() != null && enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)){
						enrollee.setEffectiveStartDate(updatedEffectiveStartDate);
					}
				}
			}*/
			boolean subscriberOnReq = false;
			List<Integer> enrolleeIdList = new ArrayList<Integer>();
			List<Member> memberList = adminUpdateIndividualRequest.getHousehold().getMembers().getMember();
			LOGGER.info("Event list size after household and responsible person update, main list :: " + enrollmentEventList.size() + " new list :: "+ newEnrollmentEventList.size());
			LOGGER.info("Looping through member list in request, member list size :: " + (null != memberList ? memberList.size() : 0 ));
			Enrollee subscriber =null;
			for(Member member : memberList){
				LOGGER.info("Member ID :: " + member.getMemberId() );
				if(isNotNullAndEmpty(member.getMemberId()) && !("0").equalsIgnoreCase(member.getMemberId())){
					Enrollee enrollee = null;
					List<Enrollee> enrollees=enrolleeService.findEnrolleeByEnrollmentIDAndMemberID(member.getMemberId(),enrollment.getId());
					if(enrollees!=null && enrollees.size()>0){
						enrollee=enrollees.get(0);
					}
					if(enrollee == null ){
						LOGGER.info("IND 57 : No member exists with id : "+member.getMemberId()+" For Enrollment id : "+enrollment.getId());
						/*if(EnrollmentConfiguration.isCaCall()){
							LOGGER.info("IND 57 : Incorrect member id, No member exists with id : "+member.getMemberId()+" For Enrollment id : "+household.getEnrollmentId());
							throw new GIException(EnrollmentConstants.ERROR_CODE_203,"IND 57 : Incorrect member id, No member exists with id : "+member.getMemberId()+" For Enrollment id : "+household.getEnrollmentId(),EnrollmentConstants.HIGH);
						}*/
						
						// Update Household Contact and Responsible person. Will occur only for SHOP - Dental, Where the Household Contact and Responsible person not Enrolled as Enrollee.
						if(isNotNullAndEmpty(enrollment.getEnrollmentTypeLkp()) && (enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP))){
							if(isNotNullAndEmpty(responsiblePersonEnrollee) && responsiblePersonEnrollee.getExchgIndivIdentifier().equalsIgnoreCase(member.getMemberId())){
								responsiblePersonEnrollee = populateHouseHoldContactOrResponsiblePerson(responsiblePersonEnrollee,member,user);

								// SETTING RESPONSIBLE PERSON TO THE ENROLLMENT OBJECT
								enrollment.setResponsiblePerson(responsiblePersonEnrollee);

								// Setting enrolleeEvent
								populateEnrollmentEvents(enrollment,responsiblePersonEnrollee,member.getMaintenanceReasonCode(),user, newEnrollmentEventList);
								eventLog=true;
							}
							if(isNotNullAndEmpty(houseHoldContactEnrollee) && houseHoldContactEnrollee.getExchgIndivIdentifier().equalsIgnoreCase(member.getMemberId())){
								houseHoldContactEnrollee = populateHouseHoldContactOrResponsiblePerson(houseHoldContactEnrollee,member,user);

								// SETTING HOUSEHOLDCONTACT PERSON TO THE ENROLLMENT OBJECT
								enrollment.setHouseholdContact(houseHoldContactEnrollee);

								// Setting enrolleeEvent
								
								populateEnrollmentEvents(enrollment,houseHoldContactEnrollee,member.getMaintenanceReasonCode(),user, newEnrollmentEventList);
								eventLog=true;
								
								String householdContactName = concatEnrolleeName(houseHoldContactEnrollee);
								if(isNotNullAndEmpty(householdContactName)){
									enrollment.setSponsorName(householdContactName);
								}

								if(isNotNullAndEmpty(houseHoldContactEnrollee.getTaxIdNumber())){
									enrollment.setSponsorTaxIdNumber(houseHoldContactEnrollee.getTaxIdNumber());
								}
							}
						} else {
							// HIX-111710 Check if member is household contact or responsible person
							updateHouseholdResponsible(member, enrollment.getEnrollees(), user, householdContactPassed,	responsiblePersonPassed, household.getMaintenanceReasonCode(), enrollment);
						}
						// No Enrollee to Update, Can occur in case of Health and Dental Enrollment being updated using same request. Hence Continue
						continue;
					}
					enrolleeIdList.add(enrollee.getId());
					updateHouseholdResponsible(member, enrollment.getEnrollees(), user, householdContactPassed, responsiblePersonPassed, household.getMaintenanceReasonCode(), enrollment);
					//Create Subscriber
					if(isNotNullAndEmpty(enrollee.getPersonTypeLkp()) && (enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){
						subscriberOnReq=true;
						subscriber= enrollee; 
						enrollment.setSubscriberName(concatEnrolleeName(subscriber));
					}


					// POPULATING THE EMPLOYEE(SUBSCRIBER) INFORMATION INTO RESPONSIBLE PERSON & HOUSEHOLDCONTACT PERSON.
					// This is sent in member Info loop only for Shop for Health Enrollment. In shop Subscriber == ResponsiblePerson == HouseholdContact
					if((isNotNullAndEmpty(enrollment.getEnrollmentTypeLkp()) && (enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)))
							&& (isNotNullAndEmpty(enrollee.getPersonTypeLkp()) && (enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))){

						if(isNotNullAndEmpty(responsiblePersonEnrollee) && responsiblePersonEnrollee.getExchgIndivIdentifier().equalsIgnoreCase(member.getMemberId())){
							responsiblePersonEnrollee = populateHouseHoldContactOrResponsiblePerson(responsiblePersonEnrollee,member,user);

							// SETTING RESPONSIBLE PERSON TO THE ENROLLMENT OBJECT
							enrollment.setResponsiblePerson(responsiblePersonEnrollee);

							// Setting enrolleeEvent
							populateEnrollmentEvents(enrollment,responsiblePersonEnrollee,member.getMaintenanceReasonCode(),user, newEnrollmentEventList);
							eventLog=true;
						}
						if(isNotNullAndEmpty(houseHoldContactEnrollee) && houseHoldContactEnrollee.getExchgIndivIdentifier().equalsIgnoreCase(member.getMemberId())){
							houseHoldContactEnrollee = populateHouseHoldContactOrResponsiblePerson(houseHoldContactEnrollee,member,user);

							// SETTING HOUSEHOLDCONTACT PERSON TO THE ENROLLMENT OBJECT
							enrollment.setHouseholdContact(houseHoldContactEnrollee);

							// Setting enrolleeEvent
							populateEnrollmentEvents(enrollment,houseHoldContactEnrollee,member.getMaintenanceReasonCode(),user, newEnrollmentEventList);
							eventLog=true;
							

							String householdContactName = concatEnrolleeName(houseHoldContactEnrollee);
							if(isNotNullAndEmpty(householdContactName)){
								enrollment.setSponsorName(householdContactName.trim());
							}

							if(isNotNullAndEmpty(houseHoldContactEnrollee.getTaxIdNumber())){
								enrollment.setSponsorTaxIdNumber(houseHoldContactEnrollee.getTaxIdNumber());
							}
						}
					}

					if(isNotNullAndEmpty(member.getDob())){
						Date dob=EnrollmentUtils.validateDOB(member.getDob());
						if(!DateUtils.isSameDay(enrollee.getEffectiveStartDate(), dob) && dob.after(enrollee.getEffectiveStartDate())){
							throw new GIException(EnrollmentConstants.ERROR_CODE_221,EnrollmentConstants.INVALID_DOB, EnrollmentConstants.HIGH);
						}
						
						if(enrollee.getBirthDate()==null || !DateUtils.isSameDay(dob, enrollee.getBirthDate())){
							doRequote=true;
						}
						enrollee.setBirthDate(dob);
					}
					if(isNotNullAndEmpty(member.getFirstName())){
						enrollee.setFirstName(member.getFirstName());
						enrollee.setMiddleName(member.getMiddleName());
					}
					if(isNotNullAndEmpty(member.getLastName())){
						enrollee.setLastName(member.getLastName());
					}
					/*if(isNotNullAndEmpty(member.getMiddleName())){
						enrollee.setMiddleName(member.getMiddleName());
					}*/
					
					//HIX-114986 Directly set suffix to request value
					enrollee.setSuffix(member.getSuffix());
					
					if(isNotNullAndEmpty(member.getPrimaryPhone())){
						enrollee.setPrimaryPhoneNo(member.getPrimaryPhone());
					}
					if(isNotNullAndEmpty(member.getSecondaryPhone())){
						enrollee.setSecondaryPhoneNo(member.getSecondaryPhone());
					}
					if(isNotNullAndEmpty(member.getPreferredEmail())){
						enrollee.setPreferredEmail(member.getPreferredEmail());
						enrollee.setPreferredSMS(null);
					}
					else if(isNotNullAndEmpty(member.getPreferredPhone())){
						enrollee.setPreferredSMS(member.getPreferredPhone());
						enrollee.setPreferredEmail(null);
					}
					if(isNotNullAndEmpty(member.getSsn())){
						enrollee.setTaxIdNumber(member.getSsn());
					}
					if(isNotNullAndEmpty(member.getMaritalStatusCode())){
						enrollee.setMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.MARITAL_STATUS, member.getMaritalStatusCode()));
					}
					if(isNotNullAndEmpty(member.getGenderCode())){
						enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GENDER, member.getGenderCode()));
					}
					if(isNotNullAndEmpty(member.getTobacco())){
						String tobaccoLkp=null;
						if(member.getTobacco().toString().equalsIgnoreCase(EnrollmentConstants.Y)){
							tobaccoLkp=EnrollmentConstants.T;
						}else if(member.getTobacco().toString().equalsIgnoreCase(EnrollmentConstants.N)){
							tobaccoLkp=EnrollmentConstants.N;
						}else if(member.getTobacco().toString().equalsIgnoreCase(EnrollmentConstants.U)){
							tobaccoLkp=EnrollmentConstants.U;
						}
						if(isNotNullAndEmpty(tobaccoLkp)){
							if(enrollee.getTobaccoUsageLkp()==null || !tobaccoLkp.equalsIgnoreCase(enrollee.getTobaccoUsageLkp().getLookupValueCode())){
								doRequote=true;
								Date tobaccoEffectiveDate=null;
								Date sysDate= new TSDate();
								if(DateUtil.getDateOfMonth(sysDate) <=EnrollmentConstants.FIFTEEN){
									tobaccoEffectiveDate=EnrollmentUtils.getNextMonthStartDate(sysDate);
									 
								 }else{
									 tobaccoEffectiveDate=EnrollmentUtils.getNextToNextMonthStartDate(sysDate);
								 }
								if(enrollee.getEffectiveStartDate()!=null && tobaccoEffectiveDate.before(enrollee.getEffectiveStartDate())){
									tobaccoEffectiveDate=enrollee.getEffectiveStartDate();
								}
								if(enrollee.getEffectiveEndDate()!=null && EnrollmentUtils.getMonthStartDate(enrollee.getEffectiveEndDate()).before(tobaccoEffectiveDate)){
									tobaccoEffectiveDate=EnrollmentUtils.getMonthStartDate(enrollee.getEffectiveEndDate());
								}
								
								enrollee.setLastTobaccoUseDate(tobaccoEffectiveDate);
							}
							enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, tobaccoLkp));
						}

						
					}/*else if(EnrollmentConfiguration.isCaCall()){
						enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.U));
					}*/
					if(isNotNullAndEmpty(member.getSpokenLanguageCode())){
						enrollee.setLanguageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_SPOKEN, member.getSpokenLanguageCode()));
					}
					if(isNotNullAndEmpty(member.getWrittenLanguageCode())){
						enrollee.setLanguageWrittenLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_WRITTEN, member.getWrittenLanguageCode()));
					}


					if(isLocationIdUpdated== null || !isLocationIdUpdated){
						if(isNotNullAndEmpty(member.getMailingAddress1())){
							Location mailingAddress = enrollee.getMailingAddressId();
							if(mailingAddress == null){
								mailingAddress = new Location();
							}
							mailingAddress.setAddress1(member.getMailingAddress1());
							mailingAddress.setAddress2(member.getMailingAddress2());
							if(isNotNullAndEmpty(member.getMailingCity())){
								mailingAddress.setCity(member.getMailingCity());
							}

							if(isNotNullAndEmpty(member.getMailingState())){
								mailingAddress.setState(member.getMailingState());
							}

							if(isNotNullAndEmpty(member.getMailingZip())){
								mailingAddress.setZip(member.getMailingZip());
							}else{
								throw new GIException(EnrollmentConstants.ERROR_CODE_222,EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE, EnrollmentConstants.HIGH);
							}

							enrollee.setMailingAddressId(mailingAddress);
						}

					}

					if(isNotNullAndEmpty(member.getHomeAddress1()) ){
						if(!isNotNullAndEmpty(member.getHomeZip())){
							throw new GIException(EnrollmentConstants.ERROR_CODE_222,EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE, EnrollmentConstants.HIGH);
						}
						if(!isNotNullAndEmpty(member.getHomeCountyCode())){
							throw new GIException(EnrollmentConstants.ERROR_CODE_223,EnrollmentConstants.ERR_MSG_COUNTY_CODE_VALUE, EnrollmentConstants.HIGH);
						}
						Location homeAddress = enrollee.getHomeAddressid();
						if (homeAddress == null) {
							homeAddress = new Location();
						}
						if (isNotNullAndEmpty(member.getHomeAddress1())) {
							homeAddress.setAddress1(member.getHomeAddress1());
						}
						if (isNotNullAndEmpty(member.getHomeAddress2())) {
							homeAddress.setAddress2(member.getHomeAddress2());
						}
						if (isNotNullAndEmpty(member.getHomeCity())) {
							homeAddress.setCity(member.getHomeCity());
						}
						if (isNotNullAndEmpty(member.getHomeState())) {
							homeAddress.setState(member.getHomeState());
						}
						if (isNotNullAndEmpty(member.getHomeZip())) {
							homeAddress.setZip(member.getHomeZip());
						}
						if (isNotNullAndEmpty(member.getHomeCountyCode())) {
							homeAddress.setCountycode(member.getHomeCountyCode());
						}
						enrollee.setHomeAddressid(homeAddress);
					}
					if(isNotNullAndEmpty(member.getExternalMemberId())){
						enrollee.setExternalIndivId(member.getExternalMemberId());	
					}



					// Setting Custodial Parent
					if(isNotNullAndEmpty(member.getCustodialParentId())){
						Enrollee custodialParentEnrollee = enrollee.getCustodialParent();

						if(custodialParentEnrollee == null ){
							custodialParentEnrollee = new Enrollee();
						}

						custodialParentEnrollee.setExchgIndivIdentifier(member.getCustodialParentId());

						if(isNotNullAndEmpty(member.getCustodialParentFirstName())){
							custodialParentEnrollee.setFirstName(member.getCustodialParentFirstName());
							custodialParentEnrollee.setMiddleName(member.getCustodialParentMiddleName());
						}
						/*if(isNotNullAndEmpty(member.getCustodialParentMiddleName())){
							custodialParentEnrollee.setMiddleName(member.getCustodialParentMiddleName());
						}*/
						if(isNotNullAndEmpty(member.getCustodialParentLastName())){
							custodialParentEnrollee.setLastName(member.getCustodialParentLastName());
						}

						//HIX-114986 Directly set suffix to request value
						custodialParentEnrollee.setSuffix(member.getCustodialParentSuffix());

						if(isNotNullAndEmpty(member.getCustodialParentPreferredEmail())){
							custodialParentEnrollee.setPreferredEmail(member.getCustodialParentPreferredEmail());
							custodialParentEnrollee.setPreferredSMS(null);
						}
						if(isNotNullAndEmpty(member.getCustodialParentPreferredPhone())){
							custodialParentEnrollee.setPreferredSMS(member.getCustodialParentPreferredPhone());
							custodialParentEnrollee.setPreferredEmail(null);
						}
						if(isNotNullAndEmpty(member.getCustodialParentPrimaryPhone())){
							custodialParentEnrollee.setPrimaryPhoneNo(member.getCustodialParentPrimaryPhone());
						}
						if(isNotNullAndEmpty(member.getCustodialParentSecondaryPhone())){
							custodialParentEnrollee.setSecondaryPhoneNo(member.getCustodialParentSecondaryPhone());
						}


						if(isNotNullAndEmpty(member.getCustodialParentHomeAddress1())){
							Location custParentAddress = custodialParentEnrollee.getHomeAddressid();

							if(custParentAddress == null){
								custParentAddress= new Location();
							}
							custParentAddress.setAddress1(member.getCustodialParentHomeAddress1());
							custParentAddress.setAddress2(member.getCustodialParentHomeAddress2());
							if(isNotNullAndEmpty(member.getCustodialParentHomeCity())){
								custParentAddress.setCity(member.getCustodialParentHomeCity());
							}
							if(isNotNullAndEmpty(member.getCustodialParentHomeState())){
								custParentAddress.setState(member.getCustodialParentHomeState());
							}
							if(isNotNullAndEmpty(member.getCustodialParentHomeZip())){
								custParentAddress.setZip(member.getCustodialParentHomeZip());
							}


							custodialParentEnrollee.setHomeAddressid(custParentAddress);
						}

						custodialParentEnrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT));
						custodialParentEnrollee.setUpdatedBy(user);
						custodialParentEnrollee.setUpdatedOn(new TSDate());
						enrollee.setCustodialParent(custodialParentEnrollee);
					}//end of custodialParent if


					// Setting enrolleeEvent
					populateEnrollmentEvents(enrollment, enrollee, member.getMaintenanceReasonCode(), user, newEnrollmentEventList);
					LOGGER.info("Event list size after member :: "+member.getMemberId() +" update, main list :: " + enrollmentEventList.size()+ " new list :: " + newEnrollmentEventList.size());
					eventLog=true;
					
					//setting Race Ethnicity
					List<AdminUpdateIndividualRequest.Household.Members.Member.Race> enrolleeRaces = member.getRace();
					if(enrolleeRaces!=null && !enrolleeRaces.isEmpty()){
						List<EnrolleeRace> oldEnrolleeRaces=enrollee.getEnrolleeRace();
						// We need to delete the existing EnrolleeRace list . This needs discussion
						List<EnrolleeRace> newEnrolleeRaces = new ArrayList<EnrolleeRace>();
						for(AdminUpdateIndividualRequest.Household.Members.Member.Race race :enrolleeRaces  ){
							if (isNotNullAndEmpty(race.getRaceEthnicityCode())){
								EnrolleeRace enrolleeRace = new EnrolleeRace();
								enrolleeRace.setRaceDescription(race.getDescription());
								enrolleeRace.setRaceEthnicityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_TYPE_RACE, race.getRaceEthnicityCode()));
								newEnrolleeRaces.add(enrolleeRace);
							}
						}

						if(!EnrollmentUtils.compareEnrolleerace(oldEnrolleeRaces, newEnrolleeRaces)){
							enrolleeRaceService.deleteEnrolleeRace(oldEnrolleeRaces);
							if(newEnrolleeRaces!=null && !newEnrolleeRaces.isEmpty()){
								for(EnrolleeRace race : newEnrolleeRaces){
									race.setEnrollee(enrollee);
								}
							}
							enrollee.setEnrolleeRace(newEnrolleeRaces);
						}else{
							enrollee.setEnrolleeRace(oldEnrolleeRaces);
						}
					}//End of EnrolleeRace Loop

					//   Setting relationship update
					List<AdminUpdateIndividualRequest.Household.Members.Member.Relationships> relList = member.getRelationships();
					if(isNotNullAndEmpty(relList) && !relList.isEmpty()){

						List<EnrolleeRelationship> enrolleeRelationshipList = enrolleeRelationshipService.getEnrolleeRelationshipDataBySourceEnrollee(enrollee.getId());

						for(AdminUpdateIndividualRequest.Household.Members.Member.Relationships relationship: relList){
							if((isNotNullAndEmpty(relationship) && isNotNullAndEmpty(relationship.getRelationshipMemberId())) && !("0").equalsIgnoreCase(relationship.getRelationshipMemberId())){	
								List<Enrollee> trgtEnrollees = enrolleeService.findEnrolleeByEnrollmentIDAndMemberID(relationship.getRelationshipMemberId(), enrollment.getId());
								if(trgtEnrollees!=null && trgtEnrollees.size()>0){
									for (Enrollee trgtEnrollee:  trgtEnrollees){
										if(trgtEnrollee != null){
											EnrolleeRelationship enrolleeRelationship=null;
											enrolleeRelationship=findRelationFromList(enrolleeRelationshipList, enrollee.getId(), trgtEnrollee.getId());
											if(enrolleeRelationship == null){
												enrolleeRelationship = new EnrolleeRelationship();
												enrolleeRelationship.setSourceEnrollee(enrollee);
												enrolleeRelationship.setTargetEnrollee(trgtEnrollee);
												enrolleeRelationshipList.add(enrolleeRelationship);
											}
											enrolleeRelationship.setRelationshipLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.RELATIONSHIP, relationship.getRelationshipToMember()));
											
											if(EnrollmentConfiguration.isCaCall()) {//HIX-108392
												if(isNotNullAndEmpty(trgtEnrollee.getPersonTypeLkp()) && trgtEnrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
													enrollee.setRelationshipToHCPLkp(enrolleeRelationship.getRelationshipLkp());
												}
											}
											
										}
									}
								}
								
							}
						}
						enrollee.setEnrolleeRelationship( enrolleeRelationshipList);
						
						if(!EnrollmentConfiguration.isCaCall()) {
							/*HIX-105676- Update AdminUpdate Service to handle new field "houseHoldContactRelationship" */
							if(isNotNullAndEmpty(member.getHouseHoldContactRelationship())) {
								enrollee.setRelationshipToHCPLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.RELATIONSHIP, member.getHouseHoldContactRelationship()));
							}else {
								throw new GIException(EnrollmentConstants.ERROR_CODE_211,EnrollmentConstants.ERROR_MSG_INVALID_RELATION,EnrollmentConstants.HIGH);
							}
						}
						
					}//End of relList empty checl

					enrollee.setUpdatedOn(new TSDate());
					enrollee.setUpdatedBy(user);
				}else{
					throw new GIException(EnrollmentConstants.ERROR_CODE_250,"Invalid member Id",EnrollmentConstants.HIGH);
				}
			}//End of enrollee while loop
			
			if(subscriber==null){
				subscriber =  enrolleeService.findSubscriberbyEnrollmentIDId(enrollment.getId());
			}

			if(isNotNullAndEmpty(subscriber)){
				String subscriberName = concatEnrolleeName(subscriber);
				if(isNotNullAndEmpty(subscriberName)){
					enrollment.setSubscriberName(subscriberName);
				}
				if(!subscriberOnReq){//HIX-84754
					populateEnrollmentEvents(enrollment, subscriber, EnrollmentConstants.EVENT_REASON_AI, user, newEnrollmentEventList);
					eventLog=true;
				}
				
				if(subscriber != null  && EnrollmentConfiguration.isCaCall()){//Sponsor Name Setting for CA
					int subscriberAge = getAge(subscriber.getBirthDate(), subscriber.getEffectiveStartDate());
					if(subscriberAge >= EnrollmentConstants.AGE_18){
						if(enrollment.getEnrollmentTypeLkp() != null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
							if(isNotNullAndEmpty(subscriberName)){
								enrollment.setSponsorName(subscriberName);
								if(isNotNullAndEmpty(subscriber.getTaxIdNumber())){
									enrollment.setSponsorTaxIdNumber(subscriber.getTaxIdNumber());
								}
							}
						}
					}else{
						String householdContactName = concatEnrolleeName(houseHoldContactEnrollee);
						if(isNotNullAndEmpty(householdContactName)){
							enrollment.setSponsorName(householdContactName.trim());
							if(isNotNullAndEmpty(houseHoldContactEnrollee.getTaxIdNumber())){
								enrollment.setSponsorTaxIdNumber(houseHoldContactEnrollee.getTaxIdNumber());
							}
						}
					}
				}//Sponsor Name Setting for CA
			}			
			
			List<Enrollee> enrolleeList = iEnrolleeRepository.getActiveEnrolleesForEnrollmentID(enrollment.getId(),new TSDate());
			if (enrolleeList != null && !enrolleeList.isEmpty()) {
				for (Enrollee enrollee : enrolleeList) {
					if (!enrolleeIdList.contains(enrollee.getId())) {
						populateEnrollmentEvents(enrollment, enrollee, EnrollmentConstants.EVENT_REASON_AI, user,
								newEnrollmentEventList);
						eventLog = true;
					}
				}
			}
			
			LOGGER.info("Main event list :: " + enrollmentEventList.size()+ " new list :: " + newEnrollmentEventList.size());
			Map<Integer,EnrollmentEvent> enrolleeEventMap = new HashMap<Integer,EnrollmentEvent>();
			for(EnrollmentEvent enrollmentEvent : newEnrollmentEventList) {
				enrolleeEventMap.put(enrollmentEvent.getEnrollee().getId(), enrollmentEvent);
			}
			LOGGER.info("Event map size :: " + enrolleeEventMap.size());
			enrollmentEventList.addAll(enrolleeEventMap.values());
		}
		enrollment.setUpdatedOn(new TSDate());
		enrollment.setUpdatedBy(user);

		if(doRequote && EnrollmentConfiguration.isIdCall() ){
			enrollmentRequotingService.updateMonthlyPremiumForEnrollment(enrollment, true, false, null, true);
		}
		/**
		 * Logging Enrollment DEMOGRAPHIC_UPDATES Application event
		 */
		if(eventLog) {
			enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.DEMOGRAPHIC_UPDATES.toString(), user);
		}
		
		if(isInternal){
			boolean onlySsapAppIdUpdated = isOnlySsapAppIdUpdated != null ? isOnlySsapAppIdUpdated : false;
			saveEnrollmentCallInd70(enrollment, onlySsapAppIdUpdated);
		}
		LOGGER.info("Event list size after complete update:: " + enrollment.getEnrollmentEvents().size());
		//HIX-44501
		setAssisterBrokerId(enrollment, user);
		return enrollment;
	}
	/**
	 * @author panda_p
	 * 
	 * @param dateOfBirth
	 * @param effectiveStartDate 
	 * @return
	 */
	private int getAge(Date dateOfBirth, Date effectiveStartDate) {
	    int age = 0;
	    Calendar born = TSCalendar.getInstance();
	    Calendar effectiveDate = TSCalendar.getInstance();
	    if(dateOfBirth!= null && effectiveStartDate !=null) {
	        effectiveDate.setTime(effectiveStartDate);
	        born.setTime(dateOfBirth);  
	        if(born.after(effectiveDate)) {
	            throw new IllegalArgumentException("Can't be born in the future");
	        }
	        age = effectiveDate.get(Calendar.YEAR) - born.get(Calendar.YEAR);             
	        if(effectiveDate.get(Calendar.DAY_OF_YEAR) < born.get(Calendar.DAY_OF_YEAR))  {
	            age-=1;
	        }
	        if(age == 0){
	        	age++;
	        }
	    }  
	    return age;
	}
	private Enrollee populateHouseHoldContactOrResponsiblePerson(Enrollee enrollee, AdminUpdateIndividualRequest.Household.Members.Member member, AccountUser user) throws GIException{

		if(isNotNullAndEmpty(member)){
			if(isNotNullAndEmpty(member.getFirstName())){
				enrollee.setFirstName(member.getFirstName());
				enrollee.setMiddleName(member.getMiddleName());
			}
			enrollee.setLastName(member.getLastName());
			enrollee.setSuffix(member.getSuffix());

			enrollee.setPreferredEmail(member.getPreferredEmail());
			enrollee.setPreferredSMS(null);
			enrollee.setPrimaryPhoneNo(member.getPrimaryPhone());
			enrollee.setSecondaryPhoneNo(member.getSecondaryPhone());



			if(isNotNullAndEmpty(member.getHomeAddress1())){
				Location enrolleeAddressObj = enrollee.getHomeAddressid();	//new Location();
				if(enrolleeAddressObj == null){
					enrolleeAddressObj = new Location();
				}

				enrolleeAddressObj.setAddress1(member.getHomeAddress1());
				enrolleeAddressObj.setAddress2(member.getHomeAddress2());
				if(isNotNullAndEmpty(member.getHomeCity())){
					enrolleeAddressObj.setCity(member.getHomeCity());
				}
				if(isNotNullAndEmpty(member.getHomeState())){
					enrolleeAddressObj.setState(member.getHomeState());
				}
				if(isNotNullAndEmpty(member.getHomeZip())){
					enrolleeAddressObj.setZip(member.getHomeZip());
				}


				enrollee.setHomeAddressid(enrolleeAddressObj);
			}
			enrollee.setTaxIdNumber(member.getSsn());
			enrollee.setUpdatedOn(new TSDate());
			enrollee.setUpdatedBy(user);
		}	
		return enrollee;	
	} 

	/**
	 * @author Aditya-S
	 * @since 27-11-2014
	 * 
	 * @param enrollment
	 * @param enrollee
	 * @param household
	 * @param user
	 * @return
	 */
	private void populateEnrollmentEvents(Enrollment enrollment, Enrollee enrollee, String mrc, AccountUser user, List<EnrollmentEvent> enrollmentEventList){
		if(!enrollee.isEventCreated()){
			LOGGER.info("Creating event for enrollee id :: " + enrollee.getId() + " time :: " + new Date() + " enrollment :: " + enrollment.getId() + " member ID :: " + enrollee.getExchgIndivIdentifier());
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent(); 
			enrollmentEvent.setEnrollee(enrollee);
			enrollmentEvent.setEnrollment(enrollment);
			enrollee.setLastEventId(enrollmentEvent);
			//add null check
			if(isNotNullAndEmpty(mrc)){
				if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, mrc))){
					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, mrc));
				}else{
					throw new RuntimeException("Lookup value is null");
				}
			}else{
				throw new RuntimeException("mrc  is null or empty");
			}
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CHANGE));
			enrollmentEvent.setUpdatedBy(user);
			enrollmentEvent.setCreatedBy(user);
			enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_UPDATE.toString());
			enrollmentEventList.add(enrollmentEvent);
			enrollee.setEventCreated(true);
		}
		
	}

	/**
	 * @author Aditya-S
	 * @since 27-11-2014
	 * 
	 * Concats given Enrollee First name and Last name
	 * 
	 * @param enrolleeToConcat
	 * @return
	 */
	private String concatEnrolleeName(Enrollee enrolleeToConcat){
		StringBuilder enrolleeName = new StringBuilder();
		if(enrolleeToConcat != null){
			if(isNotNullAndEmpty(enrolleeToConcat.getFirstName())){
				enrolleeName.append(enrolleeToConcat.getFirstName());
				enrolleeName.append(" ");
			}
			if(isNotNullAndEmpty(enrolleeToConcat.getMiddleName())){
				enrolleeName.append(enrolleeToConcat.getMiddleName());
				enrolleeName.append(" ");	
			}
			if(isNotNullAndEmpty(enrolleeToConcat.getLastName())){
				enrolleeName.append(enrolleeToConcat.getLastName());
				enrolleeName.append(" ");
			}
			if(isNotNullAndEmpty(enrolleeToConcat.getSuffix())){
				enrolleeName.append(enrolleeToConcat.getSuffix());	
			}

			if(enrolleeName.length() > EnrollmentConstants.SPONSORNAME_MAX_LENGTH ){
				enrolleeName.delete(EnrollmentConstants.SPONSORNAME_MAX_LENGTH,enrolleeName.length());
			}
		}
		return enrolleeName.toString().trim();
	}

	private EnrolleeRelationship findRelationFromList(List<EnrolleeRelationship> enrolleeRelationshipList, int sourceEnrolleeId, int trgtEnrolleeId){
		EnrolleeRelationship enrolleeRelationShip = null;

		if(enrolleeRelationshipList !=  null && !enrolleeRelationshipList.isEmpty()){
			for(EnrolleeRelationship enrlRelationShip : enrolleeRelationshipList){
				if(enrlRelationShip.getSourceEnrollee() != null && enrlRelationShip.getTargetEnrollee() !=null &&
						enrlRelationShip.getSourceEnrollee().getId() == sourceEnrolleeId && 
						enrlRelationShip.getTargetEnrollee().getId() == trgtEnrolleeId){
					enrolleeRelationShip = enrlRelationShip;
					break;
				}
			}
		}
		return enrolleeRelationShip;
	}


	private void setAssisterBrokerId(Enrollment enrollment, AccountUser user) {
		/*AccountUser user;*/
		try {
			/*user = userService.getLoggedInUser();*/
			if (user != null && enrollment.getAssisterBrokerId() == null) {
				Role role = userService.getDefaultRole(user);
				if (role != null && ("CSR").equalsIgnoreCase(role.getName())) {
					enrollment.setAssisterBrokerId(user.getId());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Could not set assister Broker Id" + e);
		}
	}
	
	public String updateAddressTwo(Long householdCaseId, List<SsapApplicant> ssapApplicantList){
		try {
			AccountUser user = userService.findByEmail("exadmin@ghix.com");
			List<Enrollment> enrollmentList = enrollmentService.findActiveEnrollmentByHouseholdCaseId(householdCaseId);
			
			for(Enrollment enrollment: enrollmentList) {
				boolean isEnrollmentUpdated = false;
				List<Enrollee> enrolleeList = enrollment.getEnrollees(); 
				for(Enrollee enrollee: enrolleeList) {
					boolean isEnrolleeUpdated = false;
					List<EnrollmentEvent> enrollmentEventList = enrollee.getEnrollmentEvents();
					SsapApplicant ssapApplicant = getApplicant(ssapApplicantList, enrollee);
					if(ssapApplicant != null) {
						Location homeLocation = enrollee.getHomeAddressid();
						Location appHomeLocation = iEnrlLocationRepository.getOne(ssapApplicant.getOtherLocationId().intValue());
						if(StringUtils.isBlank(homeLocation.getAddress2()) && StringUtils.isNotBlank(appHomeLocation.getAddress2())) {
							if(homeLocation.getAddress1().equalsIgnoreCase(appHomeLocation.getAddress1())
								&& homeLocation.getCity().equalsIgnoreCase(appHomeLocation.getCity())
								&& homeLocation.getZip().equalsIgnoreCase(appHomeLocation.getZip())){
								
								homeLocation.setAddress2(appHomeLocation.getAddress2());
								
								isEnrolleeUpdated = true;
								isEnrollmentUpdated = true;
								
							}
						}
						Location mailLocation = enrollee.getMailingAddressId();
						Location appMailLocation = iEnrlLocationRepository.getOne(ssapApplicant.getMailiingLocationId().intValue());
						if(StringUtils.isEmpty(mailLocation.getAddress2()) && StringUtils.isNotBlank(appMailLocation.getAddress2())) {
							if(mailLocation.getAddress1().equalsIgnoreCase(appMailLocation.getAddress1())
									&& mailLocation.getCity().equalsIgnoreCase(appMailLocation.getCity())
									&& mailLocation.getZip().equalsIgnoreCase(appMailLocation.getZip())){
									
								mailLocation.setAddress2(appMailLocation.getAddress2());
									
								isEnrolleeUpdated = true;
								isEnrollmentUpdated = true;
									
							}
						}
						
						if(isEnrolleeUpdated) {
							populateEnrollmentEvents(enrollment, enrollee, "43", user,  enrollmentEventList);
						}
					}
					
					
				}
				if(isEnrollmentUpdated) {
					enrollmentService.saveEnrollment(enrollment);
				}
			}
			
			return "SUCCESS";
			
		} catch(Exception e) {
			LOGGER.error("Could not Update enrollment" + e);
		}
		return "FAILURE";
	}
	
	private SsapApplicant getApplicant(List<SsapApplicant> ssapApplicantList, Enrollee enrollee) {
		for(SsapApplicant ssapApplicant : ssapApplicantList) {
			if(ssapApplicant.getApplicantGuid().equalsIgnoreCase(enrollee.getExchgIndivIdentifier())) {
				return ssapApplicant;
			}
		}
		return null;
	}
	
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	private EnrollmentResponse saveEnrollmentCallInd70(Enrollment updatedEnrollment, boolean onlySsapAppIdUpdated){
		EnrollmentResponse enrollmentResponse = null;
		updatedEnrollment = enrollmentService.saveEnrollment(updatedEnrollment);
		
		if(EnrollmentConfiguration.isCaCall() && !onlySsapAppIdUpdated) {
			List<Enrollment> enrollmentList= new ArrayList<>();
			enrollmentList.add(updatedEnrollment);
			String houseHoldCaseID = updatedEnrollment.getHouseHoldCaseId();
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
		           // Override the afterCommit which need to be executed after transaction commit
		           public void afterCommit() { 
		              //Run the time consuming async task for the persisted entity
		        	   enrollmentAhbxSyncService.saveEnrollmentAhbxSync(enrollmentList, 
		        			   											houseHoldCaseID, 
		        			                                            EnrollmentAhbxSync.RecordType.IND57_ADMINUPDATE);
		           }
			});
		}
		enrollmentResponse = new EnrollmentResponse();
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		return enrollmentResponse;
	}
	
	private void updateHouseholdResponsible(Member member, List<Enrollee> enrollees, AccountUser user, boolean householdPassed, boolean responsiblePassed, String mrc, Enrollment enrollment) throws GIException{
		if(member!=null && enrollees!=null && enrollees.size()>0){
			List<Enrollee> householdResponsible= enrollmentService.getHouseholdContactResponsiblePerson(member.getMemberId(), enrollees);
			if(householdResponsible!=null && householdResponsible.size()>0){
				
				for(Enrollee enrollee : householdResponsible){
					if(householdPassed &&EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())){
						continue;
					}
					if(responsiblePassed &&EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())){
						continue;
					}
					if(isNotNullAndEmpty(member.getFirstName()) ){
						enrollee.setFirstName(member.getFirstName());
						enrollee.setMiddleName(member.getMiddleName());
					}
					if(isNotNullAndEmpty(member.getLastName()) ){
						enrollee.setLastName(member.getLastName());
					}
					/*if(isNotNullAndEmpty(member.getMiddleName()) ){
						enrollee.setMiddleName(member.getMiddleName());
					}*/
					
					//HIX-114986 Directly set suffix to request value
					enrollee.setSuffix(member.getSuffix());
					
					if(isNotNullAndEmpty(member.getSsn()) ){
						enrollee.setTaxIdNumber(member.getSsn());
					}
					
					if(isNotNullAndEmpty(member.getDob())){
						enrollee.setBirthDate(EnrollmentUtils.validateDOB(member.getDob()));
					}
					
						if(isNotNullAndEmpty(member.getMailingAddress1())){
							Location mailingAddress = enrollee.getMailingAddressId();
							if(mailingAddress == null){
								mailingAddress = new Location();
							}
							mailingAddress.setAddress1(member.getMailingAddress1());
							mailingAddress.setAddress2(member.getMailingAddress2());
							if(isNotNullAndEmpty(member.getMailingCity())){
								mailingAddress.setCity(member.getMailingCity());
							}

							if(isNotNullAndEmpty(member.getMailingState())){
								mailingAddress.setState(member.getMailingState());
							}

							if(isNotNullAndEmpty(member.getMailingZip())){
								mailingAddress.setZip(member.getMailingZip());
							}else{
								throw new GIException(EnrollmentConstants.ERROR_CODE_222,EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE, EnrollmentConstants.HIGH);
							}

							enrollee.setMailingAddressId(mailingAddress);
						}


					if(isNotNullAndEmpty(member.getHomeAddress1())){
						Location homeAddress = enrollee.getHomeAddressid();
						if(homeAddress == null ){
							homeAddress = new Location();
						}
						homeAddress.setAddress1(member.getHomeAddress1());
						homeAddress.setAddress2(member.getHomeAddress2());
						if(isNotNullAndEmpty(member.getHomeCity())){
							homeAddress.setCity(member.getHomeCity());
						}
						if(isNotNullAndEmpty(member.getHomeState())){
							homeAddress.setState(member.getHomeState());
						}
						
						if(isNotNullAndEmpty(member.getHomeZip())){
							homeAddress.setZip(member.getHomeZip());
						}else{
							throw new GIException(EnrollmentConstants.ERROR_CODE_222,EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE, EnrollmentConstants.HIGH);
						}
						
						if(isNotNullAndEmpty(member.getHomeCountyCode())){
							homeAddress.setCountycode(member.getHomeCountyCode());
						}else{
							throw new GIException(EnrollmentConstants.ERROR_CODE_223,EnrollmentConstants.ERR_MSG_COUNTY_CODE_VALUE, EnrollmentConstants.HIGH);
						}


						enrollee.setHomeAddressid(homeAddress);
						
						if(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode())){
							String householdContactName = concatEnrolleeName(enrollee);
							if(isNotNullAndEmpty(householdContactName)){
								enrollment.setSponsorName(householdContactName);
							}

							if(isNotNullAndEmpty(enrollee.getTaxIdNumber())){
								enrollment.setSponsorTaxIdNumber(enrollee.getTaxIdNumber());
							}
						}
						
					}
					if(!enrollee.isEventCreated()){
						LOGGER.info("Creating event for household/responsible peron enrollee id :: " + enrollee.getId() + " time :: " + new Date() + " enrollment :: " + enrollment.getId() + " member ID :: " + enrollee.getExchgIndivIdentifier());
						enrollmentService.createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE, mrc, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.ADMIN_UPDATE);
						enrollee.setEventCreated(true);
					}
					
					
				}
				
			}
		}
	}
	
	@Override
	@Transactional(rollbackFor={InvalidUserException.class, GIException.class, Exception.class},propagation=Propagation.REQUIRED) 
	public void adminUpdateService(AdminUpdateIndividualRequest adminUpdateIndividualRequest, Long updatedLocationId, Boolean isLocationIdUpdated, Boolean isOnlySsapAppIdUpdated, EnrollmentResponse enrlResp) throws GIException{
		AccountUser userObj = null;
		if( (null != isLocationIdUpdated && isLocationIdUpdated) && (null != updatedLocationId && updatedLocationId > 0) ){
			enrlResp = enrollmentService.updateMailByHouseHoldId(Long.toString(adminUpdateIndividualRequest.getHousehold().getHouseholdCaseId()), 
					                                                                          updatedLocationId);
			if(enrlResp!=null && enrlResp.getStatus()!=null && enrlResp.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
				throw new GIException(enrlResp.getErrCode(),enrlResp.getErrMsg(), EnrollmentConstants.HIGH);
			}
		}

		List<Enrollment> enrollmentList = null;
		Integer enrollmenId =-1;

		if(isNotNullAndEmpty(adminUpdateIndividualRequest.getHousehold().getEnrollmentId())){
			enrollmenId =(int) adminUpdateIndividualRequest.getHousehold().getEnrollmentId();
		}

		Long ssapId = adminUpdateIndividualRequest.getHousehold().getSsapApplicationid();

		if(isNotNullAndEmpty(ssapId)&&ssapId>0 ){
			/*
			 * HIX-78557
			 */
			/*enrollmentList= enrollmentService.findBySsapApplicationid(ssapId);*/
			enrollmentList = enrollmentService.getEnrollmentNotInCancelAndAbortBySsapApplicationId(ssapId);

		}else if(isNotNullAndEmpty(enrollmenId) && enrollmenId>0){
			Enrollment enrollment = null;
			enrollment = enrollmentService.findById(enrollmenId);
			if(enrollment != null){
				enrollmentList = new ArrayList<Enrollment>();
				enrollmentList.add(enrollment);
			}
		}else if(!isLocationIdUpdated){
			LOGGER.info("IND 57 : Either Enrollment Id or SSAP Application Id is required ");
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_203);
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrlResp.setErrMsg("IND 57 : Either Enrollment Id or SSAP Application Id is required");
			/*return xstream.toXML(enrlResp);*/
			throw new GIException("IND 57 : Either Enrollment Id or SSAP Application Id or is required");
		}
		
		if((enrollmentList == null || enrollmentList.isEmpty())&& !isLocationIdUpdated){
			LOGGER.info(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID);
			enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_204);
			enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID);
			enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
			throw new GIException(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID);
		}
		else if(enrollmentList != null && !enrollmentList.isEmpty()){
			try {
				userObj = userService.getLoggedInUser();
				
				boolean isShop=false;
				
				//boolean isCA= EnrollmentConfiguration.isCaCall();
				
				for(Enrollment enrollment : enrollmentList){
					LOGGER.info("Admin update start for enrollment :: " + enrollment.getId());
					adminUpdateEnrollment(adminUpdateIndividualRequest,enrollment,userObj,  EnrollmentConfiguration.isCaCall());
					if(enrollment.getEnrollmentTypeLkp()!=null && EnrollmentConstants.ENROLLMENT_TYPE_SHOP.equalsIgnoreCase( enrollment.getEnrollmentTypeLkp().getLookupValueCode() )){
						isShop=true;
					}
				}
				enrollmentList = enrollmentService.saveAllEnrollment(enrollmentList);

				
				if(isShop){
					// SENDING EMAIL NOTIFICATION TO THE EMPLOYEE ON THE ENROLLMENT UPDATE
					enrollmentEmailNotificationService.sendEnrolledEmployeeHouseHoldInfoUpdateNotification(enrollmentList);
				}
				
			} catch (InvalidUserException iue) {
				if(enrlResp==null || enrlResp.getErrCode()==0){
				LOGGER.error("Invalid User.:: " + iue.getMessage() + EnrollmentConstants.CAUSE + iue.getCause() , iue);
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_205);
				enrlResp.setErrMsg("Invalid User." + iue.getMessage() + EnrollmentConstants.CAUSE + iue.getCause());
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				throw new GIException(EnrollmentConstants.ERROR_CODE_205,"Invalid User." + iue.getMessage() + EnrollmentConstants.CAUSE + iue.getCause(), EnrollmentConstants.HIGH);
			}catch (GIException ge) {
				
				if(enrlResp!=null && enrlResp.getErrCode()==0){
				LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause() , ge);
				enrlResp.setErrCode(ge.getErrorCode()!=0?ge.getErrorCode():EnrollmentConstants.ERROR_CODE_206);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause());
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				throw new GIException(ge.getErrorCode()!=0?ge.getErrorCode():EnrollmentConstants.ERROR_CODE_206,EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause(), EnrollmentConstants.HIGH);
			} 	
			catch (Exception ge) {
				if(enrlResp!=null && enrlResp.getErrCode()==0){
				LOGGER.error(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause() , ge);
				enrlResp.setErrCode(EnrollmentConstants.ERROR_CODE_206);
				enrlResp.setErrMsg(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause());
				enrlResp.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				throw new GIException(EnrollmentConstants.ERR_MSG_ENROLLMENT_UPDATE_FAIL + " :: " + ge.getMessage() + EnrollmentConstants.CAUSE + ge.getCause());
			}
		}
	}
	
	@Override
	public String updateExternalAppID(Long ssapApplicationId, List<String> issuerIds) {
		try {
			AccountUser user = userService.findByEmail("exadmin@ghix.com");
			List<Enrollment> enrollmentList = enrollmentService
					.findActiveEnrollmentBySsapApplicationID(ssapApplicationId);

			for (Enrollment enrollment : enrollmentList) {
				if (!CollectionUtils.isEmpty(issuerIds) && enrollment.getIssuerId() != null && !issuerIds.contains(enrollment.getIssuerId().toString()))
				{
					continue;
				}
				List<Enrollee> enrolleeList = enrollment.getEnrollees();
				for (Enrollee enrollee : enrolleeList) {
					List<EnrollmentEvent> enrollmentEventList = enrollee.getEnrollmentEvents();
					
					if(null!=enrollee.getExchgIndivIdentifier() 
							&& null!=enrollee.getExternalIndivId() 
							&& !enrollee.getExchgIndivIdentifier().equals(enrollee.getExternalIndivId())) {
						populateEnrollmentEvents(enrollment, enrollee, "25", user, enrollmentEventList);
					}else {
						populateEnrollmentEvents(enrollment, enrollee, "AI", user, enrollmentEventList);
					}
				}

				enrollmentService.saveEnrollment(enrollment);
			}

			return "SUCCESS";

		} catch (Exception e) {
			LOGGER.error("Could not Update enrollment" + e);
		}
		return "FAILURE";
	}
}
