package com.getinsured.hix.enrollment.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.JiraUtil;
/**
 * 
 * @author meher_a
 *
 */
@Service("enrollmentAsyncService")
@Transactional
public class EnrollmentAsyncServiceImpl implements EnrollmentAsyncService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAsyncServiceImpl.class);
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private UserService userService;
	@Autowired private IEnrollmentRepository enrollmentRepository;

	@Override
	public void enrollmentAsyncTerminatorTask(String cmsPlanId,String coverageYear, String terminationDate) {
		AccountUser user = getLoggedInUser();
		Map<Integer,String> exceptionStackMap = new HashMap<>();
		
		List<Enrollment> enrollmentList = enrollmentRepository.getEnrollmentByCmsPlanId(cmsPlanId, coverageYear, terminationDate);
		EnrollmentDisEnrollmentDTO disEnrollmentDTO = null;
		
		if(enrollmentList != null &&  !enrollmentList.isEmpty()){
			for (Enrollment enrollment : enrollmentList) {
				disEnrollmentDTO = new EnrollmentDisEnrollmentDTO();
				
				// Set each Enrollment as List to be disenrolled
				disEnrollmentDTO.setEnrollmentList(Arrays.asList(enrollment));

				disEnrollmentDTO.setTerminationDate(terminationDate);
				disEnrollmentDTO.setTerminationReasonCode(String.valueOf(EnrollmentConstants.TWENTY_FIVE));
				disEnrollmentDTO.setDeathDate(null);
				
				// Set Updated by as Logged In user
				disEnrollmentDTO.setUpdatedBy(user);
				disEnrollmentDTO.setAllowRetroTermination(true);
				try{
					enrollmentService.disEnrollEnrollment(disEnrollmentDTO,EnrollmentEvent.TRANSACTION_IDENTIFIER.ASYNC_TERMINATOR);
				}catch (Exception exception){
					exceptionStackMap.put(enrollment.getId(), EnrollmentUtils.shortenedStackTrace(exception, 4));
				}
			}
			
			if(exceptionStackMap.size() > 0){
				logBug(exceptionStackMap);
			}
			
		}else{
			LOGGER.info(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_PLAN_ID + cmsPlanId +" | coverage_year = "+coverageYear+ "| decertified_date = " +terminationDate);
		}

	}
	
	private AccountUser getLoggedInUser() {
		try {
			return userService.getLoggedInUser();
		} catch (InvalidUserException e) {
			LOGGER.info(" InvalidUserException "+e.getMessage());
		}
		return null;
	}

	private void logBug(Map<Integer, String> exceptionStackMap){
		StringBuilder descriptionstringBuilder = new StringBuilder();
		for (Map.Entry<Integer, String> entry : exceptionStackMap.entrySet()) {
			descriptionstringBuilder.append(" Exception while Disenrolling Enrollment with Id : "+ entry.getKey()+"\n"
		                         +" Exception Stack : "+ entry.getValue());
			descriptionstringBuilder.append("\n");
		}
		
		String enrollment_component = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		String fix_version = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
		
		JiraUtil.logBug(Arrays.asList(enrollment_component), Arrays.asList(fix_version), descriptionstringBuilder.toString(), "Fail to disEnroll Enrollment for given cmsPlanId, coverageYear and terminationDate",null);
	}

}
