package com.getinsured.hix.enrollment.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collection;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * 
 * @author rajaramesh_g
 *
 */
public final class EnrollmentUtils {
	
	private static Gson gsonStatic;
	/**
	 * Private constructor.
	 * Utility classes should not have public constructor, Added to hide implicit public constructor.
	 * 
	 */
	private EnrollmentUtils(){

	}
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentUtils.class);

	/**@param e
	 * @return boolean
	 * the below is takes input as object type (e) like (Integer, String, Character, List<String>, List<Integer>...etc)
	 * and return true when the input object is not null/empty/"null" else return false"
	 **/
	public static <T> boolean isNotNullAndEmpty(T e){
		boolean isNotNUll=false;
		if((e!=null) && !(e.toString().isEmpty()) && !(e.toString().equalsIgnoreCase("null"))){
			isNotNUll=true;
		}
		return isNotNUll;
	}


	/**
	 * Converts the string to java.util.Date
	 * 
	 * @param dateStr
	 * @param format
	 * @return java.util.Date
	 */
	public static Date StringToEODDate(String dateStr, String format) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	    dateFormat.setLenient(false);
	    Date convertedDate = null;
		try {
			convertedDate = dateFormat.parse(dateStr);
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(convertedDate);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.TWENTY_THREE);
			cal.set(Calendar.MINUTE, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.SECOND, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.NINE_NINETY_NINE);
			convertedDate=cal.getTime();
		} catch (Exception e) {
			LOGGER.error("Exception",e);
		}
	    return convertedDate;
	}
	

	public static Date getEODDate(Date date){
		if(date!=null){
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.TWENTY_THREE);
			cal.set(Calendar.MINUTE, EnrollmentConstants.FIFTY_NINE);
	 	 	cal.set(Calendar.SECOND, EnrollmentConstants.FIFTY_NINE);
	 	 	cal.set(Calendar.MILLISECOND, EnrollmentConstants.NINE_NINETY_NINE);
	 	 	date = cal.getTime();
		}
		return date;
	}
	/**@param e
	 * @return boolean
	 * the below is takes input as object type (e) like (Integer, String, Character, List<String>, List<Integer>...etc)
	 * and return true when the input object is not null else return false"
	 **/
	public static <T> boolean isNotNull(T e){
		boolean isNotNUll=false;
		if((e!=null)){
			isNotNUll=true;
		}
		return isNotNUll;
	}

	/**@param <V>
	 * @param <K>
	 * @param e
	 * @return boolean
	 * the below is takes input as object type (<K,V>) like (Map<Integer, String>, Map<String, String>.... etc)
	 * and return true when the input object (<K,V>) is not null/empty/"null" else return false"
	 **/
	public static <K,V> boolean isMapNotNullAndEmpty(Map<K,V> e){
		boolean isNotNUll=false;
		if(e != null && !e.isEmpty()){
			isNotNUll=true;
		}
		return isNotNUll;
	}

	/**@param <V>
	 * @param <K>
	 * @param e
	 * @return boolean
	 * the below is takes input as object type (<K,V>) like (Map<Integer, String>, Map<String, String>.... etc)
	 * and return true when the input object is not null else return false"
	 **/
	public static <K,V> boolean isMapNotNull(Map<K,V> e){
		boolean isNotNUll=false;
		if((e!=null)){
			isNotNUll=true;
		}
		return isNotNUll;
	}

	/**
	 * @author raja
	 * @since 10/05/2013
	 * 
	 * This method validates the date of birth with current date and 
	 * also it converts the date format from String to java util date
	 * 
	 * @param date
	 * @return
	 * @throws GIException
	 */
	public static Date validateDOB(String date) throws GIException 
	{
		Date currentDate = new TSDate();
		Date dob=null;
		try{
			if(!DateUtil.isValidDate(date, GhixConstants.REQUIRED_DATE_FORMAT)){
				throw new GIException(GhixErrors.INVALID_DATE_OF_BIRTH);
			}
			dob= DateUtil.StringToDate(date, GhixConstants.REQUIRED_DATE_FORMAT);
			if (!(currentDate.compareTo(dob) > EnrollmentConstants.ZERO))
			{
				throw new GIException(GhixErrors.INVALID_DATE_OF_BIRTH);
			}
		}catch(Exception e){
			LOGGER.error("Invalid date of birth" , e);
			throw new GIException(GhixErrors.INVALID_DATE_OF_BIRTH);
		}
		return dob;
	}

	/**
	 * @author raja
	 * @since 20/08/2013
	 * 
	 * This method checks the given date with the in start date and 
	 * and the end date.
	 * 
	 * @param startDate, endDate, givenDate
	 * @return true/false
	 * 
	 */

	public static boolean isDateInBetweenStartDateAndEndDate(Date startDate, Date endDate, Date givenDate) {

		boolean isDateBetween=false;
		if (givenDate.after(startDate) && givenDate.before(endDate)) {
			isDateBetween= true;
		}
		else if (givenDate.equals(startDate) || givenDate.equals(endDate)) {
			isDateBetween= true;
		}
		return isDateBetween;
	}

	public static String getTemplateHeader(String templateLocation) throws GIException {
		InputStream inputStreamUrl = null;
		BufferedReader bis = null;
		String inputStreamString=null;
		StringBuilder templateContent = new StringBuilder();
		try {
			inputStreamUrl = new FileInputStream(templateLocation);
			bis = new BufferedReader(new InputStreamReader(inputStreamUrl));
			while((inputStreamString = bis.readLine())!=null){
				templateContent.append(inputStreamString);
			}
			return populateHeaderFooterTemplate(templateContent.toString());			
		} catch (FileNotFoundException fnfe) {
			LOGGER.error("Error in reading template from:",templateLocation);
			throw new GIException("Error in getTemplateHeader ::"+fnfe.getCause(),fnfe);
		} catch (IOException ioe) {
			LOGGER.error("Error in reading template from:",templateLocation);
			throw new GIException("Error in getTemplateHeader ::"+ioe.getCause(),ioe);
		}catch (Exception e) {
			LOGGER.error("Error while populating the template:",e);
			throw new GIException("Error in getTemplateHeader ::"+e.getCause(),e);
		}finally
		{
			IOUtils.closeQuietly(inputStreamUrl);
			IOUtils.closeQuietly(bis);
		}	
	}

	public static String populateHeaderFooterTemplate(String templateContent) throws GIException
	{
		Map<String, Object> templateTokens = new HashMap<String, Object> ();
		templateTokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		templateTokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		templateTokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		templateTokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		templateTokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		templateTokens.put(TemplateTokens.HOST, GhixEndPoints.GHIXWEB_SERVICE_URL);

		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();

		try {
			stringLoader.putTemplate("noticeTemplate", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			Template tmpl = templateConfig.getTemplate("noticeTemplate");
			tmpl.process(templateTokens, sw);

		} catch (Exception e) {
			LOGGER.error("Exception found while populating Header Template", e);
			throw new GIException("Error in populateHeaderFooterTemplate :: "+e.getCause(),e);
		}
		finally{
			IOUtils.closeQuietly(sw);
		}		
		return sw.toString();
	}

	/**
	 * @author parhi_s
	 * @since 25th Feb 2014
	 * @param str
	 * @return true/false
	 */
	public static boolean isNumeric(String str)  
	{  
		try  
		{  
			Double.parseDouble(str);  
		}  
		catch(NumberFormatException e)  
		{  
			return false;  
		}  
		return true;  
	}

	/**
	 * @author parhi_s
	 * Checks if date2 is greater or equal date1(time in a day not considered)
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isEqualOrAfter(Date date1, Date date2){
		boolean isEqualOrBefore = false;
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

			Date date3 = formatter.parse(formatter.format(date1));
			Date date4 = formatter.parse(formatter.format(date2));

			if(date4.compareTo(date3) >= EnrollmentConstants.ZERO ){
				isEqualOrBefore= true;
			}
		}catch(Exception e){
			LOGGER.error("Error parsing date" , e);
			isEqualOrBefore = false;
		}
		return isEqualOrBefore;
	}

	/**
	 * @author parhi_s
	 * 
	 * @param date
	 * @return
	 */
	public static Date getBeforeDayDate(Date date) {
		Date prevDay = null;
		if (date != null) {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, -EnrollmentConstants.ONE);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.TWENTY_THREE);
			cal.set(Calendar.MINUTE, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.SECOND, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.NINE_NINETY_NINE);
			prevDay = cal.getTime();
		}
		return prevDay;
	}
	
	/**
	 * @author parhi_s
	 * 
	 * @param date
	 * @return
	 */
	public static Date getBeforeDayDate(Date date, int days) {
		Date prevDay = null;
		if (date != null) {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, -days);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.TWENTY_THREE);
			cal.set(Calendar.MINUTE, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.SECOND, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.NINE_NINETY_NINE);
			prevDay = cal.getTime();
		}
		return prevDay;
	}

	/**
	 * @author parhi_s
	 * 
	 * @param date
	 * @return
	 */
	public static Date getMonthEndDate(Date date) {
		Date lastDay = null;
		if (date != null) {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MONTH, EnrollmentConstants.ONE);  
			cal.set(Calendar.DAY_OF_MONTH, EnrollmentConstants.ONE);  
			cal.add(Calendar.DATE, - EnrollmentConstants.ONE);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.TWENTY_THREE);
			cal.set(Calendar.MINUTE, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.SECOND, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.NINE_NINETY_NINE);
			lastDay = cal.getTime();
		}
		return lastDay;
	}

	/**
	 * @author parhi_s
	 * @param date
	 * @return
	 */


	public static Date getNextDayDate(Date date){
		Date nextDay=null;
		if(date!=null){
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, + EnrollmentConstants.ONE);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.ZERO);
			cal.set(Calendar.MINUTE, EnrollmentConstants.ZERO);
			cal.set(Calendar.SECOND, EnrollmentConstants.ZERO);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.ZERO);
			nextDay = cal.getTime();
		}
		return nextDay;
	}
	
	/**
	 * @author parhi_s
	 * @param date
	 * @return
	 */
	
	public static Date getNextToNextMonthStartDate(Date date){
		Date nextMonthStartDate=null;
		if(date!=null){
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MONTH, +EnrollmentConstants.TWO);
			cal.set(Calendar.DATE, EnrollmentConstants.ONE);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.ZERO);
			cal.set(Calendar.MINUTE, EnrollmentConstants.ZERO);
			cal.set(Calendar.SECOND, EnrollmentConstants.ZERO);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.ZERO);
			nextMonthStartDate = cal.getTime();
		}
		return nextMonthStartDate;
	}

	
	/**
	 * @author parhi_s
	 * @param date
	 * @return
	 */
	
	public static Date getNextMonthStartDate(Date date){
		Date nextMonthStartDate=null;
		if(date!=null){
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MONTH, +EnrollmentConstants.ONE);
			cal.set(Calendar.DATE, EnrollmentConstants.ONE);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.ZERO);
			cal.set(Calendar.MINUTE, EnrollmentConstants.ZERO);
			cal.set(Calendar.SECOND, EnrollmentConstants.ZERO);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.ZERO);
			nextMonthStartDate = cal.getTime();
		}
		return nextMonthStartDate;
	}

	/**
	 * @author parhi_s
	 * @param date
	 * @return
	 */


	public static Date getYearEndDate(Date date){
		Date yearEnd=null;
		if(date!=null){
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.DATE, EnrollmentConstants.THIRTY_ONE);
			cal.set(Calendar.MONTH, Calendar.DECEMBER);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.TWENTY_THREE);
			cal.set(Calendar.MINUTE, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.SECOND, EnrollmentConstants.FIFTY_NINE);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.NINE_NINETY_NINE);
			yearEnd = cal.getTime();
		}
		return yearEnd;
	}
	
	/**
	 * Returns the start time of the Last Month 
	 * 
	 * @return Date
	 */
	public static Date getLastMonthStartDateTime() throws GIException{
		Date startDate=null;
		try{
			Calendar today = TSCalendar.getInstance();
			today.add(Calendar.MONTH, -1);
			today.set(Calendar.DATE, today.getMinimum(Calendar.DATE));
			today.set(Calendar.HOUR_OF_DAY,today.getMinimum(Calendar.HOUR_OF_DAY));
			today.set(Calendar.MINUTE, today.getMinimum(Calendar.MINUTE));
			today.set(Calendar.SECOND, today.getMinimum(Calendar.SECOND));
			
			startDate= today.getTime();
		}catch(Exception e){
			throw new GIException("Error in getLastMonthStartDateTime()::"+e.getMessage(),e);
		}
		return startDate;
	}
	
	/**
	 * Returns the start time of the current Month 
	 * 
	 * 
	 * @return Date
	 */
	public static  Date getCurrentMonthStartDateTime() throws GIException{
		Date startDate = null;
		try{
			Calendar today = TSCalendar.getInstance();
			today.set(Calendar.DATE, today.getMinimum(Calendar.DATE));
			today.set(Calendar.HOUR_OF_DAY,today.getMinimum(Calendar.HOUR_OF_DAY));
			today.set(Calendar.MINUTE, today.getMinimum(Calendar.MINUTE));
			today.set(Calendar.SECOND, today.getMinimum(Calendar.SECOND));

			startDate= today.getTime();
		}catch(Exception e){
			throw new GIException("Error in getCurrentMonthStartDateTime()::"+e.getMessage(),e);
		}
		return startDate;
	}
	
	/**
	 * Converts provided  {@link Calendar} link XMLGregorianCalendar} to {@link XMLGregorianCalendar} 
	 * 
	 * @author Aditya-S
	 * @since 30-07-2014
	 * @param calender
	 * @return
	 */
	public static XMLGregorianCalendar toXMLGregorianCalendarDateOnly(Calendar calender){
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeInMillis(calender.getTimeInMillis());
		XMLGregorianCalendar xmlGregorianCalendar = null;
		try {
			// need to add 1 in month as Calender month is zero base and XMLGregorianCalendar month is 1 base.
			xmlGregorianCalendar  = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gregorianCalendar.get(Calendar.YEAR),gregorianCalendar.get(Calendar.MONTH)+1,gregorianCalendar.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException dex) {
			LOGGER.error("DatatypeConfigurationException @ enrollmentXMLGroupJob batch" , dex);
		}
		return xmlGregorianCalendar ;
	}
	
	/**
	 * 
	 * @author Aditya-S
	 * @since 01-08-2014
	 * 
	 * Returns 1St day of Current year e.g (1st Jan 2014 00:00:00 for year 2014)
	 * 
	 * @return
	 */
	public static Date getCurrentYearStartDate(){
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, calenderObj.getMinimum(Calendar.MONTH));
		return calenderObj.getTime();
	}
	
	/**
	 * 
	 * @author Aditya-S
	 * @since 01-08-2014
	 * 
	 * Returns 1st day of given month. e.g (1st Aug 2014 00:00:00 for year 2014 and month 7)
	 * NOTE:- The month index is from (0-11)
	 * 
	 * @param month
	 * @return
	 */
	public static Date getStartDateForMonth(int month){
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MILLISECOND, calenderObj.getMinimum(Calendar.MILLISECOND));
		calenderObj.set(Calendar.MONTH, month);
		return calenderObj.getTime();
	}
	
	/**
	 * 
	 * @author Aditya-S
	 * @since 01-08-2014
	 * 
	 * Returns 1st day of Current Year. e.g (1st JAN 2014 00:00:00 for year 2014)
	 * 
	 * @param month
	 * @return
	 */
	public static Date getStartDateForCurrentYear(){
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, calenderObj.getMinimum(Calendar.MONTH));
		return calenderObj.getTime();
	}
	
	/**
	 * 
	 * @author Aditya-S
	 * @since 01-08-2014
	 * 
	 * Returns 1st day of Next Month for month provided. e.g (1st Aug 2014 00:00:00 for year 2014 and month 7)
	 * NOTE:- The month index is from (0-11)
	 * 
	 * @param month
	 * @return
	 */
	public static Date getStartDateForNextMonth(int month){
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, month + 1);
		return calenderObj.getTime();
	}
	
	
	/**
	 * @author Aditya-S
	 * 
	 * Returns count of Active enrollees for given {@linkplain List<Enrollee>}.
	 * 
	 * @return activeEnrolleeCount
	 */
	public static int getActiveEnrolleesCount(List<Enrollee> enrollees){
		int activeEnrolleeCount = 0;
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED))){
					activeEnrolleeCount++;
				}
			}
		}
		return activeEnrolleeCount;
	}

	public static List<Date> getStartEndDateList(Date date) {
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.setTime(date);
		
		List<Date> dateList = new ArrayList<Date>();
		
		if(calenderObj.get(Calendar.DATE) <= EnrollmentConstants.FIVE){
			
			Calendar startDate = TSCalendar.getInstance();
			startDate.set(Calendar.DAY_OF_MONTH, EnrollmentConstants.FIVE);
			startDate.set(Calendar.MONTH, calenderObj.get(Calendar.MONTH)-1);
			startDate.set(Calendar.YEAR, calenderObj.get(Calendar.YEAR));
			startDate.set(Calendar.HOUR, calenderObj.getMinimum(Calendar.HOUR));
			startDate.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
			startDate.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
			
			dateList.add(startDate.getTime());
			
			Calendar endDate = TSCalendar.getInstance();
			endDate.set(Calendar.DAY_OF_MONTH, EnrollmentConstants.FIVE);
			endDate.set(Calendar.MONTH, calenderObj.get(Calendar.MONTH));
			endDate.set(Calendar.YEAR, calenderObj.get(Calendar.YEAR));
			endDate.set(Calendar.HOUR, calenderObj.getMinimum(Calendar.HOUR));
			endDate.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
			endDate.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
			
			dateList.add(endDate.getTime());
			
		}else{
			
			Calendar startDate = TSCalendar.getInstance();
			startDate.set(Calendar.DAY_OF_MONTH, EnrollmentConstants.FIVE);
			startDate.set(Calendar.MONTH, calenderObj.get(Calendar.MONTH));
			startDate.set(Calendar.YEAR, calenderObj.get(Calendar.YEAR));
			startDate.set(Calendar.HOUR, calenderObj.getMinimum(Calendar.HOUR));
			startDate.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
			startDate.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
			
			dateList.add(startDate.getTime());
			
			Calendar endDate = TSCalendar.getInstance();
			endDate.set(Calendar.DAY_OF_MONTH, EnrollmentConstants.FIVE);
			endDate.set(Calendar.MONTH, calenderObj.get(Calendar.MONTH)+1);
			endDate.set(Calendar.YEAR, calenderObj.get(Calendar.YEAR));
			endDate.set(Calendar.HOUR, calenderObj.getMinimum(Calendar.HOUR));
			endDate.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
			endDate.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
			
			dateList.add(endDate.getTime());
			
		}
		
		return dateList;
	}

	/**
	 * 
	 * Method Returns String seperated by provided seperator.
	 * 
	 * @param collection
	 * @param separator
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static String getSeperatedStringFromCollection(Collection collection, String separator) {
		// handle null, zero and one elements before building a buffer
		if (collection == null) {
			return null;
		}
		Iterator iterator = collection.iterator();

		if (!iterator.hasNext()) {
			return "";
		}
		Object first = iterator.next();
		if (!iterator.hasNext()) {
			return first.toString();
		}

		// two or more elements
		StringBuilder buf = new StringBuilder(256); // Java default is 16, probably too small
		if (first != null) {
			buf.append(first);
		}

		while (iterator.hasNext()) {
			buf.append(separator);
			Object obj = iterator.next();
			if (obj != null) {
				buf.append(obj);
			}
		}
		return buf.toString();
	}
	
	/**
	 * Truncate the Float number to required decimal places.
	 * 
	 * @author Aditya_S
	 * @since 12-17-2014
	 * 
	 * @author panda_p
	 * @since 27/09/2016
	 * Modified to round_up
	 *  
	 * @param number
	 * @param decimalPlace
	 * @return
	 */
	public static float precision(float number, int decimalPlace) {
		return round(number,decimalPlace);
	}
	
	/**
	 * 
	 * @author Aditya-S
	 * @since 01-08-2014
	 * 
	 * Returns 1st day of Next Month for month provided. e.g (1st Aug 2014 00:00:00 for year 2014 and month 7)
	 * NOTE:- The month index is from (0-11)
	 * 
	 * @param month
	 * @param year
	 * @return
	 */
	public static Date getMonthEndDate(int month,int year){
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, month + 1);
		calenderObj.set(Calendar.YEAR, year);
		calenderObj.add(Calendar.DATE, -1); 
		return calenderObj.getTime();
	}
	
	public static  Date getMonthStartDateTime(int month,int year) throws GIException{
		Date startDate = null;
		try{
			  GregorianCalendar currentMonthStartDate = new GregorianCalendar();
			  currentMonthStartDate.setLenient(false);
			  currentMonthStartDate.set(GregorianCalendar.YEAR, year);
			  currentMonthStartDate.set(GregorianCalendar.MONTH, month);
			  currentMonthStartDate.set(GregorianCalendar.DATE, 1);
			  currentMonthStartDate.set(Calendar.HOUR, 0);
			  currentMonthStartDate.set(Calendar.MINUTE, 0);
			  currentMonthStartDate.set(Calendar.SECOND, 0);
			  currentMonthStartDate.set(Calendar.HOUR_OF_DAY, 0);
		       
			  startDate= currentMonthStartDate.getTime();
		}catch(Exception e){
			throw new GIException("Error in getCurrentMonthStartDateTime()::"+e.getMessage(),e);
		}
		return startDate;
	}
	
	public static  Date getMonthStartDate(int month,int year) throws GIException{
		Date startDate = null;
			  Calendar currentMonthStartDate = TSCalendar.getInstance();
			  currentMonthStartDate.setLenient(false);
			  currentMonthStartDate.set(Calendar.YEAR, year);
			  currentMonthStartDate.set(Calendar.MONTH, month);
			  currentMonthStartDate.set(Calendar.DATE, 1);
			  currentMonthStartDate.set(Calendar.HOUR, 0);
			  currentMonthStartDate.set(Calendar.MINUTE, 0);
			  currentMonthStartDate.set(Calendar.SECOND, 0);
			  currentMonthStartDate.set(Calendar.HOUR_OF_DAY, 0);
			  currentMonthStartDate.set(Calendar.MILLISECOND, currentMonthStartDate.getMinimum(Calendar.MILLISECOND));
		       
			  startDate= currentMonthStartDate.getTime();
		
		return startDate;
	}
	/**
	 * 
	 * @author Aditya-S
	 * @since 01-08-2014
	 * 
	 * Returns 1St day of Current year e.g (1st Jan 2014 00:00:00 for year 2014)
	 * 
	 * @return
	 */
	public static Date getCurrentYearStartDate(int year){
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, calenderObj.getMinimum(Calendar.MONTH));
		calenderObj.set(Calendar.YEAR, year);
		return calenderObj.getTime();
	}
	
	
	/**
	 * Convert XMLGregorianCalendar date to formatted YYYY-MM-DD  XMLGregorianCalendar date
	 * @param XMLGregorianCalendar date
	 * @return XMLGregorianCalendar 
	 */
	public static  XMLGregorianCalendar getFormattedXmlGregorianCalendarDate(XMLGregorianCalendar date) {
		
		XMLGregorianCalendar xmlGregorianDate = null;
		if(null != date){
			GregorianCalendar cal = new GregorianCalendar();
			
			cal.setTime(date.toGregorianCalendar().getTime());
			try {
				xmlGregorianDate = DatatypeFactory.newInstance()
						.newXMLGregorianCalendarDate(cal.get(Calendar.YEAR),
								cal.get(Calendar.MONTH) + 1,
								cal.get(Calendar.DAY_OF_MONTH),
								DatatypeConstants.FIELD_UNDEFINED);
			} catch (DatatypeConfigurationException e) {
				LOGGER.error("Error converting to XMLGregorianCalendar Date", e);
			}
		}
		return xmlGregorianDate;
	}
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to get all the files of a specific type from a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
	public static File[] getFilesInAFolder(String folderName, String fileType) throws GIException{
		try{
		final String fileExtn=fileType;
		File folder= new File(folderName);
		File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn)){
					return true;
				}
				return false;
			}
		});
		return listOFFiles;
		}catch(Exception e){
			LOGGER.error("Error in getFilesInAFolder"+e.getMessage(),e);
			throw new GIException("Error in getFilesInAFolder"+e.getMessage(),e);
		}
		
	}
	
	/**
	 * @author ajinkya_m
	 * @since
	 * This method is used to get files of a specific type from a folder
	 * @param file name list
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
	public static File[] getFilesFromDirectory(String folderName, List<String> fileNameList) throws GIException{
		try{
		File folder= new File(folderName);
		File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && fileNameList.contains(pathname.getName())){
					return true;
				}
				return false;
			}
		});
		
		return listOFFiles;
		
		}catch(Exception e){
			LOGGER.error("Error in getFilesFromDirectory"+e.getMessage(),e);
			throw new GIException("Error in getFilesFromDirectory"+e.getMessage(),e);
		}
		
	}
	
    //Method to move files from one folder to another folder. If lof is null then it will move all files from source to target folder else it moves the list of files
    public static void moveFiles(String sourceFolder, String targetFolder, String fileType, List<File> lof) throws GIException{
          try{
        	  if (sourceFolder!=null && !("".equals(sourceFolder.trim())) &&!(new File(sourceFolder).exists())) {
                  return;
             }
             if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
                  new File(targetFolder).mkdirs();
             }
             
             if(lof != null && !lof.isEmpty()){
                  for(File file:lof){
                       file.renameTo(new File(targetFolder+File.separator+file.getName()));
                  }
             }else{
                  //move all files from source to target folder
                  File[] files= EnrollmentUtils.getFilesInAFolder(sourceFolder,fileType);
                  if(files!=null && files.length>0){
                	  List<File> loFiles=Arrays.asList(files);
                	  moveFiles(sourceFolder,targetFolder,fileType, loFiles);
                  }
             }
          }catch(Exception e){
        	  LOGGER.error("Error in moveFiles()"+e.toString());
        	  throw new GIException("Error in moveFiles()"+e.toString(),e);
          }
    }
    public static void moveFilesWithName(String sourceFolder, String targetFolder,String fileType, List<File> lof) throws GIException{
        try{
      	  if (sourceFolder!=null && !("".equals(sourceFolder.trim())) &&!(new File(sourceFolder).exists())) {
                return;
           }
           if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
                new File(targetFolder).mkdirs();
           }
           
           if(lof != null && !lof.isEmpty()){
                for(File file:lof){
                	if(file.getName().equalsIgnoreCase("manifest.xml")){
                     file.renameTo(new File(targetFolder+File.separator+file.getName()));
                	}
                }
           }else{
                //move all files from source to target folder
                File[] files= EnrollmentUtils.getFilesInAFolder(sourceFolder,fileType);
                if(files!=null && files.length>0){
              	  List<File> loFiles=Arrays.asList(files);
              	  moveFiles(sourceFolder,targetFolder,fileType, loFiles);
                }
           }
        }catch(Exception e){
      	  LOGGER.error("Error in moveFiles()"+e.toString());
      	  throw new GIException("Error in moveFiles()"+e.toString(),e);
        }
  }
    
    public static boolean compareEnrolleerace(List<EnrolleeRace> firstList, List<EnrolleeRace> secondList){
		boolean isequal=true;
		if((firstList==null || firstList.isEmpty()) && (secondList==null || secondList.isEmpty())){
			return true;
		}
		if(((firstList==null || firstList.isEmpty()) && (secondList!=null && !secondList.isEmpty())) || ((secondList==null || secondList.isEmpty()) && (firstList!=null && !firstList.isEmpty())) )
		{
			isequal=false;
		}else if(firstList!= null && secondList != null && (firstList.size()!=secondList.size())){
			isequal=false;
		}else{
			for (EnrolleeRace race : firstList){
				boolean isFound=false;
				if(race.getRaceEthnicityLkp()!=null){
					for (EnrolleeRace newRace : secondList){
						if(newRace.getRaceEthnicityLkp()!=null && race.getRaceEthnicityLkp().getLookupValueCode().equalsIgnoreCase(newRace.getRaceEthnicityLkp().getLookupValueCode())){
							isFound=true;
							break;
						}
					}
					
					if(!isFound){
						isequal=false;
						break;
					}
				
				}
			}
		}
		
		return isequal;
	}
    
    /**
     * Returns the days elapsed between two given dates
     * @author negi_s
     * @since 17/02/2015
     * @param startDate
     * @param endDate
     * @return days elapsed
     */
	public static int getDaysBetween(Date startDate, Date endDate) {
		return Days.daysBetween(new DateTime(startDate), new DateTime(endDate)).getDays();
	}

	/**
	 * Returns the number of days in the given month of the date
	 * @author negi_s
     * @since 17/02/2015
	 * @param date
	 * @return number of days
	 */
	public static int daysInMonth(Date date) {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(date);
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * @author Aditya-S
	 * @since 23/03/2015 (DD/MM/YYYY)
	 * 
	 * Removes Time component from given Date Object
	 * 
	 * @param dateTime
	 * @return
	 * @throws GIException
	 */
	public static  Date removeTimeFromDate(Date dateTime) throws GIException{
		Date date = null;
		try{
			GregorianCalendar currentMonthStartDate = new GregorianCalendar();
			currentMonthStartDate.setTime(dateTime);
			currentMonthStartDate.setLenient(false);
			currentMonthStartDate.set(Calendar.HOUR, 0);
			currentMonthStartDate.set(Calendar.MINUTE, 0);
			currentMonthStartDate.set(Calendar.SECOND, 0);
			currentMonthStartDate.set(Calendar.MILLISECOND, 0);
			currentMonthStartDate.set(Calendar.HOUR_OF_DAY, 0);
			date = currentMonthStartDate.getTime();
		}catch(Exception e){
			throw new GIException("Error in removeTimeFromDate()::"+e.getMessage(),e);
		}
		return date;
	}
	
	/**
	 * Cleans the specified directory
	 * 
	 * @author negi_s
	 * @param directoryLocation
	 * @return boolean
	 */
	public static boolean cleanDirectory(String directoryLocation) {
		try{
			File directory = new File(directoryLocation);
			FileUtils.cleanDirectory(directory);
		}catch(Exception e){
			LOGGER.error("Could not clean directory :: " + directoryLocation, e);
			return false;
		}
		return true;
	}
	
	/**
	 * @author parhi_s
	 * @since
	 * Method used to compare two address objects for some specific fields
	 * @param firstAddress
	 * @param secondAddress
	 * @return
	 */
	public static boolean compareAddress(Location firstAddress, Location secondAddress)  {
		boolean isEqual = false;
		if (firstAddress == null && secondAddress == null) {
			isEqual = true;
		} else if (firstAddress != null && secondAddress != null 
				&& compareString(firstAddress.getAddress1(),secondAddress.getAddress1()) 
				&& (compareString(firstAddress.getAddress2(), secondAddress.getAddress2()) ||
					compareString(firstAddress.getAddress1(), secondAddress.getAddress2()) ||
					compareString(firstAddress.getAddress2(), secondAddress.getAddress1())
					)
				&& compareString(firstAddress.getCity(), secondAddress.getCity())
				&& compareString(firstAddress.getState(), secondAddress.getState()) 
				&& compareString(firstAddress.getZip(),secondAddress.getZip())){	
			isEqual=true;				
		}
		return isEqual;
	}
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to compare two strings.
	 * @param firstString
	 * @param secondString
	 * @return
	 */
	
	public static boolean compareString(String firstString,String secondString) {
		boolean isEqual = false;
		String newFirstString;
		String newSecondString;
		if(StringUtils.isBlank(firstString) && StringUtils.isBlank(secondString)){
			isEqual=true;
		}
		else if(StringUtils.isNotBlank(firstString) && StringUtils.isNotBlank(secondString)){
			newFirstString=firstString.trim();
			newSecondString=secondString.trim();
			isEqual=newFirstString.equalsIgnoreCase(newSecondString);
		}
		return isEqual;
	}
	
	/**
	 * 
	 * @author negi_s
	 * @since 10-04-2015
	 * 
	 * Returns 1st day of Next Month for month provided. e.g (1st Aug 2014 00:00:00 for year 2014 and month 7)
	 * NOTE:- The month index is from (0-11)
	 * 
	 * @param month
	 * @param year
	 * @return
	 */
	public static Date getStartDateForNextMonthIndividualReport(int month,int year){
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, month + 1);
		calenderObj.set(Calendar.YEAR, year);
		return calenderObj.getTime();
	}
	
	/**
	 * @author parhi_s
	 * @param date
	 * @return
	 */
	
	public static Date getMonthStartDate(Date date){
		Date nextMonthStartDate=null;
		if(date!=null){
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.DATE, EnrollmentConstants.ONE);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.ZERO);
			cal.set(Calendar.MINUTE, EnrollmentConstants.ZERO);
			cal.set(Calendar.SECOND, EnrollmentConstants.ZERO);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.ZERO);
			nextMonthStartDate = cal.getTime();
		}
		return nextMonthStartDate;
	}
	

	/**
	 * Replaces everything that is not a word character (a-z in any case, 0-9 or
	 * _) or whitespace.
	 * 
	 * @author negi_s
	 * @param str the input string
	 * @return String with special characters removed
	 */
	public static String removeSpecialCharacters(String str){
		String result = null;
		if(null != str){
			result = str.replaceAll("[^\\w\\s]", "");
		}
		return result;
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Calendar dateToCalendar(Date date){
		Calendar cal = TSCalendar.getInstance();
		if(date != null){
		cal.setTime(date);
		return cal;
		}
		return null;
	}
	
	 /**
	  * 
	  * @param e
	  * @param maxLines
	  * @return
	  */
	 public static String shortenedStackTrace(Exception e, int maxLines) {
		 if(e==null){
			 return "";
		 }
	     StringWriter writer = new StringWriter();
	     e.printStackTrace(new PrintWriter(writer));
	     String[] lines = writer.toString().split("\n");
	     StringBuilder sb = new StringBuilder();
	     for (int i = 0; i < Math.min(lines.length, maxLines); i++) {
	         sb.append(lines[i]).append("\n");
	     }
	     return sb.toString();
	 }
	 
	 public static String shortStackTrace(Exception e, int maxLength) {
		 if(e==null){
			 return "";
		 }
	     StringWriter writer = new StringWriter();
	     e.printStackTrace(new PrintWriter(writer));
	     String trace = writer.toString();
	     return trace.substring(0, Math.min(trace.length(), maxLength)-1);
	 }
	
	 /**
	  * Method to get FullName as per passed parameters
	  * @param firstName String
	  * @param middleName String
	  * @param lastName String
	  * @return String
	  */
	public static String getFullName(final String firstName, final String middleName, final String lastName)
	{
		StringBuffer fullName = new StringBuffer();
		if (StringUtils.isNotEmpty(firstName))
		{
			fullName.append(firstName.trim());
		}

		if (StringUtils.isNotEmpty(middleName))
		{
			if(fullName.length() != 0)
			{
				fullName.append(" ");
			}
			fullName.append(middleName.trim());
		}

		if (StringUtils.isNotEmpty(lastName))
		{
			if(fullName.length() != 0)
			{
				fullName.append(" ");
			}
			fullName.append(lastName.trim());
		}
		return fullName.toString();
	}
	
	/**
	 * Removes extra spaces from a string and replaces them with a single space
	 * @author negi_s
	 * @since 23/07/15
	 * @param str Input string
	 * @return String 
	 */
	public static String removeExtraSpaces(String str){
		String result = null;
		if(null != str){
			result = str.trim().replaceAll(" +", " ");
		}
		return result;
	}
	
	/**
	 * 
	 * @author negi_s
	 * @since 20-08-2015
	 * 
	 * Returns 1st day of Year. e.g (1st JAN 2014 00:00:00 for year 2014)
	 * 
	 * @param year
	 * @return Date
	 */
	public static Date getStartDateForYear(Integer year){
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.YEAR, year);
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, calenderObj.getMinimum(Calendar.MONTH));
		return calenderObj.getTime();
	}
	
	/**
	 * @author panda_p
	 * @since 01-Sept-2015
	 * 
	 * Subtracts n days from given or current date 
	 * 
	 * @param daysToSubtrct
	 * @param fromDate
	 * @return 'n' days subtracted date.
	 */
	public static Date getNdayBeforeDate(long daysToSubtrct, Date fromDate){
		Date d = fromDate ==null ? new TSDate():fromDate;//intialize your date to any date
		return new TSDate(d.getTime() - daysToSubtrct * 24 * 3600 * 1000l );
	}
	
	/**
	 * Delete directory from path
	 * @param path
	 * @return boolean
	 */
	public static boolean deleteDir(String path)
	{
		java.io.File dir = new java.io.File(path);
		if (dir.isDirectory())
		{
			String[] filesList = dir.list();
			for(String s : filesList)
			{
				boolean success = new java.io.File(dir, s).delete();
				if(!success)
				{
					return false;
				}
			}
		}
		return dir.delete();
	}
	
	

	/**
	 * Delete directory from File
	 * @param directory 
	 */
	public static void deleteDirectory(File directory) {
		if (directory.exists()) {
			File[] files = directory.listFiles();
			if (null != files) {
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						deleteDirectory(files[i]);
					} else {
						files[i].delete();
					}
				}
			}
		}
	}
	
	
	
	/**
	 * 
	 * @param inputStringFlag : Any string value 
	 * @return true for 'Y' or 'YES' (ignorecase)
	 */
	
	public static boolean getBooleanFromYesOrYFlag(String inputStringFlag){
		boolean flag = false;
		if(isNotNullAndEmpty(inputStringFlag) && (inputStringFlag.equalsIgnoreCase(EnrollmentConstants.YES) || inputStringFlag.equalsIgnoreCase(EnrollmentConstants.Y))){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Method to Split List into subList
	 * @param houseHoldList List <T>
	 * @param splitCount Integer
	 * @return
	 */
	public static <T> List<List<T>> splitListIntoSubList(List <T> houseHoldList, Integer splitCount)
	{
		List<List<T>> subLists = new ArrayList<List<T>>();
		if(houseHoldList != null && !houseHoldList.isEmpty() && splitCount != null && splitCount >0){
			int sizeOfSubList = houseHoldList.size() / splitCount;
		    int remainder = houseHoldList.size() % splitCount;
		    if(sizeOfSubList > 0 || remainder > 0){
		    	int splitStartIndex = 0;
		    	int splitEndIndex = splitCount;
		    	for (int i = 0; i < sizeOfSubList; i++) {		
		    	subLists.add(houseHoldList.subList(splitStartIndex, splitEndIndex));
		    	splitStartIndex = splitStartIndex + splitCount;
		    	splitEndIndex = splitEndIndex + splitCount;
		    	}
		    	if(remainder > 0){
		    		subLists.add(houseHoldList.subList(splitStartIndex, splitStartIndex+remainder));
		    	}
		    }
		}
		return subLists;
	}
	
	/**
	 * Get an array of files in a folder filtered by file name
	 * @param commonLocation
	 * @param responseType
	 * @return File[]
	 */
	public static File[] getFilesInAFolderByName(String commonLocation, String responseType) {
		File[] files = null;
		try{
			File folder= new File(commonLocation);
			final String finalResponseType=responseType;
			files = folder.listFiles( new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					if(pathname.isFile() && pathname.getName().contains(finalResponseType)){
						return true;
					}
					return false;
				}
			});
		}catch(Exception e){
			LOGGER.error("Error getting files in location", e);
		}
		return files;
	}
	
	/**
	 * Move a list of from one location to another
	 * @param targetFolder
	 * @param files
	 */
	public static void moveZipFiles(String targetFolder, File[] files){
		try{
			if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
				new File(targetFolder).mkdirs();
			}
			//move all files from source to target folder
			if(files!=null && files.length>0){
				List<File> loFiles=Arrays.asList(files);
				if(loFiles != null && !loFiles.isEmpty()){
					for(File file:loFiles){
						file.renameTo(new File(targetFolder+File.separator+file.getName()));
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Error in moveFiles()"+e.toString(),e);
		}
	}
	
	/**
	 * Move a list of from one location to another
	 * @param targetFolder
	 * @param files
	 */
	public static void copyZipFiles(String targetFolder, File[] files){
		try{
			if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
				new File(targetFolder).mkdirs();
			}
			//move all files from source to target folder
			if(files!=null && files.length>0){
				List<File> loFiles=Arrays.asList(files);
				if(loFiles != null && !loFiles.isEmpty()){
					for(File file:loFiles){
						Files.copy(file.toPath(), new File(targetFolder+File.separator+file.getName()).toPath());
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Error in moveFiles()"+e.toString(),e);
		}
	}


	public static File[] getFilesInAFolderNotByName(String outZipWipPath, String manifest) {
		File[] files = null;
		try{
			File folder= new File(outZipWipPath);
			final String finalResponseType=manifest;
			files = folder.listFiles( new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					if(pathname.isFile() && pathname.getName().contains(finalResponseType) && pathname.getName().contains(".zip") ){
						return false;
					}
					return true;
				}
			});
		}catch(Exception e){
			LOGGER.error("Error getting files in location", e);
		}
		return files;
	}
	
	/**
	 * Rounds a given float value to the specified decimal places
	 * 
	 * @author negi_s
	 * @param d float value to be rounded
	 * @param decimalPlace round off position
	 * @return rounded off float
	 */
	public static float round(Float d, int decimalPlace) {
		BigDecimal bd = null;
		bd = new BigDecimal(Float.toString(0f));
		if(null != d){
			bd = new BigDecimal(Float.toString(d));
		}
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.floatValue();
	}
	
	/**
	 * Create text file at given path containing test from Stringbuilder
	 * @param parFileName
	 * @param badEnrollmentDetailsBuilder
	 * @param failureFolderPath
	 */
	public static void logFailingRecordsData(String parFileName,StringBuilder testToLog, String  failureFolderPath){
		String fileName = parFileName.replaceAll(EnrollmentConstants.FILE_TYPE_XML, "").replaceAll(EnrollmentConstants.FILE_TYPE_CSV, "");
		File failureFile = new File(failureFolderPath + File.separator + fileName + "_failedRecords.txt");
		File failureFolder = new File(failureFolderPath);
		FileWriter writer = null;
		BufferedWriter bufferedWriter = null;
		try {
			if(!failureFolder.exists()){
				failureFolder.mkdirs();
			}
			if(!failureFile.exists()){
				failureFile.createNewFile();
			}
			writer = new FileWriter(failureFile);
			bufferedWriter = new BufferedWriter(writer,EnrollmentConstants.BUFFER_SIZE);
			bufferedWriter.write(testToLog.toString());
			bufferedWriter.flush();
		} catch (IOException e) {
			LOGGER.error("Error Writing failed records file" , e);
		}finally{
			IOUtils.closeQuietly(writer);
			IOUtils.closeQuietly(bufferedWriter);
		}
	}
	
	
	public static int countFilesInFilePayloadFolder(File directory) {
		int count = 0;
		for(File file : directory.listFiles()) {
			if(!file.isDirectory()) {
				count++;
			}


		}
		return count;
	}
	
	/**
	 * @author parhi_s
	 * 
	 * Returns Returns all Active Enrollees for Enrollment.
	 * 
	 * @return
	 */
	public static  List<Enrollee> getPendingTermEnrollees(List<Enrollee> enrollees){
		List<Enrollee> filteredEnrollees = new ArrayList<Enrollee>();
		if(enrollees != null ){
			for(Enrollee enrollee : enrollees){
					if(enrollee.getPersonTypeLkp()!=null && ((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))
							&& enrollee.getEnrolleeLkpValue()!=null
							&&((enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING))||(enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) || enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
						filteredEnrollees.add(enrollee);
					}
			}
		}
		return filteredEnrollees;
	}
	
	/**
	 * Creates Directory if not present already 
	 * @param path
	 * @return
	 */
	public static boolean createDirectory(String path){
		boolean isDirectoryExists = Boolean.FALSE;
		
		if(StringUtils.isNotEmpty(path)){
			File file = new File(path);
			if(!file.exists()){
				file.mkdirs();
			}
			isDirectoryExists = Boolean.TRUE;
		}
		return isDirectoryExists;
	}
	
	
	
	/**
	 * 
	 * @param filePath filePath where file needs to be searched
	 * @param fullFileName file name with extensions 
	 * @param recursive Value true|false if true it will search in subdirectories
	 * @return
	 * @throws GIException
	 */
	public static File searchFile(String filePath, String fullFileName, boolean recursive)throws GIException
	{
		File searchedFile = null;
		if(StringUtils.isEmpty(filePath)){
			throw new GIException("Received null or Empty filePath");
		}
		if(StringUtils.isEmpty(fullFileName)){
			throw new GIException("Received null or Empty fullFileName");
		}
		if(StringUtils.isNotEmpty(fullFileName) && !fullFileName.contains(".")){
			throw new GIException("File name is missing extension details, Received FileName: "+fullFileName);
		}
		
		File initialFilePath = new File(filePath);
		if(initialFilePath.isDirectory() && initialFilePath.canRead()){
			String fileExtension = fullFileName.substring(fullFileName.lastIndexOf(".") +1, fullFileName.length());
			String []fileExtArray = {fileExtension};
			List<File> searchedFileList =  (List<File>) FileUtils.listFiles(initialFilePath, fileExtArray, recursive);
			if(searchedFileList != null && !searchedFileList.isEmpty())
			{
				for(File file : searchedFileList){
					if(file.isFile() && file.getName().equals(fullFileName)){
						searchedFile = file;
						break;
					}
				}
			}
		}
		else{
			throw new GIException("Received FilePath is Either not a directory path or permission is denied to read the path : "+filePath+" and File Name: "+fullFileName);
		}
		return searchedFile;
	}
	
	/**
	 * @author Sharma_K
	 * @param date1 Date
	 * @param date2 Date
	 * @return boolean
	 */
    public static boolean validateSameYear(Date date1 , Date date2) {
		boolean isSameYear = Boolean.FALSE;
		if(date1 != null && date2 != null){
			Calendar receivedDate = TSCalendar.getInstance();
			receivedDate.setTime(date1);
			
			Calendar existingDate = TSCalendar.getInstance();
			existingDate.setTime(date2);
			
			if(receivedDate.get(Calendar.YEAR) == existingDate.get(Calendar.YEAR)){
				isSameYear = Boolean.TRUE;
			}
		}
		return isSameYear;
	}
    
    
    
    
    public static  <T> boolean isFloatEqual(T e1, T e2){
		boolean isEqual=false;
		try{
		
		if(!isNotNullAndEmpty(e1) && !isNotNullAndEmpty(e2)){
			isEqual=true;
		}else if(isNotNullAndEmpty(e1) && isNotNullAndEmpty(e2)){
			Float float1= Float.parseFloat(e1.toString());
			Float float2= Float.parseFloat(e2.toString());
			if((float1.compareTo(float2)== 0)){
				isEqual=true;
			}
		}
		}catch(Exception e){
			LOGGER.error("Error in parsing to float.",e);
			isEqual=false;
		}
		return isEqual;
	}
    
    public static Date truncateTime(Date date){
    	if(date!=null){
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.HOUR_OF_DAY, EnrollmentConstants.ZERO);
			cal.set(Calendar.MINUTE, EnrollmentConstants.ZERO);
			cal.set(Calendar.SECOND, EnrollmentConstants.ZERO);
			cal.set(Calendar.MILLISECOND, EnrollmentConstants.ZERO);
			date = cal.getTime();
		}
    	return date;
    }
    
    /**
     * 
     * @param date
     * @return month
     */
    public static Integer getMonthFromDate(Date date){
    	
    	if(date == null) {
			return null;
		}
    	
    	Calendar cal = TSCalendar.getInstance();
    	cal.setTime(date);
    	return cal.get(Calendar.MONTH) +1;
    	
    }
    public static Float roundFloat(Float d, int decimalPlace) {
    	if(d!=null){
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    	}else{
    		return null;
    	}
    }
    
    public static void moveFile(String sourceFolder , String targetFolder, File file) throws GIException{
    	try{
      	  if (sourceFolder!=null && !("".equals(sourceFolder.trim())) &&!(new File(sourceFolder).exists())) {
                return;
           }
           if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
                new File(targetFolder).mkdirs();
           }
           
           if(file != null){
                     file.renameTo(new File(targetFolder+File.separator+file.getName()));
           }
        }catch(Exception e){
      	  LOGGER.error("Error in moveFiles()"+e.toString());
      	  throw new GIException("Error in moveFile()"+e.toString(),e);
        }
    	
    }
    
    /**
     * @author panda_p
     * @since 07-10-2016
     * @param Exception e
     * @return
     * @throws GIException
     */
    public static String getExceptionMessage(Exception e){
		if(e!=null){
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return sw.toString();
		}else{
			return null;
		}
		
	}
    
    /**
     * Get base path builder for all reporting jobs by report type and transfer direction
     * @param reportType
     * @param direction
     * @return
     */
	public static StringBuilder getReportingBasePathBuilderByTypeAndDirection(String reportType, String direction) {
		StringBuilder basePathBuilder = new StringBuilder();
		basePathBuilder.append(DynamicPropertiesUtil
				.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_OUT_JOB_PATH));
		basePathBuilder.append(File.separator);
		switch (reportType) {
		case "CMS":
			basePathBuilder.append(EnrollmentConstants.ReportType.CMS.toString());
			break;
		case "IRS":
			basePathBuilder.append(EnrollmentConstants.ReportType.IRS.toString());
			break;
		case "PLR":
			basePathBuilder.append(EnrollmentConstants.ReportType.PLR.toString());
			break;
		case "ANNUAL":
			basePathBuilder.append(EnrollmentConstants.ReportType.ANNUAL.toString());
			break;
		case "CARRIERRECON":
			basePathBuilder.append(EnrollmentConstants.ReportType.CARRIERRECON.toString());
			break;
		case "EXTERNAL_ENROLLMENT":
			basePathBuilder.append(EnrollmentConstants.EXTERNAL_ENROLLMENT);
			break;
		case "EXTERNAL_FAILURE_NOTICES":
			basePathBuilder.append(EnrollmentConstants.EXTERNAL_FAILURE_NOTICES);		
		}
		basePathBuilder.append(File.separator);
		switch (direction) {
		case EnrollmentConstants.TRANSFER_DIRECTION_OUT:
			basePathBuilder.append(EnrollmentConstants.OUT_FOLDER);
			break;
		case EnrollmentConstants.TRANSFER_DIRECTION_IN:
			basePathBuilder.append(EnrollmentConstants.IN_FOLDER);
			break;
		}
		return basePathBuilder;
	}
	
	 /**
     * Get YHI base path builder for all reporting jobs by report type and transfer direction
     * @param reportType
     * @param direction
     * @return
     */
	public static StringBuilder getExchgReportingBasePathBuilderByTypeAndDirection(String reportType, String direction, String exchgSftpFolder) {
		StringBuilder basePathBuilder = new StringBuilder();
		basePathBuilder.append(exchgSftpFolder);
		basePathBuilder.append(File.separator);
		switch (reportType) {
		case "CMS":
			basePathBuilder.append("CMS_XML");
			break;
		case "IRS":
			basePathBuilder.append("Monthly_IRS");
			break;
		case "PLR":
			basePathBuilder.append(EnrollmentConstants.ReportType.PLR.toString());
			break;
		case "ANNUAL":
			basePathBuilder.append("Annual_IRS");
			break;
		case "RECON":
			basePathBuilder.append("Recon");
			break;
		}
		basePathBuilder.append(File.separator);
		switch (direction) {
		case EnrollmentConstants.TRANSFER_DIRECTION_OUT:
			basePathBuilder.append(EnrollmentConstants.OUT_FOLDER);
			break;
		case EnrollmentConstants.TRANSFER_DIRECTION_IN:
			basePathBuilder.append(EnrollmentConstants.IN_FOLDER);
			break;
		}
		return basePathBuilder;
	}
	
	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	public static Float calculatePercentage(final Long sourceValue, final Long targetValue){
		if(sourceValue != null && sourceValue != 0 && targetValue != null && targetValue != 0){
			if((targetValue - sourceValue) >=0){
			return new BigDecimal((sourceValue * 100.0f)/targetValue).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
		}
			else{
				return new BigDecimal((targetValue * 100.0f)/sourceValue).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
			}
		}
		return BigDecimal.ZERO.floatValue();
	}
	
    /**
     * 
     * @param address1
     * @param address2
     * @param city
     * @param stateCode
     * @param zipCode
     * @return
     */
	public static String prepareAddressString(final String address1, final String address2, final String city, final String stateCode, 
			final String zipCode, final String countyCode, final boolean isAddBracketToCountyCode){
		StringBuilder stringAddressBuilder = new StringBuilder(200);
		if(StringUtils.isNotBlank(address1)){
			stringAddressBuilder.append(address1);
		}
		if(StringUtils.isNotBlank(address2)){
			stringAddressBuilder.append(StringUtils.SPACE);
			stringAddressBuilder.append(address2);
		}
		if(StringUtils.isNotBlank(city)){
			stringAddressBuilder.append(StringUtils.SPACE);
			stringAddressBuilder.append(city);
		}
		if(StringUtils.isNotBlank(stateCode)){
			stringAddressBuilder.append(StringUtils.SPACE);
			stringAddressBuilder.append(stateCode);
		}
		if(StringUtils.isNotBlank(zipCode)){
			stringAddressBuilder.append(StringUtils.SPACE);
			stringAddressBuilder.append(zipCode);
		}
		if(StringUtils.isNotBlank(countyCode)){
			stringAddressBuilder.append(StringUtils.SPACE);
			if(isAddBracketToCountyCode){
				stringAddressBuilder.append("(");
				stringAddressBuilder.append(countyCode);
				stringAddressBuilder.append(")");
			}
			else{
				stringAddressBuilder.append(countyCode);
			}
		}
		return stringAddressBuilder.toString();
	}
	
	/**
	 * 
	 * @return
	 */
	public static Map<Integer, String> getMonthDetails(){
		Map<Integer, String> monthDetailsMap = new LinkedHashMap<Integer, String>();
		monthDetailsMap.put(EnrollmentConstants.ONE, "JANUARY");
		monthDetailsMap.put(EnrollmentConstants.TWO, "FEBRUARY");
		monthDetailsMap.put(EnrollmentConstants.THREE, "MARCH");
		monthDetailsMap.put(EnrollmentConstants.FOUR, "APRIL");
		monthDetailsMap.put(EnrollmentConstants.FIVE, "MAY");
		monthDetailsMap.put(EnrollmentConstants.SIX, "JUNE");
		monthDetailsMap.put(EnrollmentConstants.SEVEN, "JULY");
		monthDetailsMap.put(EnrollmentConstants.EIGHT, "AUGUST");
		monthDetailsMap.put(EnrollmentConstants.NINE, "SEPTEMBER");
		monthDetailsMap.put(EnrollmentConstants.TEN, "OCTOBER");
		monthDetailsMap.put(EnrollmentConstants.ELEVEN, "NOVEMBER");
		monthDetailsMap.put(EnrollmentConstants.TWELVE, "DECEMBER");
		
		return monthDetailsMap;
	}
	
	public static String getFormatedRatingArea(String ratingArea)throws NumberFormatException{
		if(StringUtils.isNotBlank(ratingArea) && NumberUtils.isNumber(ratingArea)){
			StringBuilder formatedRatingArea = new StringBuilder(EnrollmentConstants.FIFTEEN);
			formatedRatingArea.append(EnrollmentConstants.RATING_AREA_PREFIX);
			formatedRatingArea.append(DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE));
			formatedRatingArea.append(String.format("%03d", Integer.parseInt(ratingArea)));
			return formatedRatingArea.toString();
		}
		else{
			return ratingArea;
		}
	}
	
	public static Double round(Double d, int decimalPlace) {
		if (d != null) {
			BigDecimal bd = new BigDecimal(Double.toString(d));
			bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
			return bd.doubleValue();
		} else {
			return null;
		}
	}

	public static boolean isSameIssuer(final String issuer1CMSPlanID, final String issuer2CMSPlanID) {
		boolean isIssuerSame = false;
		if (StringUtils.isNotBlank(issuer1CMSPlanID) && issuer1CMSPlanID.length() >= EnrollmentConstants.FIVE
				&& StringUtils.isNotBlank(issuer2CMSPlanID) && issuer2CMSPlanID.length() >= EnrollmentConstants.FIVE) {
			isIssuerSame = issuer2CMSPlanID
					.matches("^" + issuer1CMSPlanID.substring(0, EnrollmentConstants.FIVE) + ".*");
		}
		return isIssuerSame;
	}

	public static Integer getAge(final Date dateOfBirth, final Date effectiveStartDate) {
		Integer age = EnrollmentConstants.ZERO;
		Calendar born = TSCalendar.getInstance();
		Calendar effectiveDate = TSCalendar.getInstance();
		if (dateOfBirth != null && effectiveStartDate != null) {
			effectiveDate.setTime(effectiveStartDate);
			born.setTime(dateOfBirth);
			if (born.after(effectiveDate)) {
				throw new IllegalArgumentException("Can't be born in the future");
			}
			age = effectiveDate.get(Calendar.YEAR) - born.get(Calendar.YEAR);
			if (effectiveDate.get(Calendar.DAY_OF_YEAR) < born.get(Calendar.DAY_OF_YEAR)) {
				age -= EnrollmentConstants.ONE;
			}
			if (age == EnrollmentConstants.ZERO) {
				age++;
			}
		}
		return age;
	}
	
	public static boolean isSameYear(Date date1, Date date2) {
		boolean isSameYear = false;
		if (date1 != null && date2 != null && DateUtil.getYearFromDate(date1).intValue() == DateUtil.getYearFromDate(date2).intValue()) {
			isSameYear = true;
		}
		return isSameYear;
	}
	
	/**
	 * 
	 * @param dt1
	 * @param dt2
	 * @param buffer in seconds
	 * @return
	 */
	public static boolean isSameTime(final Date dt1, final Date dt2, final int buffer){
		boolean isSame=false;
		/* For Old data where disenrolltimestamp is not set for TERM event  **/
		if(dt1 == null && dt2 == null){
			isSame= true;
		} else if(dt1!=null && dt2!=null && Math.abs(TimeUnit.MILLISECONDS.toSeconds(dt1.getTime()-dt2.getTime()))<=buffer) {
			 isSame= true;
		}
		return isSame;
	}
	
	
	public static boolean compareMiddleName(String carrierMiddlName, String hixMiddleName){
		boolean isEqual=false;
		if(isNullOrEmpty(carrierMiddlName) && isNullOrEmpty(hixMiddleName)) {
			isEqual=true;
		}else if(isNotNullAndEmpty(carrierMiddlName) && isNotNullAndEmpty(hixMiddleName)){
			carrierMiddlName=carrierMiddlName.replaceAll("\\.", "");
			hixMiddleName=hixMiddleName.replaceAll("\\.", "");
			if(carrierMiddlName.length()==1 && carrierMiddlName.equalsIgnoreCase(hixMiddleName.substring(0, 1))){
				isEqual=true;
			}else if(carrierMiddlName.equalsIgnoreCase(hixMiddleName)){
				isEqual=true;
			}
		}
		return isEqual;
	}
		
	public static <T> boolean isNullOrEmpty(T e){
		boolean isNUll=false;
		if((e==null) || e.toString().isEmpty() || e.toString().equalsIgnoreCase("null")){
			isNUll=true;
		}
		return isNUll;
	}
	public static boolean compareAgentName(String carrierName, String hixName){
		boolean isEqual=false;
		if(isNullOrEmpty(carrierName) && isNullOrEmpty(hixName)) {
			isEqual=true;
		}else if(isNotNullAndEmpty(carrierName) && isNotNullAndEmpty(hixName)){
			//hixName=hixName.replaceAll(",", "").replaceAll("\\.", "");
			String specialCharRegex="[^a-zA-Z0-9\\s]";
			String compareRegEx=".*";
			hixName= Pattern.compile(specialCharRegex).matcher(hixName).replaceAll("");
			carrierName=Pattern.compile(specialCharRegex).matcher(carrierName).replaceAll("");
			String[] splittedHixNames=hixName.split(" ");
			String[] splittedCarrierNames=carrierName.split(" ");
			if(splittedHixNames!=null && splittedHixNames.length>0){
				for(int i=0;i<splittedHixNames.length;i++){
					if(i==0 ||( i==(splittedHixNames.length-1) && isEqual)){
						isEqual=false;
						for (int j=0;j<splittedCarrierNames.length;j++){
							if(j==0 ||( j==(splittedCarrierNames.length-1))){
								isEqual = (Pattern.compile(splittedHixNames[i]+compareRegEx, Pattern.CASE_INSENSITIVE).matcher(splittedCarrierNames[j]).matches() ||  Pattern.compile(splittedCarrierNames[j]+compareRegEx, Pattern.CASE_INSENSITIVE).matcher(splittedHixNames[i]).matches());
								if(isEqual){
									break;
								}
						}
					}
					if(!isEqual){
						break;
					}
						
				}
			}
		 }
		}
		return isEqual;
	}

		
		/**
		 * 
		 * Concats given Enrollee First name and Last name
		 * 
		 * @param enrolleeToConcat
		 * @return
		 */
		public static String concatEnrolleeName(Enrollee enrolleeToConcat){
			StringBuilder enrolleeName = new StringBuilder();
			if(enrolleeToConcat != null){
				if(isNotNullAndEmpty(enrolleeToConcat.getFirstName())){
					enrolleeName.append(enrolleeToConcat.getFirstName());
					enrolleeName.append(" ");
				}
				if(isNotNullAndEmpty(enrolleeToConcat.getMiddleName())){
					enrolleeName.append(enrolleeToConcat.getMiddleName());
					enrolleeName.append(" ");	
				}
				if(isNotNullAndEmpty(enrolleeToConcat.getLastName())){
					enrolleeName.append(enrolleeToConcat.getLastName());
					enrolleeName.append(" ");
				}
				if(isNotNullAndEmpty(enrolleeToConcat.getSuffix())){
					enrolleeName.append(enrolleeToConcat.getSuffix());	
				}

				if(enrolleeName.length() > EnrollmentConstants.SPONSORNAME_MAX_LENGTH ){
					enrolleeName.delete(EnrollmentConstants.SPONSORNAME_MAX_LENGTH,enrolleeName.length());
				}
			}
			return enrolleeName.toString().trim();
		}
		
		public static Date getStartDateTime(Date date){
			Calendar calenderObj = TSCalendar.getInstance();
			calenderObj.setTime(date);
			calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
			calenderObj.set(Calendar.HOUR_OF_DAY,calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
			calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
			calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
			calenderObj.set(Calendar.MILLISECOND, calenderObj.getMinimum(Calendar.MILLISECOND));
			return calenderObj.getTime();
		}
		
		public static Integer getRequotingAge(Date dateOfBirth, Date coverageStartDate) {
			Calendar today = TSCalendar.getInstance();
			today.setTime(coverageStartDate);
			Calendar dob = TSCalendar.getInstance();
			dob.setTime(dateOfBirth);
			Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
			if (dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) || (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH)
					&& dob.get(Calendar.DATE) > today.get(Calendar.DATE))) {
				age--;
			}
			return age;
		}
		public static Gson enrollmentGson()
		{
			if(gsonStatic == null)
			{
				GsonBuilder builder = new GsonBuilder();
				builder.serializeNulls();// HIX-110406
				builder.disableHtmlEscaping();
				builder.setDateFormat(EnrollmentConstants.JSON_DATE_FORMAT);
				gsonStatic = builder.create();
			}
			return gsonStatic;
		}
		/**
		 * 
		 * @param inputList
		 * @param size
		 * @return 
		 */
	public static <T> Collection<List<T>> partitionBasedOnSize(List<T> inputList, int size) {
		final AtomicInteger counter = new AtomicInteger(0);
		Map<Integer, List<T> > partitionMap = inputList.stream().collect(Collectors.groupingBy(s -> counter.getAndIncrement() / size));
		return partitionMap.values();
	}
	
	public static Float parseFloat(String floatStr) {
		Float flt = null;
		try {
			
			flt = Float.parseFloat(floatStr);
			
		} catch (NumberFormatException e) {
			
			 LOGGER.error("NumberFormatException while parsing %S to Float : %S ", floatStr, e.toString());
		}
		return flt;
	}


	public static Integer parseInteger(String intString) {
		Integer intger = null;
		
		try {
			
			intger = Integer.parseInt(intString);
			
		} catch (NumberFormatException e) {
			
			 LOGGER.error("NumberFormatException while parsing %S to Float : %S ", intString, e.toString());
		}
		return intger;
	}
	
	public static Integer getEnrolleeAge(Enrollee enrollee) {
		Integer age = null;
		if (null != enrollee && null != enrollee.getBirthDate()) {
			if (null != enrollee.getQuotingDate()) {
				age = getRequotingAge(enrollee.getBirthDate(), enrollee.getQuotingDate());
			} else {
				age = getRequotingAge(enrollee.getBirthDate(), enrollee.getEffectiveStartDate());
			}
		} else if (null != enrollee.getAge()) {
			age = enrollee.getAge();
		}
		return age;
	}
}
