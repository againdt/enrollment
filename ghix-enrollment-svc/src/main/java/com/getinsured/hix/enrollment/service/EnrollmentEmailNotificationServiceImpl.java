package com.getinsured.hix.enrollment.service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.shop.EmployeeDetailsDTO;
import com.getinsured.hix.dto.shop.RestEmployeeDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.enrollment.email.EnrollmentCancelEmailNotification;
import com.getinsured.hix.enrollment.email.EnrollmentEmployeeHouseHoldInfoUpdateNotification;
import com.getinsured.hix.enrollment.email.EnrollmentIRSOutboundFailureEmailNotification;
import com.getinsured.hix.enrollment.email.EnrollmentReinstateEmailNotification;
import com.getinsured.hix.enrollment.email.EnrollmentStatusConfirmNotification;
import com.getinsured.hix.enrollment.email.EnrollmentTermEmailNotification;
import com.getinsured.hix.enrollment.email.IRSInboundResponseNotification;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notification.NotificationAgent;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * @author raja
 * @since 07/09/2013
 */
@Service("enrollmentEmailNotificationService")
@Transactional
public class EnrollmentEmailNotificationServiceImpl implements EnrollmentEmailNotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEmailNotificationServiceImpl.class);


	@Autowired private GhixRestTemplate restTemplate;
	@Autowired private EnrolleeService enrolleeService;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentCancelEmailNotification enrollmentCancelEmailNotification;
	@Autowired private EnrollmentTermEmailNotification enrollmentTermEmailNotification;
	@Autowired private IRSInboundResponseNotification iRSInboundResponseNotification;
	@Autowired private EnrollmentEmployeeHouseHoldInfoUpdateNotification employeeHouseHoldUpdateNotification;
	@Autowired private EnrollmentStatusConfirmNotification enrollmentStatusConfirmNotification;
	@Autowired private EnrollmentReinstateEmailNotification enrollmentReinstateEmailNotification;
	@Autowired private EnrollmentIRSOutboundFailureEmailNotification enrollmentIRSOutboundNotification;
	private static final String EMAILTITLE="EmailTitle";
	private static final String TITLE="title";
	@Autowired private LookupService lookupService;
//	private static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
//	private static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";

	@Override
	@Transactional
	public void sendEnrollmentStatusNotification(List<Enrollment> enrollmentList,String termReasonCode) throws GIException {

		if(enrollmentList!=null){
		    
			 for(Enrollment enrollment : enrollmentList){
			
				 if(enrollment.getEnrollmentStatusLkp()!=null &&  enrollment.getEmployeeId()!=null &&
						 (enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || 
						 (enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)))){
					 
					 if(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
						 sendEnrollmentCancelStatusNotification(enrollment);
					 } 
					 
					 if(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
						 sendEnrollmentTermStatusNotification(enrollment,termReasonCode);
					 }				 
				   } 
			   }	 
		  }
	}
	
	@Override
	public void sendEnrollmentReinstateNotification(List<Enrollment> enrollmentList) throws GIException{
		  if(enrollmentList != null){
              String enrollmentStatus = null;
              for(Enrollment enrollment : enrollmentList){
                    enrollmentStatus = enrollment.getEnrollmentStatusLkp().getLookupValueCode();
                     if(enrollment.getEnrollmentStatusLkp()!=null &&  enrollment.getEmployeeId()!=null &&
                                   !(enrollmentStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) &&
                                   !(enrollmentStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)))){
                            
                           sendEmployeeReinstateNotification(enrollment);
                    }
              }
       }
}

	private void sendEnrollmentTermStatusNotification(Enrollment enrollment,String termReasonCode) throws GIException {
		  Enrollee subscriber = enrollment.getSubscriberForEnrollment();
		  if(subscriber!=null){
			  sendTerminationNotification(subscriber, enrollment, termReasonCode);
			  List<Enrollee> enrolleeList =enrollmentService.getEnrolleesOnly(enrollment);
			  if(enrolleeList!=null && enrolleeList.size()>0){
				  for (Enrollee enrollee: enrolleeList){
					  if(!EnrollmentUtils.compareAddress(subscriber.getHomeAddressid(), enrollee.getHomeAddressid())){
						  sendTerminationNotification(enrollee, enrollment, termReasonCode);
					  }
				  }
			  }
		  }
	}
	private void sendEmployeeReinstateNotification(Enrollment enrollment) throws GIException {
		  List<Enrollee> enrolleeList = enrolleeService.findEnrolleeByEnrollmentID(enrollment.getId()); 
		  Employer employer = enrollment.getEmployer();
		  GhixNoticeCommunicationMethod ghixNoticeCommRef= GhixNoticeCommunicationMethod.Mail;
		  for(Enrollee enrollee:enrolleeList){
			  try{
				  if((enrollee.getPersonTypeLkp()!=null ) && 
						  (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){

					  EmployeeDetailsDTO employeeDetails = getEmployeeDetailsByEmployeeId(enrollment.getEmployeeId());
					  
					  Map<String,Object> emailData = new HashMap<String, Object>();
					  
					  // adding the headerContent to the enmail notification
					  emailData.putAll(getHeaderAndFooter(enrollmentReinstateEmailNotification));

					  //required for notice template

						 // emailData.put("isCancelled","No");
						  emailData.put(TITLE,"Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+".");
						//  emailData.put("textCancelTerm",enrollment.getEnrollmentStatusLkp().getLookupValueLabel().toLowerCase());
						  emailData.put(EMAILTITLE,"Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+"."); //Required for pdf filename and title for the email
					
					  // Employee Full Name
						  String enrolleeFullName = enrollee.getFirstName() + " " + enrollee.getLastName();
						  emailData.put("employeeName", enrolleeFullName);
						  emailData.put("employerName", enrollee.getEnrollment().getEmployer().getName());
					  // EMPLOYEE ADDRESS INFORMATION
					  if(enrollee.getHomeAddressid()!=null){
						  emailData.put("addressLine1",enrollee.getHomeAddressid().getAddress1());
						  if(StringUtils.isNotBlank(enrollee.getHomeAddressid().getAddress2())){
							  emailData.put("addressLine2",enrollee.getHomeAddressid().getAddress2()+","+"<br/>");
						  }else{
							  emailData.put("addressLine2","");
						  }
						  emailData.put("location", enrollee.getHomeAddressid());
						  emailData.put("county",enrollee.getHomeAddressid().getCounty());
						  emailData.put("city",enrollee.getHomeAddressid().getCity());
						  emailData.put("state",enrollee.getHomeAddressid().getState());
						  emailData.put("zipCode",enrollee.getHomeAddressid().getZip());
					  }
					
					  emailData.put("currentDate",DateUtil.dateToString(new TSDate(), GhixConstants.DISPLAY_DATE_FORMAT));								  
					  emailData.put("ModuleID",String.valueOf(enrollment.getEmployeeId()));//Required for pdf filename
					  emailData.put("ModuleName",ModuleUserService.EMPLOYEE_MODULE);//Required for pdf filename
					  
					  List<String> sendToEmailList = null;
					  if(employeeDetails.getEmail() != null){
						  sendToEmailList = new ArrayList<String>(); //Required for sending email, notifying a notice is generated.
						  sendToEmailList.add(employeeDetails.getEmail()); 
					  }
					  
					  emailData.put("sendToEmailList",sendToEmailList);
					  emailData.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
					  emailData.put("toFullName",enrolleeFullName);// who is sending email
					  emailData.put("updateSubject","Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+".");
					  emailData.put("fileName","Notice-"+emailData.get(EMAILTITLE)+"-"+new TSDate().getTime()+".pdf");
					  emailData.put("exchangePhoneNumber",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					  emailData.put("exchangeURL",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
					  emailData.put("exchangeFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));


					  //required for email template - [EnrollmentStatusCancelTermNotification.html]
					  emailData.put("employeeName",enrollee.getFirstName()+" "+enrollee.getLastName());
					  emailData.put("enrolledPlanName",enrollment.getPlanName());
					  emailData.put("issuerName",enrollment.getInsurerName());
					  emailData.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					  emailData.put("employerName",employer.getName());
					  String terminationDate = DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT);
					  emailData.put("terminationDate",terminationDate);
					  String effectiveStartDate = DateUtil.dateToString(enrollee.getEffectiveStartDate(), GhixConstants.REQUIRED_DATE_FORMAT);
					  emailData.put("terminationDate",terminationDate);
					  emailData.put("effectiveStartDate",effectiveStartDate);
					  emailData.put("employerPhoneNumber",employer.getContactNumber());
					  emailData.put("exchangeSignature",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_SIGNATURE));
					  emailData.put("isReinstatementEmailSend",EnrollmentConstants.TRUE);
					  emailData.put("ssapApplicationId", enrollment.getSsapApplicationid());
					  
					  //  send notification to the respective employee
					  try{
						  enrollmentReinstateEmailNotification.setNotificationData(emailData);
						  if(sendToEmailList!=null && !sendToEmailList.isEmpty() ){
							  enrollmentReinstateEmailNotification.sendNotification(Boolean.TRUE, Boolean.TRUE,  emailData,ghixNoticeCommRef);
							  enrollmentReinstateEmailNotification.sendMail();
						  }else{
							  emailData.put("ModuleName","");
							  enrollmentReinstateEmailNotification.sendNotification(Boolean.FALSE, Boolean.TRUE,  emailData,ghixNoticeCommRef);
						  }
						  
						 
						  
					  }catch (Exception ex) {
						  LOGGER.error("Enrollment Reinstate email notification failed.",ex);
						  throw new GIException("Enrollment Reinstate email notification failed.",ex);
					  }

				  } 
			  }catch(Exception e){
				  LOGGER.error("Invalid employee id->"+enrollment.getEmployeeId()+"",e);
			  }
		  }
		
	}
	
	@Override
	public void sendIRSNACKResponseFailureNotification(String responseCode, String responseDesc,String fileName ,String fileLocation, String action) throws GIException
	{
		Map<String,Object> emailData = new HashMap<String,Object>();
		emailData.putAll(getHeaderAndFooter(iRSInboundResponseNotification));
		  emailData.put(EMAILTITLE,"IRS Inbound File Transmission"); 
		  emailData.put(TITLE,"IRS Inbound File Handling"); 
		  emailData.put("ResponseCode",responseCode);
		  emailData.put("ResponseDesc",responseDesc);
		  emailData.put("Manifest_File_Name",fileName);
		  emailData.put("Manifest_File_Location",fileLocation);
		  emailData.put("Action",action);
		  emailData.put("Environment",GhixConstants.APP_URL);
		  List<String> sendToEmailList = new ArrayList<String>(); //Required for sending email, notifying a notice is generated.
		  sendToEmailList.add(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EMAIL_GRP_IRS_REPORTING));
		  emailData.put("sendToEmailList",sendToEmailList);
		  emailData.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
		  emailData.put("toFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
		  emailData.put("exchangeFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		  emailData.put("To", DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EMAIL_GRP_IRS_REPORTING));
		  emailData.put("isIRSNACKResponseFailureNotification",EnrollmentConstants.TRUE);  
		  
		  Map<String,String> tokens = new HashMap<String,String>();
		  tokens.put(EMAILTITLE, "IRS Inbound File Transmission");
		  tokens.put(TITLE,"IRS Inbound File Handling");
		  tokens.put("ResponseCode",responseCode);
		  tokens.put("ResponseDesc",responseDesc);
		  tokens.put("Manifest_File_Name",fileName);
		  tokens.put("Manifest_File_Location",fileLocation);
		  tokens.put("Action",action);
		  tokens.put("Environment",GhixConstants.APP_URL);
		  tokens.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
		  tokens.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
		  tokens.put("toFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
		  
		
		  // send notification to the respective employee
		  try{
				if(sendToEmailList != null){
					
					for(final String emailId : sendToEmailList){
						final AccountUser user = new AccountUser();
						user.setEmail(emailId);
						user.setFirstName(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
						iRSInboundResponseNotification.setUserObj(user);
						iRSInboundResponseNotification.setNotificationData(emailData);
						iRSInboundResponseNotification.setTokens(tokens);
						iRSInboundResponseNotification.sendEmail(iRSInboundResponseNotification.generateEmail());
					}	
				}
			//  iRSInboundResponseNotification.setNotificationData(emailData);
			 // Notice noticeObj = iRSInboundResponseNotification.generateEmail();
			//  iRSInboundResponseNotification.sendEmail(noticeObj);
		  }catch (Exception ex) {
			  LOGGER.error("IRSInboundResponseEmailemail notification failed.",ex);
			  throw new GIException("IRSInboundResponseEmail notification failed.",ex);
		  }

	  
  
}
	private void sendEnrollmentCancelStatusNotification(Enrollment enrollment) throws GIException {
		  List<Enrollee> enrolleeList = enrolleeService.findEnrolleeByEnrollmentID(enrollment.getId()); 
		  Employer employer = enrollment.getEmployer();
		  GhixNoticeCommunicationMethod ghixNoticeCommRef= GhixNoticeCommunicationMethod.Mail;
		  for(Enrollee enrollee:enrolleeList){
			  try{
				  if((enrollee.getPersonTypeLkp()!=null ) && 
						  (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){

					  EmployeeDetailsDTO employeeDetails = getEmployeeDetailsByEmployeeId(enrollment.getEmployeeId());
					  
					  Map<String,Object> emailData = new HashMap<String, Object>();
					  
					  // adding the headerContent to the enmail notification
					  emailData.putAll(getHeaderAndFooter(enrollmentCancelEmailNotification));

					  //required for notice template
						  emailData.put(EMAILTITLE,"Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+"."); //Required for pdf filename and title for the email
						  emailData.put(TITLE,"Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+"."); 
						  emailData.put("textCancelTerm",enrollment.getEnrollmentStatusLkp().getLookupValueLabel().toLowerCase());
						  emailData.put("isCancelled","Yes");

					
					  // EMPLOYEE ADDRESS INFORMATION
					  if(enrollee.getHomeAddressid()!=null){
						  emailData.put("addressLine1",enrollee.getHomeAddressid().getAddress1());
						  if(StringUtils.isNotBlank(enrollee.getHomeAddressid().getAddress2())){
							  emailData.put("addressLine2",enrollee.getHomeAddressid().getAddress2()+","+"<br/>");
						  }else{
							  emailData.put("addressLine2","");
						  }
						  emailData.put("location", enrollee.getHomeAddressid());
						  emailData.put("county",enrollee.getHomeAddressid().getCounty());
						  emailData.put("city",enrollee.getHomeAddressid().getCity());
						  emailData.put("state",enrollee.getHomeAddressid().getState());
						  emailData.put("zipCode",enrollee.getHomeAddressid().getZip());
					  }
					  
					  emailData.put("currentDate",DateUtil.dateToString(new TSDate(), GhixConstants.EMAIL_TEMPLATE_DATE_FORMAT));								  
					  emailData.put("ModuleID",String.valueOf(enrollment.getEmployeeId()));//Required for pdf filename
					  emailData.put("ModuleName",ModuleUserService.EMPLOYEE_MODULE);//Required for pdf filename
					  
					  List<String> sendToEmailList = null;
					  if(employeeDetails.getEmail() != null){
						  sendToEmailList = new ArrayList<String>(); //Required for sending email, notifying a notice is generated.
						  sendToEmailList.add(employeeDetails.getEmail());
					  }

					  emailData.put("sendToEmailList",sendToEmailList);
					  emailData.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
					  emailData.put("toFullName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());// who is sending email
					  emailData.put("updateSubject","Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+".");
					  emailData.put("fileName","Notice-"+emailData.get(EMAILTITLE)+"-"+new TSDate().getTime()+".pdf");
					  emailData.put("exchangePhoneNumber",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					  emailData.put("exchangeURL",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
					  emailData.put("exchangeFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));


					  //required for email template - [EnrollmentStatusCancelTermNotification.html]
					  emailData.put("employeeName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());
					  emailData.put("enrolledPlanName",enrollment.getPlanName());
					  emailData.put("issuerName",enrollment.getInsurerName());
					  emailData.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					  emailData.put("employerName",employer.getName());
					  String terminationDate = DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT);
					  emailData.put("terminationDate",terminationDate);
					  emailData.put("employerPhoneNumber",employer.getContactNumber());
					  emailData.put("exchangeSignature",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_SIGNATURE));
					  emailData.put("ssapApplicationId", enrollment.getSsapApplicationid());
					  
					  
					  //  send notification to the respective employee
					  try{
						  enrollmentCancelEmailNotification.setNotificationData(emailData);
						 
						  if(sendToEmailList!=null && !sendToEmailList.isEmpty() ){
							  enrollmentCancelEmailNotification.sendNotification(Boolean.TRUE, Boolean.TRUE,  emailData,ghixNoticeCommRef);
							  enrollmentCancelEmailNotification.sendMail();
						  }else{
							  emailData.put("ModuleName","");
							  enrollmentCancelEmailNotification.sendNotification(Boolean.FALSE, Boolean.TRUE,  emailData,ghixNoticeCommRef); 
						  }
						  
					  }catch (Exception ex) {
						  LOGGER.error("Enrollment status email notification failed.",ex);
						  throw new GIException("Enrollment status email notification failed.",ex);
					  }

				  } 
			  }catch(Exception e){
				  LOGGER.error("Invalid employee id->"+enrollment.getEmployeeId()+"",e);
			  }
		  }
		
	}

	@Override
	public void sendEnrolledEmployeeHouseHoldInfoUpdateNotification(List<Enrollment> enrollmentList) throws GIException {

		if(enrollmentList!=null){
			GhixNoticeCommunicationMethod ghixNoticeCommRef= GhixNoticeCommunicationMethod.Email;
			for(Enrollment enrollment : enrollmentList){

				if(enrollment.getEmployeeId()!=null){

					List<Enrollee> enrolleeList = enrolleeService.findEnrolleeByEnrollmentID(enrollment.getId()); 

					for(Enrollee enrollee:enrolleeList){
						try{
							if((enrollee.getPersonTypeLkp()!=null ) && (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){

								EmployeeDetailsDTO employeeDetails = getEmployeeDetailsByEmployeeId(enrollment.getEmployeeId());

								Map<String,Object> emailData = new HashMap<String, Object>();

								// adding the header and footer Content to the enmail notification
								emailData.putAll(getHeaderAndFooter(employeeHouseHoldUpdateNotification));

								//required for notice template
								emailData.put(EMAILTITLE,"EmployeeEnrollmentUpdate"); //Required for pdf filename and title for the email
								emailData.put(TITLE,"EmployeeEnrollmentUpdate"); 

								emailData.put("ModuleID",String.valueOf(enrollment.getEmployeeId()));//Required for pdf filename
								emailData.put("ModuleName",ModuleUserService.EMPLOYEE_MODULE);//Required for pdf filename
								
								List<String> sendToEmailList = null;
								if(employeeDetails.getEmail() != null){
									sendToEmailList = new ArrayList<String>(); //Required for sending email, notifying a notice is generated.
									sendToEmailList.add(employeeDetails.getEmail());
								}

								emailData.put("sendToEmailList",sendToEmailList);
								emailData.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
								emailData.put("toFullName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());// who is sending email
								emailData.put("fileName","Notice-"+emailData.get(EMAILTITLE)+"-"+new TSDate().getTime()+".pdf");
								emailData.put("exchangePhoneNumber",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
								emailData.put("updateSubject","Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+".");
								emailData.put("employeeName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());
								emailData.put("enrolledPlanName",enrollment.getPlanName());
								emailData.put("issuerName",enrollment.getInsurerName());
								emailData.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
								emailData.put("currentDate",DateUtil.dateToString(new TSDate(), GhixConstants.EMAIL_TEMPLATE_DATE_FORMAT));
								emailData.put("ssapApplicationId", enrollment.getSsapApplicationid());
								//  SEND NOTIFICATION TO THE RESPECTIVE EMPLOYEE

								employeeHouseHoldUpdateNotification.setNotificationData(emailData);
								  
								if(sendToEmailList!=null && !sendToEmailList.isEmpty() ){
									employeeHouseHoldUpdateNotification.sendNotification(Boolean.TRUE, Boolean.TRUE,  emailData,ghixNoticeCommRef);
								  }else{
									  emailData.put("ModuleName","");
									  employeeHouseHoldUpdateNotification.sendNotification(Boolean.FALSE, Boolean.TRUE,  emailData,ghixNoticeCommRef); 
								  }

							} 
						}catch (Exception ex) {
							LOGGER.error("Employee enrollment updated email notification failed.",ex);
							throw new GIException("Employee enrollment updated email notification failed.",ex);
						}
					}
				} 
			}
		}
	}

	@Override
	public void sendEmployeeEnrollmentStatusConfirmationNotification(List<Enrollment> enrollmentList) throws GIException {

		if(enrollmentList!=null){
			GhixNoticeCommunicationMethod ghixNoticeCommRef= GhixNoticeCommunicationMethod.Email;
			for(Enrollment enrollment : enrollmentList){

				if(enrollment.getEnrollmentStatusLkp()!=null &&  enrollment.getEmployeeId()!=null &&
						(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM))){

					List<Enrollee> enrolleeList = enrolleeService.findEnrolleeByEnrollmentID(enrollment.getId()); 

					for(Enrollee enrollee:enrolleeList){
						try{
							if((enrollee.getPersonTypeLkp()!=null ) && 
									(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){

								EmployeeDetailsDTO employeeDetails = getEmployeeDetailsByEmployeeId(enrollment.getEmployeeId());

								Map<String,Object> emailData = new HashMap<String, Object>();

								// adding the headerContent to the email notification
								emailData.putAll(getHeaderAndFooter(enrollmentStatusConfirmNotification));

								//required for notice template

								emailData.put(EMAILTITLE,"EnrollmentStatusConfirmed"); //Required for pdf filename and title for the email
								emailData.put(TITLE,"EnrollmentStatusConfirmed"); 
								emailData.put("ModuleID",String.valueOf(enrollment.getEmployeeId()));//Required for pdf filename
								emailData.put("ModuleName",ModuleUserService.EMPLOYEE_MODULE);//Required for pdf filename
								
								List<String> sendToEmailList = null;
								if(employeeDetails.getEmail() != null){
									sendToEmailList = new ArrayList<String>(); //Required for sending email, notifying a notice is generated.
									sendToEmailList.add(employeeDetails.getEmail());
								}
								
								emailData.put("sendToEmailList",sendToEmailList);
								emailData.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
								emailData.put("toFullName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());// who is sending email
								emailData.put("updateSubject","Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+".");
								emailData.put("fileName","Notice-"+emailData.get(EMAILTITLE)+"-"+new TSDate().getTime()+getRandonAsStr()+".pdf");
								emailData.put("exchangePhoneNumber",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));

								// EMPLOYEE ADDRESS INFORMATION
								  if(enrollee.getHomeAddressid()!=null){
									  emailData.put("addressLine1",enrollee.getHomeAddressid().getAddress1());
									  if(StringUtils.isNotBlank(enrollee.getHomeAddressid().getAddress2())){
										  emailData.put("addressLine2",enrollee.getHomeAddressid().getAddress2()+","+"<br/>");
									  }else{
										  emailData.put("addressLine2","");
									  }
									  //emailData.put("county",enrollee.getHomeAddressid().getCounty());
									  emailData.put("city",enrollee.getHomeAddressid().getCity());
									  emailData.put("state",enrollee.getHomeAddressid().getState());
									  emailData.put("zipCode",enrollee.getHomeAddressid().getZip());
								  }
								  
								//required for email template - [EnrollmentStatusConfirmationNotification.html]
								emailData.put("employeeName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());
								emailData.put("enrolledPlanName",enrollment.getPlanName());
								emailData.put("issuerName",enrollment.getInsurerName());
								emailData.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
								emailData.put("currentDate",DateUtil.dateToString(new TSDate(), GhixConstants.EMAIL_TEMPLATE_DATE_FORMAT));
								emailData.put("ssapApplicationId", enrollment.getSsapApplicationid());

								// send notification to the respective employee
								try{
									enrollmentStatusConfirmNotification.setNotificationData(emailData);
									if(sendToEmailList!=null && !sendToEmailList.isEmpty() ){
										enrollmentStatusConfirmNotification.sendNotification(Boolean.TRUE, Boolean.TRUE,  emailData,ghixNoticeCommRef);
									}else{
										emailData.put("ModuleName","");
										enrollmentStatusConfirmNotification.sendNotification(Boolean.FALSE, Boolean.FALSE,  emailData,ghixNoticeCommRef);
									}
										
								} catch (Exception ex) {
									LOGGER.error("Enrollment status confirm email notification failed.",ex);
									throw new GIException("Enrollment status confirm email notification failed.",ex);
								}

							} 
						}catch(Exception e){
							LOGGER.error("Invalid employee id->"+enrollment.getEmployeeId()+"",e);
						}
					}
				} 
			}	 
		}

	}
	
	@Override
	public void sendIRSOutboundFailureNotification(String fileName, String fileLocation, String action) throws GIException {

		List<String> sendToEmailList = new ArrayList<String>(); //Required for sending email, notifying a notice is generated.
		sendToEmailList.add(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EMAIL_GRP_IRS_REPORTING));

		Map<String,String> tokens = new HashMap<String,String>();
		tokens.put(EMAILTITLE, "IRS Outbound Failure Notification");
		tokens.put(TITLE,"IRS Outbound Failure Notification");
//		tokens.put("ResponseCode",responseCode.toString());
//		tokens.put("ResponseDesc",responseDesc);
		tokens.put("File_Name",fileName);
		tokens.put("File_Location",fileLocation);
		tokens.put("Action",action);
		tokens.put("Environment",GhixConstants.APP_URL);
		tokens.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
		tokens.put("toFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
		tokens.put("To", DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EMAIL_GRP_IRS_REPORTING));
		tokens.put("From", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put("exchangeFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put("isIRSOutboundFailureNotification", EnrollmentConstants.TRUE);
    

		// send notification to the respective employee
		try{
			if(sendToEmailList != null){

				for(final String emailId : sendToEmailList){
					final AccountUser user = new AccountUser();
					user.setEmail(emailId);
					user.setFirstName(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					enrollmentIRSOutboundNotification.setUserObj(user);
					enrollmentIRSOutboundNotification.setTokens(tokens);
					enrollmentIRSOutboundNotification.setEnrollmentEmailDetails(tokens);
					enrollmentIRSOutboundNotification.sendEmail(enrollmentIRSOutboundNotification.generateEmail());
				}	
			}
		}catch (Exception ex) {
			LOGGER.error("Enrollment IRS Outbound Failure Email Notification failed.",ex);
			throw new GIException("Enrollment IRS Outbound Failure Email Notification failed.",ex);
		}
	}

	private EmployeeDetailsDTO getEmployeeDetailsByEmployeeId(Integer employeeId)
	{
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EmployeeDetailsDTO employeeDetails=null;
		String getResp = null;

		RestEmployeeDTO restEmployeeDTO = new RestEmployeeDTO();
		restEmployeeDTO.setEmployeeId(employeeId);
		getResp = restTemplate.postForObject(GhixEndPoints.ShopEndPoints.GET_EMPLOYEE_MEMBER_DETAILS,restEmployeeDTO,String.class);
		if(getResp!=null){
			ShopResponse shopResponse = (ShopResponse)xstream.fromXML(getResp);
			Map<String, Object> responseData = shopResponse.getResponseData();
			employeeDetails = (EmployeeDetailsDTO) responseData.get("EmployeeDetails"); 
		}
		return employeeDetails;
	}

	private Map<String,Object> getHeaderAndFooter(NotificationAgent notificationAgent){
		  
  		Map<String,String> templateTokens = new HashMap<String, String>();	
  		Map<String,Object> headerFooterData = new HashMap<String, Object>();
  		
		templateTokens.put(TemplateTokens.HOST,GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
		LOGGER.debug("Exchange full Name " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		templateTokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		templateTokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		templateTokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		templateTokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		templateTokens.put(TemplateTokens.EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		//Now platform will load the header and footer for both email and PDF
		/*if(EnrollmentUtils.isNotNullAndEmpty(notificationAgent.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_HEADER_LOCATION))){
			headerFooterData.put(TemplateTokens.HEADER_CONTENT,notificationAgent.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_HEADER_LOCATION));
		} else {
			headerFooterData.put(TemplateTokens.HEADER_CONTENT,"");
		}
		if(EnrollmentUtils.isNotNullAndEmpty(notificationAgent.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_FOOTER_LOCATION))){
			headerFooterData.put(TemplateTokens.FOOTER_CONTENT,notificationAgent.getTemplateContentWithTokensReplaced(templateTokens, EMAIL_FOOTER_LOCATION));
		} else {
			headerFooterData.put(TemplateTokens.FOOTER_CONTENT,"");
		}*/
		return headerFooterData;  
  }
	
  private String getRandonAsStr() {
	  SecureRandom rand = new SecureRandom(); 
		int value = rand.nextInt(99); 
		return value+"";
	}

  private void sendTerminationNotification(Enrollee enrollee, Enrollment enrollment, String termReasonCode){
	  try{
		  Employer employer = enrollment.getEmployer();
		  GhixNoticeCommunicationMethod ghixNoticeCommRef= GhixNoticeCommunicationMethod.Mail;
		 /* if((enrollee.getPersonTypeLkp()!=null ) && 
				  (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){*/

			  EmployeeDetailsDTO employeeDetails = getEmployeeDetailsByEmployeeId(enrollment.getEmployeeId());
			  
			  Map<String,Object> emailData = new HashMap<String, Object>();
			  // adding the headerContent to the enmail notification
			  emailData.putAll(getHeaderAndFooter(enrollmentTermEmailNotification));

			  //required for notice template

				  emailData.put("isCancelled","No");
				  emailData.put(TITLE,"Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+".");
				  emailData.put("textCancelTerm",enrollment.getEnrollmentStatusLkp().getLookupValueLabel().toLowerCase());
				  emailData.put(EMAILTITLE,"Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+"."); //Required for pdf filename and title for the email
			
			  // EMPLOYEE ADDRESS INFORMATION
			  if(enrollee.getHomeAddressid()!=null){
				  emailData.put("addressLine1",enrollee.getHomeAddressid().getAddress1());
				  if(StringUtils.isNotBlank(enrollee.getHomeAddressid().getAddress2())){
					  emailData.put("addressLine2",enrollee.getHomeAddressid().getAddress2()+","+"<br/>");
				  }else{
					  emailData.put("addressLine2","");
				  }
				  emailData.put("location", enrollee.getHomeAddressid());
				  emailData.put("county",enrollee.getHomeAddressid().getCounty());
				  emailData.put("city",enrollee.getHomeAddressid().getCity());
				  emailData.put("state",enrollee.getHomeAddressid().getState());
				  emailData.put("zipCode",enrollee.getHomeAddressid().getZip());
			  }
			  
			  emailData.put("currentDate",DateUtil.dateToString(new TSDate(), GhixConstants.EMAIL_TEMPLATE_DATE_FORMAT));								  
			  emailData.put("ModuleID",String.valueOf(enrollment.getEmployeeId()));//Required for pdf filename
			  emailData.put("ModuleName",ModuleUserService.EMPLOYEE_MODULE);//Required for pdf filename
			 
			  
			  List<String> sendToEmailList = null;
			  if(employeeDetails.getEmail() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
				  sendToEmailList = new ArrayList<String>(); //Required for sending email, notifying a notice is generated.
				  sendToEmailList.add(employeeDetails.getEmail());
			  }
			  
			  emailData.put("sendToEmailList",sendToEmailList);
			  emailData.put("fromFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));// who is sending email
			  emailData.put("toFullName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());// who is sending email
			  emailData.put("updateSubject","Important information about your health insurance coverage with "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)+".");
			  emailData.put("fileName","Notice-"+emailData.get(EMAILTITLE)+"-"+new TSDate().getTime()+".pdf");
			  emailData.put("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			  emailData.put("exchangeURL",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
			  emailData.put("exchangeFullName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));


			  //required for email template - [EnrollmentStatusCancelTermNotification.html]
			  emailData.put("employeeName",employeeDetails.getFirstName()+" "+employeeDetails.getLastName());
			  emailData.put("enrolledPlanName",enrollment.getPlanName());
			  emailData.put("issuerName",enrollment.getInsurerName());
			  emailData.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			  emailData.put("employerName",employer.getName());
			  String terminationDate = DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT);
			  emailData.put("terminationDate",terminationDate);
			  String effectiveStartDate = DateUtil.dateToString(enrollee.getEffectiveStartDate(), GhixConstants.REQUIRED_DATE_FORMAT);
			  emailData.put("terminationDate",terminationDate);
			  emailData.put("effectiveStartDate",effectiveStartDate);
			  emailData.put("employerPhoneNumber",employer.getContactNumber());
			  emailData.put("exchangeSignature",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_SIGNATURE));
			  emailData.put("stateContinuation",DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.STATE_CONTINUATION)); 
			  emailData.put("terminationReasonCodeEn", lookupService.getLookupValueLabel(EnrollmentConstants.EVENT_REASON,termReasonCode));
			  emailData.put("terminationReasonCodeEs", lookupService.getLookupValueLabel(EnrollmentConstants.EVENT_REASON,termReasonCode,GhixConstants.SPANISH));
			  emailData.put("ssapApplicationId", enrollment.getSsapApplicationid());
			  //  send notification to the respective employee
			  try{
			      enrollmentTermEmailNotification.setNotificationData(emailData);
			      if(sendToEmailList!=null && !sendToEmailList.isEmpty() ){
			    	  enrollmentTermEmailNotification.sendNotification(Boolean.TRUE, Boolean.TRUE,  emailData,ghixNoticeCommRef); 
			    	  enrollmentTermEmailNotification.sendMail();
			      }else{
					  emailData.put("ModuleName","");
			    	  enrollmentTermEmailNotification.sendNotification(Boolean.FALSE, Boolean.TRUE,  emailData,ghixNoticeCommRef);
			      }
				  
				  
			  }catch (Exception ex) {
				  LOGGER.error("Enrollment status email notification failed.",ex);
				  throw new GIException("Enrollment status email notification failed.",ex);
			  }

		 // } 
	  }catch(Exception e){
		  LOGGER.error("Invalid employee id->"+enrollment.getEmployeeId()+"",e);
	  }
  }
}
