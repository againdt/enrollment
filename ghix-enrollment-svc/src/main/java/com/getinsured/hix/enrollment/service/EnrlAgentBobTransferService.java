/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.model.enrollment.EnrlAgentBOBTransfer;

/**
 * @author panda_p
 *
 */
public interface EnrlAgentBobTransferService {

	/**
	 * @author panda_p
	 * @since 15/01/2018
	 * 
	 * Log enrlAgentBOBTransfer request and response
	 * 
	 * @param enrlAgentBOBTransfer
	 * @return
	 */
	EnrlAgentBOBTransfer saveAgentBobTransferLog(EnrlAgentBOBTransfer enrlAgentBOBTransfer);
	void serveEnrlAgentBOBTransferJob(EnrlAgentBOBTransfer bobTransfer) throws  Exception;
	void processBOBTransferRequest(List<Integer> enrollmentIdList, Map<String, String> enrollmentSkipMap, EnrollmentBrokerUpdateDTO brokerReq);
	
	/**
	 * 
	 * @param Object<EnrlAgentBOBTransfer.class>enrlAgentBOBTransfer
	 */
	void processAgentBOBTranferAsync(EnrlAgentBOBTransfer enrlAgentBOBTransfer);
}
