package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
//import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentRequest;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentResponse;

/**
 * @author raja
 * @since 10/05/2013
 * 
 */

@Service("enrollmentEmployerDisEnrollService")
@Transactional
public class EnrollmentEmployerDisEnrollServiceImpl implements EnrollmentEmployerDisEnrollService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEmployerDisEnrollServiceImpl.class);
	
	@Autowired private EnrollmentService enrollmentService;
//	@Autowired private EnrolleeService enrolleeService;
//	@Autowired private UserService userService;
	@Autowired private GroupInstallationService groupInstallationService;
	@Autowired private EnrollmentEmailNotificationService enrollmentEmailNotificationService;
	
	@Override
    public EmployerDisEnrollmentResponse employerDisEnrollment(EmployerDisEnrollmentRequest employerDisEnrollmentRequest, boolean isInternal) throws GIException{
		List<Enrollment> enrollmentList = null;
		EmployerDisEnrollmentResponse employerDisEnrollmentResponse = new EmployerDisEnrollmentResponse();
		/**
		 * employerId will be ExternalId for External Call (for SOAP Request)
		 */
		
		if(employerDisEnrollmentRequest == null){
			employerDisEnrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_201));
			employerDisEnrollmentResponse.setResponseDescription(EnrollmentConstants.ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL);
		}else if(employerDisEnrollmentRequest.getEmployerCaseID() == null || employerDisEnrollmentRequest.getEmployerCaseID().equalsIgnoreCase("")){
			employerDisEnrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_202));
			employerDisEnrollmentResponse.setResponseDescription(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_NULL_OR_EMPTY);
		}else if(employerDisEnrollmentRequest.getEmployerEnrollmentId() == null || employerDisEnrollmentRequest.getEmployerEnrollmentId().intValue() <= 0){
			employerDisEnrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_203));
			employerDisEnrollmentResponse.setResponseDescription(EnrollmentConstants.ERR_MSG_EMPLOYER_ENROLLMENT_ID_NULL_OR_EMPTY);
		}
		else{
			int numberOfEmployees = (employerDisEnrollmentRequest.getNumberOfEmployees()!=null) ? employerDisEnrollmentRequest.getNumberOfEmployees().shortValue() : 0;
			String employerId = employerDisEnrollmentRequest.getEmployerCaseID();
			Integer employerEnrollmentId = employerDisEnrollmentRequest.getEmployerEnrollmentId().intValue();

			if(isInternal){
				if(Integer.parseInt(employerId) > 0 && employerEnrollmentId != null && employerEnrollmentId > 0){
					
					if(employerDisEnrollmentRequest.getTerminationDate()!=null){
						enrollmentList = (ArrayList<Enrollment>)enrollmentService.findByEmployerAndEmployerEnrollmentIdAndDate(Integer.parseInt(employerId),employerEnrollmentId, DateUtil.StringToDate(employerDisEnrollmentRequest.getTerminationDate(),GhixConstants.REQUIRED_DATE_FORMAT));
					}else{
						enrollmentList = (ArrayList<Enrollment>)enrollmentService.findByEmployerAndEmployerEnrollmentId(Integer.parseInt(employerId),employerEnrollmentId);
					}
					
				}else{
					
					employerDisEnrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_202));
					employerDisEnrollmentResponse.setResponseDescription(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_NULL_OR_EMPTY +" = " + employerId );
					LOGGER.info(EnrollmentConstants.ERR_MSG_EMPLOYER_ID_NULL_OR_EMPTY);
					LOGGER.info(EnrollmentConstants.ERR_MSG_EMPLOYER_ENROLLMENT_ID_NULL_OR_EMPTY);
				}

			}else{
				
				if(!(DateUtil.isValidDate(employerDisEnrollmentRequest.getTerminationDate(), GhixConstants.REQUIRED_DATE_FORMAT))){
					throw new GIException(EnrollmentConstants.ERROR_CODE_203,"Validation Error :: TerminationDate = "+employerDisEnrollmentRequest.getTerminationDate()+" is not a valid date for given Employer Case ID = "+employerId , EnrollmentConstants.HIGH);
				}
				enrollmentList = (ArrayList<Enrollment>)enrollmentService.findEnrollmentByExternalID(employerId);
			}	

			if(enrollmentList == null || enrollmentList.isEmpty()){
				
				employerDisEnrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_204));
				employerDisEnrollmentResponse.setResponseDescription(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_EMPLOYER_CASE_ID +" = " + employerId );
				LOGGER.info(EnrollmentConstants.ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_EMPLOYER_CASE_ID);
				//GroupTermination - Carries are missing GRP XML files when group is terminated for  no active enrollments available during group termination
				if(employerDisEnrollmentRequest.getTerminationDate()!=null){
					Date termDate = DateUtil.StringToDate(employerDisEnrollmentRequest.getTerminationDate(), GhixConstants.REQUIRED_DATE_FORMAT);
					groupInstallationService.terminateGroupForEmployer(Integer.parseInt(employerId),employerEnrollmentId,termDate);
				}else{
					Date earliestEffStartDate=enrollmentService.findEarliestEffStartDate(employerDisEnrollmentRequest.getEmployerEnrollmentId().intValue());
					groupInstallationService.terminateGroupForEmployer(Integer.parseInt(employerId),employerEnrollmentId,earliestEffStartDate);
				}
				
			}else if(!isInternal && enrollmentList.size() != numberOfEmployees){
				
				employerDisEnrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_203));
				employerDisEnrollmentResponse.setResponseDescription(EnrollmentConstants.ERR_MSG_EMPLOYEE_COUNT_DOES_NOT_MATCH_EMPLOYEE_ENROLLMENTS);
				LOGGER.info(EnrollmentConstants.ERR_MSG_EMPLOYEE_COUNT_DOES_NOT_MATCH_EMPLOYEE_ENROLLMENTS + " = " +numberOfEmployees);
			
			}else{
				
				AccountUser user = enrollmentService.getLoggedInUser();
				LOGGER.info("Enrollment Size = " + enrollmentList.size());
					
				/*List<Long> enrollmentCancelIdList = employerDisEnrollmentRequest.getEnrollmentIdList();
				List<Enrollment> enrollmentCancelList = new ArrayList<>();
				
				for (Long enrlId : enrollmentCancelIdList) {
					for (Enrollment enrollment : enrollmentList) {
						if(enrollment.getId().longValue() == enrlId){
							enrollmentCancelList.add(enrollment);
						}
					}
				}
				
				if(enrollmentCancelList != null && !enrollmentCancelList.isEmpty()){
					enrollmentList.removeAll(enrollmentCancelList);
				}*/
				
				EnrollmentDisEnrollmentDTO disenrollDto = new EnrollmentDisEnrollmentDTO();
				// Set E
				disenrollDto.setEnrollmentList(enrollmentList);
				disenrollDto.setTerminationReasonCode(employerDisEnrollmentRequest.getTerminationReason());
				disenrollDto.setTerminationDate(employerDisEnrollmentRequest.getTerminationDate());
				disenrollDto.setLastPaidInvoiceDate(employerDisEnrollmentRequest.getLastPaidInvoiceDate());
				
				// Set Updated By user as Exchange
				disenrollDto.setUpdatedBy(user);
				disenrollDto.setAllowRetroTermination(true);
				
				enrollmentList = enrollmentService.disEnrollEnrollment(disenrollDto, EnrollmentEvent.TRANSACTION_IDENTIFIER.EMPLOYER_DISENROLLMENT);
				

				employerDisEnrollmentResponse.setResponseCode(Integer.toString(EnrollmentConstants.ERROR_CODE_200));
				employerDisEnrollmentResponse.setResponseDescription(GhixConstants.RESPONSE_SUCCESS);

				if(isInternal){
					if(employerDisEnrollmentRequest.getTerminationDate()==null){
						if(employerDisEnrollmentRequest.getEmployerEnrollmentId() == null){
							LOGGER.info("Employer Enrollment Id is null");
						}else{
							Date earliestEffStartDate=enrollmentService.findEarliestEffStartDate(employerDisEnrollmentRequest.getEmployerEnrollmentId().intValue());
							if(earliestEffStartDate!=null){
								employerDisEnrollmentResponse.setEarliestEffStartDate(DateUtil.dateToString(earliestEffStartDate,  GhixConstants.REQUIRED_DATE_FORMAT));
							}
						}
					}
					if(employerDisEnrollmentRequest.getTerminationDate()!=null){
						Date termDate = DateUtil.StringToDate(employerDisEnrollmentRequest.getTerminationDate(), GhixConstants.REQUIRED_DATE_FORMAT);
						groupInstallationService.terminateGroupForEmployer(Integer.parseInt(employerId),employerEnrollmentId,termDate);
					}else{
						Date earliestEffStartDate=enrollmentService.findEarliestEffStartDate(employerDisEnrollmentRequest.getEmployerEnrollmentId().intValue());
						groupInstallationService.terminateGroupForEmployer(Integer.parseInt(employerId),employerEnrollmentId,earliestEffStartDate);
					}
					// sending the email notification to the employees who's enrollment status is cancelled or terminated for ENROLLMENT_TYPE_SHOP
					enrollmentEmailNotificationService.sendEnrollmentStatusNotification(enrollmentList,employerDisEnrollmentRequest.getTerminationReason());
				}
			}
		}
		return employerDisEnrollmentResponse;
	}
}
