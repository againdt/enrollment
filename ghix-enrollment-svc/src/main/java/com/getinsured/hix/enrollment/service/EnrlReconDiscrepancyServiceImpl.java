package com.getinsured.hix.enrollment.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.DiscrepancyActionRequestDTO;
import com.getinsured.hix.dto.enrollment.DiscrepancyCommentDTO;
import com.getinsured.hix.dto.enrollment.DiscrepancyDetailDTO;
import com.getinsured.hix.dto.enrollment.DiscrepancyEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.DiscrepancyMemberDTO;
import com.getinsured.hix.dto.enrollment.DiscrepancyPremiumDTO;
import com.getinsured.hix.dto.enrollment.EnrlReconciliationResponse;
import com.getinsured.hix.dto.enrollment.EnrollmentReconRequest;
import com.getinsured.hix.dto.enrollment.ReconEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.ReconMemberDTO;
import com.getinsured.hix.dto.enrollment.ReconMonthlyPremiumDTO;
import com.getinsured.hix.enrollment.repository.IEnrlReconCommentRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconDiscrepancyRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSnapshotRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.enrollment.EnrlReconComment;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy.DiscrepancyStatus;
import com.getinsured.hix.model.enrollment.EnrlReconLkp;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.comment.repository.ICommentRepository;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;

@Service("enrlReconDiscrepancyService")
public class EnrlReconDiscrepancyServiceImpl implements EnrlReconDiscrepancyService{
	@SuppressWarnings("rawtypes")
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired private ICommentRepository iCommentRepository;
	@Autowired private IEnrlReconCommentRepository iEnrlReconCommentRepository;
	@Autowired private IEnrlReconDiscrepancyRepository iEnrlReconDiscrepancyRepository;
	@Autowired private IEnrlReconSnapshotRepository iEnrlReconSnapshotRepository;
	@Autowired private IEnrollmentRepository iEnrollmentRepository;
	@Autowired private LookupService lookupService;
	@Autowired private Gson platformGson;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	private static final Logger LOGGER = Logger.getLogger(EnrlReconDiscrepancyServiceImpl.class);
	private static final String NEWLINEBREAK="&#xa;";
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@PersistenceUnit private EntityManagerFactory emf;
	private static final String SEARCH_RECON_DISCREPANCY_QUERY = "SELECT erd.id, erd.CREATION_TIMESTAMP, erd.FILE_ID, erd.HIX_ENROLLMENT_ID, erd.ISSUER_ENROLLMENT_ID, erd.MEMBER_ID,"
			+ " erd.STATUS, enrlLkp.CODE, enrlLkp.LABEL, enrlLkp.CATEGORY, erd.HIX_VALUE,  erd.ISSUER_VALUE"
			+ " FROM ENRL_RECON_DISCREPANCY erd, ENRL_RECON_LKP enrlLkp, ENRL_RECON_SUMMARY ersu "
			+ " WHERE erd.ENRL_RECON_LKP_ID = enrlLkp.id "
			+ " AND erd.file_id = ersu.id "
			+ " AND ersu.status NOT IN ('Duplicate') ";
	private static final String COUNT_RECON_DISCREPANCY_QUERY = "SELECT COUNT(*) FROM (";
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@SuppressWarnings("rawtypes")
	@Override
	public EnrlReconciliationResponse getEnrlReconDiscrepancyDetail(EnrollmentReconRequest enrollmentReconRequest, EnrlReconciliationResponse enrlReconciliationResponse)
	{	
		EntityManager entityManager = null;
		try 
		{
			String countQueryString = null;
			StringBuilder reconSQLQuery = new StringBuilder(200);
			reconSQLQuery.append(SEARCH_RECON_DISCREPANCY_QUERY);
			
			if(enrollmentReconRequest.getHixEnrollmentId() != null && enrollmentReconRequest.getHixEnrollmentId() != EnrollmentConstants.ZERO){
				reconSQLQuery.append(" AND erd.HIX_ENROLLMENT_ID = ");
				reconSQLQuery.append(enrollmentReconRequest.getHixEnrollmentId());
			}
			if(enrollmentReconRequest.getDiscrepancyStatusList() != null && !enrollmentReconRequest.getDiscrepancyStatusList().isEmpty()){
				reconSQLQuery.append(" AND erd.STATUS IN(:DISCREPANCY_STATUS) ");
			}
			
			countQueryString = COUNT_RECON_DISCREPANCY_QUERY + reconSQLQuery.toString() + ")";
			
			reconSQLQuery.append(" ORDER BY ");
			if(enrollmentReconRequest.getSortByField() != null){	
				if(enrollmentReconRequest.getSortByField().equals("reportedDate")){
					reconSQLQuery.append(" erd.CREATION_TIMESTAMP ");
				}
				else if(enrollmentReconRequest.getSortByField().equals("discrepancyCategory")){
					reconSQLQuery.append(" enrlLkp.CATEGORY ");
				}
				else if(enrollmentReconRequest.getSortByField().equals("discrepancyLabel")){
					reconSQLQuery.append(" enrlLkp.LABEL ");
				}
				else if(enrollmentReconRequest.getSortByField().equals("hixValue")){
					reconSQLQuery.append(" erd.HIX_VALUE ");
				}
				else if(enrollmentReconRequest.getSortByField().equals("issuerValue")){
					reconSQLQuery.append(" erd.ISSUER_VALUE ");
				}
				else if(enrollmentReconRequest.getSortByField().equals("discrepancyStatus")){
					reconSQLQuery.append(" erd.STATUS ");
				}
			}
			else{
				reconSQLQuery.append(" erd.id ");
			}
			
			if(StringUtils.isNotBlank(enrollmentReconRequest.getSortOrder())){
				reconSQLQuery.append(enrollmentReconRequest.getSortOrder().toUpperCase());
			}
			else{
				reconSQLQuery.append(SortOrder.DESC.toString());
			}
			
			entityManager = emf.createEntityManager();
			Query selectQuery = entityManager.createNativeQuery(reconSQLQuery.toString());
			Query countQuery = entityManager.createNativeQuery(countQueryString);
			if(enrollmentReconRequest.getDiscrepancyStatusList() != null && !enrollmentReconRequest.getDiscrepancyStatusList().isEmpty()){
				List<String> discrepancyStatusList = new ArrayList<String>();
				for(DiscrepancyStatus discrepancyStatus : enrollmentReconRequest.getDiscrepancyStatusList()){
					discrepancyStatusList.add(discrepancyStatus.toString());
				}
				selectQuery.setParameter("DISCREPANCY_STATUS", discrepancyStatusList);
				countQuery.setParameter("DISCREPANCY_STATUS", discrepancyStatusList);
			}
						
			if(enrollmentReconRequest.getPageSize() != null){
				selectQuery.setMaxResults(enrollmentReconRequest.getPageSize());
			}
			Integer startRecord = EnrollmentConstants.ZERO; 
			if(enrollmentReconRequest.getPageNumber() != null && 
					enrollmentReconRequest.getPageNumber() != EnrollmentConstants.ZERO){
				startRecord = enrollmentReconRequest.getPageSize() != null ?  (enrollmentReconRequest.getPageNumber() * enrollmentReconRequest.getPageSize()) - enrollmentReconRequest.getPageSize(): EnrollmentConstants.ZERO;
			}
			selectQuery.setFirstResult(startRecord);
			
			List<DiscrepancyDetailDTO> discrepancyDetailDTOList = null;
			Number totalRecordCount = (Number) countQuery.getSingleResult();
			List<?> objectList = selectQuery.getResultList();
			if(objectList != null && !objectList.isEmpty()){
				discrepancyDetailDTOList = new ArrayList<DiscrepancyDetailDTO>();
				
				Iterator rsIterator = objectList.iterator();
				Object[] enrlReconDiscrepancyObj = null;
				while (rsIterator.hasNext()) {
					enrlReconDiscrepancyObj = (Object[]) rsIterator.next();				
					if(null != enrlReconDiscrepancyObj[EnrollmentConstants.ZERO]){
						DiscrepancyDetailDTO discrepancyDetailDTO = new DiscrepancyDetailDTO();
						discrepancyDetailDTO.setDiscrepancyId(enrlReconDiscrepancyObj[EnrollmentConstants.ZERO] != null ?
								((Number)enrlReconDiscrepancyObj[EnrollmentConstants.ZERO]).intValue() :  EnrollmentConstants.ZERO);
						discrepancyDetailDTO.setReportedDate(enrlReconDiscrepancyObj[EnrollmentConstants.ONE] != null ?
								DateUtil.dateToString((Date)enrlReconDiscrepancyObj[EnrollmentConstants.ONE], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY)  : null);
						discrepancyDetailDTO.setFileId(enrlReconDiscrepancyObj[EnrollmentConstants.TWO] != null ?
								((Number)enrlReconDiscrepancyObj[EnrollmentConstants.TWO]).intValue() : EnrollmentConstants.ZERO);
						discrepancyDetailDTO.setHixEnrollmentId(enrlReconDiscrepancyObj[EnrollmentConstants.THREE] != null ?
								Long.valueOf((enrlReconDiscrepancyObj[EnrollmentConstants.THREE]).toString()): null);
						discrepancyDetailDTO.setIssuerEnrollmentId(enrlReconDiscrepancyObj[EnrollmentConstants.FOUR] != null ?
								((Number)enrlReconDiscrepancyObj[EnrollmentConstants.FOUR]).intValue(): null);
						discrepancyDetailDTO.setMemberId(enrlReconDiscrepancyObj[EnrollmentConstants.FIVE] != null ?
								((Number)enrlReconDiscrepancyObj[EnrollmentConstants.FIVE]).intValue(): null);
						discrepancyDetailDTO.setDiscrepancyStatus(enrlReconDiscrepancyObj[EnrollmentConstants.SIX] != null ? DiscrepancyStatus.valueOf((String)enrlReconDiscrepancyObj[EnrollmentConstants.SIX]) : null);
						
						discrepancyDetailDTO.setDiscrepancyCode((String)enrlReconDiscrepancyObj[EnrollmentConstants.SEVEN]);
						discrepancyDetailDTO.setDiscrepancyLabel((String)enrlReconDiscrepancyObj[EnrollmentConstants.EIGHT]);
						discrepancyDetailDTO.setDiscrepancyCategory((String)enrlReconDiscrepancyObj[EnrollmentConstants.NINE]);
						
						if(StringUtils.isNotBlank(discrepancyDetailDTO.getDiscrepancyCode()) && EnrollmentConstants.ReconErrorCode.SSN_ERRORCODE.equals(discrepancyDetailDTO.getDiscrepancyCode())){
							discrepancyDetailDTO.setHixValue(maskSSN((String)enrlReconDiscrepancyObj[EnrollmentConstants.TEN]));
							discrepancyDetailDTO.setIssuerValue(maskSSN((String)enrlReconDiscrepancyObj[EnrollmentConstants.ELEVEN]));
						}
						else{
							discrepancyDetailDTO.setHixValue((String)enrlReconDiscrepancyObj[EnrollmentConstants.TEN]);
							discrepancyDetailDTO.setIssuerValue((String)enrlReconDiscrepancyObj[EnrollmentConstants.ELEVEN]);
						}
						
						discrepancyDetailDTOList.add(discrepancyDetailDTO);
					}
				}
			}
			enrlReconciliationResponse.setDiscrepancyDetailDTOList(discrepancyDetailDTOList);
			enrlReconciliationResponse.setTotalRecordCount(totalRecordCount != null ? totalRecordCount.intValue(): EnrollmentConstants.ZERO);
		} catch (Exception ex) {
			LOGGER.error("ENRL_RECON_DISCREPANCY:: Exception occurred in getOpenDiscrepancyDetails(-,-) " ,ex);
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		finally{
			if(entityManager != null && entityManager.isOpen()){
				try{
					entityManager.clear();
					entityManager.close();
					entityManager = null;
				}
				catch(IllegalArgumentException ex){
					//Suppress exception
					LOGGER.error("Exception caught while closing EntityManager: ", ex);
				}
			}
		}
		return enrlReconciliationResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EnrlReconciliationResponse getEnrlReconCommentDetail(EnrollmentReconRequest enrollmentReconRequest,
			EnrlReconciliationResponse enrlReconciliationResponse) {
		try{
			List<String> joinTables = new ArrayList<String>();
			joinTables.add("discrepancy");
			joinTables.add("comment");
			
			
			QueryBuilder<EnrlReconComment> applicationQuery = delegateFactory.getObject();
			applicationQuery.buildSelectQuery(EnrlReconComment.class, joinTables);
			
			if(enrollmentReconRequest.getHixEnrollmentId() != null && enrollmentReconRequest.getHixEnrollmentId() != EnrollmentConstants.ZERO){
				applicationQuery.applyWhere("discrepancy.hixEnrollmentId", enrollmentReconRequest.getHixEnrollmentId(), DataType.NUMERIC, ComparisonType.EQ);
			}
			if(enrollmentReconRequest.getDiscrepancyStatusList() != null && !enrollmentReconRequest.getDiscrepancyStatusList().isEmpty()){
				applicationQuery.applyWhere("discrepancy.status", enrollmentReconRequest.getDiscrepancyStatusList(), DataType.ENUM, ComparisonType.EQUALS);
			}
			
			List<String> columns = new ArrayList<String>();
			columns.add("comment.id");
			columns.add("comment.comment");
			columns.add("comment.created");
			columns.add("comment.commenterName");
			columns.add("comment.updated");
			columns.add("discrepancy.id");
			columns.add("discrepancy.enrlReconLkpId");
			applicationQuery.applySelectColumns(columns);
			
			String sortBy = "comment.created";
			if(enrollmentReconRequest.getSortByField() != null){
				 if(enrollmentReconRequest.getSortByField().equals("commentAddedOn")){
					 sortBy = "comment.created";
				 }
				 else if(enrollmentReconRequest.getSortByField().equals("commentedByUser")){
					 sortBy =  "comment.commenterName";
				 }
				 else if(enrollmentReconRequest.getSortByField().equals("discrepancyLabel")){
					 sortBy = "discrepancy.enrlReconLkpId.label";
				 }
				 else if(enrollmentReconRequest.getSortByField().equals("commentText")){
					 sortBy = "comment.comment";
				 }
			}
			
			String sortOrder = enrollmentReconRequest.getSortOrder() != null ? enrollmentReconRequest.getSortOrder().toUpperCase() : SortOrder.DESC.toString();
			applicationQuery.applySort(sortBy, SortOrder.valueOf(sortOrder));
			
			Integer pageSize = -EnrollmentConstants.ONE;
			if(enrollmentReconRequest.getPageSize() != null){
				pageSize = enrollmentReconRequest.getPageSize();
			}
			Integer startRecord = EnrollmentConstants.ZERO; 
			if(enrollmentReconRequest.getPageNumber() != null && enrollmentReconRequest.getPageNumber() != EnrollmentConstants.ZERO &&
					pageSize != -EnrollmentConstants.ONE){
				startRecord = ((enrollmentReconRequest.getPageNumber() * pageSize) - pageSize);
			}
			applicationQuery.setFetchDistinct(true);

			List<Map<String, Object>> enrlReconCommentDataList  =  applicationQuery.getData(startRecord, pageSize);
			enrlReconciliationResponse.setTotalRecordCount(applicationQuery.getRecordCount().intValue());
			if(enrlReconCommentDataList != null && !enrlReconCommentDataList.isEmpty()){
				List<DiscrepancyCommentDTO> discrepancyCommentDTOList = new ArrayList<DiscrepancyCommentDTO>();
				for(Map<String, Object> enrlReconCommentObj : enrlReconCommentDataList){
					DiscrepancyCommentDTO discrepancyCommentDTO = new DiscrepancyCommentDTO();
					discrepancyCommentDTO.setCommentId((Integer) enrlReconCommentObj.get("commentid"));
					discrepancyCommentDTO.setCommentText(enrlReconCommentObj.get("commentcomment") != null ? 
							ESAPI.encoder().decodeForHTML(((String)  enrlReconCommentObj.get("commentcomment")).replaceAll(NEWLINEBREAK, StringUtils.SPACE))
							: null);
					discrepancyCommentDTO.setCommentAddedOn((Date) enrlReconCommentObj.get("commentcreated"));
					discrepancyCommentDTO.setCommentedByUser((String) enrlReconCommentObj.get("commentcommenterName"));				
					discrepancyCommentDTO.setCommentLastUpdatedOn((Date)enrlReconCommentObj.get("commentupdated"));
					discrepancyCommentDTO.setDiscrepancyId((Integer)enrlReconCommentObj.get("discrepancyid"));
					EnrlReconLkp enrlReconLkp = (EnrlReconLkp) enrlReconCommentObj.get("discrepancyenrlReconLkpId");
					if(enrlReconLkp != null){
						discrepancyCommentDTO.setDiscrepancyCode(enrlReconLkp.getCode());
						discrepancyCommentDTO.setDiscrepancyLabel(enrlReconLkp.getLabel());
					}
					discrepancyCommentDTOList.add(discrepancyCommentDTO);
				}
				enrlReconciliationResponse.setDiscrepancyCommentDTOList(discrepancyCommentDTOList);
			}
		}
		catch(Exception ex){
			LOGGER.error("ENRL_RECON_DISCREPANCY:: Exception occurred in getEnrlReconCommentDetail(-,-) " ,ex);
			enrlReconciliationResponse.setErrCode(EnrollmentConstants.ERROR_CODE_209);
			enrlReconciliationResponse.setErrMsg(EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			enrlReconciliationResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return enrlReconciliationResponse;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@Override
	public void addEditCommentOrSnoozeDiscrepancy(final DiscrepancyActionRequestDTO discrepancyActionRequestDTO, final AccountUser accountUser) throws GIException{
			
			Comment comment = null;
			EnrlReconDiscrepancy  enrlReconDiscrepancy  = null;
			boolean isAddEditOrSnooze = false;
			Map<String, String> requestParam = new HashMap<String, String>();
			switch(discrepancyActionRequestDTO.getDiscrepancyAction()){
			case ADD_COMMENT:
				enrlReconDiscrepancy = iEnrlReconDiscrepancyRepository.findOne(discrepancyActionRequestDTO.getDiscrepancyId());
				comment = new Comment();
				comment.setComment(discrepancyActionRequestDTO.getCommentText());
				comment.setCommenterName(accountUser.getFullName());
				comment.setComentedBy(accountUser.getId());
				enrlReconDiscrepancy.setLastUpdateBy(accountUser.getId());
				isAddEditOrSnooze = true;
				requestParam.put("Action", "Add Comment");
				break;
			case EDIT_COMMENT:
				comment = iCommentRepository.findById(discrepancyActionRequestDTO.getCommentId());
				comment.setComment(discrepancyActionRequestDTO.getCommentText());
				comment.setCommenterName(accountUser.getFullName());
				comment.setComentedBy(accountUser.getId());
				iCommentRepository.save(comment);
				requestParam.put("Action", "Edit Comment");
				break;
			case HOLD:
				enrlReconDiscrepancy = iEnrlReconDiscrepancyRepository.findOne(discrepancyActionRequestDTO.getDiscrepancyId());
				comment = new Comment();
				comment.setComment(discrepancyActionRequestDTO.getCommentText());
				comment.setCommenterName(accountUser.getFullName());
				comment.setComentedBy(accountUser.getId());
				enrlReconDiscrepancy.setLastUpdateBy(accountUser.getId());
				enrlReconDiscrepancy.setStatus(DiscrepancyStatus.HOLD);
				requestParam.put("Action", "Hold");
				isAddEditOrSnooze = true;
				break;
			case HOLD_ALL:
				List<EnrlReconDiscrepancy> enrlReconDiscrepancyList = new ArrayList<EnrlReconDiscrepancy>();
				List<DiscrepancyStatus> discrepancyStatusList = new ArrayList<DiscrepancyStatus>();
				discrepancyStatusList.add(DiscrepancyStatus.OPEN);
				requestParam.put("Action", "Hold All");
				enrlReconDiscrepancyList = iEnrlReconDiscrepancyRepository.findByHixEnrllmentIdAndStatus(new Long (discrepancyActionRequestDTO.getEnrollmentId()), discrepancyStatusList);
				if(enrlReconDiscrepancyList != null && !enrlReconDiscrepancyList.isEmpty()){
					for(EnrlReconDiscrepancy enrlReconDiscrepancyObj: enrlReconDiscrepancyList){
						enrlReconDiscrepancyObj.setLastUpdateBy(accountUser.getId());
						enrlReconDiscrepancyObj.setStatus(DiscrepancyStatus.HOLD);
						Comment snoozeAllcomment = new Comment();
						snoozeAllcomment.setComment(discrepancyActionRequestDTO.getCommentText());
						snoozeAllcomment.setCommenterName(accountUser.getFullName());
						snoozeAllcomment.setComentedBy(accountUser.getId());
						EnrlReconComment enrlReconComment = new EnrlReconComment();
						enrlReconComment.setComment(snoozeAllcomment);
						enrlReconComment.setDiscrepancy(enrlReconDiscrepancyObj);
						iEnrlReconCommentRepository.saveAndFlush(enrlReconComment);
					}
				}
				break;
			default:
				throw new GIException("Received Invalid Action code for processing: "+discrepancyActionRequestDTO.getDiscrepancyAction());
			}
			if(isAddEditOrSnooze && enrlReconDiscrepancy != null && comment != null){
				EnrlReconComment enrlReconComment = new EnrlReconComment();
				enrlReconComment.setComment(comment);
				enrlReconComment.setDiscrepancy(enrlReconDiscrepancy);
				iEnrlReconCommentRepository.saveAndFlush(enrlReconComment);
				
				if(enrlReconDiscrepancy.getHixEnrollmentId() != null && enrlReconDiscrepancy.getHixEnrollmentId() != 0){
					Enrollment enrollment = iEnrollmentRepository.getEnrollmentByEnrollmentId(enrlReconDiscrepancy.getHixEnrollmentId().intValue());
					if(enrollment != null){
						
						
						requestParam.put("Discrepany Name", enrlReconDiscrepancy.getEnrlReconLkpId() != null ? enrlReconDiscrepancy.getEnrlReconLkpId().getLabel() : null);
						enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.CARRIER_RECONCILIATION.toString(), requestParam, comment.getComment(), accountUser);
					}
					else{
						if(LOGGER.isInfoEnabled()){
							LOGGER.info("Suppressing Application Event since no Enrollment found with ID: "+enrlReconDiscrepancy.getId());
						}
					}
				}
				else{
					if(LOGGER.isInfoEnabled()){
						LOGGER.info("Suppressing Application Event since hix enrollment id is null in enrl_recon_discrepancy Id: "+enrlReconDiscrepancy.getId());
					}
				}
				
			}
	}

	@Override
	public DiscrepancyEnrollmentDTO getEnrlReconVisualSummaryData(Integer enrollmentId) throws GIException {
		DiscrepancyEnrollmentDTO discrepancyEnrollmentDTO = new DiscrepancyEnrollmentDTO();
		List<Object[]> enrlReconSnapshotDataList = iEnrlReconSnapshotRepository.getIssuerAndHixDataByEnrollmentId(enrollmentId.longValue());
		
		if(enrlReconSnapshotDataList != null && !enrlReconSnapshotDataList.isEmpty()){
			Object[] enrlReconSnapshotObj = enrlReconSnapshotDataList.get(EnrollmentConstants.ZERO);
			if(enrlReconSnapshotObj != null){
				ReconEnrollmentDTO hixEnrollmentDataDto = enrlReconSnapshotObj[EnrollmentConstants.THREE] != null ? platformGson.fromJson((String)enrlReconSnapshotObj[EnrollmentConstants.THREE], ReconEnrollmentDTO.class) : null; 
				ReconEnrollmentDTO issuerEnrollmentDataDto = enrlReconSnapshotObj[EnrollmentConstants.FOUR] != null ? platformGson.fromJson((String)enrlReconSnapshotObj[EnrollmentConstants.FOUR], ReconEnrollmentDTO.class) : new ReconEnrollmentDTO();
				List<DiscrepancyStatus> discrepancyStatusList = new ArrayList<DiscrepancyStatus>();
				discrepancyStatusList.add(DiscrepancyStatus.OPEN);
				List<DiscrepancyDetailDTO> enrlReconDiscrepancyOpenList = iEnrlReconDiscrepancyRepository.getDiscrepancyDetailByEnrollmentIDAndStatus(enrollmentId.longValue(), discrepancyStatusList);
				discrepancyEnrollmentDTO.setDiscrepancyNotesList(generateDiscepancyNotes(new HashSet<DiscrepancyDetailDTO>(enrlReconDiscrepancyOpenList)));

				if(hixEnrollmentDataDto != null && issuerEnrollmentDataDto != null){
					discrepancyEnrollmentDTO.setEnrollmentId(enrollmentId);
					discrepancyEnrollmentDTO.setEncEnrollmentID(ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollmentId.toString()));
					discrepancyEnrollmentDTO.setIssuerName(enrollmentExternalRestUtil.getIssuerNameByHiosIssuerId(hixEnrollmentDataDto.getHiosId()));
					discrepancyEnrollmentDTO.setIssuerEnrollmentIdValue(issuerEnrollmentDataDto.getExchangeAssignedPolicyid() != null ? Integer.parseInt(issuerEnrollmentDataDto.getExchangeAssignedPolicyid()) : null);
					discrepancyEnrollmentDTO.setAgentName(generateNodeDTOByErrorCodeORFieldName(hixEnrollmentDataDto.getAgentBrokerName(),
							issuerEnrollmentDataDto.getAgentBrokerName(), enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.AGENT_NAME_ERRORCODE, null));
					
					discrepancyEnrollmentDTO.setBrokerFEDTaxPayerId(generateNodeDTOByErrorCodeORFieldName(
							hixEnrollmentDataDto.getAgentBrokerNpn(), issuerEnrollmentDataDto.getAgentBrokerNpn(),
							enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.AGENT_TAX_PAYER_ERRORCODE, null));
				
					discrepancyEnrollmentDTO.setMemberCount(
							generateMemberNodeDTOByErrorCode(hixEnrollmentDataDto.getMembers() != null ? hixEnrollmentDataDto.getMembers().size(): null,
									issuerEnrollmentDataDto.getMembers() != null ? issuerEnrollmentDataDto.getMembers().size(): null,
									enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBERCOUNT_ERROR_CODE, null)
							);
					
					discrepancyEnrollmentDTO.setCmsPlanId(
							generateNodeDTO(hixEnrollmentDataDto.getQhpId(), issuerEnrollmentDataDto.getQhpId(), false));
					discrepancyEnrollmentDTO.setEnrollmentId(hixEnrollmentDataDto.getExchangeAssignedPolicyid() != null ?
							Integer.parseInt(hixEnrollmentDataDto.getExchangeAssignedPolicyid()) : null);
					
					discrepancyEnrollmentDTO.setIsEffectuated(generateNodeDTOByErrorCodeORFieldName(
							hixEnrollmentDataDto.getEnrollmentconfirmationDate() != null ?	EnrollmentConstants.Y : EnrollmentConstants.N,
							issuerEnrollmentDataDto.getInitialPremiumPaidStatus() != null ? issuerEnrollmentDataDto.getInitialPremiumPaidStatus() : EnrollmentConstants.N,
								enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.EFFECTUATION_STATUS_ERRORCODE, null));
					if(hixEnrollmentDataDto.getMembers() != null){
						List<ReconMemberDTO> hixReconMemberDTOList = 
								sortHixMemberByTypeAndDate(hixEnrollmentDataDto.getMembers()); 
						discrepancyEnrollmentDTO.setDiscrepancyMemberDTOList(prepareDiscrepancyMember(hixReconMemberDTOList, (issuerEnrollmentDataDto.getMembers() != null ? issuerEnrollmentDataDto.getMembers() : null), discrepancyEnrollmentDTO, enrlReconDiscrepancyOpenList));
					}
					if(hixEnrollmentDataDto.getMonthlyPremiums() != null){
						discrepancyEnrollmentDTO.setDiscrepancyPremiumDTOList(prepareDiscrepancyPremium(hixEnrollmentDataDto.getMonthlyPremiums(), (issuerEnrollmentDataDto.getMonthlyPremiums() != null ? issuerEnrollmentDataDto.getMonthlyPremiums() : null), enrlReconDiscrepancyOpenList));
					}
				}
			}
		}
		return discrepancyEnrollmentDTO;
	}
	
	private List<DiscrepancyMemberDTO> prepareDiscrepancyMember(final List<ReconMemberDTO> hixMemberDTOList, final List<ReconMemberDTO> issuerMemberDTOList,
			DiscrepancyEnrollmentDTO discrepancyEnrollmentDTO, final List<DiscrepancyDetailDTO> enrlReconDiscrepancyOpenList){
		List<DiscrepancyMemberDTO> discrepancyMemberDTOList = new ArrayList<DiscrepancyMemberDTO>();
		if(hixMemberDTOList != null  && !hixMemberDTOList.isEmpty()){
			Iterator<ReconMemberDTO> hixReconMemberDTOIterator = hixMemberDTOList.iterator();
			while(hixReconMemberDTOIterator.hasNext()){
				ReconMemberDTO hixReconMemberDTO = hixReconMemberDTOIterator.next();
				String exchgAssignedMemberId = hixReconMemberDTO.getExchangeAssignedMemberId();
				ReconMemberDTO issuerReconMemberDTO = (issuerMemberDTOList != null && !issuerMemberDTOList.isEmpty()) ? issuerMemberDTOList.stream()
						.filter(issuerReconMemberDTOObj -> 
						issuerReconMemberDTOObj.getExchangeAssignedMemberId().equals(exchgAssignedMemberId)).findAny().orElse(null): null;
				DiscrepancyMemberDTO discrepancyMemberDTO = new DiscrepancyMemberDTO();
				
				issuerReconMemberDTO = issuerReconMemberDTO != null ? issuerReconMemberDTO : new ReconMemberDTO();
				
				if(hixReconMemberDTO.getSubscriberIndicator() != null && hixReconMemberDTO.getSubscriberIndicator().equals(EnrollmentConstants.Y)){
					discrepancyEnrollmentDTO.setBenefitEffectiveStartDate(generateMemberNodeDTOByErrorCode(
							formatReconDate(hixReconMemberDTO.getMemberBeginDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY),
							formatReconDate(issuerReconMemberDTO.getMemberBeginDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), 
							enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_BENEFIT_BEGIN_DATE_ERRORCODE, exchgAssignedMemberId));
					discrepancyEnrollmentDTO.setBenefitEffectiveEndDate(generateMemberNodeDTOByErrorCode(
							formatReconDate(hixReconMemberDTO.getMemberEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), 
							formatReconDate(issuerReconMemberDTO.getMemberEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), 
							enrlReconDiscrepancyOpenList,  EnrollmentConstants.ReconErrorCode.MEMBER_BENEFIT_END_DATE_ERRORCODE, exchgAssignedMemberId));
					
					discrepancyEnrollmentDTO.setHomeAddress(generateMemberNodeDTOByErrorCode(
							EnrollmentUtils.prepareAddressString(hixReconMemberDTO.getHomeAddress1(),
									hixReconMemberDTO.getHomeAddress2(), hixReconMemberDTO.getHomeCity(),
									hixReconMemberDTO.getHomeState(), hixReconMemberDTO.getHomeZip(), hixReconMemberDTO.getHomeCountyCode(), true),
							EnrollmentUtils.prepareAddressString(issuerReconMemberDTO.getHomeAddress1(),
									issuerReconMemberDTO.getHomeAddress2(), issuerReconMemberDTO.getHomeCity(),
									issuerReconMemberDTO.getHomeState(), issuerReconMemberDTO.getHomeZip(), issuerReconMemberDTO.getHomeCountyCode(), true),
							enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_RESIDENTIAL_ADDRESS_ERRORCODE, exchgAssignedMemberId));
					discrepancyEnrollmentDTO.setMailingAddress(generateMemberNodeDTOByErrorCode(
							EnrollmentUtils.prepareAddressString(hixReconMemberDTO.getMailingAddress1(),
									hixReconMemberDTO.getMailingAddress2(), hixReconMemberDTO.getMailingCity(),
									hixReconMemberDTO.getMailingState(), hixReconMemberDTO.getMailingZip(), null, false),
							EnrollmentUtils.prepareAddressString(issuerReconMemberDTO.getMailingAddress1(),
									issuerReconMemberDTO.getMailingAddress2(), issuerReconMemberDTO.getMailingCity(),
									issuerReconMemberDTO.getMailingState(), issuerReconMemberDTO.getMailingZip(), null, false),
							enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_MAILING_ADDRESS_ERRORCODE, exchgAssignedMemberId));
					discrepancyEnrollmentDTO.setPhoneNumber(generateNodeDTOByErrorCodeORFieldName(hixReconMemberDTO.getTelephoneNumber(),
							issuerReconMemberDTO.getTelephoneNumber(), enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.PHONE_NUMBER_ERROR_CODE, null));
				}//Subscriber Loop Ends here
				discrepancyMemberDTO.setExchgIndivIdentifier(generateNodeDTO(hixReconMemberDTO.getExchangeAssignedMemberId(), issuerReconMemberDTO.getExchangeAssignedMemberId(), false));
				discrepancyMemberDTO.setTobaccoUsage(
						generateMemberNodeDTOByErrorCode(
								hixReconMemberDTO.getTobacco() != null &&
										hixReconMemberDTO.getTobacco().equals(String.valueOf(EnrollmentConstants.ONE)) ? EnrollmentConstants.Y: EnrollmentConstants.N,
								issuerReconMemberDTO.getTobacco() != null &&
										issuerReconMemberDTO.getTobacco().equals(String.valueOf(EnrollmentConstants.ONE)) ? EnrollmentConstants.Y: EnrollmentConstants.N,
								enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_TOBACCO_USAGE_ERRORCODE, exchgAssignedMemberId));
				
				discrepancyMemberDTO.setGender(generateMemberNodeDTOByErrorCode(hixReconMemberDTO.getGender(), 
						issuerReconMemberDTO.getGender(), enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_GENDER_ERROR_CODE, exchgAssignedMemberId));
				discrepancyMemberDTO.setMemberDateOfBirth(generateMemberNodeDTOByErrorCode(
						formatReconDate(hixReconMemberDTO.getBirthDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY),
						formatReconDate(issuerReconMemberDTO.getBirthDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), 
								enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_BIRTHDATE_ERROR_CODE, exchgAssignedMemberId));
		
				
				discrepancyMemberDTO.setMemberFullName(generateMemberNodeDTOByErrorCode(
						EnrollmentUtils.getFullName(hixReconMemberDTO.getFirstName(), hixReconMemberDTO.getMiddleName(), hixReconMemberDTO.getLastName()),
						EnrollmentUtils.getFullName(issuerReconMemberDTO.getFirstName(), issuerReconMemberDTO.getMiddleName(), issuerReconMemberDTO.getLastName()),
						enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_NAME_ERRORCODE, exchgAssignedMemberId));
				discrepancyMemberDTO.setMemberEffectiveStartDate(generateMemberNodeDTOByErrorCode(
						formatReconDate(hixReconMemberDTO.getMemberBeginDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY),
						formatReconDate(issuerReconMemberDTO.getMemberBeginDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), 
						enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_BENEFIT_BEGIN_DATE_ERRORCODE, exchgAssignedMemberId));
				discrepancyMemberDTO.setMemberEffectiveEndDate(generateMemberNodeDTOByErrorCode(
						formatReconDate(hixReconMemberDTO.getMemberEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), 
						formatReconDate(issuerReconMemberDTO.getMemberEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), 
						enrlReconDiscrepancyOpenList,  EnrollmentConstants.ReconErrorCode.MEMBER_BENEFIT_END_DATE_ERRORCODE, exchgAssignedMemberId));
				
				discrepancyMemberDTO.setRelationshipWithSubscriber(generateMemberNodeDTOByErrorCode(
								hixReconMemberDTO.getIndividualRelationshipCode() != null ? (lookupService.getLookupValueLabel(EnrollmentConstants.RELATIONSHIP, hixReconMemberDTO.getIndividualRelationshipCode()))  : hixReconMemberDTO.getIndividualRelationshipCode(),
								issuerReconMemberDTO.getIndividualRelationshipCode() != null ? (lookupService.getLookupValueLabel(EnrollmentConstants.RELATIONSHIP, issuerReconMemberDTO.getIndividualRelationshipCode()))  : issuerReconMemberDTO.getIndividualRelationshipCode(),
								enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_RELATIONSHIP_ERRORCODE, exchgAssignedMemberId));
				discrepancyMemberDTO.setSSN(generateMemberNodeDTOByErrorCode(
								maskSSN(hixReconMemberDTO.getSsn()), 
								maskSSN(issuerReconMemberDTO.getSsn()), enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.MEMBER_SSN_ERRORCODE, exchgAssignedMemberId));
				
				
				discrepancyMemberDTOList.add(discrepancyMemberDTO);
			}
		}
		return discrepancyMemberDTOList;
	} 
	
	private List<DiscrepancyPremiumDTO> prepareDiscrepancyPremium(final List<ReconMonthlyPremiumDTO> hixMonthlyPremiumList,
			final List<ReconMonthlyPremiumDTO> issuerMonthlyPremiumList, final List<DiscrepancyDetailDTO> enrlReconDiscrepancyOpenList){
		List<DiscrepancyPremiumDTO> discrepancyPremiumDTOList = new ArrayList<DiscrepancyPremiumDTO>();
		if(hixMonthlyPremiumList != null && !hixMonthlyPremiumList.isEmpty()){
			Map<Integer, String> monthDetailsMap = EnrollmentUtils.getMonthDetails();
			for (int i = EnrollmentConstants.ONE; i <= EnrollmentConstants.TWELVE; i++) {
				Integer monthNumber = i;
				DiscrepancyPremiumDTO discrepancyPremiumDTO = new DiscrepancyPremiumDTO();
				ReconMonthlyPremiumDTO monthlyPremiumDTO = hixMonthlyPremiumList.stream()
						.filter(monthlyPremiumObj -> monthlyPremiumObj.getCoverageMonth().equals(monthNumber)).findAny()
						.orElse(null);
				if(monthlyPremiumDTO != null)
				{
					ReconMonthlyPremiumDTO issuerReconMonthlyPremiumDTO = null;
					if(issuerMonthlyPremiumList != null){	
						issuerReconMonthlyPremiumDTO = issuerMonthlyPremiumList.stream()
							.filter(issuerReconMonthlyPremiumObj -> 
							issuerReconMonthlyPremiumObj.getCoverageMonth().equals(monthlyPremiumDTO.getCoverageMonth())
							&& 
							issuerReconMonthlyPremiumObj.getCoverageYear().equals(monthlyPremiumDTO.getCoverageYear())
							).findAny().orElse(null);
					}
					issuerReconMonthlyPremiumDTO = issuerReconMonthlyPremiumDTO == null ? new ReconMonthlyPremiumDTO(): issuerReconMonthlyPremiumDTO;
					discrepancyPremiumDTO.setMonthNumber(monthNumber);
					discrepancyPremiumDTO.setMonthName(monthDetailsMap.get(monthlyPremiumDTO.getCoverageMonth()));
					discrepancyPremiumDTO.setYear(monthlyPremiumDTO.getCoverageYear());
					discrepancyPremiumDTO.setAptcAmount(generatePremiumNodeDTOByFieldName(monthlyPremiumDTO.getAptcAmt(),
							issuerReconMonthlyPremiumDTO.getAptcAmt(), enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.APTC_ERROR_FIELDNAME, monthNumber));
					
					discrepancyPremiumDTO.setCsrAmount(generatePremiumNodeDTOByFieldName(monthlyPremiumDTO.getCsrAmt(),
							issuerReconMonthlyPremiumDTO.getCsrAmt(), enrlReconDiscrepancyOpenList,  EnrollmentConstants.ReconErrorCode.CSR_AMOUNT_ERROR_FIELDNAME, monthNumber));
					
					discrepancyPremiumDTO.setGrossPremiumAmount(generatePremiumNodeDTOByFieldName(monthlyPremiumDTO.getGrossPremium(),
									issuerReconMonthlyPremiumDTO.getGrossPremium(), enrlReconDiscrepancyOpenList, EnrollmentConstants.ReconErrorCode.GROSS_PREMIUM_ERROR_FIELDNAME, monthNumber));
					
					discrepancyPremiumDTO.setRatingArea(generatePremiumNodeDTOByFieldName(monthlyPremiumDTO.getRatingArea(),
							issuerReconMonthlyPremiumDTO.getRatingArea(), enrlReconDiscrepancyOpenList,  EnrollmentConstants.ReconErrorCode.RATINGAREA_ERROR_FIELDNAME, monthNumber));
					
					discrepancyPremiumDTO.setNetPremiumAmount(
							generatePremiumNodeDTOByFieldName(
										getNetPremiumAmount(monthlyPremiumDTO.getGrossPremium(), monthlyPremiumDTO.getAptcAmt()),
										getNetPremiumAmount(issuerReconMonthlyPremiumDTO.getGrossPremium(), issuerReconMonthlyPremiumDTO.getAptcAmt()), null, null, monthNumber)
										);
					discrepancyPremiumDTO.setIsFinancial(
							generatePremiumNodeDTOByFieldName((monthlyPremiumDTO.getAptcAmt() != null ? EnrollmentConstants.Y : EnrollmentConstants.N),
									issuerReconMonthlyPremiumDTO.getAptcAmt() != null ? 
											EnrollmentConstants.Y : EnrollmentConstants.N, null, null, monthNumber));	
				}
				else
				{
					discrepancyPremiumDTO.setMonthNumber(i);
					discrepancyPremiumDTO.setMonthName(monthDetailsMap.get(i));
				}
				discrepancyPremiumDTOList.add(discrepancyPremiumDTO);
			}
		}
		return discrepancyPremiumDTOList;
	}
	
	private Map<String, Object> generateNodeDTO(Object hixValue, Object issuerValue, boolean hasDiscrepancy){
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		if(hixValue != null){
			objectMap.put("hixValue", hixValue);
		}
		if(issuerValue != null){
			objectMap.put("issuerValue", issuerValue);
		}
		objectMap.put("hasDiscrepancy", hasDiscrepancy);
		
		return objectMap;
	}
	private Map<String, Object> generateMemberNodeDTOByErrorCode(Object hixValue, Object issuerValue, 
			List<DiscrepancyDetailDTO> enrlReconDiscrepancyOpenList, String[] enrlReconLkpCode, String memberID){
		boolean hasDiscrepancy  = false;
		if(enrlReconDiscrepancyOpenList != null && !enrlReconDiscrepancyOpenList.isEmpty()){
			if(enrlReconLkpCode != null && enrlReconLkpCode.length != 0){
				for(String errorCode : enrlReconLkpCode){
					DiscrepancyDetailDTO  discrepancyDetailDTO= null;
					if(StringUtils.isNotBlank(memberID)){
						discrepancyDetailDTO = enrlReconDiscrepancyOpenList.stream().
						filter(enrlReconObj -> enrlReconObj.getDiscrepancyCode().equals(errorCode) &&
						 enrlReconObj.getMemberId() != null && StringUtils.equals(enrlReconObj.getMemberId().toString(), memberID)
						).findAny().orElse(null);
					}
					else{
						discrepancyDetailDTO =  enrlReconDiscrepancyOpenList.stream().
								filter(enrlReconObj -> enrlReconObj.getDiscrepancyCode().equals(errorCode)
										).findAny().orElse(null);
					}
							
					if(discrepancyDetailDTO != null){
						hasDiscrepancy = true;
						break;
					}
				}
			}
		}
		return generateNodeDTO(hixValue, issuerValue, hasDiscrepancy);
	}
	private Map<String, Object> generatePremiumNodeDTOByFieldName(Object hixValue, Object issuerValue, 
			List<DiscrepancyDetailDTO> enrlReconDiscrepancyOpenList, String[] fieldNameArray, Integer monthNumber){
		boolean hasDiscrepancy  = false;
		if(fieldNameArray != null && fieldNameArray.length != EnrollmentConstants.ZERO &&
				enrlReconDiscrepancyOpenList != null && !enrlReconDiscrepancyOpenList.isEmpty()){
			for(String fieldName : fieldNameArray){
				DiscrepancyDetailDTO discrepancyDetailDTO =  enrlReconDiscrepancyOpenList.stream().filter(
						discrepancyDetailDTOObj -> discrepancyDetailDTOObj.getFieldName() != null && discrepancyDetailDTOObj.getFieldName().equals(fieldName+monthNumber)
						).findAny().orElse(null);
				if(discrepancyDetailDTO != null){
					hasDiscrepancy = true;
					break;
				}
			}
		}
		return generateNodeDTO(hixValue, issuerValue, hasDiscrepancy);
	}
	private Map<String, Object> generateNodeDTOByErrorCodeORFieldName(Object hixValue, Object issuerValue, 
			List<DiscrepancyDetailDTO> enrlReconDiscrepancyOpenList, String errorCode, String fieldName){
		boolean hasDiscrepancy  = false;
		if(enrlReconDiscrepancyOpenList != null && !enrlReconDiscrepancyOpenList.isEmpty()){
			DiscrepancyDetailDTO discrepancyDetailDTO =  enrlReconDiscrepancyOpenList.stream().filter(
					discrepancyDetailDTOObj -> discrepancyDetailDTOObj.getDiscrepancyCode().equals(errorCode) 
					|| (StringUtils.isNotBlank(fieldName)  && fieldName.equals(discrepancyDetailDTOObj.getFieldName()))
					).findAny().orElse(null);
			if(discrepancyDetailDTO != null){
				hasDiscrepancy = true;
			}
		}
		return generateNodeDTO(hixValue, issuerValue, hasDiscrepancy);
	}
	
	private String formatReconDate(String requestDateString, String existingDateFormat, String newDateFormat){
		String newDateString = null;
		try{
		if(StringUtils.isNotBlank(requestDateString) && StringUtils.isNotBlank(existingDateFormat)){
			newDateString = DateUtil.dateToString(DateUtil.StringToDate(requestDateString, existingDateFormat), newDateFormat);
		}
		}
		catch(Exception pe){
			//Suppressing exception
			return requestDateString;
		}
		return newDateString;
	}
	
	private String maskSSN(String requestString){
		if(StringUtils.isNotBlank(requestString) && requestString.length() >=EnrollmentConstants.FOUR){
			return ("XXX-XX-" + requestString.substring(requestString.length() - EnrollmentConstants.FOUR, requestString.length()));
		}
		else{
			return requestString;
		}
	}
	
	private static BigDecimal getNetPremiumAmount(Float grossPremium, Float aptcAmount){	
		grossPremium = grossPremium != null ? grossPremium : 0.0f;
		aptcAmount = aptcAmount != null ? aptcAmount : 0.0f;
		
		return new BigDecimal(grossPremium - aptcAmount).setScale(EnrollmentConstants.TWO, BigDecimal.ROUND_HALF_UP);
	}
	
	private List<ReconMemberDTO> sortHixMemberByTypeAndDate(List<ReconMemberDTO> hixMemberDTOList){
		List<ReconMemberDTO> reconMemberDTOList = new ArrayList<ReconMemberDTO>();
		if(hixMemberDTOList != null && !hixMemberDTOList.isEmpty()){
			//Fetch Subscriber First
			List<ReconMemberDTO> subscriberReconMemberDTOList = hixMemberDTOList.stream()
					.filter(hixReconMemberDTOObj -> 
					hixReconMemberDTOObj.getIndividualRelationshipCode().equals(EnrollmentConstants.RELATIONSHIP_SELF_CODE)).collect(Collectors.toList());
			if(subscriberReconMemberDTOList != null && !subscriberReconMemberDTOList.isEmpty()){
				reconMemberDTOList.addAll(subscriberReconMemberDTOList);
			}

			//Fetch Spouse next
			List<ReconMemberDTO> spouseReconMemberDTOList = hixMemberDTOList.stream()
					.filter(hixReconMemberDTOObj -> 
					hixReconMemberDTOObj.getIndividualRelationshipCode().equals(EnrollmentConstants.RELATIONSHIP_SPOUSE_CODE)).collect(Collectors.toList());
			if(spouseReconMemberDTOList != null && !spouseReconMemberDTOList.isEmpty()){
				reconMemberDTOList.addAll(spouseReconMemberDTOList);
			}
			//Then sort by coverage Date
			List<ReconMemberDTO> otherDependentList =  hixMemberDTOList.stream()
					.filter(hixReconMemberDTOObj -> 
					(
					!EnrollmentConstants.RELATIONSHIP_SELF_CODE.equals(hixReconMemberDTOObj.getIndividualRelationshipCode())
					&& !EnrollmentConstants.RELATIONSHIP_SPOUSE_CODE.equals(hixReconMemberDTOObj.getIndividualRelationshipCode())
					)
					).collect(Collectors.toList());  
			
			if(otherDependentList != null && !otherDependentList.isEmpty()){
				Collections.sort(otherDependentList, (obj1, obj2) -> obj1.getIndividualRelationshipCode().compareTo(obj2.getIndividualRelationshipCode()));
				reconMemberDTOList.addAll(otherDependentList);
			}	
		}
		return reconMemberDTOList;
	}
	
	private List<String> generateDiscepancyNotes(Set<DiscrepancyDetailDTO> enrlReconDiscrepancyOpenList){
		Set<String> discrepancyNotesSet = null;
		if(enrlReconDiscrepancyOpenList != null && !enrlReconDiscrepancyOpenList.isEmpty()){
			List<DiscrepancyDetailDTO> missingDiscrepancyDetailDTOList = enrlReconDiscrepancyOpenList.stream().filter(disObj -> 
			(disObj.getDiscrepancyCode().equals(EnrollmentConstants.ReconErrorCode.ENROLLMENT_MISSING_IN_HIX)||
					disObj.getDiscrepancyCode().equals(EnrollmentConstants.ReconErrorCode.ENROLLMENT_MISSING_IN_FILE)||
					disObj.getDiscrepancyCode().equals(EnrollmentConstants.ReconErrorCode.MEMBER_MISSING_IN_HIX)||
					disObj.getDiscrepancyCode().equals(EnrollmentConstants.ReconErrorCode.MEMBER_MISSING_IN_FILE)
			)
					).collect(Collectors.toList());
			if(missingDiscrepancyDetailDTOList != null && !missingDiscrepancyDetailDTOList.isEmpty()){
				discrepancyNotesSet = new HashSet<String>();
				for(DiscrepancyDetailDTO missingDiscrepancyDetailDTO: missingDiscrepancyDetailDTOList){
					StringBuilder stringBuilder = new StringBuilder(100);
					if(missingDiscrepancyDetailDTO.getDiscrepancyCode().equals(EnrollmentConstants.ReconErrorCode.ENROLLMENT_MISSING_IN_HIX)){
						stringBuilder.append("Issuer Policy Id: ");
						stringBuilder.append(missingDiscrepancyDetailDTO.getIssuerEnrollmentId() != null ? missingDiscrepancyDetailDTO.getIssuerEnrollmentId() :
							missingDiscrepancyDetailDTO.getHixEnrollmentId());
						stringBuilder.append(" is missing in HIX.");
					}
					else if(missingDiscrepancyDetailDTO.getDiscrepancyCode().equals(EnrollmentConstants.ReconErrorCode.ENROLLMENT_MISSING_IN_FILE)){
						stringBuilder.append("Missing Policy Id: ");
						stringBuilder.append(missingDiscrepancyDetailDTO.getHixEnrollmentId() != null ? missingDiscrepancyDetailDTO.getHixEnrollmentId() : EnrollmentConstants.ZERO);
						stringBuilder.append(" in last file, FileName: ");
						stringBuilder.append(missingDiscrepancyDetailDTO.getFileName() != null ? missingDiscrepancyDetailDTO.getFileName() : "Unknown");
					}
					else if(missingDiscrepancyDetailDTO.getDiscrepancyCode().equals(EnrollmentConstants.ReconErrorCode.MEMBER_MISSING_IN_HIX)){
						stringBuilder.append("Member ID: ");
						stringBuilder.append(missingDiscrepancyDetailDTO.getMemberId());
						stringBuilder.append(",");
						stringBuilder.append(" Name: ");
						stringBuilder.append(missingDiscrepancyDetailDTO.getIssuerValue() != null ? missingDiscrepancyDetailDTO.getIssuerValue().replace("|", StringUtils.SPACE): null);
						stringBuilder.append(" is missing in hix.");
					}
					else if(missingDiscrepancyDetailDTO.getDiscrepancyCode().equals(EnrollmentConstants.ReconErrorCode.MEMBER_MISSING_IN_FILE)){
						stringBuilder.append("Member ID: ");
						stringBuilder.append(missingDiscrepancyDetailDTO.getMemberId());
						stringBuilder.append(",");
						stringBuilder.append(" Name: ");
						stringBuilder.append(missingDiscrepancyDetailDTO.getHixValue() != null ? missingDiscrepancyDetailDTO.getHixValue().replace("|", StringUtils.SPACE): null);
						stringBuilder.append(" is missing in file.");
					}
					discrepancyNotesSet.add(stringBuilder.toString());
				}
			}
		}
		return discrepancyNotesSet != null ? new ArrayList<String>(discrepancyNotesSet) : null;
	}
}
