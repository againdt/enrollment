/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;



/**
 * @author rajaramesh_g
 * @since 24/12/2013
 */
public interface EnrollmentUserPrivateService {
	AccountUser getLoggedInUser();
	Role getRoleForUser(AccountUser user);
}
