/**
 * 
 */
package com.getinsured.hix.enrollment.repository;


import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.EnrollmentOutPlr;
@Repository
@Transactional
public interface IPLROutboundRepository extends JpaRepository<EnrollmentOutPlr, Integer>{
	@Query("FROM EnrollmentOutPlr as plr where plr.batchId =:batchId")
	List<EnrollmentOutPlr> findplrOutboundTransmissionByBatchId(@Param("batchId") String batchId);

	@Query("FROM EnrollmentOutPlr as plr where plr.documentFileName =:documentFileName")
	EnrollmentOutPlr findplrOutboundTransmissionByFileName(@Param("documentFileName") String documentFileName);
	
	@Query("FROM EnrollmentOutPlr as en WHERE en.documentFileName = :documentFileName AND en.batchId = :batchId ")
	EnrollmentOutPlr findByDocumentFileNameAndBatchId(@Param("documentFileName")String fileName , @Param("batchId")String batchId);
	
	@Query(" SELECT en.householdCaseIds " +
		   " FROM EnrollmentOutPlr as en " +
		   " WHERE en.documentFileName like '%' || :documentFileName || '%' " +
		   " AND en.batchId = :batchId ")
	String getHouseholdCaseIds(@Param("batchId")String batchId, @Param("documentFileName")String fileName);
	
	@Query(" SELECT en.householdCaseIds " +
			   " FROM EnrollmentOutPlr as en " +
			   " WHERE en.batchId = :batchId ")
	List<String> getHouseholdIdsByBatchId(@Param("batchId")String batchId);
	
	@Query(" SELECT en.year, en.month " + 
			" FROM EnrollmentOutPlr as en "
			+ " WHERE en.id = (SELECT MAX(id) from EnrollmentOutPlr eop where eop.batchId = :batchId AND eop.reportType = :reportType)")
	List<Object[]> getYearAndMonthByBatchIdAndType(@Param("batchId") String batchId, @Param("reportType") String reportType);
	
	@Query("SELECT en.batchCategoryCode FROM EnrollmentOutPlr as en WHERE en.batchId = :oldBatchId ")
	List<String> getBatchCategoryCodeFromBatchId(@Param("oldBatchId") String oldBatchId, Pageable pageable);
}