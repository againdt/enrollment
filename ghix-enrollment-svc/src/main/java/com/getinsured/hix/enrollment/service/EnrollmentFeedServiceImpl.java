package com.getinsured.hix.enrollment.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.carrierfeed.util.BOBCarrierFeedQueryUtil;
import com.getinsured.hix.model.enrollment.Enrollment;

@Service("enrollmentFeedService")
@Transactional
public class EnrollmentFeedServiceImpl implements EnrollmentFeedService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentFeedServiceImpl.class);

	@PersistenceUnit
	private EntityManagerFactory emf;
	
	@Autowired BOBCarrierFeedQueryUtil feedQueryUtil;
	
	@Override
	public List<Enrollment> findEnrollmentByCarrierAppId(String carrierAppId, String issuerName,Long tenantId) {
		String enrollemntQuery =  feedQueryUtil.getEnrollmentByCarrierAppIdQuery(carrierAppId, issuerName);

		EntityManager entityManager = null;

		try
		{
			entityManager = emf.createEntityManager();

			Query hqlQuery = entityManager.createQuery(enrollemntQuery);
			hqlQuery.setParameter("tenantId", tenantId);

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("FindEnrollmentByCarrierAppId HQL QUERY: {}", hqlQuery);
			}

			List<Enrollment> resultList = hqlQuery.getResultList();
			return resultList;
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					if(LOGGER.isErrorEnabled())
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		}
	}
	
	@Override
	public List<Enrollment> findEnrollmentByCarrierUID(String carrierAppUID, String issuerName,Long tenantId) {
		String enrollemntQuery = feedQueryUtil.getEnrollmentByCarrierUIDQuery(carrierAppUID, issuerName);

		EntityManager entityManager = null;

		try
		{
			entityManager = emf.createEntityManager();

			Query hqlQuery = entityManager.createQuery(enrollemntQuery);
			hqlQuery.setParameter("tenantId", tenantId);

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("FindEnrollmentByCarrierUID HQL QUERY: {}", hqlQuery);
			}

			List<Enrollment> resultList = hqlQuery.getResultList();

			return resultList;
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					if(LOGGER.isErrorEnabled())
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		}
	}
	
	@Override
	public List<Enrollment> findEnrollmentByissuerPolicyId(String policyId, String issuerName,Long tenantId) {
		String enrollmentQuery = feedQueryUtil.getEnrollmentByissuerPolicyIdQuery(policyId, issuerName);

		EntityManager entityManager = null;

		try
		{
			entityManager = emf.createEntityManager();

			Query hqlQuery = entityManager.createQuery(enrollmentQuery);
			hqlQuery.setParameter("tenantId", tenantId);

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("FindEnrollmentByissuerPolicyId HQL QUERY: {}", hqlQuery);
			}

			List<Enrollment> resultList = hqlQuery.getResultList();

			return resultList;
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					if(LOGGER.isErrorEnabled())
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		}
	}
	
	@Override
	public List<Enrollment> findEnrollmentByEnrolleeAndEffectiveDateQuery(String issuerName, String firstName, String lastName, String reqDateRangeFrom, String reqDateRangeTo,Long tenantId){
		String enrollmentQuery = feedQueryUtil.getEnrollmentByEnrolleeAndEffectiveDateQuery(issuerName, firstName, lastName, reqDateRangeFrom, reqDateRangeTo);

		EntityManager entityManager = null;

		try
		{
			entityManager = emf.createEntityManager();

			Query hqlQuery = entityManager.createQuery(enrollmentQuery);
			hqlQuery.setParameter("tenantId", tenantId);

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("findEnrollmentByEnrolleeAndEffectiveDateQuery HQL QUERY: {}", hqlQuery);
			}

			List<Enrollment> resultList = hqlQuery.getResultList();

			return resultList;
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					if(LOGGER.isErrorEnabled())
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		}
	}

	@Override
	public List<Enrollment> getEnrollmentsForFinalExceptionCheck(String issuerName, int feedSummaryId, String missingEnrollmentException,Long tenantId) {
		String enrollmentQuery = feedQueryUtil.getEnrollmentsForFinalExceptionCheck(issuerName,feedSummaryId,missingEnrollmentException);

		EntityManager entityManager = null;

		try
		{
			entityManager = emf.createEntityManager();

			Query hqlQuery = entityManager.createQuery(enrollmentQuery);
			hqlQuery.setParameter("tenantId", tenantId);
			hqlQuery.setParameter("fpeTenantId", tenantId);

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("getEnrollmentsForFinalExceptionCheck HQL QUERY: {}", hqlQuery);
			}

			List<Enrollment> resultList = hqlQuery.getResultList();

			return resultList;
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					if(LOGGER.isErrorEnabled())
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		}
	}
}


