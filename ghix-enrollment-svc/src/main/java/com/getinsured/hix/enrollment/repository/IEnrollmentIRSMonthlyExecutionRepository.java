package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentIRSMonthlyExecution;

public interface IEnrollmentIRSMonthlyExecutionRepository extends JpaRepository<EnrollmentIRSMonthlyExecution, Integer> {

	@Query("FROM EnrollmentIRSMonthlyExecution as eie WHERE eie.enrollment1095 IN (:enrollment1095Ids) ")
	List<EnrollmentIRSMonthlyExecution> getByEnrollment1095Id(@Param("enrollment1095Ids") List<Integer> enrollment1095Ids);

	@Query("FROM EnrollmentIRSMonthlyExecution as eie WHERE eie.householdCaseId IN (:householdCaseIds) AND eie.currentBatchId = :batchId")
	List<EnrollmentIRSMonthlyExecution> getEnrollmentExecutionsByHouseholdAndBatchId(@Param("householdCaseIds") List<Integer> householdCaseIds, @Param("batchId") String batchId);
	
	@Query("SELECT eie.originalBatchId FROM EnrollmentIRSMonthlyExecution as eie WHERE eie.currentBatchId = :batchId")
	List<String> getOriginalBatchId(@Param("batchId") String batchId, Pageable pageable);

	@Query("SELECT DISTINCT eie.householdCaseId FROM EnrollmentIRSMonthlyExecution as eie WHERE eie.currentBatchId = :batchId AND eie.irsInboundAction = :irsInboundAction ")
	List<Integer> getHouseholdByBatchIdAndAction(@Param("batchId") String batchId, @Param("irsInboundAction") String irsInboundAction);
	
	@Query("FROM EnrollmentIRSMonthlyExecution as eie WHERE eie.enrollment1095 IN (:enrollment1095Ids) AND eie.currentBatchId = :batchId ")
	List<EnrollmentIRSMonthlyExecution> getByEnrollment1095IdAndBatchId(@Param("enrollment1095Ids") List<Integer> enrollment1095Ids, @Param("batchId")String batchId);
}
