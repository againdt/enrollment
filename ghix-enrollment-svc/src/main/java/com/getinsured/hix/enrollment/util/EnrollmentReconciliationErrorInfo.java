package com.getinsured.hix.enrollment.util;

/**
 * 
 * @author Sharma_K
 * Information about Enrollment reconciliation error
 */
public class EnrollmentReconciliationErrorInfo {
	private String errorRecord;
	private String failureReason;
	
	
	public String getErrorRecord() {
		return errorRecord;
	}
	public void setErrorRecord(String errorRecord) {
		this.errorRecord = errorRecord;
	}
	public String getFailureReason() {
		return failureReason;
	}
	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}
}
