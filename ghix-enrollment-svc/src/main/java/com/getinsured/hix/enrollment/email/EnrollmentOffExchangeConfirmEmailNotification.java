package com.getinsured.hix.enrollment.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.enrollment.EnrollmentOffExchangeEmailDTO;
import com.getinsured.hix.platform.notification.NotificationAgent;
import com.getinsured.hix.platform.util.GhixEndPoints;
@Component
public class EnrollmentOffExchangeConfirmEmailNotification extends NotificationAgent {

	private Map<String, String> singleData;
	private EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO;
	
	private static final String PLAN_TYPE = "planType";
	private static final String PLAN_NAME = "planName";
	private static final String OFFICE_VISIT_AMT = "officeVisit";
	private static final String GENERIC_MEDICATION_AMT = "genericMedication";
	private static final String DEDUCTIBLE = "deductible";
	private static final String MAX_OOP = "maxOop";
	private static final String NET_PREMIUM_AMT = "netPremiumAmount";
	private static final String LINK = "link";
	private static final String LOGIN_POSTFIX = "account/user/login";
	private static final String INSURANCE_COMPANY = "insuranceCompany";
	private static final String COMPANY_LOGO = "companyLogo";
	
	@Override
	public Map<String, String> getSingleData() {
		Map<String,String> bean = new HashMap<String, String>();

		bean.put("customerName", enrollmentOffExchangeEmailDTO.getRecipientName());
		bean.put("host", GhixEndPoints.GHIXWEB_SERVICE_URL );
		/*
		 * Put Plan Details into bean  
		 */
		bean.put(PLAN_TYPE,enrollmentOffExchangeEmailDTO.getPlanType());
		bean.put(OFFICE_VISIT_AMT,enrollmentOffExchangeEmailDTO.getOfficeVisit());
		bean.put(GENERIC_MEDICATION_AMT,enrollmentOffExchangeEmailDTO.getGenericMedication());
		bean.put(DEDUCTIBLE,enrollmentOffExchangeEmailDTO.getDeductible());
		bean.put(MAX_OOP,enrollmentOffExchangeEmailDTO.getMaxOop());
		bean.put(NET_PREMIUM_AMT,enrollmentOffExchangeEmailDTO.getNetPremiumAmount());
		bean.put(PLAN_NAME,enrollmentOffExchangeEmailDTO.getPlanName());
		//bean.put(LINK,enrollmentOffExchangeEmailDTO.getLink());
		bean.put(LINK,GhixEndPoints.GHIXWEB_SERVICE_URL+LOGIN_POSTFIX);
		bean.put(INSURANCE_COMPANY, enrollmentOffExchangeEmailDTO.getInsuranceCompany());
		bean.put(COMPANY_LOGO, enrollmentOffExchangeEmailDTO.getCompanyLogo());
		setTokens(bean);

		Map<String, String> data = new HashMap<String, String>();
		data.put("To", enrollmentOffExchangeEmailDTO.getRecipient());
		data.put("Subject", "Congratulations on the submission of your "+ enrollmentOffExchangeEmailDTO.getInsuranceType() + " insurance application!");

		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
	}
	public void setSingleData(Map<String, String> singleData) {
		this.singleData = singleData;
	}
	public EnrollmentOffExchangeEmailDTO getEnrollmentOffExchangeEmailDTO() {
		return enrollmentOffExchangeEmailDTO;
	}
	public void setEnrollmentOffExchangeEmailDTO(
			EnrollmentOffExchangeEmailDTO enrollmentOffExchangeEmailDTO) {
		this.enrollmentOffExchangeEmailDTO = enrollmentOffExchangeEmailDTO;
	}
}
