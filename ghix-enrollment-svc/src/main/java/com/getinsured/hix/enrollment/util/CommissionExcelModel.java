package com.getinsured.hix.enrollment.util;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CommissionExcelModel {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CommissionExcelModel.class);

	private String marketSeg;
	private String region;
	private String CFO;
	private String producerName;
	private String producerAddress;
	private String consumerTaxId;
	private String loc;
	private String GA;
	private Float payeeShare;
	private Date effectiveDate;
	private String contractState;
	private String UID;
	private String consumerName;
	private String productGroup;
	private String productName;
	private String productFunding;
	private Integer productSubs;
	private Date premiumPaidMonth;
	private String subscriberMonth;
	private Float basePremium;
	private Integer baseSubscribers;
	private Float baseCommission;
	private Float bonusPremium;
	private Integer bonusSubscribers;
	private Float bonusCommission;
	private Float adjustmentAmount;
	private String adjustmentType;
	private Float totalPremium;
	private Integer totalSubscribers;
	private Float totalCommission;
	private String checkRunDate;
	private String saleAdjustmentReason;
	private String paycheckNumOrEFT;
	private String paymentType ;
	private String carrierName;
	private String carrierArranmgmtName;
	private String exchangeIdentifier;
	private String exchangeOnOffIndicator;
		
	public String getMarketSeg() {
		return marketSeg;
	}

	public void setMarketSeg(String marketSeg) {
		this.marketSeg = marketSeg;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCFO() {
		return CFO;
	}

	public void setCFO(String cFO) {
		CFO = cFO;
	}

	public String getProducerName() {
		return producerName;
	}

	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}

	public String getProducerAddress() {
		return producerAddress;
	}

	public void setProducerAddress(String producerAddress) {
		this.producerAddress = producerAddress;
	}

	public String getConsumerTaxId() {
		return consumerTaxId;
	}

	public void setConsumerTaxId(String consumerTaxId) {
		this.consumerTaxId = consumerTaxId;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public String getGA() {
		return GA;
	}

	public void setGA(String gA) {
		GA = gA;
	}

	public Float getPayeeShare() {
		return payeeShare;
	}

	public void setPayeeShare(Float payeeShare) {
		this.payeeShare = payeeShare;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getContractState() {
		return contractState;
	}

	public void setContractState(String contractState) {
		this.contractState = contractState;
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}

	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductFunding() {
		return productFunding;
	}

	public void setProductFunding(String productFunding) {
		this.productFunding = productFunding;
	}

	public Integer getProductSubs() {
		return productSubs;
	}

	public void setProductSubs(Integer productSubs) {
		this.productSubs = productSubs;
	}

	public Date getPremiumPaidMonth() {
		return premiumPaidMonth;
	}

	public void setPremiumPaidMonth(Date premiumPaidMonth) {
		this.premiumPaidMonth = premiumPaidMonth;
	}

	public String getSubscriberMonth() {
		return subscriberMonth;
	}

	public void setSubscriberMonth(String subscriberMonth) {
		this.subscriberMonth = subscriberMonth;
	}

	public Float getBasePremium() {
		return basePremium;
	}

	public void setBasePremium(Float basePremium) {
		this.basePremium = basePremium;
	}

	public Integer getBaseSubscribers() {
		return baseSubscribers;
	}

	public void setBaseSubscribers(Integer baseSubscribers) {
		this.baseSubscribers = baseSubscribers;
	}

	public Float getBaseCommission() {
		return baseCommission;
	}

	public void setBaseCommission(Float baseCommission) {
		this.baseCommission = baseCommission;
	}

	public Float getBonusPremium() {
		return bonusPremium;
	}

	public void setBonusPremium(Float bonusPremium) {
		this.bonusPremium = bonusPremium;
	}

	public Integer getBonusSubscribers() {
		return bonusSubscribers;
	}

	public void setBonusSubscribers(Integer bonusSubscribers) {
		this.bonusSubscribers = bonusSubscribers;
	}

	public Float getBonusCommission() {
		return bonusCommission;
	}

	public void setBonusCommission(Float bonusCommission) {
		this.bonusCommission = bonusCommission;
	}

	public Float getAdjustmentAmount() {
		return adjustmentAmount;
	}

	public void setAdjustmentAmount(Float adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}

	public String getAdjustmentType() {
		return adjustmentType;
	}

	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}

	public Float getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(Float totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Integer getTotalSubscribers() {
		return totalSubscribers;
	}

	public void setTotalSubscribers(Integer totalSubscribers) {
		this.totalSubscribers = totalSubscribers;
	}

	public Float getTotalCommission() {
		return totalCommission;
	}

	public void setTotalCommission(Float totalCommission) {
		this.totalCommission = totalCommission;
	}

	public String getCheckRunDate() {
		return checkRunDate;
	}

	public void setCheckRunDate(String checkRunDate) {
		this.checkRunDate = checkRunDate;
	}

	public String getSaleAdjustmentReason() {
		return saleAdjustmentReason;
	}

	public void setSaleAdjustmentReason(String saleAdjustmentReason) {
		this.saleAdjustmentReason = saleAdjustmentReason;
	}

	public String getPaycheckNumOrEFT() {
		return paycheckNumOrEFT;
	}

	public void setPaycheckNumOrEFT(String paycheckNumOrEFT) {
		this.paycheckNumOrEFT = paycheckNumOrEFT;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getCarrierArranmgmtName() {
		return carrierArranmgmtName;
	}

	public void setCarrierArranmgmtName(String carrierArranmgmtName) {
		this.carrierArranmgmtName = carrierArranmgmtName;
	}

	public String getExchangeIdentifier() {
		return exchangeIdentifier;
	}

	public void setExchangeIdentifier(String exchangeIdentifier) {
		this.exchangeIdentifier = exchangeIdentifier;
	}

	public String getExchangeOnOffIndicator() {
		return exchangeOnOffIndicator;
	}

	public void setExchangeOnOffIndicator(String exchangeOnOffIndicator) {
		this.exchangeOnOffIndicator = exchangeOnOffIndicator;
	}

	public static ObjectMapper getMapper() {
		return mapper;
	}

	public static void setMapper(ObjectMapper mapper) {
		CommissionExcelModel.mapper = mapper;
	}

	private static ObjectMapper mapper = new ObjectMapper();
	
	public String toJson() {
		String commissionAppJson = "";
		try {
			commissionAppJson = mapper.writeValueAsString(this);
		} catch (Exception excp) {
			LOGGER.error("Exception in converting toJSON" , excp);
			commissionAppJson = excp.getMessage();
		}
		return commissionAppJson;
	}

}
