package com.getinsured.hix.enrollment.carriefeed.model_io;

import com.getinsured.hix.enrollment.carriefeed.model.CsvModel;
import com.getinsured.hix.enrollment.carrierfeed.util.CsvFileWriter;
import com.getinsured.hix.enrollment.carrierfeed.util.DataExporterBase;
import com.getinsured.hix.enrollment.carrierfeed.util.ExcelClassicFileWriter;
import com.getinsured.hix.enrollment.carrierfeed.util.FileType;

import java.io.IOException;

/**
 * Created by jabelardo on 7/10/14.
 */
public class CsvModelDataExporter extends DataExporterBase<CsvModel> {

    public CsvModelDataExporter(String outputDateFilename, FileType fileType, String mappingsFilename) throws IOException {
        super(outputDateFilename, fileType, mappingsFilename);
    }

    @Override
    protected void registerFileWriterCreators() {
        register(FileType.EXCEL_CLASSIC, new ExcelClassicFileWriter<CsvModel>());
        register(FileType.CSV, new CsvFileWriter<CsvModel>());
    }
}
