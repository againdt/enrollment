package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrlCms820Premium;


public interface IEnrlCms820PremiumRepository extends JpaRepository<EnrlCms820Premium, Integer> {

	@Query("FROM EnrlCms820Premium ecp WHERE ecp.enrollmentId= :enrollmentId")
	List<EnrlCms820Premium> findByEnrollmentId(@Param("enrollmentId") Long enrollmentId);
}
