package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentCmsFileTransferLog;

public interface IEnrollmentCmsFileTransferLogRepository extends JpaRepository<EnrollmentCmsFileTransferLog, Integer> {
	
	@Query("FROM EnrollmentCmsFileTransferLog as e WHERE e.fileName = :fileName ORDER BY e.id DESC ")
	List<EnrollmentCmsFileTransferLog> getTransferLogByFileName(@Param("fileName") String fileName );
	
	@Query("FROM EnrollmentCmsFileTransferLog as e WHERE e.fileName = :fileName AND e.batchExecutionId = :batchExecutionId ORDER BY e.id DESC ")
	List<EnrollmentCmsFileTransferLog> getTransferLogByFileNameAndExecutionId(@Param("fileName") String fileName, @Param("batchExecutionId") Long batchExecutionId);
}
