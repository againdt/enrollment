/**
 * 
 */
package com.getinsured.hix.enrollment.util;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.dto.enrollment.EnrollmentSftpFileTransferDTO;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;

/**
 * SFTP download thread for multiple file downloads
 * @author negi_s
 * @since 06/10/2016
 *
 */
public class EnrollmentSftpDeleteThread implements Callable<EnrollmentSftpFileTransferDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentSftpDeleteThread.class);

	private String fileName;
	private String hostLocation;
	private Session session;

	/**
	 * Initialize thread
	 * @param fileName File to download
	 * @param hostLocation File location on remote location
	 * @param targetLocation File download location
	 * @param session JSch session
	 */
	public EnrollmentSftpDeleteThread(String fileName, String hostLocation, Session session) {
		super();
		this.fileName = fileName;
		this.hostLocation = hostLocation;
		this.session = session;
	}

	@Override
	/**
	 * Over-ridden thread call method
	 */
	public EnrollmentSftpFileTransferDTO call() throws Exception {
		EnrollmentSftpFileTransferDTO fileTransferDto= new EnrollmentSftpFileTransferDTO(); 
		Channel channel =  null;
		try {
			LOGGER.info(" Thread Name :: "+ Thread.currentThread().getName()+" File Name ::" + fileName);
			channel = session.openChannel("sftp");
			channel.connect();
			//Call Util method to download file
			fileTransferDto = EnrollmentSftpUtil.deleteFile(fileName, hostLocation, (ChannelSftp)channel);
		} catch (Exception e) {
			fileTransferDto.setFileName(fileName);
			fileTransferDto.setTransferStatus(EnrollmentConstants.FAILURE);
			fileTransferDto.setErrorMsg(e.getMessage());
			LOGGER.error("Exception occurred in EnrollmentSftpDeleteThread: "+Thread.currentThread().getName()+" File Name ::" + fileName+ "::" + e.getMessage(), e);
		}finally{
			if(null !=  channel){
				channel.disconnect();
				LOGGER.info(" Thread Name :: "+ Thread.currentThread().getName()+" :: Channel disconnected");
			}
		}
		return fileTransferDto;
	}
}
