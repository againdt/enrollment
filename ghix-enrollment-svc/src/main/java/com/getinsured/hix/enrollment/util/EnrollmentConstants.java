

/**
 * 
 */
package com.getinsured.hix.enrollment.util;
 
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

 
 
/**
 * @author Priya
 *
 * In this class we can define constants for Enrollment Module.
  * 
 */
 
 
@Component
public final class EnrollmentConstants {
 
        private EnrollmentConstants() {
        }
       
        public static enum EnrollmentAppEvent{
        	ENROLLMENT_STATUS_UPDATE, AUTOMATED_CHANGE_REPORTING, SPECIAL_ENROLLMENT,
        	DEMOGRAPHIC_UPDATES, ENROLLMENT_SUBMITTED, PLAN_CHANGE_DISENROLLMENT,VOLUNTARY_DISENROLLMENT,
        	OTHER_DISENROLLMENT, SUBSCRIBER_CHANGE_DISENROLLMENT, SUBSCRIBER_CHANGE_ENROLLMENT,PLAN_CHANGE_ENROLLMENT , REINSTATEMENT,ENROLLMENT_STATUS_CHANGED,
        	ENROLLMENT_TERMINATED, ENROLLMENT_INFORCE, ENROLLMENT_START_DATE_EDIT, ENROLLMENT_END_DATE_EDIT, MEMBER_START_DATE_EDIT,
        	MEMBER_END_DATE_EDIT, MEMBER_NAME_EDIT, MEMBER_SSN_EDIT, MEMBER_GENDER_EDIT, MEMBER_DATA_EDIT,
        	OUTBOUND_834_SENT, RESEND_834_LAST_TXN, RESEND_834_HISTORY, INBOUND_834_EDI_ERROR, INBOUND_834_DATA_REJECTED,
        	OUTBOUND_834_EDI_ERROR, CS_LEVEL_CHANGE_DISENROLLMENT, CS_LEVEL_CHANGE_ENROLLMENT,
        	INBOUND_834_999_ACK_RECEIVED, INBOUND_834_TA1_ACK_RECEIVED, OUTBOUND_834_999_ACK_RECEIVED,
        	OUTBOUND_834_TA1_ACK_RECEIVED, INBOUND_834_TERM_EVT_PROCESSED, INBOUND_834_CANCEL_EVT_PROCESSED,
        	INBOUND_834_CONFIRM_EVT_PROCESSED,ENROLLMENT_APTC_EDIT, CARRIER_CONFIRMATION_RECON, CARRIER_RECONCILIATION, ADD_OVERRIDE, CANCEL_OVERRIDE,
        	ENROLLMENT_1095_RESEND, ENROLLMENT_SS_EDIT
        };
        
        public static enum ReportType {
    		IRS, PLR, CMS, ANNUAL, CARRIERRECON
    	}
        
        
        public static enum TobacoUsage {

            TOBACO("T", "1"), NO_TOBACO("N", "2"), UNDEFINED("U", "2");

            private final String key;
            private final String value;

            TobacoUsage(String key, String value) {
                this.key = key;
                this.value = value;
            }

            public String getKey() {
                return key;
            }
            public String getValue() {
                return value;
            }
            public static String getValue(String key) {
            	for(TobacoUsage en: TobacoUsage.values()){
            		if(en.getKey().equalsIgnoreCase(key)){
            			return en.getValue();
            		}
            	}
                return null;
            }
        }
        
        public static final String JSON_DATE_FORMAT = "MMM dd, yyyy hh:mm:ss a";
        public static final String SEND_IN_834 = "834 Sent";
        public static final String RECEIVED_834 = "834_RECEIVED"; 
        public static final String SENT_834 = "834_SENT";
        public static final String STATE_CODE_CA = "CA"; 
        public static final String STATE_CODE_ID = "ID";
        public static final String STATE_CODE_NM = "NM";
        public static final String STATE_CODE_MS = "MS"; 
        public static final String ID = "id";
        public static final String DEFAULT_SORT_COLUMN = "id";
        public static final String ORDER_ID = "orderId";
        public static final String ENROLLMENT_TYPE="enrollmentType";
        public static final String AUTORENEWAL_REQUEST = "autoRenewalRequest";
        public static final String PD_RESPONSE = "pdResponse";
        public static final String ORDER_ITEM_ID = "orderItemId";
        public static final String PLAN_ID = "planId";
        public static final String PLAN_LEVEL = "planLevel";
        public static final String CHANGE_PLAN_ALLOWED = "changePlanAllowed";
        public static final String CMS_PLAN_ID="CMSPlanID" ;
        public static final String CASE_ID = "caseId";
        public static final String PLAN_TIER_ID = "planTierId";
        public static final String ENROLLMENT_ID = "enrollmentId";
        public static final String ENROLLMENT_ID_KEY = "EnrollmentID";
        public static final String EMPLOYEE_ID_KEY = "EmployeeID";
        public static final String EMPLOYER_ID = "employerid";
        public static final String EMPLOYER_ENROLLMENT_ID="employerEnrollmentId";
        public static final String MEMBER_ID = "memberId";
        public static final String MEMBER_ID_KEY = "MemberId";
        public static final String ASSISTER_BROKER_ID = "assisterBrokerId";
        public static final String EXISTING_QHP_ENROLLMENT_ID = "existingQhpEnrollmentId";
        public static final String EXISTING_SADP_ENROLLMENT_ID = "existingSadpEnrollmentId";
        public static final String EMPLOYEE_ID = "employeeId";
        public static final String CUSTODIAL_PARENT_ID = "custodialParentId";
        public static final String RESPONSIBLE_PERSON_ID = "responsiblePersonId";
        public static final String HOUSEHOLD_CONTACT_ID = "houseHoldContactId";
        public static final String ENROLLEE_ID = "enrolleeId";
        public static final String POLICY_ID = "policyId";
        public static final String JAN_2015_DATE = "01/01/2015";
        public static final String EMPLOYEE_APP_ID = "employeeAppIds";
        public static final String SUBSCRIBER_MEMBER_ID = "subscriberMemberId";
        public static final String ENROLLMENT_INFORCE = "Enrollment Inforce";
        public static final String ENROLLMENT_TERMINATED = "Enrollment Terminated";
        public static final String ENROLLMENT_SUBMITTED = "Enrollment Submitted";
        public static final String ENROLLMENT_STATUS_CHANGED = "Enrollment Status Changed";
        
        
        public static final String QHP_DIS_ENROLLMENT_END_DATE = "QHPDisenrollmentEndDate";
        public static final String SADP_DIS_ENROLLMENT_END_DATE = "SADPDisenrollmentEndDate";
        public static final String TERMINATION_DATE = "TerminationDate";
        public static final String DEATH_DATE = "DeathDate";
        public static final String CURRENT_DATE = "currentDate";
        public static final String LAST_INVOICE_DATE = "lastInvoiceDate";
        public static final String LAST_INVOICE_TIMESTAMP = "lastInvoiceTimestamp";
        public static final String EFFECTIVE_START_DATE = "effectiveStartDate";
        public static final String EFFECTIVE_END_DATE = "effectiveEndDate";
        public static final String LAST_PREMIUM_PAID_DATE = "lastPremiumPaidDate";
        public static final String ENROLLMENT_CONFIRMATION_DATE = "enrollmentConfirmationDate";
        public static final String ENROLLMENT_REASON_S = "S";
        public static final String ENROLLMENT_REASON_I = "I";
        public static final String ENROLLMENT_REASON = "enrollment_reason";
        public static final String INSURANCE_TYPE_DENTAL = "DENTAL";
        public static final String INSURANCE_TYPE_HEALTH = "HEALTH";
        public static final String CSR = "csr";
        public static final String CSR_1 = "CS1";
        public static final String USER="User";
        public static final String MAILING_ZIP="mailingZip";
        public static final String EXTERNAL_MEMBER_ID="externalMemberId";
        
        public static final String EVENT_REASON = "EVENT_REASON";
		public static final String SPCL_EVENT_REASON = "SPCL_EVENT_REASON";
        public static final String EVENT_TYPE = "EVENT_TYPE";
        public static final String LOOKUP_ENROLLMENT_TYPE = "ENROLLMENT_TYPE";
        public static final String LOOKUP_TYPE_RACE = "RACE";
        public static final String LOOKUP_INSURANCE_TYPE = "INSURANCE_TYPE";
        public static final String LOOKUP_INSURANCE_TYPE_DENTAL = "DENTAL";
        public static final String LOOKUP_INSURANCE_TYPE_DENTAL_CODE = "DEN";
        public static final String LOOKUP_INSURANCE_TYPE_HEALTH = "HEALTH";
        public static final String LOOKUP_PERSON_TYPE = "PERSON_TYPE";
        public static final String LOOKUP_PERSON_TYPE_ENROLLEE = "ENROLLEE";
        public static final String LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON = "RESPONSIBLE_PERSON";
        public static final String LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT = "CUSTODIAL_PARENT";
        public static final String LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT = "HOUSEHOLD_CONTACT";
        public static final String PERSON_TYPE_SUBSCRIBER = "SUBSCRIBER";
        public static final String ENROLLMENT_STATUS = "ENROLLMENT_STATUS";
        public static final String ENROLLMENT_STATUS_CONFIRM = "CONFIRM";
        public static final String ENROLLMENT_STATUS_CANCEL = "CANCEL";
        public static final String ENROLLMENT_STATUS_TERM = "TERM";
        public static final String ENROLLMENT_STATUS_INFORCE = "INFORCE";
        public static final String ENROLLMENT_STATUS_WITHDRAWN = "WITHDRAWN";
        public static final String ENROLLMENT_STATUS_PENDING = "PENDING";
        public static final String ENROLLMENT_PAYMENT_RECEIVED = "PAYMENT_RECEIVED";
        public static final String ENROLLMENT_STATUS_ABORTED = "ABORTED";
        public static final String BATCH_JOB_STATUS= "COMPLETED";
        public static final String GENDER = "GENDER";
        public static final String GENDER_UNKNOWN_LABEL = "Unknown";
        public static final String RACE_OTHER_CODE = "2131-1";
        public static final String MARITAL_STATUS = "MARITAL_STATUS";
        public static final String MARITAL_STATUS_UNREPORTED_LABEL = "Unreported";
        public static final String LANGUAGE_SPOKEN = "LANGUAGE_SPOKEN";
        public static final String LANGUAGE_WRITTEN = "LANGUAGE_WRITTEN";
        public static final String TOBACCO_USAGE = "TOBACCO_USAGE";
        public static final String RELATIONSHIP = "RELATIONSHIP";
        public static final String HIGH = "HIGH";
        public static final String CITIZENSHIP_STATUS = "CITIZENSHIP_STATUS";
        public static final String MAINTENANCE_REASON_CODE = "maintenanceReasonCode";
        public static final String SERC = "serc";
        public static final String IRS_EOM_IND_RESUBMIT_BATCH_REQ = "IRS_EOM_IND_RESUBMIT_BATCH_REQ";
        public static final String IRS_EOM_SHOP_RESUBMIT_BATCH_REQ = "IRS_EOM_SHOP_RESUBMIT_BATCH_REQ";
        public static final String IRS_EOM_ERROR_RESP = "IRS_EOM_ERROR_RESP";
        public static final String IRS_EOM_IND_RESUBMIT_FILE_REQ ="IRS_EOM_IND_RESUBMIT_FILE_REQ";
        public static final String IRS_EOM_SHOP_RESUBMIT_FILE_REQ ="IRS_EOM_SHOP_RESUBMIT_FILE_REQ";
        public static final String IRS_EOM_MISSING_FILE_RESP ="IRS_EOM_MISSING_FILE_RESP";
        public static final String IRS_EOM_IND_REQ ="IRS_EOM_IND_REQ";
        public static final String IRS_EOM_SHOP_REQ ="IRS_EOM_SHOP_REQ";
        public static final String IRS_EOM_NACK_RESP ="IRS_EOM_NACK_RESP";
        public static final String IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ ="IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ";
        public static final String IRS_EOM_SHOP_RESUBMIT_MISSING_FILE_REQ ="IRS_EOM_SHOP_RESUBMIT_MISSING_FILE_REQ";
        public static final String LOOKUP_VALUE_OTHER = "other";
        public static final String CSR_LEVEL_APPENDER = "CS";
                        
        public static final String YES = "YES";
        public static final String NO = "NO";
        public static final String TRUE = "true";
        public static final String FALSE = "false";
                
        public static final String STATUS = "status";
        public static final String ENROLLMENT_STATUS_KEY1 = "EnrollmentStatus";
        public static final String ENROLLMENT_STATUS_KEY = "enrollmentStatus";
        public static final String INDIVIDUAL_ENROLLMENT_BATCH_JOB="enrollmentXMLIndividualJob";
        public static final String CMS_DAILY_ENROLLMENT_BATCH_JOB="cmsDailyEnrollmentReportJob";
        public static final String CMS_RECONCILIATION_ENROLLMENT_REPORT_JOB="cmsReconciliationEnrollmentReportJob";
        public static final String SEND_CARRIER_UPDATED_ENROLLMENT_JOB= "sendCarrierUpdatedEnrollmentJob";
        public static final String SHOP_ENROLLMENT_BATCH_JOB="enrollmentXMLShopJob";
        public static final String DEPENDENT_CONTRIBUTION = "dependentContribution";
        public static final String IND_RECON_EXTRACT_MONTH_CURRENT="current";
        public static final String IND_RECON_EXTRACT_MONTH_PRIOR="prior";
        public static final String SUBSCRIBER_FLAG = "subscriberFlag";
        public static final String PROGRAM_TYPE = "programType";
        public static final String APPLICANT_ESIG = "applicant_esig";
        public static final String COVERAGE_START_DATE = "coverageStartDate";
        public static final String GROSS_PREMIUM_COST = "grossPremiumCost";
        public static final String GROSS_PREMIUM_AMT = "grossPremiumAmt";
        public static final String NET_PREMIUM_AMT = "netPremiumAmt";
        public static final String EMPLOYEE_CONTRIBUTION = "employeeContribution";
        public static final String EMPLOYER_CONTRIBUTION = "employerContribution";
        public static final String APTC_APPLIED_AMOUNT = "aptcAppliedAmount";
        public static final String NET_PREMIUM_AMOUNT = "netPremiumAmount";
        public static final String SADP = "sadp";
        public static final String ASSISTER_BROKER_ROLE = "assisterBrokerRole";
        public static final String EMPLOYER_CASE_ID = "employerCaseId";
        public static final String MEMBER_DETAILS = "memberDetails";
        public static final String PLAN_DETAILS = "planDetails";
        public static final String DIS_ENROLLMENT_TYPE = "DisEnrollmentType";
        public static final String TERMINATION_REASON_CODE = "TerminationReasonCode";
        public static final String CARRIER_STAT_UPDATE_JOB_NAME= "carrierStatusUpdate";
        public static final String EMPLOYER = "employer";
        public static final String HEALTH_COVERAGE_POLICY_NO = "healthCoveragePolicyNo";
        public static final String PLAN_TYPE = "planType";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String MIDDLE_NAME = "middleName";
        public static final String DOB = "dob";
        public static final String PRIMARY_PHONE = "primaryPhone";
        public static final String SECONDARY_PHONE = "secondaryPhone";
        public static final String PREFERRED_PHONE = "preferredPhone";
        public static final String PREFERRED_EMAIL = "preferredEmail";
        public static final String SSN = "ssn";
        public static final String SUFFIX = "suffix";
        public static final String INDIVIDUAL_PREMIUM = "individualPremium";
        public static final String EMPLOYER_RESPONSIBILITY_AMT = "contribution";
        public static final String RATING_AREA = "ratingArea";
        public static final String GENDER_CODE = "genderCode";
        public static final String MARITAL_STATUS_CODE = "maritalStatusCode";
        public static final String SPOKEN_LANGUAGE_CODE = "spokenLanguageCode";
        public static final String WRITTEN_LANGUAGE_CODE = "writtenLanguageCode";
        public static final String RELATIONSHIP_CODE = "relationshipCode";
        public static final String TOBACCO = "tobacco";
        public static final String LAST_TOBACCO_USE_DATE = "lastTobaccoUseDate";
        public static final String HOUSEHOLD_CONTACT_RELATIONSHIP = "houseHoldContactRelationship";
        public static final String CITIZENSHIP_STATUS_CODE = "citizenshipStatusCode";
        public static final String DESCRIPTION = "description";
        public static final String APTC = "aptc";
        public static final String MAX_APTC = "maxAptc";
        public static final String STATE_SUBSIDY = "stateSubsidy";
        public static final String MAX_STATE_SUBSIDY = "maxStateSubsidy";
        public static final String EHB_AMT = "ehbAmount";
        public static final String MARKET_TYPE = "marketType";
        public static final String UPDATED_ON = "updatedOn";
        public static final String CAUSE=":: Cause ::";
        public static final String MESSAGE="Message : ";
        public static final String SUBSCRIBER_HOME_ADD_ZIP = "subscribersHomeAddressZip";
        public static final String SUBSCRIBER_HOME_ADD_COUNTYCODE = "subscribersHomeAddressCountyCode";
        public static final String ENROLLEE_AGE_AND_MEMBER_DATA = "enrolleeAgeAndMemberId";
        
        public static final String SORT_BY = "sortBy";
        public static final String SORT_ORDER = "sortOrder";
        public static final String START_RECORD = "startRecord";
        public static final String PAGE_SIZE = "pageSize";
        public static final String PAGE_NUMBER = "pageNumber";
       
        public static final String REASON_CODE_ADMIN_CHANGE_29="29";
        public static final String EVENT_TYPE_CHANGE="001";
        public static final String EVENT_REASON_AI="AI";
        public static final Integer AGE_18=18;
        public static final String REASON_CODE_DEATH="03";
        public static final String RELATIONSHIP_LOOKUPVALUE_CODE = "18";
        public static final String EVENT_TYPE_ADDITION="021";
        public static final String EVENT_TYPE_CANCELLATION="024";
        public static final String EVENT_TYPE_TERMINATION="024";
        public static final String EVENT_TYPE_REINSTATEMENT="025";
        public static final String ENROLLMENT_TYPE_SHOP = "24";
        public static final String EVENT_REASON_INITIAL_ENROLLMENT = "28";
        public static final String EVENT_REASON_NON_PAYMENT = "59";
        public static final String EVENT_REASON_IDENTITY_CHANGE = "25";
        public static final String MODULE_STATUS_CODE_E000 = "E-000";
        public static final String MODULE_STATUS_CODE_E003 = "E-003";
        public static final String MODULE_STATUS_CODE_E001 = "E-001";
        public static final String MODULE_STATUS_CODE_ENSSAP01 = "EN-SSAP-001";
        public static final Integer DEFAULT_START_RECORD = 0;
        public static final Integer DEFAULT_PAGE_NUMBER = 1;
        
        public static final String EVENT_REASON_BENEFIT_SELECTION = "EC";
        public static final String EVENT_REASON_29 = "29";
        public static final String EVENT_REASON_REENROLLMENT = "41";
        public static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
        public static final String ENROLLMENT_TYPE_MEDICAL = "MEDICAL";
        public static final String RESPONSIBLE_PERSON_ID_CODE_QD = "QD";
        public static final String DEFAULT_SORT_ORDER = "ASC";
        public static final String AND_REVISION=" and revision=";
        public static final String ENROLLMENT_WITH_EFFECTIVE_END_DATE="Enrollment with invalid EffectiveEndDate = ";
        public static final String ENROLLMENT_WITH_EFFECTIVE_START_DATE="Enrollment with invalid EffectiveStartDate = ";
        public static final String DATE_YEAR_DID_NOT_MATCH_WITH= ", Date Year did not match with ";
        public static final String NO_ACTIVE_ENROLLMENT_FOUND="No Active Enrollment found";
        public static final String TERM_TXN_REJECT_REASON = "TERM Transaction Rejected due to shorted TERM date present in the system";
        public static final String REJECT_INCORRECT_COVERAGE_YEAR = "{TERM/CANCEL/CONFIRM} transaction rejected due to Incorrect Coverage Year";
        public static final String INBOUND_REJECT_EXISTING_CANCEL = "{CONFIRM/TERM/CANCEL} Transaction Rejected due to existing Enrollment is in Cancelled Status";
        public static final String INVALID_ENROLLMENT_TYPE = "Invalid enrollment type";
        public static final String POST_REJECT_MAINT_CODE = "Post Rejection Reason as {TERM/CANCEL} Rejected due to Invalid Maintenance Reason Code: ";
        public static final String TERM_TXN_REJECT = "TERM Transaction Rejected due to Incorrect TERM Date";
        public static final String MSG_ENROLLMENT_NOT_IN_REINSTATABLE_STATE = "One or more of the provided Enrollment(s) is not in a Reinstatable state.";
        
        public static final String FFM_ENROLLMENT_TXN_TYPE_E = "E";
        public static final String ENROLLMENT_TYPE_INITIAL = "I";
        public static final String ENROLLMENT_TYPE_SPECIAL = "S";
        public static final String ENROLLMENT_TYPE_NEW = "N";
        public static final String SUBSCRIBER_FLAG_YES = "Y";
        public static final String SUBSCRIBER_FLAG_NO = "N";
        public static final String ASSISTER_ROLE = "Assister";
        public static final String AGENT_ROLE = "Agent";
        public static final String CUSTODIAL_PARENT_HOME_ZIP="custodialParentHomeZip";
        public static final String NEW_MAP_KEY = "New";
        public static final String OLD_MAP_KEY = "Old";
        public static final String CURRENT_MAP_KEY = "Current";
        
        public static final String Y = "Y";
        public static final String N = "N";
        public static final String T = "T";
        public static final String U= "U";
        
        public static final String HOUSEHOLD_CASE_ID=":: Household Case Id :: ";
        
        public static final String ERR_MSG_FECTHING_ACTIVE_ENROLLMENT_COUNT_FOR_APP_ID = "Error fetching Active Health Enrollemnts for Application ID";
        public static final String ERR_MSG_MULTIPLE_SUBSCRIBERS_FOR_ENROLLMENT = "Multiple Subscribers for Enrollment ";
        public static final String ERR_MSG_NO_SUBSCRIBERS_FOR_ENROLLMENT = "No Valid Subscribers for Enrollment ";
        public static final String ERR_MSG_SUBSCRIBERS_VALIDATION_FAILED_FOR_ENROLLMENT = "Subscriber Validations for Enrollment Failed ";
        
        public static final String ERR_MSG_INVALID_INPUT = "Invalid input for mandatory field";
        public static final String ERR_MSG_HOUSEHOLD_IS_NULL = "Invalid input: HouseHold is Null";
        public static final String ERR_MSG_HOUSEHOLD_OR_EXCHG_IDENTIFIER_NULL = "Invalid input: HouseHoldCaseID or Exchg_Indiv_Identifier is Null";
        public static final String ERR_MSG_INVALID_REQUEST = "Invalid Update Request: Both isLocationIdUpdated and isOnlySsapAppIdUpdated are True";
        public static final String ERR_MSG_ENROLLMENT_STATUS_IS_NULL = "Invalid input: Enrollment Status is Null";
        public static final String ERR_MSG_EMPLOYER_ID_IS_NULL = "Invalid input: EmployerId is Null";
        public static final String ERR_MSG_ENROLLMENT_ID_IS_NULL = "Invalid input: EnrollmentId is Null";
        public static final String ERR_MSG_EMPLOYEE_ID_IS_NULL = "Invalid input: EmployeeId is Null";
        public static final String ERR_MSG_INVALID_USER = "Invalid User";
        
        public static final String ERR_MSG_INVALID_MARKET_TYPE = "Invalid input for Market Type";
        
        public static final String ERR_MSG_ADMIN_UPDATE_INDIVIDUAL_REQUEST_IS_NULL = "Invalid input: AdminUpdateIndividualRequest is Null";
        public static final String ERR_MSG_ENROLLEE_REQUEST_IS_NULL = "Invalid input: Enrollee request is Null";
        public static final String ERR_MSG_ENROLLEMENT_REQUEST_IS_NULL = "Invalid input: Enrollment request is Null";
        public static final String ERR_MSG_ENROLLEMENT_APTC_UPDATE_DTO_IS_NULL = "Invalid input: Enrollment Aptc Update DTO is Null or Empty";
        public static final String ERR_MSG_NO_RECORDS_FOUND="No records Found";
        public static final String ERR_MSG_REQUEST_IS_NULL = "Invalid input: Request is Null";
        public static final String ERR_MSG_APP_ID_IS_NULL = "Invalid input: App id is Null";
        public static final String ERR_MSG_BROKER_IS_NULL = "Invalid input: Assister/Broker Id is Null";
        public static final String ERR_MSG_OLD_BROKER_IS_NULL = "Invalid input: Old Assister/Broker Id is Null";
        public static final String ERR_MSG_IVALID_USER_ROLE = "Invalid input: Not valid Input for User Role";
        public static final String ERR_MSG_IVALID_ACTION = "Invalid input: Not valid Input for AddRemove Action";
        
        public static final String ERR_MSG_INVALID_APTC = "Invalid input: Provided APTC amount is same as existing.";
        public static final String ERR_MSG_SEARCH_CRITERIA_NULL_OR_EMPTY = "Invalid input: Search criteria is Null or empty";
        public static final String ERR_MSG_ORDER_ID_NULL_OR_EMPTY = "Invalid input: orderId is Null or empty";
        public static final String ERR_MSG_EMPLOYER_ID_NULL_OR_EMPTY = "Invalid input: Employer Id is Null or empty";
        public static final String ERR_MSG_EMPLOYER_ENROLLMENT_ID_NULL_OR_EMPTY = "Invalid input: Employer Enrollment Id is Null or empty";
        public static final String ERR_MSG_ENROLLMENT_ID_IS_NULL_OR_EMPTY = "Invalid input: Enrollment ID is Null or Empty";
        public static final String ERR_MSG_EnrollmentAttributeEnum_IS_NULL_OR_EMPTY = " Attribute List is null or empty or contains NULL value";
        public static final String ERR_MSG_ENROLLMENT_NOT_FOUND_FOR_GIVEN_ID = "Enrollment Not found for given ID";
        public static final String ERR_MSG_EMPLOYEE_ID_IS_NULL_OR_EMPTY = "Invalid input: Employee ID is Null or Empty";
        public static final String ERR_MSG_EMPLOYEE_APP_ID_IS_NULL_OR_EMPTY = "Invalid input: Employee APP ID is Null or Empty";
        public static final String ERR_MSG_SSAP_APP_ID_IS_NULL_OR_EMPTY = "Invalid input: SSAP APP ID is Null or Empty";
        public static final String ERR_MSG_TERMINATION_DATE_IS_NULL_OR_EMPTY = "Invalid input: Termination date is Null or Empty";
        public static final String ERR_MSG_LAST_INVOICE_DATE_IS_NULL_OR_EMPTY = "Invalid input: Last Invoice date is Null or Empty";
        public static final String ERR_MSG_TERMINATION_DATE_FORMAT_ERROR = "Invalid input: Termination date is required in MM/dd/yyyy format";
        public static final String ERR_MSG_MEMBER_ID_IS_NULL_OR_EMPTY = "Invalid input: MemberId ID is Null or Empty";
        public static final String ERR_MSG_CURRENT_DATE_NULL_OR_EMPTY = "Invalid input: currentDate is null  or empty";
        public static final String ERR_MSG_STATUS_NULL_OR_EMPTY = "Invalid input: status is null or empty";
        public static final String ERR_MSG_ENROLLMENT_STATUS_NULL_OR_EMPTY = "Invalid input: EnrollmentStatus is Null Or Empty";
        public static final String ERR_MSG_DIS_ENROLLMENT_TYPE_NULL_OR_EMPTY = "Invalid input: DisEnrollmentType is Null Or Empty";
        public static final String ERR_MSG_TERMINATION_REASON_CODE_NULL_OR_EMPTY = "Invalid input: TerminationReasonCode is Null Or Empty";
        public static final String ERR_MSG_DEATH_DATE_NULL_OR_EMPTY = "Invalid input: DeathDate is Null Or Empty";
        public static final String ERR_MSG_START_DATE_NULL_OR_EMPTY = "Invalid input: Start Date is Null Or Empty";
        public static final String ERR_MSG_END_DATE_NULL_OR_EMPTY = "Invalid input: End Date is Null Or Empty";
        public static final String ERR_MSG_EMPLOYEE_ID_AND_EMPLOYEE_ID_IS_NULL_OR_EMPTY = "Invalid input: Employee ID And Employee App ID is Null or Empty";
        public static final String ERR_MSG_EMPLOYEE_APP_ID_AND_SSAP_APP_ID_IS_NULL_OR_EMPTY = "Invalid input: Employee App ID and SSAP App ID is Null or Empty";
        public static final String ERR_MSG_LOGGER="Error in  findEnrolleeByEnrollmentID() ==";
        public static final String ERR_MSG_APPLICATION_ERROR = "Application Error";
        public static final String ERR_MSG_ID_CANNOT_BE_ZERO = "Invalid input: Id cannot be zero";
        public static final String ERR_MSG_TYPE_CANNOT_BE_NULL = "Invalid input: Enrollment Type cannot be null or empty";
        public static final String ERR_MSG_GLOBAL_ID_CANNOT_BE_NULL = "Invalid input: Global id cannot be null or empty";
        public static final String ERR_MSG_INVALID_ENROLLMENT_ID = "Invalid input: Invalid Enrollment ID";
        public static final String ERR_MSG_INVALID_EMPLOYEE_ID = "Invalid input: Invalid Employee ID";
        public static final String ERR_MSG_ENROLLMENT_STATUS_TERM_OR_CANCEL = "Invalid input: EnrollmentStatus should be TERM / CANCEL";
        public static final String ERR_MSG_NO_ENROLLEE = "No Enrollee found with id:";
        public static final String ERR_MSG_SHOP_DISENROLLMENT_FAILED="Shop Disenrollment failed for members:- ";
        public static final String ERR_MSG_ENROLLMENT_STATUS_ALREADY_CONFIRM = "Invalid input: EnrollmentStatus is already CONFIRM";
        
        public static final String ERR_MSG_PLAN_DATA_LIST_RECEIVED_FROM_PLAN_SELECTION_SERVICE_IS_NULL_OR_SIZE_ZERO = "Plan data list received from plan selection service is null or size is zero";
        public static final String ERR_MSG_EMPLOYEE_COUNT_DOES_NOT_MATCH_EMPLOYEE_ENROLLMENTS = "Employee count does not match employee enrollments";
        
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND = "No Enrollment found for given search criteria.";
        public static final String ERR_MSG_NO_PLAN_DATA_FOUND = "No plan data found";
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_CURRENT_MONTH = "No Enrollment found for current month.";
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_STATUS_AND_DATE = "No Enrollment found with given status and termination date.";
        public static final String ERR_MSG_NO_ACTIVE_ENROLLMENT_FOUND_FOR_EMPLOYER = "No Active Enrollment found for Employer.";
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_STATUS_EMPLOYER_AND_DATE = "No enrollment found with given status, employer and termination date.";
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_EMPLOYER_CASE_ID = "Employer has no associated employee enrollments ";
        public static final String ERR_MSG_NO_PLAN_DATA_FOUND_IN_PLAN_SELECTION_SERVICE_RESPONSE = "No Plan data found in Plan selection service response";
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_ENROLLMENT_ID = "No enrollment found for the given enrollment Id";
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_EMP_APP_ID = "No enrollment found for the given employee application Id";
        public static final String ERR_MSG_NO_ACTIVE_ENROLLMENT_FOUND_FOR_THE_GIVEN_EMP_APP_ID = "No active enrollment found for the given employee application Id";
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_SSAP_APP_ID = "No enrollment found for the given ssap application Id : %d";
        public static final String ERR_MSG_NO_ACTIVE_ENROLLMENT_FOUND_FOR_THE_GIVEN_SSAP_APP_ID = "No active enrollment found for the given ssap application Id";
        public static final String ERR_MSG_NO_RESULTS_FOUND = "No results found for given input";
        public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_GIVEN_EMPLOYEE_ID = "No Enrollment found for given Employee Id";
        public static final String ERR_MSG_UPDATE_EMPLOYEE_STATUS_FAILED = "Update Employee Status Failed";
        public static final String ERR_MSG_UPDATE_INDIVIDUAL_STATUS_FAILED = "Update Individual Status Failed";
        public static final String ERR_MSG_NO_ENROLLMENT_ID_LIST_FOR_THE_GIVEN_REQUEST =" Bad Request, No Enrollment Id list";
        
        public static final String MSG_ENROLLMENT_PROCESS_SUCCESS = "Enrollment Processed Successfully";
        public static final String MSG_ENROLLMENT_UPDATE_SUCCESS = "Enrollment updated successfully";
        public static final String MSG_SHOP_ENROLLMENT_UPDATE_SUCCESS = "Shop enrollment updated successfully";
        public static final String MSG_RETRIEVE_ENROLLMENT_SUCCESS = "Fetching enrollment is successfully done.";
        public static final String MSG_RETRIEVE_ENROLLEE_SUCCESS = "Fetching enrollee is successfully done.";
        public static final String ERR_MSG_UPDATE_ENROLLMENT_STATUS_FAILED = "Update Enrollment Status Failed";
        
        public static final String ERR_MSG_FAILED_TO_CALL_PLANDISPLAY_ENROLLMENT_DATA_SERVICE = "Failed to call 'Plan Display' findByOrderId service from enrollment for pdHouseholdId : ";
        public static final String PLANDISPLAY_ENROLLMENT_DATA_SERVICE_RETURNS_NULL = "'Plan Display' FIND_BY_HOUSEHOLD_ID service call returns null for pdHouseholdId : ";
        public static final String ERR_MSG_FAILED_TO_GET_PAYLOAD = "Failed to get payload for payload ID : ";
        public static final String PAYLOAD_OR_IND19_NULL = "Payload returns null for payload ID : ";
        public static final String ERR_MSG_ENROLLMENT_CREATE_FAILURE = "Failed to create Enrollment";
        public static final String ERR_MSG_ENROLLMENT_UPDATE_FAIL = "Failed to update enrollment";
        public static final String ERR_MSG_DIS_ENROLLMENT_FAIL = "Dis-Enrollment has failed";
        public static final String ERR_MSG_SHOP_ENROLLMENT_UPDATE_FAIL = "Failed to update shop enrollment";
        public static final String ERR_MSG_ENROLLMENT_RETRIEVE_FAIL = "Failed to retrieve the enrollment";
        public static final String ERR_MSG_ENROLLEE_RETRIEVE_FAIL = "Failed to retrieve the enrollee";
        public static final String ERR_MSG_MAINTANANCE_REASON_CODE_EMPTY="Maintanance Reason code  is null or empty";
        public static final String ERR_MSG_LOOKUP_VALUE_NULL="Lookup value is null";
        public static final String ERR_MSG_ENROLLMENT_EVENT_RETRIEVE_FAIL = "Failed to retrieve enrollment event";
        public static final String ERR_MSG_MONTHLY_APTC_AMOUNTS_FAIL = "Failed to get Monthly APTC Amounts ";
        public static final String ERR_MSG_NO_ACTIVE_ENROLLMENT = "No Active Enrollment Found for One or more Enrollment Id(s) ";
        
        
        public static final String ERR_MSG_APTC_VALUE_NULL_EMPTY = "Invalid APTC :: APTC Value provided NULL or Empty";
        public static final String ERR_MSG_APTC_DATE_VALUE_NULL_EMPTY = "Invalid APTC Effective Date :: APTC Effective Date Value provided NULL or Empty";
        public static final String ERR_MSG_NON_FINANCIAL_INVALID_APTC_DATE_AND_AMT = "Non Financial:: Invalid value received for APTC Amt :: Expected Null values";
        public static final String ERR_MSG_REASON_CODE_VALUE="Event Reason code  is null or empty";
        public static final String ERR_MSG_LOOKUP_VALUE_IS_NULL="Look up value is null";
        public static final String ERR_MSG_TERM_RSN_CODE_IS_NULL_OR_EMPTY="Termination Reason Code is null or empty";
        public static final String ERR_MSG_ENROLLMENT_LIST_IS_NULL_OR_EMPTY="Enrollment List is either null or empty";
        public static final String ERR_MSG_ZIP_CODE_VALUE="No zip code Found in Enrollee";
        public static final String ERR_MSG_COUNTY_CODE_VALUE="No county code Found in Enrollee";
        public static final String ERR_MSG_CITIZENSHIP_STATUS_NULL="Citizenship status cannot be null or empty for Enrollee";
        public static final String ERR_MSG_SUBSCRIBER_COVERAGE_MISMATCH="Subcriber coverage start date or end date does not match with enrollment";
        public static final String ERR_MSG_NULL_SLCSP_OVERRIDE="Please enter SLCSP value for all the months in the given coverage period.";
        public static final String ERR_MSG_HOME_ADDRESS_UPDATE="Home Address can not be updated through admin update.";
        public static final String ERR_MSG_RELATION_HCP_CODE_VALUE="Relation to HCP provided for member is either null or empty or invalid.";
        public static final String ERROR_MSG_INVALID_ZIP_COUNTY="HomeCountyCode or HomeZIPCode Passed in the request does not match with existing enrollment ZIP or CountyCode";
        public static final String ERROR_MSG_INVALID_RELATION="Relationship To HoldContact is mandatory when relationship is getting updated ";
        public static final String ERR_MSG_NO_LINKED_HOUSEHOLD_FOUND = "No cmr household found for given search criteria.";
        public static final String ERR_MSG_ACT_ON_NEW_APP = "Act on new application before reinstating current Enrollment ";
        public static final String ERR_MSG_FAIL_TO_UPDATE_APP_STATUS = "Failure in Updating Individual Application Status ";
        public static final String ERR_MSG_NO_APP_ID_FOUND= "No Application ID Found in Enrollment to update application status ";
        public static final String ERR_MSG_CHANGE_EFFECTIVE_DATE_NULL_OR_EMPTY = "Invalid input: Change effective date is null or empty";
        
        public static final int ERROR_CODE_200 = 200;
        public static final int ERROR_CODE_201 = 201;
        public static final int ERROR_CODE_202 = 202;
        public static final int ERROR_CODE_203 = 203;
        public static final int ERROR_CODE_204 = 204;
        public static final int ERROR_CODE_205 = 205;
        public static final int ERROR_CODE_206 = 206;
        public static final int ERROR_CODE_207 = 207;
        public static final int ERROR_CODE_208 = 208;
        public static final int ERROR_CODE_209 = 209;
        public static final int ERROR_CODE_210 = 210;
        public static final int ERROR_CODE_211 = 211;
        public static final int ERROR_CODE_212 = 212;
        public static final int ERROR_CODE_213 = 213;
        public static final int ERROR_CODE_214 = 214;
        public static final int ERROR_CODE_215 = 215;
        public static final int ERROR_CODE_216 = 216;
        public static final int ERROR_CODE_217 = 217;
        public static final int ERROR_CODE_218 = 218;
        public static final int ERROR_CODE_219 = 219;
        public static final int ERROR_CODE_220 = 220;
        public static final int ERROR_CODE_221 = 221;
        public static final int ERROR_CODE_222 = 222;
        public static final int ERROR_CODE_223 = 223;
        public static final int ERROR_CODE_224 = 224;
        public static final int ERROR_CODE_225 = 225;
        public static final int ERROR_CODE_226 = 226;
        public static final int ERROR_CODE_227 = 227;
        public static final int ERROR_CODE_230 = 230;
        public static final int ERROR_CODE_250 = 250;
        public static final int ERROR_CODE_297 = 297;
        public static final int ERROR_CODE_299 = 299;
        public static final int ERROR_CODE_301 = 301;
        public static final int ERROR_CODE_302 = 302;
        public static final int ERROR_CODE_303 = 303;
        public static final int ERROR_CODE_304 = 304;
        public static final int ERROR_CODE_305 = 305;
        public static final int ERROR_CODE_1004 = 1004;
        
        public static final int SPONSORNAME_MAX_LENGTH = 60;
        
        public static final String BATCH_DATE_FORMAT = "yyyyMMddHHmmss";
        public static final String DEFAULT_BATCH_START_DATE = "19000101010000";
        
        public static final String FINANCE_DATE_FORMAT = "dd-MMM-yyyy HH.mm.ss.S";
        
        public static final String RATING_AREA_PREFIX = "R-";
                
        public static final String GROUP_ENROLLMENT_STATUS_N = "N";
        public static final String GROUP_ENROLLMENT_STATUS_Y = "Y";
        public static final String GROUP_ENROLLMENT_BATCH_JOB="enrollmentXMLGroupJob";
        public static final String GROUP_EMPLOYER_STATUS_PAID="PAID";
        public static final String GROUP_EMPLOYER_STATUS_PARTIALLY_PAID="PARTIALLY_PAID";
        public static final String GROUP_EMPLOYER_STATUS_CANCEL="CANCEL";
        
        public static final String GROUP_ACTION_TYPE="EnrollmentAction";
        public static final String GROUP_ACTION_TYPE_TERM="Terminate";
        public static final String GROUP_ACTION_TYPE_INITIAL="OpenEnrollment";
        public static final String GROUP_ACTION_TYPE_MENT="Maintenance";
        public static final String GROUP_ACTION_TYPE_DEL="Deprecated";
        
        public static final String WIP_FOLDER_NAME="WIP";
        public static final String SUCCESS_FOLDER_NAME="SUCCESS";
        public static final String FAILURE_FOLDER_NAME="FAILURE";
        public static final String FILE_TYPE_XML=".xml";
        public static final String FILE_TYPE_OUT=".OUT";
        public static final String FILE_TYPE_IN=".IN";
        public static final String OUTBOUND_FOLDER_NAME="OUTBOUND";
        public static final String INBOUND_FOLDER_NAME="INBOUND";
        
        public static final String OUT_FOLDER_NAME="out";
        public static final String IN_FOLDER_NAME="in";
        
        public static final String RELATIONSHIP_SELF_LABEL = "Self";
        public static final String RELATIONSHIP_SELF_CODE = "18";
        public static final String RELATIONSHIP_CHILD_CODE = "19";
        public static final String RELATIONSHIP_SPOUSE_CODE = "01";
        public static final String OTHER_RELATIONSHIP_LKP_VALUE = "Other Relationship";
        
        public static final String EVENT_REASON_LOOKUP_NO_REASON = "AI";
        public static final String EVENT_REASON_LOOKUP_APTC_CH = "APTC_CH";
        
        public static final String RESEND_FLAG = "RESEND";
        public static final String HOLD_FLAG = "HOLD";
        
        public static final Integer DEFAULT_THREAD_WAIT=24;
        public static final Integer BUFFER_SIZE=1024;
        public static final String EDI_834_RECONSHOP = "834_RECONSHOP";
        public static final String EDI_834_RECONINDIV = "834_RECONINDV";
        
        public static final String BATCHCATEGORYCODE_INDIVIDUAL = "IRS_EOM_IND_REQ";
        public static final String BATCHCATEGORYCODE_SHOP = "IRS_EOM_SHOP_REQ";
        public static final String REPORT_PERIOD_IRS_EOM = "2014-12";
        public static final String REPORT_PERIOD_IRS_EOY = "2014";
        public static final int DOCUMENT_SEQ_ID = 00001;
    	public static final String SOURCE_ID = "IND";
		public static final String FUNC_INBOUND = "EOMIN";
		public static final String PLR_FUNC_INBOUND="SDBI";
		public static final String FUNC_OUTBOUND = "EOMOUT";
		public static final String TRANSFER_DIRECTION_OUT = "OUT";
		public static final String TRANSFER_DIRECTION_IN = "IN";
		public static final String CMS="CMS";
		public static final String APP = "DSH";
		public static final String PLR_APP = "MID";
		public static final String CMS_FuncCode="IS834";
		
		public static final Integer ZERO = 0;
		public static final int ONE = 1;
		public static final int TWO = 2;
		public static final int THREE = 3;
		public static final int FOUR = 4;
		public static final int FIVE = 5;
		public static final int SIX = 6;
		public static final int SEVEN = 7;
		public static final int EIGHT = 8;
		public static final int NINE = 9;
		public static final int TEN = 10;
		public static final int ELEVEN = 11;
		public static final int TWELVE = 12;
		public static final int THIRTEEN = 13;
		public static final int FOURTEEN = 14;
		public static final int FIFTEEN = 15;
		public static final int SIXTEEN = 16;
		public static final int SEVENTEEN = 17;
		public static final int EIGHTEEN = 18;
		public static final int NINETEEN = 19;
		public static final int TWENTY = 20;
		public static final int TWENTY_ONE = 21;
		public static final int TWENTY_TWO = 22;
		public static final int TWENTY_THREE = 23;
		public static final int TWENTY_FOUR = 24;
		public static final int TWENTY_FIVE = 25;
		public static final int TWENTY_SIX = 26;
		public static final int TWENTY_SEVEN = 27;
		public static final int TWENTY_EIGHT = 28;
		public static final int TWENTY_NINE = 29;
		public static final int SEVENTY = 70;
		public static final int HUNDRED = 100;
		public static final int TWO_NINETY_NINE = 299;
		public static final int TWO_THOUSAND = 2000;	
		public static final int THREE_SIXTY_FIVE = 365;
		
		public static final int FIFTY_NINE = 59;
		public static final int SIXTY = 60;
		public static final int NINE_NINETY_NINE =999;
		public static final int THIRTY = 30;
		public static final int THIRTY_ONE =31;
		public static final int ONE_THOUSAND =1000;
		public static final int THIRTY_TWO =32;
		public static final int THIRTY_THREE =33;
		public static final int THIRTY_FOUR =34;
		public static final int THIRTY_FIVE =35;
		public static final int THIRTY_SIX =36;
		public static final int THIRTY_SEVEN =37;
		public static final int THIRTY_EIGHT =38;
		public static final int THIRTY_NINE =39;
		public static final int FORTY =40;
		public static final int FORTY_ONE =41;
		
		public static final String ERR_MSG_NO_SRC_EXCHG_DIRECTORY = "Source Directory defined in ExchangePartner Lookup Table doesn't Exists for Issuer ";
		public static final String ERR_MSG_EXCHG_PARTNER_DOESNT_EXIST = "ExchangePartner Lookup Table does not have record for issuer ";
		public static final String ERR_MSG_SRC_DIR_NOT_DEFINED = "Source Directory is not defined in ExchangePartner Lookup Table ";
		public static final String ERR_MSG_ISSUER_ID_IS_NULL="Invalid input: IssuerId is Null";
		
		public static final String ENROLLMENT_EVENT_STATUS_FAILED = "Failed";
		public static final String EXCHANGE_SUBSCRIBER_IDENTIFIER="Exchange Subscriber Identifier = ";
		
		public static final String ACTION_ADD="Add";
		public static final String ACTION_REMOVE="Remove";
		
		public static final String INBOUND_SUBSCRIBER_CANCEL = "Subscriber Cancel";
		public static final String INBOUND_SUBSCRIBER_TERM = "Subscriber TERM";
		

		public static final String TERMINATION_REASON_TERMINATION_OF_BENIFITS ="07";
		public static final String IS_ENROLLMENT_DISENROLLED = "isEnrollmentDisEnrolled";
		public static final String EVENT_REASON_PLAN_CHANGE ="22";
		
		public static final String RENEWAL_FLAG_REN ="REN";
		public static final String RENEWAL_FLAG_RENP ="RENP";
		public static final String DECEMBER_END="3112";
		
		public static final String MODULE_NAME_PD= "PD";
        /*@Value("#{combinedConfig.getString('global.StateCode')}")
        public void setStateCode(String stateCode){
                STATE_CODE = stateCode;
        }*/
        public static final String NO_COVERAGE_YEAR_REQUEST_FOUND = "Coverage year parameter is either empty or null in request";
        public static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
        public static final String DATETIME_FORMAT_MM_DD_YYYY = "MM/dd/yyyy hh:mm:ss.S";
        public static final String TIME_FORMAT_HH_A = "hh a";
        public static final String ENCODING_UTF_8 = "UTF-8";
        
        public static final String APP_ENROLLMENT_EVENT = "APPEVENT_ENROLLMENT";
        public static final String INSURANCE_TYPE_HEALTH_CODE = "HLT";
        public static final String INSURANCE_TYPE_DENTAL_CODE = "DEN";
        public static final String RENEWAL_FLAG_A="A";
        public static final String RENEWAL_FLAG_M="M";
        public static final String APPLICATION_TYPE_ER = "ER";
        public static final String APPLICATION_TYPE_EN = "EN";
        public static final String APPLICAITON_TYPE_PN = "PN";
        
        public static final String NEW_ENROLLMENT_REASON_SUB_CHANGE="SUBSCRIBER_CHANGE";
        
      //IRS Annual
        public static final String FUNC_ANNUAL_INBOUND = "EOYIN";
		public static final String FUNC_ANNUAL_OUTBOUND = "EOYOUT";
		public static final String IRS_ANNUAL_BATCHCATEGORYCODE_INITIAL = "IRS_EOY_REQ";
		public static final String IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ = "IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ";
		public static final String IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ = "IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ";
		public static final String IRS_EOY_INITIAL_VOID_RECORD  = "INITIAL_VOID_RECORD";

		
		public static final String DISENROLLMENT_REASON_14="14"; 
		public static final String DISENROLLMENT_REASON_AA="AA"; 
		public static final String DISENROLLMENT_REASON_AB  ="AB";
		public static final String DISENROLLMENT_REASON_AD  ="AD";
		public static final String DISENROLLMENT_REASON_AE  ="AE";
		public static final String DISENROLLMENT_REASON_AF="AF"; 
		public static final String DISENROLLMENT_REASON_AG="AG";
		public static final String PDF1095_ECM_FOLDERNAME = "1095_PDF";
		public static final Integer HOUSEHOLD_CASE_ID_SPLIT_LIST_COUNT = 400;
		
		//1095A MSG Subject and PDF File Type
		
		public static final String INTIAL_1095A_PDF = "Initial";
		public static final String CORRECTION_1095A_PDF  = "Correction";
		public static final String VOID_1095A_PDF= "Void";
		
		public static final String SUBJECT_1095A_FORM = "1095A Form​ for ";
		public static final String SUBJECT_1095A_CORRECTED = "Corrected 1095A Form​ for ";
		public static final String SUBJECT_1095A_INVALID = "Voided 1095A Form for ";
		public static final String SUBJECT_PREFIX_RESENT = "Resent ";
		
		public static final String ERR_MSG_DECERTIFIED_PLAN_ID_NULL_OR_EMPTY = "Invalid input: DECERTIFIED_PLAN_ID is null  or empty";
		public static final String ERR_MSG_COVERAGE_YEAR_NULL_OR_EMPTY = "Invalid input: COVERAGE_YEAR is null  or empty";
		public static final String ERR_MSG_DECERTIFIED_DATE_NULL_OR_EMPTY = "Invalid input: DECERTIFIED_DATE is null  or empty";		
		public static final String ERR_MSG_NO_ENROLLMENT_FOUND_FOR_THE_GIVEN_PLAN_ID = "No enrollment found for the given plan Id, coverage date and termination date ";
		
		public static final String CMSPLAN_ID = "cmsPlanId";
		public static final String COVERAGE_YEAR = "coverageYear";
		public static final String TERM_DATE = "terminationDate";
		
		public static final String SUCCESS = "SUCCESS";
		public static final String FAILURE = "FAILURE";
		
		public static final class Enrollment_1095_PDF_BatchEmail{
			public static final String GENERATED_DATE = "generatedDate";
			public static final String INTIAL_PDF_COUNT = "intialPdfCount";
			public static final String CORRECTED_PDF_COUNT = "correctedPdfCount";
			public static final String VOID_PDF_COUNT = "voidPdfCount";
			public static final String RESEND_PDF = "resendPdf";
			public static final String FAILED_RECORD_COUNT = "failedRecordCount";
			public static final String JOB_NAME = "jobName";
		}
		
		public static final class Enrollment_1095_Staging_BatchEmail{
			public static final String GENERATED_DATE = "generatedDate";
			public static final String TOTAL_RECORD_COUNT = "totalRecords";
			public static final String UPDATED_RECORD_COUNT = "totalUpdatedRecords";
			public static final String OVERWRITTEN_RECORD_COUNT = "totalOverwrittenRecords";
			public static final String FAILED_RECORD_COUNT = "totalFailedRecords";
			public static final String JOB_NAME = "jobName";
			public static final String SKIPPED_FILE_LOCATION = "skippedFileLocation";
		}
		
		public static final class Enrollment_1095_CMS_XML_BatchEmail{
			public static final String GENERATED_DATE = "generatedDate";
			public static final String JOB_NAME = "jobName";
			public static final String INTIAL_COMPLETED_SUBMISSION_COUNT = "intialCompletedSubmissionCount";
			public static final String RESUBMIT_COMPLETED_SUBMISSION_COUNT = "resubmitCompletedSubmissionCount";
			public static final String INTIAL_FAILED_SUBMISSION_COUNT = "intialFailedSubmissionCount";
			public static final String RESUBMIT_FAILED_SUBMISSION_COUNT = "resubmitFailedSubmissionCount";
			public static final String FILE_SIZE = "fileSize";
			public static final String NUMBER_OF_FILES = "numberOfFiles";
		}
		
		public static final class Enrollment_1095_IN_BatchEmail{
			public static final String GENERATED_DATE = "generatedDate";
			public static final String IN_FILE_NAME_ARRAY = "inFileNameArray";
			public static final String IN_FILE_PROCESS_DATA = "inFileProcessData";
			public static final String JOB_NAME = "jobName";
			public static final String IN_FILE_NAME = "inFileName";
		}
		
		public static final class Enrollment_Monthly_IRS_IN_BatchEmail{
			public static final String GENERATED_DATE = "generatedDate";
			public static final String IN_FILE_NAME_ARRAY = "inFileNameArray";
			public static final String IN_FILE_PROCESS_DATA = "inFileProcessData";
			public static final String JOB_NAME = "jobName";
			public static final String IN_FILE_NAME = "inFileName";
		}
		
		public static final String NOTICE_SEQ_NAME="NOTICES_SEQ";
		public static final String DECIMAL_POINT_TWO="%.2f";
		public static final String ENROLLMENT_1095_OUT_PDF_JOB="annualIRS1095IndivPartitionedJob";
		public static final String ENROLLMENT_PLR_OUT_JOB="monthlyPLROutJob";
		public static final String ENROLLMENT_MONTHLY_OUT_JOB="monthlyIRSOutJob";
		public static final String INITIAL_1095_OUT_FORMAT="1095A_Form_Initial_";
		public static final String CORRECTION_1095_OUT_FORMAT="1095A_Form_Correction_";
		public static final String VOID_1095_OUT_FORMAT="1095A_Form_Void_";
		/*public static final String RESEND_1095_OUT_FORMAT="1095A_Form_Resend_";*/
		
		public static final String ENROLLMENT_1095_STAGING_JOB = "populate1095StagingJob";
		public static final String ENROLLMENT_1095_XML_JOB = "enrollment1095XmlPartitionedJob";
		public static final String ENROLLMENT_1095_INBOUND_PROCESS_JOB = "Enrollment1095AInboundProcessJob";
		public static final String ENROLLMENT_IRS_MONTHLY_INBOUND_PROCESS_JOB = "enrollmentIRSMonthlyInboundJob";
		
		public static final String SSN_PREFIX_MASKING_TEXT = "***-**-";
		
		public static final String CSVS_SPLITE_BY = ",";
		public static final String FILE_TYPE_CSV=".csv";
		public static final int Issuer_HIOS_ID = 0;
		public static final int Exchange_Assigned_Policy_ID = 1;
		public static final int Exchange_Assigned_Invidivual_ID = 2;
		public static final int Subscriber_Flag = 3;
		public static final int Issuer_Assigned_Policy_ID = 4;
		public static final int Issuer_Assigned_Invidivual_ID = 5;
		public static final int Last_Premium_Paid_Date = 6;
		public static final int Status = 7;
		public static final int Coverage_End_Date = 8;
		
		
		public static final String CSV_RECORD_WRONG_SUBSCRIBER_STATUS="CSV Record With Other Than Confirme Status Receive ";
		public static final String ERR_MSG_EVENT_TYPE_IS_NULL_OR_EMPTY="Event Type is either null or empty";
		public static final String ERR_MSG_RESEND_834_COMMENT_IS_NULL_OR_EMPTY = "Resend 834 provided comment is null or empty"; 
		
		public static final String ENROLLMENT_ENVIRONMENT_PRODUCTION="P";
		public static final String ENROLLMENT_ENVIRONMENT_TEST="T";
		public static final String ENROLLMENT_ENVIRONMENT_MAIL_TEMPLATE_PROD="PROD";
		public static final String ENROLLMENT_ENVIRONMENT_MAIL_TEMPLATE_QA="QA";
		
		/** Enrollment Montly PLR **/
		public static final String MONTHLY_PLR_SCHEMA_FILENAME = "SBMPolicyLevelEnrollment -1.0.xsd";
		public static final String MONTHLY_PLR_SCHEMA_BASE_DIRECORY = "IRS_Schema/PLR_Monthly/";
		/** Enrollment Annual IRS 1095 XML **/
		public static final String ENROLLMENT_1095_XML_SCHEMA_FILENAME = "IRS-Form1095ATransmissionUpstreamMessage.xsd";
		public static final String ENROLLMENT_1095_XML_SCHEMA_BASE_DIRECTORY = "IRS_Schema/1095_Annual/";
		public static final String ENROLLMENT_1095_XML_VALIDATION_LOGS_FOLDER = "IRS1095XMLValidationLogs";
		
		/** Enrollment Monthly IRS **/
		public static final String ENROLLMENT_MONTHLY_XML_SCHEMA_FILENAME = "HHS-IRS-MonthlyExchangePeriodicDataMessage-1.0.xsd";
		public static final String ENROLLMENT_MONTHLY_IRS_SCHEMA_BASE_DIRECTORY =  "IRS_Schema/IRS_Monthly/";
		
		/** Enrollment Monthly CMS XML **/
		public static final String CMS_OUT_SCHEMA_BASE_DIRECORY ="CMS";
		public static final String CMS_OUT_SCHEMA_FILENAME="SBMI.xsd";
		
		/**Reporting folders **/
		public static final String OUT_FOLDER="Outbound";
		public static final String IN_FOLDER="Inbound";
		public static final String VALID_FOLDER = "valid";
		public static final String INVALID_FOLDER = "invalid";
		public static final String ERROR_FOLDER = "error";
		public static final String ARCHIVE_FOLDER = "archive";
		public static final String FAILURE_FOLDER = "failure";
		public static final String MANIFEST_FOLDER="manifest";
		public static final String ZIP_FOLDER= "zip";
		public static final String XML_VALIDATION_LOG_FOLDER = "XmlValidationLog";
		
		/* Update Location by HouseHold case id */
		public static final String EVENT_REASON_CHANGE_OF_LOCATION = "43";
		
		public static final String ERR_MSG_INVALID_LOCATION_ID = "Invalid input: LOCATION_ID is Invalid LOCATION or one of component is NULL";
	    public static final String ERR_MSG_LOCATION_ID_IS_NULL = "Invalid input: LOCATION_ID is Null";
		
		public static final String MSG_MAILING_ADD_UPDATE_SUCCESS = "Member Mailing Address updated successfully";
	        
		public static final String HOUSEHOLD_CASE_ID_KEY="household_case_id";
		public static final String LOCATION_ID="location_id";
		public static final String ADDRESS1="address1";
		public static final String ADDRESS2="address2";
		public static final String CITY="city";
		public static final String STATE="state";
		public static final String COUNTY="county";
		public static final String COUNTYCODE="countycode";
		public static final String ZIP="zip";
		public static final String RDI="rdi";
		public static final String LAT="lat";
		public static final String LON="lon";
		
		public static final String ERR_MSG_ADDRESS1_IS_NULL = "Invalid input: ADDRESS1 is Null";
		public static final String ERR_MSG_ADDRESS2_IS_NULL = "Invalid input: ADDRESS2 is Null";
		public static final String ERR_MSG_CITY_IS_NULL = "Invalid input: CITY is Null";
		public static final String ERR_MSG_STATE_IS_NULL = "Invalid input: STATE is Null";
		public static final String ERR_MSG_COUNTY_IS_NULL = "Invalid input: COUNTY is Null";
		public static final String ERR_MSG_ZIP_IS_NULL = "Invalid input: ZIP is Null";
		
		public static final String IRS_MONTHLY_CATEGORY_CODE_LOOKUP_TYPE ="IRS_MONTHLY_CATEGORY_CODE";
		
		public static final String ERR_MSG_ACTION_IS_NULL = "Action is null or empty";
		public static final String ERR_MSG_ENROLLMENT_ID_NULL = "Enrollment Id is null or empty";
		public static final String ERR_MSG_ENROLLMENT_NOT_FOUND = "No enrollment Found for given Id ";
		public static final String ERR_MSG_NO_MEMBER_FOUND = "No member found for given Exchange Individual Identifier ";
		public static final String ERR_MSG_EFF_END_NULL = "Effective End Date provided for Enrollment End  Date change is null";
		public static final String ERR_MSG_NOT_SAME_YEAR = "Effective Start Date and Effective end date do not fall in same year";
		public static final String ERR_MSG_FINANCIAL_DATE="Financial Dates exists outside given Coverage Period";
		public static final String ERR_MSG_ORPHAN_MEMBER="This change will lead to Orphan member outside the coverage for member ";
		public static final String ERR_MSG_MEMBER_OUT_COV="Date falls outside enrollment coverage for member ";
		public static final String ERR_MSG_MEMBER_BDATE="Birth date is after the coverage date for member ";
		public static final String ERR_MSG_MEMBER_DDATE="Death date is before coverage end date for member ";
		public static final String ERR_MSG_MEMBER_EFFDATE="Benefit end date is smaller than benefit effective date for member ";
		public static final String ERR_MSG_ENRL_CANCEL="Enrollment Effective Date can not be edited for Cancelled enrollment";
		public static final String ERR_MSG_EFFDATE_NULL="Effective Date provided for Enrollment Effective Date change is null";
		public static final String ERR_MSG_ENRLMNT_OVRLAP="Enrollment overlaps. Already an enrollment exists during given date";
		public static final String ERR_MSG_EFF_DATE_LARGE="Benefit end date is smaller than benefit effective date for the enrollment";
		public static final String ERR_MSG_DATE_NULL="Date provided for Member Date change is null";
		public static final String ERR_MSG_DATE_OUT_COV="Date falls outside enrollment coverage ";
		public static final String ERR_MSG_DATE_APTC_MID_MONTH="Mid month Effective Date for Non Mid month Coverage ";
		public static final String ERR_MSG_ENRL_CANCEL_EDIT="Technical Error Occurred. To edit this Enrollment, please reinstate through the application.";
		public static final String ERR_MSG_INVALID_STATUS_CHANGE="Invalid Status Change Request.";
		public static final String ERR_MSG_PENDING_STATUS_DATE="For Pending Status end date should be year end date.";
		public static final String ERR_MSG_CONFIRMATION_DATE="Confirmation Date is required.";
		public static final String ERR_MSG_UNABLE_TO_PROCESS = "Unable to process the given request.";
		public static final String ERR_MSG_USER_MISSING_IN_REQUEST = "Logged-In user info is missing in request";
		public static final String ERR_MSG_CARRIER_USER_NOT_ALLOWED = "Carrier user is not allowed for this request";
		public static final String ERR_MSG_NO_USER_FOUND = "No user found in system with Email: ";
		public static final String ERR_MSG_DATE_GROSS_CHANGE="Can not change Financial Effective Date while changing Gross Premium Amount";
		public static final String ERR_MSG_DATE_FORMAT="Invalid date format received. Required format MM/dd/yyyy.";
		public static final String ERR_MSG_MISSING_SUBSCRIBER="Subscriber missing in the send as ADD request";
		public static final String ERR_MSG_MISSING_EFFECTIVE_DATE="Benefit effective date missing in the send as ADD request";
		public static final String ERR_MSG_APTC_HIGH="Please select an APTC Amount lower than the gross premium of $";
		public static final String ERR_MSG_NULL_EFFDATE="Effective Date provided for Enrollment is null";
		public static final String ERR_MSG_SSAP_UPDATE="Error in updating SSAP application ID.";
		public static final String ERR_MSG_ENROLLMENTS_SKIPPED="All enrollments skipped but new ssapApplicationId updated. Please check the enrollment skipped map.";
		public static final String ERR_MSG_EFFDATE_BEFORE_SUB_DOB = "Effective Start Date of enrollment is before Subscriber date of birth";

		
		public static final String LOG_GLOBAL_ID = "logGlobalId";
		public static final String GLOBAL_ID = "globalId";
		public static final String CSR_AMOUNT = "csrAmount";
		public static final String APTC_EFF_DATE = "aptcEffDate";
		public static final String GROSS_PREM_EFF_DATE = "grossPremEffDate";
		public static final String DIS_ENROLLED_ENROLLMENT_ID = "disEnrolledEnrollmentId";
		public static final String DIS_ENROLLED_MEMBER_DETAILS = "disEnrolledMemberDetails";
		
		public static final String GENDER_UNKNOWN="U";
		/*
		 * Enrollment 834 Reconciliation
		 */
		public static final String GOOD_EDI = "good_edi_tracking";
		public static final String BAD_EDI = "bad_edi_tracking";
		public static final String FILETYPE_EDI = "EDI";
		public static final String FILETYPE_TA1_999 = "TA1_999_TRACKING";
		public static final String FILETYPE_TA1_TRACKING = "TA1_tracking";
		public static final String FILETYPE_999_TRACKING = "999_tracking";
		public static final String CSV_DELIMITER = "\\*";
		public static final String FINANCIAL_DATE="financialEffectiveDate";
		
		//Enrollment Inbound 834 Reject Error Message
		public static final String MSG_REJECT_MAINT_REASON_CODE = "Rejected due to Incorrect Maintenance Reason Code: "; 
		public static final String MSG_REJECT_INCORRECT_START_DATE = "Rejected due to Incorrect Start Date by Carrier";
		public static final String MSG_REJECT_INCORRECT_END_DATE = "Rejected due to Incorrect End Date or Status by Carrier";
		public static final String MSG_REJECT_CANCEL_STATUS_PRESENT = "Rejected due to CANCEL Status in Exchange";
		public static final String MSG_REJECT_LOWER_END_DATE_PRESENT = "Rejected due to Lower End Date in Exchange"; 
		public static final String MSG_REJECT_ENROLLMENT_CONFIRMED_PRESENT = "Rejected due to Enrollment already Confirmed in Exchange"; 
		public static final String MSG_REJECT_NO_ACTIVE_ENROLLMENT = "Rejected due to No Active Enrollment in Exchange";
		public static final String MSG_REJECT_INCORRECT_COVERAGE_END_YEAR = "Rejected due to Incorrect Coverage Year in End Date by Carrier";
		public static final String MSG_REJECT_INCORRECT_COVERAGE_START_YEAR = "Rejected due to Incorrect Coverage Year in Start Date by Carrier";
		public static final String MSG_REJECT_UNSUPPORTED_STATUS = "Rejected due to Unsupported Status by Carrier";
		public static final String MSG_REJECT_NO_ACTIVE_ENROLLEE = "Rejected due to No Active Enrollee in Exchange";
		public static final String MSG_REJECT_ENROLLEE_CONFIRM_PRESENT = "Rejected due to Enrollee already Confirmed in Exchange";
		public static final String DATE_FORMAT_YYYY_MM_DD="yyyy-MM-dd";
		
		public static final String OTHER_RELATION_CODE="G8";
		public static final String OTHER="Other";
		
		public static final String ASSIGNED_POLICY_NUMBER_NULL = "ASSIGNED_POLICY_NUMBER_NULL";
		public static final String CATASTROPHIC_ENROLLMENT_RECORD = "CATASTROPHIC_ENROLLMENT_RECORD";
		public static final String COVERAGE_AND_PREMIUM_MONTH_MISMATCH = "COVERAGE_AND_PREMIUM_MONTH_MISMATCH";
		public static final String COVERAGE_START_BEFORE_BIRTH_DATE = "COVERAGE_START_BEFORE_BIRTH_DATE";
		public static final String COVERAGE_START_GREATER_THAN_END_DATE = "COVERAGE_START_GREATER_THAN_END_DATE";
		public static final String COVERAGE_YEAR_MISMATCH = "COVERAGE_YEAR_MISMATCH";
		public static final String DENTAL_ENROLLMENT = "DENTAL_ENROLLMENT";
		public static final String ENROLLMENT_1095_OVERLAPPING_COVERAGE = "ENROLLMENT_1095_OVERLAPPING_COVERAGE";
		public static final String INVALID_ADDRESS_FIELDS = "INVALID_ADDRESS_FIELDS";
		public static final String INVALID_EHB_SLCSP_AMT = "INVALID_EHB_SLCSP_AMT";
		public static final String INVALID_MEMBER_COVERAGE_DATE = "INVALID_MEMBER_COVERAGE_DATE";
		public static final String INVALID_POLICY_DATES = "INVALID_POLICY_DATES";
		public static final String MEMBER_COVERAGE_START_GREATER_THAN_END_DATE = "MEMBER_COVERAGE_START_GREATER_THAN_END_DATE";
		public static final String MEMBER_LEVEL_OVERLAPPING = "MEMBER_LEVEL_OVERLAPPING";
		public static final String MISSING_RECEIPIENT_RECORD = "MISSING_RECEIPIENT_RECORD";
		public static final String NEGATIVE_APTC = "NEGATIVE_APTC";
		public static final String NON_VOID_CASE_SAME_START_AND_END_DATE = "NON_VOID_CASE_SAME_START_AND_END_DATE";
		public static final String NO_ACTIVE_MEMBERS = "NO_ACTIVE_MEMBERS";
		public static final String NO_PREMIUM_MONTH_PRESENT = "NO_PREMIUM_MONTH_PRESENT";
		public static final String NULL_BIRTH_DATE = "NULL_BIRTH_DATE";
		public static final String POLICY_ISSUER_NAME_NULL = "POLICY_ISSUER_NAME_NULL";
		public static final String SSN_LENGTH_INVALID = "SSN_LENGTH_INVALID";
		public static final String SSN_VALUE_INVALID = "SSN_VALUE_INVALID";
		public static final String VOID_CORRECTED_CHECKBOX_SET = "VOID_CORRECTED_CHECKBOX_SET";
		public static final String ZERO_GROSS_PREMIUM = "ZERO_GROSS_PREMIUM";
		public static final String HOUSEHOLD_MISMATCH = "HOUSEHOLD_MISMATCH";
		
		public static final String FILL_ONLY_SLCSP_ALL="All";
		public static final String FILL_ONLY_SLCSP_MISSING_MONTHS="MissingMonths";
		public static final String BATCH_JOB_STATUS_STOPPING = "STOPPING";
		public static final String BATCH_JOB_STATUS_STOPPED = "STOPPED";
		public static final String BATCH_STOP_MSG="Manually requested to Stop Job, so throwing non skippable exception to stop immediately";

		public static final String QLE_INDICATOR_QEP="QEP";
		public static final String QLE_REASON_QLE="90-Qualifying Life Event";
		public static final String QLE_REASON_PLAN_CHANGE="91-Plan Change";
		public static final String QLE_REASON_SUB_CHANGE="92-Subscriber Change";
		public static final String QLE_REASON_CS_CHANGE="93-CS Level Change";
		
		public static final String INVALID_DOB="Invalid Date Of Birth ";
		public static final String INVALID_EHB_PERCENT="Invalid EHB Percentage ";
		public static final String INVALID_SLCSP_AMOUNT="Invalid SLCSP Amount ";
        public static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
        public static final String DATE_FORMAT_YYYY="yyyy";
        
        public static final String INSURANCE_TYPE_MISMATCH="Insurance type did not match ";
        
        public static final String RECON_TEST_DATA_FOLDER = "TestDataFiles";
        public static final String RECON_SUMMARY_STATUS_SNAPSHOP_GEN="SNAPSHOT_GENERATED";
        public static final String RECON_DUPLICATE_FOLDER = "duplicate";
        
        public static final String NO_SUBSCRIBER_FROM_ISSUER="No Subscriber Found in data provided by Issuer";
        public static final String RECON_DISCREPANCY_8200_AA="8200_AA";
        
		public static final int ENROLLMENT_STATUS_CSV = 0;
		public static final int EFFECTIVE_START_DATE_CSV = 1;
		public static final int EFFECTIVE_END_DATE_CSV = 2;
		public static final int APTC_CSV = 3;
		public static final int ENROLLMENT_ID_PHIX = 4;
		public static final int CAP_AGENT_NPN = 5;
		public static final int EMAIL_ADDRESS = 6;
		public static final int INSURER_NAME = 7;
        public static final String EXTERNAL_ASSISTER="BROKER";
		public static final String EXTERNAL_ENROLLMENT= "EXTERNAL_ENROLLMENT";
		public static final String EXTERNAL_FAILURE_NOTICES= "EXTERNAL_FAILURE_NOTICES";
		public static final String EMPTY_OR_ZERO_SLCSP= "Empty or 0.0 SLCSP amount been received in the request for ssapApplicationId ";

        
        public static final String TERM_FLAG="termFlag";
		public static final class GiErrorCode{
			public static final String IRS = "ENRL-IRS";
			public static final String ANNUAL = "ENRL-ANNUAL";
			public static final String PLR = "ENRL-PLR";
			public static final String CMS = "ENRL-CMS";
			public static final String PREM_POPULATION = "ENRL-PREM_POPULATION";
			public static final String SFTP = "ENRL-SFTP";
			public static final String RECONCILIATION = "ENRL-RECONCILIATION";
			public static final String FINANCIALEFFDATE="ENRL-FIN-EFF-DATE";
			public static final String BOB_TRANSFER_ERROR_CODE = "ENRL-BOB-TRANSFER";
			public static final String INVALID_SLCSP_AMOUNT = "INVALID-SLCSP-AMT";
		}
		
		public static final int MISSING_COVERAGE_YEAR = 0;
		public static final int MISSING_COUNT = 1;
		
		public static final class ReconErrorCode{
			public static final String SSN_ERRORCODE = "2100A_AE";
			public static final String[] MEMBER_NAME_ERRORCODE = {"2100A_AA", "2100A_AB", "2100A_AC"};
			public static final String[] MEMBER_MAILING_ADDRESS_ERRORCODE = {"2100C_AA", "2100C_AB", "2100C_AC", "2100C_AD", "2100C_AE", "8100_AA"};
			public static final String[] MEMBER_RESIDENTIAL_ADDRESS_ERRORCODE = {"2100A_AI", "2100A_AJ", "2100A_AK", "2100A_AL", "2100A_AM", "2100A_AN"};
			public static final String[] APTC_ERROR_FIELDNAME = {"overallocationAptcAmt_", "aptcAmt_"};
			public static final String[] CSR_AMOUNT_ERROR_FIELDNAME = {"csrAmt_"};
			public static final String[] GROSS_PREMIUM_ERROR_FIELDNAME = {"grossPremium_"};
			public static final String[] RATINGAREA_ERROR_FIELDNAME = {"ratingArea_"};
			public static final String[] MEMBERCOUNT_ERROR_CODE = {"8000_AA", "8000_AB"};
			public static final String[] MEMBER_BENEFIT_BEGIN_DATE_ERRORCODE = {"2300_AB"};
			public static final String[] MEMBER_BENEFIT_END_DATE_ERRORCODE = {"2300_AC"};
			public static final String[] MEMBER_RELATIONSHIP_ERRORCODE = {"2000A_AC"};
			public static final String[] MEMBER_SSN_ERRORCODE = {SSN_ERRORCODE};
			public static final String[] MEMBER_GENDER_ERROR_CODE = {"2100A_AP"};
			public static final String[] MEMBER_BIRTHDATE_ERROR_CODE = {"2100A_AO"};
			public static final String[] MEMBER_TOBACCO_USAGE_ERRORCODE = {"2100A_AS"};
			public static final String AGENT_NAME_ERRORCODE = "1000C_AA";
			public static final String AGENT_TAX_PAYER_ERRORCODE = "1000C_AC";
			public static final String EFFECTUATION_STATUS_ERRORCODE = "8200_AA"; 
			public static final String PHONE_NUMBER_ERROR_CODE = "2100A_AF";
			public static final String ENROLLMENT_MISSING_IN_FILE = "8000_AD";
			public static final String ENROLLMENT_MISSING_IN_HIX = "8000_AC";
			public static final String MEMBER_MISSING_IN_FILE = "8000_AB";
			public static final String MEMBER_MISSING_IN_HIX = "8000_AA";
		}
		
		public static enum EnrollmentCmsValidationDeckEnum{
			COVERAGE_AND_PREMIUM_MONTH_MISMATCH("E-0001"),
			CONFIRMATION_DATE_MISSING("E-0002"),
			COVERAGE_YEAR_MISMATCH("E-0003"),
			COVERAGE_START_GREATER_THAN_END_DATE("E-0004"),
			MEMBER_COVERAGE_YEAR_MISMATCH("E-0005"),
			MEMBER_COVERAGE_START_GREATER_THAN_END_DATE("E-0006"),
			MEMBER_COVERAGE_START_BEFORE_BIRTH_DATE("E-0007"),
			NEGATIVE_APTC("E-0008"),
			NEGATIVE_NET_PREMIUM("E-0009"),
			NO_ACTIVE_MEMBERS("E-0010");
			
			private final String value;	 
			public String getValue(){return this.value;}
			
			EnrollmentCmsValidationDeckEnum(String value){
				this.value = value;
			}
		}
		
		 public static enum InsuranceTypeMappingEnum {

			 MC_VISION("MC_VISION", "MC_VISION"),
			 MC_DEN("MC_DEN", "MC_DEN"),
			 MC_SUP("MC_SUP", "MC_SUP"),
			 MC_RX("MC_RX", "MC_RX"),
			 MC_ADV("MC_ADV", "MC_ADV"),
			 HLT("HLT", "HEALTH"),
			 DEN("DEN", "DENTAL"),
			 LIFE("LIFE", "LIFE"),
			 VISION("VISION", "VISION"),
			 AME("AME", "AME"),
			 STM("STM", "STM"),
			 MEDICARE("MEDICARE", "MEDICARE"),
			 LI("LI", "LI");

	         private final String key;
	         private final String value;
	         
	         InsuranceTypeMappingEnum(String key, String value) {
	             this.key = key;
	             this.value = value;
	         }

	         public String getKey() {
	             return key;
	         }
	         public String getValue() {
	             return value;
	         }
	         public static String getValue(String key) {
	         	for(InsuranceTypeMappingEnum en: InsuranceTypeMappingEnum.values()){
	         		if(en.getKey().equalsIgnoreCase(key)){
	         			return en.getValue();
	         		}
	         	}
	             return null;
	         }
	     }
		 
		 public static final Map<String, String> LOCALE_MAP = createLocaleMap();

		 private static Map<String, String> createLocaleMap() {
			 Map<String, String> result = new HashMap<String, String>();
			 result.put("ara", "ar");
			 result.put("cesm", "zh");
			 result.put("cmn", "zh");
			 result.put("eng", "en");
			 result.put("fas", "fa");
			 result.put("hmn", "en"); // Defaulting to en since no two digit available code
			 result.put("hye", "hy");
			 result.put("khmr", "km");
			 result.put("kor", "ko");
			 result.put("rus", "ru");
			 result.put("spa", "es");
			 result.put("tgl", "tl");
			 result.put("vie", "vi");
			 return Collections.unmodifiableMap(result);
		 }
}

