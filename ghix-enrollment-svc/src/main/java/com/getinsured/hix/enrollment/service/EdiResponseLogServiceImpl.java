package com.getinsured.hix.enrollment.service;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.repository.IEdiResponseLogRepository;
import com.getinsured.hix.model.enrollment.EdiResponseLog;

@Service("ediResponseLogService")
public class EdiResponseLogServiceImpl implements EdiResponseLogService {
	
	
	
	@Autowired private IEdiResponseLogRepository ediResponseLogRepository;

	@Override
	public EdiResponseLog addEdiResponseLog(EdiResponseLog ediResponseLog) {
		
		return ediResponseLogRepository.saveAndFlush(ediResponseLog);
	}

	@Override
	public void addAllEdiResponseLog(String jobName, long stepexecutionid,long jobId, String jobStepName , long jobInstanceId ,Map<String, Integer> resultMap) {
		
		String fileName;
		String hiosIssuerId;
		EdiResponseLog ediResponseLog;
	    Iterator<Map.Entry<String, Integer>> it = resultMap.entrySet().iterator();
	    
	    while (it.hasNext()) {
	    	
	        Entry<String, Integer> pair = it.next();
	        
	        fileName = pair.getKey();
	        String[] parts = fileName.split("_");
	        hiosIssuerId = parts[1];
	        
	        ediResponseLog = new EdiResponseLog();
	        
	        ediResponseLog.setHiosIssuerId(hiosIssuerId);
	        ediResponseLog.setJobExecutionId((int)jobId);
	        ediResponseLog.setJobStepName(jobStepName);
	        ediResponseLog.setJobInstanceId((int)jobInstanceId);
	        ediResponseLog.setStepExecutionId((int)stepexecutionid);
	        ediResponseLog.setResponseCode(pair.getValue());
	        
	        ediResponseLogRepository.saveAndFlush(ediResponseLog);
	        
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	}
	
	

}
