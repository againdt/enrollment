/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepository;


/**
 * @author panda_p
 *
 */
@Repository
public interface IEnrolleeRelationshipRepository extends JpaRepository<EnrolleeRelationship, Integer>, EnversRevisionRepository<EnrolleeRelationship, Integer, Integer>{
	@Query(" FROM EnrolleeRelationship as en " +
		   " WHERE en.sourceEnrollee.id = :sourceEnrolleeID " +
		   " AND  en.targetEnrollee.id = :targetEnrolleeID " )
	EnrolleeRelationship getRelationshipBySourceEndTargetId(@Param("sourceEnrolleeID") Integer sourceEnrolleeID, @Param("targetEnrolleeID") Integer targetEnrolleeID);
	
	@Query (" FROM EnrolleeRelationship as en " +
			   " WHERE en.sourceEnrollee.id = :sourceEnrolleeID " +
			   " AND  en.targetEnrollee.id = (SELECT eeIn.id FROM Enrollee eeIn Where eeIn.enrollment.id = :enrollmentId AND eeIn.personTypeLkp.lookupValueCode='SUBSCRIBER') ")
	EnrolleeRelationship getRelationshipWithSubsciber(@Param("sourceEnrolleeID") Integer sourceEnrolleeID, @Param("enrollmentId") Integer enrollmentId);
	
	/**
	 * 
	 * @param sourceEnrolleeId
	 * @return
	 */
	@Query(" FROM EnrolleeRelationship as en " +
			   " WHERE en.sourceEnrollee.id = :sourceEnrolleeId " )
	List<EnrolleeRelationship> getEnrolleeRelationshipDataBySourceEnrollee(@Param("sourceEnrolleeId") Integer sourceEnrolleeId);
	
}
