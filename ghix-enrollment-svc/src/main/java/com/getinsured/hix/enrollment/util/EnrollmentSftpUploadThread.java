/**
 * 
 */
package com.getinsured.hix.enrollment.util;

import java.io.File;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.dto.enrollment.EnrollmentSftpFileTransferDTO;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;

/**
 * SFTP upload thread for multiple file uploads
 * @author negi_s
 * @since 06/10/2016
 *
 */
public class EnrollmentSftpUploadThread implements Callable<EnrollmentSftpFileTransferDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentSftpFileTransferDTO.class);

	private String fileName;
	private String hostLocation;
	private Session session;

	/**
	 * Initialize thread
	 * @param fileName File to upload
	 * @param hostLocation Target location on remote 
	 * @param session JSch session
	 */
	public EnrollmentSftpUploadThread(String fileName, String hostLocation, Session session) {
		super();
		this.fileName = fileName;
		this.hostLocation = hostLocation;
		this.session = session;
	}

	@Override
	/**
	 * Over-ridden thread call method
	 */
	public EnrollmentSftpFileTransferDTO call() throws Exception {
		EnrollmentSftpFileTransferDTO fileTransferDto= new EnrollmentSftpFileTransferDTO(); 
		Channel channel =  null;
		try {
			LOGGER.info(" Thread Name :: "+ Thread.currentThread().getName()+" File Name ::" + fileName);
			channel = session.openChannel("sftp");
			channel.connect();
			//Call Util method to upload file
			fileTransferDto = EnrollmentSftpUtil.uploadFile(fileName, hostLocation, (ChannelSftp)channel);
		} catch (Exception e) {
			fileTransferDto.setFileName(fileName);
			fileTransferDto.setTransferStatus(EnrollmentConstants.FAILURE);
			try{
				File f = new File(fileName);
				fileTransferDto.setFileName(f.getName());
				fileTransferDto.setFileSize(String.valueOf(f.length()));
			}catch(Exception fileException){
				LOGGER.error("Error opening file "+fileName+" :: "+ fileException.getMessage(), fileException);	
			}
			fileTransferDto.setErrorMsg(e.getMessage());
			LOGGER.error("Exception occurred in SftpUploadThread: "+Thread.currentThread().getName()+" File Name ::" + fileName+ "::" + e.getMessage(), e);
		}finally{
			if(null !=  channel){
				channel.disconnect();
				LOGGER.info(" Thread Name :: "+ Thread.currentThread().getName()+" :: Channel disconnected");
			}
		}
		return fileTransferDto;
	}
}
