package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PdOrderResponse;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;

public interface EnrollmentPDHandshakeService {
	PldOrderResponse findByPdHouseholdId( String pdHouseholdId, EnrollmentResponse enrollmentResponse,  PdOrderResponse pdResponse, AutoRenewalRequest autoRenewalReq, String enrollmentType ) throws GIRuntimeException;

}
