package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentCmsOutDtl;

public interface IEnrollmentCmsOutDtlRepository extends JpaRepository<EnrollmentCmsOutDtl, Integer>{

	@Query(" SELECT DISTINCT(ed.enrollmentId) " 
			  + " FROM Enrollment as en, EnrollmentCmsOutDtl as ed "
			  + " WHERE ed.enrollmentId = en.id AND ed.coverageYear= :year "
			  + " AND en.enrollmentConfirmationDate IS NOT NULL"
			  + " AND en.issuerId = :issuerId"
			  + " AND en.enrollmentStatusLkp.lookupValueCode = 'CANCEL'"
			)
	List<Integer> getApplicableCancelledEnrollmentIds(@Param("issuerId") Integer issuerId, @Param("year") Integer year);

}
