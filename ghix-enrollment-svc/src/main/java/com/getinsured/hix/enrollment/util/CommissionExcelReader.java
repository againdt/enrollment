package com.getinsured.hix.enrollment.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
@Component
public class CommissionExcelReader {
	private FileInputStream excelFile = null;
	HSSFWorkbook workbook =null;//
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
	private static final Logger LOGGER = LoggerFactory.getLogger(CommissionExcelReader.class);
	
	public void loadFile(String fileNameGiven) throws IOException {
		// fileName = fileNameGiven;
		excelFile = new FileInputStream(new File(fileNameGiven));
		workbook = new HSSFWorkbook(excelFile);
	}
		public void loadFile(InputStream fileInputStream) throws IOException {
		//workbook = new XSSFWorkbook(fileInputStream);
	}

	public void closeFile() throws IOException {
		excelFile.close();
	}

	public XSSFSheet getSheet(int tab) throws IOException {
		// Get first sheet from the workbook
		return null;// workbook.getSheetAt(tab);
	}

	public XSSFSheet getSheet(String tabName) throws IOException {
		// Get first sheet from the workbook
		return null;//workbook.getSheetAt(workbook.getSheetIndex(tabName));
	}

	public String getCellValueTrim(Cell cell) {
		
		String returnValue = getCellValue(cell);
		
		if (StringUtils.isNotEmpty(returnValue)) {
			return returnValue.trim();
		}
		return returnValue;
	}

	public String getCellValue(Cell cell) {
		return getCellValue(cell, false); 
	}

	public String getCellValue(Cell cell, boolean ignoreDecimal) {
		
		String returnValue = null;
		
		if (null == cell) {
			return returnValue;
		}
		
		if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
			returnValue = getCellNumericValue(cell, ignoreDecimal);
		}
		else if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
			returnValue = cell.getStringCellValue();
		}
		else if (Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
			returnValue = cell.getStringCellValue();
		}
		return returnValue;
	}
	
	private String getCellNumericValue(Cell cell, boolean ignoreDecimal) {
		
		String returnValue = null;
		
		if (null == cell) {
			return returnValue;
		}

		if (DateUtil.isCellDateFormatted(cell)) {
			Date today = cell.getDateCellValue();
			returnValue = DATE_FORMAT.format(today);
		}
		else {
			
			double val = cell.getNumericCellValue();
			String cellFormat = cell.getCellStyle().getDataFormatString();
				
			if(ignoreDecimal) {
				returnValue = StringUtils.EMPTY + getWholeNumberValue(val);
			}
			else {
				returnValue = StringUtils.EMPTY + val;
			}
			//}
		}
		return returnValue;
	}
	
	private String getWholeNumberValue(double value) {
		return Integer.toString(Double.valueOf(value).intValue());
	}
	public static void main(String args[])
	{
		CommissionExcelReader r = new CommissionExcelReader();
		r.getDataFromFile("", "");
	}
	public  List<CommissionExcelModel>  getDataFromFile(String path,String fileName)
	{
		List<CommissionExcelModel> parsedData = null;
		try {
			//loadFile("C:\\aetnafeed\\PFG_StatusFeed_ADA_06202014.XLS");
			//loadFile("D:\\hsData\\Book1.xls");
			loadFile(fileName);
			HSSFSheet sheet = workbook.getSheetAt(0);
			parsedData = readRowwiseData(sheet, null);
			int i=0;
			for(CommissionExcelModel model : parsedData)
			{
				System.out.println("Row " +i + " Data : "+model);
				i++;
			}
			
		} catch (IOException e) {
			LOGGER.error("Error in getDataFromFile()" ,e);
		}
		return parsedData;
	}
	private List<CommissionExcelModel> readRowwiseData(HSSFSheet  sheet, CommissionExcelModel model) {
	List<CommissionExcelModel> stmList = new ArrayList<CommissionExcelModel>();
		try {
			
			if (null == sheet) {
				//LOGGER.info("XSSFSheet is null.");
				return null;
			}
			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();
			boolean isFirstRow = true;
			
			while (rowIterator.hasNext()) {
				
				Row excelRow = rowIterator.next(); 
				
				
				if (isFirstRow) {
					isFirstRow = false;
				    {
						continue;
					}
								
				}
				if(excelRow.getRowNum() < EnrollmentConstants.FIVE){
					continue;
				}
				stmList.add(readRowData(excelRow));
			}
		
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("readAndPersistSTMData() End");
		}
		return stmList;
	}
private String trimIfPossible(String str)
{
	if(!StringUtils.isEmpty(str) &&  str != null)
	{
		return str.trim();
	}
	return str;
}
public static Date StringToDate(String dateStr, String format) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
    try {
		return dateFormat.parse(dateStr);
	} catch (ParseException e) {
		LOGGER.error("ParseException",e);
	}
    catch(Exception ee)
    {
    	LOGGER.error("Error in StringToDate()" ,ee);
    }
    return null;
}
	private CommissionExcelModel readRowData(Row excelRow) {
		CommissionExcelModel commissionExcelModel = null;
		try{		
		
		 if (null != excelRow) {//excelRow.getCell(0).getStringCellValue()
			commissionExcelModel = new CommissionExcelModel();
			if(excelRow.getCell(0) != null){
			commissionExcelModel.setMarketSeg(trimIfPossible(excelRow.getCell(0).getStringCellValue()));
			}
			if(excelRow.getCell(1) != null){
			commissionExcelModel.setRegion(trimIfPossible(excelRow.getCell(1).getStringCellValue()));
			}
			if(excelRow.getCell(2) != null){
			commissionExcelModel.setCFO(trimIfPossible(excelRow.getCell(2).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.THREE) != null){
			commissionExcelModel.setProducerName(trimIfPossible(excelRow.getCell(EnrollmentConstants.THREE).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.FOUR) != null){
			 commissionExcelModel.setProducerAddress(trimIfPossible(excelRow.getCell(EnrollmentConstants.FOUR).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.FIVE) != null){
			 commissionExcelModel.setConsumerTaxId(trimIfPossible(excelRow.getCell(EnrollmentConstants.FIVE).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.SIX) != null){
			 commissionExcelModel.setLoc(trimIfPossible(excelRow.getCell(EnrollmentConstants.SIX).getStringCellValue()));	
			}
			if(excelRow.getCell(EnrollmentConstants.SEVEN) != null){
				commissionExcelModel.setGA(trimIfPossible(excelRow.getCell(EnrollmentConstants.SEVEN).getStringCellValue()));
			}		
			if(excelRow.getCell(EnrollmentConstants.EIGHT) != null){
				Float payeeShare = new Float(excelRow.getCell(EnrollmentConstants.EIGHT).getNumericCellValue());
			commissionExcelModel.setPayeeShare(payeeShare);
			}
			if(excelRow.getCell(EnrollmentConstants.NINE) != null){
		    	commissionExcelModel.setEffectiveDate(StringToDate(trimIfPossible(excelRow.getCell(EnrollmentConstants.NINE).getDateCellValue().toString()),"MM/dd/yyyy"));
			}
			if(excelRow.getCell(EnrollmentConstants.TEN) != null){
			 commissionExcelModel.setContractState(trimIfPossible(excelRow.getCell(EnrollmentConstants.TEN).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.ELEVEN) != null){
			 commissionExcelModel.setUID(trimIfPossible(excelRow.getCell(EnrollmentConstants.ELEVEN).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.TWELVE) != null){
			commissionExcelModel.setConsumerName(trimIfPossible(excelRow.getCell(EnrollmentConstants.TWELVE).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.THIRTEEN) != null){
		    	commissionExcelModel.setProductGroup(trimIfPossible(excelRow.getCell(EnrollmentConstants.THIRTEEN).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.FOURTEEN) != null){
			 commissionExcelModel.setProductName(trimIfPossible(excelRow.getCell(EnrollmentConstants.FOURTEEN).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.FIFTEEN) != null){
			 commissionExcelModel.setProductFunding(trimIfPossible(excelRow.getCell(EnrollmentConstants.FIFTEEN).getStringCellValue()));
			}
			if(excelRow.getCell(EnrollmentConstants.SIXTEEN) != null){
			 commissionExcelModel.setProductSubs(Integer.valueOf(trimIfPossible(excelRow.getCell(EnrollmentConstants.SIXTEEN).getStringCellValue())));
			}
			if(excelRow.getCell(EnrollmentConstants.SEVENTEEN) != null){
		 	 commissionExcelModel.setPremiumPaidMonth(StringToDate(trimIfPossible(excelRow.getCell(EnrollmentConstants.SEVENTEEN).getDateCellValue().toString()),"MM/dd/yyyy"));
			}
			if(excelRow.getCell(18) != null){
			 commissionExcelModel.setSubscriberMonth(trimIfPossible(excelRow.getCell(18).getStringCellValue().toString()));
			}
			if(excelRow.getCell(19) != null){
				Float basePremium = new Float(excelRow.getCell(19).getNumericCellValue());
				commissionExcelModel.setBasePremium(basePremium);
			}
			if(excelRow.getCell(20) != null){
				double baseSubscribers =excelRow.getCell(20).getNumericCellValue();
				Integer baseSub = (int)baseSubscribers;
		 	   commissionExcelModel.setBaseSubscribers(baseSub);
			}
			if(excelRow.getCell(21) != null){
				Float baseCommission = new Float(excelRow.getCell(21).getNumericCellValue());
				commissionExcelModel.setBaseCommission(baseCommission);
			}
			if(excelRow.getCell(22) != null){
				Float bonusPremium = new Float(excelRow.getCell(22).getNumericCellValue());
				commissionExcelModel.setBonusPremium(bonusPremium);
			}
			if(excelRow.getCell(23) != null){
				double bonusSubscribers =excelRow.getCell(23).getNumericCellValue();
				Integer bonuSub = (int)bonusSubscribers;
			    commissionExcelModel.setBonusSubscribers(bonuSub);
			}
			if(excelRow.getCell(24) != null){
				Float bonusCommission = new Float(excelRow.getCell(24).getNumericCellValue());
				commissionExcelModel.setBonusCommission(bonusCommission);
			}
			if(excelRow.getCell(25) != null){
				Float adjustmentAmt =  new Float(excelRow.getCell(25).getNumericCellValue());
				commissionExcelModel.setAdjustmentAmount(adjustmentAmt);
			}
			if(excelRow.getCell(26) != null){
			 commissionExcelModel.setAdjustmentType(trimIfPossible(excelRow.getCell(26).getStringCellValue()));
			}
			if(excelRow.getCell(27) != null){
				Float totalpremium =  new Float(excelRow.getCell(27).getNumericCellValue());
				commissionExcelModel.setTotalPremium(totalpremium);
			}
			if(excelRow.getCell(28) != null){
				double totalSubscribers =excelRow.getCell(28).getNumericCellValue();
				Integer totalSub = (int)totalSubscribers;
			    commissionExcelModel.setTotalSubscribers(totalSub);					
			}
			if(excelRow.getCell(29) != null){
				Float totalcommission =  new Float(excelRow.getCell(29).getNumericCellValue());
				commissionExcelModel.setTotalCommission(totalcommission);
			}
			if(excelRow.getCell(30) != null){
				String checkDate = "";
				if (DateUtil.isCellDateFormatted(excelRow.getCell(30)))
				{
				   try {

				    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				    checkDate = sdf.format(excelRow.getCell(30).getDateCellValue());

				    } catch (Exception e) {
				    	LOGGER.debug("Error getting Date" , e);
				    }
				}
			 commissionExcelModel.setCheckRunDate(checkDate);
			}
			if(excelRow.getCell(31) != null){
			 commissionExcelModel.setSaleAdjustmentReason(trimIfPossible(excelRow.getCell(31).getStringCellValue()));
			}
			if(excelRow.getCell(32) != null){
			 commissionExcelModel.setPaycheckNumOrEFT(trimIfPossible(excelRow.getCell(32).getStringCellValue()));
			}
			if(excelRow.getCell(33) != null){
			 commissionExcelModel.setPaymentType(trimIfPossible(excelRow.getCell(33).getStringCellValue()));
			}
			if(excelRow.getCell(34) != null){
			 commissionExcelModel.setCarrierName(trimIfPossible(excelRow.getCell(34).getStringCellValue()));
			}
			if(excelRow.getCell(35) != null){
			 commissionExcelModel.setCarrierArranmgmtName(trimIfPossible(excelRow.getCell(35).getStringCellValue()));
			}
			if(excelRow.getCell(36) != null){
			 commissionExcelModel.setExchangeIdentifier(trimIfPossible(excelRow.getCell(36).getStringCellValue()));
			}
			if(excelRow.getCell(37) != null){
			 commissionExcelModel.setExchangeOnOffIndicator(trimIfPossible(excelRow.getCell(37).getStringCellValue()));
			}
		 }
		}
		catch(Exception ex){
			LOGGER.error("Exception in readRowData()" , ex);
		
		}
		return commissionExcelModel;
	}
}
