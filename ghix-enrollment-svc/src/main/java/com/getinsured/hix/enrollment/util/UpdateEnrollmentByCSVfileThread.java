package com.getinsured.hix.enrollment.util;

import java.util.List;
import java.util.concurrent.Callable;

import com.getinsured.hix.enrollment.service.UpdateEnrollmentByCSVfileServiceImpl;
import com.getinsured.hix.model.enrollment.EnrollmentCSVDto;

public class UpdateEnrollmentByCSVfileThread implements Callable<List<EnrollmentCSVDto>> {
	
	List<EnrollmentCSVDto> enrlCSVDtoist;
	UpdateEnrollmentByCSVfileServiceImpl updateEnrollmentByCSVfileServiceImpl;

	public UpdateEnrollmentByCSVfileThread(List<EnrollmentCSVDto> enrlCSVDtoist, UpdateEnrollmentByCSVfileServiceImpl updateEnrollmentByCSVfileServiceImpl) {
		this.enrlCSVDtoist = enrlCSVDtoist;
		this.updateEnrollmentByCSVfileServiceImpl = updateEnrollmentByCSVfileServiceImpl;
	}

	@Override
	public List<EnrollmentCSVDto> call() throws Exception {
		List<EnrollmentCSVDto> UpdateEnrollmentByCSVfileErrorList = updateEnrollmentByCSVfileServiceImpl.updateEnrollmentByCSV(enrlCSVDtoist);
		return UpdateEnrollmentByCSVfileErrorList;
	}

}
