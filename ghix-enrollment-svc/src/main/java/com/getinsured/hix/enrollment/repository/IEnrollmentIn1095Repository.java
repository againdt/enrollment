package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.getinsured.hix.model.enrollment.EnrollmentIn1095;


public interface IEnrollmentIn1095Repository extends JpaRepository<EnrollmentIn1095, Integer> {
	@Query(" FROM EnrollmentIn1095 as en " +
		   " WHERE en.jobExecutionId = :jobExecutionId " +
		   " AND en.errorCodeLkp.lookupValueCode in (:errorCode)")
	List<EnrollmentIn1095> getbyJobExecutionIdAndReSubZip(@Param("jobExecutionId") String jobExecutionId, @Param("errorCode") List<String> errorCode);
	
	@Query(" FROM EnrollmentIn1095 as en " +
		   " WHERE en.jobExecutionId = :jobExecutionId ")
	List<EnrollmentIn1095> getbyJobExecutionId(@Param("jobExecutionId") String jobExecutionId);
}
