package com.getinsured.hix.enrollment.util;
import java.lang.management.ManagementFactory;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.enrollment.service.EnrollmentReconciliationReportService;
import com.getinsured.hix.platform.util.exception.GIException;
/**
 * 
 * @author parhi_s
 *
 */
public class EnrollmentReconCSVReportJobThread implements Callable {
	private EnrollmentReconciliationReportService enrollmentReconciliationReportService;
	private String fileName;
	private String timeStamp;
	private String fileType;
	private EnrollmentReconciliationInfo enrollmentReconciliationInfo;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentReconCSVReportJobThread.class);
	public EnrollmentReconCSVReportJobThread(String fileName, String timeStamp, String fileType, EnrollmentReconciliationReportService enrollmentReconciliationReportService, EnrollmentReconciliationInfo enrollmentReconciliationInfo){
		this.fileName = fileName;
		this.timeStamp = timeStamp;
		this.fileType = fileType;
		this.enrollmentReconciliationReportService = enrollmentReconciliationReportService;
		this.enrollmentReconciliationInfo = enrollmentReconciliationInfo;
	}

	@Override
	public Boolean call() throws GIException {
		boolean status = false;
		try{
			if(fileType.equalsIgnoreCase(EnrollmentConstants.FILETYPE_EDI)){
				status = enrollmentReconciliationReportService.serveEnrollmentBatchCSVReconciliationJob(fileName, timeStamp, enrollmentReconciliationInfo);
				LOGGER.info("Time taken by thread with ID = "+ Thread.currentThread().getId() +" For File = "+fileName+" consumed "+ ManagementFactory.getThreadMXBean().getThreadCpuTime(Thread.currentThread().getId())+" nano second of cpu time.");
			}
			else if(fileType.equalsIgnoreCase(EnrollmentConstants.FILETYPE_TA1_999)){
				status = enrollmentReconciliationReportService.processEnrollmentReconTrackingFiles(fileName, timeStamp, enrollmentReconciliationInfo);
				LOGGER.info("Time taken by thread with ID = "+ Thread.currentThread().getId() +" For File = "+fileName+" consumed "+ ManagementFactory.getThreadMXBean().getThreadCpuTime(Thread.currentThread().getId())+" nano second of cpu time.");
			}
			else{
				throw new GIException("Invalid File Type received for processing: "+fileType);
			}
			
		}catch(Exception e){
			LOGGER.error("Error while processing file "+fileName +" :: "+e.getMessage(), e);
		}
		return status;
	}

}