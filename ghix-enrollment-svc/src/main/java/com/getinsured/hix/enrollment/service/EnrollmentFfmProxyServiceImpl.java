/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils.isNotNullAndEmpty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.enrollment.EnrolleeFfmProxyDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentFfmProxyDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.enrollment.repository.IEnrollmentIssuerCommisionRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPlanPrivateRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPrivateRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateConstants;
import com.getinsured.hix.enrollment.util.EnrollmentPrivateUtils;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentIssuerCommision;
import com.getinsured.hix.model.enrollment.EnrollmentPlan;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.google.gson.Gson;


/**
 * @author negi_s
 *
 */
@Service("enrollmentFfmProxyService")
@Transactional
public class EnrollmentFfmProxyServiceImpl implements EnrollmentFfmProxyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentFfmProxyServiceImpl.class);
	
	@Autowired private LookupService lookupService;
	@Autowired private UserService userService;
	@Autowired private IEnrollmentPrivateRepository enrollmentRepository;
	@Autowired private RestTemplate restTemplate;
	@Autowired private IEnrollmentPlanPrivateRepository enrollmentPlanRepository;
	@Autowired private EnrollmentPrivateUtils enrollmentPrivateUtils;
	@Autowired private Gson platformGson;
	@Autowired private IEnrollmentIssuerCommisionRepository enrollmentIssuerCommisionRepository;
	
	/**
	 * @author panda_p
	 * @since 16-DEC-2014
	 * 
	 * This Method will update the Enrollment for change in premium amounts while re-attempting proxy FFM flow
	 * 
	 * @param enrollment
	 * @param targetId
	 * @return
	 * @throws Exception
	 */
	@Override
	public Enrollment updateReattemptEnrollment (Enrollment enrollment, String targetId) throws Exception{
		AccountUser user = null;
		Enrollment resultantEnrollment=null;
		if(isNotNullAndEmpty(targetId) &&  NumberUtils.isNumber(targetId)){
			user = userService.findById(Integer.parseInt(targetId));
			LOGGER.info("User Object :: " + ((user != null) ? user.getId() : "No User Object Found for Target Id" + targetId));
		}else{
			LOGGER.info("Target Id is null or "+targetId);
		}
		enrollment.setUpdatedBy(user);
		resultantEnrollment = enrollmentRepository.save(enrollment);
		return resultantEnrollment;
	}
	
	
	/* (non-Javadoc)
	 * @see com.getinsured.hix.enrollment.service.EnrollmentFfmProxyService#createEnrollment(com.getinsured.hix.dto.enrollment.EnrollmentFfmProxyDTO)
	 */
	@Override
	public EnrollmentResponse createEnrollment(
			EnrollmentFfmProxyDTO enrollmentFfmProxyDTO) {
		
		LOGGER.info("Inside createEnrollment()"); 
		Enrollment enrollment = new Enrollment();
		EnrollmentResponse  enrollmentResponse = new EnrollmentResponse();
		try{
		enrollment.setCmrHouseHoldId(enrollmentFfmProxyDTO.getCmrHouseholdId());
		enrollment.setPriorSsapApplicationid(enrollmentFfmProxyDTO.getSsapApplicationId());
		enrollment.setSsapApplicationid(enrollmentFfmProxyDTO.getSsapApplicationId());
		enrollment.setPlanId(enrollmentFfmProxyDTO.getPlanId());
		enrollment.setStateExchangeCode(enrollmentFfmProxyDTO.getStateExchangeCode());
		enrollment.setGiHouseholdId(enrollmentFfmProxyDTO.getPartnerAssignedConsumerId());
		enrollment.setPartnerAssignedConsumerId(enrollmentFfmProxyDTO.getPartnerAssignedConsumerId());
		enrollment.setHouseHoldCaseId(enrollmentFfmProxyDTO.getFfeAssignedConsumerId());
		enrollment.setCarrierAppId(enrollmentFfmProxyDTO.getFfeAssignedConsumerId());
		
		enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
		enrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_ENROLLMENT_TYPE, EnrollmentPrivateConstants.ENROLLMENT_TYPE_INDIVIDUAL));
		enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE, EnrollmentPrivateConstants.LOOKUP_INSURANCE_TYPE_HEALTH));
		
		enrollment.setEnrollmentReason(new Character('I'));
		
		String coverageStartDate = enrollmentFfmProxyDTO.getEnrollmentGroupStartDate();
		//enrollmentFfmProxyDTO.setEnrollmentGroupEndDate("2015-12-31 11:59:59.999");
		if (isNotNullAndEmpty(coverageStartDate)) {
			SimpleDateFormat sdf = new SimpleDateFormat(EnrollmentPrivateConstants.DEFAULT_TIMESTAMP_FORMAT);
			String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,EnrollmentPrivateConstants.DEFAULT_TIMESTAMP_FORMAT,GhixConstants.REQUIRED_DATE_FORMAT);
			enrollment.setBenefitEffectiveDate( DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT));
			enrollmentFfmProxyDTO.setEnrollmentGroupEndDate(sdf.format(EnrollmentUtils.getYearEndDate(enrollment.getBenefitEffectiveDate())));
			//enrollment.setBenefitEndDate(EnrollmentUtils.getYearEndDate(enrollment.getBenefitEffectiveDate()));
		}
		if(isNotNullAndEmpty(enrollmentFfmProxyDTO.getEnrollmentGroupEndDate())){
			String effectiveDateStr = DateUtil.changeFormat(enrollmentFfmProxyDTO.getEnrollmentGroupEndDate(),EnrollmentPrivateConstants.DEFAULT_TIMESTAMP_FORMAT,GhixConstants.REQUIRED_DATE_FORMAT);
			enrollment.setBenefitEndDate( DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT));
			//enrollment.setBenefitEndDate(DateUtil.StringToDate(enrollmentFfmProxyDTO.getEnrollmentGroupEndDate(), EnrollmentPrivateConstants.DEFAULT_TIMESTAMP_FORMAT));
		}
		enrollment.setGrossPremiumAmt(enrollmentFfmProxyDTO.getPremiumTotalPremiumAmount());
		enrollment.setAptcAmt(enrollmentFfmProxyDTO.getPremiumAPTCAppliedAmount());
		enrollment.setNetPremiumAmt(enrollmentFfmProxyDTO.getPremiumTotalIndividualResponsibilityAmount());
		enrollment.setCsrLevel(enrollmentFfmProxyDTO.getPremiumCSRLevelApplicable());
		enrollment.setHiosIssuerId(enrollmentFfmProxyDTO.getEnrollmentGroupIssuerId());
		enrollment.setSadp(EnrollmentPrivateConstants.N);
		
		enrollment.setExchangeTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.EXCHANGE_TYPE_ONEXCHANGE));
		if(EnrollmentFfmProxyDTO.Flow.FFM_PROXY.equals(enrollmentFfmProxyDTO.getFlow())){
			enrollment.setEnrollmentModalityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.MODALITY_FFM_PROXY));	
		}else if(EnrollmentFfmProxyDTO.Flow.CA_PROXY.equals(enrollmentFfmProxyDTO.getFlow())){
			enrollment.setEnrollmentModalityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_EXCHANGE_TYPE, EnrollmentPrivateConstants.MODALITY_CA_PROXY));
		}

		setEnrollmentDataFromPlanMngmt(enrollment , enrollmentFfmProxyDTO.getPlanId());
		
		List<Enrollee> enrolleeList = getEnrolleeListFromDto(enrollmentFfmProxyDTO);
		setEnrollmentSubscriberInfo(enrollment,enrolleeList);
		
		AccountUser user = null;
		if(NumberUtils.isNumber(enrollmentFfmProxyDTO.getTargetId())){
			user = userService.findById(Integer.parseInt(enrollmentFfmProxyDTO.getTargetId()));
			LOGGER.info("User Object :: " + ((user != null) ? user.getId() : "No User Object Found for Target Id" + enrollmentFfmProxyDTO.getTargetId()));
		}
		
		/**
		 * Event Creation
		 */
		enrollment.setEnrollmentEvents(createEnrollmentEvents(enrollment,enrolleeList ,user));

		enrollment.setEnrollees(enrolleeList);
		enrollment.setCreatedBy(user);
		enrollment.setUpdatedBy(user);
		enrollment.setSubmittedToCarrierDate(new TSDate());//HIX-55747
		enrollment = enrollmentRepository.saveAndFlush(enrollment);
		
		if(null != enrollment.getEnrollmentIssuerCommision()) {
			enrollment.getEnrollmentIssuerCommision().setEnrollment(enrollment);
			enrollmentIssuerCommisionRepository.saveAndFlush(enrollment.getEnrollmentIssuerCommision());
		}
		enrollmentPrivateUtils.recordApplicationEnrollmentPrivateEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString(), enrollment.getEnrollmentStatusLkp().getLookupValueLabel());
		}catch (Exception e) {
			LOGGER.error("Failed to create enrollment" , e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(e.getMessage());
			return enrollmentResponse;
		}
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);	
		enrollmentResponse.setEnrollment(enrollment);
		return enrollmentResponse;
	}
	
	private void setEnrollmentDataFromPlanMngmt(Enrollment enrollment, Integer planId) {

		PlanRequest planRequest= new PlanRequest();
		planRequest.setPlanId(""+planId);
		planRequest.setMarketType("INDIVIDUAL");
		if(null != enrollment.getBenefitEffectiveDate()) {
			planRequest.setCommEffectiveDate(DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		}
		String planInfoResponse=null;
		try{
			//planInfoResponse = restTemplate.postForObject(ghixPlanMgmtPlanDataURL,planRequest,String.class);PlanMgmtEndPoints
			planInfoResponse = restTemplate.postForObject(PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL,planRequest,String.class);
			LOGGER.info("getting successfull response of the plan data from plan management.");
		}
		catch(Exception ex){
			LOGGER.error("exception in getting plan data from the plan management" , ex);
		}
		PlanResponse planResponse = new PlanResponse();
		planResponse = platformGson.fromJson(planInfoResponse, planResponse.getClass());

		if(planResponse==null || (planResponse.getStatus() == null || planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))){
		//if(planResponse==null || planResponse.getStatus() == null){
			LOGGER.info("No Plan data found " );
		//	throw new GIException(EnrollmentPrivateConstants.ERROR_CODE_301,"No Plan data found. Plan mgmt REST services returns null ","HIGH");
		}
		if (planId !=null) {
			enrollment.setHiosIssuerId(planResponse.getHiosIssuerId());
			enrollment.setPlanId(planResponse.getPlanId());
			enrollment.setCMSPlanID(planResponse.getIssuerPlanNumber());
			enrollment.setPlanName(planResponse.getPlanName());
			enrollment.setInsurerName(planResponse.getIssuerName());
			enrollment.setInsurerTaxIdNumber(planResponse.getTaxIdNumber());
			if(planResponse.getPlanLevel()!=null){
				enrollment.setPlanLevel(planResponse.getPlanLevel().trim().toUpperCase());
			}
			enrollment.setIssuerId(planResponse.getIssuerId());
			enrollment.setCarrierAppUrl(planResponse.getApplicationUrl());
				
				// Fetch Enrollment_Plan data for Plan
			EnrollmentPlan enrollmentPlan = enrollmentPlanRepository.findEnrollmentPlanByPlanID(planResponse.getPlanId());
				// Populate Enrollment Plan table with Plan data is it doesn't exists
				if(enrollmentPlan == null){
					enrollmentPlan = new EnrollmentPlan();
					enrollmentPlan.setPlanId(planResponse.getPlanId());
					
					if(isNotNullAndEmpty(planResponse.getDeductible())){
						enrollmentPlan.setIndivDeductible(Double.valueOf(planResponse.getDeductible()));
					}
					
					if(isNotNullAndEmpty(planResponse.getOopMax())){
						enrollmentPlan.setIndivOopMax(Double.valueOf(planResponse.getOopMax()));
					}
					
					if(isNotNullAndEmpty(planResponse.getOopMaxFamily())){
						enrollmentPlan.setFamilyOopMax(Double.valueOf(planResponse.getOopMaxFamily()));
					}
					
					if(isNotNullAndEmpty(planResponse.getDeductibleFamily())){
						enrollmentPlan.setFamilyDeductible(Double.valueOf(planResponse.getDeductibleFamily()));
					}
					
					enrollmentPlan.setGenericMedication(planResponse.getGenericMedications());
					enrollmentPlan.setOfficeVisit(planResponse.getOfficeVisit());
					enrollmentPlan.setNetworkType(planResponse.getNetworkType());
					enrollmentPlan.setPhoneNumber(planResponse.getIssuerPhone());
					enrollmentPlan.setIssuerSiteUrl(planResponse.getIssuerSite());
				}
				enrollment.setEnrollmentPlan(enrollmentPlan);
				setEnrollmentCommissionData(enrollment, planResponse);
		}
	}

	private void setEnrollmentSubscriberInfo(Enrollment enrollment,
			List<Enrollee> enrolleeList) {
		//Find subscriber
		Enrollee subscriber = null;
		for (Enrollee enrollee:enrolleeList){
			if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().equals(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER))){
				//Found the subscriber
				subscriber = enrollee;
				StringBuilder subscriberName = new StringBuilder();

				if(isNotNullAndEmpty(subscriber.getFirstName())){
					subscriberName.append(subscriber.getFirstName());
					subscriberName.append(" ");
				}
				if(isNotNullAndEmpty(subscriber.getMiddleName())){
					subscriberName.append(subscriber.getMiddleName());
					subscriberName.append(" ");	
				}
				if(isNotNullAndEmpty(subscriber.getLastName())){
					subscriberName.append(subscriber.getLastName());
					subscriberName.append(" ");
				}
				if(isNotNullAndEmpty(subscriber.getSuffix())){
					subscriberName.append(subscriber.getSuffix());	
				}
				enrollment.setExchgSubscriberIdentifier(subscriber.getExchgIndivIdentifier());
				enrollment.setSubscriberName(subscriberName.toString().trim());
				enrollment.setSponsorName(subscriberName.toString().trim());
				break;
			}
		}		
	}

	private List<EnrollmentEvent> createEnrollmentEvents(Enrollment enrollment,
			List<Enrollee> enrolleeList, AccountUser user) {
		
		List<EnrollmentEvent> enrollmentEventList= new ArrayList<EnrollmentEvent>();

		for (Enrollee tempenrollee : enrolleeList) {

			tempenrollee.setEnrollment(enrollment);
			if(tempenrollee.getCustodialParent()!=null){
				tempenrollee.getCustodialParent().setEnrollment(enrollment);
			}
			//creating EnrollmentEvent
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
			enrollmentEvent.setCreatedBy(user);
			enrollmentEvent.setEnrollee(tempenrollee);
			enrollmentEvent.setEnrollment(enrollment);
			enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_REASON,EnrollmentPrivateConstants.EVENT_REASON_BENEFIT_SELECTION));
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.EVENT_TYPE, EnrollmentPrivateConstants.EVENT_TYPE_ADDITION));

			//added last event Id for Audit
			tempenrollee.setLastEventId(enrollmentEvent);
			enrollmentEventList.add(enrollmentEvent);
		}
		return enrollmentEventList;
	}

	private List<Enrollee> getEnrolleeListFromDto(
			EnrollmentFfmProxyDTO enrollmentFfmProxyDTO) {
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		List<EnrolleeFfmProxyDTO> enrolleeProxyList = enrollmentFfmProxyDTO.getEnrolleeList();
		if(enrolleeProxyList!= null){
			for(EnrolleeFfmProxyDTO proxyEnrollee : enrolleeProxyList ){
				Enrollee enrollee = new Enrollee();
				enrollee.setEnrollmentReason(new Character('I'));
				if(isNotNullAndEmpty(proxyEnrollee.getDob())){
					enrollee.setBirthDate(DateUtil.StringToDate(proxyEnrollee.getDob(), EnrollmentPrivateConstants.DEFAULT_TIMESTAMP_FORMAT));
				}
				enrollee.setInsuranceAplicantId(proxyEnrollee.getInsuranceAplicantId());
				enrollee.setFfmPersonId(proxyEnrollee.getFfmPersonId());
				if(isNotNullAndEmpty(enrollmentFfmProxyDTO.getEnrollmentGroupStartDate())){
					enrollee.setEffectiveStartDate(DateUtil.StringToDate(enrollmentFfmProxyDTO.getEnrollmentGroupStartDate(), EnrollmentPrivateConstants.DEFAULT_TIMESTAMP_FORMAT));
				}
				if(isNotNullAndEmpty(enrollmentFfmProxyDTO.getEnrollmentGroupEndDate())){
					enrollee.setEffectiveEndDate(DateUtil.StringToDate(enrollmentFfmProxyDTO.getEnrollmentGroupEndDate(), EnrollmentPrivateConstants.DEFAULT_TIMESTAMP_FORMAT));
				}
				enrollee.setExchgIndivIdentifier(proxyEnrollee.getMemberFFEAssignedMemberId());
				enrollee.setFirstName(proxyEnrollee.getFirstName());
				enrollee.setLastName(proxyEnrollee.getLastName());
				enrollee.setMiddleName(proxyEnrollee.getMiddleName());
				enrollee.setSuffix(proxyEnrollee.getSuffixName());
				enrollee.setTaxIdNumber(proxyEnrollee.getSsn());
				enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.GENDER, proxyEnrollee.getGender()));
				if(proxyEnrollee.getMemberSubscriberIndicator()!= null && proxyEnrollee.getMemberSubscriberIndicator()){
					enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.PERSON_TYPE_SUBSCRIBER));
				}
				else{
					enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE,EnrollmentPrivateConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
				}
				if(proxyEnrollee.getMemberTobaccoUseIndicator() != null && proxyEnrollee.getMemberTobaccoUseIndicator()){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, EnrollmentPrivateConstants.T));
				}else{
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.TOBACCO_USAGE, EnrollmentPrivateConstants.N));
				}
				enrollee.setRelationshipToHCPLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.RELATIONSHIP, proxyEnrollee.getMemberRelationshipToSubscriber()));
				enrollee.setIssuerIndivIdentifier(proxyEnrollee.getMemberIssuerAssignedMemberId());
				enrollee.setRatingArea(enrollmentFfmProxyDTO.getPremiumRatingArea());
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentPrivateConstants.ENROLLMENT_STATUS, EnrollmentPrivateConstants.ENROLLMENT_STATUS_PENDING));
				enrolleeList.add(enrollee);
			}
		}
		return enrolleeList;
	}
	
	private void setEnrollmentCommissionData(Enrollment enrollment, PlanResponse planResponse) {
		if(null != planResponse) {
			EnrollmentIssuerCommision enrlCommission = new EnrollmentIssuerCommision();
			enrlCommission.setInitialCommDollarAmt(planResponse.getFirstYearCommDollarAmt());
			enrlCommission.setInitialCommissionPercent(planResponse.getFirstYearCommPercentageAmt());
			enrlCommission.setRecurringCommDollarAmt(planResponse.getSecondYearCommDollarAmt());
			enrlCommission.setRecurringCommissionPercent(planResponse.getSecondYearCommPercentageAmt());
			enrlCommission.setNonCommissionFlag(planResponse.getNonCommissionFlag());
			if(!EnrollmentConstants.Y.equalsIgnoreCase(enrlCommission.getNonCommissionFlag())) {
				if(null != enrlCommission.getInitialCommDollarAmt()) {
					enrlCommission.setInitialCommissionAmt(enrlCommission.getInitialCommDollarAmt());
				}else if(null != enrlCommission.getInitialCommissionPercent() && null != enrollment.getGrossPremiumAmt()) {
					enrlCommission.setInitialCommissionAmt(EnrollmentUtils.round(enrlCommission.getInitialCommissionPercent() * enrollment.getGrossPremiumAmt(), 2));
				}
				if(null != enrlCommission.getRecurringCommDollarAmt()) {
					enrlCommission.setRecurringCommissionAmt(enrlCommission.getRecurringCommDollarAmt());
				}else if(null != enrlCommission.getRecurringCommissionPercent() && null != enrollment.getGrossPremiumAmt()) {
					enrlCommission.setRecurringCommissionAmt(EnrollmentUtils.round(enrlCommission.getRecurringCommissionPercent() * enrollment.getGrossPremiumAmt(), 2));
				}
			}else {
				enrlCommission.setInitialCommissionAmt(0d);
				enrlCommission.setRecurringCommissionAmt(0d);
			}
			enrlCommission.setEnrollment(enrollment);
			enrollment.setEnrollmentIssuerCommision(enrlCommission);
		}
	}
}
