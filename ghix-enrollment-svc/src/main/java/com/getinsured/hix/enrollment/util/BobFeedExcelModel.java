package com.getinsured.hix.enrollment.util;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class BobFeedExcelModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(BobFeedExcelModel.class);
	
	private String sellingBrokerNPN;
	private String sellingBrokerTIN;
	private String sellingBrokerFirstName;
	private String sellingBrokerLastName;
	private String sellingBrokerFirmName;
	private String servicingBrokerNPN;
	private String servicingBrokerFirstName;
	private String servicingBrokerLastName;
	private String servicingBrokerFirmName;
	private Integer groupId;
	private String groupName;
	private Date grpEffDate;
	private String grpTermDate;
	private Integer memberID;
	private String UID;
	private String memberName;
	private String memberFirstName;
	private String memberLastName;
	private String memberMiddleInitial;
	private String memberSuffix;
	private Date memberEffData;
	private Date memberTermDate;
	private String termDescription;
	private Date lastPremiumDate;
	private Date lastPremiumMonth;
	private Float lastPaidPremiumAmt;
	private Float lastSubPaidPremiumAmt;
	private String gaNpn;
	private String gaTin;
	private String gaFirstName;
	private String gaLastName;
	private String gaFirmName;
	private String letterDesc;
	private Date printDate;
	private String planName;
	private String CPID;
	private String PPID;
	private String HIPPA;
	private String RC;
	private String application_Id;
	private String applicantId;
	private Date applicationReceivedDate;
	private String UW;
	private String DentalInd;
	private Date DOB;
	private String phone;
	private String address1;
	private String address2;
	private String city;
	private String ST;
	private String ZIP;
	private String EnrollmentOnOffExchgInd;
	private String memberExchgMemId;
	private Float totalPremium;
	private Float medicalOnlyPremium;
	private Float dentalOnlyPremium;
	private Float medicalBasePremium;
	private String type1;
	private String method1;
	private Date date1;
	private Float amt1;
	private String type2;
	private String method2;
	private Date date2;
	private Float amt2;
	private String type3;
	private String method3;
	private Date date3;
	private Float amt3;
	private String type4;
	private String method4;
	private Date date4;
	private Float amt4;
	private String method5;
	private Date date5;
	private Float amt5;
	private String type6;
	private String method6;
	private Date date6;
	private Float amt6;
	private String note1;
	private String note2;
	private String note3;
	private String note4;
	private String note5;
	private String note6;
	private char SubsidyIndicator;	
	
	public String getSellingBrokerNPN() {
		return sellingBrokerNPN;
	}
	public void setSellingBrokerNPN(String sellingBrokerNPN) {
		this.sellingBrokerNPN = sellingBrokerNPN;
	}
	public String getSellingBrokerTIN() {
		return sellingBrokerTIN;
	}
	public void setSellingBrokerTIN(String sellingBrokerTIN) {
		this.sellingBrokerTIN = sellingBrokerTIN;
	}
	public String getSellingBrokerFirstName() {
		return sellingBrokerFirstName;
	}
	public void setSellingBrokerFirstName(String sellingBrokerFirstName) {
		this.sellingBrokerFirstName = sellingBrokerFirstName;
	}
	public String getSellingBrokerLastName() {
		return sellingBrokerLastName;
	}
	public void setSellingBrokerLastName(String sellingBrokerLastName) {
		this.sellingBrokerLastName = sellingBrokerLastName;
	}
	public String getSellingBrokerFirmName() {
		return sellingBrokerFirmName;
	}
	public void setSellingBrokerFirmName(String sellingBrokerFirmName) {
		this.sellingBrokerFirmName = sellingBrokerFirmName;
	}
	public String getServicingBrokerNPN() {
		return servicingBrokerNPN;
	}
	public void setServicingBrokerNPN(String servicingBrokerNPN) {
		this.servicingBrokerNPN = servicingBrokerNPN;
	}
	public String getServicingBrokerFirstName() {
		return servicingBrokerFirstName;
	}
	public void setServicingBrokerFirstName(String servicingBrokerFirstName) {
		this.servicingBrokerFirstName = servicingBrokerFirstName;
	}
	public String getServicingBrokerLastName() {
		return servicingBrokerLastName;
	}
	public void setServicingBrokerLastName(String servicingBrokerLastName) {
		this.servicingBrokerLastName = servicingBrokerLastName;
	}
	public String getServicingBrokerFirmName() {
		return servicingBrokerFirmName;
	}
	public void setServicingBrokerFirmName(String servicingBrokerFirmName) {
		this.servicingBrokerFirmName = servicingBrokerFirmName;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Date getGrpEffDate() {
		return grpEffDate;
	}
	public void setGrpEffDate(Date grpEffDate) {
		this.grpEffDate = grpEffDate;
	}
	public String getGrpTermDate() {
		return grpTermDate;
	}
	public void setGrpTermDate(String grpTermDate) {
		this.grpTermDate = grpTermDate;
	}
	public Integer getMemberID() {
		return memberID;
	}
	public void setMemberID(Integer memberID) {
		this.memberID = memberID;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberFirstName() {
		return memberFirstName;
	}
	public void setMemberFirstName(String memberFirstName) {
		this.memberFirstName = memberFirstName;
	}
	public String getMemberLastName() {
		return memberLastName;
	}
	public void setMemberLastName(String memberLastName) {
		this.memberLastName = memberLastName;
	}
	public String getMemberMiddleInitial() {
		return memberMiddleInitial;
	}
	public void setMemberMiddleInitial(String memberMiddleInitial) {
		this.memberMiddleInitial = memberMiddleInitial;
	}
	public String getMemberSuffix() {
		return memberSuffix;
	}
	public void setMemberSuffix(String memberSuffix) {
		this.memberSuffix = memberSuffix;
	}
	public Date getMemberEffData() {
		return memberEffData;
	}
	public void setMemberEffData(Date memberEffData) {
		this.memberEffData = memberEffData;
	}
	public Date getMemberTermDate() {
		return memberTermDate;
	}
	public void setMemberTermDate(Date memberTermDate) {
		this.memberTermDate = memberTermDate;
	}
	public String getTermDescription() {
		return termDescription;
	}
	public void setTermDescription(String termDescription) {
		this.termDescription = termDescription;
	}
	public Date getLastPremiumDate() {
		return lastPremiumDate;
	}
	public void setLastPremiumDate(Date lastPremiumDate) {
		this.lastPremiumDate = lastPremiumDate;
	}
	public Date getLastPremiumMonth() {
		return lastPremiumMonth;
	}
	public void setLastPremiumMonth(Date lastPremiumMonth) {
		this.lastPremiumMonth = lastPremiumMonth;
	}
	public Float getLastPaidPremiumAmt() {
		return lastPaidPremiumAmt;
	}
	public void setLastPaidPremiumAmt(Float lastPaidPremiumAmt) {
		this.lastPaidPremiumAmt = lastPaidPremiumAmt;
	}
	public Float getLastSubPaidPremiumAmt() {
		return lastSubPaidPremiumAmt;
	}
	public void setLastSubPaidPremiumAmt(Float lastSubPaidPremiumAmt) {
		this.lastSubPaidPremiumAmt = lastSubPaidPremiumAmt;
	}
	public String getGaNpn() {
		return gaNpn;
	}
	public void setGaNpn(String gaNpn) {
		this.gaNpn = gaNpn;
	}
	public String getGaTin() {
		return gaTin;
	}
	public void setGaTin(String gaTin) {
		this.gaTin = gaTin;
	}
	public String getGaFirstName() {
		return gaFirstName;
	}
	public void setGaFirstName(String gaFirstName) {
		this.gaFirstName = gaFirstName;
	}
	public String getGaLastName() {
		return gaLastName;
	}
	public void setGaLastName(String gaLastName) {
		this.gaLastName = gaLastName;
	}
	public String getGaFirmName() {
		return gaFirmName;
	}
	public void setGaFirmName(String gaFirmName) {
		this.gaFirmName = gaFirmName;
	}
	public String getLetterDesc() {
		return letterDesc;
	}
	public void setLetterDesc(String letterDesc) {
		this.letterDesc = letterDesc;
	}
	public Date getPrintDate() {
		return printDate;
	}
	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getCPID() {
		return CPID;
	}
	public void setCPID(String cPID) {
		CPID = cPID;
	}
	public String getPPID() {
		return PPID;
	}
	public void setPPID(String pPID) {
		PPID = pPID;
	}
	public String getHIPPA() {
		return HIPPA;
	}
	public void setHIPPA(String hIPPA) {
		HIPPA = hIPPA;
	}
	public String getRC() {
		return RC;
	}
	public void setRC(String rC) {
		RC = rC;
	}
	public String getApplication_Id() {
		return application_Id;
	}
	public void setApplication_Id(String application_Id) {
		this.application_Id = application_Id;
	}
	public String getApplicantId() {
		return applicantId;
	}
	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}
	public Date getApplicationReceivedDate() {
		return applicationReceivedDate;
	}
	public void setApplicationReceivedDate(Date applicationReceivedDate) {
		this.applicationReceivedDate = applicationReceivedDate;
	}
	public String getUW() {
		return UW;
	}
	public void setUW(String uW) {
		UW = uW;
	}
	public String getDentalInd() {
		return DentalInd;
	}
	public void setDentalInd(String dentalInd) {
		DentalInd = dentalInd;
	}
	public Date getDOB() {
		return DOB;
	}
	public void setDOB(Date dOB) {
		DOB = dOB;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getST() {
		return ST;
	}
	public void setST(String sT) {
		ST = sT;
	}
	public String getZIP() {
		return ZIP;
	}
	public void setZIP(String zIP) {
		ZIP = zIP;
	}
	public String getEnrollmentOnOffExchgInd() {
		return EnrollmentOnOffExchgInd;
	}
	public void setEnrollmentOnOffExchgInd(String enrollmentOnOffExchgInd) {
		EnrollmentOnOffExchgInd = enrollmentOnOffExchgInd;
	}
	public String getMemberExchgMemId() {
		return memberExchgMemId;
	}
	public void setMemberExchgMemId(String memberExchgMemId) {
		this.memberExchgMemId = memberExchgMemId;
	}
	public Float getTotalPremium() {
		return totalPremium;
	}
	public void setTotalPremium(Float totalPremium) {
		this.totalPremium = totalPremium;
	}
	public Float getMedicalOnlyPremium() {
		return medicalOnlyPremium;
	}
	public void setMedicalOnlyPremium(Float medicalOnlyPremium) {
		this.medicalOnlyPremium = medicalOnlyPremium;
	}
	public Float getDentalOnlyPremium() {
		return dentalOnlyPremium;
	}
	public void setDentalOnlyPremium(Float dentalOnlyPremium) {
		this.dentalOnlyPremium = dentalOnlyPremium;
	}
	public Float getMedicalBasePremium() {
		return medicalBasePremium;
	}
	public void setMedicalBasePremium(Float medicalBasePremium) {
		this.medicalBasePremium = medicalBasePremium;
	}
	public String getType1() {
		return type1;
	}
	public void setType1(String type1) {
		this.type1 = type1;
	}
	public String getMethod1() {
		return method1;
	}
	public void setMethod1(String method1) {
		this.method1 = method1;
	}
	public Date getDate1() {
		return date1;
	}
	public void setDate1(Date date1) {
		this.date1 = date1;
	}
	public Float getAmt1() {
		return amt1;
	}
	public void setAmt1(Float amt1) {
		this.amt1 = amt1;
	}
	public String getType2() {
		return type2;
	}
	public void setType2(String type2) {
		this.type2 = type2;
	}
	public String getMethod2() {
		return method2;
	}
	public void setMethod2(String method2) {
		this.method2 = method2;
	}
	public Date getDate2() {
		return date2;
	}
	public void setDate2(Date date2) {
		this.date2 = date2;
	}
	public Float getAmt2() {
		return amt2;
	}
	public void setAmt2(Float amt2) {
		this.amt2 = amt2;
	}
	public String getType3() {
		return type3;
	}
	public void setType3(String type3) {
		this.type3 = type3;
	}
	public String getMethod3() {
		return method3;
	}
	public void setMethod3(String method3) {
		this.method3 = method3;
	}
	public Date getDate3() {
		return date3;
	}
	public void setDate3(Date date3) {
		this.date3 = date3;
	}
	public Float getAmt3() {
		return amt3;
	}
	public void setAmt3(Float amt3) {
		this.amt3 = amt3;
	}
	public String getType4() {
		return type4;
	}
	public void setType4(String type4) {
		this.type4 = type4;
	}
	public String getMethod4() {
		return method4;
	}
	public void setMethod4(String method4) {
		this.method4 = method4;
	}
	public Date getDate4() {
		return date4;
	}
	public void setDate4(Date date4) {
		this.date4 = date4;
	}
	public Float getAmt4() {
		return amt4;
	}
	public void setAmt4(Float amt4) {
		this.amt4 = amt4;
	}
	public String getMethod5() {
		return method5;
	}
	public void setMethod5(String method5) {
		this.method5 = method5;
	}
	public Date getDate5() {
		return date5;
	}
	public void setDate5(Date date5) {
		this.date5 = date5;
	}
	public Float getAmt5() {
		return amt5;
	}
	public void setAmt5(Float amt5) {
		this.amt5 = amt5;
	}
	public String getType6() {
		return type6;
	}
	public void setType6(String type6) {
		this.type6 = type6;
	}
	public String getMethod6() {
		return method6;
	}
	public void setMethod6(String method6) {
		this.method6 = method6;
	}
	public Date getDate6() {
		return date6;
	}
	public void setDate6(Date date6) {
		this.date6 = date6;
	}
	public Float getAmt6() {
		return amt6;
	}
	public void setAmt6(Float amt6) {
		this.amt6 = amt6;
	}
	public String getNote1() {
		return note1;
	}
	public void setNote1(String note1) {
		this.note1 = note1;
	}
	public String getNote2() {
		return note2;
	}
	public void setNote2(String note2) {
		this.note2 = note2;
	}
	public String getNote3() {
		return note3;
	}
	public void setNote3(String note3) {
		this.note3 = note3;
	}
	public String getNote4() {
		return note4;
	}
	public void setNote4(String note4) {
		this.note4 = note4;
	}
	public String getNote5() {
		return note5;
	}
	public void setNote5(String note5) {
		this.note5 = note5;
	}
	public String getNote6() {
		return note6;
	}
	public void setNote6(String note6) {
		this.note6 = note6;
	}
	public char getSubsidyIndicator() {
		return SubsidyIndicator;
	}
	public void setSubsidyIndicator(char subsidyIndicator) {
		SubsidyIndicator = subsidyIndicator;
	}
	public static ObjectMapper getMapper() {
		return mapper;
	}

	public static void setMapper(ObjectMapper mapper) {
		BobFeedExcelModel.mapper = mapper;
	}

	private static ObjectMapper mapper = new ObjectMapper();
	
	public String toJson() {
		String bobAppJson = "";
		try {
			bobAppJson = mapper.writeValueAsString(this);
		} catch (Exception excp) {
			LOGGER.error("Exception in converting toJSON" , excp);
			bobAppJson = excp.getMessage();
		}
		return bobAppJson;
	}
	
	}
