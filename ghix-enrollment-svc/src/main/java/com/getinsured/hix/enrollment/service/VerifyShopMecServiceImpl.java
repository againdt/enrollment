package com.getinsured.hix.enrollment.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.DatePeriodType;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.MECVerificationCodeType;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.NonESIMECIndividualResponseType;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.NonESIMECResponseType;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.OrganizationCodeSimpleType;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.SourceInformationType;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.VerifyNonESIMECRequest;
import com.getinsured.hix.webservice.enrollment.verifyshopmec.VerifyNonESIMECResponse;

/**
 * @author Priya
 * @since 05/22/2014
 * 
 */

@Service("VerifyShopMecService")
@Transactional
public class VerifyShopMecServiceImpl implements VerifyShopMecService {	
	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyShopMecServiceImpl.class);
	
	@Autowired private EnrolleeService enrolleeService;
	
	@SuppressWarnings("deprecation")
	@Override
	public VerifyNonESIMECResponse verifyShopMec(VerifyNonESIMECRequest verifyNonESIMECRequest,AccountUser user)throws GIException{
		VerifyNonESIMECResponse verifyNonESIMECResponse = new VerifyNonESIMECResponse();
		if((verifyNonESIMECRequest != null) && (verifyNonESIMECRequest.getNonESIMECRequest().getNonESIMECindividualInformation() != null )){
			String ssn = verifyNonESIMECRequest.getNonESIMECRequest().getNonESIMECindividualInformation().getPersonSSN();
			XMLGregorianCalendar startDateXMLGregorianCalendar = verifyNonESIMECRequest.getNonESIMECRequest().getNonESIMECindividualInformation().getRequestedInsurancePeriod().getStartDate();
			XMLGregorianCalendar endDateXMLGregorianCalendar = verifyNonESIMECRequest.getNonESIMECRequest().getNonESIMECindividualInformation().getRequestedInsurancePeriod().getEndDate();
			GregorianCalendar startDateGreorianCalendar = startDateXMLGregorianCalendar.toGregorianCalendar();
			GregorianCalendar endDateGreorianCalendar = endDateXMLGregorianCalendar.toGregorianCalendar();
			TimeZone timezone = TimeZone.getDefault();
			startDateGreorianCalendar.setTimeZone(timezone);
			endDateGreorianCalendar.setTimeZone(timezone);	
			List<Enrollment> enrollmentList = null;
            // Verufy id the start date and End date are in within valid range
			Calendar now = TSCalendar.getInstance();		
			  // add 3 year from the calendar
			 now.add(Calendar.YEAR, +EnrollmentConstants.THREE);
			 Calendar future = TSCalendar.getInstance();		
			  // substract 3 year from the calendar
			 future.add(Calendar.YEAR, -EnrollmentConstants.THREE);
			 Calendar coverageEndDateCal = TSCalendar.getInstance();
			 coverageEndDateCal.setTime(endDateGreorianCalendar.getTime());
			 Calendar coverageStartDateCal = TSCalendar.getInstance();
			 coverageStartDateCal.setTime(startDateGreorianCalendar.getTime());	
			 Boolean isValidDates = true; 
			 if(coverageEndDateCal.get(Calendar.YEAR) == now.get(Calendar.YEAR) || coverageStartDateCal.get(Calendar.YEAR) == future.get(Calendar.YEAR))
			 {
				    isValidDates = false; 
					NonESIMECResponseType nonESIMECResponseType = new NonESIMECResponseType();
					NonESIMECIndividualResponseType nonESIMECIndividualResponseType = new NonESIMECIndividualResponseType();
					SourceInformationType sourceInformationType = new SourceInformationType();
					nonESIMECIndividualResponseType.setOrganizationCode(OrganizationCodeSimpleType.BOTH);
					nonESIMECIndividualResponseType.setPersonSSN(ssn);
					sourceInformationType.setMECVerificationCode(MECVerificationCodeType.X);
					nonESIMECResponseType.setNonESIMECIndividualResponse(nonESIMECIndividualResponseType);
					verifyNonESIMECResponse.setNonESIMECResponse(nonESIMECResponseType);
					verifyNonESIMECResponse.getNonESIMECResponse().setResponseCode("5600");
					verifyNonESIMECResponse.getNonESIMECResponse().setResponseDescription("Invalid Date Range ­ From Date or To Date");
					LOGGER.info("Invalid Date Range ­ From Date or To Date.");
			 }		
			 else
			 {
				 enrollmentList = enrolleeService.findActiveEnrollmentBySSN(ssn);		  	   
				for(Enrollment enrollmentData : enrollmentList){
					if(enrollmentData != null					
						 && (startDateGreorianCalendar.getTime().after(enrollmentData.getBenefitEffectiveDate()) || endDateGreorianCalendar.getTime().before(enrollmentData.getBenefitEndDate()))){
							isValidDates = false;
				   }
				}
				
				if(isValidDates){
					 enrollmentList = enrolleeService.findActiveEnrollmentBySSNAndCoverageDates(ssn,startDateGreorianCalendar.getTime(),endDateGreorianCalendar.getTime());
				}
			 }	
		    if(isValidDates)  {
				if(enrollmentList != null && !enrollmentList.isEmpty()){
					NonESIMECResponseType nonESIMECResponseType = new NonESIMECResponseType();
					NonESIMECIndividualResponseType nonESIMECIndividualResponseType = new NonESIMECIndividualResponseType();
					SourceInformationType sourceInformationType = new SourceInformationType();
					List<DatePeriodType> listDateType = new ArrayList<DatePeriodType>();
					for(Enrollment enrollment : enrollmentList){
					if(enrollment != null){
						String orgCode = enrollment.getEnrollmentTypeLkp().getLookupValueCode();
						nonESIMECIndividualResponseType.setPersonSSN(ssn);
						DatePeriodType datePeriodType = new DatePeriodType();
						XMLGregorianCalendar startDate;
						XMLGregorianCalendar endDate;
						try {
							startDate = DateToXMLGregorianCalendar(enrollment.getBenefitEffectiveDate());
							endDate = DateToXMLGregorianCalendar(enrollment.getBenefitEndDate());
							datePeriodType.setStartDate(startDate);
							datePeriodType.setEndDate(endDate);
						} catch (ParseException e) {
							LOGGER.error(e.getMessage());
						} catch (DatatypeConfigurationException e) {
							LOGGER.error(e.getMessage() , e);
						}
						listDateType.add(datePeriodType);
						sourceInformationType.setMECVerificationCode(MECVerificationCodeType.X);				  
						if(orgCode.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
							nonESIMECIndividualResponseType.setOrganizationCode(OrganizationCodeSimpleType.SHOP);
						
							
						}
						 else if(orgCode.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
							nonESIMECIndividualResponseType.setOrganizationCode(OrganizationCodeSimpleType.INDX);
							
						}								
							
					}				
						
					
				  }
					 sourceInformationType.getMECEligibilityPeriod().addAll(listDateType);
					 nonESIMECIndividualResponseType.setSourceInformation(sourceInformationType);
				     nonESIMECResponseType.setNonESIMECIndividualResponse(nonESIMECIndividualResponseType);
				   	 verifyNonESIMECResponse.setNonESIMECResponse(nonESIMECResponseType);
					 verifyNonESIMECResponse.getNonESIMECResponse().setResponseCode("0000");
					 verifyNonESIMECResponse.getNonESIMECResponse().setResponseDescription("Business Transaction Successful");	
					 LOGGER.info("verifyNonESIMECResponse formed successfully.");
					
				}
				else{
					SourceInformationType sourceInformationType = new SourceInformationType();
					NonESIMECResponseType nonESIMECResponseType = new NonESIMECResponseType();
					NonESIMECIndividualResponseType nonESIMECIndividualResponseType = new NonESIMECIndividualResponseType();
					// add org code "Both"
					nonESIMECIndividualResponseType.setOrganizationCode(OrganizationCodeSimpleType.BOTH);
					nonESIMECIndividualResponseType.setPersonSSN(ssn);
					sourceInformationType.setMECVerificationCode(MECVerificationCodeType.X);
					nonESIMECResponseType.setNonESIMECIndividualResponse(nonESIMECIndividualResponseType);
					verifyNonESIMECResponse.setNonESIMECResponse(nonESIMECResponseType);
					verifyNonESIMECResponse.getNonESIMECResponse().setResponseCode("8888");
					verifyNonESIMECResponse.getNonESIMECResponse().setResponseDescription("No eligibility information found based on date range");
					LOGGER.info("No eligibility information found based on date range.");	
				}
		    }
			if(!isValidDates){
				NonESIMECResponseType nonESIMECResponseType = new NonESIMECResponseType();
				NonESIMECIndividualResponseType nonESIMECIndividualResponseType = new NonESIMECIndividualResponseType();
				SourceInformationType sourceInformationType = new SourceInformationType();
				nonESIMECIndividualResponseType.setOrganizationCode(OrganizationCodeSimpleType.BOTH);
				nonESIMECIndividualResponseType.setPersonSSN(ssn);
				sourceInformationType.setMECVerificationCode(MECVerificationCodeType.X);
				nonESIMECResponseType.setNonESIMECIndividualResponse(nonESIMECIndividualResponseType);
				verifyNonESIMECResponse.setNonESIMECResponse(nonESIMECResponseType);
				verifyNonESIMECResponse.getNonESIMECResponse().setResponseCode("5600");
				verifyNonESIMECResponse.getNonESIMECResponse().setResponseDescription("Invalid Date Range ­ From Date or To Date");
				LOGGER.info("Invalid Date Range ­ From Date or To Date.");
			}
		}
		return verifyNonESIMECResponse;
	}
	
	public static XMLGregorianCalendar DateToXMLGregorianCalendar(
			Date date) throws ParseException,
			DatatypeConfigurationException {
		if (date == null) { 
			return null;
		} else {		
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		DateFormat outputFormat = new SimpleDateFormat("MMM-dd-yyyy");
		String outputString = outputFormat.format(inputFormat.parse(date.toString()));
		outputFormat.setLenient(false);
	    Date formattedDate = outputFormat.parse(outputString);
	   	GregorianCalendar gDate = new GregorianCalendar();
	    gDate.setTime(formattedDate);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gDate);
		}		
	}
	
	public static Date stringToCalendarDate(String dateString) throws ParseException,DatatypeConfigurationException {
		if ((dateString == null) || (dateString.equalsIgnoreCase(""))) { 
			return null;
		} else {
			SimpleDateFormat simpleDateFormat = null;
			simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");	
			Date date = simpleDateFormat.parse(dateString);
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			return cal.getTime();
		}
	}
}
