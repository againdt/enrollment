/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentRequest;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentResponse;

/**
 * @author raja
 * @since 07/11/2013
 * This method dis-enrolls the employer enrollment.
 * 
 * @param employerDisEnrollmentRequest
 * @param isInternal.
 * @return EmployerDisEnrollmentResponse
 * @throws GIException
 */
public interface EnrollmentEmployerDisEnrollService {
	EmployerDisEnrollmentResponse employerDisEnrollment(EmployerDisEnrollmentRequest employerDisEnrollmentRequest, boolean isInternal) throws GIException;	
}
