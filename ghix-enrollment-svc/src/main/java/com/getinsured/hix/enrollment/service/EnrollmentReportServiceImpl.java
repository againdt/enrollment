package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentEventHistoryDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventAudRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.util.DateUtil;

@Service("enrollmentReportService")
public class EnrollmentReportServiceImpl implements EnrollmentReportService
{
	@Autowired private IEnrollmentEventAudRepository iEnrollmentEventAudRepository;

	@Override
	public List<EnrollmentEventHistoryDTO> getEnrollmentEventHistoryDTOList(EnrollmentRequest enrollmentRequest)
	{
		List<EnrollmentEventHistoryDTO> enrollmentEventHistoryDTOList  = new ArrayList<EnrollmentEventHistoryDTO>();
		List<String> personTypeList = new ArrayList<String>();
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON);
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT);
		personTypeList.add(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT);
		List<Object[]> enrollmentEvenList = new ArrayList<Object[]>();
		
		/*
		 * EnrollmentEvent Without Special Enrollment
		 */
		List<Object[]> enrollmentEventDataList = iEnrollmentEventAudRepository.getEnrollmentEventHistoryData(enrollmentRequest.getEnrollmentId(), personTypeList);

		
		/*
		 * EnrollmentEvent With Special Enrollment
		 */
		List<Object[]> spclEnrollmentEventDataList = iEnrollmentEventAudRepository.getSpclEnrollmentEventHistoryData(enrollmentRequest.getEnrollmentId(), personTypeList);
			
		if(enrollmentEventDataList != null && !enrollmentEventDataList.isEmpty())
		{
			enrollmentEvenList.addAll(enrollmentEventDataList);
		}
		if(spclEnrollmentEventDataList != null && !spclEnrollmentEventDataList.isEmpty())
		{
			enrollmentEvenList.addAll(spclEnrollmentEventDataList);
		}
		
		/*
		 * Do sorting only when both the list are not null
		 */
		if(enrollmentEventDataList != null && spclEnrollmentEventDataList != null)
		{
			/*
			 *Sorting in ascending order by Created On date 
			 */
			Collections.sort(enrollmentEvenList, new Comparator<Object[]>() {
				@Override
				public int compare(Object[] objArray1, Object[] objArray2) {
					return (((Date)objArray1[EnrollmentConstants.ONE]).compareTo(((Date)objArray2[EnrollmentConstants.ONE])));
				}
			});
		}
		
		if(enrollmentEvenList != null && !enrollmentEvenList.isEmpty())
		{
			for(Object[] enrollmentEventData : enrollmentEvenList)
			{
				EnrollmentEventHistoryDTO enrollmentEventHistoryDTO = new EnrollmentEventHistoryDTO();
				enrollmentEventHistoryDTO.setRevisionId((Integer)enrollmentEventData[EnrollmentConstants.ZERO]);
				enrollmentEventHistoryDTO.setEventCreationDate(DateUtil.dateToString((java.util.Date)enrollmentEventData[EnrollmentConstants.ONE], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
				enrollmentEventHistoryDTO.setEventCreationTime(DateUtil.dateToString((java.util.Date)enrollmentEventData[EnrollmentConstants.ONE], EnrollmentConstants.TIME_FORMAT_HH_A));
				enrollmentEventHistoryDTO.setEnrollmentEventReason((String)enrollmentEventData[EnrollmentConstants.TWO]);
				enrollmentEventHistoryDTO.setEnrollmentEvent((String)enrollmentEventData[EnrollmentConstants.THREE]);
				enrollmentEventHistoryDTO.setEventCreatedBy(EnrollmentUtils.getFullName((String)enrollmentEventData[EnrollmentConstants.FOUR], null, (String)enrollmentEventData[EnrollmentConstants.FIVE]));
				enrollmentEventHistoryDTO.setEnrolleesName(EnrollmentUtils.getFullName((String)enrollmentEventData[EnrollmentConstants.SIX], (String)enrollmentEventData[EnrollmentConstants.SEVEN], (String)enrollmentEventData[EnrollmentConstants.EIGHT]));
				if(enrollmentEventData.length == EnrollmentConstants.TEN)
				{
					enrollmentEventHistoryDTO.setSpclEnrollmentReason((String)enrollmentEventData[EnrollmentConstants.NINE]);
				}
				enrollmentEventHistoryDTOList.add(enrollmentEventHistoryDTO);
			}
		}
		return enrollmentEventHistoryDTOList;
	}
}
