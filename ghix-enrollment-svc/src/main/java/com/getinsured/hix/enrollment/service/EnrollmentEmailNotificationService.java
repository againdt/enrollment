/**
 * 
 */

package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author raja
 * @since 23/09/2013
 */
public interface EnrollmentEmailNotificationService {
	
	void sendIRSNACKResponseFailureNotification(String responseCode,String responseDesc,String fileName ,String fileLocation, String action) throws GIException;
	void sendEnrollmentStatusNotification(List<Enrollment> enrollmentList,String termReasonCode) throws GIException;
	void sendEnrollmentReinstateNotification(List<Enrollment> enrollmentList) throws GIException;
	void sendEnrolledEmployeeHouseHoldInfoUpdateNotification(List<Enrollment> enrollmentList) throws GIException;
	void sendEmployeeEnrollmentStatusConfirmationNotification(List<Enrollment> enrollmentList) throws GIException;
	void sendIRSOutboundFailureNotification(String fileName ,String fileLocation, String action) throws GIException;
}
