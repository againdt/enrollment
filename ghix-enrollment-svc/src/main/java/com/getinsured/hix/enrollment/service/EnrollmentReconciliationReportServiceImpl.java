package com.getinsured.hix.enrollment.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrollmentInboundReconciliationReportRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentOutboundReconciliationReportRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentReconCSVDto;
import com.getinsured.hix.enrollment.util.EnrollmentReconciliationErrorInfo;
import com.getinsured.hix.enrollment.util.EnrollmentReconciliationInfo;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentInboundReconciliationReport;
import com.getinsured.hix.model.enrollment.EnrollmentOutboundReconciliationReport;
import com.getinsured.hix.model.enrollment.EnrollmentReconciliationReportList;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("enrollmentReconciliationReportService")
public class EnrollmentReconciliationReportServiceImpl implements EnrollmentReconciliationReportService{

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentReconciliationReportServiceImpl.class);
	@Autowired private IEnrollmentOutboundReconciliationReportRepository enrollmentOutboundReconciliationReportRepository;
	@Autowired private IEnrollmentInboundReconciliationReportRepository enrollmentInboundReconciliationReportRepository;
	@Autowired private LookupService lookupService;
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	private final static String PROCESS_INBOUND = "INBOUND";
	private final static String PROCESS_OUTBOUND = "OUTBOUND";
		
	
	
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public boolean serveEnrollmentBatchReconciliationJob(String fileName, String timeStamp, EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException {
		
		String wipFolderPath = null;
		String succesFolderPath = null;
		String failureFolderPath = null;
		List<File> currentFileList = null;
		boolean isSuccess = false;

		String absoluteFileName = null;
		String enrollmentReconReportPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_RECONCILIATION_REPORT_PATH) + File.separator + EnrollmentConstants.OUT_FOLDER_NAME;
		if(enrollmentReconReportPath != null){
			wipFolderPath = enrollmentReconReportPath + File.separator +EnrollmentConstants.WIP_FOLDER_NAME;
			succesFolderPath = enrollmentReconciliationInfo.getSuccessFolderPath();
			failureFolderPath = enrollmentReconciliationInfo.getFailureFolderPath();
		}
		EnrollmentReconciliationReportList enrollmentReconciliationReportList = null;
		try{
			if(fileName != null && timeStamp != null){
				currentFileList = new ArrayList<File>();
				File currentFile = new File(fileName);
				currentFileList.add(currentFile);
				absoluteFileName = currentFile.getName();
				enrollmentReconciliationReportList = getEnrollmentReconciliationReportListFromXML(currentFile);
			}

		}catch(Exception e){
			LOGGER.error("ENROLLMENT_834_RECON:: Error while parsing the file to Enrollments  = " + fileName, e);
			isSuccess = false;
		}

		try {
			if(enrollmentReconciliationReportList != null && enrollmentReconciliationReportList.getEnrRecReportList() != null && !enrollmentReconciliationReportList.getEnrRecReportList().isEmpty()){
				SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
				Date fileCreationDate = null;
				try {
					fileCreationDate = sf.parse(enrollmentReconciliationReportList.getFileCreationDate());
				} catch (ParseException e) {
					throw new GIException("Error Parsing file creation date");
				}
				for(EnrollmentOutboundReconciliationReport enrollmentReconciliationReport : enrollmentReconciliationReportList.getEnrRecReportList()){
					enrollmentReconciliationReport.setXmlFileName(absoluteFileName);
					enrollmentReconciliationReport.setXmlFileCreationDate(fileCreationDate);
				}
				//enrollmentOutboundReconciliationReportRepository.save(enrollmentReconciliationReportList.getEnrRecReportList());
				persistEnrollmentOutboundReconreport(enrollmentReconciliationReportList.getEnrRecReportList());
				isSuccess = true;
			}
			else{
				enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(fileName, "No enrollment details found in received file");				
			}
			
			if(isSuccess){
				EnrollmentUtils.moveFiles(wipFolderPath, succesFolderPath, EnrollmentConstants.FILE_TYPE_XML, currentFileList);
			}else{
				EnrollmentUtils.moveFiles(wipFolderPath, failureFolderPath, EnrollmentConstants.FILE_TYPE_XML, currentFileList);
				enrollmentReconciliationInfo.incrementTotalNumberOfBadXMLFilesReceived(EnrollmentConstants.ONE);
			}
		}
		catch (Exception e) {
			LOGGER.error("ENROLLMENT_834_RECON:: Error Saving enrollmentReconciliationReportList from file :: " + fileName, e);
			isSuccess = false;
			EnrollmentUtils.moveFiles(wipFolderPath, failureFolderPath, EnrollmentConstants.FILE_TYPE_XML, currentFileList);
			enrollmentReconciliationInfo.incrementTotalNumberOfBadXMLFilesReceived(EnrollmentConstants.ONE);
		}
		return isSuccess;
	}
	
	/**
	 * @author Aditya-S
	 * @since 05/02/2015 (DD/MM/YYYY)
	 * 
	 * @param xmlFile
	 * @return
	 * @throws GIException
	 */
	
	private EnrollmentReconciliationReportList getEnrollmentReconciliationReportListFromXML(File xmlFile) throws GIException{
		EnrollmentReconciliationReportList enrollmentReconciliationReportList = null;
		InputStream is = null;
		try(InputStream strXSLPath = EnrollmentReconciliationReportServiceImpl.class.getClassLoader().getResourceAsStream("RevEnrollmentTransReconciliation.xsl");)
		{			
			if(xmlFile!=null){
				/*JAXBContext context = JAXBContext.newInstance(EnrollmentReconciliationReportList.class);
				Unmarshaller unMarshaller = context.createUnmarshaller();*/
				String strXML=null;
				TransformerFactory factory = null;
				
				factory = TransformerFactory.newInstance();
				Source xslSource = new StreamSource(strXSLPath);
				StreamSource xmlJAXB = new StreamSource(xmlFile);
				Templates template = factory.newTemplates(xslSource);
				Transformer transformer = template.newTransformer();

				StringWriter writerXSLT = new StringWriter();
				StreamResult resultXSLT = new StreamResult(writerXSLT);
				transformer.transform(xmlJAXB, resultXSLT);

				strXML = writerXSLT.toString();

				LOGGER.info("getEnrollmentReconciliationReportListFromXML XML TO UPDATE : " + strXML);

				if(strXML == null){
					LOGGER.error("Error in getEnrollmentReconciliationReportListFromXML():: XSLT Transformation Returns NULL");
					throw new GIException("Error in getEnrollmentReconciliationReportListFromXML():: XSLT Transformation Returns NULL");                          
				}

				is = new ByteArrayInputStream(strXML.getBytes());
				enrollmentReconciliationReportList = (EnrollmentReconciliationReportList) GhixUtils.inputStreamToObject(is, EnrollmentReconciliationReportList.class);
			}
		}catch(Exception e){
			LOGGER.error("ENROLLMENT_834_RECON:: Exception at getEnrollmentsFromXML ::",e);
			throw new GIException("JAXBException in getEnrollmentReconciliationReportListFromXML():: "+e.getMessage(),e);  
		}finally{
			IOUtils.closeQuietly(is);
		}
		return enrollmentReconciliationReportList;
	}

	@Transactional(rollbackFor=Exception.class)
	@Override
	public boolean serveEnrollmentBatchCSVReconciliationJob(String fileName, String timeStamp, EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException {
		String wipFolderPath = null;
		String succesFolderPath = null;
		String failureFolderPath = null;
		List<File> currentFileList = null;
		boolean isSuccess = false;
		boolean isGoodEdi = false;

		String enrollmentReconReportPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_RECONCILIATION_REPORT_PATH) + File.separator + EnrollmentConstants.OUT_FOLDER_NAME;
		if(enrollmentReconReportPath != null){
			wipFolderPath = enrollmentReconReportPath + File.separator +EnrollmentConstants.WIP_FOLDER_NAME+"_EDI";
			succesFolderPath = enrollmentReconciliationInfo.getSuccessFolderPath();
			failureFolderPath = enrollmentReconciliationInfo.getFailureFolderPath();
		}
		if(fileName != null && timeStamp != null){
			if(fileName.contains(EnrollmentConstants.GOOD_EDI)){
				isGoodEdi =  Boolean.TRUE;
			}
			
			try {
				File csvFile = new File(fileName);
				currentFileList =  new ArrayList<File>();
				currentFileList.add(csvFile);
				
				List<EnrollmentReconCSVDto> reconCSVList = processCSVFile(csvFile, EnrollmentConstants.FILETYPE_EDI, enrollmentReconciliationInfo, PROCESS_OUTBOUND);
				List<EnrollmentOutboundReconciliationReport> finalEnrollmentOutReconReportList = new ArrayList<EnrollmentOutboundReconciliationReport>() ;
			
				if(null != reconCSVList && !reconCSVList.isEmpty()){
					for(EnrollmentReconCSVDto dto : reconCSVList){
						//Query reconciliation table
						List<EnrollmentOutboundReconciliationReport> enrollmentOutReconReportList = enrollmentOutboundReconciliationReportRepository.findByEnrollmentIdAndSubscriberEventIdAndFileName(dto.getEnrollmentId(), dto.getEnrollmentEventId(), dto.getBatchExecutionId());
						if(null != enrollmentOutReconReportList && !enrollmentOutReconReportList.isEmpty()){
							for(EnrollmentOutboundReconciliationReport enrollmentReconciliationReport :  enrollmentOutReconReportList){
								
								enrollmentReconciliationReport.setHiosIssuerId(dto.getHiosIssuerId());
								enrollmentReconciliationReport.setExchReconId(dto.getExchgReconId());
								enrollmentReconciliationReport.setIsa09(dto.getIsa09());
								enrollmentReconciliationReport.setIsa10(dto.getIsa10());
								enrollmentReconciliationReport.setIsa13(dto.getIsa13());
								enrollmentReconciliationReport.setGs04(dto.getGs04());
								enrollmentReconciliationReport.setGs05(dto.getGs05());
								enrollmentReconciliationReport.setGs06(dto.getGs06());
								enrollmentReconciliationReport.setSt02(null != dto.getSt02() ? dto.getSt02().toString() : null);
								enrollmentReconciliationReport.setSubscriberLineNo(dto.getSubscriberLineNo());
								enrollmentReconciliationReport.setIns03EventTypeCode(dto.getIns03EventTypeCode());
								enrollmentReconciliationReport.setIns04EventReasonCode(dto.getIns04EventReasonCode());
								enrollmentReconciliationReport.setHouseHoldCaseId(dto.getHouseHoldCaseId());
								
								if(isGoodEdi){
									enrollmentReconciliationReport.setEdiStatus(EnrollmentConstants.SUCCESS);
								}
								else{
									enrollmentReconciliationReport.setEdiStatus(EnrollmentConstants.FAILURE);
								}
							}
							finalEnrollmentOutReconReportList.addAll(enrollmentOutReconReportList);
						}
						else{
							enrollmentReconciliationInfo.incrementTotalNumberOfBadEDIRecordReceived(EnrollmentConstants.ONE);
							EnrollmentReconciliationErrorInfo enrollmentReconciliationErrorInfo = new EnrollmentReconciliationErrorInfo();
							enrollmentReconciliationErrorInfo.setErrorRecord(dto.getReceivedRecord());
							enrollmentReconciliationErrorInfo.setFailureReason("No such record found in Database");
							enrollmentReconciliationInfo.putToEnrollmentReconciliationErrorInfoMap(fileName, enrollmentReconciliationErrorInfo);
						}
					}
					//enrollmentOutboundReconciliationReportRepository.save(finalEnrollmentOutReconReportList);
					persistEnrollmentOutboundReconreport(finalEnrollmentOutReconReportList);
					
					Map<Integer, Enrollment> enrollmentMap = getEnrollmentData(finalEnrollmentOutReconReportList);
					if(enrollmentMap != null && !enrollmentMap.isEmpty()){
						for(Enrollment  objEnrollment : enrollmentMap.values()){
							//enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(objEnrollment, EnrollmentConstants.SENT_834);
							if(isGoodEdi){
								enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(objEnrollment, EnrollmentConstants.EnrollmentAppEvent.OUTBOUND_834_SENT.toString(), null);
							}
							else{
								enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(objEnrollment, EnrollmentConstants.EnrollmentAppEvent.OUTBOUND_834_EDI_ERROR.toString(), null);
							}
						}
					}
					isSuccess = true;
				}
			} catch (Exception e) {
				LOGGER.error("ENROLLMENT_834_RECON:: Error processing CSV file or corrupt CSV :: " + fileName, e);
				isSuccess = false;
				enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(fileName, EnrollmentUtils.shortenedStackTrace(e, EnrollmentConstants.FIVE));
			}
		}
		if(isSuccess){
			EnrollmentUtils.moveFiles(wipFolderPath, succesFolderPath, EnrollmentConstants.FILE_TYPE_CSV, currentFileList);
		}else{
			EnrollmentUtils.moveFiles(wipFolderPath, failureFolderPath, EnrollmentConstants.FILE_TYPE_CSV, currentFileList);
			enrollmentReconciliationInfo.incrementTotalNumberOfBadEDIRecordReceived(EnrollmentConstants.ONE);
		}
		return isSuccess;
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public boolean processEnrollmentReconTrackingFiles(String fileName, String timeStamp, EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException {
		String wipFolderPath = null;
		String succesFolderPath = null;
		String failureFolderPath = null;
		List<File> currentFileList = null;
		String eventType = null;
		String fileType = null;
		boolean isSuccess = false;
		if(fileName != null && timeStamp != null){
			String enrollmentReconReportPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_RECONCILIATION_REPORT_PATH) + File.separator + EnrollmentConstants.OUT_FOLDER_NAME;
			if(enrollmentReconReportPath != null){
				wipFolderPath = enrollmentReconReportPath + File.separator +EnrollmentConstants.WIP_FOLDER_NAME+"_TA1_999";
				succesFolderPath = enrollmentReconciliationInfo.getSuccessFolderPath();
				failureFolderPath = enrollmentReconciliationInfo.getFailureFolderPath();
			}
			try{
				File csvFile = new File(fileName);
				currentFileList =  new ArrayList<File>();
				currentFileList.add(csvFile);
				 if(fileName.contains(EnrollmentConstants.FILETYPE_TA1_TRACKING)){
					 fileType = EnrollmentConstants.FILETYPE_TA1_TRACKING;
				 }
				 else if(fileName.contains(EnrollmentConstants.FILETYPE_999_TRACKING)){
					 fileType = EnrollmentConstants.FILETYPE_999_TRACKING;
				 }
				
				List<EnrollmentReconCSVDto> reconTrackingList = processCSVFile(csvFile, fileType, enrollmentReconciliationInfo, PROCESS_OUTBOUND);
				List<EnrollmentOutboundReconciliationReport> finalEnrollmentOutReconReportList = new ArrayList<EnrollmentOutboundReconciliationReport>();
				if(reconTrackingList != null && !reconTrackingList.isEmpty()){
					for(EnrollmentReconCSVDto dto : reconTrackingList){
						List<EnrollmentOutboundReconciliationReport> enrollmentOutReconReportList = null;
						if(fileType.equals(EnrollmentConstants.FILETYPE_TA1_TRACKING)){
							eventType = EnrollmentConstants.EnrollmentAppEvent.OUTBOUND_834_TA1_ACK_RECEIVED.toString(); 
							enrollmentOutReconReportList = enrollmentOutboundReconciliationReportRepository.findReconRcordForTA1(dto.getHiosIssuerId(), dto.getIsa13(), dto.getIsa10());
							if(enrollmentOutReconReportList != null && !enrollmentOutReconReportList.isEmpty()){
								for(EnrollmentOutboundReconciliationReport enrollmentReconciliationReport :  enrollmentOutReconReportList){
									//HIOS_ISSUER_ID*EXCHG_RECON_ID*ISA09*ISA10*ISA13*ACK_CODE
									enrollmentReconciliationReport.setAckTa1ReceivedDate(new TSDate());
									enrollmentReconciliationReport.setAckTa1Status(dto.getAckCode());
								}
								finalEnrollmentOutReconReportList.addAll(enrollmentOutReconReportList);
							}
							else{
								enrollmentReconciliationInfo.incrementTotalNumberOfBadTrackingRecordReceived(EnrollmentConstants.ONE);
								//No Record found with the given search Criteria
								EnrollmentReconciliationErrorInfo enrollmentReconciliationErrorInfo = new EnrollmentReconciliationErrorInfo();
								enrollmentReconciliationErrorInfo.setErrorRecord(dto.getReceivedRecord());
								enrollmentReconciliationErrorInfo.setFailureReason("No such record found in Database");
								enrollmentReconciliationInfo.putToEnrollmentReconciliationErrorInfoMap(fileName, enrollmentReconciliationErrorInfo);
							}
						}
						if(fileType.equals(EnrollmentConstants.FILETYPE_999_TRACKING)){
							eventType = EnrollmentConstants.EnrollmentAppEvent.OUTBOUND_834_999_ACK_RECEIVED.toString();
							if(dto.getAckLevel().equalsIgnoreCase("IK5"))
							{
								enrollmentOutReconReportList = enrollmentOutboundReconciliationReportRepository.findReconRecordForIk5(dto.getHiosIssuerId(),dto.getIsa13(),dto.getGs04(),dto.getGs05(),dto.getGs06(), String.valueOf(dto.getSt02()));
							}
							else if(dto.getAckLevel().equalsIgnoreCase("AK9")){
								enrollmentOutReconReportList = enrollmentOutboundReconciliationReportRepository.findReconRecordForAk9(dto.getHiosIssuerId(),dto.getIsa13(),dto.getGs04(),dto.getGs05(),dto.getGs06());
							}
							if(enrollmentOutReconReportList != null && !enrollmentOutReconReportList.isEmpty()){
								for(EnrollmentOutboundReconciliationReport enrollmentReconciliationReport :  enrollmentOutReconReportList){
									enrollmentReconciliationReport.setAck999ReceivedDate(new TSDate());
									enrollmentReconciliationReport.setAck999Status(dto.getAckCode());
								}
								finalEnrollmentOutReconReportList.addAll(enrollmentOutReconReportList);
							}
							else{
								enrollmentReconciliationInfo.incrementTotalNumberOfBadTrackingRecordReceived(EnrollmentConstants.ONE);
								//No record found for the search criteria
								EnrollmentReconciliationErrorInfo enrollmentReconciliationErrorInfo = new EnrollmentReconciliationErrorInfo();
								enrollmentReconciliationErrorInfo.setErrorRecord(dto.getReceivedRecord());
								enrollmentReconciliationErrorInfo.setFailureReason("No such record found in Database");
								enrollmentReconciliationInfo.putToEnrollmentReconciliationErrorInfoMap(fileName, enrollmentReconciliationErrorInfo);
							}
						}
					}
					//enrollmentOutboundReconciliationReportRepository.save(finalEnrollmentOutReconReportList);
					persistEnrollmentOutboundReconreport(finalEnrollmentOutReconReportList);
					isSuccess = true;
					Map<Integer, Enrollment> enrollmentMap = getEnrollmentData(finalEnrollmentOutReconReportList);
					
					if(enrollmentMap!= null && !enrollmentMap.isEmpty() && StringUtils.isNotEmpty(eventType)){
						for(Enrollment  objEnrollment : enrollmentMap.values()){
							//enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(objEnrollment, EnrollmentConstants.SENT_834);
							enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(objEnrollment, eventType, null);
						}
					}
				}
			}
			catch(Exception ex){
				LOGGER.error("ENROLLMENT_834_RECON:: Exception occurred while processing Tracking file: ",ex);
				isSuccess = false;
				enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(fileName, EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			}
				if(isSuccess){
					EnrollmentUtils.moveFiles(wipFolderPath, succesFolderPath, fileType, currentFileList);
				}else{
					EnrollmentUtils.moveFiles(wipFolderPath, failureFolderPath, fileType, currentFileList);
					enrollmentReconciliationInfo.incrementTotalNumberOfBadTrackingRecordReceived(EnrollmentConstants.ONE);
				}
			}
		return isSuccess;
	}
	
	/**
	 * Get Household case Id by querying enrollment table
	 * @param enrollmentId
	 * @param enrollmentHouseholdMap 
	 * @return String returnValue
	 */
	private String getHouseholdCaseIdAndInsurerName(Integer enrollmentId, Map<Integer, String> enrollmentHouseholdMap, String paramater) {
		String householdCaseId = null;
		String insurerName  = null;
		String resultSet = null; 
		String returnValue = null;
		if(enrollmentHouseholdMap.containsKey(enrollmentId)){
			resultSet	=  enrollmentHouseholdMap.get(enrollmentId);
			householdCaseId = resultSet.split(",")[0];
			insurerName = resultSet.split(",")[1];
		}else{
			List<Object[]> obj = enrollmentRepository.getHouseholdCaseIdAndInsurerName(enrollmentId);
			if(null != obj && !obj.isEmpty() && obj.get(0).length>1){
				householdCaseId = (String)obj.get(0)[0];
				insurerName = (String)obj.get(0)[1];
				resultSet = householdCaseId+","+insurerName;
				enrollmentHouseholdMap.put(enrollmentId, resultSet);
			}
		}
		if("H".equalsIgnoreCase(paramater)){
			returnValue = householdCaseId;
		}else{
			returnValue = insurerName;
		}
		return returnValue;
	}

	private List<EnrollmentReconCSVDto> processCSVFile(File file, String fileType, EnrollmentReconciliationInfo enrollmentReconciliationInfo, final String processType) throws IOException {
		List<EnrollmentReconCSVDto> reconDTOList = new ArrayList<EnrollmentReconCSVDto>();
		if(null != file && file.isFile()){
			List<String> csvRecord = Files.readAllLines(file.toPath(),Charset.defaultCharset());
			if(csvRecord.size()>1) {
				int counter = 0;
				for(String delimitedString : csvRecord){
					if(counter++ == 0){
						continue;
					}
					try {
						if (fileType.equalsIgnoreCase(EnrollmentConstants.FILETYPE_EDI)) {
							reconDTOList.add(populateEnrollmentReconCSVDto(delimitedString, processType));
						} else if (fileType.equalsIgnoreCase(EnrollmentConstants.FILETYPE_TA1_TRACKING)) {
							reconDTOList.add(populateEnrollmentReconTA1Record(delimitedString));
						} else if (fileType.equalsIgnoreCase(EnrollmentConstants.FILETYPE_999_TRACKING)) {
							reconDTOList.add(populateEnrollmentRecon999Record(delimitedString));
						}
					} catch (GIException ex) {
						enrollmentReconciliationInfo.incrementCounterByFileType(EnrollmentConstants.ONE, fileType);
						EnrollmentReconciliationErrorInfo enrollmentReconciliationErrorInfo = new EnrollmentReconciliationErrorInfo();
						enrollmentReconciliationErrorInfo.setErrorRecord(delimitedString);
						enrollmentReconciliationErrorInfo.setFailureReason(ex.getMessage());
						enrollmentReconciliationInfo.putToEnrollmentReconciliationErrorInfoMap(file.getName(), enrollmentReconciliationErrorInfo);
					}
				}
			}else{
				LOGGER.info("ENROLLMENT_834_RECON:: No Reconciliation Data found in file: "+file.getName());
			}
		}
		return reconDTOList;		
	}
	
	private EnrollmentReconCSVDto populateEnrollmentReconCSVDto(String delimitedString, final String processType)throws GIException{
		/**
		 * @author panda_p
		 * Added more columns (ISA09, ISA10, GS04 and GS05) to OUT_RECORN table as per  HIX-85673,HIX-85674
		 */
		
		 //HIOS_ISSUER_ID*EXCHG_RECON_ID*ISA09*ISA10*ISA13*GS04*GS05*GS06*ST02*SUBSCRIBER_LINENO
		 //*ENROLLMENT_ID*ENROLLMENT_EVENT_ID*INS03_EVENTTYPECODE*INS04_EVENTREASONCODE
		 //*HOUSEHOLD_CASE_ID*SOC_SEC_LAST_4*JOB_EXECUTION_ID
		
		String[] record = delimitedString.split(EnrollmentConstants.CSV_DELIMITER);
		
		EnrollmentReconCSVDto enrlReconCSVDto =  new EnrollmentReconCSVDto();
		if(record != null){
			if(processType.equals(PROCESS_OUTBOUND) && record.length != EnrollmentConstants.SEVENTEEN){
				throw new GIException("EDI OUTBOUND RECORD: Either received record is null or Some of the required fields are missing");
			}
			/*if(processType.equals(PROCESS_INBOUND) && record.length != EnrollmentConstants.SIXTEEN){
				throw new GIException("EDI INBOUND RECORD: Either received record is null or Some of the fields are missing");
			}*/
			try{
				enrlReconCSVDto.setReceivedRecord(delimitedString);
				if(record.length > EnrollmentConstants.ZERO){
					enrlReconCSVDto.setHiosIssuerId(record[EnrollmentConstants.ZERO]);
				}
				if(record.length > EnrollmentConstants.ONE && StringUtils.isNotBlank(record[EnrollmentConstants.ONE])){
					enrlReconCSVDto.setExchgReconId(Integer.valueOf(record[EnrollmentConstants.ONE]));
				}
				if(record.length > EnrollmentConstants.TWO){
					enrlReconCSVDto.setIsa09(record[EnrollmentConstants.TWO]);
				}
				if(record.length > EnrollmentConstants.THREE){
					enrlReconCSVDto.setIsa10(record[EnrollmentConstants.THREE]);
				}
				if(record.length > EnrollmentConstants.FOUR && StringUtils.isNotBlank(record[EnrollmentConstants.FOUR])){
					enrlReconCSVDto.setIsa13(Integer.valueOf(record[EnrollmentConstants.FOUR]));
				}
				if(record.length > EnrollmentConstants.FIVE){
					enrlReconCSVDto.setGs04(record[EnrollmentConstants.FIVE]);
				}
				if(record.length > EnrollmentConstants.SIX){
					enrlReconCSVDto.setGs05(record[EnrollmentConstants.SIX]);
				}
				if(record.length > EnrollmentConstants.SEVEN && StringUtils.isNotBlank(record[EnrollmentConstants.SEVEN])){
					enrlReconCSVDto.setGs06(Integer.valueOf(record[EnrollmentConstants.SEVEN]));
				}
				if(record.length > EnrollmentConstants.EIGHT && StringUtils.isNotBlank(record[EnrollmentConstants.EIGHT])){
					enrlReconCSVDto.setSt02(Integer.valueOf(record[EnrollmentConstants.EIGHT]));
				}
				if(record.length > EnrollmentConstants.NINE){
					enrlReconCSVDto.setSubscriberLineNo(record[EnrollmentConstants.NINE]);
				}
				if(record.length > EnrollmentConstants.TEN && StringUtils.isNotBlank(record[EnrollmentConstants.TEN])){
					enrlReconCSVDto.setEnrollmentId(Integer.valueOf(record[EnrollmentConstants.TEN]));
				}
				if(record.length > EnrollmentConstants.ELEVEN &&  StringUtils.isNotBlank(record[EnrollmentConstants.ELEVEN])){
					enrlReconCSVDto.setEnrollmentEventId(Integer.valueOf(record[EnrollmentConstants.ELEVEN]));
				}
				if(record.length > EnrollmentConstants.TWELVE){
					enrlReconCSVDto.setIns03EventTypeCode(record[EnrollmentConstants.TWELVE]);
				}
				if(record.length > EnrollmentConstants.THIRTEEN){
					enrlReconCSVDto.setIns04EventReasonCode(record[EnrollmentConstants.THIRTEEN]);
				}
				if(record.length > EnrollmentConstants.FOURTEEN){
					enrlReconCSVDto.setHouseHoldCaseId(record[EnrollmentConstants.FOURTEEN]);
				}
				if(record.length > EnrollmentConstants.SIXTEEN && StringUtils.isNotBlank(record[EnrollmentConstants.SIXTEEN])){
					enrlReconCSVDto.setBatchExecutionId(Long.valueOf(record[EnrollmentConstants.SIXTEEN]));
				}
				
				//Validating out-bound record 
				if(processType.equals(PROCESS_OUTBOUND) && !isValidEDIRecord(enrlReconCSVDto)){
					LOGGER.error("EDI OUTBOUND Record Validation field : "+
							"\n EnrollmentId : " + enrlReconCSVDto.getEnrollmentId()+
							"\n EnrollmentEventId : "+enrlReconCSVDto.getEnrollmentEventId()+
							"\n BatchExecutionId : "+ enrlReconCSVDto.getBatchExecutionId());
					throw new GIException(" EDI OUTBOUND Record Validation - Either required field value is empty or null");
				}
			}
			catch(Exception ex){
				if(processType.equals(PROCESS_OUTBOUND)){
					LOGGER.error("ENROLLMENT_834_RECON OUTBOUND :: Exception occurred while processing inbound record: ", ex);
					throw new GIException("Exception occurred while processing OUTBOUND record "+EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
				}else if(processType.equals(PROCESS_INBOUND)) {
					LOGGER.error("ENROLLMENT_834_RECON INBOUND:: Exception occurred while processing inbound record: ", ex);
					throw new GIException("Exception occurred while processing INBOUND record "+EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
				}else {
					LOGGER.error("ENROLLMENT_834_RECON:: Exception occurred while processing inbound record: ", ex);
					throw new GIException("Exception occurred while processing record "+EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
				}
			}
		}
		return enrlReconCSVDto;
	}
	
	private EnrollmentReconCSVDto populateEnrollmentReconTA1Record(String delimitedString)throws GIException{
		//ACK_LEVEL*HIOS_ISSUER_ID*EXCHG_RECON_ID*ISA09*ISA10*ISA13*GS04*GS05*GS06*ST02*ACK_CODE
		//TA1*61589*1305*150225*0035*000010012*****000
		
		String[] record = delimitedString.split(EnrollmentConstants.CSV_DELIMITER);
		EnrollmentReconCSVDto enrlReconCSVDto =  new EnrollmentReconCSVDto();
		//ACK_LEVEL*HIOS_ISSUER_ID*EXCHG_RECON_ID*ISA09*ISA10*ISA13*ACK_CODE
		enrlReconCSVDto.setReceivedRecord(delimitedString);
		if(record != null && record.length == EnrollmentConstants.ELEVEN){
			String ackLevel = record[EnrollmentConstants.ZERO];
			if(StringUtils.isNotBlank(ackLevel) && ackLevel.equalsIgnoreCase("TA1")){
				try{
					enrlReconCSVDto.setAckLevel(ackLevel);
					enrlReconCSVDto.setHiosIssuerId(record[EnrollmentConstants.ONE]);
					if(StringUtils.isNotBlank(record[EnrollmentConstants.TWO])){
					enrlReconCSVDto.setExchgReconId(Integer.valueOf(record[EnrollmentConstants.TWO]));
					}
					enrlReconCSVDto.setIsa09(record[EnrollmentConstants.THREE]);
					enrlReconCSVDto.setIsa10(record[EnrollmentConstants.FOUR]);
					if(StringUtils.isNotBlank(record[EnrollmentConstants.FIVE])){
					enrlReconCSVDto.setIsa13(Integer.valueOf(record[EnrollmentConstants.FIVE]));
					}
					enrlReconCSVDto.setAckCode(record[EnrollmentConstants.TEN]);
					if(!isValidTA1TrackingRecord(enrlReconCSVDto)){
						throw new GIException("TA1 RECORD: Out of received fields, Some required fields value is empty or null");
					}
				}
				catch(Exception ex){
					LOGGER.error("ENROLLMENT_834_RECON:: Exception occurred while processing TA1 record: ",ex);
					throw new GIException("Exception occurred while processing TA1 record "+EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
				}
			}
			else{
				throw new GIException("Received invalid ACK level in TA1 tracking file, AckLevel: "+ackLevel); 
			}
		}
		else{
			throw new GIException("Either received record is null or Some of the fields are missing");
		}
		return enrlReconCSVDto;
	}
	
	private EnrollmentReconCSVDto populateEnrollmentRecon999Record(String delimitedString)throws GIException{
		String[] record = delimitedString.split(EnrollmentConstants.CSV_DELIMITER);
		EnrollmentReconCSVDto enrlReconCSVDto =  new EnrollmentReconCSVDto();
		//ACK_LEVEL*HIOS_ISSUER_ID*EXCHG_RECON_ID*ISA09*ISA10*ISA13*GS04*GS05*GS06*ST02*ACK_CODE
		//IK5*61589*1305*150225*0035*000010012*20150225*0035*10012*000000004*A
		enrlReconCSVDto.setReceivedRecord(delimitedString);
		if(record != null && record.length == EnrollmentConstants.ELEVEN){
			try{
				String ackLevel = record[EnrollmentConstants.ZERO];
				if(StringUtils.isNotBlank(ackLevel) && (ackLevel.equalsIgnoreCase("IK5") || ackLevel.equalsIgnoreCase("AK9"))){
					enrlReconCSVDto.setAckLevel(ackLevel);
					enrlReconCSVDto.setHiosIssuerId(record[EnrollmentConstants.ONE]);
					if(StringUtils.isNotBlank(record[EnrollmentConstants.TWO])){
					enrlReconCSVDto.setExchgReconId(Integer.valueOf(record[EnrollmentConstants.TWO]));
					}
					enrlReconCSVDto.setIsa09(record[EnrollmentConstants.THREE]);
					enrlReconCSVDto.setIsa10(record[EnrollmentConstants.FOUR]);
					if(StringUtils.isNotBlank(record[EnrollmentConstants.FIVE])){
					enrlReconCSVDto.setIsa13(Integer.valueOf(record[EnrollmentConstants.FIVE]));
					}
					enrlReconCSVDto.setGs04(record[EnrollmentConstants.SIX]);
					enrlReconCSVDto.setGs05(record[EnrollmentConstants.SEVEN]);
					if(StringUtils.isNotBlank(record[EnrollmentConstants.EIGHT])){
					enrlReconCSVDto.setGs06(Integer.valueOf(record[EnrollmentConstants.EIGHT]));
					}
					
					if(ackLevel.equalsIgnoreCase("IK5")){
						enrlReconCSVDto.setSt02(Integer.valueOf(record[EnrollmentConstants.NINE]));
					}
					enrlReconCSVDto.setAckCode(record[EnrollmentConstants.TEN]);
					
					if(!isValid999TrackingRecord(enrlReconCSVDto)){
						throw new GIException("999 RECORD: Either received field value is empty or null");
					}
				}
				else{
					throw new GIException("Received Invalid ACK Level in 999 tracking file, AckLevel: "+ackLevel); 
				}
			}
			catch(Exception ex){
				LOGGER.error("ENROLLMENT_834_RECON:: Exception occurred while processing 999 record: ",ex);
				throw new GIException("Exception occurred while processing 999 record "+EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			}
		}
		return enrlReconCSVDto;
	}
	
	
	private boolean isValidEDIRecord(EnrollmentReconCSVDto enrlReconCSVDto){
		boolean isValidRecord = Boolean.TRUE;
		if(enrlReconCSVDto != null){
			if(enrlReconCSVDto.getEnrollmentId() == null  || (enrlReconCSVDto.getEnrollmentId() != null && enrlReconCSVDto.getEnrollmentId() == 0)) {
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && (enrlReconCSVDto.getEnrollmentEventId() == null || 
					(enrlReconCSVDto.getEnrollmentEventId() != null && enrlReconCSVDto.getEnrollmentEventId() == 0))){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && (enrlReconCSVDto.getBatchExecutionId() == null ||
					(enrlReconCSVDto.getBatchExecutionId() != null && enrlReconCSVDto.getBatchExecutionId() == 0))){
				isValidRecord = Boolean.FALSE;
			}
		}
		else{
			isValidRecord = Boolean.FALSE;
		}
		return isValidRecord;
	}
	
	private boolean isValidTA1TrackingRecord(EnrollmentReconCSVDto enrlReconCSVDto){
		boolean isValidRecord = Boolean.TRUE;
		if(enrlReconCSVDto != null){
			if(StringUtils.isBlank(enrlReconCSVDto.getHiosIssuerId())){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && (enrlReconCSVDto.getIsa13() == null || (enrlReconCSVDto.getIsa13() != null && enrlReconCSVDto.getIsa13() == 0))){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && StringUtils.isBlank(enrlReconCSVDto.getIsa10())){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && StringUtils.isBlank(enrlReconCSVDto.getAckCode())){
				isValidRecord = Boolean.FALSE;
			}
		}
		else{
			isValidRecord = Boolean.FALSE;
		}
		return isValidRecord;
	}
	
	private boolean isValid999TrackingRecord(EnrollmentReconCSVDto enrlReconCSVDto){
		boolean isValidRecord = Boolean.TRUE;
		if(enrlReconCSVDto != null){
			if(StringUtils.isBlank(enrlReconCSVDto.getHiosIssuerId())){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && (enrlReconCSVDto.getIsa13() == null || (enrlReconCSVDto.getIsa13() != null && enrlReconCSVDto.getIsa13() == 0))){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && StringUtils.isBlank(enrlReconCSVDto.getGs04())){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && StringUtils.isBlank(enrlReconCSVDto.getGs05())){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && (enrlReconCSVDto.getGs06() == null || (enrlReconCSVDto.getGs06() != null  && enrlReconCSVDto.getGs06() == 0))){
				isValidRecord = Boolean.FALSE;
			}
			if(isValidRecord && StringUtils.isBlank(enrlReconCSVDto.getAckLevel())){
				isValidRecord = Boolean.FALSE;
			}
			else{
				if(enrlReconCSVDto.getAckLevel().equalsIgnoreCase("IK5") && (enrlReconCSVDto.getSt02() == null
						|| (enrlReconCSVDto.getSt02() != null && enrlReconCSVDto.getSt02() == 0))){
					isValidRecord = Boolean.FALSE;					
				}
			}
			if(isValidRecord && StringUtils.isBlank(enrlReconCSVDto.getAckCode())){
				isValidRecord = Boolean.FALSE;
			}
		}
		else{
			isValidRecord = Boolean.FALSE;
		}
		return isValidRecord;
	}

	@Override
	public boolean processEnrollmentReconInboundTrackingFiles(String fileName, String timeStamp,
			EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException{
		String wipFolderPath = null;
		String succesFolderPath = null;
		String failureFolderPath = null;
		List<File> currentFileList = null;
		String eventType = null;
		String fileType = null;
		boolean isSuccess = false;
		if(fileName != null && timeStamp != null){
			String enrollmentReconReportPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_RECONCILIATION_REPORT_PATH) + File.separator + EnrollmentConstants.IN_FOLDER_NAME;
			if(enrollmentReconReportPath != null){
				wipFolderPath = enrollmentReconReportPath + File.separator +EnrollmentConstants.WIP_FOLDER_NAME+"_TRACKING_FILES";
				succesFolderPath = enrollmentReconciliationInfo.getSuccessFolderPath();
				failureFolderPath = enrollmentReconciliationInfo.getFailureFolderPath();
			}
			try{
				File csvFile = new File(fileName);
				currentFileList =  new ArrayList<File>();
				currentFileList.add(csvFile);
				if(fileName.contains(EnrollmentConstants.FILETYPE_TA1_TRACKING)){
					fileType = EnrollmentConstants.FILETYPE_TA1_TRACKING;
					eventType = EnrollmentConstants.EnrollmentAppEvent.INBOUND_834_TA1_ACK_RECEIVED.toString();
				}else if(fileName.contains(EnrollmentConstants.FILETYPE_999_TRACKING)){
					fileType = EnrollmentConstants.FILETYPE_999_TRACKING;
					eventType = EnrollmentConstants.EnrollmentAppEvent.INBOUND_834_999_ACK_RECEIVED.toString();
				}else if(fileName.contains(EnrollmentConstants.BAD_EDI)){
					fileType = EnrollmentConstants.FILETYPE_EDI;
				}

				List<EnrollmentReconCSVDto> reconTrackingList = processCSVFile(csvFile, fileType, enrollmentReconciliationInfo, PROCESS_INBOUND);
				List<EnrollmentInboundReconciliationReport> finalEnrollmentInReconReportList = new ArrayList<EnrollmentInboundReconciliationReport>();
				if(reconTrackingList != null && !reconTrackingList.isEmpty()){
					for(EnrollmentReconCSVDto dto : reconTrackingList){
						List<EnrollmentInboundReconciliationReport> enrollmentInReconReportList = null;
						
						if(fileType.equals(EnrollmentConstants.FILETYPE_EDI)){
							EnrollmentInboundReconciliationReport enrollmentReconciliationReport = new EnrollmentInboundReconciliationReport();
							enrollmentReconciliationReport.setEnrollmentId(dto.getEnrollmentId());
							enrollmentReconciliationReport.setExchReconId(dto.getExchgReconId());
							enrollmentReconciliationReport.setHiosIssuerId(dto.getHiosIssuerId());
							enrollmentReconciliationReport.setHouseHoldCaseId(dto.getHouseHoldCaseId());
							enrollmentReconciliationReport.setBatchExecutionId(dto.getBatchExecutionId());
							enrollmentReconciliationReport.setIsa09(dto.getIsa09());
							enrollmentReconciliationReport.setIsa10(dto.getIsa10());
							enrollmentReconciliationReport.setIsa13(dto.getIsa13());
							enrollmentReconciliationReport.setGs06(dto.getGs06());
							enrollmentReconciliationReport.setGs04(dto.getGs04());
							enrollmentReconciliationReport.setGs05(dto.getGs05());
							enrollmentReconciliationReport.setSt02((null != dto.getSt02())? dto.getSt02().toString():null);
							enrollmentReconciliationReport.setEdiStatus(EnrollmentConstants.FAILURE);

							finalEnrollmentInReconReportList.add(enrollmentReconciliationReport);
						}
						
						if(fileType.equals(EnrollmentConstants.FILETYPE_TA1_TRACKING)){
							enrollmentInReconReportList = enrollmentInboundReconciliationReportRepository.findReconRcordForTA1(dto.getHiosIssuerId(), dto.getIsa13(), dto.getIsa10());
							if(enrollmentInReconReportList != null && !enrollmentInReconReportList.isEmpty()){
								for(EnrollmentInboundReconciliationReport enrollmentReconciliationReport :  enrollmentInReconReportList){
									//HIOS_ISSUER_ID*EXCHG_RECON_ID*ISA09*ISA10*ISA13*ACK_CODE
									enrollmentReconciliationReport.setAckTA1ReceivedDate(new TSDate());
									enrollmentReconciliationReport.setAckTA1Status(dto.getAckCode());
								}
								finalEnrollmentInReconReportList.addAll(enrollmentInReconReportList);
							}
							else{
								enrollmentReconciliationInfo.incrementTotalNumberOfBadTrackingRecordReceived(EnrollmentConstants.ONE);
								//No Record found with the given search Criteria
								EnrollmentReconciliationErrorInfo enrollmentReconciliationErrorInfo = new EnrollmentReconciliationErrorInfo();
								enrollmentReconciliationErrorInfo.setErrorRecord(dto.getReceivedRecord());
								enrollmentReconciliationErrorInfo.setFailureReason("No such record found in Database");
								enrollmentReconciliationInfo.putToEnrollmentReconciliationErrorInfoMap(fileName, enrollmentReconciliationErrorInfo);
							}
						}
						if(fileType.equals(EnrollmentConstants.FILETYPE_999_TRACKING)){
							if(dto.getAckLevel().equalsIgnoreCase("IK5"))
							{
								enrollmentInReconReportList = enrollmentInboundReconciliationReportRepository.findReconRecordForIk5(dto.getHiosIssuerId(),dto.getIsa13(),dto.getGs04(),dto.getGs05(),dto.getGs06(),String.valueOf(dto.getSt02()));
							}
							else if(dto.getAckLevel().equalsIgnoreCase("AK9")){
								enrollmentInReconReportList = enrollmentInboundReconciliationReportRepository.findReconRecordForAk9(dto.getHiosIssuerId(),dto.getIsa13(),dto.getGs04(),dto.getGs05(),dto.getGs06());
							}
							if(enrollmentInReconReportList != null && !enrollmentInReconReportList.isEmpty()){
								for(EnrollmentInboundReconciliationReport enrollmentReconciliationReport :  enrollmentInReconReportList){
									enrollmentReconciliationReport.setAck999ReceivedDate(new TSDate());
									enrollmentReconciliationReport.setAck999Status(dto.getAckCode());
								}
								finalEnrollmentInReconReportList.addAll(enrollmentInReconReportList);
							}
							else{
								enrollmentReconciliationInfo.incrementTotalNumberOfBadTrackingRecordReceived(EnrollmentConstants.ONE);
								//No record found for the search criteria
								EnrollmentReconciliationErrorInfo enrollmentReconciliationErrorInfo = new EnrollmentReconciliationErrorInfo();
								enrollmentReconciliationErrorInfo.setErrorRecord(dto.getReceivedRecord());
								enrollmentReconciliationErrorInfo.setFailureReason("No such record found in Database");
								enrollmentReconciliationInfo.putToEnrollmentReconciliationErrorInfoMap(fileName, enrollmentReconciliationErrorInfo);
							}
						}
					}
					//enrollmentInboundReconciliationReportRepository.save(finalEnrollmentInReconReportList);
					persistEnrollmentInboundReconreport(finalEnrollmentInReconReportList);
					isSuccess = true;
					Map<Integer, Enrollment> enrollmentMap = getEnrollmentData(finalEnrollmentInReconReportList);
					if(enrollmentMap != null && !enrollmentMap.isEmpty()){
						for(Enrollment  objEnrollment : enrollmentMap.values()){
							//enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(objEnrollment, EnrollmentConstants.SENT_834);
							if(fileType.equals(EnrollmentConstants.FILETYPE_EDI)){
								enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(objEnrollment, EnrollmentConstants.EnrollmentAppEvent.INBOUND_834_EDI_ERROR.toString(), null);
							}
							else if((fileType.equals(EnrollmentConstants.FILETYPE_999_TRACKING) ||
									fileType.equals(EnrollmentConstants.FILETYPE_TA1_TRACKING)) && StringUtils.isNotEmpty(eventType)){
								enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(objEnrollment, eventType, null);
							}
						}
					}
				}
			}catch(Exception ex){
				LOGGER.error("ENROLLMENT_834_RECON:: Exception occurred while processing Tracking file: ",ex);
				isSuccess = false;
				enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(fileName, EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
			}
					
				if(isSuccess){
					EnrollmentUtils.moveFiles(wipFolderPath, succesFolderPath, fileType, currentFileList);
				}else{
					EnrollmentUtils.moveFiles(wipFolderPath, failureFolderPath, fileType, currentFileList);
					enrollmentReconciliationInfo.incrementTotalNumberOfBadTrackingRecordReceived(EnrollmentConstants.ONE);
				}
		}
		return isSuccess;
	}
	
	private Map<Integer, Enrollment> getEnrollmentData(List<?> finalEnrollmentOutReconReportList){
		Map<Integer, Enrollment> enrollmentMap = new LinkedHashMap<Integer, Enrollment>();
		Map<Integer,String> enrollmentHouseholdMap = new HashMap<Integer, String>();
		if(finalEnrollmentOutReconReportList != null && !finalEnrollmentOutReconReportList.isEmpty()){
			
			for(Object enrollmentReconObj :  finalEnrollmentOutReconReportList){
				if(enrollmentReconObj instanceof EnrollmentOutboundReconciliationReport){
					EnrollmentOutboundReconciliationReport enrollmentReconciliationReport = (EnrollmentOutboundReconciliationReport)enrollmentReconObj;
					if(!enrollmentMap.containsKey(enrollmentReconciliationReport.getEnrollmentId())){
						Enrollment en= new Enrollment();
						en.setId(enrollmentReconciliationReport.getEnrollmentId());
						en.setInsurerName(getHouseholdCaseIdAndInsurerName(enrollmentReconciliationReport.getEnrollmentId(), enrollmentHouseholdMap, "I"));
						en.setHouseHoldCaseId(getHouseholdCaseIdAndInsurerName(enrollmentReconciliationReport.getEnrollmentId(), enrollmentHouseholdMap, "H"));
						en.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE,EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL));
						en.setFileName834(enrollmentReconciliationReport.getXmlFileName());
						enrollmentMap.put(enrollmentReconciliationReport.getEnrollmentId(), en);
					}
				}
				else if(enrollmentReconObj instanceof EnrollmentInboundReconciliationReport){
					EnrollmentInboundReconciliationReport enrollmentInboundReconciliationReport = (EnrollmentInboundReconciliationReport) enrollmentReconObj;
					if(!enrollmentMap.containsKey(enrollmentInboundReconciliationReport.getEnrollmentId())){
						Enrollment en= new Enrollment();
						en.setId(enrollmentInboundReconciliationReport.getEnrollmentId());
						en.setInsurerName(getHouseholdCaseIdAndInsurerName(enrollmentInboundReconciliationReport.getEnrollmentId(), enrollmentHouseholdMap, "I"));
						en.setHouseHoldCaseId(getHouseholdCaseIdAndInsurerName(enrollmentInboundReconciliationReport.getEnrollmentId(), enrollmentHouseholdMap, "H"));
						en.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE,EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL));
						en.setFileName834(enrollmentInboundReconciliationReport.getXmlFileName());
						enrollmentMap.put(enrollmentInboundReconciliationReport.getEnrollmentId(), en);
					}
				}
			}
		}
		
		return enrollmentMap;
	}
	
	/**
	 * 
	 * @param enrollmentOutboundReconciliationReportList
	 * @return
	 */
	private List<EnrollmentOutboundReconciliationReport> persistEnrollmentOutboundReconreport(List<EnrollmentOutboundReconciliationReport> enrollmentOutboundReconciliationReportList){
		List<EnrollmentOutboundReconciliationReport> persistedEnrollmentOutboundReconciliationReportList = null;
		if(enrollmentOutboundReconciliationReportList != null && !enrollmentOutboundReconciliationReportList.isEmpty()){
			persistedEnrollmentOutboundReconciliationReportList = new ArrayList<EnrollmentOutboundReconciliationReport>();
			for(EnrollmentOutboundReconciliationReport enrollmentOutboundReconciliationReportObj: enrollmentOutboundReconciliationReportList){
				try{
					persistedEnrollmentOutboundReconciliationReportList.add(enrollmentOutboundReconciliationReportRepository.saveAndFlush(enrollmentOutboundReconciliationReportObj));
				}
				catch(Exception ex){
					LOGGER.error("ENROLLMENT_834_RECON:: Exception caught while saving outbound report data: ",ex);
				}
			}
		}
		return persistedEnrollmentOutboundReconciliationReportList;
	}
	
	/**
	 * 
	 * @param enrollmentOutboundReconciliationReportList
	 * @return
	 */
	private List<EnrollmentInboundReconciliationReport> persistEnrollmentInboundReconreport(List<EnrollmentInboundReconciliationReport> enrollmentInboundReconciliationReportList){
		List<EnrollmentInboundReconciliationReport> persistedEnrollmentInboundReconciliationReportList = null;
		if(enrollmentInboundReconciliationReportList != null && !enrollmentInboundReconciliationReportList.isEmpty()){
			persistedEnrollmentInboundReconciliationReportList = new ArrayList<EnrollmentInboundReconciliationReport>();
			for(EnrollmentInboundReconciliationReport enrollmentInboundReconciliationReportObj: enrollmentInboundReconciliationReportList){
				try{
					persistedEnrollmentInboundReconciliationReportList.add(enrollmentInboundReconciliationReportRepository.saveAndFlush(enrollmentInboundReconciliationReportObj));
				}
				catch(Exception ex){
					LOGGER.error("ENROLLMENT_834_RECON:: Exception caught while saving Inbound report data: ",ex);
				}
			}
		}
		return persistedEnrollmentInboundReconciliationReportList;
	}
}
