package com.getinsured.hix.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.dto.enrollment.EnrolleeCoreDto;
import com.getinsured.hix.dto.enrollment.EnrollmentCoreDto;
import com.getinsured.hix.dto.enrollment.EnrollmentCoverageValidationRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentCoverageValidationRequest.InsuranceType;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberCoverageDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateRequest;
import com.getinsured.hix.dto.enrollment.LocationCoreDto;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.dto.externalassister.DesignateAssisterResponse;
import com.getinsured.hix.dto.planmgmt.PlanListResponse;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.shop.EmployerDTO;
import com.getinsured.hix.dto.shop.RestEmployeeDetailsDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.enrollment.repository.IEnrlHouseholdRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEsignatureRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerEnrollment;
import com.getinsured.hix.model.EmployerEnrollment.Status;
import com.getinsured.hix.model.Esignature;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEsignature;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync.RecordType;
import com.getinsured.hix.model.plandisplay.PldMemberData;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.model.plandisplay.PldPlanData;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.BrokerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.request.ssap.SsapApplicationRequest;
import com.getinsured.iex.request.ssap.SsapApplicationResponse;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.thoughtworks.xstream.XStream;

@Service("enrollmentCreationService")
public class EnrollmentCreationServiceImpl implements EnrollmentCreationService{
	
	@Autowired private UserService userService;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IEnrolleeRepository enrolleeRepository;
	@Autowired private EnrolleeService enrolleeService;
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired private LookupService lookupService;
	@Autowired private EnrollmentRequotingService enrollmentRequotingService;
	@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;
	@Autowired private GhixRestTemplate restTemplate;
	@Autowired private EnrolleeRaceService enrolleeRaceService;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private IEnrollmentEsignatureRepository enrollmentEsignatureRepository;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentAhbxSyncService enrollmentAhbxSyncService;
	@Autowired private IEnrlHouseholdRepository enrlHouseholdRepository;
	
	private static final List<String> DISENROLLMENT_EVENTS = Arrays.asList(
			EnrollmentConstants.EnrollmentAppEvent.PLAN_CHANGE_DISENROLLMENT.toString(),
			EnrollmentConstants.EnrollmentAppEvent.CS_LEVEL_CHANGE_DISENROLLMENT.toString(),
			EnrollmentConstants.EnrollmentAppEvent.SUBSCRIBER_CHANGE_DISENROLLMENT.toString(),
			EnrollmentConstants.EnrollmentAppEvent.OTHER_DISENROLLMENT.toString());
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCreationServiceImpl.class);

	private static final String OVERLAP_SQL_QUERY_STRING = "SELECT COUNT(*) AS COUNT "
			+ " FROM ENROLLEE ee, ENROLLMENT en, lookup_value insuranceTypeLkp, lookup_value statusLkp "
			+ " WHERE ee.ENROLLMENT_ID       =en.id "
			+ " AND en.INSURANCE_TYPE_LKP  = insuranceTypeLkp.lookup_value_id "
			+ " AND ee.ENROLLEE_STATUS_LKP = statusLkp.lookup_value_id "
			+ " AND (statusLkp.lookup_value_code IN ('PENDING' , 'CONFIRM' , 'TERM' , 'PAYMENT_RECEIVED')) "
			+ " AND insuranceTypeLkp.lookup_value_code   =:INSURANCE_TYPE_LKP "
			+ " AND to_date(:START_DATE, 'MM/DD/YYYY')  <= to_date(TO_CHAR(ee.EFFECTIVE_END_DATE, 'MM/DD/YYYY'), 'MM/DD/YYYY') "
			+ " AND to_date(:END_DATE, 'MM/DD/YYYY')    >= to_date(TO_CHAR(ee.EFFECTIVE_START_DATE, 'MM/DD/YYYY'), 'MM/DD/YYYY') "
			+ " AND ee.EXCHG_INDIV_IDENTIFIER =:EXCHG_INDIV_IDENTIFIER ";

	@Override
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public EnrollmentResponse createSpecialEnrollment(PldOrderResponse pldOrderResponse, String applicant_esig,
			String onBehalfActionCheckbox, RecordType recordType) throws GIException {
		
		LOGGER.info("Reached Special Enrollment Creation method inside service impl class");
		AccountUser loggedInUser = this.getLoggedInUser();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		List<PldPlanData> planDataList = new ArrayList<PldPlanData>();
		planDataList = pldOrderResponse.getPlanDataList();
		Character enrollmentReason = pldOrderResponse.getEnrollmentType();
		String houseHoldCaseID = pldOrderResponse.getHouseholdCaseId();
		/*boolean terminationFlag = false;	
		boolean dentalTerminationFlag= false;*/
		String marketType = null;
		String employeeId = pldOrderResponse.getEmployeeId();
		String serc= pldOrderResponse.getSerc();
		Long applicationId = pldOrderResponse.getApplicationId();
		Set<Long> oldSSAPIds= new HashSet<>();
		String autoRenewalFlag= null;
		int newEnrollmentKey = 0;
		List<Integer> termEnrollmentsDueToPlanFlip = new ArrayList<>();
		
		try {
			if (applicationId != null) {
				String applicationStatus = enrollmentExternalRestUtil.getApplicationStatusById(applicationId);
				if (applicationStatus.equalsIgnoreCase("CL")) {
					throw new GIException(
							"The Application is in closed status for application id " + applicationId);
				}
			}
		} catch (GIException e) {
			LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage(), e);
			LOGGER.error(EnrollmentConstants.CAUSE + e.getCause(), e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Application Closed" + e.getStackTrace());
			enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_ENSSAP01);
			return enrollmentResponse;
		}
		
		if(isNotNullAndEmpty(pldOrderResponse.getAutoRenewal())){
			autoRenewalFlag=pldOrderResponse.getAutoRenewal().toString();
		}
		Integer pdHouseholdId=null;
		if(isNotNullAndEmpty(pldOrderResponse.getPdHouseholdId())){
			pdHouseholdId=Integer.parseInt(pldOrderResponse.getPdHouseholdId().trim());
        }
		
		/*if(isNotNullAndEmpty(pldOrderResponse.getTerminationFlag()) && EnrollmentConstants.Y.equalsIgnoreCase(pldOrderResponse.getTerminationFlag())){
			terminationFlag=true;
		}
		if(isNotNullAndEmpty(pldOrderResponse.getDentalTerminationFlag()) && EnrollmentConstants.Y.equalsIgnoreCase(pldOrderResponse.getDentalTerminationFlag())){
			dentalTerminationFlag=true;
		}*/
		List<String> disEnrolledMemberIds = new ArrayList<String>();
		
		/***** Maintain Map of Enrollments and EnrollmentId *****/
		Map<Integer, Enrollment> enrollmentMap = new LinkedHashMap<Integer, Enrollment>();

		/*****  DisEnrollMemberList needs to set in the EnrollmentResponse, This would be displayed in the order confirm page. *****/
		List<Map<String, String>> disEnrollMemberList = new ArrayList<Map<String, String>>();
		List<Map<String,String>> newMembersToExistingEnrollment= new ArrayList<Map<String, String>>();
		Map<Integer, String> enrollmentExistingStatusMaP = new HashMap<>();

		if(!isNotNullAndEmpty(planDataList)){
			enrollmentResponse.setErrMsg("planDataList received is null");
			return enrollmentResponse;
		}

		/***** Individual Member Data for Disenrolling *****/
		List<PldMemberData> disenrollMemberInfoList = pldOrderResponse.getDisenrollMemberInfoList();

		Map<String, Boolean> termFlagMap= new HashMap<>();
		/***** Disenroll individual members for Enrollment *****/
		/***** Loop through the disEnroll loop and pass each member for disenrolling *****/
		if(disenrollMemberInfoList != null && !disenrollMemberInfoList.isEmpty()){
			for(PldMemberData disEnrollMemberData : disenrollMemberInfoList){
				List<String> enrollmentIds = disEnrollMemberData.getEnrollmentIds();
				for(String stringEnrollmentId : enrollmentIds){
					Integer disEnrollmentId = Integer.valueOf(stringEnrollmentId);
					if(!enrollmentMap.containsKey(disEnrollmentId)){
						Enrollment e = enrollmentRepository.findOne(disEnrollmentId);
						oldSSAPIds.add(e.getSsapApplicationid());
						if(!termFlagMap.containsKey(stringEnrollmentId)){
							if(e.getEnrollmentStatusLkp()!=null && EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(e.getEnrollmentStatusLkp().getLookupValueCode())){
								termFlagMap.put(stringEnrollmentId, true);
							}else{
								termFlagMap.put(stringEnrollmentId, false);
							}
						}
						if(isNotNullAndEmpty(pldOrderResponse.getGlobalId())){
							 e.setLogGlobalId(pldOrderResponse.getGlobalId());	
							}
						if(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL.equalsIgnoreCase(e.getEnrollmentTypeLkp().getLookupValueCode())){
							e.setPriorSsapApplicationid(applicationId);
							e.setSsapApplicationid(applicationId);	
						}
						e.setPdHouseholdId(pdHouseholdId);
						e.setUpdatedBy(loggedInUser);
						enrollmentMap.put(disEnrollmentId, e);
						if(!enrollmentExistingStatusMaP.containsKey(e.getId())){
							enrollmentExistingStatusMaP.put(e.getId(), e.getEnrollmentStatusLkp().getLookupValueCode());
						}
					}
				}
				List<Map<String, String>> disEnrolledMemberList = disEnrollMemberForSpecialEnrollment(disEnrollMemberData, employeeId,enrollmentMap,disEnrolledMemberIds,serc, termFlagMap, loggedInUser, pldOrderResponse.getAllowChangePlan());
				disEnrollMemberList.addAll(disEnrolledMemberList);
			}
		}
		Map<String,List<Map<String, String>>> finalDisEnrollmentMemberMap = new HashMap<String, List<Map<String, String>>>();
		finalDisEnrollmentMemberMap.put(houseHoldCaseID,disEnrollMemberList);
		enrollmentResponse.setFinalDisEnrollmentMemberMap(finalDisEnrollmentMemberMap);
		/***** Begin special Enrollment i.e. Update existing enrollment/enrollees or creating new enrollment in case change in plan. *****/
		
		PlanListResponse planListResponse = enrollmentExternalRestUtil.getPlanListInfo(pldOrderResponse.getPlans(),pldOrderResponse.getMarketType());
		for (PldPlanData pdlPlan : planDataList) {
			/***** Plan Data *****/
			Map<String, String> plan = pdlPlan.getPlan();
			boolean terminationFlag=pdlPlan.isTerminationFlag();
			/***** Individual Member Data *****/
			List<PldMemberData> newMemberList = pdlPlan.getNewMemberInfoList();
			List<PldMemberData> existingMemberList= pdlPlan.getExistingMemberInfoList();
			
			Integer existingEnrollmentId = null;
			try{
				if(isNotNullAndEmpty(pdlPlan.getExistingEnrollmentId())){
					existingEnrollmentId = Integer.valueOf(pdlPlan.getExistingEnrollmentId());
				}
			}
			catch(NumberFormatException e){
				LOGGER.error(e.getMessage(),e);
			}
			catch(Exception e){
				LOGGER.error(e.getMessage(),e);
			}
			
			/**Code to handle MRC-29  - Override Enrollment coverageStartDate if Enrollment is In Pending status**/
			String requestPlanId = plan.get(EnrollmentConstants.PLAN_ID);
			Date sepEffDate =null;
			String coverageStartDate = plan.get(EnrollmentConstants.COVERAGE_START_DATE);
			if (isNotNullAndEmpty(coverageStartDate)){
				String sepEffDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd hh:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
				sepEffDate =  DateUtil.StringToDate(sepEffDateStr, GhixConstants.REQUIRED_DATE_FORMAT);
			}
			 
			boolean overrideCoverageStartDate = false;
			boolean mrcFlag = false;
			Date newEffDate = null;
			if(EnrollmentConfiguration.isCaCall() && existingMemberList!=null && !existingMemberList.isEmpty()){
				
				if (isNotNullAndEmpty(coverageStartDate)){
					String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd hh:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
					newEffDate =  DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT); 
				}
				
				String existingMemberMRC =null;
				for (PldMemberData pldMemberData : existingMemberList) {
					Map<String, String> member = pldMemberData.getMemberInfo();
					
					existingMemberMRC = (String) member.get(EnrollmentConstants.MAINTENANCE_REASON_CODE);
					mrcFlag=true;
					break;
				}
				if(mrcFlag){
					try{
						Enrollment existingEnrollment =null;
						existingEnrollment = enrollmentRepository.findOne(existingEnrollmentId);
						
						if(isNotNullAndEmpty(existingMemberMRC) && existingMemberMRC.equalsIgnoreCase(EnrollmentConstants.REASON_CODE_ADMIN_CHANGE_29)){
							if(isNotNullAndEmpty(existingEnrollment)&& isNotNullAndEmpty(existingEnrollment.getEnrollmentStatusLkp()) 
									&& EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(existingEnrollment.getEnrollmentStatusLkp().getLookupValueCode())){
								/**MRR-29 and Enrollment in PENDING**/
								overrideCoverageStartDate=true;
							}else{
								/**MRR-29 and Enrollment in not in PENDING**/
								String existingEnrollmentPlanId = null;
								if(existingEnrollment.getPlanId()!=null){
									existingEnrollmentPlanId = ""+existingEnrollment.getPlanId();
									if(existingEnrollmentPlanId.equalsIgnoreCase(requestPlanId)){
										/**For Keep scenario, Through error */
										throw new Exception ("Invalid Enrollment Status for handing MRC 29, Keep scenario ");
									}else{
										/**For SHOP scenario, Continue with the Existing special enrollment flow.*/
										overrideCoverageStartDate=false;
									}
								}else{
									throw new Exception ("Invalid Enrollment Status for handing MRC 29");	
								}
							}
						}
					}catch (Exception e) {
						LOGGER.error("Failed process MRC- 29 Enrollment update  : " + e);
						enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg(EnrollmentUtils.getExceptionMessage(e));
						return enrollmentResponse;
					}
				}
			}
			/**END OF  - Code to handle MRC-29  - Override Enrollment coverageStartDate if Enrollment is In Pending status**/
			
			
			marketType = plan.get(EnrollmentConstants.MARKET_TYPE);
			if(!isNotNullAndEmpty(newMemberList) && !isNotNullAndEmpty(existingMemberList)){
				throw new GIException("memberInfoList received is null");
			}

			
			enrollmentResponse.setDisEnrollmentList(disEnrollMemberList);
			Enrollment existingEnrlmnt = null;
			if(isNotNullAndEmpty(pdlPlan.getCreateNewEnrollment()) && EnrollmentConstants.NO.equalsIgnoreCase(pdlPlan.getCreateNewEnrollment())){
				/***** Add member to existing enrollment *****/
				String existingMemberPlanId = plan.get(EnrollmentConstants.PLAN_ID);/***** Existing Plan Id.... Required to put correct Enrror message *****/
				if(isNotNullAndEmpty(existingMemberPlanId) && isNotNullAndEmpty(houseHoldCaseID)){
					
					if(existingEnrollmentId != null && enrollmentMap.containsKey(existingEnrollmentId)){
						existingEnrlmnt = enrollmentMap.get(existingEnrollmentId);
					}
					else {
						existingEnrlmnt = enrollmentRepository.findById(existingEnrollmentId);
						 if(isNotNullAndEmpty(pldOrderResponse.getGlobalId())){
							 existingEnrlmnt.setLogGlobalId(pldOrderResponse.getGlobalId());	
							}
						enrollmentMap.put(existingEnrlmnt.getId(), existingEnrlmnt);
						if(!enrollmentExistingStatusMaP.containsKey(existingEnrlmnt.getId())){
							enrollmentExistingStatusMaP.put(existingEnrlmnt.getId(), existingEnrlmnt.getEnrollmentStatusLkp().getLookupValueCode());
						}
					}
					if(existingEnrlmnt == null){
						LOGGER.info("No Enrollment found for the existing member PlanId : " +existingMemberPlanId+"And houseHoldCaseID : "+houseHoldCaseID );
						throw new GIException("No Enrollment found for the existing member PlanId : " +existingMemberPlanId+"And houseHoldCaseID : "+houseHoldCaseID );
					}
					oldSSAPIds.add(existingEnrlmnt.getSsapApplicationid());
					Date financialEffDate =pdlPlan.getFinancialEffectiveDate();
					//Date financialEffDate =pldOrderResponse.getFinancialEffectiveDate(); 
							//validateIdahoFinancialEffDate(existingEnrlmnt, pdlPlan, pldOrderResponse);
					
					Date effectiveDate = null;
					if(newMemberList!=null && !newMemberList.isEmpty()){
						//String coverageStartDate = pdlPlan.getPlan().get(EnrollmentConstants.COVERAGE_START_DATE);

						if (isNotNullAndEmpty(coverageStartDate)){
							String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd HH:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
							effectiveDate =  DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT); 
						}
						try {
							addMemberToExistingEnrollment(existingEnrlmnt, newMemberList,enrollmentReason,effectiveDate,serc, terminationFlag, newMembersToExistingEnrollment, financialEffDate, loggedInUser);
						}catch (GIException gie) {
							LOGGER.error("Enrollment ServiceImpl - createSpecialEnrollment: Failed to add new member enrollment");
							throw gie;
						}
					}
					
					List<Enrollment> statusCheckList= new ArrayList<>();
					statusCheckList.add(existingEnrlmnt);
					
					
					if(overrideCoverageStartDate){
						existingEnrlmnt.setBenefitEffectiveDate(newEffDate);
					}
					if(newEffDate!=null){
						existingEnrlmnt.setInd19CovStDate(newEffDate);
					}
					
					
					checkEnrollmentStatusSpecialEnrollment(statusCheckList);
					/***** Update Existing Enrollees and Enrollment *****/
					
					Date amountEffectiveDate = null;
					amountEffectiveDate = findAmountEffectiveDate(disEnrolledMemberIds, effectiveDate, existingEnrlmnt, enrollmentExistingStatusMaP.get(existingEnrlmnt.getId()));
					
					
					if(existingMemberList!=null && !existingMemberList.isEmpty()){
						updateExistingEnrollee(existingEnrlmnt , existingMemberList,serc, financialEffDate, overrideCoverageStartDate, loggedInUser, sepEffDate);
					}
					//existingEnrlmnt.setSsapApplicationid(applicationId);
					existingEnrlmnt.setPdHouseholdId(pdHouseholdId);
					/***** Change Person_Type lookup of Terminated/Cancelled Subscribers to Enrollee *****/
					//if((existingEnrlmnt.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)||existingEnrlmnt.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
						changeTerminatedSubscriberToEnrollee(existingEnrlmnt.getEnrollees());
					//}
					
					if(isNotNullAndEmpty(pldOrderResponse.getGiWsPayloadId())){
						existingEnrlmnt.setGiWsPayloadId(pldOrderResponse.getGiWsPayloadId());
					}
					updateexistingEnrollmentForSpecial(pldOrderResponse, existingEnrlmnt, pdlPlan, amountEffectiveDate, loggedInUser, financialEffDate, planListResponse);
					
					
					{
						List<PldMemberData> memberInfoList = new ArrayList<PldMemberData>();
						if(existingMemberList!=null && !existingMemberList.isEmpty()){
							memberInfoList.addAll(existingMemberList);
						}
						if(newMemberList!=null && !newMemberList.isEmpty()){
							memberInfoList.addAll(newMemberList);
						}
						if(disenrollMemberInfoList != null && !disenrollMemberInfoList.isEmpty()){
							memberInfoList.addAll(disenrollMemberInfoList);
						}
						populateEnrolleeRelationship(memberInfoList,existingEnrlmnt.getEnrolleesAndSubscriber());
					}
					
					
					if (isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_ID))) {
						if(!(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EnrollmentConstants.STATE_CODE_CA))){
								updateEnrollmentPlanInfoForSpecial(existingEnrlmnt, pdlPlan,amountEffectiveDate, planListResponse);
						}
					}else{
						throw new GIException("No Plans found");
					}
					if(overrideCoverageStartDate) {
						List<Enrollment> quotingDateUpdateList = enrollmentService.synchronizeQuotingDates(existingEnrlmnt, loggedInUser, EnrollmentEvent.TRANSACTION_IDENTIFIER.MRC29_QUOTING_DATE_UPDATE);
						quotingDateUpdateList.forEach(e -> enrollmentMap.put(e.getId(), e));
					}
				}else{
					LOGGER.info("existing Member PlanId or houseHoldCaseID not found");
					throw new GIException("existing Member PlanId or houseHoldCaseID not found");
				}
			}else if(isNotNullAndEmpty(pdlPlan.getCreateNewEnrollment()) && EnrollmentConstants.YES.equalsIgnoreCase(pdlPlan.getCreateNewEnrollment())){
				//	create new enrollment
				if(newMemberList!=null && !newMemberList.isEmpty()){
					if (existingEnrollmentId != null && enrollmentMap.containsKey(existingEnrollmentId)) {
						existingEnrlmnt = enrollmentMap.get(existingEnrollmentId);
					} else if (existingEnrollmentId != null){
						existingEnrlmnt = enrollmentRepository.findById(existingEnrollmentId);
						enrollmentMap.put(existingEnrlmnt.getId(), existingEnrlmnt);
					}
					List<PldPlanData> newPlanDataList = new ArrayList<PldPlanData>();
					//pdlPlan.setMemberInfoList(newMemberList);
					newPlanDataList.add(pdlPlan);
					List<Enrollment> enrlList = new ArrayList<Enrollment>();
					try{
						enrlList = getEnrollmentFromplanDataList(newPlanDataList,pldOrderResponse, loggedInUser, null, planListResponse);
						
					}catch (Exception e) {
//						LOGGER.error("Failed to create enrollment : " + gie);
						LOGGER.error("Failed to create enrollment");
						throw new GIException(e);
					}
					Enrollment newEnrollment = enrlList.get(0);
					if(isNotNullAndEmpty(pdlPlan.getNewEnrollmentReason()) &&( EnrollmentConstants.EnrollmentAppEvent.SUBSCRIBER_CHANGE_ENROLLMENT.toString().equalsIgnoreCase(pdlPlan.getNewEnrollmentReason())||EnrollmentConstants.EnrollmentAppEvent.CS_LEVEL_CHANGE_ENROLLMENT.toString().equalsIgnoreCase(pdlPlan.getNewEnrollmentReason()))){
						newEnrollment.setLastEnrollmentId(existingEnrollmentId);
					}
					newEnrollment.setAppEventReason(pdlPlan.getNewEnrollmentReason());
					// Set - newEnrollmentKey as Key. This is new Enrollment and Id is not present and is NULL.
					enrollmentMap.put(--newEnrollmentKey, newEnrollment);
					//HIX-118675 Setting change plan as N for the enrollment being terminated
					if(null != existingEnrlmnt) {
						existingEnrlmnt.setChangePlanAllowed(EnrollmentConstants.N);
					}
					termEnrollmentsDueToPlanFlip.add(existingEnrollmentId);
				}
			}
		}//End of planData loop

		/***** This is not required....Used only for CA in IND-20. *****/
		//		enrollmentResponse.setFinalDisEnrollmentMemberMap(finalDisEnrollmentMemberMap);

		/***** Call Shop DisEnroll API. Shop team performs a Hard delete of member based on memberIds *****/
		if((isNotNullAndEmpty(marketType) && marketType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)) 
				&& (pldOrderResponse.getInd19Disenrolledmembers() != null && !pldOrderResponse.getInd19Disenrolledmembers().isEmpty())){
			try{
				LOGGER.info("===== Sending Request to Send to GHIX-SHOP for Employer contributions =====");
				RestEmployeeDetailsDTO restEmployeeDetailsDTO = new RestEmployeeDetailsDTO(); 
				restEmployeeDetailsDTO.setEmployeeId(Integer.valueOf(employeeId));
				restEmployeeDetailsDTO.setMemberIds(pldOrderResponse.getInd19Disenrolledmembers().stream().map(Integer::parseInt).collect(Collectors.toList()));
				
				if(isNotNullAndEmpty(applicationId)){
					restEmployeeDetailsDTO.setEmployeeApplicationId(Integer.valueOf(applicationId.toString()));
				}
				String restEmployeeDetailsResponse = null; 
				restEmployeeDetailsResponse = restTemplate.postForObject(ShopEndPoints.DELETE_MEMBER_DETAILS, restEmployeeDetailsDTO, String.class);

				XStream xStream = GhixUtils.getXStreamStaxObject(); 
				ShopResponse shopResponse = (ShopResponse) xStream.fromXML(restEmployeeDetailsResponse);
				/***** If Response is Not Success *****/
				if (shopResponse != null && shopResponse.getErrCode() != 0 ) {
					LOGGER.error(EnrollmentConstants.ERR_MSG_SHOP_DISENROLLMENT_FAILED + disEnrolledMemberIds);
					throw new GIException(EnrollmentConstants.ERR_MSG_SHOP_DISENROLLMENT_FAILED + disEnrolledMemberIds);
				}
			}
			catch (Exception e){
				LOGGER.error(EnrollmentConstants.ERR_MSG_SHOP_DISENROLLMENT_FAILED + disEnrolledMemberIds);
				throw new GIException(EnrollmentConstants.ERR_MSG_SHOP_DISENROLLMENT_FAILED + disEnrolledMemberIds, e);
			}
		}
		
		
		List<Enrollment> enrollments = null;
		List<Enrollment> activeEnrollmentsList = new ArrayList<Enrollment>();
		List<Enrollment> terminatedEnrollmentsList = new ArrayList<Enrollment>();
		try {
			List<Enrollment> enrollmentList =  new ArrayList<Enrollment>(enrollmentMap.values());
			checkEnrollmentStatusSpecialEnrollment(enrollmentList);
			boolean isSubscriber = validateEnrollmentForSubscriber(enrollmentList);
			if(!isSubscriber){
				LOGGER.error(EnrollmentConstants.ERR_MSG_SUBSCRIBERS_VALIDATION_FAILED_FOR_ENROLLMENT);
				throw new GIException(EnrollmentConstants.ERR_MSG_SUBSCRIBERS_VALIDATION_FAILED_FOR_ENROLLMENT);
			}
			
			updateEnrollmentPremium(enrollmentList);
			List<String> cancelTermEnrollmentIds= new ArrayList<String>();
			for(Enrollment enrollment: enrollmentList){
				if(enrollment.getEnrollmentStatusLkp()!=null && (EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode()) ||EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode()))){
					cancelTermEnrollmentIds.add(""+enrollment.getId());
				}
			}
			LOGGER.info("Call saveEnrollmentAndCallIND20 from special enrollment flow at : " + new Date());
			enrollments=saveEnrollmentAndCallIND20(enrollmentList,  disEnrollMemberList, applicant_esig, pdHouseholdId+"", houseHoldCaseID, pldOrderResponse.getEnrollmentType(), enrollmentResponse, autoRenewalFlag, onBehalfActionCheckbox, cancelTermEnrollmentIds, loggedInUser, null, recordType);
			//Update Enrollments having old SSAPIDs
			for(Long oldSSAPId: oldSSAPIds){
				enrollmentRepository.updateSsapApplicationId(oldSSAPId, applicationId);
			}
			
			if(isNotNullAndEmpty(pldOrderResponse.getAllowChangePlan())) {
				if(termEnrollmentsDueToPlanFlip.isEmpty()) {
					enrollmentRepository.updatePlanChangeFlagForHousehold(houseHoldCaseID, pldOrderResponse.getAllowChangePlan(), DateUtil.dateToString(enrollments.get(0).getBenefitEffectiveDate(), EnrollmentConstants.DATE_FORMAT_YYYY));	
				} else {
					enrollmentRepository.updatePlanChangeFlagForHousehold(houseHoldCaseID, pldOrderResponse.getAllowChangePlan(), DateUtil.dateToString(enrollments.get(0).getBenefitEffectiveDate(), EnrollmentConstants.DATE_FORMAT_YYYY), termEnrollmentsDueToPlanFlip);		
				}
			}
			//enrollments = saveAllEnrollment(enrollmentList);
			/*if((terminationFlag || dentalTerminationFlag)
					&&newMembersToExistingEnrollment != null && !newMembersToExistingEnrollment.isEmpty()){
				try{
					terminateNewlyAddedmembers(newMembersToExistingEnrollment, terminationFlag, dentalTerminationFlag, EnrollmentEvent.TRANSACTION_IDENTIFIER.SPECIAL_DISENROLLMENT);
				}catch(Exception e){
					LOGGER.error("Error while disenrolling the newly added member to the disenrolled enrollment :: "+e.getMessage() , e);
				}
			}*/

			//saveEsignatureForEnrollment(enrollments,applicant_esig,onBehalfActionCheckbox);
			filterActiveAndTerminatedEnrollments(enrollments, activeEnrollmentsList, terminatedEnrollmentsList);
			
		} catch (Exception e) {
			throw new GIException("Error Occured while Saving Enrollment :" +EnrollmentUtils.getExceptionMessage(e),e);
		}
		
		
		enrollmentResponse= acknowledgeEnrollmentStatus(enrollmentResponse, pldOrderResponse, enrollments);
		if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
			throw new GIException(enrollmentResponse.getErrMsg()); 
		}
		
		//commenting following method call for CA HIX-81709
		/*enrollmentResponse=acknowledgeEnrollmentStatus(enrollmentResponse, pldOrderResponse, enrollments);
		if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
			throw new GIException(enrollmentResponse.getErrMsg()); 
		}*/
		
		
		//List<Enrollment> enrollmentAppEventList = new ArrayList<Enrollment>();
		//enrollmentAppEventList.addAll(activeEnrollmentsList);
		//enrollmentAppEventList.addAll(terminatedEnrollmentsList);
		if(enrollments != null && !enrollments.isEmpty())
		{
			/**
			 * Terminated Flow
			 */
			for(Enrollment enrollment : enrollments)
			{
				enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, isNotNullAndEmpty(enrollment.getAppEventReason())? enrollment.getAppEventReason() :
						EnrollmentConstants.EnrollmentAppEvent.SPECIAL_ENROLLMENT.toString(), loggedInUser);
			}
		}
		
		enrollmentResponse.setMemberAddedToEnrollment(newMembersToExistingEnrollment);
		enrollmentResponse.setEnrollmentList(activeEnrollmentsList);
		//enrollmentResponse.setTerminationFlag(terminationFlag);
		//enrollmentResponse.setDentalTerminationFlag(dentalTerminationFlag);
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		enrollmentResponse.setEnrolleeNameMap(getEnrolleeNameMap(activeEnrollmentsList));
		return enrollmentResponse;
	}
	
	private List<Enrollment> saveEnrollmentAndCallIND20(List<Enrollment> enrollmentList,
			List<Map<String, String>> disEnrollMemberList, String applicant_esig, String orderId, String caseId,
			Character enroll_type, EnrollmentResponse enrollmentResponse, String autoRenewalFlag,
			String onBehalfActionCheckbox, List<String> cancelTermEnrollmentIds, AccountUser loggedInUser,
			List<Enrollment> disEnrollPriorEnrollmentList, RecordType recordType) throws Exception {
		
		
			/**Disenroll the Previous enrollment if any*/
			List<Enrollment> nonDisEnrollmentList= new ArrayList<>();
		
			if(disEnrollPriorEnrollmentList ==null || disEnrollPriorEnrollmentList.isEmpty()){
				disEnrollPriorEnrollmentList = new ArrayList<Enrollment>();
				disEnrollPriorEnrollmentList.addAll(enrollmentList.stream()
						.filter(e -> (e.getAppEventReason() != null && DISENROLLMENT_EVENTS.contains(e.getAppEventReason()))
								|| EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(e.getEnrollmentStatusLkp().getLookupValueCode()))
						.collect(Collectors.toList()));

				nonDisEnrollmentList.addAll(enrollmentList.stream()
						.filter(e -> (e.getAppEventReason() == null || !DISENROLLMENT_EVENTS.contains(e.getAppEventReason()))
								&& !EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(e.getEnrollmentStatusLkp().getLookupValueCode()))
						.collect(Collectors.toList()));

			}else{
				nonDisEnrollmentList.addAll(enrollmentList);
			}
		
			if(disEnrollPriorEnrollmentList!=null && !disEnrollPriorEnrollmentList.isEmpty()) {
				saveAllEnrollment(disEnrollPriorEnrollmentList);
			}
			
			if (checkInternalOverlap(enrollmentList.stream()
					.filter(e -> EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE
							.equalsIgnoreCase(e.getInsuranceTypeLkp().getLookupValueCode()))
					.collect(Collectors.toList()))
					|| checkInternalOverlap(enrollmentList.stream()
							.filter(e -> EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE
									.equalsIgnoreCase(e.getInsuranceTypeLkp().getLookupValueCode()))
							.collect(Collectors.toList()))) {
				throw new Exception(
						"Enrollment Overlap within the request : One or more member already enrolled with another plan for given period");
			}
			/** Verify overlapping post termination  - Enrollment Member level Overlap Check */
			if (validateEnrollmentMemberOverlapping(nonDisEnrollmentList, disEnrollPriorEnrollmentList)) {
				throw new Exception("EnrollmentOverlps : One or more member already enrolled with another plan for given period");
			}
			/**Persist Enrollment*/
			Date startTime = new Date();
			LOGGER.info("Save enrollment start : " + startTime);
			enrollmentList = saveAllEnrollment(enrollmentList);
			Date endTime = new Date();
			LOGGER.info("Save enrollment end : " + endTime);
			LOGGER.info("Enrollment save time : " + (endTime.getTime() - startTime.getTime()));
			if (enrollmentList != null && !enrollmentList.isEmpty()) {

				saveEsignatureForEnrollment(enrollmentList, applicant_esig, onBehalfActionCheckbox, loggedInUser);

				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrMsg("Enrollment created successfully");
				
				Boolean isCaCall = EnrollmentConstants.STATE_CODE_CA.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode());
				
				if(isCaCall) {
				final List<Enrollment> finalEnrollmentList= enrollmentList;
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				           // Override the afterCommit which need to be executed after transaction commit
				           public void afterCommit() { 
				              //Run the time consuming async task for the newly-persisted entity
				        	   ahbxSyncCall(caseId, finalEnrollmentList, recordType);
				           }
					});
				}
				
			} else {
				LOGGER.error("No Active Enrollments to be sent in IND-20");
				throw new Exception("No Active Enrollments to be sent in IND-20");
			}
			return enrollmentList;
	}
	
	public List<Enrollment> saveAllEnrollment(List<Enrollment> enrollmentList){
		List<Enrollment> enrollments = new ArrayList<Enrollment>();
		for (Enrollment enrollment : enrollmentList) {
			Enrollment en = enrollmentRepository.saveAndFlush(enrollment);
			enrollments.add(en);
		}
		return enrollments;		
	}
	
	private void saveEsignatureForEnrollment(List<Enrollment> enrollments , String signature,String onBehalfActionCheckbox, final AccountUser loggedInUser){
		
		Esignature esignature = null;
	
		for (Enrollment enrollment : enrollments) {
			Enrollee subscriber = checkForSubscriber(enrollment.getEnrollees());
			if(subscriber == null){
				continue;
			}
			
			esignature = new Esignature();
			esignature.setCreatedBy(loggedInUser);
			esignature.setUpdatedBy(loggedInUser);
			if(enrollment.getRenewalFlag() != null && enrollment.getRenewalFlag().equalsIgnoreCase(EnrollmentConstants.RENEWAL_FLAG_A)){
				esignature.seteSignature(subscriber.getFirstName()+" "+subscriber.getLastName());
			}else{
				esignature.seteSignature(signature);
			}
			
			// get last event of subscriber and set as refId in E-Signature.
			if(subscriber.getLastEventId() != null){
				esignature.setRefId(subscriber.getLastEventId().getId());
			}
			
			esignature.setModuleName(ModuleUserService.ENROLLMENT_MODULE);
			if(enrollment.getInsuranceTypeLkp()!=null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
				if(isNotNullAndEmpty(onBehalfActionCheckbox)){
					esignature.setOnBehalfEsign(onBehalfActionCheckbox);
				}
			}
			
			//seteSignature changes done for AutoRenewal
			esignature.setEsignDate(new TSDate());
			// saving the esignature object in ESignature table.
			/*esignature = esignatureService.saveEsignature(esignature);*/

			EnrollmentEsignature enrollmentEsig = new EnrollmentEsignature();
			enrollmentEsig.setEnrollment(enrollment);
			enrollmentEsig.setEsignature(esignature);
			enrollmentEsignatureRepository.saveAndFlush(enrollmentEsig);
		
		}
	}
	
	private void ahbxSyncCall(String caseId, List<Enrollment> enrollmentList, RecordType recordType) {
		LOGGER.info("Async Call to enrollmentAhbxSyncService saveEnrollmentAhbxSync" + recordType.toString());
		enrollmentAhbxSyncService.saveEnrollmentAhbxSync(enrollmentList, caseId, recordType);
	}

@Override	
public Map<String,Object> getIndvPSDetails(String caseId, String orderId, List<Enrollment> enrollmentList, List<Map<String, String>> disEnrollMemberList, Character enroll_type, String autoRenewalFlag, List<String> cancelTermEnrollmentIds){
		
		Map<String,Object> orderMap  = new HashMap<String,Object>();
		if(enroll_type!=null){
			orderMap.put("ENROLLMENT_TYPE", enroll_type.toString());
		}else{
			orderMap.put("ENROLLMENT_TYPE", "");
		}	
		List<Map<String,Object>> planList = new ArrayList<Map<String,Object>>();
		for(Enrollment enrollment : enrollmentList)
		{
			if(enrollment!=null && 
					!(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)
							||enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){

				Map<String,Object> planMap = new HashMap<String,Object>();
				if(autoRenewalFlag!=null && !autoRenewalFlag.toString().equals("")){
					orderMap.put("AUTO_RENEWAL_FLAG", autoRenewalFlag); 
					if((isNotNullAndEmpty(enrollment.getPriorEnrollmentTermFlag()) && enrollment.getPriorEnrollmentTermFlag().equalsIgnoreCase(EnrollmentConstants.Y))  && 
							(enroll_type!=null && enroll_type.toString().equalsIgnoreCase("A"))){	
						if(enrollment.getPriorEnrollmentId()!=null){
							planMap.put("PRIOR_ENROLLMENT_ID", enrollment.getPriorEnrollmentId());
						}
					}
				}
				orderMap.put(EnrollmentConstants.CASE_ID, enrollment.getHouseHoldCaseId());
				orderMap.put(EnrollmentConstants.SADP, enrollment.getSadp());
				orderMap.put(EnrollmentConstants.ASSISTER_BROKER_ID, enrollment.getAssisterBrokerId());
				orderMap.put(EnrollmentConstants.ASSISTER_BROKER_ROLE, enrollment.getBrokerRole());
				orderMap.put(EnrollmentConstants.LOG_GLOBAL_ID, enrollment.getLogGlobalId());

				planMap.put(EnrollmentConstants.ENROLLMENT_ID, enrollment.getId());
				planMap.put(EnrollmentConstants.EMPLOYER_CASE_ID, enrollment.getEmployerCaseId());
				planMap.put(EnrollmentConstants.PLAN_ID, enrollment.getCMSPlanID());

				Enrollee subscriber = enrollment.getSubscriberForEnrollment();
				if(subscriber !=null && subscriber.getLastEventId()!=null){
					planMap.put("subscriberEventId", subscriber.getLastEventId().getId());
				}
				if(enrollment.getInsuranceTypeLkp()!=null){
					planMap.put("insuranceType", enrollment.getInsuranceTypeLkp().getLookupValueLabel());
				}
				if(enrollment.getPlanLevel() != null){
					planMap.put(EnrollmentConstants.PLAN_TIER_ID,enrollment.getPlanLevel());
				}
				if(enrollment.getEnrollmentTypeLkp()!=null){
					planMap.put(EnrollmentConstants.PROGRAM_TYPE, enrollment.getEnrollmentTypeLkp().getLookupValueCode());
				}
				if(enrollment.getInd19CovStDate()!=null){ //RE: Defect #18481 test scenarios
					planMap.put(EnrollmentConstants.COVERAGE_START_DATE, DateUtil.dateToString(enrollment.getInd19CovStDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}else{
					planMap.put(EnrollmentConstants.COVERAGE_START_DATE, DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}

				planMap.put(EnrollmentConstants.GROSS_PREMIUM_COST, enrollment.getGrossPremiumAmt());
				planMap.put(EnrollmentConstants.EMPLOYEE_CONTRIBUTION,enrollment.getEmployeeContribution());
				planMap.put(EnrollmentConstants.EMPLOYER_CONTRIBUTION,enrollment.getEmployerContribution());
				planMap.put(EnrollmentConstants.APTC_APPLIED_AMOUNT, enrollment.getAptcAmt());
				planMap.put(EnrollmentConstants.NET_PREMIUM_AMOUNT, enrollment.getNetPremiumAmt());
				planMap.put(EnrollmentConstants.CSR_AMOUNT, enrollment.getCsrAmt());
				if(enrollment.getEnrollmentStatusLkp()!=null){
					planMap.put(EnrollmentConstants.ENROLLMENT_STATUS_KEY, enrollment.getEnrollmentStatusLkp().getLookupValueCode());
				}
				if(isNotNullAndEmpty(enrollment.getAptcEffDate())){
					planMap.put(EnrollmentConstants.APTC_EFF_DATE, DateUtil.dateToString(enrollment.getAptcEffDate(), GhixConstants.REQUIRED_DATE_FORMAT));				
				}
				if(isNotNullAndEmpty(enrollment.getGrossPremEffDate())){
					planMap.put(EnrollmentConstants.GROSS_PREM_EFF_DATE, DateUtil.dateToString(enrollment.getGrossPremEffDate(), GhixConstants.REQUIRED_DATE_FORMAT));				
				}

				List<Enrollee> enrolleeList = null;
				/***** Get all enrollees and subscriber (Filter HouseholdContact, Responsible Person Custodial Parent) *****/
				enrolleeList = enrollment.getEnrolleesAndSubscriber();

				List<Map<String,Object>> memberList = new ArrayList<Map<String,Object>>();
				

				/***** Populate Eligible Members List *****/
				for(Enrollee enrollee :enrolleeList){
					Map<String,Object> memberMap = new HashMap<String,Object>();
					boolean sendMemberInfoInd20 = true;
					if(disEnrollMemberList!=null && disEnrollMemberList.size()>0){
						for(Map<String, String> disMemberMap:disEnrollMemberList){
							if(isNotNullAndEmpty(disMemberMap.get(EnrollmentConstants.MEMBER_ID)) 
									&& disMemberMap.get(EnrollmentConstants.MEMBER_ID).equalsIgnoreCase(enrollee.getExchgIndivIdentifier())){

								/***** If Enrollment is Dis-Enrolled as a new plan was selected *****/
								if((isNotNullAndEmpty(disMemberMap.get(EnrollmentConstants.IS_ENROLLMENT_DISENROLLED)) && Boolean.parseBoolean(disMemberMap.get(EnrollmentConstants.IS_ENROLLMENT_DISENROLLED))) || (cancelTermEnrollmentIds!=null && (cancelTermEnrollmentIds.contains(disMemberMap.get(EnrollmentConstants.EXISTING_QHP_ENROLLMENT_ID)) || cancelTermEnrollmentIds.contains(disMemberMap.get(EnrollmentConstants.EXISTING_SADP_ENROLLMENT_ID))))){
									if(isNotNullAndEmpty(disMemberMap.get(EnrollmentConstants.EXISTING_QHP_ENROLLMENT_ID))){
										memberMap.put(EnrollmentConstants.EXISTING_QHP_ENROLLMENT_ID, disMemberMap.get(EnrollmentConstants.EXISTING_QHP_ENROLLMENT_ID));
									}
									if(isNotNullAndEmpty(disMemberMap.get(EnrollmentConstants.QHP_DIS_ENROLLMENT_END_DATE))){
										memberMap.put(EnrollmentConstants.QHP_DIS_ENROLLMENT_END_DATE, disMemberMap.get(EnrollmentConstants.QHP_DIS_ENROLLMENT_END_DATE));
									}
									if(isNotNullAndEmpty(disMemberMap.get(EnrollmentConstants.EXISTING_SADP_ENROLLMENT_ID))){
										memberMap.put(EnrollmentConstants.EXISTING_SADP_ENROLLMENT_ID, disMemberMap.get(EnrollmentConstants.EXISTING_SADP_ENROLLMENT_ID));
									}
									if(isNotNullAndEmpty(disMemberMap.get(EnrollmentConstants.SADP_DIS_ENROLLMENT_END_DATE))){
										memberMap.put(EnrollmentConstants.SADP_DIS_ENROLLMENT_END_DATE, disMemberMap.get(EnrollmentConstants.SADP_DIS_ENROLLMENT_END_DATE));
									}
									sendMemberInfoInd20 = true;
								}
								/***** This Member Information goes in Disenrollment Section of IND-20, and not in Associated/Eligible member details *****/
								else{
									sendMemberInfoInd20 = false;
								}
							}
						}
					}
					if(sendMemberInfoInd20){
						/***** Filter Out Active Enrollees. These go in dis-enroll loop or if they were terminated in earlier RAC, the info is not sent again *****/
						if((!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))
								&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
								&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED))){
							memberMap.put(EnrollmentConstants.MEMBER_ID, enrollee.getExchgIndivIdentifier());
							memberMap.put(EnrollmentConstants.SUBSCRIBER_FLAG, EnrollmentConstants.SUBSCRIBER_FLAG_NO);
							if(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) ){
								memberMap.put(EnrollmentConstants.SUBSCRIBER_FLAG, EnrollmentConstants.SUBSCRIBER_FLAG_YES);
							}
							//HIX-48904
							//memberMap.put(EnrollmentConstants.GROSS_PREMIUM_COST, enrollment.getGrossPremiumAmt());
							memberMap.put(EnrollmentConstants.GROSS_PREMIUM_COST, enrollee.getTotalIndvResponsibilityAmt());
							memberList.add(memberMap);
						}
					}
				}
				
				planMap.put(EnrollmentConstants.MEMBER_DETAILS, memberList);
				planList.add(planMap);
			}//End of Status Check
		}//end of enrollment For Loop
		List<Map<String,String>> disEnrolledMemberList = new ArrayList<Map<String,String>>();
		if(disEnrollMemberList != null && disEnrollMemberList.size()>0){
			for(Map<String, String> disMemberMap:disEnrollMemberList){
				/***** If Only individual members are dis-enrolled *****/
				if((!(isNotNullAndEmpty(disMemberMap.get(EnrollmentConstants.IS_ENROLLMENT_DISENROLLED)) && Boolean.parseBoolean(disMemberMap.get(EnrollmentConstants.IS_ENROLLMENT_DISENROLLED)))) && !(cancelTermEnrollmentIds!=null && cancelTermEnrollmentIds.contains(disMemberMap.get(EnrollmentConstants.ENROLLMENT_ID)))){
					Map<String, String> disEnrolledMemberMap = new HashMap<String, String>();
					disEnrolledMemberMap.put(EnrollmentConstants.MEMBER_ID, disMemberMap.get(EnrollmentConstants.MEMBER_ID));
					disEnrolledMemberMap.put(EnrollmentConstants.DIS_ENROLLED_ENROLLMENT_ID, disMemberMap.get(EnrollmentConstants.ENROLLMENT_ID));
					disEnrolledMemberList.add(disEnrolledMemberMap);
				}
			}
		}
		orderMap.put(EnrollmentConstants.DIS_ENROLLED_MEMBER_DETAILS, disEnrolledMemberList);
		orderMap.put(EnrollmentConstants.PLAN_DETAILS, planList);
		return orderMap;
	}


	@Override
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public EnrollmentResponse createEnrollment(PldOrderResponse pldOrderResponse, String applicant_esig,
			String onBehalfActionCheckbox, EnrollmentAhbxSync.RecordType recordType ) throws GIException {
		LOGGER.info("CREATE::ENROLLMENT ======== Started Create Enrollment Flow for HouseholdCaseID: "+pldOrderResponse.getHouseholdCaseId() + " Application Id: "+pldOrderResponse.getApplicationId());
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		List<Enrollment> enrollmentList = null;
		List<Enrollment> enrollments =null;
		AccountUser loggedInUser = this.getLoggedInUser();
		if(loggedInUser == null){
			throw new GIException("Found null or empty logged in user");
		}
		else{
			LOGGER.info("Create Enrollment Received Request for LoggedInUser: "+loggedInUser.getEmail()+" ID:"+loggedInUser.getId()+ " HouseholdCaseID: "+pldOrderResponse.getHouseholdCaseId());
		}
		try{
			String houseHoldCaseID = pldOrderResponse.getHouseholdCaseId();
			Long ssapApplicationId = pldOrderResponse.getApplicationId();
			String orderId=pldOrderResponse.getPdHouseholdId();
			Character enrollmentResaon = pldOrderResponse.getEnrollmentType();
			String autoRenewalFlag= null;
			
			try {
				if (ssapApplicationId != null) {
					String applicationStatus = enrollmentExternalRestUtil.getApplicationStatusById(ssapApplicationId);
					if (applicationStatus.equalsIgnoreCase("CL")) {
						throw new GIException(
								"The Application is in closed status for application id " + ssapApplicationId);
					}
				}
			} catch (GIException e) {
				LOGGER.error(EnrollmentConstants.MESSAGE + e.getMessage(), e);
				LOGGER.error(EnrollmentConstants.CAUSE + e.getCause(), e);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("Application Closed" + e.getStackTrace());
				enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_ENSSAP01);
				return enrollmentResponse;
			}
			
			if(isNotNullAndEmpty(pldOrderResponse.getAutoRenewal())){
				autoRenewalFlag=pldOrderResponse.getAutoRenewal().toString();
			}
			List<Enrollment> disEnrollPriorEnrollmentList = null;
			Map<Enrollment,List<Map<String, String>>> finalDisEnrollmentMemberMap = new HashMap<Enrollment,List<Map<String, String>>>();
			PlanListResponse planListResponse = enrollmentExternalRestUtil.getPlanListInfo(pldOrderResponse.getPlans(),pldOrderResponse.getMarketType());
			enrollmentList = getEnrollmentFromplanDataList(pldOrderResponse.getPlanDataList(),pldOrderResponse, loggedInUser, finalDisEnrollmentMemberMap, planListResponse);
			
			boolean isSubscriber = validateEnrollmentForSubscriber(enrollmentList);
			if(!isSubscriber){
				LOGGER.error(EnrollmentConstants.ERR_MSG_SUBSCRIBERS_VALIDATION_FAILED_FOR_ENROLLMENT);
				throw new GIException(EnrollmentConstants.ERR_MSG_SUBSCRIBERS_VALIDATION_FAILED_FOR_ENROLLMENT);
			}
			
			List<Map<String, String>> disEnrollMemberList= new ArrayList<>();
			if(finalDisEnrollmentMemberMap != null && !finalDisEnrollmentMemberMap.isEmpty()){
				disEnrollPriorEnrollmentList = new ArrayList<Enrollment>();
				for(Entry<Enrollment, List<Map<String, String>>> entry:finalDisEnrollmentMemberMap.entrySet()){
					disEnrollPriorEnrollmentList.add(entry.getKey());
					disEnrollMemberList.addAll(entry.getValue());
				}
			}
			enrollments=saveEnrollmentAndCallIND20(enrollmentList, disEnrollMemberList, applicant_esig, orderId, houseHoldCaseID, enrollmentResaon, enrollmentResponse, autoRenewalFlag, onBehalfActionCheckbox, null, loggedInUser, disEnrollPriorEnrollmentList, recordType);
			
			enrollmentResponse.setEnrollmentList(enrollments);
			if(isNotNullAndEmpty(enrollments)) {
				enrollmentRepository.updateSsapApplicationIdForHousehold(enrollments.get(0).getHouseHoldCaseId(), enrollments.get(0).getSsapApplicationid(), new Integer(DateUtil.dateToString(enrollments.get(0).getBenefitEffectiveDate(), EnrollmentConstants.DATE_FORMAT_YYYY)));
			}
		} catch (GIException gie) {
			LOGGER.info("AUTORENEWAL:: GIException Caught: HouseHoldCaseId: "+pldOrderResponse.getHouseholdCaseId() +" SSAP Application Id: "+pldOrderResponse.getApplicationId());
			//LOGGER.error("Logged in user info :"+(loggedInUser != null ? loggedInUser.getEmail() : null)+" HouseholdCaseID: "+pldOrderResponse.getHouseholdCaseId());
			LOGGER.error("Failed to create enrollment GIException: ",gie);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(gie.getErrorCode());
			enrollmentResponse.setErrMsg(gie.getMessage()+"\n"+EnrollmentUtils.getExceptionMessage(gie));
			throw new GIException(enrollmentResponse.getErrMsg());
		}
		catch (Exception e) {
			LOGGER.info("AUTORENEWAL:: General Exception: HouseHoldCaseId: "+pldOrderResponse.getHouseholdCaseId() +" SSAP Application Id: "+pldOrderResponse.getApplicationId());
			//LOGGER.error("Logged in user info :"+(loggedInUser != null ? loggedInUser.getEmail() : null) +" HouseholdCaseID: "+pldOrderResponse.getHouseholdCaseId());
			LOGGER.error("Failed to create enrollment General Exception: ",e);
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(e.getMessage()+"\n" + EnrollmentUtils.getExceptionMessage(e));
			throw new GIException(enrollmentResponse.getErrMsg());
		}
		
		enrollmentResponse = acknowledgeEnrollmentStatus(enrollmentResponse, pldOrderResponse,enrollments);
		if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
			throw new GIException(enrollmentResponse.getErrMsg()); 
		}
		
		/**
		 * Enrollment Submitted event
		 */
		if(enrollments != null && !enrollments.isEmpty())
		{
			for(Enrollment enrollment : enrollments)
			{
				enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, 
						EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_SUBMITTED.toString(), loggedInUser);
			}
		}
		
		enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);	
		enrollmentResponse.setEnrolleeNameMap(getEnrolleeNameMap(enrollments));
		LOGGER.info("#################################################################************* Finished ENROLLMENT "+pldOrderResponse.getHouseholdCaseId() +" *********#################################################################");
		return enrollmentResponse;
	}

	@Override
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public void automateSpecialEnrollment(EnrollmentUpdateRequest request, EnrollmentResponse response)
			throws GIException {
		if(request!=null && request.getEnrollmentDto()!=null && !request.getEnrollmentDto().isEmpty())
		{
			try{
				AccountUser loggedInUser = this.getLoggedInUser();
				if(loggedInUser != null)
	               {
	                  int id = loggedInUser.getId();
	                  if(id >0) {
	                     loggedInUser = this.userService.findById(id);
	                  }
	                  else {
	                     loggedInUser = this.userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
	                  }
	               }
				
				List<Enrollment> enrollmentList= new ArrayList<>();
				List<Map<String, String>> memberAddedToEnrollment= new ArrayList<Map<String, String>>();
				for (EnrollmentCoreDto enrollmentDto: request.getEnrollmentDto()){
					if(enrollmentDto.getBenefitEffectiveDate()==null){
						response.setErrCode(EnrollmentConstants.ERROR_CODE_202);
						response.setErrMsg(EnrollmentConstants.ERR_MSG_NULL_EFFDATE);
						response.setStatus(GhixConstants.RESPONSE_FAILURE);
						return ;
					}
					String oldStatus="";
					if(isNotNullAndEmpty(enrollmentDto)){
						enrollmentDto.setEnrollmentReason('S');
						if(enrollmentDto.getId()!=null && enrollmentDto.getEnrolleeCoreDtoList()!=null && !enrollmentDto.getEnrolleeCoreDtoList().isEmpty()){
							Enrollment enrollment= enrollmentRepository.findById(enrollmentDto.getId());
							enrollmentList.add(enrollment);
							if(enrollment!=null  ){
								
								if(isNotNullAndEmpty(enrollmentDto.getGiWsPayloadId())){//HIX-97591
									enrollment.setGiWsPayloadId(enrollmentDto.getGiWsPayloadId());
								}
								
								oldStatus= enrollment.getEnrollmentStatusLkp().getLookupValueCode();
								Date amountEffectiveDate=null;
								
								validateAutomateFinancialDate(enrollmentDto,enrollment);
								for (EnrolleeCoreDto enrolleeDto: enrollmentDto.getEnrolleeCoreDtoList()){
									//get Enrollee From Enrollment
									Enrollee enrollee= null;
									if(EnrolleeCoreDto.ENROLLEE_ACTION.ADD.toString().equalsIgnoreCase(enrolleeDto.getEnrolleeAction().toString())){
										enrollee= new Enrollee();
										createEnrollee(enrolleeDto, enrollmentDto, enrollment, enrollee);
										//add to Enrollee List
										
										amountEffectiveDate= enrolleeDto.getEffectiveStartDate();

										if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())){
											Map<String,String> newMemMap=new HashMap<String, String>();
											newMemMap.put(EnrollmentConstants.MEMBER_ID,enrollee.getExchgIndivIdentifier());
											newMemMap.put(EnrollmentConstants.ENROLLMENT_ID,enrollment.getId().toString());
											newMemMap.put(EnrollmentConstants.MAINTENANCE_REASON_CODE, enrolleeDto.getMaintainenceReasonCode());
											newMemMap.put(EnrollmentConstants.SERC, enrolleeDto.getSercStr());
											newMemMap.put(EnrollmentConstants.TERM_FLAG, Boolean.toString(Boolean.TRUE));
											memberAddedToEnrollment.add(newMemMap);
										}

									}else if(EnrolleeCoreDto.ENROLLEE_ACTION.TERM.toString().equalsIgnoreCase(enrolleeDto.getEnrolleeAction().toString())){

										//get Enrollee From Enrollment
										enrollee = findEnrolleeByMemberId(enrolleeDto.getExchgIndivIdentifier(), enrollment.getEnrolleesAndSubscriber());
										if(enrollee==null){
											throw new GIException("No Enrollee Found for exchangeIndivIdentifier = "+enrolleeDto.getExchgIndivIdentifier()+" and enrollment Id = "+enrollmentDto.getId());
										}
										enrollee.setUpdatedBy(loggedInUser);
										disEnrollEnrollee(enrollee, enrollment, enrolleeDto, loggedInUser);
										if(amountEffectiveDate==null && EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode())){
											amountEffectiveDate=EnrollmentUtils.getNextDayDate(enrollee.getEffectiveEndDate());
										}
									}else if(EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE.toString().equalsIgnoreCase(enrolleeDto.getEnrolleeAction().toString())){
										//get Enrollee From Enrollment
										enrollee = findEnrolleeByMemberId(enrolleeDto.getExchgIndivIdentifier(), enrollment.getEnrolleesAndSubscriber());
										if(enrollee==null){
											throw new GIException("No Enrollee Found for exchangeIndivIdentifier = "+enrolleeDto.getExchgIndivIdentifier()+" and enrollment Id = "+enrollmentDto.getId());
										}
										enrollee.setUpdatedBy(loggedInUser);
										updateExistingEnrollee(enrolleeDto, enrollee);
										createEvent(enrollee, enrollment, enrolleeDto, loggedInUser, EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTOMATE_SPECIAL_UPDATE);
										updateHouseholdResponsible(enrolleeDto, enrollment.getEnrollees(), loggedInUser);

									}else if(EnrolleeCoreDto.ENROLLEE_ACTION.OTHER.toString().equalsIgnoreCase(enrolleeDto.getEnrolleeAction().toString())){
										
										////get Enrollee From Enrollment and just create event
										enrollee = findEnrolleeByMemberId(enrolleeDto.getExchgIndivIdentifier(), enrollment.getEnrolleesAndSubscriber());
										if(enrollee==null){
											throw new GIException("No Enrollee Found for exchangeIndivIdentifier = "+enrolleeDto.getExchgIndivIdentifier()+" and enrollment Id = "+enrollmentDto.getId());
										}
										enrollee.setUpdatedBy(loggedInUser);
										createEvent(enrollee, enrollment, enrolleeDto, loggedInUser, EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTOMATE_SPECIAL);
									}

								}
								String currentStatus=enrollment.getEnrollmentStatusLkp().getLookupValueCode();
								if(!(!EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(oldStatus) && !EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(oldStatus) &&
										(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(currentStatus) || EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(currentStatus))
										)){
									updateExistingEnrollment(enrollment, enrollmentDto, loggedInUser);
									createEnrolleeRelationship(enrollmentDto.getEnrolleeCoreDtoList(), enrollment.getEnrolleesAndSubscriber());
									
								}else{
									//code to update corresponding dental APTC
									if(EnrollmentConfiguration.isIdCall() && EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())){
										List<Enrollment> dentalEnrollments=updateDentalAPTCOnHealthDisEnrollment(enrollment, loggedInUser);
										if(dentalEnrollments!=null && dentalEnrollments.size()>0){
											enrollmentList.addAll(dentalEnrollments);
										}
									}
									
								}
								boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
								if(populateMonthlyPremium){
									enrollmentRequotingService.updateMonthlyPremiumForEnrollment(enrollment, false, true, null, true);
								}
							}else{
								throw new GIException("No Enrollment Found for enrollment Id = "+enrollmentDto.getId());
							}
						}

					}

				}
				
				/** Enrollment Member level Overlap Check*/
				if(validateEnrollmentMemberOverlapping(enrollmentList, null)){
					throw new Exception ("EnrollmentOverlps : One or more member already enrolled with another plan for given enrollment period");
				}
				saveAllEnrollment(enrollmentList);
				if(memberAddedToEnrollment!=null && !memberAddedToEnrollment.isEmpty()){
					try{
						response.setMemberAddedToEnrollment(memberAddedToEnrollment);
						response.setTerminationFlag(true);
						response.setDentalTerminationFlag(true);
						//terminateNewlyAddedmembers(response , EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTOMATE_SPECIAL_DISENROLL);
					}catch(Exception e){
						LOGGER.error("Error while disenrolling the newly added member to the disenrolled enrollment" , e);
					}
				}
				
				/**
				 * Trigger AUTOMATED_CHANGE_REPORTING Application Event
				 */
				if(enrollmentList != null && !enrollmentList.isEmpty())
				{
					for(Enrollment enrollment : enrollmentList)
					{
						enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.AUTOMATED_CHANGE_REPORTING.toString(), loggedInUser);
					}
				}
			}catch(Exception e){
				throw new GIException("Error while processing the request: "+e.getMessage(),e);
			}
		}
	}
	
	@SuppressWarnings("unused")
	private List<Map<String, String>> disEnrollMemberForSpecialEnrollment(PldMemberData disEnrollMemberData, String employeeId, Map<Integer, Enrollment> disEnrollMap, List<String> disEnrolledMemberIds, String serc, Map<String, Boolean> termFlagMap, final AccountUser loggedInUser, String allowChangePlan) throws GIException{
		
		/*****  DisEnrollMemberList needs to set in the EnrollmentResponse, This would be displayed in the order confirm page. *****/
		List<Map<String, String>> disEnrollMemberList = new ArrayList<Map<String, String>>();

		/***** DisEnrolled Enrollments List *****/
		List<Enrollment> disEnrollmentList = null;

		/***** Loop through disenrollMemberInfoList and disenroll members *****/
		Map<String,String> member  = disEnrollMemberData.getMemberInfo();
		
		List<String> enrollmentIds = disEnrollMemberData.getEnrollmentIds();
		if(enrollmentIds != null && !enrollmentIds.isEmpty() && member != null){
			if(!isNotNullAndEmpty(member.get("memberId"))){
				LOGGER.info(EnrollmentConstants.ERR_MSG_MEMBER_ID_IS_NULL_OR_EMPTY);
				throw new GIException(EnrollmentConstants.ERR_MSG_MEMBER_ID_IS_NULL_OR_EMPTY);
			}else if (!isNotNullAndEmpty(member.get("terminationDate"))){
				LOGGER.info(EnrollmentConstants.ERR_MSG_TERMINATION_DATE_IS_NULL_OR_EMPTY);
				throw new GIException(EnrollmentConstants.ERR_MSG_TERMINATION_DATE_IS_NULL_OR_EMPTY);
			}else if(!isNotNullAndEmpty(member.get("terminationReasonCode"))){
				LOGGER.info(EnrollmentConstants.ERR_MSG_TERMINATION_REASON_CODE_NULL_OR_EMPTY);
				throw new GIException(EnrollmentConstants.ERR_MSG_TERMINATION_REASON_CODE_NULL_OR_EMPTY);
			}else{ 
				/***** Call Disenroll Loop Through enrollmentIds and disEnroll *****/
				for(String stringEnrollmentId : enrollmentIds){
					try{
						boolean termFlag=false;
						Integer disEnrollmentId = Integer.valueOf(stringEnrollmentId);
						Map<String, Object> disEnrollmentMap = new HashMap<String, Object>();
						String memberId = member.get("memberId");
						Enrollment enrollment = null;
						if(disEnrollMap.containsKey(disEnrollmentId)){
							enrollment = disEnrollMap.get(disEnrollmentId);
						}else{
							enrollment = enrollmentRepository.findById(disEnrollmentId);
							disEnrollMap.put(disEnrollmentId, enrollment);
						}
						if(!isNotNullAndEmpty(allowChangePlan)) {
							enrollment.setChangePlanAllowed(EnrollmentConstants.N);
						}
						if(null == enrollment.getAppEventReason() || !DISENROLLMENT_EVENTS.contains(enrollment.getAppEventReason())) {
							enrollment.setAppEventReason(disEnrollMemberData.getAppEventReason());
						}
						/*if(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())  ){
							termFlag=terminationFlag;
						}else if(EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())  ){
							termFlag=dentalTerminationFlag;
						}*/
						if(termFlagMap!=null && termFlagMap.containsKey(stringEnrollmentId) ){
							termFlag= termFlagMap.get(stringEnrollmentId);
						}
						Enrollee enrollee = enrollment.getEnrolleeByMemberId(member.get("memberId"));
						
						//HIX-47314 Fix for duplicate names
						/** To Handle scenerio where plan-display response sends DisEnrollQhp and DisEnrollQdp flag as yes in both Health and Dental loop in response*/
						/** This dis-enrolls members twice creating two events*/
						Boolean isEnrolleeStatusNotNull = enrollee != null && enrollee.getEnrolleeLkpValue()!=null;
						Boolean isEnrolleeStatusInTermAndCancel = isEnrolleeStatusNotNull && ( enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM));
						
						if(isEnrolleeStatusInTermAndCancel && !termFlag){
							continue;
						}
						
						if(enrollee != null){
							disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_ID_KEY,disEnrollmentId);
							disEnrollmentMap.put(EnrollmentConstants.DIS_ENROLLMENT_TYPE,enrollment.getEnrollmentTypeLkp().getLookupValueCode());
							disEnrollmentMap.put(EnrollmentConstants.MEMBER_ID_KEY,member.get("memberId"));
							disEnrollmentMap.put(EnrollmentConstants.TERMINATION_REASON_CODE, member.get("terminationReasonCode"));
							disEnrollmentMap.put(EnrollmentConstants.TERMINATION_DATE, member.get("terminationDate"));
							disEnrollmentMap.put(EnrollmentConstants.DEATH_DATE, member.get("deathDate"));
							disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_REASON, EnrollmentConstants.ENROLLMENT_REASON_S);
							
							/***** Calculate whether termination/cancelletion based on termination date and enrollment effective Start date and Status. *****/
							String enrolleeDbStatus = enrollee.getEnrolleeLkpValue().getLookupValueCode();
							Date terminationDate = DateUtil.StringToDate(member.get("terminationDate"), GhixConstants.REQUIRED_DATE_FORMAT);
							if(enrollee.getEffectiveStartDate() != null){
								if(EnrollmentUtils.removeTimeFromDate(terminationDate).after(enrollee.getEffectiveStartDate())){
									
									disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_STATUS_KEY1,EnrollmentConstants.ENROLLMENT_STATUS_TERM);
									//HIX-110972 Remove cancellation logic for future termination for CA
									/*if(EnrollmentConfiguration.isCaCall()){
										if(enrolleeDbStatus!=null && (enrolleeDbStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING))){
											disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_STATUS_KEY1,EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
										}
									 }*/
									
								}else{
									disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_STATUS_KEY1,EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
								}
							}
						}
						else{
							LOGGER.info("disEnrollByenrollmentID() ::" + " No Enrollee Found for memberID=" + memberId);
							throw new GIException(EnrollmentConstants.ERROR_CODE_201,"No member found for given Member id : "+memberId + " and Enrollment ID : " + disEnrollmentId ,EnrollmentConstants.HIGH);
						}
						Map<String,String> disMemberMap = new HashMap<String, String>();

						/***** Call Disenroll *****/
						String disEnrollResponse  = disEnrollMemberSpecialEnrollment(enrollee, disEnrollmentMap, serc, termFlag, loggedInUser, EnrollmentEvent.TRANSACTION_IDENTIFIER.SPECIAL_DISENROLLMENT);

						if(isNotNullAndEmpty(disEnrollResponse) && disEnrollResponse.equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
							if(termFlag){
								enrollment.setRetroDate(enrollee.getEffectiveEndDate());
							}
							/***** disEnrolledMemberIds is passed as null from createSpecialEnrollment method in case a disenrollment is to be done for new plan *****/
							/***** the shop disenroll API is not required to be called in this case, as user is selecting new plan *****/
							if(isNotNullAndEmpty(disEnrolledMemberIds) && !disEnrolledMemberIds.contains(member.get("memberId"))){
								disEnrolledMemberIds.add(member.get("memberId"));
							}
							
							/***** Used on Order Confirm Page *****/
							if(enrollee != null){
								disMemberMap.put("fName", enrollee.getFirstName());
								disMemberMap.put("lName", enrollee.getLastName());
							}
							String insuranceType=enrollment.getInsuranceTypeLkp().getLookupValueCode();
							disMemberMap.put(EnrollmentConstants.PLAN_TYPE, enrollment.getInsuranceTypeLkp().getLookupValueLabel().toUpperCase());
							disMemberMap.put(EnrollmentConstants.MEMBER_ID, member.get(EnrollmentConstants.MEMBER_ID));
							disMemberMap.put(EnrollmentConstants.ENROLLMENT_ID, enrollment.getId().toString());
							if(insuranceType.equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE)){
								disMemberMap.put(EnrollmentConstants.EXISTING_QHP_ENROLLMENT_ID, ""+enrollment.getId());
								disMemberMap.put(EnrollmentConstants.QHP_DIS_ENROLLMENT_END_DATE,DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
							}
							else if(insuranceType.equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE)){
								disMemberMap.put(EnrollmentConstants.EXISTING_SADP_ENROLLMENT_ID,  ""+enrollment.getId());
								disMemberMap.put(EnrollmentConstants.SADP_DIS_ENROLLMENT_END_DATE, DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
							}
							disEnrollMemberList.add(disMemberMap);
						}
						else{
							throw new GIException("Disenrollment failed for enrollee with memberId : " + member.get("memberId"));
						}
					} 
					catch (GIException gie){
						LOGGER.error(gie.getMessage());
						throw  gie;
					}
				}
			}
		}

		/***** Get Updated Enrollment object list from Map. This contains all dis-enrolled enrollees needs to be saved to DB *****/
		disEnrollmentList = new ArrayList<Enrollment>(disEnrollMap.values());
		return disEnrollMemberList;
	}
	
	private String disEnrollMemberSpecialEnrollment(Enrollee enrollee, Map<String,Object> disEnrollmentMap, String serc, boolean terminationFlag, final AccountUser loggedInUser,  EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) throws GIException{
		/* Excepts map with following keys
		 * MemberId , EnrollmentID, TerminationDate , EnrollmentStatus , DisEnrollmentType , TerminationReasonCode , DeathDate, User
		 * 
		 * */
		if(disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE)==null ||disEnrollmentMap.get(EnrollmentConstants.ENROLLMENT_STATUS_KEY1)==null || disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE)==null ){
			throw new GIException("Validation Error in disEnrollByEnrollmentID() :: Any of the following mandatory fields is null"+ 
					"\n"+ "TerminationDate ="+disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE)+
					"\n"+ "EnrollmentStatus ="+disEnrollmentMap.get(EnrollmentConstants.ENROLLMENT_STATUS_KEY1)+
					"\n"+ "TerminationReasonCode ="+disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE));
		}

		boolean isSpecial = false;
		//String disEnrollmentType = (String)disEnrollmentMap.get(EnrollmentConstants.DIS_ENROLLMENT_TYPE);
		if(enrollee != null){
				/*AccountUser user = (AccountUser)disEnrollmentMap.get("User");*/
				String enrollmentStatus = (String)disEnrollmentMap.get(EnrollmentConstants.ENROLLMENT_STATUS_KEY1);
				if(enrollmentStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
					enrollee.setEffectiveEndDate(enrollee.getEffectiveStartDate());
				}else if(enrollmentStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
					enrollee.setEffectiveEndDate(EnrollmentUtils.getEODDate(DateUtil.StringToDate((String)disEnrollmentMap.get(EnrollmentConstants.TERMINATION_DATE), GhixConstants.REQUIRED_DATE_FORMAT)));
				}
				LookupValue lkpValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, (String)disEnrollmentMap.get(EnrollmentConstants.ENROLLMENT_STATUS_KEY1));
				if(lkpValue != null){
					enrollee.setEnrolleeLkpValue(lkpValue);
					if(lkpValue.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || lkpValue.getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
						enrollee.setDisenrollTimestamp(new TSDate());
					}
				}
				enrollee.setUpdatedBy(loggedInUser);

				String terminationReasonCode = disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE).toString();
				if(terminationReasonCode != null && terminationReasonCode.equals(EnrollmentConstants.REASON_CODE_DEATH) && disEnrollmentMap.get(EnrollmentConstants.DEATH_DATE)!=null){
					
					enrollee.setDeathDate(DateUtil.StringToDate((String)disEnrollmentMap.get(EnrollmentConstants.DEATH_DATE), GhixConstants.REQUIRED_DATE_FORMAT));
					
					if(enrollee.getDeathDate().before(enrollee.getEffectiveStartDate())) {
						enrollee.setEffectiveEndDate(enrollee.getEffectiveStartDate());
						enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
					}else if(enrollmentStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) && enrollee.getEffectiveEndDate().after(enrollee.getDeathDate())){
						enrollee.setEffectiveEndDate(EnrollmentUtils.getEODDate(enrollee.getDeathDate()));
					}
				}

				if(disEnrollmentMap.get(EnrollmentConstants.ENROLLMENT_REASON) != null){
					String enrollment_reason = (String)disEnrollmentMap.get(EnrollmentConstants.ENROLLMENT_REASON);
					if(enrollment_reason.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_REASON_S)){
						enrollee.setEnrollmentReason('S');
						isSpecial=true;
					}
				}
				
				if(terminationFlag){
					enrollee.setTerminationFlag(EnrollmentConstants.Y);
				}
				
				LookupValue evenetReasonLkp=null;
				if(isNotNullAndEmpty(disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE).toString())){
					if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE).toString()))){
						evenetReasonLkp=lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, disEnrollmentMap.get(EnrollmentConstants.TERMINATION_REASON_CODE).toString());
					}else{
						throw new RuntimeException(EnrollmentConstants.ERR_MSG_LOOKUP_VALUE_IS_NULL);
					}
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_TERM_RSN_CODE_IS_NULL_OR_EMPTY);
				}
				List<EnrollmentEvent> enrollmentEventList = new ArrayList<EnrollmentEvent>();
				EnrollmentEvent enrollmentEvent=null;
				if(!(terminationFlag && EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL.equalsIgnoreCase( enrollee.getEnrollment().getEnrollmentTypeLkp().getLookupValueCode()))){
					enrollmentEvent = new EnrollmentEvent();
					if(txn_idfier!=null){
						enrollmentEvent.setTxnIdentifier(txn_idfier.toString());
					}
					enrollmentEvent.setEnrollee(enrollee);
					enrollmentEvent.setEnrollment(enrollee.getEnrollment());
					enrollmentEvent.setEventReasonLkp(evenetReasonLkp);
					enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,EnrollmentConstants.EVENT_TYPE_CANCELLATION));
					if(loggedInUser != null){
						enrollmentEvent.setCreatedBy(loggedInUser);
						enrollmentEvent.setUpdatedBy(loggedInUser);
					}
					enrollee.setLastEventId(enrollmentEvent);
					enrollmentEventList.add(enrollmentEvent);
				}
				
				if(terminationFlag ){
					if(enrollmentEvent!=null){
						enrollmentEvent.setSendToCarrierFlag(EnrollmentConstants.FALSE);
					}
					EnrollmentEvent enrollmentChangeEvent = new EnrollmentEvent();
					if(txn_idfier!=null){
						enrollmentChangeEvent.setTxnIdentifier(txn_idfier.toString());
					}
					enrollmentChangeEvent.setEnrollee(enrollee);
					enrollmentChangeEvent.setEnrollment(enrollee.getEnrollment());
					enrollmentChangeEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,EnrollmentConstants.EVENT_REASON_AI));//Minor Improvement - HIX-68804 Change transaction: Validation error in EDI
					enrollmentChangeEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,EnrollmentConstants.EVENT_TYPE_CHANGE));
					enrollmentChangeEvent.setCreatedBy(loggedInUser);
					enrollmentChangeEvent.setUpdatedBy(loggedInUser);
					enrollee.setLastEventId(enrollmentChangeEvent);
					enrollmentEventList.add(enrollmentChangeEvent);
					if(isSpecial){
						if(!isNotNullAndEmpty(serc)){
							enrollmentChangeEvent.setSpclEnrollmentReasonLkp(evenetReasonLkp);
						}else{
							enrollmentChangeEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, serc));

						}
					}
				}
				//add null check
				
				if(isSpecial && enrollmentEvent!=null ){
					if(!isNotNullAndEmpty(serc)){
						enrollmentEvent.setSpclEnrollmentReasonLkp(evenetReasonLkp);
					}else{
						enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, serc));

					}
				}
				enrollee.setEnrollmentEvents(enrollmentEventList);
		}
		return GhixConstants.RESPONSE_SUCCESS;
	}
	
	
	private void populateEnrolleeRelationship(List<PldMemberData> memberInfoList,List<Enrollee> enrolleeList)
	 
	 {
		 for (PldMemberData pldMemberData : memberInfoList) {
			 //Enrollee sourceEnrollee = findEnrolleeByMemberId(pldMemberData.getMemberInfo().get(EnrollmentConstants.MEMBER_ID),enrolleeList );
			 List<Enrollee> sourceEnrollees = findEnrolleesByMemberId(pldMemberData.getMemberInfo().get(EnrollmentConstants.MEMBER_ID),enrolleeList );
			 if(sourceEnrollees!=null && sourceEnrollees.size()>0){
				 for(Enrollee sourceEnrollee: sourceEnrollees){
					 List<Map<String,String>> relationshipList  = pldMemberData.getRelationshipInfo();
					 List<EnrolleeRelationship> enrolleeRelationshipList=null;
					 if(sourceEnrollee.getEnrolleeRelationship()!=null){
						 enrolleeRelationshipList=sourceEnrollee.getEnrolleeRelationship();
					 }else{
						 enrolleeRelationshipList = new ArrayList<EnrolleeRelationship>();
					 }
					 //List<EnrolleeRelationship> enrolleeRelationshipList = new ArrayList<EnrolleeRelationship>();
					 if(relationshipList!=null && !relationshipList.isEmpty()){
						 for (Map<String,String> relationship:relationshipList){
							 List<Enrollee> targetEnrollees = findEnrolleesByMemberId(relationship.get(EnrollmentConstants.MEMBER_ID),enrolleeList );
							 if(targetEnrollees!=null && targetEnrollees.size()>0){
								 for(Enrollee targetEnrollee: targetEnrollees){
									 if(targetEnrollee!=null){
										 EnrolleeRelationship tmpEnrolleeRelationship = findRelationFromList(enrolleeRelationshipList,sourceEnrollee, targetEnrollee);
										 if(tmpEnrolleeRelationship==null){
											 tmpEnrolleeRelationship = new EnrolleeRelationship();
											 tmpEnrolleeRelationship.setSourceEnrollee(sourceEnrollee);
											 tmpEnrolleeRelationship.setTargetEnrollee(targetEnrollee);
											 enrolleeRelationshipList.add(tmpEnrolleeRelationship);
										 }
										 tmpEnrolleeRelationship.setRelationshipLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.RELATIONSHIP, relationship.get(EnrollmentConstants.RELATIONSHIP_CODE)));
									}
								 }
								 
							 }
							
						 }
					 }
					 if(enrolleeRelationshipList!=null && !enrolleeRelationshipList.isEmpty()){
						 sourceEnrollee.setEnrolleeRelationship( enrolleeRelationshipList);
					 }
				 }
			 }
		 }
	 }
	
	private List<Enrollment> getEnrollmentFromplanDataList(List<PldPlanData> planDataList , PldOrderResponse pldOrderResponse, 
			final AccountUser loggedInUser, Map<Enrollment,List<Map<String, String>>> finalDisEnrollmentMemberMap,PlanListResponse planListResponse) throws Exception, GIException {
		//Added for FFM
		Character enrollmentResaon = pldOrderResponse.getEnrollmentType();
		String houseHoldCaseID = pldOrderResponse.getHouseholdCaseId();
		String sadp_flag = pldOrderResponse.getSadpFlag();
		String brokerId = pldOrderResponse.getUserRoleId();
		String brokerType = pldOrderResponse.getUserRoleType();
		String employerId = pldOrderResponse.getEmployerId();
		String employeeId = pldOrderResponse.getEmployeeId();
		Long applicationId = pldOrderResponse.getApplicationId();
		Float ehbAmount = pldOrderResponse.getEhbAmount();
		//TODO String slcspPlanid = pldOrderResponse.getSlcspPlanid();
		String serc = pldOrderResponse.getSerc();
		String autoRenewalFlag =  null;
		if(isNotNullAndEmpty(pldOrderResponse.getAutoRenewal())){
			autoRenewalFlag=String.valueOf(pldOrderResponse.getAutoRenewal());
		}
		PlanResponse planResponse = null;
		Map<String, String> responsiblePerson = null;
		if (isNotNullAndEmpty(pldOrderResponse.getResponsiblePerson())) {
			responsiblePerson = pldOrderResponse.getResponsiblePerson();
		}
		Map<String, String> householdContact = null;
		if (isNotNullAndEmpty(pldOrderResponse.getHouseHoldContact())) {
			householdContact = pldOrderResponse.getHouseHoldContact();
		}
		
		Enrollment enrollment;
		ShopResponse responseFromShop = null;
		List<Enrollment> enrollmentList = new ArrayList<Enrollment>();
		for (PldPlanData pdlPlan : planDataList) {
			Map<String, String> plan = pdlPlan.getPlan();


			enrollment = new Enrollment();
			
		
			String coverageStartDate = plan.get(EnrollmentConstants.COVERAGE_START_DATE);
            Date effectiveDate = null;
            if (isNotNullAndEmpty(coverageStartDate)) {
                    String effectiveDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd HH:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
                    effectiveDate =  DateUtil.StringToDate(effectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT); 
                    enrollment.setBenefitEffectiveDate(effectiveDate); 
                    enrollment.setInd19CovStDate(effectiveDate);
            }
            
            if(isNotNullAndEmpty(pldOrderResponse.getGlobalId())){
				enrollment.setLogGlobalId(pldOrderResponse.getGlobalId());	
			}
            if(isNotNullAndEmpty(pldOrderResponse.getGiWsPayloadId())){
				enrollment.setGiWsPayloadId(pldOrderResponse.getGiWsPayloadId());
			}
            if (!isNotNullAndEmpty(pldOrderResponse.getAllowChangePlan())) {
            	enrollment.setChangePlanAllowed(EnrollmentConstants.N);
            }else {
            	enrollment.setChangePlanAllowed(pldOrderResponse.getAllowChangePlan());
            }
            if(isNotNullAndEmpty(applicationId)){
				if (plan.get(EnrollmentConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)) {
					enrollment.setEmployeeAppId(applicationId);
				}else{
					enrollment.setPriorSsapApplicationid(applicationId);
					enrollment.setSsapApplicationid(applicationId);
				}
			}
            
            if(isNotNullAndEmpty(pldOrderResponse.getPdHouseholdId())){
            	enrollment.setPdHouseholdId(Integer.parseInt(pldOrderResponse.getPdHouseholdId().trim()));
            }
            if(isNotNullAndEmpty(plan.get(EnrollmentConstants.ORDER_ITEM_ID))){
            	enrollment.setPdOrderItemId(Long.valueOf(plan.get(EnrollmentConstants.ORDER_ITEM_ID).trim()));
            }
            
            if(isNotNullAndEmpty(plan.get(EnrollmentConstants.MARKET_TYPE))){
				if (plan.get(EnrollmentConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)) {
					responseFromShop = enrollmentExternalRestUtil.getEmployerInfo(employerId,applicationId);
					if(responseFromShop != null){
						String benefitEndDate = (String)responseFromShop.getResponseData().get("coverageEndDate");
						enrollment.setBenefitEndDate(EnrollmentUtils.StringToEODDate(benefitEndDate, GhixConstants.REQUIRED_DATE_FORMAT));
					}
				}else if (plan.get(EnrollmentConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
					enrollment.setBenefitEndDate(EnrollmentUtils.getEODDate(getYearEndDate(enrollment.getBenefitEffectiveDate())));
				}
			}
            
            if(isNotNullAndEmpty(plan.get(EnrollmentConstants.MAX_APTC))){
            	enrollment.setMaxAPTCAmt(Float.parseFloat(plan.get(EnrollmentConstants.MAX_APTC)));
            }

            if(isNotNullAndEmpty(plan.get(EnrollmentConstants.MAX_STATE_SUBSIDY))){
            	enrollment.setMaxStateSubsidyAmt(new BigDecimal(plan.get(EnrollmentConstants.MAX_STATE_SUBSIDY)).setScale(2, BigDecimal.ROUND_HALF_UP));
			}

        	if (isNotNullAndEmpty(plan.get(EnrollmentConstants.APTC))) {
				enrollment.setAptcAmt(Float.parseFloat(plan.get(EnrollmentConstants.APTC)));	
				enrollment.setAptcEffDate(effectiveDate);
				enrollment.setNetPremEffDate(effectiveDate);
				if (isNotNullAndEmpty(pdlPlan.getFinancialEffectiveDate())) {
					enrollment.setFinancialEffectiveDate(pdlPlan.getFinancialEffectiveDate());
				} else {
					enrollment.setFinancialEffectiveDate(effectiveDate);
				}
			}

        	if(isNotNullAndEmpty(plan.get(EnrollmentConstants.STATE_SUBSIDY))){
        		enrollment.setStateSubsidyAmt(new BigDecimal(plan.get(EnrollmentConstants.STATE_SUBSIDY)).setScale(2, BigDecimal.ROUND_HALF_UP));
        		enrollment.setStateSubsidyEffDate(effectiveDate);
			}
			
			
			if (isNotNullAndEmpty(ehbAmount) && plan.get(EnrollmentConstants.PLAN_TYPE).equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH)) {
				enrollment.setSlcspAmt(ehbAmount);
				enrollment.setSlcspPlanid(null);
				enrollment.setSlcspEffDate(effectiveDate);
			}
			
			enrollment.setHouseHoldCaseId(houseHoldCaseID);
			if(EnrollmentConfiguration.isCaCall()) {
				String externalHouseHoldId = enrlHouseholdRepository.getIndividualCmrHouseholdIdFromHouseholdCaseId(Integer.valueOf(houseHoldCaseID));
				enrollment.setExternalHouseHoldCaseId(externalHouseHoldId);
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT))) {
				enrollment.setGrossPremiumAmt(Float.parseFloat(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT)));	
				enrollment.setGrossPremEffDate(effectiveDate);
				enrollment.setNetPremEffDate(effectiveDate);
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.NET_PREMIUM_AMT))) {
				enrollment.setNetPremiumAmt(Float.parseFloat(plan.get(EnrollmentConstants.NET_PREMIUM_AMT)));	
				enrollment.setNetPremEffDate(effectiveDate);
			}
			if(EnrollmentConstants.YES.equalsIgnoreCase(sadp_flag)){
				enrollment.setSadp(EnrollmentConstants.Y);
			}
			else{
				enrollment.setSadp(EnrollmentConstants.N);
			}

			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_PENDING));

			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.MARKET_TYPE))) {
				enrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE, plan.get(EnrollmentConstants.MARKET_TYPE)));
			}
			
			//Setting Employee Id::Start
			/*if (isNotNullAndEmpty(plan.get(EnrollmentConstants.EMPLOYEE_ID))) {
				enrollment.setEmployeeId(Integer.parseInt(plan.get(EnrollmentConstants.EMPLOYEE_ID)));
			}*/
			if (isNotNullAndEmpty(employeeId)) {
				enrollment.setEmployeeId(Integer.parseInt(employeeId));
			}
			//Setting Employee Id::End

			
			 boolean isInsuranceMatching = false;
			 String mismatchValues = StringUtils.EMPTY;

            if (isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_ID))) {
            	 planResponse = getPlanInfo(planListResponse,plan.get(EnrollmentConstants.PLAN_ID));
            	if(planResponse == null || (planResponse.getStatus() == null || planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))){
            		LOGGER.info("No Plan data found " );
            		throw new GIException(EnrollmentConstants.ERROR_CODE_301,"No Plan data found. Plan mgmt REST services returns null ",EnrollmentConstants.HIGH);
            	}

            	enrollment.setPlanId(planResponse.getPlanId());
            	enrollment.setCMSPlanID(planResponse.getIssuerPlanNumber());
            	enrollment.setPlanName(planResponse.getPlanName());
            	if(isNotNullAndEmpty(planResponse.getIssuerName())){
            		enrollment.setInsurerName(planResponse.getIssuerName());
            	}else{
            		throw new GIException(EnrollmentConstants.ERROR_CODE_301,"IssuerName from PlanMgmt sevice is null or empty",EnrollmentConstants.HIGH);
            	}
            	enrollment.setInsurerTaxIdNumber(planResponse.getTaxIdNumber());
            	enrollment.setCsrMultiplier(planResponse.getCsrAmount());
            	if(planResponse.getPlanLevel()!=null){
            		enrollment.setPlanLevel(planResponse.getPlanLevel().trim().toUpperCase());
            	}
            	enrollment.setIssuerId(planResponse.getIssuerId());
            	enrollment.setIssuerLogo(planResponse.getIssuerLogo());
            	enrollment.setHiosIssuerId(planResponse.getHiosIssuerId());
            	
            	boolean populateCSRForZeroAPTC=false;
				 
				 populateCSRForZeroAPTC=Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.POPULATE_CSR_APTC_ZERO));
		 
				
				 if (isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_TYPE)) && plan.get(EnrollmentConstants.PLAN_TYPE).equalsIgnoreCase(planResponse.getInsuranceType())) {
					 isInsuranceMatching = true;
				 }
					 else
					 {
					 mismatchValues = "PlanType: "+plan.get(EnrollmentConstants.PLAN_TYPE)+" did not match with InsuranceType: "+planResponse.getInsuranceType();
				 }
				 
            	//HIX-51125 setCsrAmt to null if plan type is DENTAL
                if(isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_TYPE)) && plan.get(EnrollmentConstants.PLAN_TYPE).equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL)){
                	enrollment.setCsrAmt(null);
                
                }else if(enrollment.getBenefitEffectiveDate() != null 
                		&& (plan.get(EnrollmentConstants.CSR) != null && !plan.get(EnrollmentConstants.CSR).equalsIgnoreCase(EnrollmentConstants.CSR_1)
                		&& (populateCSRForZeroAPTC ||(enrollment.getAptcAmt() != null && enrollment.getAptcAmt() > 0)))){
            		Date jan2015CoverageStartDate = DateUtil.StringToDate(EnrollmentConstants.JAN_2015_DATE, GhixConstants.REQUIRED_DATE_FORMAT);
            		// HIX-35797,  HIX-88110
            		// Calculate CSR using multiplier*grossPremiumAmount if effective date is after 1 JAN 2015
            		if(enrollment.getBenefitEffectiveDate().compareTo(jan2015CoverageStartDate) > 0 || enrollment.getBenefitEffectiveDate().compareTo(jan2015CoverageStartDate) == 0){
            			if(isNotNullAndEmpty(enrollment.getGrossPremiumAmt()) && isNotNullAndEmpty(planResponse.getCsrAmount()) && enrollment.getGrossPremiumAmt()>0.0f && planResponse.getCsrAmount()>0.0f){
            				enrollment.setCsrAmt(EnrollmentUtils.precision((enrollment.getGrossPremiumAmt() * planResponse.getCsrAmount()), 2));
            				enrollment.setCsrEffDate(effectiveDate);
            			}
            			else{
            				enrollment.setCsrAmt(null);
            			}
            		}

            		else {
            			if(isNotNullAndEmpty(planResponse.getCsrAmount()) && planResponse.getCsrAmount()>0.0f){
            				enrollment.setCsrAmt(planResponse.getCsrAmount());
            				enrollment.setCsrEffDate(effectiveDate);
            			}
            			else{
            				enrollment.setCsrAmt(null);
            			}
            		}
            	}
            	else{
            		enrollment.setCsrAmt(null);
            	}
            }
			else{
				throw new GIException("No Plans found");
			}

			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_TYPE))) {
				enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueLabel("INSURANCE_TYPE", plan.get(EnrollmentConstants.PLAN_TYPE).toString().toLowerCase()));
			}

			Enrollee responsiblePersonEnrollee = null;
			Map<String, String> responsiblePersonMap = responsiblePerson;
			if (responsiblePersonMap != null && isNotNullAndEmpty(responsiblePersonMap.get(EnrollmentConstants.RESPONSIBLE_PERSON_ID))) {
				responsiblePersonEnrollee = new Enrollee();
				responsiblePersonEnrollee.setExchgIndivIdentifier(responsiblePersonMap.get(EnrollmentConstants.RESPONSIBLE_PERSON_ID));
				responsiblePersonEnrollee.setFirstName(responsiblePersonMap.get("responsiblePersonFirstName"));
				responsiblePersonEnrollee.setMiddleName(responsiblePersonMap.get("responsiblePersonMiddleName"));
				responsiblePersonEnrollee.setLastName(responsiblePersonMap.get("responsiblePersonLastName"));
				//				responsiblePersonEnrollee.setSsn(responsiblePersonMap.get("responsiblePersonSsn"));
				responsiblePersonEnrollee.setTaxIdNumber(responsiblePersonMap.get("responsiblePersonSsn"));
				responsiblePersonEnrollee.setSuffix(responsiblePersonMap.get("responsiblePersonSuffix"));
				responsiblePersonEnrollee.setPreferredEmail(responsiblePersonMap.get("responsiblePersonPreferredEmail"));
				responsiblePersonEnrollee.setPreferredSMS(responsiblePersonMap.get("responsiblePersonPreferredPhone"));
				responsiblePersonEnrollee.setPrimaryPhoneNo(responsiblePersonMap.get("responsiblePersonPrimaryPhone"));
				responsiblePersonEnrollee.setSecondaryPhoneNo(responsiblePersonMap.get("responsiblePersonSecondaryPhone"));
				responsiblePersonEnrollee.setRespPersonIdCode(EnrollmentConstants.RESPONSIBLE_PERSON_ID_CODE_QD);
				Location responsiblePersonAddress = new Location();
				responsiblePersonAddress.setAddress1(responsiblePersonMap.get("responsiblePersonHomeAddress1"));
				responsiblePersonAddress.setAddress2(responsiblePersonMap.get("responsiblePersonHomeAddress2"));
				responsiblePersonAddress.setCity(responsiblePersonMap.get("responsiblePersonHomeCity"));
				responsiblePersonAddress.setState(responsiblePersonMap.get("responsiblePersonHomeState"));

				if(isNotNullAndEmpty(responsiblePersonMap.get("responsiblePersonHomeZip"))){
					responsiblePersonAddress.setZip(responsiblePersonMap.get("responsiblePersonHomeZip"));	
				}

				responsiblePersonEnrollee.setHomeAddressid(responsiblePersonAddress);
				responsiblePersonEnrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON));
				responsiblePersonEnrollee.setCreatedBy(loggedInUser);
				responsiblePersonEnrollee.setUpdatedBy(loggedInUser);
				//enrollment.setResponsiblePerson(responsiblePerson);
			}

			Enrollee houseHoldContactEnrollee = null;
			Map<String, String> householdContactMap = householdContact;
			if (isNotNullAndEmpty(householdContactMap) && isNotNullAndEmpty(householdContactMap.get(EnrollmentConstants.HOUSEHOLD_CONTACT_ID))) {
				houseHoldContactEnrollee = new Enrollee();
				houseHoldContactEnrollee.setExchgIndivIdentifier(householdContactMap.get(EnrollmentConstants.HOUSEHOLD_CONTACT_ID));
				houseHoldContactEnrollee.setFirstName(householdContactMap.get("houseHoldContactFirstName"));
				houseHoldContactEnrollee.setMiddleName(householdContactMap.get("houseHoldContactMiddleName"));
				houseHoldContactEnrollee.setLastName(householdContactMap.get("houseHoldContactLastName"));
				houseHoldContactEnrollee.setSuffix(householdContactMap.get("houseHoldContactSuffix"));
				houseHoldContactEnrollee.setPreferredEmail(householdContactMap.get("houseHoldContactPreferredEmail"));
				houseHoldContactEnrollee.setPreferredSMS(householdContactMap.get("houseHoldContactPreferredPhone"));
				houseHoldContactEnrollee.setPrimaryPhoneNo(householdContactMap.get("houseHoldContactPrimaryPhone"));
				houseHoldContactEnrollee.setSecondaryPhoneNo(householdContactMap.get("houseHoldContactSecondaryPhone"));
				houseHoldContactEnrollee.setTaxIdNumber(householdContactMap.get("houseHoldContactFederalTaxIdNumber"));

				Location houseHoldContactAddress = new Location();
				houseHoldContactAddress.setAddress1(householdContactMap.get("houseHoldContactHomeAddress1"));
				houseHoldContactAddress.setAddress2(householdContactMap.get("houseHoldContactHomeAddress2"));
				houseHoldContactAddress.setCity(householdContactMap.get("houseHoldContactHomeCity"));
				houseHoldContactAddress.setState(householdContactMap.get("houseHoldContactHomeState"));
				if(isNotNullAndEmpty(householdContactMap.get("houseHoldContactHomeZip"))){
					houseHoldContactAddress.setZip(householdContactMap.get("houseHoldContactHomeZip"));	
				}
				houseHoldContactEnrollee.setHomeAddressid(houseHoldContactAddress);
				houseHoldContactEnrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT));
				//	enrollment.setHouseholdContact(houseHoldContact);
			}

			List<PldMemberData> memberInfoList = pdlPlan.getNewMemberInfoList();
			List<Enrollee> enrolleeList = getEnrolleeFromMemberInfoList(enrollment, memberInfoList, enrollmentResaon, loggedInUser);
			if (houseHoldContactEnrollee != null){
				houseHoldContactEnrollee.setCreatedBy(loggedInUser);
				houseHoldContactEnrollee.setUpdatedBy(loggedInUser);
				enrolleeList.add(houseHoldContactEnrollee);
			}
			if (responsiblePersonEnrollee != null){
				responsiblePersonEnrollee.setCreatedBy(loggedInUser);
				responsiblePersonEnrollee.setUpdatedBy(loggedInUser);
				enrolleeList.add(responsiblePersonEnrollee);
			}

			populateEnrolleeRelationship(memberInfoList, enrolleeList);
			
			if(isNotNullAndEmpty(planResponse)){
				if(isNotNullAndEmpty(planResponse.getEhbPercentage()) && isNotNullAndEmpty(enrollment.getGrossPremiumAmt())){
					Float percentOfEssEhbAmt=Float.parseFloat(planResponse.getEhbPercentage());
					if(percentOfEssEhbAmt==null || percentOfEssEhbAmt==0.0f){
						throw new GIException(EnrollmentConstants.INVALID_EHB_PERCENT);
					}
					enrollment.setEhbAmt(percentOfEssEhbAmt * enrollment.getGrossPremiumAmt());
					enrollment.setEhbEffDate(effectiveDate);
					enrollment.setEhbPercent(percentOfEssEhbAmt);
				}
				if(isNotNullAndEmpty(planResponse.getStateEhbMandatePercentage()) && isNotNullAndEmpty(enrollment.getGrossPremiumAmt())){
					Float percentOfStateEhbAmt=Float.parseFloat(planResponse.getStateEhbMandatePercentage());
					enrollment.setStateEhbAmt(percentOfStateEhbAmt * enrollment.getGrossPremiumAmt());
					enrollment.setStateEhbEffDate(effectiveDate);
					enrollment.setStateEhbPercent(percentOfStateEhbAmt);
				}
				if(isNotNullAndEmpty(planResponse.getPediatricDentalComponent()) && enrollment.getInsuranceTypeLkp() != null && enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_INSURANCE_TYPE_DENTAL_CODE)){
					Float percentOfDentalEhbAmt = Float.parseFloat(planResponse.getPediatricDentalComponent().replace("$", ""));
					enrollment.setDntlEhbEffDate(effectiveDate);
					enrollment.setDntlEssentialHealthBenefitPrmDollarVal(percentOfDentalEhbAmt);
					//HIX-85422 2017 EHB portion for SADPs is a percentage of total premium and not a dollar amount
					if(planResponse.getPlanYear() >= 2017 && enrollment.getEnrollmentTypeLkp() != null 
							&& enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
						if(percentOfDentalEhbAmt<0 || percentOfDentalEhbAmt>1){
							throw new GIException(EnrollmentConstants.ERROR_CODE_301,"percentOfDentalEhbAmt comming from PlanMgmgt response is not valid ",EnrollmentConstants.HIGH);
						}
						enrollment.setDntlEhbAmt(percentOfDentalEhbAmt * enrollment.getGrossPremiumAmt());
					}else{
						enrollment.setDntlEhbAmt(percentOfDentalEhbAmt * getEnrolleeCountForDntlEhb(enrolleeList));
					}
				}
			}
			
			String subscriberZip="";
			String subscriberCountyCode="";
			//Find subscriber
			Enrollee subscriber = null;
			for (Enrollee enrollee:enrolleeList){
				if(enrollee.getPersonTypeLkp()!=null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
					subscriber = enrollee;
					break;
				}
			}

			if(subscriber != null){
				
				Location subHomeAddr = subscriber.getHomeAddressid();
				if (subHomeAddr !=null){
					if(subHomeAddr.getZip()!=null){
						subscriberZip = subHomeAddr.getZip();
					}
					if(subHomeAddr.getCountycode()!=null){
						subscriberCountyCode = subHomeAddr.getCountycode();
					}
					
				}
				//HIX-51430
				StringBuilder subscriberName = new StringBuilder();

				if(isNotNullAndEmpty(subscriber.getFirstName())){
					subscriberName.append(subscriber.getFirstName());
					subscriberName.append(" ");
				}
				if(isNotNullAndEmpty(subscriber.getMiddleName())){
					subscriberName.append(subscriber.getMiddleName());
					subscriberName.append(" ");	
				}
				if(isNotNullAndEmpty(subscriber.getLastName())){
					subscriberName.append(subscriber.getLastName());
					subscriberName.append(" ");
				}
				if(isNotNullAndEmpty(subscriber.getSuffix())){
					subscriberName.append(subscriber.getSuffix());	
				}
				if(subscriberName.length() > EnrollmentConstants.SPONSORNAME_MAX_LENGTH ){
					subscriberName.delete(EnrollmentConstants.SPONSORNAME_MAX_LENGTH,subscriberName.length());
				}
				enrollment.setExchgSubscriberIdentifier(subscriber.getExchgIndivIdentifier());
				enrollment.setSubscriberName(subscriberName.toString().trim());
			}
			else {
				LOGGER.info("Could not find Subscriber for the Enrollment ID:" + enrollment.getId());
			}

			/*******************************************setting broker information********************************************************************/
			String inputId = null;
			boolean oldBrokerApi=false;
			DesignateAssisterResponse designateAssisterResponse = null;
			try{
				if(enrollment.getEnrollmentTypeLkp() != null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
					if(EnrollmentConfiguration.isCaCall()) {// HIX-108051
						if(brokerId!=null) {
							inputId = brokerId;
							oldBrokerApi=true;
						}else if(houseHoldContactEnrollee!=null){
							inputId = houseHoldContactEnrollee.getExchgIndivIdentifier();
						}
					} else {
						inputId = enrollment.getHouseHoldCaseId();
					}
					if(EnrollmentConfiguration.isMnCall() && enrollment.getHouseHoldCaseId()!=null) {
						designateAssisterResponse = enrollmentExternalRestUtil.getExternalBrokerDetails(enrollment.getHouseHoldCaseId());
					}
				}else {
					inputId= employerId;
				}
				
				if(isNotNullAndEmpty(inputId) && !EnrollmentConfiguration.isMnCall()){
					EntityResponseDTO brokerResponse = null;
					if(oldBrokerApi) {
						brokerResponse  = enrollmentExternalRestUtil.getBrokerDetail(inputId);
					}else {
						brokerResponse  = enrollmentExternalRestUtil.getAssisterBrokerDetail(inputId);
					}
					if(null != brokerResponse && isNotNullAndEmpty(brokerResponse.getResponseCode()) && brokerResponse.getResponseCode() == EnrollmentConstants.ERROR_CODE_200){
						enrollment.setBrokerTPAFlag(EnrollmentConstants.Y);
						enrollment.setAssisterBrokerId(Integer.valueOf(""+brokerResponse.getBrokerId()));
						if(oldBrokerApi) {
							enrollment.setBrokerRole(brokerType);
						}else {
							enrollment.setBrokerRole(brokerResponse.getRole());
						}
						if (EnrollmentConfiguration.isNvCall()) {
							if (isNotNullAndEmpty(brokerResponse.getAgentNPN())) {
								enrollment.setBrokerTPAAccountNumber1(brokerResponse.getAgentNPN());
							}
							if (isNotNullAndEmpty(brokerResponse.getStateLicenseNumber())) {
								enrollment.setBrokerTPAAccountNumber2(brokerResponse.getStateLicenseNumber());
							}
						} else {
							if (isNotNullAndEmpty(brokerResponse.getStateLicenseNumber())) {
								enrollment.setBrokerTPAAccountNumber1(brokerResponse.getStateLicenseNumber());
							}
						}
						if(isNotNullAndEmpty(brokerResponse.getBrokerName())){
							enrollment.setAgentBrokerName(brokerResponse.getBrokerName());
						}
						if(isNotNullAndEmpty(brokerResponse.getStateEINNumber())){
							enrollment.setBrokerFEDTaxPayerId(brokerResponse.getStateEINNumber());
						}
					}else{
						LOGGER.info("Getting failure response code data from ghix-broker. inputId : " +inputId + "houseHoldCaseID :" + houseHoldCaseID);
					}
				}
				else {
					if(isNotNullAndEmpty(designateAssisterResponse) && designateAssisterResponse.getAssisterType() != null && designateAssisterResponse.getAssisterType().equalsIgnoreCase(EnrollmentConstants.EXTERNAL_ASSISTER)  && designateAssisterResponse.getErrCode() == EnrollmentConstants.ERROR_CODE_200) {
						if(isNotNullAndEmpty(designateAssisterResponse.getAssisterType())) {
						enrollment.setBrokerRole(EnrollmentConstants.AGENT_ROLE);
						}
						if(isNotNullAndEmpty(designateAssisterResponse.getId())) {
						enrollment.setAssisterBrokerId(designateAssisterResponse.getId());
						}
						if(isNotNullAndEmpty(designateAssisterResponse.getAssisterName())) {
						enrollment.setAgentBrokerName(designateAssisterResponse.getAssisterName());
						}
						enrollment.setBrokerFEDTaxPayerId("999999999");
						enrollment.setBrokerTPAFlag(EnrollmentConstants.Y);
						if(isNotNullAndEmpty(designateAssisterResponse.getAgentNpn())) {
							enrollment.setBrokerTPAAccountNumber1(designateAssisterResponse.getAgentNpn());
						}				
					}
					else {
						LOGGER.info("Getting failure response code data from ghix-external-assister HouseHoldCaseId : " +enrollment.getHouseHoldCaseId());
					}
					
				}
			}catch(Exception e){
				LOGGER.error("input id : "+inputId);
				if(oldBrokerApi) {
					LOGGER.error("broker id : "+inputId);
					LOGGER.error(BrokerServiceEndPoints.GET_BROKER_DETAIL);
				}else {
					LOGGER.error("ouseHoldContactEnrollee or Householdcase or Employer id : "+inputId);
					LOGGER.error(BrokerServiceEndPoints.GET_ASSISTER_BROKER_DETAIL);
				}
				LOGGER.error("Getting exception while getting assister broker data from the ghix-broker" , e);
			}
			/*********************************************End of setting broker information***************************************************/
			
			boolean isEmployerStatusActiveAndEnrollmentReasonNew=false;
			// HIX-6649, HIX-6695 :: the below snippets is to populate the employeeId, employee contribution, employer contribution in enrollment if the enrollment type is "SHOP" 
			if(enrollment.getEnrollmentTypeLkp()!=null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
				if (isNotNullAndEmpty(employerId)) {
					Employer employer = new Employer();
					employer.setId(Integer.parseInt(employerId));
					enrollment.setEmployer(employer);

//					String shopInfoResponse=null;
					EmployerDTO employerDTO = null;

					if(responseFromShop != null){
						employerDTO = (EmployerDTO) responseFromShop.getResponseData().get("Employer");
						Integer employerEnrollmentId = (Integer) responseFromShop.getResponseData().get("EmployerEnrollmentId");
						enrollment.setEmployerEnrollmentId(employerEnrollmentId);
						if(employerDTO != null){

							if(employerDTO.getName() != null){			            	
								enrollment.setSponsorName(employerDTO.getName());
							}
							if(employerDTO.getExternalId() != null){
								enrollment.setEmployerCaseId(employerDTO.getExternalId());
							}

							if(employerDTO.getFederalEIN() != null){	
								enrollment.setSponsorEIN(employerDTO.getFederalEIN().toString());
							}

							if(employerDTO.getExternalId()!= null){	
								enrollment.setExternalEmployerId(employerDTO.getExternalId());
							}
						}

						// setting the enrollment status to PAYMENT_RECEIVED when the employer enrollment status is ACTIVE & enrollment reason is 'N' 
						EmployerEnrollment.Status employerEnrollmentStatus = (EmployerEnrollment.Status) responseFromShop.getResponseData().get(EnrollmentConstants.ENROLLMENT_STATUS_KEY1); 
						if(employerEnrollmentStatus!=null  && (employerEnrollmentStatus.equals(Status.ACTIVE) || employerEnrollmentStatus.equals(Status.TERMINATED) )){
							enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
							isEmployerStatusActiveAndEnrollmentReasonNew=true;
						}
					}
				}
				
				if (isNotNullAndEmpty(plan.get(EnrollmentConstants.EMPLOYEE_CONTRIBUTION))) {
					enrollment.setEmployerContribution(Float.parseFloat(plan.get(EnrollmentConstants.EMPLOYEE_CONTRIBUTION)));
					enrollment.setEmplContEffDate(effectiveDate);
				}

				if (isNotNullAndEmpty(plan.get(EnrollmentConstants.DEPENDENT_CONTRIBUTION))) {
					//enrollment.setEmployerContribution(enrollment.getEmployerContribution() + Float.parseFloat(plan.get(EnrollmentConstants.DEPENDENT_CONTRIBUTION)));
					enrollment.setEmployerContribution(enrollment.getEmployerContribution()==null ? Float.parseFloat(plan.get(EnrollmentConstants.DEPENDENT_CONTRIBUTION)) :(enrollment.getEmployerContribution() + Float.parseFloat(plan.get(EnrollmentConstants.DEPENDENT_CONTRIBUTION))));
				}

				if (isNotNullAndEmpty(plan.get(EnrollmentConstants.NET_PREMIUM_AMT))) {
					enrollment.setEmployeeContribution(Float.parseFloat(plan.get(EnrollmentConstants.NET_PREMIUM_AMT)));	
				}

			}else if(enrollment.getEnrollmentTypeLkp()!=null 
					&& enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
				
				Enrollee sponsorEnrollee = houseHoldContactEnrollee;
				if(EnrollmentConfiguration.isCaCall() && subscriber!=null){
					int subscriberAge = EnrollmentUtils.getAge(subscriber.getBirthDate(),  subscriber.getEffectiveStartDate());
					if(subscriberAge >= EnrollmentConstants.AGE_18 && houseHoldContactEnrollee != null){
						sponsorEnrollee =subscriber;
					}
				}
				
				StringBuilder householdConttactName = new StringBuilder();
				if(null != sponsorEnrollee){
					if(isNotNullAndEmpty(sponsorEnrollee.getFirstName())){
						householdConttactName.append(sponsorEnrollee.getFirstName());
						householdConttactName.append(" ");
					}
					if(isNotNullAndEmpty(sponsorEnrollee.getMiddleName())){
						householdConttactName.append(sponsorEnrollee.getMiddleName());
						householdConttactName.append(" ");	
					}
					if(isNotNullAndEmpty(sponsorEnrollee.getLastName())){
						householdConttactName.append(sponsorEnrollee.getLastName());
						householdConttactName.append(" ");
					}
					if(isNotNullAndEmpty(sponsorEnrollee.getSuffix())){
						householdConttactName.append(sponsorEnrollee.getSuffix());	
					}

					if(householdConttactName.length() > EnrollmentConstants.SPONSORNAME_MAX_LENGTH ){
						householdConttactName.delete(EnrollmentConstants.SPONSORNAME_MAX_LENGTH,householdConttactName.length());
					}
					enrollment.setSponsorTaxIdNumber(sponsorEnrollee.getTaxIdNumber());
					enrollment.setSponsorName(householdConttactName.toString().trim());
				}
			}
			/**Setting RenewalFlag for SHOP as incomming*/
			//enrollment.setRenewalFlag(autoRenewalFlag);
			if(autoRenewalFlag!=null){
				boolean autoRenewalFlagBool =  getBooleanFromPlanDisplayFlags(String.valueOf(autoRenewalFlag));
				if(autoRenewalFlagBool){
					enrollment.setRenewalFlag("A");
				}else{
					enrollment.setRenewalFlag("M");
				}
			}
			boolean useMigratedEnrollmentsConfig = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.USE_MIGRATION_ENROLLMENTS));
			boolean autoTermPriorEnrollment = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.AUTO_TERM_FOR_RENEWALS));
			boolean isIssuerSame= false;
			boolean isSamePlan =  false;
			boolean isPriorEnrollmentConfirmed =  false;
			if(autoRenewalFlag!=null && enrollment.getEnrollmentTypeLkp()!=null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
				if(enrollmentResaon!=null && (enrollmentResaon.equals('A') || enrollmentResaon.equals('a'))){

					Long existingEnrollmentId = null;
					if(enrolleeList !=null && !enrolleeList.isEmpty()){
						for(Enrollee renewalEnrollee : enrolleeList){
							if(isNotNullAndEmpty(enrollment.getInsuranceTypeLkp()) && enrollment.getInsuranceTypeLkp().getLookupValueLabel().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH) && renewalEnrollee.getExistingHealthEnrollmentId() != null && renewalEnrollee.getExistingHealthEnrollmentId() > 0){
								existingEnrollmentId = renewalEnrollee.getExistingHealthEnrollmentId();
								break;
							}else if(isNotNullAndEmpty(enrollment.getInsuranceTypeLkp()) && enrollment.getInsuranceTypeLkp().getLookupValueLabel().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL) && renewalEnrollee.getExistingDentalEnrollmentId() != null && renewalEnrollee.getExistingDentalEnrollmentId() > 0){
								existingEnrollmentId = renewalEnrollee.getExistingDentalEnrollmentId();
								break;
							}
						}
						if (useMigratedEnrollmentsConfig) {
							enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
									EnrollmentConstants.ENROLLMENT_STATUS,
									EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
							enrollment.setEnrollmentConfirmationDate(new TSDate());
							isIssuerSame=true;
							enrollment.setPriorEnrollmentId(existingEnrollmentId);
							
						} else if (isNotNullAndEmpty(existingEnrollmentId)){
							Integer oldEnrollmentId = Integer.valueOf(existingEnrollmentId.toString());
							Enrollment priorEnrollment = enrollmentRepository.findById(oldEnrollmentId);

							if(priorEnrollment != null){
								
								if(priorEnrollment.getEnrollmentStatusLkp() != null && priorEnrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)){
									isPriorEnrollmentConfirmed = true;
								}

								isIssuerSame= EnrollmentUtils.isSameIssuer(priorEnrollment.getCMSPlanID(), enrollment.getCMSPlanID());
								/*  
								*Commenting Crosswalk call as PlanDisplay already called the Crosswalk API and passed crosswalked Plan to Enrollment*
								
								String crossWalkPlanId =null;
								crossWalkPlanId = enrollmentExternalRestUtil.getHiosPlanIdFromCrossWalkApi(priorEnrollment.getCMSPlanID(), 
										enrollment.getBenefitEffectiveDate(), 
										enrollment.getCatastrophicEligible(),
										subscriberZip,subscriberCountyCode);
								*/
								
								/** same plan check*/
								if(!EnrollmentConfiguration.isCaCall()){
									if(isIssuerSame) {
										/**same issuer case - always plan will the same */
										isSamePlan = true;
										enrollment.setRenewalSamePlan(EnrollmentConstants.Y);
									}else {
										/** different issuer case - always plan will the different */
										isSamePlan = false;
										enrollment.setRenewalSamePlan(EnrollmentConstants.N);
									}
								}
								else{
									if(priorEnrollment.getCMSPlanID()!=null && enrollment.getCMSPlanID()!=null){
										isSamePlan= EnrollmentUtils.compareString(priorEnrollment.getCMSPlanID().substring(0, 14), enrollment.getCMSPlanID().substring(0, 14));
										if(isSamePlan){
											enrollment.setRenewalSamePlan(EnrollmentConstants.Y);
										}else{
											enrollment.setRenewalSamePlan(EnrollmentConstants.N);
										}
									}
								}
								
								if (isIssuerSame) {
									enrollment.setPriorEnrollmentId(existingEnrollmentId);
								}
																
								/*if(enrollment.getRenewalFlag()!=null && "M".equalsIgnoreCase(enrollment.getRenewalFlag())){
									
									crossWalkPlanId = enrollmentExternalRestUtil.getHiosPlanIdFromCrossWalkApi(priorEnrollment.getCMSPlanID(), 
																			enrollment.getBenefitEffectiveDate(), 
																			enrollment.getCatastrophicEligible(),
																			subscriberZip,subscriberCountyCode);
									if(priorEnrollment.getCMSPlanID()!=null && crossWalkPlanId!=null){
										isSamePlan= EnrollmentUtils.compareString(enrollment.getCMSPlanID().substring(0, 14), crossWalkPlanId.substring(0, 14));
										if(isSamePlan){
											enrollment.setRenewalSamePlan(EnrollmentConstants.Y);
										}else{
											enrollment.setRenewalSamePlan(EnrollmentConstants.N);
										}
									}
								}else{
									*//** Auto renewal case - always it will the same plan*//*
									isSamePlan = true;
									enrollment.setRenewalSamePlan(EnrollmentConstants.Y);
								}*/
								
								/**CA Auto and Manual extra code Start*/
								if(EnrollmentConfiguration.isCaCall()){
									if(isPriorEnrollmentConfirmed && (isSamePlan || isIssuerSame )){
										//confirmed enrollment and Same Plan or same issuer 
											/**If Prior year enrollment is confirmed, the renewed enrollment goes to confirmed.*/
												enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
												enrollment.setEnrollmentConfirmationDate(new TSDate());
									}else{
										/** Diff issuer and diff plan*/
											/**Terminate prior year enrollment only if in confirmed state and in case of manual renewal */
										if(isPriorEnrollmentConfirmed && isNotNullAndEmpty(enrollment.getRenewalFlag()) && enrollment.getRenewalFlag().equalsIgnoreCase("M")){
													finalDisEnrollmentMemberMap = disEnrollEnrollmentForRenewal(priorEnrollment, true, null, loggedInUser);
													enrollment.setPriorEnrollmentTermFlag(EnrollmentConstants.Y);
												}
											}
								}
								else{ 
									/**Common renewal code for other states*/
									if(isPriorEnrollmentConfirmed && (isSamePlan || isIssuerSame )){
										enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
										enrollment.setEnrollmentConfirmationDate(new TSDate());
									}
									if(isIssuerSame){
										if(isNotNullAndEmpty(enrollment.getRenewalFlag()) && enrollment.getRenewalFlag().equalsIgnoreCase("A")){
											if(autoTermPriorEnrollment) {
												LOGGER.info("Auto termination in passive renewals enabled. Terminating prior enrollment {}", priorEnrollment.getId());
												finalDisEnrollmentMemberMap = disEnrollEnrollmentForRenewal(priorEnrollment, true, EnrollmentConstants.RENEWAL_FLAG_RENP, loggedInUser);
												enrollment.setPriorEnrollmentTermFlag(EnrollmentConstants.Y);												
											}
										}else{
											finalDisEnrollmentMemberMap = disEnrollEnrollmentForRenewal(priorEnrollment, true, EnrollmentConstants.RENEWAL_FLAG_REN, loggedInUser);
											enrollment.setPriorEnrollmentTermFlag(EnrollmentConstants.Y);
										}
									}else {
										finalDisEnrollmentMemberMap = disEnrollEnrollmentForRenewal(priorEnrollment, true, null, loggedInUser);
										enrollment.setPriorEnrollmentTermFlag(EnrollmentConstants.Y);
									}
									
								} 
							}else{
								throw new GIException("No Prior Enrollment found for Renewal. Enrollment id:- " + existingEnrollmentId);
							}
						}
						else{
							throw new GIException("Prior Enrollment Id not found for Renewal.");
						}
					}
				}
			}
			
						
			List<EnrollmentEvent> enrollmentEventList= new ArrayList<EnrollmentEvent>();
			
			EnrollmentEvent.TRANSACTION_IDENTIFIER txn_Idfier= null;
			if(isNotNullAndEmpty(enrollmentResaon) && enrollmentResaon == 'S'){
				txn_Idfier= EnrollmentEvent.TRANSACTION_IDENTIFIER.SPECIAL_ENROLLMENT;
			}else if(isNotNullAndEmpty(enrollment.getRenewalFlag())){
				txn_Idfier= EnrollmentEvent.TRANSACTION_IDENTIFIER.RENEWAL_ENROLLMENT;
			}else{
				txn_Idfier= EnrollmentEvent.TRANSACTION_IDENTIFIER.INITIAL_ENROLLMENT;
			}

			for (Enrollee tempenrollee : enrolleeList) {

				tempenrollee.setEnrollment(enrollment);
				if(tempenrollee.getCustodialParent()!=null){
					tempenrollee.getCustodialParent().setEnrollment(enrollment);
				}
				// setting the enrollee status to PAYMENT_RECEIVED when the employer enrollment status is ACTIVE & enrollment reason is 'N' or isEmployerStatusActiveAndEnrollmentReasonNew is true)
				if(isEmployerStatusActiveAndEnrollmentReasonNew 
					&&(tempenrollee.getPersonTypeLkp() != null && ! tempenrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON) && ! tempenrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT))){
						tempenrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
				}
								
				//creating EnrollmentEvent
				EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
				if(txn_Idfier!=null){
					enrollmentEvent.setTxnIdentifier(txn_Idfier.toString());
				}
				enrollmentEvent.setCreatedBy(loggedInUser);
				enrollmentEvent.setUpdatedBy(loggedInUser);
				enrollmentEvent.setEnrollee(tempenrollee);
				enrollmentEvent.setEnrollment(enrollment);
				enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,EnrollmentConstants.EVENT_REASON_BENEFIT_SELECTION));
				enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_ADDITION));
				
				if(isNotNullAndEmpty(pdlPlan.getQleReason()) ){
					enrollmentEvent.setQleIdentifier(pdlPlan.getQleReason());
				}
				
				// This is used only in case of special enrollment. So it may not be present for Initial Enrollment
				//add null check
				if(isNotNullAndEmpty(enrollmentResaon) && enrollmentResaon == 'S'){
					
					if(isNotNullAndEmpty(tempenrollee.getMaintainenceReasonCode()) 
							&& !EnrollmentConfiguration.isCaCall() 
							&& isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  tempenrollee.getMaintainenceReasonCode()))){
						
						enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  tempenrollee.getMaintainenceReasonCode()));
					}
					if(isNotNullAndEmpty(serc)){
						enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON,  serc));
					}
				}
				
				/** Enrollee related Auto Renewal changes */
				//Individual condition is added as the story is only for Idaho  
				if(isNotNullAndEmpty(enrollment.getRenewalFlag()) && enrollment.getEnrollmentTypeLkp()!=null && EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL.equalsIgnoreCase(enrollment.getEnrollmentTypeLkp().getLookupValueCode())){
					
					/** Confirm enrollee only if prior enrollment was confirmed and if issuer is same*/
					if((isPriorEnrollmentConfirmed && isIssuerSame) || useMigratedEnrollmentsConfig){
						
						if(tempenrollee.getPersonTypeLkp()!=null && tempenrollee.getPersonTypeLkp().getLookupValueCode()!=null 
								&& (tempenrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)
										|| tempenrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE))){
							
							tempenrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
						}
					}
					
					enrollmentEvent.setSendToCarrierFlag("true");
					if(!isIssuerSame) {
						enrollmentEvent.setSendRenewalTag(null);
					}
					else if(enrollment.getRenewalFlag().equalsIgnoreCase("M") && (isSamePlan ||isIssuerSame)){
						enrollmentEvent.setSendRenewalTag(EnrollmentConstants.RENEWAL_FLAG_REN);
					}
					else if(enrollment.getRenewalFlag().equalsIgnoreCase("A")){
						enrollmentEvent.setSendRenewalTag(EnrollmentConstants.RENEWAL_FLAG_RENP);
					}
					if(!(EnrollmentConfiguration.isCaCall() || EnrollmentConfiguration.isIdCall())){
						enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,EnrollmentConstants.EVENT_REASON_REENROLLMENT));
						enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_ADDITION));
					}else{
						if(isSamePlan){
							enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,EnrollmentConstants.EVENT_REASON_REENROLLMENT));
							enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_ADDITION));
						}else if(isIssuerSame){
							enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,EnrollmentConstants.EVENT_REASON_PLAN_CHANGE));
							enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_ADDITION));
							
						}
					}
				}

				
				//added last event Id for Audit
				tempenrollee.setLastEventId(enrollmentEvent);
				enrollmentEventList.add(enrollmentEvent);
			}
			//Setting PaymentTxnId
			boolean populatePaymentTXN= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.POPULATE_PAYMENT_TXN_ID));
			if(populatePaymentTXN && isNotNullAndEmpty(EnrollmentConfiguration.returnStateCode()) && !isNotNullAndEmpty(enrollment.getPaymentTxnId())){
				//String sequence = getSequenceNextVal("PAYMENT_TXN_SEQ");
				String sequence = getPaymentSeqNextVal();
				enrollment.setPaymentTxnId(EnrollmentConfiguration.returnStateCode()+ StringUtils.leftPad(sequence,EnrollmentConstants.ELEVEN,'0'));
			}
			
			//enrollment.setRenewalFlag(autoRenewalFlag);
			enrollment.setEnrollmentEvents(enrollmentEventList);
			if (isInsuranceMatching) {
				enrollmentList.add(enrollment);
			} else {
				throw new GIException(EnrollmentConstants.INSURANCE_TYPE_MISMATCH + " - Reason: "+mismatchValues);
			}
			enrollment.setEnrollees(enrolleeList);
			enrollment.setCreatedBy(loggedInUser);
			enrollment.setUpdatedBy(loggedInUser);
			
			//HIX-117679 Adding benchmark premium call for CA
			if(EnrollmentConfiguration.isCaCall() && enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE) ) {
				getSLCSPAmount(enrollment, subscriber, effectiveDate);
				enrollment.setSlcspEffDate(effectiveDate);
			}
			if(!EnrollmentConfiguration.isCaCall() && enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE) 
					&&enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)
					&& (enrollment.getSlcspAmt() ==null || enrollment.getSlcspAmt()<=0.0f) && enrollment.getAptcAmt() != null && enrollment.getAptcAmt() > 0.0f
					){
				String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
				enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.INVALID_SLCSP_AMOUNT, EnrollmentConstants.INVALID_SLCSP_AMOUNT, EnrollmentConstants.EMPTY_OR_ZERO_SLCSP +pldOrderResponse.getApplicationId());
			}
			boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
			if(populateMonthlyPremium){
				enrollmentRequotingService.populateMonthlyPremiumsForNewEnrollment(enrollment);
				enrollment.setIsPremiumCalculated(Boolean.TRUE);
			}
		}
		return enrollmentList;
	}
	
	private String getPaymentSeqNextVal() {
		Long seq = null;
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			seq = enrollmentRepository.getPaymentSeqNextValPostgres();
		}else{
			seq = enrollmentRepository.getPaymentSeqNextValOracle();
		}
		if (seq != null) {
			return "" + seq;
		} else {
			throw new GIRuntimeException("Could not generate payment Sequence no for enrollment");
		}
	}
	
	private Map<Enrollment,List<Map<String, String>>> disEnrollEnrollmentForRenewal(Enrollment enrollment, boolean createCarrierSendEvents, String sendRenewalTag, final AccountUser loggedInUser){
		 Map<Enrollment,List<Map<String, String>>> finalDisEnrollmentMemberMap = null;
		 
		 if(enrollment != null){
			 List<Enrollee> activeEnrolleeList = enrollment.getActiveEnrolleesForEnrollment();
			 List<Map<String, String>> disEnrollMemberList = new ArrayList<Map<String,String>>();
			 
			 enrollment.setBenefitEndDate(EnrollmentUtils.getYearEndDate(enrollment.getBenefitEffectiveDate()));
			 enrollment.setUpdatedBy(loggedInUser);
			 
			 if(activeEnrolleeList != null){
				 for(Enrollee enrollee : activeEnrolleeList){
					 enrollee.setEffectiveEndDate(EnrollmentUtils.getYearEndDate(enrollment.getBenefitEffectiveDate()));
					 enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
					 enrollee.setDisenrollTimestamp(new TSDate());
					 EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
					 enrollmentEvent.setSendRenewalTag(sendRenewalTag);
					 enrollmentEvent.setEnrollee(enrollee);
					 enrollmentEvent.setEnrollment(enrollment);
					 enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,EnrollmentConstants.EVENT_TYPE_CANCELLATION));
					 enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.RENEWAL_DISENROLLMENT.toString());
					 //TODO -- Decide What Should be reason code for Cancellation/Termination
					 enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, EnrollmentConstants.TERMINATION_REASON_TERMINATION_OF_BENIFITS));

					 /***** Sets Termination events send to carrier flag to true/false *****/
					 /***** True in case carrier/issuer is changed *****/
					 /***** False in case carrier/issuer is same. Plan may or May not change *****/
					 enrollmentEvent.setSendToCarrierFlag(String.valueOf(createCarrierSendEvents));

					 //added last event Id for Audit
					 enrollee.setLastEventId(enrollmentEvent);

					 enrollmentEvent.setCreatedBy(loggedInUser);

					 /***** Populate Data for IND-20 DisEnrollments :: Begin *****/
					 Map<String, String> disEnrolledMemberMap = new HashMap<String, String>();
					 disEnrolledMemberMap.put(EnrollmentConstants.PLAN_TYPE, enrollment.getInsuranceTypeLkp().getLookupValueLabel().toUpperCase());
					 disEnrolledMemberMap.put(EnrollmentConstants.MEMBER_ID, enrollee.getExchgIndivIdentifier());
					 disEnrolledMemberMap.put(EnrollmentConstants.ENROLLMENT_ID, enrollment.getId().toString());
					 
					 /**
					  * following code is commented as per  AHBX email communication on 8/28/2014. 
					  * in manual renewals, the ind20 will not send the 2014 enrollment id back for enrollment type ‘A’ and auto renewal flag ‘N’. In AHBX, 
					  * the IND20 will have logic to mark 2014 enrollment records ended if the enrollment type is ‘A’ and auto renewal flag is ‘N’.
					  */
					 

					 //TODO Based on Discussion with Accenture team, whether this needs to be sent in disenroll loop or 
					 // Eligible member loop sending existing enrollmentId this needs to be changed.
					 disEnrolledMemberMap.put(EnrollmentConstants.IS_ENROLLMENT_DISENROLLED, "true");

					 disEnrollMemberList.add(disEnrolledMemberMap);
					 /***** Populate Data for IND-20 DisEnrollments :: Ends *****/
				 }
				 enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
				 enrollment.setAbortTimestamp(new TSDate());
				 //TODO In case we pass enrollment object as method argument, no need to invoke save, the save will be invoked in calling method.
				 //Removing save flow HIX-88748 since it was causing issue for autoEnroll
				 //enrollmentRepository.save(enrollment);

				 if(finalDisEnrollmentMemberMap == null){
					 finalDisEnrollmentMemberMap = new HashMap<Enrollment, List<Map<String,String>>>();
				 }
				 finalDisEnrollmentMemberMap.put(enrollment, disEnrollMemberList);
			 }
		 }
		 return finalDisEnrollmentMemberMap;
	 }
	
	public boolean getBooleanFromPlanDisplayFlags(String planDisplayFlag) {
		boolean flag = false;
		if (isNotNullAndEmpty(planDisplayFlag) && (planDisplayFlag.equalsIgnoreCase(EnrollmentConstants.YES)
				|| planDisplayFlag.equalsIgnoreCase(EnrollmentConstants.Y))) {
			flag = true;
		}
		return flag;
	}
	
	private int getEnrolleeCountForDntlEhb(List<Enrollee> enrollees) {
		int activeEnrolleeCount = 0;
		if (enrollees != null) {
			for (Enrollee enrollee : enrollees) {
				boolean isActiveEnrollee = (enrollee.getPersonTypeLkp().getLookupValueCode()
						.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)
						|| enrollee.getPersonTypeLkp().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))
						&& (!enrollee.getEnrolleeLkpValue().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))
						&& (!enrollee.getEnrolleeLkpValue().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
						&& (!enrollee.getEnrolleeLkpValue().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED));
				if (isActiveEnrollee && (null != enrollee.getTotalIndvResponsibilityAmt())
						&& (enrollee.getTotalIndvResponsibilityAmt().compareTo(0f) > 0)) {
					activeEnrolleeCount++;
				}
			}
		}
		return activeEnrolleeCount;
	}
	
	private List<Enrollee> getEnrolleeFromMemberInfoList(Enrollment enrollment ,List<PldMemberData> memberInfoList, Character enrollmentReason, final AccountUser loggedInUser) throws GIException {
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		Enrollee enrollee;
		for (PldMemberData pldMemberData : memberInfoList) {
			Map<String, String> member = pldMemberData.getMemberInfo();
			enrollee = new Enrollee();
			enrollee.setNewMember(true);
			if(isNotNullAndEmpty(member.get(EnrollmentConstants.EXISTING_QHP_ENROLLMENT_ID))){
				enrollee.setExistingHealthEnrollmentId(Long.valueOf(member.get(EnrollmentConstants.EXISTING_QHP_ENROLLMENT_ID)));
			}
			if(isNotNullAndEmpty(member.get(EnrollmentConstants.EXISTING_SADP_ENROLLMENT_ID))){
				enrollee.setExistingDentalEnrollmentId(Long.valueOf(member.get(EnrollmentConstants.EXISTING_SADP_ENROLLMENT_ID)));
			}
			
			if( !isNotNullAndEmpty(enrollment.getCatastrophicEligible()) && isNotNullAndEmpty(member.get("catastrophicEligible"))){
				enrollment.setCatastrophicEligible(member.get("catastrophicEligible"));
			}
			
			if(isNotNullAndEmpty(member.get("age"))) {
				enrollee.setAge(Integer.valueOf(member.get("age")));
			}
			if(isNotNullAndEmpty(member.get("quotingDate"))) {
				enrollee.setQuotingDate(DateUtil.StringToDate(member.get("quotingDate"), GhixConstants.REQUIRED_DATE_FORMAT));;
			}
			enrollee.setEnrollmentReason(enrollmentReason);
			enrollee.setEffectiveStartDate(enrollment.getBenefitEffectiveDate());
			enrollee.setEffectiveEndDate(enrollment.getBenefitEndDate());
			if(isNotNullAndEmpty(member.get(EnrollmentConstants.DOB))){
				Date dob=DateUtil.StringToDate(member.get(EnrollmentConstants.DOB), GhixConstants.REQUIRED_DATE_FORMAT);
				
				if((enrollmentReason==null || enrollmentReason.compareTo('S') != 0) && !DateUtils.isSameDay(enrollee.getEffectiveStartDate(), dob) && dob.after(enrollee.getEffectiveStartDate())){
					throw new GIException(EnrollmentConstants.INVALID_DOB);
				}
				enrollee.setBirthDate(dob);
			}
			enrollee.setExchgIndivIdentifier(member.get(EnrollmentConstants.MEMBER_ID));
			enrollee.setFirstName(member.get(EnrollmentConstants.FIRST_NAME));
			enrollee.setLastName(member.get(EnrollmentConstants.LAST_NAME));
			if(isNotNullAndEmpty(member.get(EnrollmentConstants.MIDDLE_NAME))){
				enrollee.setMiddleName(member.get(EnrollmentConstants.MIDDLE_NAME));
			}
			enrollee.setPrimaryPhoneNo(member.get(EnrollmentConstants.PRIMARY_PHONE));
			enrollee.setSecondaryPhoneNo(member.get(EnrollmentConstants.SECONDARY_PHONE));
			enrollee.setPreferredEmail(member.get(EnrollmentConstants.PREFERRED_EMAIL));
			enrollee.setPreferredSMS(member.get(EnrollmentConstants.PREFERRED_PHONE));
			enrollee.setTaxIdNumber(member.get(EnrollmentConstants.SSN));
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.SUFFIX))) {
				enrollee.setSuffix(member.get(EnrollmentConstants.SUFFIX));
			}
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.INDIVIDUAL_PREMIUM))) {
				enrollee.setTotIndvRespEffDate(enrollment.getBenefitEffectiveDate());
				enrollee.setTotalIndvResponsibilityAmt(Float.parseFloat(member.get(EnrollmentConstants.INDIVIDUAL_PREMIUM)));
			}
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.EMPLOYER_RESPONSIBILITY_AMT))) {
				enrollee.setTotEmpResponsibilityAmt(Float.parseFloat(member.get(EnrollmentConstants.EMPLOYER_RESPONSIBILITY_AMT)));
			}
			
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.RATING_AREA))) {
				enrollee.setRatingArea(member.get(EnrollmentConstants.RATING_AREA));
				enrollee.setRatingAreaEffDate(enrollee.getEffectiveStartDate());
			}
			
			/*if (isNotNullAndEmpty(member.get(EnrollmentConstants.EMPLOYEE_ID))) {
				enrollee.setEmployeeId(Integer.parseInt(member.get(EnrollmentConstants.EMPLOYEE_ID)));
			}*/
			if(isNotNullAndEmpty(member.get(EnrollmentConstants.MAINTENANCE_REASON_CODE))){
				enrollee.setMaintainenceReasonCode(member.get(EnrollmentConstants.MAINTENANCE_REASON_CODE));
			}

			enrollee.setEffectiveStartDate(enrollment.getBenefitEffectiveDate());

			// setting Resp_Person_Id_Code to QD for EDI outbound 834
			// enrollee.setRespPersonIdCode(EnrollmentConstants.RESPONSIBLE_PERSON_ID_CODE_QD);
			
			String enrollmentStatusCode = enrollment.getEnrollmentStatusLkp().getLookupValueCode();
			String enrollmentType=null;
			
			if(enrollment.getEnrollmentTypeLkp()!=null){
				enrollmentType=enrollment.getEnrollmentTypeLkp().getLookupValueCode();
			}			
			
			if(enrollmentStatusCode != null && enrollmentStatusCode.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)&& enrollmentType!=null && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
			}else if(enrollmentStatusCode != null && enrollmentStatusCode.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED)&& enrollmentType!=null && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
			}else if(enrollmentStatusCode != null && enrollmentStatusCode.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)&& enrollmentType!=null && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
			}else{
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_PENDING));
			}
			

			if(isNotNullAndEmpty(member.get(EnrollmentConstants.SUBSCRIBER_FLAG)) && member.get(EnrollmentConstants.SUBSCRIBER_FLAG).equalsIgnoreCase(EnrollmentConstants.YES)){
				enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.PERSON_TYPE_SUBSCRIBER));
			}
			else{
				enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
			}

			if (isNotNullAndEmpty(member.get(EnrollmentConstants.GENDER_CODE))) {
				enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GENDER, member.get(EnrollmentConstants.GENDER_CODE)));
			}
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.MARITAL_STATUS_CODE))) {
				enrollee.setMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.MARITAL_STATUS, member.get(EnrollmentConstants.MARITAL_STATUS_CODE)));
			}

			if (isNotNullAndEmpty(member.get(EnrollmentConstants.SPOKEN_LANGUAGE_CODE))) {
				enrollee.setLanguageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_SPOKEN, member.get(EnrollmentConstants.SPOKEN_LANGUAGE_CODE)));	
			}

			if (isNotNullAndEmpty(member.get(EnrollmentConstants.WRITTEN_LANGUAGE_CODE))) {
				enrollee.setLanguageWrittenLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_WRITTEN, member.get(EnrollmentConstants.WRITTEN_LANGUAGE_CODE)));	
			}

			if (isNotNullAndEmpty(member.get(EnrollmentConstants.TOBACCO))) {
				if(member.get(EnrollmentConstants.TOBACCO).toString().equalsIgnoreCase(EnrollmentConstants.Y)){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.T));
				}else if(member.get(EnrollmentConstants.TOBACCO).toString().equalsIgnoreCase(EnrollmentConstants.N)){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.N));
				}else if(member.get(EnrollmentConstants.TOBACCO).toString().equalsIgnoreCase(EnrollmentConstants.U)){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.U));
				}/*
				else{
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, member.get(EnrollmentConstants.TOBACCO)));
				}*/
				enrollee.setLastTobaccoUseDate(enrollee.getEffectiveStartDate());
			}else if(EnrollmentConfiguration.isCaCall()){
				enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.U));
				enrollee.setLastTobaccoUseDate(enrollee.getEffectiveStartDate());
			}
			//LAST_TOBACCO_USE_DATE field is added for FFM flow
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.LAST_TOBACCO_USE_DATE))) {
				enrollee.setLastTobaccoUseDate(DateUtil.StringToDate( member.get(EnrollmentConstants.LAST_TOBACCO_USE_DATE),GhixConstants.REQUIRED_DATE_FORMAT));	
			}

			if (isNotNullAndEmpty(member.get(EnrollmentConstants.HOUSEHOLD_CONTACT_RELATIONSHIP))) {
				enrollee.setRelationshipToHCPLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.RELATIONSHIP, member.get(EnrollmentConstants.HOUSEHOLD_CONTACT_RELATIONSHIP)));	
			}

			if (isNotNullAndEmpty(member.get(EnrollmentConstants.CITIZENSHIP_STATUS_CODE))) {
				LookupValue lv = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.CITIZENSHIP_STATUS, member.get(EnrollmentConstants.CITIZENSHIP_STATUS_CODE));
				if(lv !=null ){
					enrollee.setCitizenshipStatusLkp(lv);	
				}else{
					throw new GIException("No CitizenshipStatus lookup found for given code : "+member.get(EnrollmentConstants.CITIZENSHIP_STATUS_CODE));
				}
			}else{
				//throw new GIException(EnrollmentConstants.ERR_MSG_CITIZENSHIP_STATUS_NULL);
			}

			// Loop Through Newly Created Enrollee List and Search Custodial Parent
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.CUSTODIAL_PARENT_ID))) {
				boolean createNewCustInd = true;
				for (Enrollee enrollee2 : enrolleeList) {
					Enrollee tempCustodialParent = enrollee2.getCustodialParent();
					if(tempCustodialParent != null && member.get(EnrollmentConstants.CUSTODIAL_PARENT_ID).equalsIgnoreCase(tempCustodialParent.getExchgIndivIdentifier())){
						enrollee.setCustodialParent(tempCustodialParent);
						createNewCustInd=false;
						break;
					}
				}//End of for
				
				// Loop Through Enrollee List from Enrollment and Search Custodial Parent. This will happen only in case of special enrollment
				if(createNewCustInd && (enrollmentReason != null && (enrollmentReason.compareTo('S') == 0 || enrollmentReason.compareTo('s') == 0))){
					for (Enrollee enrollee2 : enrollment.getEnrolleesAndSubscriber()) {
						Enrollee tempCustodialParent = enrollee2.getCustodialParent();
						if(tempCustodialParent != null && member.get(EnrollmentConstants.CUSTODIAL_PARENT_ID).equalsIgnoreCase(tempCustodialParent.getExchgIndivIdentifier())){
							enrollee.setCustodialParent(tempCustodialParent);
							createNewCustInd=false;
							break;
						}
					}//End of for
				}
				if(createNewCustInd){
					Enrollee custodialParent= new Enrollee();
					custodialParent.setExchgIndivIdentifier(member.get(EnrollmentConstants.CUSTODIAL_PARENT_ID));
					custodialParent.setFirstName(member.get("custodialParentFirstName"));
					custodialParent.setMiddleName(member.get("custodialParentMiddleName"));
					custodialParent.setLastName(member.get("custodialParentLastName"));
					//					custodialParent.setSsn(member.get("custodialParentSsn"));
					custodialParent.setTaxIdNumber(member.get("custodialParentSsn"));
					custodialParent.setSuffix(member.get("custodialParentSuffix"));
					custodialParent.setPreferredEmail(member.get("custodialParentPreferredEmail"));
					custodialParent.setPreferredSMS(member.get("custodialParentPreferredPhone"));
					custodialParent.setPrimaryPhoneNo(member.get("custodialParentPrimaryPhone"));
					custodialParent.setSecondaryPhoneNo(member.get("custodialParentSecondaryPhone"));
					custodialParent.setEnrollment(enrollment);

					if(isNotNullAndEmpty(member.get("custodialParentHomeAddress1"))){
						Location custParentAddress = new Location();
						custParentAddress.setAddress1(member.get("custodialParentHomeAddress1"));
						custParentAddress.setAddress2(member.get("custodialParentHomeAddress2"));
						custParentAddress.setCity(member.get("custodialParentHomeCity"));
						custParentAddress.setState(member.get("custodialParentHomeState"));
						if(isNotNullAndEmpty(member.get(EnrollmentConstants.CUSTODIAL_PARENT_HOME_ZIP))){
							custParentAddress.setZip(member.get(EnrollmentConstants.CUSTODIAL_PARENT_HOME_ZIP).toString());	
						}

						custodialParent.setHomeAddressid(custParentAddress);
					}
					

					custodialParent.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT));
					
					custodialParent.setCreatedBy(loggedInUser);
					custodialParent.setUpdatedBy(loggedInUser);
					enrollee.setCustodialParent(custodialParent);
//					custIndicator=true;
				}
			}
			if(isNotNullAndEmpty(member.get("mailingAddress1"))){
				Location mailingAddress = new Location();
				mailingAddress.setAddress1(member.get("mailingAddress1"));
				mailingAddress.setAddress2(member.get("mailingAddress2"));
				mailingAddress.setCity(member.get("mailingCity"));
				mailingAddress.setState(member.get("mailingState"));
				
				if (isNotNullAndEmpty(member.get(EnrollmentConstants.MAILING_ZIP))) {
					mailingAddress.setZip(member.get(EnrollmentConstants.MAILING_ZIP));
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE);
				}
				
				enrollee.setMailingAddressId(mailingAddress);
			}
			

			if(isNotNullAndEmpty(member.get("homeAddress1"))){
				Location homeAddress = new Location();
				
				homeAddress.setAddress1(member.get("homeAddress1"));
				homeAddress.setAddress2(member.get("homeAddress2"));
				homeAddress.setCity(member.get("homeCity"));
				homeAddress.setState(member.get("homeState"));
				
				if(isNotNullAndEmpty(member.get("countyCode"))){
					homeAddress.setCountycode(member.get("countyCode"));
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_COUNTY_CODE_VALUE);
				}
				
				if (isNotNullAndEmpty(member.get("homeZip"))) {
					homeAddress.setZip(member.get("homeZip"));
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE);
				}
				
				enrollee.setHomeAddressid(homeAddress);
			}
			

			List<EnrolleeRace> enrolleeRace = new ArrayList<EnrolleeRace>();
			List<Map<String, String>> raceList = pldMemberData.getRaceInfo();
			if(raceList!=null && !raceList.isEmpty()){
				for (Map<String,String> raceMap: raceList){
					if(isNotNullAndEmpty(raceMap.get("raceEthnicityCode"))){
						EnrolleeRace tmpenrolleeRace = new EnrolleeRace();
						tmpenrolleeRace.setRaceEthnicityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_TYPE_RACE, raceMap.get("raceEthnicityCode")));
						tmpenrolleeRace.setRaceDescription(raceMap.get(EnrollmentConstants.DESCRIPTION));
						tmpenrolleeRace.setEnrollee(enrollee);
						enrolleeRace.add(tmpenrolleeRace);
					}
					
				}
				enrollee.setEnrolleeRace(enrolleeRace);	
			}
			//enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.EXTERNAL_MEMBER_ID))) {
				enrollee.setExternalIndivId(member.get(EnrollmentConstants.EXTERNAL_MEMBER_ID));
			}
			if(null == enrollee.getAge()) {
				enrollee.setAge(EnrollmentUtils.getEnrolleeAge(enrollee));
			}
			enrollee.setCreatedBy(loggedInUser);
			enrollee.setUpdatedBy(loggedInUser);
			enrolleeList.add(enrollee);

		}

		return enrolleeList;
	}
	
	private Date getYearEndDate(Date benefitEffectiveDate) {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(benefitEffectiveDate);
		int year = cal.get(Calendar.YEAR);
		Calendar endCal = new GregorianCalendar(year, EnrollmentConstants.ELEVEN, EnrollmentConstants.THIRTY_ONE);
		return endCal.getTime();
	}
	
	private EnrolleeRelationship findRelationFromList(List<EnrolleeRelationship> enrolleeRelationshipList,
			Enrollee sourceMember, Enrollee trgtMember) {
		EnrolleeRelationship enrolleeRelationShip = null;
		if (enrolleeRelationshipList != null && !enrolleeRelationshipList.isEmpty()) {
			for (EnrolleeRelationship enrlRelationShip : enrolleeRelationshipList) {
				if (enrlRelationShip.getSourceEnrollee() != null && enrlRelationShip.getTargetEnrollee() != null
						&& enrlRelationShip.getSourceEnrollee().getExchgIndivIdentifier().equalsIgnoreCase(sourceMember.getExchgIndivIdentifier())
						&& enrlRelationShip.getTargetEnrollee().getExchgIndivIdentifier().equalsIgnoreCase(trgtMember.getExchgIndivIdentifier())
						&& enrlRelationShip.getSourceEnrollee().getId() == sourceMember.getId()
						&& enrlRelationShip.getTargetEnrollee().getId() == trgtMember.getId()) {
					enrolleeRelationShip = enrlRelationShip;
					break;
				}
			}
		}
		return enrolleeRelationShip;
	}
	
	private List<Enrollee> findEnrolleesByMemberId(String memberId, List<Enrollee> enrolleeList) {
		List<Enrollee> enrollees = new ArrayList<>();
		for (Enrollee enrollee : enrolleeList) {
			String personTypeCode = "";
			if (enrollee.getExchgIndivIdentifier() != null && enrollee.getExchgIndivIdentifier().equals(memberId) && enrollee.getPersonTypeLkp() != null) {
				personTypeCode = enrollee.getPersonTypeLkp().getLookupValueCode();
				if (personTypeCode.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) || personTypeCode.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)) {
					enrollees.add(enrollee);
				}
			}
		}
		return enrollees;
	}
	
	private List<Map<String,String>> addMemberToExistingEnrollment(Enrollment existingEnrlmnt, List<PldMemberData> newMemberList , Character enrollmentReason, Date effectiveDate, String serc, boolean terminationFlag, List<Map<String,String>> newMembers, Date financialEffectiveDate, AccountUser loggedInUser) throws GIException
	{
		/*
		 * Calling Logged In User to log CreatedBy and LastUpdatedBy Enrollment event
		 * Jira Id: HIX-72322
		 */
		
		List<Enrollee> enrolleeList= getEnrolleeFromMemberInfoList(existingEnrlmnt,newMemberList, enrollmentReason, loggedInUser);
		
		List<EnrollmentEvent> enrollmentEventList= new ArrayList<EnrollmentEvent>();
		for (Enrollee tempenrollee : enrolleeList) {
			
			Map<String,String> newMemMap=new HashMap<String, String>();
			newMemMap.put(EnrollmentConstants.MEMBER_ID,tempenrollee.getExchgIndivIdentifier());
			newMemMap.put(EnrollmentConstants.ENROLLMENT_ID,existingEnrlmnt.getId().toString());
			newMemMap.put(EnrollmentConstants.MAINTENANCE_REASON_CODE, tempenrollee.getMaintainenceReasonCode());
			newMemMap.put(EnrollmentConstants.SERC, serc);
			newMemMap.put(EnrollmentConstants.TERM_FLAG, Boolean.toString(terminationFlag));
			newMembers.add(newMemMap);
			tempenrollee.setEffectiveStartDate(effectiveDate);
			
			if(tempenrollee.getBirthDate()!=null && !DateUtils.isSameDay(tempenrollee.getEffectiveStartDate(), tempenrollee.getBirthDate()) && tempenrollee.getBirthDate().after(tempenrollee.getEffectiveStartDate())){
				throw new GIException(EnrollmentConstants.INVALID_DOB);
			}
			if(isNotNullAndEmpty(tempenrollee.getRatingArea())){
				tempenrollee.setRatingAreaEffDate(effectiveDate);
			}
			if(isNotNullAndEmpty(tempenrollee.getTobaccoUsageLkp())){
				tempenrollee.setLastTobaccoUseDate(effectiveDate);
			}
			if(tempenrollee.getTotalIndvResponsibilityAmt()!=null){
				if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EnrollmentConstants.STATE_CODE_CA) && financialEffectiveDate!=null){
					tempenrollee.setTotIndvRespEffDate(financialEffectiveDate);
				}else{
					tempenrollee.setTotIndvRespEffDate(effectiveDate);
				}
			}
			if(terminationFlag){
				tempenrollee.setTerminationFlag(EnrollmentConstants.Y);
				if(existingEnrlmnt.getInsuranceTypeLkp()!=null && existingEnrlmnt.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
					tempenrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
				}else{
					tempenrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_PENDING));
				}
			}
			tempenrollee.setEnrollment(existingEnrlmnt);
			if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EFFECTUATE_NEWLY_ADDED_MEMBERS))){
				if(existingEnrlmnt.getEnrollmentStatusLkp() != null && EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(existingEnrlmnt.getEnrollmentStatusLkp().getLookupValueCode())){
					tempenrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
				}
			}
			
			//creating EnrollmentEvent
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent(); 
			enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.SPECIAL_ADD.toString());
			enrollmentEvent.setEnrollee(tempenrollee);
			enrollmentEvent.setEnrollment(existingEnrlmnt);
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_ADDITION));
			if(loggedInUser != null)
			{
				enrollmentEvent.setCreatedBy(loggedInUser);
				enrollmentEvent.setUpdatedBy(loggedInUser);
			}

				
			// This is used only in case of special enrollment. So it may not be present for Initial Enrollment
			if(isNotNullAndEmpty(tempenrollee.getMaintainenceReasonCode())){
				if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  tempenrollee.getMaintainenceReasonCode()))){
					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  tempenrollee.getMaintainenceReasonCode()));
				}else{
					throw new RuntimeException(EnrollmentConstants.ERR_MSG_REASON_CODE_VALUE);
				}
			}
			
			if(!isNotNullAndEmpty(serc)){
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  tempenrollee.getMaintainenceReasonCode()));
			}
			else if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, serc))){
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, serc));
			}else{
				throw new RuntimeException("Lookup value is null for serc : "+serc);
			}
			
			//added last event Id for Audit
			tempenrollee.setLastEventId(enrollmentEvent);
			enrollmentEventList.add(enrollmentEvent);
			
		}
		List<Enrollee> existingEnrolleeList = existingEnrlmnt.getEnrollees();
		if(isNotNullAndEmpty(existingEnrolleeList)){
			existingEnrolleeList.addAll(enrolleeList);
			existingEnrlmnt.setEnrollees(existingEnrolleeList);
		}
		else if(enrolleeList != null){
			existingEnrlmnt.setEnrollees(enrolleeList);
		}
		existingEnrlmnt.setEnrollmentEvents(enrollmentEventList);
		return newMembers;
	}
	
	private Map<Integer, String> getEnrolleeNameMap(List<Enrollment> enrollmentList) {
		Map<Integer, String> enrolleeNameMap = new HashMap<Integer, String>();
		List<Enrollee> enrolleelist = null;
		for (Enrollment enrollment : enrollmentList) {
			try {
				enrolleelist = enrolleeService.getEnrolledEnrolleesForEnrollmentID(enrollment.getId());

				StringBuilder combinedEnroleeName = new StringBuilder();
				if (enrolleelist != null && !enrolleelist.isEmpty()) {
					for (Enrollee enrollee : enrolleelist) {
						combinedEnroleeName.append(enrollee.getFirstName());
						if (enrollee.getMiddleName() != null) {
							combinedEnroleeName = combinedEnroleeName.append(" ").append(enrollee.getMiddleName());
						}
						if (enrollee.getLastName() != null) {
							combinedEnroleeName = combinedEnroleeName.append(" ").append(enrollee.getLastName());
						}
						if (enrollee.getSuffix() != null) {
							combinedEnroleeName = combinedEnroleeName.append(" ").append(enrollee.getSuffix());
						}

						combinedEnroleeName.append(", ");
					}

					enrolleeNameMap.put(enrollment.getId(),
							combinedEnroleeName.substring(0, (combinedEnroleeName.length() - 2)).toString());
				}
			} catch (GIException e) {
				LOGGER.error("Error occured in getting getEnrolleeNameMap", e);
			}
		}
		return enrolleeNameMap;
	}

	private EnrollmentResponse acknowledgeEnrollmentStatus(EnrollmentResponse enrollmentResponse,
			PldOrderResponse pldOrderResponse, List<Enrollment> enrollmentList) {
		Long app_id = null;
		String coverage_date = null;
		String coverage_end_date = null;
		Integer qhp_enrollment_id = null;
		Integer qdp_enrollment_id = null;
		Integer emp_id = null;
		String enrollmentMarketType = null;
		
		for (Enrollment enrollment : enrollmentList) {
			
			if (enrollment.getEnrollmentTypeLkp() != null && !isNotNullAndEmpty(enrollmentMarketType)) {
				enrollmentMarketType = enrollment.getEnrollmentTypeLkp().getLookupValueCode();
			}
			
			if (enrollmentMarketType != null && enrollmentMarketType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)) {
				app_id = enrollment.getEmployeeAppId();
				emp_id = enrollment.getEmployeeId();
				if (isNotNullAndEmpty(enrollment.getInsuranceTypeLkp()) 
						&& enrollment.getInsuranceTypeLkp().getLookupValueLabel().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH)) {
					qhp_enrollment_id = enrollment.getId();
					coverage_date = DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT);
					coverage_end_date = DateUtil.dateToString(enrollment.getBenefitEndDate(), GhixConstants.REQUIRED_DATE_FORMAT);
				} else if (isNotNullAndEmpty(enrollment.getInsuranceTypeLkp()) 
						&& enrollment.getInsuranceTypeLkp().getLookupValueLabel().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL)) {
					qdp_enrollment_id = enrollment.getId();
				}
			} else if (enrollmentMarketType != null && enrollmentMarketType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
				app_id = enrollment.getSsapApplicationid();
			}
		}

		if (isNotNullAndEmpty(enrollmentMarketType)
				&& enrollmentMarketType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)) {
			enrollmentResponse = updateEmployeeStatus(enrollmentResponse, app_id, coverage_date, coverage_end_date,
					qhp_enrollment_id, qdp_enrollment_id, emp_id, pldOrderResponse.getTerminationFlag(), pldOrderResponse.getDentalTerminationFlag());
		} else if (isNotNullAndEmpty(enrollmentMarketType)
				&& enrollmentMarketType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
			enrollmentResponse = updateIndividualStatus(enrollmentResponse, app_id, enrollmentList, false);
		} else {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
		return enrollmentResponse;
	}
	
	private EnrollmentResponse updateEmployeeStatus(EnrollmentResponse enrollmentResponse,Long emp_app_id ,String coverage_date ,String coverage_end_date ,Integer qhp_enrollment_id ,Integer qdp_enrollment_id ,Integer emp_id, String terminationFlag, String dentalTerminationFlag){
		if(isNotNullAndEmpty(emp_app_id) && isNotNullAndEmpty(emp_id)){
			XStream xstream = GhixUtils.getXStreamStaxObject();

			String shopResponseString = null;
			ShopResponse shopResponse = null;
			StringBuilder shopUrlStringBuilder = new StringBuilder();

			shopUrlStringBuilder.append(GhixEndPoints.ShopEndPoints.SET_EMPLOYEE_ENROLLED + "?");
			shopUrlStringBuilder.append("emp_app_id="); shopUrlStringBuilder.append(emp_app_id);
			shopUrlStringBuilder.append("&employeeId="); shopUrlStringBuilder.append(emp_id);

			if(isNotNullAndEmpty(coverage_date)){
				shopUrlStringBuilder.append("&coverage_date="+coverage_date);
			}

			if(isNotNullAndEmpty(coverage_end_date)){
				shopUrlStringBuilder.append("&coverage_end_date="+coverage_end_date);
			}

			if(isNotNullAndEmpty(qhp_enrollment_id)){
				shopUrlStringBuilder.append("&qhp_enrollment_id="+qhp_enrollment_id);
			}

			if(isNotNullAndEmpty(qdp_enrollment_id)){
				shopUrlStringBuilder.append("&qdp_enrollment_id="+qdp_enrollment_id);
			}

			if(isNotNullAndEmpty(terminationFlag) && EnrollmentConstants.Y.equalsIgnoreCase(terminationFlag)){
				shopUrlStringBuilder.append("&terminationFlag="+EnrollmentConstants.Y);
			}
			if(isNotNullAndEmpty(dentalTerminationFlag) && EnrollmentConstants.Y.equalsIgnoreCase(dentalTerminationFlag)){
				shopUrlStringBuilder.append("&dentalTerminationFlag="+EnrollmentConstants.Y);
			}
			try {
				shopResponseString = restTemplate.getForObject(shopUrlStringBuilder.toString(),String.class);
				shopResponse = (ShopResponse) xstream.fromXML(shopResponseString);
			} catch (Exception e) {
				//LOGGER.error("shopResponse for Status update Response: "+ emp_app_id);
				LOGGER.error("Exception = " + e.getMessage(),e);
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
			}

			if(isNotNullAndEmpty(shopResponse) && shopResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				LOGGER.info("shopResponse for Status update Response: "+ shopResponse.getStatus());
			}else{
				enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
				enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_UPDATE_EMPLOYEE_STATUS_FAILED);
			}
		}
		return enrollmentResponse;
	}
	
	public EnrollmentResponse updateIndividualStatus(EnrollmentResponse enrollmentResponse, Long app_id, List<Enrollment> enrollmentList, boolean isAppUpdateByReInstatementOrAdditon) {
		
		SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
		SsapApplicationResponse ssapApplicationResponse = new SsapApplicationResponse(); 
		ssapApplicationRequest.setSsapApplicationId(app_id);

		try {
			List<String> exchgIndividList =null;
			if(isNotNullAndEmpty(app_id)) {
				exchgIndividList = enrolleeRepository.getMemberIdBySsapId(app_id, EnrollmentUtils.getBeforeDayDate(new TSDate(), EnrollmentConstants.SIXTY));
			}else {
				throw new Exception("SSAP Application id is null while calling application/updateSsapApplicationById");
			}

			if (null != enrollmentList && !enrollmentList.isEmpty()) {
				Optional<Date> financialEffDate;
				Date effectiveDate;
				boolean hasHealth = enrollmentList.stream().anyMatch(e -> EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE
						.equalsIgnoreCase(e.getInsuranceTypeLkp().getLookupValueCode()));

				if (hasHealth) {
					financialEffDate = enrollmentList.stream()
							.filter(e -> EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE
									.equalsIgnoreCase(e.getInsuranceTypeLkp().getLookupValueCode()))
							.map(e -> e.getFinancialEffectiveDate()).filter(e -> null != e)
							.max((d1, d2) -> d1.compareTo(d2));
					effectiveDate = financialEffDate.orElse(enrollmentList.stream()
							.filter(e -> EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE
									.equalsIgnoreCase(e.getInsuranceTypeLkp().getLookupValueCode()))
							.map(e -> e.getBenefitEffectiveDate()).filter(e -> null != e)
							.max((d1, d2) -> d1.compareTo(d2)).orElse(null));
				} else {
					LOGGER.info("Fetch financial effective date for dental only scenario");
					financialEffDate = enrollmentList.stream().map(e -> e.getFinancialEffectiveDate())
							.filter(e -> null != e).max((d1, d2) -> d1.compareTo(d2));
					effectiveDate = financialEffDate
							.orElse(enrollmentList.stream().map(e -> e.getBenefitEffectiveDate()).filter(e -> null != e)
									.max((d1, d2) -> d1.compareTo(d2)).orElse(null));
				}

				LOGGER.info("Effective date {} found for ssap Id {} ", effectiveDate, app_id);

				ssapApplicationRequest.setEffectiveDate(
						null != effectiveDate ? DateUtil.dateToString(effectiveDate, GhixConstants.REQUIRED_DATE_FORMAT)
								: DateUtil.dateToString(enrollmentList.get(0).getBenefitEffectiveDate(),
										GhixConstants.REQUIRED_DATE_FORMAT));
				if (hasHealth && !exchgIndividList.isEmpty()) {
					ssapApplicationRequest.setExchgIndivIdList(exchgIndividList);
					ssapApplicationRequest.setApplicationStatus("EN");
				}
				for (Enrollment enrollment : enrollmentList) {
					if (EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE
							.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())) {
						ssapApplicationRequest.setApplicationDentalStatus("EN");
						break;
					}
				}
				ssapApplicationRequest.setEnrlRenewalFlag(enrollmentList.stream()
						.filter(e -> null != e.getRenewalFlag()).findFirst().orElse(new Enrollment()).getRenewalFlag());
			}
			
			LOGGER.info("Application dental status in enrollment -"+ ssapApplicationRequest.getApplicationDentalStatus());
			
			ssapApplicationRequest.setAppUpdateByReInstatement(isAppUpdateByReInstatementOrAdditon);
			
			if (!exchgIndividList.isEmpty() || ssapApplicationRequest.getApplicationDentalStatus() != null) {
				
				ssapApplicationResponse = enrollmentExternalRestUtil.updateSsapApplicationById(ssapApplicationRequest);

				if (ssapApplicationResponse != null && ssapApplicationResponse.getCount() > 0 && ssapApplicationResponse.getErrorCode().equalsIgnoreCase(Integer.valueOf(EnrollmentConstants.ERROR_CODE_200).toString())) {
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_200);
				} else if (ssapApplicationResponse != null && ssapApplicationResponse.getCount() == 0 && ssapApplicationResponse.getErrorCode().equalsIgnoreCase(Integer.valueOf(EnrollmentConstants.ERROR_CODE_204).toString())) {
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_204);
				} else {
					enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
					enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_UPDATE_INDIVIDUAL_STATUS_FAILED);
				}		
			}
			
		} catch (Exception e) {
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrCode(EnrollmentConstants.ERROR_CODE_201);
			enrollmentResponse.setErrMsg(EnrollmentConstants.ERR_MSG_UPDATE_INDIVIDUAL_STATUS_FAILED+e.getMessage()+e.getStackTrace());
		}
		return enrollmentResponse;
	}
	
	private void filterActiveAndTerminatedEnrollments(List<Enrollment> enrollmentList, List<Enrollment> activeEnrollmentList, List<Enrollment> terminatedEnrollmentList){
		if(enrollmentList != null && !enrollmentList.isEmpty()){
			for(Enrollment enrollment : enrollmentList){
				if(enrollment.getEnrollmentStatusLkp() != null && (
						!enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) && 
						!enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) &&
						!enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED))){
					activeEnrollmentList.add(enrollment);
				}else if(enrollment.getEnrollmentStatusLkp() != null && enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
					terminatedEnrollmentList.add(enrollment);
				}
			}
		}
	}
	
	/**
	 * Checks enrollment status. Changes Enrollment status to cancel/terminated if all enrollments are cancelled.
	 * 
	 * @author Aditya-S
	 * @since 21-03-2014
	 * 
	 * @param enrollmentList
	 */
	public void checkEnrollmentStatusSpecialEnrollment(List<Enrollment> enrollmentList){
		for(Enrollment enrollment : enrollmentList){
			List<Enrollee> enrolleeList = enrollment.getEnrollees();
			
			int totalEnrolleeCount = 0;
			Enrollee subscriber = checkForSubscriber(enrolleeList);
			int cancellledEnrolleeCount = 0;
			int terminatedEnrolleeCount = 0;
			
			for (Enrollee enrollee : enrolleeList) {
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) || enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)){
					totalEnrolleeCount ++;
					if (enrollee.getEnrolleeLkpValue() != null) {
						if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
							cancellledEnrolleeCount++;
						} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
							terminatedEnrolleeCount++;
						}
					}
				}
			}
			
			if (totalEnrolleeCount == cancellledEnrolleeCount) {
				enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
				enrollment.setAbortTimestamp(new TSDate());
				if(subscriber != null){
					enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
				}
			}else if (totalEnrolleeCount == (cancellledEnrolleeCount + terminatedEnrolleeCount)) {
				enrollment.setAbortTimestamp(new TSDate());
				enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
				if(subscriber != null && subscriber.getEffectiveEndDate() != null){
					 enrollment.setBenefitEndDate(subscriber.getEffectiveEndDate());
				}
			}
		}
	}
	
	private Enrollee checkForSubscriber(List<Enrollee> enrolleeList) {
		if(enrolleeList !=null && !enrolleeList.isEmpty()){
			for(Enrollee enrollee:enrolleeList){
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
					return enrollee;
				}
			}
		}
		return null;	
	}
	
	private Date findAmountEffectiveDate(List<String> disEnrolledMember,Date effectiveDate, Enrollment existingEnrollment, String oldStatus){
		Date amountEffectiveDate=null;
		if(effectiveDate!=null){
			amountEffectiveDate=effectiveDate;
		}else{
			if(disEnrolledMember!=null && !disEnrolledMember.isEmpty() && existingEnrollment!=null && existingEnrollment.getEnrollmentStatusLkp()!=null){
				String enrollmentStatus=existingEnrollment.getEnrollmentStatusLkp().getLookupValueCode();
				if(!(!EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(oldStatus) && !EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(oldStatus) &&
						(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollmentStatus) || EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollmentStatus))
						)){
					List<Enrollee> enrollees= existingEnrollment.getEnrollees();
					for(String memberID:disEnrolledMember){
						Enrollee disEnrolledEnrollee= findEnrolleeByMemberId(memberID, enrollees);
						if(disEnrolledEnrollee!=null && disEnrolledEnrollee.getEffectiveEndDate()!=null && disEnrolledEnrollee.getEnrolleeLkpValue()!=null && disEnrolledEnrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) ){
							Date effEndDate=disEnrolledEnrollee.getEffectiveEndDate();
							if(effEndDate!=null){
								amountEffectiveDate= EnrollmentUtils.getNextDayDate(effEndDate);
								break;
							}
						
						}
					}
				}
			}
		}
		
		return amountEffectiveDate;
	}
	
	private Enrollee findEnrolleeByMemberId(String memberId, List<Enrollee> enrolleeList) {
		for (Enrollee enrollee : enrolleeList) { 
			String personTypeCode="";
			if (enrollee.getExchgIndivIdentifier() != null && enrollee.getExchgIndivIdentifier().equals(memberId) && enrollee.getPersonTypeLkp()!=null){
				personTypeCode=enrollee.getPersonTypeLkp().getLookupValueCode();
				if(personTypeCode.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)|| personTypeCode.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE)){
					return enrollee;
				}
			}
		}
		return null;
	}
	
	private void updateTobaccoDetails(Map<String, String> member, Enrollee enrollee, Date financialEffectiveDate, Date sepEffDate){
		Date tobaccoEffectiveDate=null;
		if(EnrollmentConfiguration.isCaCall()){
			tobaccoEffectiveDate=financialEffectiveDate;
		}else{
			tobaccoEffectiveDate=sepEffDate;
		}
		
		if(tobaccoEffectiveDate!=null &&( tobaccoEffectiveDate.after(enrollee.getEffectiveStartDate())|| DateUtils.isSameDay(enrollee.getEffectiveStartDate(), tobaccoEffectiveDate))&&
				(tobaccoEffectiveDate.before(enrollee.getEffectiveEndDate()) || DateUtils.isSameDay(enrollee.getEffectiveEndDate(), tobaccoEffectiveDate))
				) {
			if (isNotNullAndEmpty(member.get(EnrollmentConstants.TOBACCO))) {
				String enrolleeTabaccoStr = enrollee.getTobaccoUsageLkp()!=null ? enrollee.getTobaccoUsageLkp().getLookupValueCode():null;
				boolean update=true;
				if(null!=enrolleeTabaccoStr) {
					if(enrolleeTabaccoStr.equalsIgnoreCase(EnrollmentConstants.T)) {
						enrolleeTabaccoStr=EnrollmentConstants.Y;
					}else if(enrolleeTabaccoStr.equalsIgnoreCase(EnrollmentConstants.N)) {
						enrolleeTabaccoStr=EnrollmentConstants.N;
					}else if(enrolleeTabaccoStr.equalsIgnoreCase(EnrollmentConstants.U)) {
						enrolleeTabaccoStr=EnrollmentConstants.U;
					}

					if(member.get(EnrollmentConstants.TOBACCO).equalsIgnoreCase(enrolleeTabaccoStr)) {
						update=false;
					}
				}
				if(update) {
					if(member.get(EnrollmentConstants.TOBACCO).toString().equalsIgnoreCase(EnrollmentConstants.Y)){
						enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.T));
					}else if(member.get(EnrollmentConstants.TOBACCO).toString().equalsIgnoreCase(EnrollmentConstants.N)){
						enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.N));
					}else if(member.get(EnrollmentConstants.TOBACCO).toString().equalsIgnoreCase(EnrollmentConstants.U)){
						enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.U));
					}
					
					enrollee.setLastTobaccoUseDate(tobaccoEffectiveDate);
					
				}
			}else if(EnrollmentConfiguration.isCaCall()){
				
				enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.U));
				enrollee.setLastTobaccoUseDate(tobaccoEffectiveDate);
			}
		}
	}
	
private void updateEnrolleeCovDate(List<Enrollee> enrollees, Date financialEffectiveDate,  boolean overrideCoverageStartDate) throws GIException{
	
	if(overrideCoverageStartDate && enrollees!=null && enrollees.size()>0 && financialEffectiveDate!=null){
		List<Enrollee> filteredEnrollees=new ArrayList<Enrollee>();
		for(Enrollee enrollee: enrollees){
			if(financialEffectiveDate.before(enrollee.getEffectiveEndDate())){
				filteredEnrollees.add(enrollee);
			}
		}
			
		if(filteredEnrollees.size()>0){
			//choose the one with lowest coverage start date
			//sort the enrollees in ascending Order of coverage startdate
			Collections.sort(filteredEnrollees, new Comparator<Enrollee>() {
				@Override
				public int compare(Enrollee enrollee1, Enrollee enrollee2) {
					return (((Date)enrollee1.getEffectiveStartDate()).compareTo(((Date)enrollee2.getEffectiveStartDate())));
				}
			});
			
			filteredEnrollees.get(0).setEffectiveStartDate(financialEffectiveDate);
			filteredEnrollees.get(0).setAgeUpdated(true);
		}else{
			throw new GIException("The financial effective date provided is beyond the existing coverage for member id: "+ enrollees.get(0).getExchgIndivIdentifier());
		}
	}
}
	
	private void updateEnrolleePremiumDetails(Map<String, String> member, Enrollee enrollee, Date financialEffectiveDate, Date sepEffDate, Date enrlBenefitEffDate, Date enrlBenefitEndDate) throws GIException{
		Float individualPremium=null;
		Date indivPremEffDate=null;
		boolean useSameEffectiveDate = Boolean.parseBoolean(DynamicPropertiesUtil
				.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.USE_SAME_EFFECTIVE_DATE));
		boolean allowMidMonthEffectiveDate = Boolean.parseBoolean(DynamicPropertiesUtil
				.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ALLOW_MID_MONTH_EFFECTIVE_DATE));
		
		
		if(EnrollmentConfiguration.isCaCall() && financialEffectiveDate != null){
			indivPremEffDate=financialEffectiveDate;
		}else{
			indivPremEffDate=sepEffDate;
		}
		
		if (indivPremEffDate != null && !allowMidMonthEffectiveDate) {
			if (DateUtil.getDateOfMonth(indivPremEffDate) != 1) {
				if (enrlBenefitEndDate != null && indivPremEffDate.getMonth() == enrlBenefitEndDate.getMonth()) {
					indivPremEffDate = EnrollmentUtils.getMonthStartDate(enrlBenefitEndDate);
				} else {
					indivPremEffDate = EnrollmentUtils.getNextMonthStartDate(indivPremEffDate);
				}
			}
		}		
		
		if (isNotNullAndEmpty(member.get(EnrollmentConstants.INDIVIDUAL_PREMIUM))) {
			individualPremium=Float.parseFloat(member.get(EnrollmentConstants.INDIVIDUAL_PREMIUM));
		}else if(EnrollmentConfiguration.isCaCall()){
			throw new GIException("The individual premium can not be null for enrollee:  "+enrollee.getId()+ " member id: "+ member.get(EnrollmentConstants.MEMBER_ID));
		}
		
		if(indivPremEffDate!=null &&( indivPremEffDate.after(enrollee.getEffectiveStartDate())|| DateUtils.isSameDay(enrollee.getEffectiveStartDate(), indivPremEffDate))&&
				(indivPremEffDate.before(enrollee.getEffectiveEndDate()) || DateUtils.isSameDay(enrollee.getEffectiveEndDate(), indivPremEffDate))
				) {
			

			if (isNotNullAndEmpty(member.get(EnrollmentConstants.EMPLOYER_RESPONSIBILITY_AMT))) {
				    enrollee.setTotEmpResponsibilityAmt(Float.parseFloat(member.get(EnrollmentConstants.EMPLOYER_RESPONSIBILITY_AMT)));
			}
			
			if(individualPremium!=null && individualPremium>0.0f){
				if(EnrollmentConfiguration.isCaCall()){
					if(financialEffectiveDate != null && enrollee.getEffectiveEndDate()!=null && financialEffectiveDate.after(enrollee.getEffectiveEndDate()) && !DateUtils.isSameDay(financialEffectiveDate,enrollee.getEffectiveStartDate())){
						throw new GIException("The financial effective date provided is beyond the existing coverage for enrollee:  "+enrollee.getId()+ " member id: "+ member.get(EnrollmentConstants.MEMBER_ID));
					}
				}
				if(!EnrollmentUtils.isFloatEqual(individualPremium, enrollee.getTotalIndvResponsibilityAmt()) && indivPremEffDate != null || useSameEffectiveDate){
					enrollee.setTotIndvRespEffDate(indivPremEffDate);
				}
			}
			if(individualPremium !=null){
				 enrollee.setTotalIndvResponsibilityAmt(individualPremium);
			}
		}
	}
	
	private void updateEnrolleeRatingAreaDetails(Map<String, String> member, Enrollee enrollee, Date financialEffectiveDate, Date sepEffDate, boolean overrideCoverageStartDate){
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		Date ratingAreEffDate=null;
		boolean isRatingAreChanged=false;
		
		if (isNotNullAndEmpty(member.get(EnrollmentConstants.RATING_AREA))) {
			if(!member.get(EnrollmentConstants.RATING_AREA).equalsIgnoreCase(enrollee.getRatingArea())){
				isRatingAreChanged=true;
				ratingAreEffDate=sepEffDate;
			}else if(enrollee.getRatingAreaEffDate()== null){
				ratingAreEffDate=enrollee.getEffectiveStartDate();
			}
		} 
			
		if(overrideCoverageStartDate && EnrollmentConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)) {
			ratingAreEffDate=financialEffectiveDate;
			if(!member.get(EnrollmentConstants.RATING_AREA).equalsIgnoreCase(enrollee.getRatingArea())){
				isRatingAreChanged=true;
			}
		}
		
		if (ratingAreEffDate != null
				&& (ratingAreEffDate.after(enrollee.getEffectiveStartDate())
						|| DateUtils.isSameDay(enrollee.getEffectiveStartDate(), ratingAreEffDate))
				&& (ratingAreEffDate.before(enrollee.getEffectiveEndDate())
						|| DateUtils.isSameDay(enrollee.getEffectiveEndDate(), ratingAreEffDate))) {
			enrollee.setRatingAreaEffDate(ratingAreEffDate);
			if (isRatingAreChanged) {
				enrollee.getEnrollment().setIsRatingAreaChanged(true);
				enrollee.setRatingArea(member.get(EnrollmentConstants.RATING_AREA));
			}
		}
	}
	
	public void updateExistingEnrollee(Enrollment enrollment, List<PldMemberData> pldMemberList, String serc, Date financialEffectiveDate, boolean overrideCoverageStartDate, AccountUser loggedInUser, Date sepEffDate) throws GIException{
		if(enrollment == null || pldMemberList == null || pldMemberList.isEmpty()){
			return;
		}
		
		for(PldMemberData pldMemberData: pldMemberList){
			Map<String, String> member = pldMemberData.getMemberInfo();
				// Find Member from Existing enrollment based on ExchangeIndividualIdentifier/MemberID
			List<Enrollee> enrollees  = findEnrolleesByMemberId (member.get(EnrollmentConstants.MEMBER_ID),enrollment.getEnrollees());
			
			if(enrollees!=null && enrollees.size()>0){
				
				updateEnrolleeCovDate( enrollees, financialEffectiveDate,  overrideCoverageStartDate);
				
				for(Enrollee enrollee: enrollees){
					
					
					//update Tobacco Info
					
					updateTobaccoDetails( member,  enrollee,  financialEffectiveDate,  sepEffDate);
					
					updateEnrolleePremiumDetails( member,  enrollee,  financialEffectiveDate,  sepEffDate, enrollment.getBenefitEffectiveDate(), enrollment.getBenefitEndDate());
					
					updateEnrolleeRatingAreaDetails(member,  enrollee,  financialEffectiveDate,  sepEffDate, overrideCoverageStartDate);
					
					if( !isNotNullAndEmpty(enrollment.getCatastrophicEligible()) && isNotNullAndEmpty(member.get("catastrophicEligible"))){
						enrollment.setCatastrophicEligible(member.get("catastrophicEligible"));
					}
					
					enrollee.setUpdatedOn(new TSDate());
					enrollee.setUpdatedBy(loggedInUser);
					enrollee.setExchgIndivIdentifier(member.get(EnrollmentConstants.MEMBER_ID));
					enrollee.setSuffix(member.get(EnrollmentConstants.SUFFIX));
					enrollee.setFirstName(member.get(EnrollmentConstants.FIRST_NAME));
					enrollee.setLastName(member.get(EnrollmentConstants.LAST_NAME));
					enrollee.setMiddleName(member.get(EnrollmentConstants.MIDDLE_NAME));
					enrollee.setPrimaryPhoneNo(member.get(EnrollmentConstants.PRIMARY_PHONE));
					enrollee.setSecondaryPhoneNo(member.get(EnrollmentConstants.SECONDARY_PHONE));
					enrollee.setPreferredEmail(member.get(EnrollmentConstants.PREFERRED_EMAIL));
					enrollee.setPreferredSMS(member.get(EnrollmentConstants.PREFERRED_PHONE));
					enrollee.setPreferredSMS(member.get(EnrollmentConstants.PREFERRED_PHONE));
					
					if (isNotNullAndEmpty(member.get(EnrollmentConstants.CITIZENSHIP_STATUS_CODE))) {
						LookupValue lv = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.CITIZENSHIP_STATUS, member.get(EnrollmentConstants.CITIZENSHIP_STATUS_CODE));
						if(lv !=null ){
							enrollee.setCitizenshipStatusLkp(lv);	
						}else{
							throw new GIException("No CitizenshipStatus lookup found for given code : "+member.get(EnrollmentConstants.CITIZENSHIP_STATUS_CODE));
						}
					}
					
					if(isNotNullAndEmpty(member.get("age"))) {
						enrollee.setAge(Integer.valueOf(member.get("age")));
					}
					if(isNotNullAndEmpty(member.get("quotingDate"))) {
						enrollee.setQuotingDate(DateUtil.StringToDate(member.get("quotingDate"), GhixConstants.REQUIRED_DATE_FORMAT));;
					}
					boolean isAgeChange=enrollee.isAgeUpdated();
					
					
					if(isNotNullAndEmpty(member.get(EnrollmentConstants.DOB))){
						Date dob=DateUtil.StringToDate(member.get(EnrollmentConstants.DOB), GhixConstants.REQUIRED_DATE_FORMAT);
						
						if(!DateUtils.isSameDay(enrollee.getEffectiveStartDate(), dob) && dob.after(enrollee.getEffectiveStartDate())){
							throw new GIException(EnrollmentConstants.INVALID_DOB);
						}
						if(DateUtils.isSameDay(dob, enrollee.getBirthDate())){
							isAgeChange=true;
						}
						enrollee.setBirthDate(dob);
					}
					
					if (isNotNullAndEmpty(member.get(EnrollmentConstants.EXTERNAL_MEMBER_ID))) {
						enrollee.setExternalIndivId(member.get(EnrollmentConstants.EXTERNAL_MEMBER_ID));
					}
					
					enrollee.setLanguageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_SPOKEN, member.get(EnrollmentConstants.SPOKEN_LANGUAGE_CODE)));	
					enrollee.setLanguageWrittenLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_WRITTEN, member.get(EnrollmentConstants.WRITTEN_LANGUAGE_CODE)));	
					
					/*----- These cannot change for enrollee -------*/
					
					//		enrollee.setExchgIndivIdentifier(member.get(EnrollmentConstants.MEMBER_ID));
					
					/*----- These cannot change for enrollee -------*/
					enrollee.setTaxIdNumber(member.get(EnrollmentConstants.SSN));
					enrollee.setMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.MARITAL_STATUS, member.get(EnrollmentConstants.MARITAL_STATUS_CODE)));
					enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GENDER, member.get(EnrollmentConstants.GENDER_CODE)));
					
					if(isNotNullAndEmpty(member.get(EnrollmentConstants.SUBSCRIBER_FLAG)) && member.get(EnrollmentConstants.SUBSCRIBER_FLAG).equalsIgnoreCase(EnrollmentConstants.YES)){
						enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.PERSON_TYPE_SUBSCRIBER));
						if(isAgeChange && EnrollmentConfiguration.isCaCall()){
							int subscriberAge = EnrollmentUtils.getAge(enrollee.getBirthDate(),  enrollee.getEffectiveStartDate());
							if(subscriberAge >= EnrollmentConstants.AGE_18){
								StringBuilder sponsorName = new StringBuilder();
								
								if(isNotNullAndEmpty(enrollee.getFirstName())){
									sponsorName.append(enrollee.getFirstName());
									sponsorName.append(" ");
								}
								if(isNotNullAndEmpty(enrollee.getMiddleName())){
									sponsorName.append(enrollee.getMiddleName());
									sponsorName.append(" ");	
								}
								if(isNotNullAndEmpty(enrollee.getLastName())){
									sponsorName.append(enrollee.getLastName());
									sponsorName.append(" ");
								}
								if(isNotNullAndEmpty(enrollee.getSuffix())){
									sponsorName.append(enrollee.getSuffix());	
								}
					
								if(sponsorName.length() > EnrollmentConstants.SPONSORNAME_MAX_LENGTH ){
									sponsorName.delete(EnrollmentConstants.SPONSORNAME_MAX_LENGTH,sponsorName.length());
								}
								enrollment.setSponsorTaxIdNumber(enrollee.getTaxIdNumber());
								enrollment.setSponsorName(sponsorName.toString().trim());
								
							}
						}
						
					}else{
						enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
					}
					
					if (isNotNullAndEmpty(member.get(EnrollmentConstants.HOUSEHOLD_CONTACT_RELATIONSHIP))) {
						enrollee.setRelationshipToHCPLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.RELATIONSHIP, member.get(EnrollmentConstants.HOUSEHOLD_CONTACT_RELATIONSHIP)));	
					}
					
					Location mailingAddress = enrollee.getMailingAddressId();
					if(isNotNullAndEmpty(member.get("mailingAddress1"))){
						if(mailingAddress == null){
							mailingAddress = new Location();
							enrollee.setMailingAddressId(mailingAddress);
						}
						mailingAddress.setAddress1(member.get("mailingAddress1"));
						mailingAddress.setAddress2(member.get("mailingAddress2"));
						mailingAddress.setCity(member.get("mailingCity"));
						mailingAddress.setState(member.get("mailingState"));
						if (isNotNullAndEmpty(member.get(EnrollmentConstants.MAILING_ZIP))) {
							mailingAddress.setZip(member.get(EnrollmentConstants.MAILING_ZIP));
						}else{
							throw new GIException(EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE);
						}
					}
					
					
					Location homeAddress = enrollee.getHomeAddressid();
					if(isNotNullAndEmpty(member.get("homeAddress1"))){
						if(homeAddress == null){
							homeAddress = new Location();
							enrollee.setHomeAddressid(homeAddress);
						}
						homeAddress.setAddress1(member.get("homeAddress1"));
						homeAddress.setAddress2(member.get("homeAddress2"));
						homeAddress.setCity(member.get("homeCity"));
						homeAddress.setState(member.get("homeState"));
						if(isNotNullAndEmpty(member.get("countyCode"))){
							homeAddress.setCountycode(member.get("countyCode"));
						}else{
							throw new GIException(EnrollmentConstants.ERR_MSG_COUNTY_CODE_VALUE);
						}
						
						if (isNotNullAndEmpty(member.get("homeZip"))) {
							homeAddress.setZip(member.get("homeZip"));
						}else{
							throw new GIException(EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE);
						}
					}
					
					// Setting Custodial Parent
					if (isNotNullAndEmpty(member.get(EnrollmentConstants.CUSTODIAL_PARENT_ID))) {
						Enrollee custodialParent = null;
						List<Enrollee> custodials=enrolleeRepository.getEnrolleesByPersonTypeForEnrollment(enrollment.getId(), EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT);
						if(!custodials.isEmpty()){
							for(Enrollee enrlCust: custodials){
								if(enrlCust.getExchgIndivIdentifier().equals(member.get(EnrollmentConstants.CUSTODIAL_PARENT_ID))){
									custodialParent=enrlCust;
									break;
								}
							}
						}
						
						if(custodialParent==null){
							custodialParent= new Enrollee();
							custodialParent.setCreatedBy(loggedInUser);
							custodialParent.setUpdatedBy(loggedInUser);
							custodialParent.setCreatedOn(new TSDate());
						}
						enrollee.setCustodialParent(custodialParent);
						custodialParent.setExchgIndivIdentifier(member.get(EnrollmentConstants.CUSTODIAL_PARENT_ID));
						custodialParent.setFirstName(member.get("custodialParentFirstName"));
						custodialParent.setMiddleName(member.get("custodialParentMiddleName"));
						custodialParent.setLastName(member.get("custodialParentLastName"));
						//					custodialParent.setSsn(member.get("custodialParentSsn"));
						custodialParent.setTaxIdNumber(member.get("custodialParentSsn"));
						custodialParent.setSuffix(member.get("custodialParentSuffix"));
						custodialParent.setPreferredEmail(member.get("custodialParentPreferredEmail"));
						custodialParent.setPreferredSMS(member.get("custodialParentPreferredPhone"));
						custodialParent.setPrimaryPhoneNo(member.get("custodialParentPrimaryPhone"));
						custodialParent.setSecondaryPhoneNo(member.get("custodialParentSecondaryPhone"));
					
						Location custParentAddress = custodialParent.getHomeAddressid();
						if(custParentAddress == null){
							custParentAddress = new Location();
							custodialParent.setHomeAddressid(custParentAddress);
						}
						custParentAddress.setAddress1(member.get("custodialParentHomeAddress1"));
						custParentAddress.setAddress2(member.get("custodialParentHomeAddress2"));
						custParentAddress.setCity(member.get("custodialParentHomeCity"));
						custParentAddress.setState(member.get("custodialParentHomeState"));
						if(isNotNullAndEmpty(member.get(EnrollmentConstants.CUSTODIAL_PARENT_HOME_ZIP))){
							custParentAddress.setZip(member.get(EnrollmentConstants.CUSTODIAL_PARENT_HOME_ZIP).toString());	
						}
						custodialParent.setEnrollment(enrollment);
						custodialParent.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT));
					}
					
					List<EnrolleeRace> oldEnrolleeRace = enrollee.getEnrolleeRace();
					List<EnrolleeRace> enrolleeRace = new ArrayList<EnrolleeRace>();
					List<Map<String, String>> raceList = pldMemberData.getRaceInfo();
					if(raceList!=null && !raceList.isEmpty()){
						for (Map<String,String> raceMap: raceList){
							if (isNotNullAndEmpty(raceMap.get("raceEthnicityCode"))){
								EnrolleeRace tmpenrolleeRace = new EnrolleeRace();
								tmpenrolleeRace.setRaceEthnicityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_TYPE_RACE, raceMap.get("raceEthnicityCode")));
								tmpenrolleeRace.setRaceDescription(raceMap.get(EnrollmentConstants.DESCRIPTION));
								//tmpenrolleeRace.setEnrollee(enrollee);
								enrolleeRace.add(tmpenrolleeRace);
							}
						}
						
						if(!EnrollmentUtils.compareEnrolleerace(oldEnrolleeRace, enrolleeRace)){
							enrolleeRaceService.deleteEnrolleeRace(oldEnrolleeRace);
							if(enrolleeRace!=null && !enrolleeRace.isEmpty()){
								for(EnrolleeRace race : enrolleeRace){
									race.setEnrollee(enrollee);
								}
							}
							enrollee.setEnrolleeRace(enrolleeRace);
						}else{
							enrollee.setEnrolleeRace(oldEnrolleeRace);
						}
					}
					
					// Setting enrolleeEvent
					List<EnrollmentEvent> enrolleeEventList= new ArrayList<EnrollmentEvent>();
					EnrollmentEvent enrollmentEvent = new EnrollmentEvent(); 
					enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.SPECIAL_UPDATE.toString());
					enrollmentEvent.setEnrollee(enrollee);
					enrollmentEvent.setEnrollment(enrollment);
					/*
					 * Adding LoggedInUser for Jira ID: HIX-72322
					 */
					if(loggedInUser != null)
					{
						enrollmentEvent.setCreatedBy(loggedInUser);
						enrollmentEvent.setUpdatedBy(loggedInUser);
					}
					
					
					// As per HIX-25888 the implementation is correct. We need to update all members with the same maintenance reason code.
					//add null check
					if(isNotNullAndEmpty(member.get(EnrollmentConstants.MAINTENANCE_REASON_CODE))){
							if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  member.get(EnrollmentConstants.MAINTENANCE_REASON_CODE)))){
								enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  member.get(EnrollmentConstants.MAINTENANCE_REASON_CODE)));
							}else{
								throw new RuntimeException("Lookup value is null");
							}
					}else{
						throw new RuntimeException("Maintanance Reason code is null or empty");
					}
					enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CHANGE));
					
					// Commented for HIX-25858. Where we dont want to send events for Update happened to enrollee during special enrollment
					// we are concerned only new addition/deletion.
					// for finance api searchEnrollmentsForCurrentMonth.
					
					/**
					 * HIX-39078
					 * This is uncommented. In order to FIX Shop Enrollment Log issue. The Query for Finance {@see IEnrolleeRepository#getSpecialEnrolleeByEnrollmentID(Date currentDate, Date lastInvoiceDate, Integer enrollmentID)}
					 * changed to return only newly added or deleted member during special erollment
					 */
					
					//add null check
					if(!isNotNullAndEmpty(serc)){
						enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  member.get(EnrollmentConstants.MAINTENANCE_REASON_CODE)));
					}
					else if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, serc))){
						enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, serc));
					}else{
						throw new RuntimeException("Lookup value is null for serc : "+serc);
					}
					//					enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  member.get(EnrollmentConstants.MAINTENANCE_REASON_CODE)));
						
						enrollee.setLastEventId(enrollmentEvent);
						enrolleeEventList.add(enrollmentEvent);
						enrollee.setEnrollmentEvents(enrolleeEventList);
						if(null == enrollee.getAge()) {
							enrollee.setAge(EnrollmentUtils.getEnrolleeAge(enrollee));
						}
				}//enrollee loop end
				
			}//enrollees Size condition end
		} 
	}
	

	private void updateSHOPAmountAndEffectiveDatesForSpecial(PldOrderResponse pldOrderResponse, Enrollment enrollment,
			PldPlanData pdlPlan, Date amountEffectiveDate, AccountUser loggedInUser, Date financialEffectiveDate,
			Map<String, String> plan, Date sepEffDate, PlanListResponse planListResponse) throws GIException {
		
		if(isNotNullAndEmpty(plan.get(EnrollmentConstants.MAX_APTC))){
        	enrollment.setMaxAPTCAmt(Float.parseFloat(plan.get(EnrollmentConstants.MAX_APTC)));
        }

		// if (isNotNullAndEmpty(plan.get(EnrollmentConstants.APTC))) {
		if (!EnrollmentUtils.isFloatEqual(plan.get(EnrollmentConstants.APTC), enrollment.getAptcAmt())) {
			if (amountEffectiveDate != null) {
				// enrollment.setAptcEffDate(EnrollmentUtils.getNextMonthStartDate(amountEffectiveDate));
				enrollment.setAptcEffDate(amountEffectiveDate);// HIX-58608
			} else {
				// enrollment.setAptcEffDate(EnrollmentUtils.getNextMonthStartDate(new
				// Date()));
				// enrollment.setAptcEffDate(new TSDate());//HIX-58608

				if (!new TSDate().before(enrollment.getBenefitEffectiveDate())
						&& !DateUtils.isSameDay(new TSDate(), enrollment.getBenefitEffectiveDate())) {
					Date aptcEffectiveDate = new TSDate();
					if (DateUtil.getDateOfMonth(aptcEffectiveDate) <= EnrollmentConstants.FIFTEEN) {
						aptcEffectiveDate = EnrollmentUtils.getNextMonthStartDate(aptcEffectiveDate);

					} else {
						aptcEffectiveDate = EnrollmentUtils.getNextToNextMonthStartDate(aptcEffectiveDate);
					}

					if (aptcEffectiveDate.after(enrollment.getBenefitEndDate())) {
						enrollment.setAptcEffDate(EnrollmentUtils.getMonthStartDate(enrollment.getBenefitEndDate()));
						enrollment.setNetPremEffDate(enrollment.getAptcEffDate());
					} else {
						enrollment.setAptcEffDate(aptcEffectiveDate);
						enrollment.setNetPremEffDate(enrollment.getAptcEffDate());
					}
				} else {
					enrollment.setAptcEffDate(enrollment.getBenefitEffectiveDate());
					enrollment.setNetPremEffDate(enrollment.getBenefitEffectiveDate());
				}
			}
			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.APTC))) {
				enrollment.setAptcAmt(Float.parseFloat(plan.get(EnrollmentConstants.APTC)));
			} else {
				enrollment.setAptcAmt(null);
			}
		}

		/*
		 * }else{ enrollment.setAptcEffDate(null); enrollment.setAptcAmt(null);
		 * }
		 */

		if (isNotNullAndEmpty(pldOrderResponse.getEhbAmount()) && plan.get(EnrollmentConstants.PLAN_TYPE)
				.equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH)) {

			if (!EnrollmentConfiguration.isCaCall()
					&& enrollment.getEnrollmentTypeLkp().getLookupValueCode()
							.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)
					&& (pldOrderResponse.getEhbAmount() == null || pldOrderResponse.getEhbAmount() <= 0.0f)) {
				String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
				enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.INVALID_SLCSP_AMOUNT, EnrollmentConstants.INVALID_SLCSP_AMOUNT, EnrollmentConstants.EMPTY_OR_ZERO_SLCSP +pldOrderResponse.getApplicationId());		
			}

			if (!EnrollmentUtils.isFloatEqual(pldOrderResponse.getEhbAmount(), enrollment.getSlcspAmt())
					&& amountEffectiveDate != null) {
				enrollment.setSlcspEffDate(EnrollmentUtils.getNextMonthStartDate(amountEffectiveDate));
			}
			enrollment.setSlcspAmt(pldOrderResponse.getEhbAmount());

		}

		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT))) {
			if (!EnrollmentUtils.isFloatEqual(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT),
					enrollment.getGrossPremiumAmt())) {
				if (amountEffectiveDate != null) {
					enrollment.setGrossPremEffDate(amountEffectiveDate);
					enrollment.setNetPremEffDate(amountEffectiveDate);
				} else if (sepEffDate != null && enrollment.isRatingAreaChanged()) {
					enrollment.setGrossPremEffDate(sepEffDate);
					enrollment.setNetPremEffDate(sepEffDate);
				}
			}
			enrollment.setGrossPremiumAmt(Float.parseFloat(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT)));
		}
		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.NET_PREMIUM_AMT))) {
			if (!EnrollmentUtils.isFloatEqual(plan.get(EnrollmentConstants.NET_PREMIUM_AMT),
					enrollment.getNetPremiumAmt())) {
				if (amountEffectiveDate != null) {
					enrollment.setNetPremEffDate(amountEffectiveDate);
				} else if (sepEffDate != null && enrollment.isRatingAreaChanged()) {
					enrollment.setNetPremEffDate(sepEffDate);
				}
			}
			enrollment.setNetPremiumAmt(Float.parseFloat(plan.get(EnrollmentConstants.NET_PREMIUM_AMT)));
		}

		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.EMPLOYEE_CONTRIBUTION))) {
			if (!EnrollmentUtils.isFloatEqual(plan.get(EnrollmentConstants.EMPLOYEE_CONTRIBUTION),
					enrollment.getEmployerContribution()) && amountEffectiveDate != null) {
				enrollment.setEmplContEffDate(amountEffectiveDate);
			}
			enrollment.setEmployerContribution(Float.parseFloat(plan.get(EnrollmentConstants.EMPLOYEE_CONTRIBUTION)));
		}

		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.DEPENDENT_CONTRIBUTION))) {
			// enrollment.setEmployerContribution(enrollment.getEmployerContribution()
			// +
			// Float.parseFloat(plan.get(EnrollmentConstants.DEPENDENT_CONTRIBUTION)));
			enrollment.setEmployerContribution(enrollment.getEmployerContribution() == null
					? Float.parseFloat(plan.get(EnrollmentConstants.DEPENDENT_CONTRIBUTION))
					: (enrollment.getEmployerContribution()
							+ Float.parseFloat(plan.get(EnrollmentConstants.DEPENDENT_CONTRIBUTION))));
		}

		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.NET_PREMIUM_AMT))) {
			enrollment.setEmployeeContribution(Float.parseFloat(plan.get(EnrollmentConstants.NET_PREMIUM_AMT)));
		}

	}
	
	private void updateAmountAndEffectiveDatesForSpecial(PldOrderResponse pldOrderResponse, Enrollment enrollment, PldPlanData pdlPlan, Date amountEffectiveDate, AccountUser loggedInUser, Date financialEffectiveDate, Map<String, String> plan, Date sepEffDate, PlanListResponse planListResponse)  throws GIException {
		if(enrollment.getEnrollmentTypeLkp() != null && enrollment.getEnrollmentTypeLkp().getLookupValueCode()
				.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)) {
			updateSHOPAmountAndEffectiveDatesForSpecial(pldOrderResponse, enrollment, pdlPlan, amountEffectiveDate,
					loggedInUser, financialEffectiveDate, plan, sepEffDate, planListResponse);
		}else {
			updateAmountAndEffectiveDatesForSpecialEnrollment(pldOrderResponse, enrollment, pdlPlan,
					amountEffectiveDate, loggedInUser, financialEffectiveDate, plan, sepEffDate, planListResponse);
		}
	}
	
public void updateexistingEnrollmentForSpecial (PldOrderResponse pldOrderResponse, Enrollment enrollment, PldPlanData pdlPlan, Date amountEffectiveDate, AccountUser loggedInUser, Date financialEffectiveDate, PlanListResponse planListResponse) throws GIException{	
		
		Map<String, String> plan = pdlPlan.getPlan();
		String sadp_flag = pldOrderResponse.getSadpFlag();
		String employerId = pldOrderResponse.getEmployerId();
		Character enrollmentReason = pldOrderResponse.getEnrollmentType();
		
		Date sepEffDate =null;
		String coverageStartDate = plan.get(EnrollmentConstants.COVERAGE_START_DATE);
		if (isNotNullAndEmpty(coverageStartDate)){
			String sepEffDateStr = DateUtil.changeFormat(coverageStartDate,"yyyy-MM-dd hh:mm:ss.S",GhixConstants.REQUIRED_DATE_FORMAT);
			sepEffDate =  DateUtil.StringToDate(sepEffDateStr, GhixConstants.REQUIRED_DATE_FORMAT);
		}
		
		updateAmountAndEffectiveDatesForSpecial( pldOrderResponse,  enrollment,  pdlPlan,  amountEffectiveDate,  loggedInUser,  financialEffectiveDate,  plan,  sepEffDate, planListResponse);
		
		Map<String, String> responsiblePerson = null;
		if (isNotNullAndEmpty(pldOrderResponse.getResponsiblePerson())) {
			responsiblePerson = pldOrderResponse.getResponsiblePerson();
		}
		Map<String, String> householdContact = null;
		if (isNotNullAndEmpty(pldOrderResponse.getHouseHoldContact())) {
			householdContact = pldOrderResponse.getHouseHoldContact();
		}

		 if(isNotNullAndEmpty(pldOrderResponse.getPdHouseholdId())){
         	enrollment.setPdHouseholdId(Integer.parseInt(pldOrderResponse.getPdHouseholdId().trim()));
         }

		if(EnrollmentConstants.YES.equalsIgnoreCase(sadp_flag)){
			enrollment.setSadp(EnrollmentConstants.Y);
		}
		else{
			enrollment.setSadp(EnrollmentConstants.N);
		}
		
		if (!isNotNullAndEmpty(pldOrderResponse.getAllowChangePlan())) {
			enrollment.setChangePlanAllowed(EnrollmentConstants.N);
		}
		
		// This is Updated in case of Special Enrollment. Hence Update this in enrollment Table.
		if(isNotNullAndEmpty(pldOrderResponse.getApplicationId()) &&
				(plan.get(EnrollmentConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL))){
				enrollment.setPriorSsapApplicationid(pldOrderResponse.getApplicationId());
				enrollment.setSsapApplicationid(pldOrderResponse.getApplicationId());
		}

		/*************************************************** This does not change ***********************************************************************************************************/
		//				enrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE, plan.get(EnrollmentConstants.MARKET_TYPE)));
		//				enrollment.setPlan(planObj);
		//				enrollment.setCMSPlanID(planResponse.getIssuerPlanNumber());
		//				enrollment.setPlanName(planResponse.getPlanName());
		//				enrollment.setInsurerName(planResponse.getIssuerName());
		//				enrollment.setInsurerTaxIdNumber(planResponse.getTaxIdNumber());
		//				enrollment.setIssuer(issuerObj);
		//				enrollment.setExchgSubscriberIdentifier(houseHoldCaseID);

		/**************************************************************************************************************************************************************/

		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_TYPE))) {
			enrollment.setInsuranceTypeLkp(lookupService.getlookupValueByTypeANDLookupValueLabel("INSURANCE_TYPE", plan.get(EnrollmentConstants.PLAN_TYPE).toString().toLowerCase()));
		}
		if(isNotNullAndEmpty(plan.get(EnrollmentConstants.ORDER_ITEM_ID))){
			enrollment.setPdOrderItemId(Long.valueOf(plan.get(EnrollmentConstants.ORDER_ITEM_ID).trim()));
		}
		
		Enrollee responsiblePersonEnrollee = null;
		Map<String, String> responsiblePersonMap = responsiblePerson;
		if (responsiblePersonMap != null && isNotNullAndEmpty(responsiblePersonMap.get(EnrollmentConstants.RESPONSIBLE_PERSON_ID))) {
			responsiblePersonEnrollee = enrollment.getResponsiblePersonForEnrollment();
			if(responsiblePersonEnrollee == null){
				responsiblePersonEnrollee = new Enrollee();
				enrollment.setResponsiblePerson(responsiblePersonEnrollee);
				responsiblePersonEnrollee.setCreatedBy(loggedInUser);
				responsiblePersonEnrollee.setUpdatedBy(loggedInUser);
			}
			responsiblePersonEnrollee.setExchgIndivIdentifier(responsiblePersonMap.get(EnrollmentConstants.RESPONSIBLE_PERSON_ID));
			responsiblePersonEnrollee.setFirstName(responsiblePersonMap.get("responsiblePersonFirstName"));
			responsiblePersonEnrollee.setMiddleName(responsiblePersonMap.get("responsiblePersonMiddleName"));
			responsiblePersonEnrollee.setLastName(responsiblePersonMap.get("responsiblePersonLastName"));
			responsiblePersonEnrollee.setTaxIdNumber(responsiblePersonMap.get("responsiblePersonSsn"));
			responsiblePersonEnrollee.setSuffix(responsiblePersonMap.get("responsiblePersonSuffix"));
			responsiblePersonEnrollee.setPreferredEmail(responsiblePersonMap.get("responsiblePersonPreferredEmail"));
			responsiblePersonEnrollee.setPreferredSMS(responsiblePersonMap.get("responsiblePersonPreferredPhone"));
			responsiblePersonEnrollee.setPrimaryPhoneNo(responsiblePersonMap.get("responsiblePersonPrimaryPhone"));
			responsiblePersonEnrollee.setSecondaryPhoneNo(responsiblePersonMap.get("responsiblePersonSecondaryPhone"));
			responsiblePersonEnrollee.setRespPersonIdCode(EnrollmentConstants.RESPONSIBLE_PERSON_ID_CODE_QD);

			Location responsiblePersonAddress = responsiblePersonEnrollee.getHomeAddressid();
			if(responsiblePersonAddress == null){
				responsiblePersonAddress = new Location();
				responsiblePersonEnrollee.setHomeAddressid(responsiblePersonAddress);
			}
			responsiblePersonAddress.setAddress1(responsiblePersonMap.get("responsiblePersonHomeAddress1"));
			responsiblePersonAddress.setAddress2(responsiblePersonMap.get("responsiblePersonHomeAddress2"));
			responsiblePersonAddress.setCity(responsiblePersonMap.get("responsiblePersonHomeCity"));
			responsiblePersonAddress.setState(responsiblePersonMap.get("responsiblePersonHomeState"));
			responsiblePersonAddress.setZip(responsiblePersonMap.get("responsiblePersonHomeZip"));	
		}

		Enrollee houseHoldContactEnrollee = null;
		Map<String, String> householdContactMap = householdContact;
		if (isNotNullAndEmpty(householdContactMap) && isNotNullAndEmpty(householdContactMap.get(EnrollmentConstants.HOUSEHOLD_CONTACT_ID))) {
			houseHoldContactEnrollee = enrollment.getHouseholdContactForEnrollment();
			if(houseHoldContactEnrollee == null){
				houseHoldContactEnrollee = new Enrollee();
				enrollment.setHouseholdContact(houseHoldContactEnrollee);
			}

			houseHoldContactEnrollee.setExchgIndivIdentifier(householdContactMap.get(EnrollmentConstants.HOUSEHOLD_CONTACT_ID));
			houseHoldContactEnrollee.setFirstName(householdContactMap.get("houseHoldContactFirstName"));
			houseHoldContactEnrollee.setMiddleName(householdContactMap.get("houseHoldContactMiddleName"));
			houseHoldContactEnrollee.setLastName(householdContactMap.get("houseHoldContactLastName"));
			houseHoldContactEnrollee.setSuffix(householdContactMap.get("houseHoldContactSuffix"));
			houseHoldContactEnrollee.setPreferredEmail(householdContactMap.get("houseHoldContactPreferredEmail"));
			houseHoldContactEnrollee.setPreferredSMS(householdContactMap.get("houseHoldContactPreferredPhone"));
			houseHoldContactEnrollee.setPrimaryPhoneNo(householdContactMap.get("houseHoldContactPrimaryPhone"));
			houseHoldContactEnrollee.setSecondaryPhoneNo(householdContactMap.get("houseHoldContactSecondaryPhone"));
			houseHoldContactEnrollee.setTaxIdNumber(householdContactMap.get("houseHoldContactFederalTaxIdNumber"));

			Location houseHoldContactAddress = houseHoldContactEnrollee.getHomeAddressid();
			if(houseHoldContactAddress == null){
				houseHoldContactAddress = new Location();
				houseHoldContactEnrollee.setHomeAddressid(houseHoldContactAddress);
				houseHoldContactEnrollee.setCreatedBy(loggedInUser);
				houseHoldContactEnrollee.setUpdatedBy(loggedInUser);
			}
			houseHoldContactAddress.setAddress1(householdContactMap.get("houseHoldContactHomeAddress1"));
			houseHoldContactAddress.setAddress2(householdContactMap.get("houseHoldContactHomeAddress2"));
			houseHoldContactAddress.setCity(householdContactMap.get("houseHoldContactHomeCity"));
			houseHoldContactAddress.setState(householdContactMap.get("houseHoldContactHomeState"));
			if(isNotNullAndEmpty(householdContactMap.get("houseHoldContactHomeZip"))){
				houseHoldContactAddress.setZip(householdContactMap.get("houseHoldContactHomeZip"));	
			}
		}

		//Find subscriber
		Enrollee subscriber = null;
		List<Enrollee> enrolleeList = enrollment.getEnrollees();
		if(enrolleeList != null){
			for (Enrollee enrollee:enrolleeList){
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().equals(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){
					//Found the subscriber
					subscriber = enrollee;
					break;
				}
			}
		}

		if(subscriber != null){
			//HIX-51430
//			int subscriberAge = getAge(subscriber.getBirthDate());
//			if(subscriberAge < EnrollmentConstants.AGE_18 && houseHoldContactEnrollee != null && 
//					(isNotNullAndEmpty(plan.get(EnrollmentConstants.MARKET_TYPE))&& plan.get(EnrollmentConstants.MARKET_TYPE).equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)))
//			{
//				subscriber = houseHoldContactEnrollee;
//			}
			StringBuilder subscriberName = new StringBuilder();

			if(isNotNullAndEmpty(subscriber.getFirstName())){
				subscriberName.append(subscriber.getFirstName());
				subscriberName.append(" ");
			}
			if(isNotNullAndEmpty(subscriber.getMiddleName())){
				subscriberName.append(subscriber.getMiddleName());
				subscriberName.append(" ");	
			}
			if(isNotNullAndEmpty(subscriber.getLastName())){
				subscriberName.append(subscriber.getLastName());
				subscriberName.append(" ");
			}
			if(isNotNullAndEmpty(subscriber.getSuffix())){
				subscriberName.append(subscriber.getSuffix());	
			}

			if(subscriberName.length() > EnrollmentConstants.SPONSORNAME_MAX_LENGTH ){
				subscriberName.delete(EnrollmentConstants.SPONSORNAME_MAX_LENGTH,subscriberName.length());
			}

			enrollment.setSubscriberName(subscriberName.toString().trim());
			enrollment.setExchgSubscriberIdentifier(subscriber.getExchgIndivIdentifier());
		}
		else {
			LOGGER.info("Could not find Subscriber for the Enrollment ID:" + enrollment.getId());
		}

		// For getting employers/employee details
		if(enrollment.getEnrollmentTypeLkp()!=null && enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
			if (isNotNullAndEmpty(employerId) && (enrollment.getEmployer() == null || enrollment.getEmployer().equals(Integer.valueOf(employerId)))) {
				Employer employer = new Employer();
				employer.setId(Integer.parseInt(employerId));
				enrollment.setEmployer(employer);

				LOGGER.info("Calling shop service to get employer info for employerId =="+employerId);
				LOGGER.info("shop service URL == "+ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId);
				//String shopInfoResponse=null;
				EmployerDTO employerDTO = null;

				String postResponse =null;
				try{
					postResponse = restTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId, String.class);



					XStream xstream = GhixUtils.getXStreamStaxObject();
					ShopResponse shopResponse= new ShopResponse();
					shopResponse = (ShopResponse) xstream.fromXML(postResponse);
					if(shopResponse != null){

						employerDTO = (EmployerDTO) shopResponse.getResponseData().get("Employer");

						if(employerDTO != null){

							if(employerDTO.getName() != null){			            	
								enrollment.setSponsorName(employerDTO.getName());
							}
							if(employerDTO.getExternalId() != null){
								enrollment.setEmployerCaseId(employerDTO.getExternalId());
							}

							if(employerDTO.getFederalEIN() != null){	
								enrollment.setSponsorEIN(employerDTO.getFederalEIN().toString());
							}

							if(employerDTO.getExternalId()!= null){	
								enrollment.setExternalEmployerId(employerDTO.getExternalId());
							}
						}

						// setting the enrollment status to PAYMENT_RECEIVED when the employer enrollment status is ACTIVE & enrollment reason is 'N' 
						EmployerEnrollment.Status employerEnrollmentStatus = (EmployerEnrollment.Status) shopResponse.getResponseData().get(EnrollmentConstants.ENROLLMENT_STATUS_KEY1); 
						if((employerEnrollmentStatus!=null && enrollmentReason!=null) && ((employerEnrollmentStatus.equals(Status.ACTIVE)  || employerEnrollmentStatus.equals(Status.TERMINATED) ) && enrollmentReason.toString().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_NEW))){
							enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
						}
					}
				}catch(RestClientException re){
					LOGGER.error("Message : "+re.getMessage());
					LOGGER.error("Cause : "+re.getCause());
					LOGGER.error(" ",re);
				}
			}
			else{
				LOGGER.info("No Employer Found");
			}

		}else if(enrollment.getEnrollmentTypeLkp()!=null 
				&& enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
			Enrollee sponsorEnrollee = houseHoldContactEnrollee;
			if(EnrollmentConfiguration.isCaCall() && subscriber!=null){
				int subscriberAge = EnrollmentUtils.getAge(subscriber.getBirthDate(), subscriber.getEffectiveStartDate());
				if(subscriberAge >= EnrollmentConstants.AGE_18){
					sponsorEnrollee =subscriber;
				}
			}
			StringBuilder householdConttactName = new StringBuilder();
			if( null != sponsorEnrollee ){
				if(isNotNullAndEmpty(sponsorEnrollee.getFirstName())){
					householdConttactName.append(sponsorEnrollee.getFirstName());
					householdConttactName.append(" ");
				}
				if(isNotNullAndEmpty(sponsorEnrollee.getMiddleName())){
					householdConttactName.append(sponsorEnrollee.getMiddleName());
					householdConttactName.append(" ");	
				}
				if(isNotNullAndEmpty(sponsorEnrollee.getLastName())){
					householdConttactName.append(sponsorEnrollee.getLastName());
					householdConttactName.append(" ");
				}
				if(isNotNullAndEmpty(sponsorEnrollee.getSuffix())){
					householdConttactName.append(sponsorEnrollee.getSuffix());	
				}

				if(householdConttactName.length() > EnrollmentConstants.SPONSORNAME_MAX_LENGTH ){
					householdConttactName.delete(EnrollmentConstants.SPONSORNAME_MAX_LENGTH,householdConttactName.length());
				}
				enrollment.setSponsorTaxIdNumber(sponsorEnrollee.getTaxIdNumber());
				enrollment.setSponsorName(householdConttactName.toString().trim());
			}
		}

		enrollment.setUpdatedBy(loggedInUser);
		enrollment.setUpdatedOn(new TSDate());
	}

	/**
	 * @Aditya-s @since 25-11-2014
	 * 
	 * Changes Person_Type lookup of Terminated/Cancelled Subscribers
	 * to Enrollee
	 * 
	 * @param enrollee
	 */
	public void changeTerminatedSubscriberToEnrollee(List<Enrollee> enrolleeList) {
		if (enrolleeList != null && !enrolleeList.isEmpty()) {
			Date highestSubscriberEndDate = null;
			List<Enrollee> subscriberEnrollees = new ArrayList<Enrollee>();
			for (Enrollee enrollee : enrolleeList) {
				if (enrollee.getPersonTypeLkp().getLookupValueCode() != null && enrollee.getPersonTypeLkp()
						.getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)) {
					// enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
					if (highestSubscriberEndDate == null) {
						highestSubscriberEndDate = enrollee.getEffectiveEndDate();
					} else if (enrollee.getEffectiveEndDate().after(highestSubscriberEndDate)) {
						highestSubscriberEndDate = enrollee.getEffectiveEndDate();
					}
					subscriberEnrollees.add(enrollee);
				}
			}
			if (subscriberEnrollees != null && subscriberEnrollees.size() > 1) {
				for (Enrollee enrollee : subscriberEnrollees) {
					if (enrollee.getEffectiveEndDate().before(highestSubscriberEndDate)
							&& (enrollee.getEnrolleeLkpValue().getLookupValueCode()
									.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)
									|| (enrollee.getEnrolleeLkpValue().getLookupValueCode()
											.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)))) {
						enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(
								EnrollmentConstants.LOOKUP_PERSON_TYPE,
								EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
					}
				}
			}
		}
	}
	
	private void updateEnrollmentPlanInfoForSpecial(Enrollment enrollment, PldPlanData pdlPlan, Date amountEffectiveDate, PlanListResponse planListResponse) throws GIException{
		
		if(enrollment.getFinancialEffectiveDate()!=null){
			amountEffectiveDate=enrollment.getFinancialEffectiveDate();
		}
		Map<String, String> plan = pdlPlan.getPlan();
		PlanResponse planResponse=null;
		if(pdlPlan.getPlanResponse()==null){
			planResponse = getPlanInfo(planListResponse, plan.get(EnrollmentConstants.PLAN_ID));
		}else{
			planResponse=pdlPlan.getPlanResponse();
		}
		if(planResponse == null || (planResponse.getStatus() == null || planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))){
			LOGGER.info("No Plan data found " );
			throw new GIException(EnrollmentConstants.ERROR_CODE_301,"No Plan data found. Plan mgmt REST services returns null ",EnrollmentConstants.HIGH);
		}else{
			enrollment.setDisableEffectiveDate(EnrollmentConstants.YES);
		}
		
		enrollment.setIssuerLogo(planResponse.getIssuerLogo());
		 boolean populateCSRForZeroAPTC=false;
		 
		 populateCSRForZeroAPTC=Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.POPULATE_CSR_APTC_ZERO));
		
		//HIX-51125 setCsrAmt to null if plan type is DENTAL
        if(isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_TYPE)) && plan.get(EnrollmentConstants.PLAN_TYPE).equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL)){
        	enrollment.setCsrAmt(null);
        	
		}// HIX-35797
		else if(enrollment.getBenefitEffectiveDate() != null 
				&& (plan.get(EnrollmentConstants.CSR) != null && !plan.get(EnrollmentConstants.CSR).equalsIgnoreCase(EnrollmentConstants.CSR_1)
				&& (populateCSRForZeroAPTC || (enrollment.getAptcAmt() != null && enrollment.getAptcAmt() > 0)))){
			Date jan2015CoverageStartDate = DateUtil.StringToDate(EnrollmentConstants.JAN_2015_DATE, GhixConstants.REQUIRED_DATE_FORMAT);
			// HIX-35797 HIX-88110
			// Calculate CSR using multiplier*grossPremiumAmount if effective date is after 1 JAN 2015
			if(enrollment.getBenefitEffectiveDate().compareTo(jan2015CoverageStartDate) >= EnrollmentConstants.ZERO){
				if(isNotNullAndEmpty(enrollment.getGrossPremiumAmt()) && isNotNullAndEmpty(planResponse.getCsrAmount()) && enrollment.getGrossPremiumAmt()>0.0f &&  planResponse.getCsrAmount()>0.0f){
					Float csrAmt=EnrollmentUtils.precision((enrollment.getGrossPremiumAmt() * planResponse.getCsrAmount()), 2);
					if(!EnrollmentUtils.isFloatEqual(csrAmt, enrollment.getCsrAmt())){
						enrollment.setCsrAmt(csrAmt);
						if( amountEffectiveDate!=null){
							enrollment.setCsrEffDate(amountEffectiveDate);
						}
					}
				}else{
					enrollment.setCsrAmt(null);
				}
			}else {
				if(isNotNullAndEmpty(planResponse.getCsrAmount())&&!EnrollmentUtils.isFloatEqual(EnrollmentUtils.precision(planResponse.getCsrAmount(),2), enrollment.getCsrAmt())){
						if(amountEffectiveDate != null){
						    enrollment.setCsrEffDate(amountEffectiveDate);
						}
						enrollment.setCsrAmt(planResponse.getCsrAmount());
				}else{
					enrollment.setCsrAmt(null);
				}
			}
		}else{
			enrollment.setCsrAmt(null);
		}
		if(isNotNullAndEmpty(planResponse.getEhbPercentage()) && isNotNullAndEmpty(enrollment.getGrossPremiumAmt())){
			Float percentOfEssEhbAmt=Float.parseFloat(planResponse.getEhbPercentage());
			if(percentOfEssEhbAmt==null || percentOfEssEhbAmt==0.0f){
				throw new GIException(EnrollmentConstants.INVALID_EHB_PERCENT);
			}
			enrollment.setEhbAmt(percentOfEssEhbAmt*enrollment.getGrossPremiumAmt());
			if(amountEffectiveDate != null){
				enrollment.setEhbEffDate(amountEffectiveDate);
			}
			enrollment.setEhbPercent(percentOfEssEhbAmt);
		}
		if(isNotNullAndEmpty(planResponse.getStateEhbMandatePercentage()) && isNotNullAndEmpty(enrollment.getGrossPremiumAmt())){
			Float percentOfStateEhbAmt=Float.parseFloat(planResponse.getStateEhbMandatePercentage());
			enrollment.setStateEhbAmt(percentOfStateEhbAmt*enrollment.getGrossPremiumAmt());
			if(amountEffectiveDate != null){
				enrollment.setStateEhbEffDate(amountEffectiveDate);
			}
			enrollment.setStateEhbPercent(percentOfStateEhbAmt);
		}
		if(isNotNullAndEmpty(planResponse.getPediatricDentalComponent()) && enrollment.getInsuranceTypeLkp() != null && enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_INSURANCE_TYPE_DENTAL_CODE)){
			Float percentOfDentalEhbAmt = Float.parseFloat(planResponse.getPediatricDentalComponent().replace("$", ""));
			if(amountEffectiveDate != null){
				enrollment.setDntlEhbEffDate(amountEffectiveDate);
			}
			enrollment.setDntlEssentialHealthBenefitPrmDollarVal(percentOfDentalEhbAmt);
			//HIX-85422 2017 EHB portion for SADPs is a percentage of total premium and not a dollar amount
			if(planResponse.getPlanYear() >= 2017 && enrollment.getEnrollmentTypeLkp() != null 
					&& enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
				if(percentOfDentalEhbAmt<0 || percentOfDentalEhbAmt>1){
					throw new GIException(EnrollmentConstants.ERROR_CODE_301,"percentOfDentalEhbAmt comming from PlanMgmgt response is not valid ",EnrollmentConstants.HIGH);
				}
				enrollment.setDntlEhbAmt(percentOfDentalEhbAmt * enrollment.getGrossPremiumAmt());
			}else{
				enrollment.setDntlEhbAmt(percentOfDentalEhbAmt * getEnrolleeCountForDntlEhb(enrollment.getActiveEnrolleesForEnrollment()));
			}
		}
	}
	
	/**
	 * Validates to check if Enrollment/s has "0 < Subscriber < 1"
	 * 
	 * @author Aditya_S
	 * @since  08/01/2015
	 * 
	 * @param enrollmentList
	 * @return
	 * @throws GIException
	 */
	public boolean validateEnrollmentForSubscriber(List<Enrollment> enrollmentList) throws GIException{
		boolean isSubscriberValid = false;
		if(enrollmentList != null && !enrollmentList.isEmpty()){
			for(Enrollment enrollment : enrollmentList){
				int subscriberCount = 0;
				for(Enrollee enrollee : enrollment.getEnrollees()){
					if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
						subscriberCount ++;
					}
				}

				if(subscriberCount <= 0){
					LOGGER.error(EnrollmentConstants.ERR_MSG_NO_SUBSCRIBERS_FOR_ENROLLMENT + EnrollmentConstants.HOUSEHOLD_CASE_ID + enrollment.getHouseHoldCaseId());
					throw new GIException(EnrollmentConstants.ERR_MSG_NO_SUBSCRIBERS_FOR_ENROLLMENT + EnrollmentConstants.HOUSEHOLD_CASE_ID + enrollment.getHouseHoldCaseId());
				}else if(subscriberCount > 1){
					LOGGER.error(EnrollmentConstants.ERR_MSG_MULTIPLE_SUBSCRIBERS_FOR_ENROLLMENT + EnrollmentConstants.HOUSEHOLD_CASE_ID + enrollment.getHouseHoldCaseId());
					throw new GIException(EnrollmentConstants.ERR_MSG_MULTIPLE_SUBSCRIBERS_FOR_ENROLLMENT + EnrollmentConstants.HOUSEHOLD_CASE_ID + enrollment.getHouseHoldCaseId());
				}
				else{
					isSubscriberValid = true;
				}
			}
		}
		return isSubscriberValid;
	}
	
	private void updateEnrollmentPremium(List<Enrollment> enrollmentList) throws GIException{
		if(enrollmentList!=null && enrollmentList.size()>0){
			for (Enrollment enrollment: enrollmentList){
				if(!enrollment.getIsPremiumCalculated()){
					/*if(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode()) || EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())){
							enrollmentRequotingService.updateMonthlyPremiumForCancelTerm(enrollment, enrollment.getRetroDate());
					}else{*/
						enrollmentRequotingService.updateMonthlyPremiumForEnrollment(enrollment, true, true, null, false);
					//}
				}
			}
		}
	}
	
	public boolean validateEnrollmentMemberOverlapping(List<Enrollment> enrollmentList, final List<Enrollment> disEnrollmentList) throws GIException{
		boolean overlapFlag =false;
		for (Enrollment en : enrollmentList) {
			InsuranceType insuranceType = null;
			EnrollmentCoverageValidationRequest enrollmentCoverageValidationRequest = new EnrollmentCoverageValidationRequest();
			List<EnrollmentMemberCoverageDTO> memberList = new ArrayList<EnrollmentMemberCoverageDTO>();
			List<Enrollee> disEnrolledEnrolleeList = null;
			if(disEnrollmentList != null && !disEnrollmentList.isEmpty()){
				List<Enrollment> disEnrolledEnrollmentList = disEnrollmentList.stream().filter(disenrolObj -> disenrolObj.getInsuranceTypeLkp().getLookupValueCode().equals(en.getInsuranceTypeLkp().getLookupValueCode())).collect(Collectors.toList());
				if(disEnrolledEnrollmentList != null && !disEnrolledEnrollmentList.isEmpty()){
					disEnrolledEnrolleeList = new ArrayList<Enrollee>();
					for(Enrollment enr : disEnrolledEnrollmentList){
						if(en.getId() != enr.getId()){
							disEnrolledEnrolleeList.addAll(enr.getEnrolleesAndSubscriber());
						}
					}
				}
			}
			for(Enrollee ee : en.getNonCancelEnrollees()){
				EnrollmentMemberCoverageDTO objEE = new EnrollmentMemberCoverageDTO();
				if(disEnrolledEnrolleeList != null && !disEnrolledEnrolleeList.isEmpty()){
					List<Enrollee> enrolleeList = disEnrolledEnrolleeList.stream().filter(disEnrolleeObj -> disEnrolleeObj.getExchgIndivIdentifier().equals(ee.getExchgIndivIdentifier())).collect(Collectors.toList());
					if(enrolleeList != null && !enrolleeList.isEmpty()){
						objEE.setPriorDisEnrollmentExists(true);
						List<Integer> enrollmentIdList = new ArrayList<Integer>();
						for(Enrollee enrollee : enrolleeList){
							enrollmentIdList.add(enrollee.getEnrollment().getId());
						}
						objEE.setPriorDisEnrollmentList(enrollmentIdList);
					}
				}
				
				objEE.setMemberId(ee.getExchgIndivIdentifier());
				if(ee.getEffectiveStartDate()!=null){
					objEE.setMemberStartDate(DateUtil.dateToString(ee.getEffectiveStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				if(ee.getEffectiveStartDate()!=null){
					objEE.setMemberEndDate(DateUtil.dateToString(ee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				objEE.setNewMember(ee.isNewMember());
				objEE.setEnrolleeId(ee.getId());
				memberList.add(objEE);
			}
			enrollmentCoverageValidationRequest.setMemberList(memberList);
			if(en.getEmployeeAppId() !=null){
				enrollmentCoverageValidationRequest.setEmployeeApplicationId(en.getEmployeeAppId())	;
			}else{
				enrollmentCoverageValidationRequest.setHouseholdCaseId(en.getHouseHoldCaseId());
			}
			if(en.getId()!=null){
				enrollmentCoverageValidationRequest.setEnrollmentId(en.getId());
			}
			if(en.getInsuranceTypeLkp() != null){
				if(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(en.getInsuranceTypeLkp().getLookupValueCode())){
					insuranceType = InsuranceType.HLT;
				}else if(EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE.equalsIgnoreCase(en.getInsuranceTypeLkp().getLookupValueCode())){
					insuranceType = InsuranceType.DEN;
				}
				enrollmentCoverageValidationRequest.setInsuranceType(insuranceType);
			}
			if(validateEnrollmentOverlap(enrollmentCoverageValidationRequest)){
				overlapFlag=true;
				return overlapFlag;
			}
		}//Enrollment Loop
		return overlapFlag;
	}
	
	
	@Override
	public boolean validateEnrollmentOverlap(EnrollmentCoverageValidationRequest request) {
		boolean overlapFlag =false;
		if ((null != request && (null != request.getMemberList() && !request.getMemberList().isEmpty())
				&& null != request.getInsuranceType())
				&& (null != request.getEmployeeApplicationId() || null != request.getHouseholdCaseId())) {
			boolean isExistingEnrollment = (null != request.getEnrollmentId() && 0 != request.getEnrollmentId()) ? true : false;
			boolean isShopRequest = (null != request.getEmployeeApplicationId() && 0 != request.getEmployeeApplicationId()) ? true : false;
			for(EnrollmentMemberCoverageDTO memberDto : request.getMemberList()){
				
				if(null != memberDto.getMemberStartDate() && null != memberDto.getMemberEndDate()){
					if(isExistingEnrollment && isShopRequest){
						overlapFlag = isOverlapExists(null, request.getEmployeeApplicationId(), request.getInsuranceType().name(), 
								memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(),  request.getEnrollmentId(), memberDto.getPriorDisEnrollmentList(), null, false);
					}
					else if(isExistingEnrollment && !isShopRequest){
						overlapFlag = isOverlapExists(request.getHouseholdCaseId(), null, request.getInsuranceType().name(), memberDto.getMemberStartDate(), 
								memberDto.getMemberEndDate(), memberDto.getMemberId(),  request.getEnrollmentId(), memberDto.getPriorDisEnrollmentList(), null, false);
					}
					else if (!isExistingEnrollment && isShopRequest){
						overlapFlag = isOverlapExists(null, request.getEmployeeApplicationId(), 
								request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(), null, memberDto.getPriorDisEnrollmentList(), null, false);
					}
					else{
						overlapFlag = isOverlapExists(request.getHouseholdCaseId(), null,
								request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(), null, memberDto.getPriorDisEnrollmentList(), null, false);
					}
				}
				
				//ref1 starts
				/**Long overlapCount = null;
				if(null != memberDto.getMemberStartDate() && null != memberDto.getMemberEndDate()){
					if(isExistingEnrollment && isShopRequest){
						overlapCount = enrolleeRepository.getEnrolleeOverLapCountForExistingShopEnrollment(request.getEmployeeApplicationId(),
								request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(), request.getEnrollmentId());
					}else if(isExistingEnrollment && !isShopRequest){
						overlapCount = enrolleeRepository.getEnrolleeOverLapCountForExistingEnrollment(request.getHouseholdCaseId(),
								request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(), request.getEnrollmentId());
					}else if (!isExistingEnrollment && isShopRequest){
						overlapCount = enrolleeRepository.getEnrolleeOverLapCountForNewShopEnrollment(request.getEmployeeApplicationId(),
								request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId());
					}else{
						overlapCount = enrolleeRepository.getEnrolleeOverLapCountForNewEnrollment(request.getHouseholdCaseId(),
								request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId());
					}
					if(isNotNullAndEmpty(overlapCount) && overlapCount > 0){
						overlapFlag = true;
						break;
					}
				}**/
				//Ref1 Ends
				if(overlapFlag) {
					break;
				}
			}
			if(isExistingEnrollment && !overlapFlag){
				for(EnrollmentMemberCoverageDTO memberDto : request.getMemberList()){
					if(memberDto.isNewMember()){
						if(null != memberDto.getMemberStartDate() && null != memberDto.getMemberEndDate()){
							if(isShopRequest){
								overlapFlag = isOverlapExists(null, request.getEmployeeApplicationId(),
										request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(), request.getEnrollmentId(),
										memberDto.getPriorDisEnrollmentList(), memberDto.getEnrolleeId(), true);
							}
							else if(!isShopRequest){
								overlapFlag = isOverlapExists(request.getHouseholdCaseId(), null,
										request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(), request.getEnrollmentId(),
										memberDto.getPriorDisEnrollmentList(), memberDto.getEnrolleeId(), true);
							}
							
							//Ref2
							/**
							 * Long overlapCount = null;
							 * if(isShopRequest){
								overlapCount = enrolleeRepository.getOverLapCountForExistingShopEnrollmentNewEnrollees(request.getEmployeeApplicationId(),
										request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(),
										request.getEnrollmentId(), (null != memberDto.getEnrolleeId() ? memberDto.getEnrolleeId() : 0));
							}else if(!isShopRequest){
								overlapCount = enrolleeRepository.getOverLapCountForExistingEnrollmentNewEnrollee(request.getHouseholdCaseId(),
										request.getInsuranceType().name(), memberDto.getMemberStartDate(), memberDto.getMemberEndDate(), memberDto.getMemberId(),
										request.getEnrollmentId(), (null != memberDto.getEnrolleeId() ? memberDto.getEnrolleeId() : 0));
							}
							if(isNotNullAndEmpty(overlapCount) && overlapCount > 0){
								overlapFlag = true;
								break;
							}**/
							//Ref2
						}
					}
					if(overlapFlag) {
						break;
					}
				}
			}
		}else{
			LOGGER.error("Request validation failed. Please check the request :: " + request);
		}
		return overlapFlag;
	}
	
	/**
	 * 
	 * @param houseHoldCaseId String
	 * @param employeeAppId Long
	 * @param insuranceType String
	 * @param startDateProposed String
	 * @param endDateProposed String
	 * @param memberId String
	 * @param currentEnrollmentId Integer
	 * @param previousEnrollmentId Integer
	 * @param previousEnrolleeId Integer
	 * @return
	 */
	private boolean isOverlapExists(final String houseHoldCaseId, 
			final Long employeeAppId, final String insuranceType, final String startDateProposed, final String endDateProposed,
			final String memberId, final Integer currentEnrollmentId,  final List<Integer> previousEnrollmentIdList, final Integer previousEnrolleeId, boolean isNewEnrollee){
		boolean isOverlap = false;
		BigDecimal totalRecordCount = BigDecimal.ZERO;
		EntityManager entityManager = null;
		try{
			entityManager = entityManagerFactory.createEntityManager();
			Query countQuery = null;
			StringBuilder queryBuilder = new StringBuilder(200);
			queryBuilder.append(OVERLAP_SQL_QUERY_STRING);
			
			if(StringUtils.isNotBlank(houseHoldCaseId)){
				queryBuilder.append(" AND en.household_case_id = '"+houseHoldCaseId);
				queryBuilder.append("'");
			}
			if(employeeAppId != null){
				queryBuilder.append(" AND en.EMPLOYEE_APPLICATION_ID = "+employeeAppId);
			}
			if(currentEnrollmentId != null && currentEnrollmentId != EnrollmentConstants.ZERO){
				if(isNewEnrollee){
					queryBuilder.append(" AND ee.enrollment_id = "+currentEnrollmentId);
				}else{
					queryBuilder.append(" AND ee.enrollment_id != "+currentEnrollmentId);
				}
				
			}
			if(previousEnrollmentIdList != null && !previousEnrollmentIdList.isEmpty()){
				queryBuilder.append(" AND ee.enrollment_id not in(:PREVIOUS_ENROLLMENT_ID_LIST");
				queryBuilder.append(")");
			}
			if(previousEnrolleeId != null){
				queryBuilder.append(" AND ee.id <> "+previousEnrolleeId);
			}
			countQuery = entityManager.createNativeQuery(queryBuilder.toString());
			countQuery.setParameter("INSURANCE_TYPE_LKP", insuranceType);
			countQuery.setParameter("START_DATE", startDateProposed);
			countQuery.setParameter("END_DATE", endDateProposed);
			countQuery.setParameter("EXCHG_INDIV_IDENTIFIER", memberId);
			if(previousEnrollmentIdList != null && !previousEnrollmentIdList.isEmpty()){
				countQuery.setParameter("PREVIOUS_ENROLLMENT_ID_LIST", previousEnrollmentIdList);
			}
			Number resultCnt = (Number) countQuery.getSingleResult();
			totalRecordCount =  resultCnt != null ? new BigDecimal(resultCnt.toString()) : BigDecimal.ZERO;
			
		}
		catch(Exception ex){
			LOGGER.error("Exception caught : ",ex);
		}
		finally{
			if(entityManager !=null && entityManager.isOpen()){
				 try{
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
				 catch(IllegalStateException ex){
					 LOGGER.error("Exception caught while closing EntityManager: ", ex);
				 }
			 }
		}
		if(isNotNullAndEmpty(totalRecordCount) && totalRecordCount.compareTo(BigDecimal.ZERO) != EnrollmentConstants.ZERO){
			LOGGER.error("Overlap Found: houseHoldCaseId: "+houseHoldCaseId+ " employeeAppId: "+employeeAppId+
					" insuranceType: "+insuranceType+" startDateProposed "+startDateProposed+" endDateProposed: "+endDateProposed+
			" memberId: "+memberId+" currentEnrollmentId "+currentEnrollmentId+" previousEnrollmentIdList "+previousEnrollmentIdList+ " previousEnrolleeId "+ previousEnrolleeId+" isNewEnrollee "+ isNewEnrollee);
			isOverlap = true;
		}
		return isOverlap;
	}
	
	private void createEnrollee(EnrolleeCoreDto enrolleeDto, EnrollmentCoreDto enrollmentDto, Enrollment enrollment, Enrollee enrollee) throws GIException{
		if(enrolleeDto!=null && enrollmentDto!=null){
			AccountUser loggedInUser= getLoggedInUser();
			//enrollee = new Enrollee();
			enrollee.setNewMember(true);
			enrollee.setEnrollment(enrollment);
			enrollee.setEnrollmentReason(enrollmentDto.getEnrollmentReason());
			enrollee.setEffectiveStartDate(enrolleeDto.getEffectiveStartDate());
			enrollee.setEffectiveEndDate(enrollment.getBenefitEndDate());
			enrollee.setQuotingDate(enrolleeDto.getEffectiveStartDate());
			
			if(enrolleeDto.getBirthDate()!=null){
				
				if(!DateUtils.isSameDay(enrollee.getEffectiveStartDate(), enrolleeDto.getBirthDate()) && enrolleeDto.getBirthDate().after(enrollee.getEffectiveStartDate())){
					throw new GIException(EnrollmentConstants.INVALID_DOB);
				}
				enrollee.setBirthDate(enrolleeDto.getBirthDate());
			}
			enrollee.setExchgIndivIdentifier(enrolleeDto.getExchgIndivIdentifier());
			enrollee.setFirstName(enrolleeDto.getFirstName());
			enrollee.setLastName(enrolleeDto.getLastName());
			enrollee.setMiddleName(enrolleeDto.getMiddleName());
			enrollee.setPrimaryPhoneNo(enrolleeDto.getPrimaryPhoneNo());
			enrollee.setSecondaryPhoneNo(enrolleeDto.getSecondaryPhoneNo());
			enrollee.setPreferredEmail(enrolleeDto.getPreferredEmail());
			enrollee.setPreferredSMS(enrolleeDto.getPreferredSMS());
			enrollee.setTaxIdNumber(enrolleeDto.getTaxIdNumber());
			enrollee.setSuffix(enrolleeDto.getSuffix());

			if (isNotNullAndEmpty(enrolleeDto.getTotalIndvResponsibilityAmt())) {
				enrollee.setTotIndvRespEffDate(enrolleeDto.getEffectiveStartDate());
				enrollee.setTotalIndvResponsibilityAmt(enrolleeDto.getTotalIndvResponsibilityAmt());
			}
			//enrollee.setRatingArea(enrolleeDto.getRatingArea());
			if (isNotNullAndEmpty(enrolleeDto.getRatingArea())) {
				if(!enrolleeDto.getRatingArea().equalsIgnoreCase(enrollee.getRatingArea())){
					enrollee.setRatingArea(enrolleeDto.getRatingArea());
					enrollee.setRatingAreaEffDate(enrolleeDto.getEffectiveStartDate());
				}
			}

			enrollee.setMaintainenceReasonCode(enrolleeDto.getMaintainenceReasonCode());

			enrollee.setEffectiveStartDate(enrolleeDto.getEffectiveStartDate());

			String enrollmentStatusCode = enrollment.getEnrollmentStatusLkp().getLookupValueCode();
			String enrollmentTypeLkp=null;

			if(enrollment.getEnrollmentTypeLkp()!=null){
				enrollmentTypeLkp=enrollment.getEnrollmentTypeLkp().getLookupValueCode();
			}			

			if(enrollmentStatusCode != null && (enrollmentStatusCode.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM) || enrollmentStatusCode.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED) || enrollmentStatusCode.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))&& enrollmentTypeLkp!=null && enrollmentTypeLkp.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED));
			}else{
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_PENDING));
			}

			if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EFFECTUATE_NEWLY_ADDED_MEMBERS))){
				if(enrollment.getEnrollmentStatusLkp() != null && EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())){
					enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
				}
			}
			if(isNotNullAndEmpty(enrolleeDto.getSubscriberNewFlag()) && enrolleeDto.getSubscriberNewFlag().equalsIgnoreCase(EnrollmentConstants.YES)){
				enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.PERSON_TYPE_SUBSCRIBER));
			}
			else{
				enrollee.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE));
			}

			if (isNotNullAndEmpty(enrolleeDto.getGenderLkpStr())) {
				enrollee.setGenderLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.GENDER, enrolleeDto.getGenderLkpStr()));
			}
			if (isNotNullAndEmpty(enrolleeDto.getMaritalStatusLkpStr())) {
				enrollee.setMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.MARITAL_STATUS, enrolleeDto.getMaritalStatusLkpStr()));
			}

			if (isNotNullAndEmpty(enrolleeDto.getLanguageSpokenLkpStr())) {
				enrollee.setLanguageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_SPOKEN, enrolleeDto.getLanguageSpokenLkpStr()));	
			}

			if (isNotNullAndEmpty(enrolleeDto.getLanguageWrittenLkpStr())) {
				enrollee.setLanguageWrittenLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LANGUAGE_WRITTEN, enrolleeDto.getLanguageWrittenLkpStr()));	
			}

			if (isNotNullAndEmpty(enrolleeDto.getTobaccoUsageLkpStr())) {
				if(enrolleeDto.getTobaccoUsageLkpStr().toString().equalsIgnoreCase(EnrollmentConstants.Y)){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.T));
				}else if(enrolleeDto.getTobaccoUsageLkpStr().toString().equalsIgnoreCase(EnrollmentConstants.N)){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.N));
				}else if(enrolleeDto.getTobaccoUsageLkpStr().toString().equalsIgnoreCase(EnrollmentConstants.U)){
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.U));
				}/*
				else{
					enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, enrolleeDto.getTobaccoUsageLkpStr()));
				}*/
				enrollee.setLastTobaccoUseDate(enrollee.getEffectiveStartDate());
			}else if(EnrollmentConfiguration.isCaCall()){
				enrollee.setTobaccoUsageLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.TOBACCO_USAGE, EnrollmentConstants.U));
				enrollee.setLastTobaccoUseDate(enrollee.getEffectiveStartDate());
			}
			//LAST_TOBACCO_USE_DATE field is added for FFM flow
			if(enrolleeDto.getLastTobaccoUseDate()!=null){
				enrollee.setLastTobaccoUseDate(enrolleeDto.getLastTobaccoUseDate());
			}

			if (isNotNullAndEmpty(enrolleeDto.getRelationToHCPLkpStr())) {
				LookupValue relToHCP=lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.RELATIONSHIP, enrolleeDto.getRelationToHCPLkpStr());
				if(relToHCP!=null){
					enrollee.setRelationshipToHCPLkp(relToHCP);	
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_RELATION_HCP_CODE_VALUE +" for member : "+ enrolleeDto.getExchgIndivIdentifier());
				}
			}else{
				LOGGER.error(EnrollmentConstants.ERR_MSG_RELATION_HCP_CODE_VALUE+ " :: "+ enrolleeDto.getRelationToHCPLkpStr()+" for member : "+ enrolleeDto.getExchgIndivIdentifier());
				throw new GIException(EnrollmentConstants.ERR_MSG_RELATION_HCP_CODE_VALUE+ " :: "+ enrolleeDto.getRelationToHCPLkpStr()+" for member : "+ enrolleeDto.getExchgIndivIdentifier());
			}

			if (isNotNullAndEmpty(enrolleeDto.getCitizenshipStatusLkpStr())) {
				
				LookupValue lv = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.CITIZENSHIP_STATUS, enrolleeDto.getCitizenshipStatusLkpStr());
				if(lv !=null ){
					enrollee.setCitizenshipStatusLkp(lv);
				}else{
					throw new GIException("No CitizenshipStatus lookup found for given code : "+enrolleeDto.getCitizenshipStatusLkpStr());
				}
			}else{
				//throw new GIException(EnrollmentConstants.ERR_MSG_CITIZENSHIP_STATUS_NULL);
			}

			// Loop Through Newly Created Enrollee List and Search Custodial Parent
			if (enrolleeDto.getCustodialParentDto()!=null && isNotNullAndEmpty(enrolleeDto.getCustodialParentDto().getExchgIndivIdentifier())) {
				boolean createNewCustInd = true;
				for (Enrollee enrollee2 : enrollment.getEnrollees()) {
					Enrollee tempCustodialParent = enrollee2.getCustodialParent();
					if(tempCustodialParent != null && enrolleeDto.getCustodialParentDto().getExchgIndivIdentifier().equalsIgnoreCase(tempCustodialParent.getExchgIndivIdentifier())){
						enrollee.setCustodialParent(tempCustodialParent);
						createNewCustInd=false;
						break;
					}
				}//End of for

				if(createNewCustInd){
					EnrolleeCoreDto custodialDto= enrolleeDto.getCustodialParentDto();
					Enrollee custodialParent= new Enrollee();
					custodialParent.setExchgIndivIdentifier(custodialDto.getExchgIndivIdentifier());
					custodialParent.setFirstName(custodialDto.getFirstName());
					custodialParent.setMiddleName(custodialDto.getMiddleName());
					custodialParent.setLastName(custodialDto.getLastName());
					//					custodialParent.setSsn(member.get("custodialParentSsn"));
					custodialParent.setTaxIdNumber(custodialDto.getTaxIdNumber()); //ssn
					custodialParent.setSuffix(custodialDto.getSuffix());
					custodialParent.setPreferredEmail(custodialDto.getPreferredEmail());
					custodialParent.setPreferredSMS(custodialDto.getPreferredSMS());
					custodialParent.setPrimaryPhoneNo(custodialDto.getPrimaryPhoneNo());
					custodialParent.setSecondaryPhoneNo(custodialDto.getSecondaryPhoneNo());
					custodialParent.setEnrollment(enrollment);

					if(isNotNullAndEmpty(custodialDto.getHomeAddressDto())){
						LocationCoreDto locationDto= custodialDto.getHomeAddressDto();
						Location custParentAddress = new Location();
						custParentAddress.setAddress1(locationDto.getAddress1());
						custParentAddress.setAddress2(locationDto.getAddress2());
						custParentAddress.setCity(locationDto.getCity());
						custParentAddress.setState(locationDto.getState());
						custParentAddress.setZip(locationDto.getZip());	

						custodialParent.setHomeAddressid(custParentAddress);
					}


					custodialParent.setPersonTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_PERSON_TYPE,EnrollmentConstants.LOOKUP_PERSON_TYPE_CUSTODIAL_PARENT));

					custodialParent.setCreatedBy(loggedInUser);
					custodialParent.setUpdatedBy(loggedInUser);
					enrollee.setCustodialParent(custodialParent);
					//					custIndicator=true;
					List<Enrollee> enrolleeList= enrollment.getEnrollees();
					if(enrolleeList==null){
						enrolleeList= new ArrayList<Enrollee>();
						
					}
					enrolleeList.add(custodialParent);
					enrollment.setEnrollees(enrolleeList);
				}
			}
			if(isNotNullAndEmpty(enrolleeDto.getMailingAddressDto())){
				Location mailingAddress = new Location();
				LocationCoreDto locationDto= enrolleeDto.getMailingAddressDto();
				mailingAddress.setAddress1(locationDto.getAddress1());
				mailingAddress.setAddress2(locationDto.getAddress2());
				mailingAddress.setCity(locationDto.getCity());
				mailingAddress.setState(locationDto.getState());
				
				if(isNotNullAndEmpty(locationDto.getZip())){
					mailingAddress.setZip(locationDto.getZip());
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE);
				}
				//HIX-89488 Removing County Code check from Mailing address
				if(isNotNullAndEmpty(locationDto.getCountycode())){
					mailingAddress.setCountycode(locationDto.getCountycode());
				}/*else{
					throw new GIException(EnrollmentConstants.ERR_MSG_COUNTY_CODE_VALUE);
				}*/
				
				enrollee.setMailingAddressId(mailingAddress);
			}


			if(isNotNullAndEmpty(enrolleeDto.getHomeAddressDto())){
				Location homeAddress = new Location();
				LocationCoreDto locationDto =enrolleeDto.getHomeAddressDto();
				homeAddress.setAddress1(locationDto.getAddress1());
				homeAddress.setAddress2(locationDto.getAddress2());
				homeAddress.setCity(locationDto.getCity());
				homeAddress.setState(locationDto.getState());
				
				if(isNotNullAndEmpty(locationDto.getCountycode())){
					homeAddress.setCountycode(locationDto.getCountycode());
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_COUNTY_CODE_VALUE);
				}
				
				if(isNotNullAndEmpty(locationDto.getZip())){
					homeAddress.setZip(locationDto.getZip());
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_ZIP_CODE_VALUE);
				}
				
				enrollee.setHomeAddressid(homeAddress);
			}


			List<EnrolleeRace> enrolleeRace = new ArrayList<EnrolleeRace>();
			Map<String,String> raceMap=enrolleeDto.getRaceMap();
			if(raceMap!=null && !raceMap.isEmpty()){

				Set<String> keySet=raceMap.keySet();
				for(String race : keySet){
					EnrolleeRace tmpenrolleeRace = new EnrolleeRace();
					tmpenrolleeRace.setRaceEthnicityLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_TYPE_RACE, race));
					tmpenrolleeRace.setRaceDescription(raceMap.get(race));
					tmpenrolleeRace.setEnrollee(enrollee);
					enrolleeRace.add(tmpenrolleeRace);
				}

				enrollee.setEnrolleeRace(enrolleeRace);
			}
				
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent(); 
			enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTOMATE_SPECIAL_ADD.toString());
			enrollmentEvent.setEnrollee(enrollee);
			enrollmentEvent.setEnrollment(enrollment);
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_ADDITION));
			enrollmentEvent.setCreatedBy(loggedInUser);
			enrollmentEvent.setUpdatedBy(loggedInUser);
			// This is used only in case of special enrollment. So it may not be present for Initial Enrollment
			if(isNotNullAndEmpty(enrolleeDto.getMaintainenceReasonCode())){
				if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  enrolleeDto.getMaintainenceReasonCode()))){
					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, enrolleeDto.getMaintainenceReasonCode()));
				}else{
					throw new RuntimeException(EnrollmentConstants.ERR_MSG_REASON_CODE_VALUE);
				}
			}

			if(!isNotNullAndEmpty(enrolleeDto.getSercStr())){
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  enrolleeDto.getMaintainenceReasonCode()));
			}
			else if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, enrolleeDto.getSercStr()))){
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, enrolleeDto.getSercStr()));
			}else{
				throw new RuntimeException("Lookup value is null for serc : "+enrolleeDto.getSercStr());
			}

			//added last event Id for Audit
			enrollee.setLastEventId(enrollmentEvent);
			List<EnrollmentEvent> enrollmentEventList= new ArrayList<EnrollmentEvent>();
			enrollmentEventList.add(enrollmentEvent);
			enrollee.setEnrollmentEvents(enrollmentEventList);
			enrollee.setCreatedBy(loggedInUser);
			enrollee.setUpdatedBy(loggedInUser);
			enrollee.setCreatedOn(new TSDate());
			
			List<Enrollee> enrolleeList= enrollment.getEnrollees();
			if(enrolleeList==null){
				enrolleeList= new ArrayList<Enrollee>();
				
			}
			enrolleeList.add(enrollee);
			enrollment.setEnrollees(enrolleeList);
			
		}
	}
	
	private void disEnrollEnrollee(Enrollee enrollee, Enrollment enrollment, EnrolleeCoreDto enrolleeDto, final AccountUser loggedInUser) throws GIException {
		Map<String, Object> disEnrollmentMap = new HashMap<String, Object>();
		if(enrolleeDto!=null && enrollee != null){
			try{
				disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_ID_KEY,enrollment.getId());
				disEnrollmentMap.put(EnrollmentConstants.DIS_ENROLLMENT_TYPE,enrollment.getEnrollmentTypeLkp().getLookupValueCode());
				disEnrollmentMap.put(EnrollmentConstants.MEMBER_ID_KEY,enrollee.getExchgIndivIdentifier());
				disEnrollmentMap.put(EnrollmentConstants.TERMINATION_REASON_CODE, enrolleeDto.getMaintainenceReasonCode());
				disEnrollmentMap.put(EnrollmentConstants.TERMINATION_DATE, DateUtil.dateToString(enrolleeDto.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT) );
				if(enrolleeDto.getDeathDate()!=null){
					disEnrollmentMap.put(EnrollmentConstants.DEATH_DATE, DateUtil.dateToString(enrolleeDto.getDeathDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}

				String serc=enrolleeDto.getSercStr();
				disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_REASON, EnrollmentConstants.ENROLLMENT_REASON_S);
				
				/***** Calculate whether termination/cancelletion based on termination date and enrollment effective Start date and Status. *****/
				String enrolleeDbStatus = enrollee.getEnrolleeLkpValue().getLookupValueCode();
				boolean terminationFlag=false;
				if(enrollee.getEnrolleeLkpValue()!=null && EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrolleeDbStatus) ){
					terminationFlag=true;
				}else if(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrolleeDbStatus)){
					return;
				}
				
				
				Date terminationDate = enrolleeDto.getEffectiveEndDate();
				if(enrollee.getEffectiveStartDate() != null){
					if(terminationDate.after(enrollee.getEffectiveStartDate()) && !DateUtils.isSameDay(terminationDate, enrollee.getEffectiveStartDate())){
						
						disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_STATUS_KEY1,EnrollmentConstants.ENROLLMENT_STATUS_TERM);
						//HIX-110972 Remove cancellation logic for future termination for CA
						/*if(EnrollmentConfiguration.isCaCall()){
							if(enrolleeDbStatus!=null && (enrolleeDbStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING))){
								disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_STATUS_KEY1,EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
							}
						}*/
					}else{
						disEnrollmentMap.put(EnrollmentConstants.ENROLLMENT_STATUS_KEY1,EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
					}
				}

				String disEnrollResponse  = disEnrollMemberSpecialEnrollment(enrollee, disEnrollmentMap, serc, terminationFlag, loggedInUser, EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTOMATE_SPECIAL_DISENROLL);

				if(isNotNullAndEmpty(disEnrollResponse) && GhixConstants.RESPONSE_FAILURE.equalsIgnoreCase(disEnrollResponse)){
					throw new GIException("Disenrollment failed for enrollee with memberId : " + enrollee.getExchgIndivIdentifier());
				}
				List<Enrollment> enrollmentList= new ArrayList<>();
				enrollmentList.add(enrollment);
				checkEnrollmentStatusSpecialEnrollment(enrollmentList);
			} 
			catch (GIException gie){
				LOGGER.error(gie.getMessage());
				throw  gie;
			}
		}
	}
	
	private void updateExistingEnrollee(EnrolleeCoreDto enrolleeDto, Enrollee enrollee) throws GIException{
		if(enrollee!=null ){
			AccountUser loggedInUser= getLoggedInUser();
			if (isNotNullAndEmpty(enrolleeDto.getTotalIndvResponsibilityAmt()) && !EnrollmentUtils.isFloatEqual(enrolleeDto.getTotalIndvResponsibilityAmt(), enrollee.getTotalIndvResponsibilityAmt())) {
				/**As per the Spec Add Reporting Category Effective Dates 
				 * following Effective Date should not be changes for change TXN**/
				//enrollee.setTotIndvRespEffDate(enrolleeDto.getEffectiveStartDate());
				enrollee.setTotalIndvResponsibilityAmt(enrolleeDto.getTotalIndvResponsibilityAmt());
			}

			//enrollee.setRatingArea(enrolleeDto.getRatingArea());
			
			if (isNotNullAndEmpty(enrolleeDto.getRelationToHCPLkpStr())) {
				LookupValue relToHCP=lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.RELATIONSHIP, enrolleeDto.getRelationToHCPLkpStr());
				if(relToHCP!=null){
					enrollee.setRelationshipToHCPLkp(relToHCP);	
				}else{
					LOGGER.error(EnrollmentConstants.ERR_MSG_RELATION_HCP_CODE_VALUE+ " :: "+ enrolleeDto.getRelationToHCPLkpStr()+" for member : "+ enrolleeDto.getExchgIndivIdentifier());
					throw new GIException(EnrollmentConstants.ERR_MSG_RELATION_HCP_CODE_VALUE+ " :: "+ enrolleeDto.getRelationToHCPLkpStr()+" for member : "+ enrolleeDto.getExchgIndivIdentifier());
				}
			}
			
			if (isNotNullAndEmpty(enrolleeDto.getRatingArea())) {
				if(!enrolleeDto.getRatingArea().equalsIgnoreCase(enrollee.getRatingArea())){
					enrollee.setRatingArea(enrolleeDto.getRatingArea());
					enrollee.setRatingAreaEffDate(enrollee.getEnrollment().getNetPremEffDate());
				}
			}
			if(isNotNullAndEmpty(enrolleeDto.getHomeAddressDto())){
				Location homeAddress = enrollee.getHomeAddressid();
				if(homeAddress==null){
					homeAddress= new Location();
				}
				LocationCoreDto locationDto =enrolleeDto.getHomeAddressDto();
				homeAddress.setAddress1(locationDto.getAddress1());
				homeAddress.setAddress2(locationDto.getAddress2());
				homeAddress.setCity(locationDto.getCity());
				homeAddress.setState(locationDto.getState());
				if(isNotNullAndEmpty(locationDto.getCountycode())){
					homeAddress.setCountycode(locationDto.getCountycode());
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_COUNTY_CODE_VALUE);
				}
				
				if(isNotNullAndEmpty(locationDto.getZip())){
					homeAddress.setZip(locationDto.getZip());
				}else{
					throw new GIException(EnrollmentConstants.ERR_MSG_COUNTY_CODE_VALUE);
				}
				
				enrollee.setHomeAddressid(homeAddress);
			}
			if(isNotNullAndEmpty(enrolleeDto.getBirthDate())){
				
				if(!DateUtils.isSameDay(enrollee.getEffectiveStartDate(), enrolleeDto.getBirthDate()) && enrolleeDto.getBirthDate().after(enrollee.getEffectiveStartDate())){
					throw new GIException(EnrollmentConstants.INVALID_DOB);
				}
				enrollee.setBirthDate(enrolleeDto.getBirthDate());
			}
			enrollee.setCreatedBy(loggedInUser);
			enrollee.setUpdatedBy(loggedInUser);
		}
	}
	
	private void createEnrolleeEvent(Enrollee enrollee,String eventType ,String eventReasonCode, AccountUser user, boolean sendToCarrier, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier){
		createEnrolleeEvent( enrollee, eventType , eventReasonCode,  user,  sendToCarrier,false,  txn_idfier);
	}
	
	private void createEnrolleeEvent(Enrollee enrollee,String eventType ,String eventReasonCode, final AccountUser user, boolean sendToCarrier, boolean isSpecial, EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier){
		List<EnrollmentEvent> eventList = new ArrayList<EnrollmentEvent>();
		EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
		enrollmentEvent.setEnrollee(enrollee);
		enrollmentEvent.setEnrollment(enrollee.getEnrollment());
		enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,eventType));
		//add null check
		if(isNotNullAndEmpty(eventReasonCode)){
				if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode))){
					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode));
				}else{
					throw new RuntimeException(EnrollmentConstants.ERR_MSG_LOOKUP_VALUE_IS_NULL);
				}
		}else{
			throw new RuntimeException(EnrollmentConstants.ERR_MSG_REASON_CODE_VALUE);
		}
		
		if(isSpecial){
			enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, eventReasonCode));
		}
		if(!sendToCarrier){
			enrollmentEvent.setSendToCarrierFlag("false");
		}
		if(isNotNullAndEmpty(txn_idfier)){
			enrollmentEvent.setTxnIdentifier(txn_idfier.toString());
		}
		if(user != null){
			enrollmentEvent.setCreatedBy(user);
			enrollmentEvent.setUpdatedBy(user);
		}
		eventList.add(enrollmentEvent);
		enrollee.setUpdatedBy(user);
		enrollee.setLastEventId(enrollmentEvent);
	}
	
	private void createEvent(Enrollee enrollee, Enrollment enrollment, EnrolleeCoreDto enrolleeDto, AccountUser loggedInUser,EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier ){
		if(!enrollee.isEventCreated()){
			List<EnrollmentEvent> enrolleeEventList= new ArrayList<EnrollmentEvent>();
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent(); 
			if(txn_idfier!=null){
				enrollmentEvent.setTxnIdentifier(txn_idfier.toString());
			}
			enrollmentEvent.setEnrollee(enrollee);
			enrollmentEvent.setEnrollment(enrollment);
			enrollmentEvent.setCreatedBy(loggedInUser);
			enrollmentEvent.setUpdatedBy(loggedInUser);
			if(isNotNullAndEmpty(enrolleeDto.getMaintainenceReasonCode())){
				if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  enrolleeDto.getMaintainenceReasonCode()))){
					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  enrolleeDto.getMaintainenceReasonCode()));
				}else{
					throw new RuntimeException("Lookup value is null");
				}
			}else{
				throw new RuntimeException("Maintanance Reason code is null or empty");
			}
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CHANGE));
			//add null check
			if(!isNotNullAndEmpty(enrolleeDto.getSercStr())){
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  enrolleeDto.getSercStr()));
			}
			else if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, enrolleeDto.getSercStr()))){
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, enrolleeDto.getSercStr()));
			}else{
				throw new RuntimeException("Lookup value is null for serc : "+enrolleeDto.getSercStr());
			}
	
			enrollee.setLastEventId(enrollmentEvent);
			enrolleeEventList.add(enrollmentEvent);
			enrollee.setEnrollmentEvents(enrolleeEventList);
			enrollee.setEventCreated(true);
		}
	}
	
	private void updateHouseholdResponsible(EnrolleeCoreDto enrolleeDto, List<Enrollee> enrollees, AccountUser user){
		if(enrolleeDto!=null && enrollees!=null && enrollees.size()>0){
			List<Enrollee> householdResponsible= getHouseholdContactResponsiblePerson(enrolleeDto.getExchgIndivIdentifier(), enrollees);
			if(householdResponsible!=null && householdResponsible.size()>0){
				
				for(Enrollee enrollee : householdResponsible){
					boolean change =false;
					if(enrollee.getBirthDate()!=null && enrolleeDto.getBirthDate() !=null&& DateUtils.isSameDay(enrollee.getBirthDate(), enrolleeDto.getBirthDate())){
						change=true;
						enrollee.setBirthDate(enrolleeDto.getBirthDate());
					}else if(enrollee.getBirthDate()!=null ||enrolleeDto.getBirthDate()!=null ){
						change=true;
						enrollee.setBirthDate(enrolleeDto.getBirthDate());
					}
					if(isNotNullAndEmpty(enrolleeDto.getHomeAddressDto())){
						change=true;
						Location homeAddress = enrollee.getHomeAddressid();
						if(homeAddress==null){
							homeAddress= new Location();
						}
						LocationCoreDto locationDto =enrolleeDto.getHomeAddressDto();
						homeAddress.setAddress1(locationDto.getAddress1());
						homeAddress.setAddress2(locationDto.getAddress2());
						homeAddress.setCity(locationDto.getCity());
						homeAddress.setState(locationDto.getState());
						if(isNotNullAndEmpty(locationDto.getCountycode())){
							homeAddress.setCountycode(locationDto.getCountycode());
						}
						
						if(isNotNullAndEmpty(locationDto.getZip())){
							homeAddress.setZip(locationDto.getZip());
						}
						
						enrollee.setHomeAddressid(homeAddress);
					}
					
					if(change){
						createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE, enrolleeDto.getMaintainenceReasonCode(), user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTOMATE_SPECIAL);
					}
				}
				
			}
		}
	}
	
	private List<Enrollee> getHouseholdContactResponsiblePerson(String memberId, List<Enrollee> enrollees){
		List<Enrollee> householdResponsibleEnrollees = new ArrayList<Enrollee>();
		if(enrollees != null && isNotNullAndEmpty(memberId)){
			for(Enrollee enrollee : enrollees){
				if(enrollee.getPersonTypeLkp() != null &&enrollee.getExchgIndivIdentifier().equalsIgnoreCase(memberId) 
						&& (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_HOUSEHOLD_CONTACT) 
								|| enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_RESPONSIBLE_PERSON))){
					householdResponsibleEnrollees.add(enrollee) ;
				}
			}
		}
		return householdResponsibleEnrollees;
	}

	private void updateExistingEnrollment(Enrollment enrollment, EnrollmentCoreDto enrollmentDto, AccountUser loggedInUser) throws GIException{
		 Date amountEffectiveDate=null;
		 if(enrollmentDto.getFinancialEffectiveDate()!=null){
			 amountEffectiveDate=enrollmentDto.getFinancialEffectiveDate();
		 }else{
			 amountEffectiveDate=enrollmentDto.getBenefitEffectiveDate();
		 }
		 if (amountEffectiveDate != null) {
				if (DateUtil.getDateOfMonth(amountEffectiveDate) != 1 ) {
					if(enrollment.getBenefitEndDate() != null  &&
							amountEffectiveDate.getMonth()==enrollment.getBenefitEndDate().getMonth()){
						amountEffectiveDate = EnrollmentUtils.getMonthStartDate(enrollment.getBenefitEndDate());
					}else  {
						amountEffectiveDate = EnrollmentUtils.getNextMonthStartDate(amountEffectiveDate);
					}
					
				} 
			}
		 
		 if (!enrollment.isAPTCUpdated() && !EnrollmentUtils.isFloatEqual(enrollmentDto.getAptcAmt(), enrollment.getAptcAmt())) {
				enrollment.setAptcEffDate(amountEffectiveDate);
				enrollment.setNetPremEffDate(amountEffectiveDate);
				enrollment.setAptcAmt(enrollmentDto.getAptcAmt());
			}
				
		
		
		/*
		 - removed the validation as financial flow will not pass slcsp - refer HIX-97407
		 if(enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE) 
				&&enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)
				&& (enrollmentDto.getEhbAmt() ==null || enrollmentDto.getEhbAmt()<=0.0f)
				){
			throw new GIException(EnrollmentConstants.INVALID_SLCSP_AMOUNT);
		}*/
		if (isNotNullAndEmpty(enrollmentDto.getEhbAmt()) && EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())) {
			if(!EnrollmentUtils.isFloatEqual(enrollmentDto.getEhbAmt(), enrollment.getSlcspAmt()) && amountEffectiveDate!=null){
				enrollment.setSlcspEffDate(EnrollmentUtils.getNextMonthStartDate(amountEffectiveDate));
			}
			enrollment.setSlcspAmt(enrollmentDto.getEhbAmt());

		}

		
		
		if (isNotNullAndEmpty(enrollmentDto.getGrossPremiumAmt())) {
			if(!EnrollmentUtils.isFloatEqual(enrollmentDto.getGrossPremiumAmt(), enrollment.getGrossPremiumAmt()) && amountEffectiveDate!=null ) {
				enrollment.setGrossPremEffDate(amountEffectiveDate);
				enrollment.setNetPremEffDate(amountEffectiveDate);
			}
			enrollment.setGrossPremiumAmt(enrollmentDto.getGrossPremiumAmt());
		}
		if (isNotNullAndEmpty(enrollmentDto.getNetPremiumAmt())) {
			if(!EnrollmentUtils.isFloatEqual(enrollmentDto.getNetPremiumAmt(),enrollment.getNetPremiumAmt()) && amountEffectiveDate!=null ){
				enrollment.setNetPremEffDate(amountEffectiveDate);
			}
			enrollment.setNetPremiumAmt(enrollmentDto.getNetPremiumAmt());	
		}
		
		if(enrollmentDto.getFinancialEffectiveDate()!=null){
			if(enrollmentDto.getAptcAmt()!=null){
				enrollment.setAptcAmt(enrollmentDto.getAptcAmt());
				enrollment.setNetPremiumAmt(enrollment.getGrossPremiumAmt()-enrollment.getNetPremiumAmt());
			}else{
				enrollment.setAptcAmt(null);
				enrollment.setNetPremiumAmt(enrollment.getGrossPremiumAmt());
			}
			enrollment.setNetPremEffDate(enrollmentDto.getFinancialEffectiveDate());
			enrollment.setAptcEffDate(enrollmentDto.getFinancialEffectiveDate());
		}

		if(enrollment.getPlanId()!=null){				
		PlanResponse planResponse = enrollmentExternalRestUtil.getPlanInfo(enrollment.getPlanId().toString(),enrollment.getEnrollmentTypeLkp().getLookupValueCode());
		if(planResponse == null || (planResponse.getStatus() == null || planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))){
			LOGGER.info("No Plan data found " );
			throw new GIException(EnrollmentConstants.ERROR_CODE_301,"No Plan data found. Plan mgmt REST services returns null ",EnrollmentConstants.HIGH);
		}

		if(isNotNullAndEmpty(enrollment.getInsuranceTypeLkp()) && enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL)){
			enrollment.setCsrAmt(null);

		}// HIX-35797
		else if(enrollment.getBenefitEffectiveDate() != null && !EnrollmentConstants.CSR_1.equalsIgnoreCase(enrollmentDto.getCsrLevel())){
			Date jan2015CoverageStartDate = DateUtil.StringToDate(EnrollmentConstants.JAN_2015_DATE, GhixConstants.REQUIRED_DATE_FORMAT);
			// HIX-35797
			// Calculate CSR using multiplier*grossPremiumAmount if effective date is after 1 JAN 2015
			if(enrollment.getBenefitEffectiveDate().compareTo(jan2015CoverageStartDate) >= EnrollmentConstants.ZERO){
				if(isNotNullAndEmpty(enrollment.getGrossPremiumAmt()) && isNotNullAndEmpty(planResponse.getCsrAmount()) && enrollment.getGrossPremiumAmt()>0.0f &&  planResponse.getCsrAmount()>0.0f){
					Float csrAmt=EnrollmentUtils.precision((enrollment.getGrossPremiumAmt() * planResponse.getCsrAmount()), 2);
					if(!EnrollmentUtils.isFloatEqual(csrAmt, enrollment.getCsrAmt())){
						enrollment.setCsrAmt(csrAmt);
						if( amountEffectiveDate!=null){
							enrollment.setCsrEffDate(amountEffectiveDate);
						}
					}
				}
				else{
					enrollment.setCsrAmt(null);
				}
			}
			else {
				if(isNotNullAndEmpty(planResponse.getCsrAmount())&&!EnrollmentUtils.isFloatEqual(EnrollmentUtils.precision(planResponse.getCsrAmount(),2), enrollment.getCsrAmt())){
					if(amountEffectiveDate != null){
						enrollment.setCsrEffDate(amountEffectiveDate);
					}
					enrollment.setCsrAmt(planResponse.getCsrAmount());
				}

				else{
					enrollment.setCsrAmt(null);
				}
			}
		}
		else{
			enrollment.setCsrAmt(null);
		}
		
		//Plan planObj = new Plan();
    	//planObj.setId(planResponse.getPlanId());
    	//planObj.setInsuranceType(enrollmentDto.getInsuranceTypeStr().toLowerCase());
    	//enrollment.setPlan(planObj);
		if(isNotNullAndEmpty(planResponse.getEhbPercentage()) && isNotNullAndEmpty(enrollment.getGrossPremiumAmt())){
			Float percentOfEssEhbAmt=Float.parseFloat(planResponse.getEhbPercentage());
			if(percentOfEssEhbAmt==null || percentOfEssEhbAmt==0.0f){
				throw new GIException(EnrollmentConstants.INVALID_EHB_PERCENT);
			}
			enrollment.setEhbAmt(percentOfEssEhbAmt*enrollment.getGrossPremiumAmt());
			if(amountEffectiveDate != null){
				enrollment.setEhbEffDate(amountEffectiveDate);
			}
			
			enrollment.setEhbPercent(percentOfEssEhbAmt);
		}
		if(isNotNullAndEmpty(planResponse.getStateEhbMandatePercentage()) && isNotNullAndEmpty(enrollment.getGrossPremiumAmt())){
			Float percentOfStateEhbAmt=Float.parseFloat(planResponse.getStateEhbMandatePercentage());
			enrollment.setStateEhbAmt(percentOfStateEhbAmt*enrollment.getGrossPremiumAmt());
			if(amountEffectiveDate != null){
				enrollment.setStateEhbEffDate(amountEffectiveDate);
			}
			enrollment.setStateEhbPercent(percentOfStateEhbAmt);
		}
		if(isNotNullAndEmpty(planResponse.getPediatricDentalComponent()) && enrollment.getInsuranceTypeLkp() != null && enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_INSURANCE_TYPE_DENTAL_CODE)){
			Float percentOfDentalEhbAmt = Float.parseFloat(planResponse.getPediatricDentalComponent().replace("$", ""));
			if(amountEffectiveDate != null){
				enrollment.setDntlEhbEffDate(amountEffectiveDate);
			}
			enrollment.setDntlEssentialHealthBenefitPrmDollarVal(percentOfDentalEhbAmt);
			//HIX-85422 2017 EHB portion for SADPs is a percentage of total premium and not a dollar amount
			if(planResponse.getPlanYear() >= 2017 && enrollment.getEnrollmentTypeLkp() != null 
					&& enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
				if(percentOfDentalEhbAmt<0 || percentOfDentalEhbAmt>1){
					throw new GIException(EnrollmentConstants.ERROR_CODE_301,"percentOfDentalEhbAmt comming from PlanMgmgt response is not valid ",EnrollmentConstants.HIGH);
				}
				enrollment.setDntlEhbAmt(percentOfDentalEhbAmt * enrollment.getGrossPremiumAmt());
			}else{
				enrollment.setDntlEhbAmt(percentOfDentalEhbAmt * getEnrolleeCountForDntlEhb(enrollment.getActiveEnrolleesForEnrollment()));
			}
		}

	}


		// This is Updated in case of Special Enrollment. Hence Update this in enrollment Table.
		if(isNotNullAndEmpty(enrollmentDto.getSsapApplicationid()) &&
				(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL.equalsIgnoreCase(enrollment.getEnrollmentTypeLkp().getLookupValueCode()))){
			enrollment.setPriorSsapApplicationid(enrollmentDto.getSsapApplicationid());
			enrollment.setSsapApplicationid(enrollmentDto.getSsapApplicationid());
		}

		enrollment.setUpdatedBy(loggedInUser);
		enrollment.setUpdatedOn(new TSDate());

	}
	
	public List<Enrollment> updateDentalAPTCOnHealthDisEnrollment(Enrollment healthEnrollment, AccountUser loggedInUser) throws GIException {
		List<Enrollment> dentalEnrollments = null;
		try
		{
		if (healthEnrollment != null) {
			Date effDate =null; 
			if(DateUtils.isSameDay(healthEnrollment.getBenefitEndDate(), healthEnrollment.getBenefitEffectiveDate())){
				effDate= healthEnrollment.getBenefitEffectiveDate();
			}else{
				effDate=EnrollmentUtils.getNextMonthStartDate((healthEnrollment.getBenefitEndDate()));
			}
				dentalEnrollments = enrollmentRepository.findIndividualDentalEnrollment(healthEnrollment.getHouseHoldCaseId(), effDate);
			if (dentalEnrollments != null && dentalEnrollments.size() > 0) {
					LookupValue eventTypeLookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE, EnrollmentConstants.EVENT_TYPE_CHANGE);
				LookupValue eventTypeReasonLookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, EnrollmentConstants.EVENT_REASON_LOOKUP_NO_REASON);
					for (Enrollment enr : dentalEnrollments) 
					{
					enr.setAptcAmt(null);
					enr.setAptcEffDate(effDate);
					enr.setNetPremiumAmt(enr.getGrossPremiumAmt());
					enr.setNetPremEffDate(effDate);
						Enrollee subscriber = enr.getSubscriberForEnrollment();
						if (subscriber != null && !subscriber.isEventCreated()) {
						EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
						enrollmentEvent.setEnrollee(subscriber);
						enrollmentEvent.setEnrollment(enr);
						enrollmentEvent.setEventTypeLkp(eventTypeLookupValue);
						enrollmentEvent.setEventReasonLkp(eventTypeReasonLookupValue);
						enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.APTC_CH_HLT_DISENROLL.toString());
							// Set Created User
						enrollmentEvent.setCreatedBy(loggedInUser);
						enrollmentEvent.setUpdatedBy(loggedInUser);
						subscriber.setLastEventId(enrollmentEvent);
						subscriber.setEventCreated(true);
					}
					enr.setAPTCUpdated(true);
					enrollmentRequotingService.updateMonthlyPremiumsForAPTCSliderChange(enr);
				}
			}
		}
		} catch (Exception e) {
			LOGGER.error("Exception occurred in updating Dental APTC, updateDentalAPTCOnHealthDisEnrollment(-,-): ",e);
			throw new GIException("Failed to nullify dental APTC on Health Disenrollment of enrollment id :: "
					+ healthEnrollment.getId() + " :: " + e.getMessage());
		}
		return dentalEnrollments;
	}
	
private void createEnrolleeRelationship(List<EnrolleeCoreDto> enrolleeDtoList, List<Enrollee> enrolleeList){
		
		/**
		 * Sorting new Enrollee List to add target relationship
		 */
		Collections.sort(enrolleeList, new Comparator<Enrollee>() {
			@Override
			public int compare(Enrollee enrollee1, Enrollee enrollee2) {
				return (-((Date)enrollee1.getCreatedOn()).compareTo(((Date)enrollee2.getCreatedOn())));
			}
		});
		for (EnrolleeCoreDto enrolleeDto : enrolleeDtoList) {
			List<Enrollee> sourceEnrollees = findEnrolleesByMemberId(enrolleeDto.getExchgIndivIdentifier(),enrolleeList);
			if(sourceEnrollees!=null && sourceEnrollees.size()>0){
				for(Enrollee sourceEnrollee: sourceEnrollees){
					List<Map<String,String>> relationshipList  = enrolleeDto.getRelationshipList();
					if(sourceEnrollee!=null && relationshipList!=null && !relationshipList.isEmpty()){
						
						List<EnrolleeRelationship> enrolleeRelationshipList=null;
						if(sourceEnrollee.getEnrolleeRelationship()!=null){
							enrolleeRelationshipList=sourceEnrollee.getEnrolleeRelationship();
						}else{
							enrolleeRelationshipList = new ArrayList<EnrolleeRelationship>();
						}
						//List<EnrolleeRelationship> enrolleeRelationshipList = new ArrayList<EnrolleeRelationship>();
						for (Map<String,String> relationship:relationshipList){
							List<Enrollee> targetEnrollees = findEnrolleesByMemberId(relationship.get(EnrollmentConstants.MEMBER_ID),enrolleeList );
							if(targetEnrollees!=null && targetEnrollees.size()>0){
								for(Enrollee targetEnrollee: targetEnrollees){
									if(targetEnrollee!=null){
										EnrolleeRelationship tmpEnrolleeRelationship = findRelationFromList(enrolleeRelationshipList,sourceEnrollee, targetEnrollee);
										if(tmpEnrolleeRelationship==null){
											tmpEnrolleeRelationship = new EnrolleeRelationship();
											tmpEnrolleeRelationship.setSourceEnrollee(sourceEnrollee);
											tmpEnrolleeRelationship.setTargetEnrollee(targetEnrollee);
											enrolleeRelationshipList.add(tmpEnrolleeRelationship);
										}
										tmpEnrolleeRelationship.setRelationshipLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.RELATIONSHIP, relationship.get(EnrollmentConstants.RELATIONSHIP_CODE)));
									}
								}
							}
						}
						sourceEnrollee.setEnrolleeRelationship( enrolleeRelationshipList);
					}
				}
			}
			
		}

	}



@Override
@Transactional
public void terminateNewlyAddedmembers(EnrollmentResponse response,  EnrollmentEvent.TRANSACTION_IDENTIFIER txn_idfier) throws GIException{
	List<Map<String, String>> newMemberMapList= response.getMemberAddedToEnrollment();
	AccountUser loggedInUser = this.getLoggedInUser();
	String enrollmentType="";
	if(newMemberMapList!=null && !newMemberMapList.isEmpty()){
		Map<Integer, Enrollment> enrollmentListMap = new HashMap<Integer, Enrollment>();
		Set<String> disEnrolledMemberIds=new HashSet<String>();
		RestEmployeeDetailsDTO restEmployeeDetailsDTO = new RestEmployeeDetailsDTO();
		boolean termFlag;
		for(Map<String, String> newMemberMap : newMemberMapList){
			try{
			termFlag=false;
			if(newMemberMap.get(EnrollmentConstants.TERM_FLAG)!=null){
				termFlag= Boolean.valueOf(newMemberMap.get(EnrollmentConstants.TERM_FLAG));
			}
			if(!termFlag) {
				continue;
			}
			Enrollment enrollment= enrollmentRepository.findById(Integer.parseInt(newMemberMap.get(EnrollmentConstants.ENROLLMENT_ID)));
			/*if(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode()) && !response.isTerminationFlag()){
				continue;
			}
			if(EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode()) && !response.isDentalTerminationFlag()){
				continue;
			}*/
			Enrollee enrollee=findEnrolleeByMemberId(newMemberMap.get(EnrollmentConstants.MEMBER_ID), enrollment.getEnrollees());
			if(isNotNullAndEmpty(enrollment.getEmployeeId())){
				restEmployeeDetailsDTO.setEmployeeId(Integer.valueOf(enrollment.getEmployeeId()));
			}
			if(isNotNullAndEmpty(enrollment.getEmployeeAppId())){
				restEmployeeDetailsDTO.setEmployeeApplicationId(enrollment.getEmployeeAppId().intValue());
			}
			enrollmentType=enrollment.getEnrollmentTypeLkp().getLookupValueCode();
			enrollee.setEnrolleeLkpValue(enrollment.getEnrollmentStatusLkp());
			if(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) || enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
				enrollee.setDisenrollTimestamp(new TSDate());
			}
			enrollee.setEffectiveEndDate(enrollment.getBenefitEndDate());
			enrollee.setTerminationFlag(EnrollmentConstants.Y);
			EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
			if(txn_idfier!=null){
				enrollmentEvent.setTxnIdentifier(txn_idfier.toString());
			}
			enrollmentEvent.setEnrollee(enrollee);
			enrollmentEvent.setEnrollment(enrollee.getEnrollment());
			enrollmentEvent.setCreatedBy(loggedInUser);
			enrollmentEvent.setUpdatedBy(loggedInUser);
			enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,EnrollmentConstants.EVENT_TYPE_CANCELLATION));
			//add null check
			if(isNotNullAndEmpty(newMemberMap.get(EnrollmentConstants.MAINTENANCE_REASON_CODE))){
					if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  newMemberMap.get(EnrollmentConstants.MAINTENANCE_REASON_CODE)))){
						enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  newMemberMap.get(EnrollmentConstants.MAINTENANCE_REASON_CODE)));
					}else{
						throw new RuntimeException(EnrollmentConstants.ERR_MSG_LOOKUP_VALUE_IS_NULL);
					}
			}else{
				throw new GIException("Maintanance Reason code  is null or empty");
			}
			
			if(!isNotNullAndEmpty(newMemberMap.get(EnrollmentConstants.SERC))){
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,  newMemberMap.get(EnrollmentConstants.MAINTENANCE_REASON_CODE)));
			}
			else if(isNotNullAndEmpty(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, newMemberMap.get(EnrollmentConstants.SERC)))){
				enrollmentEvent.setSpclEnrollmentReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.SPCL_EVENT_REASON, newMemberMap.get(EnrollmentConstants.SERC)));
			}else{
				throw new RuntimeException("Lookup value is null for serc : " + newMemberMap.get(EnrollmentConstants.SERC));
			}
			List<EnrollmentEvent> events=enrollee.getEnrollmentEvents();
			if(events==null){
				events=new ArrayList<>();
			}
			events.add(enrollmentEvent);
			enrollee.setEnrollmentEvents(events);
			enrollee.setLastEventId(enrollmentEvent);
			if(enrollmentListMap.isEmpty()){
				List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
				enrolleeList.add(enrollee);
				enrollment.setEnrollees(enrolleeList);
				enrollmentListMap.put(enrollment.getId(), enrollment);
			}
			else if(!enrollmentListMap.isEmpty()){
				if(enrollmentListMap.containsKey(enrollment.getId())){
					enrollmentListMap.get(enrollment.getId()).getEnrollees().add(enrollee);
				}
				else{
					List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
					enrolleeList.add(enrollee);
					enrollment.setEnrollees(enrolleeList);
					enrollmentListMap.put(enrollment.getId(), enrollment);
				}
			}
			disEnrolledMemberIds.add(newMemberMap.get(EnrollmentConstants.MEMBER_ID));
			}catch(Exception e){
				LOGGER.error("Error in terminateNewlyAddedmembers :: Error while terminating newly added member "+newMemberMap.get(EnrollmentConstants.MEMBER_ID)+" and Enrollment ID ="+ newMemberMap.get(EnrollmentConstants.ENROLLMENT_ID) , e);
			}
		}
		/*
		 * Creating Subscriber change Event for new member termination loop 
		 */
		if(enrollmentListMap != null && !enrollmentListMap.isEmpty()){
			for(Integer enrollmentId : enrollmentListMap.keySet()){
				Enrollee subscriberEnrollee = enrolleeRepository.findSubscriberByEnrollmentID(enrollmentId);
				Enrollment enrollment = enrollmentListMap.get(enrollmentId);
				EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
				enrollmentEvent.setEnrollee(subscriberEnrollee);
				enrollmentEvent.setEnrollment(enrollment);
				enrollmentEvent.setCreatedBy(loggedInUser);
				enrollmentEvent.setUpdatedBy(loggedInUser);
				if(txn_idfier!=null){
					if(txn_idfier.toString().contains("AUTOMATE")){
						enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTOMATE_SPECIAL.toString());
					}else{
						enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.SPECIAL_ENROLLMENT.toString());
					}
				}
				enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,EnrollmentConstants.EVENT_TYPE_CHANGE));
				enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON,EnrollmentConstants.EVENT_REASON_LOOKUP_NO_REASON));
				
				List<EnrollmentEvent> events=subscriberEnrollee.getEnrollmentEvents();
				if(events==null){
					events=new ArrayList<>();
				}
				events.add(enrollmentEvent);
				subscriberEnrollee.setEnrollmentEvents(events);
				subscriberEnrollee.setLastEventId(enrollmentEvent);
				enrollment.getEnrollees().add(subscriberEnrollee);
				
				enrollmentRepository.saveAndFlush(enrollment);
			}
		}
		
		if(EnrollmentConstants.ENROLLMENT_TYPE_SHOP.equalsIgnoreCase(enrollmentType)){
			try{
				List<Integer> disEnrolleedMembers= new ArrayList<Integer>();
				disEnrolleedMembers.addAll(disEnrolledMemberIds.stream().map(e->Integer.parseInt(e)).collect(Collectors.toList()));
				LOGGER.info("===== Sending Request to Send to GHIX-SHOP for Employer contributions =====");
				restEmployeeDetailsDTO.setMemberIds(disEnrolleedMembers);
				String restEmployeeDetailsResponse = null; 
				restEmployeeDetailsResponse = restTemplate.postForObject(ShopEndPoints.DELETE_MEMBER_DETAILS, restEmployeeDetailsDTO, String.class);
				XStream xStream = GhixUtils.getXStreamStaxObject(); 
				ShopResponse shopResponse = (ShopResponse) xStream.fromXML(restEmployeeDetailsResponse);
				/***** If Response is Not Success *****/
				if (shopResponse != null && shopResponse.getErrCode() != 0 ) {
					LOGGER.error(EnrollmentConstants.ERR_MSG_SHOP_DISENROLLMENT_FAILED + disEnrolledMemberIds);
					throw new GIException(EnrollmentConstants.ERR_MSG_SHOP_DISENROLLMENT_FAILED + disEnrolledMemberIds);
				}
			}
			catch (Exception e){
				LOGGER.error(EnrollmentConstants.ERR_MSG_SHOP_DISENROLLMENT_FAILED + disEnrolledMemberIds , e);
			}
		}		
	}
}
	
	/**
	 * 
	 * @return
	 */
	private AccountUser getLoggedInUser() {
		AccountUser accountUser = null;
		try {
			accountUser = userService.getLoggedInUser();
			if (accountUser != null) {
				int id = accountUser.getId();
				if (id > 0) {
					accountUser = this.userService.findById(id);
					LOGGER.info("Logged in user id : " + accountUser.getId());
				} else {
					accountUser = this.userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
					LOGGER.info("Logged in user not found made exchange user as login user: " + accountUser.getId());
				}
			}
		} catch (InvalidUserException invalidUserException) {
			LOGGER.error("Error occured in EnrollmentServiceImpl.getLoggedInUser", invalidUserException);
		}
		return accountUser;
	}
	
	
	private void validateAutomateFinancialDate(EnrollmentCoreDto enrollmentDto,Enrollment enrollment){
		
		if(EnrollmentConfiguration.isIdCall() && enrollmentDto.getFinancialEffectiveDate()!=null){
			String validationFailureDesc=null;
			if(enrollmentDto.getGrossPremiumAmt()!=null && !EnrollmentUtils.isFloatEqual(enrollmentDto.getGrossPremiumAmt(), enrollment.getGrossPremiumAmt())){
				validationFailureDesc="Automated Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_GROSS_CHANGE + " for Enrollment Id :: "+ enrollment.getId() +" for financial Effective Date"+ enrollmentDto.getFinancialEffectiveDate() ;
				enrollmentDto.setFinancialEffectiveDate(null);
			}else if(enrollment.getBenefitEndDate().before(DateUtils.truncate(enrollmentDto.getFinancialEffectiveDate(), Calendar.DAY_OF_MONTH))||
					!EnrollmentUtils.isSameYear(enrollment.getBenefitEffectiveDate(), enrollmentDto.getFinancialEffectiveDate())
					){
				validationFailureDesc="Automated Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_OUT_COV + " for Enrollment Id :: "+ enrollment.getId() +" for financial Effective Date"+ enrollmentDto.getFinancialEffectiveDate() ;
				enrollmentDto.setFinancialEffectiveDate(null);
			}else if(enrollmentDto.getFinancialEffectiveDate().before(enrollment.getBenefitEffectiveDate())){
				enrollmentDto.setFinancialEffectiveDate(enrollment.getBenefitEffectiveDate());
			}else if(DateUtil.getDateOfMonth(enrollmentDto.getFinancialEffectiveDate())!=1 && DateUtil.getDateOfMonth(enrollment.getBenefitEffectiveDate())==1){
				validationFailureDesc="Automated Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_APTC_MID_MONTH + " for Enrollment Id :: "+ enrollment.getId() +" for financial Effective Date"+ enrollmentDto.getFinancialEffectiveDate() ;
				enrollmentDto.setFinancialEffectiveDate(null);
			}
			
			if(EnrollmentUtils.isNotNullAndEmpty(validationFailureDesc)){
				String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
				enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.FINANCIALEFFDATE, "Financial Effective Date Validation Failure", validationFailureDesc);
			}
		}
	}
	
	private PlanResponse getPlanInfo(PlanListResponse planListResponse, String planId ){
		PlanResponse planResponse=null;
		if(planListResponse!=null&& planListResponse.getPlanList()!=null && isNotNullAndEmpty(planId)){
			Optional<PlanResponse> resp= planListResponse.getPlanList().stream().filter(r->((""+r.getPlanId()).equalsIgnoreCase(planId))).findFirst();
			if(resp.isPresent()){
				planResponse=resp.get();
			}
		}
		return planResponse;
	}
		
/*	private Date validateIdahoFinancialEffDate(Enrollment enrollment, PldPlanData pldPlanData,  PldOrderResponse pldOrderResponse) {
		Date financialEffectiveDate = pldOrderResponse.getFinancialEffectiveDate();
		String validationFailureDesc = null;
		if(EnrollmentConfiguration.isIdCall() && financialEffectiveDate!=null){
			if(!EnrollmentUtils.isFloatEqual(pldPlanData.getPlan().get(EnrollmentConstants.GROSS_PREMIUM_AMT), enrollment.getGrossPremiumAmt())){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_GROSS_CHANGE + " for Enrollment Id :: "+ enrollment.getId() +" for financial Effective Date"+ financialEffectiveDate ;
				financialEffectiveDate = null;
			}else if(enrollment.getBenefitEndDate().before(DateUtils.truncate(financialEffectiveDate, Calendar.DAY_OF_MONTH))||
					!EnrollmentUtils.isSameYear(enrollment.getBenefitEffectiveDate(), financialEffectiveDate)
					){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_OUT_COV + " for Enrollment Id :: "+ enrollment.getId() +" for financial Effective Date"+ financialEffectiveDate ;
				financialEffectiveDate = null;
			}else if(financialEffectiveDate.before(enrollment.getBenefitEffectiveDate())){
				financialEffectiveDate = enrollment.getBenefitEffectiveDate();
			}else if(DateUtil.getDateOfMonth(financialEffectiveDate)!=1 && DateUtil.getDateOfMonth(enrollment.getBenefitEffectiveDate())==1){
				validationFailureDesc="Special Enrollment Financial Effective Date Validation Failure :: "+EnrollmentConstants.ERR_MSG_DATE_APTC_MID_MONTH + " for Enrollment Id :: "+ enrollment.getId() +" for financial Effective Date"+ financialEffectiveDate ;
				financialEffectiveDate = null;
			}
			if(EnrollmentUtils.isNotNullAndEmpty(validationFailureDesc)){
				String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
				enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.FINANCIALEFFDATE, "Financial Effective Date Validation Failure", validationFailureDesc);
			}
		}
		return financialEffectiveDate;
	}*/
	
	/**
	 * Check for overlap within the custom grouping request
	 * @param enrollmentList
	 * @return
	 */
	private boolean checkInternalOverlap(List<Enrollment> enrollmentList) {
		Map<String, List<Enrollee>> memberMap = new HashMap<>();
		for (Enrollment enrollment : enrollmentList) {
			List<Enrollee> activeEnrollees =  enrollment.getEnrolleesAndSubscriber().stream()
					.filter(m -> !"CANCEL".equalsIgnoreCase(m.getEnrolleeLkpValue().getLookupValueCode()))
					.collect(Collectors.toList());
			for (Enrollee ee : activeEnrollees) {
				if (memberMap.containsKey(ee.getExchgIndivIdentifier())) {
					List<Enrollee> members = memberMap.get(ee.getExchgIndivIdentifier());
					if (members.stream().parallel()
							.anyMatch(member -> EnrollmentUtils.isDateInBetweenStartDateAndEndDate(
									member.getEffectiveStartDate(), member.getEffectiveEndDate(),
									ee.getEffectiveStartDate())
									|| EnrollmentUtils.isDateInBetweenStartDateAndEndDate(
											member.getEffectiveStartDate(), member.getEffectiveEndDate(),
											ee.getEffectiveEndDate()))) {
						LOGGER.error("Overlap exixts between member: "+ ee.getExchgIndivIdentifier());
						return true;
					}
					memberMap.get(ee.getExchgIndivIdentifier()).add(ee);
				} else {
					memberMap.put(ee.getExchgIndivIdentifier(), new ArrayList<Enrollee>(Arrays.asList(ee)));
				}
			}
		}
		return false;
	}
	
	/**
	 * HIX-117679 Get benchmark premium value for the enrollment
	 * @param enrollment
	 * @param subscriber
	 * @param amountEffectiveDate 
	 * @throws Exception
	 */
	private void getSLCSPAmount(Enrollment enrollment, Enrollee subscriber, Date amountEffectiveDate) throws Exception {
		
		if (enrollment != null && 
				subscriber != null && 
				enrollment.getEnrollmentTypeLkp() != null && 
				enrollment.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)) {
			
			PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest();
			Map<String, Object> cRequestParams = new HashMap<>();
			List<Map<String, String>> censusData = new ArrayList<Map<String, String>>();
			List<Enrollee> enrolleeList = enrollment.getEnrolleesAndSubscriber();
			Location loc = subscriber.getHomeAddressid();
			for (Enrollee enrollee : enrolleeList) {
				Map<String, String> memberData = new HashMap<>();
				if (enrollee.getBirthDate() != null) {
					memberData.put("dob",
							DateUtil.dateToString(enrollee.getBirthDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
				}
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
						EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGE_BASED_REQUOTING))) {//HIX-105931
					if(enrollee.getAge() != null) {
						memberData.put("age", ""+enrollee.getAge());
					}
				}
				
				if (loc != null) {
					memberData.put("zip", loc.getZip());
					memberData.put("countycode", loc.getCountycode());
				}
				if (enrollee.getTobaccoUsageLkp() != null
						&& enrollee.getTobaccoUsageLkp().getLookupValueCode().equalsIgnoreCase("T")) {
					memberData.put("tobacco", "y");
				} else {
					memberData.put("tobacco", "n");
				}
				String relation = null;
				if (enrollee.getEnrolleeRelationship() != null && enrollee.getEnrolleeRelationship().size() > 0) {
					for (EnrolleeRelationship er : enrollee.getEnrolleeRelationship()) {
						if (er.getTargetEnrollee() != null && er.getTargetEnrollee().getExchgIndivIdentifier()
								.equalsIgnoreCase(subscriber.getExchgIndivIdentifier())) {
							relation = er.getRelationshipLkp().getLookupValueCode().trim();
							break;
						}
					}
				}
				if (relation != null) {
					memberData.put("relation", relation);
				} else {
					memberData.put("relation", EnrollmentConstants.OTHER_RELATION_CODE);
				}
				memberData.put("id", enrollee.getExchgIndivIdentifier());
				censusData.add(memberData);
			}
			cRequestParams.put("censusData", censusData);
			cRequestParams.put("coverageStartDate", DateUtil.dateToString(enrollment.getGrossPremEffDate(),
					EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
			planRateBenefitRequest.setRequestParameters(cRequestParams);
			Float slcspAmount = null;
			String slcspPlanid = null;

			PlanRateBenefitResponse response = enrollmentExternalRestUtil
					.getBenchmarkPlanBenefitRate(planRateBenefitRequest);

			if (response != null && response.getStatus().equalsIgnoreCase("SUCCESS")) {

				Map<String, Object> responseData = response.getResponseData();

				if (responseData != null && responseData.get("benchMarkPre") != null) {
					slcspAmount = Float.parseFloat((String) responseData.get("benchMarkPre"));
				} else {
					throw new Exception("No EHB Amount returned for enrollment ");
				}

				if (responseData != null && responseData.get("benchMarkHiosPlanId") != null) {
					slcspPlanid = (String) responseData.get("benchMarkHiosPlanId");
				} else {
					throw new Exception("No bench Mark Hios PlanId returned for enrollment ");
				}

			} else {
				throw new Exception("Failure returned by benchmark API");
			}
			enrollment.setSlcspAmt(slcspAmount);
			enrollment.setSlcspPlanid(slcspPlanid);
			if (!EnrollmentUtils.isFloatEqual(slcspAmount, enrollment.getSlcspAmt())
					&& amountEffectiveDate != null) {
				enrollment.setSlcspEffDate(amountEffectiveDate);
			}
		}
	}
	
	private void updateAmountAndEffectiveDatesForSpecialEnrollment(PldOrderResponse pldOrderResponse, Enrollment enrollment,
			PldPlanData pdlPlan, Date amountEffectiveDate, AccountUser loggedInUser, Date financialEffectiveDate,
			Map<String, String> plan, Date sepEffDate, PlanListResponse planListResponse) throws GIException {
		
		boolean useSameEffectiveDate = Boolean.parseBoolean(DynamicPropertiesUtil
				.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.USE_SAME_EFFECTIVE_DATE));
		boolean isAptcApplicable =  isNotNullAndEmpty(enrollment.getAptcAmt()) || isNotNullAndEmpty(plan.get(EnrollmentConstants.APTC));
		boolean isStateSubsidyApplicable = isNotNullAndEmpty(plan.get(EnrollmentConstants.STATE_SUBSIDY)) || isNotNullAndEmpty(enrollment.getStateSubsidyAmt());
		boolean allowMidMonthEffectiveDate = Boolean.parseBoolean(DynamicPropertiesUtil
				.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ALLOW_MID_MONTH_EFFECTIVE_DATE));
		
		if (financialEffectiveDate == null) {
			amountEffectiveDate = sepEffDate;
		} else {
			amountEffectiveDate = financialEffectiveDate;
		}
		
		if (amountEffectiveDate != null && !allowMidMonthEffectiveDate) {
			if (DateUtil.getDateOfMonth(amountEffectiveDate) != 1 ) {
				if(enrollment.getBenefitEndDate() != null  &&
						amountEffectiveDate.getMonth()==enrollment.getBenefitEndDate().getMonth()){
					amountEffectiveDate = EnrollmentUtils.getMonthStartDate(enrollment.getBenefitEndDate());
				}else  {
					amountEffectiveDate = EnrollmentUtils.getNextMonthStartDate(amountEffectiveDate);
				}
				
			} 
		}

		//APTC AMOUNT
		if(isNotNullAndEmpty(plan.get(EnrollmentConstants.MAX_APTC))){
        	enrollment.setMaxAPTCAmt(Float.parseFloat(plan.get(EnrollmentConstants.MAX_APTC)));
        }
		
		if (!EnrollmentUtils.isFloatEqual(plan.get(EnrollmentConstants.APTC), enrollment.getAptcAmt())) {
			enrollment.setAptcEffDate(amountEffectiveDate);
			enrollment.setFinancialEffectiveDate(amountEffectiveDate);
			enrollment.setShowFinancialEffectivedate(true);
		}
		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.APTC))) {
			enrollment.setAptcAmt(Float.parseFloat(plan.get(EnrollmentConstants.APTC)));
		} else {
			enrollment.setAptcAmt(null);
		}

		//STATE SUBSIDY AMOUNT
		if(isNotNullAndEmpty(plan.get(EnrollmentConstants.MAX_STATE_SUBSIDY))){
			enrollment.setMaxStateSubsidyAmt(new BigDecimal(plan.get(EnrollmentConstants.MAX_STATE_SUBSIDY)));
		}

		if(isStateSubsidyApplicable) {
			enrollment.setStateSubsidyEffDate(amountEffectiveDate);
			enrollment.setFinancialEffectiveDate(amountEffectiveDate);
			enrollment.setShowFinancialEffectivedate(true);
			if(isNotNullAndEmpty(enrollment.getStateSubsidyAmt()) && isNotNullAndEmpty(plan.get(EnrollmentConstants.STATE_SUBSIDY))) {
				if (new BigDecimal(plan.get(EnrollmentConstants.STATE_SUBSIDY)).compareTo(enrollment.getStateSubsidyAmt()) != 0) {
					enrollment.setStateSubsidyAmt(new BigDecimal(plan.get(EnrollmentConstants.STATE_SUBSIDY)));
				}
			}
			else if (!isNotNullAndEmpty(enrollment.getStateSubsidyAmt()) && isNotNullAndEmpty(plan.get(EnrollmentConstants.STATE_SUBSIDY))){
				enrollment.setStateSubsidyAmt(new BigDecimal(plan.get(EnrollmentConstants.STATE_SUBSIDY)));
			}
			else{
				enrollment.setStateSubsidyAmt(null);
			}
		}
		else{
			enrollment.setStateSubsidyAmt(null);
		}
	
		//GROSS PREMIUM AMOUNT
		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT))) {
			if (!EnrollmentUtils.isFloatEqual(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT),
					enrollment.getGrossPremiumAmt())) {
				enrollment.setGrossPremEffDate(amountEffectiveDate);
				enrollment.setNetPremEffDate(amountEffectiveDate);
			}
			enrollment.setGrossPremiumAmt(Float.parseFloat(plan.get(EnrollmentConstants.GROSS_PREMIUM_AMT)));
		}
		
		//NET PREMIUM AMOUNT
		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.NET_PREMIUM_AMT))) {
			if (!EnrollmentUtils.isFloatEqual(plan.get(EnrollmentConstants.NET_PREMIUM_AMT),
					enrollment.getNetPremiumAmt())) {
				enrollment.setNetPremEffDate(amountEffectiveDate);
			}
			enrollment.setNetPremiumAmt(Float.parseFloat(plan.get(EnrollmentConstants.NET_PREMIUM_AMT)));
		}
		
		//SLCSP AMOUNT
		if (enrollment.getInsuranceTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE)) {
			if (isNotNullAndEmpty(pldOrderResponse.getEhbAmount())) {

				if ((pldOrderResponse.getEhbAmount() == null || pldOrderResponse.getEhbAmount() <= 0.0f)) {
					String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(
							EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
					enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent,
							EnrollmentConstants.GiErrorCode.INVALID_SLCSP_AMOUNT,
							EnrollmentConstants.INVALID_SLCSP_AMOUNT,
							EnrollmentConstants.EMPTY_OR_ZERO_SLCSP + pldOrderResponse.getApplicationId());
				}

				if (!EnrollmentUtils.isFloatEqual(pldOrderResponse.getEhbAmount(), enrollment.getSlcspAmt())
						&& amountEffectiveDate != null) {
					enrollment.setSlcspEffDate(EnrollmentUtils.getNextMonthStartDate(amountEffectiveDate));
				}
				enrollment.setSlcspAmt(pldOrderResponse.getEhbAmount());

			} else {
				try {
					getSLCSPAmount(enrollment, enrollment.getSubscriberForEnrollment(), amountEffectiveDate);
				} catch (Exception e) {
					LOGGER.error("Exception in getting benchmark premium in special enrollment flow");
					throw new GIException(e);
				}
			}
		}

		//CSR AMOUNT
		PlanResponse planResponse = null;
		if (isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_ID))) {
			if (pdlPlan.getPlanResponse() == null) {
				planResponse = getPlanInfo(planListResponse,plan.get(EnrollmentConstants.PLAN_ID));
			} else {
				planResponse = pdlPlan.getPlanResponse();
			}

			if (planResponse == null || (planResponse.getStatus() == null
					|| planResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))) {
				LOGGER.info("No Plan data found ");
				throw new GIException(EnrollmentConstants.ERROR_CODE_301,
						"No Plan data found. Plan mgmt REST services returns null ", "HIGH");
			}

			Float csrAmt = null;

			boolean populateCSRForZeroAPTC = false;

			populateCSRForZeroAPTC = Boolean.parseBoolean(DynamicPropertiesUtil
					.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.POPULATE_CSR_APTC_ZERO));

			if (isNotNullAndEmpty(plan.get(EnrollmentConstants.PLAN_TYPE)) && plan.get(EnrollmentConstants.PLAN_TYPE)
					.equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL)) {
				enrollment.setCsrAmt(null);

			} else if (enrollment.getBenefitEffectiveDate() != null
					&& (plan.get(EnrollmentConstants.CSR) != null
							&& !plan.get(EnrollmentConstants.CSR).equalsIgnoreCase(EnrollmentConstants.CSR_1))
					&& (populateCSRForZeroAPTC || (enrollment.getAptcAmt() != null && enrollment.getAptcAmt() > 0))) {
				Date jan2015CoverageStartDate = DateUtil.StringToDate(EnrollmentConstants.JAN_2015_DATE,
						GhixConstants.REQUIRED_DATE_FORMAT);
				// HIX-35797 HIX-88110
				// Calculate CSR using multiplier*grossPremiumAmount if
				// effective date is after 1 JAN 2015
				if (enrollment.getBenefitEffectiveDate().compareTo(jan2015CoverageStartDate) > 0
						|| enrollment.getBenefitEffectiveDate().compareTo(jan2015CoverageStartDate) == 0) {
					if (enrollment.getGrossPremiumAmt() != null && enrollment.getGrossPremiumAmt() > 0.0f
							&& planResponse.getCsrAmount() != null && planResponse.getCsrAmount() > 0.0f) {
						csrAmt = EnrollmentUtils
								.precision((enrollment.getGrossPremiumAmt() * planResponse.getCsrAmount()), 2);
						if (enrollment.getCsrAmt() != null && enrollment.getCsrAmt() > 0
								&& !EnrollmentUtils.isFloatEqual(enrollment.getCsrAmt(), csrAmt)
								&& amountEffectiveDate != null) {
							enrollment.setCsrEffDate(amountEffectiveDate);
						}
						enrollment.setCsrAmt(csrAmt);
					} else if (enrollment.getCsrAmt() == null) {
						enrollment.setCsrAmt(null);
					}
				} else {
					if (isNotNullAndEmpty(planResponse.getCsrAmount()) && planResponse.getCsrAmount() > 0.0f) {
						csrAmt = EnrollmentUtils
								.precision((planResponse.getCsrAmount() * enrollment.getActiveEnrolleesCount()), 2);
						if (null != csrAmt && csrAmt > 0
								&& !EnrollmentUtils.isFloatEqual(enrollment.getCsrAmt(), csrAmt)
								&& amountEffectiveDate != null) {
							enrollment.setCsrEffDate(amountEffectiveDate);
						}
						enrollment.setCsrAmt(csrAmt);
					} else if (enrollment.getCsrAmt() == null) {
						enrollment.setCsrAmt(null);
					}
				}
			} else if (enrollment.getCsrAmt() == null) {
				enrollment.setCsrAmt(null);
			}
		} else {
			throw new GIException("No Plans found");
		}
		
		//HIX-118147 Setting effective date to all if use same effective date configuration is true
		if(useSameEffectiveDate) {
			if(isAptcApplicable) { //Check to avoid setting APTC effective date in case of NF application
				enrollment.setAptcEffDate(amountEffectiveDate);
				enrollment.setFinancialEffectiveDate(amountEffectiveDate);
				enrollment.setShowFinancialEffectivedate(true);
			}
			if(isStateSubsidyApplicable) { //Check to avoid setting state subsidy effective date in case of non state subsidized applications
				enrollment.setStateSubsidyEffDate(amountEffectiveDate);
			}
			enrollment.setSlcspEffDate(amountEffectiveDate);
			enrollment.setGrossPremEffDate(amountEffectiveDate);
			enrollment.setNetPremEffDate(amountEffectiveDate);
			enrollment.setStateSubsidyEffDate(amountEffectiveDate);
			if (enrollment.getCsrAmt() != null) {
				enrollment.setCsrEffDate(amountEffectiveDate);
			}
		}
	}

}
