/**
 * 
 */
package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrollmentPremium;

/**
 * Enrollment Premium table repository
 * @author negi_s
 * @since 06/20/2016
 *
 */
public interface IEnrollmentPremiumRepository extends JpaRepository<EnrollmentPremium, Integer> {
	
	@Query("FROM EnrollmentPremium as ep WHERE ep.enrollment.id = :enrollmentId")
	List<EnrollmentPremium> findByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
	
	@Query(" FROM EnrollmentPremium as ep "
		  +" WHERE ep.enrollment.id = :enrollmentId "
		  +" AND ep.month = :month "
		  +" AND ep.year = :year")
	List<EnrollmentPremium> findByEnrollmentIdMonthAndYear(@Param("enrollmentId") Integer enrollmentId, @Param("month") Integer month, @Param("year") Integer year);
	
    @Query(" FROM EnrollmentPremium as ep "
		  +" WHERE ep.enrollment.id = :enrollmentId "
		  +" AND ep.grossPremiumAmount IS NOT NULL "
		  +" ORDER BY ep.month ASC ")
	List<EnrollmentPremium> findNotNullPremiumByEnrollmentId(@Param("enrollmentId") Integer enrollmentId);
    
    
    @Query("SELECT DISTINCT ep.enrollment.id from EnrollmentPremium as ep " +
			" WHERE TO_DATE(TO_CHAR(ep.enrollment.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') >= TO_DATE(:currentYearStartDate, 'MM/DD/YYYY') "+
			" AND TO_DATE(TO_CHAR(ep.enrollment.benefitEffectiveDate,'MM/DD/YYYY'),'MM/DD/YYYY') < TO_DATE(:nextYearStartDate, 'MM/DD/YYYY') "+
			" AND ep.enrollment.enrollmentStatusLkp.lookupValueCode IN ('CONFIRM','TERM','PENDING','PAYMENT_RECEIVED') " +
			" AND ep.enrollment.insuranceTypeLkp.lookupValueCode='HLT'"+
		    " AND (ep.slcspPremiumAmount IS NULL OR ep.slcspPremiumAmount<=0 )"+
			" AND ep.year=:year"+
			" AND ep.month>=cast(to_char( ep.enrollment.benefitEffectiveDate,'MM') as integer)"+
		    " AND ep.month<=cast(to_char( ep.enrollment.benefitEndDate, 'MM') as integer)"
    		)
	List<Integer> getEmptySLCSPEnrollmentIdsForYear(@Param("currentYearStartDate") String currentYearStartDateString, @Param("nextYearStartDate") String nextYearStartDateString, @Param("year") Integer year);

    
}
