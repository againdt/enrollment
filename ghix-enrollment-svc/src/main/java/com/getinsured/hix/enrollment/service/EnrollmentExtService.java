/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.dto.enrollment.EnrollmentExtDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentExtResponseDTO;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Enrollment ACA Express service
 * @author negi_s
 * @since 12/09/2017
 */
public interface EnrollmentExtService {

	/**
	 * Creates enrollment based on the request
	 * @param acaRequest EnrollmentExtDTO request DTO
	 * @param giWsPayloadId Integer
	 * @return enrollment response
	 */
	EnrollmentExtResponseDTO createAcaEnrollment(EnrollmentExtDTO acaRequest, Integer giWsPayloadId) throws GIException;
}
