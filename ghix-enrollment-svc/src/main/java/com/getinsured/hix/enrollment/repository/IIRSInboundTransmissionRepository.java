/**
 * 
 */
package com.getinsured.hix.enrollment.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.IRSInboundTransmission;


/**
 * @author Priya C
 *
 */
@Repository
@Transactional
public interface IIRSInboundTransmissionRepository extends JpaRepository<IRSInboundTransmission, Integer>{
	@Query("FROM IRSInboundTransmission as irs where irs.batchId =:batchId")
	List<IRSInboundTransmission> findIRSInboundTransmissionByBatchId(@Param("batchId") String batchId);

	
	

}
