package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.EnrlReconSnapshot;

public interface IEnrlReconSnapshotRepository extends JpaRepository<EnrlReconSnapshot, Integer> {
	
	EnrlReconSnapshot findById(Integer enrlReconSnapshotId);
	
	@Query("SELECT distinct enrlReconSnapshot.id FROM EnrlReconSnapshot enrlReconSnapshot  where enrlReconSnapshot.fileId = :fileId ")
	List<Integer> findIdByFileId(@Param("fileId") Integer fileId);
	
	@Query("SELECT distinct enrlReconSnapshot.id FROM EnrlReconSnapshot enrlReconSnapshot  where enrlReconSnapshot.fileId = :fileId and enrlReconSnapshot.comparisonStatus IS NULL")
	List<Integer> findIdByFileIdAndEmptyStatus(@Param("fileId") Integer fileId);

	@Query("SELECT enrlReconSnapshot.issuerData FROM EnrlReconSnapshot enrlReconSnapshot where enrlReconSnapshot.fileId = :fileId "
			+ "AND enrlReconSnapshot.hixEnrollmentId = :hixEnrollmentId "
			+ "AND enrlReconSnapshot.creationTimestamp = "
			+ "(SELECT MAX(creationTimestamp) FROM EnrlReconSnapshot where fileId = :fileId AND hixEnrollmentId = :hixEnrollmentId)")
	String findIssuerDataByFileIdAndEnrollmentId(@Param("fileId") Integer fileId, @Param("hixEnrollmentId") Long hixEnrollmentId);
	
	@Query("FROM EnrlReconSnapshot enrlReconSnapshot where enrlReconSnapshot.fileId = :fileId "
			+ "AND enrlReconSnapshot.hixEnrollmentId = :hixEnrollmentId "
			+ "AND enrlReconSnapshot.creationTimestamp = "
			+ "(SELECT MAX(creationTimestamp) FROM EnrlReconSnapshot where fileId = :fileId AND hixEnrollmentId = :hixEnrollmentId)")
	EnrlReconSnapshot findSnapshotDataByFileIdAndEnrollmentId(@Param("fileId") Integer fileId, @Param("hixEnrollmentId") Long hixEnrollmentId);
	
	@Query(" SELECT ers.coverageYear,COUNT( DISTINCT ers.hixEnrollmentId ) "
			+" FROM EnrlReconSnapshot ers "
			+ "WHERE ers.fileId IN (:fileIds) AND  ers.hixEnrollmentId NOT IN (SELECT e.id FROM Enrollment e where e.createdOn < :cutOffDate)  GROUP BY ers.coverageYear ")
	List<Object[]> countEnrollmentsMissingInHix(@Param("fileIds") List<Integer> fileIds, @Param("cutOffDate") Date cutOffDate);
	
	@Query(" SELECT TO_CHAR(e.benefitEffectiveDate,'YYYY'), COUNT( DISTINCT e.id ) "
			+" FROM Enrollment e "
			+ "WHERE e.createdOn < :cutOffDate AND TO_CHAR(e.benefitEffectiveDate, 'YYYY') IN (:coverageYears)  AND e.hiosIssuerId = :hiosIssuerId  "
			+ "AND e.enrollmentStatusLkp.lookupValueCode NOT IN  ('ABORTED') "
			+ "AND e.id NOT IN (SELECT DISTINCT ers.hixEnrollmentId  FROM EnrlReconSnapshot ers WHERE ers.fileId IN (:fileIds) AND ers.hixEnrollmentId IS NOT NULL ) "
			+ "AND e.id NOT IN (SELECT DISTINCT err.enrollmentId FROM EnrlReconError err where err.fileId IN (:fileIds) AND err.enrollmentId IS NOT NULL) GROUP BY TO_CHAR(e.benefitEffectiveDate,'YYYY') ")
	List<Object[]> countEnrollmentsMissingInFile(@Param("fileIds") List<Integer> fileIds, @Param("cutOffDate") Date cutOffDate,  @Param("hiosIssuerId") String hiosIssuerId, @Param("coverageYears") List<String> coverageYears);
	
	@Query("SELECT ers.fileId, ers.hixEnrollmentId, ers.issuerAssignedPolicyId, ers.hixData, ers.issuerData "
			+ " FROM EnrlReconSnapshot as ers"
			+ " WHERE ers.id = (SELECT MAX(ersIn.id) FROM EnrlReconSnapshot as ersIn, EnrlReconSummary as ersu WHERE ersIn.fileId = ersu.id "
			+ " AND ersIn.hixEnrollmentId = :hixEnrollmentId "
			+ " AND (ersIn.hixData IS NOT NULL OR ersIn.issuerData IS NOT NULL)"
			+ "	AND ersu.status NOT IN ('Duplicate') "
			+ " ) AND ers.hixEnrollmentId = :hixEnrollmentId")
	List<Object[]> getIssuerAndHixDataByEnrollmentId(@Param("hixEnrollmentId") Long hixEnrollmentId);
	
	@Query("SELECT COUNT(DISTINCT ers.fileId) FROM EnrlReconSnapshot as ers, EnrlReconSummary as ersu WHERE ers.fileId = ersu.id"
			+ " AND ersu.status NOT IN ('Duplicate') "
			+ " AND ers.hixEnrollmentId = :hixEnrollmentId")
	Long countNumberOfTimeEnrollmentReconciled(@Param("hixEnrollmentId") Long hixEnrollmentId);
	
	@Query(" SELECT DISTINCT ers.hixEnrollmentId "
			+" FROM EnrlReconSnapshot ers "
			+ "WHERE ers.fileId = :fileId AND  ers.comparisonStatus ='MISSING_IN_HIX'")
	List<Long> getEnrollmentsMissingInHix(@Param("fileId") Integer fileId);
	
	@Query(" SELECT DISTINCT ers.hixEnrollmentId "
			+" FROM EnrlReconSnapshot ers "
			+ "WHERE ers.fileId = :fileId AND  ers.comparisonStatus ='MISSING_IN_FILE'")
	List<Long> getEnrollmentsMissingInFile(@Param("fileId") Integer fileId);
	
	@Query("SELECT enrlReconSnapshot.hixData FROM EnrlReconSnapshot enrlReconSnapshot where enrlReconSnapshot.fileId = :fileId "
			+ "AND enrlReconSnapshot.hixEnrollmentId = :hixEnrollmentId "
			+ "AND enrlReconSnapshot.creationTimestamp = "
			+ "(SELECT MAX(creationTimestamp) FROM EnrlReconSnapshot where fileId = :fileId AND hixEnrollmentId = :hixEnrollmentId)")
	String findHixDataByFileIdAndEnrollmentId(@Param("fileId") Integer fileId, @Param("hixEnrollmentId") Long hixEnrollmentId);
	
	@Query("SELECT ers.fileId FROM EnrlReconSnapshot ers, EnrlReconSummary ersu "
			+ " WHERE ers.fileId = ersu.id "
			+ " AND ers.hixEnrollmentId = :hixEnrollmentId "
			+ " AND ersu.status = 'Duplicate'")
	List<Integer> findFileIdForHixEnrollmentId(@Param("hixEnrollmentId") Long hixEnrollmentId);
}
