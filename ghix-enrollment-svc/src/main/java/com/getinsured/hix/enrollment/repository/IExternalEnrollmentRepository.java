package com.getinsured.hix.enrollment.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.ExternalEnrollment;


public interface IExternalEnrollmentRepository extends JpaRepository<ExternalEnrollment, Integer> {
	
	ExternalEnrollment findByApplicantExternalId(String applicantExternalId );
	
	List<ExternalEnrollment> findByApplicantExternalIdIn(List<String> applicantExternalIds );
	
	List<ExternalEnrollment> findByhealthExchangeAssignedPolicyIdInOrDentalExchangeAssignedPolicyIdIn(List<String> applicantHealthEnrollmentIds,List<String> applicantDetalEnrollmentIds  );
	
	
	
	@Modifying
	@Transactional
	@Query("update ExternalEnrollment set applicantMatched = 'Y', updatedOn = :currentDate where applicantExternalId in (" + 
			"select distinct en.applicantExternalId from ExternalEnrollment as en, SsapApplicant as sa where en.applicantExternalId = sa.externalApplicantId)")
	void matchApplicants(@Param("currentDate") Date currentDate);
	
	@Modifying
	@Transactional
	@Query("update ExternalEnrollment set applicantMatched = 'Y', updatedOn = :currentDate where applicantExternalId in (" + 
			"select distinct en.applicantExternalId from ExternalEnrollment as en, SsapApplicant as sa where en.applicantExternalId = sa.externalApplicantId"
			+ " and en.applicationExternalId=sa.ssapApplication.externalApplicationId)")
	void matchApplicantsAndApplication(@Param("currentDate") Date currentDate);
	
	@Modifying
	@Transactional
	@Query("update SsapApplicant set tobaccouser = :tobaccoUse, lastUpdateTimestamp = :currentDate  where externalApplicantId in (" 
			+"select distinct en.applicantExternalId from ExternalEnrollment as en where en.tobaccoIndicator = :tobaccoUse)")
	void updateSsapApplicantsForTobacco(@Param("tobaccoUse") String tobaccoUse, @Param("currentDate") Date currentDate);
	}
