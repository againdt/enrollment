/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrollmentCommissionDTO;
import com.getinsured.hix.enrollment.carrierfeed.util.CarrierFeedQueryUtil;
import com.getinsured.hix.enrollment.dto.EnrollmentCommissionRequest;
import com.getinsured.hix.enrollment.repository.IEnrollmentCommissionRepository;
import com.getinsured.hix.model.enrollment.CommissionConfigMapping;
import com.getinsured.hix.model.enrollment.EnrollmentCommission;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;


/**
 * @author Priya C
 * @since 13/02/2013
 */
@Service("enrollmentCommissionService")
@Transactional
public class EnrollmentCommissionServiceImpl implements EnrollmentCommissionService {
	
	private static final Logger LOGGER = Logger.getLogger(EnrollmentCommissionServiceImpl.class);
	
	private static final DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    
	@Autowired private IEnrollmentCommissionRepository iEnrollmentCommissionRepository;

    @Override
    public Float sumCommissionAmtByEnrollmentIdAndCommissionDateLT(Integer enrollmentId, Date commissionDate, Date premiumMonth) {
        return iEnrollmentCommissionRepository.sumCommissionAmtByEnrollmentIdAndCommissionDateLT(enrollmentId, commissionDate, premiumMonth).floatValue();
    }
	
    @Override
    public Float sumCommissionAmtByEnrollmentIdAndCommissionDateLT(Integer enrollmentId, Date commissionDate, Date premiumMonth, Integer id) {
        return iEnrollmentCommissionRepository.sumCommissionAmtByEnrollmentIdAndCommissionDateLT(enrollmentId, commissionDate, premiumMonth, id).floatValue();
    }
    
	@Override
	public	CommissionConfigMapping getcommissionConfigMappingByName(String mappingName){
		return new CommissionConfigMapping();
	}
	
	@Override
	public	List<EnrollmentCommission> findEnrollmentCommissionByEnrollmentId(Integer enrollmentId){
		return iEnrollmentCommissionRepository.findByEnrollmentId(enrollmentId);
	}

	@Override
	public	List<EnrollmentCommission> findByEnrollmentIdAndCommissionDateGE(Integer enrollmentId, Date commissionDate, Date premiumMonth) {
		return iEnrollmentCommissionRepository.findByEnrollmentIdAndCommissionDateGE(enrollmentId, commissionDate, premiumMonth);
	}

	@Override
	public	List<EnrollmentCommission> findByEnrollmentIdAndCommissionDateGE(Integer enrollmentId, Date commissionDate, Date premiumMonth, Integer id) {
		return iEnrollmentCommissionRepository.findByEnrollmentIdAndCommissionDateGE(enrollmentId, commissionDate, premiumMonth, id);
	}
	
	@Override
	public EnrollmentCommission saveEnrollmentCommission(EnrollmentCommissionDTO enrollmentCommissionDTO){
		EnrollmentCommission commission = new EnrollmentCommission();
		if(null!=enrollmentCommissionDTO){
			try
			{
				if(null==enrollmentCommissionDTO.getAmtPaidToDate() || ("").equals(enrollmentCommissionDTO.getAmtPaidToDate()))
				{
					//float amountPaidToDate = Float.parseFloat(enrollmentCommissionDTO.getCommissionAmt());
					enrollmentCommissionDTO.setAmtPaidToDate(enrollmentCommissionDTO.getCommissionAmt());
					Float amountPaidToDate = sumCommissionAmtByEnrollmentIdAndCommissionDateLT(Integer.parseInt(enrollmentCommissionDTO.getEnrollmentId()), df.parse(enrollmentCommissionDTO.getCommissionDate()), enrollmentCommissionDTO.getDueMonthYear());
			        if (enrollmentCommissionDTO.getCommissionAmt() != null && !"".equals(enrollmentCommissionDTO.getCommissionAmt())) {
			            amountPaidToDate += Float.parseFloat(enrollmentCommissionDTO.getCommissionAmt());
			        }
					enrollmentCommissionDTO.setAmtPaidToDate(String.valueOf(CarrierFeedQueryUtil.getFormattedFloatValue(amountPaidToDate)));
				}
				
				commission =  mapEnrollmentCommission(enrollmentCommissionDTO,commission);
				commission =  iEnrollmentCommissionRepository.save(commission);
				
				 // HIX-66949: update all enrollment commissions for the same enrollmentId that happens after this one
				 if (enrollmentCommissionDTO.getCommissionAmt() != null) {
			            List<EnrollmentCommission> commissions = findByEnrollmentIdAndCommissionDateGE(Integer.parseInt(enrollmentCommissionDTO.getEnrollmentId()), df.parse(enrollmentCommissionDTO.getCommissionDate()), enrollmentCommissionDTO.getDueMonthYear());
			            for (EnrollmentCommission tempCommission: commissions) {
			                if (Objects.equals(tempCommission.getId(), commission.getId())) {
			                    continue;
			                }
			                Float paid = tempCommission.getAmtPaidToDate() == null ? 0.00f : tempCommission.getAmtPaidToDate();
			                tempCommission.setAmtPaidToDate(CarrierFeedQueryUtil.getFormattedFloatValue(paid + Float.parseFloat(enrollmentCommissionDTO.getCommissionAmt())));
			                saveEnrollmentCommission(tempCommission);
			            }
				 }
			}
			catch(Exception ex)
			{
				LOGGER.error( " Error while saving Enrollment Commission details for enrollment id : " + enrollmentCommissionDTO.getEnrollmentId());
			}
		}
		
		return commission;
	}

	@Override
	public EnrollmentCommission saveEnrollmentCommission(EnrollmentCommission commission) {
		return iEnrollmentCommissionRepository.save(commission);
	}

	@Override
	public	EnrollmentCommission updateEnrollmentCommission(EnrollmentCommissionDTO enrollmentCommissionDTO){
		EnrollmentCommission commission = null;
		if(null!=enrollmentCommissionDTO && null != enrollmentCommissionDTO.getId() ){
		commission =iEnrollmentCommissionRepository.findOne(Integer.parseInt(enrollmentCommissionDTO.getId()));
		}
		if(null!=commission){
			try
			{
				if(null==enrollmentCommissionDTO.getAmtPaidToDate() || ("").equals(enrollmentCommissionDTO.getAmtPaidToDate()))
				{	
					enrollmentCommissionDTO.setAmtPaidToDate(enrollmentCommissionDTO.getCommissionAmt());
					Float amountPaidToDate = sumCommissionAmtByEnrollmentIdAndCommissionDateLT(commission.getEnrollmentId(), df.parse(enrollmentCommissionDTO.getCommissionDate()), enrollmentCommissionDTO.getDueMonthYear(), commission.getId());
			        if (enrollmentCommissionDTO.getCommissionAmt() != null && !"".equals(enrollmentCommissionDTO.getCommissionAmt())) {
			            amountPaidToDate += Float.parseFloat(enrollmentCommissionDTO.getCommissionAmt());
			        }
					enrollmentCommissionDTO.setAmtPaidToDate(String.valueOf(CarrierFeedQueryUtil.getFormattedFloatValue(amountPaidToDate)));
				}
				
				Float oldCommissionAmt = commission.getCommissionAmt();
				Float commissionAmtDiff = Float.parseFloat(enrollmentCommissionDTO.getCommissionAmt()) - oldCommissionAmt;
				Date newCommissionDate = df.parse(enrollmentCommissionDTO.getCommissionDate());
				boolean isFutureDate = false;
				if (newCommissionDate.after(commission.getCommissionDate()))
				{
					isFutureDate = true;
				}
					
				Date commissionDate = isFutureDate ? commission.getCommissionDate() : newCommissionDate;
				
				commission =  mapEnrollmentCommission(enrollmentCommissionDTO,commission);
				commission =  iEnrollmentCommissionRepository.save(commission);
				
				// HIX-66949: update all enrollment commissions for the same enrollmentId that happens after this one
				 if (enrollmentCommissionDTO.getCommissionAmt() != null) {
			            List<EnrollmentCommission> commissions = findByEnrollmentIdAndCommissionDateGE(commission.getEnrollmentId(), commissionDate, enrollmentCommissionDTO.getDueMonthYear(), commission.getId());
			            for (EnrollmentCommission tempCommission: commissions) {
			                if (Objects.equals(tempCommission.getId(), commission.getId())) {
			                	isFutureDate = false;
			                    continue;
			                }
			                Float paid = tempCommission.getAmtPaidToDate() == null ? 0.00f : tempCommission.getAmtPaidToDate();
			                tempCommission.setAmtPaidToDate(CarrierFeedQueryUtil.getFormattedFloatValue(paid + (isFutureDate ?  (-oldCommissionAmt) : commissionAmtDiff)));
			                saveEnrollmentCommission(tempCommission);
			            }
				 }
			}
			catch(Exception ex)
			{
				LOGGER.error( " Error while saving Enrollment Commission details for enrollment id : " + enrollmentCommissionDTO.getEnrollmentId());
			}
		}
		
		return commission;
	}
	
	@Override
	public boolean existsByEnrollmentIdAndCommissionDateBetween(Integer enrollmentId, Date begDate, Date endDate) {
		return iEnrollmentCommissionRepository.existsByEnrollmentIdAndCommissionDateBetween(enrollmentId, begDate, endDate);

	}

	private EnrollmentCommission mapEnrollmentCommission(
			EnrollmentCommissionDTO enrollmentCommissionDTO,
			EnrollmentCommission commission) {
		
		if (StringUtils.isNotEmpty(enrollmentCommissionDTO.getCommissionDate())) {
			commission.setCommissionDate(DateUtil.StringToDate(
					enrollmentCommissionDTO.getCommissionDate(), "MM/dd/yyyy"));
		}
		if (StringUtils.isNotEmpty(enrollmentCommissionDTO.getAmtPaidToDate())) {
			commission.setAmtPaidToDate(Float
					.valueOf(enrollmentCommissionDTO.getAmtPaidToDate()));
		}
		if (StringUtils.isNotEmpty(enrollmentCommissionDTO.getCommissionableAmount())) {
			commission
					.setCommissionableAmount(Float
							.valueOf(enrollmentCommissionDTO
									.getCommissionableAmount()));
		}

		/*if (StringUtils.isNotEmpty(enrollmentCommissionDTO.getCommissionRate())) {
			commission.setCommissionRate(Float.valueOf(enrollmentCommissionDTO
					.getCommissionRate()));
		}*/
		if (StringUtils.isNotEmpty(enrollmentCommissionDTO.getCommissionAmt())) {
			commission.setCommissionAmt(Float.valueOf(enrollmentCommissionDTO
					.getCommissionAmt()));
		}
		if (StringUtils.isNotEmpty(enrollmentCommissionDTO.getEnrollmentId())) {
			commission.setEnrollmentId(Integer.valueOf(enrollmentCommissionDTO
					.getEnrollmentId()));
		}
		commission.setPremiumMonth(enrollmentCommissionDTO.getDueMonthYear());
		if(StringUtils.isNotEmpty(enrollmentCommissionDTO.getCarrier()))
		{
			commission.setCarrier(enrollmentCommissionDTO.getCarrier());
		}
		if(StringUtils.isNotEmpty(enrollmentCommissionDTO.getState()))
		{
			commission.setState(enrollmentCommissionDTO.getState());
		}
		if(StringUtils.isNotEmpty(enrollmentCommissionDTO.getMode()))
		{
			commission.setMode(enrollmentCommissionDTO.getMode());
		}else{
			commission.setMode("Manually Entered");
		}
		commission.setCommissionType(enrollmentCommissionDTO.getCommissionType());
		commission.setCarrierFeedDetailsId(enrollmentCommissionDTO.getCarrierFeedDetailsId());
		
		return commission;
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public EnrollmentCommission saveEnrollmentCommission(EnrollmentCommissionRequest enrollmentCommissionRequest)
			throws GIException {
		EnrollmentCommission enrollmentCommission = new EnrollmentCommission();
		try{
			EnrollmentCommission commission = new EnrollmentCommission();
			if(null== enrollmentCommissionRequest.getAmtPaidToDate() || ("").equals(enrollmentCommissionRequest.getAmtPaidToDate()))
			{	
				enrollmentCommissionRequest.setAmtPaidToDate(enrollmentCommissionRequest.getCommissionAmt());
				Float amountPaidToDate = sumCommissionAmtByEnrollmentIdAndCommissionDateLT(enrollmentCommissionRequest.getEnrollmentId(), enrollmentCommissionRequest.getCommissionDate(), enrollmentCommissionRequest.getDueMonthYear());
		        if (enrollmentCommissionRequest.getCommissionAmt() != null && !"".equals(enrollmentCommissionRequest.getCommissionAmt())) {
		            amountPaidToDate += Float.parseFloat(enrollmentCommissionRequest.getCommissionAmt());
		        }
		        enrollmentCommissionRequest.setAmtPaidToDate(String.valueOf(CarrierFeedQueryUtil.getFormattedFloatValue(amountPaidToDate)));
			}
			
			commission = mapDtoToEnrollmentCommission(enrollmentCommissionRequest,commission);
			enrollmentCommission =  iEnrollmentCommissionRepository.save(commission);
			
			 if (enrollmentCommissionRequest.getCommissionAmt() != null) {
	            List<EnrollmentCommission> commissions = findByEnrollmentIdAndCommissionDateGE(enrollmentCommissionRequest.getEnrollmentId(), enrollmentCommissionRequest.getCommissionDate(), enrollmentCommissionRequest.getDueMonthYear());
		            for (EnrollmentCommission tempCommission: commissions) {
	                if (Objects.equals(tempCommission.getId(), commission.getId())) {
		                    continue;
		                }
		                Float paid = tempCommission.getAmtPaidToDate() == null ? 0.00f : tempCommission.getAmtPaidToDate();
	                tempCommission.setAmtPaidToDate(CarrierFeedQueryUtil.getFormattedFloatValue(paid + Float.parseFloat(enrollmentCommissionRequest.getCommissionAmt())));
		                saveEnrollmentCommission(tempCommission);
		            }
			 }
		}
		catch(Exception ex){
			LOGGER.error("Exception occurred while saving EnrollmentCommission for EnrollmentId: "+enrollmentCommissionRequest.getEnrollmentId(), ex);
			throw new GIException(ex);
		}
		
		return enrollmentCommission;
	}
	
	private EnrollmentCommission mapDtoToEnrollmentCommission(EnrollmentCommissionRequest enrollmentCommissionRequest, EnrollmentCommission commission){
		
		if (enrollmentCommissionRequest.getCommissionDate() != null) {
			commission.setCommissionDate(enrollmentCommissionRequest.getCommissionDate());
		}
		if (StringUtils.isNotEmpty(enrollmentCommissionRequest.getAmtPaidToDate())) {
			commission.setAmtPaidToDate(Float
					.valueOf(enrollmentCommissionRequest.getAmtPaidToDate()));
		}
		if (StringUtils.isNotEmpty(enrollmentCommissionRequest.getCommissionableAmount())) {
			commission
					.setCommissionableAmount(Float
							.valueOf(enrollmentCommissionRequest.getCommissionableAmount()));
		}
		if (StringUtils.isNotEmpty(enrollmentCommissionRequest.getCommissionAmt())) {
			commission.setCommissionAmt(Float.valueOf(enrollmentCommissionRequest.getCommissionAmt()));
		}
		if (enrollmentCommissionRequest.getEnrollmentId() != null) {
			commission.setEnrollmentId(enrollmentCommissionRequest.getEnrollmentId());
		}
		commission.setPremiumMonth(enrollmentCommissionRequest.getDueMonthYear());
		if(StringUtils.isNotEmpty(enrollmentCommissionRequest.getCarrier()))
		{
			commission.setCarrier(enrollmentCommissionRequest.getCarrier());
		}
		if(StringUtils.isNotEmpty(enrollmentCommissionRequest.getState()))
		{
			commission.setState(enrollmentCommissionRequest.getState());
		}
		if (enrollmentCommissionRequest.getIssuerId() != null) {
			commission.setIssuerId(Integer.valueOf(enrollmentCommissionRequest.getIssuerId()));
		}
		if(StringUtils.isNotEmpty(enrollmentCommissionRequest.getMode()))
		{
			commission.setMode(enrollmentCommissionRequest.getMode());
		}else{
			commission.setMode("Manually Entered");
		}
		commission.setCommissionType(enrollmentCommissionRequest.getCommissionType());
		if(enrollmentCommissionRequest.getCarrierFeedDetailsId() != null){
			commission.setCarrierFeedDetailsId(enrollmentCommissionRequest.getCarrierFeedDetailsId().intValue());
		}
		
		return commission;
	}
}