package com.getinsured.hix.enrollment.service;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

/**
 * @author negi_s
 *
 */
public interface EnrollmentFfmProxyConfirmEmailNotificationService {

	void sendEmailNotification(Household household, Enrollment enrollment) throws NotificationTypeNotFound;
}
