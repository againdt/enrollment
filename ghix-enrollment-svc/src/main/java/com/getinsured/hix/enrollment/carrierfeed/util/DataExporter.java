package com.getinsured.hix.enrollment.carrierfeed.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jabelardo on 7/15/14.
 */
public interface DataExporter<T> {

    void export(Iterator<T> iterator) throws IOException;

    void exportAll(List<T> list) throws IOException;
}
