package com.getinsured.hix.enrollment.service;

import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.repository.IEnrollment1095Repository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.google.gson.Gson;

@Service("enrollment1095Service")
public class Enrollment1095ServiceImpl implements Enrollment1095Service{

	@Autowired private IEnrollment1095Repository iEnrollment1095Repository;
	@Autowired private Gson platformGson;
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired private LookupService lookupService;
	private static final String RESEND_DESCRIPTION = "1095-A Resend request received from Bot";
	
	@Override
	public boolean resendEnrollment1095(String householdCaseID, Integer coverageYear, AccountUser accountUser, String phoneNumber) {
		boolean isEnrollment1095Success = false;
		List<Enrollment1095> enrollment1095List = iEnrollment1095Repository.getEnrollment1095ForResend(householdCaseID, coverageYear);
		
		if(enrollment1095List != null && !enrollment1095List.isEmpty()){
			Map<String, String> requestParam = new HashMap<String, String>();
			requestParam.put("Phone Number", phoneNumber);
			Enrollment enrollment = new Enrollment();
			enrollment.setHouseHoldCaseId(householdCaseID);
			enrollment.setEnrollmentTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE, EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL));
			StringBuilder stringBuilder = new StringBuilder(EnrollmentConstants.TWENTY);
			for(Enrollment1095 enrollment1095 : enrollment1095List){
				enrollment1095.setResendPdfFlag(Enrollment1095.YorNFlagIndicator.Y.toString());
				enrollment1095.setUpdateNotes("Resend Enrollment 1095 from Rest API");
				enrollment1095.setUpdatedBy(accountUser.getId());
				stringBuilder.append(enrollment1095.getExchgAsignedPolicyId());
				stringBuilder.append(EnrollmentConstants.CSVS_SPLITE_BY);
				iEnrollment1095Repository.saveAndFlush(enrollment1095);
			}
			
			if(stringBuilder.length() != EnrollmentConstants.ZERO){
				requestParam.put("Enrollment Ids", (stringBuilder.deleteCharAt(stringBuilder.length() -1)).toString());
			}
			requestParam.put("Description", RESEND_DESCRIPTION);
			enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollment, EnrollmentConstants.EnrollmentAppEvent.ENROLLMENT_1095_RESEND.toString(), requestParam, accountUser);
			isEnrollment1095Success = true;
		}
		return isEnrollment1095Success;
	}

	@Override
	public String fetchEnrollment1095DataByHouseholdCaseId(String householdCaseID) {
		
		Calendar calendar = TSCalendar.getInstance();
		Integer latestCoverageYear = calendar.get(Calendar.YEAR) - 1;
		boolean isLatest1095Present = false;
		List<Object[]> enrollment1095DataObjList = iEnrollment1095Repository.fetchEnrollment1095CoverageYearData(householdCaseID, latestCoverageYear);
		Map<String, Object> responseDataMap = new HashMap<String, Object>();
		List<LinkedHashMap<String, Object>> coverageYearMainList = new ArrayList<LinkedHashMap<String, Object>>();
		if (enrollment1095DataObjList != null && !enrollment1095DataObjList.isEmpty()) {
			for (Object[] obj : enrollment1095DataObjList) {
				LinkedHashMap<String, Object> coverageYearMap = new LinkedHashMap<String, Object>();
				
				coverageYearMap.put("coverageYear", obj[EnrollmentConstants.ZERO]);
				coverageYearMap.put("total1095Present", obj[EnrollmentConstants.ONE]);
				long failed = (long) obj[EnrollmentConstants.THREE];
				long generated = (long) obj[EnrollmentConstants.FOUR];
				long scheduled = (long) obj[EnrollmentConstants.FIVE];
				
				if(obj[EnrollmentConstants.ZERO] != null && obj[EnrollmentConstants.ZERO].equals(latestCoverageYear)){
					isLatest1095Present = true;
				}
				
				
				if(failed != EnrollmentConstants.ZERO){
					coverageYearMap.put("status", "FAILED"); 
				}else if(failed == 0 && scheduled == 0 && generated != 0){
					coverageYearMap.put("status", "GENERATED");
				}else if(failed == 0 && scheduled != 0 && generated == 0){
					coverageYearMap.put("status", "SCHEDULED");
				}else{
					coverageYearMap.put("status", "UNKNOWN");
				}
				
				
				//coverageYearMap.put("lastScheduledDate", null);

				coverageYearMainList.add(coverageYearMap);
			}
			responseDataMap.put("coverageYearDetail", coverageYearMainList);
			responseDataMap.put("householdAddress", iEnrollment1095Repository.fetch1095HouseholdAddress(householdCaseID));
		}
		responseDataMap.put("householdCaseId", householdCaseID);
		responseDataMap.put("latest1095Present", isLatest1095Present);
		responseDataMap.put("multiple1095Present", coverageYearMainList.size() > 1 ? true : false);
		responseDataMap.put("latest1095CoverageYear", latestCoverageYear);
		return platformGson.toJson(responseDataMap);
	}
}
