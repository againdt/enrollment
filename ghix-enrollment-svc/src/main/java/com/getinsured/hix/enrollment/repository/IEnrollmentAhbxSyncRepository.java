package com.getinsured.hix.enrollment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;

public interface IEnrollmentAhbxSyncRepository extends JpaRepository<EnrollmentAhbxSync, Integer> {

}
