package com.getinsured.hix.enrollment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;

/**
 * @author rajaramesh_g
 * @since 24/12/2013
 * 
 */

@Service("enrollmentUserPrivateService")
public class EnrollmentUserPrivateServiceImpl implements EnrollmentUserPrivateService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentUserPrivateServiceImpl.class);

	@Autowired private UserService userService;

	@Override
	public AccountUser getLoggedInUser() {
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException e) {
			LOGGER.error("Invalid User", e);				
		}
		
		return user;
	}

	@Override
	public Role getRoleForUser(AccountUser user) {
		return userService.getDefaultRole(user);
	}


}
	
