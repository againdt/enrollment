package com.getinsured.hix.enrollment.service;
import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.getinsured.hix.dto.planmgmt.IssuerEnrollmentEndDateRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerEnrollmentEndDateResponseDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRaceAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeUpdateSendStatusRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentInboundReconciliationReportRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentLocationAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.repository.IExtractionJobStatusRepository;
import com.getinsured.hix.enrollment.repository.IIRSInboundTransmissionRepository;
import com.getinsured.hix.enrollment.repository.IIRSOutboundTransmissionRepository;
import com.getinsured.hix.enrollment.util.EnrollmentAppEventLogUtil;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentEDIMonitoringInboundJobThread;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentReconCSVReportJobThread;
import com.getinsured.hix.enrollment.util.EnrollmentReconReportJobThread;
import com.getinsured.hix.enrollment.util.EnrollmentReconciliationInfo;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.NullCharacterEscapeHandler;
import com.getinsured.hix.enrollment.util.ZipHelper;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.ExchgPartnerLookup;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LocationAud;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.enrollment.Attachment;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.model.enrollment.EnrolleeRaceAud;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.EnrolleeRelationshipAud;
import com.getinsured.hix.model.enrollment.EnrolleeUpdateSendStatus;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentAud;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentInboundReconciliationReport;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.model.enrollment.Enrollments;
import com.getinsured.hix.model.enrollment.ExtractionJobStatus;
import com.getinsured.hix.model.enrollment.FilePayloadBatchTransmission;
import com.getinsured.hix.model.enrollment.IRSInboundTransmission;
import com.getinsured.hix.model.enrollment.IRSOutboundTransmission;
import com.getinsured.hix.model.enrollment.enrollmentAhbxSync.EnrollmentAhbxSync;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.DateUtil.StartEndYearEnum;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JiraUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @author parhi_s
 * @since 18/06/2013
 */
@Service("enrollmentBatchService")
@Transactional
public class EnrollmentBatchServiceImpl implements EnrollmentBatchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentBatchServiceImpl.class);
	private final SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.FILENAME_DATE_FORMAT);
	private static final String ERROR_IN_GETFILESINFOLDER="Error in getFilesInAFolder";
	private static final String HANDLE_IRS_INBOUND_NACK_RESPONSE="handleIRSInboundNackResponses()::";
	private static final String ROOT_ELEMENT="Root element :";
	private static final String CURRENT_ELEMENT="\nCurrent Element :";
	private static final String MANIFEST_XML="manifest.xml";
	private static final String STATE_CODE_ID = "ID";
	private static List<EnrolleeRace> defaultEnrolleeRaceList;
	
	@Autowired private GhixRestTemplate restTemplate;
	@Autowired private EnrolleeAuditService enrolleeAuditService;
	@Autowired private IEnrolleeUpdateSendStatusRepository enrolleeUpdateSendStatusRepository;
	@Autowired private IEnrolleeRepository enrolleeRepository;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IEnrollmentAudRepository enrollmentAudRepository;
	@Autowired private IEnrolleeAudRepository enrolleeAudRepository;
	@Autowired private IEnrolleeRelationshipRepository enrolleeRelationshipRepository;
	@Autowired private IEnrolleeRaceAudRepository enrolleeRaceAudRepository;
	@Autowired private IEnrolleeRelationshipAudRepository enrolleeRelationshipAudRepository;
	@Autowired private IIRSOutboundTransmissionRepository irsOutboundTransmissionRepo;
	@Autowired private IIRSInboundTransmissionRepository irsInboundTransmissionRepo;
	@Autowired private IEnrollmentLocationAudRepository locationAudRepository;
	@Autowired private UserService userService;
	@Autowired private EnrolleeService enrolleeService;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private EnrollmentEmailNotificationService enrollmentEmailNotificationService;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private LookupService lookupService;
	@Autowired private ValidationFolderPathService validationFolderPathService;
	@Autowired private IRSOutboundTransmissionService iRSOutboundTransmissionService;
	@Autowired private EnrollmentIrsReportService enrollmentIrsReportService;
	@Autowired private IEnrollmentEventRepository enrollmentEventRepository;
	@Autowired private IExtractionJobStatusRepository extractionJobStatusRepository;
	@Autowired private EnrollmentReconciliationReportService enrollmentReconciliationReportService;
	@Autowired private IEnrollmentInboundReconciliationReportRepository enrollmentInboundReconciliationReportRepository;
	@Autowired private EnrollmentAppEventLogUtil enrollmentAppEventLogUtil;
	@Autowired private EnrollmentRequotingService enrollmentRequotingService;
	@Autowired private IEnrollmentPremiumRepository enrollmentPremiumRepository;
	@Autowired private Gson platformGson;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	@Autowired private IEnrollmentEventAudRepository enrollmentEventAudRepository;
	@Autowired private IUserRepository userRepository;
	@Autowired private EnrollmentAhbxSyncService enrollmentAhbxSyncService;
	
	private static Integer threadTimeOut;
	private static Integer threadPoolSize;
	
	
	private List<Enrollee> filterEnrolleesFor834Reconciliation(List<Enrollee> allEnrollees, Date cutOffDate) throws GIException{
		List<Enrollee> filteredEnrolles=null;
		try{
			if(allEnrollees != null && !allEnrollees.isEmpty() && cutOffDate != null){
				filteredEnrolles=new ArrayList<Enrollee>();
				for(Enrollee enrollee:allEnrollees){
					if(enrollee.getPersonTypeLkp()!=null&&enrollee.getEnrolleeLkpValue()!=null){
						String personType=enrollee.getPersonTypeLkp().getLookupValueCode();
						String status=enrollee.getEnrolleeLkpValue().getLookupValueCode();
						if(personType!=null && (personType.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)||personType.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE))
							&& (status!=null && (status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)||status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)|| (status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) 
							&& enrollee.getEffectiveEndDate().after(cutOffDate))))){
								filteredEnrolles.add(enrollee);
							}
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("filterEnrolleesFor834Reconciliation()::"+e.getMessage(),e);
			//	e.printStackTrace();
			throw new GIException("Error in filterEnrolleesFor834Reconciliation()::"+e.getMessage(),e);
		}
		
		return filteredEnrolles;
	}
	/**
	 * @author parhi_s
	 * @since
	 * This method is used by reconciliation jobs to set enrollments object
	 * @param enrollmentList
	 * @return
	 * @throws GIException
	 */
	
	private Enrollments setEnrollmentsFor834Reconciliation(List<Enrollment> enrollmentList, String statusType, Date cutOffDate) throws GIException{
		Enrollments enrollments = new Enrollments();
		List<Enrollment> updatedEnrollmentList=null;
		try{
			if(enrollmentList != null && !enrollmentList.isEmpty()){
				updatedEnrollmentList= new ArrayList<Enrollment>();
				for(Enrollment enrollment:enrollmentList){
					int totalEnrollees=0;
					List<Enrollee> enrolleeList=null;
					if(statusType!=null && statusType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
						String enrollmentType = enrollment.getEnrollmentTypeLkp().getLookupValueCode();
						if(isNotNullAndEmpty(enrollmentType) && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
							if(isEnrollmentFromPendingToCancel(enrollment)){
								continue;
							}
						}
						else{
							enrolleeList=filterEnrolleesFor834ReconCancelTerm(enrollment.getEnrollees(), cutOffDate);
						}
					}else{
						//enrolleeList=enrolleeRepository.getEnrolleeByStatusAndEnrollmentID(enrollment.getId(), EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM);
						enrolleeList=filterEnrolleesFor834Reconciliation(enrollment.getEnrollees(), cutOffDate);
					}
					
					if(enrolleeList != null && !enrolleeList.isEmpty()){
						enrollment.setEnrollees(enrolleeList);
						updatedEnrollmentList.add(enrollment);
						
						
						
						//removing '-' from  InsurerTaxIdNumber
						if(enrollment.getInsurerTaxIdNumber()!=null){
							enrollment.setInsurerTaxIdNumber(enrollment.getInsurerTaxIdNumber().replaceAll("[-]", ""));
						}
						
						Enrollee subscriber = null;
						for (Enrollee enrollee:enrolleeList){
							if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals((EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){
								//Found the subscriber
								subscriber = enrollee;
								break;
							}
						}
						
						//HIX-77670
						totalEnrollees=enrolleeList.size();
						enrollment.setQTYt(totalEnrollees);
						if(subscriber!=null){
							enrollment.setQTYy(1);	
						}else{
							enrollment.setQTYy(0);
						}
						
						enrollment.setQTYn(totalEnrollees-enrollment.getQTYy());
						
						List<EnrollmentPremium> enrollmentPremiumList = enrollment.getEnrollmentPremiums();
						boolean isPremiumListEmpty = isPremiumListNullOrEmpty(enrollmentPremiumList);
						sortPremiumList(enrollmentPremiumList);
						
						for(Enrollee enrollee:enrolleeList){
							//check whether to display mailing address or not 
							enrollee.setShowMailingAddress(EnrollmentConstants.TRUE);
							if(EnrollmentUtils.compareAddress(enrollee.getHomeAddressid(), enrollee.getMailingAddressId())){
								enrollee.setShowMailingAddress(EnrollmentConstants.FALSE);
							}
							
							//set relationship to subscriber
							if(subscriber!=null){
								EnrolleeRelationship enroleeRelationship = enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrollee.getId(), subscriber.getId());
									if(enroleeRelationship!=null){
									enrollee.setRelationshipToSubscriberLkp(enroleeRelationship.getRelationshipLkp());
									}
								}
							
							if(enrollee.getLastEventId() != null)
							{
								enrollee.setFormatSignatureDate(enrollee.getLastEventId().getCreatedOn());
							}
							try{
							//Set Rating Area
							enrollee.setRatingArea(EnrollmentUtils.getFormatedRatingArea(enrollee.getRatingArea()));
							}
							catch(Exception ex){
								LOGGER.error("Error in formatting Rating Area for Enrollee with id:: " +  enrollee.getId(), ex);
							}
							
							if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.SHOW_834_PREMIUM)) && !isPremiumListEmpty){
								enrollee.setEnrollmentPremiums(enrollment.getEnrollmentPremiums());
							}
						}
					
						
						// HIX-35797
						if(enrollment.getCsrAmt() != null){
							Date jan2015CoverageStartDate = DateUtil.StringToDate(EnrollmentConstants.JAN_2015_DATE, GhixConstants.REQUIRED_DATE_FORMAT);
							if(enrollment.getBenefitEffectiveDate().compareTo(jan2015CoverageStartDate) >= 0){
								enrollment.setTotalCSRAmt(enrollment.getCsrAmt());
							}

							else {
								enrollment.setTotalCSRAmt(totalEnrollees * enrollment.getCsrAmt());
							}
						}
					
						Enrollee responsibleEnrollee = enrolleeService.getResponsiblePersonByEnrollmentID(enrollment.getId());
						if(responsibleEnrollee != null){
						enrollment.setResponsiblePerson(responsibleEnrollee);
						}
						//set 14 digit GS03
						if(enrollment.getCMSPlanID()!=null){
							enrollment.setgS02(enrollment.getCMSPlanID().substring(0, enrollment.getCMSPlanID().length()-2));
							//enrollments.setGS02(enrollment.getCMSPlanID().substring(0, enrollment.getCMSPlanID().length()-2));
						}
					}
					
					if(updatedEnrollmentList != null && !updatedEnrollmentList.isEmpty()){
						enrollments.setEnrollmentList(updatedEnrollmentList);
						
						/*ExchgPartnerLookup exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(enrollment.getIssuer().getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL,GhixConstants.OUTBOUND);
						
						if(exchgPartnerLookup != null){
							enrollments.setISA05(exchgPartnerLookup.getIsa05());
							enrollments.setISA06(exchgPartnerLookup.getIsa06());
							enrollments.setISA07(exchgPartnerLookup.getIsa07());
							enrollments.setISA08(exchgPartnerLookup.getIsa08());
							enrollments.setISA15(exchgPartnerLookup.getIsa15());
							enrollments.setGS02(exchgPartnerLookup.getGs02());
							
						}
						enrollments.setGS03(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.SOURCE_EXCHANGE_ID));
						*/
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("setEnrollmentsFor834Reconciliation()::"+e.getMessage(),e);
			throw new GIException("Error in setEnrollmentsFor834Reconciliation()::"+e.getMessage(),e);
			
		}
		return enrollments;
	}
	
	/**
	 * @author parhi_s
	 * 
	 * @param enrolleeList
	 * @return
	 * @throws GIException
	 */
	
//	private List<Enrollee> filterOutPendingToCancelEnrollees(
//			List<Enrollee> enrolleeList) throws GIException {
//		List<Enrollee> inactEnrollees=null;
//		try{
//			if(enrolleeList!=null && enrolleeList.size()>0){
//				inactEnrollees=new ArrayList<Enrollee>();
//				for(Enrollee enrollee:enrolleeList){
//					if(enrollee.getEnrolleeLkpValue() != null && 
//							enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
//						List<EnrolleeAud> enrolleeAudList = enrolleeAudRepository.getEnrolleeByIdRevAndNoPendingStatus(enrollee.getId());
//						if(enrolleeAudList == null || enrolleeAudList.size() <= 0){
//							continue;
//						}
//						inactEnrollees.add(enrollee);
//					}
//				}
//			}
//			return inactEnrollees;
//		}catch(Exception e){
//			LOGGER.error("filterOutPendingToCancelEnrollees()::"+e.getMessage(),e);
//			throw new GIException("Error in filterOutPendingToCancelEnrollees()::"+e.getMessage(),e);
//		}
//	}
	
	/**
	 * @author Aditya
	 * 
	 * @param enrolleeList
	 * @return
	 * @throws GIException
	 */
	private boolean isEnrollmentFromPendingToCancel(EnrollmentAud enrollment, Date eventDate) throws GIException {
		boolean isFromPendingToCancel = false;
		try{
			if(enrollment.getEnrollmentStatusLkp() != null && 
					enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
				List<EnrollmentAud> enrollmentAudList = enrollmentAudRepository.getEnrollmentByIdAndNoPendingStatus(enrollment.getId(), eventDate);
				if(enrollmentAudList == null || enrollmentAudList.isEmpty()){
					isFromPendingToCancel = true;
				}
			}
		}catch(Exception e){
			LOGGER.error("filterOutPendingToCancelEnrollments :: " + e.getMessage(),e);
			throw new GIException("Error in filterOutPendingToCancelEnrollments :: " + e.getMessage(),e);
		}
		
		return isFromPendingToCancel;
	}

	
	
	/**
	 * @since
	 * @author parhi_s
	 * @param enrollments
	 * @param outputPath
	 * @throws Exception
	 * 
	 * This method is used to Create XML report by taking
	 *  Enrollments record for enrollmentXML job
	 */

	private void generateEnrollmentsXMLReport(Enrollments enrollments,String outputPath, String xsltPath) throws GIException {	
		InputStream strXSLPath = null;
		InputStream strEmptyTagXSLPath = null;
		try {
			LOGGER.info("generateEnrollmentsXMLReport outputPath = "+ outputPath);
			//HIX-69083 Adding file path validation for HP-FOD
			if(enrollments!=null && enrollments.getEnrollmentList() != null && !enrollments.getEnrollmentList().isEmpty() && GhixUtils.isGhixValidPath(outputPath)){			
				//String strXML = null;
				strXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream(xsltPath);
				strEmptyTagXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream("removeEmptyTags.xsl");
				JAXBContext context = JAXBContext.newInstance(Enrollments.class, Enrollment.class, ArrayList.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

				StringWriter writerEnroll = new StringWriter();
				StreamResult resultEnroll = new StreamResult(writerEnroll);
				
				//m.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new NullCharacterEscapeHandler());
				m.marshal(enrollments, resultEnroll);
				String strEnrollTrans = new String(writerEnroll.toString());
				//TransformerFactory factory = null;
				//factory =  TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",null);
				TransformerFactory factory = new net.sf.saxon.TransformerFactoryImpl();
				Source xslSource = new StreamSource(strXSLPath);
				StreamSource xmlJAXB = new StreamSource(new StringReader(strEnrollTrans));
				Templates template = factory.newTemplates(xslSource);
				Transformer transformer = template.newTransformer();
				StringWriter writerXSLT = new StringWriter();
				StreamResult resultXSLT = new StreamResult(writerXSLT);
				transformer.transform(xmlJAXB, resultXSLT);

				//strXML = new String(writerXSLT.toString());
				StreamSource xmlWithEmpty = new StreamSource(new StringReader(writerXSLT.toString()));
				Source xslEmptyTagSource = new StreamSource(strEmptyTagXSLPath);
				Templates templateEmpty = factory.newTemplates(xslEmptyTagSource);
				Transformer transformerEmpty = templateEmpty.newTransformer();
				transformerEmpty.transform(xmlWithEmpty, new StreamResult(new File(outputPath)));
			}else{
				LOGGER.error("Enrollment list is null/empty or the Application is trying to reach a blacklisted folder or filename or extension type.");
			}

		} catch (Exception exception) {

			LOGGER.error(" EnrollmentServiceImpl.generateEnrollmentsXMLReport Exception "+ exception.getMessage());
			throw new GIException(" EnrollmentServiceImpl.generateEnrollmentsXMLReport Exception ",exception);

		}finally{
			IOUtils.closeQuietly(strXSLPath);
			IOUtils.closeQuietly(strEmptyTagXSLPath);
		}
	}
	/**
	 * @author Priya C
	 * @since
	 * This method is used to get all the files of a specific type from a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
	private File[] getIRSNackResponseFilesInAFolder(String folderName, String fileType) throws GIException{
		try{
		final String fileExtn=fileType;
		File folder= new File(folderName);
		/*File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn)){
					return true;
				}
				return false;
			}
		});
		return listOFFiles;*/
		return folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn)){
					return true;
				}
				return false;
			}
		});
		
		}catch(Exception e){
			LOGGER.error(ERROR_IN_GETFILESINFOLDER+e.getMessage(),e);
			throw new GIException(ERROR_IN_GETFILESINFOLDER+e.getMessage(),e);
		}
		
	}
	
		@Override
		public void handleIRSInboundShopResponses () throws GIException{
			 handleIRSInboundShopNackResponses();
		     handleEOMOUTResponses();
			 handleIRSInboundIRSShopResponses();
			 handleEOMACKResponses();
				 
		}		
		
		private void handleEOMACKResponses() throws GIException{
			try{
				// we need add this into GI_APP_CONFIG table
			    String IRSEOMACKFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_EOM_ACK_FILEPATH);
				//String IRSEOMACKFilePath = "C:/EOMTest/ACK";
				//String IRSResponseMarketPlacePath	="C:/InboundIRSResponseMarketPlacePath";
				String fileType = ".OUT";
				if(IRSEOMACKFilePath != null){
					File file = new File(IRSEOMACKFilePath);
					if(file.exists()){
						String[] renameZipFileNames = file.list();
						
						for(String name : renameZipFileNames){
							File SourceZipFile = new File(IRSEOMACKFilePath + File.separator + name);
						    File oldzipFile =SourceZipFile;          
							// File (or directory) with new name
						    String newZipFileName =  oldzipFile + ".zip";			
						    File newZipFile = new File(newZipFileName);   // Rename file (or directory)
						    oldzipFile.renameTo(newZipFile);
						}
						String[] ZipFileNames = file.list();
						for(String name : ZipFileNames){
							File SourceZipFile = new File(IRSEOMACKFilePath + File.separator + name);
							String destination = IRSEOMACKFilePath;
							File destiFile = new File(destination);
							ZipHelper zippy = new ZipHelper();
							zippy.extract(SourceZipFile, destiFile);
						}
						String fileTypeXML = ".xml";
						File[] IRSACKFiles=getIRSNackResponseFilesInAFolder(IRSEOMACKFilePath,fileTypeXML);
						for(File irsFile : IRSACKFiles){
							String batchID = "";
							//String batchCategoryCode = "";
							String responseCode = "";
							String docSeqId = "";
							//List<String> listDocumentSeqId = null;
							DocumentBuilderFactory dbFactory = GhixUtils.getDocumentBuilderFactoryInstance();
							DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
							Document doc = dBuilder.parse(irsFile);
							doc.getDocumentElement().normalize();		     
							System.out.println("Root element :" + doc.getDocumentElement().getNodeName());     
							NodeList nListServiceSpecificData = doc.getElementsByTagName("ServiceSpecificData");
							NodeList nListBatchMetadata = doc.getElementsByTagName("BatchMetadata");
							NodeList nListAttachment = doc.getElementsByTagName("Attachment");						
							//get the batch id from the response xml file
							for (int temp = 0; temp < nListBatchMetadata.getLength(); temp++) {			     
								Node nNode = nListBatchMetadata.item(temp);			     
								System.out.println("\nCurrent Element :" + nNode.getNodeName());			     
								if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
									Element eElement = (Element) nNode;			     
									batchID = getValue("BatchID", eElement);
									//batchCategoryCode = getValue("BatchCategoryCode", eElement);
								
			
								}
							}
							for (int temp = 0; temp < nListServiceSpecificData.getLength(); temp++) {			     
								Node nNode = nListServiceSpecificData.item(temp);			     
								System.out.println("\nCurrent Element :" + nNode.getNodeName());			     
								if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
									Element eElement = (Element) nNode;		
									//String responseDesc = getValue("ResponseDescriptionText", eElement);
									LOGGER.info("ResponseCode: " + getValue("ResponseCode", eElement));
									responseCode = getValue("ResponseCode", eElement);
								}
						     }
							
							for (int temp = 0; temp < nListAttachment.getLength(); temp++) {			     
								Node nNode = nListAttachment.item(temp);			     
								System.out.println("\nCurrent Element :" + nNode.getNodeName());			     
								if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
									Element eElement = (Element) nNode;		
									docSeqId = getValue("DocumentSequenceID", eElement);
									
								}
						     }
							List<IRSOutboundTransmission> listIRSOutboundTransmsn= irsOutboundTransmissionRepo.findIRSOutboundTransmissionByBatchId(batchID);
							if(listIRSOutboundTransmsn != null && listIRSOutboundTransmsn.size() >0)
							{
								for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
								{
									if(irsTransFile.getDocumentFileName().contains(docSeqId)){
										irsTransFile.setIsAckReceived("Y");
										irsTransFile.setACKResponseCode(responseCode);
										try{
											irsOutboundTransmissionRepo.save(irsTransFile);
										}catch(Exception e){
											LOGGER.error("Error saving Inbound data in IRSOutBoundTransmission table @ logIRSOutBoundTransmissionInformation" , e);
										}
									}
									
								
								}							
								
							}						
						
						// move files to archive
						//String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
						String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
						ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + "EOM_ACK_FOLDER";
						String timeStamp = getDateTime();
						ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + timeStamp; 
						moveZipFiles(IRSEOMACKFilePath,ArchiveFilePayloadPath,fileType,null);
						moveZipFiles(IRSEOMACKFilePath,ArchiveFilePayloadPath,".zip",null);
						EnrollmentUtils.moveFiles(IRSEOMACKFilePath,ArchiveFilePayloadPath,fileTypeXML,null);
					      }
					
						}
					  }
					}
			}
				catch(Exception e){
				LOGGER.error("handleIRSInboundNackResponses()::"+e.getMessage(),e);
				throw new GIException("Error in handleIRSInboundNackResponses()::"+e.getMessage(),e);
		  }
				
		} 
		private void handleEOMOUTResponses() throws GIException{
			try{
				// we need add this into GI_APP_CONFIG table
			   String IRSEOMOUTFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_EOM_OUT_FILEPATH);
			   String IRSResponseHubFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_RESPONSE_HUBFILE_PATH);
				//String IRSEOMOUTFilePath = "C:/EOMTest/OUT";
			   // String IRSResponseHubFilePath = "C:/InboundIRSResponseHubFilePath";
				//String IRSResponseMarketPlacePath	="C:/InboundIRSResponseMarketPlacePath";
				String fileType = ".OUT";
				if(IRSEOMOUTFilePath != null){
					File file = new File(IRSEOMOUTFilePath);
					if(file.exists()){
						String[] renameZipFileNames = file.list();
						
						for(String name : renameZipFileNames){
							File SourceZipFile = new File(IRSEOMOUTFilePath + File.separator + name);
						    File oldzipFile =SourceZipFile;          
							// File (or directory) with new name
						    String newZipFileName =  oldzipFile + ".zip";			
						    File newZipFile = new File(newZipFileName);   // Rename file (or directory)
						    oldzipFile.renameTo(newZipFile);
						}
						String[] ZipFileNames = file.list();
						for(String name : ZipFileNames){
							File SourceZipFile = new File(IRSEOMOUTFilePath + File.separator + name);
							String destination = IRSEOMOUTFilePath;
							File destiFile = new File(destination);
							ZipHelper zippy = new ZipHelper();
							zippy.extract(SourceZipFile, destiFile);
						}
						String fileTypeXML = ".xml";
						// copy the manifest file to IRSMarketPlaceFilePath
					 File[] IRSOUTFiles=getIRSNackResponseFilesInAFolder(IRSEOMOUTFilePath,fileTypeXML);
						for(File irsFile : IRSOUTFiles){
							if(irsFile.getName() != "manifest"){
								String batchID = "";
								//String batchCategoryCode = "";
								//String responseCode = "";
								String docSeqId = "";
								String EFTFileName = "";
								String IRSGroupIdentificationNum = "";
								String month= "";
								String year = "";
								//List<String> listDocumentSeqId = null;
								DocumentBuilderFactory dbFactory = GhixUtils.getDocumentBuilderFactoryInstance();
								DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
								Document doc = dBuilder.parse(irsFile);
								doc.getDocumentElement().normalize();		     
								System.out.println("Root element :" + doc.getDocumentElement().getNodeName());  
								NodeList nListHealthExchangeErrors = doc.getElementsByTagName("ns:HealthExchangeErrors");
								NodeList nListIRSHouseholdErrorDtl = doc.getElementsByTagName("irs:IRSHouseholdErrorDtl");
								NodeList listEPDErrorDetail = null;
							 
								//NodeList nListEPDErrorDetail = doc.getElementsByTagName("irs:EPDErrorDetail");
								//NodeList nListBatchMetadata = doc.getElementsByTagName("BatchMetadata");
								//NodeList nListAttachment = doc.getElementsByTagName("Attachment");
								//get the batch id from the response xml file
								
								for (int temp = 0; temp < nListHealthExchangeErrors.getLength(); temp++) {			     
									Node nNode = nListHealthExchangeErrors.item(temp);			     
									System.out.println("\nCurrent Element :" + nNode.getNodeName());			     
									if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
										Element eElement = (Element) nNode;		
										batchID = getValue("irs:BatchId", eElement);
										docSeqId = getValue("irs:DocumentSequenceNum", eElement);
										month = getValue("irs:SubmissionMonthNum", eElement);
										year = getValue("irs:SubmissionYr", eElement);
									}
							     }
								// find EFT file name for given batch Id and Document Sequence ID
								if((batchID != null && batchID != "") && (docSeqId != null && docSeqId != "")){
									IRSOutboundTransmission irsOutboundTransmission =  null;
									irsOutboundTransmission = iRSOutboundTransmissionService.findIRSOutboundTransmissionByDocumentSeqId(docSeqId, batchID);
									
									if(irsOutboundTransmission != null){
										EFTFileName = irsOutboundTransmission.getDocumentFileName();										
									}
								}
								
								for (int temp = 0; temp < nListIRSHouseholdErrorDtl.getLength(); temp++) {			     
									Node nNode = nListIRSHouseholdErrorDtl.item(temp);			
								    System.out.println("\nCurrent Element :" + nNode.getNodeName());			     
									if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
										Element eElement = (Element) nNode;			     
										IRSGroupIdentificationNum = getValue("irs:IRSGroupIdentificationNum", eElement);
										 listEPDErrorDetail = eElement.getElementsByTagName("irs:EPDErrorDetail");
				
									}
									
									if(listEPDErrorDetail != null){
									   String errorMessageCd = "";
								      String errorMessgaeText = "";
									  String xPathContent  = "";									
									  for (int temp1 = 0; temp1 < listEPDErrorDetail.getLength(); temp1++) {	
										  IRSInboundTransmission irsInboundTransmission = new IRSInboundTransmission();
												Node nNode1 = listEPDErrorDetail.item(temp1);			
											
												if (nNode1.getNodeType() == Node.ELEMENT_NODE) {			     
													Element eElement = (Element) nNode1;			     
													errorMessageCd = getValue("irs:ErrorMessageCd", eElement);
													errorMessgaeText = getValue("irs:ErrorMessageTxt", eElement);	
													xPathContent = getValue("irs:XpathContent", eElement);		
													irsInboundTransmission.setBatchId(batchID);
													irsInboundTransmission.setDocumentSeqId(docSeqId);
													irsInboundTransmission.setEFTFileName("");
													irsInboundTransmission.setErrorMessageCd(errorMessageCd);
													irsInboundTransmission.setErrorMessageTx(errorMessgaeText);
													irsInboundTransmission.setxPathContent(xPathContent);
													irsInboundTransmission.setHouseholdCaseID(IRSGroupIdentificationNum);
													irsInboundTransmission.setMonth(month);
													irsInboundTransmission.setYear(year);
													irsInboundTransmission.setResponseType("IRS");
													irsInboundTransmission.setEFTFileName(EFTFileName);
													try{
													 irsInboundTransmissionRepo.save(irsInboundTransmission);		
													}											
													catch(Exception e){
														LOGGER.error("Error saving Inbound data in IRSInboundTransmission table @ logIRSIntBoundTransmissionInformation" , e);
													}
													
												}
																				
									       }
									 }
								 }
												
						   }
						}
							
						    String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
						   // String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
							ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + "EOM_OUT_FOLDER";
							String timeStamp = getDateTime();
							ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + timeStamp; 
							moveZipFiles(IRSEOMOUTFilePath,ArchiveFilePayloadPath,fileType,null);
							moveZipFiles(IRSEOMOUTFilePath,ArchiveFilePayloadPath,".zip",null);
							for(File irsFile : IRSOUTFiles){
								if(irsFile.getName().equalsIgnoreCase("manifest.xml")){
									EnrollmentUtils.moveFilesWithName(IRSEOMOUTFilePath,IRSResponseHubFilePath,fileTypeXML,null);
								}
								else{
									EnrollmentUtils.moveFiles(IRSEOMOUTFilePath,ArchiveFilePayloadPath,fileTypeXML,null);								
								}
							}
						
							//}
						
					}
				}
			}
		
				catch(Exception e){
				LOGGER.error("handleIRSInboundNackResponses()::"+e.getMessage(),e);
				throw new GIException("Error in handleIRSInboundNackResponses()::"+e.getMessage(),e);
				}
		}

		@Override
		public void generateFilePayloadResubmitTransmissionXML(int month,int year,String batchCategoryCode,String batchID) throws GIException{
			
			String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
			String FilePackageBatchXMLPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTMANIFESTFILEPATH);
			String directoryTobeZipped = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTZIPFOLDERPATH);
			String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
			String hubFTPZipFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTHUBFTPPATH);
			String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			/*
			  String filePayloadXMLFolderPath = "C:/Data/IRSContentFileXMLFolder";	
			  String FilePackageBatchXMLPath = "C:/Data/ManifestFileFolder";
			  String directoryTobeZipped = "C:/Data/FilePayloadZipFolderLocation";
			  String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder/";
			  String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
			  String environment = "T";*/
			FilePayloadBatchTransmission filePayloadBatchTransmission = new FilePayloadBatchTransmission();						 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			String batchDate = sdf.format(new TSDate());
		    filePayloadBatchTransmission.setBatchID(batchDate);
			filePayloadBatchTransmission.setOriginalBatchID(batchID);			
			String partnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
			String cmsPartnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_CMS_PARTNERID);
			filePayloadBatchTransmission.setBatchPartnerID(cmsPartnerID);
			File filePayloadFolder = null;
			int batchAttachmentTotalQuantity = 0;
			if(StringUtils.isNotEmpty(filePayloadXMLFolderPath))
			{
				filePayloadFolder = new File(filePayloadXMLFolderPath);
				if (!filePayloadFolder.exists()) {
					throw new GIException("File Payload XML folder doesn't exists");
				}
			}
			else
			{
				throw new GIException("Empty or null payload XML folder path");
			}
			try{

				batchAttachmentTotalQuantity = countFilesInFilePayloadFolder(filePayloadFolder);
				insertBatchIDIntoIrsOutboundTrans(filePayloadFolder,batchDate);
				filePayloadBatchTransmission.setBatchAttachmentTotalQuantity(batchAttachmentTotalQuantity);
				//setBatchTransmissionQuantity is set to 1 always since hub will expect 1 zip file at a time
				filePayloadBatchTransmission.setBatchTransmissionQuantity(1);
				filePayloadBatchTransmission.setTransmissionAttachmentQuantity(batchAttachmentTotalQuantity);
				//Transmission Sequence ID equals 1 in all cases. The Hub rejects the batch if Transmission Sequence ID is greater than 1.
				filePayloadBatchTransmission.setTransmissionSequenceID(1);
				if(month == 0)
				{
					year = year-1;
					month = EnrollmentConstants.TWELVE;
					filePayloadBatchTransmission.setReportPeriod(year + "-" + month);
				}
				else{
					int length = String.valueOf(month).length();
					if(length >= 2){
						 filePayloadBatchTransmission.setReportPeriod(year + "-" + month);
					}
					else{
						filePayloadBatchTransmission.setReportPeriod(year + "-0" + month);
					
					}
				}
				List<Attachment> listAttachment = getAllAttachments(filePayloadFolder);
				  if(listAttachment != null){
					    if((listAttachment.size() == 1) && (batchCategoryCode.equalsIgnoreCase(EnrollmentConstants.IRS_EOM_ERROR_RESP))){
					    	filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.IRS_EOM_IND_RESUBMIT_FILE_REQ);	
					    }
					    else if(batchCategoryCode.equalsIgnoreCase(EnrollmentConstants.IRS_EOM_MISSING_FILE_RESP)){
					    	if(isNotNullAndEmpty(stateCode) && stateCode.equalsIgnoreCase(STATE_CODE_ID)){
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.BATCHCATEGORYCODE_INDIVIDUAL);	
					    	}else{
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.BATCHCATEGORYCODE_SHOP);
					    	}	
					    }
					    else if((listAttachment.size() > 1) && (batchCategoryCode.equalsIgnoreCase(EnrollmentConstants.IRS_EOM_NACK_RESP))){
					    	if(isNotNullAndEmpty(stateCode) && stateCode.equalsIgnoreCase(STATE_CODE_ID)){
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.IRS_EOM_IND_RESUBMIT_BATCH_REQ);	
					    	}else{
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_BATCH_REQ);
					    	}
						   	
					    }
					    else if((listAttachment.size() == 1) && (batchCategoryCode.equalsIgnoreCase(EnrollmentConstants.IRS_EOM_NACK_RESP))){
					    	if(isNotNullAndEmpty(stateCode) && stateCode.equalsIgnoreCase(STATE_CODE_ID)){
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ);
					    	}else{
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_MISSING_FILE_REQ);
					    	}
							   	
						 }
					    else if(batchCategoryCode.equalsIgnoreCase(EnrollmentConstants.IRS_EOM_NACK_RESP)){
					    	if(isNotNullAndEmpty(stateCode) && stateCode.equalsIgnoreCase(STATE_CODE_ID)){
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.BATCHCATEGORYCODE_INDIVIDUAL);	
					    	}else{
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.BATCHCATEGORYCODE_SHOP);
					    	}
						}
					    else if((listAttachment.size() == EnrollmentConstants.FIVE) && (batchCategoryCode.equalsIgnoreCase(EnrollmentConstants.IRS_EOM_NACK_RESP))){
					    	if(isNotNullAndEmpty(stateCode) && stateCode.equalsIgnoreCase(STATE_CODE_ID)){
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.BATCHCATEGORYCODE_INDIVIDUAL);	
					    	}else{
					    		filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.BATCHCATEGORYCODE_SHOP);
					    	}	
						}
					    else if(batchCategoryCode.equalsIgnoreCase(EnrollmentConstants.IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ)){
							   filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ);	
						}
					    else{
					    	filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.IRS_EOM_IND_RESUBMIT_BATCH_REQ);
					    }
				  }
				if(listAttachment != null && (!listAttachment.isEmpty())){
					filePayloadBatchTransmission.setAttachment(listAttachment);
				}
				String batchTransmissionPath = FilePackageBatchXMLPath + File.separator + "manifest" + ".xml";
				generateFilePayloadIRSXMLReport(filePayloadBatchTransmission, batchTransmissionPath,"FilePayloadIRSReport.xsl");	
				String zipFileName = "";
				// put in GI_APP_CONFIG		
			     SimpleDateFormat dateFormat = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
				String fileCreationDateTime = dateFormat.format(new TSDate());
				zipFileName = partnerID + "."+ EnrollmentConstants.APP + "." + EnrollmentConstants.FUNC_INBOUND + "."+ fileCreationDateTime + "."+ environment + "."+ EnrollmentConstants.TRANSFER_DIRECTION_IN;
				String hubFTPZipFolderPath = hubFTPZipFilePath + File.separator + zipFileName + ".zip";
				//String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder" + zipFileName + ".zip";
				//move File Payload IRS Reporting XML
				EnrollmentUtils.moveFiles(filePayloadXMLFolderPath,directoryTobeZipped,EnrollmentConstants.FILE_TYPE_XML,null);
				//move file package batch XML file
				EnrollmentUtils.moveFiles(FilePackageBatchXMLPath,directoryTobeZipped,EnrollmentConstants.FILE_TYPE_XML,null);
				//Zip all the file content and drop into Hub FTP location
				ZipHelper zippy = new ZipHelper();
				zippy.zipDir(directoryTobeZipped,hubFTPZipFolderPath);
				// File (or directory) with old name
			    File oldzipFile = new File(hubFTPZipFolderPath);          
			    // File (or directory) with new name
			    String newZipFileName =  hubFTPZipFilePath + File.separator + zipFileName;			
			    File newZipFile = new File(newZipFileName);   // Rename file (or directory)
			    batchDate = batchDate.replace('|', '-');
				batchDate = batchDate.replace(':', '-');
				String timeStampArchiveFileFolder = ArchiveFilePayloadPath + File.separator + batchDate;
				// FileUtils.cleanDirectory(new File(directoryTobeZipped));
				EnrollmentUtils.moveFiles(directoryTobeZipped,timeStampArchiveFileFolder,EnrollmentConstants.FILE_TYPE_XML,null);
				oldzipFile.renameTo(newZipFile);
				//  deleteDir(directoryTobeZipped);
				//zipDirectory(directoryTobeZipped);
			}
			catch(Exception e){
				LOGGER.error("generateFilePayloadBatchTransmissionXML()::"+e.getMessage(),e);
				throw new GIException("Error in generateFilePayloadBatchTransmissionXML()::"+e.getMessage(),e);
			}
		}
		 private void handleIRSInboundShopNackResponses() throws GIException{
			  try{
				  // we need add this into GI_APP_CONFIG table
				   String IRSNackResponseHubFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_NACK_RESPONSE_HUBFILE_PATH);
				   String IRSNackResponseMarketPlacePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_NACK_RESPONSE_MARKETPLACE_PATH);
				/*   String IRSNackResponseHubFilePath = "C:/IRSInboundNACKResponseFolder";
				   String IRSNackResponseMarketPlacePath	="C:/IRSNackResponseMarketPlacePath";
				*/  
				   String fileType = ".OUT";
					if(IRSNackResponseHubFilePath != null && IRSNackResponseMarketPlacePath != null ){
						moveZipFiles(IRSNackResponseHubFilePath,IRSNackResponseMarketPlacePath,fileType,null);
					}
					else{
						LOGGER.error("Error in handleIRSInboundNackResponses : No IRSNackResponseHubFilePath folder structure defined ");
						throw new GIException("Error in handleIRSInboundNackResponses : No IRSNackResponseHubFilePath folder structure defined"); 
					}
					File file = new File(IRSNackResponseMarketPlacePath);
					if(file.exists()){
						String[] renameZipFileNames = file.list();
						
						for(String name : renameZipFileNames){
							File SourceZipFile = new File(IRSNackResponseMarketPlacePath + File.separator + name);
						    File oldzipFile =SourceZipFile;          
							// File (or directory) with new name
						    String newZipFileName =  oldzipFile + ".zip";			
						    File newZipFile = new File(newZipFileName);   // Rename file (or directory)
						    oldzipFile.renameTo(newZipFile);
						}
						String[] ZipFileNames = file.list();
						for(String name : ZipFileNames){
							File SourceZipFile = new File(IRSNackResponseMarketPlacePath + File.separator + name);
							String destination = IRSNackResponseMarketPlacePath;
							File destiFile = new File(destination);
							ZipHelper zippy = new ZipHelper();
							zippy.extract(SourceZipFile, destiFile);
						}
			
							
					String fileTypeXML = ".xml";
					File[] IRSNackResponseFiles=getIRSNackResponseFilesInAFolder(IRSNackResponseMarketPlacePath,fileTypeXML);
					for(File nackFile : IRSNackResponseFiles)
					{
						String batchID = "";
						String batchCategoryCode = "";
						List<String> listDocumentSeqId = null;
						DocumentBuilderFactory dbFactory = GhixUtils.getDocumentBuilderFactoryInstance();
						DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
						Document doc = dBuilder.parse(nackFile);
						doc.getDocumentElement().normalize();		     
						System.out.println("Root element :" + doc.getDocumentElement().getNodeName());     
						NodeList nListAttachment = doc.getElementsByTagName("Attachment");
						NodeList nListServiceSpecificData = doc.getElementsByTagName("ServiceSpecificData");
						NodeList nListBatchMetadata = doc.getElementsByTagName("BatchMetadata");
						//get the batch id from the response xml file
						for (int temp = 0; temp < nListBatchMetadata.getLength(); temp++) {			     
							Node nNode = nListBatchMetadata.item(temp);			     
							System.out.println("\nCurrent Element :" + nNode.getNodeName());			     
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
								Element eElement = (Element) nNode;			     
								batchID = getValue("BatchID", eElement);
								batchCategoryCode = getValue("BatchCategoryCode", eElement);
								String originalBatchID = batchID;
				    			
				    			if(originalBatchID.contains("|"))
								{
								    String parts[] = originalBatchID.split("\\|");
								     batchID = parts[0];
								      List<String> seqIdlist = new ArrayList<String>(Arrays.asList(parts));
								     seqIdlist.remove(0);
								     listDocumentSeqId = seqIdlist;
								    
								}
								else{
									batchID = originalBatchID;
									listDocumentSeqId = null;
								}
				    		

							}
						}
						// checking the Service Specific Data responses
						// checking the attachment level responses
						/*    batchID = batchID.replace('|', '-');  
					    	batchID = batchID.replace(':', '-');*/
						processAttachmentLevelShopNACKResponse(nListAttachment,batchID,batchCategoryCode,listDocumentSeqId);
						processServiceSpecificShopNACKResponse(nListServiceSpecificData,batchID,batchCategoryCode,listDocumentSeqId);
				    	
					 }
					// move files to archive
					//String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
					String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
	                ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + "InboundNACKResponseArchive";
  			        String timeStamp = getDateTime();
  			        ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + timeStamp;    		
  			        moveZipFiles(IRSNackResponseMarketPlacePath,ArchiveFilePayloadPath,fileType,null);
  			        moveZipFiles(IRSNackResponseMarketPlacePath,ArchiveFilePayloadPath,".zip",null);
  			        moveFiles(IRSNackResponseMarketPlacePath,ArchiveFilePayloadPath,fileTypeXML,null);
			    }
					else{
						LOGGER.info("Error in handleIRSInboundShopNackResponses : No IRSNackResponseHubFilePath folder structure defined ");
						//throw new GIException("Error in handleIRSInboundShopNackResponses : No IRSNackResponseHubFilePath folder structure defined"); 
					}

			  }
			  
					catch(Exception e){
						LOGGER.error(HANDLE_IRS_INBOUND_NACK_RESPONSE+e.getMessage(),e);
						throw new GIException(HANDLE_IRS_INBOUND_NACK_RESPONSE+e.getMessage(),e);
					}
				 
				 
				 
	 }
			private void processAttachmentLevelShopNACKResponse(NodeList nListAttachment,String batchID,String batchCategoryCode,List<String> listDocumentSeqId){
				try{	

					if(nListAttachment != null){
						for (int temp = 0; temp < nListAttachment.getLength(); temp++) {			     
							Node nNode = nListAttachment.item(temp);			     
							LOGGER.info("\nCurrent Element :" + nNode.getNodeName());			     
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
								Element eElement = (Element) nNode;		
								String fileName = getValue("Q1:DocumentFileName", eElement);
								String responseDesc = getValue("ResponseDescriptionText", eElement);
								LOGGER.info("DocumentFileName : " + fileName);
								LOGGER.info("DocumentSequenceID: " + getValue("Q1:DocumentSequenceID", eElement));
								LOGGER.info("ResponseDescriptionText: " + responseDesc);
								LOGGER.info("ResponseCode: " + getValue("ResponseCode", eElement));
								String responseCode = getValue("ResponseCode", eElement);
								IRSInboundTransmission  irsInboundTransmission = new IRSInboundTransmission();
								irsInboundTransmission.setBatchId(batchID);
								irsInboundTransmission.setEFTFileName(fileName);
								irsInboundTransmission.setResponseType("NACK");
								//irsInboundTransmission.setYear(year);
								irsInboundTransmission.setHubResponseCode(responseCode);
								irsInboundTransmission.setHubResponseDesc(responseDesc);
								try{
								irsInboundTransmissionRepo.saveAndFlush(irsInboundTransmission);		
								}											
								catch(Exception e){
									LOGGER.error("Error saving Inbound data in IRSInboundTransmission table @ logIRSIntBoundTransmissionInformation" , e);
								}
								if(responseCode.equalsIgnoreCase("HE007000")){
									if(isNotNullAndEmpty(batchID)){
										int submissionMonth = 0;
										int submissionYear = 0;
										// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
										//HE007000 -Regenerate the entire file
										//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
										//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
										try{
					    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
					    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
					    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
					    						String householdCaseId = irsTransFile.getHouseholdCaseID();
					    						//split the housholdcaseID and pass it to the ind portal rest API call
					    						String [] arrHouseholdCaseId = householdCaseId.split(",");
					    					    List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
					    						if(irsTransFile.getMonth() != null){
					    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
					    						 submissionMonth = submissionMonth-1;
					    						}
					    						if(irsTransFile.getYear()!= null){
					    							submissionYear = Integer.parseInt(irsTransFile.getYear());
						    					}
					    						listHouseholdCaseId.addAll(newListHouseholdCaseId);
					    					 }
					    					   List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
												enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);
												generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_BATCH_REQ,batchID);
											}
										
										catch(Exception ex){
											LOGGER.error(ex.getMessage(), ex);
										}
									}
								}
								//Regenerate the manifest file
								if(responseCode.equalsIgnoreCase("HE007104") || responseCode.equalsIgnoreCase("HE007105") ||responseCode.equalsIgnoreCase("HE007107")
										||responseCode.equalsIgnoreCase("HE007110") || responseCode.equalsIgnoreCase("HE007113") ){
									if(isNotNullAndEmpty(batchID)){
										String originalBatchID = batchID;
										// get the files from archive folder with same name as batchID
										//String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
										String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
										batchID = batchID.replace('|', '-');
										batchID = batchID.replace(':', '-');
										ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + batchID;
										//	String filePayloadXMLFolderPath = "C:/Data/IRSContentFileXMLFolder";
										String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
										// move files from archive to filePayloadXMLFolderPath to recreate the manifest file
										deleteDir(ArchiveFilePayloadPath + File.separator +"ResentFile_batchID");
										File srcFile = new File(ArchiveFilePayloadPath);
										File destDir = new File(filePayloadXMLFolderPath);
										FileUtils.copyDirectory(srcFile, destDir);
										File manifestFile = new File(filePayloadXMLFolderPath  + File.separator + "manifest.xml");
										// delete the old manifest file from the filePayloadXMLFolderPath
										manifestFile.delete();
										Calendar calObj = TSCalendar.getInstance();
										int currentYear = calObj.get(Calendar.YEAR);
										int currentMonth = calObj.get(Calendar.MONTH);
										generateFilePayloadResubmitTransmissionXML(currentMonth,currentYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_BATCH_REQ,originalBatchID);
										//  enrollmentEmailNotificationService
										// create new manifest file and package the folder
									}	
								}
								//Manually fix the manifest file
								if(responseCode.equalsIgnoreCase("HE007106")) {
									StringBuilder archiveFilePayloadPath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
									batchID = batchID.replace('|', '-');
									batchID = batchID.replace(':', '-');
									String action = "Manually fix Batch Partner ID in the manifest file and resend again to IRS";
									String manifestFilePath = archiveFilePayloadPath.append(File.separator).append(batchID).toString();
									enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "manifest.xml", manifestFilePath, action);
								}
								if(responseCode.equalsIgnoreCase("HX005005") 
										|| responseCode.equalsIgnoreCase("HX005001") 
										|| responseCode.equalsIgnoreCase("HE009000")
										|| responseCode.equalsIgnoreCase("HX009000")
										|| responseCode.equalsIgnoreCase("HX025015")
										|| responseCode.equalsIgnoreCase("HX026000")
										|| responseCode.equalsIgnoreCase("HX026301")) {
									StringBuilder archivefolderPayloadPath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
									String oldBatchId = batchID;
									batchID = batchID.replace('|', '-');
									batchID = batchID.replace(':', '-');
									String action = "The last IRS Report file package was rejected by CMS. The IRS Shop Inbound batch job will automatically resubmit the file package. Please verify that file package with batch ID" + oldBatchId +"is resubmitted.";
									String manifestFilePath = archivefolderPayloadPath.append(File.separator).append(batchID).toString();
									enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "ArchiveFolderPath", manifestFilePath, action);
								}
								if(responseCode.equalsIgnoreCase("HE007000")) {
									StringBuilder archivePath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
									String oldBatchId = batchID;
									batchID = batchID.replace('|', '-');
									batchID = batchID.replace(':', '-');
									String action = "The last IRS Report file package was rejected by CMS. The IRS Shop Inbound batch job will automatically Regenerate the file package. Please verify that file package with original batch ID" + oldBatchId +"is resubmitted.";
									String manifestFilePath = archivePath.append(File.separator).append(batchID).toString();
									enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "manifest.xml", manifestFilePath, action);
				    			}
							 if(responseCode.equalsIgnoreCase("HE007111")){
				    				if(isNotNullAndEmpty(batchID)){
				    					int submissionMonth = 0;
			    						int submissionYear = 0;
			    						String householdCaseId ="";
				    					// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
				    					//HE007000 -Regenerate the entire file
				    					//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
				    					//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
				    					try{
					    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
					    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
					    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
					    						householdCaseId = "";
					    						if(listDocumentSeqId != null){				    							
						    						String ducumentFileName = irsTransFile.getDocumentFileName();
						    						for(String docSeqID : listDocumentSeqId)
						    						{
						    						
							    						if(ducumentFileName.contains(docSeqID))
							    						{
							    							 householdCaseId = irsTransFile.getHouseholdCaseID();
							    						}
							    					}
				    						  }
						    				
				    						  else{
						    						  householdCaseId = irsTransFile.getHouseholdCaseID();
						    					  }
						    				
					    						//split the housholdcaseID and pass it to the ind portal rest API call
					    						String [] arrHouseholdCaseId = householdCaseId.split(",");
					    					   List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
					    						if(irsTransFile.getMonth() != null){
					    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
					    						 submissionMonth = submissionMonth-1;
					    						}
					    						if(irsTransFile.getYear()!= null){
					    							submissionYear = Integer.parseInt(irsTransFile.getYear());
						    					}
					    						if(newListHouseholdCaseId != null && newListHouseholdCaseId.size() > 0)
					    						{
					    						 listHouseholdCaseId.addAll(newListHouseholdCaseId);
					    						}
					    					 }
					    					
					    				    List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
											enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);
											generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_BATCH_REQ,batchID);
				    					}
				    					catch(Exception ex){
				    						LOGGER.error(ex.getMessage(), ex);
				    					}
				    								    				      
				    				}
				    				
				    			}
				    		
							 //HX001103 -Resend the same file again
								if(responseCode.equalsIgnoreCase("HX001103")
										||responseCode.equalsIgnoreCase("HX005005") 
										|| responseCode.equalsIgnoreCase("HX005001") 
										|| responseCode.equalsIgnoreCase("HE009000")
										|| responseCode.equalsIgnoreCase("HX009000")
										|| responseCode.equalsIgnoreCase("HX025015")
										|| responseCode.equalsIgnoreCase("HX026000")
										|| responseCode.equalsIgnoreCase("HX026301")){										
		
									if(isNotNullAndEmpty(batchID)){
										// get the files from archive folder with same name as batchID
										//String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
										String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
										batchID = batchID.replace('|', '-');
										batchID = batchID.replace(':', '-');
										ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + batchID;
										//String filePayloadXMLFolderPath = "C:/Data/IRSContentFileXMLFolder";
										String hubFTPZipFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTHUBFTPPATH);
										String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);
										//String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
										// move files from archive to filePayloadXMLFolderPath to recreate the manifest file
										//String directoryTobeZipped = "C:/Data/FilePayloadZipFolderLocation";
										String directoryTobeZipped = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTZIPFOLDERPATH);
										//  String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder/";
										// String environment = "T";
										deleteDir(ArchiveFilePayloadPath + File.separator +"ResentFile_batchID");
										File srcFile = new File(ArchiveFilePayloadPath);
										File destDir = new File(directoryTobeZipped);
										FileUtils.copyDirectory(srcFile, destDir);
									     SimpleDateFormat dateFormat = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
										String fileCreationDateTime = dateFormat.format(new TSDate());
										String partnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
										String zipFileName = partnerID + "."+ EnrollmentConstants.APP + "." + EnrollmentConstants.FUNC_INBOUND + "."+ fileCreationDateTime + "."+ environment + "."+ EnrollmentConstants.TRANSFER_DIRECTION_IN;
										String hubFTPZipFolderPath = hubFTPZipFilePath + File.separator+ zipFileName + ".zip";
										//String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder" + zipFileName + ".zip";

										//Zip all the file content and drop into Hub FTP location
										ZipHelper zippy = new ZipHelper();
										zippy.zipDir(directoryTobeZipped,hubFTPZipFolderPath); 
										File oldzipFile = new File(hubFTPZipFolderPath);          
										    // File (or directory) with new name
									    String newZipFileName =  hubFTPZipFilePath + File.separator + zipFileName;			
										File newZipFile = new File(newZipFileName);
										//String timeStamp = getDateTime();s
										//set the archive folder name same as batch ID
										String timeStampArchiveFileFolder = ArchiveFilePayloadPath + File.separator + "ResentFile_batchID";
										// FileUtils.cleanDirectory(new File(directoryTobeZipped));
										EnrollmentUtils.moveFiles(directoryTobeZipped,timeStampArchiveFileFolder,EnrollmentConstants.FILE_TYPE_XML,null);
										oldzipFile.renameTo(newZipFile);
									}	
								}		    				
							}
						}
					}
				}
				catch(Exception ex){
					LOGGER.error(ex.getMessage(),ex);
				}
			}
	private void processServiceSpecificShopNACKResponse(NodeList nListServiceSpecificData,String batchID,String batchCategoryCode,List<String> listDocumentSeqId)
	{
		 try{
			 if(nListServiceSpecificData != null){
			    	for (int temp = 0; temp < nListServiceSpecificData.getLength(); temp++) {			     
			    		Node nNode = nListServiceSpecificData.item(temp);			     
			    		LOGGER.info("\nCurrent Element :" + nNode.getNodeName());			     
			    		if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
			    			Element eElement = (Element) nNode;		
			    			String responseDesc = getValue("ResponseDescriptionText", eElement);
			    			LOGGER.info("ResponseCode: " + getValue("ResponseCode", eElement));
			    			String responseCode = getValue("ResponseCode", eElement);
			    			   // Log NACK response details to IRS_Inbound_Transmission table
							IRSInboundTransmission  irsInboundTransmission = new IRSInboundTransmission();
							irsInboundTransmission.setBatchId(batchID);
							irsInboundTransmission.setResponseType("NACK");
							//irsInboundTransmission.setYear(year);
							irsInboundTransmission.setHubResponseCode(responseCode);
							irsInboundTransmission.setHubResponseDesc(responseDesc);
							try{
							irsInboundTransmissionRepo.saveAndFlush(irsInboundTransmission);		
							}											
							catch(Exception e){
								LOGGER.error("Error saving Inbound data in IRSInboundTransmission table @ logIRSIntBoundTransmissionInformation" , e);
							}
			    			if(responseCode.equalsIgnoreCase("HE007000")){
								if(isNotNullAndEmpty(batchID)){
									int submissionMonth = 0;
									int submissionYear = 0;
									// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
									//HE007000 -Regenerate the entire file
									//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
									//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
									try{
				    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
				    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
				    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
				    						String householdCaseId = irsTransFile.getHouseholdCaseID();
				    						//split the housholdcaseID and pass it to the ind portal rest API call
				    						String [] arrHouseholdCaseId = householdCaseId.split(",");
				    					    List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
				    						if(irsTransFile.getMonth() != null){
				    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
				    						 submissionMonth = submissionMonth-1;
				    						}
				    						if(irsTransFile.getYear()!= null){
				    							submissionYear = Integer.parseInt(irsTransFile.getYear());
					    					}
				    						listHouseholdCaseId.addAll(newListHouseholdCaseId);
				    					 }
				    					   List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
											enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);
											generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_BATCH_REQ,batchID);
										}
									
									catch(Exception ex){
										LOGGER.error(ex.getMessage(), ex);
									}
								}
							}
							//Regenerate the manifest file
							if(responseCode.equalsIgnoreCase("HE007104") || responseCode.equalsIgnoreCase("HE007105") ||responseCode.equalsIgnoreCase("HE007107")
									||responseCode.equalsIgnoreCase("HE007110") || responseCode.equalsIgnoreCase("HE007113") ){
								if(isNotNullAndEmpty(batchID)){
									String originalBatchID = batchID;
									// get the files from archive folder with same name as batchID
									//String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
									String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
									batchID = batchID.replace('|', '-');
									batchID = batchID.replace(':', '-');
									ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + batchID;
									//	String filePayloadXMLFolderPath = "C:/Data/IRSContentFileXMLFolder";
									String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
									// move files from archive to filePayloadXMLFolderPath to recreate the manifest file
								    deleteDir(ArchiveFilePayloadPath + File.separator +"ResentFile_batchID");
									File srcFile = new File(ArchiveFilePayloadPath);
									File destDir = new File(filePayloadXMLFolderPath);
									FileUtils.copyDirectory(srcFile, destDir);
									File manifestFile = new File(filePayloadXMLFolderPath  + File.separator + "manifest.xml");
									// delete the old manifest file from the filePayloadXMLFolderPath
									manifestFile.delete();
									Calendar calObj = TSCalendar.getInstance();
									int currentYear = calObj.get(Calendar.YEAR);
									int currentMonth = calObj.get(Calendar.MONTH);
									generateFilePayloadResubmitTransmissionXML(currentMonth,currentYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_BATCH_REQ,originalBatchID);
									//  enrollmentEmailNotificationService
									// create new manifest file and package the folder
								}	
							}
							//Manually fix the manifest file
							if(responseCode.equalsIgnoreCase("HE007106")) {
								StringBuilder archiveFilePayloadPath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
								batchID = batchID.replace('|', '-');
								batchID = batchID.replace(':', '-');
								String action = "Manually fix Batch Partner ID in the manifest file and resend again to IRS";
								String manifestFilePath = archiveFilePayloadPath.append(File.separator).append(batchID).toString();
								enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "manifest.xml", manifestFilePath, action);
							}
							if(responseCode.equalsIgnoreCase("HX005005") 
									|| responseCode.equalsIgnoreCase("HX005001") 
									|| responseCode.equalsIgnoreCase("HE009000")
									|| responseCode.equalsIgnoreCase("HX009000")
									|| responseCode.equalsIgnoreCase("HX025015")
									|| responseCode.equalsIgnoreCase("HX026000")
									|| responseCode.equalsIgnoreCase("HX026301")) {
								StringBuilder archivefolderPayloadPath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
								String oldBatchId = batchID;
								batchID = batchID.replace('|', '-');
								batchID = batchID.replace(':', '-');
								String action = "The last IRS Report file package was rejected by CMS. The IRS Shop Inbound batch job will automatically resubmit the file package. Please verify that file package with batch ID" + oldBatchId +"is resubmitted.";
								String manifestFilePath = archivefolderPayloadPath.append(File.separator).append(batchID).toString();
								enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "ArchiveFolderPath", manifestFilePath, action);
							}
							if(responseCode.equalsIgnoreCase("HE007000")) {
								StringBuilder archivePath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
								String oldBatchId = batchID;
								batchID = batchID.replace('|', '-');
								batchID = batchID.replace(':', '-');
								String action = "The last IRS Report file package was rejected by CMS. The IRS Shop Inbound batch job will automatically Regenerate the file package. Please verify that file package with original batch ID" + oldBatchId +"is resubmitted.";
								String manifestFilePath = archivePath.append(File.separator).append(batchID).toString();
								enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "manifest.xml", manifestFilePath, action);
			    			}
							if(responseCode.equalsIgnoreCase("HE029028") || responseCode.equalsIgnoreCase("HE029078")){
			    				if(isNotNullAndEmpty(batchID)){
			    					int submissionMonth = 0;
		    						int submissionYear = 0;
		    						String householdCaseId ="";
			    					// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
			    					//HE007000 -Regenerate the entire file
			    					//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
			    					//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
			    					try{
				    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
				    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
				    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
				    						householdCaseId = "";
				    						if(listDocumentSeqId != null){
					    						String ducumentFileName = irsTransFile.getDocumentFileName();
					    						for(String docSeqID : listDocumentSeqId)
					    						{
					    						
						    						if(ducumentFileName.contains(docSeqID))
						    						{
						    							 householdCaseId = irsTransFile.getHouseholdCaseID();
						    						}
						    					}
			    						  }
					    				
			    						  else{
					    						  householdCaseId = irsTransFile.getHouseholdCaseID();
					    					  }
					    				
				    						//split the housholdcaseID and pass it to the ind portal rest API call
				    						String [] arrHouseholdCaseId = householdCaseId.split(",");
				    					   List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
				    						if(irsTransFile.getMonth() != null){
				    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
				    						 submissionMonth = submissionMonth-1;
				    						}
				    						if(irsTransFile.getYear()!= null){
				    							submissionYear = Integer.parseInt(irsTransFile.getYear());
					    					}
				    						if(newListHouseholdCaseId != null && newListHouseholdCaseId.size() > 0)
				    						{
				    						 listHouseholdCaseId.addAll(newListHouseholdCaseId);
				    						}
				    					 }
				    					List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
										enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);		    						// package the xml file
				    				generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ,batchID);
			    					}
			    					catch(Exception ex){
			    						LOGGER.error(ex.getMessage(), ex);
			    					}
			    								    				      
			    				}
			    				
			    			}
			    		
			    		
						 if(responseCode.equalsIgnoreCase("HE007111")){
			    				if(isNotNullAndEmpty(batchID)){
			    					int submissionMonth = 0;
		    						int submissionYear = 0;
		    						String householdCaseId ="";
			    					// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
			    					//HE007000 -Regenerate the entire file
			    					//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
			    					//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
			    					try{
				    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
				    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
				    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
				    						householdCaseId = "";
				    						if(listDocumentSeqId != null){
					    						String ducumentFileName = irsTransFile.getDocumentFileName();
					    						for(String docSeqID : listDocumentSeqId)
					    						{
					    						
						    						if(ducumentFileName.contains(docSeqID))
						    						{
						    							 householdCaseId = irsTransFile.getHouseholdCaseID();
						    						}
						    					}
			    						  }
					    				
			    						  else{
					    						  householdCaseId = irsTransFile.getHouseholdCaseID();
					    					  }
					    				
				    						//split the housholdcaseID and pass it to the ind portal rest API call
				    						String [] arrHouseholdCaseId = householdCaseId.split(",");
				    					   List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
				    						if(irsTransFile.getMonth() != null){
				    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
				    						 submissionMonth = submissionMonth-1;
				    						}
				    						if(irsTransFile.getYear()!= null){
				    							submissionYear = Integer.parseInt(irsTransFile.getYear());
					    					}
				    						if(newListHouseholdCaseId != null && newListHouseholdCaseId.size() > 0)
				    						{
				    						 listHouseholdCaseId.addAll(newListHouseholdCaseId);
				    						}
				    					 }
				    					List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
										enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);
										generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,batchCategoryCode,batchID);
				    					
			    					}
			    					catch(Exception ex){
			    						LOGGER.error(ex.getMessage(), ex);
			    					}
			    								    				      
			    				}
			    				
			    			}
			    		
							//HX001103 -Resend the same file again
						 if(responseCode.equalsIgnoreCase("HX001103")
									||responseCode.equalsIgnoreCase("HX005005") 
									|| responseCode.equalsIgnoreCase("HX005001") 
									|| responseCode.equalsIgnoreCase("HE009000")
									|| responseCode.equalsIgnoreCase("HX009000")
									|| responseCode.equalsIgnoreCase("HX025015")
									|| responseCode.equalsIgnoreCase("HX026000")
									|| responseCode.equalsIgnoreCase("HX026301")){		
										
								if(isNotNullAndEmpty(batchID)){
									// get the files from archive folder with same name as batchID
									//String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
									String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
	 								batchID = batchID.replace('|', '-');
	 								batchID = batchID.replace(':', '-');
	 								ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + batchID;
									//Strin g filePayloadXMLFolderPath = "C:/Data/IRSContentFileXMLFolder";
									String hubFTPZipFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTHUBFTPPATH);
	 								String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);
									//String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
									// move files from archive to filePayloadXMLFolderPath to recreate the manifest file
									//String directoryTobeZipped = "C:/Data/FilePayloadZipFolderLocation";
	 								String directoryTobeZipped = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTZIPFOLDERPATH);
	 								//String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder";
									// String environment = "T"; 
	 								deleteDir(ArchiveFilePayloadPath + File.separator +"ResentFile_batchID");
	 								File srcFile = new File(ArchiveFilePayloadPath);
	 								File destDir = new File(directoryTobeZipped);
	 								FileUtils.copyDirectory(srcFile, destDir);
	 							     SimpleDateFormat dateFormat = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
	 								String fileCreationDateTime = dateFormat.format(new TSDate());
	 								String partnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
									String zipFileName = partnerID + "."+ EnrollmentConstants.APP + "." + EnrollmentConstants.FUNC_INBOUND + "."+ fileCreationDateTime + "."+ environment + "."+ EnrollmentConstants.TRANSFER_DIRECTION_IN;
	 								String hubFTPZipFolderPath = hubFTPZipFilePath + File.separator+ zipFileName + ".zip";
									//String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder" + zipFileName + ".zip";

									//Zip all the file content and drop into Hub FTP location
	 								ZipHelper zippy = new ZipHelper();
									zippy.zipDir(directoryTobeZipped,hubFTPZipFolderPath); 
									File oldzipFile = new File(hubFTPZipFolderPath);          
								    // File (or directory) with new name
	 						        String newZipFileName =  hubFTPZipFilePath + File.separator + zipFileName;			
								    File newZipFile = new File(newZipFileName);
									//String timeStamp = getDateTime();s
									//set the archive folder name same as batch ID
	 								String timeStampArchiveFileFolder = ArchiveFilePayloadPath + File.separator + "ResentFile_batchID";
									// FileUtils.cleanDirectory(new File(directoryTobeZipped));
	 								EnrollmentUtils.moveFiles(directoryTobeZipped,timeStampArchiveFileFolder,EnrollmentConstants.FILE_TYPE_XML,null);
									oldzipFile.renameTo(newZipFile);
								}	
		
							}		    				
						}
					}
				}
			}
			catch(Exception ex){
				LOGGER.error(ex.getMessage(),ex);
			}

		}
	private void handleIRSInboundIRSShopResponses() throws GIException{
		  try{
			  // we need add this into GI_APP_CONFIG table
			    String IRSResponseHubFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_RESPONSE_HUBFILE_PATH);
			    String IRSResponseMarketPlacePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_RESPONSE_MARKETPLACE_PATH);
				/*String IRSResponseHubFilePath = "C:/IRSInboundNACKResponseFolder";
			    String IRSResponseMarketPlacePath	="C:/IRSNackResponseMarketPlacePath";*/
			    String fileType = ".OUT";
				if(IRSResponseHubFilePath != null && IRSResponseMarketPlacePath != null ){
					moveZipFiles(IRSResponseHubFilePath,IRSResponseMarketPlacePath,fileType,null);
				}
				else{
					LOGGER.error("Error in handleIRSInboundIRSShopResponses : No IRSNackResponseHubFilePath folder structure defined ");
					throw new GIException("Error in handleIRSInboundIRSShopResponses : No IRSNackResponseHubFilePath folder structure defined"); 
				}
				File file = new File(IRSResponseMarketPlacePath);
				if(file.exists()){
					String[] renameZipFileNames = file.list();
					
					for(String name : renameZipFileNames){
						File SourceZipFile = new File(IRSResponseMarketPlacePath + File.separator + name);
					    File oldzipFile =SourceZipFile;          
						// File (or directory) with new name
					    String newZipFileName =  oldzipFile + ".zip";			
					    File newZipFile = new File(newZipFileName);   // Rename file (or directory)
					    oldzipFile.renameTo(newZipFile);
					}
					String[] ZipFileNames = file.list();
					for(String name : ZipFileNames){
						File SourceZipFile = new File(IRSResponseMarketPlacePath + File.separator + name);
						String destination = IRSResponseMarketPlacePath;
						File destiFile = new File(destination);
						ZipHelper zippy = new ZipHelper();
						zippy.extract(SourceZipFile, destiFile);
					}
						
				String fileTypeXML = ".xml";
				File[] IRSResponseFiles=getIRSNackResponseFilesInAFolder(IRSResponseMarketPlacePath,fileTypeXML);
				for(File irsFile : IRSResponseFiles)
				{
					String batchID = "";
					String batchCategoryCode = "";
					List<String> listDocumentSeqId = null;
			    	DocumentBuilderFactory dbFactory = GhixUtils.getDocumentBuilderFactoryInstance();
			    	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			    	Document doc = dBuilder.parse(irsFile);
			     	doc.getDocumentElement().normalize();		     
			       	LOGGER.info(ROOT_ELEMENT + doc.getDocumentElement().getNodeName());
			      	NodeList nListServiceSpecificData = doc.getElementsByTagName("ext:ServiceSpecificData");
			    	NodeList nListAttachment = doc.getElementsByTagName("ext:Attachment");
			     	NodeList nListBatchMetadata = doc.getElementsByTagName("hix:BatchMetadata");
			    	//get the batch id from the response xml file
			    	for (int temp = 0; temp < nListBatchMetadata.getLength(); temp++) {			     
			    		Node nNode = nListBatchMetadata.item(temp);			     
			    	//	System.out.println(CURRENT_ELEMENT + nNode.getNodeName());
			    		LOGGER.info(CURRENT_ELEMENT + nNode.getNodeName());
			    		if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
			    			Element eElement = (Element) nNode;			     
			    			batchID = getValue("hix:BatchID", eElement);
			    			batchCategoryCode = getValue("ext:BatchCategoryCode", eElement);
			    						    			     
			    		}
			    	}
			    	// checking the Service Specific Data responses
			    	/*if(batchID.contains("|"))
			    	{
			    	    String parts[] = batchID.split("|");
			    	    batchID = parts[0];
			    	}
			     	batchID = batchID.replace(':', '-');*/
			    	String originalBatchID = batchID;
	    			
	    			if(originalBatchID.contains("|"))
					{
					    String parts[] = originalBatchID.split("\\|");
					     batchID = parts[0];
					     List<String> seqIdlist = new ArrayList<String>(Arrays.asList(parts));
					     seqIdlist.remove(0);
					     listDocumentSeqId = seqIdlist;
					    
					}
					else{
						batchID = originalBatchID;
						listDocumentSeqId = null;
					}
	    			processAttachmentLevelShopIRSResponse(nListAttachment,batchID,batchCategoryCode,listDocumentSeqId);
			    	processServiceSpecificShopIRSResponse(nListServiceSpecificData,batchID,batchCategoryCode,listDocumentSeqId);
				 }
				// move files to archive
			   //String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
				String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
				ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + "InboundIRSResponseArchive";
				String timeStamp = getDateTime();
				ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + timeStamp; 
			    moveZipFiles(IRSResponseMarketPlacePath,ArchiveFilePayloadPath,fileType,null);
			    moveZipFiles(IRSResponseMarketPlacePath,ArchiveFilePayloadPath,".zip",null);
		        moveFiles(IRSResponseMarketPlacePath,ArchiveFilePayloadPath,fileTypeXML,null);
				}
				else{
					LOGGER.info("Error in handleIRSInboundIRSShopResponses : No IRSResponseHubFilePath folder structure defined ");
					//throw new GIException("Error in handleIRSInboundIRSShopResponses : No IRSResponseHubFilePath folder structure defined"); 
				 }
		  }
				catch(Exception e){
					LOGGER.error(HANDLE_IRS_INBOUND_NACK_RESPONSE+e.getMessage(),e);
					throw new GIException(HANDLE_IRS_INBOUND_NACK_RESPONSE+e.getMessage(),e);
				}
			 
		
		
		
	}
private List<Integer> getIntegerArray(List<String> stringArray) {
    List<Integer> result = new ArrayList<Integer>();
    for(String stringValue : stringArray) {
        try {
            //Convert String to Integer, and store it into integer array list.
            result.add(Integer.parseInt(stringValue));
        } catch(NumberFormatException ex) {
       			LOGGER.error(ex.getMessage());              
        } 
    }       
    return result;
}
private void processServiceSpecificShopIRSResponse(NodeList nListServiceSpecificData,String batchID,String batchCategoryCode,List<String> listDocumentSeqId)
{
	 try{
		 String action1 = "Please fix the issue in 48 hours and resend the files to IRS";
		 String action2 = "IRS File Package with Batch ID" + batchID + "Succesfully Processed by IRS";
		 StringBuilder archiveFilePayloadPath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
		 if(nListServiceSpecificData != null){
		    	for (int temp = 0; temp < nListServiceSpecificData.getLength(); temp++) {			     
		    		Node nNode = nListServiceSpecificData.item(temp);			     
		    	//	System.out.println(CURRENT_ELEMENT + nNode.getNodeName());			     
		    		LOGGER.info(CURRENT_ELEMENT + nNode.getNodeName());
		    		if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
		    			Element eElement = (Element) nNode;		
		    			String responseDesc = getValue("hix:ResponseDescriptionText", eElement);
		    			LOGGER.info("hix:ResponseCode: " + getValue("hix:ResponseCode", eElement));
		    			String responseCode = getValue("hix:ResponseCode", eElement);
		    			   // Log NACK response details to IRS_Inbound_Transmission table
						IRSInboundTransmission  irsInboundTransmission = new IRSInboundTransmission();
						irsInboundTransmission.setBatchId(batchID);
						//irsInboundTransmission.setEFTFileName(fileName);
						irsInboundTransmission.setResponseType("IRS");
						//irsInboundTransmission.setYear(year);
						irsInboundTransmission.setHubResponseCode(responseCode);
						irsInboundTransmission.setHubResponseDesc(responseDesc);
						try{
						irsInboundTransmissionRepo.saveAndFlush(irsInboundTransmission);		
						}											
						catch(Exception e){
							LOGGER.error("Error saving Inbound data in IRSInboundTransmission table @ logIRSIntBoundTransmissionInformation" , e);
						}
		    			//Re-generate the XML package
		    			if(responseCode.equalsIgnoreCase("HE029074") || responseCode.equalsIgnoreCase("HX026301") || responseCode.equalsIgnoreCase("HX026503")||responseCode.equalsIgnoreCase("HE029001")
								||responseCode.equalsIgnoreCase("HE029020")
								||responseCode.equalsIgnoreCase("HE007000")
								||responseCode.equalsIgnoreCase("HE0290025")
								||responseCode.equalsIgnoreCase("HE029027")
								||responseCode.equalsIgnoreCase("HE029035")
								||responseCode.equalsIgnoreCase("HE029051")
								||responseCode.equalsIgnoreCase("HE029070")
								||responseCode.equalsIgnoreCase("HE029075")
								||responseCode.equalsIgnoreCase("HE029077")
								||responseCode.equalsIgnoreCase("HE029081")
								||responseCode.equalsIgnoreCase("HE029085")
								||responseCode.equalsIgnoreCase("HX026503")
								||responseCode.equalsIgnoreCase("HE029085")
								||responseCode.equalsIgnoreCase("HE029085")){
		    				if(isNotNullAndEmpty(batchID)){
								int submissionMonth = 0;
								int submissionYear = 0;
								String householdCaseId ="";
								// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
								//HE007000 -Regenerate the entire file
								//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
								//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
								try{
			    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
			    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
			    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
			    						householdCaseId = "";
			    						if(listDocumentSeqId != null){
				    						String ducumentFileName = irsTransFile.getDocumentFileName();
				    						for(String docSeqID : listDocumentSeqId)
				    						{
				    						
					    						if(ducumentFileName.contains(docSeqID))
					    						{
					    							 householdCaseId = irsTransFile.getHouseholdCaseID();
					    						}
					    					}
		    						  }
				    				
		    						  else{
				    						  householdCaseId = irsTransFile.getHouseholdCaseID();
				    					  }
				    				
			    						//split the housholdcaseID and pass it to the ind portal rest API call
			    						String [] arrHouseholdCaseId = householdCaseId.split(",");
			    					   List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
			    						if(irsTransFile.getMonth() != null){
			    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
			    						 submissionMonth = submissionMonth-1;
			    						}
			    						if(irsTransFile.getYear()!= null){
			    							submissionYear = Integer.parseInt(irsTransFile.getYear());
				    					}
			    						if(newListHouseholdCaseId != null && newListHouseholdCaseId.size() > 0)
			    						{
			    						 listHouseholdCaseId.addAll(newListHouseholdCaseId);
			    						}
			    					 }
			    					List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
									enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);	
			    				    if(listDocumentSeqId != null && listDocumentSeqId.size() > 0)
			    				    {
									  generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_FILE_REQ ,batchID);
			    				    
									}
			    				    else{
			    				    	 generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_BATCH_REQ,batchID);
			    				    }
								}
		    					catch(Exception ex){
		    						LOGGER.error(ex.getMessage(), ex);
		    					}
		    								    				      
		    				}
		    				
		    			}
		    			//generate Email notification
		    			if(responseCode.equalsIgnoreCase("HE029051") ||
		    					responseCode.equalsIgnoreCase("HE029052")   || responseCode.equalsIgnoreCase("HE029053")
		    					||responseCode.equalsIgnoreCase("HE029054") || responseCode.equalsIgnoreCase("HE029057") 
		    					||responseCode.equalsIgnoreCase("HE029058") || responseCode.equalsIgnoreCase("HE029059") 
		    					||responseCode.equalsIgnoreCase("HE029060") || responseCode.equalsIgnoreCase("HE029061") 
		    					||responseCode.equalsIgnoreCase("HE029062") || responseCode.equalsIgnoreCase("HE029065") 
		    					||responseCode.equalsIgnoreCase("HE029066") || responseCode.equalsIgnoreCase("HE029067") 
		    					||responseCode.equalsIgnoreCase("HE029068") || responseCode.equalsIgnoreCase("HE029069") 
		    					||responseCode.equalsIgnoreCase("HE029070") || responseCode.equalsIgnoreCase("HE029072")
		    					|| responseCode.equalsIgnoreCase("HE029076") || responseCode.equalsIgnoreCase("HE029078")
		    					|| responseCode.equalsIgnoreCase("HE029080") || responseCode.equalsIgnoreCase("HE029082")
		    					|| responseCode.equalsIgnoreCase("HE029084") || responseCode.equalsIgnoreCase("HE029085")
		    					|| responseCode.equalsIgnoreCase("HE029999") ||responseCode.equalsIgnoreCase("HE029031")
		    					||responseCode.equalsIgnoreCase("HE029022")||responseCode.equalsIgnoreCase("HE029023")
		    					||responseCode.equalsIgnoreCase("HE029029") ||responseCode.equalsIgnoreCase("HE029030")){
		    				//	 archiveFilePayloadPath = archiveFilePayloadPath. + File.separator + batchID;
		    				     archiveFilePayloadPath.append(File.separator).append(batchID);
		    					 String manifestFilePath =archiveFilePayloadPath.toString();
	    						if(responseCode.equalsIgnoreCase("HE029022") || responseCode.equalsIgnoreCase("HE029023")
									||responseCode.equalsIgnoreCase("HE029029") || responseCode.equalsIgnoreCase("HE029030")){
	    							action1 = "Marketplace requires no action until following month";
								}
								else{
									action1 = "Please fix the issue in 48 hours and resend the files to IRS";
								}
		    					enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, MANIFEST_XML, manifestFilePath, action1);
		    				}
			    			if(responseCode.equalsIgnoreCase("HS000000")){
			    				//archiveFilePayloadPath = archiveFilePayloadPath + File.separator + batchID;
			    				archiveFilePayloadPath.append(File.separator).append(batchID);
			    				String manifestFilePath = archiveFilePayloadPath.toString();
		    					enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, MANIFEST_XML, manifestFilePath, action2);
		    				}
			    			if(responseCode.equalsIgnoreCase("HX005005") 
									|| responseCode.equalsIgnoreCase("HX005001") 
									|| responseCode.equalsIgnoreCase("HE009000")
									|| responseCode.equalsIgnoreCase("HX009000")
									|| responseCode.equalsIgnoreCase("HX025015")
									|| responseCode.equalsIgnoreCase("HX026000")
									|| responseCode.equalsIgnoreCase("HX026301")) {
								StringBuilder archivefolderPayloadPath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
								String oldBatchId = batchID;
								batchID = batchID.replace('|', '-');
								batchID = batchID.replace(':', '-');
								String action = "The last IRS Report file package was rejected by CMS. The IRS Shop Inbound batch job will automatically resubmit the file package. Please verify that file package with batch ID" + oldBatchId +"is resubmitted.";
								String manifestFilePath = archivefolderPayloadPath.append(File.separator).append(batchID).toString();
								enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "ArchiveFolderPath", manifestFilePath, action);
							}
			    			if(responseCode.equalsIgnoreCase("HE007000")) {
								StringBuilder archivePath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
								String oldBatchId = batchID;
								batchID = batchID.replace('|', '-');
								batchID = batchID.replace(':', '-');
								String action = "The last IRS Report file package was rejected by CMS. The IRS Shop Inbound batch job will automatically Regenerate the file package. Please verify that file package with original batch ID" + oldBatchId +"is resubmitted.";
								String manifestFilePath = archivePath.append(File.separator).append(batchID).toString();
								enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "manifest.xml", manifestFilePath, action);
			    			}
			    			//re-send only missing files
			    			if(responseCode.equalsIgnoreCase("HE029028") || responseCode.equalsIgnoreCase("HE029078")){
			    				if(isNotNullAndEmpty(batchID)){
			    					int submissionMonth = 0;
		    						int submissionYear = 0;
		    						String householdCaseId ="";
			    					// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
			    					//HE007000 -Regenerate the entire file
			    					//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
			    					//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
			    					try{
				    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
				    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
				    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
				    						householdCaseId = "";
				    						if(listDocumentSeqId != null){
					    						String ducumentFileName = irsTransFile.getDocumentFileName();
					    						for(String docSeqID : listDocumentSeqId)
					    						{
					    						
						    						if(ducumentFileName.contains(docSeqID))
						    						{
						    							 householdCaseId = irsTransFile.getHouseholdCaseID();
						    						}
						    					}
			    						  }
					    				
			    						  else{
					    						  householdCaseId = irsTransFile.getHouseholdCaseID();
					    					  }
					    				
				    						//split the housholdcaseID and pass it to the ind portal rest API call
				    						String [] arrHouseholdCaseId = householdCaseId.split(",");
				    					   List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
				    						if(irsTransFile.getMonth() != null){
				    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
				    						 submissionMonth = submissionMonth-1;
				    						}
				    						if(irsTransFile.getYear()!= null){
				    							submissionYear = Integer.parseInt(irsTransFile.getYear());
					    					}
				    						if(newListHouseholdCaseId != null && newListHouseholdCaseId.size() > 0)
				    						{
				    						 listHouseholdCaseId.addAll(newListHouseholdCaseId);
				    						}
				    					 }
				    					List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
										enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);				    						// package the xml file
				    				generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ,batchID);
			    					}
			    					catch(Exception ex){
			    						LOGGER.error(ex.getMessage(), ex);
			    					}
			    								    				      
			    				}
			    				
			    			}
			    		
			    		
		    				//HS000000
		    			   //HX001103 -Resend the same file again
		    				if(responseCode.equalsIgnoreCase("HE029083") 
		    						||responseCode.equalsIgnoreCase("HX005005") 
									|| responseCode.equalsIgnoreCase("HX005001") 
									|| responseCode.equalsIgnoreCase("HE009000")
									|| responseCode.equalsIgnoreCase("HX009000")
									|| responseCode.equalsIgnoreCase("HX025015")
									|| responseCode.equalsIgnoreCase("HX026000")
									|| responseCode.equalsIgnoreCase("HX026301")
									||responseCode.equalsIgnoreCase("HX026000") && isNotNullAndEmpty(batchID)){
		    					   // get the files from archive folder with same name as batchID
									//String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
									String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
									batchID = batchID.replace('|', '-');
									batchID = batchID.replace(':', '-');
									ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + batchID;
									//String filePayloadXMLFolderPath = "C:/Data/IRSContentFileXMLFolder";
									String hubFTPZipFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTHUBFTPPATH);
									String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);
									//String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
									// move files from archive to filePayloadXMLFolderPath to recreate the manifest file
									//String directoryTobeZipped = "C:/Data/FilePayloadZipFolderLocation";
									String directoryTobeZipped = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTZIPFOLDERPATH);
									//  String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder/";
									// String environment = "T";
									deleteDir(ArchiveFilePayloadPath + File.separator +"ResentFile_batchID");
									File srcFile = new File(ArchiveFilePayloadPath);
									File destDir = new File(directoryTobeZipped);
									FileUtils.copyDirectory(srcFile, destDir);
								     SimpleDateFormat dateFormat = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
									String fileCreationDateTime = dateFormat.format(new TSDate());
									String partnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
									String zipFileName = partnerID + "."+ EnrollmentConstants.APP + "." + EnrollmentConstants.FUNC_INBOUND + "."+ fileCreationDateTime + "."+ environment + "."+ EnrollmentConstants.TRANSFER_DIRECTION_IN;
									String hubFTPZipFolderPath = hubFTPZipFilePath + File.separator+ zipFileName + ".zip";
									//String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder" + zipFileName + ".zip";

									//Zip all the file content and drop into Hub FTP location
									ZipHelper zippy = new ZipHelper();
									zippy.zipDir(directoryTobeZipped,hubFTPZipFolderPath); 
									//String timeStamp = getDateTime();s
									//set the archive folder name same as batch ID
									File oldzipFile = new File(hubFTPZipFolderPath);          
								    // File (or directory) with new name
							        String newZipFileName =  hubFTPZipFilePath + File.separator + zipFileName;			
								    File newZipFile = new File(newZipFileName);
									//String timeStamp = getDateTime();s
									//set the archive folder name same as batch ID
									String timeStampArchiveFileFolder = ArchiveFilePayloadPath + File.separator + "ResentFile_batchID";
									// FileUtils.cleanDirectory(new File(directoryTobeZipped));
									EnrollmentUtils.moveFiles(directoryTobeZipped,timeStampArchiveFileFolder,EnrollmentConstants.FILE_TYPE_XML,null);
									oldzipFile.renameTo(newZipFile);
										    				
		    			       }				    			    
		    		     }
		    	  }
	    	}
	 }
	 catch(Exception ex){
		 LOGGER.error(ex.getMessage(),ex);
	 }
}
private void processAttachmentLevelShopIRSResponse(NodeList nListAttachment,String batchID,String batchCategoryCode,List<String> listDocumentSeqId){
	try{	
		 String action1 = "Please fix the issue in 48 hours and resend the files to IRS";
		 String action2 = "IRS File Package with Batch ID" + batchID + "Succesfully Processed by IRS";
		 StringBuilder archiveFilePayloadPath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
		if(nListAttachment != null){
			for (int temp = 0; temp < nListAttachment.getLength(); temp++) {			     
				Node nNode = nListAttachment.item(temp);			     
				LOGGER.info("\nCurrent Element :" + nNode.getNodeName());			     
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {			     
					Element eElement = (Element) nNode;		
					String fileName = getValue("Q1:DocumentFileName", eElement);
					String responseDesc = getValue("hix:ResponseDescriptionText", eElement);
					LOGGER.info("DocumentFileName : " + fileName);
					LOGGER.info("DocumentSequenceID: " + getValue("Q1:DocumentSequenceID", eElement));
					LOGGER.info("hix:ResponseDescriptionText: " + responseDesc);
					LOGGER.info("hix:ResponseCode: " + getValue("hix:ResponseCode", eElement));
					String responseCode = getValue("hix:ResponseCode", eElement);
					  // Log NACK response details to IRS_Inbound_Transmission table
					IRSInboundTransmission  irsInboundTransmission = new IRSInboundTransmission();
					irsInboundTransmission.setBatchId(batchID);
					irsInboundTransmission.setEFTFileName(fileName);
					irsInboundTransmission.setResponseType("IRS");
					//irsInboundTransmission.setYear(year);
					irsInboundTransmission.setHubResponseCode(responseCode);
					irsInboundTransmission.setHubResponseDesc(responseDesc);
					try{
					irsInboundTransmissionRepo.saveAndFlush(irsInboundTransmission);		
					}											
					catch(Exception e){
						LOGGER.error("Error saving Inbound data in IRSInboundTransmission table @ logIRSIntBoundTransmissionInformation" , e);
					}
					
					if(responseCode.equalsIgnoreCase("HE029074") || responseCode.equalsIgnoreCase("HX026301") || responseCode.equalsIgnoreCase("HX026503")||responseCode.equalsIgnoreCase("HE029001")
							||responseCode.equalsIgnoreCase("HE029020")
							||responseCode.equalsIgnoreCase("HE0290025")
							||responseCode.equalsIgnoreCase("HE007000")
							||responseCode.equalsIgnoreCase("HE029027")
							||responseCode.equalsIgnoreCase("HE029035")
							||responseCode.equalsIgnoreCase("HE029051")
							||responseCode.equalsIgnoreCase("HE029070")
							||responseCode.equalsIgnoreCase("HE029075")
							||responseCode.equalsIgnoreCase("HE029077")
							||responseCode.equalsIgnoreCase("HE029081")
							||responseCode.equalsIgnoreCase("HE029085")
							||responseCode.equalsIgnoreCase("HX026503")
							||responseCode.equalsIgnoreCase("HE029085")
							||responseCode.equalsIgnoreCase("HE029085")){
	    				if(isNotNullAndEmpty(batchID)){
							int submissionMonth = 0;
							int submissionYear = 0;
							String householdCaseId ="";
							// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
							//HE007000 -Regenerate the entire file
							//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
							//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
							try{
		    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
		    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
		    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
		    						householdCaseId = "";
		    						if(listDocumentSeqId != null){
			    						String ducumentFileName = irsTransFile.getDocumentFileName();
			    						for(String docSeqID : listDocumentSeqId)
			    						{
			    						
				    						if(ducumentFileName.contains(docSeqID))
				    						{
				    							 householdCaseId = irsTransFile.getHouseholdCaseID();
				    						}
				    					}
	    						  }
			    				
	    						  else{
			    						  householdCaseId = irsTransFile.getHouseholdCaseID();
			    					  }
			    				
		    						//split the housholdcaseID and pass it to the ind portal rest API call
		    						String [] arrHouseholdCaseId = householdCaseId.split(",");
		    					   List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
		    						if(irsTransFile.getMonth() != null){
		    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
		    						 submissionMonth = submissionMonth-1;
		    						}
		    						if(irsTransFile.getYear()!= null){
		    							submissionYear = Integer.parseInt(irsTransFile.getYear());
			    					}
		    						if(newListHouseholdCaseId != null && newListHouseholdCaseId.size() > 0)
		    						{
		    						 listHouseholdCaseId.addAll(newListHouseholdCaseId);
		    						}
		    					 }
		    					List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
								enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);	
		    				    if(listDocumentSeqId != null && listDocumentSeqId.size() > 0)
		    				    {
								  generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_FILE_REQ ,batchID);
		    				    
								}
		    				    else{
		    				    	 generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_SHOP_RESUBMIT_BATCH_REQ,batchID);
		    				    }
							}
	    					catch(Exception ex){
	    						LOGGER.error(ex.getMessage(), ex);
	    					}
	    								    				      
	    				}
	    				
	    			}
	    			//generate Email notification
	    			if(responseCode.equalsIgnoreCase("HE029051") ||
	    					responseCode.equalsIgnoreCase("HE029052")   || responseCode.equalsIgnoreCase("HE029053")
	    					||responseCode.equalsIgnoreCase("HE029054") || responseCode.equalsIgnoreCase("HE029057") 
	    					||responseCode.equalsIgnoreCase("HE029058") || responseCode.equalsIgnoreCase("HE029059") 
	    					||responseCode.equalsIgnoreCase("HE029060") || responseCode.equalsIgnoreCase("HE029061") 
	    					||responseCode.equalsIgnoreCase("HE029062") || responseCode.equalsIgnoreCase("HE029065") 
	    					||responseCode.equalsIgnoreCase("HE029066") || responseCode.equalsIgnoreCase("HE029067") 
	    					||responseCode.equalsIgnoreCase("HE029068") || responseCode.equalsIgnoreCase("HE029069") 
	    					||responseCode.equalsIgnoreCase("HE029070") || responseCode.equalsIgnoreCase("HE029072")
	    					|| responseCode.equalsIgnoreCase("HE029076") || responseCode.equalsIgnoreCase("HE029078")
	    					|| responseCode.equalsIgnoreCase("HE029080") || responseCode.equalsIgnoreCase("HE029082")
	    					|| responseCode.equalsIgnoreCase("HE029084") || responseCode.equalsIgnoreCase("HE029085")
	    					|| responseCode.equalsIgnoreCase("HE029999") ||responseCode.equalsIgnoreCase("HE029031")){
	    				//	 archiveFilePayloadPath = archiveFilePayloadPath. + File.separator + batchID;
	    				     archiveFilePayloadPath.append(File.separator).append(batchID);
	    					 String manifestFilePath =archiveFilePayloadPath.toString();
	    					enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, MANIFEST_XML, manifestFilePath, action1);
	    				}
		    			if(responseCode.equalsIgnoreCase("HS000000")){
		    				//archiveFilePayloadPath = archiveFilePayloadPath + File.separator + batchID;
		    				archiveFilePayloadPath.append(File.separator).append(batchID);
		    				String manifestFilePath = archiveFilePayloadPath.toString();
	    					enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, MANIFEST_XML, manifestFilePath, action2);
	    				}
		    			if(responseCode.equalsIgnoreCase("HX005005") 
								|| responseCode.equalsIgnoreCase("HX005001") 
								|| responseCode.equalsIgnoreCase("HE009000")
								|| responseCode.equalsIgnoreCase("HX009000")
								|| responseCode.equalsIgnoreCase("HX025015")
								|| responseCode.equalsIgnoreCase("HX026000")
								|| responseCode.equalsIgnoreCase("HX026301")) {
							StringBuilder archivefolderPayloadPath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
							String oldBatchId = batchID;
							batchID = batchID.replace('|', '-');
							batchID = batchID.replace(':', '-');
							String action = "The last IRS Report file package was rejected by CMS. The IRS Shop Inbound batch job will automatically resubmit the file package. Please verify that file package with batch ID" + oldBatchId +"is resubmitted.";
							String manifestFilePath = archivefolderPayloadPath.append(File.separator).append(batchID).toString();
							enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "ArchiveFolderPath", manifestFilePath, action);
						}
		    			if(responseCode.equalsIgnoreCase("HE007000")) {
							StringBuilder archivePath =new StringBuilder(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH));
							String oldBatchId = batchID;
							batchID = batchID.replace('|', '-');
							batchID = batchID.replace(':', '-');
							String action = "The last IRS Report file package was rejected by CMS. The IRS Shop Inbound batch job will automatically Regenerate the file package. Please verify that file package with original batch ID" + oldBatchId +"is resubmitted.";
							String manifestFilePath = archivePath.append(File.separator).append(batchID).toString();
							enrollmentEmailNotificationService.sendIRSNACKResponseFailureNotification(responseCode, responseDesc, "manifest.xml", manifestFilePath, action);
		    			}
		    			//re-send only missing files
		    			if(responseCode.equalsIgnoreCase("HE029028") || responseCode.equalsIgnoreCase("HE029078")){
		    				if(isNotNullAndEmpty(batchID)){
		    					int submissionMonth = 0;
	    						int submissionYear = 0;
	    						String householdCaseId ="";
		    					// query the IRS_OUTBOUND_TRANSMISSION table for given batch ID
		    					//HE007000 -Regenerate the entire file
		    					//Query the IRS_OUTBOUND_TRANSMISSION  table and get all the HouseHoldCaseID associated with Batch ID
		    					//List<IRSOutboundTransmission> listIRSOutboundTransmsn = null;
		    					try{
			    					List<IRSOutboundTransmission> listIRSOutboundTransmsn = iRSOutboundTransmissionService.findIRSOutboundTransmissionByBatchId(batchID);
			    					List<String> listHouseholdCaseId = new ArrayList<String>();			    					
			    					for(IRSOutboundTransmission irsTransFile :listIRSOutboundTransmsn ){
			    						householdCaseId = "";
			    						if(listDocumentSeqId != null){
				    						String ducumentFileName = irsTransFile.getDocumentFileName();
				    						for(String docSeqID : listDocumentSeqId)
				    						{
				    						
					    						if(ducumentFileName.contains(docSeqID))
					    						{
					    							 householdCaseId = irsTransFile.getHouseholdCaseID();
					    						}
					    					}
		    						  }
				    				
		    						  else{
				    						  householdCaseId = irsTransFile.getHouseholdCaseID();
				    					  }
				    				
			    						//split the housholdcaseID and pass it to the ind portal rest API call
			    						String [] arrHouseholdCaseId = householdCaseId.split(",");
			    					   List<String> newListHouseholdCaseId = Arrays.asList(arrHouseholdCaseId);
			    						if(irsTransFile.getMonth() != null){
			    						 submissionMonth = Integer.parseInt(irsTransFile.getMonth());
			    						 submissionMonth = submissionMonth-1;
			    						}
			    						if(irsTransFile.getYear()!= null){
			    							submissionYear = Integer.parseInt(irsTransFile.getYear());
				    					}
			    						if(newListHouseholdCaseId != null && newListHouseholdCaseId.size() > 0)
			    						{
			    						 listHouseholdCaseId.addAll(newListHouseholdCaseId);
			    						}
			    					 }
			    					List<Integer> intEmpEnrollmentIdList = getIntegerArray(listHouseholdCaseId); 
									enrollmentIrsReportService.generateShopIrsReportbByListParam(intEmpEnrollmentIdList,submissionMonth,submissionYear);				    						// package the xml file
			    				generateFilePayloadResubmitTransmissionXML(submissionMonth,submissionYear,EnrollmentConstants.IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ,batchID);
		    					}
		    					catch(Exception ex){
		    						LOGGER.error(ex.getMessage(), ex);
		    					}
		    								    				      
		    				}
		    				
		    			}
		    		
		    		
	    				//HS000000
	    			   //HX001103 -Resend the same file again
	    				if(responseCode.equalsIgnoreCase("HE029083") ||
	    						 responseCode.equalsIgnoreCase("HX005005") 
								|| responseCode.equalsIgnoreCase("HX005001") 
								|| responseCode.equalsIgnoreCase("HE009000")
								|| responseCode.equalsIgnoreCase("HX009000")
								|| responseCode.equalsIgnoreCase("HX025015")
								|| responseCode.equalsIgnoreCase("HX026000")
								|| responseCode.equalsIgnoreCase("HX026301")
								||responseCode.equalsIgnoreCase("HX026000") && isNotNullAndEmpty(batchID)){
	    					   // get the files from archive folder with same name as batchID
								//String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
								String ArchiveFilePayloadPath =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_ARCHIVE_FOLDERPATH);
								batchID = batchID.replace('|', '-');
								batchID = batchID.replace(':', '-');
								ArchiveFilePayloadPath = ArchiveFilePayloadPath + File.separator + batchID;
								//String filePayloadXMLFolderPath = "C:/Data/IRSContentFileXMLFolder";
								String hubFTPZipFilePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTHUBFTPPATH);
								String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);
								//String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
								// move files from archive to filePayloadXMLFolderPath to recreate the manifest file
								//String directoryTobeZipped = "C:/Data/FilePayloadZipFolderLocation";
								String directoryTobeZipped = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTZIPFOLDERPATH);
								//  String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder/";
								// String environment = "T";
								deleteDir(ArchiveFilePayloadPath + File.separator +"ResentFile_batchID");
								File srcFile = new File(ArchiveFilePayloadPath);
								File destDir = new File(directoryTobeZipped);
								FileUtils.copyDirectory(srcFile, destDir);
							     SimpleDateFormat dateFormat = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
								String fileCreationDateTime = dateFormat.format(new TSDate());
								String partnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
								String zipFileName = partnerID + "."+ EnrollmentConstants.APP + "." + EnrollmentConstants.FUNC_INBOUND + "."+ fileCreationDateTime + "."+ environment + "."+ EnrollmentConstants.TRANSFER_DIRECTION_IN;
								String hubFTPZipFolderPath = hubFTPZipFilePath + File.separator+ zipFileName + ".zip";
								//String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder" + zipFileName + ".zip";

								//Zip all the file content and drop into Hub FTP location
								ZipHelper zippy = new ZipHelper();
								zippy.zipDir(directoryTobeZipped,hubFTPZipFolderPath); 
								//String timeStamp = getDateTime();s
								//set the archive folder name same as batch ID
								File oldzipFile = new File(hubFTPZipFolderPath);          
							    // File (or directory) with new name
						        String newZipFileName =  hubFTPZipFilePath + File.separator + zipFileName;			
							    File newZipFile = new File(newZipFileName);
								//String timeStamp = getDateTime();s
								//set the archive folder name same as batch ID
								String timeStampArchiveFileFolder = ArchiveFilePayloadPath + File.separator + "ResentFile_batchID";
								// FileUtils.cleanDirectory(new File(directoryTobeZipped));
								EnrollmentUtils.moveFiles(directoryTobeZipped,timeStampArchiveFileFolder,EnrollmentConstants.FILE_TYPE_XML,null);
								oldzipFile.renameTo(newZipFile);
									    				
	    			       }				    	
					}		    				
				}
			}
		
	}
	catch(Exception ex){
		LOGGER.error(ex.getMessage(),ex);
	}
}

		 private  void moveZipFiles(String sourceFolder, String targetFolder, String fileType, List<File> lof) throws GIException{
	         try{
	       	  if (sourceFolder!=null && !("".equals(sourceFolder.trim())) &&!(new File(sourceFolder).exists())) {
	                 return;
	            }
	            if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
	                 new File(targetFolder).mkdirs();
	            }
	            
	            if(lof != null && !lof.isEmpty()){
	                 for(File file:lof){
	                      file.renameTo(new File(targetFolder+File.separator+file.getName()));
	                 }
	            }else{
	                 //move all files from source to target folder
	                 File[] files=getFilesInAFolder(sourceFolder,fileType);
	                 if(files!=null && files.length>0){
	               	  List<File> loFiles=Arrays.asList(files);
	               	moveZipFiles(sourceFolder,targetFolder,fileType, loFiles);
	                 }
	            }
	         }catch(Exception e){
	       	//  LOGGER.error("Error in moveFiles()"+e.toString());
	       	  throw new GIException("Error in moveFiles()"+e.toString(),e);
	         }
	   }

	private static String getValue(String tag, Element element) {
		String value = "";
		try {
			NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
			Node node = (Node) nodes.item(0);
			value = node.getNodeValue();
		} catch (Exception e) {
			LOGGER.error("Exception in finding node value", e);
		}
		return value;
	}

	
	

	/**
	 * @since
	 * @author parhi_s
	 * @param enrollments
	 * @param outputPath
	 * @throws Exception
	 * 
	 * This method is used to Create XML report by taking
	 *  Enrollments record for enrollmentXML job
	 */

	private void generateFilePayloadIRSXMLReport(FilePayloadBatchTransmission filePayloadBatchTransmission,String outputPath, String xsltPath) throws GIException {
		InputStream strXSLPath = null;
		InputStream strEmptyTagXSLPath = null;
		try {
			LOGGER.info("generateEnrollmentsXMLReport outputPath = "+ outputPath);
			if(filePayloadBatchTransmission!=null && filePayloadBatchTransmission.getAttachment() != null && !filePayloadBatchTransmission.getAttachment().isEmpty() && GhixUtils.isGhixValidPath(outputPath)){			
				String strXML = null;
				strXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream(xsltPath);
				strEmptyTagXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream("removeEmptyTags.xsl");
				JAXBContext context = JAXBContext.newInstance(com.getinsured.hix.model.enrollment.FilePayloadBatchTransmission.class,com.getinsured.hix.model.enrollment.Attachment.class,ArrayList.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

				StringWriter writerEnroll = new StringWriter();
				StreamResult resultEnroll = new StreamResult(writerEnroll);

				m.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new NullCharacterEscapeHandler());
				m.marshal(filePayloadBatchTransmission, resultEnroll);
				String strEnrollTrans = new String(writerEnroll.toString());
				//TransformerFactory factory = null;
				//factory =  TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",null);
				TransformerFactory factory = new net.sf.saxon.TransformerFactoryImpl();
				Source xslSource = new StreamSource(strXSLPath);
				StreamSource xmlJAXB = new StreamSource(new StringReader(strEnrollTrans));
				Templates template = factory.newTemplates(xslSource);
				Transformer transformer = template.newTransformer();
				StringWriter writerXSLT = new StringWriter();
				StreamResult resultXSLT = new StreamResult(writerXSLT);
				transformer.transform(xmlJAXB, resultXSLT);

				strXML = new String(writerXSLT.toString());
				StreamSource xmlWithEmpty = new StreamSource(new StringReader(strXML));
				Source xslEmptyTagSource = new StreamSource(strEmptyTagXSLPath);
				Templates templateEmpty = factory.newTemplates(xslEmptyTagSource);
				Transformer transformerEmpty = templateEmpty.newTransformer();
				transformerEmpty.transform(xmlWithEmpty, new StreamResult(new File(outputPath)));
			}else{
				LOGGER.error("Attachment List is null/empty or the Application is trying to reach a blacklisted folder or filename or extension type.");
			}

		} catch (Exception exception) {

			LOGGER.error(" EnrollmentServiceImpl.generateFilePayloadIRSXMLReport Exception "+ exception.getMessage());
			throw new GIException(" EnrollmentServiceImpl.generateFilePayloadIRSXMLReport Exception ",exception);

		}finally{
			IOUtils.closeQuietly(strXSLPath);
			IOUtils.closeQuietly(strEmptyTagXSLPath);
		}
	}



	@Override
	public void serveEnrollmentReconciliationJob(String extractMonth, String enrollmentType,String hiosIssuerID) throws GIException{
		LOGGER.info("ServeEnrollmentReconciliationJob SERVICE : START");
		serveEnrollmentReconciliationJob(extractMonth, enrollmentType, hiosIssuerID, null);

	}
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to get the end date of the job, than ran successfully
	 * @param jobName
	 * @return
	 * @throws GIException
	 */
	@Override
	public Date getJOBLastRunDate(String jobName) throws GIException{
		Date lastRunDate=null;
		//Integer highestJobID = null;
		try{
			if(jobName!=null){
				/*highestJobID = batchJobExecutionService
						.getHighestInstanceIDByJobName(jobName, EnrollmentConstants.BATCH_JOB_STATUS);
				if (highestJobID != null) {
					lastRunDate = batchJobExecutionService.getLastJobRunDate(highestJobID);
				}*/
				lastRunDate= batchJobExecutionService.getLastJobRunDate(jobName, EnrollmentConstants.BATCH_JOB_STATUS);
			}

		}catch(Exception e){
			throw new GIException("Error in getJOBLastRunDate() ::"+e.getMessage(),e);
		}
		return lastRunDate;
	}

	@Override
	public List<String> serveEnrollmentRecordJob(List<Enrollment> enrollmentList, int issuerID, String enrollmentType, Date startDate, Date endDate, String carrierResendFlag,long jobExecutionId, String jobName,  List<String> outputPathList,int issuerSubList) throws GIException{

		String hiosIssuerId = null;
		String outboundXmlExtractPath=null;
		String outputPath=null;
		//List<String> outputPathList = new ArrayList<String>();
		Map<Integer,String> failedEnrollmentMap = new HashMap<Integer, String>();
		Set<Integer> successEnrollmentSet = new HashSet<Integer>();
		LOGGER.info("serveEnrollmentRecordJob SERVICE : enrollmentType = "+enrollmentType);
		boolean isResendJob = false;

		if(carrierResendFlag != null ){
			isResendJob = true;
		}
		try{
			if(enrollmentList!=null && !enrollmentList.isEmpty()){
				boolean isExchgPartnerLkpValid = false;
				String errorMessage = null;
				ExchgPartnerLookup exchgPartnerLookup = null;
				if(isNotNullAndEmpty(enrollmentList.get(0).getHiosIssuerId())){


					// setting the source exchange id 

					
					if(enrollmentType.equals(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
						exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(enrollmentList.get(0).getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_SHOP,GhixConstants.OUTBOUND);
					}
					if(enrollmentType.equals(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
						exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(enrollmentList.get(0).getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL,GhixConstants.OUTBOUND);
					}

					hiosIssuerId = enrollmentList.get(0).getHiosIssuerId() != null ? enrollmentList.get(0).getHiosIssuerId() : "hiosIssuerId";
					if(exchgPartnerLookup!=null ){
						if(exchgPartnerLookup.getSourceDir() != null){
							outboundXmlExtractPath = exchgPartnerLookup.getSourceDir();
							outputPath = outboundXmlExtractPath;
							if ((new File(outputPath).exists())){
								isExchgPartnerLkpValid = true;
							}else{
								isExchgPartnerLkpValid = false;
								errorMessage = EnrollmentConstants.ERR_MSG_NO_SRC_EXCHG_DIRECTORY + issuerID;
							}
						}else{
							isExchgPartnerLkpValid = false;
							errorMessage = EnrollmentConstants.ERR_MSG_SRC_DIR_NOT_DEFINED;
						}
					}else{
						isExchgPartnerLkpValid = false;
						errorMessage = EnrollmentConstants.ERR_MSG_EXCHG_PARTNER_DOESNT_EXIST + issuerID;
					}
				}else{
					isExchgPartnerLkpValid = false;
					errorMessage = EnrollmentConstants.ERR_MSG_ISSUER_ID_IS_NULL;
				}
				
				if(isExchgPartnerLkpValid){
					List<Enrollments> enrollmentsList = setEnrollmentsFor834Daily(enrollmentList,startDate, endDate, enrollmentType, failedEnrollmentMap,successEnrollmentSet,isResendJob);
					String enableStateSubsidy = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);
					Boolean stateSubsidyFlag = Boolean.FALSE;
					if ("Y".equalsIgnoreCase(enableStateSubsidy)){
						stateSubsidyFlag = Boolean.TRUE;
					}

//					Enrollments enrollments=setEnrollmentsFor834Daily2(enrollmentList,startDate, endDate, enrollmentType, failedEnrollmentMap,successEnrollmentSet,isResendJob);
					for(Enrollments enrollments :  enrollmentsList){
						if(enrollments != null && enrollments.getEnrollmentList()!=null && !enrollments.getEnrollmentList().isEmpty()){
							enrollments.setStateCode(EnrollmentConfiguration.returnStateCode());
							enrollments.setStateSubsidyEnabled(stateSubsidyFlag);
							enrollments.setHiosIssuerId(hiosIssuerId);
							enrollments.setSourceExchgId(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.SOURCE_EXCHANGE_ID));
							enrollments.setJobExecutionId(jobExecutionId);
							/** Condition commented out as part of  HIX-87796 */
							//if (enrollmentType.equals(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
								enrollments.setSendBenefitEndDate(getSendBenefitEndDateFlag(issuerID));
							//}
							if(exchgPartnerLookup != null){
								enrollments.setISA05(exchgPartnerLookup.getIsa05());
								enrollments.setISA06(exchgPartnerLookup.getIsa06());
								enrollments.setISA07(exchgPartnerLookup.getIsa07());
								enrollments.setISA08(exchgPartnerLookup.getIsa08());
								enrollments.setISA15(exchgPartnerLookup.getIsa15());
								enrollments.setGS02(exchgPartnerLookup.getGs02());
								enrollments.setGS03(exchgPartnerLookup.getGs03());
							}

							if (enrollmentType.equals(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
								outputPath = outboundXmlExtractPath + File.separator +"to_" + hiosIssuerId +"_"+issuerSubList+ "_"+EnrollmentConfiguration.returnStateCode()+"_"+ GhixConstants.EDI_834_SHOP + "_" + dateFormat.format(new TSDate()) + ".tmp";
							}
							if (enrollmentType.equals(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
								String renTermStr = enrollments.getIsRenEnrollments() ? "RENTERM_" : "";
								outputPath = outboundXmlExtractPath + File.separator +"to_" + hiosIssuerId +"_"+issuerSubList+ "_"+EnrollmentConfiguration.returnStateCode()+ "_"+ GhixConstants.EDI_834_INDV + "_"+ renTermStr + dateFormat.format(new TSDate()) + ".tmp";
							}
							LOGGER.info("serveEnrollmentRecordJob SERVICE : outputPath = "+outputPath);
							generateEnrollmentsXMLReport(enrollments, outputPath,"EnrollmentXMLTrans.xsl");
							outputPathList.add(outputPath);
						}
					}
				}
				
				/** Some validation issues with Exchange Partner Lookup or Output derectory defined. Mark all enrollments for Issuer as failed.*/
				else{
					if(isResendJob){
						throw new GIException(errorMessage);
					}
					for(Enrollment enrollment : enrollmentList){
						failedEnrollmentMap.put(enrollment.getId(), errorMessage);
					}
				}
				
				/** These activity is not performend in case of RESEND JOB. The below conditions and situation will not occur
				 *  However better to have a precautionary check */
				if(!isResendJob){

				/** Update Enrollment Records which are Failed --> NULL **/
				if(successEnrollmentSet != null && !successEnrollmentSet.isEmpty()){
					List<Integer> successEnrollmentList = new ArrayList<Integer>(successEnrollmentSet);
					if(successEnrollmentList != null && !successEnrollmentList.isEmpty()){
						List<List<Integer>> splittedSuccesList= splitList(successEnrollmentList, 950);
						if(splittedSuccesList!=null && !splittedSuccesList.isEmpty()){
							for (List<Integer> subList: splittedSuccesList){
								enrollmentEventRepository.updateEventsToSuccess(subList, endDate);
							}
						}
					}
				}

				/** Update Enrollment Records as Failed. **/
				if(failedEnrollmentMap != null && !failedEnrollmentMap.isEmpty()){
					List<Integer> failedEnrollmentIdList = new ArrayList<Integer>(failedEnrollmentMap.keySet());
					if(failedEnrollmentIdList != null && !failedEnrollmentIdList.isEmpty()){
						List<List<Integer>> splittedFailedList= splitList(failedEnrollmentIdList, 950);
						if(splittedFailedList!=null && !splittedFailedList.isEmpty()){
							for (List<Integer> subList: splittedFailedList){
								enrollmentEventRepository.updateEventsToFailed(subList,startDate,endDate);
							}
						}
					}
					/** Update Extraction Job Status table */
					ExtractionJobStatus extractionJobStatus = new ExtractionJobStatus();
					
					String failedEnrollmentIds = EnrollmentUtils.getSeperatedStringFromCollection(failedEnrollmentIdList, ",");
					extractionJobStatus.setFailedEnrollmentIds(failedEnrollmentIds);
					extractionJobStatus.setCreatedOn(new TSDate());
					extractionJobStatus.setStartDate(startDate);
					extractionJobStatus.setEndDate(endDate);
					extractionJobStatus.setJobExecutionId(jobExecutionId);
					extractionJobStatus.setJobName(jobName);
					extractionJobStatus.setStatus(EnrollmentConstants.ENROLLMENT_EVENT_STATUS_FAILED);
					extractionJobStatus.setException(platformGson.toJson(failedEnrollmentMap));

					extractionJobStatusRepository.save(extractionJobStatus);
				}
			}

			}			
		}catch(Exception e){
			LOGGER.error("Exception in EnrollmentServiceImpl.serveEnrollmentRecordJob while processing Issuer Id :"+ issuerID+ "Exception Message:"+ e.getMessage());
			throw new GIException("Error in serveEnrollmentRecordJob()::"+e.getMessage(),e);
		}
		return outputPathList;
	}
	
	/**
	 * Method to split List
	 * @param list
	 * @param size
	 * @return
	 */
	private List<List<Integer>> splitList(List<Integer> list, Integer size){
		List<List<Integer>> splittedList=null;
		if(list!=null && !list.isEmpty()){
			splittedList= new ArrayList<List<Integer>>();
			int totalCount=list.size();
			for(int i=0; i<totalCount; i+=size){
				splittedList.add(new ArrayList<Integer>(list.subList(i, Math.min(totalCount, i+size))));
			}
		}
		
		return splittedList;
	}
	
	  
	/**
	 * 
	 * @param allEnrollees
	 * @param lastRunDate
	 * @param enrollmentType
	 * @return
	 * @throws GIException
	 */
	private List<Enrollee> filterEnrolleesForCMSDaily(List<Enrollee> allEnrollees, Date startDate,Date endDate, String renewalFlag ) throws GIException{
		List<Enrollee> updatedEnrolleeList = null;
		try{
			if(allEnrollees!=null ){
				updatedEnrolleeList = new ArrayList<Enrollee>();
				for(Enrollee enrollee:allEnrollees){
					if(enrollee.getPersonTypeLkp()!=null &&(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) )){
							if(EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode()) && EnrollmentConstants.RENEWAL_FLAG_A.equalsIgnoreCase(renewalFlag)){
								continue;
							}
							Date confirmationDate =null;
							if(EnrollmentConstants.RENEWAL_FLAG_A.equalsIgnoreCase(renewalFlag)){
								confirmationDate= enrolleeAudRepository.getEnrolleeConfirmationDate(enrollee.getId());
								if(confirmationDate!=null){
									Calendar calendar = TSCalendar.getInstance();
									calendar.setTime(confirmationDate);
									calendar.add(Calendar.SECOND, -EnrollmentConstants.FIVE);
									confirmationDate= calendar.getTime();
								}
							}
							List<EnrollmentEvent> allEvents= enrollee.getEnrollmentEvents();
							List<EnrollmentEvent> createdEvents= new ArrayList<EnrollmentEvent>();
							Date eventCreationDate=null;
							for(EnrollmentEvent event:allEvents){
								if(EnrollmentConstants.EVENT_TYPE_CANCELLATION.equalsIgnoreCase(event.getEventTypeLkp().getLookupValueCode()) && EnrollmentConstants.RENEWAL_FLAG_RENP.equalsIgnoreCase(event.getSendRenewalTag())){
									continue;
								}
								if(confirmationDate!=null && confirmationDate.after(event.getCreatedOn())){
									continue;
								}
								eventCreationDate=event.getCreatedOn();
								if((!(eventCreationDate.before(startDate))&&!(eventCreationDate.after(endDate))) ){
										createdEvents.add(event);
								}
							}
							if(createdEvents != null && !createdEvents.isEmpty()){
								enrollee.setEnrollmentEvents(createdEvents);
								updatedEnrolleeList.add(enrollee);
							}
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Exception in filterEnrolleesForEDIDaily()"+e.getMessage());
			throw new GIException("Exception in filterEnrolleesForEDIDaily()"+e.getMessage(),e);
		}
		return updatedEnrolleeList;
	}
	
	
	
	
//	private boolean isDateBetween(Date startDate, Date endDate, Date dateToCompare){
//		return ((dateToCompare.after(startDate) && dateToCompare.before(endDate)) || (dateToCompare.equals(startDate) || dateToCompare.equals(endDate)));
//	}
	
	private Enrollee setReferenceFieldsFromAudit(Enrollee eventEnrollee, EnrolleeAud enrolleeAud) throws GIException{
		try{
			if(eventEnrollee!=null && enrolleeAud!=null){
				eventEnrollee.setEnrolleeLkpValue(enrolleeAud.getEnrolleeLkpValue());
				eventEnrollee.setLanguageLkp(enrolleeAud.getLanguageLkp());
				eventEnrollee.setLanguageWrittenLkp(enrolleeAud.getLanguageWrittenLkp());
				eventEnrollee.setHomeAddressid(enrolleeAud.getHomeAddressid());
				eventEnrollee.setGenderLkp(enrolleeAud.getGenderLkp());
				eventEnrollee.setMaritalStatusLkp(enrolleeAud.getMaritalStatusLkp());
				eventEnrollee.setCitizenshipStatusLkp(enrolleeAud.getCitizenshipStatusLkp());
				eventEnrollee.setTobaccoUsageLkp(enrolleeAud.getTobaccoUsageLkp());
				eventEnrollee.setMailingAddressId(enrolleeAud.getMailingAddressId());
				eventEnrollee.setPersonTypeLkp(enrolleeAud.getPersonTypeLkp());
				eventEnrollee.setEnrollment(enrolleeAud.getEnrollment());
			}
		}catch(Exception e){
			//e.printStackTrace();
			LOGGER.error("Exception in setReferenceFieldsFromAudit()"+e.getMessage(),e);
			throw new GIException("Exception in setReferenceFieldsFromAudit()"+e.getMessage(),e);
		}
		return eventEnrollee;
	}
	
	private Enrollee setNormalFieldsFromAudit(Enrollee localEnrollee, EnrolleeAud eventEnrollee) throws GIException{
		try{
			if(localEnrollee!=null && eventEnrollee!=null){
				localEnrollee.setId(eventEnrollee.getId());
				localEnrollee.setTaxIdNumber(eventEnrollee.getTaxIdNumber());
				localEnrollee.setExchgIndivIdentifier(eventEnrollee.getExchgIndivIdentifier());
				localEnrollee.setIssuerIndivIdentifier(eventEnrollee.getIssuerIndivIdentifier());
				localEnrollee.setHealthCoveragePolicyNo(eventEnrollee.getHealthCoveragePolicyNo());
				localEnrollee.setEffectiveStartDate(eventEnrollee.getEffectiveStartDate());
				localEnrollee.setEffectiveEndDate(eventEnrollee.getEffectiveEndDate());
				localEnrollee.setLastName(eventEnrollee.getLastName());
				localEnrollee.setFirstName(eventEnrollee.getFirstName());
				localEnrollee.setMiddleName(eventEnrollee.getMiddleName());
				localEnrollee.setSuffix(eventEnrollee.getSuffix());
				localEnrollee.setNationalIndividualIdentifier(eventEnrollee.getNationalIndividualIdentifier());
				localEnrollee.setPrimaryPhoneNo(eventEnrollee.getPrimaryPhoneNo());
				localEnrollee.setSecondaryPhoneNo(eventEnrollee.getSecondaryPhoneNo());
				localEnrollee.setPreferredSMS(eventEnrollee.getPreferredSMS());
				localEnrollee.setPreferredEmail(eventEnrollee.getPreferredEmail());
				localEnrollee.setBirthDate(eventEnrollee.getBirthDate());
				localEnrollee.setDeathDate(eventEnrollee.getDeathDate());
				localEnrollee.setTotalIndvResponsibilityAmt(eventEnrollee.getTotalIndvResponsibilityAmt());
				localEnrollee.setTotIndvRespEffDate(eventEnrollee.getTotIndvRespEffDate());
				localEnrollee.setMaintenanceEffectiveDate(eventEnrollee.getMaintenanceEffectiveDate());
				localEnrollee.setRatingArea(eventEnrollee.getRatingArea());
				localEnrollee.setRatingAreaEffDate(eventEnrollee.getRatingAreaEffDate());
				localEnrollee.setLastPremiumPaidDate(eventEnrollee.getLastPremiumPaidDate());
				localEnrollee.setTotEmpResponsibilityAmt(eventEnrollee.getTotEmpResponsibilityAmt());
				localEnrollee.setRespPersonIdCode(eventEnrollee.getRespPersonIdCode());
				localEnrollee.setCreatedOn(eventEnrollee.getCreatedOn());
				localEnrollee.setUpdatedOn(eventEnrollee.getUpdatedOn());
				//localEnrollee.setEmployerGroupNo(eventEnrollee.getEmployerGroupNo());
				//localEnrollee.setIssuerSubscriberIdentifier(eventEnrollee.getIssuerSubscriberIdentifier());
				localEnrollee.setId(eventEnrollee.getId());
				
				//localEnrollee.setHomeAddressid(eventEnrollee.getHomeAddressid());
				//localEnrollee.setMailingAddressId(eventEnrollee.getMailingAddressId());	
			}
			
		}catch(Exception e){
			//e.printStackTrace();
			LOGGER.error("Exception in setNormalFieldsFromAudit()"+e.getMessage(),e);
			throw new GIException("Exception in setNormalFieldsFromAudit()"+e.getMessage(),e);
		}
		return localEnrollee;
		
	}
	
	private Location setLocationFieldsFromAudit(Location localLocation, LocationAud revLocation) throws GIException {
		if(localLocation != null && revLocation!= null){
			try{
				localLocation.setId(revLocation.getId());
				localLocation.setAddOnZip(revLocation.getAddOnZip());
				localLocation.setAddress1(removeSpaces(revLocation.getAddress1()));
				localLocation.setAddress2(removeSpaces(revLocation.getAddress2()));
				localLocation.setCity(removeSpaces(revLocation.getCity()));
				localLocation.setCounty(removeSpaces(revLocation.getCounty()));
				localLocation.setCountycode(removeSpaces(revLocation.getCountycode()));
				localLocation.setLat(revLocation.getLat());
				localLocation.setLon(revLocation.getLon());
				localLocation.setRdi(revLocation.getRdi());
				localLocation.setState(removeSpaces(revLocation.getState()));
				localLocation.setZip(removeSpaces(revLocation.getZip()));
			}
			catch (Exception e){
				LOGGER.error("Exception in setNormalFieldsFromAudit()"+e.getMessage(),e);
				throw new GIException("Exception in setLocationFieldsFromAudit() :: " ,e);
			}
		}
		return localLocation;
	}
	
	protected boolean isDisenrolledWithSubscriber(EnrolleeAud subscriber, EnrolleeAud enrollee) {
		boolean isDisenrolledWithSubscriber = false;
		if (subscriber != null
				&& (subscriber.getEnrolleeLkpValue().getLookupValueCode()
						.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)
						|| subscriber.getEnrolleeLkpValue().getLookupValueCode()
								.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
				&& EnrollmentUtils.isSameTime(enrollee.getDisenrollTimestamp(), subscriber.getDisenrollTimestamp(),
						60)) {
			isDisenrolledWithSubscriber = true;
		}
		return isDisenrolledWithSubscriber;
	}
	
	private void setEnrollmentFieldsFromAudit(Enrollment enrollmentLocal, EnrollmentAud enrollmentAudit, Employer employer) throws GIException{
		try{
			if(enrollmentAudit!=null && enrollmentAudit!=null){
				enrollmentLocal.setId(enrollmentAudit.getId());
				
				enrollmentLocal.setSponsorName(enrollmentAudit.getSponsorName());
				enrollmentLocal.setSponsorEIN(enrollmentAudit.getSponsorEIN());
				enrollmentLocal.setSponsorTaxIdNumber(enrollmentAudit.getSponsorTaxIdNumber());
				enrollmentLocal.setEnrollmentTypeLkp(enrollmentAudit.getEnrollmentTypeLkp());
				enrollmentLocal.setEnrollmentStatusLkp(enrollmentAudit.getEnrollmentStatusLkp());
				enrollmentLocal.setInsurerName(enrollmentAudit.getInsurerName());
				enrollmentLocal.setInsurerTaxIdNumber(enrollmentAudit.getInsurerTaxIdNumber());
				enrollmentLocal.setIssuerSubscriberIdentifier(enrollmentAudit.getIssuerSubscriberIdentifier());
				enrollmentLocal.setExchgSubscriberIdentifier(enrollmentAudit.getExchgSubscriberIdentifier());
				enrollmentLocal.setInsuranceTypeLkp(enrollmentAudit.getInsuranceTypeLkp());
				enrollmentLocal.setBenefitEffectiveDate(enrollmentAudit.getBenefitEffectiveDate());
				enrollmentLocal.setBenefitEndDate(enrollmentAudit.getBenefitEndDate());
				if(EnrollmentConfiguration.isCaCall()) {
					enrollmentLocal.setHouseHoldCaseId(enrollmentAudit.getExternalHouseHoldCaseId());
				}else {
				enrollmentLocal.setHouseHoldCaseId(enrollmentAudit.getHouseHoldCaseId());
				}
				enrollmentLocal.setGroupPolicyNumber(enrollmentAudit.getGroupPolicyNumber());
				enrollmentLocal.setAgentBrokerName(enrollmentAudit.getAgentBrokerName());
				enrollmentLocal.setCMSPlanID(enrollmentAudit.getCMSPlanID());
				if(enrollmentAudit.getCMSPlanID()!=null){
					enrollmentLocal.setgS02(enrollmentAudit.getCMSPlanID().substring(0, enrollmentAudit.getCMSPlanID().length()-2));
				}
				enrollmentLocal.setBrokerFEDTaxPayerId(enrollmentAudit.getBrokerFEDTaxPayerId());
				enrollmentLocal.setBrokerTPAAccountNumber1(enrollmentAudit.getBrokerTPAAccountNumber1());
				if (!EnrollmentConfiguration.isNvCall()) {
				enrollmentLocal.setBrokerTPAAccountNumber2(enrollmentAudit.getBrokerTPAAccountNumber2());
				}
				enrollmentLocal.setBrokerRole(enrollmentAudit.getBrokerRole());
				enrollmentLocal.setCreatedOn(enrollmentAudit.getCreatedOn());
				enrollmentLocal.setUpdatedOn(enrollmentAudit.getUpdatedOn());
//				enrollmentLocal.setIssuer(enrollmentAudit.getIssuer());
				enrollmentLocal.setEmployer(employer);
				enrollmentLocal.setEmployerCaseId(enrollmentAudit.getEmployerCaseId());
				boolean paymntTxnId = Boolean.parseBoolean(DynamicPropertiesUtil
						.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.SHOW_834_PAYMENT_TXN_ID));
				if (paymntTxnId) {
					enrollmentLocal.setPaymentTxnId(enrollmentAudit.getPaymentTxnId());
				}
				if(enrollmentAudit.getRevtype()==0){
					enrollmentLocal.setLastEnrollmentId(enrollmentAudit.getLastEnrollmentId());
				}
				if (enrollmentAudit.getPriorEnrollmentId() != null) {
					enrollmentLocal.setPriorEnrollmentId(enrollmentAudit.getPriorEnrollmentId());
				}

				String stateSubsidyEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);
				if("Y".equalsIgnoreCase(stateSubsidyEnabled)){
					if(enrollmentAudit.getStateSubsidyAmt()!=null){
						enrollmentLocal.setStateSubsidyAmt(enrollmentAudit.getStateSubsidyAmt());
						enrollmentLocal.setStateSubsidyEffDate(enrollmentAudit.getStateSubsidyEffDate());
					}
					else{
						enrollmentLocal.setStateSubsidyAmt(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP));
						enrollmentLocal.setStateSubsidyEffDate(enrollmentAudit.getStateSubsidyEffDate());
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Exception in setEnrollmentFieldsFromAudit()"+e.getMessage(),e);
			throw new GIException("Exception in setEnrollmentFieldsFromAudit()"+e.getMessage(),e);
		}
		
	}
	
	private List<Enrollment> setPerRevisionEnrollmentAndEnrollee(List<Enrollee> enrolleeList, boolean isCMS) throws GIException{
		List<Enrollee> perEventEnrollee = null;
		LOGGER.debug("setPerRevisionEnrollmentAndEnrollee :: Started");
		Map<Integer,Enrollment> enrollmentRevisionMap=null;
		try{
			if(enrolleeList != null && !enrolleeList.isEmpty()){
				enrollmentRevisionMap= new LinkedHashMap<Integer, Enrollment>();
				perEventEnrollee = new ArrayList<Enrollee>();
				EnrollmentAud enrollmentAud = null;
				Integer previousEventEnrolleeRevision = 0;
				Enrollment enrollment = enrolleeList.get(0).getEnrollment();
				Enrollee responsiblePerson=enrolleeService.getResponsiblePersonByEnrollmentID(enrollment.getId());
				List<EnrollmentPremium> enrollmentPremiumList = enrollmentPremiumRepository.findByEnrollmentId(enrolleeList.get(0).getEnrollment().getId());
				boolean isPremiumListEmpty = isPremiumListNullOrEmpty(enrollmentPremiumList);
				sortPremiumList(enrollmentPremiumList);
//				String auditToleranceMillisecond = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.AUDIT_TIMESTAMP_DELTA);
				for(Enrollee enrollee:enrolleeList){
					List<EnrollmentEvent> eventList = enrollee.getEnrollmentEvents();
					
					if(eventList!=null && !eventList.isEmpty()){
						Collections.sort(eventList);
						for(EnrollmentEvent event: eventList){
							Date eventDate= event.getCreatedOn();
							// Get enrollment event revision by event id
							Integer eventRevId = enrollmentEventAudRepository.getRevIdByEventId(event.getId());
									
							if(eventDate!=null){
//							if(eventDate!=null && isNotNullAndEmpty(auditToleranceMillisecond)){
								Calendar calendar = TSCalendar.getInstance();
								calendar.setTime(eventDate);
								//calendar.add(Calendar.SECOND, EnrollmentConstants.FIVE);
//								calendar.add(Calendar.MILLISECOND, Integer.parseInt(auditToleranceMillisecond));
								eventDate= calendar.getTime();
							}
							// Get by Revision
							List<EnrolleeAud>  enrolleeAuditList = enrolleeAudRepository.getEnrolleeAudByIdEventIdAndRev(enrollee.getId(), event.getId(), eventRevId);
							if(null == enrolleeAuditList || enrolleeAuditList.isEmpty()){
								enrolleeAuditList=enrolleeAudRepository.findRevisionByEvent(enrollee.getId(), event.getId());
							}
							if(null == enrolleeAuditList || enrolleeAuditList.isEmpty()){
								enrolleeAuditList=enrolleeAudRepository.findRevisionByEventDate(enrollee.getId(), eventDate);
							}
							if(enrolleeAuditList != null && !enrolleeAuditList.isEmpty()){
								EnrolleeAud enrolleeAud=null;
								Enrollee enrolleeLocal= new Enrollee();
								Enrollment enrollmentLocal=new Enrollment();
								Integer enrolleeRevisionNumber=null;
								enrolleeAud= enrolleeAuditList.get(0);
								if(enrolleeAud != null) {
									enrolleeRevisionNumber = enrolleeAud.getRev();
								}
								EnrolleeAud subscriber=null;
								// Get by Revision
								List<EnrolleeAud> subscriberAudList = enrolleeAudRepository.getEnrollSubscByRev(enrollee.getEnrollment().getId(),eventRevId);
								if(null == subscriberAudList || subscriberAudList.isEmpty()){
									subscriberAudList=enrolleeAudRepository.getEnrollSubscByDate(enrollee.getEnrollment().getId(),eventDate);
								}
								if(subscriberAudList!=null && subscriberAudList.size()>0){
									subscriber=subscriberAudList.get(0);
								}
								if(subscriber==null){
									LOGGER.error("setPerRevisionEnrollmentAndEnrollee :: subscriber audit fetched from Audit is null for enrollment= "+ enrollee.getEnrollment().getId() + EnrollmentConstants.AND_REVISION+enrolleeRevisionNumber);
									throw new GIException("setPerRevisionEnrollmentAndEnrollee :: subscriber audit fetched from Audit is null for enrollment= "+ enrollee.getEnrollment().getId() + EnrollmentConstants.AND_REVISION +enrolleeRevisionNumber);
								}
								// Fetch enrollment_aud records for Amount related fields.
								// Fetch only if revision number is different, as the enrollment record is going to be same for multiple enrollees/enrollee events 
								if(previousEventEnrolleeRevision != null && enrolleeRevisionNumber != null && (!previousEventEnrolleeRevision.equals(enrolleeRevisionNumber))){
									List<EnrollmentAud> enrollmentAudList= new ArrayList<EnrollmentAud>();
									previousEventEnrolleeRevision = enrolleeRevisionNumber;

									// Fetch Enrollment Aud for revision as it needs to reflect previous premiums in that sate.
									// Get by Revision
									enrollmentAudList = enrollmentAudRepository.getEnrollmentAudByIdAndRev(event.getEnrollment().getId(), eventRevId);
									if(null == enrollmentAudList || enrollmentAudList.isEmpty()){
										enrollmentAudList = enrollmentAudRepository.getEnrollmentAudByIdAndDate(event.getEnrollment().getId(), eventDate);
									}
									if(enrollmentAudList!=null && !enrollmentAudList.isEmpty()){
										enrollmentAud= enrollmentAudList.get(0);
									}
								}
								if(enrollmentAud==null){
									LOGGER.error("setPerRevisionEnrollmentAndEnrollee :: EnrollmentAudit fetched from Audit is null for enrollment= "+ enrollee.getEnrollment().getId() + EnrollmentConstants.AND_REVISION+enrolleeRevisionNumber);
									throw new GIException("setPerRevisionEnrollmentAndEnrollee :: EnrollmentAudit fetched from Audit is null for enrollment= "+ enrollee.getEnrollment().getId() + EnrollmentConstants.AND_REVISION+enrolleeRevisionNumber);
								}
								// Check from Audit if person is Enrollee
								// If Enrollee filter out cancel events form enrollee. As those are not needed in Extracts
								// Also only filter out cancel events from enrollee only if subscriber is canclled, else these are needed in extracts
								if(event.getEventTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.EVENT_TYPE_CANCELLATION)){
									if(enrolleeAud != null && enrolleeAud.getPersonTypeLkp() != null && enrolleeAud.getPersonTypeLkp().getLookupValueCode() != null 
										&& (enrolleeAud.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE))
										&& isDisenrolledWithSubscriber(subscriber, enrolleeAud) 
											){
											LOGGER.debug("setPerRevisionEnrollmentAndEnrollee :: Subscriber level disenrollment, so skip for enrollee= "+ enrollee.getId() +" event ="+event.getId());
											continue;
									}else if(enrollmentAud.getEnrollmentTypeLkp()!=null && enrollmentAud.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP) && isEnrollmentFromPendingToCancel(enrollmentAud, eventDate, eventRevId)){ 		// Get by Revision
										//check if it was from pending to cancel, if so then ignore
										continue;
								}
									/*else if(enrollmentAud.getEnrollmentTypeLkp()!=null && enrollmentAud.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP) && isEnrollmentFromPendingToCancel(enrollmentAud, eventDate)){ 		
										//check if it was from pending to cancel, if so then ignore
											continue;
									}*/
								}

								enrolleeLocal = setNormalFieldsFromAudit(enrolleeLocal,enrolleeAud);
								enrolleeLocal = setReferenceFieldsFromAudit(enrolleeLocal,enrolleeAud );
								setExtractsFieldsFormEnrollmentAud(enrolleeLocal , enrollmentAud); 
								setEnrollmentFieldsFromAudit(enrollmentLocal, enrollmentAud, enrollee.getEnrollment().getEmployer());
								//code to set csrAmount only for subscriber and event type cannot be cancel or term. The second logic is in the xsl.
								if(enrolleeAud.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
									setSubscriberCSRAmount(enrolleeLocal, enrollmentAud);
								}
								// Get by Revision
								List<EnrolleeRelationshipAud> relationAudList = enrolleeRelationshipAudRepository.findRelationShipRevisionBySourceTargetRev(enrollee.getId(), subscriber.getId(), eventRevId);
								if(null == relationAudList || relationAudList.isEmpty()){
									relationAudList= enrolleeRelationshipAudRepository.findLastRelationAudit(enrollee.getId(), subscriber.getId(), eventDate);
								}
								if(relationAudList!=null && !relationAudList.isEmpty()){
									EnrolleeRelationshipAud enrolleeRelationAud=relationAudList.get(0);
									if(enrolleeRelationAud != null){
										enrolleeLocal.setRelationshipToSubscriberLkp(enrolleeRelationAud.getRelationshipLkp());
									}
								}

								//write code to set EnrolleeRace list From history
								// Get by Revision
//								enrolleeLocal.setEnrolleeRace(getEnrolleeRaceFromRevision(enrollee.getId(),eventDate));
								enrolleeLocal.setEnrolleeRace(getEnrolleeRaceFromRevision(enrollee.getId(),eventDate, eventRevId));
								
								//Code to get Address from History
								if(enrollee.getHomeAddressid()!=null){
									// Get by Revision
//									enrolleeLocal.setHomeAddressid(getLocationFromRevision(enrollee.getHomeAddressid().getId(), eventDate));
									enrolleeLocal.setHomeAddressid(getLocationFromRevision(enrollee.getHomeAddressid().getId(), eventDate, eventRevId));
								}

								//Mailing Address
								if(enrollee.getMailingAddressId()!=null){
									// Get by Revision
//									enrolleeLocal.setMailingAddressId(getLocationFromRevision(enrollee.getMailingAddressId().getId(), eventDate));
									enrolleeLocal.setMailingAddressId(getLocationFromRevision(enrollee.getMailingAddressId().getId(), eventDate, eventRevId));
								}

								//Code to get and set Custodial parent from revision
								if(enrollee.getCustodialParent()!=null){
									// Get by Revision
//									enrolleeLocal.setCustodialParent(getCustodialOrResponsibleFromRevision(enrollee.getCustodialParent(), eventDate));
									enrolleeLocal.setCustodialParent(getCustodialOrResponsibleFromRevision(enrollee.getCustodialParent(), eventDate, eventRevId));
								}
								//Code to get responsible from Revision and set to enrollment
								if(responsiblePerson!=null){
									// Get by Revision
//									enrollmentLocal.setResponsiblePerson(getCustodialOrResponsibleFromRevision(responsiblePerson, eventDate));
									enrollmentLocal.setResponsiblePerson(getCustodialOrResponsibleFromRevision(responsiblePerson, eventDate, eventRevId));
								}
								
								List<EnrollmentEvent> perEnrolleeEventList= new ArrayList<EnrollmentEvent>();
								perEnrolleeEventList.add(event);
								enrolleeLocal.setEnrollmentEvents(perEnrolleeEventList);
								setDemographicChanges(enrolleeLocal);

								// Set this date to enrollment created in case of addition event
								if(event.getEventTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.EVENT_TYPE_ADDITION) && event.getSpclEnrollmentReasonLkp() == null){
									Date enrollmentDate = null;
									//HIX-65630
									enrollmentDate = enrolleeLocal.getEnrollment().getCreatedOn();
									if(isNotNullAndEmpty(enrollmentDate) && isNotNullAndEmpty(event.getCreatedOn()) && EnrollmentUtils.removeTimeFromDate(enrollmentDate).equals(EnrollmentUtils.removeTimeFromDate(event.getCreatedOn()))){
										enrolleeLocal.setFormatSignatureDate(enrollmentDate);
									}else{
										enrolleeLocal.setFormatSignatureDate(event.getCreatedOn());	
									}
								}
								// Set date of event created in case of event other than addition	
								else{
									enrolleeLocal.setFormatSignatureDate(event.getCreatedOn());
								}
								if(EnrollmentConfiguration.isCaCall()){
									if(EnrollmentUtils.compareAddress(enrolleeLocal.getHomeAddressid(), enrolleeLocal.getMailingAddressId())){
										enrolleeLocal.setShowMailingAddress(EnrollmentConstants.FALSE);
									}else{
										enrolleeLocal.setShowMailingAddress(EnrollmentConstants.TRUE);
									}
								}else if(enrolleeLocal.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
									if(EnrollmentUtils.compareAddress(enrolleeLocal.getHomeAddressid(), enrolleeLocal.getMailingAddressId())){
										enrolleeLocal.setShowMailingAddress(EnrollmentConstants.FALSE);
									}else{
										enrolleeLocal.setShowMailingAddress(EnrollmentConstants.TRUE);
									}
								}
								//set Rating Area
								try{
									enrolleeLocal.setRatingArea(EnrollmentUtils.getFormatedRatingArea(enrolleeLocal.getRatingArea()));
								}
								catch(Exception ex){
									LOGGER.error("Error in formatting Rating Area for Enrollee with id:: " +  enrollee.getId(), ex);
								}
								if(!enrollmentRevisionMap.containsKey(enrolleeAud.getRev())){
									enrollmentRevisionMap.put(enrolleeAud.getRev(),enrollmentLocal);
								}
								enrolleeLocal.setRev(enrolleeAud.getRev());
								enrollmentLocal.setRev(enrolleeAud.getRev());
								if(isNotNullAndEmpty(event.getSendRenewalTag())){
									enrolleeLocal.setRenewalStatus(event.getSendRenewalTag());
								}
								
								if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.SHOW_834_PREMIUM)) && !isPremiumListEmpty){
									enrolleeLocal.setEnrollmentPremiums(enrollmentPremiumList);
								}
								
								perEventEnrollee.add(enrolleeLocal);
								if((EnrollmentConstants.RENEWAL_FLAG_RENP.equalsIgnoreCase(event.getSendRenewalTag()) || EnrollmentConstants.RENEWAL_FLAG_REN.equalsIgnoreCase(event.getSendRenewalTag()))
										&& EnrollmentConstants.EVENT_TYPE_TERMINATION.equalsIgnoreCase(event.getEventTypeLkp().getLookupValueCode())){
									enrollmentLocal.setIsRenTermEvent(Boolean.TRUE);
								}
							}else{
								LOGGER.error("setPerRevisionEnrollmentAndEnrollee :: myEnrollee fetched from Audit is null for enrollee= "+ enrollee.getId() + " and event="+event.getId());
								if(!isCMS){
									throw new GIException("setPerRevisionEnrollmentAndEnrollee :: myEnrollee fetched from Audit is null for enrollee= "+ enrollee.getId() + " and event="+event.getId());
								}
							}
							
						}

					}
					LOGGER.debug("setPerRevisionEnrollmentAndEnrollee :: Ended for enrollee "+ enrollee.getId());
				}
			}else{
				LOGGER.debug("setPerRevisionEnrollmentAndEnrollee :: enrollee list empty");
			}
		}catch(Exception e){
			LOGGER.error("Exception in setPerEventEnrolleeFromEnrollee"+e.getMessage(),e);
			throw new GIException("Exception in setPerEventEnrolleeFromEnrollee"+e.getMessage(),e);
		}
		List<Enrollment> perRevisionEnrollment= null; 
		if(enrollmentRevisionMap!=null && perEventEnrollee!=null && !perEventEnrollee.isEmpty()){
			perRevisionEnrollment= setPerRevisionEnrollment(enrollmentRevisionMap, perEventEnrollee);
		}
		return perRevisionEnrollment;
	}
	
	private void sortPremiumList(List<EnrollmentPremium> enrollmentPremiumList) {
		Collections.sort(enrollmentPremiumList, new Comparator<EnrollmentPremium>() {
			@Override
			public int compare(EnrollmentPremium o1, EnrollmentPremium o2) {
				if(null != o1.getMonth() && null != o2.getMonth()){
					return Integer.compare(o1.getMonth(),  o2.getMonth());
				}
				return 0;
			}
		});
	}
	
	private boolean isPremiumListNullOrEmpty(List<EnrollmentPremium> enrollmentPremiumList) {
		boolean isEmptyOrNull = true;
		if(null != enrollmentPremiumList && !enrollmentPremiumList.isEmpty()) {
			for(EnrollmentPremium premium : enrollmentPremiumList){
				if(null != premium.getGrossPremiumAmount() && (premium.getGrossPremiumAmount().compareTo(0.0f) > 0)){
					isEmptyOrNull = false;
				}
			}
		}
		return isEmptyOrNull;
	}
	
	private boolean isEnrollmentFromPendingToCancel(EnrollmentAud enrollment, Date eventDate, Integer eventRevId) throws GIException {
		boolean isFromPendingToCancel = false;
		try{
			if(enrollment.getEnrollmentStatusLkp() != null && 
					enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
				List<EnrollmentAud> enrollmentAudList = enrollmentAudRepository.getEnrollmentByIdRevAndNoPendingStatus(enrollment.getId(), eventRevId);
				if(null == enrollmentAudList || enrollmentAudList.isEmpty()){
					enrollmentAudList = enrollmentAudRepository.getEnrollmentByIdAndNoPendingStatus(enrollment.getId(), eventDate);	
				}
				if(enrollmentAudList == null || enrollmentAudList.isEmpty()){
					isFromPendingToCancel = true;
				}
			}
		}catch(Exception e){
			LOGGER.error("filterOutPendingToCancelEnrollments :: " + e.getMessage(),e);
			throw new GIException("Error in filterOutPendingToCancelEnrollments :: " + e.getMessage(),e);
		}
		
		return isFromPendingToCancel;
	}
	
	private Enrollee getCustodialOrResponsibleFromRevision(Enrollee enrollee, Date eventDate, Integer eventRevId) throws GIException {
		Enrollee localEnrollee=null;
		try{
			//Revision<Integer,Enrollee> lastSubscriberRevision = enrolleeRepository.findLastSubscriberChangeRevision(enrollee.getId(), enrolleeRevisionNumber);
			List<EnrolleeAud> custodialOrResponsibleList= enrolleeAudRepository.getEnrolleeAudByIdAndRev(enrollee.getId(),eventRevId);
			if(null == custodialOrResponsibleList || custodialOrResponsibleList.isEmpty()){
				custodialOrResponsibleList= enrolleeAudRepository.findEnrolleeRevByDate(enrollee.getId(),eventDate);
			}
			if(custodialOrResponsibleList!=null && !custodialOrResponsibleList.isEmpty()){
				EnrolleeAud custodialOrResponsible=custodialOrResponsibleList.get(0);
				if(custodialOrResponsible!=null){
					localEnrollee= new Enrollee();
					localEnrollee=setNormalFieldsFromAudit(localEnrollee, custodialOrResponsible);
					if(enrollee.getHomeAddressid()!=null){
						localEnrollee.setHomeAddressid(getLocationFromRevision(enrollee.getHomeAddressid().getId(), eventDate, eventRevId));
					}

					if(enrollee.getMailingAddressId()!=null){
						localEnrollee.setMailingAddressId(getLocationFromRevision(enrollee.getMailingAddressId().getId(), eventDate, eventRevId));
					}
				}
			}
		}catch( Exception e){
			LOGGER.error("Exception in getCustodialOrResponsibleFromRevision"+e.getMessage(),e);
			throw new GIException("Exception in getCustodialOrResponsibleFromRevision"+e.getMessage(),e);
		}
		return localEnrollee;
	}
	
	private Location getLocationFromRevision(int locationID, Date eventDate, Integer eventRevId) throws GIException {
		Location localLocation=null;
		try{
			List<LocationAud> revLocationAudList = locationAudRepository.getLocationAudByIdAndRev(locationID, eventRevId);
			if(null == revLocationAudList || revLocationAudList.isEmpty()){
				revLocationAudList = locationAudRepository.getLocationAudByIdAndDate(locationID, eventDate);
			}
			if(revLocationAudList != null && !revLocationAudList.isEmpty()){
				localLocation = new Location();
				localLocation = setLocationFieldsFromAudit(localLocation, revLocationAudList.get(0));
			}
		}catch(Exception e){
			LOGGER.error("Exception in getLocationFromRevision"+e.getMessage(),e);
			throw new GIException("Exception in getLocationFromRevision"+e.getMessage(),e);
		}
		return localLocation;
	}
	private List<EnrolleeRace> getEnrolleeRaceFromRevision(int enrolleeID, Date eventDate, Integer eventRevId) throws GIException {

		List<EnrolleeRace> raceList= null;
		try{
			Integer erRevisionNumber = enrolleeRaceAudRepository.getEnrolleeRaceAudByEnrolleeIdAndRev(enrolleeID, eventRevId);
			if(null == erRevisionNumber || erRevisionNumber == 0){
				erRevisionNumber = enrolleeRaceAudRepository.findLastRaceRevisionByDate(enrolleeID, eventDate);
			}
			if(erRevisionNumber!=null){
				List<EnrolleeRaceAud> enrolleeRaceAudList=enrolleeRaceAudRepository.getRaceAudByEnrolleeIdAndRev(enrolleeID,erRevisionNumber);
				
				if(enrolleeRaceAudList!=null && !enrolleeRaceAudList.isEmpty()){
					raceList=new ArrayList<EnrolleeRace>();
					for (EnrolleeRaceAud enrolleeRaceAud: enrolleeRaceAudList ){
						EnrolleeRace raceEntity= new EnrolleeRace();
						raceEntity.setRaceEthnicityLkp(enrolleeRaceAud.getRaceEthnicityLkp());
						raceList.add(raceEntity);
					}
					
				}
				
				/*
				
				List<Object> objList = enrolleeRaceRepository.findRacesCreatedForEnrollee(enrolleeID, erRevisionNumber);
				int size=objList.size();
				EnrolleeRace raceEntity=null;

				for(int i=0;i<size;i++){
					Object[] objArray = (Object[])objList.get(i);
					if(objArray!=null){
						raceEntity = (EnrolleeRace)objArray[0];
					}
					if(raceEntity!=null){
						EnrolleeRaceAud enrolleeRaceAud=enrolleeRaceAudRepository.getEnrolleeRaceAudByIdAndRev(raceEntity.getId(),erRevisionNumber);
						if(enrolleeRaceAud!=null){
							raceEntity.setRaceEthnicityLkp(enrolleeRaceAud.getRaceEthnicityLkp());
						}
						raceList.add(raceEntity);
					}
				}
			*/
				}
		}catch(Exception e){
			LOGGER.error("Exception in getEnrolleeRaceFromRevision" + e.getMessage(),e);
			throw new GIException("Exception in getEnrolleeRaceFromRevision"+e.getMessage(),e);
		}
		return raceList;
	
	}
	private List<EnrolleeRace> getEnrolleeRaceFromRevision(Integer enrolleeID, Date eventDate) throws GIException{
		List<EnrolleeRace> raceList= null;
		try{
			//Integer erRevisionNumber = enrolleeRaceRepository.findLastRaceRevision(enrolleeID, enrolleeRevisionNumber);
			
			Integer erRevisionNumber = enrolleeRaceAudRepository.findLastRaceRevisionByDate(enrolleeID, eventDate);
			
			if(erRevisionNumber!=null){
				List<EnrolleeRaceAud> enrolleeRaceAudList=enrolleeRaceAudRepository.getRaceAudByEnrolleeIdAndRev(enrolleeID,erRevisionNumber);
				
				if(enrolleeRaceAudList!=null && !enrolleeRaceAudList.isEmpty()){
					raceList=new ArrayList<EnrolleeRace>();
					for (EnrolleeRaceAud enrolleeRaceAud: enrolleeRaceAudList ){
						EnrolleeRace raceEntity= new EnrolleeRace();
						raceEntity.setRaceEthnicityLkp(enrolleeRaceAud.getRaceEthnicityLkp());
						raceList.add(raceEntity);
					}
					
				}
				
				/*
				
				List<Object> objList = enrolleeRaceRepository.findRacesCreatedForEnrollee(enrolleeID, erRevisionNumber);
				int size=objList.size();
				EnrolleeRace raceEntity=null;

				for(int i=0;i<size;i++){
					Object[] objArray = (Object[])objList.get(i);
					if(objArray!=null){
						raceEntity = (EnrolleeRace)objArray[0];
					}
					if(raceEntity!=null){
						EnrolleeRaceAud enrolleeRaceAud=enrolleeRaceAudRepository.getEnrolleeRaceAudByIdAndRev(raceEntity.getId(),erRevisionNumber);
						if(enrolleeRaceAud!=null){
							raceEntity.setRaceEthnicityLkp(enrolleeRaceAud.getRaceEthnicityLkp());
						}
						raceList.add(raceEntity);
					}
				}
			*/
				}
		}catch(Exception e){
			LOGGER.error("Exception in getEnrolleeRaceFromRevision" + e.getMessage(),e);
			throw new GIException("Exception in getEnrolleeRaceFromRevision"+e.getMessage(),e);
		}
		return raceList;
	}	
	
	private Enrollee getCustodialOrResponsibleFromRevision(Enrollee enrollee, Date eventDate) throws GIException{
		Enrollee localEnrollee=null;
		try{
			//Revision<Integer,Enrollee> lastSubscriberRevision = enrolleeRepository.findLastSubscriberChangeRevision(enrollee.getId(), enrolleeRevisionNumber);
			List<EnrolleeAud> custodialOrResponsibleList= enrolleeAudRepository.findEnrolleeRevByDate(enrollee.getId(),eventDate);
			if(custodialOrResponsibleList!=null && !custodialOrResponsibleList.isEmpty()){
				EnrolleeAud custodialOrResponsible=custodialOrResponsibleList.get(0);
				if(custodialOrResponsible!=null){
					localEnrollee= new Enrollee();
					localEnrollee=setNormalFieldsFromAudit(localEnrollee, custodialOrResponsible);
					if(enrollee.getHomeAddressid()!=null){
						localEnrollee.setHomeAddressid(getLocationFromRevision(enrollee.getHomeAddressid().getId(), eventDate));
					}

					if(enrollee.getMailingAddressId()!=null){
						localEnrollee.setMailingAddressId(getLocationFromRevision(enrollee.getMailingAddressId().getId(), eventDate));
					}
				}
			}
		}catch( Exception e){
			LOGGER.error("Exception in getCustodialOrResponsibleFromRevision"+e.getMessage(),e);
			throw new GIException("Exception in getCustodialOrResponsibleFromRevision"+e.getMessage(),e);
		}
		return localEnrollee;
	}	
	private Location getLocationFromRevision(Integer locationID, Date eventDate) throws GIException{
		Location localLocation=null;
		try{
			//LocationAud revLocationAud = locationRepository.getLocationAudByIdAndRev(locationID, revisionNumber);
			List<LocationAud> revLocationAudList = locationAudRepository.getLocationAudByIdAndDate(locationID, eventDate);
			if(revLocationAudList != null && !revLocationAudList.isEmpty()){
				localLocation = new Location();
				localLocation = setLocationFieldsFromAudit(localLocation, revLocationAudList.get(0));
			}
		}catch(Exception e){
			LOGGER.error("Exception in getLocationFromRevision"+e.getMessage(),e);
			throw new GIException("Exception in getLocationFromRevision"+e.getMessage(),e);
		}
		return localLocation;
	}
	
	private List<Enrollment> setPerRevisionEnrollment(Map<Integer,Enrollment> enrollmentRevisionMap, List<Enrollee> perEventEnrollee) throws GIException{
		List<Enrollment> revisionEnrollmentList=null;
		try{
			if(enrollmentRevisionMap!=null && perEventEnrollee!=null){
				Enrollment enrollment=null;
				revisionEnrollmentList=new ArrayList<Enrollment>();
				for(Integer key: enrollmentRevisionMap.keySet()){
					int subscriberCount=0;
					enrollment=enrollmentRevisionMap.get(key);
					List<Enrollee> enrolleeList=new ArrayList<Enrollee>();
					for(Enrollee enrollee:perEventEnrollee){
						if(key.equals(enrollee.getRev())){
							enrolleeList.add(enrollee);
							if(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
								subscriberCount++;
							}
						}
					}
					if(!enrolleeList.isEmpty()){
						enrolleeList = sortEnrolleeListBySubscriberFirst(enrolleeList);
						enrollment.setEnrollees(enrolleeList);
						enrollment.setQTYt(enrolleeList.size());
						enrollment.setQTYn(enrolleeList.size()-subscriberCount);
						enrollment.setQTYy(subscriberCount);
						revisionEnrollmentList.add(enrollment);
					}
				}
			}
			if(revisionEnrollmentList!=null && !revisionEnrollmentList.isEmpty()){
				Collections.sort(revisionEnrollmentList);
			}
		}catch(Exception e){
			LOGGER.error("Exception in setPerRevisionEnrollment"+e.getMessage(),e);
			throw new GIException("Exception in setPerRevisionEnrollment"+e.getMessage(),e);
		}
		
		return revisionEnrollmentList;
	}
	private void setExtractsFieldsFormEnrollmentAud(Enrollee enrollee, EnrollmentAud enrollmentAud){
		if(enrollee == null || enrollmentAud == null){
			return;
		}
		enrollee.setEmployerContribution(enrollmentAud.getEmployerContribution());
		if(enrollmentAud.getAptcAmt()!=null){
			enrollee.setAptcAmt(enrollmentAud.getAptcAmt());
		}
		//enrollee.setCsrAmt(enrollmentAud.getCsrAmt());
		enrollee.setGrossPremiumAmt(enrollmentAud.getGrossPremiumAmt());
		enrollee.setNetPremiumAmt(enrollmentAud.getNetPremiumAmt());
		
		//set effective date fields too
		enrollee.setEmplContEffDate(enrollmentAud.getEmplContEffDate());
		enrollee.setAptcEffDate(enrollmentAud.getAptcEffDate());
		enrollee.setCsrEffDate(enrollmentAud.getCsrEffDate());
		enrollee.setGrossPremEffDate(enrollmentAud.getGrossPremEffDate());
		enrollee.setNetPremEffDate(enrollmentAud.getNetPremEffDate());
		
	}
	private void setSubscriberCSRAmount(Enrollee enrollee, EnrollmentAud enrollmentAud){
		if(enrollee!=null && enrollmentAud!=null && enrollmentAud.getCsrAmt()!=null && enrollmentAud.getCsrAmt()>0.0f){
			Date jan2015CoverageStartDate = DateUtil.StringToDate(EnrollmentConstants.JAN_2015_DATE, GhixConstants.REQUIRED_DATE_FORMAT);
			if(enrollee.getEnrollment().getBenefitEffectiveDate().compareTo(jan2015CoverageStartDate) >= 0){
				enrollee.setCsrAmt(enrollmentAud.getCsrAmt());
			}

			else {
				long activeEnrolleeCount=0;
				activeEnrolleeCount= enrolleeAudRepository.countActiveEnrolleeForRevision(enrollmentAud.getId(), enrollmentAud.getRev());
				if(activeEnrolleeCount>0){
					enrollee.setCsrAmt(activeEnrolleeCount*enrollmentAud.getCsrAmt());
				}
			}
		}
	}
	
//	private boolean isSubscriber(Enrollee enrollee){
//		boolean isSubscriber = false;
//		if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
//			isSubscriber = true;
//		}
//		return isSubscriber;
//	}
	
	
/*	private List<Enrollment> setPerRevisionEnrollmentFor834Daily(){
		
	}*/
	
	/**
	 * @since
	 * @author parhi_s
	 * @param issuer
	 * @param lastRunDate
	 * @param enrollmentType
	 * @throws Exception
	 * This method is used to create Enrollments object for the XML
	 * generation purpose
	 */
	private Enrollments setEnrollmentsForCMSDaily(List<Enrollment> enrollmentList, Date startDate, Date endDate,String enrollmentType) throws GIException {

		Enrollments enrollments = new Enrollments();

		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			List<Enrollment> updatedEnrollmentList = new ArrayList<Enrollment>();
			//			enrollments.setEnrollmentList(enrollmentList);

			for (Enrollment enrollment : enrollmentList) {

				try {
					// Filter Out enrollments which are cancelled from Pending status for Shop Type Enrollment.
					if(isNotNullAndEmpty(enrollmentType) && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)
							&& isEnrollmentFromPendingToCancel(enrollment)){
							continue;
					}

					List<Enrollee> enrolleesList=null;

					List<Enrollee> allEnrolleeList = enrollment.getEnrollees();

					enrolleesList=filterEnrolleesForCMSDaily(allEnrolleeList, startDate,endDate, enrollment.getRenewalFlag());
					if(enrolleesList!=null && !enrolleesList.isEmpty()){
						enrollment.setEnrollees(enrolleesList);

						if(enrollment.getInsurerTaxIdNumber()!=null){
							enrollment.setInsurerTaxIdNumber(enrollment.getInsurerTaxIdNumber().replaceAll("[-]", ""));
						}
						List<Enrollment> revisionEnrollmentList=null;
						if(enrolleesList != null && !enrolleesList.isEmpty()){
							
								revisionEnrollmentList=setPerRevisionEnrollmentAndEnrollee(enrolleesList, true);
						}

						if(revisionEnrollmentList != null && !revisionEnrollmentList.isEmpty()){
							updatedEnrollmentList.addAll(revisionEnrollmentList);
						}
					}
					
				} catch (Exception e) {
					LOGGER.error("Error occured in setEnrollmentsFor834Daily. ", e);
					throw new GIException("Error occured in setEnrollmentsFor834Daily()::"+e.getMessage(),e);
				}

			}
			enrollments.setEnrollmentList(updatedEnrollmentList);
		}
		return enrollments;
	}
	
	
	/**
	 * @since
	 * @author parhi_s
	 * @param issuer
	 * @param lastRunDate
	 * @param enrollmentType
	 * @throws Exception
	 * This method is used to create Enrollments object for the XML
	 * generation purpose
	 */
	private List<Enrollments> setEnrollmentsFor834Daily(List<Enrollment> enrollmentList, Date startDate, Date endDate,String enrollmentType,  Map<Integer,String> failedEnrollmentsMap, Set<Integer> successEnrollmentIdList, boolean isResendJob) throws GIException {
		List<Enrollments> enrollmentsList = new ArrayList<Enrollments>();
		Enrollments enrollments = new Enrollments();
		Enrollments renTermEnrollments = new Enrollments();
		renTermEnrollments.setIsRenEnrollments(Boolean.TRUE);

		if (enrollmentList != null && !enrollmentList.isEmpty()) {
			List<Enrollment> updatedEnrollmentList = new ArrayList<Enrollment>();
			List<Enrollment> updatedRenEventEnrollmentList = new ArrayList<Enrollment>();
			List<Enrollment> updatedOtherEnrollmentList = new ArrayList<Enrollment>();
			//			enrollments.setEnrollmentList(enrollmentList);

			for (Enrollment enrollment : enrollmentList) {
				try {
					
					LOGGER.debug(Thread.currentThread().getName() + " : setEnrollmentsFor834Daily2 :: Started processing for enrollment id="+enrollment.getId());
					
					
					//add the enrollment into failed map if it got any error in reader
					if(enrollment.getReaderStatus()!=null && enrollment.getReaderStatus().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_EVENT_STATUS_FAILED)){
						failedEnrollmentsMap.put(enrollment.getId(), enrollment.getReaderErrorMsg());
						continue;
					}
					
					long failedEventsForEnrollmentCount = enrollmentEventRepository.getFailedEventsCountForEnrollment(enrollment.getId());
					/** Check if Enrollment has any events in FAILED status. If Yes mark enrollment and any new/Resend events also as FAILED */
					if(failedEventsForEnrollmentCount > 0){
						
						if(isResendJob){
							throw new GIException("Failed Event for Enrollment Exists.");
						}
						
						failedEnrollmentsMap.put(enrollment.getId(), "Failed Event for Enrollment Exists.");
						continue;
					}else{
					List<Enrollee> enrolleesList=enrollment.getEnrollees();
					
					if(enrollment.getInsurerTaxIdNumber()!=null){
						enrollment.setInsurerTaxIdNumber(enrollment.getInsurerTaxIdNumber().replaceAll("[-]", ""));
					}
					List<Enrollment> revisionEnrollmentList=null;
					if(enrolleesList != null && !enrolleesList.isEmpty()){
							revisionEnrollmentList=setPerRevisionEnrollmentAndEnrollee(enrolleesList, false);
							if(revisionEnrollmentList != null && !revisionEnrollmentList.isEmpty()){
								updatedEnrollmentList.addAll(revisionEnrollmentList);
							}else{
								LOGGER.debug(Thread.currentThread().getName() +  " : Empty list returned by setPerRevisionEnrollmentAndEnrollee for enrollment :" + enrollment.getId());
							}
					}else{
						LOGGER.debug(Thread.currentThread().getName() +  " : No enrollee  found for enrollment :" + enrollment.getId());
						continue;
					}
				}
					successEnrollmentIdList.add(enrollment.getId());
				} catch (Exception e) {
					LOGGER.error("Error occured in setEnrollmentsFor834Daily :: for enrollment id= "+enrollment.getId(), e);
					if(isResendJob){
						throw new GIException("Failed Event for Enrollment Exists.");
					}

					failedEnrollmentsMap.put(enrollment.getId(), "Error occured in setEnrollmentsFor834Daily() :: "+ e.getMessage());
				}

			}
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			boolean isIdahoStateCode = null!=stateCode && EnrollmentConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode); 
			for(Enrollment enrollment : updatedEnrollmentList){
				if(enrollment.getIsRenTermEvent() && isIdahoStateCode){
					updatedRenEventEnrollmentList.add(enrollment);
				}else{
					updatedOtherEnrollmentList.add(enrollment);
				}
			}
			enrollments.setEnrollmentList(updatedOtherEnrollmentList);
			renTermEnrollments.setEnrollmentList(updatedRenEventEnrollmentList);
			enrollmentsList.add(enrollments);
			enrollmentsList.add(renTermEnrollments);
		}
		return enrollmentsList;
	}

	
	
	/**
	 * @since
	 * @author parhi_s
	 * @param issuer
	 * @param lastRunDate
	 * @param enrollmentType
	 * @throws Exception
	 * This method is used to create Enrollments object for the XML
	 * generation purpose
	 *//*
	private Enrollments setEnrollmentsFor834Daily(List<Enrollment> enrollmentList, Date startDate, Date endDate,String enrollmentType) throws GIException {
		
		Enrollments enrollments = new Enrollments();
		
		if (enrollmentList != null && enrollmentList.size() > 0) {
			List<Enrollment> updatedEnrollmentList = new ArrayList<Enrollment>();
			//			enrollments.setEnrollmentList(enrollmentList);
			
			for (Enrollment enrollment : enrollmentList) {

				try {
					// Filter Out enrollments which are cancelled from Pending status for Shop Type Enrollment.
					if(isNotNullAndEmpty(enrollmentType) && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
						if(isEnrollmentFromPendingToCancel(enrollment)){
							continue;
						}
					}
					
					long activeEnrolleeCount=0;
					List<Enrollee> enrolleesList=null;
					
					List<Enrollee> allEnrolleeList = enrollment.getEnrollees();
					
					activeEnrolleeCount=enrolleeRepository.totalActiveMembersByEnrollmentID(enrollment.getId());
					
					//enrolleesList=filterEnrolleesForEDIDaily(allEnrolleeList, startDate,endDate, enrollmentType);
					enrolleesList=filterEnrolleesForEDIDaily(allEnrolleeList, startDate,endDate);
					enrollment.setEnrollees(enrolleesList);
					
					if(enrollment.getInsurerTaxIdNumber()!=null){
						enrollment.setInsurerTaxIdNumber(enrollment.getInsurerTaxIdNumber().replaceAll("[-]", ""));
					}
					
					if(enrolleesList!=null && enrolleesList.size()>0){
						//Find subscriber
						Enrollee subscriber = checkForSubscriber(enrolleesList);

						
						// Subscriber here could be null, as subscriber may not be needed for extracts, in that case pass
						// subscriber Id from ellEnrollees list. Subscriber Id is needed to query EnrolleeAud repository for relationship and other stuff.
		 	 	 	 	if(subscriber == null){
		 	 	 	 		Enrollee noExtractSubscriber = checkForSubscriber(allEnrolleeList);
		 	 	 	 		enrolleesList=setPerEventEnrolleeFromEnrollee(enrolleesList,subscriber,noExtractSubscriber.getId());
		 	 	 	 	}
		 	 	 	 	else{
		 	 	 	 		enrolleesList=setPerEventEnrolleeFromEnrollee(enrolleesList,subscriber,subscriber.getId());
		 	 	 	 	}
		 	 	 	 	
						 // Revisit the code to set QTY values
						 // Code to set QTY values
		 	 	 	 	
		 	 	 	 	// calculate subscriber event count before resetting subscriber from all enrollee list of enrollment.
		 	 	 	 	int subscriberEvent = 0;
		 	 	 	 	if(subscriber!=null){
		 	 	 	 		// Sort the list, so that subscriber is first member. Order of other members don't matter.
		 	 	 	 		enrolleesList = sortEnrolleeListBySubscriberFirst(enrolleesList);
		 	 	 	 		subscriberEvent = subscriber.getEnrollmentEvents().size();
		 	 	 	 	}
		 	 	 	 	
		 	 	 	 	// If subscriber is null, fetch form all enrollees list, after consedering all events anc count required for extracts.
		 	 	 	 	if(subscriber==null){
							subscriber=checkForSubscriber(allEnrolleeList);
						}
		 	 	 	 	
		 	 	 	 	int totalEnrolleeEvent = getEnrollmentEvent(enrollment.getEnrollees(), subscriber);
		 	 	 	 	enrollment.setQTYy(subscriberEvent);
		 	 	 	 	enrollment.setQTYn(totalEnrolleeEvent-subscriberEvent);
		 	 	 	 	enrollment.setQTYt(totalEnrolleeEvent);
						
						for (Enrollee enrollee: enrolleesList){
							try {
								enrollee.setShowMailingAddress(EnrollmentConstants.TRUE);
								if(compareAddress(enrollee.getHomeAddressid(), enrollee.getMailingAddressId())){
									enrollee.setShowMailingAddress(EnrollmentConstants.FALSE);
								}
							}
							 catch (Exception e) {
								//	e.printStackTrace();
								LOGGER.error("" ,e);
								throw new GIException("Error occured in setEnrollmentsFor834Daily()::"+e.getMessage(),e);
							}
						}
					}
					
					enrollment.setEnrollees(enrolleesList);
					
					//code to set total CSR amount
					if(enrollment.getCsrAmt()!=null){
						enrollment.setTotalCSRAmt(activeEnrolleeCount*enrollment.getCsrAmt());
					}
					Enrollee responsibleEnrollee = enrolleeService.getResponsiblePersonByEnrollmentID(enrollment.getId());
					
					if(responsibleEnrollee != null){
					enrollment.setResponsiblePerson(responsibleEnrollee);
					}
																
				} catch (Exception e) {
					LOGGER.error("Error occured in setEnrollmentsFor834Daily. ", e);
					throw new GIException("Error occured in setEnrollmentsFor834Daily()::"+e.getMessage(),e);
				}
				 if(enrollment.getEnrollees()!=null && enrollment.getEnrollees().size()>0){
					 updatedEnrollmentList.add(enrollment);	 
				 }
				
			}
			enrollments.setEnrollmentList(updatedEnrollmentList);
	  }
		return enrollments;
	}*/
	
	protected List<Enrollee> sortEnrolleeListBySubscriberFirst(List<Enrollee> enrolleeList){
		List<Enrollee> sortedEnrolleeList = new ArrayList<Enrollee>();
		List<Enrollee> nonSubscriberEnrolleeList = new ArrayList<Enrollee>();
		for(Enrollee enrollee : enrolleeList){
			if(enrollee != null && enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode() != null){
				if(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
					// Sort to add enrollees to top of extracts list.
					if(enrollee.getEnrolleeLkpValue() != null) { //&& !enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
						sortedEnrolleeList.add(enrollee);
					}
					
					// If enrollee status is cancel it should go at end in extracts.
					else{
						nonSubscriberEnrolleeList.add(enrollee);
					}
				}
				// add other non subscriber enrollees to different list.
				else{
					nonSubscriberEnrolleeList.add(enrollee);
				}
			}
		}
		// add all enrollees, to get subscriber at top, followed by other enrollees.
		sortedEnrolleeList.addAll(nonSubscriberEnrolleeList);
		return sortedEnrolleeList;
	}
	
			


	/**
	 * @author parhi_s
	 * @since
	 * Method used by batch services to check if any enrollee has undergone demographic changes and if so then set the incorrect details
	 * @param enrollee
	 * @param lastRunDate
	 * @throws GIException
	 */
	private void setDemographicChanges(Enrollee enrollee) throws GIException{
		try{
			if(enrollee != null && enrollee.getEnrollmentEvents() !=null){
				List<EnrollmentEvent> enrollmentEventList = enrollee.getEnrollmentEvents();
				//boolean isEnrolleeRaceChanged = false; 
				for(EnrollmentEvent event : enrollmentEventList){
					if(event.getEventTypeLkp().getLookupValueCode().equals(EnrollmentConstants.EVENT_TYPE_CHANGE)){
						Date eventDate= (event.getUpdatedOn()!=null) ? event.getUpdatedOn() : event.getCreatedOn();
						Integer eventRevId = enrollmentEventAudRepository.getRevIdByEventId(event.getId());
						List<EnrolleeAud>  currentEnrolleeAudList = enrolleeAudRepository.getEnrolleeAudByIdEventIdAndRev(enrollee.getId(), event.getId(), eventRevId);
						if(null != currentEnrolleeAudList && !currentEnrolleeAudList.isEmpty()){
							EnrolleeAud currentEnrollee = currentEnrolleeAudList.get(0);
							if(eventDate!=null){
								Calendar calendar = TSCalendar.getInstance();
								calendar.setTime(eventDate);
								calendar.add(Calendar.SECOND, -5);
								eventDate= calendar.getTime();
							}
							//Revision<Integer, Enrollee> myEnrollee=null;
							//Enrollee oldEnrollee=null;
							EnrolleeAud enrolleeAud=null;
							/*myEnrollee = enrolleeAuditService.findRevisionByDate(enrollee.getId(),event.getCreatedOn());
							if(myEnrollee!= null){
								oldEnrollee = myEnrollee.getEntity();
								enrolleeAud  = enrolleeAudRepository.getEnrolleeAudByIdAndRev(oldEnrollee.getId(), myEnrollee.getRevisionNumber());
							}*/
							List<EnrolleeAud> enrolleeAudList= enrolleeAudRepository.findPreviousEnrolleeRevByDate(enrollee.getId(), currentEnrollee.getUpdatedOn());
							if(enrolleeAudList!=null && !enrolleeAudList.isEmpty()){
								enrolleeAud=enrolleeAudList.get(0);
							}
							// use enrolleeAud for lookup values
							boolean setIncorrectMemberLastName = false;
							Boolean nm1ChangeFlag = false;
							Boolean dmgChangeFlag = false;

							if(enrolleeAud != null)	{
								if(!compareString(enrollee.getFirstName() , enrolleeAud.getFirstName())
										|| !compareString(enrollee.getLastName() , enrolleeAud.getLastName())
										|| !compareString(enrollee.getMiddleName(), enrolleeAud.getMiddleName())
										|| !compareString(enrollee.getSuffix(), enrolleeAud.getSuffix())){
									setIncorrectMemberLastName = true;
									nm1ChangeFlag = true;
								}							
								
								if(!compareString(enrollee.getNationalIndividualIdentifier(), enrolleeAud.getNationalIndividualIdentifier())){
									enrollee.setIncorrectMemberNationalIndivID(enrolleeAud.getNationalIndividualIdentifier());
									setIncorrectMemberLastName = true;
									nm1ChangeFlag = true;
								}
								if(!compareString(enrollee.getTaxIdNumber(), enrolleeAud.getTaxIdNumber())){
									enrollee.setIncorrectMemberSSN(enrolleeAud.getTaxIdNumber());
									setIncorrectMemberLastName = true;
									nm1ChangeFlag = true;
								}
								if(!(DateUtil.isEqual(enrollee.getBirthDate(), enrolleeAud.getBirthDate()))){
									enrollee.setIncorrectMemberBirthDate(enrolleeAud.getBirthDate());
									setIncorrectMemberLastName = true;
									dmgChangeFlag = true;
								}
								if(enrolleeAud!=null){
									if(!compareLookup(enrollee.getGenderLkp(), enrolleeAud.getGenderLkp())){
										if(enrolleeAud.getGenderLkp() != null){
										enrollee.setIncorrectMemberGenderLkp(enrolleeAud.getGenderLkp());
										}
										else{
											enrollee.setIncorrectMemberGenderLkp(lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentConstants.GENDER, EnrollmentConstants.GENDER_UNKNOWN_LABEL.toLowerCase()));
										}
										setIncorrectMemberLastName = true;
										dmgChangeFlag = true;
									}
									if(!compareLookup(enrollee.getMaritalStatusLkp(), enrolleeAud.getMaritalStatusLkp())){
										if(enrolleeAud.getMaritalStatusLkp() != null){
										enrollee.setIncorrectMemberMaritalStatusLkp(enrolleeAud.getMaritalStatusLkp());
										}
										else{
											enrollee.setIncorrectMemberMaritalStatusLkp(lookupService.getlookupValueByTypeANDLookupValueLabel(EnrollmentConstants.MARITAL_STATUS, EnrollmentConstants.MARITAL_STATUS_UNREPORTED_LABEL.toLowerCase()));
										}
										setIncorrectMemberLastName = true;
										dmgChangeFlag = true;
									}
									if(!compareLookup(enrollee.getCitizenshipStatusLkp(), enrolleeAud.getCitizenshipStatusLkp())){
										if (enrolleeAud.getCitizenshipStatusLkp() != null) {
											enrollee.setIncorrectMemberCitizenshipStatusLkp(enrolleeAud.getCitizenshipStatusLkp());
										} else {
											enrollee.setIncorrectMemberCitizenshipStatusLkp(enrollee.getCitizenshipStatusLkp());
										}
										setIncorrectMemberLastName = true;
										dmgChangeFlag = true;
									}
								}

								List<EnrolleeRace> oldRaceList= getEnrolleeRaceFromRevision(enrollee.getId(), enrolleeAud.getUpdatedOn(), enrolleeAud.getRev());
								if(!EnrollmentUtils.compareEnrolleerace(oldRaceList, enrollee.getEnrolleeRace())){
									if(oldRaceList != null && !oldRaceList.isEmpty()){
										enrollee.setIncorrectMemberRaceEthnicityLkp(oldRaceList);
									}
									else{
										// Jira Id: HIX-76870
										enrollee.setIncorrectMemberRaceEthnicityLkp(getDefaultEnrolleeRaceList());
									}
									setIncorrectMemberLastName = true;
								}

								if(setIncorrectMemberLastName){
									enrollee.setIncorrectMemberLastName(enrolleeAud.getLastName());
									enrollee.setIncorrectMemberFirstName(enrolleeAud.getFirstName());
									enrollee.setIncorrectMemberMiddleName(enrolleeAud.getMiddleName());
									enrollee.setIncorrectMemberNameSuffix(enrolleeAud.getSuffix());
								}
							}
							
							/** If changes are present only to DMG set this to IL */
							if(dmgChangeFlag && ! nm1ChangeFlag){
								enrollee.setMemberEntityIdentifierCode("IL");
							}
							else if(nm1ChangeFlag){/** Else set the code to 74 if there are changes to NM1, or both NM1 and DMG */
								enrollee.setMemberEntityIdentifierCode("74");
							}else{//HIX-85063:Always add Subscriber Event is all 834 transactions
								enrollee.setMemberEntityIdentifierCode("IL");
							}
						}
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Error in setDemographicChanges() :: For Enrollee id = "+enrollee.getId()+e.getMessage(),e);
			throw new GIException("Error in setDemographicChanges():: For Enrollee id = "+enrollee.getId()+e.getMessage(),e);
		}
	}
	
	//Method to compare lookup by lookup value
    private boolean compareLookup(LookupValue firstLKP, LookupValue secondLKP){
    	boolean isEqual = false;
    	if(firstLKP==null && secondLKP==null){
    		isEqual=true;
    	}else if(firstLKP !=null && secondLKP!=null ){
    		isEqual=compareString(firstLKP.getLookupValueCode(), secondLKP.getLookupValueCode());
    	}
    	return isEqual;
    }	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to compare two strings.
	 * @param firstString
	 * @param secondString
	 * @return
	 */
	
	private boolean compareString(String firstString,String secondString) {
		boolean isEqual = false;
		String newFirstString;
		String newSecondString;
		if(firstString ==null &&secondString==null){
			isEqual=true;
		}
		else if(StringUtils.isNotBlank(firstString) && StringUtils.isNotBlank(secondString)){
			newFirstString=firstString.trim();
			newSecondString=secondString.trim();
			isEqual=newFirstString.equalsIgnoreCase(newSecondString);
		}
		return isEqual;
	}
	
    //Method to move files from one folder to another folder. If lof is null then it will move all files from source to target folder else it moves the list of files
    private void moveFiles(String sourceFolder, String targetFolder, String fileType, List<File> lof) throws GIException{
          try{
        	  if (sourceFolder!=null && !("".equals(sourceFolder.trim())) &&!(new File(sourceFolder).exists())) {
                  return;
             }
             if (targetFolder!=null && !("".equals(targetFolder.trim())) &&!(new File(targetFolder).exists())) {
                  new File(targetFolder).mkdirs();
             }
             
             if(lof != null && !lof.isEmpty()){
                  for(File file:lof){
                       file.renameTo(new File(targetFolder+File.separator+file.getName()));
                  }
             }else{
                  //move all files from source to target folder
                  File[] files=getFilesInAFolder(sourceFolder,fileType);
                  if(files!=null && files.length>0){
                	  List<File> loFiles=Arrays.asList(files);
                	  moveFiles(sourceFolder,targetFolder,fileType, loFiles);
                  }
             }
          }catch(Exception e){
        	  LOGGER.error("Error in moveFiles()"+e.toString());
        	  throw new GIException("Error in moveFiles()"+e.toString(),e);
          }
    }

    public void createContentForBadEnrollmentFile(StringBuilder badEnrollmentDetailsBuilder, Enrollment enrollment, String reason){
		if(badEnrollmentDetailsBuilder != null && enrollment != null){
			badEnrollmentDetailsBuilder.append("Reason for Failure: ");
			badEnrollmentDetailsBuilder.append(reason);
			badEnrollmentDetailsBuilder.append(", Exchange_Subscriber_Identifier=");
			badEnrollmentDetailsBuilder.append(enrollment.getExchgSubscriberIdentifier());
			badEnrollmentDetailsBuilder.append(",CMS_Plan_Id=");
			badEnrollmentDetailsBuilder.append(enrollment.getCMSPlanID());
			badEnrollmentDetailsBuilder.append("\n");
		}
	}
    
    private void populateInboundReconDtoForError(EnrollmentInboundReconciliationReport enrollmentInboundReconciliationReport, Enrollment enrollment, String errorMessage,
    		String carrierSentStatus, Enrollment enrollmentDb, Enrollee enrollee){
    	enrollmentInboundReconciliationReport.setEnrollmentId(enrollment.getId());
    	if(errorMessage!=null){
    	enrollmentInboundReconciliationReport.setErrorMessage( errorMessage.length()>EnrollmentConstants.ERROR_CODE_299 ? errorMessage.substring(0, EnrollmentConstants.ERROR_CODE_297):errorMessage);
    	}
    	if(StringUtils.isNotBlank(carrierSentStatus)){
    		if(carrierSentStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
    			enrollmentInboundReconciliationReport.setCareerSentStatus(EnrollmentConstants.INBOUND_SUBSCRIBER_CANCEL);
    		}
    		else if(carrierSentStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
    			enrollmentInboundReconciliationReport.setCareerSentStatus(EnrollmentConstants.INBOUND_SUBSCRIBER_TERM);
    		}
    		else{
    			enrollmentInboundReconciliationReport.setCareerSentStatus(carrierSentStatus);
    		}
    		
    	}
    	if(enrollmentDb != null){
    		enrollmentInboundReconciliationReport.setHiosIssuerId(enrollmentDb.getHiosIssuerId());
    		enrollmentInboundReconciliationReport.setIssuerName(enrollmentDb.getInsurerName());
    	}
    	if(enrollee!=null){
    		enrollmentInboundReconciliationReport.setCarrierSentStartDate(enrollee.getEffectiveStartDate());
        	enrollmentInboundReconciliationReport.setCarrierSentEndDate(enrollee.getEffectiveEndDate());
        	
        	if(enrollee.getEnrollmentEvents() != null && !enrollee.getEnrollmentEvents() .isEmpty()){
				for(EnrollmentEvent enrollmentEvent: enrollee.getEnrollmentEvents() ){
					if(enrollmentEvent.getEventReasonLkp() != null){
						enrollmentInboundReconciliationReport.setCarrierSentMrc(enrollmentEvent.getEventReasonLkp().getLookupValueCode());
						break;
					}
				}
			}
    	}
    	
    }
    
    
    private void logFailingRecordsData(String parFileName,StringBuilder badEnrollmentDetailsBuilder, String  failureFolderPath){
		String fileName = parFileName.replaceAll(EnrollmentConstants.FILE_TYPE_XML, "");
		File failureFile = new File(failureFolderPath + File.separator + fileName + "_failedRecords.txt");
		File failureFolder = new File(failureFolderPath);
		FileWriter writer = null;
		BufferedWriter bufferedWriter = null;
		try {
			if(!failureFolder.exists()){
				failureFolder.mkdirs();
			}
			if(!failureFile.exists()){
				failureFile.createNewFile();
			}
			writer = new FileWriter(failureFile);
			bufferedWriter = new BufferedWriter(writer,EnrollmentConstants.BUFFER_SIZE);
			bufferedWriter.write(badEnrollmentDetailsBuilder.toString());
			bufferedWriter.flush();
		} catch (IOException e) {
			LOGGER.error("Error Writing failed records file" , e);
		}finally{
			IOUtils.closeQuietly(writer);
			IOUtils.closeQuietly(bufferedWriter);
		}
	}
    
    private void setAssisterBrokerId(Enrollment enrollment) {
		AccountUser user;
		try {
			user = userService.getLoggedInUser();

			if (null != user && null == enrollment.getAssisterBrokerId()) {
				Role role = userService.getDefaultRole(user);
				if (null != role && role.getName().equalsIgnoreCase("CSR")) {
					enrollment.setAssisterBrokerId(user.getId());
				}
			}
		} catch (InvalidUserException e) {
			LOGGER.error("Could not set assister Broker Id" + e);
		}

	}
    
    private boolean validateInboundCanceltermRequest(Enrollment enrollmentDb, Enrollment enrollment, String maintReasonCode, Enrollee subscriber , StringBuilder badEnrollmentDetailsBuffer, EnrollmentInboundReconciliationReport enrollmentInboundReconciliationReport){
    	boolean containsBadEnrollments=false;
    	try{
    		String subscriberStatus=subscriber.getEnrolleeLkpValue().getLookupValueCode();
    		if(enrollmentDb.getEnrollmentTypeLkp().getLookupValueCode() == null || enrollmentDb.getEnrollmentTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
				containsBadEnrollments = true;
				createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, EnrollmentConstants.INVALID_ENROLLMENT_TYPE);
				populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_UNSUPPORTED_STATUS,
						subscriberStatus, enrollmentDb,	subscriber);
				return containsBadEnrollments;
			}
        	if(!EnrollmentConfiguration.isCaCall() && (maintReasonCode == null || !maintReasonCode.equalsIgnoreCase(EnrollmentConstants.EVENT_REASON_NON_PAYMENT))){
    			// Received Maintenance Reason Code Other than NON_PAYMENT for CANCEL and TERM txn.
    			containsBadEnrollments = true;
    			createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, EnrollmentConstants.POST_REJECT_MAINT_CODE + maintReasonCode);
    			populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_MAINT_REASON_CODE + maintReasonCode,
    					subscriberStatus, enrollmentDb, subscriber);
    		}
        	
        	if(subscriber.getEffectiveEndDate()!=null){

            	if(!EnrollmentUtils.validateSameYear(subscriber.getEffectiveEndDate(), enrollmentDb.getBenefitEffectiveDate())){
        			containsBadEnrollments = true;
        			createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, EnrollmentConstants.REJECT_INCORRECT_COVERAGE_YEAR);
        			populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_COVERAGE_END_YEAR,
        					subscriberStatus, enrollmentDb,	subscriber);
        			
        		}
            	
            	if(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(subscriberStatus) && !EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveEndDate()).equals(EnrollmentUtils.removeTimeFromDate(enrollmentDb.getBenefitEffectiveDate()))){
        			containsBadEnrollments = true;
        			createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, "Enrollment with invalid EffectiveEnd Date = "+subscriber.getEffectiveEndDate());
        			populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_END_DATE,
        					subscriberStatus, enrollmentDb,	subscriber);
        		}
            	if(!EnrollmentConfiguration.isCaCall() && EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(subscriberStatus)){
            		
            		if(enrollmentDb.getBenefitEndDate()!=null && EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveEndDate()).after(EnrollmentUtils.removeTimeFromDate(enrollmentDb.getBenefitEndDate()))){
    					//if received term date higher than existing end date
    					containsBadEnrollments = true;
    					createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment,EnrollmentConstants.TERM_TXN_REJECT_REASON);
    					populateInboundReconDtoForError(enrollmentInboundReconciliationReport,enrollment,EnrollmentConstants.MSG_REJECT_LOWER_END_DATE_PRESENT,
    							subscriberStatus, enrollmentDb,	subscriber);
    				}
            		
            		if(!EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveEndDate()).after(EnrollmentUtils.removeTimeFromDate(enrollmentDb.getBenefitEffectiveDate()))) {
            			
        					//If received term date before existing start date {CANCEL scenario}
        					containsBadEnrollments = true;
        					createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, EnrollmentConstants.TERM_TXN_REJECT);
        					populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_END_DATE,
        							subscriberStatus, enrollmentDb,	subscriber);
        					LOGGER.info(EnrollmentConstants.TERM_TXN_REJECT  +subscriber.getEffectiveEndDate());
        				
                	}
            	}
        	}else{
        		containsBadEnrollments = true;
    			createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_END_DATE);
    			populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_END_DATE,
    					subscriberStatus, enrollmentDb,	subscriber);
        	}
        	
        	
    	}catch(Exception exception){
    		LOGGER.error(exception.getMessage(), exception);
			containsBadEnrollments = true;
			createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, exception.getMessage());
			populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, exception.getMessage(),
					null, null,	null);
    	}
    	    	
    	return containsBadEnrollments;
    }
  
   private Enrollment findEnrollmentForInbound(Enrollment enrollment, Enrollee subscriber){
    	Enrollment enrollmentDb=null;
    	if(!EnrollmentConfiguration.isCaCall()){
    		enrollmentDb = enrollmentRepository.getEnrollmentForInbound(enrollment.getId());
    	}else{
    		String subscriberStatus="";
    		if(subscriber!=null && subscriber.getEnrolleeLkpValue()!=null){
    			subscriberStatus=subscriber.getEnrolleeLkpValue().getLookupValueCode();
    		}
    		List<String> statusList= new ArrayList<String>();
    		List<Enrollment> enrollmentActiveDbList= null;
    			
    		if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(subscriberStatus)){
    			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM);
				statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_TERM);
				
				if(enrollment.getId()!=null){
					enrollmentActiveDbList= enrollmentRepository.getEnrollmentByIdStatus(enrollment.getId(), statusList);
				}
				else if(isNotNullAndEmpty(enrollment.getHouseHoldCaseId())){
					enrollmentActiveDbList = enrollmentRepository.getEnrollmentByExchgSubscriberIdentifierForTerm(enrollment.getExchgSubscriberIdentifier(), enrollment.getCMSPlanID(),enrollment.getHouseHoldCaseId(),DateUtil.getYearStartDate(subscriber.getEffectiveEndDate(),StartEndYearEnum.START),DateUtil.getYearStartDate(subscriber.getEffectiveEndDate(),StartEndYearEnum.END));
				}else{
					enrollmentActiveDbList = enrollmentRepository.getEnrollmentByExchgSubscriberIdentifierForTerm(enrollment.getExchgSubscriberIdentifier(), enrollment.getCMSPlanID(),DateUtil.getYearStartDate(subscriber.getEffectiveEndDate(),StartEndYearEnum.START),DateUtil.getYearStartDate(subscriber.getEffectiveEndDate(),StartEndYearEnum.END));
				}
    		}else if(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(subscriberStatus)){
    			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_PENDING);
    			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM);
    			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_TERM);
    			
    			if(enrollment.getId()!=null){
					enrollmentActiveDbList= enrollmentRepository.getEnrollmentByIdStatus(enrollment.getId(), statusList);
				}
				else if(isNotNullAndEmpty(enrollment.getHouseHoldCaseId())){
					enrollmentActiveDbList = enrollmentRepository.getEnrollmentByExchgSubscriberIdentifierForCancel(enrollment.getExchgSubscriberIdentifier(), enrollment.getCMSPlanID(), enrollment.getHouseHoldCaseId(),subscriber.getEffectiveEndDate());
				}else{
					enrollmentActiveDbList = enrollmentRepository.getEnrollmentByExchgSubscriberIdentifierForCancel(enrollment.getExchgSubscriberIdentifier(), enrollment.getCMSPlanID(), subscriber.getEffectiveEndDate());
				}
    		}else if(enrollment.getEnrollees()!=null && enrollment.getEnrollees().size()>0){
    			statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_PENDING);
				statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM);
				statusList.add(EnrollmentConstants.ENROLLMENT_STATUS_TERM);
				
				if(enrollment.getId()!=null){
					enrollmentActiveDbList= enrollmentRepository.getEnrollmentByIdStatus(enrollment.getId(), statusList);
				}
				else if(isNotNullAndEmpty(enrollment.getHouseHoldCaseId())){
					enrollmentActiveDbList = enrollmentRepository.getEnrollmentByExchgSubscriberIdentifierForConfirm(enrollment.getExchgSubscriberIdentifier(), enrollment.getCMSPlanID(), enrollment.getHouseHoldCaseId(),DateUtil.getYearStartDate(enrollment.getEnrollees().get(0).getEffectiveStartDate(),StartEndYearEnum.START),DateUtil.getYearStartDate(enrollment.getEnrollees().get(0).getEffectiveStartDate(),StartEndYearEnum.END));
				}else{
					enrollmentActiveDbList = enrollmentRepository.getEnrollmentByExchgSubscriberIdentifierForConfirm(enrollment.getExchgSubscriberIdentifier(), enrollment.getCMSPlanID(),DateUtil.getYearStartDate(enrollment.getEnrollees().get(0).getEffectiveStartDate(),StartEndYearEnum.START),DateUtil.getYearStartDate(enrollment.getEnrollees().get(0).getEffectiveStartDate(),StartEndYearEnum.END));
				}

    		}
    		
    		if(enrollmentActiveDbList!=null && enrollmentActiveDbList.size()>0){
    			enrollmentDb= enrollmentActiveDbList.get(0);
    		}
    	}
    	
    	return enrollmentDb;
    }
    
    @Override
	@Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
	public boolean updateEnrollmentByXML2(Enrollment enrollment, String fileName, String timeStamp, Long jobExecutionId) throws GIException{
		boolean containsBadEnrollments = false;
		String carrierStatus=null;
		StringBuilder badEnrollmentDetailsBuffer = new StringBuilder();
		List<EnrollmentInboundReconciliationReport> enrollmentInboundReconciliationReportList = new ArrayList<EnrollmentInboundReconciliationReport>();
		EnrollmentInboundReconciliationReport enrollmentInboundReconciliationReport = new EnrollmentInboundReconciliationReport();
		String failureFolderPath=null;
		Enrollee loggingEnrollee=null;
		
		try{
			
			if(enrollment!=null && enrollment.getEnrollees()!=null &&  enrollment.getEnrollees().size()>0){
				loggingEnrollee=enrollment.getEnrollees().get(0);
			}
			boolean autoEffectuate = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EFFECTUATE_NEWLY_ADDED_MEMBERS));
			String inbound834XMLPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INBOUND834XMLPATH);
			if(inbound834XMLPath != null){
				failureFolderPath = inbound834XMLPath + File.separator + EnrollmentConstants.FAILURE_FOLDER_NAME +File.separator+ timeStamp;
			}
			enrollmentInboundReconciliationReport.setBatchExecutionId(jobExecutionId);
			enrollmentInboundReconciliationReport.setXmlFileName(fileName);
			
			AccountUser user = userRepository.findByUserName(GhixConstants.USER_NAME_CARRIER);//userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
			if(user==null){
				throw new Exception("Not able to fetch user with user name as :: "+GhixConstants.USER_NAME_CARRIER);
			}
			enrollment.updateEnrollees();
			Enrollee subscriber = enrollment.getSubscriberEnrollee();
			
			boolean updateEnrollmentFlag = false;
			String subscriberStatus=null;
			String maintReasonCode = null;
			if(subscriber != null && subscriber.getEnrolleeLkpValue() != null){
				loggingEnrollee=subscriber;
				subscriberStatus = subscriber.getEnrolleeLkpValue().getLookupValueCode();
				List<EnrollmentEvent> enrollmentEventList = subscriber.getEnrollmentEvents();
				if(enrollmentEventList != null && !enrollmentEventList.isEmpty()){
					for(EnrollmentEvent enrollmentEvent: enrollmentEventList){
						if(enrollmentEvent.getEventReasonLkp() != null){
							maintReasonCode = enrollmentEvent.getEventReasonLkp().getLookupValueCode();
							break;
						}
					}
				}
			}
			
			Enrollment enrollmentDb = null;
			List<Enrollee> enrolleeList = enrollment.getEnrollees();
			StringBuilder logStatement = new StringBuilder();
			logStatement.append("Enrollment_Id= "+enrollment.getId()+" ");
			logStatement.append(EnrollmentConstants.EXCHANGE_SUBSCRIBER_IDENTIFIER);
			logStatement.append(enrollment.getExchgSubscriberIdentifier());
			logStatement.append(" and CMS Plan ID = ");
			logStatement.append(enrollment.getCMSPlanID());
			logStatement.append(" and HouseHoldCase Id = ");
			logStatement.append(enrollment.getHouseHoldCaseId());
			
			enrollmentDb= findEnrollmentForInbound(enrollment, subscriber);
			enrollmentInboundReconciliationReport.setCarrierSentMrc(maintReasonCode);
			if(enrollmentDb == null){
				containsBadEnrollments = true;
				createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, EnrollmentConstants.NO_ACTIVE_ENROLLMENT_FOUND);
				populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_NO_ACTIVE_ENROLLMENT,
						subscriberStatus,null, loggingEnrollee);
				LOGGER.info("No Unique Enrollment found with " + logStatement.toString());
			}
			if(!containsBadEnrollments){
				String enrollmentType = enrollmentDb.getEnrollmentTypeLkp().getLookupValueCode();

				if(subscriberStatus != null && (subscriberStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)
						|| subscriberStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
					
					containsBadEnrollments=validateInboundCanceltermRequest(enrollmentDb, enrollment, maintReasonCode, subscriber, badEnrollmentDetailsBuffer, enrollmentInboundReconciliationReport);
					if(!containsBadEnrollments){
						
						if(subscriberStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){

							//CANCEL Carrier Event
							carrierStatus = EnrollmentConstants.ENROLLMENT_STATUS_CANCEL;
							//if(EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveEndDate()).equals(EnrollmentUtils.removeTimeFromDate(enrollmentDb.getBenefitEffectiveDate()))){
								enrollmentDb.setFileName834(fileName);
								enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollmentDb, EnrollmentConstants.RECEIVED_834, user);		
								List<Enrollee> cancelEnrolleeList = getPendingConfirmTermEnrollees(enrollmentDb.getEnrollees());
								enrollmentDb.setIssuerSubscriberIdentifier(enrollment.getIssuerSubscriberIdentifier());
								enrollmentDb.setBenefitEndDate(enrollmentDb.getBenefitEffectiveDate());
								if(cancelEnrolleeList != null && !cancelEnrolleeList.isEmpty()){
									for (Enrollee enrolleeDb : cancelEnrolleeList) {

										enrolleeDb.setEffectiveEndDate(enrolleeDb.getEffectiveStartDate());
										enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
										enrolleeDb.setDisenrollTimestamp(new TSDate());

										if(enrolleeDb.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) &&subscriber.getIssuerIndivIdentifier()!=null){
											enrolleeDb.setIssuerIndivIdentifier(subscriber.getIssuerIndivIdentifier());
										}
										enrolleeDb.setUpdatedBy(user);
										enrollmentService.createEnrolleeEvent(enrolleeDb, EnrollmentConstants.EVENT_TYPE_CANCELLATION, EnrollmentConstants.EVENT_REASON_NON_PAYMENT, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.CARRIER_INBOUND_DISENROLL);
										updateEnrollmentFlag = true;

										enrollmentInboundReconciliationReport = prepareEnrollmentInboundReconciliationReport(jobExecutionId, fileName,
												EnrollmentConstants.INBOUND_SUBSCRIBER_CANCEL, enrollmentDb, enrolleeDb, loggingEnrollee, enrollment);
										enrollmentInboundReconciliationReportList.add(enrollmentInboundReconciliationReport);
									}
								}else{
									containsBadEnrollments = true;
									createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, "No Enrollees found for CANCEL request enrollment "+enrollmentDb.getId());
									populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_NO_ACTIVE_ENROLLEE,
											subscriberStatus, enrollmentDb,	loggingEnrollee);
									LOGGER.info("CANCEL enrollment request no enrollees found:  "+enrollmentDb.getId());
								}
						}else if(subscriberStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
							//TERM CASE
							//******* CASE STARTS HERE 
							carrierStatus=EnrollmentConstants.ENROLLMENT_STATUS_TERM;
							enrollmentDb.setFileName834(fileName);
							enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollmentDb, EnrollmentConstants.RECEIVED_834, user);
							List<Enrollee> termEnrolleeList = getPendingConfirmTermEnrollees(enrollmentDb.getEnrollees());
							enrollmentDb.setBenefitEndDate(EnrollmentUtils.getEODDate(subscriber.getEffectiveEndDate()));
							for(Enrollee enrolleeDb : termEnrolleeList) 
							{
								if(enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) 
										&& EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveEndDate()).after(enrolleeDb.getEffectiveEndDate())){
									if(!enrolleeDb.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
										continue;
									}
									else{
									containsBadEnrollments = true;
									createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment,EnrollmentConstants.TERM_TXN_REJECT_REASON +" For member :"+enrolleeDb.getExchgIndivIdentifier());
									populateInboundReconDtoForError(enrollmentInboundReconciliationReport,enrollment,EnrollmentConstants.MSG_REJECT_LOWER_END_DATE_PRESENT,
											subscriberStatus, enrollmentDb,	loggingEnrollee);
									LOGGER.info("Member level Short Term TXN Date already present" + logStatement.toString()+" Member = "+enrolleeDb.getExchgIndivIdentifier());
									break;
								}
								}
								else{
									enrolleeDb.setLastPremiumPaidDate(subscriber.getLastPremiumPaidDate());
									if(EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveEndDate()).after(EnrollmentUtils.removeTimeFromDate(enrolleeDb.getEffectiveStartDate()))){
										enrolleeDb.setEffectiveEndDate(EnrollmentUtils.getEODDate(subscriber.getEffectiveEndDate()));
										if(enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)){
											enrolleeDb.setIssuerIndivIdentifier(subscriber.getIssuerIndivIdentifier());
											enrollmentDb.setIssuerSubscriberIdentifier(enrollment.getIssuerSubscriberIdentifier());
											if(enrollmentDb.getEnrollmentConfirmationDate() == null){
												enrollmentDb.setEnrollmentConfirmationDate(new TSDate());
											}
										}
										enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_TERM));
									}
									else if (EnrollmentUtils.removeTimeFromDate(enrolleeDb.getEffectiveStartDate())
											.after(EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveEndDate()))
											|| EnrollmentUtils.removeTimeFromDate(enrolleeDb.getEffectiveStartDate())
													.equals(EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveEndDate()))) {
										//Only CANCELS the member based on Date received in request 
										enrolleeDb.setEffectiveEndDate(enrolleeDb.getEffectiveStartDate());
										enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CANCEL));
										enrolleeDb.setDisenrollTimestamp(new TSDate());
									}
									if(null != enrollment.getPremiumPaidToDateEnd()) {
										enrollmentDb.setPremiumPaidToDateEnd(enrollment.getPremiumPaidToDateEnd());
									}else if(null != subscriber.getEffectiveEndDate() && null != enrollmentDb.getAptcAmt()) {
										enrollmentDb.setPremiumPaidToDateEnd(
												EnrollmentUtils.removeTimeFromDate(EnrollmentUtils.getBeforeDayDate(
														DateUtil.getMonthStartDate(subscriber.getEffectiveEndDate()))));
									}else if(null != subscriber.getEffectiveEndDate()) {
										enrollmentDb.setPremiumPaidToDateEnd(subscriber.getEffectiveEndDate());
									}
									enrolleeDb.setUpdatedBy(user);
									setAssisterBrokerId(enrollmentDb);
									enrollmentService.createEnrolleeEvent(enrolleeDb, EnrollmentConstants.EVENT_TYPE_CANCELLATION, EnrollmentConstants.EVENT_REASON_NON_PAYMENT, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.CARRIER_INBOUND_DISENROLL);
									updateEnrollmentFlag=true;
									enrollmentInboundReconciliationReport = prepareEnrollmentInboundReconciliationReport(jobExecutionId,
										fileName, EnrollmentConstants.INBOUND_SUBSCRIBER_TERM, enrollmentDb, enrolleeDb, loggingEnrollee, enrollment);
										enrollmentInboundReconciliationReportList.add(enrollmentInboundReconciliationReport);
								}
								//******* CASE ENDS HERE
							} 
						}
					}
				}else if (enrolleeList != null && !enrolleeList.isEmpty()){

					
					//CONFIRM CASE
					carrierStatus=EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM;
					enrollmentDb.setFileName834(fileName);
					enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollmentDb, EnrollmentConstants.RECEIVED_834, user);
					Enrollee subElee = enrolleeRepository.findSubscriberByEnrollmentID(enrollmentDb.getId());
					Date subsciberStartDate =null;
					if(subscriber!=null && subscriber.getEffectiveStartDate()!=null){
						subsciberStartDate = subscriber.getEffectiveStartDate();
					}else{
						subsciberStartDate = subElee.getEffectiveStartDate();
					}

					if(subscriber!=null && subscriber.getEmployerGroupNo()!=null){
						enrollmentDb.setGroupPolicyNumber(subscriber.getEmployerGroupNo());
					}

					/**
					 * Subscriber level effectuation for active enrollees missing in inbound xml\
					 */
					if(autoEffectuate && subscriber != null && subElee != null){
						
						if(subscriber.getEffectiveStartDate()==null){
							containsBadEnrollments = true;
							createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment,"Subscriber with wrong EffectiveStartDate = "+subscriber.getEffectiveStartDate());
							populateInboundReconDtoForError(enrollmentInboundReconciliationReport,enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_START_DATE,
									subscriberStatus, enrollmentDb,	loggingEnrollee);
							LOGGER.info("Subscriber with wrong EffectiveStartDate = "+subscriber.getEffectiveStartDate() );
						}else if(EnrollmentUtils.removeTimeFromDate(subscriber.getEffectiveStartDate()).
								equals(EnrollmentUtils.removeTimeFromDate(subElee.getEffectiveStartDate()))){
							List<Enrollee> filteredEnrollees = getFilteredEnrollees(enrolleeList, enrollmentDb.getEnrollees());
							if(filteredEnrollees != null && !filteredEnrollees.isEmpty()){
								for (Enrollee dbEnrollee : filteredEnrollees) 
								{
									if(dbEnrollee.getEnrolleeLkpValue() != null 
											&& (!dbEnrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) 
													&& !dbEnrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))){
										dbEnrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
									}
									dbEnrollee.setUpdatedBy(user);
									enrollmentService.createEnrolleeEvent(dbEnrollee, EnrollmentConstants.EVENT_TYPE_ADDITION, EnrollmentConstants.EVENT_REASON_INITIAL_ENROLLMENT, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.CARRIER_INBOUND_CONFIRM);

									enrollmentInboundReconciliationReport = prepareEnrollmentInboundReconciliationReport(jobExecutionId, fileName,
											EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM, enrollmentDb, dbEnrollee, loggingEnrollee, enrollment);
									enrollmentInboundReconciliationReportList.add(enrollmentInboundReconciliationReport);
								}
							}
						}
						else{
							containsBadEnrollments = true;
							createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, "Existing Subscriber DB start date is not matching with received start date : "+subscriber.getEffectiveStartDate());
							populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_START_DATE,
							subscriberStatus, enrollmentDb,	loggingEnrollee);		
						}
					}
					//else{ //else reason commented out as we need to confirm matching enrollees also
					if(!containsBadEnrollments){
						for (Enrollee enrollee : enrolleeList) {
							loggingEnrollee=enrollee;
							
							String enrolleeStatus = enrollee.getEnrolleeLkpValue().getLookupValueCode();
							String existingEnrolleeStatus=null;
							Enrollee enrolleeDb = getActiveEnrolleeForConfirmation(enrollmentDb.getEnrollees(), enrollee.getExchgIndivIdentifier());
							if(enrollee.getEffectiveStartDate()==null){
								containsBadEnrollments = true;
								createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment,"Enrollment with wrong EffectiveStartDate = "+enrollee.getEffectiveStartDate());
								populateInboundReconDtoForError(enrollmentInboundReconciliationReport,enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_START_DATE,
										subscriberStatus, enrollmentDb,	loggingEnrollee);
								LOGGER.info("Enrollment with wrong EffectiveStartDate = "+enrollee.getEffectiveStartDate() );
								break;
								
							}else if(enrolleeDb == null){
								containsBadEnrollments = true;
								createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment,EnrollmentConstants.ERR_MSG_NO_ENROLLEE + " :- " + enrollee.getExchgIndivIdentifier());
								populateInboundReconDtoForError(enrollmentInboundReconciliationReport,enrollment,EnrollmentConstants.MSG_REJECT_NO_ACTIVE_ENROLLEE,
										subscriberStatus, enrollmentDb,	loggingEnrollee);
								LOGGER.info(EnrollmentConstants.ERR_MSG_NO_ENROLLEE + " :- " + enrollee.getExchgIndivIdentifier());
								break;
							}else if(isNotNullAndEmpty(enrolleeStatus) &&  enrolleeStatus.equals(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)){
								
								if(enrolleeDb.getEnrolleeLkpValue()!=null){
									existingEnrolleeStatus=enrolleeDb.getEnrolleeLkpValue().getLookupValueCode();
								}
								
								if(existingEnrolleeStatus!=null &&  existingEnrolleeStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
									containsBadEnrollments = true;
									createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment,
											"Enrollee with Exchange Individual Identifier = "
													+ enrollee.getExchgIndivIdentifier() + " and enrollment ID = "
													+ enrollmentDb.getId() + " is in CANCEL status");
									populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment,
											EnrollmentConstants.MSG_REJECT_NO_ACTIVE_ENROLLEE, subscriberStatus, enrollmentDb, loggingEnrollee);
									break;
								}
								else{
									if(existingEnrolleeStatus!=null && !existingEnrolleeStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)){
										if(!DateUtil.equateDateYear(enrollee.getEffectiveStartDate(),enrolleeDb.getEffectiveStartDate())){
											containsBadEnrollments = true;
											createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment,"Enrollment with wrong EffectiveStartDate Year = "+enrollee.getEffectiveStartDate() +EnrollmentConstants.DATE_YEAR_DID_NOT_MATCH_WITH+ enrolleeDb.getEffectiveStartDate());
											populateInboundReconDtoForError(enrollmentInboundReconciliationReport,enrollment, EnrollmentConstants.MSG_REJECT_INCORRECT_COVERAGE_START_YEAR,
													subscriberStatus, enrollmentDb,	loggingEnrollee);
											LOGGER.info("Enrollment with wrong EffectiveStartDate Year = "+enrollee.getEffectiveStartDate() +EnrollmentConstants.DATE_YEAR_DID_NOT_MATCH_WITH+ enrolleeDb.getEffectiveStartDate());
											break;
										}

										enrolleeDb.setIssuerIndivIdentifier(enrollee.getIssuerIndivIdentifier());
										enrollmentDb.setIssuerSubscriberIdentifier(enrollment.getIssuerSubscriberIdentifier());
										enrolleeDb.setLastPremiumPaidDate(enrollee.getLastPremiumPaidDate());
										
										if(enrollmentType!=null && !enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
											String updateEffectuateDateFlag =DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.UPDATE_INBOUND_EFFECTUATION_DATE);
											if(getBooleanFromYesOrYFlag(updateEffectuateDateFlag)){
												if(EnrollmentUtils.removeTimeFromDate(enrollee.getEffectiveStartDate()).before(subsciberStartDate)){
													enrolleeDb.setEffectiveStartDate(subsciberStartDate);
												}else{
													enrolleeDb.setEffectiveStartDate(enrollee.getEffectiveStartDate());
												}
											}
										}
										if(enrolleeDb.getEnrolleeLkpValue() != null && (!enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) && !enrolleeDb.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))){
											enrolleeDb.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
										}
										//HIX-110973 Accepting Confirmation Date on a Term Enrollment
										if (enrollmentDb.getEnrollmentConfirmationDate() == null
												&& enrollmentDb.getEnrollmentStatusLkp() != null
												&& enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)) {
											enrollmentDb.setEnrollmentConfirmationDate(new TSDate());
										}
										enrolleeDb.setHealthCoveragePolicyNo(enrollee.getHealthCoveragePolicyNo());
										enrolleeDb.setUpdatedBy(user);
										enrollmentService.createEnrolleeEvent(enrolleeDb, EnrollmentConstants.EVENT_TYPE_ADDITION, EnrollmentConstants.EVENT_REASON_INITIAL_ENROLLMENT, user, false, EnrollmentEvent.TRANSACTION_IDENTIFIER.CARRIER_INBOUND_CONFIRM);
										updateEnrollmentFlag=true;

										enrollmentInboundReconciliationReport = prepareEnrollmentInboundReconciliationReport(jobExecutionId,
										fileName, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM, enrollmentDb, enrolleeDb, loggingEnrollee, enrollment);
										enrollmentInboundReconciliationReportList.add(enrollmentInboundReconciliationReport);

									}else{
										containsBadEnrollments = true;
										createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment,"Enrollee with Exchange Individual Identifier = " + enrollee.getExchgIndivIdentifier() + " and enrollment ID = " + enrollmentDb.getId() + " is already Confirmed");
										populateInboundReconDtoForError(enrollmentInboundReconciliationReport,enrollment,EnrollmentConstants.MSG_REJECT_ENROLLEE_CONFIRM_PRESENT,
												subscriberStatus, enrollmentDb,	enrollee);
										LOGGER.info("This Enrollee with Exchange Individual Identifier = "+enrollee.getExchgIndivIdentifier()+"and enrollment ID ="+enrollmentDb.getId()+" is already Confirmed");
										break;
									}
								}
							}else{
								containsBadEnrollments = true;
								createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer,enrollment,"Invalid status received for Enrollee with Exchange Individual Identifier = "+enrollee.getExchgIndivIdentifier()+"and enrollment ID ="+enrollmentDb.getId() +". Status Received in inbound is :"+enrolleeStatus);
								populateInboundReconDtoForError(enrollmentInboundReconciliationReport,enrollment, EnrollmentConstants.MSG_REJECT_UNSUPPORTED_STATUS,
										subscriberStatus, enrollmentDb,	enrollee);
								LOGGER.info("Invalid status received for Enrollee with Exchange Individual Identifier = "+enrollee.getExchgIndivIdentifier()+"and enrollment ID ="+enrollmentDb.getId() +". Status Received in inbound is :"+enrolleeStatus);
								break;
							}
						}//End of Enrollee for loop
					}
					//}
				}//End of ONFIRM Block
			}
			
			// check Enrollment Status
			if (updateEnrollmentFlag && !containsBadEnrollments) {
				List<Enrollment> enrollmentList= new ArrayList<Enrollment>();
				enrollmentList.add(enrollmentDb);
				enrollmentService.checkEnrollmentStatus(user,enrollmentDb);
				
				boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
				
				
				if(isNotNullAndEmpty(carrierStatus) && (carrierStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)
						|| carrierStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
					if(populateMonthlyPremium ){
						enrollmentRequotingService.updateMonthlyPremiumForCancelTerm( enrollmentDb, null);
					}
					if(EnrollmentConfiguration.isIdCall() && EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollmentDb.getInsuranceTypeLkp().getLookupValueCode())){
						List<Enrollment> dentalEnrollments=enrollmentService.updateDentalAPTCOnHealthDisEnrollment(enrollmentDb, user, EnrollmentEvent.TRANSACTION_IDENTIFIER.APTC_CH_HLT_CARRIER_DISENROLL);
						if(dentalEnrollments!=null && dentalEnrollments.size()>0){
							enrollmentList.addAll(dentalEnrollments);
						}
					}
					
				}//else condition is not handled as Linu said that effective date change is not be allowed in confirm scenario
				

				//enrollmentDb = enrollmentRepository.saveAndFlush(enrollmentDb);
				//enrollmentList = enrollmentRepository.save(enrollmentList);
				
				enrollmentList = enrollmentService.saveAllEnrollment(enrollmentList);
				
				Integer enId = enrollmentDb.getId();
				
				enrollmentDb= enrollmentList.stream().filter(eObj->eObj.getId()==enId).findFirst().get();
				
				/**
				 * Logging CARRIER updates in application_event_LOG
				 */
				if (carrierStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) {
					enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollmentDb, EnrollmentConstants.EnrollmentAppEvent.INBOUND_834_CONFIRM_EVT_PROCESSED.toString(), user);
				} else if (carrierStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
					enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollmentDb, EnrollmentConstants.EnrollmentAppEvent.INBOUND_834_CANCEL_EVT_PROCESSED.toString(), user);
				}else if(carrierStatus.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)){
					enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollmentDb, EnrollmentConstants.EnrollmentAppEvent.INBOUND_834_TERM_EVT_PROCESSED.toString(), user);
				}
				
				for (EnrollmentInboundReconciliationReport tempEnrollmentInboundReconciliationReport : enrollmentInboundReconciliationReportList) {
					if (isNotNullAndEmpty(tempEnrollmentInboundReconciliationReport.getMemberId())) {
						for(Enrollment enr : enrollmentList) {
							if(enrollmentDb.getId() == enr.getId()) {
								enrollmentDb= enr;
								break;
							}
						}
						Enrollee enrollee = enrollmentDb
								.getEnrolleeByMemberId(tempEnrollmentInboundReconciliationReport.getMemberId());
						if (enrollee != null && enrollee.getLastEventId() != null) {
							tempEnrollmentInboundReconciliationReport.setEvent(enrollee.getLastEventId());
						}
					}
				}

				Boolean isCaCall = EnrollmentConstants.STATE_CODE_CA.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode());
				
				if(isCaCall) {
					//Calling EnrollmentAhbxSyncService to save enrollment update Data replacement of IND21
					LOGGER.info("Async Call to enrollmentAhbxSyncService saveEnrollmentAhbxSync IND21_UPDATE");
					enrollmentAhbxSyncService.saveEnrollmentAhbxSync(updateEnrlList(enrollmentList), enrollmentDb.getHouseHoldCaseId(), EnrollmentAhbxSync.RecordType.IND21_UPDATE);
				}

			} else {
				enrollmentInboundReconciliationReportList.clear();
				enrollmentInboundReconciliationReportList.add(enrollmentInboundReconciliationReport);
				enrollmentAppEventLogUtil.recordApplicationEnrollmentEvent(enrollmentDb,
						EnrollmentConstants.EnrollmentAppEvent.INBOUND_834_DATA_REJECTED.toString(), user);
			}
			
		} catch (Exception exception) {
			LOGGER.error(exception.getMessage(), exception);
			containsBadEnrollments = true;
			createContentForBadEnrollmentFile(badEnrollmentDetailsBuffer, enrollment, exception.getMessage()+EnrollmentUtils.shortenedStackTrace(exception, 3));
			populateInboundReconDtoForError(enrollmentInboundReconciliationReport, enrollment, EnrollmentUtils.shortStackTrace(exception, EnrollmentConstants.TWO_NINETY_NINE),
					null, null,	null);
			enrollmentInboundReconciliationReportList.add(enrollmentInboundReconciliationReport);
		}
		enrollmentInboundReconciliationReportRepository.save(enrollmentInboundReconciliationReportList);

		if (containsBadEnrollments) {
			// Log failing enrollment data to file.
			File targetFolder = new File(failureFolderPath);
			if (!(targetFolder.exists())) {
				targetFolder.mkdirs();
			}
			logFailingRecordsData(fileName, badEnrollmentDetailsBuffer, failureFolderPath);
		}
		return !containsBadEnrollments;			
	}
	
	/**
	 * @author parhi_s
	 * @since
	 * This method is used to get all the files of a specific type from a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
	@Override
	public List<String> getFileNamesInAFolder(String folderName, String fileType) throws GIException{
		try{
		final String fileExtn=fileType;
		File folder= new File(folderName);
		List<String> fileNames= null;
		File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn)){
					return true;
				}
				return false;
			}
		});
		if(listOFFiles!=null){
			fileNames= new ArrayList<String>();
			for (File listOFFile : listOFFiles) {
				fileNames.add(listOFFile.getAbsolutePath());
			}
		}
		return fileNames;
		}catch(Exception e){
			throw new GIException(ERROR_IN_GETFILESINFOLDER+e.getMessage(),e);
		}
		
	}

	/**
	 * @author parhi_s
	 * @since
	 * This method is used to get all the files of a specific type from a folder
	 * @param folderName
	 * @param fileType
	 * @return
	 * @throws GIException
	 */
	private File[] getFilesInAFolder(String folderName, String fileType) throws GIException{
		try{
		final String fileExtn=fileType;
		File folder= new File(folderName);
		File[] listOFFiles= folder.listFiles( new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn)){
					return true;
				}
				return false;
			}
		});
		return listOFFiles;
		}catch(Exception e){
			LOGGER.error(ERROR_IN_GETFILESINFOLDER+e.getMessage(),e);
			throw new GIException(ERROR_IN_GETFILESINFOLDER+e.getMessage(),e);
		}
		
	}
	
	
	
	/**
	 * @author ajinkya_m
	 * @param enrolleeList
	 * @return true if List has only one Enrollee and of type SUBSCRIBER else false
	 *//*
	private Enrollee checkForSubscriber(List<Enrollee> enrolleeList) {
		if(enrolleeList !=null && !enrolleeList.isEmpty()){
			for(Enrollee enrollee:enrolleeList){
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
					return enrollee;
				}
			}
		}
		return null;	
	}
	*/
	@Override
	public void updateShopEnrollemntToCancelled(int employerId){
		LOGGER.info(" updateShopEnrollemntToCancelled Service START");
		int enrollmentTypeShopId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE,EnrollmentConstants.ENROLLMENT_TYPE_SHOP);
		int enrollmentStatusPendingId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_PENDING);
		Date currentDate = new TSDate();
		List<Enrollment> enrollmentList = enrollmentRepository.getPendingShopEnrollment(employerId,currentDate,enrollmentTypeShopId,enrollmentStatusPendingId);
		
		if(enrollmentList != null && !enrollmentList.isEmpty())
			{
				// Getting user
				AccountUser user = enrollmentService.getUserByName(GhixConstants.USER_NAME_EXCHANGE);
			
				//EnrollmentStatus = Cancelled = CANCEL
				int enrollmentStatusCancelledId = lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,"CANCEL");
				LookupValue cancelledLookup = new LookupValue();
				cancelledLookup.setLookupValueId(enrollmentStatusCancelledId);
				
				// Set enrollment status to Cancelled
				for (Enrollment enrollment : enrollmentList) {
					List<Enrollee> enrolleeList = null;
					try {
						enrolleeList = enrolleeService.getEnrolleeByEnrollmentID(enrollment.getId());
					} catch (GIException e) {
						LOGGER.error("Error occured in getEnrolleeByEnrollmentID.", e);
					}
					if(enrolleeList != null){
					for (Enrollee enrollee : enrolleeList) {
						
						enrollee.setEnrolleeLkpValue(cancelledLookup);
						enrollee.setDisenrollTimestamp(new TSDate());
						
						//	Save enrollee
						enrolleeRepository.saveAndFlush(enrollee);
						
						//	Create Event
						enrollmentService.createEventForEnrollee(enrollee,EnrollmentConstants.EVENT_TYPE_CANCELLATION,EnrollmentConstants.EVENT_REASON_NON_PAYMENT, user, false);
					 }
					}
					// Check if all the enrollee of given enrollment are cancel/terminated
					enrollmentService.checkEnrollmentStatus(enrollment ,user);
					LOGGER.info(" updateShopEnrollemntToCancelled Service END");
				}
			
			}
		}
	
		@Override
	@Transactional
	public void updateEnrolleeUpdateSendStatusForFailure(Map<Integer, String> errorMap){
		if(errorMap != null && !errorMap.isEmpty()){
			try{
				for(Integer enrolleeId : errorMap.keySet()){
					EnrolleeUpdateSendStatus enrolleeUpdateSendStatusFail = new EnrolleeUpdateSendStatus();
					Enrollee en = new Enrollee();
					en.setId(enrolleeId);
					enrolleeUpdateSendStatusFail.setEnrollee(en);
					enrolleeUpdateSendStatusFail.setAhbxStatusCode("ERR01");
					enrolleeUpdateSendStatusFail.setAhbxStatusDesc("Unable to send update to AHBX : "+errorMap.get(enrolleeId));
					enrolleeUpdateSendStatusRepository.save(enrolleeUpdateSendStatusFail);
				}		
			}
			catch(Exception ex){
				LOGGER.error("Failed to update enrolleeUpdateSendStatus Exception: ", ex);
			}
		}
	}

	@Override
	public void clearCarrierSendFlag(String carrierFlagStatus, String startDateString, String endDateString){
		Date startDate = null;
		Date endDate = null;
		
		if(startDateString == null && endDateString == null){
			startDate = new TSDate(0);
			endDate = new TSDate();
		}
		else if(startDateString != null && endDateString != null){
			startDate = DateUtil.StringToDate(startDateString, EnrollmentConstants.BATCH_DATE_FORMAT);
			endDate = DateUtil.StringToDate(endDateString, EnrollmentConstants.BATCH_DATE_FORMAT);
		}
		else if(startDateString != null && endDateString == null){
			startDate = DateUtil.StringToDate(startDateString, EnrollmentConstants.BATCH_DATE_FORMAT);
			endDate = new TSDate();
		}
		else if(startDateString == null && endDateString != null){
			startDate = new TSDate(0);
			endDate = DateUtil.StringToDate(endDateString, EnrollmentConstants.BATCH_DATE_FORMAT);
		}
		
		enrollmentRepository.clearCarrierSendFlag(carrierFlagStatus, startDate, endDate);
	}


	
		 private String getDateTime() {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
				return df.format(new TSDate());
		 }
	 public static boolean deleteDir(String path)
		{
			java.io.File dir = new java.io.File(path);
			if (dir.isDirectory())
			{
				String[] filesList = dir.list();
				for(String s : filesList)
				{
					boolean success = new java.io.File(dir, s).delete();
					if(!success)
					{
						return false;
					}
				}
			}
			return dir.delete();
		}
		public static void deleteDirectory(File directory) {
	        if (directory.exists()) {
	               File[] files = directory.listFiles();
	               if (null != files) {
	                     for (File file : files) {
	                               if (file.isDirectory()) {
	                                   deleteDirectory(file);
	                            } else {
	                                   file.delete();
	                            }
	                     }
	               }
	        }
	 }
	 public static int countFilesInFilePayloadFolder(File directory) {
		    int count = 0;
		    for(File file : directory.listFiles()) {
		    	 if(!file.isDirectory()) {
		    		 count++;
		    	 }
		    	
		       
		    }
		    return count;
		}	 
	 public List<Attachment> getAllAttachments(File directory) {
			List<Attachment> listAttachment = new ArrayList<Attachment>();
			int count = 0;
			if(directory.isDirectory()){
				File[] files = directory.listFiles();
				Arrays.sort(files, new Comparator<File>(){
				    @Override
					public int compare(File f1, File f2)
				    {
				    	return f1.getName().compareToIgnoreCase(f2.getName());
//				        return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
			    } });
				
				for(File file : files) {
					count = count +1;
					if(!file.isDirectory()) {
						Attachment attachment = new Attachment();
						attachment.setBinarySizeValue(file.length());
						attachment.setDocumentFileName(file.getName());
						String fileName = file.getName();
						String parts[] = fileName.split("\\_");
					     String seqId = parts[EnrollmentConstants.TWO];
						attachment.setDocumentSequenceID(seqId);
						// get the MD5 checksum number
						try(FileInputStream fis = new FileInputStream(file);){
							String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
//							String md5 = org.apache.commons.codec.digest.DigestUtils.sha384Hex(fis);
							attachment.setMD5ChecksumText(md5);
						}catch(Exception ex){
							LOGGER.error(ex.getMessage(),ex);
						}
						listAttachment.add(attachment);
					}
				}
			}
			return listAttachment;
		}
	 
	 public void insertBatchIDIntoIrsOutboundTrans(File directory, String batchID) {
			for(File file : directory.listFiles()) {
				if(!file.isDirectory()) {
					String fileName = file.getName();
					String fileSize = String.valueOf(file.length());
					if (! fileName.equalsIgnoreCase("manifest.xml")){
						IRSOutboundTransmission irsOutBoundTransmission = irsOutboundTransmissionRepo.findIRSOutboundTransmissionByFileName(fileName);
						if(irsOutBoundTransmission != null)
						{
							String houseHoldCaseIds = irsOutBoundTransmission.getHouseholdCaseID();
							String parts[] = houseHoldCaseIds.split("\\,");
							Integer numberOfHousehold = parts.length;
							irsOutBoundTransmission.setBatchId(batchID);
							irsOutBoundTransmission.setDocumentFileSize(fileSize);
							irsOutBoundTransmission.setTotalHouseholdPerXML(numberOfHousehold);
							
						}
					 	try{
							irsOutboundTransmissionRepo.save(irsOutBoundTransmission);
						}catch(Exception e){
							LOGGER.error("Error saving data in IRSOutBoundTransmission table @ logIRSOutBoundTransmissionInformation" , e);
						}
					}
				}
			}

		} 	
	 	
	private List<Enrollee> filterEnrolleesFor834ReconCancelTerm(List<Enrollee> allEnrollees, Date cutOffDate) throws GIException{
		List<Enrollee> filteredEnrolles=null;
		try{
			if(allEnrollees!=null && !(allEnrollees.isEmpty()) && cutOffDate!=null){
				filteredEnrolles=new ArrayList<Enrollee>();
				for(Enrollee enrollee:allEnrollees){
					if(enrollee.getPersonTypeLkp()!=null&&enrollee.getEnrolleeLkpValue()!=null){
						String personType=enrollee.getPersonTypeLkp().getLookupValueCode();
						String status=enrollee.getEnrolleeLkpValue().getLookupValueCode();
						if(personType!=null && (personType.equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)||personType.equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE))){
							if(status!=null && (status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)|| (status.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)&&enrollee.getEffectiveEndDate().after(cutOffDate)))){
								filteredEnrolles.add(enrollee);
							}
						}
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("filterEnrolleesFor834ReconCancelTerm()::"+e.getMessage(),e);
			//	e.printStackTrace();
			throw new GIException("Error in filterEnrolleesFor834ReconCancelTerm()::"+e.getMessage(),e);
		}
		
		return filteredEnrolles;
	}
	
	private String getCmsfileName(String iSA15){
		final SimpleDateFormat cmsDateFormat = new SimpleDateFormat(GhixConstants.GROUP_ENRL_DATE_FORMAT);
		final SimpleDateFormat cmsTimeFormat = new SimpleDateFormat(GhixConstants.CMS_ENRL_TIME_FORMAT);
		
		Date fileDate = new java.util.Date();
		Calendar calendar = TSCalendar.getInstance();
		calendar.setTime(fileDate);
		//int oneTenthOfSecond = calendar.get(Calendar.SECOND) /EnrollmentConstants.TEN ;
		
		String tradingPartnerID = DynamicPropertiesUtil.getPropertyValue("enrollment.TradingPartnerID");
		
		return File.separator +tradingPartnerID+"."+EnrollmentConstants.APP+"."+EnrollmentConstants.CMS_FuncCode+".D"+ cmsDateFormat.format(fileDate)+".T"+cmsTimeFormat.format(fileDate)+"."+iSA15+"."+EnrollmentConstants.TRANSFER_DIRECTION_IN+".xml";
	}
	
	
	/**
	 * @author parhi_s
	 * 
	 * Returns Returns all Active Enrollees for Enrollment.
	 * 
	 * @return
	 */
	private Enrollee getActiveEnrolleeForConfirmation(List<Enrollee> enrollees, String memberId ){
		Enrollee activeEnrollee = null;
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if(enrollee!=null && enrollee.getExchgIndivIdentifier()!=null && enrollee.getExchgIndivIdentifier().equalsIgnoreCase(memberId) ){
					if(enrollee.getPersonTypeLkp()!=null && ((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))
							&& enrollee.getEnrolleeLkpValue()!=null
							//&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))
//							&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
							&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED))){
						activeEnrollee=enrollee;
						break;
					}
				}
			}
		}
		return activeEnrollee;
	}
	
	/**
	 * @author shanbhag_a
	 * @since 23-02-2015 (DD-MM-YYYY)
	 * @param enrollees
	 * @return
	 */
	/*private List<Enrollee> getEnrolleesForCancel(List<Enrollee> enrollees){
		List<Enrollee> filteredEnrollees = new ArrayList<Enrollee>();
		if(enrollees != null){
			//TODO Add Abort status condition for not picking enrollee
			for(Enrollee enrollee : enrollees){
				if(enrollee.getPersonTypeLkp() != null && ((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))
						&& (enrollee.getEnrolleeLkpValue() != null && !(enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)))){
					filteredEnrollees.add(enrollee);
				}
			}
		}
		return filteredEnrollees;
	}*/
	
	/**
	 * Returns Returns all Active Enrollees for Enrollment.
	 * @author parhi_s
	 * @param enrollees
	 * @return
	 */
	private List<Enrollee> getPendingConfirmTermEnrollees(List<Enrollee> enrollees){
		List<Enrollee> filteredEnrollees = new ArrayList<Enrollee>();
		if(enrollees != null ){
			for(Enrollee enrollee : enrollees){
					if(enrollee.getPersonTypeLkp()!=null && ((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))
							&& enrollee.getEnrolleeLkpValue()!=null
							&&((enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING))||(enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) || enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))){
						filteredEnrollees.add(enrollee);
					}
			}
		}
		return filteredEnrollees;
	}
	
	
	
	/**
	 * @author Aditya_S
	 * @since 05/02/2015 (DD/MM/YYYY)
	 * 
	 * @param timeStamp
	 */
	@Override
	public void serveEnrollmentReonReportJob(String timeStamp) throws GIException {
		EnrollmentReconciliationInfo enrollmentReconciliationInfo = new EnrollmentReconciliationInfo();
		List<String> lof = null;
		List<String> lofGoodEDITrackingFileList = null;
		List<String> lofBadEDITrackingFileList = null;
		List<String> lofTA1TrackingFileList = null;
		List<String> lof999TrackingFileList = null;
		String wipFolderPath = null;
		String wipFolderEDIPath = null;
		String wipFolderTA1_N_999Path = null;
		ExecutorService executor = null;
		String enrollmentReconReportPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_RECONCILIATION_REPORT_PATH) + File.separator + EnrollmentConstants.OUT_FOLDER_NAME;
		String threadPoolSizeString =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INBOUNDTHREADPOOLSIZE);
		String strInboundThreadTimeOut = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INBOUNDTHREADTIMEOUT);
		
		if(strInboundThreadTimeOut != null){
			threadTimeOut = Integer.parseInt(strInboundThreadTimeOut);
		}
		if(threadPoolSize != null){
			threadPoolSize = Integer.parseInt(threadPoolSizeString);
		}
		
		if(enrollmentReconReportPath != null && timeStamp != null ){
			wipFolderPath = enrollmentReconReportPath + File.separator + EnrollmentConstants.WIP_FOLDER_NAME;
			wipFolderEDIPath = enrollmentReconReportPath + File.separator + EnrollmentConstants.WIP_FOLDER_NAME + "_EDI";
			wipFolderTA1_N_999Path = enrollmentReconReportPath + File.separator + EnrollmentConstants.WIP_FOLDER_NAME + "_TA1_999";
			
			/**
			 * Clear the WIP directory before Processing, to remove unprocessed files
			 */
			String failureFolderPath = enrollmentReconReportPath + File.separator + EnrollmentConstants.FAILURE_FOLDER_NAME + File.separator + timeStamp;
			String succesFolderPath = enrollmentReconReportPath + File.separator + EnrollmentConstants.SUCCESS_FOLDER_NAME + File.separator + timeStamp;
			enrollmentReconciliationInfo.setServerURL(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
			enrollmentReconciliationInfo.setFailureFolderPath(failureFolderPath);
			enrollmentReconciliationInfo.setSuccessFolderPath(succesFolderPath);
			
			moveUnprocessedFilesToFailureFolder(enrollmentReconciliationInfo, wipFolderPath);
			moveUnprocessedFilesToFailureFolder(enrollmentReconciliationInfo, wipFolderEDIPath);
			moveUnprocessedFilesToFailureFolder(enrollmentReconciliationInfo, wipFolderTA1_N_999Path);
			
			
			EnrollmentUtils.moveFiles(enrollmentReconReportPath, wipFolderPath,EnrollmentConstants.FILE_TYPE_XML,null);
			EnrollmentUtils.moveFiles(enrollmentReconReportPath, wipFolderEDIPath,EnrollmentConstants.GOOD_EDI,null);
			EnrollmentUtils.moveFiles(enrollmentReconReportPath, wipFolderEDIPath,EnrollmentConstants.BAD_EDI,null);
			EnrollmentUtils.moveFiles(enrollmentReconReportPath, wipFolderTA1_N_999Path,EnrollmentConstants.FILETYPE_TA1_TRACKING,null);
			EnrollmentUtils.moveFiles(enrollmentReconReportPath, wipFolderTA1_N_999Path,EnrollmentConstants.FILETYPE_999_TRACKING,null);
			lof = this.getFileNamesInAFolder(wipFolderPath, EnrollmentConstants.FILE_TYPE_XML);
		
			lofGoodEDITrackingFileList = this.getFileNamesInAFolder(wipFolderEDIPath, EnrollmentConstants.GOOD_EDI);
			lofBadEDITrackingFileList = this.getFileNamesInAFolder(wipFolderEDIPath, EnrollmentConstants.BAD_EDI);
			lofTA1TrackingFileList = this.getFileNamesInAFolder(wipFolderTA1_N_999Path, EnrollmentConstants.FILETYPE_TA1_TRACKING);
			lof999TrackingFileList = this.getFileNamesInAFolder(wipFolderTA1_N_999Path, EnrollmentConstants.FILETYPE_999_TRACKING);
			
			/**
			 * Processing of Received files Starts here
			 */
			processXMLFiles(lof, timeStamp, executor, threadPoolSize, threadTimeOut, enrollmentReconciliationInfo);
			processEDIFiles(lofGoodEDITrackingFileList, timeStamp, executor, threadPoolSize, threadTimeOut, enrollmentReconciliationInfo);
			processEDIFiles(lofBadEDITrackingFileList, timeStamp, executor, threadPoolSize, threadTimeOut, enrollmentReconciliationInfo);
			processTrackingFiles(lofTA1TrackingFileList, timeStamp, executor, threadPoolSize, threadTimeOut, enrollmentReconciliationInfo);
			processTrackingFiles(lof999TrackingFileList, timeStamp, executor, threadPoolSize, threadTimeOut, enrollmentReconciliationInfo);
			
			/**
			 * Processing of Received files Ends here
			 */
			
			if ((enrollmentReconciliationInfo.getEnrollmentReconciliationErrorInfoMap() != null
					&& !enrollmentReconciliationInfo.getEnrollmentReconciliationErrorInfoMap().isEmpty())
					|| (enrollmentReconciliationInfo.getGeneralFileFailureResponseMap() != null
							&& !enrollmentReconciliationInfo.getGeneralFileFailureResponseMap().isEmpty())) {
				//Log Jira for the reported issue
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				String bugDescription = gson.toJson(enrollmentReconciliationInfo);
				logBug("enrollmentReconciliationReportJob: Failed to reconcile received files", bugDescription);
				EnrollmentUtils.logFailingRecordsData("Outbound_Error_Log_"+timeStamp+".txt", new StringBuilder(bugDescription), failureFolderPath);
			}
		}else{
			LOGGER.error("Error in serveEnrollmentReonReportJob : ENROLLMENT_RECONCILIATION_REPORT_PATH not defined ");
			throw new GIException("Error in serveEnrollmentReonReportJob : ENROLLMENT_RECONCILIATION_REPORT_PATH not defined "); 
		}
	}
	
	/**
	 * 
	 * @param enrollmentReconciliationInfo
	 * @param folderPath
	 */
	private void moveUnprocessedFilesToFailureFolder(EnrollmentReconciliationInfo enrollmentReconciliationInfo, String folderPath) {
		File[] files = null;
		try {
			File folder = new File(folderPath);
			files = folder.listFiles();
			if (files != null && files.length > 0) {
				LOGGER.info("Found unprocessed file at location: " + folderPath);
				String failureFolderPath = enrollmentReconciliationInfo.getFailureFolderPath();
				if (StringUtils.isNotBlank(failureFolderPath) && !("".equals(failureFolderPath.trim())) && !(new File(failureFolderPath).exists())) {
					new File(failureFolderPath).mkdirs();
				}
				for (File file : files) {
					enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(file.getName(), "Unprocessed file from last batch job execution.");
					file.renameTo(new File(failureFolderPath + File.separator + file.getName()));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error getting files in location", e);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private void processXMLFiles(List<String> lof, String timeStamp, ExecutorService executor, Integer threadPoolSize, Integer threadTimeOut, EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException {
		if(lof != null && !lof.isEmpty()){
			enrollmentReconciliationInfo.setTotalNumberOfXMLFilesReceived(lof.size());
			if(threadPoolSize!=null){
				executor=Executors.newFixedThreadPool(threadPoolSize);
			}else{
				executor=Executors.newFixedThreadPool(EnrollmentConstants.TEN);
			}
			for(String fileName : lof){
				try{
					executor.submit(new EnrollmentReconReportJobThread(fileName,timeStamp, enrollmentReconciliationReportService, enrollmentReconciliationInfo));
				}catch(Exception e){
					enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(fileName, EnrollmentUtils.shortenedStackTrace(e, EnrollmentConstants.FIVE));
					LOGGER.error("EnrollmentReconciliationReportJob :: Exception Occurred while processing FileName: " + fileName , e);
				}
			}
			executor.shutdown();
			try {
				if(threadTimeOut!=null){
					executor.awaitTermination(threadTimeOut, TimeUnit.HOURS);
				}else{
					executor.awaitTermination(EnrollmentConstants.DEFAULT_THREAD_WAIT, TimeUnit.HOURS);
				}
			} catch (InterruptedException e) {
				LOGGER.error("EnrollmentReconciliationReportJob:: Exception occurred while terminating thread :"+e.getMessage());
				throw new GIException("XML Processing Thread Interrupted Exception :: " + e.getMessage(),e);
			}
		}else{
			LOGGER.error("EnrollmentReconciliationReportJob :: No XML file found OR Received for processing");
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private void processEDIFiles(List<String> lofEDI, String timeStamp, ExecutorService executor, Integer threadPoolSize, Integer threadTimeOut, EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException {
		if(lofEDI != null && !lofEDI.isEmpty()){
			enrollmentReconciliationInfo.setTotalNumberOfEDIFilesReceived(lofEDI.size());
			if(threadPoolSize!=null){
				executor=Executors.newFixedThreadPool(threadPoolSize);
			}else{
				executor=Executors.newFixedThreadPool(EnrollmentConstants.TEN);
			}
			for(String fileName : lofEDI){
				try{
					executor.submit(new EnrollmentReconCSVReportJobThread(fileName, timeStamp, EnrollmentConstants.FILETYPE_EDI, enrollmentReconciliationReportService, enrollmentReconciliationInfo));
				}catch(Exception e){
					enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(fileName, EnrollmentUtils.shortenedStackTrace(e, EnrollmentConstants.FIVE));
					LOGGER.error("EnrollmentReconciliationReportJob :: Exception Occurred while processing FileName: " + fileName , e);
				}
			}
			executor.shutdown();
			try {
				if(threadTimeOut!=null){
					executor.awaitTermination(threadTimeOut, TimeUnit.HOURS);
				}else{
					executor.awaitTermination(EnrollmentConstants.DEFAULT_THREAD_WAIT, TimeUnit.HOURS);
				}
			} catch (InterruptedException e) {
				LOGGER.error("Error :"+e.getMessage());
				throw new GIException("Thread Interrupted Exception :: " + e.getMessage(),e);
			}
		}
		else{
			LOGGER.warn("EnrollmentReconciliationReportJob :: No EDI file found OR Received for processing");
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processTA1And999TrackingFile(List<String> lofTrackingFileList, String timeStamp, ExecutorService executor, Integer threadPoolSize, Integer threadTimeOut, EnrollmentReconciliationInfo enrollmentReconciliationInfo)throws GIException{
		if(lofTrackingFileList != null && !lofTrackingFileList.isEmpty()){
			enrollmentReconciliationInfo.setTotalNumberOfTrackingFilesReceived(lofTrackingFileList.size());
			if(threadPoolSize != null && threadPoolSize != 0){
				executor=Executors.newFixedThreadPool(threadPoolSize);
			}else{
				executor=Executors.newFixedThreadPool(EnrollmentConstants.TEN);
			}
			for(String fileName : lofTrackingFileList){
				try{
					executor.submit(new EnrollmentReconCSVReportJobThread(fileName, timeStamp, EnrollmentConstants.FILETYPE_TA1_999, enrollmentReconciliationReportService, enrollmentReconciliationInfo));
				}
				catch(Exception ex){
					enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(fileName, EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
					LOGGER.error("EnrollmentReconciliationReportJob ::Exception Occurred while processing FileName: "+fileName, ex);
				}
			}
			executor.shutdown();
			try {
				if(threadTimeOut != null && threadTimeOut != 0) {
					executor.awaitTermination(threadTimeOut, TimeUnit.HOURS);
				} else {
					executor.awaitTermination(EnrollmentConstants.DEFAULT_THREAD_WAIT, TimeUnit.HOURS);
				}
			}
			catch (InterruptedException ex){
				throw new GIException("Exception occurred while processing TA1 or 999 tracking files :: "+ex.getMessage(), ex);
			}
		}
		else{
			LOGGER.warn("EnrollmentReconciliationReportJob :: No Tracking file found OR Received for processing");
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void processAutoTermEnrollment(Integer dateYear){
		Calendar calendar = TSCalendar.getInstance();
		calendar.set(Calendar.YEAR, dateYear);
		
		Date effectiveEndDate = DateUtil.getYearStartDate(calendar.getTime(), DateUtil.StartEndYearEnum.END);

		List<Integer> enrollmentIdList = enrollmentRepository.getEnrollmentIdsForAutoTermination(EnrollmentConstants.DECEMBER_END+dateYear.toString().trim());
		//List<Integer> priorEnrollmentIdList = enrollmentRepository.getPriorEnrollmentId(Integer.valueOf(dateYear + 1).toString());
		
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
		for (Integer enrollmentId : enrollmentIdList){
			try{
				String sendTocarrieFlag = "true";
				/*if(priorEnrollmentIdList.contains(enrollmentId)){
					sendTocarrieFlag="false";
				}*/
				disEnrollForAutoTermination(enrollmentId,effectiveEndDate,sendTocarrieFlag,user);
			}
			catch (Exception exception){
				LOGGER.error("Auto Termination Failed for Enrollment :: " + enrollmentId);
			}
		}
	}
	
	/*private void checkIfEnrollmentStatusConfirmed(Enrollment enrollmentDb) {
    	List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
    	
    	if (enrollmentDb != null) {
			try {
				//enrolleeList = enrolleeService.getEnrolleeByEnrollmentID(enrollment.getId());
				enrolleeList=enrollmentDb.getEnrolleesAndSubscriber();
			} catch (Exception e) {
				LOGGER.error("Error occured in getEnrolleesAndSubscriber : "+e);
			}
		}

		int comfirmedEnrolleeCount = 0;
		int paymentReceivedEnrolleeCount=0;
		int pendingEnrolleeCount = 0;
		
		if (enrolleeList != null && !enrolleeList.isEmpty()) {
			for (Enrollee enrollee : enrolleeList) {
				if (enrollee.getEnrolleeLkpValue() != null) {
					if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)) {
						comfirmedEnrolleeCount++;
					} else if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED)) {
						paymentReceivedEnrolleeCount++;
					} else if(enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING)){
						pendingEnrolleeCount++;
					}
				}
			}
		}
		
		if(comfirmedEnrolleeCount>0 && 
				paymentReceivedEnrolleeCount==0 && 
				pendingEnrolleeCount==0 && 
						enrollmentDb.getEnrollmentStatusLkp()!=null && 
				 	(enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING) 
				 			|| enrollmentDb.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_PAYMENT_RECEIVED )) ){
			
			enrollmentDb.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM));
		}
	}*/
	
	
	private void disEnrollForAutoTermination(Integer enrollmentId,Date effectiveEndDate,String sendTocarrieFlag,AccountUser user){
		Enrollment enrollment = enrollmentRepository.findById(enrollmentId);
		/**
		 * Adding this block to reverify if the Enrollment is already terminated or not, to avoid duplicate Aud and Enrollment_event entries.
		 * Since call to this block is from spring Batch where user can accidently clicks twice to execute job.
		 * Reference Jira Id: HIX-75016
		 */
		if(enrollment != null && !enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM))
		{
			List<EnrollmentEvent> enrollmentEventList = new ArrayList<EnrollmentEvent>();
			List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
			EnrollmentEvent enrollmentEvent = null;
			enrollment.setUpdatedBy(user);
			enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
			enrollment.setBenefitEndDate(effectiveEndDate);//set to end date of the year of effective start date
			for (Enrollee enrollee :  enrolleeRepository.getEnrolledEnrolleesForEnrollmentID(enrollment.getId())){
				if(enrollee!=null){
					enrollee.setEffectiveEndDate(effectiveEndDate);//set to end date of the year of effective start date
					enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
					enrollee.setDisenrollTimestamp(new TSDate());
					enrollee.setUpdatedBy(user);
					enrollmentEvent = new EnrollmentEvent();
					enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTO_DISENROLL_BATCH.toString());
					enrollmentEvent.setEnrollee(enrollee);
					enrollmentEvent.setEnrollment(enrollee.getEnrollment());
					enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,EnrollmentConstants.EVENT_TYPE_CANCELLATION));
					enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, EnrollmentConstants.TERMINATION_REASON_TERMINATION_OF_BENIFITS));
					enrollmentEvent.setSendToCarrierFlag(sendTocarrieFlag);
					enrollmentEvent.setSendRenewalTag(EnrollmentConstants.RENEWAL_FLAG_RENP);
					if(user!=null){
						enrollmentEvent.setCreatedBy(user);
					}
					enrollee.setLastEventId(enrollmentEvent);
					enrollmentEventList.add(enrollmentEvent);
					enrolleeList.add(enrollee);
				}
			}
			enrollment.setAbortTimestamp(new TSDate());
			enrollment.setEnrollmentEvents(enrollmentEventList);
			enrollment.setEnrollees(enrolleeList);
			enrollmentRepository.save(enrollment);
		}
	}
	
	/**
	 * @author panda_p
	 * @since 21-Aug-2015
	 * @see  HIX-74230: Suppress Benefit End Date from Carrier 834 XML based on Configuration
	 * @param issuerId
	 * @return
	 */
	private String getSendBenefitEndDateFlag(Integer issuerId){
		String sendBenefitEndDateFlag = EnrollmentConstants.FALSE;
		IssuerEnrollmentEndDateRequestDTO  objReq = null;
		IssuerEnrollmentEndDateResponseDTO objPlanResp = null;
		try{
			objReq = new IssuerEnrollmentEndDateRequestDTO ();
			objReq.setIssuerId(issuerId);
			objPlanResp = restTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_ENROLLMENT_END_DATE_FLAG, objReq, IssuerEnrollmentEndDateResponseDTO.class);
			if(isNotNullAndEmpty(objPlanResp)){
				if(objPlanResp.getEnrollmentEndDateFlag()){
					sendBenefitEndDateFlag= EnrollmentConstants.TRUE;
				}
			}
			else{
				LOGGER.warn("Received null or empty response from GET_ENROLLMENT_END_DATE_FLAG call");
			}
		}catch (Exception e){
			LOGGER.error("Error calling GET_ENROLLMENT_END_DATE_FLAG : ",e);
		}
		return sendBenefitEndDateFlag;
	}
	
	 /**
     * @author panda_p
     * @since 07-09-2015
     * 
     * @param inputStringFlag
     * @return
     */
	private boolean getBooleanFromYesOrYFlag(String inputStringFlag){
		boolean flag = false;
		if(isNotNullAndEmpty(inputStringFlag) && (inputStringFlag.equalsIgnoreCase(EnrollmentConstants.YES) || inputStringFlag.equalsIgnoreCase(EnrollmentConstants.Y))){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * returns the default EnrolleeRace for Addition(Initial Enrollment)
	 * @return List<Object<EnrolleeRace>>
	 */
	private List<EnrolleeRace> getDefaultEnrolleeRaceList()
	{
		// HIX-76870 ID-R3: EDI validation errors(The Incorrect Member Demographics (2100B, DMG))
		if(defaultEnrolleeRaceList == null)
		{
			defaultEnrolleeRaceList = new ArrayList<EnrolleeRace>();
			LookupValue lookupValueForOtherRace = lookupService
					.getlookupValueByTypeANDLookupValueLabel(
							EnrollmentConstants.LOOKUP_TYPE_RACE, EnrollmentConstants.LOOKUP_VALUE_OTHER);
			if(lookupValueForOtherRace != null)
			{
				EnrolleeRace raceEntity= new EnrolleeRace();
				raceEntity.setRaceEthnicityLkp(lookupValueForOtherRace);
				defaultEnrolleeRaceList.add(raceEntity);
			}
		}
		return defaultEnrolleeRaceList;
	}
	
	private List<Enrollee> getFilteredEnrollees(List<Enrollee> enrolleesFromRequest , List<Enrollee> enrollees){
		List<Enrollee> matchingEnrollees = new ArrayList<Enrollee>();
		List<Enrollee> missingEnrollees = new ArrayList<Enrollee>(enrollees);
		List<Enrollee> filteredEnrollees = new ArrayList<Enrollee>();
		if(enrolleesFromRequest != null )
		{
			//outerloop:
			for(Enrollee enrollee : enrolleesFromRequest)
			{
				for(Enrollee  en : enrollees){
					if(enrollee.getExchgIndivIdentifier()!=null && enrollee.getExchgIndivIdentifier().equals(en.getExchgIndivIdentifier()) && en.getPersonTypeLkp()!=null && ((en.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) 
							|| en.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))) &&  en.getEnrolleeLkpValue()!=null
							&&(!en.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))
							&&(!en.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED))
							&&(!en.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM))
							){
						matchingEnrollees.add(en);
						//break outerloop;
						break;
					}
				}
			}

			missingEnrollees.removeAll(matchingEnrollees);
		}

		for(Enrollee enr : missingEnrollees)
		{
			/* Excluding the Confirmed Enrollee for Subscriber level confirmation */
			if( enr.getPersonTypeLkp()!=null 
					&& ((enr.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) 
							|| enr.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)))
							&& enr.getEnrolleeLkpValue()!=null
							&&(!enr.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL))
							&&(!enr.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED))
							&&(!enr.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM))
							){

				filteredEnrollees.add(enr);
			}
		}
		return filteredEnrollees;
	}
	
	/**
	 * 
	 * @param batchExecutionId
	 * @param xmlFileName
	 * @param carrierSentStatus
	 * @param enrollmentDb
	 * @param enrolleeDb
	 * @param subscriber
	 * @return
	 */
	private EnrollmentInboundReconciliationReport prepareEnrollmentInboundReconciliationReport(Long batchExecutionId, String xmlFileName, String carrierSentStatus,
			Enrollment enrollmentDb, Enrollee enrolleeDb, Enrollee enrollee, Enrollment enrollmentXML){
		EnrollmentInboundReconciliationReport enrollmentInboundReconciliationReport = new EnrollmentInboundReconciliationReport();
		enrollmentInboundReconciliationReport.setBatchExecutionId(batchExecutionId);
		enrollmentInboundReconciliationReport.setXmlFileName(xmlFileName);
		enrollmentInboundReconciliationReport.setCareerSentStatus(carrierSentStatus);
		enrollmentInboundReconciliationReport.setEnrollmentId(enrollmentDb.getId());
		enrollmentInboundReconciliationReport.setIssuerName(enrollmentDb.getInsurerName());
		if(xmlFileName != null){
			enrollmentInboundReconciliationReport.setHiosIssuerId(xmlFileName.substring(5, 10));
		}else{
			enrollmentInboundReconciliationReport.setHiosIssuerId(enrollmentDb.getHiosIssuerId());
		}
		enrollmentInboundReconciliationReport.setEdiStatus("GOOD_XML");
		enrollmentInboundReconciliationReport.setHouseHoldCaseId(enrollmentDb.getHouseHoldCaseId());
		enrollmentInboundReconciliationReport.setGs04(enrollmentXML.getGs04());
		enrollmentInboundReconciliationReport.setGs05(enrollmentXML.getGs05());
		enrollmentInboundReconciliationReport.setGs06(enrollmentXML.getGs06());
		enrollmentInboundReconciliationReport.setGs04(enrollmentXML.getGs04());
		enrollmentInboundReconciliationReport.setIsa09(enrollmentXML.getIsa09());
		enrollmentInboundReconciliationReport.setIsa10(enrollmentXML.getIsa10());
		enrollmentInboundReconciliationReport.setIsa13(enrollmentXML.getIsa13());
		enrollmentInboundReconciliationReport.setSt02(enrollmentXML.getSt02());
		
		enrollmentInboundReconciliationReport.setMemberId(enrolleeDb.getExchgIndivIdentifier());
		if(enrollee!=null){
			enrollmentInboundReconciliationReport.setCarrierSentStartDate(EnrollmentUtils.getEODDate(enrollee.getEffectiveStartDate()));
			enrollmentInboundReconciliationReport.setCarrierSentEndDate(EnrollmentUtils.getEODDate(enrollee.getEffectiveEndDate()));
			
			if(enrollee.getEnrollmentEvents() != null && !enrollee.getEnrollmentEvents() .isEmpty()){
				for(EnrollmentEvent enrollmentEvent: enrollee.getEnrollmentEvents() ){
					if(enrollmentEvent.getEventReasonLkp() != null){
						enrollmentInboundReconciliationReport.setCarrierSentMrc(enrollmentEvent.getEventReasonLkp().getLookupValueCode());
						break;
					}
				}
			}
			
		}
		return enrollmentInboundReconciliationReport;
	}
	
	private void logBug(String subject, String bugDescription){
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
		try{
			if(isJiraEnabled){
				JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
						Arrays.asList(fixVersion), bugDescription, 
						subject,
						null);
			}
		}
		catch(Exception exc){
			LOGGER.error("Unable to log jira for xml validation failure: ", exc);
			LOGGER.error("Unable to log jira so printing information for Subject: "+subject +" Description: "+bugDescription);
		}
		enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.RECONCILIATION,
				subject, bugDescription);
	}

	@Override
	public boolean isEnrollmentFromPendingToCancel(Enrollment enrollment) throws GIException {
		boolean isFromPendingToCancel = false;
		try{
			if(enrollment.getEnrollmentStatusLkp() != null && 
					enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
				List<EnrollmentAud> enrollmentAudList = enrollmentAudRepository.getEnrollmentByIdAndNoPendingStatus(enrollment.getId());
				if(enrollmentAudList == null || enrollmentAudList.isEmpty()){
					isFromPendingToCancel = true;
				}
			}
		}catch(Exception e){
			LOGGER.error("filterOutPendingToCancelEnrollments :: " + e.getMessage(),e);
			throw new GIException("Error in filterOutPendingToCancelEnrollments :: " + e.getMessage(),e);
		}
		
		return isFromPendingToCancel;
	}
	
	@Override
	public void monitorInboundEDIjob(String timeStamp) throws GIException {
		EnrollmentReconciliationInfo enrollmentReconciliationInfo = new EnrollmentReconciliationInfo();
		List<String> lofBadEDITrackingFileList = null;
		List<String> lofTA1TrackingFileList = null;
		List<String> lof999TrackingFileList = null;
		String wipFolderTrackingFilePath = null;
		ExecutorService executor = null;
		String enrollmentReconReportPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_RECONCILIATION_REPORT_PATH) + File.separator + EnrollmentConstants.IN_FOLDER_NAME;
		String threadPoolSizeString =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INBOUNDTHREADPOOLSIZE);
		String strInboundThreadTimeOut = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INBOUNDTHREADTIMEOUT);
		
		if(strInboundThreadTimeOut != null){
			threadTimeOut = Integer.parseInt(strInboundThreadTimeOut);
		}
		if(threadPoolSize != null){
			threadPoolSize = Integer.parseInt(threadPoolSizeString);
		}
		
		if(enrollmentReconReportPath != null && timeStamp != null ){
			wipFolderTrackingFilePath = enrollmentReconReportPath + File.separator + EnrollmentConstants.WIP_FOLDER_NAME + "_TRACKING_FILES";
			
			/**
			 * Clear the WIP directory before Processing, to remove unprocessed files
			 */
			String failureFolderPath = enrollmentReconReportPath + File.separator + EnrollmentConstants.FAILURE_FOLDER_NAME + File.separator + timeStamp;
			String succesFolderPath = enrollmentReconReportPath + File.separator + EnrollmentConstants.SUCCESS_FOLDER_NAME + File.separator + timeStamp;
			enrollmentReconciliationInfo.setFailureFolderPath(failureFolderPath);
			enrollmentReconciliationInfo.setSuccessFolderPath(succesFolderPath);
			enrollmentReconciliationInfo.setServerURL(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
			
			moveUnprocessedFilesToFailureFolder(enrollmentReconciliationInfo, wipFolderTrackingFilePath);
			
			EnrollmentUtils.moveFiles(enrollmentReconReportPath, wipFolderTrackingFilePath,EnrollmentConstants.BAD_EDI,null);
			EnrollmentUtils.moveFiles(enrollmentReconReportPath, wipFolderTrackingFilePath,EnrollmentConstants.FILETYPE_TA1_TRACKING,null);
			EnrollmentUtils.moveFiles(enrollmentReconReportPath, wipFolderTrackingFilePath,EnrollmentConstants.FILETYPE_999_TRACKING,null);
		
			lofBadEDITrackingFileList = this.getFileNamesInAFolder(wipFolderTrackingFilePath, EnrollmentConstants.BAD_EDI);
			lofTA1TrackingFileList = this.getFileNamesInAFolder(wipFolderTrackingFilePath, EnrollmentConstants.FILETYPE_TA1_TRACKING);
			lof999TrackingFileList = this.getFileNamesInAFolder(wipFolderTrackingFilePath, EnrollmentConstants.FILETYPE_999_TRACKING);
			
			/**
			 * Processing of Received files Starts here
			 */
			processTrackingFiles(lofBadEDITrackingFileList, timeStamp, executor, threadPoolSize, threadTimeOut, enrollmentReconciliationInfo);
			processTA1And999TrackingFile(lofTA1TrackingFileList, timeStamp, executor, threadPoolSize, threadTimeOut, enrollmentReconciliationInfo);
			processTA1And999TrackingFile(lof999TrackingFileList, timeStamp, executor, threadPoolSize, threadTimeOut, enrollmentReconciliationInfo);
			
			/**
			 * Processing of Received files Ends here
			 */
			
			if ((enrollmentReconciliationInfo.getEnrollmentReconciliationErrorInfoMap() != null
					&& !enrollmentReconciliationInfo.getEnrollmentReconciliationErrorInfoMap().isEmpty())
					|| (enrollmentReconciliationInfo.getGeneralFileFailureResponseMap() != null
							&& !enrollmentReconciliationInfo.getGeneralFileFailureResponseMap().isEmpty())) {
				//Log Jira for the reported issue
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				String bugDescription = gson.toJson(enrollmentReconciliationInfo);
				logBug("monitorInboundEDIjob: Failed to reconcile received files", bugDescription);
				EnrollmentUtils.logFailingRecordsData("Inbound_Error_log_"+timeStamp+".txt", new StringBuilder(bugDescription), failureFolderPath);
			}
		}else{
			LOGGER.error("Error in monitorInboundEDIjob : ENROLLMENT_RECONCILIATION_REPORT_PATH not defined ");
			throw new GIException("Error in monitorInboundEDIjob : ENROLLMENT_RECONCILIATION_REPORT_PATH not defined "); 
		}
	}
	private void processTrackingFiles(List<String> lofTrackingFileList, String timeStamp,	ExecutorService executor, Integer threadPoolSize, Integer threadTimeOut, EnrollmentReconciliationInfo enrollmentReconciliationInfo) throws GIException {
		
		if(lofTrackingFileList != null && !lofTrackingFileList.isEmpty()){
			enrollmentReconciliationInfo.setTotalNumberOfTrackingFilesReceived(lofTrackingFileList.size());
			if(threadPoolSize != null && threadPoolSize != 0){
				executor=Executors.newFixedThreadPool(threadPoolSize);
			}else{
				executor=Executors.newFixedThreadPool(EnrollmentConstants.TEN);
			}
			for(String fileName : lofTrackingFileList){
				try{
					executor.submit(new EnrollmentEDIMonitoringInboundJobThread(fileName, timeStamp, enrollmentReconciliationReportService, enrollmentReconciliationInfo));
				}
				catch(Exception ex){
					enrollmentReconciliationInfo.putGeneralFileFailureResponseMap(fileName, EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FIVE));
					LOGGER.error("EnrollmentEDIMonitoringInboundJobThread ::Exception Occurred while processing FileName: "+fileName, ex);
				}
			}
			executor.shutdown();
			try {
				if(threadTimeOut != null && threadTimeOut != 0) {
					executor.awaitTermination(threadTimeOut, TimeUnit.HOURS);
				} else {
					executor.awaitTermination(EnrollmentConstants.DEFAULT_THREAD_WAIT, TimeUnit.HOURS);
				}
			}
			catch (InterruptedException ex){
				throw new GIException("Exception occurred while processing TA1 or 999 tracking files :: "+ex.getMessage(), ex);
			}
		}
		else{
			LOGGER.warn("EnrollmentEDIMonitoringInboundJob :: No Tracking file found OR Received for processing");
		}
	}
	
	/**
	 * Remove trailing and leading whitespaces
	 * @param str
	 * @return String
	 */
	private String removeSpaces(String str){
		String result = null;
		if(null != str){
			result = str.trim();
		}
		return result;
	}
	@Override
	public void serveEnrollmentReconciliationJob(String extractMonth, String enrollmentType, String hiosIssuerID,
			String coverageYear) throws GIException {
		boolean isCoverageYearProvided = false;
		if(StringUtils.isNotBlank(coverageYear)){
			isCoverageYearProvided = true;
		}
		try{
			Date cutOffDate=null;
			Date termCondDate=null;
			String outboundReconXmlExtractPath = "";
			String hiosIssuerId ="";
			
			if(extractMonth!=null && extractMonth.equalsIgnoreCase(EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR)){
				cutOffDate=EnrollmentUtils.getCurrentMonthStartDateTime();
				termCondDate=EnrollmentUtils.getLastMonthStartDateTime();
			}else{
				cutOffDate=new TSDate();
				termCondDate=EnrollmentUtils.getCurrentMonthStartDateTime();
			}
			
			LOGGER.info("ServeEnrollmentReconciliationJob SERVICE : Extracting Enrollment confirmed till startDate = "+cutOffDate);
			List<String> taxIDList = null;
			
			if(isNotNullAndEmpty(hiosIssuerID)){		
				if(isCoverageYearProvided){
					taxIDList = enrollmentRepository.getinsurerTaxIdNumbersByIssuerIdAndCoverageYear(cutOffDate, enrollmentType, termCondDate, hiosIssuerID, coverageYear);
				}
				else{
					taxIDList= enrollmentRepository.getinsurerTaxIdNumbersByIssuerId( cutOffDate, enrollmentType, termCondDate, hiosIssuerID);
				}
			}else{
				if(isCoverageYearProvided){
					taxIDList= enrollmentRepository.getinsurerTaxIdNumbersByCoverageYear(cutOffDate, enrollmentType, termCondDate, coverageYear);
				}
				else{
					taxIDList= enrollmentRepository.getinsurerTaxIdNumbers( cutOffDate, enrollmentType, termCondDate);
				}
			}
			
			if(taxIDList != null && !taxIDList.isEmpty()){
				String outputfolder=null;

				for(String insurerTaxId :taxIDList ){
					if(insurerTaxId!=null){
						String outputPath=null;
						
						List<Enrollment>  enrollmentList= new ArrayList<Enrollment>();
						if(isCoverageYearProvided){
							enrollmentList= enrollmentRepository.findEnrollmentByStatusAndUpdateDateAndCoverageYear(cutOffDate, enrollmentType, insurerTaxId, termCondDate, coverageYear);
						}
						else{
							enrollmentList= enrollmentRepository.findEnrollmentByStatusAndUpdateDate( cutOffDate, enrollmentType, insurerTaxId, termCondDate);
						}
						
						if(enrollmentList != null && !enrollmentList.isEmpty()){
											
							// setting the source exchange id 
							ExchgPartnerLookup exchgPartnerLookup = null;
							if(isNotNullAndEmpty(enrollmentList.get(0).getHiosIssuerId())){	
								
								LOGGER.info("calling FindExchgPartnerByHiosIssuerID to create ISA section in XML");
								LOGGER.info("HIOS_ISSUER_ID =" + enrollmentList.get(0).getHiosIssuerId());
								//LOGGER.info("GhixConstants.ISA05 =" + ISA05);
								//LOGGER.info("GhixConstants.GS08 =" + GS08);
								
								if(enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
								 exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(enrollmentList.get(0).getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_SHOP,GhixConstants.OUTBOUND);
								}
								if(enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
									 exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(enrollmentList.get(0).getHiosIssuerId(),GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL,GhixConstants.OUTBOUND);
								}
								
								if(exchgPartnerLookup != null && exchgPartnerLookup.getSourceDir() != null) {
									//	if(exchgPartnerLookup.getSourceDir() != null){
										  outboundReconXmlExtractPath = exchgPartnerLookup.getSourceDir();
									//	}
									}
								}
							hiosIssuerId = enrollmentList.get(0).getHiosIssuerId() != null ? enrollmentList.get(0).getHiosIssuerId() : "hiosIssuerId";
							outputfolder = outboundReconXmlExtractPath;
							if(outputfolder!=null && !("".equals(outputfolder.trim()))){
								if (!(new File(outputfolder).exists())) {
									throw new GIException("ServeEnrollmentReconciliationJob SERVICE : No Source Directory Exists defined in ExchangePartner Lookup Table");
								}
							}else{
								throw new GIException("ServeEnrollmentReconciliationJob SERVICE :No Source Directory defined in ExchangePartner Lookup Table");
							}
						
							Enrollments enrollments= setEnrollmentsFor834Reconciliation(enrollmentList, EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM,termCondDate);
							enrollments.setSourceExchgId(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.SOURCE_EXCHANGE_ID));
							if(exchgPartnerLookup != null){
								enrollments.setHiosIssuerId(hiosIssuerId);
								enrollments.setISA05(exchgPartnerLookup.getIsa05());
								enrollments.setISA06(exchgPartnerLookup.getIsa06());
								enrollments.setISA07(exchgPartnerLookup.getIsa07());
								enrollments.setISA08(exchgPartnerLookup.getIsa08());
								enrollments.setISA15(exchgPartnerLookup.getIsa15());
								enrollments.setGS02(exchgPartnerLookup.getGs02());
								enrollments.setGS03(exchgPartnerLookup.getGs03());
							}
							enrollments.setHiosIssuerId(hiosIssuerId);
							if (enrollmentType.equals(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
								outputPath = outputfolder + "//" +"to_" + hiosIssuerId + "_" + EnrollmentConfiguration.returnStateCode() + "_" + EnrollmentConstants.EDI_834_RECONSHOP + "_"+ dateFormat.format(new TSDate()) + ".xml";
							}
							if (enrollmentType.equals(EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL)){
								outputPath = outputfolder + "//" +"to_" + hiosIssuerId + "_" + EnrollmentConfiguration.returnStateCode() + "_" + EnrollmentConstants.EDI_834_RECONINDIV + "_" + dateFormat.format(new TSDate()) + ".xml";
							}
							generateEnrollmentsXMLReport(enrollments, outputPath,"INDEnrollmentReconXMLTrans.xsl");
						}
					}
				}
			}
			
		LOGGER.info("ServeEnrollmentReconciliationJob SERVICE : END");
		}catch(Exception e){
			LOGGER.error("Error in serveEnrollmentReconciliationJob() ::"+e.getMessage(),e);		
			throw new GIException("Error in serveEnrollmentReconciliationJob() ::"+e.getMessage(),e);
		}
	}
	
	private List<Enrollment> updateEnrlList(List<Enrollment> enrollmentList) {
		for (Enrollment enrollment : enrollmentList) {
			enrollment.setEnrollmentPremiums(enrollmentPremiumRepository.findByEnrollmentId(enrollment.getId()));
		}
		return enrollmentList;
	}
	
}
