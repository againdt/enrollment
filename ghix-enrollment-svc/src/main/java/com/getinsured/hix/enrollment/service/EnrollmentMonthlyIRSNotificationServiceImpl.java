/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.enrollment.email.EnrollmentMonthlyIRSInDoNothingNotification;
import com.getinsured.hix.enrollment.email.EnrollmentMonthlyIRSInNotification;
import com.getinsured.hix.enrollment.email.EnrollmentMonthlyIRSInProcessNotification;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
@Service("enrollmentMonthlyIRSNotificationService")
public class EnrollmentMonthlyIRSNotificationServiceImpl implements EnrollmentMonthlyIRSNotificationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyIRSNotificationServiceImpl.class);
	
	@Autowired private EnrollmentMonthlyIRSInNotification enrollmentMonthlyIRSInNotification;
	@Autowired private EnrollmentMonthlyIRSInProcessNotification enrollmentMonthlyIRSInProcessNotification;
	@Autowired private EnrollmentMonthlyIRSInDoNothingNotification enrollmentMonthlyIRSInDoNothingNotification;
	
	@Override
	public void sendEnrollmentMonthlyIRSInEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("EnrollmentMonthlyIRSNotificationService:: SEND EnrollmentMonthlyIRSInNotification details");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("EnrollmentMonthlyIRSNotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("EnrollmentMonthlyIRSNotificationService:: ( sendEnrollmentMonthlyIRSInEmail ) Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollmentMonthlyIRSInNotification.setEmailData(emailData);
			enrollmentMonthlyIRSInNotification.setRequestData(requestMap);
			Notice noticeObj = enrollmentMonthlyIRSInNotification.generateEmail();
			Notification notificationObj = enrollmentMonthlyIRSInNotification.generateNotification(noticeObj);
			enrollmentMonthlyIRSInNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			LOGGER.error("EnrollmentMonthlyIRSNotificationService:: Exception caught in sendEnrollmentMonthlyIRSInEmail method. ");
			throw new GIException(ex);
		}
	}

	@Override
	public void sendEnrollmentMonthlyInProcessEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("EnrollmentMonthlyIRSNotificationService:: SEND EnrollmentIn1095ProcessedEmail details");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("EnrollmentMonthlyIRSNotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("EnrollmentMonthlyIRSNotificationService:: (sendEnrollmentMonthlyInProcessEmail) Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollmentMonthlyIRSInProcessNotification.setEmailData(emailData);
			enrollmentMonthlyIRSInProcessNotification.setRequestData(requestMap);
			Notice noticeObj = enrollmentMonthlyIRSInProcessNotification.generateEmail();
			Notification notificationObj = enrollmentMonthlyIRSInProcessNotification.generateNotification(noticeObj);
			enrollmentMonthlyIRSInProcessNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			LOGGER.error("EnrollmentMonthlyIRSNotificationService:: Exception caught in sendEnrollmentMonthlyInProcessEmail method. ");
			throw new GIException(ex);
		}		
	}

	@Override
	public void sendDoNothingEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("EnrollmentMonthlyIRSNotificationService:: SEND DoNothingEmail details");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("EnrollmentMonthlyIRSNotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("EnrollmentMonthlyIRSNotificationService:: (sendDoNothingEmail) Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollmentMonthlyIRSInDoNothingNotification.setEmailData(emailData);
			enrollmentMonthlyIRSInDoNothingNotification.setRequestData(requestMap);
			Notice noticeObj = enrollmentMonthlyIRSInDoNothingNotification.generateEmail();
			Notification notificationObj = enrollmentMonthlyIRSInDoNothingNotification.generateNotification(noticeObj);
			enrollmentMonthlyIRSInDoNothingNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			LOGGER.error("EnrollmentMonthlyIRSNotificationService:: Exception caught in DoNothingEmail method. ");
			throw new GIException(ex);
		}	
		
	}
}
