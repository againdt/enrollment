/**
 * 
 */
package com.getinsured.hix.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.enrollment.EnrolleeRace;

/**
 * @author panda_p
 *
 */
public interface EnrolleeRaceService {
	void deleteEnrolleeRace(List<EnrolleeRace> enrolleeRaces);
     
}
