package com.getinsured.inject.enrollment;

import com.getinsured.hix.platform.audit.envers.repository.support.EnversRevisionRepositoryFactoryBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(repositoryFactoryBeanClass = EnversRevisionRepositoryFactoryBean.class,
        basePackages = {"com.getinsured.hix.enrollment.repository"})
public class EnrollmentServiceConfiguration {
}
