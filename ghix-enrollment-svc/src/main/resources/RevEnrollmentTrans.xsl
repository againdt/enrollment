<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"  byte-order-mark="no"  /> 
	<xsl:template match="/">
	
		<xsl:variable name="UPPER" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
		<xsl:variable name="lower" select="'abcdefghijklmnopqrstuvwxyz'"/>
		
		<enrollments>
			<hiosIssuerID><xsl:value-of select="enrollments/HIOS_Issuer_ID"></xsl:value-of></hiosIssuerID>
			<xsl:for-each select="enrollments/GS_Info">
			<xsl:for-each select="enrollment">
				<enrollment>
					<xsl:if test="count(enrollee)>0">
						<issuerSubscriberIdentifier><xsl:value-of select="enrollee[1]/issuerSubscriberIdentifier"></xsl:value-of></issuerSubscriberIdentifier>
						<exchgSubscriberIdentifier><xsl:value-of select="enrollee[1]/exchgSubscriberIdentifier"></xsl:value-of></exchgSubscriberIdentifier>
						<!-- <plan><id><xsl:value-of select="enrollee[1]/healthCoverage/classOfContractCode"></xsl:value-of></id></plan> -->
						<insurerCMSPlanID><xsl:value-of select="enrollee[1]/healthCoverage/classOfContractCode"></xsl:value-of></insurerCMSPlanID>
						<id><xsl:value-of select="enrollee[1]/healthCoverage/exchgAssignedPolicyID"></xsl:value-of></id>
						
						<xsl:for-each select="enrollee">
							<enrollee>
								<xsl:if test="translate(subscriberFlag,$UPPER,$lower)='y'">
									<personType><lookupValueCode>SUBSCRIBER</lookupValueCode></personType>	
								</xsl:if>
								<issuerIndivIdentifier><xsl:value-of select="issuerIndivIdentifier"></xsl:value-of></issuerIndivIdentifier>
								<benefitEffectiveBeginDate><xsl:value-of select="healthCoverage/benefitEffectiveBeginDate"></xsl:value-of></benefitEffectiveBeginDate>
								<benefitEffectiveEndDate><xsl:value-of select="healthCoverage/benefitEffectiveEndDate"></xsl:value-of></benefitEffectiveEndDate>
								<lastPremiumPaidDate><xsl:value-of select="healthCoverage/lastPremiumPaidDate"></xsl:value-of></lastPremiumPaidDate>
								<exchgIndivIdentifier><xsl:value-of select="exchgIndivIdentifier"></xsl:value-of></exchgIndivIdentifier>
									<enrolleeLkpValue>
										<lookupValueCode><xsl:value-of select="memberReportingCategory/additionalMaintReason/lookupValueCode"></xsl:value-of></lookupValueCode>
									</enrolleeLkpValue>
								<healthCoveragePolicyNo><xsl:value-of select="healthCoverage/healthCoveragePolicyNo"></xsl:value-of></healthCoveragePolicyNo>
								<employerGroupNo><xsl:value-of select="healthCoverage/employerGroupNo"></xsl:value-of></employerGroupNo>
							</enrollee>
						</xsl:for-each>
					</xsl:if>
				</enrollment>
			</xsl:for-each>
			</xsl:for-each>
		</enrollments>
	</xsl:template>
</xsl:stylesheet>