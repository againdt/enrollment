<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" >
   <xsl:output method="xml" encoding="UTF-8" indent="yes" byte-order-mark="no" cdata-section-elements="sponsorName insurerName name address1 address2" />
   <xsl:template match="/">
      <xsl:variable name="UPPER" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
      <xsl:variable name="lower" select="'abcdefghijklmnopqrstuvwxyz'" />
      <enrollments>
         <HIOS_Issuer_ID>
            <xsl:value-of select="enrollments/hiosIssuerID"  />
         </HIOS_Issuer_ID>
         <txnCreateDateTime>
            <xsl:value-of select="enrollments/txnCreateDateTime" />
         </txnCreateDateTime>
         <jobExecutionId><xsl:value-of select="enrollments/jobExecutionId"/></jobExecutionId>
         <ISA_Info>
            <ISA05>
               <xsl:value-of select="enrollments/ISA05" />
            </ISA05>
            <ISA06>
               <xsl:value-of select="enrollments/ISA06" />
            </ISA06>
            <ISA07>
               <xsl:value-of select="enrollments/ISA07" />
            </ISA07>
            <ISA08>
               <xsl:value-of select="enrollments/ISA08" />
            </ISA08>
            <ISA13>
               <xsl:value-of select="enrollments/ISA13" />
            </ISA13>
            <ISA15>
               <xsl:value-of select="enrollments/ISA15" />
            </ISA15>
           </ISA_Info>
           
         
           
         <xsl:variable name="txnCreateDateTimeVar" select="enrollments/txnCreateDateTime" />
         <GS_Info>
            <GS02>
               <xsl:value-of select="enrollments/GS02" />
            </GS02>
            <GS03>
               <xsl:value-of select="enrollments/GS03" />
            </GS03>
         <xsl:for-each select="enrollments/enrollment">
          <xsl:sort select="id" order="ascending" data-type="number"></xsl:sort>
             <!-- <xsl:sort select="revID" order="ascending" data-type="number"></xsl:sort> -->
            <xsl:variable name="exchgSubscriberIdentifierVar" select="exchgSubscriberIdentifier" />
            <xsl:variable name="issuerSubscriberIdentifierVar" select="issuerSubscriberIdentifier" />
            <xsl:variable name="insuranceTypeLkpVar" select="insuranceTypeLkp/lookupValueCode" />
            <xsl:variable name="planCoverageDescriptionVar" select="planCoverageDescription" />
            <xsl:variable name="classOfContractCodeVar" select="insurerCMSPlanID" />
            <xsl:variable name="employerIdVar" select="employer/id"/>
            
           
			<!-- <xsl:variable name="employerContributionVar" select="employerContribution" />-->            
			<!-- <xsl:variable name="aptcAmtVar" select="aptcAmt" /> -->
            <!-- <xsl:variable name="csrAmtVar" select="csrAmt" /> -->
            <!-- <xsl:variable name="healthCoveragePremiumAmtVar" select="healthCoveragePremiumAmt" /> -->
            <!-- <xsl:variable name="netPremiumAmtVar" select="netPremiumAmt" /> -->
            <xsl:variable name="groupPolicyNumberVar" select="groupPolicyNumber" />
            <xsl:variable name="responsiblePersonVar" select="responsiblePerson" />
            <xsl:variable name="createdOnVar" select="concat(substring(createdOn, 1, 4), substring(createdOn, 6, 2), substring(createdOn, 9, 2))" />
            <xsl:value-of select="ancestor::enrollment/id"></xsl:value-of>
            
        
            <enrollment>
            
               <id>
                  <xsl:value-of select="id" />
               </id>
               <actionCode>2</actionCode>
               <sponsorName>
                  <xsl:value-of select="normalize-space(sponsorName)" disable-output-escaping="yes" />
               </sponsorName>
               <xsl:choose>
                  <xsl:when test="sponsorEIN">
                     <sponsorEIN>
                        <xsl:value-of select="sponsorEIN" />
                     </sponsorEIN>
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:choose>
                       <xsl:when test="sponsorTaxIdNumber">
                       <sponsorTaxIdNumber>
                       <xsl:value-of select="sponsorTaxIdNumber" />
                       </sponsorTaxIdNumber>
                      </xsl:when>
                        <xsl:otherwise>
                         <exchgSubscriberIdentifier>
                          <xsl:value-of select="$exchgSubscriberIdentifierVar" />
                         </exchgSubscriberIdentifier>
                        </xsl:otherwise>
                      </xsl:choose>
                  </xsl:otherwise>
               </xsl:choose>
               <insurerName>
                  <xsl:value-of select="normalize-space(insurerName)"  disable-output-escaping="yes" />
               </insurerName>
               <xsl:choose>
                  <xsl:when test="insurerCMSPlanID">
                     <insurerCMSPlanID>
                        <xsl:value-of select="insurerCMSPlanID" />
                     </insurerCMSPlanID>
                  </xsl:when>
                  <xsl:otherwise />
               </xsl:choose>
               <xsl:if test="translate(brokerRole,$UPPER,$lower)='agent'">
                  <brokerAgentName>
                     <xsl:value-of select="brokerAgentName" />
                  </brokerAgentName>
  				  <brokerFEDTaxPayerId>
	                  <xsl:value-of select="brokerFEDTaxPayerId" />
                  </brokerFEDTaxPayerId>
                  <brokerTPAAccountNumber1>
                     <xsl:value-of select="brokerTPAAccountNumber1" />
                  </brokerTPAAccountNumber1>
                  <brokerTPAAccountNumber2>
                     <xsl:value-of select="brokerTPAAccountNumber2" />
                  </brokerTPAAccountNumber2>
               </xsl:if>
               <QTYy>
                  <xsl:value-of select="QTYy" />
               </QTYy>
               <QTYn>
                  <xsl:value-of select="QTYn" />
               </QTYn>
               <QTYt>
                  <xsl:value-of select="QTYt" />
               </QTYt>
               <issuer>
                  <name>
                     <xsl:value-of select="normalize-space(insurerName)"   disable-output-escaping="yes"/>
                  </name>
               </issuer>
               <xsl:for-each select="enrollee">
                  <!-- <xsl:sort select="personType/lookupValueCode" order="descending"></xsl:sort> -->
                  <xsl:variable name="benefitStartDateVar" select="benefitEffectiveBeginDate" />
                  <xsl:variable name="benefitEndDateVar" select="benefitEffectiveEndDate" />
                  <xsl:variable name="lastPremiumDateVar" select="lastPremiumPaidDate" />
                  <xsl:variable name="eventTypeLkpVar" select="enrollmentEvents[1]/eventTypeLkp/lookupValueCode"/>
                  <xsl:variable name="eventCreatedOnVar" select="enrollmentEvents[1]/createdOn"/>
                  <enrollee>
                  	 <enrollmentEventId><xsl:value-of select="enrollmentEvents[1]/id"/></enrollmentEventId>
                     <xsl:choose>
                        <xsl:when test="translate(personType/lookupValueCode,$UPPER,$lower)='subscriber'">
                           <subscriberFlag>Y</subscriberFlag>
                           <relationshipLkp>
                              <lookupValueCode>18</lookupValueCode>
                           </relationshipLkp>
                        </xsl:when>
                        <xsl:otherwise>
                           <subscriberFlag>N</subscriberFlag>
                           <relationshipLkp>
                               <xsl:choose>
                           <xsl:when test="relationshipToSubscriber/lookupValueCode">
                              <lookupValueCode>
                                 <xsl:value-of select="relationshipToSubscriber/lookupValueCode" />
                              </lookupValueCode>
                             </xsl:when>
                              <xsl:otherwise>
                              	<lookupValueCode>G8</lookupValueCode>
                              </xsl:otherwise>
                              </xsl:choose>
                           </relationshipLkp>
                        </xsl:otherwise>
                     </xsl:choose>
                     <xsl:for-each select="enrollmentEvents">
                        <enrollmentEvents>
                           <eventTypeLkp>
                              <lookupValueCode>
                                 <xsl:value-of select="eventTypeLkp/lookupValueCode" />
                              </lookupValueCode>
                           </eventTypeLkp>
                           <eventReasonLkp>
                              <lookupValueCode>
                                 <xsl:value-of select="eventReasonLkp/lookupValueCode" />
                              </lookupValueCode>
                           </eventReasonLkp>
                           <createdOn>
                              <xsl:value-of select="concat(substring(createdOn, 1, 4), substring(createdOn, 6, 2), substring(createdOn, 9, 2))" />
                           </createdOn>
                           <updatedOn>
                              <xsl:value-of select="concat(substring(updatedOn, 1, 4), substring(updatedOn, 6, 2), substring(updatedOn, 9, 2))" />
                           </updatedOn>
                        </enrollmentEvents>
                     </xsl:for-each>
                     <exchgSubscriberIdentifier>
                        <xsl:value-of select="$exchgSubscriberIdentifierVar" />
                     </exchgSubscriberIdentifier>
                    <exchgAssignedPolicyID>
                       <xsl:value-of select="ancestor::enrollment/id"></xsl:value-of>
                    </exchgAssignedPolicyID>
                     <exchgIndivIdentifier>
                        <xsl:value-of select="exchgIndivIdentifier" />
                     </exchgIndivIdentifier>
                     <issuerIndivIdentifier>
                        <xsl:value-of select="issuerIndivIdentifier" />
                     </issuerIndivIdentifier>
                     <xsl:if test="ancestor::enrollment/enrollmentTypeLkp and translate(ancestor::enrollment/enrollmentTypeLkp/lookupValueCode,$UPPER,$lower)='fi'">
                     	<paymentTxnID>
                        	<xsl:value-of select="ancestor::enrollment/paymentTxnId" />
                     	</paymentTxnID>
                     </xsl:if>
                     <issuerSubscriberIdentifier>
                        <xsl:value-of select="$issuerSubscriberIdentifierVar" />
                     </issuerSubscriberIdentifier>
                     <memberSignatureDate>
                        <xsl:value-of select="concat(substring(formatSignatureDate, 1, 4), substring(formatSignatureDate, 6, 2), substring(formatSignatureDate, 9, 2))" />
                     </memberSignatureDate>
                     <xsl:if test="translate(enrolleeLkpValue/lookupValueCode,$UPPER,$lower)='cancel' or translate(enrolleeLkpValue/lookupValueCode,$UPPER,$lower)='term'">
	                     <memberEligibilityEndDate>
	                           <xsl:value-of select="concat(substring($benefitEndDateVar, 1, 4), substring($benefitEndDateVar, 6, 2), substring($benefitEndDateVar, 9, 2))" />
	                     </memberEligibilityEndDate>
                     </xsl:if>
                     <memberMaintEffectiveDate>
                           <xsl:value-of select="concat(substring($eventCreatedOnVar, 1, 4), substring($eventCreatedOnVar, 6, 2), substring($eventCreatedOnVar, 9, 2))" />
                     </memberMaintEffectiveDate>
                     <xsl:choose>
                        <xsl:when test="memberEntityIdentifierCode">
                         <memberEntityIdentifierCode>
                          <lookupValueCode>
                            <xsl:value-of select="memberEntityIdentifierCode" />
                          </lookupValueCode>
                         </memberEntityIdentifierCode>
                        </xsl:when>
                        <xsl:otherwise>
                         <memberEntityIdentifierCode>
                          <lookupValueCode>IL</lookupValueCode>
                       </memberEntityIdentifierCode>
                        </xsl:otherwise>
                     </xsl:choose>
                     <memberLastName>
                        <xsl:value-of select="lastName" />
                     </memberLastName>
                     <memberFirstName>
                        <xsl:value-of select="firstName" />
                     </memberFirstName>
                     <memberMiddleName>
                        <xsl:value-of select="middleName" />
                     </memberMiddleName>
                     <memberNameSuffix>
                        <xsl:value-of select="nameSuffix" />
                     </memberNameSuffix>
                     <xsl:choose>
                        <xsl:when test="SSN">
                           <memberSSN>
                              <xsl:value-of select="SSN" />
                           </memberSSN>
                        </xsl:when>
                        <xsl:otherwise>
                           <memberNationalIndivID>
                              <xsl:value-of select="nationalIndivID" />
                           </memberNationalIndivID>
                        </xsl:otherwise>
                     </xsl:choose>
                     <memberPrimaryPhoneNo>
                        <xsl:value-of select="primaryPhoneNo" />
                     </memberPrimaryPhoneNo>
                     <memberSecondaryPhoneNo>
                        <xsl:value-of select="secondaryPhoneNo" />
                     </memberSecondaryPhoneNo>
                     <xsl:choose>
                        <xsl:when test="preferredSMS">
                           <memberPreferredSMS>
                              <xsl:value-of select="preferredSMS" />
                           </memberPreferredSMS>
                        </xsl:when>
                        <xsl:otherwise>
                           <memberPreferredEmail>
                              <xsl:value-of select="preferredEmail" />
                           </memberPreferredEmail>
                        </xsl:otherwise>
                     </xsl:choose>
                     <memberHomeAddress>
                        <address1>
                           <xsl:value-of select="homeAddress/address1"   disable-output-escaping="yes"/>
                        </address1>
                        <xsl:if test =" translate(translate(homeAddress/address2,$UPPER,$lower),' ','') != translate(translate(homeAddress/address1,$UPPER,$lower),' ','')">
                        <address2>
                           <xsl:value-of select="homeAddress/address2"   disable-output-escaping="yes" />
                        </address2>
                        </xsl:if>
                        <city>
                           <xsl:value-of select="homeAddress/city" />
                        </city>
                        <state>
                           <xsl:value-of select="homeAddress/state" />
                        </state>
                        <xsl:choose>                      
                        <xsl:when test="homeAddress/zip" >
                        <xsl:choose>
                        <xsl:when test="(string-length(homeAddress/zip)+1) &gt; 5">
                        <zip>
	                        <xsl:value-of select="substring(homeAddress/zip,1,5)" />
	                    </zip>
					    </xsl:when>
					    <xsl:otherwise>
					    <zip>
	                         <xsl:value-of select="substring(concat('00000',homeAddress/zip),(string-length(homeAddress/zip)+1),5)" />
	                    </zip>
					    </xsl:otherwise>
					    </xsl:choose>
                        </xsl:when>
                        </xsl:choose>
                        <county>
                           <xsl:value-of select="homeAddress/county_code" />
                        </county>
                     </memberHomeAddress>
                     <memberBirthDate>
                        <xsl:value-of select="concat(substring(birthDate, 1, 4), substring(birthDate, 6, 2), substring(birthDate, 9, 2))" />
                     </memberBirthDate>
                     <memberIndivDeathDate>
                        <xsl:value-of select="concat(substring(memberIndivDeathDate, 1, 4), substring(memberIndivDeathDate, 6, 2), substring(memberIndivDeathDate, 9, 2))" />
                     </memberIndivDeathDate>
                     <memberGenderLkp>
                        <lookupValueCode>
                           <xsl:value-of select="genderLkp/lookupValueCode" />
                        </lookupValueCode>
                     </memberGenderLkp>
                     <xsl:choose>
                        <xsl:when test="translate(personType/lookupValueCode,$UPPER,$lower) ='subscriber'">
                           <memberMaritalStatusLkp>
                              <lookupValueCode>
                                 <xsl:value-of select="maritalStatusLkp/lookupValueCode" />
                              </lookupValueCode>
                           </memberMaritalStatusLkp>
                           <memberCitizenshipStatusLkp>
                              <lookupValueCode>
                                 <xsl:value-of select="citizenshipStatusLkp/lookupValueCode" />
                              </lookupValueCode>
                           </memberCitizenshipStatusLkp>
                        </xsl:when>
                        <xsl:otherwise />
                     </xsl:choose>
                     <xsl:for-each select="raceEthnicity">
                     	<xsl:sort select="id" order="ascending" data-type="number"></xsl:sort>
                        <memberRaceEthnicityLkp>
                           <lookupValueCode>
                              <xsl:value-of select="raceEthnicityLkp/lookupValueCode" />
                           </lookupValueCode>
                        </memberRaceEthnicityLkp>
                     </xsl:for-each>
                     <memberTobaccoUsageLkp>
                        <lookupValueCode>
                        	<xsl:choose>
                        	<xsl:when test="tobaccoUsageLkp">
                           <xsl:value-of select="tobaccoUsageLkp/lookupValueCode" />
                        	</xsl:when>
                           	<xsl:otherwise>U</xsl:otherwise>
                           </xsl:choose>
                        </lookupValueCode>
                     </memberTobaccoUsageLkp>
                     <languageSpokenLkp>
                        <lookupValueCode>
                           <xsl:value-of select="languageSpokenLkp/lookupValueCode" />
                        </lookupValueCode>
                     </languageSpokenLkp>
                     <languageWrittenLkp>
                        <lookupValueCode>
                           <xsl:value-of select="languageWrittenLkp/lookupValueCode" />
                        </lookupValueCode>
                     </languageWrittenLkp>
                     <incorrectMemberLastName>
                        <xsl:value-of select="incorrectMemberLastName" />
                     </incorrectMemberLastName>
                     <incorrectMemberFirstName>
                        <xsl:value-of select="incorrectMemberFirstName" />
                     </incorrectMemberFirstName>
                     <incorrectMemberMiddleName>
                        <xsl:value-of select="incorrectMemberMiddleName" />
                     </incorrectMemberMiddleName>
                     <incorrectMemberNameSuffix>
                        <xsl:value-of select="incorrectMemberNameSuffix" />
                     </incorrectMemberNameSuffix>
                     <incorrectMemberNationalIndivID>
                        <xsl:value-of select="incorrectMemberNationalIndivID" />
                     </incorrectMemberNationalIndivID>
                      <incorrectMemberSSN>
                        <xsl:value-of select="incorrectMemberSSN" />
                     </incorrectMemberSSN>
                     <incorrectMemberBirthDate>
                        <xsl:value-of select="concat(substring(incorrectMemberBirthDate, 1, 4), substring(incorrectMemberBirthDate, 6, 2), substring(incorrectMemberBirthDate, 9, 2))" />
                     </incorrectMemberBirthDate>
                     <incorrectMemberGenderLkp>
                        <lookupValueCode>
                           <xsl:value-of select="incorrectMemberGenderLkp/lookupValueCode" />
                        </lookupValueCode>
                     </incorrectMemberGenderLkp>
                     <incorrectMemberMaritalStatusLkp>
                        <lookupValueCode>
                           <xsl:value-of select="incorrectMemberMaritalStatusLkp/lookupValueCode" />
                        </lookupValueCode>
                     </incorrectMemberMaritalStatusLkp>
                        <xsl:for-each select="incorrectMemberRaceEthnicityLkp">
                        	<xsl:sort select="id" order="ascending" data-type="number"></xsl:sort>
                           <incorrectMemberRaceEthnicityLkp>
                              <lookupValueCode>
                                 <xsl:value-of select="raceEthnicityLkp/lookupValueCode" />
                              </lookupValueCode>
                           </incorrectMemberRaceEthnicityLkp>
                        </xsl:for-each>
                     <incorrectMemberCitizenshipStatusLkp>
                        <lookupValueCode>
                           <xsl:value-of select="incorrectMemberCitizenshipStatusLkp/lookupValueCode" />
                        </lookupValueCode>
                     </incorrectMemberCitizenshipStatusLkp>
                     <xsl:if test="translate(showMailingAddress,$UPPER,$lower)='true'">
                        <memberMailingAddress>
                           <address1>
                              <xsl:value-of select="mailingAddress/address1"   disable-output-escaping="yes"/>
                           </address1>
                           <xsl:if test =" translate(translate(mailingAddress/address2,$UPPER,$lower),' ','') != translate(translate( mailingAddress/address1,$UPPER,$lower),' ','')">
                           <address2>
                              <xsl:value-of select="mailingAddress/address2"   disable-output-escaping="yes"/>
                           </address2>
                           </xsl:if>
                           <city>
                              <xsl:value-of select="mailingAddress/city" />
                           </city>
                           <state>
                              <xsl:value-of select="mailingAddress/state" />
                           </state>
							<xsl:choose>                      
							<xsl:when test="mailingAddress/zip" >
							<xsl:choose>
							<xsl:when test="(string-length(mailingAddress/zip)+1) &gt; 5">
							<zip>
							  <xsl:value-of select="substring(mailingAddress/zip,1,5)" />
							</zip>
							</xsl:when>
							<xsl:otherwise>
							<zip>
							   <xsl:value-of select="substring(concat('00000',mailingAddress/zip),(string-length(mailingAddress/zip)+1),5)" />
							</zip>
							</xsl:otherwise>
							</xsl:choose>
							</xsl:when>
							</xsl:choose>
                        </memberMailingAddress>
                     </xsl:if>
                     <!-- Custodial display condition start to be put here-->
                     <xsl:if test="custodialParent">
                        <custodialParent>
                           <lastName>
                              <xsl:value-of select="custodialParent/lastName" />
                           </lastName>
                           <firstName>
                              <xsl:value-of select="custodialParent/firstName" />
                           </firstName>
                           <middleName>
                              <xsl:value-of select="custodialParent/middleName" />
                           </middleName>
                           <nameSuffix>
                              <xsl:value-of select="custodialParent/nameSuffix" />
                           </nameSuffix>
                           <xsl:choose>
                              <xsl:when test="custodialParent/SSN">
                                 <SSN>
                                    <xsl:value-of select="custodialParent/SSN" />
                                 </SSN>
                              </xsl:when>
                              <xsl:otherwise>
                                 <nationalIndivID>
                                    <xsl:value-of select="custodialParent/nationalIndivID" />
                                 </nationalIndivID>
                              </xsl:otherwise>
                           </xsl:choose>
                           <primaryPhoneNo>
                              <xsl:value-of select="custodialParent/primaryPhoneNo" />
                           </primaryPhoneNo>
                           <secondaryPhoneNo>
                              <xsl:value-of select="custodialParent/secondaryPhoneNo" />
                           </secondaryPhoneNo>
                           <xsl:choose>
                              <xsl:when test="custodialParent/preferredSMS">
                                 <preferredSMS>
                                    <xsl:value-of select="custodialParent/preferredSMS" />
                                 </preferredSMS>
                              </xsl:when>
                              <xsl:otherwise>
                                 <preferredEmail>
                                    <xsl:value-of select="custodialParent/preferredEmail" />
                                 </preferredEmail>
                              </xsl:otherwise>
                           </xsl:choose>
                           <address>
                              <id>
                                 <xsl:value-of select="custodialParent/homeAddress/id" />
                              </id>
                              <address1>
                                 <xsl:value-of select="custodialParent/homeAddress/address1"   disable-output-escaping="yes"/>
                              </address1>
                              <xsl:if test =" translate(translate(custodialParent/homeAddress/address2,$UPPER,$lower),' ','') != translate(translate( custodialParent/homeAddress/address1,$UPPER,$lower),' ','')">
                              <address2>
                                 <xsl:value-of select="custodialParent/homeAddress/address2"   disable-output-escaping="yes"/>
                              </address2>
                              </xsl:if>
                              <city>
                                 <xsl:value-of select="custodialParent/homeAddress/city" />
                              </city>
                              <state>
                                 <xsl:value-of select="custodialParent/homeAddress/state" />
                              </state>
                              <xsl:choose>                      
								<xsl:when test="custodialParent/homeAddress/zip" >
								<xsl:choose>
								<xsl:when test="(string-length(custodialParent/homeAddress/zip)+1) &gt; 5">
								<zip>
								  <xsl:value-of select="substring(custodialParent/homeAddress/zip,1,5)" />
								</zip>
								</xsl:when>
								<xsl:otherwise>
								<zip>
								   <xsl:value-of select="substring(concat('00000',custodialParent/homeAddress/zip),(string-length(custodialParent/homeAddress/zip)+1),5)" />
								</zip>
								</xsl:otherwise>
								</xsl:choose>
								</xsl:when>
								</xsl:choose>
                              <createdOn>
                                 <xsl:value-of select="concat(substring(custodialParent/homeAddress/createdOn, 1, 4), substring(custodialParent/homeAddress/createdOn, 6, 2), substring(custodialParent/homeAddress/createdOn, 9, 2))" />
                              </createdOn>
                              <updatedOn>
                                 <xsl:value-of select="concat(substring(custodialParent/homeAddress/updatedOn, 1, 4), substring(custodialParent/homeAddress/updatedOn, 6, 2), substring(custodialParent/homeAddress/updatedOn, 9, 2))" />
                              </updatedOn>
                           </address>
                        </custodialParent>
                     </xsl:if>
                     <!-- Custodial display condition end to be put here-->
                     <!-- Responsible display condition start to be put here-->
                     <xsl:if test="$responsiblePersonVar">
                        <responsiblePerson>
                           <idCode>
                              <xsl:value-of select="$responsiblePersonVar/idCode" />
                           </idCode>
                           <lastName>
                              <xsl:value-of select="$responsiblePersonVar/lastName" />
                           </lastName>
                           <firstName>
                              <xsl:value-of select="$responsiblePersonVar/firstName" />
                           </firstName>
                           <middleName>
                              <xsl:value-of select="$responsiblePersonVar/middleName" />
                           </middleName>
                           <nameSuffix>
                              <xsl:value-of select="$responsiblePersonVar/nameSuffix" />
                           </nameSuffix>
                           <xsl:choose>
                              <xsl:when test="$responsiblePersonVar/SSN">
                                 <SSN>
                                    <xsl:value-of select="$responsiblePersonVar/SSN" />
                                 </SSN>
                              </xsl:when>
                              <xsl:otherwise>
                                 <nationalIndivID>
                                    <xsl:value-of select="$responsiblePersonVar/nationalIndivID" />
                                 </nationalIndivID>
                              </xsl:otherwise>
                           </xsl:choose>
                           <primaryPhoneNo>
                              <xsl:value-of select="$responsiblePersonVar/primaryPhoneNo" />
                           </primaryPhoneNo>
                           <secondaryPhoneNo>
                              <xsl:value-of select="$responsiblePersonVar/secondaryPhoneNo" />
                           </secondaryPhoneNo>
                           <xsl:choose>
                              <xsl:when test="$responsiblePersonVar/preferredSMS">
                                 <preferredSMS>
                                    <xsl:value-of select="$responsiblePersonVar/preferredSMS" />
                                 </preferredSMS>
                              </xsl:when>
                              <xsl:otherwise>
                                 <preferredEmail>
                                    <xsl:value-of select="$responsiblePersonVar/preferredEmail" />
                                 </preferredEmail>
                              </xsl:otherwise>
                           </xsl:choose>
                           <address>
                              <address1>
                                 <xsl:value-of select="$responsiblePersonVar/homeAddress/address1"   disable-output-escaping="yes"/>
                              </address1>
                              <xsl:if test =" translate(translate($responsiblePersonVar/homeAddress/address2,$UPPER,$lower),' ','') != translate(translate($responsiblePersonVar/homeAddress/address1,$UPPER,$lower),' ','')">
                              <address2>
                                 <xsl:value-of select="$responsiblePersonVar/homeAddress/address2"   disable-output-escaping="yes"/>
                              </address2>
                              </xsl:if>
                              <city>
                                 <xsl:value-of select="$responsiblePersonVar/homeAddress/city" />
                              </city>
                              <state>
                                 <xsl:value-of select="$responsiblePersonVar/homeAddress/state" />
                              </state>
                           	  <xsl:choose>                      
								<xsl:when test="$responsiblePersonVar/homeAddress/zip" >
								<xsl:choose>
								<xsl:when test="(string-length($responsiblePersonVar/homeAddress/zip)+1) &gt; 5">
								<zip>
								  <xsl:value-of select="substring($responsiblePersonVar/homeAddress/zip,1,5)" />
								</zip>
								</xsl:when>
								<xsl:otherwise>
								<zip>
								   <xsl:value-of select="substring(concat('00000',$responsiblePersonVar/homeAddress/zip),(string-length($responsiblePersonVar/homeAddress/zip)+1),5)" />
								</zip>
								</xsl:otherwise>
								</xsl:choose>
								</xsl:when>
								</xsl:choose>
                           </address>
                        </responsiblePerson>
                     </xsl:if>
                     <!-- Responsible display condition end to be put here-->
                     <healthCoverage>
					 <maintenanceTypeCode>
                           <xsl:for-each select="enrollmentEvents">
                              <lookupValueCode>
                                 <xsl:value-of select="eventTypeLkp/lookupValueCode" />
                              </lookupValueCode>
                           </xsl:for-each>
                        </maintenanceTypeCode>
                        <maintenanceReasonCode>
                           <xsl:for-each select="enrollmentEvents">
                              <lookupValueCode>
                                 <xsl:value-of select="eventReasonLkp/lookupValueCode" />
                              </lookupValueCode>
                           </xsl:for-each>
                        </maintenanceReasonCode>
                        <insuranceTypeLkp>
                           <lookupValueCode>
                              <xsl:value-of select="$insuranceTypeLkpVar" />
                           </lookupValueCode>
                        </insuranceTypeLkp>
                        <planCoverageDescription>
                           <xsl:value-of select="$planCoverageDescriptionVar"  />
                        </planCoverageDescription>
                        <benefitEffectiveBeginDate>
                           <xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                        </benefitEffectiveBeginDate>
							<xsl:choose>
								<xsl:when test="ancestor::enrollments/sendBenefitEndDate and translate(ancestor::enrollments/sendBenefitEndDate,$UPPER,$lower)='true'">
										<benefitEffectiveEndDate>
										   <xsl:value-of select="concat(substring($benefitEndDateVar, 1, 4), substring($benefitEndDateVar, 6, 2), substring($benefitEndDateVar, 9, 2))" />
										</benefitEffectiveEndDate>
								</xsl:when>
								<xsl:when test="ancestor::enrollments/sendBenefitEndDate and translate(ancestor::enrollments/sendBenefitEndDate,$UPPER,$lower)='false'">
									<xsl:if test="translate(enrolleeLkpValue/lookupValueCode,$UPPER,$lower)='cancel' or translate(enrolleeLkpValue/lookupValueCode,$UPPER,$lower)='term'">
									<benefitEffectiveEndDate>
									   <xsl:value-of select="concat(substring($benefitEndDateVar, 1, 4), substring($benefitEndDateVar, 6, 2), substring($benefitEndDateVar, 9, 2))" />
									</benefitEffectiveEndDate>
									</xsl:if>
								</xsl:when>
							</xsl:choose>
                        <lastPremiumPaidDate>
                           <xsl:value-of select="concat(substring($lastPremiumDateVar, 1, 4), substring($lastPremiumDateVar, 6, 2), substring($lastPremiumDateVar, 9, 2))" />
                        </lastPremiumPaidDate>
                        <healthCoveragePolicyNo>
                           <xsl:value-of select="healthCoveragePolicyNo" />
                        </healthCoveragePolicyNo>
                        <exchgAssignedPolicyID>
                            <xsl:value-of select="ancestor::enrollment/id"></xsl:value-of>
                        </exchgAssignedPolicyID>
                        <classOfContractCode>
                           <xsl:value-of select="$classOfContractCodeVar" />
                        </classOfContractCode>
                        <employerGroupNo>
                           <xsl:value-of select="$employerIdVar" />
                        </employerGroupNo>
						<householdOrEmployeeCaseID>
							<xsl:value-of select="ancestor::enrollment/houseHoldCaseId"></xsl:value-of>
						</householdOrEmployeeCaseID>
                        <createdOn>
                           <xsl:value-of select="$createdOnVar" />
                        </createdOn>
                     </healthCoverage>
                     <!-- HIX-88192 Changes ends here -->
                     <memberReportingCategory>
                     	<xsl:if test="formatSignatureDate">
                       	  	<requestSubmitTimestampDate>
                       	  		 <xsl:value-of select="concat(substring(formatSignatureDate, 1, 4), substring(formatSignatureDate, 6, 2), substring(formatSignatureDate, 9, 2))"/>
                       	  	</requestSubmitTimestampDate>
                       	  	<requestSubmitTimestamp>
                       	  		 <xsl:value-of select="concat(substring(formatSignatureDate, 1, 4), substring(formatSignatureDate, 6, 2), substring(formatSignatureDate, 9, 2),substring(formatSignatureDate, 12, 2),substring(formatSignatureDate, 15, 2),substring(formatSignatureDate, 18, 2) )"/>
                       	  	</requestSubmitTimestamp>
                        </xsl:if>
                     	<xsl:if test="renewalStatus">
                     		<renewalStatus>
                     			<xsl:value-of select="renewalStatus"/>
                     		</renewalStatus>
                     	</xsl:if>
                     	<xsl:if test="ancestor::enrollment/lastEnrollmentId">
							<previousExchgAssignedPolicyIDDate><xsl:value-of select="concat(substring(formatSignatureDate, 1, 4), substring(formatSignatureDate, 6, 2), substring(formatSignatureDate, 9, 2))"/></previousExchgAssignedPolicyIDDate> 	
                     		<previousExchgAssignedPolicyID><xsl:value-of select="ancestor::enrollment/lastEnrollmentId"/></previousExchgAssignedPolicyID>	
                     	</xsl:if>
                     	<xsl:if test="ancestor::enrollment/priorEnrollmentId">
							<previousExchgAssignedPolicyIDDate><xsl:value-of select="concat(substring(formatSignatureDate, 1, 4), substring(formatSignatureDate, 6, 2), substring(formatSignatureDate, 9, 2))"/></previousExchgAssignedPolicyIDDate> 	
                     		<previousExchgAssignedPolicyID><xsl:value-of select="ancestor::enrollment/priorEnrollmentId"/></previousExchgAssignedPolicyID>	
                     	</xsl:if>
                     	
                        <xsl:if test="$eventTypeLkpVar!='024'">
                           <xsl:if test="translate(personType/lookupValueCode,$UPPER,$lower)='subscriber'">
                              <xsl:if test="aptcAmt">
                                 <aptcDate>
                                    <xsl:value-of select="concat(substring(aptcDate, 1, 4), substring(aptcDate, 6, 2), substring(aptcDate, 9, 2))" />
                                    <xsl:if test="not(aptcDate)">
                                    	<xsl:value-of select="concat(substring(ancestor::enrollment/benefitStartDate, 1, 4), substring(ancestor::enrollment/benefitStartDate, 6, 2), substring(ancestor::enrollment/benefitStartDate, 9, 2))" />
                                  	</xsl:if>
                                 </aptcDate>
                                 <aptcAmt>
                                    <xsl:value-of select="format-number(aptcAmt,'#0.00')" />
                                 </aptcAmt>
                              </xsl:if>
                              <xsl:if test="csrAmt and string-length(normalize-space(csrAmt))!=0">
                                 <csrDate>
                                    <xsl:value-of select="concat(substring(csrDate, 1, 4), substring(csrDate, 6, 2), substring(csrDate, 9, 2))" />
                                    <xsl:if test="not(csrDate)">
                                    	<xsl:value-of select="concat(substring(ancestor::enrollment/benefitStartDate, 1, 4), substring(ancestor::enrollment/benefitStartDate, 6, 2), substring(ancestor::enrollment/benefitStartDate, 9, 2))" />
                                  	</xsl:if>
                                 </csrDate>
                                 <csrAmt>
                                    <xsl:value-of select="format-number(csrAmt,'#0.00')" />
                                 </csrAmt>
                              </xsl:if>
                              <xsl:if test="translate(ancestor::enrollments/stateSubsidyEnabled,$UPPER,$lower)='true'">
                                   <othPayAmt1Date>
                                       <xsl:value-of select="concat(substring(ancestor::enrollment/stateSubsidyDate, 1, 4), substring(ancestor::enrollment/stateSubsidyDate, 6, 2), substring(ancestor::enrollment/stateSubsidyDate, 9, 2))" />
                                       <xsl:if test="not(ancestor::enrollment/stateSubsidyDate)">
                                           <xsl:value-of select="concat(substring(ancestor::enrollment/benefitStartDate, 1, 4), substring(ancestor::enrollment/benefitStartDate, 6, 2), substring(ancestor::enrollment/benefitStartDate, 9, 2))" />
                                       </xsl:if>
                                   </othPayAmt1Date>
                                   <othPayAmt1>
                                       <xsl:if test="not(ancestor::enrollment/stateSubsidy)">
                                           <xsl:text>0.00</xsl:text>
                                       </xsl:if>
                                       <xsl:if test="ancestor::enrollment/stateSubsidy and string-length(normalize-space(ancestor::enrollment/stateSubsidy))!=0">
                                           <xsl:value-of select="format-number(ancestor::enrollment/stateSubsidy,'#0.00')" />
                                       </xsl:if>
                                   </othPayAmt1>
                              </xsl:if>
                              <xsl:if test="totalIndivResponsibilityAmt">
                                 <healthCoveragePremiumDate>
                                    <xsl:value-of select="concat(substring(healthCoveragePremiumDate, 1, 4), substring(healthCoveragePremiumDate, 6, 2), substring(healthCoveragePremiumDate, 9, 2))" />
                                    <xsl:if test="not(healthCoveragePremiumDate)">
                                   		<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                                  	</xsl:if>
                                 </healthCoveragePremiumDate>
                                 <healthCoveragePremiumAmt>
                                    <xsl:value-of select="format-number(totalIndivResponsibilityAmt,'#0.00')" />
                                 </healthCoveragePremiumAmt>
                              </xsl:if>
                              <xsl:if test="ratingArea">
                                 <ratingAreaDate>
                                 	<xsl:value-of select="concat(substring(ratingAreaEffDate, 1, 4), substring(ratingAreaEffDate, 6, 2), substring(ratingAreaEffDate, 9, 2))" />
                                    <xsl:if test="not(ratingAreaEffDate)">
                                    <xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                                    </xsl:if>
                                 </ratingAreaDate>
                                 <ratingArea>
                                    <xsl:value-of select="ratingArea" />
                                 </ratingArea>
                              </xsl:if>
                              <xsl:if test="netPremiumAmt">
                                 <totalIndivResponsibilityDate>
                                    <xsl:value-of select="concat(substring(totalIndivResponsibilityDate, 1, 4), substring(totalIndivResponsibilityDate, 6, 2), substring(totalIndivResponsibilityDate, 9, 2))" />
                                    <xsl:if test="not(totalIndivResponsibilityDate)">
                                   		<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                                  	</xsl:if>
                                 </totalIndivResponsibilityDate>
                                 <totalIndivResponsibilityAmt>
                                    <xsl:value-of select="format-number(netPremiumAmt,'#0.00')" />
                                 </totalIndivResponsibilityAmt>
                              </xsl:if>
                           </xsl:if>
                           <!-- Subscriber Condition Ends Here -->
                           
                           <xsl:if test="translate(personType/lookupValueCode,$UPPER,$lower)!='subscriber'">
                              <xsl:if test="totalIndivResponsibilityAmt">
                                 <healthCoveragePremiumDate>
                                    <xsl:value-of select="concat(substring(healthCoveragePremiumDate, 1, 4), substring(healthCoveragePremiumDate, 6, 2), substring(healthCoveragePremiumDate, 9, 2))" />
                                    <xsl:if test="not(healthCoveragePremiumDate)">
                                   		<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                                  	</xsl:if>
                                 </healthCoveragePremiumDate>
                                 <healthCoveragePremiumAmt>
                                    <xsl:value-of select="format-number(totalIndivResponsibilityAmt,'#0.00')" />
                                 </healthCoveragePremiumAmt>
                              </xsl:if>
                           </xsl:if>
                           <xsl:if test="translate(personType/lookupValueCode,$UPPER,$lower)='subscriber'">
                              <xsl:if test="employerContribution">
                                 <totalEmpResponsibilityDate>
                                    <xsl:value-of select="concat(substring(totalEmpResponsibilityDate, 1, 4), substring(totalEmpResponsibilityDate, 6, 2), substring(totalEmpResponsibilityDate, 9, 2))" />
                                 	<xsl:if test="not(totalEmpResponsibilityDate)">
                                   		<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                                  	</xsl:if>
                                 </totalEmpResponsibilityDate>
                                 <totalEmpResponsibilityAmt>
                                    <xsl:value-of select="format-number(employerContribution,'#0.00')" />
                                 </totalEmpResponsibilityAmt>
                              </xsl:if>
                              <xsl:if test="healthCoveragePremiumAmt">
                                 <totalPremiumDate>
                                    <xsl:value-of select="concat(substring(totalPremiumDate, 1, 4), substring(totalPremiumDate, 6, 2), substring(totalPremiumDate, 9, 2))" />
                                    <xsl:if test="not(totalPremiumDate)">
                                   		<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                                  	</xsl:if>
                                 </totalPremiumDate>
                                 <totalPremiumAmt>
                                    <xsl:value-of select="format-number(healthCoveragePremiumAmt,'#0.00')" />
                                 </totalPremiumAmt>
                              </xsl:if>
                           </xsl:if>
                        </xsl:if>
                        <!-- 024 Condition ends here -->
                        <xsl:if test="enrollmentEvents[1]/spclEnrollmentReasonLkp">
                           <sepReasonDate>
                           		<xsl:value-of select="concat(substring(enrollmentEvents[1]/createdOn, 1, 4), substring(enrollmentEvents[1]/createdOn, 6, 2), substring(enrollmentEvents[1]/createdOn, 9, 2))" />
                           </sepReasonDate>
                           <sepReason>
                                 <xsl:value-of select="enrollmentEvents[1]/spclEnrollmentReasonLkp/lookupValueCode" />-<xsl:value-of select="enrollmentEvents[1]/spclEnrollmentReasonLkp/lookupValueLabel" />
                           </sepReason>
                        </xsl:if>
                        <xsl:if test="enrollmentEvents[1]/qleIdentifier and translate(ancestor::enrollments/stateCode,$UPPER,$lower)!='ca' ">
                       		<qleType>SEP</qleType>
               				<qleReasonDate>
               					<xsl:value-of select="concat(substring(ancestor::enrollment/createdOn, 1, 4), substring(ancestor::enrollment/createdOn, 6, 2), substring(ancestor::enrollment/createdOn, 9, 2))" />
               				</qleReasonDate>
               				<qleReason>
               					<xsl:value-of select="enrollmentEvents[1]/qleIdentifier"></xsl:value-of>
               				</qleReason>
                        </xsl:if>
                        
                        <!-- <xsl:if test="translate(enrolleeLkpValue/lookupValueCode,$UPPER,$lower)!='pending' and translate(enrolleeLkpValue/lookupValueCode,$UPPER,$lower)!='payment_received' and translate(enrolleeLkpValue/lookupValueCode,$UPPER,$lower)!='confirm'"> -->
                        <xsl:if test="$eventTypeLkpVar='024'  or contains( renewalStatus, 'REN')">
							<xsl:if test="$eventTypeLkpVar='024'">
							<additionalMaintReasonDate>
								<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
							</additionalMaintReasonDate>
							<additionalMaintReason>
											<lookupValueCode><xsl:value-of select="enrolleeLkpValue/lookupValueCode" /></lookupValueCode>
								</additionalMaintReason>
								</xsl:if>
								<xsl:if test="$eventTypeLkpVar!='024' and translate(ancestor::enrollments/stateCode,$UPPER,$lower)!='ca' ">
								<additionalMaintReasonDate>
									<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
								</additionalMaintReasonDate>
								<additionalMaintReason>
									<xsl:choose>
										<xsl:when test="translate(renewalStatus,$UPPER,$lower)='ren'">
										<lookupValueCode>RENEW</lookupValueCode>
										</xsl:when>
										<xsl:when test="translate(renewalStatus,$UPPER,$lower)='renp'">
											<lookupValueCode>AUTORENEW</lookupValueCode>
										</xsl:when>
									</xsl:choose>
							</additionalMaintReason>
						</xsl:if>
						</xsl:if>
                        <xsl:if test="$eventTypeLkpVar!='024'">
                        	<sourceExchgIDDate>
                           		<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                        	</sourceExchgIDDate>
                        	<sourceExchgID>
                           		<xsl:value-of select="ancestor::enrollments/sourceExchgID" />
                        	</sourceExchgID>
                        </xsl:if>
                       <xsl:if test ="premium">
                        <monthlyPremiums>
                         <xsl:for-each select="premium">
                          <xsl:if test="grossPremiumAmt and grossPremiumAmt &gt; 0 ">
                         	<premium>
                          	   <monthStartDate>
                          	   		<xsl:variable name="monthNumVar" select="format-number(monthNum, '00')" />
                          	   		<xsl:choose>
                          	   			<xsl:when test="substring($benefitStartDateVar, 6, 2) = $monthNumVar">
                          	   				<!-- For Initial Month populate the Benefit Start Date -->
                          	   				<xsl:value-of select="concat(substring($benefitStartDateVar, 1, 4), substring($benefitStartDateVar, 6, 2), substring($benefitStartDateVar, 9, 2))" />
                          	   			</xsl:when>
                          	   			<xsl:when test="substring($benefitStartDateVar, 6, 2) != $monthNumVar">
                          	   				<!-- For Rest of Month populate the Month's Start Date -->
                          	   				<xsl:value-of select="concat(yearNum, $monthNumVar, '01')" />
                          	   			</xsl:when>
                          	   		</xsl:choose>
                          	   </monthStartDate>
                         	   <monthNum>
                       			 <xsl:value-of select="monthNum" />
                    			</monthNum>
                         		<yearNum>
                       				 <xsl:value-of select="yearNum" />
                    			 </yearNum>
                    			 <xsl:if test="grossPremiumAmt">  
                    			  <grossPremiumAmt>
                       				 <xsl:value-of select="format-number(grossPremiumAmt,'#0.00')" />
                    			 </grossPremiumAmt>
                    			</xsl:if>
                    			<xsl:if test="aptcAmt"> 
                    			 <aptcAmt>
                       				 <xsl:value-of select="format-number(aptcAmt,'#0.00')" />
                    			 </aptcAmt>
                    			</xsl:if>
                    			<xsl:if test="netPremiumAmt">
                    			<netPremiumAmt>
                       				 <xsl:value-of select="format-number(netPremiumAmt, '#0.00')" />
                    			 </netPremiumAmt> 
                    			</xsl:if>
                         	</premium>
                          </xsl:if>
                           </xsl:for-each>
                         <!-- adding cretedOn as End Tag for monthly loop -->
                         <createdOn>
                           <xsl:value-of select="concat(substring(createdOn, 1, 4), substring(createdOn, 6, 2), substring(createdOn, 9, 2))" />
                        </createdOn>
                        </monthlyPremiums>
                      </xsl:if>
                        <createdOn>
                           <xsl:value-of select="concat(substring(createdOn, 1, 4), substring(createdOn, 6, 2), substring(createdOn, 9, 2))" />
                        </createdOn>
                     </memberReportingCategory>
                     <!-- HIX-88192 Changes ends here -->
                     <createdOn>
                        <xsl:value-of select="concat(substring(createdOn, 1, 4), substring(createdOn, 6, 2), substring(createdOn, 9, 2))" />
                     </createdOn>
                  </enrollee>
               </xsl:for-each>
               <createdOn>
                  <xsl:value-of select="concat(substring(createdOn, 1, 4), substring(createdOn, 6, 2), substring(createdOn, 9, 2))" />
               </createdOn>
            </enrollment>
         </xsl:for-each>
          </GS_Info>
      </enrollments>
   </xsl:template>
</xsl:stylesheet>