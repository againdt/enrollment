<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"  byte-order-mark="no"  /> 
	<xsl:template match="/">
	<xsl:variable name="batchExecutionIdVar" select="enrollments/jobExecutionId" />
	<xsl:variable name="hiosIssuerIdVar" select="enrollments/HIOS_Issuer_ID" />
	<enrollmentReconciliationReportList>
		<fileCreationDate><xsl:value-of select="enrollments/txnCreateDateTime"/></fileCreationDate>
		    <xsl:for-each select="enrollments/GS_Info">
			<xsl:for-each select="enrollment">
				<xsl:variable name="insurerNameVar" select="insurerName" />
				<xsl:variable name="brokerAgentNameVar" select="brokerAgentName" />
				<xsl:variable name="brokerFEDTaxPayerIdVar" select="brokerFEDTaxPayerId" />
				<xsl:variable name="qTYtVar" select="QTYt" />
				<xsl:for-each select="enrollee">
					<!-- Adding Reconciliation at Subscriber level -->
					<xsl:if test="subscriberFlag = 'Y'">
				<EnrollmentReconciliationReport>
					<batchExecutionId><xsl:value-of select="$batchExecutionIdVar"/></batchExecutionId>
					<!-- <createdOn><xsl:value-of select="createdOn"/></createdOn> -->
					<enrollmentId><xsl:value-of select="exchgAssignedPolicyID"/></enrollmentId>
					<subscriberEventId><xsl:value-of select="enrollmentEventId"/></subscriberEventId>
					<hiosIssuerId><xsl:value-of select="$hiosIssuerIdVar"/></hiosIssuerId>
					<subscriberMemberId><xsl:value-of select="exchgIndivIdentifier"/></subscriberMemberId>
					<householdOrEmployeeCaseID><xsl:value-of select="healthCoverage/householdOrEmployeeCaseID"/></householdOrEmployeeCaseID>
					<insurerName><xsl:value-of select="$insurerNameVar"/></insurerName>
							<maintenanceType><xsl:value-of select="enrollmentEvents/eventTypeLkp/lookupValueCode"/></maintenanceType>		
							<maintenanceReasonCode><xsl:value-of select="enrollmentEvents/eventReasonLkp/lookupValueCode"/></maintenanceReasonCode>         
							<renpFlag><xsl:value-of select="memberReportingCategory/renewalStatus"/></renpFlag>
							<coverageStartDate><xsl:value-of select="healthCoverage/benefitEffectiveBeginDate"/></coverageStartDate>         
							<coverageEndDate><xsl:value-of select="healthCoverage/benefitEffectiveEndDate"/></coverageEndDate>         
							<totalQTY><xsl:value-of select="$qTYtVar"/></totalQTY>         
							<agentName><xsl:value-of select="$brokerAgentNameVar"/></agentName>         
							<agentID><xsl:value-of select="$brokerFEDTaxPayerIdVar"/></agentID>
				</EnrollmentReconciliationReport>
					</xsl:if>
			</xsl:for-each>
			</xsl:for-each>
			</xsl:for-each>
	</enrollmentReconciliationReportList>
	</xsl:template>
</xsl:stylesheet>