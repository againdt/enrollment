<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ext="http://birsrep.dsh.cms.gov/extension/1.0" xmlns:hix-core="http://hix.cms.gov/0.1/hix-core"
	xmlns:i="http://niem.gov/niem/appinfo/2.0" xmlns:nc="http://niem.gov/niem/niem-core/2.0"
	xmlns:niem-xsd="http://niem.gov/niem/proxy/xsd/2.0" xmlns:s="http://niem.gov/niem/structures/2.0"
	xmlns:exch="http://birsrep.dsh.cms.gov/exchange/1.0" xmlns:ns0="http://niem.gov/niem/appinfo/2.1"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"
		byte-order-mark="no" cdata-section-elements="" />
	<xsl:template match="/">
		<exch:BatchHandlingServiceRequest>
			<hix-core:BatchMetadata>
				<hix-core:BatchID>
					<xsl:value-of select="batchTransmission/batchID" />
				</hix-core:BatchID>
				<hix-core:BatchPartnerID>
					<xsl:value-of select="batchTransmission/batchPartnerID" />
				</hix-core:BatchPartnerID>
				<hix-core:BatchAttachmentTotalQuantity>
					<xsl:value-of select="batchTransmission/batchAttachmentTotalQuantity" />
				</hix-core:BatchAttachmentTotalQuantity>
				<ext:BatchCategoryCode>
					<xsl:value-of select="batchTransmission/batchCategoryCode" />
				</ext:BatchCategoryCode>
				<hix-core:BatchTransmissionQuantity>
					<xsl:value-of select="batchTransmission/batchTransmissionQuantity" />
				</hix-core:BatchTransmissionQuantity>
			</hix-core:BatchMetadata>
			<hix-core:TransmissionMetadata>
				<hix-core:TransmissionAttachmentQuantity>
					<xsl:value-of select="batchTransmission/transmissionAttachmentQuantity" />
				</hix-core:TransmissionAttachmentQuantity>
				<hix-core:TransmissionSequenceID>
					<xsl:value-of select="batchTransmission/transmissionSequenceID" />
				</hix-core:TransmissionSequenceID>
			</hix-core:TransmissionMetadata>
			<ext:ServiceSpecificData>
				<ext:ReportPeriod>
					<nc:YearMonth>
						<xsl:value-of select="batchTransmission/reportPeriod" />
					</nc:YearMonth>
				</ext:ReportPeriod>
				<ext:OriginalBatchID>
					<xsl:value-of select="batchTransmission/originalBatchID" />
				</ext:OriginalBatchID>
			</ext:ServiceSpecificData>
			<xsl:for-each select="batchTransmission/attachment">
				<ext:Attachment>
					<nc:DocumentBinary>
			<!-- 			<hix-core:ChecksumAugmentation>
							<hix-core:MD5ChecksumText>
								<xsl:value-of select="MD5ChecksumText" />
							</hix-core:MD5ChecksumText>
						</hix-core:ChecksumAugmentation> -->
						<hix-core:ChecksumAugmentation>
							<ext:SHA256HashValueText>
								<xsl:value-of select="SHA256HashValueText" />
							</ext:SHA256HashValueText>
						</hix-core:ChecksumAugmentation>
						<hix-core:BinarySizeValue>
							<xsl:value-of select="binarySizeValue" />
						</hix-core:BinarySizeValue>
					</nc:DocumentBinary>
					<nc:DocumentFileName>
						<xsl:value-of select="documentFileName" />
					</nc:DocumentFileName>
					<nc:DocumentSequenceID>
						<xsl:value-of select="documentSequenceID" />
					</nc:DocumentSequenceID>
				</ext:Attachment>
			</xsl:for-each>
		</exch:BatchHandlingServiceRequest>
	</xsl:template>
</xsl:stylesheet>